<?php
//impide la llamada directa del archivo
if (!defined("MAIN")) {
  die("Inicialización directa del archivo no permitida");
}
require_once('inc/Factory.class.php');
?>
<div class="secc_bar">
  Menú
</div>

<?php
if (!in_array($idUsuario, array(2, 12, 13, 19, 3, 22, 24, 34, 57, 126, 149, 150, 151,198, 242))) {
  ?>

  <div class="accordion">

    <h2>Datos Maestros</h2>
    <div class="pane">
      <div class="accordion">

        <!-- Menu de clientes -->
        <h2>Clientes</h2>
        <div class="pane">
          <ul>
            <?php if (check_permiso(43)): ?>
              <li><a href="sites/clientes/cr_cliente.php">Creación</a></li>
            <?php endif;
            if (check_permiso(45)): ?>
              <li><a href="sites/clientes/src_cliente.php">Gestion</a></li>
            <?php endif;
            if (check_permiso(53)): ?>
              <li><a href="sites/clientes/src_other_fields.php">Base de conocimiento</a></li>
  <?php endif; ?>
          </ul>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/src_tipo_cliente.php">Tipos de cliente</a></li>
                <li><a href="sites/mantenedores/src_mercado.php">Mercados</a></li>
                <li><a href="sites/mantenedores/src_tipo_direccion_cliente.php">Tipos de dirección</a></li>
                <li><a href="sites/mantenedores/clientes/src_medio_pago.php">Medios de pago</a></li>
                <li><a href="sites/mantenedores/src_cargos_contacto.php">Cargos contactos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu de clientes -->

        <!-- menu de proveedores -->
        <h2>Proveedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/proveedores/cr_proveedor.php">Creación</a></li>
            <li><a href="sites/proveedores/src_proveedor.php">Gestión</a></li>
          </ul>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/src_tipo_proveedor.php">Tipos de proveedor</a></li>
                <li><a href="sites/mantenedores/src_mercado.php">Mercados</a></li>
                <li><a href="sites/mantenedores/src_tipo_direccion_proveedor.php">Tipos de dirección</a></li>
                <li><a href="sites/mantenedores/src_cargos_contacto.php">Cargos contactos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu de proveedores -->

        <!-- menu de productos -->
        <h2>Productos</h2>
        <div class="pane">
          <ul>
  <?php if (check_permiso(70)): ?>
              <li><a href="sites/productos/src_producto.php">Gestión</a></li>
              <li><a href="sites/productos/cr_producto.php">Creación</a></li>
              <li><a href="<?php echo Factory::buildUrl('CrearProducto', 'productos', 'index') ?>">crearFactory</a></li>
              <li><a href="sites/productos/ed_producto.php">Edición</a></li>
              <li><a href="sites/productos/del_producto.php">Eliminar</a></li>
  <?php endif ?>
          </ul>
          <div class="accordion">
            <h2>Costeos</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/productos/costeo/cr_costeo.php">Crear costeo</a></li>
              </ul>
            </div>
          </div>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/src_marca_producto.php">Marcas</a></li>
                <li><a href="sites/mantenedores/src_tipo_producto.php">Tipos de producto</a></li>
                <li><a href="sites/mantenedores/src_linea_negocio_producto.php">Lineas de negocio</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu de productos -->

        <!-- menu estructura empresa -->
        <h2>Estructura empresa</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/est_empresa/src_ceco.php">Estructura CeCo</a></li>
            <li><a href="sites/est_empresa/src_cebe.php">Estructura CeBe</a></li>
          </ul>
          <div class="accordion">

            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/est_empresa/src_segmento.php">Segmentos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu estructura empresa -->

        <!-- mantenedores -->
        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/src_comuna.php">Comunas</a></li>
            <li><a href="sites/mantenedores/src_region.php">Regiones</a></li>
            <li><a href="sites/mantenedores/src_empresa.php">Empresas</a></li>
          </ul>
        </div>
        <!-- END: mantenedores -->

      </div>
    </div>

    <h2>Contabilidad</h2>
    <div class="pane">
      <div class="accordion">
        <h2>Comprobantes contables</h2>
        <div class="pane">
          <ul>
            <?php
            if (check_permiso(1))
              echo('<li><a href="' . Factory::buildUrl('crearComprobante', 'contabilidad') . '">Crear</a></li>');
            if (check_permiso(5))
              echo('<li><a href="sites/contabilidad/src_comprobante_contable.php">Consulta</a></li>');
            ?>
          </ul>
        </div>

        <h2>Informes</h2>
        <div class="pane">
          <ul>
            <?php
            if (check_permiso(6))
              echo('<li><a href="sites/contabilidad/informes/src_cuenta_contable.php">Cuentas contables</a></li>');
            if (check_permiso(7))
              echo('<li><a href="sites/contabilidad/informes/src_libro_mayor.php">Libro mayor</a></li>');
            if (check_permiso(8))
              echo('<li><a href="sites/contabilidad/informes/src_balance_general.php">Balance General</a></li>');
            ?>
            <li><a href="<?php echo Factory::buildUrl('InformeExistencias', 'contabilidad', 'index', 'informe_existencias') ?>">Informe de Existencias</a></li>
            <li><a href="sites/contabilidad/src_centralizacion.php">Centralización</a></li>
            <li><a href="<?php echo Factory::buildUrl('CentralizacionCompras', 'contabilidad', 'index', 'informes') ?>">Centralizacion libro Compras</a></li>
            <li><a href="<?php echo Factory::buildUrl('ConsultaCentralizacionCompras', 'contabilidad', 'index', 'informes') ?>">Consulta Centralizacion</a></li>
          </ul>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <?php
                if (check_permiso(9))
                  echo('<li><a href="sites/mantenedores/contabilidad/informes/src_libro_mayor_folio.php">Folios libro mayor</a></li>');
                ?>
              </ul>
            </div>
          </div>
        </div>


        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/contabilidad/src_cuenta_contable.php">Cuentas contables</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_grupo_cuenta.php">Grupos de cuentas</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_tipo_movimiento.php">Tipos de movimiento</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_periodo_contable.php">Periodos contables</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_tipo_documento.php">Tipos de documento</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_banco.php">Bancos</a></li>
          </ul>
        </div>
        
        <h2>Centralización</h2>
        <div class="pane">
            <ul>
                <li><a href="<?php echo Factory::buildUrl('CentralizacionCompras', 'contabilidad', 'index', 'informes') ?>">Centralización libro Compras</a></li>
                <li><a href="<?php echo Factory::buildUrl('ConsultaCentralizacionCompras', 'contabilidad', 'index', 'informes') ?>">Gestión Centralización Compra</a></li>
                <li><a href="<?php echo Factory::buildUrl('CentralizacionVentas', 'contabilidad', 'index', 'informes') ?>">Centralización libro Ventas</a></li>
                <li><a href="<?php echo Factory::buildUrl('ConsultaCentralizacionVentas', 'contabilidad', 'index', 'informes') ?>">Gestión Centralización Ventas</a></li>
            </ul>
        </div>
        
      </div>
    </div>

    <!-- END: menu de contabilidad -->

    <h2>Finanzas</h2>
    <div class="pane">
      <div class="accordion">

        <h2>Pagos a proveedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/finanzas/pagos/src_informe_pago.php">Informe de pagos</a></li>
            <li><a href="sites/finanzas/pagos/src_propuesta_pago.php">Propuestas de pago</a></li>
            <li><a href="sites/finanzas/pagos/src_giros.php">Confirmar Giros</a></li>
			<?php if (check_permiso(87)): ?>
			<li><a href="<?php echo Factory::buildUrl('PagoFacturaVenta', 'finanzas', 'index', 'pagos') ?>" >Pago con Factura de Venta</a></li>
			<?php endif; ?>
          </ul>
        </div>

        <h2>Cobros a Clientes</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/finanzas/cobros/src_informe_cobranza.php">Informe de cobranza</a></li>
            <li><a href="sites/finanzas/cobros/src_giros.php">Confirmar Giros</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/finanzas/src_banco.php">Bancos</a></li>
            <li><a href="sites/finanzas/pagos/src_medio_pago.php">Medios de pago</a></li>
            <li><a href="sites/finanzas/cobros/src_medio_cobro.php">Medios de cobro</a></li>
          </ul>
        </div>

      </div>
    </div>

    <!-- Menú ventas -->

    <h2>Ventas</h2>
    <div class="pane">
      <div class="accordion">

        <h2>Oportunidad</h2>
        <div class="pane">
          <ul>
            <li><a href="<?php echo Factory::buildUrl('CrearOportunidad', 'ventas', 'index', 'oportunidad') ?>">Crear</a></li>
            <!--<li><a href="sites/ventas/oportunidad/cr_oportunidad.php">Crear</a></li>-->
            <li><a href="<?php echo Factory::buildUrl('ConsultarOportunidad', 'ventas', 'index', 'oportunidad') ?>">Consultar</a></li>
            <!--<li><a href="sites/ventas/oportunidad/src_oportunidad.php">Consulta</a></li>-->
            <li><a href="sites/ventas/oportunidad/informe_oportunidad.php">Informe de Oportunidades</a></li>
            <li><a href="<?php echo Factory::buildUrl('InformeOp', 'ventas', 'mostrarPorFiltro', 'oportunidad') ?>">Informe de Oportunidades II</a></li>
          </ul>
        </div>

        <h2>Cotización</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/cr_cotizacion.php">Crear</a></li>
            <li><a href="sites/ventas/src_cotizacion.php">Consulta</a></li>
            <li><a href="sites/ventas/informes/src_informe_cotizaciones.php">Informes</a></li>
            <li><a href="<?php echo Factory::buildUrl('Forecast', 'ventas', 'index', 'cotizacion') ?>">Forecast</a></li>
            <li><a href="sites/ventas/cotizacion_cextra.php">Contactos extra</a></li>
          </ul>
        </div>

        <h2>Nota de venta</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/nota_venta/cr_nota_venta.php">Crear</a></li>
            <li><a href="sites/ventas/nota_venta/src_nota_venta.php">Consultar</a></li>
            <li><a href="sites/ventas/nota_venta/src_informe_estado.php">Informe de estado</a></li>
            <li><a href="sites/ventas/nota_venta/src_pipeline.php">Pipeline</a></li>
            <li><a href="sites/ventas/nota_venta/src_contacto_extra.php">Contactos extra</a></li>
            <li><a href="<?php echo Factory::buildUrl('InformeArriboRenovacion', 'ventas', 'index', 'nota_venta') ?>">Informe Arribo/Renovacion</a></li>
          </ul>
        </div>

        <h2>Factura de venta</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/factura_venta/cr_factura_venta.php">Crear</a></li>
            <li><a href="sites/ventas/factura_venta/src_factura_venta.php">Consulta</a></li>
            <li><a href="sites/ventas/factura_venta/gestor_factura_venta.php">Gestor de facturas</a></li>
            <li><a href="sites/ventas/factura_venta/src_libro_venta.php">Libros de venta</a></li>
            <?php if (check_permiso(56)): ?>
              <li><a href="sites/ventas/factura_venta/src_informe_cobranza.php">Informe de cobranza</a></li>
  <?php endif; ?>
            <li><a href="sites/ventas/contrato/src_factura_masiva.php">Facturación contratos</a></li>
          </ul>
        </div>

        <h2>Factura de compra</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/factura_compra/cr_factura_compra.php">Crear</a></li>
            <li><a href="sites/ventas/factura_compra/src_factura_compra.php">Gestión</a></li>
            <?php if(check_permiso(100)):?>
            <li><a href="<?php echo Factory::buildUrl('AsignacionCostos', 'ventas', 'launch', 'factura_compra') ?>">Asignacion de Costos</a></li>
            <?php endif;?>
            <li><a href="sites/ventas/factura_compra/src_libro_compra.php">Libro de compra</a></li>
            <li><a href="sites/ventas/factura_compra/src_informe_compra.php">Informe de compras</a></li>
          </ul>
        </div>

        <h2>Boleta de honorario</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/boleta_honorario/cr_boleta_honorario.php">Crear</a></li>
            <li><a href="sites/ventas/boleta_honorario/src_boleta_honorario.php">Gestión</a></li>
          </ul>
        </div>

        <h2>Nota de Crédito</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/nota_credito/cr_nota_credito.php">Crear</a></li>
            <li><a href="sites/ventas/nota_credito/src_nota_credito.php">Consulta</a></li>
            <li><a href="<?php echo Factory::buildUrl('AsignarSaldo', 'ventas', 'index', 'nota_credito') ?>">Asignar Saldos</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="<?php echo Factory::buildUrl('TipoNotaCredito', 'maestros', 'crear', 'ventas') ?>">Tipos de notas de credito</a></li>
              </ul>
            </div>
          </div>
        </div>

        <h2>Nota de Crédito Proveedor</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/nota_credito_proveedor/cr_nota_credito.php">Crear</a></li>
            <li><a href="sites/ventas/nota_credito_proveedor/src_nota_credito.php">Consulta</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/ventas/nota_credito_proveedor/src_tipo_nota_credito.php">Tipos de notas de credito</a></li>
              </ul>
            </div>
          </div>

        </div>

        <h2>Guía de despacho</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/guia_despacho/cr_guia_despacho.php">Crear</a></li>
            <li><a href="sites/ventas/guia_despacho/src_guia_despacho.php">Consulta</a></li>
          </ul>
        </div>

        <h2>Orden de compra</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/orden_compra/cr_orden_compra.php">Crear</a></li>
            <li><a href="sites/ventas/orden_compra/src_orden_compra.php">Consulta</a></li>
            <li><a href="sites/ventas/orden_compra/gestor_orden_compra.php">Gestor de compras</a></li>
          </ul>
        </div>

        <h2>Guía de recepción</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/guia_recepcion/cr_guia_recepcion.php">Crear</a></li>
            <li><a href="sites/ventas/guia_recepcion/src_guia_recepcion.php">Gestión</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/ventas/src_tipo_guia_recepcion.php">Tipos de guía</a></li>
              </ul>
            </div>
          </div>

        </div>

        <h2>Informes</h2>
        <div class="pane">
          <ul>
            <li><a href="<?php echo Factory::buildUrl('InformeMargenes', 'ventas', 'index', 'factura_venta') ?>">Informe de Margenes</a></li>
            <li><a href="<?php echo Factory::buildUrl('ConsultaCentralizacionCostos', 'contabilidad', 'index', 'informes') ?>">Consulta Centralizacion</a></li>
            <li><a href="sites/ventas/guia_despacho/src_informe_guia_despacho.php">Informe Guías de despacho</a></li>
            <li><a href="sites/ventas/informes/src_informe_metas.php">Informe de Metas</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/ventas/src_tipo_cambio.php">Tipos de cambio</a></li>
            <li><a href="sites/mantenedores/ventas/src_modo_envio.php">Modos de envio</a></li>
            <li><a href="sites/mantenedores/ventas/src_termino_comercial.php">Términos comerciales</a></li>
            <li><a href="sites/mantenedores/ventas/src_estado_cotizacion.php">Estados cotización</a></li>
            <li><a href="sites/mantenedores/ventas/src_estado_nota_venta.php">Estados nota de venta</a></li>
          </ul>
        </div>

      </div>
    </div>

    <!-- END: menú ventas -->

    <!--Menú servicios -->

    <h2>Area de servicios</h2>
    <div class="pane">
      <div class="accordion">
        <h2>Orden de servicio</h2>
        <div class="pane">
          <ul>
              <?php if (check_permiso(29)): ?>
              <li><a href="sites/servicios/cr_orden_servicio.php">Crear</a>
              <?php endif;
              if (check_permiso(30)): ?>
              <li><a href="sites/servicios/src_orden_servicio.php">Gestión</a>
  <?php endif; ?>
            <li><a href="sites/informes/main.php">Monitor</a></li>
            <li><a href="sites/servicios/src_informe_sucursal.php">Informe Sucursal</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/servicios/src_tipo_falla.php">Tipos de falla</a></li>
            <li><a href="sites/mantenedores/servicios/src_estado.php">Estados</a></li>
            <li><a href="<?php echo Factory::buildUrl('AreaServicio', 'maestros', 'crear', 'servicios') ?>">Áreas de servicio</a></li>
            <li><a href="<?php echo Factory::buildUrl('PerfilTecnico', 'maestros', 'crear', 'servicios') ?>">Perfil Técnico</a></li>
            <li><a href="<?php echo Factory::buildUrl('ChecklistFalla', 'maestros', 'crear', 'servicios') ?>">Checklist Falla</a></li>
          </ul>
        </div>
      </div>
    </div>

    <!--END: menú servicios -->

    <!-- menu logistica -->
    <h2>Logística</h2>
    <div class="pane">
      <div class="accordion">
        <h2>Bodega</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/src_stock_report.php">Informe de stock</a></li>
            <li><a href="sites/logistica/src_traspaso_simple.php">Traspaso simple</a></li>
  <?php if (check_permiso(74)): ?>
              <li><a href="sites/logistica/cr_ajuste_cantidad.php">Ajuste de cantidad</a></li>
  <?php endif ?>
            <li><a href="sites/logistica/src_ajuste_cantidad.php">Consulta ajustes</a></li>
            <li><a href="sites/logistica/cr_devolucion.php">Devolución de guías</a></li>
            <li><a href="sites/logistica/src_informe_movimientos.php">Informe de movimientos</a></li>
          </ul>
        </div>

        <h2>Rutas</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/cr_ruta.php">Crear</a></li>
            <li><a href="sites/logistica/src_ruta.php">Consulta</a></li>
          </ul>
        </div>

        <h2>Stock Reservado</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/src_liberar_nota_venta.php">Nota de Venta</a></li>
            <li><a href="<?php echo Factory::buildUrl('AsignacionStockLibre', 'logistica') ?>">Asignación Stock Libre</a></li>
          </ul>
        </div>

        <h2>Guía Despacho Proveedor</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/guia_despacho_proveedor/cr_guia_despacho.php">Crear</a></li>
            <li><a href="sites/logistica/guia_despacho_proveedor/src_guia_despacho.php">Gestión</a></li>
            <li><a href="sites/logistica/guia_despacho_proveedor/cr_devolucion.php" class="">Devolución proveedores</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/src_bodega.php">Bodegas</a></li>
            <li><a href="sites/logistica/src_tipo_movimiento.php">Tipos de movimiento</a></li>
          </ul>
        </div>

      </div>
    </div>
    <!-- END: menu logistica -->
    <h2>Recursos humanos</h2>
    <div class="pane">
      <ul>
          <?php if (check_permiso(78) || check_permiso(80) || check_permiso(81)): ?>
          <li><a href="<?php echo Factory::buildUrl('gestorFuncionario', 'rrhh', 'index') ?>">Gestion funcionarios</a></li>
          <?php       endif;?>
        <li><a href="<?php echo Factory::buildUrl('Evaluacion', 'rrhh') ?>">Evaluación</a></li>
      </ul>
    </div>
    <?php if (check_permiso(79)||check_permiso(82)||check_permiso(83)||check_permiso(84)): ?>
    <h2>Usuarios</h2>
    <div class="pane">

      <ul>
        <li><a href="sites/usuarios/gestor_usuarios.php">Gestion usuarios</a></li>
      </ul>

    </div>
    <?php endif; ?>
    <h2>Administración</h2>
    <div class="pane">
      <ul>
  <?php if (check_permiso(57)): ?>
          <li><a href="sites/admin/src_asterisk_phone.php">Informe asterisk</a></li>
  <?php endif; ?>
      </ul>
    </div>
  </div>

  <?php
}else {
  ?>
  <div class="accordion">

    <h2>Datos Maestros</h2>
    <div class="pane">
      <div class="accordion">

        <!-- Menu de clientes -->
        <h2>Clientes</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/clientes/cr_cliente.php">Creación</a></li>
            <li><a href="sites/clientes/src_cliente.php">Gestion</a></li>
            <li><a href="sites/clientes/src_informe_cliente.php">Informe de Clientes</a></li>
            <li><a href="sites/clientes/src_other_fields.php">Base de conocimiento</a></li>
          </ul>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/src_tipo_cliente.php">Tipos de cliente</a></li>
                <li><a href="sites/mantenedores/src_mercado.php">Mercados</a></li>
                <li><a href="sites/mantenedores/src_tipo_direccion_cliente.php">Tipos de dirección</a></li>
                <li><a href="sites/mantenedores/clientes/src_medio_pago.php">Medios de pago</a></li>
                <li><a href="sites/mantenedores/src_cargos_contacto.php">Cargos contactos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu de clientes -->

        <!-- menu de proveedores -->
        <h2>Proveedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/proveedores/cr_proveedor.php">Creación</a></li>
            <li><a href="sites/proveedores/src_proveedor.php">Gestión</a></li>
          </ul>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/src_tipo_proveedor.php">Tipos de proveedor</a></li>
                <li><a href="sites/mantenedores/src_mercado.php">Mercados</a></li>
                <li><a href="sites/mantenedores/src_tipo_direccion_proveedor.php">Tipos de dirección</a></li>
                <li><a href="sites/mantenedores/src_cargos_contacto.php">Cargos contactos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu de proveedores -->

        <!-- menu de productos -->
        <h2>Productos</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/productos/src_producto.php">Consulta</a></li>
            <li><a href="sites/productos/cr_producto.php">Creación</a></li>
            <li><a href="<?php echo Factory::buildUrl('CrearProducto', 'productos', 'index') ?>">crearFactory</a></li>
            <li><a href="<?php echo Factory::buildUrl('EditarProducto', 'productos', 'index') ?>">modificarFactory</a></li>
            <li><a href="<?php echo Factory::buildUrl('BuscarProducto', 'productos', 'index') ?>">consultarFactory</a></li>
            <li><a href="<?php echo Factory::buildUrl('EliminarProducto', 'productos', 'index') ?>">eliminarFactory</a></li>
            <li><a href="sites/productos/ed_producto.php">Edición</a></li>
            <li><a href="sites/productos/del_producto.php">Eliminar</a></li>
          </ul>
          <div class="accordion">
            <h2>Costeos</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/productos/costeo/cr_costeo.php">Crear costeo</a></li>
              </ul>
            </div>
          </div>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/src_marca_producto.php">Marcas</a></li>
                <li><a href="sites/mantenedores/src_tipo_producto.php">Tipos de producto</a></li>
                <li><a href="<?php echo Factory::buildUrl('LineaNegocio', 'maestros', 'crear', 'productos') ?>">Linea de Negocio</a></li>
                <!--li><a href="sites/mantenedores/src_linea_negocio_producto.php">Lineas de negocio</a></li-->
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu de productos -->

        <!-- menu estructura empresa -->
        <h2>Estructura empresa</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/est_empresa/src_ceco.php">Estructura CeCo</a></li>
            <li><a href="sites/est_empresa/src_cebe.php">Estructura CeBe</a></li>
          </ul>
          <div class="accordion">

            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/est_empresa/src_segmento.php">Segmentos</a></li>
              </ul>
            </div>
          </div>
        </div>
        <!-- END: menu estructura empresa -->

        <!-- mantenedores -->
        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/src_comuna.php">Comunas</a></li>
            <li><a href="sites/mantenedores/src_region.php">Regiones</a></li>
            <li><a href="sites/mantenedores/src_empresa.php">Empresas</a></li>
          </ul>
        </div>
        <!-- END: mantenedores -->

      </div>
    </div>

    <h2>Ventas</h2>
    <div class="pane">
      <div class="accordion">

        <h2>Oportunidad</h2>
        <div class="pane">
          <ul>
            <li><a href="<?php echo Factory::buildUrl('CrearOportunidad', 'ventas', 'index', 'oportunidad') ?>">Crear</a></li>
            <!--<li><a href="sites/ventas/oportunidad/cr_oportunidad.php">Crear</a></li>-->
            <li><a href="<?php echo Factory::buildUrl('ConsultarOportunidad', 'ventas', 'index', 'oportunidad') ?>">Consultar</a></li>
            <!--<li><a href="sites/ventas/oportunidad/src_oportunidad.php">Consulta</a></li>-->
            <li><a href="sites/ventas/oportunidad/informe_oportunidad.php">Informe de Oportunidades</a></li>
            <li><a href="<?php echo Factory::buildUrl('InformeOp', 'ventas', 'mostrarPorFiltro', 'oportunidad') ?>">Informe de Oportunidades II</a></li>
          </ul>
        </div>

        <h2>Cotización</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/cr_cotizacion.php">Crear</a></li>
            <li><a href="sites/ventas/src_cotizacion.php">Consulta</a></li>
            <li><a href="sites/ventas/informes/src_informe_cotizaciones.php">Informes</a></li>
            <li><a href="<?php echo Factory::buildUrl('Forecast', 'ventas', 'index', 'cotizacion') ?>">Forecast</a></li>
            <li><a href="sites/ventas/cotizacion_cextra.php">Contactos extra</a></li>
          </ul>
        </div>

        <h2>Nota de venta</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/nota_venta/cr_nota_venta.php">Crear</a></li>
            <li><a href="<?php echo Factory::buildUrl('ConsultaNotaVenta', 'ventas', 'index', 'nota_venta') ?>">Consultar</a></li>
            <li><a href="sites/ventas/nota_venta/src_informe_estado.php">Informe de estado</a></li>
            <li><a href="sites/ventas/nota_venta/src_pipeline.php">Pipeline</a></li>
            <li><a href="sites/ventas/nota_venta/src_contacto_extra.php">Contactos extra</a></li>
            <li><a href="<?php echo Factory::buildUrl('InformeArriboRenovacion', 'ventas', 'index', 'nota_venta') ?>">Informe Arribo/Renovacion</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/ventas/src_tipo_nota_venta.php">Tipos de venta</a></li>
              </ul>
            </div>
          </div>

        </div>

        <h2>Factura de venta</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/factura_venta/cr_factura_venta.php">Crear</a></li>
            <li><a href="sites/ventas/factura_venta/src_factura_venta.php">Consulta</a></li>
            <li><a href="sites/ventas/factura_venta/gestor_factura_venta.php">Gestor de facturas</a></li>
            <li><a href="sites/ventas/factura_venta/src_informe_cobranza.php">Informe de cobranza</a></li>
            <li><a href="sites/ventas/factura_venta/src_libro_venta.php">Libros de venta</a></li>
            <li><a href="sites/ventas/contrato/src_factura_masiva.php">Facturación contratos</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/ventas/src_tipo_operacion.php">Tipos de operación</a></li>
                <li><a href="sites/mantenedores/ventas/src_motivo_anulacion.php">Motivos de anulación</a></li>
                <li><a href="sites/mantenedores/ventas/src_tipo_cargo.php">Tipo de Cargo</a></li>
              </ul>
            </div>
          </div>

        </div>

        <h2>Factura de compra</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/factura_compra/cr_factura_compra.php">Crear</a></li>
            <li><a href="sites/ventas/factura_compra/src_factura_compra.php">Gestión</a></li>
            <li><a href="<?php echo Factory::buildUrl('AsignacionCostos', 'ventas', 'launch', 'factura_compra') ?>">Asignacion de Costos</a></li>
            <li><a href="sites/ventas/factura_compra/src_libro_compra.php">Libro de compra</a></li>
            <li><a href="sites/ventas/factura_compra/src_informe_compra.php">Informe de compras</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="<?php echo Factory::buildUrl('TipoFacturaCompra', 'maestros', 'crear', 'ventas') ?>">Tipos de Factura</a></li>
                <li><a href="sites/mantenedores/ventas/src_motivo_anulacion.php">Motivos de anulación</a></li>
              </ul>
            </div>
          </div>

        </div>

        <h2>Boleta de honorario</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/boleta_honorario/cr_boleta_honorario.php">Crear</a></li>
            <li><a href="sites/ventas/boleta_honorario/src_boleta_honorario.php">Gestión</a></li>
          </ul>
        </div>

        <h2>Nota de Crédito</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/nota_credito/cr_nota_credito.php">Crear</a></li>
            <li><a href="sites/ventas/nota_credito/src_nota_credito.php">Consulta</a></li>
            <li><a href="<?php echo Factory::buildUrl('AsignarSaldo', 'ventas', 'index', 'nota_credito') ?>">Asignar Saldos</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="<?php echo Factory::buildUrl('TipoNotaCredito', 'maestros', 'crear', 'ventas') ?>">Tipos de notas de credito</a></li>
              </ul>
            </div>
          </div>
        </div>

        <h2>Nota de Crédito Proveedor</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/nota_credito_proveedor/cr_nota_credito.php">Crear</a></li>
            <li><a href="sites/ventas/nota_credito_proveedor/src_nota_credito.php">Consulta</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/ventas/nota_credito_proveedor/src_tipo_nota_credito.php">Tipos de notas de credito</a></li>
              </ul>
            </div>
          </div>

        </div>

        <h2>Guía de despacho</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/guia_despacho/cr_guia_despacho.php">Crear</a></li>
            <li><a href="sites/ventas/guia_despacho/src_guia_despacho.php">Consulta</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/ventas/src_tipo_guia_despacho.php">Tipos de guía</a></li>
              </ul>
            </div>
          </div>

        </div>

        <h2>Orden de compra</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/orden_compra/cr_orden_compra.php">Crear</a></li>
            <li><a href="sites/ventas/orden_compra/src_orden_compra.php">Consulta</a></li>
            <li><a href="sites/ventas/orden_compra/gestor_orden_compra.php">Gestor de compras</a></li>
          </ul>
        </div>

        <h2>Guía de recepción</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/ventas/guia_recepcion/cr_guia_recepcion.php">Crear</a></li>
            <li><a href="sites/ventas/guia_recepcion/src_guia_recepcion.php">Gestión</a></li>
          </ul>

          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/ventas/src_tipo_guia_recepcion.php">Tipos de guía</a></li>
              </ul>
            </div>
          </div>
        </div>

        <h2>Informes</h2>
        <div class="pane">
          <ul>
            <li><a href="<?php echo Factory::buildUrl('InformeMargenes', 'ventas', 'index', 'factura_venta') ?>">Informe de Margenes</a></li>
            <li><a href="<?php echo Factory::buildUrl('ConsultaCentralizacionCostos', 'contabilidad', 'index', 'informes') ?>">Consulta Centralizacion</a></li>
            <li><a href="sites/ventas/guia_despacho/src_informe_guia_despacho.php">Informe Guías de despacho</a></li>
            <li><a href="sites/ventas/informes/src_informe_metas.php">Informe de Metas</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/ventas/src_tipo_cambio.php">Tipos de cambio</a></li>
            <li><a href="sites/mantenedores/ventas/src_modo_envio.php">Modos de envio</a></li>
            <li><a href="sites/mantenedores/ventas/src_termino_comercial.php">Términos comerciales</a></li>
            <li><a href="sites/mantenedores/ventas/src_estado_cotizacion.php">Estados cotización</a></li>
            <li><a href="sites/mantenedores/ventas/src_estado_nota_venta.php">Estados nota de venta</a></li>
            <li><a href="sites/ventas/meta/src_meta.php">Metas</a></li>
          </ul>
        </div>

      </div>
    </div>

    <!-- menu de contabilidad -->
    <h2>Contabilidad</h2>
    <div class="pane">
      <div class="accordion">
        <h2>Comprobantes contables</h2>
        <div class="pane">
          <ul>
            <li><a href="<?php echo Factory::buildUrl('crearComprobante', 'contabilidad') ?>">Crear</a></li>
            <li><a href="sites/contabilidad/src_comprobante_contable.php">Consulta</a></li>
          </ul>
        </div>

        <h2>Informes</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/contabilidad/informes/src_cuenta_contable.php">Cuentas contables</a></li>
            <li><a href="sites/contabilidad/informes/src_libro_mayor.php">Libro mayor</a></li>
            <li><a href="sites/contabilidad/informes/src_balance_general.php">Balance General</a></li>
            <li><a href="<?php echo Factory::buildUrl('InformeExistencias', 'contabilidad', 'index', 'informe_existencias') ?>">Informe de Existencias</a></li>
            <li><a href="sites/contabilidad/src_centralizacion.php">Centralización</a></li>
          </ul>
          <div class="accordion">
            <h2>Mantenedores</h2>
            <div class="pane">
              <ul>
                <li><a href="sites/mantenedores/contabilidad/informes/src_libro_mayor_folio.php">Folios libro mayor</a></li>
              </ul>
            </div>
          </div>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/contabilidad/src_cuenta_contable.php">Cuentas contables</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_grupo_cuenta.php">Grupos de cuentas</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_tipo_movimiento.php">Tipos de movimiento</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_periodo_contable.php">Periodos contables</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_tipo_documento.php">Tipos de documento</a></li>
            <li><a href="sites/mantenedores/contabilidad/src_banco.php">Bancos</a></li>
          </ul>
        </div>
        
        <h2>Centralización</h2>
        <div class="pane">
            <ul>
                
                <li><a href="<?php echo Factory::buildUrl('CentralizacionCompras', 'contabilidad', 'index', 'informes') ?>">Centralización libro Compras</a></li>
                <li><a href="<?php echo Factory::buildUrl('ConsultaCentralizacionCompras', 'contabilidad', 'index', 'informes') ?>">Gestión Centralización Compra</a></li>
                <li><a href="<?php echo Factory::buildUrl('CentralizacionVentas', 'contabilidad', 'index', 'informes') ?>">Centralización libro Ventas</a></li>
                <li><a href="<?php echo Factory::buildUrl('ConsultaCentralizacionVentas', 'contabilidad', 'index', 'informes') ?>">Gestión Centralización Ventas</a></li>
            </ul>
        </div>
        
      </div>
    </div>
    <!-- END: menu de proveedores -->

    <h2>Finanzas</h2>
    <div class="pane">
      <div class="accordion">

        <h2>Pagos a proveedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/finanzas/pagos/src_informe_pago.php">Informe de pagos</a></li>
            <li><a href="sites/finanzas/pagos/src_propuesta_pago.php">Propuestas de pago</a></li>
            <li><a href="sites/finanzas/pagos/src_giros.php">Confirmar Giros</a></li>
			<li><a href="<?php echo Factory::buildUrl('PagoFacturaVenta', 'finanzas', 'index', 'pagos') ?>" >Pago con Factura de Venta</a></li>
          </ul>
        </div>

        <h2>Cobros a Clientes</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/finanzas/cobros/src_informe_cobranza.php">Informe de cobranza</a></li>
            <li><a href="sites/finanzas/cobros/src_giros.php">Confirmar Giros</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/finanzas/src_banco.php">Bancos</a></li>
            <li><a href="sites/finanzas/pagos/src_medio_pago.php">Medios de pago</a></li>
            <li><a href="sites/finanzas/cobros/src_medio_cobro.php">Medios de cobro</a></li>
          </ul>
        </div>

      </div>
    </div>

    <h2>Area de servicios</h2>
    <div class="pane">
      <div class="accordion">
        <h2>Orden de servicio</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/servicios/cr_orden_servicio.php">Crear</a>
            <li><a href="sites/servicios/src_orden_servicio.php">Gestión</a>
            <li><a href="sites/servicios/src_informe_sucursal.php">Informe Sucursal</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/servicios/src_tipo_falla.php">Tipos de falla</a></li>
            <li><a href="sites/mantenedores/servicios/src_estado.php">Estados</a></li>
            <li><a href="<?php echo Factory::buildUrl('AreaServicio', 'maestros', 'crear', 'servicios') ?>">Áreas de servicio</a></li>
            <li><a href="<?php echo Factory::buildUrl('PerfilTecnico', 'maestros', 'crear', 'servicios') ?>">Perfil Técnico</a></li>
            <li><a href="<?php echo Factory::buildUrl('ChecklistFalla', 'maestros', 'crear', 'servicios') ?>">Checklist Falla</a></li>
          </ul>
        </div>
      </div>
    </div>

    <h2>Logística</h2>
    <div class="pane">
      <div class="accordion">
        <h2>Bodega</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/src_stock_report.php">Informe de stock</a></li>
            <li><a href="sites/logistica/src_traspaso_simple.php">Traspaso simple</a></li>
            <li><a href="sites/logistica/cr_ajuste_cantidad.php">Ajuste de cantidad</a></li>
            <li><a href="sites/logistica/src_ajuste_cantidad.php">Consulta ajustes</a></li>
            <li><a href="sites/logistica/cr_devolucion.php">Devolución de guías</a></li>
            <li><a href="sites/logistica/src_informe_movimientos.php">Informe de movimientos</a></li>
          </ul>
        </div>

        <h2>Rutas</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/cr_ruta.php">Crear</a></li>
            <li><a href="sites/logistica/src_ruta.php">Consulta</a></li>
          </ul>
        </div>

        <h2>Stock Reservado</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/src_liberar_nota_venta.php">Nota de Venta</a></li>
            <li><a href="<?php echo Factory::buildUrl('AsignacionStockLibre', 'logistica') ?>">Asignación Stock Libre</a></li>
          </ul>
        </div>

        <h2>Guía Despacho Proveedor</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/guia_despacho_proveedor/cr_guia_despacho.php">Crear</a></li>
            <li><a href="sites/logistica/guia_despacho_proveedor/src_guia_despacho.php">Gestión</a></li>
            <li><a href="sites/logistica/guia_despacho_proveedor/cr_devolucion.php" class="">Devolución proveedores</a></li>
          </ul>
        </div>

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/logistica/src_bodega.php">Bodegas</a></li>
            <li><a href="sites/logistica/src_tipo_movimiento.php">Tipos de movimiento</a></li>
          </ul>
        </div>

      </div>
    </div>

    <h2>Recursos humanos</h2>
    <div class="pane">
      <ul>
        <!--<li><a href="sites/rrhh/gestor_funcionarios.php">Gestion funcionarios</a></li>-->
        <li><a href="<?php echo Factory::buildUrl('gestorFuncionario', 'rrhh', 'index') ?>">Gestion funcionarios</a></li>
        <li><a href="sites/rrhh/cargo_funcionario.php">Cargo funcionarios</a></li>
        <li><a href="<?php echo Factory::buildUrl('Evaluacion', 'rrhh') ?>">Evaluación</a></li>
        <li><a href="<?php echo Factory::buildUrl('compromisos', 'rrhh') ?>">Compromisos</a></li>
      </ul>

      <div class="accordion">

        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/rrhh/src_afp.php">AFP</a></li>
            <li><a href="sites/mantenedores/rrhh/src_isapre.php">Isapres</a></li>
            <li><a href="sites/mantenedores/rrhh/src_tipo_contrato.php">Tipos de contrato</a></li>
            <li><a href="<?php echo Factory::buildUrl('crear_cargo', 'rrhh','index','mantenedor') ?>">Cargos</a></li>
            <li><a href="<?php echo Factory::buildUrl('preguntas', 'rrhh','index','mantenedor') ?>">Preguntas</a></li>
            <li><a href="<?php echo Factory::buildUrl('grupos', 'rrhh','index','mantenedor') ?>">Grupos</a></li>
            <li><a href="<?php echo Factory::buildUrl('Compromisos', 'rrhh','index','mantenedor') ?>">Compromisos</a></li>
            <li><a href="<?php echo Factory::buildUrl('AutorizaComp', 'rrhh','index','mantenedor') ?>">Autoriza Compromisos</a></li>
          </ul>
        </div>

      </div>

    </div>

    <h2>Informes</h2>
    <div class="pane">
      <ul>
        <li><a href="sites/informes/main.php">Monitor</a></li>
        <li><a href="sites/ventas/informes/src_informe_cotizaciones.php">Informe cotizaciones</a></li>
      </ul>
    </div>

    <h2>Usuarios</h2>
    <div class="pane">

      <ul>
        <li><a href="sites/usuarios/gestor_usuarios.php">Gestion usuarios</a></li>
        <li><a href="sites/usuarios/gestor_grupos.php">Gestion grupos</a></li>
      </ul>

    </div>

    <h2>Producción</h2>
    <div class="pane">
      <ul>
        <li><a href="sites/produccion/cr_proyecto.php">Crear proyecto</a></li>
        <li><a href="sites/produccion/src_proyecto.php">Gestion proyecto</a></li>
      </ul>

      <div class="accordion">
        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/src_marca_producto.php">Marcas</a></li>
            <li><a href="sites/mantenedores/produccion/src_categoria.php">Categorías</a></li>
            <li><a href="sites/mantenedores/produccion/src_material.php">Materiales</a></li>
            <li><a href="sites/mantenedores/produccion/src_unidad_medida.php">Unidad de medida</a></li>
            <li><a href="sites/mantenedores/produccion/src_servicio.php">Servicios</a></li>
          </ul>
        </div>
      </div>
    </div>

    <h2>Administración</h2>
    <div class="pane">
      <ul>
        <li><a href="sites/admin/src_asterisk_phone.php">Informe asterisk</a></li>
        <li><a href="sites/admin/gestor_empresa.php">Gestion empresas</a></li>
        <li><a href="sites/carga_excel/load_form.php">Carga masiva CC</a></li>
        <li><a href="sites/carga_excel/load_formasdasd.php">Carga masiva CC</a></li>
      </ul>
    </div>

    <h2>Contratos</h2>
    <div class="pane">
      <ul>
        <li><a href="sites/contratos/cr_contrato.php">Crear</a></li>
        <li><a href="sites/contratos/src_contrato.php">Gestión</a></li>
      </ul>

      <div class="accordion">
        <h2>Mantenedores</h2>
        <div class="pane">
          <ul>
            <li><a href="sites/mantenedores/contratos/src_tipo_contrato.php">Tipos de contrato</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
