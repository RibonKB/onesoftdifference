<?php
  $form->Start(Factory::buildActionUrl('procesarAsignarCuentasContables'),'cr_asignar_cuentas');
    $form->Header('Indique la cuenta contable que será asignada al tipo de Nota de Crédito administrativa.'.
                  '<br /><strong>Los campos marcados con [*] son obligatorios.</strong>');

    $form->Section();
      $form->DBSelect('Cuenta Contable','dc_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),true);
    $form->EndSection();

    $form->Hidden('dc_tipo_nota_credito',$tipo->dc_tipo_nota_credito);
  $form->Group();
  $form->End('Asignar Cuentas','addbtn');
?>
<table class="tab " width="100%">
  <caption>Cuentas contables ya asignadas al tipo de Nota de Crédito</caption>
  <thead>
    <tr>
      <th>Código</th>
      <th>Cuenta Contable</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($cuentas as $c): ?>
      <tr>
        <td><strong><?php echo $c->dg_codigo ?></strong></td>
        <td><?php echo $c->dg_cuenta_contable ?></td>
        <td>
          <a href="<?php Factory::buildActionUrl('deleteCuentaContable') ?>" class="loadOnOverlay">
            <img src="images/delbtn.png" alt="[X]" />
          </a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
