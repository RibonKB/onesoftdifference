<?php
$form->Start(Factory::buildActionUrl('procesarCrear'),'cr_tipo_nota_credito');
$form->Header('<b>Indique los datos para crear el tipo de nota de crédito</b><br />Los campos marcados con [*] son obligatorios');
    
    $form->Section();
      $form->Text('Nombre','dg_tipo_nota_credito',true);
    $form->EndSection();
    
    $form->Section();
      $form->RadioBox('Retorna Stock','dm_retorna_stock',array('NO','SI'),0,' ');
      $form->RadioBox('Facturación Anticipada','dm_retorna_anticipado',array('NO','SI'),0,' ');
      $form->DBSelect('Bodega de entrada','dc_bodega_entrada','tb_bodega',array('dc_bodega','dg_bodega'));
    $form->EndSection();

$form->End('Crear','addbtn');
?>