<?php
$form->Start($this->actionURL('procesarAsignarCuentaContable'),'cr_asignar_cuentas');
$form->Header('<b>Indique la cuenta contable que quiere agregar al tipo de factura</b><br />
			   Los campos marcados con [*] son obligatorios');
			   
	$form->DBSelect('Cuenta Contable','dc_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),true);
	
	$form->Hidden('dc_tipo',$tipo->dc_tipo);

$form->End('Agregar Cuenta','addbtn');
?>
<table class="tab" width="100%">
	<caption>Asociaciones actuales</caption>
    <thead>
    	<th>Código Cuenta Contable</th>
        <th>Cuenta Contable</th>
        <th>Opciones</th>
    </thead>
    <tbody>
    	<?php while($i = $list->fetch(PDO::FETCH_OBJ)): ?>
        <tr>
            <td><?php echo $i->dg_codigo ?></td>
            <td><?php echo $i->dg_cuenta_contable ?></td>
            <td>
            	<a href="<?php echo $this->actionURL('eliminarCuentaContable') ?>&dc_tipo=<?php echo $tipo->dc_tipo ?>&dc_cuenta_contable=<?php echo $i->dc_cuenta_contable ?>" class="loadOnOverlay">
                	<img src="images/delbtn.png" alt="(X)" title="Eliminar Cuenta" />
                </a>
            </td>
        </tr>
        <?php endwhile; ?>
    </tbody>
</table>