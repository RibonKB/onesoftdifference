<?php
  $form->Start(Factory::buildActionUrl('procesarEditar'),'ed_tipo_factura');
    $form->Header("Indique los nuevos datos asociados al tipo de factura<br />".
                  "<strong>Los datos marcados con [*] son obligatorios</strong>");
    $form->Section();
      $form->Text("Nombre","ed_dg_tipo",true,255,$tipo->dg_tipo);
    $form->EndSection();
    $form->Section();
      if($cuentas):
        $this->getErrorMan()->showAviso("El tipo de factura posee cuentas contables asociadas para la centralización, debido a esto no es posible cambiar el tipo de uso.");
      else:
        $form->RadioBox("Tipo uso",'ed_dm_factura_manual',array('General','Solo Factura Manual'),$tipo->dm_factura_manual);
      endif;
    $form->EndSection();
  $form->Hidden('dc_tipo',$tipo->dc_tipo);
  $form->End('Editar','editbtn');
?>
