<?php
$form->Start($this->actionURL('procesarCrear'),'cr_tipo_factura_compra');
$form->Header('<b>Indique los datos para crear el tipo de factura de compra</b><br />Los campos marcados con [*] son obligatorios');

	$form->Section();
		$form->Text('Nombre','dg_tipo',true);
	$form->EndSection();
	
	$form->Section();
		$form->RadioBox('Tipo uso','dm_factura_manual',array('General','Solo Factura Manual'));
	$form->EndSection();
	
$form->End('Crear','addbtn');
?>