<?php
$form->Start($this->actionURL('procesarEditar'),'ed_linea_negocio');
$form->Header('<b>Indique los datos actualizados de la linea de negocio</b><br />Los campos marcados con [*] son obligatorios');

	$form->Section();
		$form->Text('Nombre','dg_linea_negocio_ed',true,255,$linea_negocio->dg_linea_negocio);
        $form->DBSelect(
                'Cuenta Contable Provisión',
                'dc_cuenta_provision_ed',
                'tb_cuenta_contable',
                array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
                true,
                $linea_negocio->dc_cuenta_contable_provision);
				$form->DBSelect(
				        'Cuenta Contable Por Defecto Para Márgenes',
				        'dc_cuenta_margenes_ed',
				        'tb_cuenta_contable',
				        array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
				        true,
								$linea_negocio->dc_cuenta_contable_margenes);
        //$form->DBSelect('Cuenta Contable Desprovisión','dc_cuenta_desprovision_ed','tb_cuenta_contable',array('dc_cuenta_contable','dg_codigo','dg_cuenta_contable'),true,$linea_negocio->dc_cuenta_contable_desprovision);
	$form->EndSection();

	$form->Section();
		/*$form->DBSelect(
			'Cuenta Contable Servicios',
			'dc_cuenta_servicios_ed',
			'tb_cuenta_contable',
			array('dc_cuenta_contable','dg_codigo','dg_cuenta_contable'),
			true,
			$linea_negocio->dc_cuenta_contable_servicio
		);/**/
		$form->DBSelect(
			'Cuenta Contable Por Defecto Para compras',
			'dc_cuenta_compras_ed',
			'tb_cuenta_contable',
			array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
			true,
			$linea_negocio->dc_cuenta_contable_compra
		);
		$form->DBSelect(
			'Cuenta Contable Por Defecto Para ventas',
			'dc_cuenta_ventas_ed',
			'tb_cuenta_contable',
			array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
			true,
			$linea_negocio->dc_cuenta_contable_venta
		);
	$form->EndSection();

	$form->Hidden('dc_linea_negocio',$linea_negocio->dc_linea_negocio);

$form->End('Editar','editbtn');
?>
