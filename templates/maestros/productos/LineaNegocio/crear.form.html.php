<?php
$form->Start($this->actionURL('procesarCrear'),'cr_linea_negocio');
$form->Header('<b>Indique los datos para crear la nueva linea de negocio</b><br />Los campos marcados con [*] son obligatorios');

	$form->Section();
		$form->Text('Nombre','dg_linea_negocio',true);
        $form->DBSelect(
                'Cuenta Contable Provisión',
                'dc_cuenta_provision',
                'tb_cuenta_contable',
                array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
                true);
				$form->DBSelect(
				        'Cuenta Contable Por Defecto Para Márgenes',
				        'dc_cuenta_margenes',
				        'tb_cuenta_contable',
				        array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
				        true);
        //$form->DBSelect('Cuenta Contable Desprovisión','dc_cuenta_desprovision','tb_cuenta_contable',array('dc_cuenta_contable','dg_codigo','dg_cuenta_contable'),true);
	$form->EndSection();

	$form->Section();
		//$form->DBSelect('Cuenta Contable Servicios','dc_cuenta_servicios','tb_cuenta_contable',array('dc_cuenta_contable','dg_codigo','dg_cuenta_contable'),true);
		$form->DBSelect(
                'Cuenta Contable Por Defecto Para compras',
                'dc_cuenta_compras',
                'tb_cuenta_contable c',
                array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
                true);
		$form->DBSelect(
                'Cuenta Contable Por Defecto Para ventas',
                'dc_cuenta_ventas',
                'tb_cuenta_contable',
                array('dc_cuenta_contable','dg_cuenta_contable','dg_codigo'),
                true);
	$form->EndSection();

$form->End('Crear','addbtn');
?>
