<?php
$form->Start($this->actionURL('procesarAsignarBodegaCuentaContable'),'cr_asignar_cuentas');
$form->Header('<b>Indique la bodega y cuenta contable que se asociará a esta linea de negocio</b><br />
			   Los campos marcados con [*] son obligatorios');
$this->getErrorMan()->showAviso('Si selecciona una bodega que ya tenga una cuenta asociada se reemplazará');
	
	$form->Section();
		$form->DBSelect('Bodega','dc_bodega','tb_bodega',array('dc_bodega','dg_bodega'),true);
	$form->EndSection();
	
	$form->Section();
		$form->DBSelect('Cuenta Contable','dc_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),true);
	$form->EndSection();
	
	$form->Hidden('dc_linea_negocio',$linea_negocio->dc_linea_negocio);
	
$form->End('Agregar','addbtn');
?>
<table class="tab" width="100%">
	<caption>Asociaciones actuales</caption>
    <thead>
    	<th>Bodega</th>
    	<th>Código Cuenta Contable</th>
        <th>Cuenta Contable</th>
    </thead>
    <tbody>
    	<?php while($i = $list->fetch(PDO::FETCH_OBJ)): ?>
        <tr>
        	<td><?php echo $i->dg_bodega ?></td>
            <td><?php echo $i->dg_codigo ?></td>
            <td><?php echo $i->dg_cuenta_contable ?></td>
        </tr>
        <?php endwhile; ?>
    </tbody>
</table>