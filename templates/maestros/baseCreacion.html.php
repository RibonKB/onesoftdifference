<!-- Formulario del mantenedor, varia dependiendo del módulo y/o maestro -->
<?php echo $form ?>

<!-- Listado de elementos del maestro ya creado -->
<table class="tab" id="item_list" width="100%">
	<thead>
    	<tr>
        	<?php foreach($tableHeader as $title): ?>
            <th><?php echo $title ?></th>
            <?php endforeach; ?>
            <th>Fecha Creación</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
    	<?php while($i = array_shift($items)): ?>
        <tr>
        	<?php foreach($tableHeader as $field => $t): ?>
            <td><?php echo $i->$field ?></td>
            <?php endforeach ?>
            <td><?php echo $this->getConnection()->dateTimeLocalFormat($i->df_creacion) ?></td>
            <td>
            	<?php foreach($buttons as $image => $action): ?>
                <a href="<?php echo $this->actionURL($action, array($this->idField => $i->{$this->idField})) ?>" class="loadOnOverlay">
                	<img src="images/<?php echo $image ?>" alt="<?php echo $image ?>" width="18" />
                </a>
                <?php endforeach ?>
            </td>
        </tr>
        <?php endwhile ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#item_list').tableExport().tableAdjust(10);
</script>