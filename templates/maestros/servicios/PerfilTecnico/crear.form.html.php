<?php
$form->Start($this->actionURL('procesarCrear'),'cr_perfil_tecnico');
$form->Header("<b>Indique los datos para crear el perfil técnico</b><br />Los campos marcados con [*] son obligatorios");

  $form->Section();
      $form->Text('Nombre','dg_perfil', true);
      $form->ComboBox('','dm_es_supervisor',array('Perfil Supervisor'));
  $form->EndSection();
      
  $form->Section();
      $form->DBMultiSelect('Areas involucradas','dc_area_servicio','tb_area_servicio',array('dc_area_servicio','dg_area_servicio'));
      $form->DBMultiSelect('Funionarios','dc_funcionario','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
  $form->EndSection();
  
$form->End('Crear','addbtn');
?>
<script type="text/javascript">
  $('#dc_area_servicio, #dc_funcionario').multiSelect({
    selectAll: true,
    selectAllText: "Seleccionar todos",
    noneSelected: "---",
    oneOrMoreSelected: "% seleccionado(s)"
  });
</script>