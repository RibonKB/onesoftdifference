<?php
  $form->Start(AreaServicioFactory::buildActionUrl('procesarAsignarTecnicos'),'cr_asignar_tecnicos','overlayValidar');
  $form->Header("<b>Indique los técnicos asociados al área de servicio</b><br />Los campos marcados con [*] son obligatorios");
    
    $form->DBMultiSelect('Técnicos [*]','dc_tecnico','tb_funcionario',array(
        'dc_funcionario',
        'dg_nombres',
        'dg_ap_paterno',
        'dg_ap_materno'
       ),$tecnicos[0]);
  
    $form->Hidden('dc_area_servicio',$area->dc_area_servicio);
  $form->End('Asignar Técnicos','addbtn');
?>

<?php if(count($tecnicos[1])): ?>
<table class="tab bicolor_tab" width="100%">
  <caption>Técnicos asignados actualmente al área de servicio</caption>
  <tbody>
    <?php foreach($tecnicos[1] as $t): ?>
    <tr>
      <td><?php echo $t->dg_nombres.' '.$t->dg_ap_paterno.' '.$t->dg_ap_materno ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>

<script type="text/javascript">
  $('#dc_tecnico').multiSelect({
      selectAll: true,
      selectAllText: "Seleccionar todos",
      noneSelected: "---",
      oneOrMoreSelected: "% seleccionado(s)"
  });
</script>