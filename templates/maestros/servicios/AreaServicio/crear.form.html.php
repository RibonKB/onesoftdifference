<?php
$form->Start(AreaServicioFactory::buildActionUrl('procesarCrear'),'cr_area_servicio');
$form->Header("Indique un nombre y correos de los encargados en el área de servicio<br />Puede consultar los que ya están agregados.");
    $form->Section();
      $form->Text("Nombre","dg_area_servicio",1);
	$form->EndSection();
	$form->Section();
      $form->Textarea('E-mails encargados - <small>Separados con ; (punto y coma)</small>','dg_email',1);
	$form->EndSection();
$form->End('Crear','addbtn');
?>
