<?php $form->Start(AreaServicioFactory::buildActionUrl('procesarPerfilTecnico'),'cr_perfil_tecnico','overlayValidar'); ?>
<table class="tab bicolor_tab" width="100%">
  <thead>
    <tr>
      <th>Técnico/Especialización</th>
      <?php foreach($tipos_falla as $tf): ?>
      <th width="20"><?php echo $tf->dg_tipo_falla ?></th>
      <?php endforeach ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach($tecnicos as $t): ?>
    <tr>
      <td><?php echo $t->dg_nombres.' '.$t->dg_ap_paterno.' '.$t->dg_ap_materno ?></td>
      <?php
        foreach($tipos_falla as $tf):
            $value = $t->dc_tecnico.'_'.$tf->dc_tipo_falla;
      ?>
      <td align="center">
        <input type="checkbox" name="dc_tecnico_tipo_falla[]" value="<?php echo $value ?>" <?php if(in_array($value, $perfiles_activos)): ?>checked="checked"<?php endif ?> />
      </td>
      <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php $form->Hidden('dc_area_servicio',$area->dc_area_servicio) ?>
<?php $form->End('Crear','addbtn') ?>

