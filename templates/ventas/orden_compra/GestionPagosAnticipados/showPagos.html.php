<table class="tab bicolor_tab" width="100%">
  <thead>
    <tr>
      <th>Comprobante</th>
      <th>Fecha</th>
      <th>Medio Pago</th>
      <th>Monto</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($pagos as $p): ?>
    <tr<?php if($p->dm_nula == 1): ?> style="color:#F00"<?php endif ?>>
      <td><b><?php echo $p->dq_comprobante ?></b></td>
      <td><?php echo $this->getConnection()->dateLocalFormat($p->df_pago) ?></td>
      <td><?php echo $p->dg_medio_pago ?></td>
      <td><?php echo moneda_local($p->dq_monto) ?></td>
      <td>
        <a href="<?php echo Factory::buildActionUrl('anular', array(
            'dc_comprobante' => $p->dc_comprobante,
            'dc_orden_compra' => $this->orden_compra->dc_orden_compra
        )) ?>" class="loadOnOverlay">
          <img src="images/delbtn.png" alt="[X]" />
        </a>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>