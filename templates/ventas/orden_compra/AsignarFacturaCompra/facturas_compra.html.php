<table width='100%' class='tab bicolor_tab' id="tabla_asignacion_factura">
    <caption>
        <strong>Seleccione las facturas de compra que cumplan con su criterio </strong><br />
        (Al seleccionar más de una, sólo se utilizarán los detalles de las facturas hasta completar la orden de compra)
    </caption>
    <thead>
        <th>
            &nbsp;
        </th>
        <th>
            Factura de compra
        </th>
        <th>
            Folio
        </th>
        <th>
            Fecha de Emisión
        </th>
        <th>
            Monto Total Factura
        </th>
    </thead>
    <tbody>
        <?php 
        if(!empty($errorMsg)):
            foreach($errorMsg as $v) {
                echo $v;
            }
        endif;
        foreach ($facturasDisponibles as $fc): 
        ?>
        <tr>
            <td>
                <input type="checkbox" class="dc_factura_venta" name="dc_factura_compra[]" value="<?php echo $fc->dc_factura; ?>" />
            </td>
            <td>
                <?php echo $fc->dq_factura;?>
            </td>
            <td>
                <?php echo $fc->dq_folio;?>
            </td>
            <td>
                <?php echo $this->getConnection()->dateLocalFormat($fc->df_emision);?>
            </td>
            <td>
                <?php echo Functions::monedaLocal($fc->dq_total);?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>