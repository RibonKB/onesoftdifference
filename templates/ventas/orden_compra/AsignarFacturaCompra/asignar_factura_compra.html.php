<?php
$form->Start(Factory::buildActionUrl('asignarFacturas'),'asignar_facturas');
$form->Header('Seleccione las facturas de compra que desea asociar a esta orden de compra.');
$form->Hidden('dc_orden_compra', $ordenCompra->dc_orden_compra);

include __DIR__ . '/facturas_compra.html.php';

$form->End('Asignar Facturas','addbtn btn_enviar');

include __DIR__ . '/init_scripts.html.php';
