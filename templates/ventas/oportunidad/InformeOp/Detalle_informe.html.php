
<?php
$funcionario = '';
$marca = '';
$ln = '';
$op = '';
$cl='';
$tp='';
        $total_m=0;
        $total_c=0;
if ($tipo == 'Ejecutivo') {
    $funcionario = " style='display:none' ";
}
if ($tipo == 'Marca') {
    $marca = " style='display:none' ";
}
if ($tipo == 'Linea de Negocio') {
    $ln = " style='display:none' ";
}
if ($tipo == 'Oportunidad') {
    $op =" style='display:none' ";
}
if ($tipo == 'Cliente' || $tipo=='Cliente NR') {
    $cl =" style='display:none' ";
}
if ($tipo == 'Tipo Producto') {
    $tp =" style='display:none' ";
}
$si=null;
$monto='Monto';
/*foreach($detalle as $o):
    if($o->dq_monto==$o->dq_monto_marca){
                $si=1;
            }
endforeach;*/
if($si==1):
    $monto='Monto Marca';
endif;



?>

<table class="tab bicolor_tab" width="100%">
    <caption id="ttl"></caption>
    <?php if (!isset($f)) { ?>
        <thead>
            <tr align="center">
                <th <?= $op ?>>N° Oportunidad</th>
                <th>Estado B.A.N.T.</th>
                <th>Funcionario</th>
				<th>Televenta</th>
                <th>Fecha creacion</th>
                <th>Fecha Ultima Gestion</th>
                <th>Fecha proyeccion de cierre</th>
                <th>Comentario Ultima Gestion</th>
                <th <?= $marca ?>>Marca</th>
                <th <?= $ln ?>>Linea de Negocio</th>
                <th <?= $tp ?>>Tipo de producto</th>
                <th>Cantidad Producto</th>
                <th>Monto</th>
                <th>Cliente</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody>
        <?php  $t=0; 
        foreach ($detalle as $o) { 
            if ($o->dc_empresa == $empresa) {  
                $total_c+=$o->dq_cantidad;
                $total_m+=$o->dq_monto_marca;
                ?>
               
                <tr align="center">
                    
                    <td id="opu"<?= $op ?>><?= $o->dq_oportunidad ?></td>
                    <?php if($tipo=='Oportunidad'){ 
                        if($t==0){ ?>
                    <td> <?php
                        for($i=1;$i<=4;$i++){
                            $nom="dc_pregunta".$i."_bant";
                            if($o->$nom == 1){
                                ?>
                        <img src="images/greenBall.png" alt="" height="15px" width="15px" />
                        <?php
                            }else{
                        ?>
                        <img src="images/redBall.png" alt="" height="15px" width="15px" />
                        <?php
                            }
                            
                        }
                        ?></td>
                    <td><?= $o->nombre ?></td>
					<td><?= $o->televenta ?></td>
                    <td><?= $o->df_fecha_emision ?></td>
                    <td><?= $o->creacion_avance ?></td>
                    <td><?= $o->df_proyeccion_cierre ?></td>
                    <td><?= $o->comentario_avance ?></td>
                    <?php $t++; }else { ?>
                    <td colspan='6'></td>
                    <?php $t++; }}else{?>
                        <td> <?php
                        for($i=1;$i<=4;$i++){
                            $nom="dc_pregunta".$i."_bant";
                            if($o->$nom == 1){
                                ?>
                        <img src="images/greenBall.png" alt="" height="15px" width="15px" />
                        <?php
                            }else{
                        ?>
                        <img src="images/redBall.png" alt="" height="15px" width="15px" />
                        <?php
                            }
                            
                        }
                        ?></td>
                    <td><?= $o->nombre ?></td>
					<td><?= $o->televenta ?></td>
                    <td><?= $o->df_fecha_emision ?></td>
                    <td><?= $o->creacion_avance ?></td>
                    <td><?= $o->df_proyeccion_cierre ?></td>
                    <td><?= $o->comentario_avance ?></td>
                    <?php } ?>
                    <td id="mca" <?= $marca ?>><?= $o->dg_marca ?></td>
                    <td id="lng" <?= $ln ?>><?= $o->dg_linea_negocio ?></td>
                    <td <?= $tp ?> id="tp"><?= $o->dg_tipo_producto ?></td>
                    <td align="center"><?= $o->dq_cantidad ?></td>
                    <td align="right"><?= moneda_local($o->dq_monto_marca / $cambio, 2) ?></td>
                    <td><?= $o->dg_cliente ?></td>
                    <td><?= $o->dg_estado ?></td>
                </tr>
                <?php
            }
        }?></tbody><?php
    } else {
        ?>
                <thead>
        <tr align="center">
            <th>N° Oportunidad</th>
            <th>Estado B.A.N.T.</th>
            <th <?= $funcionario ?>>Funcionario</th>
			<th>Televenta</th>
            <th>Fecha cracion</th>
            <th>Fecha Ultima Gestion</th>
            <th>Fecha proyeccion de cierre</th>
            <th>Comentario Ultima Gestion</th>
            <th>Cantidad producto</th>
            <th><?= $monto ?></th>
            <th <?= $cl ?>>Cliente</th>
            <th>Estado</th>
        </tr>
                </thead>
                <tbody>
        <?php
        $ant='';
        $monto_d=0;
        $total_cc=0;

        foreach ($detalle as $o) {
            
            $ss=0;
            if ($o->dc_empresa == $empresa) {
                $total_c+=$o->dq_cantidad;
                if($o->dq_oportunidad != $ant){
                    
                    $total_m+=$o->dq_monto;
                    
                    $ant=$o->dq_oportunidad;
                    foreach($detalle as $r):
                        if($r->dq_oportunidad == $ant){
                            $ss+=$r->dq_cantidad;
                        }
                        
                    endforeach;
                ?>
                <tr align="center">
                    <td><?= $o->dq_oportunidad ?></td>
                    <td>
                        <?php
                        for($i=1;$i<=4;$i++){
                            $nom="dc_pregunta".$i."_bant";
                            if($o->$nom == 1){
                                ?>
                        <img src="images/greenBall.png" alt="" height="15px" width="15px" />
                        <?php
                            }else{
                        ?>
                        <img src="images/redBall.png" alt="" height="15px" width="15px" />
                        <?php
                            }
                            
                        }
                        ?>
                    </td>
                    <td id="eje" <?= $funcionario ?>><?= $o->nombre ?></td>
					<td><?= $o->televenta ?></td>
                    <td><?= $o->df_fecha_emision ?></td>
                    <td><?= isset($o->creacion_avance) ? $o->creacion_avance:'No registra avances' ?></td>
                    <td><?= $o->df_proyeccion_cierre ?></td>
                    <td><?= isset ($o->comentario_avance) ? $o->comentario_avance:'No registra avances' ?></td>
                    <td align="center"><?= $ss ?></td>
                    <td align="right"><?= moneda_local($o->dq_monto / $cambio, 2) ?></td>
                    <td id="clie"<?= $cl ?>><?= $o->dg_cliente ?></td>
                    <td><?= $o->dg_estado ?></td>
                    
                </tr>
                <?php
           }
            }
        }
    }
    ?>
        </tbody>
        <tfoot>
            <tr align="right" style="font-weight: bolder">
                    <?php if($tipo=='Ejecutivo'){ ?>
                    <th colspan="6"> <?php }
                    elseif($tipo=='Cliente' || $tipo=='Cliente NR'){ ?>
                    <th colspan="7"> <?php } else{ ?>
                    <th colspan="9"> <?php } ?>
                        Total
                    </th>
                    <th align="center"><?= $total_c?></th>
                    <th><?= moneda_local($total_m / $cambio,2)?></th>
                    <th colspan="2"></th>
                </tr>
        </tfoot>
</table>
<div style="background-color: gray;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;height: 20px"></div>

<script>
    var tipo = "<?php echo $tipo ?>";
    var va='';
    var id='';
    switch(tipo){
        case('Cliente'):
            id='clie';
            break;
        case('Cliente NR'):
            id='clie';
            break;
       case('Ejecutivo'):
           id='eje';
           break;
       case('Marca'):
           id='mca';
           break;
       case('Oportunidad'):
           id='opu';
           break;
       case('Linea de Negocio'):
           id='lng';
           break;
       case('Tipo Producto'):
           id='tp';
           break;
    }
    va = document.getElementById(id).innerHTML;
        document.getElementById('ttl').innerHTML = "<span style='font-weight: bolder;color:white;'>" + va + "</span>";
   // $(".secc_bar").text(function() {
     //   this.innerHTML = "Detalle " + tipo;

   // });

</script>