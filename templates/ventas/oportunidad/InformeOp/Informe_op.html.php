<?php
$form->start(Factory::buildActionUrl('mostrarPorFiltro'), 'Informe_op', 'cValidar');
$m = '';
$l = '';
$op = '';
$cl = '';
$tp = '';
$anio = '';
for ($i = 1; $i <= 12; $i++) {
    $c1[$i] = '';
}
for ($i = 1; $i <= 4; $i++) {
    $c2[$i] = '';
}
if ($filt == 'Marca'):
    $m = ' selected="" ';
endif;
if ($filt == 'Linea de Negocio'):
    $l = 'selected=""';
endif;
if ($filt == 'Oportunidad'):
    $op = 'selected=""';
endif;
if ($filt == 'Cliente'):
    $cl = 'selected=""';
endif;
if ($filt == 'Tipo Producto'):
    $tp = 'selected=""';
endif;
$form->Section();
?>

<label>Grupo Cabecera: <br/>
    <select id="filtro" name="filtro" class="filtro">
        <option  value="ejecutivo">Ejecutivo</option>
        <option <?= $m ?> value="marca">Marca</option>
        <option <?= $l ?> value="linea de negocio">Linea de Negocio</option>
        <option <?= $op ?>value="oportunidad" >Oportunidad</option>
        <option <?= $cl ?>value="cliente">Cliente</option>
        <option <?= $tp ?>value="tipo producto">Tipo Producto</option>
    </select> 
</label><br/>
<?php $form->Select("Tipo de cambio :", 'tipo_cambio', $cambios, '', $cambio_sel); ?>
<?php
$form->Select('Año :', 'filtro_anho', array(
    date('Y') - 2 => date('Y') - 2,
    date('Y') - 1 => date('Y') - 1,
    date('Y') => date('Y'),
    date('Y') + 1 => date('Y') + 1,
    date('Y') + 2 => date('Y') + 2
        ), '', $anho
);
$anio = $anho;
$nan = $anio;
$form->Select('Primer mes del año :', 'filtro_mes', array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril',
    5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Septiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Diciembre'), '', $primer_mes);
$form->Radiobox('', 'Ver_Por', array('dq_monto_marca' => 'Por Monto', 'dq_cantidad' => 'Por Cantidad'), isset($ver_por_sel) ? $ver_por_sel : '');
$form->EndSection();
?>
<fieldset>
    <?php
    $form->Section();
    ?>
    <label> Filtros:
    </label><br/>
    <?php
    $form->MultiSelect('Ejecutivo :', 'Filtro_funcionario', $funcionario, isset($sele1) ? $sele1 : '');
	$form->MultiSelect('Televenta :','Filtro_televenta', $funcionario);
    $form->MultiSelect('Linea de Negocio', 'Filtro_LN', $lineas, isset($sele2) ? $sele2 : '');
    $form->MultiSelect('Tipo de Producto', 'filtro_tipo_producto', $tipo_prod, isset($tipo_p_select) ? $tipo_p_select : '');
    $form->MultiSelect('Marca', 'filtro_marca', $marcas, isset($sele3) ? $sele3 : '');
    $form->MultiSelect('Cliente', 'filtro_cliente', $clientes, isset($cliente_select) ? $cliente_select : '');
    $form->MultiSelect('Otros Clientes', 'filtro_cliente_otro', $clientes_otro, isset($cli_otro_sel) ? $cli_otro_sel : '');
    $form->EndSection();
    $form->Section();
    ?>


</p>

<div id="estado" >

    <?php
    if (isset($estado_select)) {
        $form->MultiSelect('Estado:', 'Filtro_estado', $estados, $estado_select);
    } else {
        $form->MultiSelect('Estado:', 'Filtro_estado', $estados);
    }
    ?>
    <br/>
</div>
<div id="B_A_N_T" >
    <fieldset>
        Por Puntos : <input type="checkbox" checked="" name="por_puntos" id="por_puntos" /><br/>
        <label>Puntos: <br/>
            <select id="Filtro_bant_puntos" name="Filtro_bant_puntos" class="estado">
                <option value="<=100">Todos</option>
                <option value="<=50">Menor a 50</option>
                <option value=">50">Superior a 50</option>
            </select> </label><br/>
        Pesonalizado: <input type="checkbox" id="perso" name="f_perso" /><input type="text" name="filtro_pts_perso" id="filtro_pts_perso" disabled="" /> </p>
    </fieldset>
</div>
<?php
$form->EndSection();
$form->Section();
?>
<div id="B_A_N_T2">
    <fieldset>
        Por preguntas : <input type="checkbox" name="por_preguntas" id="por_preguntas" /><br/><br/><br/>

        <label id="pr_1" style="background-color: #0E9749;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder;color: black;font-size: 13px"> Tiene un presupuesto asignado </label>
        <label>
            <input type='checkbox' class="SN"  id="sino_1" name='sino_1' value='30' disabled="" />
        </label>
        <br/><br/>
        <label id="pr_2" style="background-color: #32779a;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder;color: black;font-size: 13px"> Tiene autoridad para la compra </label>
        <label>
            <input type='checkbox' class="SN" id="sino_2" name='sino_2' value='30' disabled="" />
        </label>
        <br/><br/>
        <label id="pr_3" style="background-color: #0E9749;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder;color: black;font-size: 13px"> Tiene una necesidad que resolver </label>
        <label>
            <input type='checkbox' class="SN" id="sino_3" name='sino_3' value='20' disabled="" />
        </label>
        <br/><br/>
        <label id="pr_4"  style="background-color: #32779a;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder;color: black;font-size: 13px"> A definido un plazo de compra </label>
        <label>
            <input type='checkbox' class="SN" id="sino_4" name='sino_4' value='20' disabled="" />
        </label>
        <br/><br/>
        <input type="checkbox" name="tipo_busqueda_preguntas" disabled="" id="tipo_busqueda_preguntas" /> De forma exacta.
    </fieldset>
</div>
<?php
$form->EndSection();
?>
</fieldset>
<?php
$form->End('Buscar', 'searchbtn');
?>

<table class="tab bicolor_tab" bolder="1">
    <tr>
    <caption></caption>
    <thead>
        </tr>
        <tr align="center" style="font-weight: bolder; color: lightseagreen">
            <td width="25%">Filtro</td>
            <th colspan="3" width="25%">Q1</th>
            <td colspan="3" width="25%">Q2</td>
            <th colspan="3" width="25%">Q3</th>
            <td colspan="3" width="25%">Q4</td>
        </tr>
        <tr align="center" style="font-weight: bolder">
            <th><?= $filt ?></th>
            <?php
            $n = 1;
            if ($primer_mes):
                $n = $primer_mes;
            endif;
            $k = 0;
            for ($i = 1; $i <= 12; $i++):
                if ($k == 0) {
                    ?>
                    <td ><?= "<label>" . $anho . "</label><p>" . $meses[$n] . "</p>"; ?></td>
                    <?php
                    $k++;
                } else {
                    ?>
                    <th ><?= "<label>" . $anho . "</label><p>" . $meses[$n] . "</p>"; ?></th>

                    <?php
                    $k++;
                }
                if ($k == 2) {
                    $k = 0;
                }
                if ($n == 12):
                    $n = 0;
                    $anho+=1;
                endif;
                $n++;
            endfor;
            ?>
        </tr>
    </thead>
    <?php
    $en = 0;
    if (isset($datos) && $dd == NULL) {
        foreach ($datos as $id => $ar1):
            $en = 0;
            ?><?php
            foreach ($ar1 as $nombre => $ar2):
                if (in_array('o', $ar2)) {
                    ?>
                    <tr align="right">
                        <td align="center"><?= $nombre ?></td>
                        <?php
                        $en++;
                    }
                    for ($i = 1; $i <= 12; $i++):
                        if ($ar2[$n] != 0) {
                            ?>

                            <td>

                                <label class="link" style="cursor: pointer;font-weight: bolder;color: midnightblue"
                                       onclick="over('<?php
                    echo
                    Factory::buildActionUrl(
                            'VerDetalle', array('id' => $id,
                        'tipo' => $filt,
                        'mes' => $n,
                        'anho' => $anio,
                        'cambio' => $cambio,
                        'total' => $cambio_s_n == 0 ? moneda_local($ar2[$n] / $cambio, 2) : $ar2[$n],
                        'puntos' => $puntos_b,
                        'ln' => isset($sele2) ? $sele2 : '',
                        'otro' => isset($sele3) ? $sele3 : '',
                        'otro2' => isset($sele1) ? $sele1 : '',
                        'estado' => $estado_select,
                        'cliente' => $cliente_select,
                        'cliente2' => $cli_otro_sel,
                        'tipo_prod' => isset($tipo_p_select) ? $tipo_p_select : ''));
                            ?>')">
                                       <?= $cambio_s_n == 0 ? moneda_local($ar2[$n] / $cambio, 2) : $ar2[$n] ?>
                                </label>

                            </td>
                            <?php
                        } else {
                            if (is_numeric($ar2[$n])) {
                                ?>
                                <td><?= $cambio_s_n == 0 ? moneda_local($ar2[$n] / $cambio, 2) : $ar2[$n] ?></td>
                                <?php
                            }
                        }
                        if ($n == 12):
                            $anio++;
                            $n = 0;
                        endif;
                        $n++;
                        if ($i == 12) {
                            $anio = $anio - 1;
                        }
                    endfor;
                endforeach;
                if (in_array('o', $ar2)) {
                    ?></tr><?php
                }
            endforeach;
        } else {
            if ($datos && $dd) {
                foreach ($datos as $id => $ar1):

                    foreach ($ar1 as $nombre => $ar2):
                        foreach ($ar2 as $estado => $ar3):
                            foreach ($ar3 as $puntos => $ar4):
                                foreach ($ar4 as $preguntas => $ar5):
                                    if (in_array('o', $ar5)) {
                                        ?>
                                    <tr align="right">  
                                        <td align="center"><?= $nombre ?></td>
                                        <?php
                                    }
                                    for ($i = 1; $i <= 12; $i++):
                                        if ($ar5[$n] != 0) {
                                            ?>
                                            <td>
                                                <label class="link" style="cursor: pointer;font-weight: bolder;color: midnightblue" 
                                                       onclick="over('<?php
                                    echo
                                    Factory::buildActionUrl('VerDetalle', array('id' => $id, 'tipo' => $filt, 'cambio' => $cambio, 'mes' => $n, 'anho' => $anio, 'total' => moneda_local($ar5[$n] / $cambio, 2), 'ln' => isset($sele2) ? $sele2 : '', 'otro' => isset($sele3) ? $sele3 : '', 'otro2' => isset($sele1) ? $sele1 : '', 'estado' => $estado_select, 'cliente' => $cliente_select,
                                        'cliente2' => $cli_otro_sel, 'puntos' => $puntos_b, 'tipo_prod' => isset($tipo_p_select) ? $tipo_p_select : ''));
                                            ?>')">
                                                       <?= $cambio_s_n == 0 ? moneda_local($ar5[$n] / $cambio, 2) : $ar5[$n] ?>
                                                </label>
                                            </td>
                                            <?php
                                        } else {
                                            if (is_numeric($ar5[$n])) {
                                                ?>
                                                <td><?= $cambio_s_n == 0 ? moneda_local($ar5[$n] / $cambio, 2) : $ar5[$n] ?></td>
                                                <?php
                                            }
                                        }
                                        if ($n == 12):
                                            $anio++;
                                            $n = 0;
                                        endif;
                                        $n++;
                                        if ($i == 12) {
                                            $anio = $anio - 1;
                                        }
                                    endfor;
                                endforeach;
                            endforeach;
                        endforeach;
                    endforeach;
                    if (in_array('o', $ar5)) {
                        ?></tr><?php
                    }
                endforeach;
            } else {
                ?>
            <tr align="center">
                <td colspan="13">
                    No existen Oportunidades con los criterios seleccionados
                </td>
            </tr>
            <?php
        }
    }
    for ($i = 1; $i <= 12; $i++) {
        if ($TtlMes[$i] < 0) {
            $c1[$i] = ' style="color: red" ';
        } else if ($TtlMes[$i] > 0) {
            $c1[$i] = ' style="color: mediumblue" ';
        }
    }
    for ($i = 1; $i <= 4; $i++) {
        if ($TtlQ[$i] < 0) {
            $c2[$i] = ' style="color: red" ';
        } else if ($TtlQ[$i] > 0) {
            $c2[$i] = ' style="color: mediumblue" ';
        }
    }
    ?>
    <tfoot>
        <tr align="right" style="font-weight: bolder;">
            <th align="center">Total Por Mes</th>
            <?php
            for ($i = 1; $i <= 12; $i++):
                ?>
                <th <?= $c1[$i] ?>><?= $cambio_s_n == 0 ? moneda_local($TtlMes[$i] / $cambio, 2) : $TtlMes[$i] ?></th>
                <?php
            endfor;
            ?>
        </tr>
        <tr align="center" style="font-weight: bolder;">
            <th width="25%">Total Por Q</th>
            <?php
            for ($i = 1; $i <= 4; $i++):
                ?>
                <th colspan="3" width="25%" <?= $c2[$i] ?>><?= $cambio_s_n == 0 ? moneda_local($TtlQ[$i] / $cambio, 2) : $TtlQ[$i] ?></th>
                <?php
            endfor;
            ?>
        </tr>
    </tfoot>
</table>
<div style="background-color: gray;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;height: 17px"></div>
<?php
if ($datos2):
    ?>
    <br>
    <br>
    <table class="tab bicolor_tab">
        <caption>Otros Clientes</caption>
        <thead>
            <tr>
        <thead>
            </tr>
            <tr align="center" style="font-weight: bolder; color: lightseagreen">
                <td width="25%">Filtro</td>
                <th colspan="3" width="25%">Q1</th>
                <td colspan="3" width="25%">Q2</td>
                <th colspan="3" width="25%">Q3</th>
                <td colspan="3" width="25%">Q4</td>
            </tr>
            <tr align="center" style="font-weight: bolder">
                <th><?= $filt ?></th>
                <?php
                $n = 1;
                if ($primer_mes):
                    $n = $primer_mes;
                endif;
                $k = 0;
                for ($i = 1; $i <= 12; $i++):
                    if ($k == 0) {
                        ?>
                        <td ><?= "<label>" . $anho . "</label><p>" . $meses[$n] . "</p>"; ?></td>
                        <?php
                        $k++;
                    } else {
                        ?>
                        <th ><?= "<label>" . $anho . "</label><p>" . $meses[$n] . "</p>"; ?></th>

                        <?php
                        $k++;
                    }
                    if ($k == 2) {
                        $k = 0;
                    }
                    if ($n == 12):
                        $n = 0;
                        $anho+=1;
                    endif;
                    $n++;
                endfor;
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $en = 0;
            foreach ($datos2 as $id => $ar1):
                $en = 0;
                ?><?php
                foreach ($ar1 as $nombre => $ar2):
                    if (in_array('o', $ar2)) {
                        ?>
                        <tr align="right">
                            <td align="center"><?= $nombre ?></td>
                            <?php
                            $en++;
                        }
                        for ($i = 1; $i <= 12; $i++):
                            if ($ar2[$n] != 0) {
                                ?>

                                <td>

                                    <label class="link" style="cursor: pointer;font-weight: bolder;color: midnightblue"
                                           onclick="over('<?php
                    echo
                    Factory::buildActionUrl('VerDetalle', array('id' => $nombre,
                        'tipo' => $filt . " NR",
                        'mes' => $n,
                        'anho' => $anio,
                        'cambio' => $cambio,
                        'total' => moneda_local($ar2[$n] / $cambio, 2),
                        'puntos' => $puntos_b,
                        'ln' => isset($sele2) ? $sele2 : '',
                        'otro' => isset($sele3) ? $sele3 : '',
                        'otro2' => isset($sele1) ? $sele1 : '',
                        'estado' => $estado_select,
                        'cliente' => $cliente_select,
                        'cliente2' => $cli_otro_sel,
                        'tipo_prod' => isset($tipo_p_select) ? $tipo_p_select : ''));
                                ?>')">
                                           <?= $cambio_s_n == 0 ? moneda_local($ar2[$n] / $cambio, 2) : $ar2[$n] ?>
                                    </label>

                                </td>
                                <?php
                            } else {
                                if (is_numeric($ar2[$n])) {
                                    ?>
                                    <td><?= $cambio_s_n == 0 ? moneda_local($ar2[$n] / $cambio, 2) : $ar2[$n] ?></td>
                                    <?php
                                }
                            }
                            if ($n == 12):
                                $anio++;
                                $n = 0;
                            endif;
                            $n++;
                            if ($i == 12) {
                                $anio = $anio - 1;
                            }
                        endfor;
                    endforeach;
                    if (in_array('o', $ar2)) {
                        ?></tr><?php
                    }
                endforeach;
                ?>
        </tbody>
        <?php
        for ($i = 1; $i <= 12; $i++) {
            if ($TtlMes2[$i] < 0) {
                $c1[$i] = ' style="color: red" ';
            } else if ($TtlMes2[$i] > 0) {
                $c1[$i] = ' style="color: mediumblue" ';
            }
        }
        for ($i = 1; $i <= 4; $i++) {
            if ($TtlQ2[$i] < 0) {
                $c2[$i] = ' style="color: red" ';
            } else if ($TtlQ2[$i] > 0) {
                $c2[$i] = ' style="color: mediumblue" ';
            }
        }
        ?>
        <tfoot>
            <tr align="right" style="font-weight: bolder;">
                <th align="center">Total Por Mes</th>
                <?php
                for ($i = 1; $i <= 12; $i++):
                    ?>
                    <th <?= $c1[$i] ?>><?= $cambio_s_n == 0 ? moneda_local($TtlMes2[$i] / $cambio, 2) : $TtlMes2[$i] ?></th>
                    <?php
                endfor;
                ?>
            </tr>
            <tr align="center" style="font-weight: bolder;">
                <th width="25%">Total Por Q</th>
                <?php
                for ($i = 1; $i <= 4; $i++):
                    ?>
                    <th colspan="3" width="25%" <?= $c2[$i] ?>><?= $cambio_s_n == 0 ? moneda_local($TtlQ2[$i] / $cambio, 2) : $TtlQ2[$i] ?></th>
                    <?php
                endfor;
                ?>
            </tr>
        </tfoot>
    </table>
    <?php
endif;
?>
<div style="background-color: gray;border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;height: 17px"></div>
<script>



                                           $('#Filtro_estado,#Filtro_funcionario,#Filtro_televenta,#Filtro_LN,#filtro_marca,#filtro_cliente,#filtro_cliente_otro,#filtro_tipo_producto').multiSelect({
                                               selectAll: true,
                                               selectAllText: "Seleccionar todos",
                                               noneSelected: "---",
                                               oneOrMoreSelected: "% seleccionado(s)"
                                           });
                                           $("#perso").click(function() {
                                               if ($(this).attr('checked')) {
                                                   $("#filtro_pts_perso").attr('disabled', false);
                                                   $("#Filtro_bant_puntos").attr('disabled', true);
                                               } else {
                                                   $("#filtro_pts_perso").attr('disabled', true);
                                                   $("#Filtro_bant_puntos").attr('disabled', false);
                                               }
                                           });


                                           $('.link').hover(function() {
                                               this.style.background = 'tomato';
                                           });
                                           $('.link').mouseout(function() {
                                               this.style.background = '';
                                           });

                                           function over(link) {
                                               loadOverlay(link);
                                           }

                                           $("#por_preguntas").click(function() {
                                               $(this).attr('checked', true);
                                               if ($(this).attr('checked')) {
                                                   $("#Filtro_bant_puntos").attr('disabled', true);
                                                   $("#perso").attr('disabled', true);
                                                   if ($("#perso").attr('checked')) {
                                                       $("#perso").attr('checked', false)
                                                   }
                                                   $("#tipo_busqueda_preguntas").attr('disabled', false);
                                                   $("#por_puntos").attr('checked', false);
                                                   $("#por_puntos").attr('disabled', false);
                                                   var n = 'sino_';
                                                   for (var i = 1; i <= 4; i++) {
                                                       $("#" + n + i).attr('disabled', false);
                                                   }
                                               }
                                           });

                                           $("#por_puntos").click(function() {
                                               $(this).attr('checked', true);
                                               if ($(this).attr('checked')) {
                                                   $("#Filtro_bant_puntos").attr('disabled', false);
                                                   $("#perso").attr('disabled', false);

                                                   $("#por_preguntas").attr('checked', false);
                                                   $("#por_preguntas").attr('disabled', false);
                                                   $("#tipo_busqueda_preguntas").attr('disabled', true);
                                                   var n = 'sino_';
                                                   for (var i = 1; i <= 4; i++) {
                                                       $("#" + n + i).attr('disabled', true);
                                                       if ($("#" + n + i).attr('checked')) {
                                                           $("#" + n + i).attr('checked', false);
                                                           document.getElementById('pr_' + i).style.opacity = 0.5;
                                                           document.getElementById('pr_' + i).style.color = 'black';
                                                           document.getElementById('pr_' + i).style.fontWeight = 'normal';
                                                       }
                                                   }
                                               }
                                           });

                                           $('.SN').click(function() {
                                               var v1 = document.getElementById('sino_1');
                                               var v2 = document.getElementById('sino_2');
                                               var v3 = document.getElementById('sino_3');
                                               var v4 = document.getElementById('sino_4');

                                               var p1 = document.getElementById('pr_1');
                                               var p2 = document.getElementById('pr_2');
                                               var p3 = document.getElementById('pr_3');
                                               var p4 = document.getElementById('pr_4');


                                               if (v1.checked == true)
                                               {

                                                   p1.style.opacity = 1;
                                                   p1.style.color = 'white';
                                                   p1.style.fontWeight = 'bolder';
                                               } else
                                               {
                                                   p1.style.opacity = 0.5;
                                                   p1.style.color = 'black';
                                                   p1.style.fontWeight = 'bolder';
                                               }

                                               if (v2.checked == true)
                                               {
                                                   p2.style.opacity = 1;
                                                   p2.style.color = 'white';
                                                   p2.style.fontWeight = 'bolder';
                                               } else
                                               {
                                                   p2.style.opacity = 0.5;
                                                   p2.style.color = 'black';
                                                   p2.style.fontWeight = 'bolder';
                                               }

                                               if (v3.checked == true)
                                               {
                                                   p3.style.opacity = 1;
                                                   p3.style.color = 'white';
                                                   p3.style.fontWeight = 'bolder';
                                               } else
                                               {
                                                   p3.style.opacity = 0.5;
                                                   p3.style.color = 'black';
                                                   p3.style.fontWeight = 'bolder';
                                               }

                                               if (v4.checked == true)
                                               {
                                                   p4.style.opacity = 1;
                                                   p4.style.color = 'white';
                                                   p4.style.fontWeight = 'bolder';
                                               } else
                                               {
                                                   p4.style.opacity = 0.5;
                                                   p4.style.color = 'black';
                                                   p4.style.fontWeight = 'bolder';
                                               }
                                           });

</script>