<?php
//
$form->Start(Factory::buildActionUrl('grabar'), 'cr_oportunidad', 'op_form');
$form->Header("<b>Indique los datos para crear Oportunidad</b><br />Los campos marcados con [*] son obligatorios");
?><hr /><?php
$form->Section();
?><fieldset><?php
    $form->Select('Cliente', 'op_cliente', $cliente, 1);
    ?><label><input type='checkbox' name='op_other_cli'/>Otro</label><?php
    $form->Text('', 'op_cliente_txt', 0, '', '', '', 30, 1);
    ?></fieldset><?php
    $form->Text("Nombre Contacto", 'op_nombre_contacto', 1);
    $form->Text("Monto aproximado (USD)", 'op_monto', 1, 13);

    $form->Textarea("Comentario", 'op_comentario', 1);
    $form->EndSection();

    $form->Section();
    $form->Text("E-Mail de contacto", 'op_email', 1);
    $form->Text("Telefono de Contacto", 'op_contacto_fono', 1);
    $form->Select("Tele-Venta", "op_tele_venta", $tele_venta, 1, $tv);
    $form->Select('Ejecutivo', 'op_ejecutivo', $funcionario, 1);
    $form->Date("Proyección de Cierre", 'op_proyeccion', 0, 2);
    $form->Date('Llamar día', 'op_llamada', 0, 1);

    //Hora de la gestión
    $form->Select2('Hora:', 'hora', $hora, 0, 1, 'width:50px;');
    $form->Select2(':', 'minuto', $minuto, '', 0, 'width:50px;');
    $form->Select2('-', 'meridiano', array(0 => 'AM', 12 => 'PM'), '', 0, 'width:70px;');
    $form->EndSection();
    $form->Section();
    $form->Text('Deal ID', 'deal_id_op', 0);
    ?>
<table border="0" style="background-color: window; margin-left: 100px; margin-top: 20px;border-radius: 10px">
    <tr>
        <td  id="cl_pote1" colspan="2" align="center"  height="30px" style="background-color: red;font-weight: bolder;border-top-left-radius :10px;border-top-right-radius: 10px;-webkit-transition-duration: 0.5s">PROSPECTO</td>
        <td></td>
    </tr>
    <tr>
        <td id="cl_pote2" align="center" rowspan="7" style="background-color: red; font-weight: bolder; font-size: 20pt;-webkit-transition-duration: 0.5s" width="35px">B<br />A<br />N<br />T</td>
        <td id="pr_1" style="background-color: #0E9749;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder"><br />Tiene un presupuesto asignado</td>
        <td>
            <label>
                <input type='checkbox' class="SN"  id="sino_1" name='sino_1' value='30' />
            </label>
        </td>

    </tr>
    <tr><td></td></tr>
    <tr>

        <td id="pr_2" height="10px" style="background-color: #32779a;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder"><br />Tiene autoridad para la compra</td>
        <td>
            <label>
                <input type='checkbox' class="SN" id="sino_2" name='sino_2' value='30' />
            </label>
        </td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td id="pr_3" height="20px" style="background-color: #0E9749;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder">Tiene una necesidad que resolver</td>
        <td>
            <label>
                <input type='checkbox' class="SN" id="sino_3" name='sino_3' value='20' />
            </label>
        </td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td id="pr_4" height="20px" style="background-color: #32779a;opacity: 0.5;-webkit-transition-duration: 0.5s;font-weight: bolder">A definido un plazo de compra</td>
        <td>
            <label>
                <input type='checkbox' class="SN" id="sino_4" name='sino_4' value='20' />
            </label>
        </td>
    </tr>
    <tr>

        <td id="cl_pote3" align="center" colspan="2" height="30px" style="background-color: red;font-weight: bolder;-webkit-transition-duration: 0.5s; border-bottom-left-radius: 10px;border-bottom-right-radius: 10px">
            CLIENTE POTENCIAL 
            <input type="text" id='pts' name="pts" readOnly="readOnly" value='0' style='background-color: transparent; border: 0px;text-align: right;font-weight: bolder; width: 30px;-webkit-transition-duration: 0.5s' /> 
            %
        </td>
    </tr>
</table>
<?php
$form->EndSection();
$form->Group();
?>
<div style="text-align: center">
    <label ><strong>M.P.M &nbsp;(Monto Por Marca)</strong></label><br/>
    <table class="tab bicolor_tab" style="width:100%">
        <thead>
		<th style="width:4%">[X]</th>
        <th style="width:18%">Marca[*]</th>        
        <th style="width:18%">Linea de negocio[*]</th>
        <th style="width:18%">Tipo de Producto[*]</th>
        <th style="width:15%">Cantidad[*]</th>
        <th style="width:15%">Monto(USD)[*]</th>
		<th style="width:15%">Margen[%][*]</th>
        
		<!-- Codigo Anterior 05-12-14
		<th width="24%">Marca[*]</th>        
        <th width="24%">Linea de negocio[*]</th>
        <th width="24%">Tipo de Producto[*]</th>
        <th width="13%">Cantidad[*]</th>
        <th width="13%">Monto (USD)[*]</th>
        <th width="2%">
		-->
        </thead>
        <tbody id="detalle_list">
            <tr id="empty_detalle">
                <td colspan="8">
                    <div class="center">No ha agregado ningún producto a la Oportunidad</div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5" style="visibility: hidden">
                </th>
                <!--th class="cantidad_p">
                    0
                </th-->
                <th class="total_op">
                    0
                </th>
            </tr>
        </tfoot>
    </table>
</div>
<div class="center">
    <?php
    $form->Button('Agregar Detalle', 'id="add_detalle"', 'addbtn');
    ?>
</div>
<?php
$form->End('Crear', 'addbtn');
?>
<table class="hidden">
    <tbody id="detalle_template">
        <tr align="center">
    <input type="hidden" name="item_id[]" class="item_id" />	
    <td style="width:4%"><img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" /></td>
    <td><?= $form->Select2('', 'dc_marca[]', $marca,'', 1, 'width:90%;'); ?></td>
    <td><?php $form->Select2('', 'op_linea_negocio[]', $linea_negocio,'', 1, 'width:90%;') ?></td>
    <td><?= $form->Select2('', 'op_tipo_producto[]', $tipo_producto,'', 1, 'width:90%;') ?></td>
    <td><input type="text" name="cantidad[]" id="cantidad[]" style="text-align: center;  width:90%;" class="cantidad" required="required" maxlength="9" /></td>
    <td><input type="text" name="valor[]" id="valor[]" style="text-align: center;  width:90%;" class="valor" required="required" maxlength="13" /></td>
	<!--td class="total_usd">0</td-->
	<td><input type="text" name="margen[]" id="margen[]" style="text-align: center;  width:90%;" class="margen" required="required" maxlength="3" /></td>
</tr>
</tbody>
</table>
<script src="<?php echo Factory::getJavascriptUrl('cr_oportunidad') ?>?v=2_0_19" type="text/javascript"></script>
<script type="text/javascript">
    js_data.init();
    
	$('.valor').change(function(){
        var s=0;
                $('.valor').each(function(){
                    var g = parseFloat(this.value);
                    if(!isNaN(g)){
                        s=s+g;
                    }
                });
		$('.total_op').text(s);		
    });
	
    $('.cantidad').change(function(){
        var s=0;
                $('.cantidad').each(function(){
                    var g = parseFloat(this.value);
                    if(!isNaN(g)){
                        s=s+g;
                    }
                });
        $('.cantidad_p').text(s);  
    });
	//Comprueba si un campo es numérico, alerta si tiene letras
	var comprobarNumerico = function(campo){
		var monto = $(campo).val();
		var filtro = /[^0-9.]/; 
		if(filtro.test(monto)){
			alert('Numero puede tener un largo máximo de 12 numeros, con dos decimales incluidos separados con punto. Las letras serán eliminadas');
			$(campo).focus();
		}
	}
	
	//Verificar que el monto aproximado es correcto}
	$('#op_monto').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	
	$('.cantidad').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9.]/g,'');
	});
	
	$('.valor').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9.]/g,'');
	});
	
	$('.margen').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9.]/g,'');
	});
	
	//Fin de control de textos numericos
	
    $('input[name=op_other_cli]').click(function() {
        if ($(this).attr('checked')) {
            $(this).parent().next().attr('disabled', false).attr('required', true);
            $('#op_cliente').attr('required', false);
        } else {
            $(this).parent().next().attr('disabled', true).attr('required', false);
            $('#op_cliente').attr('required', true);
        }
    });
    $('.op_form').submit(function(e) {
	e.preventDefault();
        var mail = $('#op_email').val();
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/.test(mail.trim()))) {
            $('#op_email').addClass('invalid');
            $('#op_email').focus();
            return;
        }
        monto = parseFloat($('#op_monto').val());
        if (!monto) {
            $('#op_monto').addClass('invalid');
            $('#op_monto').focus();
            return;
        }
        $('#op_monto').val(monto);
        $('#op_email,#op_monto').removeClass('invalid');
        i = '#' + $(this).attr('id');
        confirmEnviarForm(i, i + '_res');
    });
    $('.SN').click(function() {
        var v_1 = 0;
        var v_2 = 0;
        var v_3 = 0;
        var v_4 = 0;
        var v1 = document.getElementById('sino_1');
        var v2 = document.getElementById('sino_2');
        var v3 = document.getElementById('sino_3');
        var v4 = document.getElementById('sino_4');
        var p1 = document.getElementById('pr_1');
        var p2 = document.getElementById('pr_2');
        var p3 = document.getElementById('pr_3');
        var p4 = document.getElementById('pr_4');
        if ($('.SN:checked').size() == 0)
        {
            $('#sino_1').attr('required', true);
            $('#sino_2').attr('required', true);
            $('#sino_3').attr('required', true);
            $('#sino_4').attr('required', true);
        } else {
            $('#sino_1').attr('required', false);
            $('#sino_2').attr('required', false);
            $('#sino_3').attr('required', false);
            $('#sino_4').attr('required', false);
        }
        if (v1.checked == true)
        {
            v_1 = toNumber(v1.value);
            p1.style.opacity = 1;
            p1.style.color = 'white';
            p1.style.fontWeight = 'bolder';
        } else
        {
            p1.style.opacity = 0.5;
            p1.style.color = 'black';
            p1.style.fontWeight = 'bolder';
        }

        if (v2.checked == true)
        {
            v_2 = toNumber(v2.value);
            p2.style.opacity = 1;
            p2.style.color = 'white';
            p2.style.fontWeight = 'bolder';
        } else
        {
            p2.style.opacity = 0.5;
            p2.style.color = 'black';
            p2.style.fontWeight = 'bolder';
        }

        if (v3.checked == true)
        {
            v_3 = toNumber(v3.value);
            p3.style.opacity = 1;
            p3.style.color = 'white';
            p3.style.fontWeight = 'bolder';
        } else
        {
            p3.style.opacity = 0.5;
            p3.style.color = 'black';
            p3.style.fontWeight = 'bolder';
        }

        if (v4.checked == true)
        {
            v_4 = toNumber(v4.value);
            p4.style.opacity = 1;
            p4.style.color = 'white';
            p4.style.fontWeight = 'bolder';
        } else
        {
            p4.style.opacity = 0.5;
            p4.style.color = 'black';
            p4.style.fontWeight = 'bolder';
        }

        var suma = v_1 + v_2 + v_3 + v_4;
        var color = '';
        switch (suma) {
            case 0:
                color = 'red';
                break;
            case 20:
                color = 'tomato';
                break;
            case 30:
                color = 'salmon';
                break;
            case 40:
                color = 'lightsalmon';
                break;
            case 50:
                color = 'coral';
                break;
            case 60:
                color = '#C99337';
                break;
            case 70:
                color = 'khaki';
                break;
            case 80:
                color = 'gold';
                break;
            case 100:
                color = 'yellowgreen';
                break;
        }
        document.getElementById('cl_pote1').style.backgroundColor = color;
        document.getElementById('cl_pote2').style.backgroundColor = color;
        document.getElementById('cl_pote3').style.backgroundColor = color;
        document.getElementById('pts').value = suma
    });
</script>