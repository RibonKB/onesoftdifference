<?php
echo("<div class='panes' style='width:850px;'>");
if(count($data)){
?>
	<table class='tab' width='100%'>
	<caption>Últimas actividades realizadas sobre la oportunidad</caption>
	<thead><tr>
		<th>Gestión</th>
		<th width='150'>Contacto</th>
		<th width='120'>Estado Oportunidad</th>
		<th width='90'>Fecha creación</th>
                <th width='90'>Proxima Gestion</th>
		<th width='90'>Cierre estimado</th>
		<th width='90'>Fecha cierre</th>
	</thead><tbody>
            <?php
		foreach($data as $d){
		?>
               
			<tr>
				<td><?=$d->dg_gestion?></td>
				<td><?=$d->dg_contacto?></td>
                                <td><?=$d->dg_estado?></td>
				<td><?=$d->df_creacion?></td>
				<td><?=$d->df_estimada?></td>
                                <td><?=$d->df_cierre_estimado?></td>
				<td><?=$d->df_cierre?></td>
			</tr><?php
		}
	?>
                </tbody></table><hr />
                <?php

}
$estado='';
foreach ($estados as $e):
    $estado[$e->dc_estado]=$e->dg_estado;
endforeach;
$form->Start(Factory::buildActionUrl('Gestionar'),'cr_gestion');
$form->Header('Nueva actividad');
$form->Section();
$form->Text('Contacto','gestion_contacto',1);
$form->Select('Estado oportunidad','gestion_estado',$estado,0,0);
$form->Date('Fecha próxima gestión','gestion_fecha');

//Hora de la gestión
?><label>Hora</label>
<select name='gestion_hora' class='inputtext' style='width:50px;'><?php
foreach(range(1,12) as $h){
	$h = str_pad($h,2,'0',STR_PAD_LEFT);
	?><option value='<?=$h?>'><?=$h?></option><?php
}
?></select> : <select name='gestion_minuto' class='inputtext' style='width:50px;'><?php
foreach(range(0,45,15) as $m){
	$m = str_pad($m,2,'0',STR_PAD_LEFT);
	?><option value='<?=$m?>'><?=$m?></option><?php
}
?></select> - 
<select name='gestion_meridiano' class='inputtext' style='width:70px;'>
<option value='0'>AM</option>
<option value='12'>PM</option>
</select><br/><?php

$form->Date('Fecha de Cierre','fecha_cierre');
$form->EndSection();
$form->Section();
$form->Textarea('Gestión','gestion_gestion',1);
if($tarea=='si'){
    if($aviso=='no'){
    $form->Radiobox3('Avisarme de esta tarea','aviso_op',array(1=>''));
    }
}
if($tarea=='nunca'){
    echo 'No existe tarea para esta oportunidad';
}
$form->EndSection();
$form->Hidden('op_id',$_POST['id']);
if(count($data))
	$form->Hidden('gestion_avance',$data[0]->dc_avance);
        
$form->End('Crear','addbtn');
?>
</div>
<script type="text/javascript">
    
$('#gestion_estado').change(function(){
	$('input[name=gestion_cambio]').val($(this).val());
});
$("#gestion_fecha").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});

$("#fecha_cierre").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});
</script>