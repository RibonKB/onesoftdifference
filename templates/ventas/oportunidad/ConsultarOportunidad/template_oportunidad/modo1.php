<?php

require("inc/fpdf.php");

class PDF extends FPDF {
    private $header;
    private $datosEmpresa;
    private $datosOportunidad;
    private $cambio;
    private $detalle;

    public function PDF($datosOportunidad, $empresa, $db, $cambio) {
        $this->datosOportunidad = $datosOportunidad;
        $this->cambio = $cambio;
        $this->header=array('MARCA','LINEA DE NEGOCIO','MONTO');
        $datosE = $db->prepare($db->select(
                        "(SELECT * FROM tb_empresa WHERE dc_empresa={$empresa}) e
	LEFT JOIN (SELECT * FROM tb_empresa_configuracion WHERE dc_empresa={$empresa}) ec ON e.dc_empresa = ec.dc_empresa
	LEFT JOIN tb_comuna c ON e.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region r ON c.dc_region = r.dc_region", "e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region"));
        $db->stExec($datosE);
        $this->datosEmpresa = $datosE->fetch(PDO::FETCH_OBJ);

        unset($datosE);
        foreach ($this->datosOportunidad as $r) {
            $det = $db->prepare($db->select('tb_oportunidad_detalle od JOIN tb_marca m JOIN tb_linea_negocio ln', 'm.dg_marca,ln.dg_linea_negocio,od.dq_monto_marca', "od.dc_oportunidad={$r->dc_oportunidad} AND od.dm_activo=1 AND od.dc_marca=m.dc_marca AND od.dc_linea_negocio=ln.dc_linea_negocio"));
            $db->stExec($det);
        }

        $this->detalle = $det->fetch(PDO::FETCH_OBJ);
        unset($det);
        parent::__construct('P', 'mm', 'Letter');
    }

    function Header() {

        foreach ($this->datosOportunidad as $d) {
            $this->SetFont('Arial', '', 12);
            $this->SetDrawColor(0, 100, 0);
            $this->SetTextColor(0, 100, 0);
            $this->SetX(160);
            $this->MultiCell(50, 7, "\nOPORTUNIDAD\nN� {$d->dq_oportunidad}\n\n", 1, 'C');
            $this->SetY(10);
            $this->SetTextColor(0);

            $this->SetFont('', 'B', 12);
            $this->Cell(145, 5, $d->dg_razon);
            $this->ln();
            $this->SetFont('Arial', '', 7);
            $this->SetTextColor(110);

            $this->MultiCell(120, 2.4, "{$this->datosEmpresa->dg_giro}\n{$this->datosEmpresa->dg_direccion}, {$this->datosEmpresa->dg_comuna} {$this->datosEmpresa->dg_region}\n{$this->datosEmpresa->dg_fono}");
            $this->SetTextColor(0);
            $this->Ln(4);
        }
    }

    function setBody() {
        foreach ($this->datosOportunidad as $d) {
            if ($d->dg_cliente != " ")
                $d->dg_razon = $d->dg_cliente;
            $d->dq_monto = moneda_local($d->dq_monto);
            $this->AddPage();
            $this->SetFont('Arial', '', 7);
            $this->SetDrawColor(200);
            $this->SetTextColor(50);
            $this->Cell(0, 5, '', 'B');
            $this->Ln();
            $y = $this->GetY();
            $this->MultiCell(0, 6, "CLIENTE\nFECHA EMISI�N\nMONTO ESTIMADO\nNOMBRE CONTACTO\nTELEFONO CONTACTO\nCORREO\nEJECUTIVO\nLLAMADA\nRESPONSABLE", 'L');
            $this->Cell(0, 5, '', 'T');
            $this->SetY($y);
            $this->SetX(40);
            $this->SetFont('', 'B', 10);
            $montos = number_format(str_replace(',', '',$d->dq_monto ) / $this->cambio,2);
            $this->MultiCell(166, 6, "{$d->dg_razon}\n{$d->df_emision}\n$ {$montos}\n{$d->dg_nombre_contacto}\n{$d->dg_fono}\n{$d->dg_correo}\n{$d->dg_ejecutivo}\n{$d->df_llamada}\n{$d->dg_responsable}", 'R');        
            $this->SetFillColor(180);
            $this->Cell(0, 6, 'Comentario:', 1, 1, 'L', 1);
            $this->MultiCell(0, 6, $d->dg_comentario, 1);
        }
    }

    function setFoot() {
        
    }

}

?>