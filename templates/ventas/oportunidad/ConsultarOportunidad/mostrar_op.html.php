<div id='show_oportunidad'></div>
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Oportunidades<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Oportunidad</th>
	</tr>
</thead>
<tbody>
    <?php

    foreach($datos as $o){
?>
<tr>
	<td align='left'>
            <?php if(isset($o->dc_pts_bant)){ ?>
            <a href='<?php echo Factory::buildActionUrl('MostrarDet', array('id'=>$o->dc_oportunidad,'bant'=>$o->dc_pts_bant))?>' class='op_load'>
            <?php } else {?>
                <a href='<?php echo Factory::buildActionUrl('MostrarDet', array('id'=>$o->dc_oportunidad))?>' class='op_load'>
                <?php }?>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' /><?= $o->dq_oportunidad ?></a>
	</td>
</tr> <?php
}
    ?>
</tbody>
</table>
</div>

<button class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='editbtn' id='edit_oportunidad'>Editar</button>
<button type='button' id='gestion_oportunidad' class='imgbtn' style='background-image:url(images/management.png);'>Gestión</button>
<button type='button' id='historial_actividad' class='imgbtn' style='background-image:url(images/archive.png);'>Historial</button>
<button type='button' id='cotizar_oportunidad' class='imgbtn' style='background-image:url(images/doc.png);'>Cotizar</button>
</div>

<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".op_load").click(function(e){
		e.preventDefault();
		$('#show_oportunidad').html("<img src='images/ajax-loader.gif' alt='' /> cargando oportunidad ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_oportunidad');
	}).first().trigger('click');
	
	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>