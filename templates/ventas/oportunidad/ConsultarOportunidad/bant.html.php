<?php
$form->Start(Factory::buildActionUrl('GBant'), 'ing_bant');
$form->Header("<b>B.A.N.T</b><br />Indique Bant del cliente");
$pregunta1='checked="checked"';
$pregunta2='checked="checked"';
$pregunta3='checked="checked"';
$pregunta4='checked="checked"';
$ptss=0;
foreach($marcados as $pr):
    if($pr->dc_pregunta1_bant == 0):
        $pregunta1='';
    endif;
    if($pr->dc_pregunta2_bant == 0):
        $pregunta2='';
    endif;
    if($pr->dc_pregunta3_bant == 0):
        $pregunta3='';
    endif;
    if($pr->dc_pregunta4_bant == 0):
        $pregunta4='';
    endif;
    if($pts=$pr->dc_pts_bant != 0):
        $ptss=$pr->dc_pts_bant;
    endif;

endforeach;
?>
<table border="0" style="background-color: window; margin-left: 100px; margin-top: 20px;border-radius: 10px">
    <tr>
        <td  id="cl_pote1" colspan="2" align="center"  height="30px" style="background-color: red;font-weight: bolder;border-top-left-radius :10px;border-top-right-radius: 10px;-webkit-transition-duration: 0.5s">PROSPECTO</td>
        <td></td>
    </tr>
    <tr>
        <td id="cl_pote2" align="center" rowspan="7" style="background-color: red; font-weight: bolder; font-size: 20pt;-webkit-transition-duration: 0.5s" width="35px">B<br />A<br />N<br />T</td>
        <td id="pr_1" style="background-color: #0E9749;opacity: 0.5;-webkit-transition-duration: 0.5s;color: black;font-weight: bold"><br />Tiene un presupuesto asignado</td>
        <td>
            <label>
                <input type='checkbox' class="SN"  id="sino_1" name='sino_1' value='30' <?=$pregunta1?> />
            </label>
        </td>

    </tr>
    <tr><td></td></tr>
    <tr>

        <td id="pr_2" height="10px" style="background-color: #32779a;opacity: 0.5;-webkit-transition-duration: 0.5s;color: black;font-weight: bold"><br />Tiene autoridad para la compra</td>
        <td>
            <label>
                <input type='checkbox' class="SN" id="sino_2" name='sino_2' value='30' <?=$pregunta2?> />
            </label>
        </td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td id="pr_3" height="20px" style="background-color: #0E9749;opacity: 0.5;-webkit-transition-duration: 0.5s;color: black;font-weight: bold">Tiene una necesidad que resolver</td>
        <td>
            <label>
                <input type='checkbox' class="SN" id="sino_3" name='sino_3' value='20' <?=$pregunta3?> />
            </label>
        </td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td id="pr_4" height="20px" style="background-color: #32779a;opacity: 0.5;-webkit-transition-duration: 0.5s;color: black;font-weight: bold">A definido un plazo de compra</td>
        <td>
            <label>
                <input type='checkbox' class="SN" id="sino_4" name='sino_4' value='20' <?=$pregunta4?> />
            </label>
        </td>
    </tr>
    <tr>

        <td id="cl_pote3" align="center" colspan="2" height="30px" style="background-color: red;font-weight: bolder;-webkit-transition-duration: 0.5s; border-bottom-left-radius: 10px;border-bottom-right-radius: 10px">
            CLIENTE POTENCIAL 
            <input type="text" id='pts' name="pts" readOnly="readOnly" value='<?=$ptss?>' style='background-color: transparent; border: 0px;text-align: right;font-weight: bolder; width: 30px;-webkit-transition-duration: 0.5s' /> 
            %
        </td>
    </tr>
</table>
<?php
$form->Hidden('oportunidad_id',$id_op);
$form->End('Ingresar','addbtn');
?>
<script type="text/javascript">
    var v1 = document.getElementById('sino_1');
        var v2 = document.getElementById('sino_2');
        var v3 = document.getElementById('sino_3');
        var v4 = document.getElementById('sino_4');
        
        var p1 = document.getElementById('pr_1');
        var p2 = document.getElementById('pr_2');
        var p3 = document.getElementById('pr_3');
        var p4 = document.getElementById('pr_4');
        
        var suma= document.getElementById('pts').value;
    if (v1.checked == true)
        {
            p1.style.opacity = 1;
            p1.style.color = 'white';
            p1.style.fontWeight = 'bolder';
        } else
        {
            p1.style.opacity = 0.5;
            p1.style.color = 'black';
            p1.style.fontWeight = 'bolder';
        }

        if (v2.checked == true)
        {
            p2.style.opacity = 1;
            p2.style.color = 'white';
            p2.style.fontWeight = 'bolder';
        } else
        {
            p2.style.opacity = 0.5;
            p2.style.color = 'black';
            p2.style.fontWeight = 'bolder';
        }

        if (v3.checked == true)
        {
            p3.style.opacity = 1;
            p3.style.color = 'white';
            p3.style.fontWeight = 'bolder';
        } else
        {
            p3.style.opacity = 0.5;
            p3.style.color = 'black';
            p3.style.fontWeight = 'bolder';
        }

        if (v4.checked == true)
        {
            p4.style.opacity = 1;
            p4.style.color = 'white';
            p4.style.fontWeight = 'bolder';
        } else
        {
            p4.style.opacity = 0.5;
            p4.style.color = 'black';
            p4.style.fontWeight = 'bolder';
        }

        var color = '';
        switch (toNumber(suma)) {
            case 0:
                color = 'red';
                break;
            case 20:
                color = 'tomato';
                break;
            case 30:
                color = 'salmon';
                break;
            case 40:
                color = 'lightsalmon';
                break;
            case 50:
                color = 'coral';
                break;
            case 60:
                color = '#C99337';
                break;
            case 70:
                color = 'khaki';
                break;
            case 80:
                color = 'gold';
                break;
            case 100:
                color = 'yellowgreen';
                break;
        }
        document.getElementById('cl_pote1').style.backgroundColor = color;
        document.getElementById('cl_pote2').style.backgroundColor = color;
        document.getElementById('cl_pote3').style.backgroundColor = color;
    
    
$('.SN').click(function() {
        var v_1 = 0;
        var v_2 = 0;
        var v_3 = 0;
        var v_4 = 0;

        var v1 = document.getElementById('sino_1');
        var v2 = document.getElementById('sino_2');
        var v3 = document.getElementById('sino_3');
        var v4 = document.getElementById('sino_4');

        var p1 = document.getElementById('pr_1');
        var p2 = document.getElementById('pr_2');
        var p3 = document.getElementById('pr_3');
        var p4 = document.getElementById('pr_4');

        if (v1.checked == true)
        {
            v_1 = toNumber(v1.value);
            p1.style.opacity = 1;
            p1.style.color = 'white';
            p1.style.fontWeight = 'bolder';
        } else
        {
            p1.style.opacity = 0.5;
            p1.style.color = 'black';
            p1.style.fontWeight = 'bolder';
        }

        if (v2.checked == true)
        {
            v_2 = toNumber(v2.value);
            p2.style.opacity = 1;
            p2.style.color = 'white';
            p2.style.fontWeight = 'bolder';
        } else
        {
            p2.style.opacity = 0.5;
            p2.style.color = 'black';
            p2.style.fontWeight = 'bolder';
        }

        if (v3.checked == true)
        {
            v_3 = toNumber(v3.value);
            p3.style.opacity = 1;
            p3.style.color = 'white';
            p3.style.fontWeight = 'bolder';
        } else
        {
            p3.style.opacity = 0.5;
            p3.style.color = 'black';
            p3.style.fontWeight = 'bolder';
        }

        if (v4.checked == true)
        {
            v_4 = toNumber(v4.value);
            p4.style.opacity = 1;
            p4.style.color = 'white';
            p4.style.fontWeight = 'bolder';
        } else
        {
            p4.style.opacity = 0.5;
            p4.style.color = 'black';
            p4.style.fontWeight = 'bolder';
        }

        var suma = v_1 + v_2 + v_3 + v_4;
        var color = '';
        switch (suma) {
            case 0:
                color = 'red';
                break;
            case 20:
                color = 'tomato';
                break;
            case 30:
                color = 'salmon';
                break;
            case 40:
                color = 'lightsalmon';
                break;
            case 50:
                color = 'coral';
                break;
            case 60:
                color = '#C99337';
                break;
            case 70:
                color = 'khaki';
                break;
            case 80:
                color = 'gold';
                break;
            case 100:
                color = 'yellowgreen';
                break;
        }

        document.getElementById('cl_pote1').style.backgroundColor = color;
        document.getElementById('cl_pote2').style.backgroundColor = color;
        document.getElementById('cl_pote3').style.backgroundColor = color;
        document.getElementById('pts').value = suma
    });
    
</script>