<br />
<br />
<?php
$form->Start(Factory::buildActionUrl('MostrarOportunidad'),'src_oportunidad');
$form->Header("<strong>Indicar los parámetros de búsqueda de oportunidades</strong>");
?>
<table class="tab" style="text-align:left;" width="100%" id="form_container">
	<tr>
        <td>
			Número de oportunidad
		</td>
		<td>
			<?php $form->Text("Desde","op_numero_desde");?>
		</td>
		<td>
			<?php $form->Text('Hasta','op_numero_hasta');?>
		</td>
	</tr>
	<tr>
		<td>
			Fecha emisión
		</td>
		<td>
			<?php $form->Date('Desde','op_emision_desde',1,"01/".date("m/Y"));?>
		</td>
		<td>
			<?php $form->Date('Hasta','op_emision_hasta',1,1);?>
		</td>
	</tr>
	<tr>
		<td>
			Cliente
		</td>
		<td>
			<?php $form->Multiselect('','op_client',$clientes);?>
		</td>
		<td>
			<?php $form->Combobox('','op_bant',array(1=>'Mostrar Bant'),array(1)); ?>
		</td>
	</tr>
	<?php 
	if(check_permiso(89)):
	?>
	<tr>
		<td>
			Ejecutivo
		</td>
		<td>
			<?php $form->Multiselect('','op_executive',$funcionarios);?>
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	<?php 
	else:
		$form->Hidden('op_executive[]',$userdata->dc_funcionario);
	endif;
	if(check_permiso(88)):
	?>
	<tr>
		<td>
			Ejecutivo televenta
		</td>
		<td>
			<?php $form->Multiselect('','op_televenta',$funcionarios);?>
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	<?php
	else:
		$form->Hidden('op_televenta[]',$userdata->dc_funcionario);
	endif;
	?>
	<tr>
		<td>
			Linea de negocio
		</td>
		<td>
			<?php $form->Multiselect('','op_buss_line',$linea_negocio);?>
        </td>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td>
			Marca
		</td>
		<td>
			<?php $form->Multiselect('','dc_marca',$marca);?>
		</td>
		<td>
			&nbsp;
		</td>
	</tr><!--<tr><td colspan="3" align="center">
	<?php//$form->Combobox('Incluir','op_status',array('Oportunidades abiertas','Oportunidades Cerradas'),array(0,1)," ");?>
	</td></tr>-->
</table>
	<?php $form->End('Ejecutar consulta','searchbtn');
?>
<script type="text/javascript">
$('#op_client,#op_executive,#op_buss_line,#dc_marca, #op_televenta').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});

$(':checkbox[name="op_status[]"]').click(function(e){
	if($(':checked[name="op_status[]"]').size() < 1){
		e.preventDefault();
	}
});
</script>