<?php foreach ($datos as $data) { ?>
    <?php
    $form->Start(Factory::buildActionUrl('ActualizaOp'),'edita_oportunidad','ed_form');
    $form->Header("<b>Indique los datos actualizados de la oportunidad</b><br />Los campos marcados con [*] son obligatorios");
    echo('<hr />');
    $form->Section();
    echo("<fieldset>");
    $checked = $data->dg_cliente != "" ? 'checked="checked"' : '';
    $disable = $data->dg_cliente == "" ? "disabled='disabled'" : '';
    $form->Select('Cliente', 'op_cliente', $clientes, 1, $data->dc_cliente);
    echo("<label><input type='checkbox' name='op_other_cli' {$checked}/>Otro</label>
<input type='text' name='op_cliente_txt' id='op_cliente_txt' {$disable} class='inputtext' value='{$data->dg_cliente}' size='30' /></fieldset><br />");
    $form->Text("Monto aproximado (USD)", 'op_monto', 1, 13, round($data->dq_monto / $cambio));
    $form->EndSection();
    $form->Section();
    $form->Textarea("Comentario", 'op_comentario', 1, $data->dg_comentario);
    $form->Text("E-Mail de contacto", 'op_email', 1, 255, $data->dg_correo);
    $form->Select('Ejecutivo', 'op_ejecutivo', $funcionarios, 1, $data->dc_ejecutivo);
    $form->Select('Tele Venta', 'tele_venta', $funcionarios, 1, $data->dc_tele_venta);

    $form->EndSection();
    $form->Section();
    $form->Text('Nombre Contacto', 'op_nom_contacto', 1, 255, $data->dg_nombre_contacto);
    $form->Text('Telefono Contacto', 'op_fono_contacto', 1, 255, $data->dg_fono);
    $form->Text('Deal ID', 'deal_id_op', 0, '', $data->dg_deal_id);
    $form->EndSection();
}$form->Group();
?> 
<div style="text-align: center">
    <label ><strong>M.P.M &nbsp;(Monto Por Marca)</strong></label><br/>
    <table class="tab" width="100%">
        <thead>
		<th>[X]</th>
        <th>Marca[*]</th>        
        <th>Linea de negocio[*]</th>
        <th>Tipo de Producto[*]</th>
        <th>Cantidad[*]</th>
        <th>Monto (USD)[*]</th>
		<th>Margen[%][*]</th>
        </thead>
        <tbody id="detalle_list">
            <?php
            $i=0;
            $suma_cant=0;
            $suma_total=0;
            foreach ($detalle_op as $d):
                ?>
            <tr align="center">
                <input type="hidden" name="item_id[]" id="item_id[]" class="item_id" value="<?= $i ?>" />
                <td>
                    <img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
                </td>
				<td>				
                    <?php $form->Select2('', 'dc_marca[]', $marca, 1, $d->dc_marca, 'width:190px'); ?>
                </td>
                <td>
                    <?php $form->Select2('', 'op_linea_negocio[]', $linea_negocio, 1, $d->dc_linea_negocio, 'width:190px') ?>
                </td>
                <td>
                    <?php $form->Select2('', 'op_tipo_producto[]', $tipo_producto, 1, $d->dc_tipo_producto, 'width:190px') ?>
                </td>
                <td>
                    <input type="text" name="cantidad[]" value="<?=$d->dq_cantidad ?>" class="cantidad" style="width: 70px;text-align: center" id="cantidad[]" required="required" maxlength="9" />
                </td>
                <td>
                    <input type="text" name="valor[]" id="valor[]" value="<?=  round($d->dq_monto_marca/$cambio) ?>" class="valor" style="width: 100px;text-align: center" required="required" maxlength="13" />
                </td>
				<td>
                    <input type="text" name="margen[]" id="margen[]" value="<?=  round($d->dq_margen * 100) ?>" class="margen" style="width: 100px;text-align: center" required="required" maxlength="3" />
                </td>
            </tr>
        <?php 
        $i++;
        $suma_cant+=$d->dq_cantidad;
        $suma_total+=$d->dq_monto_marca;
        endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">
                </td>
                <!--td class="cantidad_p" Comentado el 09-12-14>
                    <?//=$suma_cant?>
                </td-->
                <td class="total_op">
                    <?=  moneda_local($suma_total/$cambio) ?>
                </td>
				<td></td>
            </tr>
        </tfoot>
    </table>
</div>
<div class="center">
    <?php
    $form->Button('Agregar Detalle', 'id="add_detalle"', 'addbtn');
    ?>
</div>
<?php
$form->Hidden('op_id', $idsa);
$form->End('Editar', 'editbtn');
?>
    
<table class="hidden">
    <tbody id="detalle_template">
        <tr align="center">
            <input type="hidden" name="item_id[]" class="item_id" />
			<td>
                <img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
            </td>
            <td>
                <?php $form->Select2('','dc_marca[]', $marca, 1, '', 'width:190px'); ?>
            </td>
            <td>
                <?php $form->Select2('','op_linea_negocio[]', $linea_negocio, 1, '', 'width:190px') ?>
            </td>
            <td>
                <?php $form->Select2('','op_tipo_producto[]', $tipo_producto, 1, '', 'width:190px') ?>
            </td>
            <td>
                <input type="text" name="cantidad[]" id="cantidad[]" class="cantidad" style="width: 70px;text-align: center" required="required" maxlength="9" />
            </td>
            <td>
                <input type="text" name="valor[]" id="valor[]" class="valor" style="width: 100px;text-align: center" required="required" maxlength="13" />
            </td>
			<td>
                    <input type="text" name="margen[]" id="margen[]"  class="margen" style="width: 100px;text-align: center" required="required" maxlength="3" />
            </td>
        </tr>
</tbody>
</table>

<script src="<?php echo Factory::getJavascriptUrl('ed_oportunidad') ?>?v=2_0_26" type="text/javascript"></script>
<script type="text/javascript">
    js_data.init();
    $('.valor').change(function(){
        var s=0;
                $('.valor').each(function(){
                    var g = parseFloat(this.value);
                    if(!isNaN(g)){
                        s=s+g;
                    }
                });
        $('.total_op').text(s);  
    });
    $('.cantidad').change(function(){
        var s=0;
                $('.cantidad').each(function(){
                    var g = parseFloat(this.value);
                    if(!isNaN(g)){
                        s=s+g;
                    }
                });
        $('.cantidad_p').text(s);  
    });
	
	//Comprueba si un campo es numérico, alerta si tiene letras
	var comprobarNumerico = function(campo){
		var monto = $(campo).val();
		var filtro = /[^0-9.]/; 
		if(filtro.test(monto)){
			alert('Numero puede tener un largo máximo de 12 numeros, con dos decimales incluidos separados con punto. Las letras serán eliminadas');
			$(campo).focus();
		}
	}
	
	//Verificar que el monto aproximado es correcto}
	$('#op_monto').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	
	$('.cantidad').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9.]/g,'');
	});
	
	$('.valor').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9.]/g,'');
	});
	
	$('.margen').change(function () {
		campo = this;
		comprobarNumerico(campo);
		this.value = this.value.replace(/[^0-9.]/g,'');
	});
	
	//Fin de control de textos numericos
    
    $('.del_detail').click(function(){
        $(this).parent().parent().remove();
        var s=0;
                $('.valor').each(function(){
                    var g = parseFloat(this.value);
                    if(!isNaN(g)){
                        s=s+g;
                    }
                });
         var s2=0;
                $('.cantidad').each(function(){
                    var g = parseFloat(this.value);
                    if(!isNaN(g)){
                        s2=s2+g;
                    }
                });
        $('.cantidad_p').text(s2); 
        $('.total_op').text(s);  
    });
    
    $('.ed_form').submit(function(e) {
        e.preventDefault();
        var mail=$('#op_email').val();
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/.test(mail.trim()))) {
            $('#op_email').addClass('invalid');
            $('#op_email').focus();
            return;
        }
        monto = parseFloat($('#op_monto').val());
        if (!monto) {
            $('#op_monto').addClass('invalid');
            $('#op_monto').focus();
            return;
        }
        $('#op_monto').val(monto);
        $('#op_email,#op_monto').removeClass('invalid');
         i = '#' + $(this).attr('id');
        confirmEnviarForm(i, i + '_res');    
    });
    var txt = document.getElementById('op_cliente_txt').value;
    if (txt) {
        $('#op_cliente').attr('required', false);
        $('#op_cliente').attr('disabled', true);
    }
    $('input[name=op_other_cli]').click(function() {
        if ($(this).attr('checked')) {
            $(this).parent().next().attr('disabled', false).attr('required', true);
            $('#op_cliente').attr('required', false);
        } else {
            $(this).parent().next().attr('disabled', true).attr('required', false);
            $('#op_cliente').attr('required', true);
        }
    });
</script>