<?php
foreach ($datos as $data) {
    
    $fecha_cierre=  explode(' ',$data->df_proyeccion_cierre);
    $fecha_sep=  explode('-', $fecha_cierre[0]);
    
        $data->df_proyeccion_cierre=$fecha_sep[2].'/'.$fecha_sep[1].'/'.$fecha_sep[0];
    
    /*$estado = 'Cerrada';
    if ($data->dm_estado == '0') {
        $estado = 'Abierta';
    }*/

    $bant1 = '';
    if (isset($bant)) {
        $bant1 = "
	<td>B.A.N.T.</td>
	<td id='bant'>{$data->dc_pts_bant} &nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' class='editbtn' id='edit_bant'>&nbsp;</button></td>
</tr><tr>";
    }
    if($data->dg_cliente == " " || $data->dg_cliente == "")
    {
        $data->dg_cliente=$data->dg_razon;
    }
    ?>
    <div class='title center'>Oportunidad nº <?= $data->dq_oportunidad ?></div>
    <table class='tab' width='100%' style='text-align:left;'>
        <caption>Cliente:<br /><strong> <?= $data->dg_cliente ?></strong></caption>
        <tr>
            <td width='160'>Fecha emision</td>
            <td><b><?= $data->df_emision ?></td>
            <td rowspan='12' width='250'>
                <label>Estado de Oportunidad</label><br />
                <h3 class='info' style='margin:0'><?= $data->dg_estado ?></h3>
            </td>
        </tr><tr>
                <td width='160'>Fecha de Cierre (Estimado)</td>
            <td><b><?= $data->df_proyeccion_cierre ?></td>
        </tr><tr>
            <td>Deal ID</td>
            <td><b><?=$data->dg_deal_id?></b></td>
        </tr><tr>
            <td>Cotizaciones asignada</td>
            <td id='load_cotizaciones'><a href='#'>Ver</a></td>
        </tr><tr>
            <?= $bant1 ?>
            <td>Monto estimado:</td>
            <td ><?= $prefijo ?> <b>    $ <?= number_format($data->dq_monto / $cambio,2) ?></b></b></td>
        </tr><tr>
            <td>Llamada</td>
            <td ><b><?= $data->df_llamada ?></b></td>
        </tr><tr>
            <td>Correo</td>
            <td ><b><?= $data->dg_correo ?></b></td>
        </tr><tr>
            <td>Telefono de Contacto</td>
            <td ><b><?= $data->dg_fono ?></b></td>
        </tr><tr>
            <td>Nombre del Contacto</td>
            <td ><b><?= $data->dg_nombre_contacto ?></b></td>
        </tr><tr>
            <td>Ejecutivo</td>
            <td><b><?= $data->dg_ejecutivo ?></b></td>
        </tr><tr>
            <td>Tele-Venta</td>
            <td ><b><?= $data->dg_responsable ?></b></td>
        </tr><tr>
            <td><b>Comentario</b></td>
            <td colspan="2" ><?= $data->dg_comentario ?></td>
        </tr></table>
    <table class='tab bicolor_tab' width='100%' style='text-align:left;'>
        <caption>M.P.M (Monoto Por Marca)</caption>
        <tr align='center'>
            <th>Marca</th>
            <th>Linea Negocio</th>
            <th>Tipo de Producto</th>
            <th>Cantidad</th>
            <th>Monto (pesos)</th>
            <th>Monto (<?= $prefijo ?>)</th>
			<th>Margen [%]</th>
        </tr>
        <?php
        if($detalle){
        $suma=0;
        foreach ($detalle as $prd) {
                $suma=$suma+$prd->dq_monto_marca;
                ?>
                <tr align='center'>
                    <td><?= $prd->dg_marca ?></td>
                    <td><?= $prd->dg_linea_negocio ?></td>
                    <td><?=$prd->dg_tipo_producto?></td>
                    <td><?=$prd->dq_cantidad?></td>
                    <td>$ <?= number_format($prd->dq_monto_marca) ?></td>
                    <td>$ <?= number_format($prd->dq_monto_marca/$cambio,2) ?></td>
					<td><?= number_format($prd->dq_margen * 100) ?></td>
                </tr>
                
                <?php
        }
        ?>
                <tr align='center' >
                    <td  style="font-weight: bolder" colspan='4' align="right">Total</td>
                    <td style="background-color: wheat;font-weight: bolder">$ <?=number_format($suma)?></td>
                    <td  style="background-color: wheat;font-weight: bolder">$ <?=number_format($suma/$cambio,2)?></td>
					<td></td>
                </tr>
    </table>
<?php } 
}?>
<script type="text/javascript">
$('#print_version').unbind('click');
$('#print_version').click(function(){
	window.open("<?php echo Factory::buildActionUrl('VerOp', array('ids'=>$id)) ?>",'print_oportunidad','width=800;height=600');
});
$('#edit_oportunidad').unbind('click');
$('#edit_oportunidad').click(function(){
	loadOverlay("<?php echo Factory::buildActionUrl('EditarOp', array('ids'=>$id)) ?>");
});
$('#gestion_oportunidad').unbind('click');
$('#gestion_oportunidad').click(function(){
        loadOverlay("<?php echo Factory::buildUrl('GestionarOportunidad','ventas','index','oportunidad', array('id'=>$id)) ?>");
       
});
$('#historial_actividad').unbind('click');
$('#historial_actividad').click(function(){
	loadOverlay("sites/ventas/oportunidad/history_gestion_oportunidad.php?id=<?=$_POST['id'] ?>");
});
$('#cotizar_oportunidad').unbind('click');
$('#cotizar_oportunidad').click(function(){
	loadOverlay("sites/ventas/oportunidad/cotizar_oportunidad.php?id=<?=$_POST['id'] ?>");
});
$('#load_cotizaciones a').click(function(e){
	e.preventDefault();
	loadFile('sites/ventas/oportunidad/proc/get_cotizaciones.php?id=<?=$_POST['id'] ?>','#load_cotizaciones');
});

$('#edit_bant').unbind('click');
$('#edit_bant').click(function(){
	loadOverlay("<?php echo Factory::buildActionUrl('bant', array('ids'=>$id)) ?>");
});
</script>