<?php
$form->Start('sites/ventas/factura_compra/proc/null_factura_compra.php','null_factura_form');
$form->Header('Indique las razones por las que se anulará la factura de compra');
?>
<div class="alert">
  Atención, está a punto de anular la factura N° <b><?php echo $factura->dq_factura ?></b>
</div>
<?php
  $form->Section();
    $form->DBSelect('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),true);
  $form->EndSection();
  $form->Section();
    $form->Textarea('Comentario','null_comentario',true);
  $form->EndSection();
  $form->Hidden('id_factura',$factura->dc_factura);
$form->End('Anular','delbtn');
?>