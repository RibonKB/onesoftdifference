<div class="warning">
  No se puede anular la factura de compra debido a que esta tiene relacionada y vigentes las siguientes guías de recepción
</div>
<table class="tab" width="100%">
  <thead>
    <tr>
      <th>Guía Recepción</th>
      <th>Fecha Emisión</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($guias as $g): ?>
    <tr>
      <td><?php echo $g->dq_guia_recepcion ?></td>
      <td><?php echo $g->df_fecha_emision ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>