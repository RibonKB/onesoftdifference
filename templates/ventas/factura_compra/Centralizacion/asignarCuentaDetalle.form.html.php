<table class="tab" width="100%">
  <caption>Detalle Seleccionado</caption>
  <tbody>
    <tr>
      <td width="100">Código</td>
      <td><strong><?php echo $detalle->producto->dg_codigo ?></strong></td>
    </tr>
    <tr>
      <td>Descripcion</td>
      <td><strong><?php echo $detalle->dg_descripcion ?></strong></td>
    </tr>
  </tbody>
</table>
<?php $form->Start(Factory::buildActionUrl('procesarAsignarCuentaDetalle'),'asignar_cuenta_detalle') ?>
<?php $form->Header("Seleccione la cuenta contable asociada al detalle seleccionado.<br />".
                    "<strong>Los campos marcados con [*] son obligatorios</strong>") ?>
<?php $form->Select('Cuenta Contable','dc_cuenta_contable',$cuentas,true)  ?>
<?php $form->Hidden('dc_detalle',$detalle->dc_detalle) ?>
<?php $form->Hidden('dc_factura',$fc->dc_factura) ?>
<?php $form->End('Asignar Cuenta','editbtn') ?>
