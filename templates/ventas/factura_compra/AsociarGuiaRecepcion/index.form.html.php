<div class="center">
  <?php $form->Start(Factory::buildActionUrl('procesarAsignacion'),'cr_asignacion_guia_recepcion'); ?>
  <?php $form->Header('<strong>Indique la guía de recepción que desea relacionar a la factura de compra</strong><br />'.
                      'Los campos marcados con [*] son obligatorios'); ?>
    <?php $form->Text('Guía de Recepción','dq_guia_recepcion',1,20); ?>
    <?php $form->Hidden('dc_factura',$factura->dc_factura) ?>
  <?php $form->End('Asociar','addbtn'); ?>
</div>