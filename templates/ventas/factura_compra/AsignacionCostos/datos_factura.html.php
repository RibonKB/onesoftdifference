<table class="tab" width="100%" align="center">
	<caption>Resumen Factura Compra</caption>
	<tbody>
    	<tr>
        	<td>Número Interno</td>
            <td align="right"><b><?php echo $factura->dq_factura ?></b></td>
        </tr>
        <tr>
        	<td>Folio Factura</td>
            <td align="right"><b><?php echo $factura->dq_folio ?></b></td>
        </tr>
        <tr>
        	<td>Proveedor</td>
            <td align="right"><b><?php echo $factura->dg_razon ?></b></td>
        </tr>
        <tr>
        	<td>Total NETO</td>
            <td align="right"><b><?php echo Functions::monedaLocal($factura->dq_neto) ?></b></td>
        </tr>
    </tbody>
</table>