<div class="hidden" >
<table>
    <tr class="asig_copy asig_detail" >
        <td align='center'>
			<img src='images/delbtn.png' alt='X' title='Eliminar' title='Eliminar detalle' class='del_detail' />
            <!--img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" /-->
            <?php $form->Hidden('dc_asignacion[]', '');?>
            <?php $form->Hidden('dm_contrato[]', '0');?>
        </td>
        <td>
            <?php 
                $form->Text(false,'', true, 255, '', 'inputtext auto_notaventa');
            ?>
        </td>
        <td class="det_factura_venta" ></td>
        <td><?php $form->Text(false,'dq_monto[]', true, 255, '', 'inputtext det_monto'); ?></td>
        <td><?php $form->Text(false,'dg_descripcion[]', true, 255, '', 'inputtext det_descripcion'); ?></td>
        <td>
            <?php $form->Select2('','dc_detalle_factura_compra[]',$selectDetalles,true,
                    count($selectDetalles) > 1 ? '':1,"width: 150px");
            ?>
        </td>
        <td><?php $form->Select2('','dc_cebe[]',$cebe,true,'',"width: 150px"); ?></td>
        <td><?php $form->Date3('','df_fecha_asignacion[]','','inputtext det_fecha',1); ?></td>
        <td class="det_cbcuotas"></td>
        <td class="td_numero_cuotas"><?php $form->Text(false, 'dc_cuotas[]', true, 3, '1', 'inputtext det_cuotas', 2,0,1); ?></td>
        <td><?php $form->hidden("dc_cuotas_saldadas[]",'0'); ?></td>
        <td class="det_monto_cuota" >0</td>
    </tr>
</table>
</div>