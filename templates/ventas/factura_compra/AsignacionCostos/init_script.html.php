<script type="text/javascript" src="<?php echo Factory::getJavascriptUrl('asigna_costo_nv'); ?>" ></script>
<script>
    
    var acnv = {
        autocompleteUrl: "<?php echo Factory::buildActionUrl('notaVentaAutocompleter'); ?>",
        montoTotal: <?php echo $factura->dq_neto; ?>,

    };
    asigna_costos.init();
    
<?php

    foreach($costosAsignados as $v):
?>
    
    var detail = asigna_costos.create_empty_detail();
    pymerp.init(detail);            
    asigna_costos.elimina_detalle(detail);
    asigna_costos.monto_event(detail);
    asigna_costos.edit_cuota_event(detail);
    
    detail.find('img.right').remove();
    detail.find('.dc_asignacion').val('<?php echo $v->dc_asignacion; ?>');
    detail.find('.det_monto').val('<?php echo $v->dq_monto; ?>');
    detail.find('.det_descripcion').val('<?php echo $v->dg_descripcion; ?>');
    detail.find('.dc_detalle_factura_compra').val('<?php echo $v->dc_detalle_factura_compra; ?>');
    detail.find('.dc_cebe').val('<?php echo $v->dc_cebe; ?>');
    detail.find('.det_fecha').val('<?php echo $this->getConnection()->dateLocalFormat($v->df_fecha_asignacion); ?>');
<?php
    if($v->dm_contrato):
?>
    detail.find('.det_cbcuotas').append($('<input>').attr('class','det_divide_cuotas').attr('type','checkbox').attr('checked', true));
    detail.find('.dm_contrato').val('1');
    var cuotas_pendientes  = <?php echo $v->dc_cuotas - $v->dc_facturadas ; ?>;
    detail.find('.td_numero_cuotas').append($('<input>').attr('class','det_cuotas_pendientes').attr('type','hidden').val(cuotas_pendientes));
    asigna_costos.distribuye_cuotas(detail);
    
<?php
    endif;
?>
	detail.find('.dc_cuotas_saldadas').val(<?php echo $v->dc_cuotas_saldadas; ?>).parent().append('<?php echo $v->dc_cuotas_saldadas; ?>');
    detail.find('.det_factura_venta').append($('<select>', {
        name:       'dc_factura_venta[]',
        class:      'inputtext cb_factura_venta',
        style:      'width: 150px'
    }));
    detail.find('.cb_factura_venta').append($('<option>', {
        value: 0,
        text: 'No asignar Factura de venta'
    }));
    
<?php
    foreach($v->facturasVenta as $fv):
?>
    detail.find('.cb_factura_venta').append($('<option>', {
        value: <?php echo $fv->dc_factura; ?>,
        text:  '<?php echo $fv->dq_folio; ?> (<?php echo $fv->dq_factura; ?>)'
    }));
<?php
    if($fv->dc_factura == $v->dc_factura_venta && $fv->dm_centralizada == 1):
?>
    detail.find('.del_detail').remove();
    // detail.find('.cb_factura_venta').attr('disabled','disabled').removeAttr('name');
    // detail.find('.det_factura_venta').append($('<input>').attr('name','dc_factura_venta[]').attr(
        // 'type','hidden').val('<?php echo $v->dc_factura_venta; ?>'));
    // detail.find('.det_monto').attr('readonly','readonly');
    // detail.find('.det_descripcion').attr('readonly','readonly');
    // detail.find('.dc_cebe').attr('disabled','disabled').removeAttr('name');
    // detail.find('.dc_cebe').parent().append($('<input>').attr('name','dc_cebe;[]').attr(
        // 'type','hidden').val('<?php echo $v->dc_cebe;; ?>'));
    // detail.find('.det_fecha').attr('readonly','readonly');
    // detail.find('.det_fecha').unbind();
    // detail.find('.det_divide_cuotas').attr('disabled','disabled');
    // detail.find('.det_cuotas').attr('readonly','readonly');
    
<?php    
    endif;
    endforeach;
?>
    detail.find('.cb_factura_venta').val('<?php echo $v->dc_factura_venta; ?>');
    detail.find('.det_cuotas').val('<?php echo $v->dc_cantidad_cuotas; ?>');
    asigna_costos.actualiza_monto_cuota(detail);
	
    detail.find('.cb_factura_venta').attr('disabled','disabled').removeAttr('name');
    detail.find('.det_factura_venta').append($('<input>').attr('name','dc_factura_venta[]').attr(
        'type','hidden').val('<?php echo $v->dc_factura_venta; ?>'));
    detail.find('.det_monto').attr('readonly','readonly');
    detail.find('.det_descripcion').attr('readonly','readonly');
    detail.find('.dc_detalle_factura_compra').attr('disabled','disabled').removeAttr('name');
    detail.find('.dc_detalle_factura_compra').parent().append($('<input>').attr('name','dc_detalle_factura_compra[]').attr(
        'type','hidden').val('<?php echo $v->dc_detalle_factura_compra; ?>'));
    detail.find('.dc_cebe').attr('disabled','disabled').removeAttr('name');
    detail.find('.dc_cebe').parent().append($('<input>').attr('name','dc_cebe[]').attr(
        'type','hidden').val('<?php echo $v->dc_cebe; ?>'));
    detail.find('.det_fecha').attr('readonly','readonly');
    detail.find('.det_fecha').unbind();
    detail.find('.det_divide_cuotas').attr('disabled','disabled');
    detail.find('.det_cuotas').attr('readonly','readonly');
	
    $('#asig_detalles').append(detail);
    var input_nv = detail.find('.auto_notaventa');
    var div = pymerp.getEditableBox('<?php echo $v->dq_nota_venta; ?>').width(111);
	div.find('img.right').remove();
    div.append($('<input>').attr('name','dc_nota_venta[]').attr('type','hidden').val('<?php echo $v->dc_nota_venta; ?>'));
    input_nv.replaceWith(div);
    asigna_costos.actualiza_total();
	
	
    
<?php    
    endforeach;

?>
    
</script>