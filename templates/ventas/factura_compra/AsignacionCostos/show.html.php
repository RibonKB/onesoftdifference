<?php

$form->start(Factory::buildActionUrl('asignarCostos'),'asignar_costos');
$form->Header('Asigne costos a las notas de ventas a partir de esta factura de compra');
$form->Hidden('dc_factura', $factura->dc_factura);

echo '<br />';
include __DIR__ . '/datos_factura.html.php';
echo '<br />';
include __DIR__ . '/template_detalles.html.php';

$form->Button('Agregar Detalle','','addbtn add_item');
$form->End('Asignar Costos','addbtn btn_enviar');

include __DIR__ . '/maqueta_detalles.html.php';
include __DIR__ . '/init_script.html.php';