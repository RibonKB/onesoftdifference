<?php

$form->start(self::buildActionUrl('search'), 'search_factura_compra');
$form->header('Seleccione una Factura de compra para comenzar');
$this->getErrorMan()->showInfo('La factura de compra no debe estar relacionada con una Nota de Venta');
$form->Section();
$form->Text('Factura de Compra', 'dq_factura_compra', true);
$form->Hidden2('dc_factura','');
$form->EndSection();
$form->End('Asignar Costos');

$facturaCompraAutoCompleter = Factory::getJavascriptUrl('fc_autocompleter');
$facturaCompraAutoCompleterAction = Factory::buildActionUrl('facturaCompraAutocompleter');

$preload = "<script type='text/javascript'>";
$preload .= "var fc_auto_url = '{$facturaCompraAutoCompleterAction}';";
$preload .= "</script>";

echo $preload;
echo "<script type='text/javascript' src='{$facturaCompraAutoCompleter}'></script>";