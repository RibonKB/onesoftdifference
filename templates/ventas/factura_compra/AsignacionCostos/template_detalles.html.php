<table width='100%' class='tab bicolor_tab' id="asig_tabla_detalles">
    <caption>Detalle de asignación de costos</caption>
    <thead>
        <tr>
            <th></th>
            <th width="130px" >Nota de Venta</th>
            <th width="130px" >Factura Venta</th>
            <th width="130px" >Monto</th>
            <th width="150px" >Descripción</th>
            <th width="130px" >Detalle Factura</th>
            <th width="130px" >CeBe</th>
            <th width="130px" >Fecha</th>
            <th width="60px" >Distribuir en cuotas</th>
            <th width="60px" >N° de cuotas</th>
            <th width="50px" >Cuotas saldadas</th>
            <th width="80px" >Monto por cuota</th>
        </tr>
    </thead>
    
    <tbody id="asig_detalles" ></tbody>
    
    <tfoot>
        <tr>
            <th colspan="3" >Total Utilizado</th>
            <th id="asig_total" ><?php $form->Hidden('asig_total','0'); ?>0</th>
            <th colspan="8"></th>
        </tr>
    </tfoot>
</table>