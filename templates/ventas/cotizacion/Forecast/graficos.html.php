<ul id="tabs">
  <li><a href="#" class="current">Gráficos</a></li>
  <li><a href="#">Ingresos individuales</a></li>
  <li><a href="#">Detalles</a></li>
</ul>
<br class="clear" />
<div class="tabpanes">
  
  <div>
    <div id="grafico_torta" style="width:450px;height:300px;float:left;"></div>
    <div id="grafico_torta_estado" style="width:450px;height:300px;float:left;"></div>
    <div id="grafico_torta_marca" style="width:450px;height:300px;float:left;"></div>
    <div id="grafico_torta_linea" style="width:450px;height:300px;float:left;"></div>
  </div>

  <div>
    <div id="data_table"></div>
    <br class="clear" />
  </div>

  <div>
    <div id="detail_data_table">

    <table class="tab" width="2500">
    <thead><tr>
        <th>Cotización</th>
        <th>Fecha emisión</th>
        <th>Oportunidad</th>
        <th>Estado</th>
        <th>Fecha Última Gestión</th>
        <th>Gestión</th>
        <th>Ejecutivo</th>
        <th>Cliente</th>
        <th>Marca</th>
        <th>Linea negocio</th>
        <th>Descripción</th>
        <th>Código</th>
        <th>Proveedor</th>
        <th>Precio Unitario</th>
        <th>Costo unitario</th>
        <th>Cantidad</th>
        <th>Precio total</th>
        <th>Costo total</th>
        <th>Margen</th>
    </tr></thead>
    <tbody>
    <?php foreach($data as $d): ?>
        <tr>
            <td><?php echo $d->dq_cotizacion ?></td>
            <td><?php echo $d->df_fecha_emision ?></td>
            <td><?php echo $d->dq_oportunidad ?></td>
            <td class="dc_estado<?php echo $d->dc_estado ?>"><?php echo $d->dg_estado ?></td>
            <td><?php echo $d->df_ultima_gestion ?></td>
            <td><?php echo $d->dg_gestion ?></td>
            <td class="dc_ejecutivo<?php echo $d->dc_ejecutivo ?>">
                <?php echo $d->dg_ejecutivo ?>
            </td>
            <td class="dc_cliente<?php echo $d->dc_cliente ?>">
                <?php echo $d->dg_razon ?>
            </td>
            <td class="dc_marca<?php echo $d->dc_marca ?>">
                <?php echo $d->dg_marca ?>
            </td>
            <td class="dc_linea_negocio<?php echo $d->dc_linea_negocio ?>">
                <?php echo $d->dg_linea_negocio ?>
            </td>
            <td><?php echo $d->dg_descripcion ?></td>
            <td><?php echo $d->dg_producto ?></td>
            <td><?php echo $d->dg_proveedor ?></td>
            <td align="right"><?php echo Functions::monedaLocal($d->dq_precio_venta,$tipo_cambio->dm_decimales) ?></td>
            <td align="right"><?php echo Functions::monedaLocal($d->dq_precio_compra,$tipo_cambio->dm_decimales) ?></td>
            <td align="center"><?php echo $d->dq_cantidad ?></td>
            <td align="right"><?php echo Functions::monedaLocal($d->dq_precio_total,$tipo_cambio->dm_decimales) ?></td>
            <td align="right"><?php echo Functions::monedaLocal($d->dq_costo_total,$tipo_cambio->dm_decimales) ?></td>
            <td align="right"><?php echo Functions::monedaLocal($d->dq_margen,$tipo_cambio->dm_decimales) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
        <th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
        <th align="right"></th>
        <th align="right"></th>
        <th></th>
        <th align="right"></th>
        <th align="right"></th>
        <th align="right"></th>
    </tfoot>
    </table>

    </div>
  </div>
</div>

<script type="text/javascript">
	js_data.piePlotData = <?php echo json_encode($ejecutivo) ?>;
	js_data.piePlotStatus = <?php echo json_encode($estado) ?>;
	js_data.piePlotMarca = <?php echo json_encode($marca) ?>;
	js_data.piePlotLinea = <?php echo json_encode($lineaNegocio) ?>;
	
	js_data.cotizacionData = <?php echo json_encode($data) ?>;
	js_data.tcDecimals = <?php echo $tipo_cambio->dm_decimales ?>;
	
	$('#detail_data_table table').tableExport();
</script>