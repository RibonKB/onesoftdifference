<?php
$form->Start(Factory::buildActionUrl('obtenerForecast'),'load_forecast','load_forecast','method="post"');
$form->Header('Indique los filtros a aplicar en el forecast');
$form->Section();

	$form->Select('Mes inicio','dc_mes_inicio',$meses,1,1);
	$form->Select('Mes fin','dc_mes_final',$meses,1,date('m'));

$form->EndSection();
$form->Section();

	$form->Text('Año','dc_anho',1,4,date('Y'));
	$form->DBSelect('Tipo de cambio','dc_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'));

$form->EndSection();
$form->End('Ejecutar');
?>
<div id="result_forecast"></div>
<script type="text/javascript" src="jscripts/lib/jquery.flot.js"></script>
<script type="text/javascript" src="jscripts/lib/jquery.flot.pie.js"></script>
<script type="text/javascript" src="jscripts/sites/ventas/cotizacion/src_forecast.js?v0_11_3a"></script>