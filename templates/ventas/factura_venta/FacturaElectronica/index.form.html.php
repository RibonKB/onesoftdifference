<?php
	$form->Start($this->buildActionUrl('emitirFactura'),'cr_emitir_factura_electronica');
	$form->Header('<b>Confirmar la emisión de la Factura de Venta como Electrónica</b>');
?>
<div class='hidden div_leasing' >
<?php
    echo $this->getErrorMan()->showAviso('Recuerde que debe existir un detalle que contenga el Número de Operación Leasing');
?>
</div>
<?php
	$form->Section();
		$form->Select('Tipo de factura','dc_tipo_factura',array(
			33 => "Factura electrónica",
			34 => "Factura electrónica Exenta"
		),true,33);
    $form->Combobox3('', 'dm_leasing', array('Facturación Leasing'));
	$form->EndSection();
    $form->Section();
?>
<div class='hidden div_leasing' >
<?php
    $form->Text('Código Interno Receptor', 'dc_cod_receptor');
    $form->Date('Fecha Orden de compra', 'df_fecha_odc');
?>
</div>
<?php
    $form->EndSection();
	$form->Hidden('dc_factura',$dc_factura);
	$form->End('Emitir','addbtn');
?>
<script type="text/javascript" src="<?php echo Factory::getJavascriptUrl('FacturaElectronicaLeasing'); ?>" ></script>
<script>
    FacturaElectronicaLeasing.init();
</script>