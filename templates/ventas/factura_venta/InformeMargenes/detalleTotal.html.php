<tr <?php if ($d->dg_tipo_doc == 'NC'): ?>style="color:#F00;"<?php endif ?>>
	<td>
	<?php if(!$d->dm_margen_centralizada):?>
		<input type="checkbox" name="dc_documento[<?php echo $d->dg_tipo_doc; ?>][]" value="<?php echo $d->dc_documento; ?>" class="cb_documento" />
	<?php endif; ?>
	</td>
	<td>
		<a href="<?php
		echo Factory::buildActionUrl('obtenerDetalleDocumento', array(
			'dc_documento' => $d->dc_documento,
			'dc_tipo_cambio' => $tipo_cambio->dc_tipo_cambio,
			'tipo' => $d->dg_tipo_doc
		))
		?>&<?php echo $dataTotalUrl ?>" class="enlace_carga_detalle">
			<img src="images/addbtn.png" alt="(+)" />
		</a>
	</td>
	<td><?php echo $d->dg_tipo_doc ?></td>
	<td><?php echo $d->dq_documento ?></td>
	<td><?php echo $d->dq_folio ?></td>
	<td><?php echo $d->dg_cliente ?></td>
	<td><?php echo $d->dq_nota_venta ?></td>
	<td><?php echo $d->dq_orden_servicio ?></td>
	<td><?php echo $d->dg_nombre_ejecutivo . ' ' . $d->dg_ap_ejecutivo . ' ' . $d->dg_am_ejecutivo ?></td>
	<td align="right">
		<b><?php echo Functions::monedaLocal($d->dq_venta_total / $tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></b>
	</td>
	<td align="right">
		<b><?php echo Functions::monedaLocal($d->dq_costo_total / $tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></b>
	</td>
	<td align="right">
		<b><?php echo Functions::monedaLocal($d->dq_margen_total / $tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></b>
	</td>
	<td align="center">
		<b><?php echo Functions::monedaLocal($d->dq_margen_porcentual_total,4) ?></b>
	</td>
</tr>
