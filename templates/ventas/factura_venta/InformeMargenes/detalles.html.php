<table class="tab bicolor_tab" width="100%" id="detalles_table">
  <thead>
    <tr>
      <th>src</th>
      <th>Documento</th>
      <th>Folio</th>
      <th>Código</th>
      <th>Descripcion</th>
      <th>OC</th>
      <th>GR</th>
      <th>FC</th>
      <th>Q</th>
      <th title="Origenes de Costo">OrCs</th>
      <th>Venta</th>
      <th>Costo</th>
      <th>Total Venta</th>
      <th>Total Costo</th>
      <th>Margen</th>
      <th>Margen %</th>
      <th>Proveedor</th>
      <th>Cliente</th>
      <th>Linea Negocio</th>
      <th>Marca</th>
      <th>Ejecutivo</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th align="right" colspan="12">Totales:</th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_venta_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_costo_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_margen_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th><?php echo $detallesPie->dq_margen_porcentual ?></th>
      <th colspan="5"></th>
    </tr>
  </tfoot>
  <tbody>
    <?php foreach($detalles as $detalle): ?>
      <?php foreach($detalle[1] as $d): ?>
        <tr <?php if($detalle[0]->dg_tipo_doc == 'NC' || substr($d->dg_tipo_costo,0,3) == 'NCP' ): ?>style="color:#F00;"<?php endif ?>>
          <td><?php echo $detalle[0]->dg_tipo_doc ?></td>
          <td><?php echo $detalle[0]->dq_documento ?></td>
          <td><?php echo $detalle[0]->dq_folio ?></td>
          <td><?php echo $d->dg_codigo ?></td>
          <td>
            <div class="div-overflow" title="<?php echo htmlentities($d->dg_detalle) ?>" style="width: 250px;">
              <?php echo trim(htmlentities($d->dg_detalle)) ?>
            </div>
          </td>
          <td><?php echo $d->dq_orden_compra ?></td>
          <td><?php echo $d->dq_guia_recepcion ?></td>
          <td><?php echo $d->dq_factura_compra ?></td>
          <td><?php echo $d->dq_cantidad ?></td>
          <td title="<?php echo $d->dg_glosa_tipo_costo ?>" ><?php echo $d->dg_tipo_costo ?></td>
          <td align="right">
            <b><?php echo Functions::monedaLocal($d->dq_venta_unitario/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></b>
          </td>
          <td align="right">
            <b><?php echo Functions::monedaLocal($d->dq_costo_unitario/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></b>
          </td>
          <td align="right">
            <b><?php echo Functions::monedaLocal(($d->dq_venta_total)/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></b>
          </td>
          <td align="right">
            <b><?php echo Functions::monedaLocal(($d->dq_costo_total)/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></b>
          </td>
          <td align="right">
            <b><?php echo Functions::monedaLocal(($d->dq_margen)/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></b>
          </td>
          <td>
            <b><?php echo $d->dq_margen_p ?></b>
          </td>
          <td>
            <div class="div-overflow" title="<?php echo $d->dg_proveedor ?>" style="width: 250px;">
              <?php echo $d->dg_proveedor ?>
            </div>
          </td>
          <td>
            <div class="div-overflow" title="<?php echo $d->dg_cliente ?>" style="width: 250px;">
              <?php echo $d->dg_cliente ?>
            </div>
          </td>
          <td>
            <div class="div-overflow" title="<?php echo $d->dg_linea_negocio ?>" style="width: 250px;">
              <?php echo $d->dg_linea_negocio ?>
            </div>
          </td>
          <td>
            <div class="div-overflow" title="<?php echo $d->dg_marca ?>" style="width: 250px;">
              <?php echo $d->dg_marca ?>
            </div>
          </td>
          <td>
            <div class="div-overflow" title="<?php echo $d->dg_ejecutivo ?>" style="width: 250px;">
              <?php echo $d->dg_ejecutivo ?>
            </div>
          </td>
        </tr>
      <?php endforeach; ?>
    <?php endforeach; ?>
  </tbody>
</table>