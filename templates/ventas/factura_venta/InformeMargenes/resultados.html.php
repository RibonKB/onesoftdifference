<button id="btn_centralizar" class="imgbtn" style="background-image:url(images/management.png)">Centralizar Selección</button>
<ul id="tabs">
  <li><a href="#">Totales</a></li>
  <li><a href="#">Detalles</a></li>
  <li>
    <a href="<?php echo Factory::buildActionUrl('getDetalleAgrupado', array('campo' => 'dg_tipo_costo', 'tipo' => 'group')) ?>&<?php echo $dataTotalUrl ?>" id="carga_origen_costo" class="carga_agrupado">
      Origen Costo
    </a>
  </li>
  <li>
    <a href="<?php echo Factory::buildActionUrl('getDetalleAgrupado', array('campo' => 'dc_producto', 'tipo' => 'group')) ?>&<?php echo $dataTotalUrl ?>" id="carga_producto" class="carga_agrupado">
      Productos
    </a>
  </li>
  <li>
    <a href="<?php echo Factory::buildActionUrl('getDetalleAgrupado', array('campo' => 'dc_proveedor', 'tipo' => 'group')) ?>&<?php echo $dataTotalUrl ?>" id="carga_proveedor" class="carga_agrupado">
      Proveedores
    </a>
  </li>
  <li>
    <a href="<?php echo Factory::buildActionUrl('getDetalleAgrupado', array('campo' => 'dc_linea_negocio', 'tipo' => 'group')) ?>&<?php echo $dataTotalUrl ?>" id="carga_linea_negocio" class="carga_agrupado">
      Lineas de Negocio
    </a>
  </li>
  <li>
    <a href="<?php echo Factory::buildActionUrl('getDetalleAgrupado', array('campo' => 'dc_marca', 'tipo' => 'group')) ?>&<?php echo $dataTotalUrl ?>" id="carga_marca" class="carga_agrupado">
      Marcas
    </a>
  </li>
  <li>
    <a href="<?php echo Factory::buildActionUrl('getDetalleAgrupado', array('campo' => 'dc_ejecutivo', 'tipo' => 'group')) ?>&<?php echo $dataTotalUrl ?>" id="carga_ejecutivo" class="carga_agrupado">
      Ejecutivos
    </a>
  </li>
  <li>
    <a href="<?php echo Factory::buildActionUrl('getDetalleAgrupado', array('campo' => 'dc_cliente', 'tipo' => 'group')) ?>&<?php echo $dataTotalUrl ?>" id="carga_cliente" class="carga_agrupado">
      Clientes
    </a>
  </li>
</ul>
<br class="clear" />
<div class="tabpanes">
  <div>
    <?php include __DIR__.'/totales.html.php'; ?>
  </div>
  <div id="detalles_cargados">
    <div class="info">
      Seleccione en la lista de totales el documento que quiera detallar
    </div>
  </div>
  <div id="data_origen_costo">
    <img src="images/ajax-loader.gif" />
  </div>
  <div id="data_producto">
    <img src="images/ajax-loader.gif" />
  </div>
  <div id="data_proveedor">
    <img src="images/ajax-loader.gif" />
  </div>
  <div id="data_linea_negocio">
    <img src="images/ajax-loader.gif" />
  </div>
  <div id="data_marca">
    <img src="images/ajax-loader.gif" />
  </div>
  <div id="data_ejecutivo">
    <img src="images/ajax-loader.gif" />
  </div>
  <div id="data_cliente">
    <img src="images/ajax-loader.gif" />
  </div>
</div>

<script type="text/javascript">
	js_data.urlCentralizacion = '<?php echo Factory::buildActionUrl('centralizar', $periodoContable); ?>';
	js_data.init();
</script>
