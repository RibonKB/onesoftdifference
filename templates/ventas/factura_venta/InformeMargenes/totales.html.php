<table class="tab bicolor_tab" width="100%" id="totales_table">
  <thead>
    <tr>
      <th><input type="checkbox" class="cb_select_all" title="Seleccionar todas las Facturas"/></th>
      <th>&nbsp;</th>
      <th>src</th>
      <th>Documento</th>
      <th>Folio</th>
      <th>Cliente</th>
      <th>Nota Venta</th>
      <th>Orden Servicio</th>
      <th>Ejecutivo</th>
      <th>Neto Venta</th>
      <th>Neto Costo</th>
      <th>Margen</th>
      <th>Margen %</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th>
        <a href="<?php echo Factory::buildActionUrl('obtenerDetalleDocumento') ?>&tipo=TT&<?php echo $dataTotalUrl ?>" class="enlace_carga_detalle">
          <img src="images/addbtn.png" alt="(+)" />
        </a>
      </th>
      <th align="right" colspan="8">Totales:</th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_venta_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_costo_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_margen_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th><?php echo Functions::monedaLocal($detallesPie->dq_margen_porcentual,4) ?></th>
    </tr>
  </tfoot>
  <tbody>
  <?php foreach($detalles as $d): ?>
    <?php include __DIR__.'/detalleTotal.html.php' ?>
  <?php endforeach ?>
  </tbody>
</table>
<?php if(count($extraData)): ?>
<hr />
<table class="tab bicolor_tab" width="100%" id="totales_extra_table">
  <thead>
    <tr>
      <th><input type="checkbox" class="cb_select_all" title="Seleccionar todas las Notas de Crédito" /></th>
      <th>&nbsp;</th>
      <th>src</th>
      <th>Documento</th>
      <th>Folio</th>
      <th>Cliente</th>
      <th>Nota Venta</th>
      <th>Orden Servicio</th>
      <th>Ejecutivo</th>
      <th>Neto Venta</th>
      <th>Neto Costo</th>
      <th>Margen</th>
      <th>Margen %</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th align="right" colspan="8">Totales:</th>
      <th align="right"><?php echo Functions::monedaLocal($extraDataPie->dq_venta_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($extraDataPie->dq_costo_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($extraDataPie->dq_margen_total/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
      <th><?php echo Functions::monedaLocal($extraDataPie->dq_margen_porcentual,2) ?></th>
    </tr>
  </tfoot>
  <tbody>
  <?php foreach($extraData as $d): ?>
    <?php include __DIR__.'/detalleTotal.html.php' ?>
  <?php endforeach ?>
  </tbody>
</table>
<?php endif ?>

<!-- Asignacion de costos a Facturas de compra -->
<?php if (count($asignacionCostos)):?>
<hr />
<table class="tab bicolor_tab" width="100%" id="totales_costos">
	<thead>
		<tr>
			<th>
				<input type="checkbox" class="cb_select_all" title="Seleccionar Todos los costos asignados" />
			</th>
			<th>
				src
			</th>
			<th>
				Documento
			</th>
			<th>
				Folio
			</th>
			<th>
				Proveedor
			</th>
			<th>
				Nota de Venta
			</th>
			<th>
				Ejecutivo
			</th>
			<th>
				Descripción
			</th>
			<th>
				Costo
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th align="right" colspan="8">Costos Totales:</th>
			<th align="right"><?php echo Functions::monedaLocal($totalesAsignacionCostos/$tipo_cambio->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?></th>
		</tr>
	</tfoot>
	<tbody>
	<?php
		foreach($asignacionCostos as $ac){
			include __DIR__ . '/detalleAsignacionCostosFacturaCompra.html.php';
		}
	?>
	</tbody>
</table>
<?php endif;?>
