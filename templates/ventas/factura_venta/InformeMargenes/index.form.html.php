<?php
$form->Start(Factory::buildActionUrl('obtenerTotalMargenes'),'src_informe_margenes');
$form->Header("<b>Indique el periodo del que quiere obtener el informe de margenes</b><br />Los campos marcados con [*] son obligatorios");
?>
<table class="tab" width="100%">
    <tbody>
      <tr>
        <td>Periodo</td>
        <td><?php $form->Select('Mes','dc_mes',$meses,1,date('n')); ?></td>
        <td><?php $form->Text('Año','dc_anho',1,4,date('Y')); ?></td>
      </tr>
      <?php if(check_permiso(72)): ?>
      <tr>
        <td>Ejecutivo</td>
        <td>
           <?php $form->DBMultiSelect('','dc_ejecutivo','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 1"); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <?php endif; ?>
      <tr>
        <td>Tipo de cambio</td>
        <td><?php $form->DBSelect('','dc_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'),1) ?></td>
        <td>&nbsp;</td>
      </tr>
    </tbody>
</table>
<?php
if(!check_permiso(72)):
	$form->Hidden('dc_ejecutivo[]',$this->getUserData()->dc_funcionario);
endif;
$form->End('Consultar','searchbtn');
?>
<script type="text/javascript" src="jscripts/sites/ventas/factura_venta/src_informe_margenes.js?v=1_2_2"></script>
<script type="text/javascript">
	$('#dc_ejecutivo').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>