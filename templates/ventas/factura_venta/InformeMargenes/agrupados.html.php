<table class="tab" width="100%">
  <thead>
    <tr>
      <th><?php echo $nombre_campo ?></th>
      <th width="120">Neto Venta</th>
      <th width="120">Costo</th>
      <th width="120">Margen</th>
      <th width="120">Margen %</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th align="right">Totales</th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_venta_total/$tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_costo_total/$tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($detallesPie->dq_margen_total/$tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></th>
      <th align="center"><?php echo $detallesPie->dq_margen_porcentual ?></th>
    </tr>
  </tfoot>
  <tbody>
    <?php foreach($detalles as $d): ?>
    <tr>
      <td>
        <?php if($d->dg_glosa != NULL): ?>
        <b><?php echo $d->dg_glosa ?></b>
        <?php else: ?>
        <i style="color:#888">Sin información</i>
        <?php endif ?>
      </td>
      <td align="right"><?php echo Functions::monedaLocal($d->dq_venta_total/$tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></td>
      <td align="right"><?php echo Functions::monedaLocal($d->dq_costo_total/$tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></td>
      <td align="right"><?php echo Functions::monedaLocal($d->dq_margen/$tipo_cambio->dq_cambio, $tipo_cambio->dn_cantidad_decimales) ?></td>
      <td align="center"><?php echo $d->dq_margen_porcentual ?></td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>