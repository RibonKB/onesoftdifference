<?php
	
	$form->Start($this->buildActionUrl('setEditOrdenCompraData'),'editar_datos_orden_compra');
	$form->Header('<b>Edición de los datos de la orden de compra asociados a la factura</b>');
	
	$form->Section();
	$form->Text('Orden de compra', 'dg_orden_compra', 0, 255, $ordenCompra);
    $form->Date('Fecha Orden de compra', 'df_fecha_orden_compra', 0, $fechaOrdenCompra);
	$form->Hidden('dc_factura',$idFactura);
	$form->Hidden('dc_cliente',$cliente);
	$form->EndSection();
	
	$form->End('Actualizar Orden de Compra','addbtn');