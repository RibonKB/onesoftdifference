<div class='title center'>
  Nota de venta Nº <?php echo $nota_venta->dq_nota_venta ?>
</div>
<table class='tab' width='100%' style='text-align:left;'>
  <caption>
    <a href='sites/clientes/ed_cliente.php?id=<?php echo $nota_venta->dc_cliente ?>' class='loadOnOverlay'>
      <img src='images/editbtn.png' alt='[ED]' />
    </a>
    Cliente:<br />
    <strong>(<?php echo $nota_venta->dg_rut ?>) <?php echo $nota_venta->dg_razon ?></strong>
  </caption>
  <tbody>
    <tr>
      <td width='160'>Fecha emision</td>
      <td><?php echo $nota_venta->df_fecha_emision ?></td>
      <td rowspan='9' width='200' valign="top">
        
        <?php include __DIR__.'/estadoWorkflow.html.php' ?>
        <?php include __DIR__.'/estadoFacturacion.html.php' ?>
        <?php include __DIR__.'/estadoAnulacion.html.php' ?>
        <?php include __DIR__.'/estadoContrato.html.php' ?>
        
      </td>

    </tr><tr>
      <td>Tipo de venta</td>
      <td><b><?php echo $nota_venta->dg_tipo ?></b></td>
    </tr><tr>
      <td>Domicilio cliente</td>
      <td>
        <b><?php echo $nota_venta->dg_direccion ?></b>
        <?php echo $nota_venta->dg_comuna ?>
        <label><?php echo $nota_venta->dg_region ?></label>
      </td>
    </tr><tr>
      <td>Tipo de cambio</td>
      <td><b><?php echo $nota_venta->dg_tipo_cambio ?></b> (<?php echo moneda_local($nota_venta->dq_cambio) ?>)</td>
    </tr><tr>
      <td>Ejecutivo:</td>
      <td><b><?php echo $nota_venta->dg_ejecutivo ?></b></td>
    </tr><tr>
      <td>Término Comercial</td>
      <td><?php echo $nota_venta->dg_termino_comercial ?></td>
    </tr><tr>
      <td>Modo de envio</td>
      <td><?php echo $nota_venta->dg_modo_envio ?></td>
    </tr><tr>
      <td>Fecha de envio</td>
      <td><?php echo $nota_venta->df_fecha_envio ?></td>
    </tr><tr>
      <td>Observación</td>
      <td><?php echo $nota_venta->dg_observacion ?></td>
    </tr>
  </tbody>
</table>
<?php include __DIR__.'/detalle.html.php'; ?>
<?php include __DIR__.'/scriptsShow.html.php'; ?>