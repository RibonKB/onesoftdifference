<?php
$form->Start(ConsultaNotaVentaFactory::buildActionUrl('consultar'), "src_nota_venta");
$form->Header("<strong>Indicar los parámetros de búsqueda de la nota de venta</strong>");
?>
<table class="tab" style="text-align:left;" id="form_container" width="100%">
  <tbody>
    <tr>
      <td width="50">Número de nota de venta</td>
      <td width="280"><?php $form->Text("Desde", "dq_nota_venta_desde"); ?></td>
      <td width="280">
        <?php $form->Text('Hasta', 'dq_nota_venta_hasta'); ?>
      </td>
    </tr>
    <tr>
      <td>Fecha emisión</td>
      <td><?php $form->Date('Desde', 'df_emision_desde', 1, "01/" . date("m/Y")); ?></td>
      <td><?php $form->Date('Hasta', 'df_emision_hasta', 1, 0); ?></td>
    </tr>
    <tr>
      <td>Cliente</td>
      <td><?php $form->DBMultiSelect('', 'dc_cliente', 'tb_cliente', array('dc_cliente', 'dg_razon')); ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Cliente Final</td>
      <td><?php $form->DBMultiSelect('', 'dc_cliente_final', 'tb_cliente', array('dc_cliente', 'dg_razon')); ?></td>
      <td>&nbsp;</td>
    </tr>
    <?php if (check_permiso(38)): ?>
      <tr>
        <td>Ejecutivo</td>
        <td><?php $form->MultiSelect('', 'dc_ejecutivo', $ejecutivos); ?></td>
        <td>&nbsp;</td>
      </tr>
    <?php endif ?>

    <tr>
      <td>Buscar en Detalle</td>
      <td><?php $form->Text('Código de producto', 'dg_codigo_producto'); ?></td>
      <td><?php $form->Text('Descripción', 'dg_descripcion_detalle'); ?></td>
    </tr>
    <tr>
      <td>Incluir Notas de venta<br /><small>(Deja en blanco para ignorar estado)</small></td>
      <td><?php $form->Combobox('', 'dm_estado', array('dm_validada' => 'Validadas', 'dm_confirmada' => 'Confirmadas')); ?></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>

<?php
if (!check_permiso(38))
    $form->Hidden('dc_ejecutivo[]', $this->getUserData()->dc_funcionario);

$form->End('Ejecutar consulta', 'searchbtn');
?>
<script type="text/javascript">
  $('#dc_cliente, #dc_ejecutivo, #dc_cliente_final').multiSelect({
    selectAll: true,
    selectAllText: "Seleccionar todos",
    noneSelected: "---",
    oneOrMoreSelected: "% seleccionado(s)"
  });
</script>