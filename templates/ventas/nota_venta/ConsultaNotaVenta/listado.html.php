<div id='show_nota_venta'></div>
<div id='options_menu'>
  <div id='res_list'>
    <table class='tab sortable' width='100%'>
      <caption>Notas de venta<br />Encontradas</caption>
      <thead>
        <tr>
          <th>Nº Nota de venta</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($notas_venta as $nv): ?>
        <tr>
          <td>
            <a href='<?php echo ConsultaNotaVentaFactory::buildActionUrl('show',array('id' => $nv->dc_nota_venta)) ?>' class='nv_load'>
              <img src='images/doc.png' alt='' style='vertical-align:middle;' /><?php echo $nv->dq_nota_venta ?>
            </a>
          </td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
    <button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
    <button type='button' class='button' id='print_version'>Version de impresión</button>
    
    <?php if(check_permiso(60)): ?>
        <button type='button' class='delbtn' id='null_nota_venta'>Anular</button>
    <?php endif ?>
        
    <?php if(check_permiso(42) || check_permiso(40)): ?>
        <button type='button' class='editbtn' id='edit_nota_venta'>Editar</button>
    <?php endif ?>
        
    <button type='button' id='costeo_nota_venta' class='imgbtn' style='background-image:url(images/doc.png);'>Costos de venta</button>
    <button type='button' id='comprar_nota_venta' class='imgbtn' style='background-image:url(images/doc.png);'>Comprar</button>
    
    <?php if(check_permiso(24)): ?>
        <button type='button' id='facturar_nota_venta' class='imgbtn' style='background-image:url(images/doc.png);'>Facturar</button>
    <?php endif ?>
        
    <?php if(check_permiso(32)): ?>
        <button type="button" id="valida_nota_venta" class="checkbtn">Validar</button>
    <?php endif ?>
        
    <?php if(check_permiso(33)): ?>
        <button type="button" id="confirma_nota_venta" class="checkbtn">Confirmar</button>
    <?php endif ?>
        
    <button type="button" id="show_workflow" class="searchbtn">Workflosfddrfw</button>
	<?php if(check_permiso(90)): ?>
    <button type="button" id="print_special" class="button">Impresión con Márgenes</button>
    <?php endif ?>
</div>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".nv_load").click(function(e){
		e.preventDefault();
		$('#show_nota_venta').html("<img src='images/ajax-loader.gif' alt='' /> cargando nota de venta ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		pymerp.loadFile($(this).attr('href'),'#show_nota_venta');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
    
    $('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px',marginTop:'30px'});
</script>