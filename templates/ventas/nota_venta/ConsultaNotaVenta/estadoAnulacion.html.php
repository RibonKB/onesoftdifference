<?php if ($nota_venta->dm_nula == '1'): ?>
  <h3 class='alert'>NOTA VENTA ANULADA</h3>
  <?php if ($nota_venta->datos_anulacion != false): ?>
    <div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
      <div class='info'>Info anulación</div>
      Motivo: <b><?php echo $nota_venta->datos_anulacion->dg_motivo ?></b><br />
      Comentario: <b><?php echo $nota_venta->datos_anulacion->dg_comentario ?></b>
    </div>
  <?php endif; ?>
<?php endif; ?>