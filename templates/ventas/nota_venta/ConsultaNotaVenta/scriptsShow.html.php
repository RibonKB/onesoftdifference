<script type="text/javascript">
  var db_permiso_42 = <?php echo json_encode(check_permiso(42)) ?>;
  var db_permiso_40 = <?php echo json_encode(check_permiso(40)) ?>;
  var db_permiso_75 = <?php echo json_encode(check_permiso(75)) ?>;
  var db_permiso_90 = <?php echo json_encode(check_permiso(90)) ?>;
  var dc_usuario_actual = <?php echo json_encode($this->getUserData()->dc_usuario) ?>;
  var nota_venta = <?php echo json_encode($nota_venta) ?>;

  $('#print_version').unbind('click').click(function() {
    window.open("sites/ventas/nota_venta/ver_nota_venta.php?id=" + nota_venta.dc_nota_venta, 'print_nota_venta', 'width=800;height=600');
  });
  
  //Impresión de nota venta con costos.
  if(db_permiso_90){
	pymerp.enableButton('#print_special');
	$('#print_special').unbind('click').click(function() {
		window.open("sites/ventas/nota_venta/ver_nota_venta.php?id=" + nota_venta.dc_nota_venta + "&type=1", 'print_nota_venta', 'width=800;height=600');
	});
  } else {
    pymerp.disableButton('#edit_nota_venta');
  }

  $('#null_nota_venta').unbind('click').click(function() {
    pymerp.loadOverlay("sites/ventas/nota_venta/null_nota_venta.php?id=" + nota_venta.dc_nota_venta);
  });

  if ((db_permiso_42 && dc_usuario_actual == nota_venta.dc_usuario_creacion) || db_permiso_40) {
    pymerp.enableButton('#edit_nota_venta');
    $('#edit_nota_venta').unbind('click').click(function() {
      pymerp.loadPage('sites/ventas/nota_venta/ed_nota_venta.php?id=' + nota_venta.dc_nota_venta);
    });
  } else {
    pymerp.disableButton('#edit_nota_venta');
  }

  $('#show_workflow').unbind('click').click(function() {
    pymerp.loadOverlay("sites/ventas/src_workflow.php?mode=nv&id=" + nota_venta.dc_nota_venta);
  });

  //Opciones de notas de venta con costos asociados
  if (nota_venta.dm_costeable == 1 && ((nota_venta.dm_validada == 1 && nota_venta.dm_confirmada) || db_permiso_75)) {
    pymerp.enableButton('#costeo_nota_venta');
    $('#costeo_nota_venta').unbind('click').click(function() {
      pymerp.loadOverlay("sites/ventas/nota_venta/cr_nota_venta_costeo.php?id=" + nota_venta.dc_nota_venta + "&tc=" + nota_venta.dq_cambio + "&tcdec=" + nota_venta.dn_cantidad_decimales);
    });
  } else {
    $('#costeo_nota_venta').unbind('click');
    pymerp.disableButton('#costeo_nota_venta');
  }

  //Nota de venta validada
  if (nota_venta.dm_validada == 0) {
    pymerp.enableButton('#valida_nota_venta');
    $('#valida_nota_venta').unbind('click').click(function() {
      pymerp.loadOverlay("sites/ventas/nota_venta/proc/val_nota_venta.php?id=" + nota_venta.dc_nota_venta);
    }).text('Validar').addClass('checkbtn').removeClass('delbtn');
  } else {
    pymerp.enableButton('#valida_nota_venta');
    $('#valida_nota_venta').unbind('click').click(function() {
      pymerp.loadOverlay("sites/ventas/nota_venta/proc/val_nota_venta.php?id=" + nota_venta.dc_nota_venta + "&mode=0");
    }).text('Desvalidar').removeClass('checkbtn').addClass('delbtn');
  }

  //Nota de venta confirmada
  if (nota_venta.dm_confirmada == 0) {
    pymerp.enableButton('#confirma_nota_venta');
    $('#confirma_nota_venta').unbind('click').click(function() {
      pymerp.loadOverlay("sites/ventas/nota_venta/proc/conf_nota_venta.php?id=" + nota_venta.dc_nota_venta);
    }).text('Confirmar').addClass('checkbtn').removeClass('delbtn');
  } else {
    pymerp.enableButton('#confirma_nota_venta');
    $('#confirma_nota_venta').unbind('click').click(function() {
      pymerp.loadOverlay("sites/ventas/nota_venta/proc/conf_nota_venta.php?id=" + nota_venta.dc_nota_venta + "&mode=0");
    }).text('Desconfirmar').removeClass('checkbtn').addClass('delbtn');
  }

  //No se puede continuar workflow con notas de venta sin confirmar o validar
  if (nota_venta.dm_validada == 0 || nota_venta.dm_confirmada == 0) {
    pymerp.disableButton('#comprar_nota_venta,#facturar_nota_venta');
  } else {
    pymerp.enableButton('#comprar_nota_venta,#facturar_nota_venta');
    $('#comprar_nota_venta').unbind('click').click(function() {
      pymerp.loadOverlay("sites/ventas/nota_venta/proc/cr_orden_compra_nota_venta.php?nv_numero=" + nota_venta.dq_nota_venta);
    });
    $('#facturar_nota_venta').unbind('click').click(function() {
      pymerp.loadPage("sites/ventas/factura_venta/proc/cr_factura_venta.php?nv_numero=" + nota_venta.dq_nota_venta + "&cli_id=" + nota_venta.dc_cliente);
    });
  }

  if (nota_venta.dm_nula == 1) {
    pymerp.disableButton('#null_nota_venta,#edit_nota_venta,#costeo_nota_venta,#comprar_nota_venta,#facturar_nota_venta,#valida_nota_venta,#confirma_nota_venta');
  }

  if (nota_venta.dm_contratable == 1) {
    $('#show_contratos').click(function() {
      pymerp.loadOverlay('sites/ventas/nota_venta/proc/show_contrato.php', 'dc_nota_venta=' + nota_venta.dc_nota_venta);
    });
    $('#show_contratos_table').click(function() {
      pymerp.loadOverlay('sites/ventas/nota_venta/proc/show_contrato_table.php', 'dc_nota_venta=' + nota_venta.dc_nota_venta);
    });
    $('#edit_contrato').click(function() {
      pymerp.loadOverlay('sites/ventas/contrato/ed_contrato.php', 'dc_nota_venta=' + nota_venta.dc_nota_venta);
    });
  }

  pymerp.init('#show_nota_venta');

</script>
<script type="text/javascript" src="jscripts/clientes/src_cliente.js?v0_4"></script>