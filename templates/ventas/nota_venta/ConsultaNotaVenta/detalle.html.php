<table width='100%' class='tab bicolor_tab'>
  <caption>Detalle</caption>
  <thead>
    <tr>
      <th colspan='4'>Avance</th>
	  <th>Fecha Arribo / Renovación</th>
      <th>Cantidad</th>
      <th>Código</th>
      <th>Descripción</th>
      <th width='4%'>Proveedor</th>
      <th width='8%'>Costo</th>
      <th width='8%'>Costo Total</th>
      <th width='8%'>Precio</th>
      <th width='8%'>Total</th>
      <th width='8%'>Margen</th>
      <th width='8%'>Margen (%)</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($nota_venta->detalle as $d): ?>
      <tr>
        <?php include __DIR__.'/cantidadesERP.html.php'; ?>
        <td align="left"><?php echo $d->dg_codigo ?></td>
        <td align='left'><?php echo $d->dg_descripcion ?></td>
        <td align='left'><?php echo $d->dg_proveedor ?></td>
        <td align='right'><?php echo moneda_local($d->dq_precio_compra_tc, $nota_venta->dn_cantidad_decimales) ?></td>
        <td align='right'><?php echo moneda_local($d->dq_total_costo_tc, $nota_venta->dn_cantidad_decimales) ?></td>
        <td align='right'><?php echo moneda_local($d->dq_precio_venta_tc, $nota_venta->dn_cantidad_decimales) ?></td>
        <td align='right'><?php echo moneda_local($d->dq_total_tc, $nota_venta->dn_cantidad_decimales) ?></td>
        <td align='right'><?php echo moneda_local($d->dq_margen_tc, $nota_venta->dn_cantidad_decimales) ?></td>
        <td align='right'><?php echo $d->dg_margen_porcentual ?> %</td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  <tfoot>
    <tr>
      <th colspan='11' align='right'>Total (<?php echo $nota_venta->dg_tipo_cambio ?>)</th>
      <th colspan='2' align='right'><?php echo moneda_local($nota_venta->dq_neto_tc, $nota_venta->dn_cantidad_decimales) ?></th>
      <th colspan='2'>&nbsp;</th>
    </tr>
    <tr>
      <th colspan='11' align='right'>Total Neto</th>
      <th colspan='2' align='right'><?php echo moneda_local($nota_venta->dq_neto) ?></th>
      <th colspan='2'>&nbsp;</th>
    </tr>
    <tr>
      <th colspan='11' align='right'>IVA</th>
      <th colspan='2' align='right'><?php echo moneda_local($nota_venta->dq_iva) ?></th>
      <th colspan='2'>&nbsp;</th>
    </tr>
    <tr>
      <th colspan='11' align='right'>Total a Pagar</th>
      <th colspan='2' align='right'><?php echo moneda_local($nota_venta->dq_total) ?></th>
      <th colspan='2'>&nbsp;</th>
    </tr>
    <tr>
      <th colspan='11' align='right'>Margen (<?php echo $nota_venta->dg_tipo_cambio ?>)</th>
      <th colspan='2' align='right'><?php echo moneda_local($nota_venta->dq_margen_tc, $nota_venta->dn_cantidad_decimales) ?></th>
      <th colspan='2'>&nbsp;</th>
    </tr>
    <tr>
      <th colspan='11' align='right'>Margen Total</th>
      <th colspan='2' align='right'>
        <?php echo moneda_local($nota_venta->dq_margen) ?> (<?php echo $nota_venta->dg_margen_porcentual ?>%)
      </th>
      <th colspan='2'>&nbsp;</th>
    </tr>
    <?php if ($nota_venta->dm_costeable && check_permiso(64)): ?>
      <tr>
        <th colspan='11' align='right'>Margen Con costos reales</th>
        <th colspan='2' align='right'>
          <?php echo moneda_local($nota_venta->dq_margen_real) ?>
          <?php echo $nota_venta->dg_margen_real_porcentual ?>
        </th>
        <th colspan='2'>&nbsp;</th>
      </tr>
    <?php endif; ?>
  </tfoot>
</table>
