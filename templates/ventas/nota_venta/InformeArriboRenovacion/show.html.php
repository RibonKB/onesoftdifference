<div class="informe_arribo_renovacion">
    <table width="100%" class="tab" id="resultados" >
        <thead>
            <tr>
                <th width="15%" >Numero Nota Venta</th>
                <th width="15%" >Tipo de producto</th>
                <th width="15%" >Marca</th>
                <th width="40%" >Descripción</th>
                <th width="15%" >Fecha de arribo/renovación</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productos as $v): ?>
            <tr>
                <td>
                    <?php echo $v['dq_nota_venta']; ?>
                </td>
                <td>
                    <?php echo $v['dg_tipo_producto']; ?>
                </td>
                <td>
                    <?php echo $v['dg_marca']; ?>
                </td>
                <td>
                    <?php echo $v['dg_producto']; ?>
                </td>
                <td>
                    <?php echo empty($v['df_fecha_arribo']) ? 'Sin fecha registrada': $v['df_fecha_arribo']; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    informeArriboRenovacion.ajustarTabla();
</script>
