<?php
$form->Start(self::buildActionUrl('show'), 'form_informe_arribo_renovacion');
$header = 'Indique los parámetros de búsqueda';
$form->Header($header);
?>
<div class="bordered_table">
    <table width="100%" class="tab">
        <tr>
            <td>
                Número de nota de venta
            </td>
            <td>
                <?php $form->Text('Desde','nv_desde'); ?>
            </td>
            <td>
                <?php $form->Text('Hasta', 'nv_hasta'); ?>
            </td>
        </tr>
        <tr>
            <td>
                Fecha de emisión
            </td>
            <td>
                <?php $form->Date('Desde','df_nv_desde',1,"01/".date("m/Y")); ?>
            </td>
            <td>
                <?php $form->Date('Hasta','df_nv_hasta',1,0); ?>
            </td>
        </tr>
        <tr>
            <td>
                Tipo producto/Marca
            </td>
            <td>
                <?php $form->Multiselect('Tipo de Producto', 'dc_tipo_producto', $tiposProductos); ?>
            </td>
            <td>
                <?php $form->Multiselect('Marca', 'dc_marca', $marcas); ?>
            </td>
        </tr>
        <tr>
            <td>
                Opciones
            </td>
            <td>
                <?php $form->Combobox('', 'df_null', array('Incluir detalles sin fecha de arribo/renovación definida')); ?>
            </td>
            <td>

            </td>
        </tr>
    </table>
</div>
<?php
    $form->End('Ejecutar consulta','searchbtn');
?>
<script type="text/javascript" src="<?php echo Factory::getJavascriptUrl('InformeArriboRenovacion'); ?>"></script>
<script type="text/javascript">
    informeArriboRenovacion.activateMultiselect();
</script>
