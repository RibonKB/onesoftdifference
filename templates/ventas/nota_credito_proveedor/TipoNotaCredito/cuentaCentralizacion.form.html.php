<?php
$form->Start(Factory::buildActionUrl('procesarCuentaCentralizacion'),"cr_cuenta_centralizacion");
$form->Header("Indique una Cuenta Contable para agregar al tipo de Nota de Crédito<br />".
              "<strong>Lo campos marcaos con [*] son obligatorios.</strong>");
  $form->Section();
    $form->DBSelect(
      "Cuenta Contable",
      "dc_cuenta_contable",
      "tb_cuenta_contable",
      array('dc_cuenta_contable',"dg_cuenta_contable","dg_codigo"),
      true);
  $form->EndSection();
$form->Hidden('dc_tipo',$tipo->dc_tipo);
$form->End("Asignar cuenta","addbtn") ?>
<?php include __DIR__."/listadoCuentasContableCentralizacion.html.php" ?>
