<table class="tab" width="100%">
  <caption>Cuentas contables asociadas al tipo de Nota de Crédito</caption>
  <thead>
    <tr>
      <th> </th>
      <th>Código</th>
      <th>Cuenta contable</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($cuentas_contables as $c): ?>
      <tr>
        <td>
          <a href="<?php echo Factory::buildActionUrl('eliminarCuentaContableCentralizacion',array(
            "dc_tipo" => $tipo->dc_tipo,
            "dc_cuenta_contable" => $c->dc_cuenta_contable
          )) ?>" class="loadOnOverlay">
            <img src="images/delbtn.png" alt="X" title="Quitar cuenta contable" />
          </a>
        </td>
        <td>
          <strong><?php echo $c->dg_codigo ?></strong>
        </td>
        <td>
          <strong><?php echo $c->dg_cuenta_contable ?></strong>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
