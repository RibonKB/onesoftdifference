<div class='title center'>
  Generando nota de crédito de <strong style='color:#000;'><?php echo $proveedor->dg_razon ?></strong>
  <br />
  <small>
    Tipo: <strong><?php echo $tipo->dg_tipo ?></b></strong>
  </small>
</div>
<?php
$form->Start(Factory::buildActionUrl('procesarCrear'),'cr_nota_credito','ventas_form');
  $form->Header("Indique los datos para la creación de la nota de crédito.<br />".
                "<strong>Los campos marcados con [*] son obligatorios.</strong>");
  $form->Section();
    $form->Date("Fecha de emisión","df_emision",true,0);
    $form->Select("Contacto","dc_contacto",$contactos,true,$proveedor->dc_contacto_default);
    $form->DBSelect('Medio de pago','fv_medio_pago','tb_medio_pago_proveedor',array('dc_medio_pago','dg_medio_pago'),true);
    $form->Text("Factura de compra","dq_factura",true);
    $form->Hidden('dc_factura_simple',0);
  $form->EndSection();

  $form->Section();
    $form->Text("Folio","dq_folio",true,20);
    $form->Date("Fecha de vencimiento","df_vencimiento",true);
    $form->Date("Fecha contable","df_libro_compra",true,0);
    if($tipo->dm_retorna_stock == 1):
      ?>
      <br /><div id="selected_guias"></div>
      <?php
      $form->Button('Asignar guías de despacho','id="get_guia_despacho"','addbtn');
    endif;
  $form->EndSection();

  $form->Section();
    $form->Textarea('Comentario','dg_comentario',false);
  $form->EndSection();

  $form->Group();

  include __DIR__."/contenedorDetalle.html.php";

  $form->Group();

  $form->Hidden('dc_proveedor',$proveedor->dc_proveedor);
	$form->Hidden('dc_tipo',$tipo->dc_tipo);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('nc_max_neto',0);

$form->End("Crear Nota de Crédito",'addbtn');

include __DIR__ . "/templateDetalle.html.php";

?>
<script type="text/javascript">
	var empresa_iva = <?php echo $iva_empresa ?>;
	var empresa_dec = <?php echo $decimales_empresa ?>;
	$('#dq_neto, #dq_exento, #dq_iva, #dq_total').css({'textAlign':'right'});
  $("#prods_form select").css({width:"150px"});
</script>
<script type="text/javascript" src="jscripts/product_manager/nota_credito.js?v1_12a"></script>
<script type="text/javascript" src="jscripts/sites/ventas/nota_credito_proveedor/cr_nota_credito.js?v=1_15b"></script>
<script type="text/javascript">
	js_data.dc_proveedor = <?php echo $proveedor->dc_proveedor ?>;
	js_data.init();
</script>
