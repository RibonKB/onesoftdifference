<table id='prods_form' class='hidden'>
  <tr class='main'>
    <td align='center'>
      <img src='images/delbtn.png' alt='' title='Eliminar detalle' class='del_detail' />
      <img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
    </td>
    <td>
      <input type='text' name='prod[]' class='prod_codigo searchbtn' size='15' />
    </td>
    <td>
      <input type='text' name='desc[]' class='prod_desc inputtext' size='20' required='required' />
    </td>
    <?php if($tipo->dm_retorna_stock == 0): ?>
      <td>
        <?php
          if(count($cuentas_contables) > 1):
            $form->Select("","dc_cuenta_contable[]",$cuentas_contables,true);
          else:
            foreach ($cuentas_contables as $dc_cuenta_contable => $value):
              echo '<strong>'.$value.'</strong>';
              $form->Hidden('dc_cuenta_contable[]',$dc_cuenta_contable);
            endforeach;
          endif;
        ?>
      </td>
    <?php else: ?>
      <td>
        <?php $form->DBSelect('',"dc_bodega[]",'tb_bodega',array('dc_bodega','dg_bodega'),true); ?>
      </td>
    <?php endif; ?>
    <td>
      <input type='text' name='cant[]' class='prod_cant inputtext' size='15' style='text-align:right;' required='required' />
    </td>
    <td>
      <input type='text' name='precio[]' class='prod_price inputtext' size='15' style='text-align:right;' required='required' />
    </td>
    <td class='total' align='right'>0</td>
  </tr>
</table>
