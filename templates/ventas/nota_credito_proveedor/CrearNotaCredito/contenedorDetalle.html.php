<div id="prods">
  <div class='info'>
    Detalle (Valores en <strong><?php echo $moneda ?></strong>)
  </div>
  <table class="tab" width="100%">
    <thead>
      <tr>
        <th>Opciones</th>
        <th>Código Producto [*]</th>
        <th>Descripción [*]</th>
        <?php if($tipo->dm_retorna_stock == 0): $span = array(5,3); ?>
          <th  width="150">
            Cuenta Contable [*]
          </th>
        <?php else: $span = array(5,3); ?>
          <th>
            Bodega [*]
          </th>
        <?php endif; ?>
        <th>Cantidad [*]</th>
        <th width="20">Precio [*]</th>
        <th width="20%">Total</th>
      </tr>
    </thead>
    <tbody id="prod_list"></tbody>
    <tfoot>
      <tr>
        	<th align='right' colspan='<?php echo $span[0] ?>'>Total Neto</th>
          <th align='right' colspan='<?php echo $span[1] ?>'>
            <?php $form->Text('','dq_neto',true,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35) ?>
          </th>
      </tr>
      <tr>
        	<th align='right' colspan='<?php echo $span[0] ?>'>Total Exento</th>
          <th align='right' colspan='<?php echo $span[1] ?>'>
            <?php $form->Text('','dq_exento',true,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35) ?>
          </th>
      </tr>
      <tr>
        	<th align='right' colspan='<?php echo $span[0] ?>'>IVA</th>
          <th align='right' colspan='<?php echo $span[1] ?>'>
            <?php $form->Text('','dq_iva',true,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35) ?>
          </th>
      </tr>
      <tr>
        	<th align='right' colspan='<?php echo $span[0] ?>'>Total</th>
          <th align='right' colspan='<?php echo $span[1] ?>'>
            <?php $form->Text('','dq_total',true,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35) ?>
          </th>
      </tr>
    </tfoot>
  </table>
  <div class="center">
    <input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
  </div>
</div>
