<?php
$this->getErrorMan()->showConfirm("Se ha almacenado la nota de crédito correctamente.");
$this->getErrorMan()->showInfo("El número de folio para nota de crédito física es el <strong>{$dq_folio}</strong><br />
Además de eso el número de identificación interno es el <h1 style='margin:0;color:#000;'>{$dq_nota_credito}</h1>
<button type='button' class='button' id='print_nc'>Imprimir</button>");
?>
<script type="text/javascript">
$('#print_nc').click(function(){
	pymerp.loadOverlay('sites/ventas/nota_credito/ver_nota_credito.php?id=<?php echo $dc_nota_credito ?>');
});
</script>