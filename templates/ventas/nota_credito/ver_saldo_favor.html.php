<table class="tab" width="100%">
	<caption>Cliente: <b><?php echo $cliente->dg_razon ?></b></caption>
    <thead>
    	<th>Nota de crédito</th>
        <th>Folio</th>
        <th>Fecha Emisión</th>
        <th>FV</th>
        <th>Folio FV</th>
        <th>Total</th>
        <th>Saldo Disponible</th>
    </thead>
    <tbody>
    <?php while($n = $notas_credito->fetch(PDO::FETCH_OBJ)): ?>
    	<tr>
        	<td><?php echo $n->dq_nota_credito ?></td>
            <td><?php echo $n->dq_folio ?></td>
            <td><?php echo $this->getConnection()->dateLocalFormat($n->df_emision) ?></td>
            <td><?php echo $n->dq_factura ?></td>
            <td><?php echo $n->dq_folio_factura ?></td>
            <td align="right"><?php echo Functions::monedaLocal($n->dq_total) ?></td>
            <td align="right"><?php echo Functions::monedaLocal($n->dq_total-$n->dq_favor_usado) ?></td>
        </tr>
    <?php endwhile ?>
    </tbody>
</table>