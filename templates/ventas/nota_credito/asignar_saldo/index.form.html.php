<?php $form->Start('sites/ventas/nota_credito/index.php?factory=AsignarSaldo&action=procesarAsignacion','asignar_saldo_favor_form') ?>
<?php $form->Header('<b>Indique qué nota de crédito se utilizará para el saldo y luego indique la factura a la que se le asignará</b><br />
					 Los campos marcados con [*] son obligatorios') ?>
                     
    <?php $form->Section() ?>
		<?php $form->Text('Nota de crédito','dc_nota_credito',true) ?>
        <div id="nota_credito_data"></div>
    <?php $form->EndSection() ?>
    
    <?php $form->Section() ?>
    	<?php $form->Text('Factura de Venta','dc_factura_venta',true) ?>
        <div id="factura_venta_data"></div>
    <?php $form->EndSection() ?>
    
    <?php $form->Section() ?>
    	<?php $form->Date('Fecha Contable','df_fecha_contable',true,0) ?>
        <?php $form->Text('Monto a Saldar (Dejar en blanco para asignar automáticamente)','dq_monto_saldar') ?>
    <?php $form->EndSection() ?>
    
<?php $form->End('Asignar Saldo') ?>
<script type="text/javascript" src="jscripts/sites/ventas/nota_credito/asignar_saldo.js?rand=<?php echo rand() ?>"></script>
<script type="text/javascript">
	js_data.init();
</script>