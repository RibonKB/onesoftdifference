<table class="tab" width="95%" align="center">
	<caption>Info NC</caption>
	<tbody>
    	<tr>
        	<td>Nota de crédito</td>
            <td><b><?php echo $nota_credito->dq_nota_credito ?></b></td>
        </tr>
        <tr>
        	<td>Folio</td>
            <td><b><?php echo $nota_credito->dq_folio ?></b></td>
        </tr>
        <tr>
        	<td>Cliente</td>
            <td><b><?php echo $cliente->dg_razon ?></b></td>
        </tr>
        <tr>
        	<td>Total</td>
            <td align="right"><b><?php echo Functions::monedaLocal($nota_credito->dq_total) ?></b></td>
        </tr>
        <tr>
        	<td>Saldo Disponible</td>
            <td align="right"><b><?php echo Functions::monedaLocal($nota_credito->dq_total-$nota_credito->dq_favor_usado) ?></b></td>
        </tr>
    </tbody>
</table>