<?php
	$form->Start($this->buildActionUrl('emitirNotaCredito'),'cr_emitir_nc_electronica');
	$form->Header('<b>Confirmar la emisión de la Nota de Crédito como Electrónica</b>');
	
	$form->Section();
		$form->Select(
				'Tipo de corrección',
				'dm_tipo_correccion',
				array(
					1=>'Anula Documento de referencia',
					2=>'Corrección texto documento de referencia',
					3=>'Corrección Montos'),
				true,
				0);
		$form->Select(
			'Tipo de referencia',
			'dc_tipo_referencia',
			array(
				33 => 'Factura electrónica',
				30 => 'Factura física'
			),
			true,
			33);
	$form->EndSection();
	$form->Section();
		$form->Text('Razón Emisión','dg_razon_nc',0,90);
	$form->EndSection();
	
	$form->Hidden('dc_nota_credito',$dc_nota_credito);
	$form->End('Emitir','addbtn');
?>
