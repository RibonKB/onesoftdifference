<table class="tab" width="95%" align="center">
	<caption>Info FV</caption>
	<tbody>
    	<tr>
        	<td>Factura de Venta</td>
            <td><b><?php echo $factura->dq_factura ?></b></td>
        </tr>
        <tr>
        	<td>Folio</td>
            <td><b><?php echo $factura->dq_folio ?></b></td>
        </tr>
        <tr>
        	<td>Cliente</td>
            <td><b><?php echo $cliente->dg_razon ?></b></td>
        </tr>
        <tr>
        	<td>Total</td>
            <td align="right"><b><?php echo Functions::monedaLocal($factura->dq_total) ?></b></td>
        </tr>
        <tr>
        	<td>Pago Pendiente</td>
            <td align="right"><b><?php echo Functions::monedaLocal($factura->dq_total-$factura->dq_monto_pagado) ?></b></td>
        </tr>
    </tbody>
</table>