

<div id="main_cont">
    <div class="panes">
        <?php
        $form->Header("<strong>Complete los datos para la edición del producto</strong><br />(Los campos marcados con [*] son obligatorios)");
        $form->Start(Factory::buildActionUrl('procesarForm'), "prod_edit", "form", "method='post' enctype='multipart/form-data'");
        $form->Section();
        $form->Text("Nombre", "prod_nombre", 1, 255,$producto->dg_producto);
        $form->DBSelect("Linea de negocio", "prod_lnegocio", "tb_linea_negocio", array("dc_linea_negocio", "dg_linea_negocio"), 1, $producto->dc_linea_negocio);
        $form->DBSelect("Tipo de producto", "prod_tipo", "tb_tipo_producto", array("dc_tipo_producto", "dg_tipo_producto"), 1, $producto->dc_tipo_producto);
        $form->Text("Unidad de medida", "prod_unidad", 1, 15, $producto->dg_unidad_medida);
        $form->File("Imagen del producto", "prod_foto");
        $form->Combobox('', 'prod_serie', array('Requiere serie'), $producto->dm_requiere_serie == 0 ? array(0) : array());
        $form->EndSection();
        $form->Section();
        $form->Text("Código", "prod_codigo", 1, 255, $producto->dg_codigo);
        $form->Text("Precio venta [$] (Referencia)", "prod_precio_venta", 1, 10, $producto->dq_precio_venta / $producto->dq_cambio);
        $form->Text("Precio compra [$] (Referencia)", "prod_precio_compra", 1, 10, $producto->dq_precio_compra / $producto->dq_cambio);
        $form->DBSelect("Tipo de cambio", 'prod_tipo_cambio', 'tb_tipo_cambio', array("dc_tipo_cambio", "dg_tipo_cambio"), 1, $producto->dc_tipo_cambio);
        $form->DBSelect("Marca", "prod_marca", "tb_marca", array("dc_marca", "dg_marca"), 1, $producto->dc_marca);
        $form->DBSelect("Centro beneficio", "prod_cebe", "tb_cebe", array("dc_cebe", "dg_cebe"), 1, $producto->dc_cebe);
        $form->EndSection();
        $form->Hidden("id_prod", $producto->dc_producto);
        $form->End("Crear", "addbtn");
        ?>
        <iframe name="edproducto_fres" id="edproducto_fres" style="display:none;"></iframe>
    </div>
</div>

<script type="text/javascript">
    $("#prod_edit").attr("target", "edproducto_fres");
    $("#prod_edit").submit(function(e) {
        if (validarForm("#prod_edit")) {
            disableForm("#prod_edit");
        } else {
            e.preventDefault();
        }
    });
    $("#edproducto_fres").load(function() {
        res = frames['edproducto_fres'].document.getElementsByTagName("body")[0].innerHTML;
        $("#prod_edit_res").html(res);
	hide_loader();
    });
    
</script>
