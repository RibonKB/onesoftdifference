<div id="other_options">
	<ul>
		<li><a href="<?php echo Factory::buildUrl('BuscarProducto', 'productos', 'index')?>" class="loader">Consulta</a></li>
		<li><a href="<?php echo Factory::buildUrl('CrearProducto', 'productos', 'index')?>" class="loader">Creación</a></li>
		<li><a href="<?php echo Factory::buildUrl('EditarProducto', 'productos', 'index')?>" class="loader">Edición</a></li>
		<li class="oo_active"><a href="<?php echo Factory::buildUrl('EliminarProducto', 'productos', 'index')?>">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont">
	<div class="panes">
	<fieldset>
		<div class="left">
			<img src="<?php echo($foto); ?>" alt="" style="width:90px;border:1px solid #CCC;background:#EEE;padding:3px;margin:5px;" />
		</div>
		<div class="left">
			<label>Código: </label><?=$datos->dg_codigo ?><br />
			<label>Nombre: </label><?=$datos->dg_producto ?><br />
			<label>Precio de venta: </label>$<?=number_format($datos->dq_precio_venta,0,",",".") ?><br />
			<label>Precio de compra: </label>$<?=number_format($datos->dq_precio_compra,0,",",".") ?><br />
			<label>Marca: </label><?=$datos->dg_marca ?><br />
			<label>Línea de Negocio: </label><?=$datos->dg_linea_negocio ?><br />
		</div>
		<hr class="clear" />
		<div class="center">
		¿Está seguro que quiere eliminar el producto seleccionado?<br />
                <form action="<?php echo Factory::buildActionUrl('eliminar') ?>" class="confirmValidar" id="del_prod">
			<input type="hidden" name="id_prod" value="<?php echo($datos->dc_producto); ?>" />
			<input type="submit" class="delbtn" value="Eliminar" />
		</form>
		<div id="del_prod_res"></div>
		</div>
	</fieldset>
	</div>
</div>
