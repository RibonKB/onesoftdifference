
<div id="options_menu">
	<a href="sites/logistica/proc/src_stock_report.php?st_productos[]=<?=$datos->dc_producto ?>" class="loadOnOverlay button">Detalle del Stock</a>
	<a href="sites/logistica/proc/src_producto_cartola.php?id=<?=$datos->dc_producto ?>" class="loadOnOverlay button">Cartola de existencias</a>
</div>
<br /><br />
	<div class="panes">
	<fieldset>
		<div class="left">
			<img src="<?=$foto ?>" alt="" style="width:90px;border:1px solid #CCC;background:#EEE;padding:3px;margin:5px;" />
		</div>
		<div class="left">
			<label>Código: </label><?=$datos->dg_codigo ?><br />
			<label>Nombre: </label><?=$datos->dg_producto ?><br />
			<label>Precio de venta (Referencial): </label>$<?=number_format($datos->dq_precio_venta,$empresa_conf->dn_decimales_local,$empresa_conf->dm_separador_decimal,$empresa_conf->dm_separador_miles) ?><br />
			<label>Precio de compra (Referencial): </label>$<?=number_format($datos->dq_precio_compra,$empresa_conf->dn_decimales_local,$empresa_conf->dm_separador_decimal,$empresa_conf->dm_separador_miles) ?><br />
			<label>Marca: </label><?=$datos->dg_marca ?><br />
			<label>Línea de Negocio: </label><?=$datos->dg_linea_negocio ?><br />
		</div>
	</fieldset>
	</div>
</div>
