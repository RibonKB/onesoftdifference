<div id="other_options">
	<ul>
		<li><a href="#" class="loader">Consulta</a></li>
		<li><a href="<?php echo Factory::buildUrl('CrearProducto', 'productos', 'index')?>" class="loader">Creación</a></li>
		<li><a href="<?php echo Factory::buildUrl('BEditarProducto', 'productos', 'index')?>" class="loader">Edición</a></li>
		<li><a href="<?php echo Factory::buildUrl('EliminarProducto', 'productos', 'index')?>" class="loader">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
		
	$form->Header("<strong>Ingrese el código del producto que quiere consultar.</strong><br />
	(Los criterios de búsqueda son codigo,nombre del producto, tipo de producto,marca, línea de negocio)");
	$form->Start(Factory::buildActionUrl('buscar'),"busqueda_prod","cValidar");
	$form->text("Código del producto","prod_codigo",1);
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[0]+" ( "+row[1]+" )";
}
$("#prod_codigo").autocomplete('sites/proc/autocompleter/producto.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>
