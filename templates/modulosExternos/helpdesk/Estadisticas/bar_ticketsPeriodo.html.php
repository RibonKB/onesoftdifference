<div class="box span6">
  <div class="box-header">
    <span class="title">
      <i class="icon-bar-chart"></i>
      Periodos tickets
    </span>
  </div>
  <div class="box-content">
    <table class="table table-normal">
      <tbody>
        <tr>
          <td rowspan="2" width="410">
            <div id="bar" style="width: 410px; height: 180px;"></div>
          </td>
          <td>
            Leyenda
          </td>
        </tr>
        <tr>
          <td>
            <table class="table table-normal">
              <tbody>
                <tr>
                  <td>
                    <span style="color:#3880aa;" class="icon-circle"></span>
                    0 - 5 Días
                  </td>
                </tr>
                <tr>
                  <td>
                    <span style="color:#4da944;" class="icon-circle"></span>
                    6 - 10 Días
                  </td>
                </tr>
                <tr>
                  <td>
                    <span style="color:#f26522;" class="icon-circle"></span>
                    10 o más Días
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="box-footer">
    <small><i class="icon-asterisk"></i> En Cantidad de tickets</small>
  </div>
</div>
<script type="text/javascript">
  var myChart = new xChart('bar',{
  "xScale": "ordinal",
  "yScale": "linear",
  "type": "bar",
  "main": <?php echo json_encode($bar_ticketsOS) ?>
} ,'#bar');
</script>