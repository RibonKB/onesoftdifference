<div class="box span5">
  <div class="box-header">
    <span class="title">
      <i class="icon-bar-chart"></i>
      Evaluación técnica
    </span>
  </div>
  <div class="box-content">
    <table class="table table-normal">
      <thead>
        <tr>
          <th>Técnico</th>
          <th>Promedio Evaluación</th>
          <th>OS</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($stat_evaluacion as $t): ?>
        <tr>
          <td><?php echo $t->dg_nombres.' '.$t->dg_ap_paterno.' '.$t->dg_ap_materno ?></td>
          <td>(<?php echo round($t->dq_promedio) ?>)
            <?php for($i = 0; $i < $t->dq_promedio; $i++): ?>
            <i class="icon-star" style="color:#FF8C00"></i>
            <?php endfor ?>
          </td>
          <td><?php echo $t->dq_ordenes_servicio ?></td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </div>
</div>