<div class="box span5">
  <div class="box-header">
    <span class="title">
      <i class="icon-bar-chart"></i>
      Estado Orden de Servicio
    </span>
  </div>
  <div class="box-content">
    <table class="table table-normal">
      <tbody>
        <tr>
          <td rowspan="2" width="<?php echo $pie_ticketsOS['options']['width'] ?>">
            <div id="pie"></div>
          </td>
          <td class="box-header">
            Leyendas
          </td>
        </tr>
        <tr>
          <td>
            <table class="table table-normal">
              <tbody>
                <?php foreach ($pie_ticketsOS['options']['tooltipValueLookups']['names'] as $i => $name): ?>
                  <tr>
                    <td>
                      <span style="color:<?php echo $pie_ticketsOS['options']['sliceColors'][$i] ?>" class="icon-circle"></span>
                      <?php echo $name ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="box-footer">
    <small><i class="icon-asterisk"></i> Porcentaje en cantidad de Tickets</small>
  </div>
</div>

<script type="text/javascript">
  $('#pie').sparkline(<?php echo json_encode($pie_ticketsOS['values']) ?>,<?php echo json_encode($pie_ticketsOS['options']) ?>);
</script>