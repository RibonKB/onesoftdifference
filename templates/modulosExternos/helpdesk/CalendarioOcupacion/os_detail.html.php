<!-- Datos del cliente -->
<div class="box-section news with-icons">
  <div class="avatar blue">
    <i class="icon-user icon-2x"></i>
  </div>
  <div class="news-content">
    <div class="news-title">
      <?php echo $cliente->dg_razon ?>
    </div>
    <div class="news-text">
      <div>
        <i class="icon-user"></i>
        <?php echo $orden_servicio->dg_solicitante ?>
      </div>
      <div>
        <i class="icon-building"></i>
        <?php echo $sucursal->dg_sucursal ?>
      </div>
    </div>
  </div>
</div>

<!-- Datos Supervisor -->
<div class="box-section news with-icons">
  <div class="avatar blue">
    <i class="icon-eye-open icon-2x"></i>
  </div>
  <div class="news-content">
    <div class="news-title">
      Supervisor
    </div>
    <div class="news-text">
      <i class="icon-eye-open"></i>
      <?php if($supervisor): ?>
      	<?php echo $supervisor->dg_nombres . ' ' . $supervisor->dg_ap_paterno . ' ' . $supervisor->dg_ap_materno ?>
      <?php else: ?>
      	--No especificado--
      <?php endif ?>
    </div>
  </div>
</div>

<!-- Datos Ejecutores -->
<div class="box-section news with-icons">
  <div class="avatar blue">
    <i class="icon-group icon-2x"></i>
  </div>
  <div class="news-content">
    <div class="news-title">
      Ejecutores
    </div>
    <div class="news-text">
      <?php foreach ($ejecutores as $e): ?>
        <div>
          <i class="icon-user"></i>
          <?php echo $e->dg_nombres . ' ' . $e->dg_ap_paterno . ' ' . $e->dg_ap_materno ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>

<!-- Datos Requerimiento -->
<div class="box-section news with-icons">
  <div class="avatar blue">
    <i class="icon-info-sign icon-2x"></i>
  </div>
  <div class="news-content">
    <div class="news-title">
      Detalle Requerimiento
    </div>
    <div class="news-text">
      <div>
      	<i class="icon-file-alt"></i> OS:
        <b><?php echo $orden_servicio->dq_orden_servicio ?></b>
      </div>
      <div>
      	<i class="icon-calendar"></i> D:
        <?php echo $orden_servicio->df_compromiso ?>
      </div>
      <div>
      	<i class="icon-calendar"></i> H:
        <?php echo $orden_servicio->df_fecha_final ?>
      </div>
      <div>
        <i class="icon-file-alt"></i>
        <?php echo $orden_servicio->dg_requerimiento ?>
      </div>
      <div>
        <i class="icon-check"></i>
        <?php echo $orden_servicio->dg_solucion ?>
      </div>
    </div>
  </div>
</div>

<!-- Datos Proyecto -->
<?php if(isset($orden_servicio->proyecto) && $orden_servicio->proyecto): ?>
<div class="box-section news with-icons">
  <div class="avatar blue">
    <i class="icon-suitcase icon-2x"></i>
  </div>
  <div class="news-content">
    <div class="news-title">
      Proyecto
    </div>
    <div class="news-text">
      - <?php echo $orden_servicio->proyecto->dg_nombre ?>
    </div>
  </div>
</div>
<?php endif ?>

<!-- Datos facturación -->
<?php if($orden_servicio->dm_facturable): ?>
<div class="box-section">
  <div class="alert alert-success">
    <i class="icon-check icon-2x"></i>
    Orden de Servicio Facturable
  </div>
</div>
<?php endif; ?>