<div class="box">
  <div class="box-header">
    <ul class="nav nav-tabs nav-tabs-left">
      <li><a href="#proyectos" data-toggle="tab">Proyectos</a></li>
      <li><a href="#supervisor" data-toggle="tab">Supervisor</a></li>
      <li><a href="#tecnico" data-toggle="tab">Técnico</a></li>
    </ul>
    <div class="title">
      Filtros
      <span class="icon-refresh" id="reset_filtros" title="Resetear filtros"></span>
    </div>
  </div>
  <div class="box-content">
    <div class="tab-content">
      <div class="tab-pane" id="proyectos">
        <div class="box-section">
          <?php $form->Select('Seleccione el proyecto a filtrar', 'dc_proyecto', $proyectos[0]) ?>
        </div>
      </div>
      <div class="tab-pane" id="supervisor">
        <div class="box-section">
          <?php $form->Select('Seleccione el supervisor a filtrar', 'dc_supervisor', $supervisores[0]) ?>
        </div>
      </div>
      <div class="tab-pane" id="tecnico">
        <div class="box-section">
          <?php $form->Select('Seleccione el técnico a filtrar', 'dc_tecnico', $tecnicos[0]) ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    var os_por_proyecto = <?php echo json_encode($proyectos[1]) ?>;
    var os_por_supervisor = <?php echo json_encode($supervisores[1]) ?>;
    var os_por_tecnico = <?php echo json_encode($tecnicos[1]) ?>;
    
    var mostrar_os = function(os){
      $('.fc-event').addClass('hidden');
      for(i in os){
        $('.evento'+os[i]).removeClass('hidden');
      }
    };
    
    $('#dc_proyecto').change(function(){
      $('#dc_tecnico,#dc_supervisor').val(0);
      var dc_proyecto = $(this).val();
      mostrar_os(os_por_proyecto[dc_proyecto]);
    });
    
    $('#dc_supervisor').change(function(){
      $('#dc_proyecto,#dc_tecnico').val(0);
      var dc_supervisor = $(this).val();
      mostrar_os(os_por_supervisor[dc_supervisor]);
    });
    
    $('#dc_tecnico').change(function(){
      $('#dc_proyecto,#dc_supervisor').val(0);
      var dc_tecnico = $(this).val();
      mostrar_os(os_por_tecnico[dc_tecnico]);
    });
    
    $('#reset_filtros').click(function(){
      $('.fc-event').removeClass('hidden');
    });
    
</script>