<div class="box">
  <div class="box-header">
    <span class="title"><i class="icon-calendar"></i></span>
  </div>
  <div class="box-content">
    <div id="tec-cal" class="span7"></div>
    <div class="span4">
      <br />

      <div class="progress progress-striped progress-blue active hidden" id="loaderFiltros">
        <div class="bar tip" title="" style="width: 100%;" data-original-title="10%"></div>
      </div>
      <div id="filtrosContainer"></div>

      <div class="box">
        <div class="box-header">
          <span class="title">
            <i class="icon-list"></i>
            Detalle Ocupación
          </span>
        </div>
        <div class="box-content" id="event-data-os">
          <div class="alert alert-info">
            <i class="icon-exclamation-sign"></i> <small>Seleccione un evento en el calendario para obtener detalles</small>
          </div>
        </div>
        <div class="box-footer">
          <i class="icon-info-sign"></i>
          <small>Horarios correspondientes a fechas estimadas en ordenes de servicio</small>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

  var data_url = '<?php echo CalendarioOcupacionFactory::buildActionUrl('getEvents') ?>';
  var details_url = '<?php echo CalendarioOcupacionFactory::buildActionUrl('viewOSDetail') ?>';
  var filtros_url = '<?php echo Factory::buildActionUrl('getFiltrosData') ?>';

  var getEventIcon = function(icon_class) {
    return $('<i>').addClass('icon-' + icon_class).css({
      'float': 'left',
      'marginLeft': '5px'
    });
  }

  $('#tec-cal').fullCalendar({
    header: {
      left: 'prev,next,today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: false,
    events: data_url,
    loading: function(isLoading, view) {
      if (isLoading) {
        var date = $('#tec-cal').fullCalendar('getDate');
        $('#loaderFiltros').removeClass('hidden');
        $('#filtrosContainer').load(filtros_url, {dc_mes: date.getMonth(), dc_anho: date.getFullYear()}, function(data) {
            $('#loaderFiltros').addClass('hidden');
        });
      }
    },
    eventRender: function(event, element) {
      element.addClass('evento'+event.id);
      $('.fc-event-time', element).remove();
      if (event.solucionado) {
        $('.fc-event-inner', element).prepend(getEventIcon('check'));
      }
      if (event.facturable) {
        $('.fc-event-inner', element).prepend(getEventIcon('money'));
      }
      if (event.telefonica) {
        var icon = getEventIcon('phone-sign');
        if (event.evaluacion <= 5) {
          icon.css({
            'color': '#F00'
          });
        } else {
          icon.css({
            'color': '#0F0'
          });
        }
        $('.fc-event-inner', element).prepend(icon);
      }
      $(element).attr('title', event.description).tooltip();
    },
    eventClick: function(calEvent, jsEvent, view) {
      $.ajax({
        url: details_url,
        data: {
          dc_orden_servicio: calEvent.id
        },
        dataType: 'html',
        success: function(data) {
          $('#event-data-os').html(data);
        }
      });

    }
  });
</script>