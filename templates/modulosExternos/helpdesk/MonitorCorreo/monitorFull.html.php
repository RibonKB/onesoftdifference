<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th colspan="2">Asunto</th>
      <th>Remitente</th>
      <th>Fecha</th>
      <th>Factor Confianza</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($correos as $c): ?>
      <tr class="status-pending">
        <td width="10">
          <?php if ($c->dm_attachment): ?>
            <span class="icon-paper-clip" title="Este correo incluye archivos adjuntos"></span>
          <?php endif ?>
        </td>
        <td>
          <a href="<?php echo MonitorCorreoFactory::buildActionUrl('leerCorreo',array('id' => $c->dc_correo, 'rand' => rand())) ?>" data-toggle="modal" data-target="#modal<?php echo $c->dc_correo ?>">
            <strong>
              <?php
                if($c->dg_asunto):
                  echo $c->dg_asunto;
                 else:
                  echo '[sin asunto]';
                endif;
              ?>
            </strong>
          </a>
              <div id="modal<?php echo $c->dc_correo ?>" class="modal hide fade">
                <div class="modal-body nopadding" style="max-height:550px"></div>
              </div>
        </td>
        <td><?php echo $c->dg_from ?></td>
        <td><?php echo (new DateTime($c->df_correo))->format('d-m-Y') ?></td>
        <td class="dashboard-stats">
          <div class="progress progress-blue">
            <div class="bar tip" data-percent="<?php echo $c->dc_factor_confianza * 10 ?>" data-original-title="<?php echo $c->dc_factor_confianza * 10 ?>%"></div>
          </div>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>