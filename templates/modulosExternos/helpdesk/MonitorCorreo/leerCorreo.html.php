<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <span class="title">
    <?php
    if ($correo->getAsunto()):
      echo $correo->getAsunto();
    else:
      echo '[sin asunto]';
    endif;
    ?>
  </span>
</div>
<div class="scrollable" style="max-height: 500px; overflow:auto">
  <div class="alert alert-info" style="margin:0">
    <div>
      <span class="icon-user"></span> <?php echo $correo->getFrom() ?>
    </div>
    <div>
      <span class="icon-time"></span> <?php echo (new DateTime($correo->getFecha()))->format('d-m-Y H:i') ?>
    </div>
  </div>

  <?php if ($correo->getHasAttachment()): ?>
    <div class="box-section">
      <?php foreach ($correo->getAttachments() as $a): ?>
        <span class="growl-info btn btn-blue mail-attachment">
          <i class="<?php echo CorreoManagerService::getAttachmentIcon($a->dg_mimetype) ?>"></i>
          <b><?php echo $a->dg_filename; ?></b>
        </span>
      <?php endforeach; ?>
    </div>
  <?php endif ?>

  <div class="box-section">
    <?php if ($correo->getHasInline()): ?>
      <div class="alert">
        Haga clic acá para mostrar las imagenes del correo
      </div>
    <?php endif ?>
    <?php echo $correo->getBody() ?>
  </div>
</div>