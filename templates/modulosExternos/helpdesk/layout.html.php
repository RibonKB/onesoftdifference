<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800">
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <title>Ticket Monitor - Onesoft PYMERP</title>
    <link href="styles/core_admin/application.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="jscripts/lib/core_admin/application.js" type="text/javascript"></script>
  </head>
  <body>

    <?php include(__DIR__."/topnav.html.php"); ?>
    <?php include(__DIR__."/sidebar.html.php"); ?>

    <div class="main-content">
      <div class="container-fluid padded">
        <div class="row-fluid">

          <!-- Breadcrumb line. first item has a blue class attached to breadcrumb-button. there's a yellow class too, try it -->

          <div id="breadcrumbs">
            <div class="breadcrumb-button blue">
              <a href="#">
              <span class="breadcrumb-label"><i class="icon-home"></i>Inicio</span>
              </a>
              <span class="breadcrumb-arrow"><span></span></span>
            </div>

            <div class="breadcrumb-button">
              <span class="breadcrumb-label">
                <span class="icon-<?php echo $header_icon ?>"></span> <?php echo $this->title ?>
              </span>
              <span class="breadcrumb-arrow"><span></span></span>
            </div>
          </div>

        </div>
      </div>

      <div class="container-fluid hpadded">
          <?php include(__DIR__."/bodyLayout.html.php"); ?>
        </div>
      </div>
    </body>
  </html>