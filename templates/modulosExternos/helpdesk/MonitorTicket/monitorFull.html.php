<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>N° Ticket</th>
      <th>Título</th>
      <th>Cliente</th>
      <th>Fecha</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($tickets as $t): ?>
    <tr>
      <td><strong><?php echo $t->getNumeroTicket() ?></strong></td>
      <td><strong><?php echo $t->getTitulo() ?></strong></td>
      <?php if($t->getCliente()): ?>
      <td><?php echo $t->getCliente()->dg_razon ?></td>
      <?php else: ?>
      <td>-</td>
      <?php endif ?>
      <td><?php echo $t->getFecha()->format('d-m-Y H:i') ?></td>
      <td>
        <a href="<?php echo Factory::buildActionUrl('verTicket', array('id' => $t->getId())) ?>" data-toggle="modal" data-target="#modal<?php echo $t->getId() ?>">
          <i class="icon-envelope" title="Detalles"></i>
        </a>
        <div id="modal<?php echo $t->getId() ?>" class="modal hide fade">
           <div class="modal-body nopadding" style="max-height:550px"></div>
        </div>
      </td>
    </tr>
  <?php endforeach ?>
  </tbody>
</table>