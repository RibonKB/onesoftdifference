<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h4 class="title">
    Ticket de Soporte N° <strong><?php echo $ticket->getNumeroTicket() ?></strong>
  </h4>
</div>
<div class="modal-body scrollable" style="max-height: 450px; overflow:auto">
  <div class="alert alert-info">
    <h4>
      <span class="icon-tag"></span> <strong><?php echo $ticket->getTitulo() ?></strong>
    </h4>
    <div>
      <span class="icon-time"></span> <?php echo $ticket->getFecha()->format('d-m-Y H:i') ?>
    </div>
    <div>
      <span class="icon-building"></span> <?php echo $ticket->getCliente()->dg_razon ?>
    </div>
  </div>
  
  <!-- Listado de correos -->
  <ul id="correos-tab" class="nav nav-tabs" style="margin-bottom:0">
    <li class="dropdown active">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        Correos
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu">
      <?php foreach($correos as $c): ?>
        <li<?php if($c->getId() == $ticket->getCorreo_default()): ?> class="active"<?php endif; ?>>
          <a href="#correo<?php echo $c->getId() ?>" data-toggle="tab"><?php echo $c->getAsunto() ?></a>
        </li>
      <?php endforeach; ?>
      </ul>
    </li>
  </ul>
  <div class="box">
  <div id="correos-tabContent" class="tab-content box-content">
    <?php foreach($correos as $c): ?>
    <div class="box-section tab-pane<?php if($c->getId() == $ticket->getCorreo_default()): ?> active<?php endif ?>" id="correo<?php echo $c->getId() ?>">
      <?php echo $c->getBody() ?>
    </div>
    <?php endforeach; ?>
  </div>
  </div>
</div>