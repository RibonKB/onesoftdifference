<div class="navbar navbar-top navbar-inverse">
  <div class="navbar-inner">
    <div class="container-fluid">

      <a class="brand" href="#">Ticket System</a>

      <!-- the toggle buttons. notice the data-targets on them, they point to the topbar/sidebar -->

      <ul class="nav pull-right">
        <li class="toggle-primary-sidebar hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-primary"><a><i class="icon-th-list"></i></a></li>
        <li class="collapsed hidden-desktop" data-toggle="collapse" data-target=".nav-collapse-top"><a><i class="icon-align-justify"></i></a></li>
      </ul>

      <div class="nav-collapse nav-collapse-top">

        <ul class="nav full pull-right">
          <li class="dropdown user-avatar">

            <!-- the dropdown has a custom user-avatar class, this is the small avatar with the badge -->

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span>
                <span class="icon-user icon-2x"></span>
                <span><?php echo $this->getUserData()->dg_usuario ?> <i class="icon-caret-down"></i></span>
              </span>
            </a>

            <ul class="dropdown-menu">

              <!-- the first element is the one with the big avatar, add a with-image class to it -->

              <li class="with-image">
                <div class="avatar">
                  <img src="images/avatar/default_photo.jpg" />
                </div>
                <span><?php echo $this->getUserData()->dg_nombres.' '.$this->getUserData()->dg_ap_paterno.' '.$this->getUserData()->dg_ap_materno; ?></span>
              </li>

            </ul>
          </li>
        </ul>

      </div>
      <div class="navbar-search pull-right">
        <div class="span2 text-right">Actualización automática</div>
        <div class="span1 text-left">
          <input type="checkbox" id="refresh-page-checkbox" checked="checked" />
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#refresh-page-checkbox').iButton();
  window.setInterval(function(){
    if($('#refresh-page-checkbox').is(':checked')){
      window.location.reload();
    }
  },300000)
</script>