<div class="sidebar-background">
  <div class="primary-sidebar-background">
  </div>
</div>
<div class="primary-sidebar">
  <ul class="nav nav-collapse collapse nav-collapse-primary">

    <li>
      <span class="glow"></span>
      <a href="<?php echo Factory::buildUrl('MonitorCorreo', 'modulosExternos', 'index', 'helpdesk') ?>">
        <i class="icon-envelope icon-2x"></i>
        <span>Correos</span>
      </a>
    </li>

    <li>
      <span class="glow"></span>
      <a href="<?php echo Factory::buildUrl('MonitorTicket', 'modulosExternos', 'index', 'helpdesk') ?>">
        <i class="icon-wrench icon-2x"></i>
        <span>Tickets</span>
      </a>
    </li>
    
    <li>
      <span class="glow"></span>
      <a href="<?php echo Factory::buildUrl('Estadisticas', 'modulosExternos', 'index', 'helpdesk') ?>">
        <i class="icon-bar-chart icon-2x"></i>
        <span>Estadísticas</span>
      </a>
    </li>
    
    <li>
      <span class="glow"></span>
      <a href="<?php echo Factory::buildUrl('CalendarioOcupacion', 'modulosExternos', 'index', 'helpdesk') ?>">
        <i class="icon-calendar icon-2x"></i>
        <span>Calendario Ocupación</span>
      </a>
    </li>

  </ul>
</div>
