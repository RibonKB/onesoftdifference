<?php if(!count($data[0])): ?>
	<div class="alert">
		No se ha encontrado información de llamados para la persona especificada
	</div>
<?php else:?>
<table id="cllr_datatable" class="tab">
	<thead>
		<tr>
			<th title="Base de conocimiento">BC</th>
			<th>Cliente</th>
			<th>Último llamado</th>
			<th>Teléfono</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($data[0] as $dc_cliente => $cliente): ?>
		<tr>
			<td><img src="images/<?=$data[2][$dc_cliente] ?>.png" width="20" /></td>
			<td><?=$cliente[0]->dg_razon ?></td>
			<?php if($data[1][$dc_cliente] === false): ?>
			<td colspan="2" style="color:#D00">Sin llamadas registradas</td>
			<?php elseif($data[1][$dc_cliente] == 'NO'): ?>
			<td colspan="2" style="color:#999">Sin contactos asociados</td>
			<?php else: ?>
			<td><?=$data[1][$dc_cliente]->ultima_llamada ?> días</td>
			<td>
				<a href="http://192.168.0.202/phone/asterisk_call_trigger.php?<?=http_build_query(array(
					'call' => $data[1][$dc_cliente]->dst,
					'ext' => $caller->dg_anexo,
					'razon' => $cliente[0]->dg_razon
				)) ?>" target="cllr_call_frame" class="cllr_call_button" data-dccliente="<?=$dc_cliente ?>">
				<img src="images/call.png" width="20" alt="[CALL]" style="margin-right:3px" />
				<?=$data[1][$dc_cliente]->dst ?></a>
			</td>
			<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<iframe id="cllr_call_frame" name="cllr_call_frame" class="hidden"></iframe>
<?php endif; ?>