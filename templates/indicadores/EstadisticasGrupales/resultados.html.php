<table id="cllr_datatable2" class="tab">
	<thead>
		<tr>
			<th>Métrica</th>
			<th>Prom. Grupal</th>
			<th>Mayor</th>
			<th>Mi dato</th>
			<th>Grupo/Mayor</th>
		</tr>
	</thead>
	<tbody>
		<tr class="">
			<td width="200"><b>Oportunidades Activas</b></td>
			<td width="100"><?=$opActivas[0] ?></td>
			<td width="100"><?=$opActivas[1] ?></td>
			<td width="100"><?=$opActivas[2] ?></td>
			<td width="100">
				<img src="images/<?=$opActivas[3] ?>_arrow.png" />
				<img src="images/<?=$opActivas[4] ?>_arrow.png" />
			</td>
		</tr>
		<tr>
			<td><b>Clientes Asignados</b></td>
			<td><?=$clAsignados[0] ?></td>
			<td><?=$clAsignados[1] ?></td>
			<td><?=$clAsignados[2] ?></td>
			<td>
				<img src="images/<?=$clAsignados[3] ?>_arrow.png" />
				<img src="images/<?=$clAsignados[4] ?>_arrow.png" />
			</td>
		</tr>
		<tr>
			<td><b>Porcentaje de Cierre</b></td>
			<td><?=$opPorcentajeCierre[0] ?>%</td>
			<td><?=$opPorcentajeCierre[1] ?>%</td>
			<td><?=$opPorcentajeCierre[2] ?>%</td>
			<td>
				<img src="images/<?=$opPorcentajeCierre[3] ?>_arrow.png" />
				<img src="images/<?=$opPorcentajeCierre[4] ?>_arrow.png" />
			</td>
		</tr>
		<?php foreach($aporteNV[0] as $i => $a):?>
		<tr>
			<td><b>Aporte NV (<?=$aporteNV[0][$i][0] ?>)</b></td>
			<td><?=$aporteNV[0][$i][1] ?></td>
			<td><?=$aporteNV[1][$i][1] ?></td>
			<td><?=$aporteNV[2][$i][1] ?></td>
			<td>
				<?php if($aporteNV[0][$i][1] > $aporteNV[2][$i][1]): ?>
					<img src="images/down_arrow.png" />
				<?php else: ?>
					<img src="images/up_arrow.png" />
				<?php endif; ?>
				
				<?php if($aporteNV[1][$i][1] > $aporteNV[2][$i][1]): ?>
					<img src="images/down_arrow.png" />
				<?php else: ?>
					<img src="images/up_arrow.png" />
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
		
		<?php foreach($facturaData[1][0] as $i => $a):?>
		<tr>
			<td><b>Aporte FV (<?=$facturaData[1][0][$i][0] ?>)</b></td>
			<td><?=$facturaData[1][0][$i][1] ?></td>
			<td><?=$facturaData[1][1][$i][1] ?></td>
			<td><?=$facturaData[1][2][$i][1] ?></td>
			<td>
				<?php if($facturaData[1][0][$i][1] > $facturaData[1][2][$i][1]): ?>
					<img src="images/down_arrow.png" />
				<?php else: ?>
					<img src="images/up_arrow.png" />
				<?php endif; ?>
				
				<?php if($facturaData[1][1][$i][1] > $facturaData[1][2][$i][1]): ?>
					<img src="images/down_arrow.png" />
				<?php else: ?>
					<img src="images/up_arrow.png" />
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
		
		<?php foreach($facturaData[0][0] as $i => $a):?>
		<tr>
			<td><b>Total Facturado (<?=$facturaData[0][0][$i][0] ?>)</b></td>
			<td><?=$facturaData[0][0][$i][1] ?></td>
			<td><?=$facturaData[0][1][$i][1] ?></td>
			<td><?=$facturaData[0][2][$i][1] ?></td>
			<td>
				<?php if($facturaData[0][0][$i][1] > $facturaData[0][2][$i][1]): ?>
					<img src="images/down_arrow.png" />
				<?php else: ?>
					<img src="images/up_arrow.png" />
				<?php endif; ?>
				
				<?php if($facturaData[0][1][$i][1] > $facturaData[0][2][$i][1]): ?>
					<img src="images/down_arrow.png" />
				<?php else: ?>
					<img src="images/up_arrow.png" />
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
