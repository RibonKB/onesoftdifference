<table id="cllr_datatable2" class="tab">
	<tr class="">
		<td width="200"><b>Oportunidades Activas</b></td>
		<td width="100"><?=$op_activas ?></td>
		<td width="15"></td>
	</tr>
	<tr>
		<td><b>Clientes Activos</b></td>
		<td><?=$cl_activos ?></td>
		<td>
			<?php if($cl_activos >= 30): ?>
			<img src="images/up_arrow.png" />
			<?php else: ?>
			<img src="images/down_arrow.png" />
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td><b>Clientes Nuevos del mes</b></td>
		<td><?=$clientes_mes ?></td>
		<td>
			<?php if($clientes_mes >= 10): ?>
			<img src="images/up_arrow.png" />
			<?php else: ?>
			<img src="images/down_arrow.png" />
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td><b>clientes Asignados</b></td>
		<td><?=$cl_asignados ?></td>
		<td>
			<?php if($cl_asignados >= 100): ?>
			<img src="images/up_arrow.png" />
			<?php else: ?>
			<img src="images/down_arrow.png" />
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<td><b>Porcentaje de Cierre</b></td>
		<td><?=$porcentaje_cierre ?>%</td>
		<td>
			<?php if($porcentaje_cierre > 50): ?>
			<img src="images/up_arrow.png" />
			<?php else: ?>
			<img src="images/down_arrow.png" />
			<?php endif; ?>
		</td>
	</tr>
	<?php foreach($aporte_nv as $a): ?>
	<tr>
		<td><b>Aporte NV (<?=$a[0] ?>)</b></td>
		<td><?=$a[1] ?></td>
		<td>
			<?php if($a[1] > $this->aporteNV): ?>
			<img src="images/up_arrow.png" />
			<?php else: ?>
			<img src="images/down_arrow.png" />
			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; ?>
	<?php foreach($aporte_fv as $a): ?>
	<tr>
		<td><b>Aporte FV (<?=$a[0] ?>)</b></td>
		<td><?=$a[1] ?></td>
		<td>
			<?php if($a[1] > $this->aporteFV): ?>
			<img src="images/up_arrow.png" />
			<?php else: ?>
			<img src="images/down_arrow.png" />
			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; ?>
	
	<?php foreach($promedio_anual as $a): ?>
	<tr>
		<td><b>Aporte Promedio Anual (<?=$a[0] ?>)</b></td>
		<td><?=$a[1] ?></td>
		<td>
			<?php if($a[1] > $this->aporteAnual): ?>
			<img src="images/up_arrow.png" />
			<?php else: ?>
			<img src="images/down_arrow.png" />
			<?php endif; ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>