<?php
?>

<table class="tab bicolor_tab">
    <thead>
        <tr>
            <th>
              Tarea  
            </th>
            <th>
                Descripción
            </th>
            <th>
                Fecha Limite
            </th>
            <th>
                Estado
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if($tareas){
        foreach($tareas as $D_Tareas):
        ?>
        <tr>
            <td>
                <?=$D_Tareas->Nombre_tarea?>
            </td>
            <td>
                <?=$D_Tareas->Descripcion_tarea?>
            </td>
            <td>
                <?=$D_Tareas->Fecha_tarea?>
            </td>
            <td>
                <?=$D_Tareas->Estado_tarea?>
            </td>
        </tr>
        <?php 
        endforeach;
        }else{
            ?>
        <tr>
            <td colspan="4" align="center">
                No tiene tareas
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <th>
                
            </th>
        </tr>
    </tfoot>
</table>