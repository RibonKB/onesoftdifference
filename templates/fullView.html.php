<div id="secc_bar">
  <?php echo $title ?>
</div>
<div id="main_cont">
  <div class="left">
    <a href="<?php echo Factory::getRequestURI() ?>" class="loader">
      <img src="images/reload.png" />
    </a>
  </div>
  <div class="panes">
    <?php echo $content ?>
  </div>
</div>