<?php
$form->Start(Factory::buildActionUrl('procesarCrear'),'cr_asignacion_stock');
$form->Header('Indique la orden de compra que utilizará en el proceso de asignación de stock<br />
  <strong>Los campos marcados con [*] son obligatorios</strong>');

    $form->Text('Orden de Compra','dq_orden_compra',true);
    $form->Group('id="orden_compra_datatable" class="hidden"');
    $form->Group();

$form->End('Crear','addbtn');
?>
<script type="text/javascript" src="jscripts/sites/logistica/cr_asignacion_stock_libre.js?v=0_1_7"></script>
<script type="text/javascript">
  js_data.OCAutocompleteUrl = '<?php echo Factory::buildActionUrl('acOrdenCompra') ?>';
  js_data.OCDetailDataResultUrl = '<?php echo Factory::buildActionUrl('getOCDetailDataResult') ?>';
  js_data.init();
</script>
