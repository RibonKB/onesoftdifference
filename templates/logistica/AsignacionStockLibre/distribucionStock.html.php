<table class="tab" width="70%" align="center">
  <thead>
    <tr>
      <th>Bodega</th>
      <th width="100">Cantidad</th>
      <th width="100">Asignación</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($detalle as $d): ?>
    <tr>
      <td><b><?php echo $d->dg_bodega ?></b></td>
      <td>
        <?php echo $d->dq_stock_libre ?>
        <input type="hidden" class="dc_stock_maximo" value="<?php echo $d->dq_stock_libre ?>" />
        <input type="hidden" class="dc_bodega" value="<?php echo $d->dc_bodega ?>" />
      </td>
      <td align="right"><input type="text" class="inputtext dq_stock_value" /></td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>
<div class="center">
  <input type="hidden" id="dc_detalle_oc" value="<?php echo $dc_detalle ?>" />
  <button id="btn_asignar_stock_libre" class="button">Asignar</button>
</div>
<script type="text/javascript">
    js_data.initAsignacionStock();
</script>