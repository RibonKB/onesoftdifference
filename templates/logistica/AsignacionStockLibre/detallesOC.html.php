<table class="tab bicolor_tab" width="80%" align="center">
  <thead>
    <tr>
      <th>Producto</th>
      <th width="100">Cantidad OC</th>
      <th width="100">Recepcionada</th>
      <th width="100">En Stock</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($detalle as $d): ?>
    <tr>
      <td><b><?php echo $d->dg_descripcion ?></b></td>
      <td align="right"><?php echo $d->dq_cantidad ?></td>
      <td align="right"><?php echo $d->dc_recepcionada ?></td>
      <td id="dc_detalle_<?php echo $d->dc_detalle ?>">
        <?php if($d->dc_stock_libre > 0): ?>
        <a href="<?php echo Factory::buildActionUrl('asignarStock', array(
            'dc_detalle' => $d->dc_detalle
        )) ?>" class="loadOnOverlay alert">
        <b><?php echo $d->dc_stock_libre ?></b>
        </a>
        <?php else: ?>
          -
        <?php endif ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>