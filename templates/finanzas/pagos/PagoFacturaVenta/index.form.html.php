<?php $form->Start(self::buildActionUrl('procesarAsignacion'),'asignar_saldo_favor_form') ?>
<?php $form->Header('<b>Indique qué factura de venta se utilizará para saldar y luego indique la factura de compra que se saldará</b><br />
					 Los campos marcados con [*] son obligatorios') ?>
                     
    <?php $form->Section() ?>
    	<?php $form->Text('Factura de Venta','dc_factura_venta',true) ?>
        <div id="factura_venta_data"></div>
    <?php $form->EndSection() ?>
    
    <?php $form->Section() ?>
		<?php $form->Text('Factura de Compra','dc_factura_compra',true) ?>
        <div id="factura_compra_data"></div>
    <?php $form->EndSection() ?>
    
    <?php $form->Section() ?>
    	<?php $form->Date('Fecha Contable','df_fecha_contable',true,0) ?>
        <?php $form->Text('Monto a Saldar (Dejar en blanco para asignar automáticamente)','dq_monto_saldar') ?>
    <?php $form->EndSection() ?>
    
<?php $form->End('Asignar Saldo') ?>
<script type="text/javascript" src="jscripts/sites/finanzas/pagos/asignar_saldo.js?rand=<?php echo rand() ?>"></script>
<script type="text/javascript">
	js_data.autocompleterURL.facturaVenta = "<?php echo Factory::buildActionUrl('facturaVentaAutoCompletar') ?>";
	js_data.autocompleterURL.facturaCompra = "<?php echo Factory::buildActionUrl('facturaCompraAutoCompletar') ?>";
	js_data.detalleFacturaVenta = "<?php echo Factory::buildActionUrl('obtenerDetallesFacturaVenta') ?>";
	js_data.detalleFacturaCompra = "<?php echo Factory::buildActionUrl('obtenerDetallesFacturaCompra') ?>";
	js_data.init();
</script>