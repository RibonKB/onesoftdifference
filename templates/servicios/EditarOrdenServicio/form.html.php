<?php

//$form = new Form($empresa);

$form->Start(EditarOrdenServicioFactory::buildActionUrl('procesarFormulario'), 'cr_orden_servicio');
$form->Header('Indique los datos actualizados para la orden de servicio <strong>'.$orden_servicio->dq_orden_servicio.'</strong>'.
        '<br /><b>Los datos marcados con [*] son obligatorios</b>');

$form->Section();
  $form->DBSelect('Cliente', 'os_cliente', 'tb_cliente', array('dc_cliente', 'dg_razon'), 1, $orden_servicio->dc_cliente);
  $form->Text('Solicitante', 'os_solicitante', 1, Form::DEFAULT_TEXT_LENGTH, $orden_servicio->dg_solicitante);
  $form->DBSelect('Sucursal','os_sucursal','tb_cliente_sucursal',array('dc_sucursal','dg_sucursal'),1,$orden_servicio->dc_sucursal,"dc_cliente={$orden_servicio->dc_cliente}");
  $form->Text('Nota de venta', 'os_nota_venta', Form::DEFAULT_MANDATORY_STATUS, Form::DEFAULT_TEXT_LENGTH, $nota_venta?$nota_venta->dq_nota_venta:'');
  $form->Hidden('dc_nota_venta', 0);
  $form->Text('Ticket Servicio', 'os_ticket_servicio', Form::DEFAULT_MANDATORY_STATUS, Form::DEFAULT_TEXT_LENGTH, $ticket?$ticket->dq_ticket:'');
  $form->Hidden('dc_ticket_servicio', 0);
  ?><br /><fieldset><?php
  $form->Radiobox('Facturable', 'os_facturable', array('SI', 'NO'), ($orden_servicio->dm_facturable*-1)+1, '');
  $form->Text('Glosa Facturación', 'os_glosa_factura', Form::DEFAULT_MANDATORY_STATUS, Form::DEFAULT_TEXT_LENGTH, $orden_servicio->dg_glosa_facturacion);
  ?></fieldset><?php
$form->EndSection();

$form->Section();
  ?>
  <fieldset>
    <?php
    $form->DBSelect('Área de servicio', 'os_area', 'tb_area_servicio', array('dc_area_servicio', 'dg_area_servicio'), true, $orden_servicio->dc_area_servicio);
    $form->DBSelect('Tipo de falla', 'os_tipo_falla', 'tb_tipo_falla_os', array('dc_tipo_falla', 'dg_tipo_falla'), false, $orden_servicio->dc_tipo_falla);
    if($orden_servicio->dg_tipo_falla_manual)
		$select = array('checked="checked"',"value='{$orden_servicio->dg_tipo_falla_manual}'");
	else
		$select = array('','disabled="disabled"');
    ?>
    <label>
      <input type='checkbox' name='os_other_falla' <?php echo $select[0] ?>/>Otra
    </label>
    <input type='text' name='os_tipo_falla_txt' class='inputtext' size='30' <?php echo $select[1] ?> />
  </fieldset><br />
  <?php

  $form->Textarea('Requerimiento', 'os_request', true, $orden_servicio->dg_requerimiento);
$form->EndSection();

$form->Section();
  ?><fieldset><?php
  $form->DateTime('Fecha inicio', 'os_fecha', true, $orden_servicio->df_compromiso);
  $form->DateTime('Fecha estimada de término', 'os_fecha_final', true, $orden_servicio->df_fecha_final);
  ?></fieldset><?php
  $form->DBSelect('Supervisor', 'os_tecnico', 'tb_funcionario', array('dc_funcionario', 'dg_nombres', 'dg_ap_paterno', 'dg_ap_materno'), true, $orden_servicio->dc_tecnico);
  $form->DBMultiSelect('Ejecutores', 'os_tecnico_apoyo', 'tb_funcionario', array('dc_funcionario', 'dg_nombres', 'dg_ap_paterno', 'dg_ap_materno'), $ejecutores);
$form->EndSection();

$form->Hidden('os_id',$orden_servicio->dc_orden_servicio);

$form->End('Editar', 'editbtn');
?>
<script type="text/javascript" src="jscripts/sites/servicios/cr_orden_servicio.js?v=0_3"></script>
<script type='text/javascript' src='jscripts/sites/servicios/ed_orden_servicio.js?v=2_0_1b'></script>
<script type='text/javascript'>
  js_data.editInit(
      <?php echo json_encode($nota_venta) ?>,
      <?php echo json_encode($ticket) ?>
  );
</script>