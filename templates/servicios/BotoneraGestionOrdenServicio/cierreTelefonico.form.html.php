<?php
$form->Start(Factory::buildActionUrl('procesarCierreTelefonico'),'cr_cierre_telefonico');
  $form->Header("<b>Indique los datos para la gestión telefónica en la orden de servicio {$orden_servicio->dq_orden_servicio}</b><br />
                 Los datos marcados con [*] son obligatorios");
  $form->Section();
    $form->Textarea('Gestión','dg_gestion',true);
    $form->RadioBox('Estado Solución','dm_estado_solucion',array('Solucionado','Reabrir'),0);
  $form->EndSection();
  
  $form->Section('tel_gestion_solucionado');
    $form->DateTime('Inicio '.Form::MANDATORY_ICON,'df_inicio_telefonico',false,$orden_servicio->df_inicio);
    $form->DateTime('Término '.Form::MANDATORY_ICON,'df_fin_telefonico',false,$orden_servicio->df_fin);
    echo '<br /><label>Evaluación Servicio '.Form::MANDATORY_ICON.'</label><br />';
    $form->Hidden('dc_evaluacion_servicio',0);
  $form->EndSection();
  
  $form->Section('tel_gestion_reabrir');
    $form->DBSelect('Estado Orden de Servicio '.Form::MANDATORY_ICON,'dc_estado_orden_reabrir','tb_estado_orden_servicio',array('dc_estado','dg_estado'));
  $form->EndSection();
  
  $form->Hidden('dc_orden_servicio',$orden_servicio->dc_orden_servicio);
  
$form->End('Aceptar','addbtn');
?>
<script type="text/javascript" src="jscripts/sites/servicios/cierre_telefonico.js?v=0_1_2"></script>