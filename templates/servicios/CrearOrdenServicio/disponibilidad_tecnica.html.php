<div class="info">
  Área de Servicio: <b><?php echo $area->dg_area_servicio ?></b>
  <br />
  Tipo de falla: <b><?php echo $tipo_falla->dg_tipo_falla ?></b>
  <br />
  Fecha inicio: <b><?php echo $fecha_inicio->format('d-m-Y H:i') ?></b>
  <br />
  Fecha Término <b><?php echo $fecha_termino->format('d-m-Y H:i') ?></b>
</div>

<table class="tab bicolor_tab" width="100%">
  <caption>Técnicos disponibles</caption>
  <tbody>
    <?php foreach($tecnicos as $t): ?>
    <tr>
      <td><?php echo $t->dg_nombres.' '.$t->dg_ap_paterno.' '.$t->dg_ap_materno ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>