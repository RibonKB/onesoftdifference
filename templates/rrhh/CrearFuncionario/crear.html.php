<div id="secc_bar">Creación de funcionarios</div>
<div id="other_options">
	<ul>
		<li class="oo_active"><a href="#">Creación</a></li>
		<li><a href="<?php echo Factory::buildUrl('editarFuncionario', 'rrhh') ?>" class="loader">Edición</a></li>
		<li><a href="<?php echo Factory::buildUrl('eliminaFuncionario', 'rrhh') ?>" class="loader">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont">
<div class="panes">
<?php
	$form->Header("<strong>Complete los datos para la creación del funcionario</strong><br />(Los campos marcados con [*] son obligatorios)");
	$form->Start(Factory::buildActionUrl('crear'),"func_crear","form","method='post' enctype='multipart/form-data'");
	$form->Section();
	$form->Text("RUT","func_rut",1,255,"","inputrut");
	$form->Text("Apellido Paterno","func_ap_paterno",1);
	$form->Date("Fecha nacimiento","func_nacimiento",1);
	$form->EndSection();
	$form->Section();
	$form->Text("Nombres","func_nombres",1);
	$form->Text("Apellido Materno","func_ap_materno",1);
	$form->Radiobox("Sexo","func_sexo",array("Hombre","Mujer"),0);
	$form->EndSection();
	$form->Group();
	$form->Section();
	$form->select("Centro costo","func_ceco",$ceco,1);
	$form->Date("Inicio contrato","func_ini_contr",1,0);
	$form->select("Salud","func_salud",$isapre,1);
	$form->Text("Sueldo bruto (\$)","func_sueldo");
	$form->Text('Anexo','func_anexo',0,20);
	$form->EndSection();
	$form->Section();
	$form->select("Tipo de contrato","func_t_contrato",$tcontr,1);
	$form->Date("Término contrato","func_fin_contr");
	$form->select("AFP","func_afp",$afp,1);
	$form->File("Foto","fc_foto");
        $form->select('Cargo','cargo',$cargo,1);
	$form->EndSection();
	$form->Section();
		$form->Radiobox('Evaluación Tickets','dm_evaluacion_ticket',array('NO','SI'),0);
	$form->EndSection();
	$form->End("Crear","addbtn");
?>
<iframe name="addfuncionario_fres" id="addfuncionario_fres" class="hidden"></iframe>
</div>
</div>

<script type="text/javascript">
$("#func_crear").attr("target","addfuncionario_fres");
$("#func_crear").submit(function(e){
	if(validarForm("#func_crear")){	
		disableForm("#func_crear");
	}else{
		e.preventDefault();
	}
});
$("#addfuncionario_fres").load(function(){
	res = frames['addfuncionario_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#func_crear_res").html(res);
	hide_loader();
});

</script>
