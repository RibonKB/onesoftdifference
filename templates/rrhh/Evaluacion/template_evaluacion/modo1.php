<?php

require("inc/fpdf.php");

class PDF extends FPDF {

    private $datosEmpresa;
    private $evaluacion;
    private $tipo_eval;
    private $nombre_func;
    private $promedio;
    private $rut;
    private $fechaEvaluacion;

    public function PDF($evaluacion, $empresa, $db, $nombre_funcionario, $tipo_eval, $prom,$r,$f) {
        $this->promedio = $prom;
        $this->evaluacion = $evaluacion;
        $this->tipo_eval = $tipo_eval;
        $this->nombre_func = $nombre_funcionario;
        $this->rut=$r;
        $fe=explode(' ',$f);
        $fe2=explode('-',$fe[0]);
        $mes='';
        switch($fe2[1]){
            case 1:
                $mes='Enero';
                break;
            case 2:
                $mes='Febrero';
                break;
            case 3:
                $mes='Marzo';
                break;
            case 4:
                $mes='Abril';
                break;
            case 5:
                $mes='Mayo';
                break;
            case 6:
                $mes='Junio';
                break;
            case 7:
                $mes='Julio';
                break;
            case 8:
                $mes='Agosto';
                break;
            case 9:
                $mes='Septiembre';
                break;
            case 10:
                $mes='Octubre';
                break;
            case 11:
                $mes='Noviembre';
                break;
            case 12:
                $mes='Diciembre';
                break;
             
        }
        $this->fechaEvaluacion="de ".$mes." del ".$fe2[0];
        $datosE = $db->prepare($db->select(
                        "(SELECT * FROM tb_empresa WHERE dc_empresa={$empresa}) e
	LEFT JOIN (SELECT * FROM tb_empresa_configuracion WHERE dc_empresa={$empresa}) ec ON e.dc_empresa = ec.dc_empresa
	LEFT JOIN tb_comuna c ON e.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region r ON c.dc_region = r.dc_region", 
                "e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region,e.dg_razon"));
        $db->stExec($datosE);
        $this->datosEmpresa = $datosE->fetch(PDO::FETCH_OBJ);

        unset($datosE);
        parent::__construct('P', 'mm', 'Letter');
    }

    function Header() {
        $this->SetFont('Arial', '', 7);
        $this->SetTextColor(110);
        $this->SetY(5);
        $this->MultiCell(120, 2.4, utf8_decode("{$this->datosEmpresa->dg_razon}\n{$this->datosEmpresa->dg_direccion}, {$this->datosEmpresa->dg_comuna} {$this->datosEmpresa->dg_region}\n{$this->datosEmpresa->dg_fono}"));
        $this->SetTextColor(0);
        $this->Ln(4);
        $this->SetFont('Arial', '', 12);
        $this->SetFont('', '', 10);
        $this->SetY(20);
        $this->SetX(140);
        $this->Cell(0, 5, utf8_decode('Evaluación '.$this->fechaEvaluacion), 0, 1, 'C');
        $this->SetY(25);
        $this->Ln();
        $this->SetFont('', '', 10);
        $this->SetY(27);
        $this->SetX(-350);
        
        $this->Cell(0, 5, utf8_decode($this->nombre_func), 0, 0, 'C');
        $this->SetY(31);
        $this->SetX(-353);
        $this->Cell(0, 5, utf8_decode($this->rut), 0, 0, 'C');
        $this->ln();
        $this->SetFont('Arial', '', 7);
        $this->SetTextColor(110);
        $this->MultiCell(120, 2.4, "");
        $this->SetTextColor(0);
        $this->Ln(4);
    }

    function setBody() {
        $this->AddPage();
        $this->SetY(35);
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $this->SetFont('', 'B');
        $this->Cell(120, 7, 'Pregunta', 2, 0, 'C', 1);
        $this->Cell(15, 7, 'Calificacion', 2, 0, 'C', 1);
        $this->Cell(60, 7, 'Comentario', 2, 0, 'C', 1);
        $this->AliasNbPages();
        foreach ($this->evaluacion as $d => $a) {
            foreach ($a as $r => $s) {
                $this->Ln();
                $this->Cell(120, 0, "", 1, 0, 'C', 1);
                $this->Cell(15, 0, "", 1, 0, 'C', 1);
                $this->Cell(60, 0, "", 1, 0, 'L', 1);
                $this->Ln(1);
                foreach ($s as $f => $g) {
                    $this->Cell(120, 10, utf8_decode("{$r}"), 2, 0, 'L', 1);
                    $this->Cell(15, 10, "{$f}", 2, 0, 'C', 1);
                    $rr = explode(' ', $g);
                    $i = 0;
                    $t = '';
                    foreach ($rr as $a) {

                        if ($i == 7) {
                            $i = 0;
                            $t = $t . "\n";
                            $this->Cell(60, 8, utf8_decode("{$t}"), 2, 0, 'L', 1);
                            $t = '';
                            $this->Ln(6);
                            $this->Cell(120, 10, '', 2, 0, 'C', 1);
                            $this->Cell(15, 10, '', 2, 0, 'C', 1);
                        }
                        $t = $t . " " . $a;
                        $i++;
                    }
                    $this->Cell(60, 10, utf8_decode("{$t}"), 2, 0, 'L', 1);
                }
            }
        }
        $this->Ln();
        $this->Cell(120, 0, "", 1, 0, 'C', 1);
        $this->Cell(15, 0, "", 1, 0, 'C', 1);
        $this->Cell(60, 0, "", 1, 0, 'L', 1);
        $this->Ln(1);
        $this->Cell(120, 7, "Promedio", 2, 0, 'R', 1);
        $this->Cell(15, 7, "{$this->promedio->dc_promedio}", 2, 0, 'C', 1);
        $cs = explode(' ', $this->promedio->dg_comentario);
        $cd = '';
        $u = 0;
        $h = count($cs);
        foreach ($cs as $f):
            if ($u == 7):
                $u = 0;
                $this->Cell(60, 7, utf8_decode("{$cd}"), 2, 0, 'L', 1);
                $this->Ln(6);
                $this->Cell(120, 7, "", 2, 0, 'R', 1);
                $this->Cell(15, 7, "", 2, 0, 'C', 1);
                $cd = '';
            endif;
            $cd = $cd . " " . $f;
            $u++;
        endforeach;
        $this->Cell(60, 7, utf8_decode("{$cd}"), 2, 0, 'L', 1);
        $this->Ln();
        $this->Cell(120, 0, "", 1, 0, 'C', 1);
        $this->Cell(15, 0, "", 1, 0, 'C', 1);
        $this->Cell(60, 0, "", 1, 0, 'L', 1);

        $this->SetTextColor(255);
        $this->SetLineWidth(.3);
    }

    function setFoot() {
        $this->SetFont('Arial', '', 7);
        $this->SetDrawColor(200);
        $this->SetTextColor(50);
        $this->Cell(0, 3, '', 'B');
        $this->Ln();
        $y = $this->GetY();
        $this->Ln();
        $this->Ln();
        $this->Ln();
        $this->Ln();
        $this->Ln();
        $this->Cell(130, 3, "____________________________________________", 'C');
        $this->Cell(100, 3, "____________________________________________", 'C');
        $this->Ln();
        $this->SetFont('', '', 10);
         $this->SetX(155);
        $this->Cell(100, 5, "Firma Evaluador", 'C');
        $this->SetX(-339);
        $this->Cell(0, 5, utf8_decode($this->nombre_func), 0, 0, 'C');
        $this->ln();
        $this->SetX(-339);
        $this->Cell(0, 5, utf8_decode($this->rut), 0, 0, 'C');
       
    }

}

?>