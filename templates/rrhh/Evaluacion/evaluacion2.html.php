
<?PHP
if (!isset($solo_ve)) {
    $form->Start(Factory::buildUrl('evaluacionF', 'rrhh', 'index'), 'dl_ev', 'cValidar');
    $form->Header("Seleccione");
    if ($lstEv == 'si') {
        $form->radiobox3('', 'hola', array(2 => 'Evaluacion', 3 => 'Evaluacion en consenso'));
    } else {
        $form->radiobox3('', 'hola', array(1 => 'Auto-Evaluacion', 2 => 'Evaluacion', 3 => 'Evaluacion en consenso'));
    }

    $form->Section();
    $form->select2("Seleccione Usuario a evaluar", "user_a_ev", $usr, 1, '', 'display:none');
    $form->select2("Seleccione Usuario a evaluar", "user_a_ev_cons", $user_cons, 1, '', 'display:none');
    $form->EndSection();
    $form->hidden2('evaluar', $ev_fu);
    $form->hidden2('conseso', $conse);
    $form->End2('Evaluar', 'addbtn', 'visibility:hidden');
} else {
    if ($lstEv == 'no'):
        $form->Start(Factory::buildUrl('Evaluacion', 'rrhh', 'autoEva'), 'dl_ev', 'cValidar');
        $form->Header("Seleccione");
        $form->radiobox3('', 'hola', array(1 => 'Auto-Evaluacion'),1,1);
        $form->Section();
        $form->EndSection();
        $form->End2('Evaluar', 'addbtn');
    endif;
}
?>
<table class="tab bicolor_tab" width="100%">
    <caption></caption>
    <thead>
    <tr align="center">
        <th width="30%">
            Funcionario
        </th>
        <th width="15%">
            Auto-evaluación
        </th>
        <th width="15%">
            Evaluación
        </th>
        <th width="15%">
            Consenso
        </th>
        <th width="25%">
            Estrellas
        </th>
    </tr>
    </thead>
    <tbody>
    <?php
    if ($tbl_prom != null) {
        foreach ($tbl_prom as $func => $A_Ev) {
            ?>
            <tr align="center">
                <td align="left">
                    <?= $func ?>
                </td>
                <td>
                    <?php if ($tbl_prom[$func]['auto-evaluacion'] != 0) { ?>
                        <a href="#" onclick="over('<?php echo Factory::buildActionUrl('VerEvaluacion', array('funcionario_nombre' => $func, 'funcionario_id' => $tbl_prom[$func]['id_funcionario'], 'tipo_eval' => 2)) ?>')"> <?= $tbl_prom[$func]['auto-evaluacion'] ?> </a>
                        <?php
                    } else {
                        echo $tbl_prom[$func]['auto-evaluacion'];
                    }
                    ?>
                </td>
                <td>
                    <?php if ($tbl_prom[$func]['evaluacion'] != 0) { ?>
                        <a href="#" onclick="over('<?php echo Factory::buildActionUrl('VerEvaluacion', array('funcionario_nombre' => $func, 'funcionario_id' => $tbl_prom[$func]['id_funcionario'], 'tipo_eval' => 1)) ?>')"  > <?= $tbl_prom[$func]['evaluacion'] ?> </a>
                        <?php
                    } else {
                        echo $tbl_prom[$func]['evaluacion'];
                    }
                    ?>
                </td>
                <td>
                    <?php if ($tbl_prom[$func]['evaluacion_consenso'] != 0) { ?>
                        <a href="#" onclick="over('<?php echo Factory::buildActionUrl('VerEvaluacion', array('funcionario_nombre' => $func, 'funcionario_id' => $tbl_prom[$func]['id_funcionario'], 'tipo_eval' => 3)) ?>')"  > <?= $tbl_prom[$func]['evaluacion_consenso'] ?> </a>
                        <?php
                    } else {
                        echo $tbl_prom[$func]['evaluacion_consenso'];
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $est = $tbl_prom[$func]['evaluacion_consenso'];
                    if ($est >= 2 && $est < 4.9) {
                        echo "<label style='color:#000000'>★★</label>";
                    }
                    if ($est >= 5 && $est < 6.9) {
                        echo "<label style='color:#dd0000'>★★★★★</label>";
                    }

                    if ($est >= 7 && $est < 9.9) {
                        echo "<label style='color:#cccc00'>★★★★★★★</label>";
                    }

                    if ($est >= 10) {
                        echo "<label style='color:#33cc00'>★★★★★★★★★★</label>";
                    }
                    ?>
                </td>

            </tr>
            <?php
        }
    }
    ?>
    </tbody>
</table>
<script>

                            function over(link) {
                                loadOverlay(link);
                            }

                            $(".hola").click(function() {
                                var v = this.value;
                                var eval = document.getElementById('evaluar').value;
                                var cons = document.getElementById('conseso').value;
                                if (v == 1)
                                {
                                    $(".selbtn").val("Auto-Evaluacion");
                                    document.forms.dl_ev.action = "<?php echo Factory::buildUrl('Evaluacion', 'rrhh', 'autoEva') ?>";
                                    document.getElementById('user_a_ev').required = '';
                                    document.getElementById('user_a_ev').style.display = 'none';
                                    document.getElementById('euser_a_ev').style.display = 'none';
                                    document.getElementById('user_a_ev_cons').style.display = 'none';
                                    document.getElementById('euser_a_ev_cons').style.display = 'none';
                                    document.getElementById('user_a_ev_cons').required = '';
                                    document.getElementById('Evaluar').style.visibility = 'visible';
                                }
                                if (v == 2)
                                {
                                    if (eval == 'si')
                                    {
                                        $(".selbtn").val("Evaluar");
                                        document.forms.dl_ev.action = "<?php echo Factory::buildUrl('evaluacionF', 'rrhh', 'index') ?>";
                                        document.getElementById('euser_a_ev').style.display = 'block';
                                        document.getElementById('user_a_ev').required = 'required';
                                        document.getElementById('user_a_ev').style.display = 'block';
                                        document.getElementById('user_a_ev_cons').required = '';
                                        document.getElementById('euser_a_ev_cons').style.display = 'none';
                                        document.getElementById('user_a_ev_cons').style.display = 'none';

                                        document.getElementById('Evaluar').style.visibility = 'visible';

                                    } else if (eval == 'no')
                                    {
                                        document.getElementById('user_a_ev_cons').required = '';
                                        document.getElementById('euser_a_ev_cons').style.display = 'none';
                                        document.getElementById('user_a_ev_cons').style.display = 'none';
                                        document.getElementById('Evaluar').style.visibility = 'hidden';
                                    }



                                }
                                if (v == 3)
                                {


                                    if (cons == 'si')
                                    {
                                        $(".selbtn").val("Evaluacion en Consenso");
                                        document.forms.dl_ev.action = "<?php echo Factory::buildUrl('Evaluacion_consenso', 'rrhh', 'index') ?>";
                                        document.getElementById('user_a_ev_cons').style.display = 'block';
                                        document.getElementById('euser_a_ev_cons').style.display = 'block';
                                        document.getElementById('user_a_ev_cons').required = 'required';

                                        document.getElementById('user_a_ev').style.display = 'none';
                                        document.getElementById('euser_a_ev').style.display = 'none';
                                        document.getElementById('user_a_ev').required = '';
                                        document.getElementById('Evaluar').style.visibility = 'visible';


                                    } else if (cons == 'no')
                                    {
                                        document.getElementById('user_a_ev').style.display = 'none';
                                        document.getElementById('euser_a_ev').style.display = 'none';
                                        document.getElementById('user_a_ev').required = '';

                                        document.getElementById('Evaluar').style.visibility = 'hidden';
                                    }
                                }


                            });
</script>

