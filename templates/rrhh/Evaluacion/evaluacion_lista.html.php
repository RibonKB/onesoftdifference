<ul id="tabs">
    <?php
    $i = 0;
    foreach ($datos as $gp => $p):
        if ($i === 0) {
            ?>
            <li><a href="#" class="current"><?= $gp ?></a></li>
            <?php
            $i++;
        } else {
            ?>
            <li><a href="#" ><?= $gp ?></a></li>
            <?php
        }
    endforeach;
    ?>
</ul>   
<br class="clear" />
<br class="clear" />
<br class="clear" />

<button type='button' class='button' id='print_version'>Version de impresión</button>

<div class="tabpanes">

    <?php
    /*
     * $datos = $datos[dg_grupo_pregunta][dg_pregunta][dc_nota][dg_comentario];
     * 
     *                $datos(array)
     *                          |
     *                      gp(datos)    =>p(array)
     *                                         |
     *                                    pregunta(datos)=>n(array)
     *                                                       |  
     *                                                 nota(datos) => comentario(datos)
     */
    foreach ($datos as $gp => $p):
        ?>
        <div>
            <table class="tab bicolor_tab" width="100%">
                <caption><?php echo $tipo_evaluacion."  ".$nombre_funcionario; ?></caption>
                <thead>
                    <tr>
                        <th>
                            Pregunta 
                        </th>
                        <th>
                            Nota 
                        </th>
                        <th>
                            Comentario
                        </th>
                    </tr>
                </thead>
                <tbody>
                            <?php foreach ($p as $pregunta => $n):
                                foreach ($n as $nota => $comentario):
                                    ?>
                            <tr align="center">
                                <td  align="left">
            <?= $pregunta ?>
                                </td>
                                <td>
                                    <?php
                                    switch($nota):
                                        case 2:
                                            ?>
                                        <label style='color:#000000;font-size: 15px'>★★</label>
                                        <?php
                                        break;
                                        case 5:
                                            ?>
                                        <label style='color:#dd0000;font-size: 15px'>★★★★★</label>
                                        <?php
                                            break;
                                        case 7:
                                            ?>
                                        <label style='color:#cccc00;font-size: 15px'>★★★★★★★</label>
                                        <?php
                                        break;
                                        case 10:
                                            ?>
                                        <label style='color:#33cc00;font-size: 15px'>★★★★★★★★★★</label>
                                        <?php
                                            break;
                                    endswitch; ?>
                                </td>
                                <td>
                            <?= $comentario ?>
                                </td>
                            </tr>
            <?php
        endforeach;
    endforeach;
    ?>
                </tbody>
                <tfoot>
                    <tr>

                    </tr>
                </tfoot>
            </table>
        </div>
<?php endforeach; ?>
</div>

<script>
    $('#print_version').unbind('click');
$('#print_version').click(function(){
	window.open("<?php echo Factory::buildActionUrl('PrintVersion', 
                array(
                    'funcionario_id'=>$funcionario_id,
                    'funcionario_nombre'=>$nombre_funcionario,
                    'tipo_eval'=>$tipo_eval)) ?>",'','width=800;height=600');
});
</script>
