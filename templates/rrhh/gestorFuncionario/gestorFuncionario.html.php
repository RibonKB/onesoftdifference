<div id="secc_bar">
	Gestión de funcionarios
</div>
<div id="main_cont">

<br />
<br />
<div align="center" class="opciones">
    <?php if (check_permiso(78)): ?>
	<a href="<?php echo Factory::buildUrl('CrearFuncionario', 'rrhh') ?>" class="loader"><img src="images/submenu/creacionfuncionarios.jpg" alt="Creaci&oacute;n Funcionarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Creación</div></a>
     <?php endif; ?>   
       <?php if (check_permiso(80)):?>
        <a href="<?php echo Factory::buildUrl('editarFuncionario', 'rrhh') ?>" class="loader"><img src="images/submenu/edicionfuncionarios.jpg" alt="Edici&oacute;n Funcionarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Edición</div></a>
        <?php 
        endif;
        if (check_permiso(81)):?>
	<a href="<?php echo Factory::buildUrl('eliminarFuncionario', 'rrhh') ?>" class="loader"><img src="images/submenu/eliminarfuncionarios.jpg" alt="Eliminar Funcionarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Eliminación</div></a>
        <?php
        endif;
        ?>
        
</div>

</div>