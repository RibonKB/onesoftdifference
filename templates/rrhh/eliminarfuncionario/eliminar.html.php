<div id="secc_bar">
	Edición de funcionarios
</div>
<div id="other_options">
	<ul>
		<li><a href="<?php echo Factory::buildUrl('CrearFuncionario', 'rrhh') ?>" class="loader">Creación</a></li>
		<li><a href="<?php echo Factory::buildUrl('editarFuncionario', 'rrhh') ?>" class="loader">Edición</a></li>
		<li class="oo_active"><a href="#">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	
	$form->Header("<strong>Ingrese el RUT del funcionario que desea eliminar.</strong><br />(Los criterios de búsqueda son nombre del funcionario, rut del funcionario)");
	$form->Start(Factory::buildActionUrl('eliminar'),"del_func","cValidar");
	$form->Text("RUT Funcionario","fc_rut",1,20,"","inputrut");
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[0]+" ( "+row[1]+" )";
}
$("#fc_rut").autocomplete('../sites/proc/autocompleter/funcionario.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>