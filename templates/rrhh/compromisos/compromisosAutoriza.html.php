
<script>
   var t= window.setTimeout(function(){pymerp.showLoader()},0);
</script>
<ul id="tabs">
    <li><a href="#" class="current">Mis Compromisos</a></li>
    <li><a href="#">Cumplimiento</a></li>
    <li><a href="#">Aprobar/rechazar compromisos</a></li>
    <li><a href="#">Estado de mis compromisos</a></li>
</ul>   
<br class="clear" />
<br class="clear" />
<br class="clear" />
<div class="tabpanes">


    <!-- mis compromisos -->
    <div >
        <table class='tab bicolor_tab' width='100%'>
            <caption>Compromisos del Funcionario</caption>
            <tr>
                <th>
                    Funcionario
                </th>
                <th>
                    Compromiso
                </th>
                <th>
                    Fecha de Compromiso
                </th>
                <th width="7%">
                    Cumplimiento
                </th>
                <th>
                    Compromiso Empresa
                </th>
            </tr>
            <?php
            $i = 0;
            if ($mis_compromisos) {
                foreach ($mis_compromisos as $dc_user => $ll) {
                    foreach ($mis_compromisos[$dc_user] as $func_nombre => $rr) {
                        foreach ($mis_compromisos[$dc_user][$func_nombre] as $compromiso => $ss) {
                            foreach ($mis_compromisos[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                foreach ($mis_compromisos[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                    foreach ($mis_compromisos[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                        ?>
                                        <tr align='center'>
                                            <td><?= $func_nombre ?></td>
                                            <td><?= $compromiso ?></td>
                                            <td><?= $fecha_compromiso ?></td>
                                            <?php
                                            if ($cumple == 1) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    $i++;
                                                    ?>
                                                    <td style="color: goldenrod ; font-weight: bolder">En Espera</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == 2) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == 3) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td style="color: maroon; font-weight: bolder" >No</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == '--') {
                                                ?>
                                                <td><?= $cumple ?></td>
                                                <?php
                                            }
                                            ?>
                                            <td><?= $compromiso_empresa ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                ?>
                <td colspan="5" align="center">No tiene Compromisos</td>
                <?php
            }
            ?>
        </table>
        <br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" /><br class="clear" />
            <br class="clear" />
    </div>
    <br class="clear" />
            <br class="clear" />

    <!-- Compromisos a evaluar -->
    <div>

        <div>
            <?php
            $form->Start(Factory::buildActionUrl('CompromisoCumpleSave'), 'sv_compromiso');
            ?>

            <table class='tab bicolor_tab' width='100%'>
                <caption>En Espera</caption>
                <tr>
                    <th>
                        Funcionario
                    </th>
                    <th>
                        Compromiso
                    </th>
                    <th>
                        Fecha de Compromiso
                    </th>
                    <th width="7%">
                        Cumplimiento
                    </th>
                    <th>
                        Compromiso Empresa
                    </th>
                </tr>
                <?php
                $i = 0;
                if ($evaluar) {
                    foreach ($evaluar as $dc_user => $ll) {
                        foreach ($evaluar[$dc_user] as $func_nombre => $rr) {
                            foreach ($evaluar[$dc_user][$func_nombre] as $compromiso => $ss) {
                                foreach ($evaluar[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                    foreach ($evaluar[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                        foreach ($evaluar[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                            ?>
                                            <tr align='center'>
                                                <td><?= $func_nombre ?></td>
                                                <td><?= $compromiso ?></td>
                                                <td><?= $fecha_compromiso ?></td>
                                                <?php
                                                if ($cumple == 1) {
                                                    if ($estado == 3) {
                                                        ?>
                                                        <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                        <?php
                                                    } else {
                                                        $i++;
                                                        ?>
                                                        <td>
                                                            <?php
                                                            $form->Radiobox3('', 'sino' . $i, array($dc_user . '_' . $fecha_compromiso . '_2' => 'Si', $dc_user . '_' . $fecha_compromiso . '_3' => 'No'));
                                                            ?>
                                                        </td>
                                                        <?php
                                                    }
                                                }
                                                if ($cumple == 2) {
                                                    if ($estado == 3) {
                                                        ?>
                                                        <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                        <?php
                                                    }
                                                }
                                                if ($cumple == 3) {
                                                    if ($estado == 3) {
                                                        ?>
                                                        <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <td style="color: maroon; font-weight: bolder" >No</td>
                                                        <?php
                                                    }
                                                }
                                                if ($cumple == '--') {
                                                    ?>
                                                    <td><?= $cumple ?></td>
                                                    <?php
                                                }
                                                ?>
                                                <td><?= $compromiso_empresa ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else {
                                ?>
                                <td colspan="5" align="center">No tiene ningún compromiso</td>
                                <?php
                            }
                ?>
            </table>
            <?php
            $form->Hidden('Cant_sino', $i);
            $form->End3('Aceptar', 'addbtn');
            ?>
        </div> 
        <br class="clear" />
        <br class="clear" />

        <div>
            <div id="data_table"> 
                <?php
            $form->Start(Factory::buildActionUrl('CompromisoCumpleSave'), 'sv_compromiso');
            ?>
                <table class='tab bicolor_tab' width='100%'>
                    <caption>Cumplimiento (Si)</caption>
                    <tr>
                        <th>
                            Funcionario
                        </th>
                        <th>
                            Compromiso
                        </th>
                        <th>
                            Fecha de Compromiso
                        </th>
                        <th width="7%">
                            Cumplimiento
                        </th>
                        <th>
                            Compromiso Empresa
                        </th>
                    </tr>
                    <?php
                    $i = 0;
                    if ($evaluado) {
                        foreach ($evaluado as $dc_user => $ll) {
                            foreach ($evaluado[$dc_user] as $func_nombre => $rr) {
                                foreach ($evaluado[$dc_user][$func_nombre] as $compromiso => $ss) {
                                    foreach ($evaluado[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                        foreach ($evaluado[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                            foreach ($evaluado[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                                ?>
                                                <tr align='center'>
                                                    <td><?= $func_nombre ?></td>
                                                    <td><?= $compromiso ?></td>
                                                    <td><?= $fecha_compromiso ?></td>
                                                    <?php
                                                    if ($cumple == 1) {
                                                        if ($estado == 3) {
                                                            ?>
                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                            <?php
                                                        } else {
                                                            $i++;
                                                            ?>
                                                            <td>
                                                                En Espera
                                                            </td>
                                                            <?php
                                                        }
                                                    }
                                                    if ($cumple == 2) {
                                                        if ($estado == 3) {
                                                            ?>
                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                            <?php
                                                        }
                                                    }
                                                    if ($cumple == 3) {
                                                        if ($estado == 3) {
                                                            ?>
                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <td style="color: maroon; font-weight: bolder" >No</td>
                                                            <?php
                                                        }
                                                    }
                                                    if ($cumple == '--') {
                                                        ?>
                                                        <td><?= $cumple ?></td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td><?= $compromiso_empresa ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else {
                                ?>
                                <td colspan="5" align="center">No tiene ningún compromiso</td>
                                <?php
                            }
                    ?>
                </table>
            </div>
        </div>

        <br class="clear" />
        <br class="clear" />
        <div>
            <div id="data_table">
                <?php
            $form->Start(Factory::buildActionUrl('CompromisoCumpleSave'), 'sv_compromiso');
            ?>
                <table class='tab bicolor_tab' width='100%'>
                    <caption>Cumplimiento (No)</caption>
                    <tr>
                        <th>
                            Funcionario
                        </th>
                        <th>
                            Compromiso
                        </th>
                        <th>
                            Fecha de Compromiso
                        </th>
                        <th width="7%">
                            Cumplimiento
                        </th>
                        <th>
                            Compromiso Empresa
                        </th>
                    </tr>
                    <?php
                    $i = 0;
                    if ($evaluadon) {
                        foreach ($evaluadon as $dc_user => $ll) {
                            foreach ($evaluadon[$dc_user] as $func_nombre => $rr) {
                                foreach ($evaluadon[$dc_user][$func_nombre] as $compromiso => $ss) {
                                    foreach ($evaluadon[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                        foreach ($evaluadon[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                            foreach ($evaluadon[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                                ?>
                                                <tr align='center'>
                                                    <td><?= $func_nombre ?></td>
                                                    <td><?= $compromiso ?></td>
                                                    <td><?= $fecha_compromiso ?></td>
                                                    <?php
                                                    if ($cumple == 1) {
                                                        if ($estado == 3) {
                                                            ?>
                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                            <?php
                                                        } else {
                                                            $i++;
                                                            ?>
                                                            <td>
                                                                En Espera
                                                            </td>
                                                            <?php
                                                        }
                                                    }
                                                    if ($cumple == 2) {
                                                        if ($estado == 3) {
                                                            ?>
                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                            <?php
                                                        }
                                                    }
                                                    if ($cumple == 3) {
                                                        if ($estado == 3) {
                                                            ?>
                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <td style="color: maroon; font-weight: bolder" >No</td>
                                                            <?php
                                                        }
                                                    }
                                                    if ($cumple == '--') {
                                                        ?>
                                                        <td><?= $cumple ?></td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td><?= $compromiso_empresa ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else {
                                ?>
                                <td colspan="5" align="center">No tiene ningún compromiso</td>
                                <?php
                            }
                    ?>
                </table>
            </div>
        </div>
        <br class="clear" />
        <br class="clear" />
    </div>






    <!-- Aprobar/rechazar compromisos -->
    <div>
        <div id="detail_data_table">
            <br class="clear" />
                <div class="current" >
                    <?php
                    $form->Start(Factory::buildActionUrl('CompromisoEstadoSave'), 'sv_e_compromiso');
                    ?>

                    <table class='tab bicolor_tab' width='100%'>
                        <caption>Compromisos para ser aprobados</caption>
                        <tr>
                            <th>
                                Creador
                            </th>
                            <th>
                                Funcionario
                            </th>
                            <th>
                                Compromiso
                            </th>
                            <th>
                                Fecha de Compromiso
                            </th>
                            <th>
                                Creación Compromiso
                            </th>
                            <th width="7%">
                                Cumplimiento
                            </th>
                            <th>
                                Compromiso Empresa
                            </th>
                            <th>
                                Estado
                            </th>
                        </tr>
                        <?php
                        $i = 0;
                        if ($en_espera) {
                            foreach ($en_espera as $dc_user => $ll) {
                                foreach ($en_espera[$dc_user] as $func_nombre => $rr) {
                                    foreach ($en_espera[$dc_user][$func_nombre] as $compromiso => $ss) {
                                        foreach ($en_espera[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                            foreach ($en_espera[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                                foreach ($en_espera[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $es) {
                                                    foreach ($en_espera[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa][$cumple] as $estado => $ro) {
                                                        foreach ($en_espera[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa][$cumple][$estado] as $df_creacion => $nombre_creador) {
                                                            ?>
                                                            <tr align='center'>
                                                                <td><?= $nombre_creador ?></td>
                                                                <td><?= $func_nombre ?></td>
                                                                <td><?= $compromiso ?></td>
                                                                <td><?= $fecha_compromiso ?></td>
                                                                <td><?= $df_creacion ?></td>
                                                                <?php
                                                                if ($cumple == 1) {
                                                                    if ($estado == 3) {
                                                                        ?>
                                                                        <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <td style="color: goldenrod ; font-weight: bolder">En Espera</td>
                                                                        <?php
                                                                    }
                                                                }
                                                                if ($cumple == 2) {
                                                                    if ($estado == 3) {
                                                                        ?>
                                                                        <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                                        <?php
                                                                    }
                                                                }
                                                                if ($cumple == 3) {
                                                                    if ($estado == 3) {
                                                                        ?>
                                                                        <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <td style="color: maroon; font-weight: bolder" >No</td>
                                                                        <?php
                                                                    }
                                                                }
                                                                if ($cumple == '--') {
                                                                    ?>
                                                                    <td><?= $cumple ?></td>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <td><?= $compromiso_empresa ?></td>
                                                                <?php
                                                                if ($estado == 1) {
                                                                    $i++;
                                                                    ?>
                                                                    <td>
                                                                        <?php
                                                                        $form->Radiobox3('', 'sino' . $i, array($dc_user . '_' . $fecha_compromiso . '_2' => 'Aprobar', $dc_user . '_' . $fecha_compromiso . '_3' => 'Rechazar'));
                                                                        ?>
                                                                    <td>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }else {
                                ?>
                                <td colspan="8" align="center">No tiene ningún compromiso</td>
                                <?php
                            }
                        ?>
                    </table>
                    <?php
                    $form->Hidden('Cant_sino', $i);
                    $form->End3('Aceptar', 'addbtn');
                    ?>
                </div>
            <br class="clear" />
            <br class="clear" />
                <div>
                    <div id="data_table">
                        <table class='tab bicolor_tab' width='100%'>
                            <caption>Compromisos aprobados</caption>
                            <tr>
                                <th>
                                    Creador
                                </th>
                                <th>
                                    Funcionario
                                </th>
                                <th>
                                    Compromiso
                                </th>
                                <th>
                                    Fecha de Compromiso
                                </th>
                                <th>
                                    Creación Compromiso
                                </th>
                                <th width="7%">
                                    Cumplimiento
                                </th>
                                <th>
                                    Compromiso Empresa
                                </th>
                                <th>
                                    Estado
                                </th>
                            </tr>
                            <?php
                            $i = 0;
                            if ($aprobados) {
                                foreach ($aprobados as $dc_user => $ll) {
                                    foreach ($aprobados[$dc_user] as $func_nombre => $rr) {
                                        foreach ($aprobados[$dc_user][$func_nombre] as $compromiso => $ss) {
                                            foreach ($aprobados[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                                foreach ($aprobados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                                    foreach ($aprobados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $es) {
                                                        foreach ($aprobados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa][$cumple] as $estado => $ro) {
                                                            foreach ($aprobados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa][$cumple][$estado] as $df_creacion => $nombre_creador) {
                                                                ?>
                                                                <tr align='center'>
                                                                    <td><?= $nombre_creador ?></td>
                                                                    <td><?= $func_nombre ?></td>
                                                                    <td><?= $compromiso ?></td>
                                                                    <td><?= $fecha_compromiso ?></td>
                                                                    <td><?= $df_creacion ?></td>
                                                                    <?php
                                                                    if ($cumple == 1) {
                                                                        if ($estado == 3) {
                                                                            ?>
                                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                            <?php
                                                                        } else {
                                                                            $i++;
                                                                            ?>
                                                                            <td style="color: goldenrod ; font-weight: bolder">En Espera</td>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    if ($cumple == 2) {
                                                                        if ($estado == 3) {
                                                                            ?>
                                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    if ($cumple == 3) {
                                                                        if ($estado == 3) {
                                                                            ?>
                                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <td style="color: maroon; font-weight: bolder" >No</td>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    if ($cumple == '--') {
                                                                        ?>
                                                                        <td><?= $cumple ?></td>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <td><?= $compromiso_empresa ?></td>
                                                                    <?php
                                                                    if ($estado == 2) {
                                                                        ?>
                                                                        <td style="color: #00aa00; font-weight: bolder">Aprobado</td>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                ?>
                                <td colspan="8" align="center">No tiene ningún compromiso</td>
                                <?php
                            }
                            ?>
                           
                        </table>
                    </div>
                    <br class="clear" />
                </div>
            <br class="clear" />
            <br class="clear" />
                <div>
                    <div id="detail_data_table">
                        <table class='tab bicolor_tab' width='100%'>
                            <caption>Compromisos rechazados</caption>
                            <tr>
                                <th>
                                    Creador
                                </th>
                                <th>
                                    Funcionario
                                </th>
                                <th>
                                    Compromiso
                                </th>
                                <th>
                                    Fecha de Compromiso
                                </th>
                                <th>
                                    Creación Compromiso
                                </th>
                                <th width="7%">
                                    Cumplimiento
                                </th>
                                <th>
                                    Compromiso Empresa
                                </th>
                                <th>
                                    Estado
                                </th>
                            </tr>
                            <?php
                            $i = 0;
                            if ($rechazados) {
                                foreach ($rechazados as $dc_user => $ll) {
                                    foreach ($rechazados[$dc_user] as $func_nombre => $rr) {
                                        foreach ($rechazados[$dc_user][$func_nombre] as $compromiso => $ss) {
                                            foreach ($rechazados[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                                foreach ($rechazados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                                    foreach ($rechazados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $es) {
                                                        foreach ($rechazados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa][$cumple] as $estado => $ro) {
                                                            foreach ($rechazados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa][$cumple][$estado] as $df_creacion => $nombre_creador) {
                                                                ?>
                                                                <tr align='center'>
                                                                    <td><?= $nombre_creador ?></td>
                                                                    <td><?= $func_nombre ?></td>
                                                                    <td><?= $compromiso ?></td>
                                                                    <td><?= $fecha_compromiso ?></td>
                                                                    <td><?= $df_creacion ?></td>
                                                                    <?php
                                                                    if ($cumple == 1) {
                                                                        if ($estado == 3) {
                                                                            ?>
                                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                            <?php
                                                                        } else {
                                                                            $i++;
                                                                            ?>
                                                                            <td style="color: goldenrod ; font-weight: bolder">En Espera</td>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    if ($cumple == 2) {
                                                                        if ($estado == 3) {
                                                                            ?>
                                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    if ($cumple == 3) {
                                                                        if ($estado == 3) {
                                                                            ?>
                                                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <td style="color: maroon; font-weight: bolder" >No</td>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    if ($cumple == '--') {
                                                                        ?>
                                                                        <td><?= $cumple ?></td>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <td><?= $compromiso_empresa ?></td>
                                                                    <?php
                                                                    if ($estado == 3) {
                                                                        ?>
                                                                        <td style="color: maroon; font-weight: bolder">Rechazado</td>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                ?>
                                <td colspan="8" align="center">No tiene ningún compromiso</td>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                </div>

                
            
        </div>
    </div>
    
    <div>
        <br class="clear" />
        
                 <table class='tab bicolor_tab' width='100%'>
                        <caption>Estado de Mis compromisos creados</caption>
                        <tr>
                            <th>
                                Funcionario
                            </th>
                            <th>
                                Compromiso
                            </th>
                            <th>
                                Fecha de Compromiso
                            </th>
                            <th width="7%">
                                Cumplimiento
                            </th>
                            <th>
                                Compromiso Empresa
                            </th>
                            <th>
                                Estado
                            </th>
                        </tr>
                        <?php
                        $i = 0;
                        if ($mis_compromisos_estado) {
                            foreach ($mis_compromisos_estado as $dc_user => $ll) {
                                foreach ($mis_compromisos_estado[$dc_user] as $func_nombre => $rr) {
                                    foreach ($mis_compromisos_estado[$dc_user][$func_nombre] as $compromiso => $ss) {
                                        foreach ($mis_compromisos_estado[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                            foreach ($mis_compromisos_estado[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                                foreach ($mis_compromisos_estado[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                                    ?>
                                                    <tr align='center'>
                                                        <td><?= $func_nombre ?></td>
                                                        <td><?= $compromiso ?></td>
                                                        <td><?= $fecha_compromiso ?></td>
                                                        <?php
                                                        if ($cumple == 1) {
                                                            if ($estado == 3) {
                                                                ?>
                                                                <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td style="color: goldenrod ; font-weight: bolder">En Espera</td>
                                                                <?php
                                                            }
                                                        }
                                                        if ($cumple == 2) {
                                                            if ($estado == 3) {
                                                                ?>
                                                                <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                                <?php
                                                            }
                                                        }
                                                        if ($cumple == 3) {
                                                            if ($estado == 3) {
                                                                ?>
                                                                <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td style="color: maroon; font-weight: bolder" >No</td>
                                                                <?php
                                                            }
                                                        }
                                                        if ($cumple == '--') {
                                                            ?>
                                                            <td><?= $cumple ?></td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <td><?= $compromiso_empresa ?></td>
                                                        <?php
                                                        if ($estado == 1) {
                                                            ?>
                                                            <td style="color: goldenrod ; font-weight: bolder">En Espera</td>
                                                            <?php
                                                        }
                                                        if ($estado == 2) {
                                                            ?>
                                                            <td style="color: #00aa00; font-weight: bolder">Aprobado</td>
                                                            <?php
                                                        }
                                                        if ($estado == 3) {
                                                            ?>
                                                            <td style="color: maroon; font-weight: bolder" >Rechazado</td>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }else {
                                ?>
                                <td colspan="8" align="center">No tiene ningún compromiso</td>
                                <?php
                            }
                        ?>
                    </table>
    </div>




</div>

<script>
var t= window.setTimeout(function(){pymerp.hideLoader()},250);
</script>