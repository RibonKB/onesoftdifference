<table class='tab bicolor_tab' width='100%'>
            <caption>Compromisos del Funcionario</caption>
            <tr>
                <th>
                    Funcionario
                </th>
                <th>
                    Compromiso
                </th>
                <th>
                    Fecha de Compromiso
                </th>
                <th width="7%">
                    Cumplimiento
                </th>
                <th>
                    Compromiso Empresa
                </th>
            </tr>
            <?php
            $i = 0;
            if ($mis_compromisos) {
                foreach ($mis_compromisos as $dc_user => $ll) {
                    foreach ($mis_compromisos[$dc_user] as $func_nombre => $rr) {
                        foreach ($mis_compromisos[$dc_user][$func_nombre] as $compromiso => $ss) {
                            foreach ($mis_compromisos[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                foreach ($mis_compromisos[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                    foreach ($mis_compromisos[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                        ?>
                                        <tr align='center'>
                                            <td><?= $func_nombre ?></td>
                                            <td><?= $compromiso ?></td>
                                            <td><?= $fecha_compromiso ?></td>
                                            <?php
                                            if ($cumple == 1) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    $i++;
                                                    ?>
                                                    <td style="color: goldenrod ; font-weight: bolder">En Espera</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == 2) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == 3) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td style="color: maroon; font-weight: bolder" >No</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == '--') {
                                                ?>
                                                <td><?= $cumple ?></td>
                                                <?php
                                            }
                                            ?>
                                            <td><?= $compromiso_empresa ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {?>
                                        <td colspan="5" align="center">No tiene Compromisos</td>
                <?php
                
            }
            ?>
        </table>
