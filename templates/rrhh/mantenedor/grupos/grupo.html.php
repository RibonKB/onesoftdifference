
<?php
$form->Start(Factory::buildActionUrl('insertar'), 'cr_cargo');
$form->Header("Complete los campos para crear un grupo de pregunta");

$form->Section();
$form->text("Nombre del Grupo", "grupo", 1);
$form->EndSection();
$form->Section();
$form->select('Cargo', 'cargo', $cargo, 1);
$form->EndSection();
$form->End('Crear', 'addbtn');
?>
<table class='tab bicolor_tab' width="100%" >
    <caption>Grupos ya creados</caption>
    <thead>
        <tr>
            <th width="65%">Grupo</th>
            <th width="20%">Cargo</th>
            <th width="15%">Opciones</th>
        </tr>
    </thead>
    <?php
    $k=0;
    $i = 0;
    if($grupos)
    {
    foreach ($grupos as $id_gr => $ti) {

        foreach ($grupos[$id_gr] as $ca => $gr) {
            
            $i++;
            $id_cargo=  split("_",$id_gr);
            ?>
            <tr >
                <td >
        <?php echo "<label class='grupo' id='grupo_".$i."' style='cursor: pointer;'>►</label>".$gr ?>
                </td >
                <td>
        <?php echo $ca ?>
                </td>
                <td>
                    <a href='<?php echo Factory::buildUrl('editar_grupo', 'rrhh', 'index', 'mantenedor', array('id_grupo' => $id_cargo[0],'id_cargo'=>$id_cargo[1])) ?>' class='loadOnOverlay'>
                        <img src='images/editbtn.png' alt='' title='Editar Nombre de Grupo' />
                    </a>
                    <a href='<?php echo Factory::buildUrl('editar_grupo', 'rrhh', 'editCargo', 'mantenedor', array('id_grupo' => $id_cargo[0],'id_cargo'=>$id_cargo[1])) ?>' class='loadOnOverlay'>
                        <img src='images/editbtn.png' alt='' title='Editar Cargo de Grupo' />
                    </a>
                    <a href='<?php echo Factory::buildUrl('eliminar_grupo', 'rrhh', 'index', 'mantenedor', array('id_grupo' => $id_cargo[0],'id_cargo'=>$id_cargo[1])) ?>' class='loadOnOverlay'>
                        <img src='images/delbtn.png' alt='' title='Eliminar Grupo' />
                    </a>
                    <a href='<?php echo Factory::buildUrl('eliminar_grupo', 'rrhh', 'relacion', 'mantenedor', array('id_grupo' => $id_cargo[0],'id_cargo'=>$id_cargo[1])) ?>' class='loadOnOverlay'>
                        <img src='images/delbtn.png' alt='' title='Eliminar Relacion' />
                    </a>
                    <a href='<?php echo Factory::buildUrl('copiar_grupo', 'rrhh', 'index', 'mantenedor', array('id_grupo' => $id_cargo[0],'id_cargo'=>$id_cargo[1])) ?>' class='loadOnOverlay'>
                        <img src='images/editbtn.png' alt='' title='Copiar grupo a otro cargo' />
                    </a>
                </td>
            </tr>
        <?php
        $l = 0;
        if (isset($preguntas[$id_cargo[0]])) {
            foreach ($preguntas[$id_cargo[0]] as $id_pr => $pregunta) {
                $k++;
                $l++;
                ?>
                    <tr id="<?= $i ?>_<?= $l ?>" style="display : none" >
                        <td colspan="3">
                    <?php echo "&nbsp;&nbsp;&nbsp;• <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>". $pregunta ?>
                        </td>
                        
                    </tr>
                <?php
            }
        }
    }
}
}
?>
</table>
</div>
</div>
<input type="hidden" id="id_anterior" value="0"/>
<script>

    $('.grupo').click(function()
    {
        var id_ant = document.getElementById('id_anterior').value;
        var id = this.id;

        var idspl = id.split('_');

        
        if (id_ant != idspl[1])
        {
                            
            for(var i=1;i<=<?=$k?>;i++)
                {
                    if(document.getElementById(id_ant+"_"+i))
                        {
                            document.getElementById('grupo_'+id_ant).innerHTML="►";
                           document.getElementById(id_ant+"_"+i).style.display='none'
                        }
                
            }

        }
        
        for(var i=1;i<=<?=$k?>;i++)
                {
                    if(document.getElementById(idspl[1]+"_"+i))
                        {
                            if (document.getElementById(idspl[1]+"_"+i).style.display=='table-row')
                                {
                                    document.getElementById(id).innerHTML="►";
                                    document.getElementById(idspl[1]+"_"+i).style.display='none';
                                }else
                                    {
                                        document.getElementById(id).innerHTML="▼";
                                        document.getElementById(idspl[1]+"_"+i).style.display='table-row';
                                    }
                            
                           
                        }
                    
                }
        
        document.getElementById('id_anterior').value = idspl[1];

    });
$('.addbtn').click(function(){
    
//al transcurrir 3 segundos se redirige al formulario de búsqueda nuevamente
setTimeout("loadpage('<?php echo factory::buildUrl('grupos', 'rrhh','index','mantenedor') ?>')",200);
});
</script>