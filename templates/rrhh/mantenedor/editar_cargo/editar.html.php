

        <?php
        $form->Start(Factory::buildActionUrl('editar'), 'edit_cargo');
        $form->Header("Editar Cargos a Evaluar");

        $form->Section();
        $form->text("Cargo", "cargo", 0,250,$cargo->dg_cargo);
        $form->EndSection();

        $form->Section();
        $form->Select('CeCo', 'ceco', $sel,0,$datos->dc_ceco);
        $form->EndSection();
        $form->Section();
        $form->Combobox3('','evaluador',array(1=>'Evaluador'),'',$checkEv);
        $form->Combobox2('','continuar',array(1=>'Reemplazar evaluador del cargo (en caso de haberlo)'));
        $form->EndSection();
        $f=0;
        ?>
        <table class="tab bicolor_tab" width="100%" id="tabla"  >
            <tr>
        <?php
        foreach ($cargos as $gr => $ti) {
                
            
                        
                    $form->Section();?>
                    <td colspan="100%" align="center"><?=$gr?></td><tr>
                        <?php
                    foreach($cargos[$gr] as $sb=>$no)
                    {
                        $f++;
                        if (isset($cargos_ocup[$gr][$sb]))
                        {
                        ?>
                    
                <td >
                    <?php
                    
                    $form->Combobox4('', 'evalua'.$f, array($sb=>$no),$checkCargo);
                    ?>
                        </td>
                    
                        <?php
                        }else
                        {
                           ?>
                    
                        <td>
                    <?php
                    
                    $form->Combobox2('', 'evalua'.$f, array($sb=>$no),'',$checkCargo);
                    ?>
                        </td>
                    
                        <?php 
                        }
                    }?></tr><br /><?php
                    $form->EndSection();
            }
            ?>
            
            </tr>
            </table>
            <?php
            $form->Hidden('id_evaluador',$ev);
            $form->Hidden('valorf',$f);
        $form->End('Editar', 'addbtn');
        ?>