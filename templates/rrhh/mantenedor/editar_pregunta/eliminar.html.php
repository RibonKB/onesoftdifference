
        <?php
        $form->Start(Factory::buildActionUrl('eliminar'), 'del_pregunta');

        $form->Section();
        
        foreach ($pregunta as $id => $area) {
            ?>
        
        <table class='tab bicolor_tab' width='100%'>
            <thead>
            <tr>
                <th width="50%">Pregunta</th>
                <th width="50%">Grupo</th>
            </tr>
            </thead>
            <tr align='center'>
            
                <?php
                foreach($pregunta[$id] as $gr=>$pr)
                {
                ?>
                <td><?=$pr?></td>
                <td><?=$gr?></td>
                <?php
                }
                ?>
            </tr>
            <tr align='center'>
                <td colspan='2'>¿Está seguro de eliminar la pregunta?</td>
            </tr>
        </table>
            <?php
            $form->Hidden('id_pregunta', $id);
        }
        $form->EndSection();
       
        $form->End('Eliminar', 'delbtn');
        ?>