
        <?php
        $form->Start(Factory::buildActionUrl('crear'), 'cr_cargo');
        $form->Header("Complete los campos para crear un cargo");

        $form->Section();
        $form->text("Cargo", "cargo", 1);
        $form->EndSection();

        $form->Section();
        $form->Select('CeCo', 'ceco', $sel);
        $form->EndSection();
        $form->Section();
        $form->Combobox3('','evaluador',array(1=>'Evaluador'));
        $form->Combobox2('','continuar',array(1=>'Reemplazar evaluador del cargo (en caso de haberlo)'),'display:none');
        $form->EndSection();
        $f=0;
        ?>
        <table class="tab bicolor_tab" width="100%" id="tabla" style="margin-top: -60px; display: none" >
            <tr>
        <?php
        if($cargos)
        {
        foreach ($cargos as $gr => $ti) {
                
            
                        
                    $form->Section();?>
                    <td colspan="100%" align="center"><?=$gr?></td><tr>
                        <?php
                    foreach($cargos[$gr] as $sb=>$no)
                    {
                        $f++;
                        $spl=  explode('_', $sb);
                        if (isset($cargos_ocup[$gr][$sb]))
                        {?>
                        <td >
                    <?php
                    
                    $form->Combobox4('', 'evalua'.$f, array($spl[0]=>$no));
                    ?>
                        </td>
                    
                        <?php
                        }else
                        {
                           ?>
                    
                        <td>
                    <?php
                    
                    $form->Combobox2('', 'evalua'.$f, array($spl[0]=>$no));
                    ?>
                        </td>
                    
                        <?php
                    }}?></tr><br /><?php
                    $form->EndSection();
            }
        }
            ?>
            
            </tr>
            </table>
            <?php
            $form->Hidden('valorf',$f);
        $form->End('Crear', 'addbtn');
        $g=0;
        ?>
        
        <table class='tab bicolor_tab' width="100%" >
            <caption>Cargos ya creados</caption>
<thead>
                    <tr>
                        <th width="65%">Cargo</th>
                        <th width="20%">CeCo</th>
                        <th width="15%">Opciones</th>
                    </tr>
                </thead>
            <?php
            if($cargos)
            {
             
            foreach ($cargos as $gr => $ti) {
                $l=0;
                ?>
                
                <?PHP
                foreach ($cargos[$gr] as $pr => $hi) {
                    $l++;
                    $g++;
                    $spt=  explode('_', $pr);
                    ?>
                <tr align='center' id="<?=$spt[1].$l?>" style="display: none" >
                        <td>
                            <?php echo $hi ?>
                        </td>
                        <td>
                            <?php echo $gr ?>
                        </td>
                        <td> 
                            <a href='<?php echo Factory::buildUrl('editar_cargo', 'rrhh', 'index', 'mantenedor',array('id_edit'=>$spt[0])) ?>' class='loadOnOverlay edita'>
                            <img src='images/editbtn.png' alt='' title='Editar Cargos' />
                        </a>
                             <a href='<?php echo Factory::buildUrl('editar_cargo', 'rrhh', 'editarNombre', 'mantenedor',array('id_edit'=>$spt[0])) ?>' class='loadOnOverlay edita'>
                            <img src='images/editbtn.png' alt='' title='Editar Nombre' />
                        </a>
                            <a href='<?php echo Factory::buildUrl('editar_cargo', 'rrhh', 'editarCeco', 'mantenedor',array('id_edit'=>$spt[0])) ?>' class='loadOnOverlay edita'>
                            <img src='images/editbtn.png' alt='' title='Editar CeCo' />
                        </a>
                        <a href='<?php echo Factory::buildUrl('eliminar_cargo','rrhh','index','mantenedor',array('id_del'=>$spt[0]))?>' class='loadOnOverlay'>
                            <img src='images/delbtn.png' alt='' title='Eliminar' />
                        </a>
                        </td>
                    </tr>
                    <?php
                }?><?php 
            }
            }
            ?>

        </table>
<input type="hidden" id="id_anterior" />
<script>
    $('#evaluador').click(function(){
        $('#continuar').slideToggle('slow');
        $('#tabla').slideToggle();
        
        
    });    

    
    
    $("#ceco").change(function() {
        var id_ceco = document.getElementById('ceco').value;
        var id_ant = document.getElementById('id_anterior').value;
        var t =<?= $g ?>;
        if(id_ant != id_ceco)
            {
                for (var i = 1; i <= t; i++)
        {
            if (document.getElementById(id_ant + i))
            {

                document.getElementById(id_ant + i).style.display = 'none';
            }

        }
                
            }
        for (var i = 1; i <= t; i++)
        {
            if (document.getElementById(id_ceco + i))
            {

                document.getElementById(id_ceco + i).style.display = 'table-row';
            }

        }
        document.getElementById('id_anterior').value=id_ceco;
    });
   </script>