
<?php
$form->Start(Factory::buildActionUrl('insertar'), 'cr_pregunta');
$form->Header("Genere pregunta");

$form->Section();
$form->text("Ingrese Pregunta", "pregunta", 1);
$form->EndSection();

$form->Section();
$form->Select('Grupo', 'grupo', $gr);
$form->EndSection();
$form->End('Crear', 'addbtn');
$t = 0;
$i = 0;
$s=0;
$l='';
?>
<table class='tab bicolor_tab' width="100%" >
    <caption>Cargos ya creados</caption>
    <thead>
        <tr>
            <th width="45%">Pregunta</th>
            <th width="20%">Cargo</th>
            <th width="20%">Grupo preguntas</th>
            <th width="15%">Opciones</th>
        </tr>
    </thead>
    <?php
    $i = 0;
    if($preguntas)
    {
    foreach ($preguntas as $id_gru => $ti) {

        $id_g = split("-", $id_gru);
        $i++;
        ?>
        <tr align='center' id="<?= $id_g[1] . $i ?>" style="display: none" >
            <?php
            foreach ($preguntas[$id_gru] as $gru => $pre) {
                $t++;
                ?>
                <td>
                    <?php echo $pre; ?>   
                </td>
                <?php
                $id_gd=$id_g[1];
                if ($cargos[$id_gd] != false)
                {
                   
                   
                   if($l != $id_gd)
                   {
                    $s=0;   
                   }
                   
                   if($s==0)
                   {
                       $s++;
                ?>
                <td rowspan="100%">
                    <?php 
                    $l=$id_gd;
                    foreach($cargos[$id_gd] as $id_cargo=>$cargo)
                    { 
                        
                    echo $cargo."</br>";
                    }
                    ?>
                </td>
                
                <td rowspan="100%">
                    <?php echo $gru; ?>
                </td><?php
                   }
                }
                ?>
                <td>  
                    <a href='<?php echo Factory::buildUrl('editar_pregunta', 'rrhh','index','mantenedor',array('id_pregunta'=>$id_g[0])); ?>' class='loadOnOverlay'>
                        <img src='images/editbtn.png' alt='' title='Editar Pregunta' />
                    </a>
                    <a href='<?php echo Factory::buildUrl('editar_pregunta', 'rrhh','Grupo','mantenedor',array('id_pregunta'=>$id_g[0],'grupo'=>$id_g[1])); ?>' class='loadOnOverlay'>
                        <img src='images/editbtn.png' alt='' title='Editar Grupo' />
                    </a>
                    <a href='<?php echo Factory::buildUrl('editar_pregunta', 'rrhh','del','mantenedor',array('id_pregunta'=>$id_g[0],'grupo'=>$id_g[1])); ?>' class='loadOnOverlay'>
                        <img src='images/delbtn.png' alt='' title='Eliminar' />
                    </a>
                </td>

                <?php
            }
            ?></tr><?php
    }
    }
    ?>

</table>
<input type="hidden" id="id_anterior" value="" />
<script type="text/javascript">
    $("#grupo").change(function() {
        var id_grupo = document.getElementById('grupo').value;
        var spl=id_grupo.split("_");
        var id_ant = document.getElementById('id_anterior').value;
        var t =<?= $t ?>;
        if(id_ant != spl[0])
            {
                for (var i = 1; i <= t; i++)
        {
            if (document.getElementById(id_ant + i))
            {

                document.getElementById(id_ant + i).style.display = 'none';
            }

        }
                
            }
        for (var i = 1; i <= t; i++)
        {
            if (document.getElementById(spl[0] + i))
            {

                document.getElementById(spl[0] + i).style.display = 'table-row';
            }

        }
        document.getElementById('id_anterior').value=spl[0];
    });
    
    $('.addbtn').click(function(){
    
//al transcurrir 3 segundos se redirige al formulario de búsqueda nuevamente
setTimeout("loadpage('<?php echo factory::buildUrl('preguntas', 'rrhh','index','mantenedor') ?>')",200);
});
</script>