
<?php
$form->Start(Factory::buildActionUrl('eliminar'), 'del_grupo');

$form->Section();

foreach ($grupo as $id => $cargo) {
    ?>

    <table class='tab bicolor_tab' width='100%'>
        <thead>
            <tr>
                <th width="40%">Grupo</th>
                <th width="30%">Preguntas Asociadas</th>
            </tr>
        </thead>
        <tr align='center'>

            <?php
            foreach ($grupo[$id] as $car => $nom) {
                ?>
                <td><?= $nom ?></td>
                <td>
                    <?php
                    foreach ($preguntas as $idpreg => $pregunta) {
                        echo $pregunta.'</br>';
                    }
                    ?>
                </td>
                    <?php
                }
                ?>
        </tr>
        <tr align='center'>
            <td colspan='2'>Al eliminar el grupo tambien se eliminaran las preguntas asociadas a este</td>
        </tr>
        <tr align='center'>
            <td colspan='2'>¿Está seguro de eliminar el grupo?</td>
        </tr>
    </table>
    <?php
    $form->Hidden('id_cargo', $id_c);
    $form->Hidden('id_grupo', $id);
}
$form->EndSection();

$form->End('Eliminar', 'delbtn');
?>