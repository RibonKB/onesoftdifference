<?php
$form->Start(Factory::buildActionUrl('delRel'), 'del_grupo_rel');

$form->Section();

foreach ($grupo as $id => $cargo) {
    ?>

    <table class='tab bicolor_tab' width='100%'>
        <thead>
            <tr>
                <th width="40%">Grupo</th>
                <th width="30%">Cargo</th>
            </tr>
        </thead>
        <tr align='center'>

            <?php
            foreach ($grupo[$id] as $car => $nom) {
                ?>
                <td><?= $nom ?></td>
                <td><?=$car?></td>
                    
                
                    <?php
                }
                ?>
        </tr>
    </table>
    <?php
    $form->Hidden('id_cargo', $id_c);
    $form->Hidden('id_grupo', $id);
}
$form->EndSection();

$form->End('Eliminar', 'delbtn');
?>
