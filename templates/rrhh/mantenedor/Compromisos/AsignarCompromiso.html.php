<?php
$form->Start(Factory::buildActionUrl('crearCompromiso'), 'cr_compromiso');
$form->Header("Asignación de compromiso");

$form->Section();
$form->text("Compromiso del funcionario", "compromiso_func", 1, 250);
$form->Select('Funcionario', 'funcionario', $funcionario, 1);
$form->EndSection();

$form->Section();
$form->text("Compromiso de la empresa", "compromiso_emp", 0, 250);
$form->Date("Fecha limite compromiso", "fecha_compromiso", 1, 1);
$form->EndSection();
$form->End('Agregar', 'addbtn');
?>

<table class='tab bicolor_tab' width='100%'>
    <caption>Compromisos del Funcionario</caption>
    <tr>
        <th>
            Funcionario
        </th>
        <th>
            Compromiso
        </th>
        <th>
            Fecha de Compromiso
        </th>
        <th>
            Cumplimiento
        </th>
        <th>
            Compromiso Empresa
        </th>
        <th>
            estado
        </th>
    </tr>
    <?php
    $i=0;
    $l=0;
    if ($compromisos_creados) {
        foreach ($compromisos_creados as $dc_user => $ll) {
           $i=0;
            foreach ($compromisos_creados[$dc_user] as $func_nombre => $rr) {
                 
                foreach ($compromisos_creados[$dc_user][$func_nombre] as $compromiso => $ss) {
                    
                    foreach ($compromisos_creados[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                        foreach ($compromisos_creados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                            foreach ($compromisos_creados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                $i++;$l++;//la $l es el contador para el script.....
                                ?>

                                
                                        <tr align='center' id="<?=$dc_user?><?=$i?>" style="display:none">
                                    <td><?= $func_nombre ?></td>
                                    <td><?= $compromiso ?></td>
                                    <td><?= $fecha_compromiso ?></td>
                                    <?php
                                    if ($cumple == 1) {
                                        if ($estado == 3) {
                                            ?>
                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                            <?php
                                        } else {
                                            ?>
                                            <td style="color: #E29B1E ; font-weight: bolder">En espera</td><?php
                                        }
                                    }
                                    if ($cumple == 2) {
                                        if ($estado == 3) {
                                            ?>
                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                            <?php
                                        } else {
                                            ?>
                                            <td style="color: #00aa00; font-weight: bolder">Si</td>
                                        <?php
                                        }
                                    }
                                    if ($cumple == 3) {
                                        if ($estado == 3) {
                                            ?>
                                            <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                            <?php
                                        } else {
                                            ?>
                                            <td style="color: maroon; font-weight: bolder" >No</td>
                                        <?php }
                                    } ?>
                                    <td><?= $compromiso_empresa ?></td>
                                    <?php if ($estado == 1) { ?>
                                        <td>En espera</td>
                                        <?php
                                    }
                                    if ($estado == 2) {
                                        ?>
                                        <td style="color: #00aa00; font-weight: bolder">Aprobado</td>
                                        <?php
                                    }
                                    if ($estado == 3) {
                                        ?>
                                        <td style="color: maroon; font-weight: bolder">Rechazado</td>
                            <?php }
                            
                            }
                        }
                    }
                }
            }
            ?>

                                </tr>

                                <?php
        }
    }
    ?>

</table>
<input type="hidden" id="id_anterior" value="" />
<script type="text/javascript">
    $('.addbtn').click(function() {

        //al transcurrir 3 segundos se redirige al formulario de búsqueda nuevamente
        setTimeout("loadpage('<?php echo factory::buildUrl('Compromisos', 'rrhh', 'index', 'mantenedor') ?>')", 1);

    });
    
    $('#funcionario').change(function(){
        
         var id_funcionario = document.getElementById('funcionario').value;
        var spl=id_funcionario.split("_");
        var id_ant = document.getElementById('id_anterior').value;
        var t =<?= $l ?>;
        
        if(id_ant != spl[0])
            {
                for (var i = 1; i <= t; i++)
        {
            if (document.getElementById(id_ant + i))
            {

                document.getElementById(id_ant + i).style.display = 'none';
            }

        }
                
            }
        for (var i = 1; i <= t; i++)
        {
            if (document.getElementById(spl[0] + i))
            {

                document.getElementById(spl[0] + i).style.display = 'table-row';
            }

        }
        document.getElementById('id_anterior').value=spl[0];
    });
    

</script>