
        <?php
        $form->Start(Factory::buildActionUrl('eliminar'), 'del_cargo');

        $form->Section();
        
        foreach ($cargo as $id => $area) {
            ?>
        
        <table class='tab bicolor_tab' width='100%'>
            <thead>
            <tr>
                <th width="50%">Cargo</th>
                <th width="50%">Area</th>
            </tr>
            </thead>
            <tr align='center'>
            
                <?php
                foreach($cargo[$id] as $ar=>$ca)
                {
                ?>
                <td><?=$ca?></td>
                <td><?=$ar?></td>
                <?php
                }
                ?>
            </tr>
            <tr align='center'>
                <td colspan='2'>¿Está seguro de eliminar el cargo?</td>
            </tr>
        </table>
            <?php
            $form->Hidden('id_cargo', $id);
        }
        $form->EndSection();
       
        $form->End('Eliminar', 'delbtn');
        ?>
