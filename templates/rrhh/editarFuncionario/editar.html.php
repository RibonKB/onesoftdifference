<div id="secc_bar">
	Edición de funcionarios
</div>
<div id="other_options">
	<ul>
		<li><a href="<?php echo Factory::buildUrl('CrearFuncionario', 'rrhh') ?>" class="loader">Creación</a></li>
		<li class="oo_active"><a href="#">Edición</a></li>
                <li><a href="<?php echo Factory::buildUrl('eliminaFuncionario', 'rrhh') ?>" class="loader">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont">
<div class="panes" align="center">
<?php
	
	$form->Header("<strong>Ingrese el RUT del funcionario que desea editar.</strong><br />(Los criterios de búsqueda son nombre del funcionario, rut del funcionario)");
	$form->Start( Factory::buildActionUrl('FormEditar'),"ed_func","cValidar");
	$form->Text("RUT funcionario","fc_rut",1,20,"","inputrut");
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[0]+" ( "+row[1]+" )";
}
$("#fc_rut").autocomplete('sites/proc/autocompleter/funcionario.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>