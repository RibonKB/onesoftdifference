<div id="secc_bar">
	Edición de funcionarios
</div>
<div id="other_options">
	<ul>
		<li><a href="<?php echo Factory::buildUrl('CrearFuncionario', 'rrhh') ?>" class="loader">Creación</a></li>
		<li class="oo_active"><a href="#">Edición</a></li>
		<li><a href="<?php echo Factory::buildUrl('eliminaFuncionario', 'rrhh') ?>" class="loader">Eliminación</a></li>
	</ul>
</div>
<div id="main_cont">
<div class="panes">
<?php
	
	$form->Header("<strong>Ingrese los datos actualizados del funcionario</strong><br />(Los campos marcados con [*] son obligatorios)");
	$form->Start(Factory::buildActionUrl('edicion'),"ed_funcionario","form","method='post' enctype='multipart/form-data'");
	$form->Section();
	$form->Text("RUT","func_rut",1,255,$datos->dg_rut,"inputrut");
	$form->Text("Apellido Paterno","func_ap_paterno",1,255,$datos->dg_ap_paterno);
	$form->Date("Fecha nacimiento","func_nacimiento",1,$datos->df_nacimiento);
	$form->EndSection();
	$form->Section();
	$form->Text("Nombres","func_nombres",1,255,$datos->dg_nombres);
	$form->Text("Apellido Materno","func_ap_materno",1,255,$datos->dg_ap_materno);
	$form->Radiobox("Sexo","func_sexo",array("Hombre","Mujer"),$datos->dm_sexo);
	$form->EndSection();
        $form->Section();
        //$form->Combobox('','compromisos',array(1=>'Autoriza compromisos'));
        $form->EndSection();
	$form->Group();
	$form->Section();
	$form->select("Centro costo","func_ceco",$ceco,1,$datos->dc_ceco);
	$form->Date("Inicio contrato","func_ini_contr",1,$datos->df_inicio_contrato);
	$form->select("Salud","func_salud",$isapre,1,$datos->dc_isapre);
	$form->Text("Sueldo bruto (\$)","func_sueldo",0,10,$datos->dq_sueldo_bruto);
	$form->Text('Anexo','func_anexo',0,20,$datos->dg_anexo);
	$form->EndSection();
	$form->Section();
	$form->select("Tipo de contrato","func_t_contrato",$tipoCont,1,$datos->dc_tipo_contrato);
	$form->Date("Término contrato","func_fin_contr",0,$datos->df_fin_contrato);
	$form->select("AFP","func_afp",$afp,1,$datos->dc_afp);
	$form->File("Foto","fc_foto");
        $form->select('Cargo','cargo',$cargos,0,$datos->dc_cargo);
	$form->EndSection();
	$form->Section();
		$form->Radiobox('Evaluación Tickets','dm_evaluacion_ticket',array('NO','SI'),$datos->dm_evaluacion_ticket);
	$form->EndSection();
	$form->Hidden("id_func",$datos->dc_funcionario);
	$form->End("Editar","editbtn");
?>
<iframe name="edfuncionario_fres" id="edfuncionario_fres" style="display:none;"></iframe>
</div>
</div>

<script type="text/javascript">
$("#ed_funcionario").submit(function(e){
	if(validarForm("#ed_funcionario")){	
		disableForm("#ed_funcionario");
	}else{
		e.preventDefault();
	}
});
$("#edfuncionario_fres").load(function(){
	res = frames['edfuncionario_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#ed_funcionario_res").html(res);
	hide_loader();
});
$("#ed_funcionario").attr("target","edfuncionario_fres");	
</script>
