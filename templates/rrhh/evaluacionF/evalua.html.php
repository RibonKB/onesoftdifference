<script>
    var t = window.setTimeout(function() {
        pymerp.showLoader()
    }, 0);

</script>

<ul id="tabs">
    <li><a href="#" class="current">Compromisos</a></li>
    <li><a href="#" >Evaluación</a></li>
</ul>   
<br class="clear" />
<br class="clear" />
<br class="clear" />
<div class="tabpanes">
    <div>
        <?php
        $form->Start(Factory::buildActionUrl('compSiNo'), 'sv_compromiso');
        ?>
        <table class='tab bicolor_tab' width='100%'>
            <caption>Compromisos de <?=$fun?></caption>
            <tr>
                <th>
                    Funcionario
                </th>
                <th>
                    Compromiso
                </th>
                <th>
                    Fecha de Compromiso
                </th>
                <th width="7%">
                    Cumplimiento
                </th>
                <th>
                    Compromiso Empresa
                </th>
            </tr>
            <?php
            $i = 0;
            if ($compromisos_creados) {
                foreach ($compromisos_creados as $dc_user => $ll) {
                    foreach ($compromisos_creados[$dc_user] as $func_nombre => $rr) {
                        foreach ($compromisos_creados[$dc_user][$func_nombre] as $compromiso => $ss) {
                            foreach ($compromisos_creados[$dc_user][$func_nombre][$compromiso] as $fecha_compromiso => $n) {
                                foreach ($compromisos_creados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso] as $compromiso_empresa => $f) {
                                    foreach ($compromisos_creados[$dc_user][$func_nombre][$compromiso][$fecha_compromiso][$compromiso_empresa] as $cumple => $estado) {
                                        ?>
                                        <tr align='center'>
                                            <td><?= $func_nombre ?></td>
                                            <td><?= $compromiso ?></td>
                                            <td><?= $fecha_compromiso ?></td>
                                            <?php
                                            if ($cumple == 1) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    $i++;
                                                    ?>
                                                    <td>
                                                        <?php
                                                        $form->Radiobox3('', 'sino' . $i, array($dc_user . '_' . $fecha_compromiso . '_2' => 'Si', $dc_user . '_' . $fecha_compromiso . '_3' => 'No'));
                                                        ?>
                                                    </td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == 2) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td style="color: #00aa00; font-weight: bolder">Si</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == 3) {
                                                if ($estado == 3) {
                                                    ?>
                                                    <td style="color: maroon ; font-weight: bolder">Rechazado</td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td style="color: maroon; font-weight: bolder" >No</td>
                                                    <?php
                                                }
                                            }
                                            if ($cumple == '--') {
                                                ?>
                                                <td><?= $cumple ?></td>
                                                <?php
                                            }
                                            ?>
                                            <td><?= $compromiso_empresa ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ?>
        </table>
        <?php
        $form->Hidden('Cant_sino', $i);
        $form->End3('Aceptar', 'addbtn');
        ?>
    </div>


    <div>
        <div id="detail_data_table">
            
            <?php
            $n = 0;
            $tp = 0;
            $t = 0;
            $k = 0;
            $ti=0;
            if ($evalua != null) {
                ?>

                <?php
                $form->Start(Factory::buildActionUrl('guardar'), 'evaluacion', 'cValidar');
                ?>
            <ul class="manual_tab" >
                    <?php
                    foreach ($evalua as $tipoPr => $hi) {
                        $ti++;
                        /*
                        if ($ti==1)
                        {
                        ?>
                        <li><a href="#" onclick="var t= window.setTimeout(function(){document.getElementById('pr_dv_2').style.display='table'},0)"><?= $tipoPr ?></a></li>
                        <?php }else{ ?>
                        <li><a href="#" class="innertabpanes" onclick="var t= window.setTimeout(function(){document.getElementById('pr_dv_2').style.display='none'},0)"><?= $tipoPr ?></a></li>
                    <?php }} */ 
                        ?>
                            <li><a href="#"><?= $tipoPr ?></a></li>
                        <?php }
                  ?>
                </ul>   
                <br class="clear" />
                <br class="clear" />
                <br class="clear" />
                <div class="innertabpanes">

                    
                    <?php
                    foreach ($evalua as $tipoPr => $hi) {
                        $tp++;
                        $t = 0;
                        if($t==0)
                        {
                        ?>
                        
                        <div id="pr_dv">
                        <?php }else{?>
                            <div>
                        <?php } ?>
                            <table align='center' class='tab bicolor_tab' id="pr_dv_2">
                                <caption style='text-transform: capitalize'>Evaluando a: <?= $fun ?> </caption>
                                <tbody>
                                    <tr align='center' class="preguntasTipo" id="<?= $tp ?>">
                                        <th align='center'>

                                            <?php
                                            $form->Header($tipoPr);
                                            ?>
                                        </th>
                                        <th>L&nbsp;&nbsp;&nbsp;&nbsp; ML&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MNL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NL</th>
                                        <th>Razon</th>
                                    </tr>

                                    <?php
                                    foreach ($evalua[$tipoPr] as $preg => $ti) {
                                        $n++;
                                        $t++;
                                        $form->Section2();
                                        ?>
                                        <tr align='left' id="<?= $tp . $t ?>" >
                                            <td>
                                                <?php
                                                echo $ti;
                                                ?>
                                            </td>
                                            <td>

                                                <?php
                                                $form->Textarea3("", "estrella" . $n);
                                                $form->Radiobox2(" ", "nota_" . $n, array('10' => 'Logrado',
                                                    '7' => 'Medianamente logrado',
                                                    '5' => 'Medianamente no logrado',
                                                    '2' => 'No logrado'), 1);
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $form->Textarea2('', 'razon' . $n,'','','',35,4);
                                                ?>
                                            </td>
                                        </tr>



                                        <?php
                                        $form->hidden('pregunta' . $n, $preg);

                                        $form->EndSection();
                                    }
                                    ?>
                                    </div>
                                    
                                    </tbody>
                            </table></div>
                        <?php
                    }
                } else {
                    
                }
                ?>
            </div>
                <table align="center">
                    <tr align="center">
                                        <td colspan="3" align="center"><?php
                                            $form->Textarea("", "comentario", '', '', 79, 8);
                                            ?>
                                        </td>
                                    </tr>
                </table>
            <?php
            $form->hidden('cant_n', $n);
            $form->hidden('dc_user', $dc_user);
            $form->hidden('dc_us_eva', $dc_user_ev);
            $form->End3('Enviar', 'addbtn');
            ?>
        </div>
    </div>
</div>
</div>

<script>
    
    function visible()
   {
       document.getElementById('pr_dv').style.display='block';
   }
   $('#ll').click(function(){
       visible();
   });
   
   var t= window.setTimeout(function(){pymerp.hideLoader()},500);
   
   
    $(".nota").click(function() {
        var valor = this.value;
        var id = this.id;
        var c = id.split("_");
        var estrella = document.getElementById('estrella' + c[1]);

        var es = '';
        var estilo = 0;
        var color = '';
        for (var i = 0; i < valor; i++) {
            es = es + "★";
            estilo = estilo + 12;
        }
        if (valor == 2)
        {
            color = "#000000";
        }
        if (valor == 5)
        {
            color = "#dd0000";
        }
        if (valor == 7)
        {
            color = "#cccc00";
        }
        if (valor == 10)
        {
            color = "#33cc00";
        }
        estrella.value = es;
        estrella.style.color = color;
        estrella.style.align = 'center';
        estrella.style.width = estilo + "px";


    });

    $(".cValidar").submit(function(event) {
        var error = '';
        var noR = '';
        var t = 0;
        var k = '';
        for (var i = 1; i <=<?= $n ?>; i++)
        {
            k = '';
            if (!$("input[name=nota_" + i + "]:checked").val())
            {
                error = 'si';
                noR = noR + '/nota_' + i;
                t++;
            }
            else
            {
                k = document.getElementById('nota_' + i).value;
            }
        }
        if (error == 'si')
        {
            var NR = noR.split('/');
            alert('existen ' + t + ' pregunta(s) sin reponder');
            event.preventDefault();
            exit();
        }
        else
        {

        }
    });

</script>
   