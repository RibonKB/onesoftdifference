<div class="title center">
    Comprobante contable para el tipo de movimiento <strong><?php echo $tipo_movimiento->dg_tipo_movimiento ?></strong>
</div>
<?php
$suma_debe=0;
    $suma_haber=0;

$form->Start(Factory::buildActionUrl('procesarCrear'), "cr_comprobante_contable");
$form->Header("<strong>Indique los datos para la generación del comprobante contable</strong><br />Los datos marcados con [*] son obligatorios");
$form->Section();
$form->Text('Número de comprobante interno', 'dg_numero_interno', 0, '', $numero_interno);
$form->Date('Fecha contable', 'df_contable_comprobante', 1,$fecha_contable);
$form->EndSection();
$form->Section();
$form->Textarea("Glosa general", "dg_glosa_general", 0, $glosa);
$form->EndSection();
$form->Group();
?>
<div class="info">
    Detalle del comprobante
</div>
<table width="100%" class="tab">
    <thead>
        <tr>
            <th width="80">Opciones</th>
            <th width="120">Cuenta Contable</th>
            <th>Descripción</th>
            <th>DEBE</th>
            <th>HABER</th>
            <th colspan="2">Glosa</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th colspan="3">&nbsp;</th>
            <th width="130">Total DEBE</th>
            <th width="130">Total HABER</th>
            <th width="130">Diferencia DEBE-HABER</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <td colspan="3"></th>
            <td align="right" id="total_debe">0</th>
            <td align="right" id="total_haber">0</th>
            <td align="right" id="diferencia_debe_haber">0</th>
            <td>&nbsp;</th>
        </tr>
    </tfoot>
   
    
    <tbody id="detalle_list">
             <?php    
             $o=0;
if ($detalle) {

    ?>
            <?php
            /*
            foreach ($detalle as $idDet => $ar1) {
                foreach ($ar1 as $ctaContableId => $ar2) {
                    foreach ($ar2 as $ctaContNombre => $ar3) {
                        foreach ($ar3 as $debe => $ar4) {
                            foreach ($ar4 as $haber => $glosa) { */
                                foreach($detalle as $det):
                                    $ids='';
                                $suma_debe=$suma_debe+$det->dq_debe;
                                $suma_haber=$suma_haber+$det->dq_haber;
                                ?>
                                <tr>
                                    <td>
                                        <img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" />
                                        <img src="images/addbtn.png" alt="+" title="Más detalles" class="show_more" />
                                        <img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
                                        <input type="hidden" name="item_id[]" class="item_id" value="<?=$o?>" />
                                        <input type="hidden" name="id_comp[]" value="<?=$comprobante?>" />
                                        <input type="hidden" class="comp" />
                       <?php foreach($analitico as $ContAn):
                           
                           if ($ContAn->dc_detalle_financiero == $det->dc_detalle):
                               if($ContAn->dc_cuenta_contable == $det->dc_cuenta_contable):
                           ?>    
                                        
            <input type="hidden" class="detalle_analitico_hidden" id="pet" name="detalle_analitico[<?=$o?>][an_dq_debe][]" value="<?=$ContAn->dq_debe?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dg_glosa][]" value="<?=$ContAn->dg_glosa?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_factura_compra][]" value="<?=$ContAn->dc_factura_compra?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_nota_venta][]" value="<?=$ContAn->dc_nota_venta?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_proveedor][]" value="<?=$ContAn->dc_proveedor?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_tipo_proveedor][]" value="<?=$ContAn->dc_tipo_proveedor?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_orden_compra][]" value="<?=$ContAn->dc_orden_compra?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_nota_credito_proveedor][]" value="<?=$ContAn->dc_nota_credito_proveedor?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_producto][]" value="<?=$ContAn->dc_producto?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_cebe][]" value="<?=$ContAn->dc_cebe?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_segmento][]" value="<?=$ContAn->dc_segmento?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_marca][]" value="<?=$ContAn->dc_marca?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_banco][]" value="<?=$ContAn->dc_banco?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dg_cheque][]" value="<?=$ContAn->dg_cheque?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_medio_pago_proveedor][]" value="<?=$ContAn->dc_medio_pago_proveedor?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_mercado_proveedor][]" value="<?=$ContAn->dc_mercado_proveedor?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dq_haber][]" value="<?=$ContAn->dq_haber?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_ejecutivo][]" value="<?=$ContAn->dc_ejecutivo?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_factura_venta][]" value="<?=$ContAn->dc_factura_venta?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_orden_servicio][]" value="<?=$ContAn->dc_orden_servicio?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_cliente][]" value="<?=$ContAn->dc_cliente?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_tipo_cliente][]" value="<?=$ContAn->dc_tipo_cliente?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_guia_recepcion][]" value="<?=$ContAn->dc_guia_recepcion?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_nota_credito_cliente][]" value="<?=$ContAn->dc_nota_credito?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_tipo_producto][]" value="<?=$ContAn->dc_tipo_producto?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_ceco][]" value="<?=$ContAn->dc_ceco?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_bodega][]" value="<?=$ContAn->dc_bodega?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_linea_negocio][]" value="<?=$ContAn->dc_linea_negocio?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_banco_cobro][]" value="<?=$ContAn->dc_banco_cobro?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_df_cheque][]" value="<?=$ContAn->df_cheque?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_medio_cobro_cliente][]" value="<?=$ContAn->dc_medio_cobro_cliente?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_mercado_cliente][]" value="<?=$ContAn->dc_mercado_cliente?>">
            <input type="hidden" class="detalle_analitico_hidden" name="detalle_analitico[<?=$o?>][an_dc_detalle][]" value="<?=$ContAn->dc_detalle?>" style="background-color: none">
            <?php
            if($ids==''){
                $ids=$ContAn->dc_detalle;
            }else{
                $ids=$ids.",".$ContAn->dc_detalle;
            }
            endif;
                endif;
            endforeach ?>    
            <input type="hidden" name="id_an_dc_detalle[<?=$o?>]" value="<?=$ContAn->dc_detalle?>" />
            <input type="hidden" name="id_detalle_comprobante[<?=$o?>]" value="<?=$det->dc_detalle?>" /> 
            <input type="hidden" name="ides_detalle_analitico[<?=$o?>]" value="<?=$ids?>" />
                                    </td>
    
                                    <td><input type="text" class="dg_codigo inputtext" readOnly="readOnly" name="codigo" size="12" required="required" value="<?= $det->dg_codigo ?>" />
                                    <input type="hidden" name="dc_cuenta_contable[]" value="<?= $det->dc_cuenta_contable ?>" />
                                    </td>
                                    <td><?= $det->dg_cuenta_contable ?></td>
                                    <td>
                                        <input type="text" name="dq_debe[]" class="dq_debe inputtext" size="16" required="required" value="<?= $det->dq_debe ?>" style="text-align:right;" />
                                    </td>
                                    <td>
                                        <input type="text" name="dq_haber[]" class="dq_haber inputtext" size="12" required="required" value="<?= $det->dq_haber ?>" style="text-align:right;" />
                                    </td>
                                    <td colspan="2"><input type="text" name="dg_glosa[]" class="dg_glosa inputtext" size="36" value="<?= $det->dg_glosa ?>" required="required" /></td>
                                </tr>
                                
                                <?php $o++;
                                endforeach; /*
                            }
                        }
                    }
                }
               
                
            }*/

}
?>
</tbody>
</table>

<br />
<?php 
$form->Hidden2('id_it',$o);
$form->Hidden2('comprobante_id',$comprobante);
$form->Group() ?>
<div class="center">
    <?php $form->Button('Agregar Detalle', 'id="add_detalle"', 'addbtn') ?>
</div>

<?php
$form->Hidden('dc_tipo_movimiento', $tipo_movimiento->dc_tipo_movimiento);
$form->End('Editar', 'editbtn');
?>

<!-- Template del detalle de comprobante -->

<table class="hidden">
    <tbody id="detalle_template">
        <tr>
            <td>
                <img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" />
                <img src="images/addbtn.png" alt="+" title="Más detalles" class="show_more" />
                <img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
                <input type="hidden" name="item_id[]" class="item_id" />
                
            </td>
            <td><input type="text" class="dg_codigo inputtext" size="12" required="required" /></td>
            <td>&nbsp;</td>
            <td>
                <input type="text" name="dq_debe[]" class="dq_debe inputtext" size="16" required="required" style="text-align:right;" />
            </td>
            <td>
                <input type="text" name="dq_haber[]" class="dq_haber inputtext" size="12" required="required" style="text-align:right;" />
            </td>
            <td colspan="2"><input type="text" name="dg_glosa[]" class="dg_glosa inputtext" size="36" required="required" /></td>
        </tr>
    </tbody>
</table>
<!-- END: Template -->

<script src="<?php echo Factory::getJavascriptUrl('cr_comprobante_contable') ?>?v=2_0_999" type="text/javascript"></script>

<script type="text/javascript">
    var debe=<?=$suma_debe ?>;
    var haber=<?=$suma_haber?>   
    var dif=debe-haber;
    document.getElementById('total_debe').innerHTML=debe;
    document.getElementById('total_haber').innerHTML=haber;
    document.getElementById('diferencia_debe_haber').innerHTML=dif;
    
    js_data.urlContabilidadAnalitica = '<?php echo Factory::buildUrl('ContabilidadAnaliticaManager', 'contabilidad') ?>';
    js_data.cuentas = <?php echo json_encode($cuentas) ?>;
    js_data.init();
    $('.dg_codigo',js_data.bodyDetail).each(function(){
        var ctacbl=$(this).next().val();
        $(this).next().remove();
        js_data.ACCuentaResult.call
    ($(this),undefined,{ 
        dg_codigo: $(this).val(), 
        dc_cuenta_contable: ctacbl, 
        dg_cuenta_contable: $(this).parent().next().text() 
                           }
    );
    });
    
    $('.comp').each(function(){
        $(this).parent().next().next().next().children('.dq_debe').attr('readOnly',true);
        $(this).parent().next().next().next().next().children('.dq_haber').attr('readOnly',true);
        
    });
    
    $('.dq_debe').click(function(){
        if(! $(this).parent().prev().prev().prev().children('.detalle_analitico_hidden').val()){
            $(this).attr('readOnly',false);
            $(this).parent().next().children('.dq_haber').attr('readOnly',false);
        }
    });
     $('.dq_haber').click(function(){
        if(! $(this).parent().prev().prev().prev().prev().children('.detalle_analitico_hidden').val()){
            $(this).attr('readOnly',false);
            $(this).parent().prev().children('.dq_debe').attr('readOnly',false);
        }
    });
    
</script>