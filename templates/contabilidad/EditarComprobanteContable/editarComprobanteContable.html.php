<div class="title center">
    Comprobante contable para el tipo de movimiento <strong><?php echo $tipo_movimiento->dg_tipo_movimiento ?></strong>
</div>
<?php
//Iniciar el formulario utilizando la clase form.
$form->Start(Factory::buildActionUrl('editarComprobanteContable'), "cr_comprobante_contable", "iFrameValidar", 'entype="multipart/form-data"');
$form->Header("<strong>Indique los datos para la generación del comprobante contable</strong><br />Los datos marcados con [*] son obligatorios");
$form->Section();
$form->Text('Número de comprobante interno', 'dg_numero_interno', 0, '', $numero_interno);
$form->Date('Fecha contable', 'df_contable_comprobante', 1,$fecha_contable);
$form->EndSection();
$form->Section();
$form->Textarea("Glosa general", "dg_glosa_general", 0, $glosa);
$form->EndSection();
$form->Section();
$form->Textarea("Motivo de edicion", "dg_motivo", 1);
$form->EndSection();
$form->Group();
?>
<div class="info">
    Detalle del comprobante
</div>
<!-- Inicio del detalle del comprobante Financiero. Se crea una tabla que contiene los detalles -->
<table width="100%" class="tab">
	<!-- Cabecera de la tabla -->
    <thead>
        <tr>
            <th width="80">Opciones</th>
            <th width="120">Cuenta Contable</th>
            <th>Descripción</th>
            <th>DEBE</th>
            <th>HABER</th>
            <th colspan="2">Glosa</th>
        </tr>
    </thead><!-- Fin de Cabecera -->
	<!-- Pie de la tabla -->
    <tfoot>
        <tr>
            <th colspan="3">&nbsp;</th>
            <th width="130">Total DEBE</th>
            <th width="130">Total HABER</th>
            <th width="130">Diferencia DEBE-HABER</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <td colspan="3"></th>
            <td align="right" id="total_debe">0</th>
            <td align="right" id="total_haber">0</th>
            <td align="right" id="diferencia_debe_haber">0</th>
            <td>&nbsp;</th>
        </tr>
    </tfoot>
	
	<tbody id="detalle_list">
		<?php
		$contadorDf = 0;
		if(!empty($detalleFinanciero)):
			foreach($detalleFinanciero as $df):
		?>
		<tr>
            <td>
                <img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" />
                <img src="images/editbtn.png" alt="+" title="Más detalles" class="show_more" />
                <img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
                <input type="hidden" name="item_id[]" class="item_id" value="<?php echo $contadorDf; ?>" />
				<input type="hidden" name="id_comp[]" value="<?php echo $comprobante; ?>" />
				<input type="hidden" class="comp" />
                <?php
				foreach($detalleAnalitico as $da):
					if($da->dc_detalle_financiero == $df->dc_detalle):
						foreach($da as $c => $v):
				?>
				<input type="hidden" class="detalle_analitico_hidden an_<?php echo $c; ?>" name="detalle_analitico[<?php echo $contadorDf; ?>][an_<?php echo $c; ?>][]" value="<?php echo $v; ?>">
						<?php
						endforeach;
						?>
						<input type="hidden" class="id_detalle_comprobante" name="id_detalle_comprobante[<?php echo $contadorDf; ?>]" value="<?=$df->dc_detalle; ?>" />
				<?php
					endif;
				endforeach;
				?>
            </td>
            <td>
				<input type="text" class="dg_codigo inputtext" size="12" required="required" name="codigo" readOnly="readOnly" value="<?php echo $df->dg_codigo; ?>" />
				<input type="hidden" name="dc_cuenta_contable[]" value="<?php echo $df->dc_cuenta_contable ?>" />
			</td>
            <td><?php echo $df->dg_cuenta_contable; ?></td>
            <td>
                <input type="text" name="dq_debe[]" class="dq_debe inputtext" size="16" required="required" style="text-align:right;" value="<?php echo $df->dq_debe; ?>" />
            </td>
            <td>
                <input type="text" name="dq_haber[]" class="dq_haber inputtext" size="12" required="required" style="text-align:right;" value="<?php echo $df->dq_haber; ?>" />
            </td>
            <td colspan="2"><input type="text" name="dg_glosa[]" class="dg_glosa inputtext" size="36" required="required" value="<?php echo $df->dg_glosa; ?>" /></td>
        </tr>
<?php
				$contadorDf++;
			endforeach;
		endif;
?>
	</tbody>	
</table>
<div class="hidden" id="removedElements">
</div>
<br />
<?php
$form->Hidden2('id_it',$contadorDf);
$form->Hidden2('dc_comprobante',$comprobante);
$form->Group();
?>
<div class="center">
    <?php $form->Button('Agregar Detalle', 'id="add_detalle"', 'addbtn') ?>
</div>

<?php
$form->Hidden('dc_tipo_movimiento', $tipo_movimiento->dc_tipo_movimiento);
$form->End('Editar', 'addbtn');
?>

<!-- Template del detalle de comprobante -->
<table class="hidden">
    <tbody id="detalle_template">
        <tr>
            <td>
                <img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" />
                <img src="images/addbtn.png" alt="+" title="Más detalles" class="show_more" />
                <img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
                <input type="hidden" name="item_id[]" class="item_id" />
                
            </td>
            <td><input type="text" class="dg_codigo inputtext" size="12" required="required" /></td>
            <td>&nbsp;</td>
            <td>
                <input type="text" name="dq_debe[]" class="dq_debe inputtext" size="16" required="required" style="text-align:right;" />
            </td>
            <td>
                <input type="text" name="dq_haber[]" class="dq_haber inputtext" size="12" required="required" style="text-align:right;" />
            </td>
            <td colspan="2"><input type="text" name="dg_glosa[]" class="dg_glosa inputtext" size="36" required="required" /></td>
        </tr>
    </tbody>
</table>
<!-- Fin del template de detalle de comprobante -->
<!-- Input para la inclusión de detalles eliminados -->
<input type="hidden" name="del_financiero[]" class="del_financiero" id="removeDetalleFinancieroTemplate" />
<input type="hidden" name="del_analitico[]" class="del_analitico" id="removeDetalleAnaliticoTemplate" />
<!-- END: Template -->
<script src="<?php echo Factory::getJavascriptUrl('cr_comprobante_contable') ?>?v=2_1" type="text/javascript"></script>
<script src="<?php echo Factory::getJavascriptUrl('editarComprobanteContable') ?>?v=2_0_999" type="text/javascript"></script>
<script>
		js_data.urlContabilidadAnalitica = '<?php echo Factory::buildUrl('ContabilidadAnaliticaManager', 'contabilidad') ?>';
		js_data.cuentas = <?php echo json_encode($cuentas) ?>;
		editarComprobanteContable.init();
</script>
