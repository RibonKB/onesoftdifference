<div id="show_informe_existencias"></diV>
<div id="show_movimientos_pendientes"></div>
<?php
  $form->Start(self::buildActionUrl('showInforme'), 'src_informe_existencias','existencias_validar');
    $form->Header("Indique los parametros de filtro para mostrar el informe de existencias<br />
    <strong>Los campos marcados con [*] son obligatorios</strong>");
?>

<table class="tab" width="100%">
  <tbody>
    <tr>
      <th>Periodo</th>
      <td><?php $form->Select("Mes","dc_mes",$meses,1,date('m',strtotime("now - 1 month"))); ?></td>
      <td><?php $form->Text("Año","dc_anho",1,4,date('Y',strtotime("now - 1 month"))); ?></td>
    </tr>
    <tr>
      <th>Bodega <?php echo form::MANDATORY_ICON ?></th>
      <td colspan="2">
        <?php $form->DBSelect("","dc_bodega","tb_bodega",array('dc_bodega','dg_bodega'),1) ?>
      </td>
    </tr>
  </tbody>
</table>

<?php $form->End("Ejecutar Consulta",'searchbtn'); ?>
<script src="<?php echo Factory::getJavascriptUrl('main') ?>"></script>
<script type="text/javascript">
  js_data.tipoOperacionFechasURL = "<?php echo Factory::buildUrl("TipoOperacionFechas","contabilidad","index","informe_existencias") ?>";
  js_data.init();
</script>
