<?php include "botonera.html.php" ?>
<table class="tab">
  <thead>
    <tr>
      <th>Codigo</th>
      <th>Producto</th>
      <th>Marca</th>
      <th>Cantidad</th>
      <th>PMP</th>
      <th>Valor en Bodega</th>
    </tr>
  </thead>
  <tbody>
    <?php if(empty($productos)): ?>
      <tr>
        <td colspan="6">
          <?php $this->getErrorMan()->showAviso("No existen productos en existencias en el periodo seleccionado"); ?>
        </td>
      </tr>
    <?php else: ?>
      <?php foreach($productos as $dc_producto => $d): ?>
        <tr>
          <td><?php echo $d->dg_codigo ?></td>
          <td><?php echo $d->dg_producto ?></td>
          <td><?php echo $d->dg_marca ?></td>
          <td><?php echo $d->dc_cantidad ?></td>
          <td><?php echo $d->dq_pmp ?></td>
          <td><?php echo $d->dq_valor ?></td>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>
