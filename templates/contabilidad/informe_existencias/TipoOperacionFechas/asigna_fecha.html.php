<div class="panes">
	<div class="center">
		<?php
        $form->start(Factory::buildActionUrl('asignaFechaManual'),'src_asigna_fecha');
        $form->Header('Indique la fecha correspondiente al movimiento');
		$form->Date('Fecha del movimiento del producto','df_movimiento',0 );
        $form->Hidden('dc_movimiento',$dc_movimiento);
		$form->End('Asignar Fecha Manual', 'checkbtn btn_asigna_fecha');
        ?>
	</div>
</div>
