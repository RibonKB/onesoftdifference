<?php
    if(count($movimientosSinFecha) > 0):
?>
<table class="tab">
    <thead>
        <tr>
            <th><img src="images/cal.png" alt="Asignar Fecha de movimiento" title="Asignar Fecha de movimiento"></th>
            <th>Codigo Producto</th>
            <th>Cliente</th>
            <th>Proveedor</th>
            <th>Movimiento</th>
            <th>Bodega de Entrada</th>
            <th>Bodega de Salida</th>
            <th>Monto</th>
            <th>Cantidad</th>
            <th>Documentos Relacionados</th>
            
        </tr>
    </thead>
<?php 
    foreach($movimientosSinFecha as $msf):
?>
    <tbody>
        <tr>
            <td><img src="images/cal.png" alt="Asignar Fecha de movimiento" title="Asignar Fecha de movimiento" 
            class="asigna_fecha_movimiento" data-movimiento="<?php echo $msf->id; ?>" ></td>
            <td><?php echo $msf->producto->dg_codigo; ?></td>
            <td><?php echo $msf->cliente->dg_razon; ?></td>
            <td><?php echo $msf->proveedor->dg_razon; ?></td>
            <td><?php echo $msf->movimiento->dg_tipo_movimiento; ?></td>
            <td><?php echo $msf->bodegaEntrada->dg_bodega; ?></td>
            <td><?php echo $msf->bodegaSalida->dg_bodega; ?></td>
            <td><?php echo $msf->monto; ?></td>
            <td><?php echo $msf->cantidad; ?></td>
            <td>
            <?php 
                foreach($msf->documentos as $name => $doc){
                    $imprimeTitulo = true;
                    if($name == 'series'){
                        echo '<br /><strong>'.ucwords(strtr($name, '_', ' ')).': </strong>';
                        echo $doc . ';<br />';
                        continue;
                    }
                    foreach($doc as $e => $v){
                        if($imprimeTitulo && !empty($v)){
                            echo '<strong>'.ucwords(strtr($name, '_', ' ')).': </strong>';
                        }
                        $imprimeTitulo = false;
                        if(substr($e, 0, 2) == 'dc' || empty($v)){
                            continue;
                        }
                        echo ucwords(strtr(substr($e, 3), '_', ' ')).': ';
                        echo $v . '; ';
                    }
                    echo $imprimeTitulo ? '<br />':'';
                }
            ?>
            </td>
        </tr>
    </tbody>
<?php    
    endforeach; 
?>
</table>
<script type="text/javascript" >
    js_data.disableCierre();
    var urlAsignarFecha = '<?php echo Factory::buildActionUrl('showAsignarFecha'); ?>';
</script>
<script type="text/javascript" src="<?php echo Factory::getJavascriptUrl('fechas_pendientes'); ?>" ></script>
<script type="text/javascript" >
    js_data.evento_calendario();
</script>
<?php    
    endif;