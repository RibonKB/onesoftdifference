<?php
  $form->Start(Factory::buildActionUrl('procesarBusqueda'),'src_centralizacion_compras');
    $form->Header("Indique los datos de filtro para obtener las Facturas de compra incluídas en la Centralización.<br />".
                  "<strong>Los campos marcados con [*] son obligatorios.</strong>");
?>

<table class="tab" width="100%">
  <tbody>
    <tr>
      <td width="150">
        Periodo
      </td>
      <td>
        <?php $form->Select('Mes','dc_mes',$periodo,true,$periodoMesDefault) ?>
      </td>
      <td>
        <?php $form->Text('Año','dc_anho',true,4,$periodoAnhoDefault) ?>
      </td>
    </tr>
    <tr>
      <td>
        Proveedor
      </td>
      <td colspan="2">
        <?php $form->DBMultiSelect("",'dc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon')) ?>
      </td>
    </tr>
    <tr>
      <td>
        Ejecutivo
      </td>
      <td colspan="2">
        <?php $form->DBMultiSelect("",'dc_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno')) ?>
      </td>
    </tr>
  </tbody>
</table>

<?php $form->End('Ejecutar consulta','searchbtn'); ?>
<script type="text/javascript">
  $('#dc_proveedor, #dc_ejecutivo').multiSelect({
    selectAll: true,
    selectAllText: "Seleccionar todos",
    noneSelected: "---",
    oneOrMoreSelected: "% seleccionado(s)"
  });
</script>
