<table class="tab bicolor_tab" width="100%" id="tabla_analitico">
  <thead>
    <tr>
      <th>Código</th>
      <th>Cuenta Contable</th>
      <th>DEBE</th>
      <th>HABER</th>
      <th>Glosa</th>
      <th>Proveedor</th>
      <th>Producto</th>
      <th>Cliente</th>
      <th>Ejecutivo</th>
    </tr>
  </thead>
  <tbody>
  <?php
  foreach($fc_analiticos as $i => $d):
    foreach($d as $por_cuenta):
      foreach($por_cuenta as $a):
        include __DIR__.'/detalleAnalitico.html.php';
      endforeach;
    endforeach;

    foreach($nc_analiticos[$i] as $por_cuenta):
      foreach($por_cuenta as $a):
        include __DIR__.'/detalleAnalitico.html.php';
      endforeach;
    endforeach;

  endforeach;
  ?>
  </tbody>
  <tfoot>
    <tr>
      <th colspan="2">Saldo</th>
      <th align="right"><?php echo Functions::monedaLocal($saldo_debe) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($saldo_haber) ?></th>
      <th align="right" colspan="5"><small>Diferencia: <?php echo Functions::monedaLocal($saldo_debe-$saldo_haber) ?></small></th>
    </tr>
  </tfoot>
</table>
