<?php
  $form->Start($this->buildActionUrl('centralizar'),'confirma_centralizacion_form');
    $form->Header('Confirme los datos del comprobante contable que será emitido para la centralización'.
                  '<br /><strong>Los campos marcados con [*] son obligatorios.</strong>');

    $form->Section();
      $form->Date('Fecha Contable','df_fecha_contable',true,0);
    $form->EndSection();

  $form->Group();

  foreach(Factory::getRequest()->dc_documento as $tipo => $documentos):
    foreach($documentos as $d):
      $form->Hidden("dc_documento[{$tipo}][]",$d);
    endforeach;
  endforeach;

  $form->End('Confirmar emisión de Comprobante Contable','addbtn');
?>
