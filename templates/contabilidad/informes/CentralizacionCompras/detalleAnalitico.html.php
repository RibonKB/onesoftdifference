<tr>
  <td><strong><?php echo $a->cuenta->dg_codigo ?></strong></td>
  <td><strong><?php echo $a->cuenta->dg_cuenta_contable ?></strong></td>
  <td><?php echo Functions::monedaLocal($a->dq_debe) ?></td>
  <td><?php echo Functions::monedaLocal($a->dq_haber) ?></td>
  <td><?php echo $a->dg_glosa ?></td>
  <td><?php echo $a->proveedor->dg_razon ?></td>
  <td><?php echo $a->producto->dg_producto ?></td>
  <td>
    <?php
      if(isset($a->cliente)):
        echo $a->cliente->dg_razon;
      endif;
    ?>
  </td>
  <td>
    <?php
      if(isset($a->ejecutivo)):
        echo $a->ejecutivo->dg_nombres . ' ' . $a->ejecutivo->dg_ap_paterno . ' ' . $a->ejecutivo->dg_ap_materno;
      endif;
    ?>
  </td>
</tr>
