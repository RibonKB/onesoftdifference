<table class="tab bicolor_tab" width="100%" id="tabla_financiero">
  <thead>
    <tr>
      <th>Código</th>
      <th>Cuenta Contable</th>
      <th>DEBE</th>
      <th>HABER</th>
      <th>Glosa</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $saldo_debe = $saldo_haber = 0;
    foreach($financieros as $d):
      foreach($d as $a):
        $saldo_debe  += $a->dq_debe;
        $saldo_haber += $a->dq_haber;
        if($a->dq_debe > 0):
    ?>
        <tr>
          <td><strong><?php echo $a->cuenta->dg_codigo ?></strong></td>
          <td><strong><?php echo $a->cuenta->dg_cuenta_contable ?></strong></td>
          <td align="right"><?php echo Functions::monedaLocal($a->dq_debe) ?></td>
          <td align="right"><?php echo Functions::monedaLocal(0) ?></td>
          <td><?php echo $a->dg_glosa ?></td>
        </tr>
    <?php
      endif;
      if($a->dq_haber > 0):
    ?>
        <tr>
          <td><strong><?php echo $a->cuenta->dg_codigo ?></strong></td>
          <td><strong><?php echo $a->cuenta->dg_cuenta_contable ?></strong></td>
          <td align="right"><?php echo Functions::monedaLocal(0) ?></td>
          <td align="right"><?php echo Functions::monedaLocal($a->dq_haber) ?></td>
          <td><?php echo $a->dg_glosa ?></td>
        </tr>
    <?php
      endif;
      endforeach;
    endforeach; ?>
  </tbody>
  <tfoot>
    <tr>
      <th colspan="2">Saldo</th>
      <th align="right"><?php echo Functions::monedaLocal($saldo_debe) ?></th>
      <th align="right"><?php echo Functions::monedaLocal($saldo_haber) ?></th>
      <th align="right"><small>Diferencia: <?php echo Functions::monedaLocal($saldo_debe-$saldo_haber) ?></small></th>
    </tr>
  </tfoot>
</table>
