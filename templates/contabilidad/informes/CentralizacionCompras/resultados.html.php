<div class="right">
	<button id="btn_centralizar" class="imgbtn" style="background-image:url(images/management.png)">Centralizar Selección</button>
</div>
<table class="tab" width="100%" id="grid_centralizacion">
	<thead>
    	<tr>
        	<th><input type="checkbox" id="select_all_chbx" /></th>
        	<th>Documento</th>
            <th>Folio</th>
            <th>Tipo</th>
            <th>Fecha Emisión</th>
            <th>Proveedor</th>
            <th>Exento</th>
            <th>Neto</th>
            <th>IVA</th>
            <th>Total</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
        	<th align="right" colspan="6">Totales</th>
            <th align="right"><?php echo Functions::monedaLocal($total_exento) ?></th>
            <th align="right"><?php echo Functions::monedaLocal($total_neto) ?></th>
            <th align="right"><?php echo Functions::monedaLocal($total_iva) ?></th>
            <th align="right"><?php echo Functions::monedaLocal($total) ?></th>
        </tr>
    </tfoot>
    <tbody id="tbody_list">
    <?php foreach($data as $doc): ?>
    <?php if($doc->tp === 'NC'): ?>
    	<tr style="color:#F00;">
    <?php else: ?>
    	<tr>
    <?php endif; ?>
        	<td><input type="checkbox" name="dc_documento[<?php echo $doc->tp ?>][]" value="<?php echo $doc->dc_documento ?>" /></td>
        	<td><b><?php echo $doc->dq_factura ?></b></td>
            <td><b><?php echo $doc->dq_folio ?></b></td>
            <td><?php echo $doc->dg_tipo ?></td>
            <td><?php echo $this->getConnection()->dateLocalFormat($doc->df_emision) ?></td>
            <td><?php echo $doc->dg_razon ?></td>
            <td align="right"><b><?php echo Functions::monedaLocal($doc->dq_exento) ?></b></td>
            <td align="right"><b><?php echo Functions::monedaLocal($doc->dq_neto) ?></b></td>
            <td align="right"><b><?php echo Functions::monedaLocal($doc->dq_iva) ?></b></td>
            <td align="right"><b><?php echo Functions::monedaLocal($doc->dq_total) ?></b></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript" src="jscripts/sites/contabilidad/src_centralizacion_compra.js?v=2_0a"></script>
<script type="text/javascript">
	<?php //js_data.tipo_centralizacion = <?php echo $tipo_centralizacion; ?>
  js_data.urlCentralizacion = "<?php echo Factory::buildActionUrl('centralizar',array('dc_mes' => $mes, 'dc_anho' => $anho)) ?>";
	js_data.init();
</script>
