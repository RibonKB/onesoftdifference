<?php echo $form ?>
<div class="info">
  Comprobante contable Preliminar Centralización de Compras
</div>
<ul id="tabs">
	<li><a href="#">Detalle Financiero</a></li>
    <li><a href="#">Detalle Analítico</a></li>
</ul>
<br class="clear" />
<div class="tabpanes">
<div>
  <?php include(__DIR__.'/detallesFinancierosPreliminares.html.php') ?>
</div>
<div style="max-width: 850px;">
  <?php include(__DIR__.'/detallesAnaliticosPreliminares.html.php') ?>
</div>
</div>
<script type="text/javascript">
window.setTimeout(function(){
  $('#tabla_analitico')
    .tableExport()
    .tableOverflow([false,false,80,80,200,100,100,100,100,100,100,100,100,100])
    .tableAdjust(7,undefined,800);

  $('#tabla_financiero')
    .tableExport()
    .tableAdjust(7,undefined,800);
},100);
</script>
