<?php
$form->Start('#','AM_formContabilidadAnalitica','analitics_form_class');
$form->Header('Indique los datos extra del detalle para ser asignado como contabilidad analítica');
  $form->Button('Agregar Detalle Analítico','id="AM_addDetalle"','addbtn');
  $form->Group('id="AM_detalleAnaliticoContainer" class="hidden"');
  $form->Group();
$form->End('Asignar','addbtn');
?>

<div class="hidden" id="AM_DetailTemplate">
  <fieldset class="AM_DetailItem">
    <div style="background-color: #EEE;border:1px solid #999;">
      <i style="color:#666;line-height: 30px;padding:0 3px" class="AM_glosaContainer">Sin Glosa</i>
      <span class="right button AM_DeleteItem">X</span>
      <span class="right button AM_HideItem">_</span>
      <br class="clear" />
    </div>
    <div>
<?php
   $form->Section();
      $form->Text('DEBE','an_dq_debe[]',true,Form::DEFAULT_TEXT_LENGTH,'','an_dq_debe inputtext');
      $form->Text('Glosa','an_dg_glosa[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dg_glosa inputtext');
      $form->Text('Factura Compra','an_dq_factura_compra[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dq_factura_compra inputtext');
      $form->Hidden('an_dc_factura_compra[]',0);
      $form->Text('Nota de Venta','an_dq_nota_venta[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dq_nota_venta inputtext');
      $form->Hidden('an_dc_nota_venta[]',0);
      $form->DBSelect('Proveedor','an_dc_proveedor[]','tb_proveedor',array('dc_proveedor','dg_razon'));
      $form->DBSelect('Tipo Proveedor','an_dc_tipo_proveedor[]','tb_tipo_proveedor',array('dc_tipo_proveedor','dg_tipo_proveedor'));
      $form->Text('Orden de Compra','an_dq_orden_compra[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dq_orden_compra inputtext');
      $form->Hidden('an_dc_orden_compra[]',0);
      $form->Text('Nota de Crédito Proveedor','an_dq_nota_credito_proveedor[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_nota_credito_proveedor inputtext');
      $form->Hidden('an_dc_nota_credito_proveedor[]',0);
      $form->Text('Producto','an_dg_producto[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dg_producto inputtext');
      $form->Hidden('an_dc_producto[]',0);
      $form->DBSelect('Centro de Beneficio','an_dc_cebe[]','tb_cebe',array('dc_cebe','dg_cebe'));
      $form->DBSelect('Segmento','an_dc_segmento[]','tb_segmento',array('dc_segmento','dg_segmento'));
      $form->DBSelect('Marca','an_dc_marca[]','tb_marca',array('dc_marca','dg_marca'));
      $form->DBSelect('Banco Interno','an_dc_banco[]','tb_banco',array('dc_banco','dg_banco'));
      $form->Text('Cheque','an_dg_cheque[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dg_cheque inputtext');
      $form->DBSelect('Medio Pago Proveedor','an_dc_medio_pago_proveedor[]','tb_medio_pago_proveedor',array('dc_medio_pago','dg_medio_pago'));
      $form->DBSelect('Mercado Proveedor','an_dc_mercado_proveedor[]','tb_mercado',array('dc_mercado','dg_mercado'));
   $form->EndSection();
   
   $form->Section();
      $form->Text('HABER','an_dq_haber[]',true,Form::DEFAULT_TEXT_LENGTH,'','an_dq_haber inputtext');
      $form->DBSelect('Ejecutivo','an_dc_ejecutivo[]','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
      $form->Text('Factura Venta','an_dq_factura_venta[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dq_factura_venta inputtext');
      $form->Hidden('an_dc_factura_venta[]',0);
      $form->Text('Orden de Servicio','an_dq_orden_servicio[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dq_orden_servicio inputtext');
      $form->Hidden('an_dc_orden_servicio[]',0);
      $form->DBSelect('Cliente','an_dc_cliente[]','tb_cliente',array('dc_cliente','dg_razon'));
      $form->DBSelect('Tipo Cliente','an_dc_tipo_cliente[]','tb_tipo_cliente',array('dc_tipo_cliente','dg_tipo_cliente'));
      $form->Text('Guía de Recepción','an_dq_guia_recepcion[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dq_guia_recepcion inputtext');
      $form->Hidden('an_dc_guia_recepcion[]',0);
      $form->Text('Nota de Crédito Cliente','an_dq_nota_credito_cliente[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_nota_credito_cliente inputtext');
      $form->Hidden('an_dc_nota_credito_cliente[]',0);
      $form->DBSelect('Tipo de Producto','an_dc_tipo_producto[]','tb_tipo_producto',array('dc_tipo_producto','dg_tipo_producto'));
      $form->DBSelect('Centro de Costo','an_dc_ceco[]','tb_ceco',array('dc_ceco','dg_ceco'));
      $form->DBSelect('Bodega','an_dc_bodega[]','tb_bodega',array('dc_bodega','dg_bodega'));
      $form->DBSelect('Linea de Negocio','an_dc_linea_negocio[]','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio'));
      $form->DBSelect('Banco Externo','an_dc_banco_cobro[]','tb_banco_cobro',array('dc_banco','dg_banco'));
      $form->Date('Fecha Cheque','an_df_cheque[]',false,'','an_df_cheque inputtext');
      $form->DBSelect('Medio Pago Cliente','an_dc_medio_cobro_cliente[]','tb_medio_cobro_cliente',array('dc_medio_cobro','dg_medio_cobro'));
      $form->DBSelect('Mercado Cliente','an_dc_mercado_cliente[]','tb_mercado',array('dc_mercado','dg_mercado'));
      $form->Text('','an_dc_detalle[]',false,Form::DEFAULT_TEXT_LENGTH,'','an_dc_detalle inputtext',41,0,"display:none");
   $form->EndSection();

?>
  </div>
  </fieldset>
</div>
<script src="<?php echo Factory::getJavascriptUrl('src_contabilidad_analitica_manager') ?>?v=2_0_3" type="text/javascript"></script>
<script type="text/javascript">
  js_data.AM_init();
  js_data.AM_AutocompleteParams = {
            an_dq_factura_compra: '<?php echo Factory::buildActionUrl('acFacturaCompra') ?>',
            an_dq_nota_venta: '<?php echo Factory::buildActionUrl('acNotaVenta') ?>',
            an_dq_orden_compra: '<?php echo Factory::buildActionUrl('acOrdenCompra') ?>',
            an_nota_credito_proveedor: '<?php echo Factory::buildActionUrl('acNotaCreditoProveedor') ?>',
            an_dg_producto: '<?php echo Factory::buildActionUrl('acProducto') ?>',
            an_dq_factura_venta: '<?php echo Factory::buildActionUrl('acFacturaVenta') ?>',
            an_dq_orden_servicio: '<?php echo Factory::buildActionUrl('acOrdenServicio') ?>',
            an_dq_guia_recepcion: '<?php echo Factory::buildActionUrl('acGuiaRecepcion') ?>',
            an_nota_credito_cliente: '<?php echo Factory::buildActionUrl('acNotaCreditoCliente') ?>'
  };
  js_data.AM_selectedValues = <?php echo json_encode($selected) ?>;
  js_data.AM_nameMapping = <?php echo json_encode($mapping) ?>;
  window.setTimeout(function(){
    js_data.AM_setSelectedValues();
  },300);
</script>