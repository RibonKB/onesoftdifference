<div class="secc_bar">Autorizar comprobante contable para su edición</div>
<div class="panes">
<?php
	$form->Start(Factory::buildActionUrl('autorizar'), "autorizarEdicion");
	?>
	<fieldset>
	<div class="center alert">
		¿Está seguro de querer autorizar la modificación del comprobante <strong><?php echo $dg_comprobante; ?></strong>?<br />
		El comprobante será nuevamente bloqueado para edición luego de que este sea editado
	</div>
	</fieldset>
<?php
	$form->Hidden('dc_comprobante', $dc_comprobante);
	$form->End('Autorizar Edición', 'checkbtn');
?>
</div>