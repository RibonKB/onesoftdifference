<div class="title center">
  Comprobante contable para el tipo de movimiento <strong><?php echo $tipo_movimiento->dg_tipo_movimiento ?></strong>
</div>
<?php
$form->Start(self::buildActionUrl('procesarCrear'),"cr_comprobante_contable");	
	$form->Header("<strong>Indique los datos para la generación del comprobante contable</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();
      $form->Text('Número de comprobante interno','dg_numero_interno');
      $form->Date('Fecha contable','df_contable_comprobante',1,date('d/m/Y'));
	$form->EndSection();
  $form->Section();
	$form->Textarea("Glosa general","dg_glosa_general");
  $form->EndSection();
$form->Group();
?>
    <div class="info">
      Detalle del comprobante
    </div>
  <table width="100%" class="tab">
    <thead>
      <tr>
        <th width="80">Opciones</th>
        <th width="120">Cuenta Contable</th>
        <th>Descripción</th>
        <th>DEBE</th>
        <th>HABER</th>
        <th colspan="2">Glosa</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th colspan="3">&nbsp;</th>
        <th width="130">Total DEBE</th>
        <th width="130">Total HABER</th>
        <th width="130">Diferencia DEBE-HABER</th>
        <th>&nbsp;</th>
      </tr>
      <tr>
        <td colspan="3"></th>
        <td align="right" id="total_debe">0</th>
        <td align="right" id="total_haber">0</th>
        <td align="right" id="diferencia_debe_haber">0</th>
        <td>&nbsp;</th>
      </tr>
    </tfoot>
    <tbody id="detalle_list">
      <tr id="empty_detalle">
        <td colspan="7">
          <div class="center">No ha agregado ningún detalle al comprobante</div>
        </td>
      </tr>
    </tbody>
  </table>

<br />
<?php $form->Group() ?>
<div class="center">
  <?php $form->Button('Agregar Detalle','id="add_detalle"','addbtn') ?>
</div>

<?php
$form->Hidden2('id_it',0);
  $form->Hidden('dc_tipo_movimiento',$tipo_movimiento->dc_tipo_movimiento);
  $form->End('Crear','addbtn');
?>

<!-- Template del detalle de comprobante -->
<table class="hidden">
  <tbody id="detalle_template">
    <tr>
      <td>
		<img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" />
		<img src="images/addbtn.png" alt="+" title="Más detalles" class="show_more" />
		<img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
        <input type="hidden" name="item_id[]" class="item_id" />
      </td>
      <td><input type="text" class="dg_codigo inputtext" size="12" required="required" /></td>
      <td>&nbsp;</td>
      <td>
        <input type="text" name="dq_debe[]" class="dq_debe inputtext" size="16" required="required" style="text-align:right;" />
      </td>
      <td>
        <input type="text" name="dq_haber[]" class="dq_haber inputtext" size="12" required="required" style="text-align:right;" />
      </td>
      <td colspan="2"><input type="text" name="dg_glosa[]" class="dg_glosa inputtext" size="36" required="required" /></td>
    </tr>
  </tbody>
</table>
<!-- END: Template -->

<script src="<?php echo Factory::getJavascriptUrl('cr_comprobante_contable') ?>?v=2_0_35" type="text/javascript"></script>
<script type="text/javascript">
  js_data.urlContabilidadAnalitica = '<?php echo Factory::buildUrl('ContabilidadAnaliticaManager', 'contabilidad') ?>';
  js_data.cuentas = <?php echo json_encode($cuentas) ?>;
  js_data.init();
</script>