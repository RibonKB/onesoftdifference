<div class="center">
<?php
$form->Start(self::buildActionUrl('crear'),"cr_comprobante_contable","cValidar");
$form->Header("<strong>Indique el código del tipo de movimiento que afectará el comprobante contable</strong>
		<br />Los criterios de busqueda son el código, nombre o descripción del tipo de movimiento");
	$form->Text("Código tipo de movimiento","dg_codigo_tipo_movimiento",1);
$form->End("Buscar","searchbtn");
?>
</div>
<script type="text/javascript">
$("#dg_codigo_tipo_movimiento").autocomplete('<?php echo self::buildActionUrl('tipoMovimientoAutoCompletar') ?>',{
	formatItem: function(row){return "<span title='"+row[2]+"'><b>"+row[0]+"</b> "+row[1]+"</span>"},
	minChars: 2,
	width:300
});
</script>