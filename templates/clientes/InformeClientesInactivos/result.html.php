<table class="tab" width="100%" id="tb_clientes_inactivos">
	<thead>
		<tr>
			<th>Cliente</th>
			<th>Ejecutivo</th>
			<th>Última Cotización</th>
			<th>Cotización</th>
			<th>Última llamada</th>
			<th>Destino</th>
			<th>Origen</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($clientes as $c): ?>
		<tr>
			<td><?php echo $c->dg_razon ?></td>
			
			<?php if(isset($c->dg_nombres)): ?>
			<td><?php echo $c->dg_nombres.' '.$c->dg_ap_paterno.' '.$c->dg_ap_materno ?></td>
			<?php else: ?>
			<td>-</td>
			<?php endif; ?>
			
			<?php if($c->cotizacion != false): ?>
			<td><?php echo $c->cotizacion->df_fecha_emision->format("d-m-Y") ?></td>
			<td><?php echo $c->cotizacion->dq_cotizacion ?></td>
			<?php else: ?>
			<td colspan="2"><i>No registra cotizaciones</i></td>
			<?php endif; ?>
			
			<?php if($c->ultimo_llamado != false): ?>
			<td><?php echo $c->ultimo_llamado->calldate->format('d-m-Y') ?></td>
			<td><?php echo $c->ultimo_llamado->dst  ?></td>
			<td><?php echo $c->ultimo_llamado->src ?></td>
			<?php else: ?>
			<td colspan="3"><i>Cliente no registra llamados</i></td>
			<?php endif; ?>
			
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">
	$("#tb_clientes_inactivos").tableExport().tableAdjust(20);
</script>