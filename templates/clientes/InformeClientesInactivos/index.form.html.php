<?php
	$form->Start(Factory::buildActionUrl('verInforme'),'src_inf_clientes_inactivos');
	$form->Header('<strong>Indique los filtros para realizar la búsqueda de clientes inactivos</strong><br />Los datos marcados con [*] son obligatorios');
?>

<table class="tab" width="100%">
	<tbody>
		<tr>
			<td>Periodo</td>
			<td>
				<?php $form->Text('','dq_periodo',true,2,InformeClientesInactivosFactory::PERIODO_DEFAULT, Form::DEFAULT_INPUT_CLASS,5); ?>
				Meses
			</td>
		</tr>
		<tr>
			<td>Ejecutivo asociado</td>
			<td>
				<?php $form->Section(); ?>
				<?php $form->DBMultiSelect('','dc_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno')) ?>
				<?php $form->EndSection(); $form->Section(); ?>
				<?php $form->Combobox('','dm_sin_ejecutivo',array('Clientes sin ejecutivo asignado')) ?>
				<?php $form->EndSection(); ?>
			</td>
		</tr>
		<tr>
			<td>Parámetros de selección</td>
			<td>
				<?php $form->Combobox('','dc_parametros_seleccion',array(
					'Sin emisión de cotización',
					'Sin llamados'
				),array(0,1)); ?>
			</td>
		</tr>
	</tbody>
</table>

<?php
	$form->End('Ejecutar','searchbtn');
?>
<script type="text/javascript" src="jscripts/sites/clientes/src_informe_clientes_inactivos.js"></script>