<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestorTareasFactory
 *
 * @author Eduardo
 */
require_once('enviaM.php');
require_once 'inc/FactoryNoLogin.php';

class gestorTareasFactory extends FactoryNoLogin {

    public function Actualiza_estado_tareas() {
        $estado = 'P';
        $tarea = $this->coneccion('select', 'tb_tareas ta 
                    LEFT JOIN tb_tipo_tarea tita ON tita.dc_tarea=ta.dc_tipo_tarea', 'ta.dc_tarea,
                    MONTH(ta.df_fecha)-MONTH(NOW()) as mes,
                    DAY(ta.df_fecha)-DAY(NOW()) dia, 
                    HOUR(ta.df_fecha)-HOUR(NOW()) hora,
                    tita.dg_tipo_tarea', "dm_estado NOT IN ('T')");
        $tarea = $tarea->fetchAll(PDO::FETCH_OBJ);
        foreach ($tarea as $t):
                if ($t->mes == 0) {
                    if ($t->dia < 0):
                        $estado = 'A';
                    endif;
                    if ($t->dia == 0):
                        if (strstr($t->dg_tipo_tarea, 'Oportunidad')) {
                            if ($t->hora >= 0) {
                                $estado = 'P';
                            } else {
                                $estado = 'A';
                            }
                        } else {
                            $estado = 'P';
                        }
                    endif;
            }
            $this->coneccion('update', 'tb_tarea', array('dm_estado' => $estado), "dc_tarea={$t->dc_tarea}");
        endforeach;
    }

    public function enviar_notificacion() {
        $e = new enviaM();
        $datos = $this->coneccion('select', 'tb_tarea ta
                            LEFT JOIN tb_tipo_tarea tita ON tita.dc_tipo_tarea=ta.dc_tipo_tarea', 'ta.dc_funcionario,
                                tita.dg_tipo_tarea,
                                ta.dc_tipo_tarea,
                                ta.dg_descripcion,
                                ta.dm_estado,
                                ta.df_fecha,
                                Month(ta.df_fecha)-Month(NOW()) Mes_t,
                                Day(ta.df_fecha)-Day(NOW()) Dia_t,
                                Hour(ta.df_fecha)-Day(NOW()) Hora_t
                                Month(ta.df_notificado)-Month(NOW()) Mes_n,
                                Day(ta.df_notificado)-Day(NOW()) Dia_n,
                                ta.dm_estado,
                                ta.dc_tarea', 
                                'dm_estado IN("P","A")');
        $datos = $datos->fetchAll(PDO::FETCH_OBJ);
        $destinatarios=$this->obtener_detinatarios($datos->dc_tarea);
        foreach ($datos as $d) {
            foreach ($destinatarios as $a):
            $func = $this->obtener_datos_FT($a->dc_funcionario);
            $tarea = $d->dg_tipo_tarea;
            //------------------------------
                if($d->Mes_t == 0){
                    if($d->Dia_t == 0){
                        if($d->dm_estado == 'P'){
                            if($d->Hora_t == 1){
                                $e->Enviar_mail(
                                        $func->mail, 
                                        $e->subjet(
                                                $tarea
                                                  ), 
                                        $e->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()'),"dc_tarea={$d->dc_tarea}");
                            }
                            if($d->Hora_t == 0){
                                $e->Enviar_mail(
                                        $func->mail, 
                                        $e->subjet(
                                                $tarea
                                                  ), 
                                        $e->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()','dm_estado'=>'A'),"dc_tarea={$d->dc_tarea}");
                            }
                            if($d->Hora_t == '-1'){
                                $e->Enviar_mail(
                                        $func->mail, 
                                        $e->subjet(
                                                $tarea
                                                  ), 
                                        $e->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()'),"dc_tarea={$d->dc_tarea}");
                            }
                        }
                    }elseif($d->Dia_t > 0){
                        if($d->Dia_n == 1){
                            $e->Enviar_mail(
                                        $func->mail, 
                                        $e->subjet(
                                                $tarea
                                                  ), 
                                        $e->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()'),"dc_tarea={$d->dc_tarea}");
                        }
                    }
                }
            //------------------------------
            endforeach;
        }
    }

    private function obtener_datos_FT($dc_funcionario) {
        $datos = $this->coneccion('select', 'tb_funcionario', 'dg_email mail,CONCAT(dg_nombre," ",dg_ap_paterno," ",dg_ap_materno)  nombre', "dc_funcionario={$dc_funcionario}");
        $datos = $datos->fetch(PDO::FETCH_OBJ);
        return $datos;
    }
    
    private function obtener_detinatarios($id_tarea){
        $dest=$this->coneccion('select','tb_tarea_usuario_notificacion','dc_funcionario',"dc_tarea={$id_tarea}");
        $dest=$dest->fetchAll(PDO::FETCH_OBJ);
        return $dest->dc_funcionario;
    }

}

?>
