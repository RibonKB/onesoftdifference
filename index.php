<?php
	define("MAIN",1);
	require_once("inc/global.php");
	
	$razon = $db->select('tb_empresa','dg_fantasia',"dc_empresa={$empresa}");
	$razon = $razon[0]['dg_fantasia'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Onesoft PYMEERP</title>
<link rel="stylesheet" type="text/css" href="styles/main.css?v=2_0a" />
<?php if(check_permiso(85)): ?>
<link rel="stylesheet" type="text/css" href="styles/callreg_interface.css" />
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="styles/index.css" />
<link rel="stylesheet" type="text/css" href="styles/jquery.multiSelect.css" />
<link rel="stylesheet" type="text/css" href="styles/jquery.autocomplete.css" />
<link rel="stylesheet" type="text/css" href="styles/calendario.css" />
<link rel="stylesheet" type="text/css" href="styles/flexigrid.css" />
<!--script type="text/javascript" src="jscripts/jquery-1.7.1.min.js" ></script -->
<script type="text/javascript" src="jscripts/lib/jquery.tools.2.min.js" ></script>
<?php if(check_permiso(85)): ?>
<script type="text/javascript" src="jscripts/sites/indicadores/src_registro_llamados.js" ></script>
<?php endif; ?>
<script type="text/javascript" src="jscripts/lib/jquery.multiattr.js"></script>
<script type="text/javascript" src="jscripts/lib/jquery.Rut.min.js" ></script>
<script type="text/javascript" src="jscripts/lib/jquery.multiSelect.js" ></script>
<script type="text/javascript" src="jscripts/lib/jquery.tools.form.min.js" ></script>
<script type="text/javascript" src="jscripts/lib/jquery.autocomplete.min.js" ></script>
<script type="text/javascript" src="jscripts/lib/jquery.tablesorter.min.js" ></script>
<script type="text/javascript" src="jscripts/lib/flexigrid.pack.js" ></script>
<script type="text/javascript" src="jscripts/lib/tableExport.js?v=1_1-3" ></script>
<script type="text/javascript" src="jscripts/lib/tableOverflow.js" ></script>
<script type="text/javascript" src="jscripts/pymerp.js?v=0_5b" ></script>
<script type="text/javascript" src="jscripts/lib/textareaUtil.js" ></script>
<script type="text/javascript">
	var conf = new Object();
	
	conf.sep_miles = '<?php echo $empresa_conf['dm_separador_miles'] ?>';
	conf.sep_decimal = '<?php echo $empresa_conf['dm_separador_decimal'] ?>';
	conf.moneda_ndecimal = <?php echo $empresa_conf['dn_decimales_local'] ?>;
	
	pymerp.localIVA = <?php echo $empresa_conf['dq_iva'] ?>;
	pymerp.localDecimales = <?php echo $empresa_conf['dn_decimales_local'] ?>;
</script>
<script type="text/javascript" src="jscripts/onesoft.js?v0_4" ></script>
<script type="text/javascript" src="jscripts/lib/tableadjust.js?v0_3" ></script>
</head>

<body>

<div id="header">


<div id="header_links">
	 Conectado en <strong><?=$razon ?></strong>,&nbsp;&nbsp; | &nbsp;&nbsp;
	 <a href="help.php">Ayuda</a>&nbsp;&nbsp; | &nbsp;&nbsp;<a href="cambiar_pass.php">Perfil</a>&nbsp;&nbsp; | &nbsp;&nbsp;<a href="logout.php">Logout</a>
</div>
<br class="clear" />
<input type="text" name="access_id" id="access_id_src" size="25" />

<div  id="logo"></div>

<div id="header_menu">
<div class="header_menu_item">
	<a href="#">Campañas</a>
	<div class="menu_content">
		Contenido campañas
	</div>
</div>

<div class="header_menu_item">
	<a href="#">Indicadores</a>
	<div class="menu_content">
		Contenido
	</div>
</div>

<div class="header_menu_item">
	<a href="#">Tareas</a>
	<div class="menu_content">
		Contenido
	</div>
</div>

<div class="header_menu_item">
	<a href="#">Actividades</a>
	<div class="menu_content">
		Contenido
	</div>
</div>

<div class="header_menu_item">
	<a href="#">Perfil</a>
	<div class="menu_content">
		Contenido
	</div>
</div>
</div>


<br class="clear" />
</div>


<div id="secc">
<?php
	include("menu.php");
?></div>

<div id="content">
<div id="secc_bar">
	Página principal
</div>

<div id="main_cont" style="text-align:center;">
<?php

echo("
<div class='group_index'>
	<div class='ind_group_head'>
		<span class='small'>▼</span> Campaña
	</div>
	<div class='ind_group_body' id='group_campana'>
	<img src='image002.jpg' alt='Campaña'>
	</div>
	");


//Módulo de indicadores

	$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio,dg_prefijo',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
	if(count($ind_cambio)){
		$ind_cambio_prefix = $ind_cambio[0]['dg_prefijo'];
		$ind_cambio = $ind_cambio[0]['dq_cambio'];
	}else{
		$ind_cambio_prefix = '$';
		$ind_cambio = 1;
	}
	
        if(check_permiso(77)){
            ?>
    <div class='ind_group_head'>
			<span class='small'>▼</span> Tareas
		</div>
		<div class='ind_group_body hidden' style='text-align:left'>
                <?php include ('sites/indicadores/MostrarTareasFactory.php');
                ?>    
                <br class="clear" /></div>
            <?php
        }
        
	if(check_permiso(10) || check_permiso(11) || check_permiso(12) || check_permiso(32) || check_permiso(33) || check_permiso(22) || check_permiso(73)){
		
		//Indicador facturas por gestionar y/o pagar
	if(check_permiso(73)){
		
		echo("<div class='ind_group_head'>
			<span class='small'>▼</span> Facturas en fecha de gestión
		</div>
		<div class='ind_group_body hidden' style='text-align:left'>");
		
		include('sites/indicadores/src_indicador_gestion_factura.php');
		
		echo('<br class="clear" /></div>');
	}
	
	echo("<div class='ind_group_head'>
		<span class='small'>▼</span> Ruta
	</div>
	<div class='ind_group_body hidden'>");
	
	//Indicador de Rutas
	if(check_permiso(10)){
		include('sites/indicadores/src_indicador_ruta.php');
	}
	
	echo('<br class="clear" /></div>');
	
	echo("<div class='ind_group_head'>
		<span class='small'>▼</span> Indicadores comerciales
	</div>
	<div class='ind_group_body hidden'>");
	
	//Indicador de Oportunidades pendientes
	if(check_permiso(10)){
		include('sites/indicadores/src_indicador_oportunidad.php');
	}
	
	//Indicador cotizaciones pendientes
	if(check_permiso(11)){
		include('sites/indicadores/src_indicador_cotizacion.php');
	}
	
	//Indicadores de notas de venta
	if(check_permiso(22)){
		include('sites/indicadores/src_indicador_nota_venta_compra.php');
	}
	
	//Indicadores de notas de venta pendientes de confirmación/validacion
	if(check_permiso(32) || check_permiso(33)){
		include('sites/indicadores/src_indicador_nota_venta_valida_confirma.php');
	}
	
	echo("<br class='clear' /></div>
	</div>");
	}
?>
</div>
</div>
<div id="loader"><img src="images/ajax-loader-big.gif" alt="Cargando..." /></div>


<?php
	if(check_permiso(85)):
		include('sites/indicadores/src_indicadores_costado.php');
	endif;
?>

</body>
</html>
