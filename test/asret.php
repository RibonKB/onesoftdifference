<?php
class Testa{

  private $var;

  public function getSetVar($v){
    $in = intval($v);

    return $this->var = $in;
  }

  public function dump(){
    var_dump($this->var);
  }

}

$t = new Testa();
echo $t->getSetVar(1000);
echo "<hr>";
$t->dump();
