<?php

define("MAIN",1);
require_once("../../inc/global.php");

if(!isset($_GET['fid'])){
	header("Content-type: image/jpeg");
	echo fread(fopen("default_photo.jpg", "r"), filesize("default_photo.jpg"));
}
else{
	$imagen = $db->select("tb_funcionario","dr_foto_usuario,dg_tipo_foto","dc_funcionario = {$_GET['fid']}");
	if(!count($imagen)){
		header("Content-type: image/jpeg");
		echo fread(fopen("default_photo.jpg", "r"), filesize("default_photo.jpg"));
	}
	else{
		$imagen = $imagen[0];
		if($imagen['dr_foto_usuario'] == ""){
			header("Content-type: image/jpeg");
			echo fread(fopen("default_photo.jpg", "r"), filesize("default_photo.jpg"));
		}
		else{
			header("Content-type: {$imagen['dg_tipo_foto']}");
			echo $imagen['dr_foto_usuario'];
		}
	}
}
?>