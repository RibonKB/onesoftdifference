<?php
	function reemplazar_sp_chars($str){
		$str = utf8_decode($str);
		$str=ereg_replace ("/", "-", $str);
		$str=ereg_replace (", ", "_", $str);
		$str=ereg_replace ("_", "_", $str);
		$str=ereg_replace ("_-_", "-", $str);
		$str=strtr(strtolower("$str"),
		"ÀÁÂÃÄÅàáâãäåÈÉÊËèéêëÌÍÎÏìíîïÒÓÔÕÖØòóôõöøÙÚÛÜùúûüÇçÑñÿ",
		"AAAAAAaaaaaaEEEEeeeeIIIIiiiiOOOOOOooooooUUUUuuuuCcNny");
		return $str;
	}
	
	function randomString($largo){
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$gen = "";
		for($i=0; $i<$largo; $i++){
			$gen .= substr($str,rand(0,62),1);
		}
		return $gen;
	}
	
	function moneda_local($cash,$dec=false){
		global $empresa_conf;
		return number_format($cash,$dec===false?$empresa_conf['dn_decimales_local']:$dec,$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
	}
	
	function toNumber($number){
		global $empresa_conf;
		return floatval(str_replace($empresa_conf['dm_separador_decimal'],'.',str_replace($empresa_conf['dm_separador_miles'],'',$number)));
	}
	
	function check_permiso($num){
		if(in_array($num,$_SESSION['usuario_permisos']))
			return true;
		return false;
	}
?>