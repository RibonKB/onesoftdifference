<?php
/**
*	Clase representativa de un contacto, contiene métodos de inserción de nuevos contactos y de creación obtención de ellos dependiendo el cliente
*
*	@author Tomás Lara Valdovinos
*	@version 0.1
**/
class Contacto{
private $id = array();
private $nombreContacto = array();
private $cargo = array();
private $fono = array();
private $fax = array();
private $correo = array();
private $comentario = array();
private $fechaIngreso = array();
private $comuna = array();
private $region = array(); //Util solo a la hora de rescatar los registros desde la base de datos, la inserción de un contacto nuevo solo requiere la comuna
private $direccion = array();
private $horario = array();

private $tipo = array();
private $tipos = array();

private $def = array();

//parámetros de proceso
private $actual = 0;

private $ids = array();

/**
*	constructor multiple, sin argumentos inicializa un contactoComercial sin parámetros.
*	con argumentos (el identificador del cliente) construye un objeto Contacto con todos los que posea el cliente
*
**/
public function __construct(){
	switch(func_num_args()){
		case 0: $this->constr1(); break;
		case 1: $this->constr2(func_get_arg(0)); break;
	}
}

/**
*	Construye un contacto sin parámetros, para la creación de un nuevo listado de contactos
**/
private function constr1(){
	$actual = 0;
}

/**
*	Construye un contacto desde la base de datos, dependiendo del identificador del cliente.
*	Se diferencian los parámetros con respecto a la creación de uno nuevo en que de esta manera se almacenan los contenidos como array (con el ID y un string con el conetenido) para consulta, no solo los identificadores.
*
*	param mixed $idCliente Identificador del cliente del cual se obtendrá el listado de contactosComerciales.
**/
private function constr2($target,$tipo="cliente"){
	global $db,$empresa;

	$tables = "
	tb_domicilio_{$tipo} d,
	tb_contacto_{$tipo} c,
	tb_domicilio_{$tipo}_tipo_direccion dtd,
	tb_comuna co,
	tb_region re,
	tb_cargo_contacto cc,
	tb_contacto_evento ce
	";
	$fields = "*";
	$conditions = "
	d.dc_comuna = co.dc_comuna AND
	d.dc_domicilio = dtd.dc_domicilio AND
	co.dc_region = re.dc_region AND
	c.dc_cargo_contacto = cc.dc_cargo_contacto AND
	c.dc_contacto = dtd.dc_contacto AND
	d.dc_cliente = {$idCliente} AND
	ce.dg_nombre_campo = 'CREATION' AND
	c.db_visible = '1'
	";
	
	//Obtiene desde la base de datos los contactos del cliente especifico, y rescatando solo los que son visibles.
	$contactos = $db->select($tables,$fields,$conditions);
	unset($tables,$fields,$conditions);
	
	//Almacena en un array los datos en pares la mayoria de las veces representando el par {ID , VALOR}
	//Luego es almacenado en los arrays del objeto
	foreach($contactos as $c){
		$contacto['nombre'] = array($c['dc_contacto'],$c['dg_contacto']);
		$contacto['cargo'] = array($c['dc_cargo_contacto'],$c['dg_cargo_contacto']);
		$contacto['fono'] = $c['dg_fono'];
		$contacto['fax'] = $c['dg_fax'];
		$contacto['correo'] = $c['dg_correo'];
		$contacto['comentario'] = $c['dg_comentario'];
		$contacto['fechaIngreso'] = $c['df_evento'];
		$contacto['comuna'] = array($c['dc_comuna'],$c['dg_comuna']);
		$contacto['region'] = array($c['dc_region'],$c['dg_region']);
		$contacto['direccion'] = $c['dg_direccion'];
		$contacto['horario'] = $c['dg_horario_atencion'];
		
		//Rescata los tipos de direccion asignados para el contacto en particular y los almacena en un array {tipo1, tipo2, tipo3, ... }
		$contacto['tipo'] = array();
		$tempTipos = $db->select("tb_domicilio_{$tipo}_tipo_direccion","dc_tipo_direccion","dc_contacto=".$c['dc_contacto']);
		foreach($tempTipos as $t){
			$contacto['tipo'][] = $t['dc_tipo_direccion'];
		}
		
		//Finalmente almacena los datos rescatados en las propiedades del objeto
		$this->addContacto($contacto);
	}
}

/**
*	Retorna la cantidad de contactos almacenados en el objeto
**/
public function getSize(){
	return count($this->nombreContacto);
}

public function getId($index){
	return $this->ids[$index];
}

public function setDefault($def){
	$this->def = $def;
}

public function setNombreContacto($nombreContacto){
	$this->nombreContacto = $nombreContacto;
}

public function setCargo($cargo){
	$this->cargo = $cargo;
}

public function setFono($fono){
	$this->fono = $fono;
}

public function setFax($fax){
	$this->fax = $fax;
}

public function setCorreo($correo){
	$this->correo = $correo;
}

public function setComentario($comentario){
	$this->comentario = $comentario;
}

public function setFechaIngreso($fechaIngreso){
	$this->fechaIngreso = $fechaIngreso;
}

public function setComuna($comuna){
	$this->comuna = $comuna;
}

public function setRegion($region){
	$this->region = $region;
}

public function setDireccion($direccion){
	$this->direccion = $direccion;
}

public function setHorario($horario){
	$this->horario = $horario;
}

public function setTipo($tipo){
	$this->tipo = $tipo;
}

private function addDefault($def){
	$this->def[] = $def;
}

private function addNombreContacto($nombreContacto){
	$this->nombreContacto[] = $nombreContacto;
}

private function addCargo($cargo){
	$this->cargo[] = $cargo;
}

private function addFono($fono){
	$this->fono[] = $fono;
}

private function addFax($fax){
	$this->fax[] = $fax;
}

private function addCorreo($correo){
	$this->correo[] = $correo;
}

private function addComentario($comentario){
	$this->comentario[] = $comentario;
}

private function addFechaIngreso($fechaIngreso){
	$this->fechaIngreso[] = $fechaIngreso;
}

private function addComuna($comuna){
	$this->comuna[] = $comuna;
}

private function addRegion($region){
	$this->region[] = $region;
}

private function addDireccion($direccion){
	$this->direccion[] = $direccion;
}

private function addHorario($horario){
	$this->horario[] = $horario;
}

private function addTipo($tipo){
	$this->tipo[] = $tipo;
}

/**
*	Inserta una tupla completa en el registro con los datos de un contacto
*
*	@param array $contacto Array asociativo con los datos del nuevo contacto. sus parametros deben ser:
*	$contacto['nombre']
*	$contacto['cargo']
*	$contacto['fono']
*	$contacto['fax']
*	$contacto['correo']
*	$contacto['comentario']
*	$contacto['comuna']
*	$contacto['direccion']
*	$contacto['horario']
*
*	En caso de creación desde la base de datos también puede incluirse
*	$contacto['region']
*	$contacto['fechaIngreso']
**/
public function addContacto($contacto){
	$this->addNombreContacto($contacto['nombre']);
	$this->addCargo($contacto['cargo']);
	$this->addFono($contacto['fono']);
	$this->addFax($contacto['fax']);
	$this->addCorreo($contacto['correo']);
	$this->addComentario($contacto['comentario']);
	$this->addComuna($contacto['comuna']);
	$this->addDireccion($contacto['direccion']);
	$this->addHorario($contacto['horario']);
	$this->addTipo($contacto['tipo']);
	
	//a la hora de la consulta de un contacto son requeridos otros 2 campos llenos.
	if(isset($contacto['region'])){
		$this->addRegion($contacto['region']);
	}
	if(isset($contacto['fechaIngreso'])){
		$this->addFechaIngreso($contacto['fechaIngreso']);
	}
}

/**
*	Inserta todos los datos de los arrays de contacto en la base de datos, idealmente utilizado para crear un nuevo contacto
*	
*	@param mixed $cliente El identificador único asociado al cliente al que se le agregarán los datos del contacto
*
*	@deprecated será reemplazado por la funcion insertarnuevos, ya que realiza lo mismo pero tomando la referencia el ultimo registro insertado como deteccion de duplicados
**/
public function insertarEnBD($cliente){
	global $db;
	
	//para cada contacto realizar una inserción
	for($i=0;$i<$this->getSize();$i++){
		
		$idDomicilio = $db->insert("tb_domicilio",
		array(
		"dc_cliente" => $cliente,
		"dc_comuna" => $this->comuna[$i]==""?0:$this->comuna[$i],
		"dg_direccion" => $this->direccion[$i],
		"dg_horario_atencion" => $this->horario[$i]
		));
		
		$idContacto = $db->insert("tb_contacto",
		array(
		"dc_cargo_contacto" => $this->cargo[$i]==""?0:$this->cargo[$i],
		"dg_contacto" => $this->nombreContacto[$i],
		"dg_fono" => $this->fono[$i],
		"dg_fax" => $this->fax[$i],
		"dg_correo" => $this->correo[$i],
		"dg_comentario" => $this->comentario[$i]
		));
		
		foreach($this->tipo[$i] as $t){
			$db->insert("tb_domicilio_tipo_direccion",
			array(
			"dc_domicilio" => $idDomicilio,
			"dc_tipo_direccion" => $t,
			"dc_contacto" => $idContacto
			));
		}
		
	}
	
	//El puntero de contacto agregado a la base de datos hasta el momento es modificado para que los registros insertados recien no vuelvan a ser insertados.
	$this->actual = $this->getSize();
}

/**
*	Modifica en la base de datos el contacto especificado (Nota: si el contacto está agregado en el objeto instanciando a esta clase, el contacto no se actualiza en el objeto, solo en la base de datos)
*
*	@param mixed $id El identificador del contacto que se modificará.
*	@param array $datos Array asociativo con la nueva información del contacto. debe tener la siguiente estructura
*	array(
*		'dc_cliente' => 'El ID del cliente',
*		'dc_comuna'	=>	'La comuna del contacto',
*		'dg_direccion' => 'La direccion del contacto',
*		'dg_horario_atencion' => 'El horario de atención del contacto',
*		'dg_contacto' => 'El nuevo nombre de contacto',
*		'dg_cargo_contacto' => 'El cargo del contacto',
*		'dg_fono' => 'El telefono del contacto',
*		'dg_correo' => 'El correo electronico del contacto',
*		'dg_fax' => 'El fax del contacto',
*		'dg_comentario' => 'Algun comentario sobre el contacto',
*		'tipos' => Array(tipo1,tipo2,tipo3,...)
*	);
**/
public function actualizarContacto($contacto,$domicilio,$tipo="cliente"){
	global $db;
	
	//para cada contacto rescatado antiguo actualizar en la base de datos.
	foreach($contacto as $i => $v){
		$values = array(
			"dg_contacto" => $this->nombreContacto[$i],
			"dg_fono" => $this->fono[$i],
			"dg_fax" => $this->fax[$i],
			"dg_correo" => $this->correo[$i],
			"dg_comentario" => $this->comentario[$i],
			"dm_default" => $this->def[$i]
		);
		
		//Actualizar los datos de contacto
		$db->update("tb_contacto_{$tipo}",$values,"dc_contacto = {$v}");
		
		$data = array(
			"dc_comuna" => $this->comuna[$i] == ""?'0':$this->comuna[$i],
			"dg_direccion" => $this->direccion[$i],
			"dg_horario_atencion" => $this->horario[$i]
		);
		
		//Actualizar los datos de domicilio
		$db->update("tb_domicilio_{$tipo}",$data,"dc_domicilio={$domicilio[$i]}");
		
		//Inicio del algoritmo de habilitar, deshabilitar o agregar nuevos tipos de direccion para el contacto activo
		
		//traigo los contactos antiguos desde la base de datos
		$tipos = $db->select("tb_domicilio_{$tipo}_tipo_direccion","dc_domicilio_tipo_direccion,dc_tipo_direccion,dm_activo","dc_contacto=".$v);
		$antiguos = array();
		//para cada tipo antiguo de contacto compruebo si hay que habilitarlo, deshabilitarlo o insertarlo
		foreach($tipos as $t){
			//El nuevo está en el antiguo pero el antiguo deshabilitado -> habilitar
			if(( in_array($t['dc_tipo_direccion'],$this->tipo[$i])) && ($t['dm_activo'] == '0') ){
				$db->update(
				"tb_domicilio_{$tipo}_tipo_direccion",
				array("dm_activo" => '1'),
				"dc_contacto={$v} AND dc_tipo_direccion={$t['dc_tipo_direccion']}");
			}
			//El antiguo no está en el nuevo -> Deshabilitar
			elseif( !in_array($t['dc_tipo_direccion'],$this->tipo[$i]) ){
				$db->update("tb_domicilio_{$tipo}_tipo_direccion",array("dm_activo" => '0'),"dc_contacto=$v AND dc_tipo_direccion={$t['dc_tipo_direccion']}");
			}

			//el array con antiguos
			$antiguos[] = $t['dc_tipo_direccion'];
			
		}
			foreach($this->tipo[$i] as $t){
				//El nuevo no está en el antiguo -> insert
				if( !in_array($t,$antiguos) ){
					$db->insert("tb_domicilio_{$tipo}_tipo_direccion",
					array(
					"dc_domicilio" => $domicilio[$i],
					"dc_tipo_direccion" => $t,
					"dc_contacto" => $v
					));
				}
			}
		
		//fin de habilitacion, deshabilitacion o creacion de tipos de direccion
		
	}
}

/**
*	Inserta en la base de datos los contactos que no se hayan agregado desde la ultima inserción
*
*	@param mixed $cliente El identificador unico del cliente donde se agregarán los nuevos contactos.
**/
public function insertarNuevos($target,$tipo="cliente"){
	global $db;
	
	//similar a el for de insertarenBD, almacena en la base de datos desde el ultimo insertado, tomando como referencia el puntero|indice $this->actual.
	for($i=$this->actual;$i<$this->getSize();$i++){
		
		$idDomicilio = $db->insert("tb_domicilio_{$tipo}",
		array(
		"dc_{$tipo}" => $target,
		"dc_comuna" => $this->comuna[$i],
		"dg_direccion" => $this->direccion[$i],
		"dg_horario_atencion" => $this->horario[$i]
		));
		
		$idContacto = $db->insert("tb_contacto_{$tipo}",
		array(
		"dc_cargo_contacto" => $this->cargo[$i],
		"dg_contacto" => $this->nombreContacto[$i],
		"dg_fono" => $this->fono[$i],
		"dg_fax" => $this->fax[$i],
		"dg_correo" => $this->correo[$i],
		"dg_comentario" => $this->comentario[$i]
		));
		
		$this->ids[] = $idContacto;
		
		foreach($this->tipo[$i] as $t){
			$db->insert("tb_domicilio_{$tipo}_tipo_direccion",
			array(
			"dc_domicilio" => $idDomicilio,
			"dc_tipo_direccion" => $t,
			"dc_contacto" => $idContacto
			));
		}
	}
	
	//actualizar el puntero para no volver a insertar los datos ya insertados.
	$this->actual = $this->getSize();
}

}
?>