<?php

/**
 *
 * @author Tomás Lara Valdovinos
 */
interface FactoryServiceInterface {
  
    public function setFactory(Factory $factory);
   
}

?>
