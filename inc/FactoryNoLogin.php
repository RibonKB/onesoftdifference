<?php

/**
 * Description of FactoryNoLogin
 *
 * @author Tomás Lara Valdovinos
 * @date 18-06-2013
 */
class FactoryNoLogin extends Factory {

  public function __construct() {
    if (!defined("MAIN")) {
      self::setNotFound();
    }
  }
  
  public function getEmpresa() {
    return 4;
  }

}

?>
