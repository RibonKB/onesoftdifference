<?php
require_once('Factory.class.php');

class Functions{

    /**
     *
     * @var Factory
     */
	private static $factory;

	public static function setFactory(Factory $factory){
		self::$factory = $factory;
	}

	public static function monedaLocal($cash,$dec=false){
		$conf = self::$factory->getParametrosEmpresa();
		//NumberFormatter::formatCurrency($cash,'CLP');
		return number_format($cash,$dec===false?$conf->dn_decimales_local:$dec,$conf->dm_separador_decimal,$conf->dm_separador_miles);
	}

  public static function toNumber($number){
      //$conf = self::$factory->getParametrosEmpresa();
      return floatval(str_replace('.','.',str_replace(',','',$number)));
  }

  public static function fixNumberToLocal($number){
    $conf = self::$factory->getParametrosEmpresa();
    return round($number, $conf->dn_decimales_local);
  }

  public static function generateDocNumber($table, $campo_number, $format=2, $emision = 'df_emision'){
    $db = self::$factory->getConnection();
    $empresa = self::$factory->getEmpresa();

    $doc_prefix = '';
    $largo_prefix = 0;

    switch ($format) {
	    case 1: $doc_prefix = date('Y');
        $largo_prefix = 4;
        break;
      case 2: $doc_prefix = date('Ym');
        $largo_prefix = 6;
        break;
      case 4: $doc_prefix = date('m');
        $largo_prefix = 2;
        break;
    }

    if ($format != 3) {
      $Mactual = date('m');
      $Yactual = date('Y');
      $last = $db->doQuery($db->select($table, "MAX({$campo_number}) AS dq", "MONTH({$emision})='{$Mactual}' AND YEAR({$emision})='{$Yactual}' AND dc_empresa={$empresa}"));
    } else {
      $last = $db->doQuery($db->select($table, "MAX({$campo_number}) AS dq AND dc_empresa={$empresa}"));
    }

    $last = $last->fetch(PDO::FETCH_OBJ)->dq;

    if ($last != NULL) {
      $doc_num = substr($last, $largo_prefix) + 1;
    } else {
      $doc_num = 1;
    }

    return $doc_prefix . str_pad($doc_num, 4, 0, STR_PAD_LEFT);
  }

  public static function getRequestDateTime($field){
    $r = Factory::getRequest();
    $hora = $field . '_hora';
    $minu = $field . '_minuto';
    $meri = $field . '_meridiano';

	  if($r->$hora == 12){
      $r->$hora = 0;
    }

    $hora = str_pad($r->$hora + $r->$meri, 2, '0', STR_PAD_LEFT) . ":" . $r->$minu;
    return self::$factory->getConnection()->sqlDate2($r->{$field} . ' ' . $hora);
  }

  public static function validateDate($date, $format = 'Y-m-d H:i:s'){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
  }

	public static function validaRut($rut){
		$conf = self::$factory->getParametrosEmpresa();
		$rut_preg = $conf->dg_preg_rut;
		return preg_match($rut_preg, $rut);
	}

	public static function objToArray($obj){
		$res = array();
		foreach($obj as $i => $v):
			$res[$i] = $v;
		endforeach;
		return $res;
	}

	public static function isPK($val){
		return boolval(preg_match("/^\d{1,11}$/",$val)) && $val != 0;
	}

}
