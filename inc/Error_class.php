<?php
/**
*	Clase encargada de mostrar mensajes de alerta y error, existen 4 niveles de alerta
*	1.- Aviso: Muestra un aviso al usuario, por lo general es utilizado cuando una solicitud fue realizada con exito pero no hubo resultados, también se utiliza para dar precaución a un usuario de algún hecho.
*	2.- Warning: Se utiliza mayormente cuando se le da un mensaje de error al usuario y que su solicitud no fue procesada correctamente.
*	3.- Info: Muestra un mensaje de información, utilizado generalmente para dar información al usuario de que es lo que debe hacer, etc.
*	4.- Error_fatal: Similar a warning, unicamente que esta termina la ejecución del código.
*
*	@author Tomás Lara Valdovinos
*	@version 0.1
**/
class Error{

	/**
	*	Muestra un error fatal al usuario terminando la ejecución del script.
	*
	*	@param string $title la cabecera del mensaje de error para dar una idea rápida del por qué se produjo el error
	*	@param array $data colección de errores producidos, en formato $['tipo error'] = "informacion". Es el listado de errores especificos que se mostrarán.
	*
	**/
	public function show_fatal_error($title,$data){
		$error = "<div class=\"warning\" style='background:#EEE;'>Fatal error :<h2 style='display:inline;margin:0;'>".$title."</h1>";
		foreach($data as $key => $value){
			$error .= "<hr /><div><strong>".$key."</strong>: ".$value."</div>";
		}
		$error .= "</div>";
		echo $error;
		die();
	}

	/**
	*	Muestra un aviso de advertencia al usuario
	*
	*	@param string $mensaje El mensaje que se mostrará con formato de alerta
	**/
	public function showAviso($mensaje, $tipo = 0){
		switch($tipo){
			case 0:
				echo("<div class=\"alert\">".$mensaje."</div>");
				break;
			case 1:
				return "<div class=\"alert\">".$mensaje."</div>";
				break;
		}
	}

	/**
	*	Muestra un mensaje de operacion existosa al usuario
	*
	*	@param string $mensaje El mensaje que se mostrará con formato de confirmacion
	**/
	public function showConfirm($mensaje, $tipo = 0){
		switch($tipo){
			case 0:
				echo("<div class=\"confirm\">".$mensaje."</div>");
				break;
			case 1:
				return "<div class=\"confirm\">".$mensaje."</div>";
				break;
		}
	}

	public function showSuccess($mensaje, $tipo = 0){
		return $this->showConfirm($mensaje, $tipo);
	}

	/**
	*	Muestra un Warning al usuario
	*
	*	@param string $mensaje El mensaje que se mostrará en modo Warning
	**/
	public function showWarning($mensaje, $tipo = 0){
		switch($tipo){
			case 0:
				echo("<div class=\"warning\">".$mensaje."</div>");
				break;
			case 1:
				return "<div class=\"warning\">".$mensaje."</div>";
				break;
		}

	}

	/**
	*	Muestra un mensaje de información al usuario.
	*
	*	@param string $mensaje El mensaje que se mostrará en modo informátivo.
	**/
	public function showInfo($mensaje, $tipo = 0){
		switch($tipo){
			case 0:
				echo("<div class=\"info\">".$mensaje."</div>");
				break;
			case 1:
				return "<div class=\"info\">".$mensaje."</div>";
				break;
		}
	}

	/**
	*	Muestra un error , termina la ejecución del script y redirige a la pagina especificada por {$redirect}
	*
	*	@param string $mensaje El mensaje de error que se mostrará.
	*	@param string $redirect La URL donde será redirigido luego de mostrar el error.
	**/
	public function showErrorRedirect($mensaje,$redirect){
		include("notfoundtemplate.php");
		exit();
	}

}

?>
