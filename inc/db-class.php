<?php

class DBConnector extends PDO {
    private $user;
    private $server;
    private $currentDB;
    private $driver = 'mysql:';
    private $currentDateTime = 'now()';
	private $in_transaction = false;

	//DELETE ON PRODUCTION
	private $debugQuery = false;

    public function DBConnector(array $data) {
        $this->user = $data['user'];
        $this->server = $data['server'];
        $this->currentDB = $data['dbname'];
        $this->driver .= "host={$this->server};";
        if (isset($data['port']))
            $this->driver .= "port={$data['port']};";
        $this->driver .= "dbname={$this->currentDB}";

        try {
            parent::__construct($this->driver, $this->user, $data['password'], array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
            ));
        } catch (PDOException $e) {
            echo($e->getMessage());
        }
    }

    public function doQuery($query) {
		if($this->debugQuery)
			echo($query.'<br /><br />');
        try {
            $result = $this->query($query);
            if (!$result) {
                echo($query);
				if($this->in_transaction)
                	$this->rollBack();
                $info = $this->errorInfo();
                throw new Exception($info[2], $info[1]);
                exit;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

	public function doExec($query){
		if($this->debugQuery)
			echo($query.'<br /><br />');
		try{
			$result = $this->exec($query);
			if($result === false){
				echo($query);
				if($this->in_transaction)
                	$this->rollBack();
                $info = $this->errorInfo();
                throw new Exception($info[2], $info[1]);
                exit;
			}
			return $result;
		}catch(Exception $e){
			echo $e->getMessage();
            exit;
		}
	}

    public function stExec(PDOStatement $st) {
		if($this->debugQuery)
			echo($st->queryString.'<br /><br />');
        try {
            if (!$st->execute()) {
                echo($st->queryString.'<br />');
				/*if($this->in_transaction)
                	$this->rollBack();*/
                $info = $st->errorInfo();
                throw new Exception($info[2], $info[1]);
            }
        } catch (Exception $e) {
            echo($e->getMessage());
            $this->rollBack();
            exit();
        }
    }

public function delete($tabla,$condicion="1"){

    return "DELETE FROM {$tabla} WHERE {$condicion} ";
}

    public function insert($tabla, $valores) {
        $campos = array();
        $val = array();
        foreach ($valores as $i => $v) {
            $campos[] = $i;
            $val[] = $v;
        }
        $campos = implode(',', $campos);
        $val = implode(',', $val);
        return "INSERT INTO {$tabla}({$campos}) VALUES ({$val})";
    }

    public function update($tabla, $valores, $condiciones="true") {
        $edit = array();
        foreach ($valores as $i => $v) {
            $edit[] = "{$i}={$v}";
        }
        $edit = implode(',', $edit);
        return "UPDATE {$tabla} SET {$edit} WHERE {$condiciones}";
    }

    public function select($tabla, $campos="*", $condiciones="", $opciones=array()) {

        //formato básico de un select
        $query = "SELECT " . $campos . " FROM {$tabla}";

        //se insertan las condiciones en caso de haber
        if ($condiciones != "") {
            $query .= " WHERE " . $condiciones;
        }

        if (isset($opciones['group_by'])) {
            $query .= " GROUP BY " . $opciones['group_by'];
        }

        if (isset($opciones['having']) && $opciones['having'] != '') {
            $query .= " HAVING " . $opciones['having'];
        }
        //se ordenarán los registros? por cual campo se hará?
        if (isset($opciones['order_by'])) {
            $query .= " ORDER BY " . $opciones['order_by'];
            if (isset($opciones['order_dir'])) {
                $query .= " " . strtoupper($opciones['order_dir']);
            }
        }

        //especifica el límite de registros que se mostrarán, indicando limite inferior y cantidad de registros ...
        if (isset($opciones['limit_start']) && isset($opciones['limit'])) {
            $query .= " LIMIT " . $opciones['limit_start'] . ", " . $opciones['limit'];
        }
        // ... o indicando el primer registro como limite inferior
        elseif (isset($opciones['limit'])) {
            $query .= " LIMIT " . $opciones['limit'];
        }
        //convierte los registros obtenidos en un preparedStatement para luego retornarlo
        return $query;
    }

	public function start_transaction(){
		if($this->debugQuery)
			echo("START TRANSACTION<br><br>");
		$this->in_transaction = true;
		$this->beginTransaction();
	}

	public function end_transaction(){
		$this->in_transaction = false;
	}

	public function dateTimeLocalFormat($sqlDate){
		return preg_replace("/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})/","$3/$2/$1 $4:$5",$sqlDate);
	}

	public function dateLocalFormat($sqlDate){
		return preg_replace("/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/","$3/$2/$1",$sqlDate);
	}

    public function sqlDate($formDate) {
        if ($formDate == '')
            return 'NULL';
        if (strlen($formDate) > 11)
            return preg_replace("/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/", "'$3-$2-$1 $4:$5'", $formDate);
        return preg_replace("/(\d{2})\/(\d{2})\/(\d{4})/", "'$3-$2-$1 00:00:00'", $formDate);
    }

	public function sqlDate2($formDate){
		if ($formDate == '')
            return 'NULL';
        if (strlen($formDate) > 11)
            return preg_replace("/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/", "$3-$2-$1 $4:$5", $formDate);
        return preg_replace("/(\d{2})\/(\d{2})\/(\d{4})/", "$3-$2-$1 00:00:00", $formDate);
	}

    public function getNow() {
        return $this->currentDateTime;
    }

	private $rowByIdCache = array();

	public function getRowById($tabla, $id, $campo_id, $campos = '*'){

		if(isset($this->rowByIdCache[$tabla][$id])){
			return $this->rowByIdCache[$tabla][$id];
		}

		$query = $this->prepare($this->select($tabla,$campos,$campo_id.'=?'));
		$query->bindValue(1,$id,PDO::PARAM_INT);
		$this->stExec($query);

		$this->rowByIdCache[$tabla][$id] = $query->fetch(PDO::FETCH_OBJ);

		return $this->rowByIdCache[$tabla][$id];

	}

}

?>
