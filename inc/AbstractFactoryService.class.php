<?php

require_once 'inc/FactoryServiceInterface.php';

/**
 * Description of AbstractFactoryService
 *
 * @author Tomás Lara Valdovinos
 * @date 26-04-2013
 */
class AbstractFactoryService implements FactoryServiceInterface{

    /** @var Factory */
    protected $factory;

    public function setFactory(Factory $factory){
        $this->factory = $factory;
    }

    public function getErrorMan(){
      return $this->factory->getErrorMan();
    }

    public function getConnection(){
      return $this->factory->getConnection();
    }

}

?>
