<?php
/**
*	Clase padre encargada de difundir notificaciones en Onesoft PYMERP
*	
*	Características
*	1.0:
*		- Envío de notificaciones por correo
*		- Conexión de salida SMTP
*/
require_once("../mail/class.phpmailer.php");

abstract class Diffuser{
	
	private $mailer;
	
	//SMTP dataconfig
	private $SMTPServer = "pod51009.outlook.com";
	private $SMTPUser = "onesoft@adischile.cl";
	private $SMTPPassword = "Gtadis2012";
	private $SMTPPort = 587;
	private $SMTPCrypto = "TLS";
	
	//Mail Content config
	private $SMTPUserFullName = "Onesoft PYMERP";
	private $recipients = array();
	private $replyMail = array();
	private $mailBody;
	private $mailSubject;
	
	/**
	*	Al construir el difusor se inicializa el objeto encargado de enviar los correos electrónicos ($this->mailer)
	*/
	public function __construct(){
		$this->mailer = new PHPMailer();
		$this->mailer->IsSMTP();
		
		$this->mailer->Host = $this->SMTPServer;
		$this->mailer->SMTPSecure = $this->SMTPCrypto;
		$this->mailer->SMTPKeepAlive = true;
		$this->mailer->Port = $this->SMTPPort;
		$this->mailer->SMTPAuth = true;
		$this->mailer->Username = $this->SMTPUser;
		$this->mailer->Password = $this->SMTPPassword;
		
		$this->mailer->CharSet = 'UTF-8';
		$this->mailer->SetFrom($this->SMTPUser,$this->SMTPUserFullName);
	}
	
	public function isValidEmail($email){
		$pattern = "/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/";
		return preg_match($pattern,$email);
	}
	
	public abstract function sendNotification();
	
	protected function sendMail(){
		
		//Configurar asunto y cuerpo del mensaje
		if(empty($this->mailSubject)){
			throw new Exception("NO_SUBJECT_EXCEPTION: No se ha incluido un asunto para el mensaje, no se puede continuar.",10003);
			return false;
		}
		$this->mailer->Subject = $this->mailSubject;
		
		if(empty($this->mailBody)){
			throw new Exception("NO_MSGBODY_EXCEPTION: No se ha incluido un cuerpo para el mensaje no puede continuar.");
			return false;
		}
		$this->mailer->MsgHTML($this->mailBody);
		
		//Configurar los correos que recibirán la notificación
		if(!empty($this->recipients)){
			throw new Exception("NO_RECIPIENTS_EXCEPTION: No hay correos a quien enviarle la notificación, no se puede continuar.",10001);
			return false;
		}
		
		foreach($this->recipients as $r){
			if(!$this->isValidEmail($r)){
				throw new Exception("INVALID_RECIPENT_EMAIL: <b>{$r}</b> No parece ser una dirección de correo válida, no se puede continuar.",10002);
				return false;
			}
			$this->mailer->AddAddress($r,'');
		}
		
		//Configurar las personas que recibirán las respuestas a los mensajes de notificación
		foreach($this->replyMail as $r){
			if(!$this->isValidEmail($r)){
				throw new Exception("INVALID_RECIPENT_EMAIL: <b>{$r}</b> No parece ser una dirección de correo válida, no se puede continuar.",10002);
				return false;
			}
			$this->mailer->AddReplyTo($r,'');
		}
		
	}
	
	/**
	*	Inicializa el cuerpo del mensaje que se enviará
	*	
	*	El cuerpo es obligatorio antes de enviar cualquier correo.
	*/
	public function setBody($body){
		$this->mailBody = $body;
	}
	
	/**
	*	Asigna un Asunto al mensaje que se enviará
	*
	*	Si bien el asunto no es obligatorio en un envío de correo, es una medida interna que obliga a describir el asunto de una notificación
	*/
	public function setSubject($subject){
		$this->mailSubject = $subject;
	}
	
}