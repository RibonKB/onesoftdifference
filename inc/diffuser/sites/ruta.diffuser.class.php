<?php
/**
*	
*/
require_once("../diffuser.class.php");

class RutaDiffuser extends Diffuser{
	
	private $db = NULL;
	private $dc_ruta = NULL;
	
	public function sendNotification(){
		if($this->db === NULL){
			throw new Exception("NO_DB_EXCEPTION: Ocurrió un error al intentar conectarse con la base de datos",10005);
			return false;
		}
		if($this->dc_ruta === NULL){
			throw new Exception("NO_ROUTE_SELECTED_EXCEPTION: No se ha especificado una ruta para la notificación",10006);
			return false;
		}
		
		$data = $this->db->prepare($this->db->select('tb_ruta_detalle d
		JOIN tb_nota_venta_detalle nvd ON d.'));
		
	}
	
	public function setDatabaseConnection(DBConnector $db){
		$this->db = $db;
	}
	
	public function setDcRuta($dc_ruta){
		$this->dc_ruta = $dc_ruta;
	}
	
}