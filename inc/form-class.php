<?php
class Form{
	private $empresa;
	private $db;
	
	const DEFAULT_TEXT_LENGTH = 255;
	const DEFAULT_MANDATORY_STATUS = false;
	const DEFAULT_FORM_CLASS = 'validar';
	const LIST_DEFAULT_SELECTED = '';
	const GROUP_DEFAULT_EXTRA = '';
	const MANDATORY_ICON = '[*]';
	const DEFAULT_INPUT_CLASS = 'inputtext';

	public function __construct($empresa){
		$this->empresa = $empresa;
	}
	
	public function Start($action,$id,$class="validar",$extra=""){
		echo "
		<div id='{$id}_res'></div>
		<form action='{$action}' id='{$id}' class='{$class}' {$extra}>
		<fieldset>
		";
	}
	
	public function Header($title,$extra=''){
		echo "
		<div class='info' {$extra}>{$title}</div>
		";
	}
	
	public function Group($extra = Form::GROUP_DEFAULT_EXTRA){
		echo "
		</fieldset>
		<fieldset {$extra}>
		";
	}
	
	public function Section($id = NULL){
      if($id !== NULL){
        echo "<div class='left' id='{$id}'>";
      }else{
		echo "<div class='left'>";
      }
	}
        
        public function Section2($id = NULL){
      if($id !== NULL){
        echo "<div class='left' id='{$id}' style='padding-left:50px;'>";
      }else{
		echo "<div class='left' style='padding-left:25px;' >";
      }
	}
	
	public function EndSection(){
		echo "</div>";
	}
	
	public function Text($title,$name,$required=0,$max=255,$value="",$class="inputtext",$size=41,$disable=0 ,$readonly=0){
	$ast = $required?" [*]":"";
	if($title)
		echo("<label>{$title}{$ast}<br />");
		echo "<input type='text' name='{$name}' id='{$name}' class='{$class}' value='{$value}' size='{$size}' maxlength='{$max}' ";
		if($required){
			echo "required='required' ";
		}
        if($disable){
            echo "disabled='disabled' ";
        }
        if($readonly){
            echo 'readonly="readonly" ';
        }
		echo"/>";
        if($title)
            echo "</label><br />";
	}
	
	public function Textarea($title,$name,$required=0,$value="",$cols=32,$rows=5){
	$ast = $required?" [*]":"";
		echo "
		<label>{$title}{$ast}<br />
			<textarea name='{$name}' id='{$name}' class='inputtext' rows='{$rows}' cols='{$cols}' ";
		if($required){
			echo "required='required' ";
		}
		echo ">{$value}</textarea>
		</label><br />
		";
	}
        
        public function Textarea2($title,$name,$required=0,$value="",$readOnly=0,$cols=30,$rows=3){
	$ast = $required?" [*]":"";
		echo "
		<label>{$title}{$ast}<br />
			<textarea name='{$name}' id='{$name}' class='inputtext' rows='{$rows}' cols='{$cols}' ";
		if($required){
			echo "required='required' ";
		}
                if($readOnly)
                {
                    echo " ReadOnly='ReadOnly' style='background:transparent;'";
                }
                
		echo ">{$value}</textarea>
		</label><br />
		";
	}
        public function Textarea3($title,$name,$required=0,$value=""){
	$ast = $required?" [*]":"";
		echo "
		<label>{$title}{$ast}
			<input type='text' name='{$name}' id='{$name}' class='inputtext' ReadOnly='ReadOnly' style='background:transparent;border:0px;width:120px' ";
		if($required){
			echo "required='required' ";
		}
                
                
		echo "/></label>";
	}
        
	
	public function Password($title,$name,$required=0,$max=255){
	$ast = $required?" [*]":"";
		echo "
		<label for='{$name}'>{$title}{$ast}<br />
			<input type='password' name='{$name}' id='{$name}' class='inputtext' size='41' maxlength='$max' ";
		if($required){
			echo "required='required' ";
		}
		echo "/>
		</label><br />
		";
	}
	
	public function Date($title,$name,$required=0,$value="",$class="inputtext"){
	$ast = $required?" [*]":"";
		echo empty($title) ? '':"<label>{$title}{$ast}<br />";
        echo "<input type='text' name='{$name}' id='{$name}' class='date {$class}' value='{$value}' size='38' ";
		if($required){
			echo "required='required' ";
		}
		echo "/>";
        echo empty($title) ? '':"</label><br />";
	}
	
	public function DateManual($title,$name,$required=0,$value="",$class="inputtext"){
		$ast = $required?" [*]":"";
		echo "
			<label>{$title}{$ast}</label><br />
			<input type='text' name='{$name}' id='{$name}' class='manual_date {$class}' value='{$value}' size='36' maxlength='10' ";
		if($required){
			echo "required='required' ";
		}
		echo "/>
			<br />
		";
	}
    
    //Se agrega nueva funcion Date3
    public function Date3($title, $name, $id, $class = 'inputtext', $required = 0, $value = 0){
        $ast = $required ? '[*]':'';
        echo empty($title) ? '':"<label>{$title} {$ast}<br />";
        $input = '<input type="text" ';
        $input .= "name='{$name}' ";
        $input .= "class='date {$class}' ";
        $input .= empty($id) ? '':"id='date {$class}' ";
        $input .= $required ? "required='required' ":'';
        $input .= "value='{$value}' ";
        $input .= " /> <br />";
        echo $input;
        
    }
    //Fin de la función Date3
	
	public function DateTime($title,$name,$required=0,$value="",$class="inputtext"){
      if($value == '0000-00-00 00:00:00'){
        $value = '';
      }
	$ast = $required?" [*]":"";
		echo "
			<label>{$title}{$ast}<br />
			<input type='text' name='{$name}' id='{$name}' class='date {$class}' value='{$value}' size='38' ";
		if($required){
			echo "required='required' ";
		}
		echo "/></label><br />";
        
        if($value){
          $time = strtotime($value);
          $value_h = date('h',$time);
          $value_m = date('i',$time);
          $value_mer = date('A',$time)=='AM'?0:12;
        }else{
          $value_h = 1;
          $value_m = 0;
          $value_mer = 0;
        }
		
		//hora
		echo("<label>Hora</label>
		<select name='{$name}_hora' class='inputtext' style='width:50px;'>");
		foreach(range(1,12) as $h){
            if($h == $value_h){
              $sel = 'selected="selected"';
            }else{
              $sel = '';
            }
			$h = str_pad($h,2,'0',STR_PAD_LEFT);
			echo("<option value='{$h}' {$sel}>{$h}</option>");
		}
		echo("</select> : <select name='{$name}_minuto' class='inputtext' style='width:50px;'>");
		foreach(range(0,45,15) as $m){
            if($m == $value_m){
              $sel = 'selected="selected"';
            }else{
              $sel = '';
            }
			$m = str_pad($m,2,'0',STR_PAD_LEFT);
			echo("<option value='{$m}' {$sel}>{$m}</option>");
		}
		echo("</select> - ");
        
		echo("<select name='{$name}_meridiano' class='inputtext' style='width:70px;'>");
        if($value_mer == 0){
          echo("<option value='0'>AM</option>");
          echo("<option value='12'>PM</option>");
        }else{
          echo("<option value='0'>AM</option>");
          echo("<option value='12' selected='selected'>PM</option>");
        }
		echo("</select><br />");
	}
	
	public function Time($title, $name, $required = true, $value = '66:66'){
		$value = explode(':',$value);
		echo("<br /><label>{$title}</label><br />
		<select name='{$name}_hora' id='{$name}_hora' class='inputtext' style='width:50px;'>");
		if(!$required){
			echo('<option value="0"></option>');
		}
		foreach(range(1,12) as $h){
			$h = str_pad($h,2,'0',STR_PAD_LEFT);
			if($h == $value[0])
				echo("<option value='{$h}' selected='selected'>{$h}</option>");
			else
				echo("<option value='{$h}'>{$h}</option>");
		}
		echo("</select> : <select name='{$name}_minuto' id='{$name}_minuto' class='inputtext' style='width:50px;'>");
		if(!$required){
			echo('<option value="0"></option>');
		}
		foreach(range(0,45,15) as $m){
			$m = str_pad($m,2,'0',STR_PAD_LEFT);
			if($m == $value[1])
				echo("<option value='{$m}' selected='selected'>{$m}</option>");
			else
				echo("<option value='{$m}'>{$m}</option>");
		}
		echo("</select> - 
		<select name='{$name}_meridiano' id='{$name}_meridiano' class='inputtext' style='width:70px;'>");
		if(!$required){
			echo('<option value="0"></option>');
		}
		echo("<option value='0'>AM</option>
		<option value='12'>PM</option>
		</select><br />");
	}
	
	public function Combobox($title,$name,$items,$checked=array(),$salto="<br />"){
		echo "
		<div id='{$name}'>
			{$title}<br />";
		foreach($items as $v => $i){
			$check = in_array($v,$checked)?"checked='checked'":"";
			echo "
			<label>
				<input type='checkbox' name='{$name}[]' value='{$v}' {$check} /> {$i}
			</label>{$salto}";
		}
		echo "
		</div>
		";
	}
        
        public function Combobox2($title,$name,$items,$style="",$checked=array(),$salto="<br />"){
		echo "
		<div id='{$name}' style='{$style}'>
			{$title}<br />";
		foreach($items as $v => $i){
			$check = in_array($v,$checked)?"checked='checked'":"";
			echo "
			<label>
				<input type='checkbox' id='{$name}' name='{$name}' value='{$v}' {$check} /> {$i}
			</label>{$salto}";
		}
		echo "
		</div>
		";
	}
        public function Combobox4($title,$name,$items,$checked=array(),$salto="<br />",$style="background-color: #fff696"){
		echo "
		<div id='{$name}' style='{$style}'>
			{$title}<br />";
		foreach($items as $v => $i){
			$check = in_array($v,$checked)?"checked='checked'":"";
			echo "
			<label>
				<input type='checkbox' id='{$name}' name='{$name}' value='{$v}' {$check} /> {$i}
			</label>{$salto}";
		}
		echo "
		</div>
		";
	}
        public function Combobox5($title,$name,$id_c,$items,$checked=array(),$salto="<br />",$style="background-color: #fff696"){
		echo "
		<div id='{$name}' style='{$style}'>
			{$title}<br />";
		foreach($items as $v => $i){
			$check = in_array($v,$checked)?"checked='checked'":"";
			echo "
			<label>
				<input type='checkbox' id='{$id_c}' name='{$name}' value='{$v}' {$check} /> {$i}
			</label>{$salto}";
		}
		echo "
		</div>
		";
	}
	
        public function Combobox3($title,$name,$items,$style='',$checked=array(),$salto="<br />"){
		echo "
		<div >
			{$title}<br />";
		foreach($items as $v => $i){
			$check = in_array($v,$checked)?"checked='checked'":"";
			echo "
			<label>
				<input type='checkbox' id='{$name}' name='{$name}' value='{$v}' {$check} /> {$i}
			</label>{$salto}";
		}
		echo "
		</div>
		";
	}
        
	public function Radiobox($title,$name,$items,$checked="",$salto="<br />"){
		echo "
		<div id='{$name}'>
			<label>{$title}</label><br />";
		foreach($items as $v => $i){
			$sel = $v==$checked?"checked='checked'":"";
			echo "
			<label>
				<input type='radio' name='{$name}' value='{$v}' {$sel} /> {$i}
			</label>{$salto}";
		}
		echo "
		</div>
		";
	}
        
        public function Radiobox2($title,$name,$items,$checked=""){
		echo "
		<div id='div{$name}'><label>{$title}</label>";
		foreach($items as $v => $i){
			$sel = $v==$checked?"checked='checked'":"";
			echo "<label ><input title='{$i}' type='radio' class='nota' id='{$name}'  name='{$name}' value='{$v}' {$sel} />&nbsp;&nbsp;&nbsp;&nbsp;</label>";	
		}
		echo "
		</div>
		";
	}
        public function Radiobox3($title,$name,$items,$checked=""){
		echo "
		<div id='div{$name}'><label>{$title}</label>";
		foreach($items as $v => $i){
			$sel = $v==$checked?"checked='checked'":"";
                        echo "<label ><input title='{$i}' type='radio' class='{$name}' id='{$name}'  name='{$name}' value='{$v}' {$sel} />{$i}</label>";	
		}
		echo "
		</div>
		";
	}
	
	public function Hidden($name,$value){
        if(substr($name, -2) == '[]'){
          $extra = 'class="'.  substr($name, 0, -2).'"';
        }else{
          $extra = '';
        }
		echo "
		<input type='hidden' name='{$name}' value='{$value}' {$extra} />
		";
	}
        
        
        public function Hidden2($name,$value){
        if(substr($name, -2) == '[]'){
          $extra = 'class="'.  substr($name, 0, -2).'"';
        }else{
          $extra = '';
        }
		echo "
		<input type='hidden' id='{$name}' name='{$name}' value='{$value}' {$extra} />
		";
	}
	
	public function File($title,$name){
		echo "
		<label for='{$name}'>{$title}<br />
			<input type='file' name='{$name}' id='{$name}' size='41' class='inputtext' />
			</label><br />
		";
	}
	
	public function Select($title,$name,$items,$required=0,$selected=""){
	$ast = $required?" [*]":"";
    if(substr($name, -2) == '[]'){
      $class = ' '.substr($name, 0 , -2);
    }else{
      $class = '';
    }
		$req = $required?"required='required'":"";
		if($title)
			echo "<label>{$title}{$ast}<br />";
		echo"<select name='{$name}' class='inputtext{$class}' id='{$name}' {$req} >
				<option value='0'></option>";
		foreach($items as $v => $i){
			$sel = $v==$selected?"selected='selected'":"";
			echo "<option value='{$v}' {$sel}>{$i}</option>";
		}
		if($title)
			echo("</select>");
		echo "</label><br />";
	}
        
        public function Select2($title,$name,$items,$required=0,$selected="",$style=''){
	$ast = $required?" [*]":"";
    if(substr($name, -2) == '[]'){
      $class = ' '.substr($name, 0 , -2);
    }else{
      $class = '';
    }
		$req = $required?"required='required'":"";
		if($title)
			echo "<label id='e{$name}' style='{$style}'>{$title}{$ast}";
		echo"<select name='{$name}' class='inputtext{$class}' style='{$style}' id='{$name}' {$req} >
                            <option value='0' style='display: none'></option>";
				
		foreach($items as $v => $i){
			$sel = $v==$selected?"selected='selected'":"";
			echo "<option value='{$v}' {$sel}>{$i}</option>";
		}
		if($title)
			echo("</select>");
		echo "</label>";
	}
	
	public function Multiselect($title,$name,$items=array(),$selected=array()){
		echo"
		<label>{$title}</label><br />
			<select name='{$name}[]' multiple='multiple' class='inputtext multiple' id='{$name}' size='6' >
		";
		foreach($items as $v => $i){
			$sel = in_array($v,$selected)?"selected='selected'":"";
			echo "<option value='{$v}' {$sel}>{$i}</option>";
		}
		echo "</select><br />
		";
	}
	
	public function Listado($title,$name,$table,$fields,$required=0,$selected="",$conditions="dc_empresa",$options=array()){
	$options = count($options)?$options:array('order_by'=>"{$fields[1]}");
	$conditions = $conditions=="dc_empresa"?"dc_empresa={$this->empresa} AND dm_activo = '1'":$conditions;
		global $db;
		$aux = $fields[0];
		for($i=1;$i<count($fields);$i++){
			$aux .= ",{$fields[$i]}";
		}
		$data = $db->select($table,$aux,$conditions,$options,MYSQL_NUM);
		$list = array();
		foreach($data as $d){
			$aux = $d[1];
			if(count($fields)>2){
				for($i=2;$i<count($fields);$i++){
					$aux .= " {$d[$i]}";
				}
			}
			$list[$d[0]] = $aux;
		}
		$this->Select($title,$name,$list,$required,$selected);
	}
	
	public function ListadoMultiple($title,$name,$table,$fields,$selected=array(),$conditions="dc_empresa"){
	$conditions = $conditions=="dc_empresa"?"dc_empresa={$this->empresa} AND dm_activo = '1'":$conditions;
		global $db;
		$aux = $fields[0];
		for($i=1;$i<count($fields);$i++){
			$aux .= ",{$fields[$i]}";
		}
		$data = $db->select($table,$aux,$conditions,array("order_by"=>"{$fields[1]}"),MYSQL_NUM);
		$list = array();
		foreach($data as $d){
			$aux = $d[1];
			if(count($fields)>2){
				for($i=2;$i<count($fields);$i++){
					$aux .= " {$d[$i]}";
				}
			}
			$list[$d[0]] = $aux;
		}
		$this->MultiSelect($title,$name,$list,$selected);
	}
	
	public function DBSelect($title,$name,$table,$fields,$required=0,$selected="",$conditions="dc_empresa",$options=array()){
		
		if($this->db instanceof DBConnector){
			$db = $this->db;
		}else{
			global $db;
		}
		
		$options = count($options)?$options:array('order_by'=>"{$fields[1]}");
		$conditions = $conditions=="dc_empresa"?"dc_empresa={$this->empresa} AND dm_activo = '1'":$conditions;
		
		$aux = implode(',',$fields);
		$data = $db->doQuery($db->select($table,$aux,$conditions,$options))->fetchAll(PDO::FETCH_NUM);
		$list = array();
		foreach($data as $d){
			$aux = $d[1];
			if(count($fields)>2){
				for($i=2;$i<count($fields);$i++){
					$aux .= " {$d[$i]}";
				}
			}
			$list[$d[0]] = $aux;
		}
		$this->Select($title,$name,$list,$required,$selected);
	}
	
	public function DBMultiSelect($title,$name,$table,$fields,$selected=array(),$conditions="dc_empresa"){
		
		if($this->db instanceof DBConnector){
			$db = $this->db;
		}else{
			global $db;
		}
		
		$conditions = $conditions=="dc_empresa"?"dc_empresa={$this->empresa} AND dm_activo = '1'":$conditions;
		$aux = $fields[0];
		for($i=1;$i<count($fields);$i++){
			$aux .= ",{$fields[$i]}";
		}
		$data = $db->doQuery($db->select($table,$aux,$conditions,array("order_by"=>"{$fields[1]}")))->fetchAll(PDO::FETCH_NUM);
		$list = array();
		foreach($data as $d){
			$aux = $d[1];
			if(count($fields)>2){
				for($i=2;$i<count($fields);$i++){
					$aux .= " {$d[$i]}";
				}
			}
			$list[$d[0]] = $aux;
		}
		$this->MultiSelect($title,$name,$list,$selected);
	}
	
	public function setConnection(DBConnector $db){
		$this->db = $db;
	}
	
	public function Button($value,$action,$class='button',$type='button'){
		echo "<button type='{$type}' class='{$class}' {$action} >{$value}</button>";
	}
	
	public function End($submit="Enviar",$class="button"){
		echo "
		<br class='clear' />
		<div class='center'>
			<input type='submit' class='{$class}' value='{$submit}' />
		</div>
		</fieldset>
		</form>
		";
	}
        public function End2($submit="Enviar",$class="button",$style=''){
		echo "
		<br class='clear' />
		<div class='center'>
			<input type='submit' class='{$class}' style='{$style}' id='{$submit}' value='{$submit}' />
		</div>
		</fieldset>
		</form>
		";
	}
        public function End3($submit="Enviar",$class="button"){
		echo "
		<br class='clear' />
		<div class='center'>
			<input type='submit' class='{$class}' id='{$submit}' value='{$submit}' />
		</div>
		</fieldset>
		</form>
		";
	}
        
        public function EndSB(){
		echo "
		<br class='clear' />
		</fieldset>
		</form>
		";
	}
}
?>