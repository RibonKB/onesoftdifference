<?php
/**
*	Clase encargada de realizar las operaciones sobre la base de datos, contiene métodos de conexión, inserción y selección de registros.
*
*	@author Tomás Lara Valdovinos
*	@version 0.1
**/
class DBConnector{
private $user;
private $server;
private $conn; 
private $bd;
private $on_transaction = false;

/**
*	Construye una nueva intancia de conexión con la Base de Datos y resguarda los datos de conexión.
*
*	@param array $config Array asociativo conteniendo la información de conexion a la base de datos.
*	@param string $config['server'] Servidor al cual se conectará.
*	@param string $config['user'] Usuario utilizado para realizar la conexión.
*	@param string $config['password'] Contraseña utilizada por el usuario a conectarse.
*	@param string $config['database'] Base de datos a la cual se conectará.
*	
**/
public function __construct($config){
	$this->user = $config['user'];
	$this->server = $config['server'];
	//Por alguna extraña razon no me deja inicializar la conexión directamente en la variable $this->conn, por lo que lo realizo
	//en una variable local y luego $this->conn pasa a ser un puntero a esa configuración.
	$tmpconn = mysql_connect($this->server,$this->user,$config['password']);
	$this->conn =& $tmpconn;
	if(!$this->conn)
	{
		$this->error("Se produjo un error al intentar conectarse con la base de datos");
	}
	mysql_query("SET NAMES 'utf8'");
	$this->cambiarBD($config['database']);
}

/**
*	Al destruir el objeto de tipo DBConnector se cierra la conexión a la base de datos hecha por el conector principal.
*	De esta manera se ahorra el cerrar la conexión cada vez que se acceda a la base de datos.
**/
public function __destruct(){
	if($this->on_transaction){
		$this->rollback();
	}
	
	$this->close();
}

/**
*	Devuelve el usuario conectado actualmente a la base de datos.
*	
*	@return string El nombre de usuario de la base de datos en la actual conexión
**/
public function getUser(){
	return $this->user;
}

/**
*	Retorna el servidor en el cual se está conectado en este momento.
*
*	@return string Servidor al que se está conectado actualmente.
**/
public function getServer(){
	return $this->server;
}

/**
*	Retorna la base de datos a la que se está conectado actualmente.
*
*	@return string Base de datos actualmente seleccionada.
**/
public function getDB(){
	return $this->bd;
}

/**
*	Genera una nueva conexión con la base de datos usando los nuevos datos de ingreso
*
*	@param string $config['server'] Servidor al cual se conectará.
*	@param string $config['user'] Usuario utilizado para realizar la conexión.
*	@param string $config['password'] Contrase$ntilde;a utilizada por el usuario a conectarse.
**/
public function reconnect($config){
	$this->user = $config['user'];
	$this->server = $config['server'];
	//Por alguna extraña razon no me deja inicializar la conexión directamente en la variable $this->conn, por lo que lo realizo
	//en una variable local y luego $this->conn pasa a ser un puntero a esa configuración.
	$tempconn = @mysql_connect($this->server,$this->user,$config['password']);
	$this->conn =& $tempconn;
	if(!$this->conn){
		$this->error("Se produjo un error al intentar conectarse al servidor de la base de datos");
	}
}

/**
*	Cambia la base de datos a la cual se está conectado.
*
*	@param string $bd Nombre de la base de datos a conectarse
*
**/
public function cambiarBD($bd){
	$this->bd = $bd;
	if(!mysql_select_db($this->bd,$this->conn)){
		$this->error("Base de datos seleccionada no existe");
	}
}

/**
*	Cierra la conexión actual con la base de datos.
**/
public function close(){
	if(!mysql_close($this->conn)){
		$this->error("No se pudo desconectar de la base de datos");
	}
}

/**
*	Realiza una consulta a la base de datos.
*
*	@param string $query Un string con la consulta que se hará a la base de datos.
*	@return query_resource Retorna los resultados de la query.
**/
public function query($query){
global $idUsuario;

	/*if($idUsuario == 2)
	echo("<pre><br>$query<hr></pre>");exit;/**/
	
	$result = mysql_query($query,$this->conn);
	if(!$result){
		$this->error("Mysql error: ".mysql_error());
	}
	return $result;
}

/**
*	Inserta registros en una tabla dentro de la base de datos
*
*	@param string $tabla La tabla en la cual se hará la inserción
*	@param string $campos [opcional]Los campos de la tabla en la que se realizarán los cambios (separados por comas)
*	@param string $valores valores correctamente formateados para insertar en la base de datos.
**/
public function insert($tabla,$valores){
	// Si los campos son definidos formatearlos para la query
	// en caso de no indicar campos se considera que se insertarán en todos
	$campos = "";
	$val = "";
	foreach($valores as $i => $v){
		$campos .= ",{$i}";
		if(in_array(substr($i,0,3),array("dg_","dm_"))){
			$v = "'".mysql_real_escape_string($v,$this->conn)."'";
		}
		$val .= ",{$v}";
	}
	$campos = substr($campos,1);
	$val = substr($val,1);
	$this->query("INSERT INTO {$tabla}({$campos}) values ({$val})");

	return $this->lastId();
}

/**
*	Realiza un update en la tabla especificada.
*	en caso de que la tabla se necesiten realizar logs, se registran para no tener que hacerlo manualmente ensuciando el código.
*	
*	param string $tabla tabla en la que se hará el update.
*	param Array $campos un array conteniendo una serie de strings con los nombres de los campos para el update
*	param Array $valores La lista de datos que se cambiarán por lo antiguos, debe estar en el mismo orden en que se insertarán dependiendo de $campos
*	param string [$condiciones] las condiciones que se utilizarán como criterio para el update (nota: si no se especifica todas las tuplas serán modificadas)
*	param mixed $id solo debe ser incluido en caso de que la tabla del update tiene una tabla de logs, si no se especifica no se realizarán backups ni logs aunque haya una tabla para hacerlo.
**/
public function update($tabla,$valores,$condiciones="true"){
	$edit = "";
	foreach($valores as $i => $v){
		if(in_array(substr($i,0,3),array("dg_","dm_"))){
			$v = "'".mysql_real_escape_string($v,$this->conn)."'";
		}
		$edit .= ",{$i}={$v}";
	}
	$edit = substr($edit,1);
	$this->query("UPDATE {$tabla} SET {$edit} WHERE {$condiciones}");
}

/**
*	Realiza un select usando la base de datos actual.
*
*	<b>estructura de $opciones</b><br /><br />
*	$opciones = array(<br />
*		'order_by' => campo,<br />
*		'order_dir' => ASC|DESC,<br />
*		'limit' => cantidad de registros(int),<br />
*		'limit_start' => limite inferior(int) (si vacio se considera desde el inicio)
*	);
*		
*
*	@param string $tabla tabla en la que se realizará la selección.
*	@param string $campos [opcional] campos de la selección (separados por coma).
*	@param string $condiciones [opcional]Las condiciones de la seleccion.
*	@param array $opciones opciones de la seleccion ORDER BY, LIMIT.
*	@return array Retorna un array asociativo con los datos de la selección.
**/
public function select($tabla,$campos="*",$condiciones="",$opciones=array(),$index=MYSQL_ASSOC){
		
		//formato básico de un select
		$query = "SELECT ".$campos." FROM {$tabla}";
		
		//se insertan las condiciones en caso de haber
		if($condiciones != "")
		{
			$query .= " WHERE ".$condiciones;
		}
		
		if(isset($opciones['group_by'])){
			$query .= " GROUP BY ".$opciones['group_by'];
		}
		
		if(isset($opciones['having']) && $opciones['having'] != ''){
			$query .= " HAVING ".$opciones['having'];
		}
		//se ordenarán los registros? por cual campo se hará?
		if(isset($opciones['order_by']))
		{
			$query .= " ORDER BY ".$opciones['order_by'];
			if(isset($opciones['order_dir']))
			{
				$query .= " ".strtoupper($opciones['order_dir']);
			}
		}
		
		//especifica el límite de registros que se mostrarán, indicando limite inferior y cantidad de registros ...
		if(isset($opciones['limit_start']) && isset($opciones['limit']))
		{
			$query .= " LIMIT ".$opciones['limit_start'].", ".$opciones['limit'];
		}
		// ... o indicando el primer registro como limite inferior
		elseif(isset($opciones['limit']))
		{
			$query .= " LIMIT ".$opciones['limit'];
		}
		//convierte los registros obtenidos en un array para luego retornarlo
	 
	
		return $this->selectToArray($this->query($query),$index);
}

/**
*	realiza una selección en la tabla similar a select(), pero recibe los campos en un array
*
*	@param string $tabla Nombre de la tabla en la que se realiza la selección
*	@param array $campos Array con los campos que estarán en la selección
*	@param string $condiciones Las condiciones de la selección
**/
public function arraySelect($tabla, $campos, $condiciones){
	$c = implode(',',$campos);
	$res = $this->select($tabla,$c,$condiciones);
	return $res;
}

/**
*	Retorna los resultados de una consulta SELECT como una Array bidimensional asociativo.
*
*	@param query_resource $result el resultado de la query [p.e. la retornada por query()]
*	@return array Retorna un array asociativo con los resultados de la query
**/
public function selectToArray($result,$index=MYSQL_ASSOC){
	$results = array();
	while($res = mysql_fetch_array($result,$index)){
		$results[] = $res;
	}
	return $results;
}

/**
*	retorna el numero del ultimo error registrado por MySQL
*
*	@return int Numero del último error generado por MYSQL
**/
public function error_number(){
	if($this->conn)
		{
			return @mysql_errno($this->conn);
		}
		else
		{
			return @mysql_errno();
		}
}

/**
*	retorna el mensaje MySQL generado por el ultimo error MYSQL
*
*	@return string Mensaje del error del último error generado MYSQL
**/
public function error_string(){
	if($this->conn)
		{
			return @mysql_error($this->conn);
		}
		else
		{
			return @mysql_error();
		}
}

/**
*	Muestra un error MySQL en la interfaz del usuario con los datos del ultimo error registrado por MySQL
*
*	@param string $mensaje Mensaje [detalle] del error personalizado a mostrar
**/
public function error($mensaje=""){
	global $error_man;
	
	if(!is_object($error_man)){
		require_once("Error_class.php");
		$error_man = new Error();
	}
	
	$err_data = array(
		"number" => $this->error_number(),
		"string" => $this->error_string(),
		"mensaje" => $mensaje
		);
	if($this->on_transaction){
		$this->rollback();
	}
	$error_man->show_fatal_error("Error MYSQL",$err_data);
}

/**
* retorna el numero de columnas devueltas por la query especificada
*
*	@param query_resource $query el resultado de la query se contarán su numero de columnas
*	@return int El número de columnas
**/
public function num_rows($query){
	return mysql_num_rows($query);
}

/**
*	devuelve el numero de columnas afectadas con la ultima query
*
*	@return int El número de columnas afectadas
**/
public function affected_rows(){
	return mysql_affected_rows($this->conn);
}

/**
*	comprueba si la tabla especificada existe en la base de datos
*
*	@param string $tabla El nombre de la tabla a comprobar si existe
**/
public function tablaExiste($tabla){
	$r = $this->query("SHOW tables FROM $this->bd");
	while($t = mysql_fetch_array($r)){
		if($tabla == $t[0])
			return true;
	}
	return false;
}

/**
*	Inserta en la tabla de eventos de la tabla especificada los cambios realizados sobre los campos recibidos por parámetro.
*
*	@param string $tabla El nombre de la tabla que generó un evento
*	@param array $campos array con los campos afectados
*	@param mixed $id El id de la tupla afectada en la tabla
*	@param array(matriz) $old Array con los registros en tuplas antiguos
*	@param array $valores Array con los nuevos valores insertados
**/
public function registrarEvento($tabla,$campos,$id,$old,$valores)
{
	global $idUsuario;
	$c = "";
	//para cada campo modificado comprobar si es necesario hacer log
	for($j=0;$j<count($old);$j++)
	{
		for($i=0;$i<count($campos);$i++){
				// En caso de que el registro insertado haya sido string, deben quitarsele las comillas
				$inicial = str_replace(array("'",'"'),"",$old[$j][$campos[$i]]);
				$final = str_replace(array("'",'"'),"",$valores[$i]);
			//si los valores antiguos y nuevos fueron cambiados entonces realizar log, en caso contrario no es necesario	
			if($inicial != $final){
				$c = "DEFAULT,$id,'".date("Y-m-d H:i:s")."',$idUsuario,'$inicial','$final','$campos[$i]'";
				$this->insert($tabla."_evento","",$c);
			}
		}
	}
}

/**
*	Retorna el ultimo identificador generado con auto_increment
**/
public function lastId(){
	return mysql_insert_id($this->conn);
}

/**
*	Funciones de formato
**/
public function sqlDate($formDate){
	if($formDate == '')
	return 'NULL';
	if(strlen($formDate) > 11)
	return preg_replace("/(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2})/","'$3-$2-$1 $4:$5'",$formDate);
	return preg_replace("/(\d{2})\/(\d{2})\/(\d{4})/","'$3-$2-$1 00:00'",$formDate);
}

public function dateTimeLocalFormat($sqlDate){
	return preg_replace("/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})/","$3/$2/$1 $4:$5",$sqlDate);
}

public function dateLocalFormat($sqlDate){
	return preg_replace("/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/","$3/$2/$1",$sqlDate);
}

public function start_transaction(){
	if(!$this->on_transaction){
		$this->query('START TRANSACTION');
		$this->on_transaction = true;
	}
}

public function commit(){
	if($this->on_transaction){
		$this->query('COMMIT');
		$this->on_transaction = false;
	}
}

public function rollback(){
	if($this->on_transaction){
		$this->query('ROLLBACK');
		$this->on_transaction = false;
	}
}

public function escape(&$str){
	$str = mysql_real_escape_string($str,$this->conn);
}

}
?>
