<?php

require_once("settings.php");
require_once("functions.php");

class Factory {

	private $db;
	private static $request;
	private $error_man;
	private $userData;
	private $empresa_conf;
    PRIVATE $ActiveServices = array();

	const URL_TEMPLATE = 1;
	const STRING_TEMPLATE = 2;

	protected $title = 'Indica Factory->$title';
	public static $modulo;
	public static $submodulo = null;
	public static $action;
	public static $factoryName;

    public function __construct() {
      @session_start();

      if(!isset($_SESSION['uid']) || !defined("MAIN")){
          self::setNotFound();
      }
    }

	/**
	* @return DBConnector Conexión a la base de datos por defecto del sistema
	*/
	public function getConnection(){
		if($this->db instanceof DBConnector){
			return $this->db;
		}

		global $settings;

		require_once(dirname(__FILE__).'/db-class.php');
		$this->db = new DBConnector(array(
			"user" => $settings["DBusername"],
			"password" => $settings["DBpassword"],
			"server" => $settings["DBserver"],
			"dbname" => $settings["DBbasedatos"]
		));

		return $this->db;

	}

	/**
	*
	*/
	public static function getRequest(){

		if(self::$request instanceof stdClass){
			return self::$request;
		}

		$r = new stdClass();

		foreach($_GET as $i => $v){
			$r->$i = $v;
		}

		foreach($_POST as $i => $v){
			$r->$i = $v;
		}

		self::$request = $r;

		return self::$request;
	}

    public static function getRequestURI(){
      $clean_request = clone self::$request;
      unset($clean_request->asinc);
      return $_SERVER['REQUEST_URI'].'?'.http_build_query(get_object_vars($clean_request));
    }

	/**
	*
	*/
	public function getView($urlTemplate, $params = array()){

		$url_h4x0r_1337_back_1234567890_rt = $urlTemplate;
		$param_seter_willsonperez_runaptget_cowPowerOnTheFloor = $params;
		unset($urlTemplate,$params);

		ob_start();
		extract($param_seter_willsonperez_runaptget_cowPowerOnTheFloor);
		include(dirname(__FILE__).'/../templates/'.$url_h4x0r_1337_back_1234567890_rt.'.html.php');

		$return_template = ob_get_contents();
		ob_clean();

		return $return_template;
	}

	/**
	*
	*/
	public function getFullView($contentData, $params = array(), $type = self::URL_TEMPLATE){
		if($type === self::URL_TEMPLATE){
			$content = $this->getView($contentData, $params);
		}else if($type === self::STRING_TEMPLATE){
			$content = $contentData;
		}else{
			throw new Exception('INVALID_TYPE_TEMPLATE');
		}

		return $this->getView('fullView',array(
			'content' => $content,
			'title' => $this->title
		));
	}

	/**
	*
	*/
	public function getFullViewWOPanes($contentData, $params = array(), $type = self::URL_TEMPLATE){
		if($type === self::URL_TEMPLATE){
			$content = $this->getView($contentData, $params);
		}else if($type === self::STRING_TEMPLATE){
			$content = $contentData;
		}else{
			throw new Exception('INVALID_TYPE_TEMPLATE');
		}

		return $this->getView('fullViewWOpanes',array(
			'content' => $content,
			'title' => $this->title
		));
	}

	/**
	*
	*/
	public function getOverlayView($contentData, $params = array(), $type = self::URL_TEMPLATE){
		if($type === self::URL_TEMPLATE){
			$content = $this->getView($contentData, $params);
		}else if($type === self::STRING_TEMPLATE){
			$content = $contentData;
		}else{
			throw new Exception('INVALID_TYPE_TEMPLATE');
		}

		return $this->getView('overlayView',array(
			'content' => $content,
			'title' => $this->title
		));
	}

	/**
	*
	*/
	public function getFormView($urlTemplate, $params = array(), $withDB = true){
		require_once(dirname(__FILE__).'/form-class.php');

		$form = new Form($this->getEmpresa());
		if($withDB){
			$form->setConnection($this->getConnection());
		}

		$params['form'] = $form;

		return $this->getView($urlTemplate,$params);
	}

	/**
	*
	*/
    public function getUserData() {

        if ($this->userData instanceof stdClass) {
			return $this->userData;
		}

		$dc_usuario = intval($_SESSION['uid']);
		$db = $this->getConnection();
		$userData = $db->prepare($db->select('tb_usuario us
										 JOIN tb_funcionario fu ON fu.dc_funcionario = us.dc_funcionario', 'us.dg_usuario,us.dc_usuario,us.dc_empresa,us.dc_usuario,us.dm_cambio_pass,fu.dc_cargo,fu.dc_ceco,
										 fu.dc_funcionario,fu.dg_rut,fu.dg_nombres,fu.dg_ap_paterno,fu.dg_ap_materno', 'us.dc_usuario = ?'));
        $userData->bindValue(1, $dc_usuario, PDO::PARAM_INT);
		$db->stExec($userData);

		$this->userData = $userData->fetch(PDO::FETCH_OBJ);

		return $this->userData;
	}

	/**
	*
	*/
	public function getEmpresa(){
		return $this->getUserData()->dc_empresa;
	}

	/**
	*
	*/
	public function getParametrosEmpresa(){
		if($this->empresa_conf instanceof stdClass){
			return $this->empresa_conf;
		}
		$this->empresa_conf = $this->getConnection()->getRowById('tb_empresa_configuracion',$this->getEmpresa(),'dc_empresa');
		return $this->empresa_conf;
	}

	/**
	* @return Error -
	*/
	public function getErrorMan(){
		if($this->error_man instanceof Error){
			return $this->error_man;
		}

		require_once(dirname(__FILE__).'/Error_class.php');
		$this->error_man = new Error();

		return $this->error_man;
	}

	/**
	*
	*/
	public static function validaUrlParam($param){
		if(preg_match('/[.\/]/',$param)){
			self::setNotFound();
		}
	}

	/**
	*
	*/
	public static function validaFolder($folder){
		if(!is_dir($folder)){
			self::setNotFound();
		}
	}

	/**
	*
	*/
	public static function getFactoryInstance($folder,$class){

		self::validaFolder($folder);

		$file = $folder.'/'.$class.'Factory.php';
		$class = $class.'Factory';

		if(!file_exists($file)){
			self::setNotFound();
		}

		require_once($file);

		if(!class_exists($class)){
			echo 'Error de programación: Define la clase <b>'.$class.'</b> en <b>'.$folder.'</b>';
			exit;
		}

		return new $class();
	}

	/**
	*
	*/
	public static function callAction($action, $factory){
		if(!preg_match('/^[a-z0-9]+$/i',$action)){
			self::setNotFound();
		}

		$action = $action.'Action';

		if(!method_exists($factory, $action)){
			self::setNotFound();
		}

		$factory->$action();
	}

	/**
	*
	*/
	private static function setNotFound(){
		header("HTTP/1.0 404 Not Found");
		header("Status: 404 Not Found");
		exit;
	}

	/**
	*
	*/
	public function initFunctionsService(){
		require_once(__DIR__.'/Functions.class.php');
		Functions::setFactory($this);
	}

    /**
     *
     * @param string $serviceId
     * @return FactoryServiceInterface
     */
    public function getService($serviceId){

        if(isset($this->ActiveServices[$serviceId]) and $this->ActiveServices[$serviceId] instanceof FactoryServiceInterface){
            return $this->ActiveServices[$serviceId];
        }

        $services = array(
            'ComprobanteContable' => __DIR__ . '/../sites/services/ComprobanteContableService.php',
            'LogisticaStuff' => __DIR__ . '/../sites/services/LogisticaStuffService.php',
            'DocumentCreation' => __DIR__ . '/../sites/services/DocumentCreationService.php',
            'IMAP' => __DIR__ . '/../sites/services/IMAPService.php',
            'CorreoManager' => __DIR__ . '/../sites/modulosExternos/helpdesk/classes/CorreoManagerService.php',
            'ListadosFuncionarios' => __DIR__ . '/../sites/services/ListadosFuncionariosService.php',
            'SMTP' => __DIR__ . '/../sites/services/SMTPService.php',
            'AdministradorTarea'=>__DIR__ . '/../sites/services/AdministradorTareaService.php',
						'CentralizacionFacturaCompra' => __DIR__ . "/../sites/contabilidad/informes/CentralizacionFacturaCompraService.php",
						'CentralizacionNotaCreditoProveedor' => __DIR__ . "/../sites/contabilidad/informes/CentralizacionNotaCreditoProveedorService.php",
						'CentralizacionMargenes' => __DIR__ . "/../sites/contabilidad/informes/CentralizacionMargenesService.php",
						'CentralizacionMargenesFacturaVenta' => __DIR__ . "/../sites/contabilidad/informes/CentralizacionMargenesFacturaVentaService.php",
						'CentralizacionMargenesNotaCredito' => __DIR__ . "/../sites/contabilidad/informes/CentralizacionMargenesNotaCreditoService.php",
						'CentralizacionFacturaVenta' =>	__DIR__ . "/../sites/contabilidad/informes/CentralizacionFacturaVentaService.php",
						'CentralizacionNotaCredito' => __DIR__ . "/../sites/contabilidad/informes/CentralizacionNotaCreditoService.php",
        );

        require_once $services[$serviceId];
        $class = $serviceId.'Service';

        $instance = new $class();
        $instance->setFactory($this);

        $this->ActiveServices[$serviceId] = $instance;

        return $instance;
    }

	/**
	*
	*/
	public static function buildUrl($factory, $modulo, $action = null, $submodulo = null, $params = array()){
		$data = array(
			'factory' => $factory,
			'modulo' => $modulo
		);
		if($action){
			$data['action'] = $action;
		}
		if($submodulo){
			$data['submodulo'] = $submodulo;
		}
		$data = array_merge($data, $params);
		return 'god.php?'.http_build_query($data);
	}

	/**
	*
	*/
	public static function buildActionUrl($action, $params = array()){
		return self::buildUrl(self::$factoryName, self::$modulo, $action, self::$submodulo, $params);
	}

	/**
	*
	*/
	protected function getTemplateURL($template){
		if(self::$submodulo)
			return self::$modulo.'/'.self::$submodulo.'/'.self::$factoryName.'/'.$template;
		else
			return self::$modulo.'/'.self::$factoryName.'/'.$template;
	}

    /**
     *
     */
    protected static function getJavascriptUrl($script){
        if(self::$submodulo)
          return 'jscripts/sites/'.self::$modulo.'/'.self::$submodulo.'/'.$script.'.js';
        else
          return 'jscripts/sites/'.self::$modulo.'/'.$script.'.js';
    }

    /**
     * Funcion getAutocompleterClass
     * Retorna la inclusión de autocomplete.
     */

     public static function getAutocompleterClass(){
         require_once( __DIR__ . '/Autocompleter.class.php' );
     }

	//Borrar en producción
	/**
	*
	*/
	public static function debugMethods($class, $is_object = true){

		if($is_object){
			$class = get_class($class);
		}

		$methods = get_class_methods($class);
		echo '<div style="border:1px dashed #444;background-color:#F6F6F6;padding:15px;">';
		echo "<div class='info'>Métodos Clase <b>{$class}</b></div>";
		foreach($methods as $m){
			echo '<b>'.$m.'</b><ul>';
			$r = new ReflectionMethod($class, $m);
			$params = $r->getParameters();
			if(!count($params)){
				echo '<li><i>Sin parámetros</i></li>';
			}
			foreach ($params as $param) {
				//$param is an instance of ReflectionParameter
				echo '<li>';
				echo $param->getName();
				if($param->isDefaultValueAvailable())
					echo $param->isOptional()?' [= '.htmlentities(print_r($param->getDefaultValue(),true)).']':' (*)';
				else
					echo $param->isOptional()?' [= <i>NO DISPONIBLE</i>]':' (*)';
				echo '</li>';
			}
			echo '</ul>';
		}
		echo '</div>';
	}

    public static function debug($var){
      echo '<pre>';
      var_dump($var);
      echo '</pre>';
    }

    /**
     *
     *
     * @param type $tipo puede ser Select, insert o Update
     *
     * @param type $tb la tabla a la que se quiere acceder
     *
     * @param type $campos los campos 'campo1,campo2'. En caso de $tipo=delete, el parametro $campos
     *                     equivale a las condiciones.
     *
     * @param type $condicion las condiciones de la query
     *
     * @param type $bind deben ser ingresados como array asociativo $Valor_A_ingresar => $Tipo('INT','STR'....etc)
     *                   el $Valor_A_ingresar debe tener un distintivo y un separador ej: 'a::'.$Valor_A_ingresar,
     *                   el separador debe ser siempre '::', se agrega un caracter antes de este para evitar
     *                   confucion del sistema ante valores iguales.
     *
     * @param type $last_insert En caso de insertar datos y querer obtener el 'lastinsertid()' de algun campo,
     *                          se debe colocar el nombre del campo en esta variable, y retornara su lasinsert.
     *
     * @return type : Si se hace un 'insert' retornará la consulta; Si es un insert y se le da valor(nombre del campo)
     *                al $last_insert, retornará el lastInsertId(), del campo indicado.
     *
     *
     */
    public function coneccion($tipo, $tb, $campos, $condicion = NULL, $bind = NULL, $last_insert = NULL, $t = 0,$others=NULL) {
        $db = $this->getConnection();
        $consulta = '';
            $consulta = $db->prepare($db->$tipo($tb, $campos, $condicion,$others));
            $i=0;
            if ($bind) {
                foreach ($bind as $v => $tipo2) {
                    $va = explode('::', $v);
                    $valor = $va[1];
                    $i++;
                    if ($tipo2 == 'INT') {
                        $consulta->bindValue($i, $valor, PDO::PARAM_INT);
                    }
                    if ($tipo2 == 'STR') {
                        $consulta->bindValue($i, $valor, PDO::PARAM_STR);
                    }
                    if ($tipo2 == 'BOOL') {
                        $consulta->bindValue($i, $valor, PDO::PARAM_BOOL);
                    }
                }
            }
        //var_dump($consulta);
        $db->stExec($consulta);
        if ($tipo == 'select') {
            if($t){
                $consulta=$consulta->fetchAll(PDO::FETCH_OBJ);
            }
            return $consulta;
        } else if ($tipo == 'insert') {
            if ($last_insert) {
                return $lst = $db->lastInsertId($last_insert);
            }
        }
    }

    public function UNION_SELECT($tb_1, $campos_1, $campos_2 = NULL, $tb_2 = NULL, $condicion_1 = NULL, $condicion_2 = NULL, $condicion = 0, $fetch = 0) {
        $db = $this->getConnection();
        $consulta = '';
        if ($condicion_1 && $condicion_2) {
                $consulta = $db->prepare("(" . $db->select($tb_1, $campos_1, $condicion_1)
                        . ")UNION (" .
                        $db->select($tb_2 ? $tb_2 : $tb_1, $campos_2 ? $campos_2 : $campos_1, $condicion_2) . ")");
        } elseif ($condicion_1 && !$condicion_2) {
            if ($condicion == 2) {
                $consulta = $db->prepare("(" . $db->select($tb_1, $campos_1, $condicion_1)
                        . ")UNION (" .
                        $db->select($tb_2 ? $tb_2 : $tb_1, $campos_2 ? $campos_2 : $campos_1, $condicion_1) . ")");
            } elseif ($condicion == 1) {
                $consulta = $db->prepare("(" . $db->select($tb_1, $campos_1, $condicion_1)
                        . ")UNION (" .
                        $db->select($tb_2 ? $tb_2 : $tb_1, $campos_2 ? $campos_2 : $campos_1) . ")");
            }
        } elseif (!$condicion_1 && $condicion_2) {
            if ($condicion == 1) {
                $consulta = $db->prepare("(" . $db->select($tb_1, $campos_1)
                        . ")UNION (" .
                        $db->select($tb_2 ? $tb_2 : $tb_1, $campos_2 ? $campos_2 : $campos_1, $condicion_2) . ")");
            } elseif ($condicion == 2) {
                $consulta = $db->prepare("(" . $db->select($tb_1, $campos_1, $condicion_2)
                        . ")UNION (" .
                        $db->select($tb_2 ? $tb_2 : $tb_1, $campos_2 ? $campos_2 : $campos_1, $condicion_2) . ")");
            }
        } else {
                $consulta = $db->prepare("(" . $db->select($tb_1, $campos_1)
                        . ")UNION (" .
                        $db->select($tb_2 ? $tb_2 : $tb_1, $campos_2 ? $campos_2 : $campos_1) . ")");
        }
        $db->stExec($consulta);
        if ($fetch) {
            $consulta = $consulta->fetchAll(PDO::FETCH_OBJ);
        }
        return $consulta;
    }

    public function Modulo_Mantencion() {
        $this->getErrorMan()->showAviso('El Moldulo está en mantencion, Disculpe las molestias');
        exit;
    }

}

?>
