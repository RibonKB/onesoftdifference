<?php
session_start();

if(!isset($_SESSION['uid']) || !defined("MAIN")){
	header("HTTP/1.0 404 Not Found");
	header("Status: 404 Not Found");
	exit;
}

require_once("settings.php");

// Genera el objeto encargado de mostrar errores
require_once("Error_class.php");
$error_man = new Error();

if(!isset($_POST['asinc'])){
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
    	$pymerp['ajax'] = true;
	}  else {
		$pymerp['ajax'] = false;
	}
}

require_once("db-class.php");
$datosdb = array(
	"user" => $settings["DBusername"],
	"password" => $settings["DBpassword"],
	"server" => $settings["DBserver"],
	"dbname" => $settings["DBbasedatos"]
);
$db = new DBConnector($datosdb);

//Se crea otra referencia al conector de la base de datos, para los que confunden DataBase con BaseDatos
$bd =& $db;

//Se incluye el archivo con funciones
require_once("functions.php");

$userdata = $db->doQuery($db->select("(SELECT * FROM tb_usuario WHERE dc_usuario={$_SESSION['uid']} AND dm_activo = 1) us
JOIN tb_funcionario fu ON fu.dc_funcionario = us.dc_funcionario",
'us.dg_usuario,us.dc_empresa,us.dc_usuario,us.dm_cambio_pass,fu.dc_funcionario,fu.dg_rut,fu.dg_nombres,fu.dg_ap_paterno,fu.dg_ap_materno'))->fetch(PDO::FETCH_ASSOC);

if($userdata === false){
	die();
}

if($userdata['dm_cambio_pass'] == '1' && !isset($cambiar_pass))
	echo('<script type="text/javascript">	document.location.href="cambiar_pass.php?redirect";	</script>');

$empresa =& $userdata['dc_empresa'];
$efectos = 1;
$idUsuario =& $userdata['dc_usuario'];

$empresa_conf = $db->doQuery($db->select('tb_empresa_configuracion','*',"dc_empresa={$empresa}"))->fetch(PDO::FETCH_ASSOC);

?>