<?php
session_start();

if(!isset($_SESSION['uid'])){
	header("Location:login.php");
}

//impide la llamada directa del archivo
if(!defined("MAIN")){
	die("Inicialización directa del archivo no permitida");
}

require_once("settings.php");

// Genera el objeto encargado de mostrar errores
require_once("Error_class.php");
$error_man = new Error();

// Generar el objeto que se conecta con la base de datos.
require_once("DBConnector.php");
$datosdb = array(
	"user" => $settings["DBusername"],
	"password" => $settings["DBpassword"],
	"server" => $settings["DBserver"],
	"database" => $settings["DBbasedatos"]
);
$db = new DBConnector($datosdb);

//Se crea otra referencia al conector de la base de datos, para los que confunden DataBase con BaseDatos
$bd =& $db;

//Se incluye el archivo con funciones
require_once("functions.php");


//Los datos del usuario conectado
$userdata = $db->select(
"tb_usuario us,tb_funcionario fu",
"us.dg_usuario,
us.dc_empresa,
us.dc_usuario,
us.dm_cambio_pass,
fu.dc_funcionario,
fu.dg_rut,
fu.dg_nombres,
fu.dg_ap_paterno,
fu.dg_ap_materno"
,"us.dc_funcionario = fu.dc_funcionario AND us.dc_usuario={$_SESSION['uid']}");
$userdata = $userdata[0];
// Completa la información correspondiente a los datos y configuración de la empresa del usuario

if($userdata['dm_cambio_pass'] == '1' && !isset($cambiar_pass)){
?>
<script type="text/javascript">
	document.location.href="cambiar_pass.php?redirect";
</script>
<?php
}

$empresa =& $userdata['dc_empresa'];
$efectos = 1;
$idUsuario =& $userdata['dc_usuario'];

// Configuración de la empresa
$empresa_conf = $db->select(
"tb_empresa_configuracion",
"*",
"dc_empresa = {$empresa}");
$empresa_conf = $empresa_conf[0];

?>