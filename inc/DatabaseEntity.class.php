<?php
require_once('db-class.php');

class DatabaseEntity{
	
	protected $db;
	protected $tableName;
	protected $idField;
	
	public function __construct(DBConnector $db){
		$this->db = $db;
	}
	
	protected function getById($id){
		$this->db->getRowById($this->tableName,$id,$this->idField);
	}
	
}