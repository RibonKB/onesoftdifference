<?php
	define("MAIN",1);
	$cambiar_pass = true;
	require_once("inc/global.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cambio de contraseña</title>
<link type="text/css" href="styles/main.css" rel="stylesheet" />
<script type="text/javascript" src="jscripts/lib/jquery.tools.2.min.js" ></script>
<script type="text/javascript" src="jscripts/md5-min.js"></script>
<script type="text/javascript" src="jscripts/ch_pass.js"></script>
</head>

<body>

<div id="header">
<div class="left">
<table>
	<tr>
		<td><img src="images/logo.gif" alt="PYMEERP" /></td>
		<td><div class="country"></div></td>
	</tr>
</table>
</div>

<div class="right">
	<img src="images/help.png" alt="?" title="Ayuda" width="25"  />
	<a href="index.php"><img src="images/home.png" alt="Inicio" title="Ir al inicio" width="25" /></a>
	<a href="logout.php"><img src="images/logout.png" alt="Logout" title="Desconectarse" width="25" /></a>
</div>

<br class="clear" />
</div>

<div id="secc_bar">
	Cambio de contraseña
</div>

<?php
	if(isset($_GET['redirect'])){
		$error_man->showWarning("Su contraseña debe ser cambiada para poder acceder al sistema, indique una nueva  a continuación");
	}
?>
<br /><br /><br /><br /><br /><br />
<?php 
$email=$db->select('tb_funcionario fu LEFT JOIN tb_usuario us ON us.dc_funcionario=fu.dc_funcionario','dg_email',"us.dc_usuario={$idUsuario}");

if(!$email[0]['dg_email'] || $email[0]['dg_email'] == ''):
    ?>
<div id="other_options">
	<ul>
		<li><a href="ingresa_mail.php" class="loader">Ingresar Correo</a></li>
	</ul>
</div>
<?php
endif;
?>
	<div class="panes">
		<div class="info">
			<strong>Ingrese los datos requeridos para el cambio de su contraseña</strong>
		</div>
		<div id="log_results"></div>
		<div align="center">
		<form action="sites/cambiar_pass.php" method="post" id="ch_pass">
			<fieldset>
				<label>Contraseña actual<br />
				<input type="password" class="inputtext" name="old_pass" size="30" maxlength="50" />
				</label><br />
				
				<label>Nueva contraseña<br />
				<input type="password" class="inputtext" name="pass1" size="30" maxlength="50" />
				</label><br />
				
				<label>Repetir Contraseña<br />
				<input type="password" class="inputtext" name="pass2" size="30" maxlength="50" />
				</label><br /><br />
				
				<input type="submit" class="editbtn" value="Cambiar contrase&ntilde;a" />
			</fieldset>
		</form>
		</div>
</div>

</body>
</html>
