# Tomás Lara



ALTER TABLE `tb_nota_credito_proveedor_detalle` ADD `dc_bodega` INT(11) NULL , ADD INDEX (`dc_bodega`) ;
ALTER TABLE `tb_nota_credito_detalle` ADD FOREIGN KEY (`dc_cuenta_contable_centralizacion`) REFERENCES `pymerp`.`tb_cuenta_contable`(`dc_cuenta_contable`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `tb_nota_credito_detalle` ADD `dc_cuenta_contable_centralizacion` INT(11) NULL DEFAULT NULL AFTER `dq_total`, ADD INDEX (`dc_cuenta_contable_centralizacion`) ;

ALTER TABLE `tb_nota_credito_proveedor` ADD `dc_factura` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `dc_proveedor`, ADD INDEX (`dc_factura`) ;
ALTER TABLE `tb_nota_credito_proveedor` ADD FOREIGN KEY (`dc_factura`) REFERENCES `pymerp`.`tb_factura_compra`(`dc_factura`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Estructura de tabla para la tabla `tb_centralizacion_compra`
--

CREATE TABLE IF NOT EXISTS `tb_centralizacion_compra` (
`dc_centralizacion` int(10) unsigned NOT NULL,
  `dq_centralizacion` bigint(20) unsigned NOT NULL,
  `dc_comprobante_contable` int(11) NOT NULL,
  `df_emision` datetime NOT NULL,
  `df_creacion` datetime NOT NULL,
  `dc_usuario_creacion` int(11) NOT NULL,
  `dc_empresa` int(11) NOT NULL,
  `dm_nula` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tb_centralizacion_compra`
 ADD PRIMARY KEY (`dc_centralizacion`), ADD KEY `dc_comprobante_contable` (`dc_comprobante_contable`);

ALTER TABLE `tb_centralizacion_compra`
MODIFY `dc_centralizacion` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `tb_centralizacion_compra`
ADD CONSTRAINT `tb_centralizacion_compra_ibfk_1` FOREIGN KEY (`dc_comprobante_contable`) REFERENCES `tb_comprobante_contable` (`dc_comprobante`);

--
-- Estructura de tabla para la tabla `tb_centralizacion_compra_detalle_factura`
--

CREATE TABLE IF NOT EXISTS `tb_centralizacion_compra_detalle_factura` (
`dc_detalle` int(10) unsigned NOT NULL,
  `dc_centralizacion` int(10) unsigned NOT NULL,
  `dc_factura` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tb_centralizacion_compra_detalle_factura`
 ADD PRIMARY KEY (`dc_detalle`), ADD KEY `dc_centralizacion_compra` (`dc_centralizacion`), ADD KEY `dc_factura` (`dc_factura`);

ALTER TABLE `tb_centralizacion_compra_detalle_factura`
MODIFY `dc_detalle` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `tb_centralizacion_compra_detalle_factura`
ADD CONSTRAINT `tb_centralizacion_compra_detalle_factura_ibfk_1` FOREIGN KEY (`dc_centralizacion`) REFERENCES `tb_centralizacion_compra` (`dc_centralizacion`),
ADD CONSTRAINT `tb_centralizacion_compra_detalle_factura_ibfk_2` FOREIGN KEY (`dc_factura`) REFERENCES `tb_factura_compra` (`dc_factura`);

--
-- Estructura de tabla para la tabla `tb_centralizacion_compra_detalle_nota_credito`
--

CREATE TABLE IF NOT EXISTS `tb_centralizacion_compra_detalle_nota_credito` (
`dc_detalle` int(10) unsigned NOT NULL,
  `dc_centralizacion` int(10) unsigned NOT NULL,
  `dc_nota_credito` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tb_centralizacion_compra_detalle_nota_credito`
 ADD PRIMARY KEY (`dc_detalle`), ADD KEY `dc_centralizacion` (`dc_centralizacion`), ADD KEY `dc_nota_credito` (`dc_nota_credito`);

ALTER TABLE `tb_centralizacion_compra_detalle_nota_credito`
MODIFY `dc_detalle` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `tb_centralizacion_compra_detalle_nota_credito`
ADD CONSTRAINT `tb_centralizacion_compra_detalle_nota_credito_ibfk_1` FOREIGN KEY (`dc_centralizacion`) REFERENCES `tb_centralizacion_compra` (`dc_centralizacion`),
ADD CONSTRAINT `tb_centralizacion_compra_detalle_nota_credito_ibfk_2` FOREIGN KEY (`dc_nota_credito`) REFERENCES `tb_nota_credito_proveedor` (`dc_nota_credito`);


- sites\maestros\ventas\TipoFacturaCompraFactory.php
- templates\maestros\ventas\TipoFacturaCompra\editar.form.html.php
- sites\ventas\factura_compra\CentralizacionFactory.php
- templates\ventas\factura_compra\Centralizacion\asignarCuentaDetalle.form.html.php

- sites\ventas\nota_credito_proveedor\src_tipo_nota_credito.php
- sites\ventas\nota_credito_proveedor\CrearNotaCreditoFactory.php
- sites\ventas\nota_credito_proveedor\TipoNotaCreditoFactory.php
- sites\ventas\nota_credito_proveedor\cr_nota_credito.php

- inc\Functions.class.php

// - Agregar campo de validacion de rut a la tabla de configuración de empresa
ALTER TABLE `tb_empresa_configuracion` ADD `dg_preg_rut` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '/^\\d{1,2}\\.\\d{3}\\.\\d{3}-(\\d|k)$/' ;

//agregar campo de cuenta contable centralizacion al detalle de la NC de proveedor
ALTER TABLE `tb_nota_credito_proveedor_detalle` ADD `dc_cuenta_contable_centralizacion` INT NULL DEFAULT NULL , ADD INDEX (`dc_cuenta_contable_centralizacion`) ;

ALTER TABLE `tb_nota_credito_proveedor_detalle` ADD FOREIGN KEY (`dc_cuenta_contable_centralizacion`) REFERENCES `pymerp`.`tb_cuenta_contable`(`dc_cuenta_contable`) ON DELETE RESTRICT ON UPDATE RESTRICT;


- sites\ventas\factura_compra\proc\show_factura_compra.php

- /sites/logistica/AsignacionStockLibreFactory.php

- Cambiar tipos de movimiento de salida en tipos de guía de despacho manual y sin movimiento

- Agregar campo dc_movimiento_relacionado a la tabla tb_movimiento_bodega

-Indexar y relacionar movimiento

ALTER TABLE `tb_movimiento_bodega`
ADD CONSTRAINT `FK_MOVIMIENTO_BODEGA_RELACIONADO`
FOREIGN KEY (`dc_movimiento_relacionado`)
REFERENCES `pymerp`.`tb_movimiento_bodega`(`dc_movimiento`)
ON DELETE RESTRICT ON UPDATE RESTRICT;

- jscripts/sites/logistica/cr_asignacion_stock_libre.js
- templates/logistica/AsignacionStockLibre/crear.form.html.php

- sites/ventas/orden_compra/proc/crear_orden_compra.php

- sites/logistica/proc/set_traspaso_simple.php

- sites/logistica/proc/crear_ajuste_cantidad.php
- Crear relación con ajuste logistica

ALTER TABLE `tb_comprobante_ajuste_logistico`
  ADD `dc_comprobante_ajuste_logistico` INT NULL DEFAULT NULL ,
  ADD INDEX (`dc_comprobante_ajuste_logistico`) ;

ALTER TABLE `tb_comprobante_ajuste_logistico`
  CHANGE `dc_comprobante_ajuste_logistico` `dc_comprobante_ajuste_logistico` INT(11) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `tb_comprobante_ajuste_logistico`
  ADD CONSTRAINT `FK_MOVIMIENTO_AJUSTE_LOGISTICO`
  FOREIGN KEY (`dc_comprobante_ajuste_logistico`)
  REFERENCES `pymerp`.`tb_comprobante_ajuste_logistico`(`dc_comprobante`)
  ON DELETE RESTRICT ON UPDATE RESTRICT;

- Crear fecha en tabla movimientos
ALTER TABLE `tb_movimiento_bodega` ADD `df_movimiento` DATETIME NULL DEFAULT NULL AFTER `dc_empresa`;

- sites/contabilidad/informe_existencias/InformeExistenciasFactory.php
- templates/contabilidad/informe_existencias/InformeExistencias/index.form.html.php
- jscripts/sites/contabilidad/informe_existencias/main.js
- templates/contabilidad/informe_existencias/InformeExistencias/tablaInforme.html.php
- templates/contabilidad/informe_existencias/InformeExistencias/botonera.html.php

# Correción fecha guias de recepción
- sites/ventas/guia_recepcion/cr_guia_recepcion.php
- jscripts/product_manager/guia_recepcion.js
- sites/ventas/guia_recepcion/CrearGuiaRecepcionFactory.php
- Crear campo en tabla y poblar con df_fecha_emision:
ALTER TABLE `tb_guia_recepcion` ADD `df_recepcion` DATETIME NULL DEFAULT NULL AFTER `df_fecha_emision`;

# Creación de las tablas para informe de existencias

-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-11-2015 a las 12:58:31
-- Versión del servidor: 10.1.6-MariaDB-1~jessie-log
-- Versión de PHP: 5.6.13-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `pymerp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_comprobante_cierre_existencias`
--

CREATE TABLE IF NOT EXISTS `tb_comprobante_cierre_existencias` (
`dc_comprobante` int(11) unsigned NOT NULL,
  `dq_comprobante` bigint(20) unsigned NOT NULL,
  `df_emision` datetime NOT NULL,
  `df_creacion` datetime NOT NULL,
  `dc_usuario_creacion` int(11) NOT NULL,
  `dc_mes` int(11) NOT NULL,
  `dc_anho` int(11) NOT NULL,
  `dm_nula` tinyint(1) NOT NULL DEFAULT '0',
  `dc_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tb_comprobante_cierre_existencias`
--
ALTER TABLE `tb_comprobante_cierre_existencias`
 ADD PRIMARY KEY (`dc_comprobante`), ADD KEY `dc_usuario_creacion` (`dc_usuario_creacion`,`dc_empresa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tb_comprobante_cierre_existencias`
--
ALTER TABLE `tb_comprobante_cierre_existencias`
MODIFY `dc_comprobante` int(11) unsigned NOT NULL AUTO_INCREMENT;

# Detalle cierre informe de existencias

-- phpMyAdmin SQL Dump
-- version 4.2.12deb2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-11-2015 a las 13:00:23
-- Versión del servidor: 10.1.6-MariaDB-1~jessie-log
-- Versión de PHP: 5.6.13-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `pymerp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tb_detalle_cierre_existencias`
--

CREATE TABLE IF NOT EXISTS `tb_detalle_cierre_existencias` (
`dc_detalle` int(10) unsigned NOT NULL,
  `dc_comprobante` int(11) unsigned NOT NULL,
  `dc_producto` int(11) NOT NULL,
  `dc_cantidad` int(11) NOT NULL,
  `dq_valor` decimal(10,2) NOT NULL,
  `dq_pmp` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tb_detalle_cierre_existencias`
--
ALTER TABLE `tb_detalle_cierre_existencias`
 ADD PRIMARY KEY (`dc_detalle`), ADD KEY `dc_comprobante` (`dc_comprobante`,`dc_producto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tb_detalle_cierre_existencias`
--
ALTER TABLE `tb_detalle_cierre_existencias`
MODIFY `dc_detalle` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tb_detalle_cierre_existencias`
--
ALTER TABLE `tb_detalle_cierre_existencias`
ADD CONSTRAINT `tb_detalle_cierre_existencias_ibfk_1` FOREIGN KEY (`dc_comprobante`) REFERENCES `tb_comprobante_cierre_existencias` (`dc_comprobante`);

--
-- Estructura de tabla para la tabla `tb_tipo_nota_credito_proveedor_cuenta_contable`
--

CREATE TABLE IF NOT EXISTS `tb_tipo_nota_credito_proveedor_cuenta_contable` (
  `dc_tipo_nota_credito` int(11) unsigned NOT NULL,
  `dc_cuenta_contable` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tb_tipo_nota_credito_proveedor_cuenta_contable`
--
ALTER TABLE `tb_tipo_nota_credito_proveedor_cuenta_contable`
 ADD PRIMARY KEY (`dc_tipo_nota_credito`,`dc_cuenta_contable`);

 ALTER TABLE `tb_tipo_nota_credito_proveedor_cuenta_contable` ADD FOREIGN KEY (`dc_tipo_nota_credito`) REFERENCES `pymerp`.`tb_tipo_nota_credito_proveedor`(`dc_tipo`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `tb_tipo_nota_credito_proveedor_cuenta_contable` ADD FOREIGN KEY (`dc_cuenta_contable`) REFERENCES `pymerp`.`tb_cuenta_contable`(`dc_cuenta_contable`) ON DELETE RESTRICT ON UPDATE RESTRICT;
