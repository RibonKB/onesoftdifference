<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$conditions = "YEAR(df_fecha_emision) = '{$_POST['os_ann']}'";

if(isset($_POST['os_month'])){
	$month = implode("','",$_POST['os_month']);
	$conditions .= " AND MONTH(df_fecha_emision) IN ('{$month}')";
	unset($month);
}

if(isset($_POST['os_client'])){
	$cli = implode(',',$_POST['os_client']);
	$conditions .= " AND dc_cliente IN ({$cli})";
	unset($cli);
}

if(isset($_POST['os_stat'])){
	$stat = implode(',',$_POST['os_stat']);
	$conditions .= " AND dc_estado IN ({$stat})";
	unset($stat);
}

$data = $db->select("(SELECT * FROM tb_orden_servicio WHERE {$conditions}) os
LEFT JOIN (SELECT * FROM tb_estado_orden_servicio WHERE dc_empresa={$empresa}) es ON os.dc_estado = es.dc_estado
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa = {$empresa}) cl ON os.dc_cliente = cl.dc_cliente",
"os.dc_orden_servicio,os.dq_orden_servicio,es.dg_estado,os.dg_solicitante,cl.dg_razon,os.dg_requerimiento,os.dg_solucion,os.dm_estado");

if(!count($data)){
	$error_man->showAviso("No se encontraron ordenes de servicio con los filtros indicados");
	exit;
}

$no_terminados = array();
$terminados = array();
foreach($data as $os){
	if($os['dm_estado'] == '0')
		$no_terminados[] = $os['dc_orden_servicio'];
	else
		$terminados[] = $os['dc_orden_servicio'];
}



echo("<table class='tab' width='100%'>
<thead>
<tr>
	<th width='80'>Orden</th>
	<th>Estado</th>
	<th width='200'>Solicitante</th>
	<th>Cliente</th>
	<th>Requerimiento</th>
	<th>Solución</th>
</tr>
</thead>
<tbody>");

foreach($data as $os){
echo("<tr>
<td>{$os['dq_orden_servicio']}</td>
<td>{$os['dg_estado']}</td>
<td>{$os['dg_solicitante']}</td>
<td>{$os['dg_razon']}</td>
<td>{$os['dg_requerimiento']}</td>
<td>{$os['dg_solucion']}</td>
</tr>");
}

echo("</tbody>
</table>");
?>