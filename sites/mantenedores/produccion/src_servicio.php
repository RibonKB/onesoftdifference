<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Servicios</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear la mano de obra<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/produccion/proc/crear_servicio.php","cr_material");
	$form->Section();
	$form->Text("Nombre","svc_name",1);
	$form->EndSection();
	$form->Section();
	$form->Listado("Sección/Taller","svc_categoria",'tb_produccion_categoria',array('dc_categoria','dg_categoria'),1);
	$form->Text("Costo","svc_cost");
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"(SELECT * FROM tb_produccion_servicio WHERE dc_empresa={$empresa} AND dm_activo='1') s
		LEFT JOIN (SELECT * FROM tb_produccion_categoria WHERE dc_empresa={$empresa}) c ON c.dc_categoria = s.dc_categoria",
		"s.dc_servicio, s.dg_servicio, s.dq_costo, DATE_FORMAT(s.df_creacion,'%d/%m/%Y %H:%i') as df_fecha,c.dg_categoria","",
		array("order_by" => "dg_servicio")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Servicios ya agregados</caption>
	<thead>
		<tr>
			<th width="40%">Servicio</th>
			<th width="20%">Sección/Taller</th>
			<th width="10%">Costo</th>
			<th width="15%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		$l['dq_costo'] = moneda_local($l['dq_costo']);
		echo("
		<tr id='item{$l['dc_servicio']}'>
			<td>{$l['dg_servicio']}</td>
			<td>{$l['dg_categoria']}</td>
			<td align='right'>$ <b>{$l['dq_costo']}</b></td>
			<td class='center'>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/produccion/ed_servicio.php?id={$l['dc_servicio']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/produccion/del_servicio.php?id={$l['dc_servicio']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>
<script type="text/javascript">
$('#svc_cost').change(function(){
	val = parseFloat($(this).val());
	if(val)
		$(this).val(val);
	else
		$(this).val('');
});
</script>