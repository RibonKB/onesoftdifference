<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Materiales</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo material<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/produccion/proc/crear_material.php","cr_material");
	$form->Section();
	$form->Text("Nombre","mat_name",1);
	$form->Listado("Categoría","mat_categoria",'tb_produccion_categoria',array('dc_categoria','dg_categoria'),1);
	$form->EndSection();
	$form->Section();
	$form->Listado("Unidad de medida",'mat_unidad','tb_unidad_medida',array('dc_unidad_medida','dg_unidad_medida'));
	$form->Listado("Marca",'mat_marca','tb_marca',array('dc_marca','dg_marca'));
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"(SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa} AND dm_activo='1') m
		LEFT JOIN (SELECT * FROM tb_unidad_medida WHERE dc_empresa = {$empresa}) u ON u.dc_unidad_medida = m.dc_unidad_medida
		LEFT JOIN (SELECT * FROM tb_marca WHERE dc_empresa={$empresa}) mr ON mr.dc_marca = m.dc_marca
		LEFT JOIN (SELECT * FROM tb_produccion_categoria WHERE dc_empresa = {$empresa}) c ON c.dc_categoria = m.dc_categoria",
		"m.dc_material, m.dg_material, DATE_FORMAT(m.df_creacion,'%d/%m/%Y %H:%i') as df_fecha, u.dg_unidad_medida,
		c.dg_categoria,mr.dg_marca",'',
		array("order_by" => "dg_material")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Materiales ya agregados</caption>
	<thead>
		<tr>
			<th width="20%">Material</th>
			<th width="20%">Categoria</th>
			<th width="15%">Marca</th>
			<th width="10%">Unidad de medida</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_material']}'>
			<td>{$l['dg_material']}</td>
			<td>{$l['dg_categoria']}</td>
			<td>{$l['dg_marca']}</td>
			<td>{$l['dg_unidad_medida']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/produccion/ed_material.php?id={$l['dc_material']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/produccion/del_material.php?id={$l['dc_material']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>