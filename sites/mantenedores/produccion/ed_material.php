<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_produccion_material",
"dg_material,dc_categoria,dc_unidad_medida,dc_marca",
"dc_material = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el material especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Materiales</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la categoría");
	$form->Start("sites/mantenedores/produccion/proc/editar_categoria.php","ed_categoria");
	$form->Section();
	$form->Text("Nombre","ed_mat_name",1,255,$datos['dg_material']);
	$form->Listado("Categoría","ed_mat_categoria",'tb_produccion_categoria',array('dc_categoria','dg_categoria'),1,$datos['dc_categoria']);
	$form->EndSection();
	$form->Section();
	$form->Listado("Unidad de medida",'ed_mat_unidad','tb_unidad_medida',array('dc_unidad_medida','dg_unidad_medida'),0,$datos['dc_unidad_medida']);
	$form->Listado("Marca",'ed_mat_marca','tb_marca',array('dc_marca','dg_marca'),0,$datos['dc_marca']);
	$form->EndSection();
	$form->Hidden("ed_material_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>