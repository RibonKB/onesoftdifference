<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_unidad_medida",
"dg_unidad_medida",
"dc_unidad_medida = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la unidad de medida especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición unidades de medida</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la unidad de medida");
	$form->Start("sites/mantenedores/produccion/proc/editar_unidad_medida.php","ed_unidad_medida");
	$form->Text("Nombre","ed_um_name",1,255,$datos['dg_unidad_medida']);
	$form->Hidden("ed_um_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>