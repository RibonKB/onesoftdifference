<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_produccion_material",
"dg_material",
"dc_material= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el material especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar Material</div>
<div class="panes">
	<form action="sites/mantenedores/produccion/proc/delete_material.php" class="confirmValidar" id="del_material">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el material '<?=$datos[0]['dg_material'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_material_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_material_res"></div>
</div>