<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_produccion_categoria",
"dg_categoria",
"dc_categoria= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la categoría especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar Categoria</div>
<div class="panes">
	<form action="sites/mantenedores/produccion/proc/delete_categoria.php" class="confirmValidar" id="del_categoria">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la categoría '<?=$datos[0]['dg_categoria'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_categoria_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_categoria_res"></div>
</div>