<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_unidad_medida",
"dg_unidad_medida",
"dc_unidad_medida= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la unidad de medida especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar Unidad de medida</div>
<div class="panes">
	<form action="sites/mantenedores/produccion/proc/delete_unidad_medida.php" class="confirmValidar" id="del_unidad_medida">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la unidad de medida '<?=$datos[0]['dg_unidad_medida'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_um_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_unidad_medida_res"></div>
</div>