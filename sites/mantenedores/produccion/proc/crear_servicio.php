<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$new = $db->insert("tb_produccion_servicio",
array(
	"dg_servicio" => $_POST['svc_name'],
	"dc_categoria" => $_POST['svc_categoria'],
	"dq_costo" => $_POST['svc_cost'],
	"dc_empresa" => $empresa,
	"df_creacion" => 'NOW()',
	"dc_usuario_creacion" => $idUsuario
));

$error_man->showConfirm("Se ha creado el servicio <strong>{$_POST['svc_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");

echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['svc_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/produccion/ed_servicio.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/produccion/del_servicio.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>