<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_produccion_categoria",
array(
	"dg_categoria" => trim(ucwords($_POST['ed_cat_name'])),
	"dc_categoria_padre" => $_POST['ed_cat_padre']
),"dc_categoria = {$_POST['ed_categoria_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/produccion/src_categoria.php");
</script>