<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_produccion_material",
array(
	"dg_material" => trim(ucwords($_POST['ed_mat_name'])),
	"dc_categoria" => $_POST['ed_mat_categoria'],
	"dc_unidad_medida" => $_POST['ed_mat_unidad'],
	"dc_marca" => $_POST['ed_mat_marca']
),"dc_material = {$_POST['ed_material_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/produccion/src_material.php");
</script>