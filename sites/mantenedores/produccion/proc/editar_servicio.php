<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_produccion_servicio",
array(
	"dg_servicio" => trim(ucwords($_POST['ed_svc_name'])),
	"dc_categoria" => $_POST['ed_svc_categoria'],
	"dq_costo" => $_POST['ed_svc_cost']
),"dc_servicio = {$_POST['ed_servicio_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/produccion/src_servicio.php");
</script>