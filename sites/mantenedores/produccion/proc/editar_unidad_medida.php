<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_unidad_medida",
array(
	"dg_unidad_medida" => trim(ucwords($_POST['ed_um_name']))
),"dc_unidad_medida = {$_POST['ed_um_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/produccion/src_unidad_medida.php");
</script>