<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_produccion_servicio",
"dg_servicio,dc_categoria,dq_costo",
"dc_servicio = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el servicio especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Servicios</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la categoría");
	$form->Start("sites/mantenedores/produccion/proc/editar_categoria.php","ed_categoria");
	$form->Section();
	$form->Text("Nombre","ed_svc_name",1,255,$datos['dg_servicio']);
	$form->Listado("Sección/Taller","ed_svc_categoria",'tb_produccion_categoria',array('dc_categoria','dg_categoria'),1,$datos['dc_categoria']);
	$form->EndSection();
	$form->Section();
	$form->Text("Costo","ed_svc_cost",0,255,$datos['dq_costo']);
	$form->EndSection();
	$form->Hidden("ed_servicio_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>