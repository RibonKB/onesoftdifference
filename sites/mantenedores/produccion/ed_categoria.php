<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_produccion_categoria",
"dg_categoria,dc_categoria_padre",
"dc_categoria = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la categoría especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición categorías</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la categoría");
	$form->Start("sites/mantenedores/produccion/proc/editar_categoria.php","ed_categoria");
	$form->Text("Nombre","ed_cat_name",1,255,$datos['dg_categoria']);
	$form->Listado("Categoría padre","ed_cat_padre",'tb_produccion_categoria',array('dc_categoria','dg_categoria'),0,
	$datos['dc_categoria_padre'],"dc_empresa = {$empresa} AND dm_activo = '1' AND dc_categoria_padre = 0 AND dc_categoria != {$_POST['id']} ");
	$form->Hidden("ed_categoria_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>