<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_medio_pago","dg_medio_pago",
"dc_medio_pago = {$_POST['id']} AND dc_empresa = {$empresa}");

if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el medio de pago especificado");
	exit();
}
$datos = $datos[0];

$aux_clientes = $db->select('tb_cliente_medio_pago','dc_cliente',"dc_medio_pago={$_POST['id']}");
$clientes = array();
foreach($aux_clientes as $c){
	$clientes[] = $c['dc_cliente'];
}
unset($aux_clientes);

?>
<div class="secc_bar">Edición Medios de pago</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados del tipo de falla");
	$form->Start("sites/mantenedores/clientes/proc/editar_medio_pago.php","ed_medio_pago");
	$form->Text("Nombre","ed_medio_name",1);
	$form->ListadoMultiple('Clientes con este medio de pago','ed_medio_clientes','tb_cliente',array('dc_cliente','dg_razon'),$clientes);
	$form->Hidden('ed_medio_id',$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>