<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$new = $db->insert("tb_medio_pago",
array(
	"dg_medio_pago" => ucwords($_POST['medio_name']),
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_creacion" => "NOW()"
));

if(isset($_POST['medio_clientes'])){
	foreach($_POST['medio_clientes'] as $m){
		$db->insert("tb_cliente_medio_pago",
		array(
			"dc_cliente" => $m,
			"dc_medio_pago" => $new
		));
	}
}

$db->commit();
$error_man->showConfirm("Se ha creado el medio de pago <strong>{$_POST['medio_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");
echo("<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['medio_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/clientes/ed_medio_pago.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' class='left' />
		</a>
		<a href='sites/mantenedores/clientes/del_medio_pago.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' class='left' />
		</a>
	</td>
</tr>
</table>");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>