<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$db->update("tb_medio_pago",
array(
	"dg_medio_pago" => ucwords($_POST['medio_name'])
),"dc_medio_pago={$_POST['ed_medio_id']}");

$db->query("DELETE FROM tb_cliente_medio_pago WHERE dc_medio_pago={$_POST['ed_medio_id']}");

if(isset($_POST['ed_medio_clientes'])){
	foreach($_POST['ed_medio_clientes'] as $m){
		$db->insert("tb_cliente_medio_pago",
		array(
			"dc_cliente" => $m,
			"dc_medio_pago" => $_POST['ed_medio_id']
		));
	}
}

$db->commit();
?>
<script type="text/javascript">
	show_confirm('Se ha modificado la cuenta correctamente');
	loadpage("sites/mantenedores/contabilidad/src_cuenta_contable.php");
</script>