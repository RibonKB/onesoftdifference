<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_direccion_proveedor",
"dg_tipo_direccion",
"dc_tipo_direccion = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de dirección especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipos de dirección</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el tipo de dirección");
	$form->Start("sites/mantenedores/proc/editar_tipo_direccion_proveedor.php","ed_tipo_direccion");
	$form->Text("Nombre","ed_tipo_name",1,255,$datos['dg_tipo_direccion']);
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>