<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_cargo_contacto",
"dg_cargo_contacto",
"dc_cargo_contacto = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el cargo especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición cargos de contacto</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el cargo");
	$form->Start("sites/mantenedores/proc/editar_cargo_contacto.php","ed_cargo_contacto");
	$form->Text("Nombre","ed_cargo_name",1,255,$datos['dg_cargo_contacto']);
	$form->Hidden("ed_cargo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>