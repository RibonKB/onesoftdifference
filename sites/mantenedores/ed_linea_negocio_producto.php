<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_linea_negocio",
"dg_linea_negocio",
"dc_linea_negocio = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la linea de negocio especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición linea de negocio</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la linea de negocio");
	$form->Start("sites/mantenedores/proc/editar_linea_negocio_producto.php","ed_linea_negocio");
	$form->Text("Nombre","ed_linea_name",1,255,$datos['dg_linea_negocio']);
	$form->Hidden("ed_linea_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>