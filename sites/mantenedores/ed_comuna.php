<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_comuna",
"dg_comuna,dc_region",
"dc_comuna = {$_POST['id']}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la comuna especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición de comunas</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la comuna");
	$form->Start("sites/mantenedores/proc/editar_comuna.php","ed_comuna");
	$form->Section();
	$form->Text("Nombre","ed_comuna_name",1,255,$datos['dg_comuna']);
	$form->EndSection();
	$form->Section();
	$form->Listado("Región","ed_comuna_region","tb_region",array("dc_region","dg_region"),1,$datos['dc_region'],"");
	$form->EndSection();
	$form->Hidden("ed_comuna_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>