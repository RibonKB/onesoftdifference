<?php
/**
*
*	Permite a los usuarios agregar nuevas lineas de negocio.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de cliente</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de cliente<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/proc/crear_tipo_cliente.php","cr_tipo_cliente");
	$form->Text("Nombre","tipo_name",1);
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_tipo_cliente",
		"dc_tipo_cliente, dg_tipo_cliente, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_tipo_cliente")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Tipos de cliente ya agregados</caption>
	<thead>
		<tr>
			<th width="65%">Tipo de cliente</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_tipo_cliente']}'>
			<td>{$l['dg_tipo_cliente']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ed_tipo_cliente.php?id={$l['dc_tipo_cliente']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/del_tipo_cliente.php?id={$l['dc_tipo_cliente']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>