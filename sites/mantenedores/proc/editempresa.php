<?php
/**
*	Modifica las ciudades en la base de datos.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$id = $_POST['m_empresa'];
$rs = htmlentities($_POST['ers'],ENT_QUOTES,'UTF-8');
$comuna = $_POST['ecomuna'];
$dir = htmlentities($_POST['edir'],ENT_QUOTES,'UTF-8');
$fono = htmlentities($_POST['efono'],ENT_QUOTES,'UTF-8');
$campos = array('dc_comuna','dg_razon','dg_direccion','dg_fono');
$valores = array("$comuna","'$rs'","'$dir'","'$fono'");
$db->update('tb_empresa',$campos,$valores,"dc_empresa=$id");
$error_man->showConfirm("Los datos de '<strong>$rs</strong>' han sido cambiados satisfactoriamente");

?>
<script type="text/javascript">
	loadFile("sites/proc/listados/gettiposdireccion.php","#tdireccion_edit,#tdireccion_add");
</script>