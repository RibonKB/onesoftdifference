<?php
/**
*	Edicion de contacto de cotizacion extra
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->update("tb_tipo_direccion",array("dm_activo" => '0'),"dc_tipo_direccion = {$_POST['del_tipo_id']} AND dc_empresa={$empresa}");

$error_man->showConfirm("<div class='center'>Se ha eliminado el tipo de dirección correctamente<br /><a href='#' onclick='$(\"#genOverlay\").remove();'>Cerrar</a></div>");

?>
<script type="text/javascript">
	$("#item<?=$_POST['del_tipo_id'] ?>").remove();
</script>