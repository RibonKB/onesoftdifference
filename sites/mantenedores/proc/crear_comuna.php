<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$new = $db->insert("tb_comuna",
array(
	"dg_comuna" => ucwords($_POST['comuna_name']),
	"dc_region" => $_POST['comuna_region'],
	"df_creacion" => "NOW()"
));

$error_man->showConfirm("Se ha creado la comuna <strong>{$_POST['comuna_name']}</strong> correctamente");

$region = $db->select("tb_region","dg_region","dc_region = {$_POST['comuna_region']}");

$date_now = date("d/m/Y H:i");
echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['comuna_name']}</td>
	<td>{$region[0]['dg_region']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/ed_comuna.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/del_comuna.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>