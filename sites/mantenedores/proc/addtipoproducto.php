<?php
/**
*	almacena en la base de datos el mercado especificado en el formulario de ingreso.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript.";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$tproducto = htmlentities($_POST['name'],ENT_QUOTES,'UTF-8');
$db->insert('tb_tipo_producto','dg_tipo_producto,dc_empresa',"'{$tproducto}',{$empresa}");
$error_man->showConfirm("Se ha agregado el tipo de producto \"<strong> {$tproducto} </strong>\" a la base de datos");

?>
<script type="text/javascript">
	loadFile("sites/proc/listados/gettproducto.php","#tproducto_edit,#tproducto_add");
</script>