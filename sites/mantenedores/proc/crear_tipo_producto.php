<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$new = $db->insert("tb_tipo_producto",
array(
	"dg_tipo_producto" => ucwords($_POST['tipo_name']),
	"dm_controla_inventario" => $_POST['tipo_inventario'],
	"dm_requiere_costeo" => $_POST['tipo_costeo'],
    "dm_orden_compra" => intval($_POST['dm_orden_compra']),
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_creacion" => "NOW()"
));

$error_man->showConfirm("Se ha creado el tipo de producto <strong>{$_POST['tipo_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");

$inventario = $_POST['tipo_inventario']=='0'?"<img src='images/inventario.png' alt='I' title='Controla inventario' />":"";

echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$inventario}</td>
	<td>{$_POST['tipo_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/ed_tipo_producto.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/del_tipo_producto.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>