<?php
/**
*	Edicion de linea de negocio
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_tipo_producto",
array(
	"dg_tipo_producto" => trim(ucwords($_POST['ed_tipo_name'])),
	"dm_controla_inventario" => $_POST['ed_tipo_inventario'],
    'dm_orden_compra' => $_POST['dm_orden_compra_ed']
),"dc_tipo_producto = {$_POST['ed_tipo_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/src_tipo_producto.php");
</script>