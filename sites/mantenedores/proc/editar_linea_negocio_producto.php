<?php
/**
*	Edicion de linea de negocio
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_linea_negocio",
array(
	"dg_linea_negocio" => trim(ucwords($_POST['ed_linea_name']))
),"dc_linea_negocio = {$_POST['ed_linea_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/src_linea_negocio_producto.php");
</script>