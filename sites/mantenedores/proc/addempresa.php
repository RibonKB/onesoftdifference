<?php
/**
*	almacena en la base de datos la empresa especificada en el formulario de ingreso.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript.";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$doc_moneda = htmlentities($_POST['doc_moneda'],ENT_QUOTES,'UTF-8');
$doc_iva = str_replace(",",".",$_POST['doc_iva']);
if(!is_numeric($doc_iva)){
	$error_man->showWarning("El formato númerico para el IVA es incorrecto. utilice solo separador decimal, no de miles.");
	exit();
}

$id_empresa = $db->insert('tb_empresa',
array(
	'dg_rut' => $_POST['rut'],
	'dg_razon' => $_POST['razon'],
	'dg_direccion' => $_POST['direccion'],
	'dc_comuna' => $_POST['comuna'],
	'dg_fono' => $_POST['fono']
));

$db->insert('tb_empresa_configuracion',
array(
	'dc_empresa' => $id_empresa,
	'dg_moneda_local' => $_POST['doc_moneda'],
	'dc_correlativo_cotizacion' => $_POST['cot_corr'],
	'dg_correlativo_cotizacion' => $_POST['cot_comienzo'],
	'dg_pie_cotizacion' => $_POST['doc_pie_cot'],
	'dq_iva' => $_POST['doc_iva'],
	'dc_modo_cotizacion' => $_POST['cot_modo'],
	'dc_correlativo_nota_credito' => $_POST['nota_credito_corr'],
	'dg_correlativo_nota_credito' => $_POST['nota_credito_comienzo'],
	'dc_correlativo_nota_venta' => $_POST['nota_venta_corr'],
	'dg_correlativo_nota_venta' => $_POST['nota_venta_comienzo'],
	'dc_correlativo_nota_debito' => $_POST['nota_debito_corr'],
	'dg_correlativo_nota_debito' => $_POST['nota_debito_comienzo'],
	'dc_correlativo_documento_contable' => $_POST['contable_corr'],
	'dg_correlativo_documento_contable' => $_POST['contable_comienzo']
));

$error_man->showConfirm("Se ha agregado la empresa <strong> $rs ( $rut ) </strong> a la base de datos");

?>