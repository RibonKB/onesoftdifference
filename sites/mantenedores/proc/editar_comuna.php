<?php
/**
*	Edicion de linea de negocio
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_comuna",
array(
	"dg_comuna" => trim(ucwords($_POST['ed_comuna_name'])),
	"dc_region" => $_POST['ed_comuna_region']
),"dc_comuna = {$_POST['ed_comuna_id']}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/src_comuna.php");
</script>