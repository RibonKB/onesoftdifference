<?php
/**
*	Edicion de cargos de contacto
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_cargo_contacto",
array(
	"dg_cargo_contacto" => trim(ucwords($_POST['ed_cargo_name']))
),"dc_cargo_contacto = {$_POST['ed_cargo_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/src_cargos_contacto.php");
</script>