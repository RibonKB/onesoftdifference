<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de ventas</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Start("sites/mantenedores/ventas/proc/crear_tipo_nota_venta.php","cr_tipo_nota_venta");
	$form->Header("Complete los datos requeridos para crear un nuevo tipo de venta<br />Puede consultar los que ya están agregados.");
	$form->Section();
	$form->Text('Nombre','tipo_name',1);
	$form->EndSection();
	$form->Section();
	$form->ListadoMultiple('Tipos de guía de despacho permitidos','tipo_tipos_guia','tb_tipo_guia_despacho',array('dc_tipo','dg_tipo'));
	$form->Select('Tipo de guía por defecto','tipo_guia_defecto',array());
	$form->EndSection();
	
	$form->Group();
	$form->Header('Opciones de facturación');
	$form->Section();
	$form->Radiobox('Facturable','tipo_facturable',array(1=>'SI',0=>'NO'),1,' ');
	$form->EndSection();
	
	$form->Section();
	$form->Radiobox('Tipo de impresión por defecto','tipo_impresion_factura',array(1=>'Impresión detalle',2=>'Impresión glosa'),1);
	$form->EndSection();
	
	$form->Group();
	$form->Header('Opciones de contrato');
	$form->Section();
	$form->Radiobox('Asignable a contrato','tipo_contratable',array(1=>'SI',0=>'NO'),1,' ');
	$form->EndSection();
	
	$form->Group();
	$form->Header('Opciones de costeo');
	$form->Section();
	$form->Radiobox('Costeo habilitado','tipo_costeable',array(1=>'SI',0=>'NO'),1,' ');
	$form->EndSection();
	
	$form->Group();
	$form->End("Crear","addbtn");
	
	$list = $db->select("tb_tipo_nota_venta",'dc_tipo, dg_tipo, DATE_FORMAT(df_creacion,"%d/%m/%Y") as df_fecha',"dc_empresa = {$empresa}",array('order_by'=> 'dg_tipo'));
	
	echo('<table class="tab" width="100%">
	<caption>Tipos de guía ya agregados</caption>
	<thead>
		<tr>
			<th width="35%">Tipo de guía</th>
			<th width="15%">Fecha de creación</th>
			<th width="10%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("<tr id='item{$l['dc_tipo']}'>
			<td>{$l['dg_tipo']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ventas/ed_tipo_nota_venta.php?id={$l['dc_tipo']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/ventas/del_tipo_nota_venta.php?id={$l['dc_tipo']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div></div>
<script type="text/javascript">
$('#tipo_tipos_guia').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});

$('#tipo_guia_defecto').click(function(e){
	if($(this).find('option').size() == 1){
		show_aviso('Seleccione antes los tipos de guía permitidos');
	}
});

$(':input[name=tipo_tipos_guia[]], .multiSelectOptions :input.selectAll').click(function(){
	$('#tipo_guia_defecto').html('<option value="0"></option>');
	$(':input[name=tipo_tipos_guia[]]:checked').each(function(){
		var txt = $(this).parent().text();
		var val = $(this).val();
		$('#tipo_guia_defecto').append($('<option>').attr('value',val).text(txt));
	});
});
</script>