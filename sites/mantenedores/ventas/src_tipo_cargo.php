<?php
define('MAIN',1);
require_once("../../../inc/init.php");

$tipo_cargo = $db->prepare($db->select('tb_tipo_cargo','dc_tipo_cargo, dg_tipo_cargo, df_creacion','dc_empresa = ? AND dm_activo = 1'));
$tipo_cargo->bindValue(1,$empresa,PDO::PARAM_INT);
$db->stExec($tipo_cargo);

require_once("../../../inc/form-class.php");
$form = new Form($empresa);


?>
<div id="secc_bar">
	Tipos de cargo factura de venta
</div>
<div id="main_cont">
	<div class="panes">
    	<?php $form->Start('sites/mantenedores/ventas/proc/crear_tipo_cargo.php','cr_tipo_cargo') ?>
        	<?php $form->Header('<b>Ingrese el nombre del tipo de cargo</b><br />Los campos marcados con [*] son obligatorios') ?>
        	<?php $form->Text('Nombre','dg_tipo_cargo',true) ?>
        <?php $form->End('Crear','addbtn') ?>
        
        <table class="tab" width="100%">
        	<thead>
            	<th>Nombre</th>
                <th>Fecha de creación</th>
                <th>Opciones</th>
            </thead>
            <tbody>
            	<?php while($tc = $tipo_cargo->fetch(PDO::FETCH_OBJ)): ?>
                	<tr>
                    	<td><?php echo $tc->dg_tipo_cargo ?></td>
                        <td><?php echo $tc->df_creacion ?></td>
                        <td>
                        	<a href="sites/mantenedores/ventas/ed_tipo_cargo.php?id=<?php echo $tc->dc_tipo_cargo ?>" class="loadOnOverlay">
                            	<img src="images/editbtn.png" alt="[ED]" title="Editar" />
                            </a>
                            <a href="sites/mantenedores/ventas/del_tipo_cargo.php?id=<?php echo $tc->dc_tipo_cargo ?>" class="loadOnOverlay">
                            	<img src="images/delbtn.png" alt="[ED]" title="Eliminar" />
                            </a>
                        </td>
                    </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        
    </div>
</div>