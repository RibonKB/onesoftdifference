<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_tipo_operacion",
"dg_tipo_operacion,dm_anticipada,dc_tipo_guia,dc_tipo_movimiento, dc_tipo_cambio, dm_exenta, dm_agrega_iva_producto,dm_zona_franca",
"dc_tipo_operacion = {$_POST['id']} AND dc_empresa = {$empresa}");

if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de operación especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipos de operación</div>
<div class="panes">
<?php
	if (is_null($datos['dc_tipo_cambio'])){
		$datos['dc_tipo_cambio'] = 0;
	}
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el modo de envio");
	$form->Start("sites/mantenedores/ventas/proc/editar_tipo_operacion.php","ed_tipo_operacion");
	
	$form->Section();
	$form->Text("Nombre","ed_tipo_name",1,255,$datos['dg_tipo_operacion']);
	if($datos['dm_anticipada'] == 0)
		echo('<br /><label><input type="checkbox" name="ed_tipo_anticipado" /> Permite factura anticipada</label>');
	else
		echo('<br /><label><input type="checkbox" name="ed_tipo_anticipado" checked="checked" /> Permite factura anticipada</label>');
	
	if($datos['dm_zona_franca'] == 0){
		echo('<br /><label><input type="checkbox" name="tipo_zofri" /> Venta Zona Franca </label>');
	} else {
		echo('<br /><label><input type="checkbox" name="tipo_zofri" checked="checked" /> Venta Zona Franca </label>');
	}
	$form->EndSection();
	$form->Section();
	$form->Listado('Tipo de guía virtual generada','ed_tipo_tipo_guia','tb_tipo_guia_despacho',array('dc_tipo','dg_tipo'),1,$datos['dc_tipo_guia']);
	$form->Listado('Tipo de movimiento logístico','ed_tipo_tipo_mov','tb_tipo_movimiento_logistico',array('dc_tipo_movimiento','dg_tipo_movimiento'),0,$datos['dc_tipo_movimiento']);
	$form->EndSection();
	$form->Section();
	$form->Listado('Tipo de cambio a mostrar', 'ed_tipo_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'),0,$datos['dc_tipo_cambio']);
	if($datos['dm_exenta'] == 0){
		echo('<br /><label><input type="checkbox" name="ed_tipo_exenta" /> No incluir IVA al total</label>');
	} else {
		echo('<br /><label><input type="checkbox" name="ed_tipo_exenta" checked="checked" /> No incluir IVA al total</label>');
	}
	if($datos['dm_agrega_iva_producto'] == 0){
		echo('<br /><label><input type="checkbox" name="ed_tipo_incluye_iva" /> Aplicar IVA directamente a los productos</label>');
	} else {
		echo('<br /><label><input type="checkbox" name="ed_tipo_incluye_iva" checked="checked" /> Aplicar IVA directamente a los productos</label>');
	}
	$form->EndSection();
	
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>