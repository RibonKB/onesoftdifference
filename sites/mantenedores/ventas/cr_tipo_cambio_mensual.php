<?php
define("MAIN",1);
require_once("../../../inc/init.php");

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo_cambio = intval($_POST['dc_tipo_cambio']);
$dc_mes = intval($_POST['dc_mes']);
$dc_anho = intval($_POST['dc_anho']);

$tipo_cambio = $db->prepare($db->select('tb_tipo_cambio','dg_tipo_cambio','dc_tipo_cambio = ? AND dm_activo = 1'));
$tipo_cambio->bindValue(1,$dc_tipo_cambio,PDO::PARAM_INT);
$db->stExec($tipo_cambio);
$tipo_cambio = $tipo_cambio->fetch(PDO::FETCH_OBJ);

if($tipo_cambio === false){
	$error_man->showWarning('El tipo de cambio seleccionado no existe, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

$mes = mktime(0,0,0,$dc_mes,1,$dc_anho);
setlocale(LC_TIME, 'es_CL.UTF8');
$ultimo_dia = intval(date('t',$mes));
$dia_primero = intval(date('N',$mes));

$aux = $db->prepare($db->select('tb_tipo_cambio_mensual','DAY(df_cambio) dc_dia, dq_cambio','dc_tipo_cambio = ? AND MONTH(df_cambio) = ? AND YEAR(df_cambio) = ?'));
$aux->bindValue(1,$dc_tipo_cambio,PDO::PARAM_INT);
$aux->bindValue(2,$dc_mes,PDO::PARAM_INT);
$aux->bindValue(3,$dc_anho,PDO::PARAM_INT);
$db->stExec($aux);

$values = array();
while($v = $aux->fetch(PDO::FETCH_OBJ)){
	$values[$v->dc_dia] = $v->dq_cambio;
}

?>
<div class="secc_bar">
	Configurando tipo de cambio para <b><?php echo $tipo_cambio->dg_tipo_cambio ?></b> 
</div>
<div class="panes">
	<?php $form->Start('sites/mantenedores/ventas/proc/crear_tipo_cambio_mensual.php','cr_tipo_cambio_mensual') ?>
		<?php $form->Header('Indique los montos para el tipo de cambio para los días del mes indicados') ?>
        
        <table class="tab" align="center" width="360" id="tc_tab">
        	<thead>
            	<tr>
                	<th width="50">Día hábil</th>
                    <th>Día</th>
                    <th>cambio</th>
                </tr>
            </thead>
            <tbody>
            <?php for($dc_dia = 1; $dc_dia <= $ultimo_dia; $dc_dia++): ?>
            	<tr>
                	<td align="center">
                    	<?php if(($dia_primero) % 6 != 0 && ($dia_primero) % 7 != 0 ): ?>
                        <input type="checkbox" name="dm_habil[]" value="<?php echo $dc_dia ?>" checked="checked" tabindex="<?php echo $dc_dia ?>" />
                        <?php else: ?>
                        <input type="checkbox" name="dm_habil[]" value="<?php echo $dc_dia ?>" tabindex="<?php echo $dc_dia ?>" />
                        <?php endif; ?>
                    </td>
                    <td><?php echo strftime('%a, %d/%m/%Y',mktime(0,0,0,$dc_mes,$dc_dia,$dc_anho)) ?></td>
                    <td>
                    	<input type="text" name="dq_cambio[]" class="inputtext" value="<?php echo isset($values[$dc_dia])?$values[$dc_dia]:0 ?>" style="text-align:right" />
                    </td>
                </tr>
            <?php
            	if(($dia_primero+1) % 7 == 0){
					$dia_primero = 0;
				}else{
					$dia_primero++;
				}
				endfor;
			?>
            </tbody>
        </table>
        
        <?php $form->Hidden('dc_tipo_cambio',$dc_tipo_cambio) ?>
        <?php $form->Hidden('dc_anho',$dc_anho) ?>
        <?php $form->Hidden('dc_mes',$dc_mes) ?>
    <?php $form->End('Crear','addbtn') ?>
</div>
<script type="text/javascript">
	$('#genOverlay').css({'minWidth':0}).width(400);
	window.setTimeout(function(){
	$('#tc_tab').tableAdjust(10);
	},100);
</script>