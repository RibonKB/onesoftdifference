<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos guías de recepción</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de guía de recepción<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/ventas/proc/crear_tipo_guia_recepcion.php","cr_tipo_guia");
	$form->Text("Nombre","tipo_name",1);
	$form->Listado('Tipo de movimiento logístico','tipo_tipo_mov','tb_tipo_movimiento_logistico',array('dc_tipo_movimiento','dg_tipo_movimiento'));
	$form->Listado('Bodega por defecto','tipo_bodega','tb_bodega',array('dc_bodega','dg_bodega'));
	$form->End("Crear","addbtn");
	
	$list = $db->select("tb_tipo_guia_recepcion",
		"dc_tipo, dg_tipo, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",array("order_by" => "dg_tipo"));
	
	echo('<table class="tab" width="100%">
	<caption>Tipos de guía ya agregados</caption>
	<thead>
		<tr>
			<th width="65%">Tipo de guía</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("<tr id='item{$l['dc_tipo']}'>
			<td>{$l['dg_tipo']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ventas/ed_tipo_guia_despacho.php?id={$l['dc_tipo']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/ventas/del_tipo_guia_despacho.php?id={$l['dc_tipo']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>