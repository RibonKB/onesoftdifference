<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de operación</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de operación<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/ventas/proc/crear_tipo_operacion.php","cr_tipo_operacion");
	$form->Section();
	$form->Text("Nombre","tipo_name",1);
	echo('<br /><label><input type="checkbox" name="tipo_anticipado" /> Permite factura anticipada</label>');
	echo('<br /><label><input type="checkbox" name="tipo_zofri" /> Venta Zona Franca </label>');
	$form->EndSection();
	$form->Section();
	$form->Listado('Tipo de guía virtual generada','tipo_tipo_guia','tb_tipo_guia_despacho',array('dc_tipo','dg_tipo'),1);
	$form->Listado('Tipo de movimiento logístico','tipo_tipo_mov','tb_tipo_movimiento_logistico',array('dc_tipo_movimiento','dg_tipo_movimiento'));
	$form->EndSection();
	
	$form->Section();
	$form->Listado('Tipo de cambio a mostrar', 'tipo_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'));
	echo('<br /><label><input type="checkbox" name="tipo_exenta" /> No incluir IVA al total</label>');
	echo('<br /><label><input type="checkbox" name="tipo_incluye_iva" /> Suma IVA directamente a los productos</label>');
	$form->EndSection();
	
	
	$form->End("Crear","addbtn");
	
	$list = $db->select("(SELECT * FROM tb_tipo_operacion WHERE dc_empresa={$empresa} AND dm_activo='1') top
	LEFT JOIN tb_tipo_guia_despacho tgd ON tgd.dc_tipo = top.dc_tipo_guia
	LEFT JOIN tb_tipo_movimiento_logistico tml ON tml.dc_tipo_movimiento = top.dc_tipo_movimiento",
	"top.dc_tipo_operacion, top.dg_tipo_operacion, DATE_FORMAT(top.df_creacion,'%d/%m/%Y %H:%i') as df_fecha,
	tgd.dg_tipo, tml.dg_tipo_movimiento","",array("order_by" => "top.dg_tipo_operacion"));
	
	echo('<table class="tab" width="100%">
	<caption>Tipos de operación ya agregados</caption>
	<thead>
		<tr>
			<th width="30%">Tipo de operacion</th>
			<th width="20%">Tipo de guía virtual</th>
			<th width="20%">Tipo de movimiento</th>
			<th width="15%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("<tr id='item{$l['dc_tipo_operacion']}'>
			<td>{$l['dg_tipo_operacion']}</td>
			<td>{$l['dg_tipo']}</td>
			<td>{$l['dg_tipo_movimiento']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ventas/ed_tipo_operacion.php?id={$l['dc_tipo_operacion']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/ventas/del_tipo_operacion.php?id={$l['dc_tipo_operacion']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>