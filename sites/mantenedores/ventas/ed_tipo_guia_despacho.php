<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_tipo_guia_despacho",
"dg_tipo,dc_tipo_movimiento,dc_bodega,dc_bodega_entrada,dm_facturable",
"dc_tipo = {$_POST['id']} AND dc_empresa = {$empresa}");

if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de guía de despacho especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipo de guía de despacho</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el tipo de guía");
	$form->Start("sites/mantenedores/ventas/proc/editar_tipo_guia_despacho.php","ed_tipo_guia");
	$form->Section();
	$form->Text("Nombre","ed_tipo_name",1,255,$datos['dg_tipo']);
	$form->Listado('Tipo de movimiento logístico','ed_tipo_tipo_mov','tb_tipo_movimiento_logistico',array('dc_tipo_movimiento','dg_tipo_movimiento'),1,$datos['dc_tipo_movimiento']);
	$form->Radiobox('Facturable','ed_tipo_facturable',array('NO','SI'),$datos['dm_facturable'],' ');
	$form->EndSection();
	$form->Section();
	$form->Listado('Bodega salida por defecto','ed_tipo_bodega','tb_bodega',array('dc_bodega','dg_bodega'),0,$datos['dc_bodega']);
	$form->Listado('Bodega entrada','ed_tipo_bodega_entrada','tb_bodega',array('dc_bodega','dg_bodega'),0,$datos['dc_bodega_entrada']);
	$form->EndSection();
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>