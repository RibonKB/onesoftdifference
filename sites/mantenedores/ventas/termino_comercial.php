<?php
/**
*	Mantenedor de Términos comerciales de los registros de venta
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Mantenedor de T&eacute;rminos comerciales</div>

<div id="main_cont">

<ul id="tabs">
	<li><a href="#" class="current">Agregar</a></li>
	<li><a href="#">Editar</a></li>
	<li><a href="#">Eliminar</a></li>
</ul>
<div class="tabpanes">
<br /><br />
	<div>
		<div class="info">
			<strong>Complete los datos para agregar el nuevo t&eacute;rmino comercial</strong>
		</div>
		<fieldset>
		<div class="center">
			<label>T&eacute;rminos comerciales ya agregados<br />
			<select class="tcomercial_list inputtext" style="width:280px">
			<option></option>
			<?php include("../../proc/listados/getterm_comerciales.php"); ?>
			</select></label>
		</div>
		<hr />
		<form action="sites/mantenedores/ventas/proc/crear_ter_comercial.php" id="cr_t_comercial" class="validar" >
		<div class="left">
		
			<label>Nombre<br />
			<input type="text" name="t_comercial_nombre" class="inputtext" size="40" maxlength="255" required="required" />
			</label><br /><br />
			
		</div>
		<hr class="clear" />
		<div class="center">
		<input type="submit" class="addbtn" value="Crear" />
		</div>
		</form>
		</fieldset>
		<div id="cr_t_comercial_res" class="panes" style="width:680px;"></div>
	</div>
	
	<div>
		<div class="info">Seleccione el t&eacute;rmino comercial que desee editar</div>
		<fieldset>
		<form action="sites/mantenedores/ventas/proc/editar_ter_comercial.php" id="ed_t_comercial" class="validar" >
		<div class="center">
			<select name="ed_id" class="tcomercial_list inputtext" style="width:280px" required="required">
			<option></option>
			<?php include("../../proc/listados/getterm_comerciales.php"); ?>
			</select></label>
		</div>
		<hr />
		<div class="left">
		
			<label>Nombre<br />
			<input type="text" name="t_comercial_ed_nombre" class="inputtext" size="40" maxlength="255" required="required" />
			</label><br /><br />
			
		</div>
		<hr class="clear" />
		<div class="center">
		<input type="submit" class="editbtn" value="Editar" />
		</div>
		</form>
		</fieldset>
		<div id="ed_t_comercial_res" class="panes" style="width:680px;"></div>
	</div>
	
	<div>
		<div class="info">Seleccione el t&eacute;rmino comercial que desee eliminar</div>
		<fieldset>
		<form action="sites/mantenedores/ventas/proc/delete_ter_comercial.php" id="del_t_comercial" class="confirmValidar" >
		<div class="center">
			<select name="del_id" class="tcomercial_list inputtext" style="width:280px" required="required">
			<option></option>
			<?php include("../../proc/listados/getterm_comerciales.php"); ?>
			</select></label>
		</div>
		<hr />
		<div class="center">
		<input type="submit" class="delbtn" value="Eliminar" />
		</div>
		</form>
		</fieldset>
		<div id="del_t_comercial_res" class="panes" style="width:680px;"></div>
	</div>
</div>

</div>