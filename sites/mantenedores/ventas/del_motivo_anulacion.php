<?php
define('MAIN',1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_motivo = intval($_POST['id']);

$motivo = $db->prepare($db->select('tb_motivo_anulacion',
	'dg_motivo, dm_activo','dc_motivo = ? AND dc_empresa = ?'));
$motivo->bindValue(1,$dc_motivo,PDO::PARAM_INT);
$motivo->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($motivo);

$motivo = $motivo->fetch(PDO::FETCH_OBJ);

if($motivo === false){
	$error_man->showWarning('El motivo de anulación seleccionado no fue encontrado');
	exit;
}
?>

<div class="secc_bar" >
	Eliminar motivo de anulación
</div>

<div class="panes" >
	<?php $form->Start('sites/mantenedores/ventas/proc/delete_motivo_anulacion.php', 'del_motivo_anulacion') ?>
    <?php $error_man->showAviso("¿Está seguro que desea eliminar el motivo de anulación <strong>{$motivo->dg_motivo}</strong>?") ?>
    
    
    <?php $form->Hidden('dc_motivo_ed', $dc_motivo)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>
