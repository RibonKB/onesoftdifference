<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_motivo = intval($_POST['id']);

$dg_motivo = $db->prepare($db->select('tb_motivo_anulacion','dg_motivo','dc_motivo = ? AND dc_empresa = ?'));
$dg_motivo->bindValue(1,$dc_motivo,PDO::PARAM_INT);
$dg_motivo->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($dg_motivo);

$dg_motivo = $dg_motivo->fetch(PDO::FETCH_OBJ);

if($dg_motivo === false){
	$error_man->showWarning('No se encontro ese motivo de anulación');
	exit();
}

$dc_modulo = $db->prepare($db->select('tb_motivo_anulacion_modulo','dc_modulo','dc_motivo = ?'));
$dc_modulo->bindValue(1,$dc_motivo,PDO::PARAM_INT);
$db->stExec($dc_modulo);

$select = array();
while($s = $dc_modulo->fetch(PDO::FETCH_OBJ)){
	$select[]=$s->dc_modulo;
}


?>

<div class="secc_bar">
	Editar motivos de anulación
</div>
<div class="panes">
	<?php $form->Start('sites/mantenedores/ventas/proc/editar_motivo_anulacion.php', 'ed_motivo_anulacion'); ?>
	<?php $form->Header('<b>Ingrese el nombre para actualizar el motivo de anulación</b><br />Los campos marcados con [*] son 		  							obligatorios'); ?>
	<?php $form->Section(); 
		$form->Text('Nombre','dg_motivo_ed',true,Form::DEFAULT_TEXT_LENGTH,$dg_motivo->dg_motivo);  
		$form->Multiselect('Modulos','dc_modulo_ed',array(
						'1'=>'Ventas',
						'2'=>'Contabilidad',
						'3'=>'Finanzas',
						'4'=>'Area de servicios',
						'5'=>'Logistica'),
						$select);
		$form->EndSection();
	?>
    <?php $form->Hidden('dc_motivo_ed',$dc_motivo) ?>
    <?php $form->End('Editar','editbtn') ?>
    
	
<script type="text/javascript">
$('#dc_modulo_ed').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>