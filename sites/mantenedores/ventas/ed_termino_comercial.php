<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_termino_comercial",
"dg_termino_comercial",
"dc_termino_comercial = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el término comercial especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición términos comerciales</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el término comercial");
	$form->Start("sites/mantenedores/ventas/proc/editar_termino_comercial.php","ed_termino_comercial");
	$form->Text("Nombre","ed_termino_name",1,255,$datos['dg_termino_comercial']);
	$form->Hidden("ed_termino_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>