<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_termino_comercial",
"dg_termino_comercial",
"dc_termino_comercial= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el término comercial especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar términos comerciales</div>
<div class="panes">
	<form action="sites/mantenedores/ventas/proc/delete_termino_comercial.php" class="confirmValidar" id="del_termino_comercial">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el término comercial'<?=$datos[0]['dg_termino_comercial'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_termino_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_termino_comercial_res"></div>
</div>