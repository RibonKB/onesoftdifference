<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos guías de despacho</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de guía de despacho<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/ventas/proc/crear_tipo_guia_despacho.php","cr_tipo_guia");
	$form->Section();
	$form->Text("Nombre","tipo_name",1);
	$form->Listado('Tipo de movimiento logístico','tipo_tipo_mov','tb_tipo_movimiento_logistico',array('dc_tipo_movimiento','dg_tipo_movimiento'));
	$form->Radiobox('Facturable','tipo_facturable',array('NO','SI'),0,' ');
	$form->EndSection();
	$form->Section();
	$form->Listado('Bodega salida por defecto','tipo_bodega','tb_bodega',array('dc_bodega','dg_bodega'));
	$form->Listado('Bodega entrada','tipo_bodega_entrada','tb_bodega',array('dc_bodega','dg_bodega'));
	$form->Combobox('','dm_stock_reservado',array('Movimiento a Stock reservado'));
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select("(SELECT * FROM tb_tipo_guia_despacho WHERE dc_empresa={$empresa} AND dm_activo='1') tg
	LEFT JOIN tb_bodega b1 ON b1.dc_bodega = tg.dc_bodega
	LEFT JOIN tb_bodega b2 ON b2.dc_bodega = tg.dc_bodega_entrada",
	"tg.dc_tipo, tg.dg_tipo, b1.dg_bodega AS dg_bodega_salida, b2.dg_bodega AS dg_bodega_entrada, DATE_FORMAT(tg.df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
	"",array("order_by" => "dg_tipo"));
	
	echo('<table class="tab" width="100%">
	<caption>Tipos de guía ya agregados</caption>
	<thead>
		<tr>
			<th width="35%">Tipo de guía</th>
			<th width="20%">Bodega de salida</td>
			<th width="20%">Bodega de entrada</td>
			<th width="15%">Fecha de creación</th>
			<th width="10%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("<tr id='item{$l['dc_tipo']}'>
			<td>{$l['dg_tipo']}</td>
			<td>{$l['dg_bodega_salida']}</td>
			<td>{$l['dg_bodega_entrada']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ventas/ed_tipo_guia_despacho.php?id={$l['dc_tipo']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/ventas/del_tipo_guia_despacho.php?id={$l['dc_tipo']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>
<script type="text/javascript">
	$('#tipo_facturable :radio').change(function(){
		$('#tipo_bodega_entrada').attr('disabled',$(this).val() == 1);
	});
</script>