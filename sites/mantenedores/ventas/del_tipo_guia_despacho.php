<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_tipo_guia_despacho",
"dg_tipo",
"dc_tipo = {$_POST['id']} AND dc_empresa = {$empresa}");


if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de guía especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar tipos de guía de despacho</div>
<div class="panes">
	<div id="del_tipo_guia_res"></div>
	<form action="sites/mantenedores/ventas/proc/delete_tipo_guia_despacho.php" class="confirmValidar" id="del_tipo_guia">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el tipo de guía '<?=$datos[0]['dg_tipo'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_tipo_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
</div>