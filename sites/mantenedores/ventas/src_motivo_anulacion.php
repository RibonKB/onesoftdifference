<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Motivos de anulación</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo motivo de anulación<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/ventas/proc/crear_motivo_anulacion.php","cr_motivo_anulacion");
	$form->Text("Nombre","motivo_name",1);
	
	#aca va el multiselect
	
	$form->Multiselect('Modulos donde estara disponible el motivo de anulación','motivo_anulacion',array(
			'1'=>'Ventas',
			'2'=>'Contabilidad',
			'3'=>'Finanzas',
			'4'=>'Area de servicios',
			'5'=>'Logistica'
			));
	
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_motivo_anulacion",
		"dc_motivo, dg_motivo, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_motivo")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Motivos de anulación ya agregados</caption>
	<thead>
		<tr>
			<th width="65%">Motivo</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_motivo']}'>
			<td>{$l['dg_motivo']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ventas/ed_motivo_anulacion.php?id={$l['dc_motivo']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/ventas/del_motivo_anulacion.php?id={$l['dc_motivo']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>

<script type="text/javascript">
$('#motivo_anulacion').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>