<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_tipo_nota_venta",
"dg_tipo,dc_tipo_guia_default,dm_facturable,dm_costeable,dm_contratable,dm_tipo_impresion_factura",
"dc_tipo = {$_POST['id']} AND dc_empresa = {$empresa}");

if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el modo de envio especificado");
	exit();
}
$datos = $datos[0];

$aux = $db->select("(SELECT * FROM tb_tipo_nota_venta_tipo_guia WHERE dc_tipo_nota_venta={$_POST['id']}) tv
JOIN tb_tipo_guia_despacho tg ON tg.dc_tipo = tv.dc_tipo_guia_despacho",'tg.dc_tipo,tg.dg_tipo');
$tipos_guia = array();
$selected_tg = array();
foreach($aux as $v){
	$tipos_guia[] = $v['dc_tipo'];
	$selected_tg[$v['dc_tipo']] = $v['dg_tipo'];
}
unset($aux);

?>
<div class="secc_bar">Edición tipos de nota de venta</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el tipo de nota de venta");
	$form->Start("sites/mantenedores/ventas/proc/editar_tipo_nota_venta.php","ed_tipo_venta");
	
	$form->Section();
	$form->Text('Nombre','ed_tipo_name',1,255,$datos['dg_tipo']);
	$form->EndSection();
	$form->Section();
	$form->ListadoMultiple('Tipos de guía de despacho permitidos','ed_tipo_tipos_guia','tb_tipo_guia_despacho',array('dc_tipo','dg_tipo'),$tipos_guia);
	$form->Select('Tipo de guía por defecto','ed_tipo_guia_defecto',$selected_tg,0,$datos['dc_tipo_guia_default']);
	$form->EndSection();
	
	$form->Group();
	$form->Header('Opciones de facturación');
	$form->Section();
	$form->Radiobox('Facturable','ed_tipo_facturable',array(1=>'SI',0=>'NO'),$datos['dm_facturable'],' ');
	$form->EndSection();
	
	$form->Section();
	$form->Radiobox('Tipo de impresión por defecto','ed_tipo_impresion_factura',array(1=>'Impresión detalle',0=>'Impresión glosa'),$datos['dm_tipo_impresion_factura']);
	$form->EndSection();
	
	$form->Group();
	$form->Header('Opciones de contrato');
	$form->Section();
	$form->Radiobox('Asignable a contrato','ed_tipo_contratable',array(1=>'SI',0=>'NO'),$datos['dm_contratable'],' ');
	$form->EndSection();
	
	$form->Group();
	$form->Header('Opciones de costeo');
	$form->Section();
	$form->Radiobox('Costeo habilitado','ed_tipo_costeable',array(1=>'SI',0=>'NO'),$datos['dm_costeable'],' ');
	$form->EndSection();
	
	$form->Group();
	
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>
<script type="text/javascript">
$('#ed_tipo_tipos_guia').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});

$('#ed_tipo_guia_defecto').click(function(e){
	if($(this).find('option').size() == 1){
		show_aviso('Seleccione antes los tipos de guía permitidos');
	}
});

$(':input[name=ed_tipo_tipos_guia[]], .multiSelectOptions :input.selectAll').click(function(){
	$('#ed_tipo_guia_defecto').html('<option value="0"></option>');
	$(':input[name=ed_tipo_tipos_guia[]]:checked').each(function(){
		var txt = $(this).parent().text();
		var val = $(this).val();
		$('#ed_tipo_guia_defecto').append($('<option>').attr('value',val).text(txt));
	});
});
</script>

