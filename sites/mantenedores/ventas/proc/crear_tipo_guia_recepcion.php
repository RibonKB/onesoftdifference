<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$new =$db->insert('tb_tipo_guia_recepcion',array(
	'dg_tipo' => $_POST['tipo_name'],
	'dc_tipo_movimiento' => $_POST['tipo_tipo_mov'],
	'dc_bodega' => $_POST['tipo_bodega'],
	'df_creacion' => 'NOW()',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
));

$error_man->showConfirm("Se ha creado el tipo de guia de recepción <strong>{$_POST['tipo_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");

echo("<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['tipo_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/ventas/ed_tipo_guia_despacho.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/ventas/del_tipo_guia_despacho.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>