<?php
define('MAIN',1);
require_once('../../../../inc/init.php');

//Validar que el nombre del tipo de cargo no esté siendo utilizado
$existe = $db->prepare($db->select('tb_tipo_cargo','dc_tipo_cargo','dc_empresa = ? AND dg_tipo_cargo = ?'));
$existe->bindValue(1,$empresa,PDO::PARAM_INT);
$existe->bindValue(2,$_POST['dg_tipo_cargo'],PDO::PARAM_STR);
$db->stExec($existe);

if($existe->fetch() !== false){
	$error_man->showWarning('El tipo de cargo ya existe');
	exit;
}

$db->start_transaction();

	//Insertar el nuevo tipo de cargo
	$tipo_cargo = $db->prepare($db->insert('tb_tipo_cargo',array(
		'dg_tipo_cargo' => '?',
		'df_creacion' => $db->getNow(),
		'dc_usuario_creacion' => $idUsuario,
		'dc_empresa' => $empresa,
		'dm_activo' => '1'
	)));
	$tipo_cargo->bindValue(1,$_POST['dg_tipo_cargo'],PDO::PARAM_STR);
	$db->stExec($tipo_cargo);

$db->commit();

$error_man->showConfirm('Se ha creado correctamente el tipo de cargo');
?>