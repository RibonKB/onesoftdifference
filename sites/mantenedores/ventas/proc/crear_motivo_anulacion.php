<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['motivo_anulacion'])){
	$error_man->showWarning('Debe especificar al menos un módulo');
	exit();
}

$new = $db->insert("tb_motivo_anulacion",
array(
	"dg_motivo" => ucwords($_POST['motivo_name']),
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_creacion" => "NOW()"
));

foreach($_POST['motivo_anulacion'] as $ma){
	$db->insert('tb_motivo_anulacion_modulo',array(
	'dc_modulo'=>$ma,
	'dc_motivo'=>$new
	));
	
}



$error_man->showConfirm("Se ha creado el motivo de anulación <strong>{$_POST['motivo_name']}</strong> correctamente");



$date_now = date("d/m/Y H:i");
echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['motivo_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/ventas/ed_motivo_anulacion.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/ventas/del_motivo_anulacion.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
$("#item<?=$new ?>").appendTo("#list");
$("a.loadOnOverlay").click(function(e){
	e.preventDefault();
	loadOverlay(this.href);
});
</script>