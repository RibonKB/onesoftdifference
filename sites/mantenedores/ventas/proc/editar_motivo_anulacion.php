<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dg_motivo = $_POST['dg_motivo_ed'];
$dc_motivo = intval($_POST['dc_motivo_ed']);



$motivo = $db->prepare($db->select('tb_motivo_anulacion','true','dg_motivo = ? AND dc_motivo != ? AND dc_empresa = ?'));
$motivo->bindValue(1,$dg_motivo,PDO::PARAM_STR);
$motivo->bindValue(2,$dc_motivo,PDO::PARAM_INT);
$motivo->bindValue(3,$empresa,PDO::PARAM_INT);
$db->stExec($motivo);

if($motivo->fetch() !== false){
	$error_man->showWarning("ya existe");
	exit;	
}


if(!isset($_POST['dc_modulo_ed'])){
	$error_man->showWarning('Debe especificar al menos un módulo');
	exit();
}

$db->start_transaction();

$editar = $db->prepare($db->update('tb_motivo_anulacion',array(
		'dg_motivo' => '?'
		),'dc_motivo = ? AND dc_empresa = ?'));
$editar->bindValue(1,$dg_motivo,PDO::PARAM_STR);
$editar->bindValue(2,$dc_motivo,PDO::PARAM_INT);
$editar->bindValue(3,$empresa,PDO::PARAM_INT);
$db->stExec($editar);

$editar2 = $db->prepare('DELETE FROM tb_motivo_anulacion_modulo WHERE dc_motivo = ?');
$editar2->bindValue(1,$dc_motivo,PDO::PARAM_INT);
$db->stExec($editar2);

foreach($_POST['dc_modulo_ed'] as $ma){
	$db->doExec($db->insert('tb_motivo_anulacion_modulo',array(
	'dc_modulo'=>$ma,
	'dc_motivo'=>$dc_motivo
	)));
	
}

$db->commit();

$error_man->showConfirm('Se ha modificado correctamente'); 
?>