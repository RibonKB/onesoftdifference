<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$tipoCambio = $_POST['tipo_tipo_cambio'];
if($tipoCambio == 0 ){
	$tipoCambio  = "NULL";
}

if(isset($_POST['tipo_zofri']) && $_POST['tipo_zofri'] == 1){
	$_POST['tipo_exenta'] = 1;
}

$new = $db->insert('tb_tipo_operacion',array(
	'dg_tipo_operacion' => $_POST['tipo_name'],
	'dc_tipo_movimiento' => $_POST['tipo_tipo_mov'],
	'dm_anticipada' => isset($_POST['tipo_anticipado'])?1:0,
	'dc_tipo_guia' => $_POST['tipo_tipo_guia'],
	'dc_tipo_cambio' => $tipoCambio,
	'dm_exenta' => isset($_POST['tipo_exenta'])?1:0,
	'dm_agrega_iva_producto' => isset($_POST['tipo_incluye_iva'])?1:0,
	'dm_zona_franca' => isset($_POST['tipo_zofri'])?1:0,
	'df_creacion' => 'NOW()',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
));

$error_man->showConfirm("Se ha creado el tipo operación <strong>{$_POST['tipo_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");

echo("<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['tipo_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/ventas/ed_tipo_operacion.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/ventas/del_tipo_operacion.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>