<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_tipo_cambio = intval($_POST['dc_tipo_cambio']);

foreach($_POST['dq_cambio'] as $v){
	if($v != '0' && !floatval($v)){
		$error_man->showWarning('El valor <b>'.$v.'</b> es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
		exit;
	}
}

if(!isset($_POST['dm_habil'])){
	$_POST['dm_habil'] = array();
}

$dc_mes = intval($_POST['dc_mes']);
if(!$dc_mes || $dc_mes > 12 || $dc_mes < 1){
	$error_man->showWarning('Error al intentar recuperar la información del mes, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

$dc_anho = intval($_POST['dc_anho']);
if(!$dc_anho || $dc_anho < 1970){
	$error_man->showWarning('Error al intentar la información del año, compruebe los datos de entrada y vuelva a intentarlo');
}


$db->start_transaction();
	
	$delete_old = $db->prepare('DELETE FROM tb_tipo_cambio_mensual WHERE YEAR(df_cambio) = ? AND MONTH(df_cambio) = ? AND dc_tipo_cambio = ?');
	$delete_old->bindValue(1,$dc_anho,PDO::PARAM_INT);
	$delete_old->bindValue(2,$dc_mes,PDO::PARAM_INT);
	$delete_old->bindValue(3,$dc_tipo_cambio,PDO::PARAM_INT);
	$db->stExec($delete_old);
	
	$insert_monto = $db->prepare($db->insert('tb_tipo_cambio_mensual',array(
		'dc_tipo_cambio' => '?',
		'df_cambio' => '?',
		'dq_cambio' => '?',
		'dm_habil' => '?',
		'df_creacion' => $db->getNow(),
		'dc_usuario_creacion' => $idUsuario
	)));
	$insert_monto->bindValue(1,$dc_tipo_cambio,PDO::PARAM_INT);
	$insert_monto->bindParam(2,$df_cambio,PDO::PARAM_STR);
	$insert_monto->bindParam(3,$dq_cambio,PDO::PARAM_STR);
	$insert_monto->bindParam(4,$dm_habil,PDO::PARAM_STR);
	
	
	foreach($_POST['dq_cambio'] as $dc_dia => $v){
		$df_cambio = $db->sqlDate2(date('d/m/Y',mktime(0,0,0,$dc_mes,$dc_dia+1,$dc_anho)));
		$dq_cambio = floatval($v);
		$dm_habil = in_array($dc_dia+1,$_POST['dm_habil'])?'1':'0';
		
		$db->stExec($insert_monto);
	}
	$update=$db->prepare($db->update('tb_tipo_cambio',array('dq_cambio'=>'?'),"dc_tipo_cambio= ? "));
                $dq_cambio2=$db->prepare($db->select('tb_tipo_cambio_mensual','dq_cambio',"DATE(df_cambio)=DATE(NOW()) AND dq_cambio != 0 AND dc_tipo_cambio={$dc_tipo_cambio}"));
                $db->stExec($dq_cambio2);
                $dq_cambio2=$dq_cambio2->fetchAll(PDO::FETCH_OBJ);
                $dq_cambio2=$dq_cambio2[0]->dq_cambio;
                
                if($dq_cambio2 != FALSE){
                   $update->bindValue(1,$dq_cambio2,PDO::PARAM_STR);
                   $update->bindValue(2,$dc_tipo_cambio,PDO::PARAM_INT);
                   $db->stExec($update);
                }
            
$db->commit();

$error_man->showConfirm('Se han generado los cambios correctamente.');
?>
<script type="text/javascript">
	loadpage("sites/mantenedores/ventas/src_tipo_cambio.php");
</script>