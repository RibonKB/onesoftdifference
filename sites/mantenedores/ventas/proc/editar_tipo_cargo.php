<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dg_tipo_cargo = $_POST['dg_tipo_cargo_ed'];
$dc_tipo_cargo = intval($_POST['dc_tipo_cargo_ed']);

//Rama aferente de validación
$tipo_cargo = $db->prepare($db->select('tb_tipo_cargo','true','dc_empresa = ? AND dg_tipo_cargo = ? AND dc_tipo_cargo <> ?'));
$tipo_cargo->bindValue(1,$empresa,PDO::PARAM_INT);
$tipo_cargo->bindValue(2,$dg_tipo_cargo,PDO::PARAM_STR);
$tipo_cargo->bindValue(3,$dc_tipo_cargo,PDO::PARAM_INT);
$db->stExec($tipo_cargo);

if($tipo_cargo->fetch() !== false){
	$error_man->showWarning('Ya existe un tipo de cargo con el nombre seleccionado');
	exit;
}

//Rama de proceso
$db->start_transaction();

$editar = $db->prepare($db->update('tb_tipo_cargo',array(
				'dg_tipo_cargo' => '?'
			),'dc_tipo_cargo = ? AND dc_empresa = ?'));
$editar->bindValue(1,$dg_tipo_cargo,PDO::PARAM_STR);
$editar->bindValue(2,$dc_tipo_cargo,PDO::PARAM_INT);
$editar->bindValue(3,$empresa,PDO::PARAM_INT);
$db->stExec($editar);

$db->commit();

//Rama deferente o de salida

$error_man->showConfirm('Se ha modificado el tipo de cargo correctamente');

?>