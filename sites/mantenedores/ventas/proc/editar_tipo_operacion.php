<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$tipoCambio = $_POST['ed_tipo_tipo_cambio'];
if($tipoCambio == 0 ){
	$tipoCambio  = 'NULL';
}

if(isset($_POST['tipo_zofri']) && $_POST['tipo_zofri'] == 1){
	$_POST['ed_tipo_exenta'] = 1;
}

//Se modifican los datos en la base de datos
$db->update("tb_tipo_operacion",
array(
	'dg_tipo_operacion' => $_POST['ed_tipo_name'],
	'dc_tipo_movimiento' => $_POST['ed_tipo_tipo_mov'],
	'dm_anticipada' => isset($_POST['ed_tipo_anticipado'])?1:0,
	'dc_tipo_guia' => $_POST['ed_tipo_tipo_guia'],
	'dc_tipo_cambio' => $tipoCambio,
	'dm_exenta' => isset($_POST['ed_tipo_exenta'])?1:0,
	'dm_agrega_iva_producto' => isset($_POST['ed_tipo_incluye_iva'])?1:0,
	'dm_zona_franca' => isset($_POST['tipo_zofri'])?1:0,
),"dc_tipo_operacion = {$_POST['ed_tipo_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/ventas/src_tipo_operacion.php");
</script>