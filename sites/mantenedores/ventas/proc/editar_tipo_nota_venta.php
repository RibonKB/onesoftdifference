<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

//Se modifican los datos en la base de datos
$db->update("tb_tipo_nota_venta",
array(
	'dg_tipo' => $_POST['ed_tipo_name'],
	'dc_tipo_guia_default' => $_POST['ed_tipo_guia_defecto'],
	'dm_facturable' => $_POST['ed_tipo_facturable'],
	'dm_costeable' => $_POST['ed_tipo_costeable'],
	'dm_contratable' => $_POST['ed_tipo_contratable'],
	'dm_tipo_impresion_factura' => $_POST['ed_tipo_impresion_factura']
),"dc_tipo = {$_POST['ed_tipo_id']} AND dc_empresa={$empresa}");

$db->query("DELETE FROM tb_tipo_nota_venta_tipo_guia WHERE dc_tipo_nota_venta = {$_POST['ed_tipo_id']}");

foreach($_POST['ed_tipo_tipos_guia'] as $tg){
	$db->insert('tb_tipo_nota_venta_tipo_guia',array(
		'dc_tipo_nota_venta' => $_POST['ed_tipo_id'],
		'dc_tipo_guia_despacho' => $tg
	));
}

$db->commit();

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/ventas/src_tipo_nota_venta.php");
</script>