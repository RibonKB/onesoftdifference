<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dc_motivo = intval($_POST['dc_motivo_ed']);

$motivo = $db->prepare($db->select('tb_motivo_anulacion','dm_activo','dc_empresa = ? AND dc_motivo = ?'));
$motivo->bindValue(1,$empresa,PDO::PARAM_INT);
$motivo->bindValue(2,$dc_motivo,PDO::PARAM_INT); 
$db->stExec($motivo);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_motivo_anulacion',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_motivo =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_motivo,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamente'); 


?>