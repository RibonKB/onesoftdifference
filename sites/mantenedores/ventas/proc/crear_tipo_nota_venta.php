<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['tipo_tipos_guia'])){
	$error_man->showWarning('Debe especificar al menos un tipo de guía de despacho permitida para poder continuar');
	exit();
}

$db->start_transaction();

$tipo = $db->insert('tb_tipo_nota_venta',array(
	'dg_tipo' => $_POST['tipo_name'],
	'dc_tipo_guia_default' => $_POST['tipo_guia_defecto'],
	'dm_facturable' => $_POST['tipo_facturable'],
	'dm_tipo_impresion_factura' => $_POST['tipo_impresion_factura'],
	'dm_costeable' => $_POST['tipo_costeable'],
	'dm_contratable' => $_POST['tipo_contratable'],
	'df_creacion' => 'NOW()',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
));

foreach($_POST['tipo_tipos_guia'] as $tg){
	$db->insert('tb_tipo_nota_venta_tipo_guia',array(
		'dc_tipo_nota_venta' => $tipo,
		'dc_tipo_guia_despacho' => $tg
	));
}

$db->commit();
$error_man->showConfirm('Se ha creado el tipo de nota de venta correctamente');
?>