<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$new = $db->insert("tb_cotizacion_estado",
array(
	"dg_estado" => ucwords($_POST['estado_name']),
	"dm_fin_proceso" => $_POST['estado_finaliza'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_creacion" => "NOW()"
));

$error_man->showConfirm("Se ha creado el estado de cotización <strong>{$_POST['estado_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");
$lock = $_POST['estado_finaliza']?"<img src='images/lock.png' alt='' title='Finaliza el proceso de cotización' />":'';
echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$lock}</td>
	<td>{$_POST['estado_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/ventas/ed_estado_cotizacion.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/ventas/del_estado_cotizacion.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>