<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_tipo = intval($_POST['dc_tipo_ed']);

$tipo_venta = $db->prepare($db->select('tb_tipo_nota_venta','dm_activo','dc_empresa = ? AND dc_tipo = ?'));
$tipo_venta->bindValue(1,$empresa,PDO::PARAM_INT);
$tipo_venta->bindValue(2,$dc_tipo,PDO::PARAM_INT);
$db->stExec($tipo_venta);

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_tipo_nota_venta',array('dm_activo' => '0'),'dc_empresa = ? AND dc_tipo = ?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_tipo,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamente');

?>