<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_tipo_guia_recepcion",
array(
	'dg_tipo' => $_POST['ed_tipo_name'],
	'dc_tipo_movimiento' => $_POST['ed_tipo_tipo_mov'],
	'dc_bodega' => $_POST['ed_tipo_bodega']
),"dc_modo_envio = {$_POST['ed_tipo_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/ventas/src_tipo_guia_recepcion.php");
</script>