<?php
/**
*	Edicion de linea de negocio
**/
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_tipo_cambio",
array(
	"dg_tipo_cambio" => trim(ucwords($_POST['ed_cambio_name'])),
	"dq_cambio" => $_POST['ed_cambio_cambio'],
	"dn_cantidad_decimales" => $_POST['ed_cambio_decimales'],
),"dc_tipo_cambio = {$_POST['ed_cambio_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/ventas/src_tipo_cambio.php");
</script>
