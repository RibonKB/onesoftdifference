<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_modo_envio",
"dg_modo_envio",
"dc_modo_envio= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el modo de envio especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar modos de envio</div>
<div class="panes">
	<form action="sites/mantenedores/ventas/proc/delete_modo_envio.php" class="confirmValidar" id="del_modo_envio">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el modo de envio '<?=$datos[0]['dg_modo_envio'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_envio_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_modo_envio_res"></div>
</div>