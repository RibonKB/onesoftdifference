<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_cotizacion_estado",
"dg_estado",
"dc_estado= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el estado de cotizacion especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar estados de cotización</div>
<div class="panes">
	<form action="sites/mantenedores/ventas/proc/delete_estado_cotizacion.php" class="confirmValidar" id="del_estado_cotizacion">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el estado de cotización '<?=$datos[0]['dg_estado'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_estado_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_estado_cotizacion_res"></div>
</div>