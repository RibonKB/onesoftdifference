<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo = intval($_POST['id']);

$tipo_venta = $db->prepare($db->select('tb_tipo_nota_venta','dg_tipo, dm_activo', 'dc_tipo = ? AND dc_empresa = ?'));
$tipo_venta->bindValue(1,$dc_tipo,PDO::PARAM_INT);
$tipo_venta->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($tipo_venta);

$tipo_venta = $tipo_venta->fetch(PDO::FETCH_OBJ);

if($tipo_venta === false){
	$error_man->showWarning('El tipo de venta seleccionado no existe');
	exit();
}

?>

<div class="secc_bar" >
	Eliminar medios de cobro
</div>

<div class="panes" >
	<?php $form->Start('sites/mantenedores/ventas/proc/delete_tipo_nota_venta.php','del_tipo_venta') ?>
	<?php $form->Header('¿Está seguro que desea eliminar el tipo de nota de venta?') ?>
	<div class="center">
    	<?php echo $tipo_venta->dg_tipo ?>
     </div>
    <?php $form->Hidden('dc_tipo_ed',$dc_tipo) ?>
    <?php $form->End('Eliminar','delbtn') ?>
</div> 
	