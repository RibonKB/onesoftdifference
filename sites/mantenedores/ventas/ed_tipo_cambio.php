<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_cambio",
"dg_tipo_cambio,dq_cambio,dn_cantidad_decimales",
"dc_tipo_cambio = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de cambio especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipos de cambio</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el tipo de cambio");
	$form->Start("sites/mantenedores/ventas/proc/editar_tipo_cambio.php","ed_tipo_cambio");
	$form->Section();
	$form->Text("Nombre","ed_cambio_name",1,255,$datos['dg_tipo_cambio']);
	$form->Select('Cantidad de decimales','ed_cambio_decimales',array(0,1,2,3,4,5),0,$datos['dn_cantidad_decimales']);
	$form->EndSection();
	$form->Section();
	$form->Text("Cambio","ed_cambio_cambio",1,10,$datos['dq_cambio']);
	$form->EndSection();
	$form->Hidden("ed_cambio_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>