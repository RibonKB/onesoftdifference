<?php
/**
*
*	Permite a los usuarios agregar nuevas lineas de negocio.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Estados de nota de venta</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo estado de nota de venta<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/ventas/proc/crear_estado_nota_venta.php","cr_estado_nota_venta");
	$form->Text("Estado","estado_name",1);
	$form->RadioBox('<br />Finaliza el proceso de nota de venta','estado_finaliza',array('NO','SI'),0,' ');
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_nota_venta_estado",
		"dc_estado, dg_estado, dm_fin_proceso, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_estado")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Estados ya agregados</caption>
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th width="65%">Estado cotización</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
	$lock = $l['dm_fin_proceso']?'<img src="images/lock.png" alt="" title="Finaliza el proceso de nota de venta" />':'';
		echo("
		<tr id='item{$l['dc_estado']}'>
			<td>{$lock}</td>
			<td>{$l['dg_estado']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ventas/ed_estado_nota_venta.php?id={$l['dc_estado']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/ventas/del_estado_nota_venta.php?id={$l['dc_estado']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>