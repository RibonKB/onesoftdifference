<?php
define('MAIN',1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo_cargo = intval($_POST['id']);

$tipo_cargo = $db->prepare($db->select('tb_tipo_cargo','dg_tipo_cargo','dc_tipo_cargo = ? AND dc_empresa = ?'));
$tipo_cargo->bindValue(1,$dc_tipo_cargo,PDO::PARAM_INT);
$tipo_cargo->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($tipo_cargo);

$tipo_cargo = $tipo_cargo->fetch(PDO::FETCH_OBJ);

if($tipo_cargo === false){
	$error_man->showWarning('El tipo de cargo seleccionado no fue encontrado');
	exit;
}

//Rama aferente de entrada de datos
?>
<div class="secc_bar">
	Editar tipos de cargo
</div>
<div class="panes">
	<?php $form->Start('sites/mantenedores/ventas/proc/editar_tipo_cargo.php','ed_tipo_cargo') ?>
        <?php $form->Header('<b>Ingrese el nombre para actualizar el tipo de cargo</b><br />Los campos marcados con [*] son obligatorios') ?>
        <?php $form->Text('Nombre','dg_tipo_cargo_ed',true,Form::DEFAULT_TEXT_LENGTH,$tipo_cargo->dg_tipo_cargo) ?>
        <?php $form->Hidden('dc_tipo_cargo_ed',$dc_tipo_cargo) ?>
    <?php $form->End('Editar','editbtn') ?>
</div>