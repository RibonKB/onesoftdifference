<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_modo_envio",
"dg_modo_envio",
"dc_modo_envio = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el modo de envio especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición modos de envio</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el modo de envio");
	$form->Start("sites/mantenedores/ventas/proc/editar_modo_envio.php","ed_modo_envio");
	$form->Text("Nombre","ed_envio_name",1,255,$datos['dg_modo_envio']);
	$form->Hidden("ed_envio_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>