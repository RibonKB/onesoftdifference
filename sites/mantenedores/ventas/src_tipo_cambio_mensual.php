<?php
define("MAIN",1);
require_once("../../../inc/init.php");

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo_cambio = intval($_POST['id']);

$meses = array(
	1 => 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
);

?>
<div class="secc_bar">Configuración de valores de tipo de cambio mensuales</div>
<div class="panes">
	<?php $form->Start('sites/mantenedores/ventas/cr_tipo_cambio_mensual.php','src_tipo_cambio_mensual','overlayValidar') ?>
    <?php $form->Header('Indique el periodo para inicializar el tipo de cambio mensual') ?>
    	<?php
			$form->Section();
				$form->Select('Mes','dc_mes',$meses,1,date('m'));
				$form->Text('Año','dc_anho',1,4,date('Y'));
			$form->EndSection();
		?>
    <?php $form->Hidden('dc_tipo_cambio',$dc_tipo_cambio) ?>
    <?php $form->End('Ejecutar búsqueda','searchbtn') ?>
</div>