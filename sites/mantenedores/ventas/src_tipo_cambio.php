<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de cambio</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de cambio<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/ventas/proc/crear_tipo_cambio.php","cr_tipo_cambio");
	$form->Section();
	$form->Text("Nombre","cambio_name",1);
	$form->Select('Cantidad de decimales','cambio_decimales',array(0,1,2,3,4,5));
	$form->EndSection();
	$form->Section();
	$form->Text('1 <strong id="tc_nombre" style="text-transform:capitalize;">-</strong> en pesos = ',"cambio_cambio",1,10);
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_tipo_cambio",
		"dc_tipo_cambio, dg_tipo_cambio, dq_cambio, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_tipo_cambio")
	);
                
       $cm=$db->select('tb_tipo_cambio tc '
               . 'LEFT JOIN tb_tipo_cambio_mensual tcm ON tcm.dc_tipo_cambio=tc.dc_tipo_cambio',
               'tc.dc_tipo_cambio, tc.dg_tipo_cambio,tcm.dq_cambio',"DATE(tcm.df_cambio)=DATE(NOW()) AND tc.dc_empresa={$empresa}");
	
	echo('
	<table class="tab" width="100%">
	<caption>Tipos de cambio ya agregados</caption>
	<thead>
		<!-- tr>
			<th colspan="2">&nbsp;</th>
			<th colspan="3">Moneda por defecto</th>
			<th colspan="2">&nbsp;</th>
		</tr -->
		<tr>
			<th width="45%">Tipo de cambio</th>
			<th width="20%">Cambio</th>
			<!-- th>Ventas</th>
			<th>Compras</th>
			<th>Contabilidad</th -->
			<th width="20%">Fecha de creación</th>
			<th>Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
            $cambio=$l['dq_cambio'];
            foreach($cm as $t):
                if($t['dc_tipo_cambio']==$l['dc_tipo_cambio']){
                    if($t['dq_cambio'] != '0,00' || $t['dq_cambio'] != 0){
                        $cambio=$t['dq_cambio'];
                    }
                }
            endforeach;
		echo("
		<tr id='item{$l['dc_tipo_cambio']}'>
			<td>{$l['dg_tipo_cambio']}</td>
			<td align='right'>".moneda_local($cambio)." {$empresa_conf['dg_moneda_local']}</td>
			<td align='center'>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ventas/ed_tipo_cambio.php?id={$l['dc_tipo_cambio']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' class='left' />
				</a>
				<a href='sites/mantenedores/ventas/del_tipo_cambio.php?id={$l['dc_tipo_cambio']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' class='left' />
				</a>
				<a href='sites/mantenedores/ventas/src_tipo_cambio_mensual.php?id={$l['dc_tipo_cambio']}' class='loadOnOverlay'>
					<img src='images/doc.png' alt='' title='Configurar tipo de cambio mensual'>
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>
<script type="text/javascript">
$("#cambio_name").keyup(function(){
	$("#tc_nombre").html($(this).val());
});
</script>