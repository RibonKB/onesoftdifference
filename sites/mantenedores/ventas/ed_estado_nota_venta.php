<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_nota_venta_estado",
"dg_estado",
"dc_estado = {$_POST['id']} AND dc_empresa = {$empresa}");
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el modo de envio especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición estados nota de venta</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el estado de nota de venta");
	$form->Start("sites/mantenedores/ventas/proc/editar_estado_nota_venta.php","ed_estado_nota_venta");
	$form->Text("Nombre","ed_estado_name",1,255,$datos['dg_estado']);
	$form->Hidden("ed_estado_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>