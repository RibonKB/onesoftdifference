<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$list = $db->select(
	"tb_tipo_proveedor",
	"dc_tipo_proveedor, dg_tipo_proveedor, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha, dm_incluido_libro_compra",
	"dc_empresa={$empresa} AND dm_activo='1'",
	array("order_by" => "dg_tipo_proveedor")
);
?>
<div id="secc_bar">Tipos de proveedores</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de proveedor<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/proc/crear_tipo_proveedor.php","cr_tipo_proveedor");
	$form->Text("Nombre","tipo_name",1);
	$form->Radiobox('Facturas de este proveedor aparecen en libro de compras','dm_incluido_libro_compra',array(1 => 'SI', 0 => 'NO'),1);
	$form->End("Crear","addbtn");
?>

	<table class="tab" width="100%">
	<caption>Tipos de proveedor ya agregados</caption>
	<thead>
		<tr>
			<th>Tipo de proveedor</th>
			<th>Fecha de creación</th>
			<th>Incluido en el libro de compras</th>
			<th>Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	
	<?php foreach($list as $l): ?>
		<tr id='item{$l['dc_tipo_proveedor']}'>
			<td><?php echo $l['dg_tipo_proveedor'] ?></td>
			<td><?php echo $l['df_fecha'] ?></td>
			<td><?php echo $l['dm_incluido_libro_compra']==1?'SI':'NO' ?></td>
			<td>
				<a href='sites/mantenedores/ed_tipo_proveedor.php?id=<?php echo $l['dc_tipo_proveedor'] ?>' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/del_tipo_proveedor.php?id=<?php echo $l['dc_tipo_proveedor'] ?>' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
	
	</tbody></table>
</div>
</div>