<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_linea_negocio",
"dg_linea_negocio",
"dc_linea_negocio= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la linea de negocio especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar cargos de contacto</div>
<div class="panes">
	<form action="sites/mantenedores/proc/delete_linea_negocio_producto.php" class="confirmValidar" id="del_linea">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la linea de negocio '<?=$datos[0]['dg_linea_negocio'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_linea_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_linea_res"></div>
</div>