<?php
/**
*
*	Permite a los usuarios agregar nuevas regiones.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Regiones</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear una nueva región<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/proc/crear_region.php","cr_region");
	$form->Text("Nombre","region_name",1);
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_region",
		"dc_region, dg_region, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"",
		array("order_by" => "dg_region")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Regiones ya agregadas</caption>
	<thead>
		<tr>
			<th width="65%">Región</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_region']}'>
			<td>{$l['dg_region']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ed_region.php?id={$l['dc_region']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/del_region.php?id={$l['dc_region']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>