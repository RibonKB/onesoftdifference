<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_region",
"dg_region",
"dc_region = {$_POST['id']}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la región especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición regiones</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la región");
	$form->Start("sites/mantenedores/proc/editar_region.php","ed_region");
	$form->Text("Nombre","ed_region_name",1,255,$datos['dg_region']);
	$form->Hidden("ed_region_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>