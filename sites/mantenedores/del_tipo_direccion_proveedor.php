<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_direccion_proveedor",
"dg_tipo_direccion",
"dc_tipo_direccion= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de dirección especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar tipo de dirección</div>
<div class="panes">
	<form action="sites/mantenedores/proc/delete_tipo_direccion_proveedor.php" class="confirmValidar" id="del_tipo_direccion">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el tipo de dirección '<?=$datos[0]['dg_tipo_direccion'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_tipo_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_tipo_direccion_res"></div>
</div>