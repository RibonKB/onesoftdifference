<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Isapres</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear una nueva isapre<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/rrhh/proc/crear_isapre.php","cr_isapre");
	$form->Text("Nombre","isapre_name",1);
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_isapre",
		"dc_isapre, dg_isapre, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_isapre")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Isapres ya agregadas</caption>
	<thead>
		<tr>
			<th width="65%">Isapre</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_isapre']}'>
			<td>{$l['dg_isapre']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/rrhh/ed_isapre.php?id={$l['dc_isapre']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/rrhh/del_isapre.php?id={$l['dc_isapre']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>