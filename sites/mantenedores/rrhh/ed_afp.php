<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_afp",
"dg_afp",
"dc_afp = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la AFP especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición AFP</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la AFP");
	$form->Start("sites/mantenedores/rrhh/proc/editar_afp.php","ed_afp");
	$form->Text("Nombre","ed_afp_name",1,255,$datos['dg_afp']);
	$form->Hidden("ed_afp_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>