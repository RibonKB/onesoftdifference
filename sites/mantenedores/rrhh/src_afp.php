<?php
/**
*
*	Permite a los usuarios agregar nuevas lineas de negocio.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">AFP</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear una nueva AFP<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/rrhh/proc/crear_afp.php","cr_segmento");
	$form->Text("Nombre","afp_name",1);
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_afp",
		"dc_afp, dg_afp, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_afp")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>AFP ya agregadas</caption>
	<thead>
		<tr>
			<th width="65%">AFP</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_afp']}'>
			<td>{$l['dg_afp']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/rrhh/ed_afp.php?id={$l['dc_afp']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/rrhh/del_afp.php?id={$l['dc_afp']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>