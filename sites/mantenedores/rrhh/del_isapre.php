<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_isapre",
"dg_isapre",
"dc_isapre= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la isapre especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar isapre</div>
<div class="panes">
	<form action="sites/mantenedores/rrhh/proc/delete_isapre.php" class="confirmValidar" id="del_isapre">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la isapre '<?=$datos[0]['dg_isapre'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_isapre_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_isapre_res"></div>
</div>