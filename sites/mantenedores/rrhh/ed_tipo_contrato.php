<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_contrato",
"dg_tipo_contrato",
"dc_tipo_contrato = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de contrato especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipos de contrato</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el tipo de contrato");
	$form->Start("sites/mantenedores/rrhh/proc/editar_tipo_contrato.php","ed_tipo_contrato");
	$form->Text("Nombre","ed_tipo_name",1,255,$datos['dg_tipo_contrato']);
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>