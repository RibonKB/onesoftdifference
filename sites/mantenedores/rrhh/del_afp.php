<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_afp",
"dg_afp",
"dc_afp= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la AFP especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar AFP</div>
<div class="panes">
	<form action="sites/mantenedores/rrhh/proc/delete_afp.php" class="confirmValidar" id="del_afp">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la AFP '<?=$datos[0]['dg_afp'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_afp_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_afp_res"></div>
</div>