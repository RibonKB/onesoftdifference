<?php
/**
*	Edicion de linea de negocio
**/
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_isapre",
array(
	"dg_isapre" => trim(ucwords($_POST['ed_isapre_name']))
),"dc_isapre = {$_POST['ed_isapre_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/rrhh/src_isapre.php");
</script>