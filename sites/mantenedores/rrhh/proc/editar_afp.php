<?php
/**
*	Edicion de linea de negocio
**/
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_afp",
array(
	"dg_afp" => trim(ucwords($_POST['ed_afp_name']))
),"dc_afp = {$_POST['ed_afp_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/rrhh/src_afp.php");
</script>