<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_segmento",
"dg_segmento",
"dc_segmento= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el segmento especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar segmentos</div>
<div class="panes">
	<form action="sites/mantenedores/est_empresa/proc/delete_segmento.php" class="confirmValidar" id="del_segmento">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el segmento '<?=$datos[0]['dg_segmento'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_segmento_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_segmento_res"></div>
</div>