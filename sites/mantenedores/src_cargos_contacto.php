<?php
/**
*
*	Permite a los usuarios agregar nuevos cargos de contactos.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Cargos para contactos de clientes y proveedores</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo cargo<br />Puede consultar los que ya est&aacute;n agregados.");
	$form->Start("sites/mantenedores/proc/crear_cargo_contacto.php","cr_cargo_contacto");
	$form->Text("Nombre","cargo_name",1);
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_cargo_contacto",
		"dc_cargo_contacto, dg_cargo_contacto, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_cargo_contacto")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Cargos de contacto ya agregados</caption>
	<thead>
		<tr>
			<th width="65%">Cargo</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $c){
		echo("
		<tr id='item{$c['dc_cargo_contacto']}'>
			<td>{$c['dg_cargo_contacto']}</td>
			<td>{$c['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ed_cargo_contacto.php?id={$c['dc_cargo_contacto']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/del_cargo_contacto.php?id={$c['dc_cargo_contacto']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>