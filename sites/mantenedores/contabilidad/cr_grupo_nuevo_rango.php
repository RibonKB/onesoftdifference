<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}


echo("<div class='secc_bar'>Gestor de rangos de cuentas para grupos de cuentas contables</div>
<div class='panes'>");

if(isset($_POST['rid'])){
	$db->update("tb_rango_cuenta_contable",array('dm_actual' => 0),"dc_grupo_cuenta = {$_POST['id']} AND dm_actual = '1'");
	$db->update("tb_rango_cuenta_contable",array('dm_actual' => 1),"dc_rango = {$_POST['rid']}");
	$error_man->showConfirm("El rango actual se a cambiado correctamente");
}

require_once('../../../inc/form-class.php');
$form = new Form($empresa);

$form->Header("Indique los datos para agregar un nuevo rango al grupo");
$form->Start('sites/mantenedores/contabilidad/proc/crear_grupo_nuevo_rango.php','cr_rango_cuenta');
$form->Section();
$form->Text('Desde','rango_desde',1);
$form->Combobox('','rango_actual',array('Utilizar este rango como el activo'),array(0));
$form->Endsection();
$form->Section();
$form->Text('Hasta','rango_hasta',1);
$form->EndSection();
$form->Hidden('rango_grupo',$_POST['id']);
$form->End('Crear','addbtn');

$rangos = $db->select(
"(SELECT * FROM tb_grupo_cuenta_contable WHERE dc_grupo_cuenta = {$_POST['id']} AND dc_empresa = {$empresa} AND dm_activo = '1') AS c
JOIN tb_rango_cuenta_contable AS r ON c.dc_grupo_cuenta = r.dc_grupo_cuenta
LEFT JOIN tb_cuenta_contable AS cta ON (c.dc_grupo_cuenta = cta.dc_grupo_cuenta AND cta.dg_codigo BETWEEN r.dc_desde AND r.dc_hasta)",
"c.dg_grupo_cuenta, r.dc_desde, r.dc_hasta, dm_actual, DATE_FORMAT(r.df_creacion,'%d/%m/%Y %H:%i') as df_fecha, dg_cuenta_contable, MAX(cta.dg_codigo) AS dg_codigo, dc_rango",'1',array("group_by" => "r.dc_desde"));

if(!count($rangos)){
	$error_man->showAviso('No hay rangos asignados a este grupo</div>');
	exit();
}

echo("<table class='tab' width='100%'>
<caption>Rangos del grupo de cuentas {$rangos[0]['dg_grupo_cuenta']}</caption>
<thead>
<tr>
	<th width='90'>Desde</th>
	<th width='90'>Hasta</th>
	<th width='32'>Opciones</th>
	<th>Última cuenta agregada</th>
	<th width='90'>Cupos disponibles</th>
</tr>
</thead>
<tbody>");

foreach($rangos as $r){
	if($r['dg_codigo'] == NULL){
		$cta = "Sin cuentas agregadas";
		$cupos = $r['dc_hasta']-$r['dc_desde'];
		$opt_del = "<a href='sites/mantenedores/contabilidad/proc/del_grupo_rango.php?rid={$r['dc_rango']}' class='loadOnOverlay right'>
			<img src='images/delbtn.png' alt='' title='Eliminar rango' /></a>";
	}else{
		$cta = "({$r['dg_codigo']}) {$r['dg_cuenta_contable']}";
		$cupos = $r['dc_hasta']-$r['dg_codigo'];
		$opt_del = '';
	}
	if($r['dm_actual'] == '0'){
		$act = '';
		if($cupos > 0){
			$act = "
			<a href='sites/mantenedores/contabilidad/cr_grupo_nuevo_rango.php?id={$_POST['id']}&rid={$r['dc_rango']}' class='loadOnOverlay left'>
			<img src='images/emptybtn.png' alt='' title='usar como activo' /></a>";
		}
	}else{
		$act = "<img src='images/confirm.png' alt='A' title='Rango activo' />";
	}
	
	echo("
	<tr>
		<td>{$r['dc_desde']}</td>
		<td>{$r['dc_hasta']}</td>
		<td>{$act}{$opt_del}</td>
		<td>{$cta}</td>
		<td>{$cupos}</td>
	</tr>
	");
}

echo("</tbody></table>")
?>
</div>
<script type="text/javascript">
$("#rango_desde").change(function(){
	v = $(this).val();
	for(i=v.length;i<<?=$empresa_conf['dn_largo_codigo_cuenta'] ?>;i++){
		v += '0';
	}
	$(this).val(v);
});
$("#rango_hasta").change(function(){
	v = $(this).val();
	for(i=v.length;i<<?=$empresa_conf['dn_largo_codigo_cuenta'] ?>;i++){
		v += '9';
	}
	$(this).val(v);
});
</script>