<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_movimiento",
"dg_codigo,dg_tipo_movimiento,dg_descripcion",
"dc_tipo_movimiento = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de movimiento especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipos de movimiento</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$cuentas_seleccionadas = $db->select("tb_cuenta_contable c,tb_cuentas_tipo_movimiento t",
	"c.dc_cuenta_contable,c.dg_cuenta_contable,c.dg_codigo",
	"c.dc_cuenta_contable = t.dc_cuenta_contable AND t.dc_tipo_movimiento = {$_POST['id']} AND c.dc_empresa = {$empresa} AND c.dm_activo = 1",
	array("order_by" => "dg_cuenta_contable"));
	
	$cuentas = $db->select("tb_cuenta_contable",
	"dc_cuenta_contable,dg_cuenta_contable,dg_codigo",
	"dc_cuenta_contable NOT IN(SELECT dc_cuenta_contable FROM tb_cuentas_tipo_movimiento WHERE dc_tipo_movimiento = {$_POST['id']}) AND dc_empresa = {$empresa} AND dm_activo = 1",
	array("order_by" => "dg_cuenta_contable"));
	
	$c_left = array();
	$c_right = array();
	
	foreach($cuentas_seleccionadas as $c){
		$c_left[$c['dc_cuenta_contable']] = $c['dg_codigo']." ".$c['dg_cuenta_contable'];
	}
	foreach($cuentas as $c){
		$c_right[$c['dc_cuenta_contable']] = $c['dg_codigo']." ".$c['dg_cuenta_contable'];
	}
	
	$form->Header("Indique los datos actualizados para el tipo de movimiento");
	$form->Start("sites/mantenedores/contabilidad/proc/editar_tipo_movimiento.php","ed_tipo_movimiento","confirmValidar");
	$form->Section();
	$form->Text("Nombre","ed_tipo_name",1,255,$datos['dg_tipo_movimiento']);
	$form->Textarea("Descripción","ed_tipo_desc",0,$datos['dg_descripcion']);
	$form->EndSection();
	$form->Section();
	$form->Text("Código","ed_tipo_codigo",1,255,$datos['dg_codigo']);
	$form->EndSection();
	$form->Group();
	$form->Header("Agregar Cuentas contables al tipo de movimiento");
	echo("<table align='center'><tr><td valign='bottom'>");
	$form->Multiselect('Cuentas contables en el tipo de movimiento','ed_tipo_cuentas',$c_left);
	echo("</td><td valign='middle'><br />
	<button type='button' id='ed_add_cuentas'>&laquo;</button><br />
	<button type='button' id='ed_del_cuentas'>&raquo;</button>
	</td><td valign='bottom'>");
	$form->Multiselect('Filtro: <input type="text" id="ed_cuenta_filtro" size="35" />','ed_lista_cuentas',$c_right);
	echo("</td></tr></table>");
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
	
	echo("<select id='ed_filtradas' style='display:none;'></select>");
?>
</div>
<script type="text/javascript">
$("#ed_cuenta_filtro").keyup(function(){
	cri = $(this).val();
	$("#ed_lista_cuentas option").each(function(){
		if($(this).text().toLowerCase().indexOf(cri) == -1){
			$(this).remove().appendTo("#ed_filtradas");
		}
	});
	$("#ed_filtradas option").each(function(){
		if($(this).text().toLowerCase().indexOf(cri) != -1){
			$(this).remove().appendTo("#ed_lista_cuentas");
		}
	});
});

$("#ed_add_cuentas").click(function(){
	$("#ed_lista_cuentas option:selected").remove().appendTo("#ed_tipo_cuentas");
});
$("#ed_del_cuentas").click(function(){
	$("#ed_tipo_cuentas option:selected").remove().appendTo("#ed_lista_cuentas");
	$("#ed_cuenta_filtro").trigger('keyup');
});

$("#ed_tipo_movimiento").submit(function(){ 
	$("#ed_tipo_cuentas option").attr("selected",true);
});
</script>