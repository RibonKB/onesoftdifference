<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_banco",
"dg_banco",
"dc_banco= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el banco especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar bancos</div>
<div class="panes">
	<form action="sites/mantenedores/contabilidad/proc/delete_banco.php" class="confirmValidar" id="del_banco">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el banco '<?=$datos[0]['dg_banco'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_banco_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_banco_res"></div>
</div>