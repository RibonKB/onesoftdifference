<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_periodo_contable",
"dc_anho",
"dc_periodo_contable= {$_POST['id']}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el periodo contable especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar periodos contables</div>
<div class="panes">
	<form action="sites/mantenedores/contabilidad/proc/delete_periodo_contable.php" class="confirmValidar" id="del_periodo">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el periodo contable '<?=$datos[0]['dc_anho'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_periodo_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_periodo_res"></div>
</div>