<?php
/**
*	mantenedor de periodos contables
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$list = $db->select(
		"tb_mes_contable m
		JOIN tb_periodo_contable p ON p.dc_mes_contable = m.dc_mes_contable",
		"p.dc_anho, p.dm_estado, p.dm_estado_factura_compra, p.dm_estado_factura_venta, p.dc_periodo_contable,m.dc_mes_contable, m.dc_mes,p.df_creacion",
		'p.dm_estado = 1 AND m.dc_empresa = '.$empresa,
		array("order_by" => "dc_mes ASC, dc_anho DESC")
	);
	
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
?>
<div id="secc_bar">Periodos contables</div>
<div id="main_cont">
	<div class="panes">
		<?php
		$form->Header("Complete los datos requeridos para crear un nuevo Periodo contable<br />Puede consultar los que ya están agregados.");
		$form->Start("sites/mantenedores/contabilidad/proc/crear_periodo_contable.php","cr_banco");
		$form->Section();
		$form->Text("Mes contable","periodo_mes",1,2);
		$form->Textarea("Comentario","periodo_desc");
		$form->EndSection();
		$form->Section();
		?>
		<table width='375' class='tab' style='display:none;'><caption>Periodos</caption>
			<thead>
				<tr>
					<th width='7%'>&nbsp;</th>
					<th width='35%'>Año</th>
					<th>Estado Contabilidad</th>
					<th width='29%'>Opciones</th>
				</tr>
			</thead>
			<tbody id='periodos'>
			</tbody>
		</table>
		<br /><button type='button' class='addbtn' id='add_periodo'>Añadir Periodo</button>
		<?php
		$form->EndSection();
		$form->End("Crear","addbtn");
		?>

	<table id='plantilla' style='display:none;'>
	<tbody>
		<tr>
			<td><img src='images/delbtn.png' alt='' class='del_periodo' /></td>
			<td><input type='text' name='per_anho[]' class='inputtext per_anho' size='15' required='required' maxlength='4' /></td>
			<td>
				<select name='per_estado[]' class='inputtext' style='width:100px;'>
					<option value='1'>Abierto</option>
					<option value='0'>Cerrado</option>
				</select>
			</td>
			<td>
				<select name='per_filtro[]' class='per_filtro inputtext' style='width:100px;'>
					<option value='0'>No filtrar</option>
					<option value='1'>Filtrar</option>
				</select>
			</td>
		</tr>
		<tr style='display:none;'>
			<td colspan='4' align='center'>
				<select name='per_filtro_tipo[]' class='per_filtro_tipo inputtext' style='width:140px;'>
					<option value='1'>Filtrar por grupo</option>
					<option value='2'>Filtrar por usuarios</option>
				</select>
				<input type='text' class='per_filtro_grupo' />
				<input type='text' class='per_filtro_usuario' style='display:none;' />
				<input type='hidden' name='per_filtro_list[]' class='per_filtro_list' value='' />
			</td>
		</tr>
		<tr style='display:none;'>
			<td colspan='4' class='filtro_list'></td>
		</tr>
	</tbody>
	</table>
	
	<table class="tab" width="100%">
	<caption>Periodos contables ya agregados</caption>
	<thead>
		<tr>
			<th width="11%">Mes contable</th>
			<th width="25%">Periodo contable</th>
			<th>Estado Cont</th>
			<th>Estado FC</th>
			<th>Estado FV</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	
	<?php foreach($list as $l): ?>
		<?php if($l['dc_mes'] % 2) $st = ""; else $st = '';	?>
		<tr id='item{$l['dc_periodo_contable']}'>
			<td align='center' <?php echo $st ?>><?php echo $l['dc_mes'] ?></td>
			<td <?php echo $st ?>><?php echo $l['dc_anho'] ?></td>
			<td>
				<?php if($l['dm_estado'] == 1): ?>
					<div class="confirm">Abierto</div>
				<?php else: ?>
					<div class="warning">Cerrado</div>
				<?php endif; ?>
			</td>
			<td>
				<?php if($l['dm_estado_factura_compra'] == 1): ?>
					<div class="confirm">Abierto</div>
				<?php else: ?>
					<div class="warning">Cerrado</div>
				<?php endif; ?>
			</td>
			<td>
				<?php if($l['dm_estado_factura_venta'] == 1): ?>
					<div class="confirm">Abierto</div>
				<?php else: ?>
					<div class="warning">Cerrado</div>
				<?php endif; ?>
			</td>
			<td <?php echo $st ?>><?php echo $db->dateTimeLocalFormat($l['df_creacion']) ?></td>
			<td <?php echo $st ?>>
				<a
					href='sites/mantenedores/contabilidad/ed_periodo_contable.php?id=<?php echo $l['dc_periodo_contable'] ?>&m=<?php echo $l['dc_mes_contable'] ?>'
					class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' class='left' />
				</a>
				<a href='sites/mantenedores/contabilidad/del_periodo_contable.php?id=<?php $l['dc_periodo_contable'] ?>' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' class='left' />
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
	
	</tbody></table>
</div>
</div>
<script type="text/javascript" src="jscripts/sites/contabilidad/src_periodo_contable.js?v=1_0b"></script>