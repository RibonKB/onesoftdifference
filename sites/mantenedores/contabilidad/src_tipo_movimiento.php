<?php
/**
*	mantenedor de bancos
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de movimiento</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$datacuentas = $db->select("tb_cuenta_contable",
	"dc_cuenta_contable,dg_cuenta_contable,dg_codigo",
	"dc_empresa= {$empresa} AND dm_activo ='1'",
	array("order_by" => "dg_cuenta_contable"));
	$cuentas = array();
	foreach($datacuentas as $c){
		$cuentas[$c['dc_cuenta_contable']] = $c['dg_codigo']." ".$c['dg_cuenta_contable'];
	}

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de movimiento<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/contabilidad/proc/crear_tipo_movimiento.php","cr_tipo_movimiento","confirmValidar");
	$form->Section();
	$form->Text("Nombre","tipo_name",1);
	$form->Textarea("Descripción","tipo_desc",0);
	$form->EndSection();
	$form->Section();
	$form->Text("Código","tipo_codigo",1);
	$form->EndSection();
	$form->Group();
	$form->Header("Agregar Cuentas contables al tipo de movimiento");
	echo("<table align='center'><tr><td valign='bottom'>");
	$form->Multiselect('Cuentas contables en el tipo de movimiento','tipo_cuentas');
	echo("</td><td valign='middle'><br />
	<button type='button' id='add_cuentas'>&laquo;</button><br />
	<button type='button' id='del_cuentas'>&raquo;</button>
	</td><td valign='bottom'>");
	$form->Multiselect('Filtro: <input type="text" id="cuenta_filtro" size="35" />','lista_cuentas',$cuentas);
	echo("</td></tr></table>");
	$form->End("Crear","addbtn");
	
	echo("<select id='filtradas' style='display:none;'></select>");
	
	$list = $db->select(
		"tb_tipo_movimiento",
		"dc_tipo_movimiento, dg_codigo, dg_tipo_movimiento, dg_descripcion, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_tipo_movimiento")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Tipo de movimiento ya agregados</caption>
	<thead>
		<tr>
			<th width="15%">Código</th>
			<th width="50%">Nombre</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_tipo_movimiento']}'>
			<td>{$l['dg_codigo']}</td>
			<td title='{$l['dg_descripcion']}'>{$l['dg_tipo_movimiento']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/contabilidad/ed_tipo_movimiento.php?id={$l['dc_tipo_movimiento']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/contabilidad/del_tipo_movimiento.php?id={$l['dc_tipo_movimiento']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>
<script type="text/javascript">
$("#cuenta_filtro").keyup(function(){
	cri = $(this).val();
	$("#lista_cuentas option").each(function(){
		if($(this).text().toLowerCase().indexOf(cri) == -1){
			$(this).remove().appendTo("#filtradas");
		}
	});
	$("#filtradas option").each(function(){
		if($(this).text().toLowerCase().indexOf(cri) != -1){
			$(this).remove().appendTo("#lista_cuentas");
		}
	});
});

$("#add_cuentas").click(function(){
	$("#lista_cuentas option:selected").remove().appendTo("#tipo_cuentas");
});
$("#del_cuentas").click(function(){
	$("#tipo_cuentas option:selected").remove().appendTo("#lista_cuentas");
	$("#cuenta_filtro").trigger('keyup');
});

$("#cr_tipo_movimiento").submit(function(){ 
	$("#tipo_cuentas option").attr("selected",true);
});
</script>