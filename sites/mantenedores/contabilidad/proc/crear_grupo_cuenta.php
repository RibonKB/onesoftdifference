<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!is_numeric($_POST['grupo_desde']) || !is_numeric($_POST['grupo_hasta'])){
	$error_man->showWarning("Los rangos de cuentas deben ser numéricos");
	exit();
}

if(strlen($_POST['grupo_desde']) != $empresa_conf['dn_largo_codigo_cuenta'] ||
strlen($_POST['grupo_hasta']) != $empresa_conf['dn_largo_codigo_cuenta']){
	$error_man->showWarning("Los números en los rangos deben tener {$empresa_conf['dn_largo_codigo_cuenta']} dígitos");
	exit();
}

if($_POST['grupo_desde'] > $_POST['grupo_hasta']){
	$error_man->showAviso("Rango invertido, se asume <b>({$_POST['grupo_hasta']};{$_POST['grupo_desde']})</b>");
	$aux = $_POST['grupo_desde'];
	$_POST['grupo_desde'] = $_POST['grupo_hasta'];
	$_POST['grupo_hasta'] = $aux;
	unset($aux);
}

$r_actuales = $db->select("tb_grupo_cuenta_contable AS c
JOIN tb_rango_cuenta_contable AS r ON c.dc_grupo_cuenta = r.dc_grupo_cuenta","dg_grupo_cuenta",
"({$_POST['grupo_desde']} BETWEEN r.dc_desde AND r.dc_hasta) ||
 ({$_POST['grupo_hasta']} BETWEEN r.dc_desde AND r.dc_hasta) ||
 (r.dc_desde BETWEEN {$_POST['grupo_desde']} AND {$_POST['grupo_hasta']}) ||
 (r.dc_hasta BETWEEN {$_POST['grupo_desde']} AND {$_POST['grupo_hasta']})");

if(count($r_actuales)){
 	$inters = array();
	foreach($r_actuales as $v){
		$inters[] = $v['dg_grupo_cuenta'];
	}
	$error_man->showWarning("El rango intersecta con el de el(los) grupo(s) <b>".implode(',',$inters)."</b>");
	exit();
}

$new = $db->insert("tb_grupo_cuenta_contable",
array(
	"dg_grupo_cuenta" => ucwords($_POST['grupo_name']),
	"dg_descripcion" => $_POST['grupo_desc'],
	"dc_orden" => $_POST['grupo_orden'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_creacion" => "NOW()",
    "dm_linea_negocio" => isset($_POST['dm_linea_negocio'])?1:0
));

$db->insert("tb_rango_cuenta_contable",
array(
	"dc_grupo_cuenta" => $new,
	"dc_desde" => $_POST['grupo_desde'],
	"dc_hasta" => $_POST['grupo_hasta'],
	"dm_actual" => 1,
	"df_creacion" => 'NOW()',
	"dc_usuario_creacion" => $idUsuario
));

$error_man->showConfirm("Se ha creado el grupo de cuentas contables <strong>{$_POST['grupo_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");
echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['grupo_name']}</td>
	<td>{$_POST['grupo_desc']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/contabilidad/ed_grupo_cuenta.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/contabilidad/del_grupo_cuenta.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
		<a href='sites/mantenedores/contabilidad/cr_grupo_nuevo_rango.php?id={$new}' class='loadOnOverlay'>
			<img src='images/range.png' alt='' title='Gestor de rangos' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	$("a.loadOnOverlay").unbind('click');
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>