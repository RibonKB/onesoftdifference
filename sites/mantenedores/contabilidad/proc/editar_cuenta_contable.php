<?php
/**
*	Edicion de banco
**/
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = array(
	"dc_grupo_cuenta" => $_POST['ed_cuenta_grupo'],
	"dg_cuenta_contable" => trim(ucwords($_POST['ed_cuenta_desc'])),
	"dm_tipo_cuenta" => $_POST['ed_cuenta_tipo'],
	"dg_descripcion_larga" => $_POST['ed_cuenta_long_desc']
);

if(isset($_POST['ed_cuenta_valido'])){
	$data["dm_valido"] = 1;
}

//Se modifican los datos en la base de datos
$db->update("tb_cuenta_contable",$data,"dc_cuenta_contable = {$_POST['ed_cuenta_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	show_confirm('Se ha modificado la cuenta correctamente');
	loadpage("sites/mantenedores/clientes/src_medio_pago.php");
</script>