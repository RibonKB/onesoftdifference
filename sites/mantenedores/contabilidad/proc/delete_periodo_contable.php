<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->update("tb_periodo_contable",array("dm_activo" => '0'),"dc_periodo_contable = {$_POST['del_periodo_id']}");

$error_man->showConfirm("<div class='center'>Se ha eliminado el periodo contable correctamente<br /><a href='#' onclick='$(\"#genOverlay\").remove();'>Cerrar</a></div>");

?>
<script type="text/javascript">
	$("#item<?=$_POST['del_periodo_id'] ?>").remove();
</script>