<?php
/**
*	Edicion de banco
**/
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_banco",
array(
	"dg_banco" => trim(ucwords($_POST['ed_banco_name']))
),"dc_banco = {$_POST['ed_banco_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/contabilidad/src_banco.php");
</script>