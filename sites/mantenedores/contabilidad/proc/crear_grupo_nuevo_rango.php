<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!is_numeric($_POST['rango_desde']) || !is_numeric($_POST['rango_hasta'])){
	$error_man->showWarning("Los rangos de cuentas deben ser numéricos");
	exit();
}

if(strlen($_POST['rango_desde']) != $empresa_conf['dn_largo_codigo_cuenta'] ||
strlen($_POST['rango_hasta']) != $empresa_conf['dn_largo_codigo_cuenta']){
	$error_man->showWarning("Los números en los rangos deben tener {$empresa_conf['dn_largo_codigo_cuenta']} dígitos");
	exit();
}

if($_POST['rango_desde'] > $_POST['rango_hasta']){
	$error_man->showAviso("Rango invertido, se asume <b>({$_POST['rango_hasta']};{$_POST['rango_desde']})</b>");
	$aux = $_POST['rango_desde'];
	$_POST['rango_desde'] = $_POST['rango_hasta'];
	$_POST['rango_hasta'] = $aux;
	unset($aux);
}

if(count($db->select("(SELECT * FROM tb_grupo_cuenta_contable WHERE dc_empresa={$empresa} AND dm_activo = 1) AS c
JOIN tb_rango_cuenta_contable AS r ON c.dc_grupo_cuenta = r.dc_grupo_cuenta","1",
"({$_POST['rango_desde']} BETWEEN r.dc_desde AND r.dc_hasta) ||
 ({$_POST['rango_hasta']} BETWEEN r.dc_desde AND r.dc_hasta) ||
 (r.dc_desde BETWEEN {$_POST['rango_desde']} AND {$_POST['rango_hasta']}) ||
 (r.dc_hasta BETWEEN {$_POST['rango_desde']} AND {$_POST['rango_hasta']})"))){
 	
	$error_man->showWarning("El rango intersecta con el de otro grupo ingresado antes");
	exit();
}

$data = array(
	"dc_grupo_cuenta" => $_POST['rango_grupo'],
	"dc_desde" => $_POST['rango_desde'],
	"dc_hasta" => $_POST['rango_hasta'],
	"df_creacion" => 'NOW()',
	"dc_usuario_creacion" => $idUsuario
);

if(isset($_POST['rango_actual'])){
	$db->query('START TRANSACTION;');
	$db->update("tb_rango_cuenta_contable",array('dm_actual' => 0),"dc_grupo_cuenta = {$_POST['rango_grupo']} AND dm_actual = '1'");
	$data['dm_actual'] = 1;
}

$db->insert("tb_rango_cuenta_contable",$data);

if(isset($_POST['rango_actual'])){
	$db->query('COMMIT;');
}

$error_man->showConfirm("Se ha creado el rango de cuentas contables <strong>[{$_POST['rango_desde']} ; {$_POST['rango_hasta']}]</strong> correctamente");

?>
<script type="text/javascript">
	loadOverlay("sites/mantenedores/contabilidad/cr_grupo_nuevo_rango.php?id=<?=$_POST['rango_grupo'] ?>");
</script>