<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->update("tb_cuenta_contable",array("dm_activo" => '0'),"dc_cuenta_contable IN ({$_POST['mdel_cuenta_id']}) AND dc_empresa={$empresa}");

$ctas = "#item".implode(',#item',explode(',',$_POST['mdel_cuenta_id']));

$error_man->showConfirm("<div class='center'>Se han eliminado <b>".count(explode(',',$_POST['mdel_cuenta_id']))."</b> cuentas contables correctamente<br /><a href='#' onclick='$(\"#genOverlay\").remove();'>Cerrar</a></div>");

?>
<script type="text/javascript">
	$("<?=$ctas ?>").remove();
</script>