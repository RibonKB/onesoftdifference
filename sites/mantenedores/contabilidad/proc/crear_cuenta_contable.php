<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = array(
	"dg_cuenta_contable" => ucwords($_POST['cuenta_desc']),
	"dm_tipo_cuenta" => $_POST['cuenta_tipo'],
	"dc_grupo_cuenta" => $_POST['cuenta_grupo'],
	"dg_descripcion_larga" => $_POST['cuenta_long_desc'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_creacion" => "NOW()"
);

if(isset($_POST['cuenta_valido'])){
	$data['dm_valido'] = 1;
}

$db->query('START TRANSACTION;');
$code = $db->select(
	"(SELECT * FROM tb_grupo_cuenta_contable WHERE dc_grupo_cuenta = {$_POST['cuenta_grupo']} AND dc_empresa={$empresa}) AS g
	JOIN (SELECT * FROM tb_rango_cuenta_contable WHERE dc_grupo_cuenta = {$_POST['cuenta_grupo']} AND dm_actual = '1') AS r ON g.dc_grupo_cuenta = r.dc_grupo_cuenta
	LEFT JOIN tb_cuenta_contable AS c ON (g.dc_grupo_cuenta = c.dc_grupo_cuenta AND c.dg_codigo BETWEEN r.dc_desde AND r.dc_hasta)",
	'r.dc_desde, r.dc_hasta,MAX(c.dg_codigo)+1 as dc_ultimo');
if(!count($code)){
	$error_man->showWarning("El grupo está bloqueado para la creación directa de cuentas debido a que no tiene rangos activos, comuniquese con el encargado");
	exit();
}
$code = $code[0];

if($code['dc_ultimo'] == NULL){
	$data['dg_codigo'] = $code['dc_desde'];
}else if($code['dc_ultimo'] > $code['dc_hasta']){
	$error_man->showWarning('No se puede crear la nueva cuenta por no haber cupo de código');
	$db->query('ROLLBACK;');
	exit();
}else{
	$data['dg_codigo'] = $code['dc_ultimo'];
}

$new = $db->insert("tb_cuenta_contable",$data);

$db->query('COMMIT;');

$error_man->showConfirm("Se ha creado la cuenta contable <strong>{$_POST['cuenta_desc']}</strong> correctamente");

$date_now = date("d/m/Y H:i");
echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$data['dg_codigo']}</td>
	<td title='{$_POST['cuenta_long_desc']}'>{$_POST['cuenta_desc']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/contabilidad/ed_cuenta_contable.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/contabilidad/del_cuenta_contable.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").prependTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>