<?php
/**
*	Edicion de banco
**/
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(count($db->select("tb_tipo_movimiento","1","dg_codigo='{$_POST['ed_tipo_codigo']}' AND dc_empresa={$empresa} AND dc_tipo_movimiento <> {$_POST['ed_tipo_id']}"))){
	$error_man->showWarning("Ya se ha ingresado un tipo de movimiento con ese código");
	exit();
}

//Se modifican los datos en la base de datos
$db->update("tb_tipo_movimiento",
array(
	"dg_tipo_movimiento" => trim(ucwords($_POST['ed_tipo_name'])),
	"dg_codigo" => $_POST['ed_tipo_codigo'],
	"dg_descripcion" => $_POST['ed_tipo_desc']
),"dc_tipo_movimiento = {$_POST['ed_tipo_id']} AND dc_empresa={$empresa}");

$db->query("DELETE FROM tb_cuentas_tipo_movimiento WHERE dc_tipo_movimiento = {$_POST['ed_tipo_id']}");

$_POST['ed_tipo_cuentas'] = array_unique($_POST['ed_tipo_cuentas']);
foreach($_POST['ed_tipo_cuentas'] as $c){
	$db->insert("tb_cuentas_tipo_movimiento",
	array(
	"dc_tipo_movimiento" => $_POST['ed_tipo_id'],
	"dc_cuenta_contable" => $c
	));
}

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/contabilidad/src_tipo_movimiento.php");
</script>