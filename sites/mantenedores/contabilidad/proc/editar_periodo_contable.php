<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(count($db->select('tb_periodo_contable','1',"dc_anho = '{$_POST['ed_per_anho']}' AND dc_mes_contable = {$_POST['ed_per_mes']} AND dm_activo='1' AND dc_periodo_contable <> {$_POST['ed_per_id']}"))){
	$error_man->showWarning("El periodo <b>{$_POST['ed_per_anho']}</b> ya existe en el mes contable correspondiente, intentelo denuevo");
	exit();
}

if($_POST['ed_per_filtro'] == '1' && $_POST['ed_per_filtro_list'] != ''){
	$per_filtro = $_POST['ed_per_filtro_tipo'];
	$per_filtro_list = $_POST['ed_per_filtro_list'];
}else{
	$per_filtro = '0';
	$per_filtro_list = '';
}

$curr = $db->update("tb_periodo_contable",
array(
	"dc_anho" => $_POST['ed_per_anho'],
	"dm_estado" => $_POST['ed_per_estado'],
	"dm_filtro" => $per_filtro,
	"dg_permisos" => $per_filtro_list
),"dc_periodo_contable={$_POST['ed_per_id']}");

?>
<script type="text/javascript">
	alert('Se ha actualizado el periodo contable correctamente');
	loadpage('sites/mantenedores/contabilidad/src_periodo_contable.php');
</script>