<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = array();
$flag = true;

if($_POST['med_cuenta_grupo'] != 0){
	$data['dc_grupo_cuenta'] = $_POST['med_cuenta_grupo'];
	$flag = false;
}
if($_POST['med_cuenta_tipo'] != 0){
	$data['dm_tipo_cuenta'] = $_POST['med_cuenta_tipo'];
	$flag = false;
}
if($_POST['med_cuenta_val'] != 2){
	$data['dm_valido'] = $_POST['med_cuenta_val'];
	$flag = false;
}

if($flag){
	$error_man->showAviso('No se genero ningún cambio en las cuentas');
	exit();
}

//Se modifican los datos en la base de datos
$db->update("tb_cuenta_contable",$data,"dc_cuenta_contable IN ({$_POST['med_cuenta_id']}) AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	alert("Se modificaron <?=count(explode(',',$_POST['med_cuenta_id'])) ?> registros");
	loadpage("sites/mantenedores/contabilidad/src_cuenta_contable.php");
</script>