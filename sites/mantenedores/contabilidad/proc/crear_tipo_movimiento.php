<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(count($db->select("tb_tipo_movimiento","1","dg_codigo='{$_POST['tipo_codigo']}' AND dc_empresa={$empresa}"))){
	$error_man->showWarning("Ya se ha ingresado un tipo de movimiento con ese código");
	exit();
}

$new = $db->insert("tb_tipo_movimiento",
array(
	"dg_tipo_movimiento" => ucwords($_POST['tipo_name']),
	"dg_descripcion" => $_POST['tipo_desc'],
	"dg_codigo" => $_POST['tipo_codigo'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_creacion" => "NOW()"
));

$error_man->showConfirm("Se ha creado el tipo de movimiento <strong>{$_POST['tipo_name']}</strong> correctamente");

$_POST['tipo_cuentas'] = array_unique($_POST['tipo_cuentas']);
foreach($_POST['tipo_cuentas'] as $c){
	$db->insert("tb_cuentas_tipo_movimiento",
	array(
	"dc_tipo_movimiento" => $new,
	"dc_cuenta_contable" => $c
	));
}

$error_man->showConfirm("Se le asignaron <strong>".count($_POST['tipo_cuentas'])."</strong> cuentas contables al tipo de movimiento");

$date_now = date("d/m/Y H:i");
echo("
<table style='display:none;'>
<tr id='item{$new}'>
	<td>{$_POST['tipo_codigo']}</td>
	<td>{$_POST['tipo_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/mantenedores/contabilidad/ed_tipo_movimiento.php?id={$new}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/mantenedores/contabilidad/del_tipo_movimiento.php?id={$new}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>