<?php
/**
*	Edicion de banco
**/
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_grupo_cuenta_contable",
array(
	"dg_grupo_cuenta" => trim(ucwords($_POST['ed_grupo_name'])),
	"dg_descripcion" => trim(ucwords($_POST['ed_grupo_desc'])),
	"dc_orden" => $_POST['ed_grupo_orden'],
    "dm_linea_negocio" => isset($_POST['dm_linea_negocio_ed'])?1:0
),"dc_grupo_cuenta = {$_POST['ed_grupo_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	pymerp.loadPage("sites/mantenedores/contabilidad/src_grupo_cuenta.php");
</script>