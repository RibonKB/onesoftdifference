<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("tb_mes_contable","dc_mes_contable","dc_mes = {$_POST['periodo_mes']} AND dc_empresa={$empresa}");
if(!count($datos)){
	$new = $db->insert("tb_mes_contable",
	array(
		"dc_mes" => trim($_POST['periodo_mes']),
		"dg_descripcion" => $_POST['periodo_desc'],
		"dc_empresa" => $empresa,
		"dc_usuario_creacion" => $idUsuario,
		"df_creacion" => "NOW()"
	));
	
	$error_man->showConfirm("Se ha creado el mes contable <strong>{$_POST['periodo_mes']}</strong> correctamente");
}else{
	$new = $datos[0]['dc_mes_contable'];
	$error_man->showAviso("El mes contable <strong>{$_POST['periodo_mes']}</strong> ya existe, se agregaran los nuevos periodos a este");
}

$date_now = date("d/m/Y H:i");
$res = '';

foreach($_POST['per_anho'] as $i => $v){

if(count($db->select('tb_periodo_contable','1',"dc_anho = '{$v}' AND dc_mes_contable = {$new} AND dm_activo='1'"))){
	$error_man->showAviso("El periodo <b>{$v}</b> ya ha sido agregado al mes contable, su inserción fue omitida.");
	unset($_POST['per_anho'][$i]);
}else{
	if($_POST['per_filtro'][$i] == '1' && $_POST['per_filtro_list'][$i] != ''){
		$per_filtro = $_POST['per_filtro_tipo'][$i];
		$per_filtro_list = $_POST['per_filtro_list'][$i];
	}else{
		$per_filtro = '0';
		$per_filtro_list = '';
	}
	
	$curr = $db->insert("tb_periodo_contable",
	array(
		"dc_anho" => $v,
		"dm_estado" => $_POST['per_estado'][$i],
		"dm_filtro" => $per_filtro,
		"dg_permisos" => $per_filtro_list,
		"df_creacion" => "NOW()",
		"dc_mes_contable" => $new
	));
	
	$curr_st = $_POST['per_estado'][$i]=='1'?'<div class="confirm">Abierto</div>':'<div class="warning">Cerrado</div>';
	
	$res .= "
	<tr id='item{$curr}'>
		<td align='center'>{$_POST['periodo_mes']}</td>
		<td>{$v}</td>
		<td>{$curr_st}</td>
		<td>{$date_now}</td>
		<td>
			<a href='sites/mantenedores/contabilidad/ed_periodo_contable.php?id={$curr}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
			</a>
			<a href='sites/mantenedores/contabilidad/del_periodo_contable.php?id={$curr}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
			</a>
		</td>
	</tr>";
}
}

if(count($_POST['per_anho'])){
	echo("<table style='display:none;' id='nuevas'>{$res}</table>");
	$ans = implode(', ',$_POST['per_anho']);
	$error_man->ShowConfirm("Se han agregado los periodos <b>{$ans}</b> al mes contable");
}

?>
<script type="text/javascript">
	$("#nuevas tr").prependTo("#list");
	$("#nuevas").remove();
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>