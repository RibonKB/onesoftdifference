<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_cuenta_contable",
"dm_tipo_cuenta,dm_valido,dg_cuenta_contable,dg_descripcion_larga,dc_grupo_cuenta",
"dc_cuenta_contable = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la cuenta contable especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición cuentas contables</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la cuenta contable");
	$form->Start("sites/mantenedores/contabilidad/proc/editar_cuenta_contable.php","ed_cuenta_contable");
	$form->Section();
	$form->Text("Descripción (Nombre de la cuenta)","ed_cuenta_desc",1,255,$datos['dg_cuenta_contable']);
	$form->Textarea("Descripción larga","ed_cuenta_long_desc",0,$datos['dg_descripcion_larga']);
	$form->EndSection();
	$form->Section();
	$form->Select('Tipo de cuenta','ed_cuenta_tipo',array(1 =>'Activo',2 =>'Pasivo',3 =>'Pérdida',4=>'Ganancia'),1,$datos['dm_tipo_cuenta']);
	$form->Listado('Grupo de cuenta','ed_cuenta_grupo','tb_grupo_cuenta_contable',array('dc_grupo_cuenta','dg_grupo_cuenta'),1,$datos['dc_grupo_cuenta']);
	$form->Combobox('','ed_cuenta_valido',array(1=>'Activar'),array($datos['dm_valido']));
	$form->EndSection();
	$form->Hidden("ed_cuenta_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>