<?php
/**
*	mantenedor de tipos de documento
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de documento</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de documento<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/contabilidad/proc/crear_tipo_documento.php","cr_tipo_documento");
	$form->Section();
	$form->Text("Código","tipo_codigo",1);
	$form->Textarea("Descripción","tipo_desc",0);
	$form->EndSection();
	$form->Section();
	$form->Text("Nombre","tipo_name",1);
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_tipo_documento",
		"dc_tipo_documento, dg_codigo, dg_tipo_documento, dg_descripcion, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_tipo_documento")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Tipos de documento ya agregados</caption>
	<thead>
		<tr>
			<th width="15%">Código</th>
			<th width="50%">Tipo de documento</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_tipo_documento']}'>
			<td>{$l['dg_codigo']}</td>
			<td title='{$l['dg_descripcion']}'>{$l['dg_tipo_documento']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/contabilidad/ed_tipo_documento.php?id={$l['dc_tipo_documento']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/contabilidad/del_tipo_documento.php?id={$l['dc_tipo_documento']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>