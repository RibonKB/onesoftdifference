<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_documento",
"dg_codigo,dg_tipo_documento,dg_descripcion",
"dc_tipo_documento = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de documento especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipos de documento</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el tipo de documento");
	$form->Start("sites/mantenedores/contabilidad/proc/editar_tipo_documento.php","ed_tipo_documento");
	$form->Section();
	$form->Text("Código","ed_tipo_codigo",1,255,$datos['dg_codigo']);
	$form->Textarea("Descripción","ed_tipo_desc",0,$datos['dg_descripcion']);
	$form->EndSection();
	$form->Section();
	$form->Text("nombre","ed_tipo_name",1,255,$datos['dg_tipo_documento']);
	$form->EndSection();
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>