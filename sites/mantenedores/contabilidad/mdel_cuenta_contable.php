<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div class="secc_bar">Eliminar cuentas contables</div>
<div class="panes">
	<form action="sites/mantenedores/contabilidad/proc/delete_cuenta_contable_masivo.php" class="confirmValidar" id="mdel_cuenta">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar las cuentas contables seleccionadas?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="mdel_cuenta_id" value="<?=implode(',',$_POST['masive_cuentas']) ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="mdel_cuenta_res"></div>
</div>