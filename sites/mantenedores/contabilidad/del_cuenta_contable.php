<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_cuenta_contable",
"dg_cuenta_contable",
"dc_cuenta_contable= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la cuenta contable especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar cuentas contables</div>
<div class="panes">
	<form action="sites/mantenedores/contabilidad/proc/delete_cuenta_contable.php" class="confirmValidar" id="del_grupo_cuenta">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la cuenta contable '<?=$datos[0]['dg_cuenta_contable'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_cuenta_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_grupo_cuenta_res"></div>
</div>