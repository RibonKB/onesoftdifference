<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Folios para el libro mayor</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Configuración de los folios para el libro mayor");
	$form->Start("sites/mantenedores/contabilidad/informes/proc/editar_libro_mayor_folio.php","ed_folio");
	$form->Textarea("Encabezado","folio_head",1,$empresa_conf['dg_encabezado_libro_mayor'],80);
	$form->Text('Nº de folio actual','folio_num',1,255,$empresa_conf['dc_folio_actual']);
	$form->End("Editar","editbtn");
	
	$form->Start('sites/mantenedores/contabilidad/informes/proc/imprimir_folios.php','imp_folio');
	$form->Header("Impresion de folios para libro mayor");
	$form->Text('Cantidad de páginas a imprimir','folio_cant',1,4);
	$form->End("Generar páginas con encabezado");
	
	$list = $db->select(
		"tb_log_impresion_folio",
		"dc_log, dg_log, dc_desde, dc_hasta, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa}",
		array("order_by" => "df_creacion DESC")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Bancos ya agregados</caption>
	<thead>
		<tr>
			<th width="50%">Archivo</th>
			<th width="20%">Rango de folios impresos</th>
			<th width="20%">Fecha de creación</th>
			<th width="10%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_log']}'>
			<td><img src='images/pdf.png' alt='' /> {$l['dg_log']}.pdf</td>
			<td>[ {$l['dc_desde']} - {$l['dc_hasta']} ]</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/contabilidad/informes/pdf/{$l['dg_log']}.pdf'>Descargar</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table></div></div>");
?>
<script type="text/javascript">
	$("#list a").click(function(e){
		e.preventDefault();
		window.open($(this).attr('href'),'pdf_open','width=800,height=600');
	});
</script>