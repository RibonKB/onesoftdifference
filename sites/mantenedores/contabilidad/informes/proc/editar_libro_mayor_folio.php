<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = array(
	"dg_encabezado_libro_mayor" => $_POST['folio_head']
);

if(isset($_POST['folio_num'])){
	if(!is_numeric($_POST['folio_num'])){
		$error_man->showWarning('El formato del número de folio es incorrecto');
		exit();
	}
	$data['dc_folio_actual'] = $_POST['folio_num'];
}

$db->update("tb_empresa_configuracion",$data,"dc_empresa = {$empresa}");

$error_man->showConfirm('Se han actualizado los datos de folio correctamente');
?>