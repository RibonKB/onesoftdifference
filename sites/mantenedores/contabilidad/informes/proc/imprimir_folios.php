<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once("../../../../../inc/fpdf.php");

class PDF extends FPDF{
	function Header(){
		global $cabecera,$page;
		
		$this->SetFont('Arial','',7);
		$this->Cell(0,4,$page++,0,1,'R');
		$this->MultiCell(0,3,$cabecera);
	}
}

$page = $empresa_conf['dc_folio_actual'];
$cabecera = utf8_decode($empresa_conf['dg_encabezado_libro_mayor']);
$pdf = new PDF('P','mm','Letter');
for($i=0;$i<$_POST['folio_cant'];$i++){
	$pdf->AddPage();
}

$pdf_id = uniqid($empresa.'.'.$idUsuario.'.');

$db->insert('tb_log_impresion_folio',
array(
	"dg_log" => $pdf_id,
	"dc_desde" => $empresa_conf['dc_folio_actual'],
	"dc_hasta" => $page-1,
	"df_creacion" => 'NOW()',
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario
));
$db->update('tb_empresa_configuracion',array("dc_folio_actual" => $page),"dc_empresa = {$empresa}");

$pdf->Output("../pdf/{$pdf_id}.pdf");
$error_man->showConfirm("Se ha generado el PDF con los folios correctamente<br /><a href='#' id='open_pdf'>haga click aquí para verlo</a>");
?>
<script type="text/javascript">
	$('#open_pdf').click(function(){
		window.open('sites/mantenedores/contabilidad/informes/pdf/<?=$pdf_id ?>.pdf','pdf_open','width=800,height=600');
	});
	$('#open_pdf').trigger('click');
</script>