<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select('tb_periodo_contable','dc_anho,dm_filtro,dg_permisos,dm_estado',"dc_periodo_contable={$_POST['id']}");

if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el periodo contable especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición periodos contables</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el periodo contable");
	$form->Start('sites/mantenedores/contabilidad/proc/editar_periodo_contable.php','ed_periodo_contable');
	$form->Section();
	$form->Text('Periodo contable','ed_per_anho',1,4,$datos['dc_anho']);
	$form->Select('Estado','ed_per_estado',array('Cerrado','Abierto'),0,$datos['dm_estado']);
	$form->EndSection();
	$form->Section();
	$filtrar = 0;
	if($datos['dm_filtro'])
		$filtrar = 1;
	echo("<label>Filtro<br />
	<select name='ed_per_filtro' class='inputtext' id='ed_per_filtro' style='width:280px;'>
		<option value='0'>No filtrar</option>
		<option value='1'>Filtrar</option>
	</select>
	</select></label><br />
	<div class='hidden' style='background:#F6F6F6;padding:13px;margin-top:5px;padding-bottom:3px;'>
	<select name='ed_per_filtro_tipo' class='inputtext' id='ed_per_filtro_tipo' style='width:140px;'>");
	switch($datos['dm_filtro']){
	case '0':
	case '1':
		echo("<option value='1' selected='selected'>Filtrar por grupo</option><option value='2'>Filtrar por usuarios</option>
		</select>
		<input type='text' class='ed_per_filtro_grupo' />
		<input type='text' class='ed_per_filtro_usuario' style='display:none;' />");
		break;
	case '2':
		echo("<option value='1'>Filtrar por grupo</option><option value='2' selected='selected'>Filtrar por usuarios</option>
		</select>
		<input type='text' class='ed_per_filtro_grupo' style='display:none;' />
		<input type='text' class='ed_per_filtro_usuario' />");
		break;
	}
	echo("<input type='hidden' name='ed_per_filtro_list' class='ed_per_filtro_list' value='{$datos['dg_permisos']}' /><br />
	<div id='ed_filtrados'>");
	switch($datos['dm_filtro']){
	case '1':
		$datos['dg_permisos'] = substr($datos['dg_permisos'],0,-1);
		$permisos = $db->select('tb_grupo','dg_grupo',"dc_grupo IN ({$datos['dg_permisos']}) AND dm_activo='1'");
		foreach($permisos as $p){
			echo("<div style='border:1px solid #CCC;float:left;padding:2px;margin:1px;background:#EEE;'>
			<img src='images/delbtn.png' alt='{$p['dc_grupo']}' style='width:10px;' class='ed_rev_per_elem' />
			{$p['dg_grupo']}
			</div>");
		}
	break;
	case '2':
		$datos['dg_permisos'] = substr($datos['dg_permisos'],0,-1);
		$permisos = $db->select('tb_usuario','dc_usuario,dg_usuario',"dc_usuario IN ({$datos['dg_permisos']}) AND dm_activo='1'");
		foreach($permisos as $p){
			echo("<div style='border:1px solid #CCC;float:left;padding:2px;margin:1px;background:#EEE;'>
			<img src='images/delbtn.png' alt='{$p['dc_usuario']}' style='width:10px;' class='ed_rev_per_elem' />
			{$p['dg_usuario']}
			</div>");
		}
	break;
	}
	echo("<br class='clear' /></div></div>");
	$form->EndSection();
	$form->Hidden('ed_per_id',$_POST['id']);
	$form->Hidden('ed_per_mes',$_POST['m']);
	$form->End('Editar','editbtn');
?>
</div>
<script type="text/javascript">
$('.ed_per_filtro_grupo').autocomplete('sites/proc/autocompleter/grupo.php',
{width:300}).result(function(e,row){
	hid = $(this).next().next();
	hid.val(hid.val()+row[1]+",");
	$(this).val('');
	$(this).parent().find('#ed_filtrados')
	.prepend('<div style="border:1px solid #CCC;float:left;padding:2px;margin:1px;background:#EEE;"><img src="images/delbtn.png" style="width:10px;" alt="'+row[1]+'" class="ed_rev_per_elem" /> '+row[0]+'</div>');

	$(".ed_rev_per_elem").click(function(){
	v = $(this).attr('alt');
	hd = $(this).parent().parent().parent().find(".ed_per_filtro_list");
	hd.val(hd.val().replace(v+',',''));
	$(this).parent().remove();
	});
});

$('.ed_per_filtro_usuario').autocomplete('sites/proc/autocompleter/usuario.php',
{width:300,
formatItem: function(row){
	return "<b>"+row[0]+"</b> ( "+row[1]+" ) "+row[2];
}
}).result(function(e,row){
	hid = $(this).next();
	hid.val(hid.val()+row[3]+",");
	$(this).val('');
	$(this).parent().find('#ed_filtrados')
	.prepend('<div style="border:1px solid #CCC;float:left;padding:2px;margin:1px;background:#EEE;"><img src="images/delbtn.png" style="width:10px;" alt="'+row[3]+'" class="rev_per_elem" /> '+row[0]+'</div>');

	$(".ed_rev_per_elem").click(function(){
	v = $(this).attr('alt');
	hd = $(this).parent().parent().parent().find(".ed_per_filtro_list");
	hd.val(hd.val().replace(v+',',''));
	$(this).parent().remove();
	});
});

$("#ed_per_filtro").change(function(){
	if($(this).val() == '1'){
		$('#ed_filtrados').parent().show();
	}else{
		$('#ed_filtrados').parent().hide();
	}
});
$("#ed_per_filtro").trigger('change');

$("#ed_per_filtro_tipo").change(function(){
	if($(this).val() == '1'){
		$(this).next().show();
		$(this).next().next().hide();
	}else{
		$(this).next().hide();
		$(this).next().next().show();
	}
	$("#ed_filtrados div").remove();
	$('.ed_per_filtro_list').val('');
});

$('#ed_per_anho').change(function(){
v = parseInt($(this).val());
if(v && v > 1900)
	$(this).val(v);
else
	$(this).val('');
});
</script>