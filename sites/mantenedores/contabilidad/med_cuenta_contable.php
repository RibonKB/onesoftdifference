<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

?>
<div class="secc_bar">Edición cuentas contables</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la cuenta contable");
	$form->Start("sites/mantenedores/contabilidad/proc/editar_cuenta_contable_masivo.php","ed_cuentas_contable");
	$form->Section();
	$form->Listado('Grupo de cuenta','med_cuenta_grupo','tb_grupo_cuenta_contable',array('dc_grupo_cuenta','dg_grupo_cuenta'));
	if($empresa_conf['dm_workflow'] == '1'){
	$form->Radiobox('','med_cuenta_val',array('Bloquear','Validar','Intacto'),2,'');
	}else{
	$form->Hidden('med_cuenta_val',1); }
	$form->EndSection();
	$form->Section();
	$form->Select('Tipo de cuenta','med_cuenta_tipo',array(1 =>'Activo',2 =>'Pasivo',3 =>'Pérdida',4=>'Ganancia'));
	$form->EndSection();
	$form->Hidden("med_cuenta_id",implode(',',$_POST['masive_cuentas']));
	$form->End("Editar","editbtn");
?>
</div>