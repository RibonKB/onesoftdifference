<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Cuentas contables</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("<b>Complete los datos requeridos para crear un nuevo grupo de cuentas contables</b><br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/contabilidad/proc/crear_grupo_cuenta.php","cr_grupo_cuenta");
	$form->Section();
	$form->Text("Nombre","grupo_name",1);
	$form->Textarea("Descripción","grupo_desc");
	$form->Text("Orden","grupo_orden",1,4,0);
	$form->EndSection();
	$form->Section();
	echo("<fieldset><legend>Rango de cuentas</legend>");
	$form->Header("La cantidad de dígitos debe ser <b>{$empresa_conf['dn_largo_codigo_cuenta']}</b>");
	$form->Text("Desde","grupo_desde",1);
	$form->Text("Hasta","grupo_hasta",1);
	echo("</fieldset>");
    $form->Combobox('', 'dm_linea_negocio', array(
        'Incluir cuentas contables del grupo en las lineas de negocio'
    ));
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_grupo_cuenta_contable AS c
		LEFT JOIN tb_rango_cuenta_contable AS r ON (c.dc_grupo_cuenta = r.dc_grupo_cuenta AND dm_actual = '1')",
		"c.dc_grupo_cuenta, c.dg_grupo_cuenta, r.dc_desde, r.dc_hasta, DATE_FORMAT(c.df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "c.dg_grupo_cuenta")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Grupos de cuentas contables ya agregados</caption>
	<thead>
		<tr>
			<th width="35%">Nombre</th>
			<th width="30%">Rango actual de cuenta</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_grupo_cuenta']}'>
			<td>{$l['dg_grupo_cuenta']}</td>
			<td>{$l['dc_desde']} - {$l['dc_hasta']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/contabilidad/ed_grupo_cuenta.php?id={$l['dc_grupo_cuenta']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' class='left' />
				</a>
				<a href='sites/mantenedores/contabilidad/del_grupo_cuenta.php?id={$l['dc_grupo_cuenta']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' class='left' />
				</a>
				<a href='sites/mantenedores/contabilidad/cr_grupo_nuevo_rango.php?id={$l['dc_grupo_cuenta']}' class='loadOnOverlay'>
				<img src='images/range.png' alt='' title='Gestión de rangos' class='left' />
				</a>
			</td>
		</tr>");
	}
	
	echo("
	</tbody></table>");
	
?>
</div>
</div>
<script type="text/javascript">
$("#grupo_desde").change(function(){
	v = $(this).val();
	for(i=v.length;i<<?=$empresa_conf['dn_largo_codigo_cuenta'] ?>;i++){
		v += '0';
	}
	$(this).val(v);
});
$("#grupo_hasta").change(function(){
	v = $(this).val();
	for(i=v.length;i<<?=$empresa_conf['dn_largo_codigo_cuenta'] ?>;i++){
		v += '9';
	}
	$(this).val(v);
});
</script>