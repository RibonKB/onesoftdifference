<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_grupo_cuenta_contable",
"dg_grupo_cuenta",
"dc_grupo_cuenta= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el grupo de cuentas contables especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar grupo cuentas contables</div>
<div class="panes">
	<form action="sites/mantenedores/contabilidad/proc/delete_grupo_cuenta.php" class="confirmValidar" id="del_grupo_cuenta">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el grupo de cuentas '<?=$datos[0]['dg_grupo_cuenta'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_grupo_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_grupo_cuenta_res"></div>
</div>