<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_banco",
"dg_banco",
"dc_banco = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el banco especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición bancos</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el banco");
	$form->Start("sites/mantenedores/contabilidad/proc/editar_banco.php","ed_banco");
	$form->Text("Nombre","ed_banco_name",1,255,$datos['dg_banco']);
	$form->Hidden("ed_banco_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>