<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_grupo_cuenta_contable",
"dg_grupo_cuenta,dg_descripcion,dc_orden",
"dc_grupo_cuenta = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el grupo de cuentas contables especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición grupo de cuentas contables</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la cuenta contable");
	$form->Start("sites/mantenedores/contabilidad/proc/editar_grupo_cuenta.php","ed_grupo_cuenta");
	$form->Section();
	$form->Text("Nombre","ed_grupo_name",1,255,$datos['dg_grupo_cuenta']);
	$form->Text("Orden","ed_grupo_orden",1,255,$datos['dc_orden']);
	$form->EndSection();
	$form->Section();
	$form->Textarea("Descripción","ed_grupo_desc",0,$datos['dg_descripcion']);
        $form->Combobox('', 'dm_linea_negocio', array(
        'Incluir cuentas contables del grupo en las lineas de negocio'
    ));
	$form->EndSection();
	$form->Hidden("ed_grupo_id",$_POST['id']);
        
	$form->End("Editar","editbtn");
?>
</div>