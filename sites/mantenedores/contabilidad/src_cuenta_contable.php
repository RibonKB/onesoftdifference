<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Cuentas contables</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear una nueva cuenta contable<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/contabilidad/proc/crear_cuenta_contable.php","cr_cuenta_contable");
	$form->Section();
	$form->Listado('Grupo de cuenta','cuenta_grupo','tb_grupo_cuenta_contable',array('dc_grupo_cuenta','dg_grupo_cuenta'),1);
	$form->Select('Tipo de cuenta','cuenta_tipo',array(1 =>'Activo',2 =>'Pasivo',3 =>'Pérdida',4=>'Ganancia'),1);
	if($empresa_conf['dm_workflow'] == '1'){
	$form->Combobox('','cuenta_valido',array('Validar'),array(0));
	}else{
	$form->Hidden('cuenta_valido',1); }
	$form->EndSection();
	$form->Section();
	$form->Text("Descripción (Nombre de la cuenta)","cuenta_desc",1);
	$form->Textarea("Descripción larga","cuenta_long_desc");
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_cuenta_contable",
		"dc_cuenta_contable, dc_grupo_cuenta, dg_codigo, dg_cuenta_contable, dg_descripcion_larga, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_codigo"));
	
	echo('
	<form action="#" id="masive_form">
	<button type="button" id="masive_edit" class="editbtn">Edición masiva</button>
	<button type="button" id="masive_del" class="delbtn">Eliminar masiva</button>
	<table class="tab sortable" width="100%">
	<caption>Cuentas contables ya agregadas</caption>
	<thead>
		<tr>
			<th width="20"></th>
			<th width="100">Código</th>
			<th width="360">Descripción</th>
			<th width="100">Fecha creación</th>
			<th width="70">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_cuenta_contable']}' class='grupo{$l['dc_grupo_cuenta']}'>
			<td><input type='checkbox' name='masive_cuentas[]' value='{$l['dc_cuenta_contable']}'></td>
			<td>{$l['dg_codigo']}</td>
			<td title='{$l['dg_descripcion_larga']}'>{$l['dg_cuenta_contable']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/contabilidad/ed_cuenta_contable.php?id={$l['dc_cuenta_contable']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' class='left' />
				</a>
				<a href='sites/mantenedores/contabilidad/del_cuenta_contable.php?id={$l['dc_cuenta_contable']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' class='left' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table></form>");
	
?>
</div>
</div>
<script type="text/javascript">
	$("#masive_edit").click(function(){
		ser = $('#masive_form').serialize();
		if(ser == ''){
		alert('Debe marcar al menos un campo para editar');
		}else{
			loadOverlay('sites/mantenedores/contabilidad/med_cuenta_contable.php?'+ser);
		}
	});
	$("#masive_del").click(function(){
		ser = $('#masive_form').serialize();
		if(ser == ''){
		alert('Debe marcar al menos un campo para eliminar');
		}else{
			loadOverlay('sites/mantenedores/contabilidad/mdel_cuenta_contable.php?'+ser);
		}
	});
	$('.sortable').tablesorter();
	$("#cuenta_desc").attr("disabled",1);
	$('#cuenta_grupo').change(function(){
		val = $(this).val();
		if(val != '0'){
			$("#cuenta_desc").attr("disabled",0);
			$('#list tr').hide();
			$('#list .grupo'+val).show();
		}else{
			$("#cuenta_desc").attr("disabled",1).val('');
			$('#list tr').show();
		}
	});
	$('#masive_form table').tableAdjust(6);
</script>