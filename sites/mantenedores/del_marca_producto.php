<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_marca",
"dg_marca",
"dc_marca= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la marca especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar marcas</div>
<div class="panes">
	<form action="sites/mantenedores/proc/delete_marca_producto.php" class="confirmValidar" id="del_marca">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la marca '<?=$datos[0]['dg_marca'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_marca_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_marca_res"></div>
</div>