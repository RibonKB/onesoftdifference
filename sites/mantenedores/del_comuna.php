<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_comuna",
"dg_comuna",
"dc_comuna= {$_POST['id']}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la comuna especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar comunas</div>
<div class="panes">
	<form action="sites/mantenedores/proc/delete_comuna.php" class="confirmValidar" id="del_comuna">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la comuna '<?=$datos[0]['dg_comuna'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_comuna_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_comuna_res"></div>
</div>