<?php
/**
*
*	Permite a los usuarios agregar nuevas lineas de negocio.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de producto</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Start("sites/mantenedores/proc/crear_tipo_producto.php","cr_tipo_producto");
	$form->Header("Complete los datos requeridos para crear un nuevo tipo de producto<br />Puede consultar los que ya están agregados.");
	$form->Section();
      $form->Text("Nombre","tipo_name",1);
	$form->EndSection();
	$form->Section();
      $form->Radiobox("Controla inventario","tipo_inventario",array("si","no"),0,"");
      $form->Radiobox("Debe costearse","tipo_costeo",array("si","no"),1,"");
      $form->Radiobox('Incluir en Orden de Compra', 'dm_orden_compra', array(1 => 'si', 0 => 'no'), 1, "");
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_tipo_producto",
		"dc_tipo_producto, dg_tipo_producto, dm_controla_inventario, dm_orden_compra, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_tipo_producto")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Tipos de producto ya agregados</caption>
	<thead>
		<tr>
			<th width="4%"></th>
            <th width="4%"></th>
			<th width="61%">Tipo de producto</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
	$inventario = $l['dm_controla_inventario']=='0'?"<img src='images/inventario.png' alt='I' title='Controla inventario' />":"";
    $orden_compra = $l['dm_orden_compra']=='1'?"<img src='images/carrito.gif' alt='OC' title='Incluir en Orden de Compra' />":"";
		echo("
		<tr id='item{$l['dc_tipo_producto']}'>
			<td>{$inventario}</td>
            <td>{$orden_compra}</td>
			<td>{$l['dg_tipo_producto']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ed_tipo_producto.php?id={$l['dc_tipo_producto']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/del_tipo_producto.php?id={$l['dc_tipo_producto']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>