<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Áreas de servicio</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Indique un nombre y correos de los encargados en el área de servicio<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/servicios/proc/crear_area_servicio.php","cr_area_servicio");
	$form->Section();
	$form->Text("Nombre","area_name",1);
	$form->EndSection();
	$form->Section();
	$form->Textarea('E-mails encargados - <small>Separados con ; (punto y coma)</small>','area_email',1);
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_area_servicio",
		"dc_area_servicio, dg_area_servicio, dg_correos, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_area_servicio")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Áreas de servicio ya agregados</caption>
	<thead>
		<tr>
			<th width="30%">Área de servicio</th>
			<th>Correos</th>
			<th width="20%">
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_area_servicio']}'>
			<td>{$l['dg_area_servicio']}</td>
			<td>{$l['dg_correos']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/servicios/ed_area_servicio.php?id={$l['dc_area_servicio']}' class='loadOnOverlay left'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/servicios/del_area_servicio.php?id={$l['dc_area_servicio']}' class='loadOnOverlay left'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>