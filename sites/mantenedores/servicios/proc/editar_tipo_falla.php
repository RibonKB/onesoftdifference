<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_tipo_falla_os",
array(
	"dg_tipo_falla" => trim(ucwords($_POST['ed_tipo_name'])),
	"dc_area_servicio" => $_POST['ed_tipo_area'],
    'dg_checklist_herramientas' => $_POST['dg_checklist_herramientas_ed'],
    'dg_checklist_estado' => $_POST['dg_checklist_estado_ed']
),"dc_tipo_falla = {$_POST['ed_tipo_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/servicios/src_tipo_falla.php");
</script>