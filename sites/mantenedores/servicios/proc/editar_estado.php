<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dg_estado = $_POST['dg_estado_ed'];
$dc_estado = $_POST['dc_estado_ed'];

$estado = $db->prepare($db->select('tb_estado_orden_servicio','true','dc_empresa = ? AND dg_estado = ?'));
$estado->bindValue(1,$empresa,PDO::PARAM_INT);
$estado->bindValue(2,$dg_estado,PDO::PARAM_STR);
$db->stExec($estado);

if($estado->fetch() !== false){
	$error_man->showWarning("Ya existe un estado con ese nombre");
	exit; 
}

$db->start_transaction();

$editar = $db->prepare($db->update('tb_estado_orden_servicio',array('dg_estado' => '?'),
				'dc_estado = ? AND dc_empresa = ?'));
$editar->bindValue(1,$dg_estado,PDO::PARAM_STR);
$editar->bindValue(2,$dc_estado,PDO::PARAM_INT);
$editar->bindValue(3,$empresa,PDO::PARAM_INT);
$db->stExec($editar);

$db->commit();


$error_man->showConfirm('Se ha modificado correctamente'); 

?>