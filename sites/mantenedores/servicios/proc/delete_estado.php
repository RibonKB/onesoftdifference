<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dc_estado = intval($_POST['dc_estado_ed']);

$estado = $db->prepare($db->select('tb_estado_orden_servicio','dm_activo','dc_empresa = ? AND dc_estado = ?'));
$estado->bindValue(1,$empresa,PDO::PARAM_INT);
$estado->bindValue(2,$dc_estado,PDO::PARAM_INT); 
$db->stExec($estado);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_estado_orden_servicio',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_estado =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_estado,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamento'); 


?>