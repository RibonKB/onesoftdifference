<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dc_tipo_falla = intval($_POST['dc_tipo_falla_ed']);

$tipo_falla = $db->prepare($db->select('tb_tipo_falla_os','dm_activo','dc_empresa = ? AND dc_tipo_falla = ?'));
$tipo_falla->bindValue(1,$empresa,PDO::PARAM_INT);
$tipo_falla->bindValue(2,$dc_tipo_falla,PDO::PARAM_INT); 
$db->stExec($tipo_falla);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_tipo_falla_os',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_tipo_falla =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_tipo_falla,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamento'); 


?>