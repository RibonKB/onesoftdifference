<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_area_servicio",
array(
	"dg_area_servicio" => trim(ucwords($_POST['ed_area_name'])),
	"dg_correos" => $_POST['ed_area_email']
),"dc_area_servicio = {$_POST['ed_area_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/mantenedores/servicios/src_area_servicio.php");
</script>