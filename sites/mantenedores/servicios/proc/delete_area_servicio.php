<?php
define("MAIN",1);
require_once('../../../../inc/init.php');

$dc_area_servicio = intval($_POST['dc_area_servicio_ed']);

$area_servicio = $db->prepare($db->select('tb_area_servicio','dm_activo','dc_empresa = ? AND dc_area_servicio = ?'));
$area_servicio->bindValue(1,$empresa,PDO::PARAM_INT);
$area_servicio->bindValue(2,$dc_area_servicio,PDO::PARAM_INT); 
$db->stExec($area_servicio);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_area_servicio',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_area_servicio =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_area_servicio,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamento'); 


?>
