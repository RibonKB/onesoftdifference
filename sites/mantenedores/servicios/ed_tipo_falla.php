<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_tipo_falla_os",
"dg_tipo_falla,dc_area_servicio,dg_checklist_estado,dg_checklist_herramientas",
"dc_tipo_falla = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de falla especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Tipos de falla</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/mantenedores/servicios/proc/editar_tipo_falla.php","ed_tipo_falla");
	$form->Header("Indique los datos actualizados del tipo de falla");
    
    $form->Section();
      $form->Text("Nombre","ed_tipo_name",1,255,$datos['dg_tipo_falla']);
      $form->Listado("Área de servicio","ed_tipo_area","tb_area_servicio",array('dc_area_servicio','dg_area_servicio'),1,$datos['dc_area_servicio']);
    $form->EndSection();
    
    $form->Section();
      $form->Textarea('Checlist Herramientas (Uno por línea)', 'dg_checklist_herramientas_ed', false, $datos['dg_checklist_herramientas']);
      $form->Textarea('Checlist Estado (Uno por línea)', 'dg_checklist_estado_ed', false, $datos['dg_checklist_estado']);
    $form->EndSection();
    
	$form->Hidden('ed_tipo_id',$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>