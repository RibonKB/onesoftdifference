<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo_falla = intval($_POST['id']);

$tipo_falla = $db->prepare($db->select('tb_tipo_falla_os',
	'dg_tipo_falla, dm_activo','dc_tipo_falla = ? AND dc_empresa = ?'));
$tipo_falla->bindValue(1,$dc_tipo_falla,PDO::PARAM_INT);
$tipo_falla->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($tipo_falla);

$tipo_falla = $tipo_falla->fetch(PDO::FETCH_OBJ);

if($tipo_falla === false){
	$error_man->showWarning('El tipo de falla seleccionado no fue encontrado');
	exit;
}

?>

<div class="secc_bar" >
	Eliminar tipo de falla
</div>

<div class="panes" >
	<?php $form->Start('sites/mantenedores/servicios/proc/delete_tipo_falla.php', 'del_tipo_falla') ?>
    <?php $form->Header('¿Está seguro que desea eliminar el tipo de falla?') ?>
    
    <div class="center" >
    	<?php echo $tipo_falla->dg_tipo_falla ?>
    </div>
    <?php $form->Hidden('dc_tipo_falla_ed', $dc_tipo_falla)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>