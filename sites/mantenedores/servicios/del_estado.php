<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_estado = intval($_POST['id']);

$estado = $db->prepare($db->select('tb_estado_orden_servicio',
	'dg_estado, dm_activo','dc_estado = ? AND dc_empresa = ?'));
$estado->bindValue(1,$dc_estado,PDO::PARAM_INT);
$estado->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($estado);

$estado = $estado->fetch(PDO::FETCH_OBJ);

if($estado === false){
	$error_man->showWarning('El estado seleccionado no fue encontrado');
	exit;
}


?>


<div class="secc_bar" >
	Eliminar estado
</div>

<div class="panes" >
	<?php $form->Start('sites/mantenedores/servicios/proc/delete_estado.php', 'del_estado') ?>
    <?php $error_man->showAviso("¿Está seguro que desea eliminar el estado <strong>{$estado->dg_estado}</strong>?") ?>
    <?php $form->Hidden('dc_estado_ed', $dc_estado)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>
