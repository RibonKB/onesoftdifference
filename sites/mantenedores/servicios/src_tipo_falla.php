<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de falla</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Start("sites/mantenedores/servicios/proc/crear_tipo_falla.php","cr_tipo_falla");
    $form->Header("Complete los datos requeridos para crear un nuevo tipo de falla<br />Puede consultar los que ya están agregados.");
    
    $form->Section();
      $form->Text("Nombre","tipo_name",1);
      $form->Listado("Área de servicio","tipo_area","tb_area_servicio",array('dc_area_servicio','dg_area_servicio'),1);
    $form->EndSection();
    
    $form->Section();
      $form->Textarea('Checklist herramientas (Uno por línea)', 'dg_checklist_herramientas');
    $form->EndSection();
    
    $form->Section();
      $form->Textarea('Checklist estado (Uno por línea)', 'dg_checklist_estado');
    $form->EndSection();
    
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"(SELECT * FROM tb_tipo_falla_os WHERE dc_empresa={$empresa} AND dm_activo='1') tf
		JOIN tb_area_servicio a ON a.dc_area_servicio = tf.dc_area_servicio",
		"tf.dc_tipo_falla, tf.dg_tipo_falla, DATE_FORMAT(tf.df_creacion,'%d/%m/%Y %H:%i') as df_fecha, a.dg_area_servicio","",
		array("order_by" => "dg_tipo_falla")
	);
	
	echo('<table class="tab" width="100%">
	<caption>Tipos de falla ya agregados</caption>
	<thead>
		<tr>
			<th width="40%">Tipo de fallo</th>
			<th width="25%">Area de servicio</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("<tr id='item{$l['dc_tipo_falla']}'>
			<td>{$l['dg_tipo_falla']}</td>
			<td>{$l['dg_area_servicio']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/servicios/ed_tipo_falla.php?id={$l['dc_tipo_falla']}' class='loadOnOverlay left'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/servicios/del_tipo_falla.php?id={$l['dc_tipo_falla']}' class='loadOnOverlay left'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>