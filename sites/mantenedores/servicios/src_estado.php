<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Estado de orden de servicio</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo estado de orden de servicio<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/servicios/proc/crear_estado.php","cr_estado_os");
	$form->Text("Nombre","estado_name",1);
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_estado_orden_servicio",
		"dc_estado, dg_estado, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"dc_empresa={$empresa} AND dm_activo='1'",
		array("order_by" => "dg_estado")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Estados de orden de servicio ya agregados</caption>
	<thead>
		<tr>
			<th width="65%">Estado</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_estado']}'>
			<td>{$l['dg_estado']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/servicios/ed_estado.php?id={$l['dc_estado']}' class='loadOnOverlay left'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/servicios/del_estado.php?id={$l['dc_estado']}' class='loadOnOverlay left'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>