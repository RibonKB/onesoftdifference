<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_area_servicio = intval($_POST['id']);

$area_servicio = $db->prepare($db->select('tb_area_servicio', 'dg_area_servicio, dg_correos','dc_area_servicio = ? AND dc_empresa = ?'));
$area_servicio->bindValue(1,$dc_area_servicio,PDO::PARAM_INT);
$area_servicio->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($area_servicio);

$area_servicio = $area_servicio->fetch(PDO::FETCH_OBJ);

if($area_servicio === false){
	$error_man->showWarning("No se encontrado la área de servicio seleccionada");
	exit();
}

?>

<div class="secc_bar">
	Eliminar área de servicio
</div>

<div class="panes">
	<?php $form->Start('sites/mantenedores/servicios/proc/delete_area_servicio.php','del_area_servicio'); ?>
    <?php $form->Header('¿Esta seguro que desea eliminar el área de servicio'); ?>
    
    <div class="center">
    	<?php echo $area_servicio->dg_area_servicio ?>
    </div>
    
    <?php $form->Hidden('dc_area_servicio_ed',$dc_area_servicio); ?>
    <?php $form->End('Eliminar','delbtn'); ?>
</div>