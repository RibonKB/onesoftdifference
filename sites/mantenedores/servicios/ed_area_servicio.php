<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_area_servicio",
"dg_area_servicio,dg_correos",
"dc_area_servicio = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el área de servicio especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición áreas de servicio</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados del área de servicio");
	$form->Start("sites/mantenedores/servicios/proc/editar_area_servicio.php","ed_area_servicio");
	$form->Section();
	$form->Text("Nombre","ed_area_name",1,255,$datos['dg_area_servicio']);
	$form->EndSection();
	$form->Section();
	$form->Textarea('E-mails encargados - <small>Separados con ; (punto y coma)</small>','ed_area_email',1,$datos['dg_correos']);
	$form->EndSection();
	$form->Hidden("ed_area_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>