<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_estado = intval($_POST['id']);

$estado = $db->prepare($db->select('tb_estado_orden_servicio',
	'dg_estado','dc_estado = ? AND dc_empresa = ?'));
$estado->bindValue(1,$dc_estado,PDO::PARAM_INT);
$estado->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($estado);

$estado = $estado->fetch(PDO::FETCH_OBJ);

if($estado === false){
	$error_man->showWarning('El estado seleccionado no fue encontrado');
	exit;
}
?>

<div class="secc_bar">  
	Editar estado  
</div>
<div class="pannes">
<?php $form->Start('sites/mantenedores/servicios/proc/editar_estado.php', 'ed_estado') ?>
<?php $form->Header('<b>Ingrese el nombre para actualizar el medio de pago</b><br />Los campos marcados con [*] son obligatorios') ?>

		<?php 
		$form->Section(); 
        	$form->Text('Nombre','dg_estado_ed',true,Form::DEFAULT_TEXT_LENGTH,$estado->dg_estado);    
        $form->EndSection();			
		?>
        
             
        <?php $form->Hidden('dc_estado_ed',$dc_estado) ?>
    <?php $form->End('Editar','editbtn') ?>
    
    
</div>
