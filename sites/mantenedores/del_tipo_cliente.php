<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_cliente",
"dg_tipo_cliente",
"dc_tipo_cliente= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de cliente especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar tipos de cliente</div>
<div class="panes">
	<form action="sites/mantenedores/proc/delete_tipo_cliente.php" class="confirmValidar" id="del_tipo_cliente">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar  el tipo de cliente '<?=$datos[0]['dg_tipo_cliente'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_tipo_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_tipo_cliente_res"></div>
</div>