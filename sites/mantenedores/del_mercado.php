<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_mercado",
"dg_mercado",
"dc_mercado= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el mercado especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar mercados</div>
<div class="panes">
	<form action="sites/mantenedores/proc/delete_mercado.php" class="confirmValidar" id="del_mercado">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el mercado '<?=$datos[0]['dg_mercado'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_mercado_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_mercado_res"></div>
</div>