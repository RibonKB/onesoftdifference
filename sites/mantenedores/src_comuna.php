<?php
/**
*
*	Permite a los usuarios agregar nuevas comunas.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Comunas</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear una nueva comuna<br />Puede consultar los que ya están agregados.");
	$form->Start("sites/mantenedores/proc/crear_comuna.php","cr_comuna");
	$form->Section();
	$form->Text("Nombre","comuna_name",1);
	$form->EndSection();
	$form->Section();
	$form->Listado("Región","comuna_region","tb_region",array("dc_region","dg_region"),1,'',"");
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_comuna c,tb_region r",
		"c.dc_comuna,c.dg_comuna, r.dg_region, DATE_FORMAT(c.df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"c.dc_region = r.dc_region",
		array("order_by" => "c.dg_comuna")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Comunas ya agregadas</caption>
	<thead>
		<tr>
			<th width="30%">Comuna</th>
			<th width="35%">Región</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_comuna']}'>
			<td>{$l['dg_comuna']}</td>
			<td>{$l['dg_region']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ed_comuna.php?id={$l['dc_comuna']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/del_comuna.php?id={$l['dc_comuna']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>