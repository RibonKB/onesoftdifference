<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_tipo_producto",
"dg_tipo_producto,dm_controla_inventario,dm_orden_compra",
"dc_tipo_producto = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el tipo de producto especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición tipos de producto</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el tipo de producto");
	$form->Start("sites/mantenedores/proc/editar_tipo_producto.php","ed_tipo_producto");
	$form->Section();
	$form->Text("Nombre","ed_tipo_name",1,255,$datos['dg_tipo_producto']);
	$form->Endsection();
	$form->Section();
	$form->Radiobox("Controla inventario","ed_tipo_inventario",array("si","no"),$datos['dm_controla_inventario'],"");
    $form->Radiobox('Incluir en Orden de Compra', 'dm_orden_compra_ed', array(1 => 'si', 0 => 'no'), $datos['dm_orden_compra'], "");
	$form->EndSection();
	$form->Hidden("ed_tipo_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>