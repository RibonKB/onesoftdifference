<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_marca",
"dg_marca",
"dc_marca = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el cargo especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición de marcas</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para la marca");
	$form->Start("sites/mantenedores/proc/editar_marca_producto.php","ed_marca");
	$form->Text("Nombre","ed_marca_name",1,255,$datos['dg_marca']);
	$form->Hidden("ed_marca_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>