<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_mercado",
"dg_mercado",
"dc_mercado = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el mercado especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición de mercados</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el mercado");
	$form->Start("sites/mantenedores/proc/editar_mercado.php","ed_linea_negocio");
	$form->Text("Nombre","ed_mercado_name",1,255,$datos['dg_mercado']);
	$form->Hidden("ed_mercado_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>