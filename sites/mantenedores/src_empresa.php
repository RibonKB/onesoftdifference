<?php
/**
*
*	Permite a los usuarios agregar nuevas lineas de negocio.
*
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Linea de negocio para productos</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Complete los datos para crear la nueva empresa");
	$form->Start("sites/mantenedores/proc/addempresa.php","cr_empresa");
	echo("<legend>Datos generales</legend>");
	$form->Section();
	$form->Text("Rut","e_rut",1,12);
	$form->Text("Razón social","e_razon",1);
	$form->Text("Teléfono","e_fono",1);
	$form->EndSection();
	$form->Section();
	$form->Text("Dirección","e_direccion",1);
	
	$regiones = $db->select("tb_region");
	echo("<label>Region<br />
		<select onchange='showComunas(\"e_comuna\",this)' class='inputtext'>
		<option value='0'></option>
	");
	foreach($regiones as $r){
		echo("<option value='{$r['dc_region']}'>{$r['dg_region']}</option>");
	}
	echo("</select></label><br />
		<label>Comuna[*]<br />
		<select name='e_comuna' id='e_comuna' class='inputtext' required='required'>
			<option></option>
		</select>
		</label>
		<span id='e_comuna_comloader'></span>
		<br />
	");
	
	$form->Group();
	echo("<legend>Datos documentación</legend>");
	$form->Section();
	$form->Text("Moneda local","doc_moneda",1,255,"Pesos");
	$form->Text("I.V.A. (%)","doc_iva",1,255,"19.00");
	$form->Textarea("Pie de cotización","doc_pie_cot",1);
	$form->EndSection();
	$form->Section();
	$form->Radiobox("Modo de cotización","cot_modo",array(1=> "Modo 1 [ejemplo]",2=> "Modo 2 [ejemplo]"),1);
	$form->EndSection();
	echo('<br class="clear" />');
	$form->Section();
	echo("<fieldset><legend>Correlativo <strong>cotización</strong></legend>");
	$form->Radiobox("Correlativo","cot_corr",
	array(
		1 => "Año-Correlativo (Ej: 2011001)",
		2 => "Año-Mes-Correlativo (Ej: 201103001)",
		3 => "Correlativo único infinito (Ej: 0001)"
	),1);
	$form->Text("Nº Comienzo","cot_comienzo",1);
	$form->EndSection();
	
	$form->Section();
	echo("<fieldset><legend>Correlativo <strong>nota de crédito</strong></legend>");
	$form->Radiobox("Correlativo","notas_credito_corr",
	array(
		1 => "Año-Correlativo (Ej: 2011001)",
		2 => "Año-Mes-Correlativo (Ej: 201103001)",
		3 => "Correlativo único infinito (Ej: 0001)"
	),1);
	$form->Text("Nº Comienzo","nota_credito_comienzo",1);
	$form->EndSection();
	
	echo("<br class='clear'/>");
	
	$form->Section();
	echo("<fieldset><legend>Correlativo <strong>nota de venta</strong></legend>");
	$form->Radiobox("Correlativo","nota_venta_corr",
	array(
		1 => "Año-Correlativo (Ej: 2011001)",
		2 => "Año-Mes-Correlativo (Ej: 201103001)",
		3 => "Correlativo único infinito (Ej: 0001)"
	),1);
	$form->Text("Nº Comienzo","nota_venta_comienzo",1);
	$form->EndSection();
	
	$form->Section();
	echo("<fieldset><legend>Correlativo <strong>nota de débito</strong></legend>");
	$form->Radiobox("Correlativo","nota_debito_corr",
	array(
		1 => "Año-Correlativo (Ej: 2011001)",
		2 => "Año-Mes-Correlativo (Ej: 201103001)",
		3 => "Correlativo único infinito (Ej: 0001)"
	),1);
	$form->Text("Nº Comienzo","nota_debito_comienzo",1);
	$form->EndSection();
	
	echo("<br class='clear'/>");
	
	$form->Section();
	echo("<fieldset><legend>Correlativo <strong>documento contable</strong></legend>");
	$form->Radiobox("Correlativo","contable_corr",
	array(
		1 => "Año-Correlativo (Ej: 2011001)",
		2 => "Año-Mes-Correlativo (Ej: 201103001)",
		3 => "Correlativo único infinito (Ej: 0001)"
	),1);
	$form->Text("Nº Comienzo","contable_comienzo",1);
	$form->EndSection();
	
	$form->End("Crear","addbtn");
	
	$list = $db->select(
		"tb_empresa",
		"dc_empresa, dg_rut, dg_razon, DATE_FORMAT(df_creacion,'%d/%m/%Y %H:%i') as df_fecha",
		"",
		array("order_by" => "dg_razon")
	);
	
	echo('
	<table class="tab" width="100%">
	<caption>Lineas de negocio ya agregadas</caption>
	<thead>
		<tr>
			<th width="15%">Rut</th>
			<th width="50%">Razón social</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	');
	
	foreach($list as $l){
		echo("
		<tr id='item{$l['dc_empresa']}'>
			<td>{$l['dg_rut']}</td>
			<td>{$l['dg_razon']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/mantenedores/ed_empresa.php?id={$l['dc_empresa']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/mantenedores/del_empresa.php?id={$l['dc_empresa']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>