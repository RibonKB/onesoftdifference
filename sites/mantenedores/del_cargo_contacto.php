<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_cargo_contacto",
"dg_cargo_contacto",
"dc_cargo_contacto= {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el cargo especificado");
	exit();
}
?>
<div class="secc_bar">Eliminar cargos de contacto</div>
<div class="panes">
	<form action="sites/mantenedores/proc/delete_cargo_contacto.php" class="confirmValidar" id="del_cextra">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el cargo de contacto '<?=$datos[0]['dg_cargo_contacto'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_cargo_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
	<div id="del_cextra_res"></div>
</div>