<?php
/**
*	Formulario de edición de centro beneficio
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$cebe =& $_POST['id'];
$datos = $db->select("tb_cebe","dg_cebe,dc_ceco,df_termino,dg_responsable",
"dc_cebe = {$cebe} AND dc_empresa = {$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showWarning("No se encontró el centro beneficio especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Centro de Beneficio</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos para la edición del centro beneficio<br />(Los datos marcados con [*] son obligatorios)");
	$form->Start("sites/est_empresa/proc/editar_cebe.php","ed_cebe");
	$form->Section();
	$form->Text("Nombre","cebe_name",1,255,$datos['dg_cebe']);
	$form->Date("Termino","cebe_fin",0,$datos['df_termino']);
	$form->EndSection();
	$form->Section();
	$form->Listado("Centro costo","cebe_ceco","tb_ceco",array("dc_ceco","dg_ceco"),1,$datos['dc_ceco']);
	$form->Text("Responsable","cebe_resp",0,255,$datos['dg_responsable']);
	$form->EndSection();
	$form->Hidden("cebe_id",$cebe);
	$form->End("Editar","editbtn");
?>
</div>
<script type="text/javascript">
	$("select[name=cebe_ceco]").val(<?php echo($datos['dc_ceco']); ?>);
</script>