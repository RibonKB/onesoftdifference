<?php
/**
*	Formulario de edición de grupo de centros de costo
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$g_id =& $_POST['id'];

$datos = $db->select("tb_grupo_ceco","dg_grupo_ceco,dg_responsable",
"dc_grupo_ceco = {$g_id} AND dc_empresa = {$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showWarning("No se encontró el grupo especificado");
	exit();
}
$datos = $datos[0];

?>
<div class="secc_bar">Edición grupos Centro de Costo</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos para la edición del grupo<br />(Los datos marcados con [*] son obligatorios)");
	$form->Start("sites/est_empresa/proc/editar_grupo.php","ed_grupo");
	$form->Section();
	$form->Text("Nombre","grupo_nombre",1,255,$datos['dg_grupo_ceco']);
	$form->EndSection();
	$form->Section();
	$form->Text("Responsable","grupo_resp",0,255,$datos['dg_responsable']);
	$form->EndSection();
	$form->Hidden("grupo_id",$g_id);
	$form->End("Editar","editbtn");
?>
</div>