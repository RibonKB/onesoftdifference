<?php
/**
*	Formulario de creación de un nuevo centro beneficio
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div class="secc_bar">Creación Centro de Beneficio</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Header("Indique los datos para la creación del centro beneficio<br />(Los datos marcados con [*] son obligatorios)");
	$form->Start("sites/est_empresa/proc/crear_cebe.php","cr_cebe");
	$form->Section();
	$form->Text("Nombre","cebe_name",1);
	$form->Date("Término","cebe_fin");
	$form->EndSection();
	$form->Section();
	$form->Listado("Centro costo","cebe_ceco","tb_ceco",array("dc_ceco","dg_ceco"),1);
	$form->Text("Responsable","cebe_resp");
	$form->EndSection();
	$form->Hidden("cebe_group",$_POST['id']);
	$form->End("Crear","addbtn");
?>
</div>