<?php
/**
*	Formulario de creación de un nuevo subgrupo de centros de costo
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div class="secc_bar">Creación subgrupos Centro de Costo</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos para la creación del subgrupo<br />(Los datos marcados con [*] son obligatorios)");
	$form->Start("sites/est_empresa/proc/crear_subgrupo.php","cr_subgrupo");
	$form->Section();
	$form->Text("Nombre","subgrupo_nombre",1);
	$form->EndSection();
	$form->Section();
	$form->Text("Responsable","subgrupo_resp");
	$form->EndSection();
	$form->Hidden("group_id",$_POST['id']);
	$form->End("Crear","addbtn");
?>
</div>