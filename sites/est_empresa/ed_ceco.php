<?php
/**
*	Formulario de edición de centro costo
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$ceco =& $_POST['id'];
$datos = $db->select("tb_ceco","dg_ceco,dc_segmento,df_termino,dg_responsable",
"dc_ceco = {$ceco} AND dc_empresa = {$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showWarning("No se encontró el centro costo especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Centro de Costo</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Header("Indique los datos para la edición del centro costo<br />(Los datos marcados con [*] son obligatorios)");
	$form->Start("sites/est_empresa/proc/editar_ceco.php","ed_ceco");
	$form->Section();
	$form->Text("Nombre","ceco_name",1,255,$datos['dg_ceco']);
	$form->Date("Término","ceco_fin",0,$datos['df_termino']);
	$form->EndSection();
	$form->Section();
	$form->Listado("Segmento","ceco_segm","tb_segmento",array("dc_segmento","dg_segmento"),1,$datos['dc_segmento']);
	$form->Text("Responsable","ceco_resp",0,255,$datos['dg_responsable']);
	$form->EndSection();
	$form->Hidden("ceco_id",$ceco);
	$form->End("Crear","addbtn");
?>
</div>
<script type="text/javascript">
	$("select[name=ceco_segm]").val(<?php echo($datos['dc_segmento']); ?>);
</script>