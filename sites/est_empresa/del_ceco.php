<?php
/**
*	Formulario de eliminación de centro de costo
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$ceco =& $_POST['id'];

$datos = $db->select("tb_ceco","dg_ceco,dc_segmento,df_termino,dg_responsable",
"dc_ceco = '{$ceco}' AND dc_empresa = {$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showWarning("No se encontró el centro costo especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Centro de Costo</div>

<div class="panes">
	<form action="sites/est_empresa/proc/delete_ceco.php" class="validar" id="del_ceco">
	<fieldset>
	<div class="alert">
		¿Está seguro que quiere eliminar el centro costo <strong><?php echo($datos['dg_ceco']); ?></strong>
	</div>
	<div class="center">
		<input type="hidden" name="ceco_id" value="<?php echo($ceco); ?>" />
		<input type="submit" class="delbtn" value="Eliminar" />
		<input type="button" class="button" value="Cancelar" onclick="$('#genOverlay').remove();" />
	</div>
	</fieldset>
	</form>
	<div id="del_ceco_res"></div>
</div>