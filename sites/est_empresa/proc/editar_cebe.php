<?php
/**
*	Edicion de centro de beneficio
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se les da formato a los datos que lo requieren
$c_nombre =& $_POST['cebe_name'];
$c_resp =& $_POST['cebe_resp'];
$c_fin = explode("/",$_POST['cebe_fin']);
$c_fin = count($c_fin)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$c_fin[1],$c_fin[0],$c_fin[2]))."'":"null";
$c_ceco =& $_POST['cebe_ceco'];

//identificador del centro beneficio a editar
$c_id =& $_POST['cebe_id'];

//no se puede crear un centro beneficio con el mismo nombre que otro ya creado
if(count($db->select("tb_cebe","1","dg_cebe = '{$c_nombre}' AND dc_empresa = {$empresa} AND dc_cebe <> {$c_id}"))){
	$error_man->showWarning("Ya existe un centro de beneficio con ese nombre");
	exit();
}

//se actualizan los datos
$db->update("tb_cebe",
array(
"dg_cebe" => $c_nombre,
"dc_ceco" => $c_ceco,
"df_termino" => $c_fin,
"dg_responsable" => $c_resp),
"dc_cebe = {$c_id}"
);

$error_man->showConfirm("
Se han actualizado los datos del centro beneficio <strong>{$c_nombre}</strong> correctamente<br />
<strong class=\"center\"><a href=\"#\" onclick=\"$('#genOverlay').remove();\">Cerrar</a></strong>
");

?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_cebe.php");
</script>