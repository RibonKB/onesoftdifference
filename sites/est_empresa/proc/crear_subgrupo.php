<?php
/**
*	Creación de subgrupo de centros de costo
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se da formato a los datos que lo requiren
$sg_nombre =& $_POST['subgrupo_nombre'];
$sg_resp =& $_POST['subgrupo_resp'];

//grupo al que se le debe asignar
$sg_gid =& $_POST['group_id'];

//se inserta en la base de datos el nuevo subgrupo
$sg_id = $db->insert("tb_grupo_ceco",
array(
"dg_grupo_ceco" => $sg_nombre,
"dc_empresa" => $empresa,
"dg_responsable" => $sg_resp,
"df_creacion" => "NOW()",
"dc_usuario_creacion" => $idUsuario
));

//se inserta la conexión con el grupo al que pertenece
$db->insert("tb_subgrupo_ceco",
array(
"dc_grupo_ceco" => $sg_gid,
"dc_subgrupo_ceco" => $sg_id
));

$error_man->showConfirm("
Se a creado el subgrupo <strong>{$sg_nombre}</strong> correctamente<br />
<strong class=\"center\"><a href=\"#\" onclick=\"$('#genOverlay').remove();\">Cerrar</a></strong>
");

//se obtienen los datos del subgrupo recien creado para mostrarlos en el formatod de la estructura
$datos = $db->select(
"tb_grupo_ceco g,tb_usuario u",
"dc_grupo_ceco,dg_grupo_ceco,dg_responsable,g.df_creacion,dg_usuario",
"dc_usuario_creacion = dc_usuario AND dc_grupo_ceco = {$sg_id}");

$datos = $datos[0];

?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_ceco.php");
</script>