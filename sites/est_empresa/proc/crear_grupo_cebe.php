<?php
/**
*	Creación del grupo de centros de beneficio
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se da formato a los datos que lo requieren
$g_nombre =& $_POST['grupo_nombre'];
$g_resp =& $_POST['grupo_resp'];

// se inserta el nuevo grupo en la base de datos y se obtiene el identificador generado
$g_id = $db->insert("tb_grupo_cebe",
array(
"dg_grupo_cebe" => $g_nombre,
"dc_empresa" => $empresa,
"dg_responsable" => $g_resp,
"df_creacion" => "NOW()",
"dc_usuario_creacion" => $idUsuario
));

$error_man->showConfirm("
Se a creado el grupo <strong>{$g_nombre}</strong> correctamente<br />
<strong class=\"center\"><a href=\"#\" onclick=\"$('#genOverlay').remove();\">Cerrar</a></strong>
");

//se obtienen los datos del grup recien creado para ser mostrado en el formato de la estructura
$datos = $db->select(
"tb_grupo_cebe g,tb_usuario u",
"dc_grupo_cebe,dg_grupo_cebe,dg_responsable,g.df_creacion,dg_usuario",
"dc_usuario_creacion = dc_usuario AND dc_grupo_cebe = {$g_id}");
$datos = $datos[0];
?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_cebe.php");
</script>