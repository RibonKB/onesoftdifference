<?php
/**
*	Desactivación de centros de beneficio
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se desactiva el registro en la base de datos
$db->update("tb_cebe",array("dm_activo" => "0"),"dc_cebe = {$_POST['cebe_id']}");

?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_cebe.php");
</script>