<?php
/**
*	Creación de un nuevo centro de beneficio según los datos ingresados en el formulario de creación.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se da formato a los datos que lo requieren
$c_nombre =& $_POST['cebe_name'];
$c_resp =& $_POST['cebe_resp'];
$c_fin = explode("/",$_POST['cebe_fin']);
$c_fin = count($c_fin)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$c_fin[1],$c_fin[0],$c_fin[2]))."'":"null";
$c_grupo =& $_POST['cebe_group'];
$c_ceco =& $_POST['cebe_ceco'];

//no se permiten mas de un centro de beneficio con el mismo nombre
if(count($db->select("tb_cebe","1","dg_cebe = '{$c_nombre}' AND dc_empresa = {$empresa}"))){
	$error_man->showWarning("Ya existe un centro de beneficio con ese nombre");
	exit();
}

$genId = $db->insert("tb_cebe",
array(
"dg_cebe" => $c_nombre,
"df_termino" => $c_fin,
"dc_grupo_cebe" => $c_grupo,
"dc_ceco" => $c_ceco,
"dg_responsable" => $c_resp,
"dc_empresa" => $empresa,
"df_creacion" => "NOW()",
"dc_usuario_creacion" => $idUsuario
));

$error_man->showConfirm("
Se a creado el centro de beneficio <strong>{$c_nombre}</strong> correctamente<br />
<strong class=\"center\"><a href=\"#\" onclick=\"$('#genOverlay').remove();\">Cerrar</a></strong>
");

//se obtienen los datos del centro de beneficio agregado para mostrar con el formato aceptado por la estructura
$datos = $db->select(
		"tb_cebe c,tb_ceco s,tb_usuario u",
		"dg_cebe,c.df_termino,c.dg_responsable,c.df_creacion,dg_ceco,dg_usuario",
		"c.dc_ceco = s.dc_ceco AND u.dc_usuario = c.dc_usuario_creacion AND c.dc_cebe = {$genId}");
$datos = $datos[0];

?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_cebe.php");
</script>