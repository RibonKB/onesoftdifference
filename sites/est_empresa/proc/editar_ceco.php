<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se les da formato a los datos que lo requiren
$c_nombre =& $_POST['ceco_name'];
$c_resp =& $_POST['ceco_resp'];
$c_fin = explode("/",$_POST['ceco_fin']);
$c_fin = count($c_fin)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$c_fin[1],$c_fin[0],$c_fin[2]))."'":"null";
$c_segmento =& $_POST['ceco_segm'];

//identificador del centro de costo
$c_id =& $_POST['ceco_id'];

//no se permite ponerle el mismo nombre que otro centro costo ya creado
if(count($db->select("tb_ceco","1","dg_ceco = '{$c_nombre}' AND dc_empresa = {$empresa} AND dc_ceco <> {$c_id}"))){
	$error_man->showWarning("Ya existe un centro de costo con ese nombre");
	exit();
}

//Se actualizan los datos en la Base de datos
$db->update("tb_ceco",
array(
"dg_ceco" => $c_nombre,
"dc_segmento" => $c_segmento,
"df_termino" => $c_fin,
"dg_responsable" => $c_resp),
"dc_ceco = {$c_id}"
);

$error_man->showConfirm("
Se han actualizado los datos del centro costo <strong>{$c_nombre}</strong> correctamente<br />
<strong class=\"center\"><a href=\"#\" onclick=\"$('#genOverlay').remove();\">Cerrar</a></strong>
");

?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_ceco.php");
</script>