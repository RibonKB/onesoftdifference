<?php
/**
*	Edicion de grupo de centros de costo
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se da formato a los datos que lo requiren
$g_nombre =& $_POST['grupo_nombre'];
$g_resp =& $_POST['grupo_resp'];
$g_id =& $_POST['grupo_id'];

//se actualizan los datos en la base de datos
$db->update("tb_grupo_ceco",
array(
"dg_grupo_ceco" => $g_nombre,
"dg_responsable" => $g_resp),
"dc_grupo_ceco = {$g_id}");

//se recarga la pagina para mostrar la estructura con los datos actualizados
?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_ceco.php");
</script>