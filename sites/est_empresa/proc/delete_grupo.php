<?php
/**
*	Desactivación de grupo de centros de costo
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se deshabilita el registro en la base de datos
$db->update("tb_grupo_ceco",array("dm_activo" => 0),"dc_grupo_ceco = {$_POST['ceco_id']}");

//se recarga la pagina para ver la estructura actualizada
?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_ceco.php");
</script>