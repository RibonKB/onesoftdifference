<?php
/**
*	Creación de subgrupos de centros de beneficio
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se les da formato a los datos que lo requiren
$sg_nombre =& $_POST['subgrupo_nombre'];
$sg_resp =& $_POST['subgrupo_resp'];

//grupo al cual se les asignará el subgrupo
$sg_gid =& $_POST['group_id'];

//se inserta en la base de datos los datos del subgrupo
$sg_id = $db->insert("tb_grupo_cebe",
array(
"dg_grupo_cebe" => $sg_nombre,
"dc_empresa" => $empresa,
"dg_responsable" => $sg_resp,
"df_creacion" => "NOW()",
"dc_usuario_creacion" => $idUsuario
));

//se inserta la relacion del subgrupo con el grupo
$db->insert("tb_subgrupo_cebe",
array(
"dc_grupo_cebe" => $sg_gid,
"dc_subgrupo_cebe" => $sg_id
));

$error_man->showConfirm("
Se a creado el subgrupo <strong>{$sg_nombre}</strong> correctamente<br />
<strong class=\"center\"><a href=\"#\" onclick=\"$('#genOverlay').remove();\">Cerrar</a></strong>
");

//se obtienen los datos del subgrupo recien ingresado para ser mostrados en el formato de la estructura
$datos = $db->select(
"tb_grupo_cebe g,tb_usuario u",
"dc_grupo_cebe,dg_grupo_cebe,dg_responsable,g.df_creacion,dg_usuario",
"dc_usuario_creacion = dc_usuario AND dc_grupo_cebe = {$sg_id}");

$datos = $datos[0];

?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_cebe.php");
</script>