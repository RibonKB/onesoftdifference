<?php
/**
*	Creación de un nuevo centro de costo según los datos ingresados en el formulario de creación.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se da formato a los datos que lo requieren
$c_nombre =& $_POST['ceco_name'];
$c_resp =& $_POST['ceco_resp'];
$c_fin = explode("/",$_POST['ceco_fin']);
$c_fin = count($c_fin)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$c_fin[1],$c_fin[0],$c_fin[2]))."'":"null";
$c_grupo =& $_POST['ceco_group'];
$c_segmento =& $_POST['ceco_segm'];

//no se permiten mas de un centro de costo con el mismo nombre
if(count($db->select("tb_ceco","1","dg_ceco = '{$c_nombre}' AND dc_empresa = {$empresa}"))){
	$error_man->showWarning("Ya existe un centro de costo con ese nombre");
	exit();
}

//se inserta en la base de datos el centro de costo creado
$genId = $db->insert("tb_ceco",
array(
"dg_ceco" => $c_nombre,
"df_termino" => $c_fin,
"dc_grupo_ceco" => $c_grupo,
"dc_segmento" => $c_segmento,
"dg_responsable" => $c_resp,
"dc_empresa" => $empresa,
"df_creacion" => "NOW()",
"dc_usuario_creacion" => $idUsuario
));

$error_man->showConfirm("
Se a creado el centro de costo <strong>{$c_nombre}</strong> correctamente<br />
<strong class=\"center\"><a href=\"#\" onclick=\"$('#genOverlay').remove();\">Cerrar</a></strong>
");

//se obtienen los datos del centro de costo agregado para mostrar con el formato aceptado por la estructura
$datos = $db->select(
		"tb_ceco c,tb_segmento s,tb_usuario u",
		"dg_ceco,df_termino,dg_responsable,c.df_creacion,dg_segmento,dg_usuario",
		"c.dc_segmento = s.dc_segmento AND u.dc_usuario = c.dc_usuario_creacion AND c.dc_ceco = {$genId}");
$datos = $datos[0];

?>
<script type="text/javascript">
	loadpage("sites/est_empresa/src_ceco.php");
</script>