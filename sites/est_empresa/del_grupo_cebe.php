<?php
/**
*	Formulario de eliminación de grupos de centros de beneficio
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$g_id =& $_POST['id'];

$datos = $db->select("tb_grupo_cebe","dg_grupo_cebe",
"dc_grupo_cebe = {$g_id} AND dc_empresa = {$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showWarning("No se encontró el grupo especificado");
	exit();
}
$datos = $datos[0];

?>
<div class="secc_bar">Eliminar grupo Centro de Beneficio</div>

<div class="panes">
	<form action="sites/est_empresa/proc/delete_grupo_cebe.php" class="validar" id="del_grupo">
	<fieldset>
	<div class="alert">
		&iquest;Está seguro que quiere eliminar el grupo centro beneficio <strong><?php echo($datos['dg_grupo_cebe']); ?></strong>
	</div>
	<div class="center">
		<input type="hidden" name="cebe_id" value="<?php echo($g_id); ?>" />
		<input type="submit" class="delbtn" value="Eliminar" />
		<input type="button" class="button" value="Cancelar" onclick="$('#genOverlay').remove();" />
	</div>
	</fieldset>
	</form>
	<div id="del_grupo_res"></div>
</div>