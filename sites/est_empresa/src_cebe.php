<?php
/**
*	Muestra la estructura de los centro de beneficio de la empresa y permite modificarla
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Datos de la empresa
$datos_empresa = $db->select("tb_empresa","dg_rut,dg_razon","dc_empresa={$empresa}");
$datos_empresa= $datos_empresa[0];

$gr_cebe = $db->select("(SELECT * FROM tb_grupo_cebe WHERE dc_empresa={$empresa} AND dm_activo = '1') g
LEFT JOIN (SELECT * FROM tb_usuario WHERE dc_empresa={$empresa}) u ON g.dc_usuario_creacion = u.dc_usuario
LEFT JOIN tb_subgrupo_cebe s ON g.dc_grupo_cebe = s.dc_subgrupo_cebe",
"g.dc_grupo_cebe,g.dg_grupo_cebe,g.dg_responsable,DATE_FORMAT(g.df_creacion,'%d/%m/%Y') as df_creacion,u.dg_usuario",
"s.dc_subgrupo_cebe is null");

?>
<link type="text/css" href="styles/treeview.css" rel="stylesheet" />
<div id="secc_bar">Estructura Centro Beneficio</div>

<div id="main_cont">
<div style="margin-left:70px;">
<div style="border:1px solid #000;background:#FFF793;width:250px;padding:10px;">
<label>RS: </label><?= $datos_empresa['dg_razon'] ?><br />
<label>RUT: </label><?= $datos_empresa['dg_rut'] ?><br />
</div>
<ul class="filetree">
<?php
	//para cada grupo
	foreach($gr_cebe as $grupo){
		//se obtienen los centros de beneficio del grupo
		$cebes = $db->select(
		"(SELECT * FROM tb_cebe WHERE dc_grupo_cebe = {$grupo['dc_grupo_cebe']} AND dc_empresa={$empresa} AND dm_activo='1') c
		LEFT JOIN (SELECT * FROM tb_ceco WHERE dc_empresa={$empresa} AND dm_activo='1') s ON c.dc_ceco = s.dc_ceco
		LEFT JOIN (SELECT * FROM tb_usuario WHERE dc_empresa={$empresa}) u ON u.dc_usuario = c.dc_usuario_creacion",
		"dc_cebe,dg_cebe,c.df_termino,c.dg_responsable,c.df_creacion,dg_ceco,dg_usuario");
		
		//se obtienen los subgrupos del grupo
		$subgrupos = $db->select(
		"(SELECT * FROM tb_grupo_cebe WHERE dm_activo = '1') gc
		JOIN (SELECT * FROM tb_subgrupo_cebe WHERE dc_grupo_cebe = {$grupo['dc_grupo_cebe']}) sg ON gc.dc_grupo_cebe = sg.dc_subgrupo_cebe
		LEFT JOIN (SELECT * FROM tb_usuario WHERE dc_empresa={$empresa}) u ON u.dc_usuario = gc.dc_usuario_creacion",
		"dc_subgrupo_cebe,dg_grupo_cebe,dg_responsable,gc.df_creacion,dg_usuario");
		
		//el botón para borrar grupos solo se muestra si el grupo no posee ningun subgrupo o centro de beneficio
		$del_btn = "";
		if(!count($cebes) && !count($subgrupos)){
			$del_btn = "<a href='sites/est_empresa/del_grupo_cebe.php?id={$grupo['dc_grupo_cebe']}' id='del_{$grupo['dc_grupo_cebe']}'><img src='images/delbtn.png'  alt='X' /> Eliminar grupo</a>";
		}
		
		//Se muestra el grupo con toda su información y con los botones de edición y agregación.
		echo("
		<li class='closed'>
		<span class='folder'>{$grupo['dg_grupo_cebe']}</span>
		<div class='tooltip-left'>
		<h2>{$grupo['dg_grupo_cebe']}</h2>
		<label>Responsable: </label><strong>{$grupo['dg_responsable']}</strong><br />
		<label>Creado por: </label><strong>{$grupo['dg_usuario']}</strong><br />
		<label>Fecha creación: </label><strong>{$grupo['df_creacion']}</strong>
		<br /><br />
		<div class='tiplinks'>
		<a href='sites/est_empresa/ed_grupo_cebe.php?id={$grupo['dc_grupo_cebe']}'>
		<img src='images/editbtn.png' alt='E' />Editar grupo</a>
		{$del_btn}
		<a href='sites/est_empresa/cr_cebe.php?id={$grupo['dc_grupo_cebe']}'>
		<img src='styles/images/file.gif' alt='' /> Crear Centro beneficio</a>
		<a href='sites/est_empresa/cr_subgrupo_cebe.php?id={$grupo['dc_grupo_cebe']}'>
		<img src='styles/images/folder.gif' alt='' /> Crear Subgrupo</a>
		</div>
		</div>");
		
		echo("<ul id='folder_{$grupo['dc_grupo_cebe']}'>");
		
		//si hay centro de beneficio o subgrupos se muestran
			foreach($cebes as $cebe){
				echo("
				<li>
				<span class='file'>{$cebe['dg_cebe']}</span>
				<div class='tooltip-left'>
				<h2>{$cebe['dg_cebe']}</h2>
				<label>Responsable: </label><strong>{$cebe['dg_responsable']}</strong><br />
				<label>Centro Costo: </label><strong>{$cebe['dg_ceco']}</strong><br />
				<label>Término validez: </label><strong>{$cebe['df_termino']}</strong><br />
				<label>Creado por: </label><strong>{$cebe['dg_usuario']}</strong><br />
				<label>Fecha Creación: </label><strong>{$cebe['df_creacion']}</strong>
				<br /><br />
				<div class='tiplinks'>
				<a href='sites/est_empresa/ed_cebe.php?id={$cebe['dc_cebe']}'><img src='images/editbtn.png' alt='E' /> Editar Centro Beneficio</a>
				<a href='sites/est_empresa/del_cebe.php?id={$cebe['dc_cebe']}'><img src='images/delbtn.png' alt='X' /> Eliminar Centro Beneficio</a>
				</div>
				</div>
				</li>");
			}
			
			//en caso de que hayan subgrupos para el grupo especificado estos se muestran como un arbol distinto dentro del grupo
			foreach($subgrupos as $subgrupo){
			
				//se obtienen los centros de beneficio del subgrupo
				$subcebes = $db->select(
				"tb_cebe c,tb_ceco s,tb_usuario u",
				"dc_cebe,dg_cebe,c.df_termino,c.dg_responsable,c.df_creacion,dg_ceco,dg_usuario",
				"c.dc_ceco = s.dc_ceco AND u.dc_usuario = c.dc_usuario_creacion AND c.dm_activo = '1' AND dc_grupo_cebe = {$subgrupo['dc_subgrupo_cebe']}"
				);
				
				//el boton para borrar subgrupos solo se muestra en el caso en que el subgrupo no tenga ningun centro de beneficio asignado
				$del_btn = "";
				if(!count($subcebes)){
					$del_btn = "<a href='sites/est_empresa/del_grupo_cebe.php?id={$subgrupo['dc_subgrupo_cebe']}' id='del_{$subgrupo['dc_subgrupo_cebe']}'><img src='images/delbtn.png' alt='X' /> Eliminar subgrupo</a>";
				}
				
				//se muestran los detalles del subgrupo actual y sus botones de edición y agregación.
				echo("
				<li class='closed'>
				<span class='folder'>{$subgrupo['dg_grupo_cebe']}</span>
				<div class='tooltip-left'>
				<h2>{$subgrupo['dg_grupo_cebe']}</h2>
				<label>Responsable: </label><strong>{$subgrupo['dg_responsable']}</strong><br />
				<label>Creado por: </label><strong>{$subgrupo['dg_usuario']}</strong><br />
				<label>Fecha creación: </label><strong>{$subgrupo['df_creacion']}</strong>
				<br /><br />
				<div class='tiplinks'>
				<a href='sites/est_empresa/ed_grupo_cebe.php?id={$subgrupo['dc_subgrupo_cebe']}'><img src='images/editbtn.png' alt='E' /> Editar subgrupo</a>
				{$del_btn}
				<a href='sites/est_empresa/cr_cebe.php?id={$subgrupo['dc_subgrupo_cebe']}'><img src='styles/images/file.gif' alt='' /> Crear Centro beneficio</a>
				</div>
				</div>");
				
				//se muestran los centros de beneficio asignados al subgrupo actual
				echo("<ul id='folder_{$subgrupo['dc_subgrupo_cebe']}'>");
					foreach($subcebes as $subcebe){
						echo("
						<li>
						<span class='file'>{$subcebe['dg_cebe']}</span>
						<div class='tooltip-left'>
						<h2>{$subcebe['dg_cebe']}</h2>
						<label>Responsable: </label><strong>{$subcebe['dg_responsable']}</strong><br />
						<label>Centro Costo: </label><strong>{$subcebe['dg_ceco']}</strong><br />
						<label>Término validez: </label><strong>{$subcebe['df_termino']}</strong><br />
						<label>Creado por: </label><strong>{$subcebe['dg_usuario']}</strong><br />
						<label>Fecha Creación: </label><strong>{$subcebe['df_creacion']}</strong>
						<br /><br />
						<div class='tiplinks'>
						<a href='sites/est_empresa/ed_cebe.php?id={$subcebe['dc_cebe']}'><img src='images/editbtn.png' alt='E' /> Editar Centro Beneficio</a>
						<a href='sites/est_empresa/del_cebe.php?id={$subcebe['dc_cebe']}'><img src='images/delbtn.png' alt='X' /> Eliminar Centro Beneficio</a>
						</div></div>
						</li>");
					}
				echo("</ul>");
				echo("</li>");
		}
		echo("</ul>");
		echo("</li>");
	}
?>
</ul>
<br />
<a href="sites/est_empresa/cr_grupo_cebe.php" class="onOverlay addbtn">Crear grupo</a>

</div></div>

<script type="text/javascript" src="jscripts/jquery.treeview.js"></script>
<script type="text/javascript" src="jscripts/jquery.dragndrop.js"></script>
<script type="text/javascript" src="jscripts/est_empresa.js"></script>