<?php
/**
*	Formulario de eliminación de centro beneficio
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$cebe =& $_POST['id'];

$datos = $db->select("tb_cebe","dg_cebe,df_termino,dg_responsable",
"dc_cebe = '{$cebe}' AND dc_empresa = {$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showWarning("No se encontró el centro beneficio especificado");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Centro de Beneficio</div>

<div class="panes">
	<form action="sites/est_empresa/proc/delete_cebe.php" class="validar" id="del_cebe">
	<fieldset>
	<div class="alert">
		¿Está seguro que quiere eliminar el centro beneficio <strong><?=$datos['dg_cebe'] ?></strong>
	</div>
	<div class="center">
		<input type="hidden" name="cebe_id" value="<?=$cebe ?>" />
		<input type="submit" class="delbtn" value="Eliminar" />
		<input type="button" class="button" value="Cancelar" onclick="$('#genOverlay').remove();" />
	</div>
	</fieldset>
	</form>
	<div id="del_cebe_res"></div>
</div>