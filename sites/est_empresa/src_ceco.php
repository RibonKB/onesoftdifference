<?php
/**
*	Muestra la estructura de los centro de costo de la empresa y permite modificarla
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//datos de la empresa
$datos_empresa = $db->select("tb_empresa","dg_rut,dg_razon","dc_empresa={$empresa}");
$datos_empresa= $datos_empresa[0];

//subgrupos, para excluirlos a la hora se obtener los grupos.
$subgr_ceco = $db->select("tb_subgrupo_ceco","dc_subgrupo_ceco");
$subgrps = "''";

//da el formato de lista a los subgrupos separados por coma
foreach($subgr_ceco as $sg){
	$subgrps .= ",{$sg['dc_subgrupo_ceco']}";
}
unset($subgr_ceco);

//obtiene todos los grupos excluyendo los que sean subgrupos
$gr_ceco = $db->select(
"tb_grupo_ceco g,tb_usuario u",
"dc_grupo_ceco,dg_grupo_ceco,dg_responsable,g.df_creacion,dg_usuario",
"g.dc_empresa = {$empresa} AND g.dm_activo = '1' AND dc_usuario_creacion = dc_usuario AND dc_grupo_ceco NOT IN ({$subgrps})");
unset($subgrps);
?>
<link type="text/css" href="styles/treeview.css" rel="stylesheet" />
<div id="secc_bar">Estructura Centro de Costo</div>

<div id="main_cont">
<div style="margin-left:70px;">
<div style="border:1px solid #000;background:#FFF793;width:250px;padding:10px;">
<label>RS: </label><?php echo($datos_empresa['dg_razon']); ?><br />
<label>RUT: </label><?php echo($datos_empresa['dg_rut']); ?><br />
</div>
<ul class="filetree">
<?php
	//para cada grupo
	foreach($gr_ceco as $grupo){
		//se obtienen los centros de costo del grupo
		$cecos = $db->select(
		"tb_ceco c,tb_segmento s,tb_usuario u",
		"dc_ceco,dg_ceco,df_termino,dg_responsable,c.df_creacion,dg_segmento,dg_usuario",
		"c.dc_segmento = s.dc_segmento AND u.dc_usuario = c.dc_usuario_creacion AND c.dm_activo = '1' AND dc_grupo_ceco = {$grupo['dc_grupo_ceco']}");
		
		//se obtienen los subgrupos del grupo
		$subgrupos = $db->select(
		"tb_grupo_ceco gc,tb_subgrupo_ceco sg,tb_usuario u",
		"dc_subgrupo_ceco,dg_grupo_ceco,dg_responsable,gc.df_creacion,dg_usuario",
		"gc.dc_grupo_ceco = sg.dc_subgrupo_ceco AND u.dc_usuario = gc.dc_usuario_creacion AND sg.dc_grupo_ceco = {$grupo['dc_grupo_ceco']} AND gc.dm_activo = '1'");
		
		//el boton de eliminación del grupo se muestra solo en el caso en que el grupo no posee ningun subgrupo o centro de costo asociado
		$del_btn = "";
		if(!count($cecos) && !count($subgrupos)){
			$del_btn = "<a href='sites/est_empresa/del_grupo.php?id={$grupo['dc_grupo_ceco']}' id='del_{$grupo['dc_grupo_ceco']}'><img src='images/delbtn.png' alt='X' /> Eliminar Grupo</a>";
		}
		
		//Se muestran los detalles del grupo actual y los botones de edición y agregación
		echo("
		<li class='closed'>
		<span class='folder'>{$grupo['dg_grupo_ceco']}</span>
		<div class='tooltip-left'>
		<h2>{$grupo['dg_grupo_ceco']}</h2>
		<label>Responsable: </label><strong>{$grupo['dg_responsable']}</strong><br />
		<label>Creado por: </label><strong>{$grupo['dg_usuario']}</strong><br />
		<label>Fecha creación: </label><strong>{$grupo['df_creacion']}</strong>
		<br /><br />
		<div class='tiplinks'>
		<a href='sites/est_empresa/ed_grupo.php?id={$grupo['dc_grupo_ceco']}'><img src='images/editbtn.png' alt='E' /> Editar grupo</a>
		{$del_btn}
		<a href='sites/est_empresa/cr_ceco.php?id={$grupo['dc_grupo_ceco']}'>
		<img src='styles/images/file.gif' alt='' /> Crear Centro costo</a>
		<a href='sites/est_empresa/cr_subgrupo.php?id={$grupo['dc_grupo_ceco']}'>
		<img src='styles/images/folder.gif' alt='' /> Crear Subgrupo</a>
		</div>
		</div>");
		
		echo("<ul id='folder_{$grupo['dc_grupo_ceco']}'>");
		
		//si hay centro de costo o subgrupos se muestran
			foreach($cecos as $ceco){
				echo("
				<li>
				<span class='file'>{$ceco['dg_ceco']}</span>
				<div class='tooltip-left'>
				<h2>{$ceco['dg_ceco']}</h2>
				<label>Responsable: </label><strong>{$ceco['dg_responsable']}</strong><br />
				<label>Segmento: </label><strong>{$ceco['dg_segmento']}</strong><br />
				<label>Término validez: </label><strong>{$ceco['df_termino']}</strong><br />
				<label>Creado por: </label><strong>{$ceco['dg_usuario']}</strong><br />
				<label>Fecha Creación: </label><strong>{$ceco['df_creacion']}</strong>
				<br /><br />
				<div class='tiplinks'>
				<a href='sites/est_empresa/ed_ceco.php?id={$ceco['dc_ceco']}'><img src='images/editbtn.png' alt='E' /> Editar Centro Costo</a>
				<a href='sites/est_empresa/del_ceco.php?id={$ceco['dc_ceco']}'><img src='images/delbtn.png' alt='X' /> Eliminar Centro Costo</a>
				</div></div>
				</li>");
			}
			
			//los subgrupos del grupo
			foreach($subgrupos as $subgrupo){
			
				//se obtienen los centros de costo del subgrupo
				$subcecos = $db->select(
				"tb_ceco c,tb_segmento s,tb_usuario u",
				"dc_ceco,dg_ceco,df_termino,dg_responsable,c.df_creacion,dg_segmento,dg_usuario",
				"c.dc_segmento = s.dc_segmento AND u.dc_usuario = c.dc_usuario_creacion AND c.dm_activo = '1' AND dc_grupo_ceco = {$subgrupo['dc_subgrupo_ceco']}"
				);
				
				//el boton para eliminar subgrupos solo se muestra en caso de que el subgrupo no tenga ningún centro de costo asociado
				$del_btn = "";
				if(!count($subcecos)){
					$del_btn = "<a href='sites/est_empresa/del_grupo.php?id={$subgrupo['dc_subgrupo_ceco']}' id='del_{$subgrupo['dc_subgrupo_ceco']}'><img src='images/delbtn.png' alt='X' />Eliminar subgrupo</a>";
				}
				
				//Se muestran los detalles del subgrupo y los botones de edición y agregación
				echo("
				<li class='closed'>
				<span class='folder'>{$subgrupo['dg_grupo_ceco']}</span>
				<div class='tooltip-left'>
				<h2>{$subgrupo['dg_grupo_ceco']}</h2>
				<label>Responsable: </label><strong>{$subgrupo['dg_responsable']}</strong><br />
				<label>Creado por: </label><strong>{$subgrupo['dg_usuario']}</strong><br />
				<label>Fecha creación: </label><strong>{$subgrupo['df_creacion']}</strong>
				<br /><br />
				<div class='tiplinks'>
				<a href='sites/est_empresa/ed_grupo.php?id={$subgrupo['dc_subgrupo_ceco']}'><img src='images/editbtn.png' alt='E' /> Editar subgrupo</a>
				{$del_btn}
				<a href='sites/est_empresa/cr_ceco.php?id={$subgrupo['dc_subgrupo_ceco']}'><img src='styles/images/file.gif' alt='' /> Crear Centro costo</a>
				</div>
				</div>");
				
				//Se muetran los centros de costo asociados al subgrupo actual
				echo("<ul id='folder_{$subgrupo['dc_subgrupo_ceco']}'>");
					foreach($subcecos as $subceco){
						echo("
						<li>
						<span class='file'>{$subceco['dg_ceco']}</span>
						<div class='tooltip-left'>
						<h2>{$subceco['dg_ceco']}</h2>
						<label>Responsable: </label><strong>{$subceco['dg_responsable']}</strong><br />
						<label>Segmento: </label><strong>{$subceco['dg_segmento']}</strong><br />
						<label>Término validez: </label><strong>{$subceco['df_termino']}</strong><br />
						<label>Creado por: </label><strong>{$subceco['dg_usuario']}</strong><br />
						<label>Fecha Creación: </label><strong>{$subceco['df_creacion']}</strong>
						<br /><br />
						<div class='tiplinks'>
						<a href='sites/est_empresa/ed_ceco.php?id={$subceco['dc_ceco']}'><img src='images/editbtn.png' alt='E' /> Editar Centro Costo</a>
						<a href='sites/est_empresa/del_ceco.php?id={$subceco['dc_ceco']}'><img src='images/delbtn.png' alt='X' />Eliminar Centro costo</a>
						</div></div>
						</li>");
					}
				echo("</ul>");
				echo("</li>");
		}
		echo("</ul>");
		echo("</li>");
	}
?>
</ul>
<br />
<a href="sites/est_empresa/cr_grupo.php" class="onOverlay addbtn">Crear grupo</a>

</div></div>

<script type="text/javascript" src="jscripts/jquery.treeview.js"></script>
<script type="text/javascript" src="jscripts/est_empresa.js"></script>