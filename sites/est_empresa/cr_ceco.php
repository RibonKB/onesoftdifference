<?php
/**
*	Formulario de creación de un nuevo centro beneficio
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div class="secc_bar">Creación Centro de Costo</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Header("Indique los datos para la creación del centro costo<br />(Los datos marcados con [*] son obligatorios)");
	$form->Start("sites/est_empresa/proc/crear_ceco.php","cr_ceco");
	$form->Section();
	$form->Text("Nombre","ceco_name",1);
	$form->Date("Término","ceco_fin");
	$form->EndSection();
	$form->Section();
	$form->Listado("Segmento","ceco_segm","tb_segmento",array("dc_segmento","dg_segmento"),1);
	$form->Text("Responsable","ceco_resp");
	$form->EndSection();
	$form->Hidden("ceco_group",$_POST['id']);
	$form->End("Crear","addbtn");
?>
</div>