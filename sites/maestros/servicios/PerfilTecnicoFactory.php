<?php

require_once('sites/maestros/MaestrosFactory.class.php');

/**
 * Description of PerfilTecnicoFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 11-07-2013
 */
class PerfilTecnicoFactory extends MaestrosFactory {

  protected $idField = 'dc_perfil';
  protected $title = "Perfiles Técnicos";
  private $dc_perfil_tecnico;

  public function crearAction() {

    $perfiles = $this->getPerfilesActivos();

    echo $this->getMaestrosView('crear.form', true, array(
        'tableHeader' => array(
            'dg_perfil' => 'Perfil Técnico',
            'dm_es_supervisor' => 'CPerfil Supervisor'
        ),
        'items' => $perfiles,
        'buttons' => array(
            parent::BUTTON_EDIT => 'editar',
            parent::BUTTON_DELETE => 'eliminar'
        )
    ));
  }

  public function procesarCrearAction() {

    $this->validarInsercion();

    $this->getConnection()->start_transaction();
      $this->insertarPerfil();
      $this->insertarTecnicos();
      $this->insertarAreasServicio();
    $this->getConnection()->commit();

    $this->getErrorMan()->showConfirm('Se ha creado el perfil técnico correctamente');
  }

  private function validarInsercion() {
    $r = self::getRequest();
    if (!isset($r->dc_funcionario)) {
      $this->getErrorMan()->showWarning('Indique al menos un funcionario para asignarlo al perfil técnico');
      exit;
    }

    if (!isset($r->dc_area_servicio)) {
      $this->getErrorMan()->showWarning('Indique al menos un area de servicio afectada por el perfil técnico');
      exit;
    }
  }

  public function editarAction() {
    
  }

  public function eliminarAction() {
    
  }

  private function getPerfilesActivos() {
    $db = $this->getConnection();

    $items = $db->prepare($db->select('tb_perfil_tecnico', 'dc_perfil, dg_perfil, dm_es_supervisor, df_creacion', 'dc_empresa = ? AND dm_activo = 1'));
    $items->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($items);

    $perfiles = array();
    while ($i = $items->fetch(PDO::FETCH_OBJ)) {
      $i->dm_es_supervisor = $i->dm_es_supervisor == 1 ? 'SI' : 'NO';
      $perfiles[] = $i;
    }

    return $perfiles;
  }

  private function insertarPerfil() {
    $request = self::getRequest();
    $db = $this->getConnection();

    $insertar = $db->prepare($db->insert('tb_perfil_tecnico', array(
                'dg_perfil' => '?',
                'dm_es_supervisor' => '?',
                'dc_empresa' => $this->getEmpresa(),
                'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
                'df_creacion' => $db->getNow(),
    )));
    $insertar->bindValue(1, $request->dg_perfil, PDO::PARAM_STR);
    $insertar->bindValue(2, isset($request->dm_es_supervisor), PDO::PARAM_BOOL);
    $db->stExec($insertar);

    $this->dc_perfil_tecnico = $db->lastInsertId();
  }

  private function insertarTecnicos() {
    $db = $this->getConnection();
    $request = self::getRequest();

    $insertar = $db->prepare($db->insert('tb_perfil_funcionario', array(
                'dc_perfil_tecnico' => $this->dc_perfil_tecnico,
                'dc_tecnico' => '?'
    )));
    $insertar->bindParam(1, $dc_tecnico, PDO::PARAM_INT);

    foreach ($request->dc_funcionario as $dc_tecnico):
      $db->stExec($insertar);
    endforeach;
  }

  private function insertarAreasServicio() {
    $db = $this->getConnection();
    $request = self::getRequest();

    $insertar = $db->prepare($db->insert('tb_perfil_tecnico_area_servicio', array(
                'dc_perfil_tecnico' => $this->dc_perfil_tecnico,
                'dc_area_servicio' => '?'
    )));
    $insertar->bindParam(1, $dc_area_servicio, PDO::PARAM_INT);

    foreach ($request->dc_area_servicio as $dc_area_servicio):
      $db->stExec($insertar);
    endforeach;
  }

}

?>
