<?php

require_once('sites/maestros/MaestrosFactory.class.php');

/**
 * Description of AreaServicioFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 15-07-2013
 */
class AreaServicioFactory extends MaestrosFactory {
  
  protected $idField = 'dc_area_servicio';
  protected $title = "Áreas de Servicio";
  
  private $area = null;

  public function crearAction() {
      $areas = $this->getAreasServicioActivas();
      
      echo $this->getMaestrosView('crear.form', true, array(
        'tableHeader' => array(
            'dg_area_servicio' => 'Área de Servicio',
            'dg_correos' => 'Correos'
        ),
        'items' => $areas,
        'buttons' => array(
            parent::BUTTON_EDIT => 'editar',
            parent::BUTTON_DELETE => 'eliminar',
            'tecnico.png' => 'asignarTecnicos',
            'setupbtn.png' => 'perfilTecnico'
        )
    ));
  }
  
  public function procesarCrearAction(){
      $this->getConnection()->start_transaction();
        $this->insertarAreaServicio();
      $this->getConnection()->commit();
      
      $this->getErrorMan()->showConfirm("Se ha creado el area de servicio <strong>".self::getRequest()->dg_area_servicio."</strong> correctamente");
  }

  public function editarAction() {
      
  }

  public function eliminarAction() {
      
  }
  
  public function asignarTecnicosAction(){
      $this->area = $this->getAreaServicioById(self::getRequest()->dc_area_servicio);
      $this->title = "Asignar técnicos área de servicio ".$this->area->dg_area_servicio;
      
      if($this->area === false){
        $this->getErrorMan()->showWarning('No se ha encontrado el área de servicio especificada, compruebe los datos de entrada y vuelva a intentarlo');
      }
      
      $form = $this->getFormView($this->getTemplateURL('asignar_tecnicos.form'), array(
          'area' => $this->area,
          'tecnicos' => $this->getTecnicosArea()
      ), true);
      
      echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
      
  }
  
  public function procesarAsignarTecnicosAction(){
    $request = self::getRequest();
    
    $this->area = $this->getAreaServicioById($request->dc_area_servicio);
    
    $this->getConnection()->start_transaction();
      $this->deleteTecnicosArea();
      if(isset($request->dc_tecnico))
        $this->insertarTecnicosArea($request->dc_tecnico);
    $this->getConnection()->commit();
    
    $this->asignarTecnicosAction();
  }
  
  public function perfilTecnicoAction(){
      $this->area = $this->getAreaServicioById(self::getRequest()->dc_area_servicio);
      $tecnicos = $this->getTecnicosArea();
      $tipos_falla = $this->getTiposFallaArea();
      $this->title = "Perfil Técnico";
      
      $form = $this->getFormView($this->getTemplateURL('perfil_tecnico.form'), array(
          'area' => $this->area,
          'tecnicos' => $tecnicos[1],
          'tipos_falla' => $tipos_falla,
          'perfiles_activos' => $this->getPerfilTecnicoArea()
      ));
      
      echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
  }
  
  public function procesarPerfilTecnicoAction(){
      $r = self::getRequest();
      $this->area = $this->getAreaServicioById($r->dc_area_servicio);
      
      $this->getConnection()->start_transaction();
        $this->deletePerfilTecnico();
        if(isset($r->dc_tecnico_tipo_falla))
          $this->insertarPerfilTecnico($r->dc_tecnico_tipo_falla);
      $this->getConnection()->commit();
    
      $this->perfilTecnicoAction();
  }
  
  private function getAreasServicioActivas(){
      $db = $this->getConnection();
      
      $items = $db->prepare($db->select('tb_area_servicio', '*', 'dc_empresa = ? AND dm_activo = 1'));
      $items->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
      $db->stExec($items);
      
      return $items->fetchAll(PDO::FETCH_OBJ);
  }
  
  private function insertarAreaServicio(){
      $db = $this->getConnection();
      $r = self::getRequest();
      
      $insertar = $db->prepare($db->insert('tb_area_servicio', array(
          "dg_area_servicio" => '?',
          "dg_correos" => '?',
          "dc_empresa" => $this->getEmpresa(),
          "dc_usuario_creacion" => $this->getUserData()->dc_usuario,
          "df_creacion" => $db->getNow()
      )));
      $insertar->bindValue(1, $r->dg_area_servicio, PDO::PARAM_STR);
      $insertar->bindValue(2, $r->dg_email, PDO::PARAM_STR);
      
      $db->stExec($insertar);
      
  }
  
  private function getAreaServicioById($dc_area_servicio){
    if($this->area instanceof stdClass){
      return $this->area;
    }
    return $this->getConnection()->getRowById('tb_area_servicio', $dc_area_servicio, 'dc_area_servicio');
  }
  
  private function getTecnicosArea(){
    
    $db = $this->getConnection();
    
    $tecnicos = $db->prepare(
                  $db->select(
                          'tb_area_servicio_tecnico t
                           JOIN tb_funcionario f ON f.dc_funcionario = t.dc_tecnico',
                          't.dc_tecnico, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno',
                          'dc_area_servicio = ?'));
    $tecnicos->bindValue(1, $this->area->dc_area_servicio, PDO::PARAM_INT);
    $db->stExec($tecnicos);
    
    $ret = array();
    $ret2 = array();
    while($t = $tecnicos->fetch(PDO::FETCH_OBJ)):
      $ret[] = $t->dc_tecnico;
      $ret2[] = $t;
    endwhile;
    
    return array($ret, $ret2);
    
  }
  
  private function getTiposFallaArea(){
      $db = $this->getConnection();
      
      $data = $db->prepare($db->select('tb_tipo_falla_os', '*', 'dc_area_servicio = ?'));
      $data->bindValue(1, $this->area->dc_area_servicio, PDO::PARAM_INT);
      $db->stExec($data);
      
      return $data->fetchAll(PDO::FETCH_OBJ);
      
  }
  
  private function deleteTecnicosArea(){
    $db = $this->getConnection();
    
    $delete = $db->prepare("DELETE FROM tb_area_servicio_tecnico WHERE dc_area_servicio = ?");
    $delete->bindValue(1, $this->area->dc_area_servicio, PDO::PARAM_INT);
    $db->stExec($delete);
    
  }
  
  private function insertarTecnicosArea(&$tecnicos){
    $db = $this->getConnection();
    
    $insertar = $db->prepare($db->insert('tb_area_servicio_tecnico', array(
        'dc_area_servicio' => '?',
        'dc_tecnico' => '?'
    )));
    $insertar->bindValue(1, $this->area->dc_area_servicio, PDO::PARAM_INT);
    $insertar->bindParam(2, $dc_tecnico, PDO::PARAM_INT);
    
    foreach($tecnicos as $dc_tecnico){
      $db->stExec($insertar);
    }
  }
  
  private function getPerfilTecnicoArea(){
    $db = $this->getConnection();
    
    $data = $db->prepare($db->select('tb_perfil_tecnico', 'dc_tecnico, dc_tipo_falla', 'dc_area_servicio = ?'));
    $data->bindValue(1, $this->area->dc_area_servicio, PDO::PARAM_INT);
    $db->stExec($data);
    
    $perfiles = array();
    while($perfil = $data->fetch(PDO::FETCH_OBJ)){
      $perfiles[] = $perfil->dc_tecnico . '_' . $perfil->dc_tipo_falla;
    }
    
    return $perfiles;
    
  }
  
  private function deletePerfilTecnico(){
    $db = $this->getConnection();
    
    $delete = $db->prepare("DELETE FROM tb_perfil_tecnico WHERE dc_area_servicio = ?");
    $delete->bindValue(1, $this->area->dc_area_servicio, PDO::PARAM_INT);
    $db->stExec($delete);
    
  }
  
  private function insertarPerfilTecnico(&$perfil_tecnico){
      $db = $this->getConnection();
      
      $insertar = $db->prepare($db->insert('tb_perfil_tecnico', array(
          'dc_area_servicio' => '?',
          'dc_tecnico' => '?',
          'dc_tipo_falla' => '?'
      )));
      $insertar->bindValue(1, $this->area->dc_area_servicio, PDO::PARAM_INT);
      $insertar->bindParam(2, $dc_tecnico, PDO::PARAM_INT);
      $insertar->bindParam(3, $dc_tipo_falla, PDO::PARAM_INT);
      
      foreach($perfil_tecnico as $perfil){
        $perfil = explode('_', $perfil);
        $dc_tecnico = $perfil[0];
        $dc_tipo_falla = $perfil[1];
        
        $db->stExec($insertar);
      }
  }

}

?>
