<?php
//require_once('../../inc/Factory.class.php');

abstract class MaestrosFactory extends Factory{
	
	const templateFolder = 'maestros';
	const mainTemplateFile = 'baseCreacion';
	
	const BUTTON_EDIT = 'editbtn.png';
	const BUTTON_DELETE = 'delbtn.png';
	
	protected function getMaestrosView($formUrl, $withDB = false, $param = array()){
		$form = $this->getFormView($this->getTemplateURL($formUrl), $param, $withDB);
		
		return $this->getFullView(self::templateFolder.'/'.self::mainTemplateFile,array(
			'form' => $form,
			'tableHeader' => $param['tableHeader'],
			'items' => $param['items'],
			'buttons' => $param['buttons']
		));
		
	}
	
	protected function actionURL($action, $params = array()){
		return self::buildActionUrl($action, $params);
	}
	
	public abstract function crearAction();
	
	public abstract function editarAction();
	
	public abstract function eliminarAction();
	
}
?>