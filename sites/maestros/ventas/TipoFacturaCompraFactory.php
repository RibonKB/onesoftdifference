<?php
require_once('sites/maestros/MaestrosFactory.class.php');
//require_once('../../../inc/db-class.php');
//require_once('../../../inc/Factory.class.php');

class TipoFacturaCompraFactory extends MaestrosFactory{

	protected $idField = 'dc_tipo';
	protected $title = "Tipos de Factura de Compra";

	public function crearAction(){

		$items = $this->obtenerTipos();

		echo $this->getMaestrosView('crear.form',true,array(
			'tableHeader' => array(
					'dg_tipo' => 'Tipo de Factura',
					'dm_factura_manual' => 'Tipo uso'
				),
			'items' => $items,
			'buttons' => array(
					parent::BUTTON_EDIT => 'editar',
					parent::BUTTON_DELETE => 'eliminar',
					'addbtn.png' => 'asignarCuentasContables'
				)
		));

	}

	public function procesarCrearAction(){
		$data = self::getRequest();

		if($this->getByName($data->dg_tipo) !== false){
			$this->getErrorMan()->showWarning('Ya existe un tipo de factura de compra con el nombre especificado');
			exit;
		}

		$db = $this->getConnection();

		$tipo = $db->prepare($db->insert('tb_tipo_factura_compra',array(
			'dg_tipo' => '?',
			'dm_factura_manual' => '?',
			'df_creacion' => $db->getNow(),
			'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
			'dc_empresa' => $this->getEmpresa()
		)));
		$tipo->bindValue(1,$data->dg_tipo,PDO::PARAM_STR);
		$tipo->bindValue(2,$data->dm_factura_manual,PDO::PARAM_STR);
		$db->stExec($tipo);

		$this->getErrorMan()->showConfirm('Se ha creado el tipo de factura de compra correctamente');

	}

	private function getTipo($dc_tipo){
		$db = $this->getConnection();

		$tipo = $db->getRowById('tb_tipo_factura_compra',intval($dc_tipo),'dc_tipo');

		if($tipo === false){
			$this->getErrorMan()->showWArning('No se encontró el tipo de factura especificado, compruebe los datos de entrada y vuelva a intentarlo.');
			exit;
		}

		return $tipo;
	}

	public function editarAction(){
		$r = self::getRequest();

		$tipo = $this->getTipo($r->dc_tipo);

		if($tipo->dm_factura_manual == 1):
			$cuentas = count($this->getCuentasContablesTipo($tipo->dc_tipo)->fetchAll()) > 0;
		else:
			$cuentas = false;
		endif;

		$form = $this->getFormView($this->getTemplateURL('editar.form'),array(
			'tipo' => $tipo,
			'cuentas' => $cuentas
		),true);

		echo $this->getOverlayView($form, array(), self::STRING_TEMPLATE);
	}

	public function procesarEditarAction(){
		$db = $this->getConnection();
		$r = self::getRequest();

		$tipo = $this->getTipo($r->dc_tipo);

		$update = $db->prepare($db->update('tb_tipo_factura_compra',array(
			'dg_tipo' => '?',
			'dm_factura_manual' => '?'
		),'dc_tipo = ?'));
		$update->bindValue(1,$r->ed_dg_tipo,PDO::PARAM_STR);
		$update->bindValue(2,$r->ed_dm_factura_manual,PDO::PARAM_INT);
		$update->bindValue(3,$tipo->dc_tipo,PDO::PARAM_INT);
		$db->stExec($update);

		$this->getErrorMan()->showConfirm("Los datos del tipo de factura han sido actualizados correctamente.");

	}

	public function eliminarAction(){

	}

	public function asignarCuentasContablesAction(){
		$db = $this->getConnection();
		$dc_tipo = self::getRequest()->dc_tipo;

		$tipo = $db->getRowById('tb_tipo_factura_compra',$dc_tipo,'dc_tipo');

		if($tipo === false){
			$this->getErrorMan()->showWArning('No se encontró el tipo de factura especificado, compruebe los datos de entrada y vuelva a intentarlo.');
			exit;
		}

		if($tipo->dm_factura_manual == 0){
			$this->getErrorMan()->showWArning('Esta opción solo está disponible para facturas que solo permiten ingreso manual');
			exit;
		}

		$this->title = "Cuentas contables asignadas al tipo de factura <b>{$tipo->dg_tipo}</b>";

		$cuentas = $this->getCuentasContablesTipo($dc_tipo);

		$form = $this->getFormView($this->getTemplateURL('asignarCuentaContable.form'),array(
			'tipo' => $tipo,
			'list' => $cuentas
		),true);

		echo $this->getOverlayView($form, array(), self::STRING_TEMPLATE);

	}

	private function getCuentasContablesTipo($dc_tipo){
		$db = $this->getConnection();
		$cuentas = $db->prepare(
						$db->select('tb_tipo_factura_compra_cuenta_contable t
							JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = t.dc_cuenta_contable',
						'c.dc_cuenta_contable, c.dg_codigo, c.dg_cuenta_contable',
						't.dc_tipo_factura = ?'));
		$cuentas->bindValue(1,$dc_tipo,PDO::PARAM_INT);
		$db->stExec($cuentas);

		return $cuentas;
	}

	public function procesarAsignarCuentaContableAction(){
		$data = self::getRequest();
		$db = $this->getConnection();

		$exists = $db->prepare($db->select('tb_tipo_factura_compra_cuenta_contable','true','dc_tipo_factura = ? AND dc_cuenta_contable = ?'));
		$exists->bindValue(1,$data->dc_tipo,PDO::PARAM_INT);
		$exists->bindValue(2,$data->dc_cuenta_contable,PDO::PARAM_INT);
		$db->stExec($exists);

		if($exists->fetch() !== false){
			$this->getErrorMan()->showAviso('La Cuenta contable seleccionada ya ha sido asignada anteriormente al tipo de factura');
			exit;
		}

		$setCuenta = $db->prepare($db->insert('tb_tipo_factura_compra_cuenta_contable',array(
			'dc_tipo_factura' => '?',
			'dc_cuenta_contable' => '?'
		)));
		$setCuenta->bindValue(1,$data->dc_tipo,PDO::PARAM_INT);
		$setCuenta->bindValue(2,$data->dc_cuenta_contable,PDO::PARAM_INT);
		$db->stExec($setCuenta);

		$this->getErrorMan()->showConfirm('Se ha agregado la cuenta contable correctamente al tipo de factura');

	}

	public function eliminarCuentaContableAction(){
		$data = self::getRequest();
		$db = $this->getConnection();

		$delete = $db->prepare('DELETE FROM tb_tipo_factura_compra_cuenta_contable WHERE dc_tipo_factura = ? AND dc_cuenta_contable = ?');
		$delete->bindValue(1,$data->dc_tipo,PDO::PARAM_INT);
		$delete->bindValue(2,$data->dc_cuenta_contable,PDO::PARAM_INT);
		$db->stExec($delete);

		//$this->getErrorMan()->showAviso('Se ha eliminado la cuenta contable del tipo de factura');
		$this->asignarCuentasContablesAction();
	}

	private function obtenerTipos(){
		$db = $this->getConnection();

		$list = $db->prepare(
					$db->select(
						'tb_tipo_factura_compra',
						'dc_tipo,dg_tipo,df_creacion,dm_factura_manual',
						'dc_empresa = ? AND dm_activo = 1',
						array('order_by' => 'dg_tipo')));
		$list->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($list);

		$tipos = array();

		while($t = $list->fetch(PDO::FETCH_OBJ)){
			$t->dm_factura_manual = $t->dm_factura_manual==1?'Solo Factura Manual':'General';
			$tipos[] = $t;
		}

		return $tipos;

	}

	private function getByName($name){
		$db = $this->getConnection();

		$item = $db->prepare($db->select('tb_tipo_factura_compra','dc_tipo','dg_tipo = ? AND dc_empresa = ?'));
		$item->bindValue(1,$name,PDO::PARAM_STR);
		$item->bindValue(2,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($item);

		return $item->fetch(PDO::FETCH_OBJ);
	}

}
