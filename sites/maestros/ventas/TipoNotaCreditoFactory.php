<?php

require_once('sites/maestros/MaestrosFactory.class.php');

/**
 * Description of TipoNotaCreditoFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 19-08-2013
 */
class TipoNotaCreditoFactory extends MaestrosFactory {

  protected $idField = 'dc_tipo_nota_credito';
  protected $title = 'Tipos Nota de Crédito';
  private $tipo, $cuentas;

  public function crearAction() {
    $items = $this->obtenerTipos();

    echo $this->getMaestrosView('crear.form',true,array(
			'tableHeader' => array(
					'dg_tipo_nota_credito' => 'Tipo de Nota de Crédito',
					'dm_retorna_stock' => 'Retorna Stock',
                    //'dm_retorna_libre' => 'Retorna Stock Libre',
                    'dm_retorna_anticipado' => 'Facturación Anticipada',
                    'dg_bodega' => 'Bodega Entrada'
				),
			'items' => $items,
			'buttons' => array(
					parent::BUTTON_EDIT => 'editar',
					parent::BUTTON_DELETE => 'eliminar',
          'doc.png' => 'asignarCuentasContables'
				)
		));
  }

  public function procesarCrearAction(){
    $this->validarEntrada();

    $this->getConnection()->start_transaction();
      $this->insertarTipoNotaCredito();
    $this->getConnection()->commit();

    $this->getErrorMan()->showConfirm('El tipo de Nota de Crédito ha sido creado correctamente');
  }

  public function editarAction() {

  }

  public function eliminarAction() {

  }

  public function asignarCuentasContablesAction(){
    $this->initAsignarCuentasForm();

    $form = $this->getFormView($this->getTemplateURL('asignarCuentas.form'),array(
      'tipo' => $this->tipo,
      'cuentas' => $this->cuentas
    ));

    echo $this->getOverlayView($form,array(),Factory::STRING_TEMPLATE);
  }

  public function procesarAsignarCuentasContablesAction(){
    $this->initProcesarAsignarCuentas();

    $this->getConnection()->start_transaction();
      $this->insertarCuentaContable();
    $this->getConnection()->commit();

    $this->getErrorMan()->showConfirm('Se ha agregado la cuenta contable correctamente al tipo de Nota de Crédito <strong>'
                                      .$this->tipo->dg_tipo_nota_credito.'</strong>');
  }

  private function initProcesarAsignarCuentas(){
    $r = self::getRequest();

    $this->initTipoNotaCredito($r, true);
    $this->initCuentasContables();
    $this->initCuentaSeleccionada($r);
    $this->validarCuentaExistente();

  }

  private function insertarCuentaContable(){
    $db = $this->getConnection();

    $insert = $db->prepare($db->insert('tb_tipo_nota_credito_cuenta_contable',array(
      'dc_tipo_nota_credito' => '?',
      'dc_cuenta_contable' => '?'
    )));
    $insert->bindValue(1, $this->tipo->dc_tipo_nota_credito, PDO::PARAM_INT);
    $insert->bindValue(2, $this->cuenta->dc_cuenta_contable, PDO::PARAM_INT);

    $db->stExec($insert);
  }

  private function initAsignarCuentasForm(){
    $r = self::getRequest();

    $this->initTipoNotaCredito($r, true);
    $this->initCuentasContables();
  }

  private function initCuentasContables(){
    $db = $this->getConnection();

    $select = $db->prepare(
                  $db->select('tb_tipo_nota_credito_cuenta_contable t
                  JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = t.dc_cuenta_contable',
                  'cc.*',
                  't.dc_tipo_nota_credito = ?'));
    $select->bindValue(1,$this->tipo->dc_tipo_nota_credito,PDO::PARAM_INT);
    $db->stExec($select);

    $this->cuentas = $select->fetchAll(PDO::FETCH_OBJ);
  }

  private function validarCuentaExistente(){

    foreach($this->cuentas as $c):
      if($c->dc_cuenta_contable == $this->cuenta->dc_cuenta_contable):
        $this->getErrorMan()->showWarning('La cuenta contable indicada ya ha sido agregada anteriormente al tipo de nota de Crédito,'.
                                          ' verifique los datos de entrada y vuelva a intentarlo.');
        exit;
      endif;
    endforeach;

  }

  private function initCuentaSeleccionada($r){

    if(!isset($r->dc_cuenta_contable) or !preg_match('/^\d{1,11}$/',$r->dc_cuenta_contable)):
      $this->getErrorMan()->showWarning('La cuenta contable a asignar no fue indicada o esta posee un ofrmato inválido,'.
                                        ' verifique los datos de entrada y vuelva a intentarlo.');
      exit;
    endif;

    $db = $this->getConnection();
    $this->cuenta = $db->getRowById('tb_cuenta_contable',$r->dc_cuenta_contable,'dc_cuenta_contable');

    if($this->cuenta === false):
      $this->getErrorMan()->showWarning('La cuenta contable seleccionada no fue encontrada o esta fue eliminada,'.
                                        ' verifique los datos de entrada y vuelva a intentarlo.');
      exit;
    endif;

  }

  private function initTipoNotaCredito($r, $noStock = false){

    if(!isset($r->dc_tipo_nota_credito) or !preg_match('/^\d{1,11}$/',$r->dc_tipo_nota_credito)):
      $this->getErrorMan()->showWarning("No se ha especificado un tipo de Nota de Crédito o esta posee un formato inválido,".
                                        " verifique los datos de entrada  vuelva a intentarlo.");
      exit;
    endif;

    $db = $this->getConnection();
    $this->tipo = $db->getRowById('tb_tipo_nota_credito',$r->dc_tipo_nota_credito,'dc_tipo_nota_credito');

    if($noStock):
      if($this->tipo->dm_retorna_stock == 1):
        $this->getErrorMan()->showWarning("Esta operación solo está disponible para Tipos de Nota de Crédito que no retornan stock.".
                                          "<br />Verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;
    endif;

  }

  private function validarEntrada(){
    $r = self::getRequest();

    if($r->dm_retorna_stock == 1 and $r->dc_bodega_entrada == 0):
      $this->getErrorMan()->showWarning('Debe indicar una bodega de entrada si selecciona que retornará stock');
      exit;
    endif;

    if($r->dm_retorna_anticipado == 1 and $r->dm_retorna_stock == 0):
      $this->getErrorMan()->showWarning('Si selecciona procesar facturas anticipadas debe seleccionar retorno de stock');
      exit;
    endif;

  }

  private function insertarTipoNotaCredito(){
    $r = self::getRequest();
    $db = $this->getConnection();

    $insertar = $db->prepare($db->insert('tb_tipo_nota_credito', array(
        'dg_tipo_nota_credito' => '?',
        'dm_retorna_stock' => '?',
        'dm_retorna_anticipado' => '?',
        'dc_bodega_entrada' => '?',
        'dc_empresa' => $this->getEmpresa(),
        'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
        'df_creacion' => $db->getNow(),
    )));
    $insertar->bindValue(1, $r->dg_tipo_nota_credito, PDO::PARAM_STR);
    $insertar->bindValue(2, $r->dm_retorna_stock, PDO::PARAM_INT);
    $insertar->bindValue(3, $r->dm_retorna_anticipado, PDO::PARAM_INT);
    $insertar->bindValue(4, $r->dc_bodega_entrada, PDO::PARAM_INT);

    $db->stExec($insertar);
  }

  private function obtenerTipos(){
    $db = $this->getConnection();

    $list = $db->prepare($db->select('tb_tipo_nota_credito t
      LEFT JOIN tb_bodega b ON b.dc_bodega = t.dc_bodega_entrada',
    't.*, b.dg_bodega', 't.dc_empresa = ?'));
    $list->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($list);

    $tipos = array();

    while($t = $list->fetch(PDO::FETCH_OBJ)):
      $t->dm_retorna_stock = $t->dm_retorna_stock==1?'SI':'NO';
      $t->dm_retorna_libre = $t->dm_retorna_libre==1?'SI':'NO';
      $t->dm_retorna_anticipado = $t->dm_retorna_anticipado==1?'SI':'NO';

      $tipos[] = $t;
    endwhile;

    return $tipos;

  }

}

?>
