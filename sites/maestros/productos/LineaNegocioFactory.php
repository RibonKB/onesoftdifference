<?php
require_once('sites/maestros/MaestrosFactory.class.php');
//require_once('../../../inc/db-class.php');

class LineaNegocioFactory extends MaestrosFactory{

	protected $idField = 'dc_linea_negocio';
	protected $title = "Lineas de Negocio";

	public function crearAction(){

		$db = $this->getConnection();

		$items = $db->prepare(
					$db->select('tb_linea_negocio ln
						LEFT JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = ln.dc_cuenta_contable_compra
						LEFT JOIN tb_cuenta_contable cv ON cv.dc_cuenta_contable = ln.dc_cuenta_contable_venta
                        LEFT JOIN tb_cuenta_contable cp ON cp.dc_cuenta_contable = ln.dc_cuenta_contable_provision',
					'ln.dc_linea_negocio, ln.dg_linea_negocio, ln.df_creacion,
					 cc.dg_cuenta_contable dg_cuenta_compra, cv.dg_cuenta_contable dg_cuenta_venta,
                     cp.dg_cuenta_contable dg_cuenta_provision',
					'ln.dc_empresa = ? AND ln.dm_activo = 1'
					,array('order_by' => 'ln.dg_linea_negocio')));
		$items->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($items);

		echo $this->getMaestrosView('crear.form',true,array(
			'tableHeader' => array(
					'dg_linea_negocio' => 'Linea de Negocio',
					'dg_cuenta_compra' => 'Cuenta Contable Compras',
					'dg_cuenta_venta' => 'Cuenta Contable Ventas',
                    'dg_cuenta_provision' => 'Cuenta Contable Provisión'
				),
			'items' => $items->fetchAll(PDO::FETCH_OBJ),
			'buttons' => array(
					parent::BUTTON_EDIT => 'editar',
					parent::BUTTON_DELETE => 'eliminar',
					'inventario.png' => 'asignarBodegaCuentaContable'
				)
		));

	}

	public function procesarCrearAction(){

		$data = self::getRequest();

		if($this->getByName($data->dg_linea_negocio) !== false){
			$this->getErrorMan()->showWarning('Ya existe una linea de negocio con el nombre especificado');
			exit;
		}

		$db = $this->getConnection();

		$linea = $db->prepare($db->insert('tb_linea_negocio',array(
			'dg_linea_negocio' => '?',
			'dc_cuenta_contable_compra' => '?',
			'dc_cuenta_contable_venta' => '?',
      'dc_cuenta_contable_provision' => '?',
			'dc_cuenta_contable_margenes' =>'?',
			'df_creacion' => $db->getNow(),
			'dc_empresa' => $this->getEmpresa(),
			'dc_usuario_creacion' => $this->getUserData()->dc_usuario
		)));
		$linea->bindValue(1,$data->dg_linea_negocio,PDO::PARAM_STR);
		$linea->bindValue(2,$data->dc_cuenta_compras,PDO::PARAM_INT);
		$linea->bindValue(3,$data->dc_cuenta_ventas,PDO::PARAM_INT);
    $linea->bindValue(4,$data->dc_cuenta_provision,PDO::PARAM_INT);
		$linea->bindValue(5,$data->dc_cuenta_margenes,PDO::PARAM_INT);
		$db->stExec($linea);

		$this->getErrorMan()->showConfirm('Se ha creado la linea de negocio correctamente');

	}

	public function editarAction(){
		$dc_linea_negocio = self::getRequest()->dc_linea_negocio;
		$db = $this->getConnection();

		$data = $db->prepare(
			$db->select(
				'tb_linea_negocio',
				'dc_linea_negocio, dg_linea_negocio, dc_cuenta_contable_compra, dc_cuenta_contable_venta, dc_cuenta_contable_provision, dc_cuenta_contable_margenes',
				'dc_linea_negocio = ? AND dc_empresa = ?'));
		$data->bindValue(1,$dc_linea_negocio,PDO::PARAM_INT);
		$data->bindValue(2,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($data);
		$data = $data->fetch(PDO::FETCH_OBJ);

		if($data === false){
			$this->getErrorMan()->showWarning('No se ha encontrado la linea de negocio, compruebe los datos de entrada y vuelva a intentarlo');
			exit;
		}

		$this->title = "Editar Linea de Negocio <b>'".$data->dg_linea_negocio."'</b>";

		$form = $this->getFormView($this->getTemplateURL('edit.form'),array(
			'linea_negocio' => $data
		),true);

		echo $this->getOverlayView($form, array(), self::STRING_TEMPLATE);

	}

	public function procesarEditarAction(){

		$data = self::getRequest();

		$exists = $this->getByName($data->dg_linea_negocio_ed);

		if($exists !== false){
			if($exists->dc_linea_negocio != $data->dc_linea_negocio){
				$this->getErrorMan()->showWarning('Ya existe una linea de negocio con el nombre especificado');
			}
		}

		$db = $this->getConnection();

		$linea = $db->prepare($db->update('tb_linea_negocio',array(
			'dg_linea_negocio' => '?',
			'dc_cuenta_contable_compra' => '?',
			'dc_cuenta_contable_venta' => '?',
      'dc_cuenta_contable_provision' => '?',
			'dc_cuenta_contable_margenes' =>'?',
		),'dc_linea_negocio = ?'));
		$linea->bindValue(1,$data->dg_linea_negocio_ed,PDO::PARAM_STR);
		$linea->bindValue(2,$data->dc_cuenta_compras_ed,PDO::PARAM_INT);
		$linea->bindValue(3,$data->dc_cuenta_ventas_ed,PDO::PARAM_INT);
    $linea->bindValue(4,$data->dc_cuenta_provision_ed,PDO::PARAM_INT);
		$linea->bindValue(5,$data->dc_cuenta_margenes_ed,PDO::PARAM_INT);
		$linea->bindValue(6,$data->dc_linea_negocio,PDO::PARAM_INT);
		$db->stExec($linea);

		$this->getErrorMan()->showConfirm('Se han actualizado los datos de la linea de negocio correctamente');

	}

	public function eliminarAction(){

	}

	public function procesarEliminarAction(){

	}

	public function asignarBodegaCuentaContableAction(){

		$db = $this->getConnection();
		$dc_linea_negocio = self::getRequest()->dc_linea_negocio;

		$linea_negocio = $db->getRowById('tb_linea_negocio',$dc_linea_negocio,'dc_linea_negocio');

		if($linea_negocio === false){
			$this->getErrorMan()->showWArning('No se encontró la linea de negocio especificada, compruebe los datos de entrada y vuelva a intentarlo.');
			exit;
		}

		$list = $db->prepare(
					$db->select('tb_linea_negocio_cuenta_contable ln
						JOIN tb_bodega b ON b.dc_bodega = ln.dc_bodega
						JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = ln.dc_cuenta_contable',
					'b.dg_bodega, c.dg_codigo, c.dg_cuenta_contable',
					'ln.dc_linea_negocio = ?'));
		$list->bindValue(1,$dc_linea_negocio,PDO::PARAM_INT);
		$db->stExec($list);

		$this->title = "Cuenta Contable Bodega para línea de negocio <b>'{$linea_negocio->dg_linea_negocio}'</b>";

		$form = $this->getFormView($this->getTemplateURL('asignarBodegaCuentaContable.form'),array(
			'linea_negocio' => $linea_negocio,
			'list' => $list
		),true);

		echo $this->getOverlayView($form, array(), self::STRING_TEMPLATE);

	}

	public function procesarAsignarBodegaCuentaContableAction(){

		$data = self::getRequest();
		$db = $this->getConnection();

		$exists = $db->prepare($db->select('tb_linea_negocio_cuenta_contable','true','dc_linea_negocio = ? AND dc_bodega = ?'));
		$exists->bindValue(1,$data->dc_linea_negocio,PDO::PARAM_INT);
		$exists->bindValue(2,$data->dc_bodega,PDO::PARAM_INT);
		$db->stExec($exists);
		$exists = $exists->fetch();

		if($exists === false){
			$setCuenta = $db->prepare($db->insert('tb_linea_negocio_cuenta_contable',array(
				'dc_cuenta_contable' => '?',
				'dc_linea_negocio' => '?',
				'dc_bodega' => '?'
			)));
		}else{
			$setCuenta = $db->prepare($db->update('tb_linea_negocio_cuenta_contable',array(
				'dc_cuenta_contable' => '?'
			),'dc_linea_negocio = ? AND dc_bodega = ?'));
		}

		$setCuenta->bindValue(1,$data->dc_cuenta_contable,PDO::PARAM_INT);
		$setCuenta->bindValue(2,$data->dc_linea_negocio,PDO::PARAM_INT);
		$setCuenta->bindValue(3,$data->dc_bodega,PDO::PARAM_INT);

		$db->stExec($setCuenta);

		if($exists === false){
			$this->getErrorMan()->showConfirm('Se ha creado la relación correctamente');
		}else{
			$this->getErrorMan()->showConfirm('Se ha actualizado la relación correctamente');
		}

	}

	private function getByName($name){
		$db = $this->getConnection();

		$item = $db->prepare($db->select('tb_linea_negocio','dc_linea_negocio','dg_linea_negocio = ? AND dc_empresa = ?'));
		$item->bindValue(1,$name,PDO::PARAM_STR);
		$item->bindValue(2,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($item);

		return $item->fetch(PDO::FETCH_OBJ);

	}

}
