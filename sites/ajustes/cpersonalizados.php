<?php
/**
*
**/

// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
Gestor de campos personalizados
</div>

<div id="main_cont">

<hr />
<div class="info">
A continuaci&oacute;n se muestra una lista de todos los menus donde est&aacute;n permitidos los campos personalizados.
</div>
<hr />
<select id="menus_selector">
	<option value="0" >-- Seleccione el filtro de men&uacute;s a mostrar --</option>
	<option value="1">Creaci&oacute;n de Clientes</option>
	<option value="2">Creaci&oacute;n de Productos</option>
	<option value="3">ETC...</option>
</select>
<br />

<!-- Para el menu de creacion de clientes -->
<div id="cp_menus_1" style="display:none;">
	<div class="title">Creaci&oacute;n de clientes</div>
	<fieldset>
		<legend>Lista de campos personalizados</legend>
		<label>- No hay campos personalizados asignados a este men&uacute;</label>
		<hr />
		<input type="button" class="addbtn" value="Agregar un campo personalizado nuevo" />
	</fieldset>
</div>
<!-- END: creacion clientes -->

<!-- Para el menu de creación de productos -->
<div id="cp_menus_2" style="display:none;">
	<div class="title">Creaci&oacute;n de productos</div>
	<fieldset>
		<legend>Lista de campos personalizados</legend>
		<table class="tab" width="100%">
		<thead>
			<tr>
				<th width="16" scope="col">&nbsp;&#9660;</th>
				<th width="140" scope="col">Nombre del campo</th>
				<th scope="col">Descripci&oacute;n</th>
				<th width="80" scope="col">tipo</th>
				<th width="60" scope="col">Requerido</th>
				<th width="60">Opciones</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><input type="checkbox" name="campo[]" value="id" /></td>
				<td><label>Serie ISO-390909932</label></td>
				<td><label>Indica la serie comprometida en el acta n&uacute;mero #42342342 de la asdf qwerty</label></td>
				<td><label>texto</label></td>
				<td>SI</td>
				<td>
				<img src="images/editbtn.png" alt="editar" title="Editar" />
				<img src="images/delbtn.png" alt="eliminar" title="Eliminar" />
				</td>
			</tr>
			
			<tr>
				<td><input type="checkbox" name="campo[]" value="id" /></td>
				<td><label>Marca agua</label></td>
				<td><label>Blurp! blarp blerp sarintaron peri core</label></td>
				<td><label>checkbox</label></td>
				<td>NO</td>
				<td>
				<img src="images/editbtn.png" alt="editar" title="Editar" />
				<img src="images/delbtn.png" alt="eliminar" title="Eliminar" />
				</td>
			</tr>
		</tbody>
		</table>
		<hr />
		<input type="button" class="addbtn" value="Agregar un campo personalizado nuevo" />
	</fieldset>
</div>
<!-- END: Creacion clientes -->

<!-- un tercer menu -->
<div id="cp_menus_3" style="display:none;">
uno que otro menu =D
</div>
<!-- END:tercer menu -->

<input type="button" class="delbtn" value="Borrar seleccionados" style="display:none;" />

</div>
<script type="text/javascript">
var cp_activo = '0';
	$("#menus_selector").change(function(){
		if(cp_activo != '0'){
			$("#cp_menus_"+cp_activo).slideUp(500);
		}
		cp_activo = $(this).val();
		if(cp_activo != '0'){
			$("#cp_menus_"+$(this).val()).slideDown(500);
			$(".delbtn").show();
		}else{
			$(".delbtn").hide();
		}
		
	});
</script>