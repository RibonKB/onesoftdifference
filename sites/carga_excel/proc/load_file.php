<?php
define("MAIN",1);
include("../../../inc/global.php");
	if(!$_FILES['load_file']['size']){
		$error_man->showWarning("<b>El archivo entregado es inválido</b><br />Indique uno nuevo o contacte con el administrador");
		exit();
	}
	
	$ext = substr($_FILES['load_file']['name'],strrpos($_FILES['load_file']['name'],"."));
	
	if(!in_array($ext,array('.xls','.xlsx'))){
		$error_man->showWarning("<b>El formato de archivo especificado es incorrecto</b><br />
		Los formatos soportados son .xls y .xlsx");
		exit();
	}
	
	if(!move_uploaded_file($_FILES['prod_foto']['tmp_name'],"../fotos/prod_{$p_id}{$p_ext}")){
		$error_man->showWarning("Error al intentar copiar el archivo al servidor, pruebe nuevamente.");
		exit();
	}
	
	
	
?>