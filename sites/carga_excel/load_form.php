<?php
define('MAIN',1);
include_once("../../inc/global.php");
include_once("../../inc/form-class.php");

echo('<div id="secc_bar">Ingreso masivo comprobantes contables</div>
<div id="main_cont">
<div class="panes center">');

$form = new Form($empresa);

$form->Start("sites/carga_excel/proc/load_file.php","load_excel","form","method='post' enctype='multipart/form-data'");
$form->Header('Cargue el archivo excel con los comprobantes en el formato definido<br />
<b>Procure no incluir más información de la necesaria en los archivos para no retrasar el proceso de carga</b>');
$form->File('<br />Archivo (XLS o XLSX)','load_file');
$form->End("Cargar");
echo('
<iframe name="load_excel_file" id="load_excel_file" class="hidden"></iframe>
</div></div>');

?>
<script type="text/javascript">
$("#load_excel").attr("target","load_excel_file");
$("#load_excel").submit(function(e){
	$("#load_excel_res").html("<img src='images/ajax-loader.gif' alt='' /> Cargando datos de archivo excel en el servidor");
});
$("#load_excel_file").load(function(){
	res = frames['load_excel_file'].document.getElementsByTagName("body")[0].innerHTML;
	$("#load_excel_res").html(res);
});
</script>