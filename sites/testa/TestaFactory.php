<?php

class TestaFactory extends Factory {
	
	public function indexAction(){
		$db = $this->getConnection();
		$notaVenta = $db->doQuery($db->select('tb_nota_venta_detalle', '*', 'dc_nota_venta = 8285 AND dm_tipo = 1'));
		$notaVenta = $notaVenta->fetchAll(PDO::FETCH_OBJ);
		
		$factura = $db->prepare($db->update('tb_factura_venta_detalle', array('dc_cantidad' => '?'),'dc_detalle_nota_venta = ?'));
		$factura->bindParam(1, $cantidad, PDO::PARAM_INT);
		$factura->bindParam(2, $dc_nota_venta, PDO::PARAM_INT);
		
		$db->start_transaction();
		foreach($notaVenta as $v){
			$cantidad = $v->dq_cantidad;
			$dc_nota_venta = $v->dc_nota_venta_detalle;
			$db->stExec($factura);
		}
		$factura = $db->doQuery($db->select('tb_factura_venta_detalle', '*', 'dc_factura = 10460'));;
		Factory::debug($factura->fetchAll(PDO::FETCH_OBJ));
		$db->commit();
		
	}
	
}