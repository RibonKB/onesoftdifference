<?php
class EstadisticasIndividualesFactory extends Factory{

	private $funcionario;
	protected $aporteNV = 8000;
	protected $aporteFV = 8000;
	protected $aporteAnual = 8000;

	public function reloadDataAction(){
		$this->initFuncionario();
		//self::debug($this->getClientesAsignados());
		
		echo $this->getView($this->getTemplateURL('resultados'),array(
			'op_activas' => $this->getOportunidadesActivas(),
			'cl_activos' => $this->getClientesActivos(),
			'cl_asignados' => $this->getClientesAsignados(),
			'porcentaje_cierre' => $this->getPorcentajeCierre(),
			'aporte_nv' => $this->getAporteMensual(),
			'aporte_fv' => $this->getAporteFacturas(),
			'promedio_anual' => $this->getPromedioAnual(),
			'clientes_mes' => $this->getClientesMes()
		));
	}
	
	private function initFuncionario(){
		$r = self::getRequest();
		$db = $this->getConnection();
		if(isset($r->id)){
			$this->funcionario = $db->getRowById('tb_usuario',$r->id,'dc_funcionario');
		}else{
			$this->funcionario = $this->getUserData();
		}
	}
	
	private function getOportunidadesActivas(){
		$db = $this->getConnection();
		
		$oportunidades = $db->prepare(
							$db->select('tb_oportunidad op
							JOIN tb_oportunidad_avance a ON a.dc_oportunidad = op.dc_oportunidad
							JOIN tb_estados e ON op.dm_estado = e.dc_estado',
							'COUNT(DISTINCT op.dc_oportunidad)',
							'op.dc_ejecutivo = ? AND e.dm_requiere = 0 AND a.df_creacion > DATE_SUB(NOW(), INTERVAL 1 MONTH)')
						 );
		$oportunidades->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($oportunidades);
		
		return $oportunidades->fetchColumn();
						 
	}
	
	private function getClientesActivos(){
		$db = $this->getConnection();
		$clientes = $db->prepare(
						$db->select(
							'tb_oportunidad op',
							'count(DISTINCT op.dc_cliente)',
							'op.dc_ejecutivo = ? AND op.df_fecha_emision > DATE_SUB(NOW(), INTERVAL 1 MONTH)'
						));
		$clientes->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($clientes);
		
		return $clientes->fetchColumn();
		
	}
	
	private function getClientesAsignados(){
		$db = $this->getConnection();
		$clientes = $db->prepare($db->select('tb_cliente','count(*)','dc_ejecutivo = ?'));
		$clientes->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($clientes);
		
		return $clientes->fetchColumn();
	}
	
	private function getPorcentajeCierre(){
		$db = $this->getConnection();
		
		$total = $db->prepare($db->select(
						'tb_oportunidad',
						'COUNT(*)',
						'dc_ejecutivo=? AND df_fecha_emision > date_sub(NOW(), INTERVAL 1 MONTH)'));
		$total->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($total);
		
		$cerradas = $db->prepare(
							$db->select('tb_oportunidad op
							JOIN tb_estados e ON op.dm_estado = e.dc_estado',
							'COUNT(*)',
							'op.dc_ejecutivo = ? AND e.dm_cierre_correcto = 1 AND op.df_fecha_emision > DATE_SUB(NOW(), INTERVAL 1 MONTH)')
						 );
		$cerradas->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($cerradas);
		
		$cerradas = $cerradas->fetchColumn();
		$total = $total->fetchColumn();
		
		if($total == 0):
			return 0;
		endif;
		
		return round(($cerradas/$total)*100,2);
		
	}
	
	private function getAporteMensual(){
		$db = $this->getConnection();
		
		$notas = $db->prepare($db->select(
					'tb_nota_venta nv
					 JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta',
					'SUM((d.dq_precio_venta-d.dq_precio_compra)*d.dq_cantidad)',
					'nv.dc_ejecutivo = ? AND
					 MONTH(nv.df_fecha_emision) = MONTH(NOW()) AND
					 YEAR(nv.df_fecha_emision) = YEAR(NOW()) AND
					 d.dm_tipo = 0'));
		$notas->bindValue(1,$this->funcionario->dc_funcionario);
		$db->stExec($notas);
		
		return $this->descomponerTipoCambio($notas->fetchColumn());
		
	}
	
	private function descomponerTipoCambio($valor){
		$db = $this->getConnection();
		$tipos = $db->prepare($db->select('tb_tipo_cambio','dg_tipo_cambio, dq_cambio, dn_cantidad_decimales','dc_empresa = ? AND dm_principal = 1'));
		$tipos->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($tipos);
		
		$res = array();
		while($r = $tipos->fetch(PDO::FETCH_OBJ)):
			$res[] = array($r->dg_tipo_cambio, round($valor/$r->dq_cambio,$r->dn_cantidad_decimales));
		endwhile;
		
		return $res;
	}
	
	private function getAporteFacturas(){
		$db = $this->getConnection();
		
		$facturas = $db->prepare($db->select(
						'tb_factura_venta',
						'SUM(dq_margen)',
						'dm_nula = 0 AND
						 dc_ejecutivo = ? AND
						 MONTH(df_emision) = MONTH(NOW()) AND
						 YEAR(df_emision) = YEAR(NOW())'));
		$facturas->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($facturas);
		
		return $this->descomponerTipoCambio($facturas->fetchColumn());
		
	}
	
	private function getPromedioAnual(){
		$db = $this->getConnection();
		
		$mes = date('n')-1;
		
		if($mes == 0){
			return $this->descomponerTipoCambio(0);
		}
		
		$data = $db->prepare($db->select(
				'tb_factura_venta',
				'SUM(dq_margen)',
				'dm_nula = 0
				 AND dc_ejecutivo = ?
				 AND df_emision >= DATE_FORMAT(NOW(),"%Y-01-01")
				 AND df_emision < DATE_FORMAT(NOW(),"%Y-%m-01")'
		));
		$data->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($data);
		
		return $this->descomponerTipoCambio($data->fetchColumn()/$mes);
		
	}
	
	private function getClientesMes(){
		$db = $this->getConnection();
		
		$data = $db->prepare($db->select(
						'tb_cliente cl
						 LEFT JOIN tb_oportunidad op ON op.dc_cliente = cl.dc_cliente
						 LEFT JOIN tb_cotizacion cot ON cot.dc_cliente = cl.dc_cliente',
						'COUNT(DISTINCT cl.dc_cliente)',
						'cl.dc_ejecutivo = ?
						 AND MONTH(cl.df_creacion) = MONTH(NOW())
						 AND YEAR(cl.df_creacion) = YEAR(NOW())
						 AND (
							op.dc_oportunidad IS NOT NULL
							OR cot.dc_cotizacion IS NOT NULL
						 )',
						 array(
							'group_by' => 'cl.dc_cliente'
						 )));
		$data->bindValue(1,$this->funcionario->dc_funcionario,PDO::PARAM_INT);
		$db->stExec($data);
		
		return intval($data->fetchColumn());
		
		
	}
	
}
?>