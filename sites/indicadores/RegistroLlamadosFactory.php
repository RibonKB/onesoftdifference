<?php

class RegistroLlamadosFactory extends Factory{

	private $caller = null;
	private $local_prefix = array('9','92','922');
	private $mobile_prefix = array('909','99');
	private $minDuration = 30;

	public function reloadDataAction(){
		$data = $this->getCallData($this->getCallerId());
		echo $this->getView($this->getTemplateURL('resultados'), array(
			'data' => $data,
			'caller' => $this->caller
		));
	}
	
	public function showCallInfoAction(){
		echo 'asdasdasd';
	}
	
	private function getCallData($dc_funcionario){
		$db = $this->getConnection();
		$this->caller = $db->getRowById('tb_funcionario',$dc_funcionario,'dc_funcionario');
		$clientes = $this->getCallerClientes($this->caller->dc_funcionario);
		
		return array(
			$clientes,
			$this->getCallerCalls($clientes),
			$this->getBaseConocimiento($clientes)
		);
	}
	
	private function getCallerId(){
		$r = self::getRequest();
		if(isset($r->id)){
			return (int)$r->id;
		}else{
			return $this->getUserData()->dc_funcionario;
		}
	}
	
	private function getCallerClientes($dc_funcionario){
		$db = $this->getConnection();
		$clientes = $db->prepare(
						$db->select('tb_cliente cl'.
									' JOIN tb_contacto_cliente c ON c.dc_cliente = cl.dc_cliente',
									'cl.dc_cliente, cl.dg_razon, c.dg_fono, c.dg_movil',
									'cl.dc_ejecutivo = ? AND cl.dm_activo = 1'));
		$clientes->bindValue(1,$dc_funcionario,PDO::PARAM_INT);
		$db->stExec($clientes);
		
		$group = array();
		
		foreach($clientes->fetchAll(PDO::FETCH_OBJ) as $c):
			if(!isset($group[$c->dc_cliente])):
				$group[$c->dc_cliente] = array();
			endif;
			
			$c->dg_fono = preg_replace("/[^0-9]/","",$c->dg_fono);
			$c->dg_movil = preg_replace("/[^0-9]/","",$c->dg_movil);
			$group[$c->dc_cliente][] = $c;
			
		endforeach;
		
		return $group;
	}
	
	private function getCallerCalls($clientes){
		$db = $this->getConnection();
		$calls = array();
		
		foreach($clientes as $i => $cliente):
			$numeros = $this->getNumerosCliente($cliente);
			$like = substr(str_repeat(',?',count($numeros)),1);
			
			if(!count($numeros)){
				$calls[$i] = 'NO';
				continue;
			}
			
			$data = $db->prepare($db->select('cdr','dst, DATEDIFF(NOW(), calldate) ultima_llamada',"src = ? AND billsec > {$this->minDuration} AND dst IN ({$like})",array(
				'order_by' => 'calldate DESC',
				'limit' => '1'
			)));
			$data->bindValue(1,$this->caller->dg_anexo,PDO::PARAM_STR);
			
			for($j = 0; $j < count($numeros); $j++):
				$data->bindValue($j+2,$numeros[$j],PDO::PARAM_STR);
			endfor;
			
			$db->stExec($data);
			
			$calls[$i] = $data->fetch(PDO::FETCH_OBJ);
			
		endforeach;
		
		return $calls;
	}
	
	private function getNumerosCliente($cliente){
		$numeros = array();
		foreach($cliente as $c): //Recorrer todos los contactos
			if($c->dg_fono):
				foreach($this->local_prefix as $p):
					$numeros[] = $p.$c->dg_fono;
				endforeach;
			endif;
			
			if($c->dg_movil):
				foreach($this->mobile_prefix as $p):
					$numeros[] = $p.$c->dg_movil;
				endforeach;
			endif;
			
		endforeach;
		return $numeros;
	}
	
	private function getBaseConocimiento($clientes){
		
		$total = $this->getTotalBaseConocimiento();
		$res = array();
		
		foreach($clientes as $dc_cliente => $cliente){
			$val = $this->getBaseConocimientoCliente($dc_cliente);
			if($val >= $total):
				$res[$dc_cliente] = 'confirm';
			elseif($val > 1):
				$res[$dc_cliente] = 'alert';
			else:
				$res[$dc_cliente] = 'warning';
			endif;
		}
		
		return $res;
		
	}
	
	private function getTotalBaseConocimiento(){
		$db = $this->getConnection();
		
		$data = $db->prepare($db->select('tb_campo_personalizado','count(*)','dc_empresa = ? AND dm_activo = 1'));
		$data->bindValue(1,$this->getUserData()->dc_empresa);
		$db->stExec($data);
		
		return (int)$data->fetchColumn();
	}
	
	private function getBaseConocimientoCliente($dc_cliente){
		$db = $this->getConnection();
		
		$data = $db->prepare(
					$db->select('tb_cliente_campo_personalizado c
								 JOIN tb_campo_personalizado p ON p.dc_campo = c.dc_campo',
								'COUNT(*)',
								'c.dc_cliente = ? AND p.dm_activo = 1 AND c.dg_valor <> ""'));
		$data->bindValue(1,$dc_cliente,PDO::PARAM_INT);
		$db->stExec($data);
		
		return $data->fetchColumn();
	}
	
}







