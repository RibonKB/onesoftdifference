<?php
$ruta_dia = $db->select('tb_ruta r
JOIN tb_ruta_detalle d ON d.dc_ruta = r.dc_ruta AND d.dm_activo = 1
JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
JOIN tb_nota_venta nv ON nv.dc_nota_venta = nvd.dc_nota_venta
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
JOIN tb_producto p ON p.dc_producto = nvd.dc_producto',
'r.dq_ruta,d.dq_cantidad,d.dt_hora,p.dg_codigo,nvd.dg_descripcion dg_producto,nv.dq_nota_venta,nv.dc_nota_venta,cl.dg_razon,r.dc_tipo_ruta',
"r.dm_nula = 0 AND DATE(r.df_ruta) = CURDATE() AND nv.dm_nula = 0",array('orderby' => 'd.dt_hora ASC'));

$ruta_pendiente = $db->select('tb_nota_venta nv
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
JOIN tb_nota_venta_detalle d ON nv.dc_nota_venta = d.dc_nota_venta
JOIN tb_producto p ON p.dc_producto = d.dc_producto
JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
'nv.dq_nota_venta, cl.dc_cliente, cl.dg_razon, p.dg_codigo, d.dg_descripcion dg_producto, (d.dq_cantidad-d.dc_rutada) dq_cantidad, nv.df_fecha_emision',
'd.dc_rutada < d.dq_cantidad AND nv.dm_nula = 0 AND nv.df_fecha_emision > DATE_SUB(NOW(), INTERVAL 3 MONTH) AND t.dm_controla_inventario = 0 AND nv.dc_empresa = '.$empresa,
array('orderby'=>'cl.dc_cliente'));

$tipos_ruta = $db->select('tb_tipo_ruta','dc_tipo, dg_tipo, dt_rango_inicio, dt_rango_fin',"dc_empresa = {$empresa}",array('order_by' => 'dt_rango_inicio ASC'));

?>

<link href="styles/ruta_calendar.css" type="text/css" rel="stylesheet" />
<div id="calendar_navegating_days">
<span class="right">&#9658;</span>
<span class="left">&#9668;</span>
Rutas: <strong id="date_to_route">HOY</strong>
</div>
<br class="clear" />
<div id="ruta_calendar">
<?php foreach($tipos_ruta as $t): ?>
<div class="calendar_hour_item" id="tipo<?php echo $t['dc_tipo'] ?>">
	<span>
		<b><?php echo $t['dg_tipo'] ?></b>
		de <b><?php echo substr($t['dt_rango_inicio'],0,5) ?></b>
		a <b><?php echo substr($t['dt_rango_fin'],0,5) ?></b>
	</span>
</div>
<?php endforeach; ?>
<?php /*foreach(range(0,23) as $h):
	foreach(array('00','30') as $m):
	$hora = str_pad($h, 2, 0, STR_PAD_LEFT).':'.$m;
	?>
	<div class="calendar_hour_item" id="<?php echo str_replace(':','',$hora) ?>">
		<span><?php echo $hora ?></span>
	</div>
<?php endforeach;
endforeach; */?>
</div>

<div id="righterDiv">
<?php if(count($ruta_pendiente)): ?>
	<table class="tab bicolor_tab" width="95%">
	<caption>Productos pendientes de ruta</caption>
	<thead><tr>
		<th>Cliente</th>
		<th>Código</th>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>NV</th>
		<th>Emisión</th>
	</tr></thead>
	<tbody>
	<?php foreach($ruta_pendiente as $d): ?>
	<tr>
		<td title="<?php echo htmlentities(str_replace('"',"''",$d['dg_razon'])) ?>"><?php echo substr($d['dg_razon'],0,15) ?></td>
		<td><?php echo $d['dg_codigo'] ?></td>
		<td title="<?php echo htmlentities(str_replace('"',"''",$d['dg_producto'])) ?>"><?php echo substr($d['dg_producto'],0,15) ?></td>
		<td align="right"><b><?php echo $d['dq_cantidad'] ?></b></td>
		<td><?php echo $d['dq_nota_venta'] ?></td>
		<td><?php echo $db->dateLocalFormat($d['df_fecha_emision']) ?></td>
	</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
<?php else: ?>
	<div class="confirm">Enhorabuena! No hay productos pendientes de enrutar.</div>
	<!-- xD -->
<?php endif; ?>
</div>
<br class="clear" />
<script type="text/javascript" src="jscripts/sites/indicadores/src_indicador_ruta.js?v=0_1a1"></script>
<script type="text/javascript">
	js_data.initRutaData = <?php echo json_encode($ruta_dia) ?>;
	js_data.init();
</script>