<?php

class EstadisticasGrupalesFactory extends Factory{

	private $funcionarios = array();
	private $funcionario = null;
	private $funcionariosLIKE;

	public function reloadDataAction(){
		$this->initFuncionarios();
		$this->initFuncionario();
		
		echo $this->getView($this->getTemplateURL('resultados'),array(
			'facturaData' => $this->getMontoFacturacion(),
			'aporteNV' => $this->getAporteNV(),
			'opActivas' => $this->filtrarUpDown($this->getOportunidadesActivas()),
			'clAsignados' => $this->filtrarUpDown($this->getClientesAsignados()),
			'opPorcentajeCierre' => $this->filtrarUpDown($this->getPorcentajeCierre())
		));
	}
	
	private function initFuncionarios(){
		$db = $this->getConnection();
		
		$data = $db->prepare($db->select(
					'tb_funcionario f
					 JOIN tb_ceco c ON c.dc_ceco = f.dc_ceco',
					'f.dc_funcionario',
					'f.dc_empresa = ? AND c.dm_analisis_estadistico_grupal = 1'));
		$data->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($data);
		
		while($this->funcionarios[] = $data->fetchColumn());
		
		array_pop($this->funcionarios);
		
		$this->funcionariosLIKE = substr(str_repeat(',?',count($this->funcionarios)),1);
		
	}
	
	private function initFuncionario(){
		$r = self::getRequest();
		if(isset($r->id)):
			$this->funcionario = $r->id;
		else:
			$this->funcionario = $this->getUserData()->dc_funcionario;
		endif;
	}
	
	private function getMontoFacturacion(){
		$db = $this->getConnection();
		
		$data = $db->prepare($db->select(
					'tb_factura_venta',
					'dc_ejecutivo, SUM(dq_total) dq_precio, SUM(dq_margen) dq_margen',
					"dm_nula = 0
					 AND dc_ejecutivo IN ({$this->funcionariosLIKE})
					 AND MONTH(df_emision) = MONTH(NOW())
					 AND YEAR(df_emision) = YEAR(NOW())",
					 array('group_by' => 'dc_ejecutivo')
		));
		
		foreach($this->funcionarios as $i => $f):
			$data->bindValue($i+1,$f,PDO::PARAM_INT);
		endforeach;
		
		$db->stExec($data);
		
		$precio = $margen = array();
		
		while($d = $data->fetch(PDO::FETCH_OBJ)):
			$precio[] = array($d->dc_ejecutivo,$d->dq_precio);
			$margen[] = array($d->dc_ejecutivo,$d->dq_margen);
		endwhile;
		
		return array(
			$this->descomponerTresNiveles($precio,true),
			$this->descomponerTresNiveles($margen,true));
		
	}
	
	private function getAporteNV(){
		$db = $this->getConnection();
		
		$data = $db->prepare($db->select(
					'tb_nota_venta nv
					 JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta',
					'nv.dc_ejecutivo, SUM((d.dq_precio_venta-d.dq_precio_compra)*d.dq_cantidad)',
					"nv.dm_nula = 0
					 AND nv.dm_confirmada = 1
					 AND d.dm_tipo = 0
					 AND MONTH(nv.df_fecha_emision) = MONTH(NOW())
					 AND YEAR(nv.df_fecha_emision) = YEAR(NOW())
					 AND nv.dc_ejecutivo IN ({$this->funcionariosLIKE})",
					 array('group_by' => 'nv.dc_ejecutivo')
		));
		
		foreach($this->funcionarios as $i => $f):
			$data->bindValue($i+1,$f,PDO::PARAM_INT);
		endforeach;
		
		$db->stExec($data);
		
		return $this->descomponerTresNiveles($data->fetchAll(PDO::FETCH_NUM),true);
		
	}
	
	private function getOportunidadesActivas(){
		$db = $this->getConnection();
		
		$data = $db->prepare($db->select(
					'tb_oportunidad op
					 JOIN tb_oportunidad_avance a ON a.dc_oportunidad = op.dc_oportunidad
					 JOIN tb_estados e ON e.dc_estado = op.dm_estado',
					'op.dc_ejecutivo, COUNT(DISTINCT op.dc_oportunidad)',
					"e.dm_requiere = 0
					 AND a.df_creacion > DATE_SUB(NOW(),INTERVAL 1 MONTH)
					 AND op.dc_ejecutivo IN ({$this->funcionariosLIKE})",
					array('group_by' => 'op.dc_ejecutivo')
		));
		
		foreach($this->funcionarios as $i => $f):
			$data->bindValue($i+1,$f,PDO::PARAM_INT);
		endforeach;
		
		$db->stExec($data);
		
		return $this->descomponerTresNiveles($data->fetchAll(PDO::FETCH_NUM));
		
	}
	
	private function getClientesAsignados(){
		$db = $this->getConnection();
		
		$data = $db->prepare($db->select(
					'tb_cliente',
					'dc_ejecutivo, COUNT(*)',
					"dc_ejecutivo IN ({$this->funcionariosLIKE})",
					array('group_by' => 'dc_ejecutivo')
		));
		
		foreach($this->funcionarios as $i => $f):
			$data->bindValue($i+1,$f,PDO::PARAM_INT);
		endforeach;
		
		$db->stExec($data);
		
		return $this->descomponerTresNiveles($data->fetchAll(PDO::FETCH_NUM));
		
	}
	
	private function getPorcentajeCierre(){
		$db = $this->getConnection();
		
		//
		$total = $db->prepare($db->select(
						'tb_oportunidad',
						'dc_ejecutivo, COUNT(*)',
						"dc_ejecutivo IN({$this->funcionariosLIKE})
						 AND df_fecha_emision > date_sub(NOW(), INTERVAL 1 MONTH)",
						 array('group_by' => 'dc_ejecutivo')));
		
		foreach($this->funcionarios as $i => $f):
			$total->bindValue($i+1,$f,PDO::PARAM_INT);
		endforeach;
		
		$db->stExec($total);
		
		$total = $this->descomponerTresNiveles($total->fetchAll(PDO::FETCH_NUM));
		
		if($total == array(0,0,0)){
			return $total;
		}
		
		//
		$cerradas = $db->prepare(
							$db->select('tb_oportunidad op
							JOIN tb_estados e ON op.dm_estado = e.dc_estado',
							'op.dc_ejecutivo, COUNT(*)',
							"op.dc_ejecutivo IN({$this->funcionariosLIKE})
							 AND e.dm_cierre_correcto = 1
							 AND op.df_fecha_emision > DATE_SUB(NOW(), INTERVAL 1 MONTH)",
							array('group_by' => 'op.dc_ejecutivo'))
						 );
		
		foreach($this->funcionarios as $i => $f):
			$cerradas->bindValue($i+1,$f,PDO::PARAM_INT);
		endforeach;
		
		$db->stExec($cerradas);
		
		$cerradas = $this->descomponerTresNiveles($cerradas->fetchAll(PDO::FETCH_NUM));
		
		$res = array(0,0,0);
		
		if($total[0] > 0 and $cerradas[0] > 0):
			$res[0] = round(($cerradas[0]/$total[0])*100,2);
		endif;
		
		if($total[1] > 0 and $cerradas[1] > 0):
			$res[1] = round(($cerradas[1]/$total[1])*100,2);
		endif;
		
		if($total[2] > 0 and $cerradas[2] > 0):
			$res[2] = round(($cerradas[2]/$total[2])*100,2);
		endif;
		
		return $res;
	}
	
	private function descomponerTipoCambio($valor){
		$db = $this->getConnection();
		$tipos = $db->prepare($db->select('tb_tipo_cambio','dg_tipo_cambio, dq_cambio, dn_cantidad_decimales','dc_empresa = ? AND dm_principal = 1'));
		$tipos->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($tipos);
		
		$res = array();
		while($r = $tipos->fetch(PDO::FETCH_OBJ)):
			$res[] = array($r->dg_tipo_cambio, round($valor/$r->dq_cambio,$r->dn_cantidad_decimales));
		endwhile;
		
		return $res;
	}
	
	/**
	*	0 => dc_funcionario
	*	1 => dq_valor
	**/
	private function descomponerTresNiveles($data, $descomponerTipoCambio = false){
		$mayor = 0;
		$suma = 0;
		$my_val = 0;
		$muestra = count($data);
		
		if($muestra == 0):
			if($descomponerTipoCambio):
				return array(
					$this->descomponerTipoCambio(0),
					$this->descomponerTipoCambio(0),
					$this->descomponerTipoCambio(0)
				);
			endif;
			
			return array(0,0,0);
		endif;
		
		$data = $this->completarEjecutivos($data);
		
		foreach($data as $d):
			//Filtrar mayor valor
			if($d[1] > $mayor):
				$mayor = $d[1];
			endif;
			
			//Suma para promedio
			$suma += $d[1];
			
			//Comprobar si es la nuestra
			if($d[0] == $this->funcionario):
				$my_val = $d[1];
			endif;
			
		endforeach;
		
		if($descomponerTipoCambio):
			return array(
				$this->descomponerTipoCambio($suma/$muestra),
				$this->descomponerTipoCambio($mayor),
				$this->descomponerTipoCambio($my_val)
			);
		endif;
		
		return array(round($suma/$muestra,0),$mayor,$my_val);
		
	}
	
	private function completarEjecutivos($data){
		$res = array();
		$data = $this->agruparEjecutivos($data);
		foreach($this->funcionarios as $dc_ejecutivo):
			if(isset($data[$dc_ejecutivo])):
				$res[] = array($dc_ejecutivo,$data[$dc_ejecutivo]);
			else:
				$res[] = array($dc_ejecutivo,0);
			endif;
		endforeach;
		
		return $res;
		
	}
	
	private function agruparEjecutivos($data){
		$res = array();
		
		foreach($data as $d):
			$res[$d[0]] = $d[1];
		endforeach;
		
		return $res;
	}
	
	private function filtrarUpDown($data){
		return array(
			$data[0],
			$data[1],
			$data[2],
			$data[0]>$data[2]?'down':'up',
			$data[1]>$data[2]?'down':'up'
		);
	}
	
}
