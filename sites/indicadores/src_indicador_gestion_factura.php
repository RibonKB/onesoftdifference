<?php
$facturas_gestion = $db->select('tb_factura_venta_gestion g
								 JOIN tb_factura_venta fv ON fv.dc_factura = g.dc_factura
								 JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente',
								'g.dg_comentario, fv.dq_factura, fv.dq_folio, fv.dc_factura, cl.dg_razon',
								'DATE(NOW()) = DATE(fv.df_proxima_gestion) AND DATE(NOW()) = DATE(g.df_proxima_gestion) AND fv.dc_empresa = '.$empresa);

$facturas_compromiso = $db->select('tb_factura_venta_gestion g
									JOIN tb_factura_venta fv ON fv.dc_factura = g.dc_factura
									JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente',
									'g.dg_comentario, fv.dq_factura, fv.dq_folio, fv.dc_factura, cl.dg_razon',
									'DATE(NOW()) = DATE(g.df_compromiso) AND fv.dc_empresa = '.$empresa);


?>
<ul class="manual_tab">
	<li><a href="#">Por fecha de compromiso</a></li>
    <li><a href="#">Por fecha de gestión</a></li>
</ul>
<div class="innertabpanes">
<br class="clear" />
<div>
    <!-- Facturas pago -->
    <table class="tab left" width="100%" style="margin:5px;">
        <thead>
            <tr>
                <th colspan="5">
                    <button class="gestionbtn imgbtn left" style="background-image:url(images/management.png)">Gestión</button>
                    <button class="pagobtn imgbtn left" style="background-image:url(images/monedas-icon.png)">Pago</button>
                </th>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th>Factura</th>
                <th>Folio</th>
                <th>Cliente</th>
                <th>Comentario</th>
            </tr>
        </thead>
        <tbody>
            <?php if(count($facturas_compromiso)): ?>
                <?php foreach($facturas_compromiso as $f): ?>
                    <tr>
                        <td><input type="checkbox" name="dc_factura[]" value="<?php echo $f['dc_factura'] ?>" /></td>
                        <td><?php echo $f['dq_factura'] ?></td>
                        <td><?php echo $f['dq_folio'] ?></td>
                        <td><?php echo $f['dg_razon'] ?></td>
                        <td><?php echo $f['dg_comentario'] ?></td>
                    </tr>
                <?php endforeach ?>
            <?php else: ?>
                <tr>
                    <td colspan="5">No hay facturas por gestionar hoy</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div><div>
<!-- Facturas gestion -->
<table class="tab left" width="100%" style="margin:5px;">
    <thead>
    	<tr>
        	<th colspan="5">
            	<button class="gestionbtn imgbtn left" style="background-image:url(images/management.png)">Gestión</button>
                <button class="pagobtn imgbtn left" style="background-image:url(images/monedas-icon.png)">Pago</button>
            </th>
        </tr>
    	<tr>
        	<th>&nbsp;</th>
            <th>Factura</th>
            <th>Folio</th>
            <th>Cliente</th>
            <th>Comentario</th>
        </tr>
    </thead>
    <tbody>
    	<?php if(count($facturas_gestion)): ?>
        	<?php foreach($facturas_gestion as $f): ?>
            	<tr>
                	<td><input type="checkbox" name="dc_factura[]" value="<?php echo $f['dc_factura'] ?>" /></td>
                    <td><?php echo $f['dq_factura'] ?></td>
                    <td><?php echo $f['dq_folio'] ?></td>
                    <td><?php echo $f['dg_razon'] ?></td>
                    <td><?php echo $f['dg_comentario'] ?></td>
                </tr>
            <?php endforeach ?>
        <?php else: ?>
        	<tr>
            	<td colspan="5">No hay facturas por gestionar hoy</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>
</div>
</div>
<script type="text/javascript" src="jscripts/sites/finanzas/cobros/src_informe_cobranza.js?v=0_13b"></script>
<script>
	var gestion_url = 'sites/finanzas/cobros/cr_gestion_cobranza_masiva.php?mode=';
	$('.gestionbtn').click(function(){
		pymerp.loadOverlay(gestion_url+'G',$(this).parents('table').find(':input').serialize());
	});
	$('.pagobtn').click(function(){
		pymerp.loadOverlay(gestion_url+'P',$(this).parents('table').find(':input').serialize());
	});
	js_data.separadorMiles = '<?php echo $empresa_conf['dm_separador_miles'] ?>';
	js_data.separadorDecimal = '<?php echo $empresa_conf['dm_separador_decimal'] ?>';
</script>