<?php
$ind_cot_permisos = $db->select('tb_indicador_cot_permisos', 'dg_permisos', "dc_usuario={$idUsuario}");
$cond = '';
if (!count($ind_cot_permisos)) {
    $cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
} else {
    if ($ind_cot_permisos[0]['dg_permisos'] != 'ALL') {
        $cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
    }
}

$indicador_cotizacion = $db->select("(SELECT c.dc_cotizacion,c.dq_neto FROM (SELECT * FROM tb_cotizacion WHERE dc_empresa={$empresa} {$cond}) c
	JOIN tb_cotizacion_estado es ON c.dc_estado = es.dc_estado AND dm_fin_proceso='0') cot
	JOIN tb_cotizacion_avance av ON dm_estado_actividad = '1' AND cot.dc_cotizacion = av.dc_cotizacion",
	'UNIX_TIMESTAMP(av.df_proxima_actividad)-UNIX_TIMESTAMP(NOW()) AS diferencia,cot.dq_neto');

$indicadores = array(
    'verde' => 0,
    'ambar' => 0,
    'rojo' => 0
);

$divisor = & $empresa_conf['dc_indicador_unidad_cot'];
$verde = & $empresa_conf['dc_indicador_verde_cot'];
$rojo = & $empresa_conf['dc_indicador_rojo_cot'];
$monto = 0;
$monto_verde = 0;
$monto_ambar = 0;
$monto_rojo = 0;

foreach ($indicador_cotizacion as $cot) {
    $monto += $cot['dq_neto'];
    if ($cot['diferencia'] / $divisor > $verde) {
        $indicadores['verde']++;
        $monto_verde += $cot['dq_neto'];
    } else if ($cot['diferencia'] / $divisor < $rojo) {
        $indicadores['rojo']++;
        $monto_rojo += $cot['dq_neto'];
    } else {
        $indicadores['ambar']++;
        $monto_ambar += $cot['dq_neto'];
    }
}

$monto = moneda_local($monto / $ind_cambio);
$monto_verde = moneda_local($monto_verde / $ind_cambio);
$monto_ambar = moneda_local($monto_ambar / $ind_cambio);
$monto_rojo = moneda_local($monto_rojo / $ind_cambio);

echo("<div class='indicador'>
<div class='ind_header' onclick='$(this).next().slideToggle(100);'>
	Estado Cotizaciones
</div>
<div class='ind_body'>
	<div class='ind_subhead'>
		Actividades Pendientes
	</div>
	<div class='ind_rojo'>
		<a href='sites/indicadores/proc/detalle_cotizacion.php?c=3' class='loadOnOverlay'>
			{$indicadores['rojo']} Cot, {$ind_cambio_prefix} {$monto_rojo}
		</a>
	</div>
	<div class='ind_ambar'>
		<a href='sites/indicadores/proc/detalle_cotizacion.php?c=2' class='loadOnOverlay'>
		{$indicadores['ambar']} Cot, {$ind_cambio_prefix} {$monto_ambar}
		</a>
	</div>
	<div class='ind_verde'>
		<a href='sites/indicadores/proc/detalle_cotizacion.php?c=1' class='loadOnOverlay'>
		{$indicadores['verde']} Cot, {$ind_cambio_prefix} {$monto_verde}
		</a>
	</div>
	<br />
	<div class='ind_foot'>
		<a href='sites/indicadores/proc/detalle_cotizacion.php' class='loadOnOverlay'>
		ver todo
		</a>
	</div>
</div></div>");

unset($ind_cot_permisos, $cond, $indicador_cotizacion, $indicadores, $divisor, $verde, $rojo);
?>