<?php
define("MAIN",1);
require_once("../../../inc/global.php");

$data = array();

$direction = intval($_GET['direction']);

switch($direction){
	case 0: $data['selectedDay'] = 'HOY'; break;
	case 1: $data['selectedDay'] = 'MAÑANA'; break;
	case -1: $data['selectedDay'] = 'AYER'; break;
	default: $data['selectedDay'] = date('d-m-Y',time() + (60*60*24*$direction)); break;
}

if($direction > 0){
	$query_date = "ADDDATE(CURDATE(),{$direction})";
}else if($direction == 0){
	$query_date = "CURDATE()";
}else{
	$mDirection = abs($direction);
	$query_date = "SUBDATE(CURDATE(),{$mDirection})";
}

$data['routeData'] = $db->select('tb_ruta r
JOIN tb_ruta_detalle d ON d.dc_ruta = r.dc_ruta AND d.dm_activo
JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
JOIN tb_nota_venta nv ON nv.dc_nota_venta = nvd.dc_nota_venta
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
JOIN tb_producto p ON p.dc_producto = nvd.dc_producto',
'r.dq_ruta,d.dq_cantidad,d.dt_hora,p.dg_codigo,nvd.dg_descripcion dg_producto,nv.dq_nota_venta,
nv.dc_nota_venta,cl.dg_razon,r.dc_tipo_ruta,DATE_FORMAT(nv.df_fecha_emision, "%d/%m/%Y") df_fecha_emision',
"r.dm_nula = 0 AND DATE(r.df_ruta) = {$query_date} AND nv.dm_nula = 0",array('orderby' => 'd.dt_hora ASC'));

echo json_encode($data);
?>