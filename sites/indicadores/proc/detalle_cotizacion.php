<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

	$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio,dg_prefijo',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
	if(count($ind_cambio)){
		$ind_cambio_prefix = $ind_cambio[0]['dg_prefijo'];
		$ind_cambio = $ind_cambio[0]['dq_cambio'];
	}else{
		$ind_cambio_prefix = '$';
		$ind_cambio = 1;
	}
	
	if(check_permiso(11)){
	
		$divisor =& $empresa_conf['dc_indicador_unidad_cot'];
		$verde =& $empresa_conf['dc_indicador_verde_cot'];
		$rojo =& $empresa_conf['dc_indicador_rojo_cot'];
		$monto = 0;
		
		$filtro = '';
		if(isset($_POST['c'])){
			switch($_POST['c']){
				case 1:
					$filtro = "diferencia/{$divisor} > {$verde}"; break;
				case 3:
					$filtro = "diferencia/{$divisor} < {$rojo}"; break;
				case 2:
					$filtro = "diferencia/{$divisor} < {$verde} AND diferencia/{$divisor} > {$rojo}"; break;
			}
		}
		
		$ind_cot_permisos = $db->select('tb_indicador_cot_permisos','dg_permisos',"dc_usuario={$idUsuario}");
		$cond = '';
		if(!count($ind_cot_permisos)){
			$cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
		}else{
			if($ind_cot_permisos[0]['dg_permisos'] != 'ALL'){
				$cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
			}
		}
		
		$indicador_oportunidad = $db->select("
		(SELECT c.* FROM (SELECT * FROM tb_cotizacion WHERE dc_empresa={$empresa} {$cond}) c
			JOIN (SELECT * FROM tb_cotizacion_estado WHERE dm_fin_proceso='0') es ON c.dc_estado = es.dc_estado) cot
		JOIN (SELECT * FROM tb_cotizacion_avance WHERE dm_estado_actividad = '1') av ON cot.dc_cotizacion = av.dc_cotizacion
		LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) f ON cot.dc_ejecutivo = f.dc_funcionario",
		'UNIX_TIMESTAMP(av.df_proxima_actividad)-UNIX_TIMESTAMP(NOW()) AS diferencia,cot.dc_cotizacion,
		cot.dq_neto,cot.dq_cotizacion,CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS ejecutivo,
		DATE_FORMAT(cot.df_fecha_emision,"%d/%m/%Y") AS emision,DATE_FORMAT(av.df_proxima_actividad,"%d/%m/%Y") AS estimado',
		'',array('having' => $filtro, 'order_by' => 'diferencia ASC, estimado ASC'));
		
		if(!count($indicador_oportunidad)){
			$error_man->showInfo('No hay detalles para mostrar');
			exit();
		}
		
		
		echo("<div class='panes'><table width='100%' class='tab' id='indicadores_table_list' style='margin:5px;'>
		<caption>Indicador Oportunidades pendientes</caption>
		<thead>
		<tr>
			<th width='30'>&nbsp;</th>
			<th width='140'>Número de cotización</th>
			<th width='95'>Fecha emisión</th>
			<th width='95'>Cierre estimado</th>
			<th>Ejecutivo</th>
			<th width='130'>Monto estimado ({$ind_cambio_prefix})</th>
		</tr>
		</thead>
		<tbody>");
		
		foreach($indicador_oportunidad as $op){
		$monto += $op['dq_neto']/$ind_cambio;
		$op['dq_neto'] = moneda_local($op['dq_neto']/$ind_cambio);
			if($op['diferencia']/$divisor > $verde){
				$color = '#0F0';
				$bg2 = '#D1FFD1';
			}else if($op['diferencia']/$divisor < $rojo){
				$color = '#F00';
				$bg2 = '#FFD1D1';
			}else{
				$color = '#FF0';
				$bg2 = '#FFFFD1';
			}
				
			echo("<tr>
				<td style='background:{$color};'></td>
				<td style='background:{$bg2};'>
					<a href='sites/ventas/proc/show_cotizacion.php?id={$op['dc_cotizacion']}'>{$op['dq_cotizacion']}</a>
				</td>
				<td style='background:{$bg2};'>{$op['emision']}</td>
				<td style='background:{$bg2};'>{$op['estimado']}</td>
				<td style='background:{$bg2};'>{$op['ejecutivo']}</td>
				<td style='background:{$bg2};' align='right'>{$op['dq_neto']}</td>
			<tr>");
		}
		$monto = moneda_local($monto);
		
		echo("</tbody>
		<tfoot>
		<tr>
			<th colspan='5' align='right'>Monto Total</th>
			<th align='right'>{$monto}</th>
		</tr>
		</tfoot></table>
		
		<div id='op_options' class='hidden'>
		 <button type='button' class='button' id='print_version'>Version de impresión</button>
		 <button type='button' id='gestion_cotizacion' class='button' style='background:#F5F5F5 url(images/management.png) no-repeat center left;'>Gestión</button>
		 <button type='button' id='historial_actividad' class='button' style='background:#F5F5F5 url(images/archive.png) no-repeat center left;'>Historial</button>
		</div>
		<div id='cot_loader'></div>
		</div>");
		
	}else{
		$error_man->shor_fatal_error("Acceso denegado",array('Detalle','No tiene permisos para acceder al recurso. Consulte con un encargado.'));
	}
?>
<script type="text/javascript">
	$("#indicadores_table_list a").click(function(e){
		e.preventDefault();
		$('#cot_loader').html("<img src='images/ajax-loader.gif' alt='Cargando...' />");
		loadFile($(this).attr('href'),'#cot_loader','',function(){
			$('#op_options').show();
		});
	});
</script>