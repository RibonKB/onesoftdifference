<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

	$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio,dg_prefijo',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
	if(count($ind_cambio)){
		$ind_cambio_prefix = $ind_cambio[0]['dg_prefijo'];
		$ind_cambio = $ind_cambio[0]['dq_cambio'];
	}else{
		$ind_cambio_prefix = '$';
		$ind_cambio = 1;
	}
	
	
	
	if(check_permiso(10)){
	
		$divisor =& $empresa_conf['dc_indicador_unidad_op'];
		$verde =& $empresa_conf['dc_indicador_verde_op'];
		$rojo =& $empresa_conf['dc_indicador_rojo_op'];
		$monto = 0;
		
		$filtro = '';
		if(isset($_POST['c'])){
			switch($_POST['c']){
				case 1:
					$filtro = "diferencia/{$divisor} > {$verde}"; break;
				case 3:
					$filtro = "diferencia/{$divisor} < {$rojo}"; break;
				case 2:
					$filtro = "diferencia/{$divisor} < {$verde} AND diferencia/{$divisor} > {$rojo}"; break;
			}
		}
		
		$ind_op_permisos = $db->select('tb_indicador_op_permisos','dg_permisos',"dc_usuario={$idUsuario}");
		$cond = '';
		if(!count($ind_op_permisos)){
			$cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
		}else{
			if($ind_op_permisos[0]['dg_permisos'] != 'ALL'){
				$cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
			}
		}
		$indicador_oportunidad = $db->select("(SELECT * FROM tb_oportunidad WHERE dm_estado='0' {$cond}) op
		JOIN (SELECT * FROM tb_oportunidad_avance WHERE dm_estado_actividad = '1') av ON op.dc_oportunidad = av.dc_oportunidad
		LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) f ON op.dc_ejecutivo = f.dc_funcionario",
		'UNIX_TIMESTAMP(av.df_proxima_actividad)-UNIX_TIMESTAMP(NOW()) AS diferencia,op.dc_oportunidad,
		op.dq_monto,op.dq_oportunidad,CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS ejecutivo,
		DATE_FORMAT(op.df_fecha_emision,"%d/%m/%Y") AS emision,DATE_FORMAT(av.df_proxima_actividad,"%d/%m/%Y") AS estimado',
		'',array('having' => $filtro, 'order_by' => 'diferencia ASC, estimado ASC'));
		
		if(!count($indicador_oportunidad)){
			$error_man->showInfo('No hay detalles para mostrar');
			exit();
		}
		
		
		echo("<div class='panes'><table width='100%' class='tab' id='indicadores_table_list' style='margin:5px;'>
		<caption>Indicador Oportunidades pendientes</caption>
		<thead>
		<tr>
			<th width='30'>&nbsp;</th>
			<th width='140'>Número de oportunidad</th>
			<th width='95'>Fecha emisión</th>
			<th width='95'>Cierre estimado</th>
			<th>Ejecutivo</th>
			<th width='130'>Monto estimado ({$ind_cambio_prefix})</th>
		</tr>
		</thead>
		<tbody>");
		
		foreach($indicador_oportunidad as $op){
		$monto += $op['dq_monto']/$ind_cambio;
		$op['dq_monto'] = moneda_local($op['dq_monto']/$ind_cambio);
			if($op['diferencia']/$divisor > $verde){
				$color = '#0F0';
				$bg2 = '#D1FFD1';
			}else if($op['diferencia']/$divisor < $rojo){
				$color = '#F00';
				$bg2 = '#FFD1D1';
			}else{
				$color = '#FF0';
				$bg2 = '#FFFFD1';
			}
				
			echo("<tr>
				<td style='background:{$color};'></td>
				<td style='background:{$bg2};'>
					<a href='sites/ventas/oportunidad/proc/show_oportunidad.php?id={$op['dc_oportunidad']}'>{$op['dq_oportunidad']}</a>
				</td>
				<td style='background:{$bg2};'>{$op['emision']}</td>
				<td style='background:{$bg2};'>{$op['estimado']}</td>
				<td style='background:{$bg2};'>{$op['ejecutivo']}</td>
				<td style='background:{$bg2};' align='right'>{$op['dq_monto']}</td>
			<tr>");
		}
		$monto = moneda_local($monto);
		
		echo("</tbody>
		<tfoot>
		<tr>
			<th colspan='5' align='right'>Monto Total</th>
			<th align='right'>{$monto}</th>
		</tr>
		</tfoot></table>
		
		<div id='op_options' class='hidden'>
		 <button type='button' class='button' id='print_version'>Version de impresión</button>
		 <button type='button' class='editbtn' id='edit_oportunidad'>Editar</button>
		 <button type='button' id='gestion_oportunidad' class='button' style='background:#F5F5F5 url(images/management.png) no-repeat center left;'>Gestión</button>
		 <button type='button' id='historial_actividad' class='button' style='background:#F5F5F5 url(images/archive.png) no-repeat center left;'>Historial</button>
		 <button type='button' id='cotizar_oportunidad' class='button' style='background:#F5F5F5 url(images/doc.png) no-repeat;'>Cotizar</button>
		</div>
		<div id='op_loader'></div>
		</div>");
		
	}else{
		$error_man->shor_fatal_error("Acceso denegado",array('Detalle','No tiene permisos para acceder al recurso. Consulte con un encargado.'));
	}
?>
<script type="text/javascript">
	$("#indicadores_table_list a").click(function(e){
		e.preventDefault();
		$('#op_loader').html("<img src='images/ajax-loader.gif' alt='Cargando...' />");
		loadFile($(this).attr('href'),'#op_loader','',function(){
			$('#op_options').show();
		});
	});
</script>