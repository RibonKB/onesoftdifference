<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

	$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio,dg_prefijo,dn_cantidad_decimales',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
	if(count($ind_cambio)){
		$ind_cambio_prefix = $ind_cambio[0]['dg_prefijo'];
		$ind_cambio_decimals = $ind_cambio[0]['dn_cantidad_decimales'];
		$ind_cambio = $ind_cambio[0]['dq_cambio'];
	}else{
		$ind_cambio_prefix = '$';
		$ind_cambio = 1;
		$ind_cambio_decimals = 0;
	}
	
	if(check_permiso(22)){
	
		$divisor =& $empresa_conf['dc_indicador_unidad_cot'];
		$verde =& $empresa_conf['dc_indicador_verde_cot'];
		$rojo =& $empresa_conf['dc_indicador_rojo_cot'];
		$monto = 0;
		
		$filtro = '';
		if(isset($_POST['c'])){
			switch($_POST['c']){
				case 1:
					$filtro = "diferencia/{$divisor} > {$verde}"; break;
				case 3:
					$filtro = "diferencia/{$divisor} < {$rojo}"; break;
				case 2:
					$filtro = "diferencia/{$divisor} < {$verde} AND diferencia/{$divisor} > {$rojo}"; break;
			}
		}
		
		if($_POST['mode'] == 1){
		
		//Pendientes de compra
		$indicador_oportunidad = $db->select("
		(SELECT * FROM tb_nota_venta WHERE dc_empresa={$empresa} AND dm_validada=1 AND dm_confirmada=1 AND dc_solicitada > dc_comprada AND dm_nula=0) nv
		LEFT JOIN tb_funcionario f ON nv.dc_ejecutivo = f.dc_funcionario",
		'UNIX_TIMESTAMP(GREATEST(nv.df_confirmacion,nv.df_validacion,nv.df_fecha_emision))-UNIX_TIMESTAMP(NOW()) diferencia,nv.dc_nota_venta,
		nv.dq_neto,nv.dq_nota_venta,CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) ejecutivo,
		DATE_FORMAT(nv.df_fecha_emision,"%d/%m/%Y %H:%i") emision,
		DATE_FORMAT(nv.df_validacion,"%d/%m/%Y %H:%i") df_validacion, DATE_FORMAT(nv.df_confirmacion,"%d/%m/%Y %H:%i") df_confirmacion',
		'',array('having' => $filtro, 'order_by' => 'diferencia ASC'));
		
		}else if($_POST['mode'] == 2){
			
		//Pendientes de confirmación/validación	
		$indicador_oportunidad = $db->select("(SELECT * FROM tb_nota_venta WHERE dc_empresa={$empresa} AND (dm_validada=0 OR dm_confirmada=0) AND dm_nula=0) nv
		LEFT JOIN tb_funcionario f ON nv.dc_ejecutivo = f.dc_funcionario",
		'UNIX_TIMESTAMP(nv.df_fecha_emision)-UNIX_TIMESTAMP(NOW()) diferencia,nv.dc_nota_venta,
		nv.dq_neto,nv.dq_nota_venta,CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) ejecutivo,
		DATE_FORMAT(nv.df_fecha_emision,"%d/%m/%Y %H:%i") emision,
		DATE_FORMAT(nv.df_validacion,"%d/%m/%Y %H:%i") df_validacion, DATE_FORMAT(nv.df_confirmacion,"%d/%m/%Y %H:%i") df_confirmacion',
		'',array('having' => $filtro, 'order_by' => 'diferencia ASC'));
			
		}else{
			exit;
		}
		
		if(!count($indicador_oportunidad)){
			$error_man->showInfo('No hay detalles para mostrar');
			exit();
		}
		
		
		echo("<div class='panes'><table width='100%' class='tab' id='indicadores_table_list' style='margin:5px;'>
		<caption>Indicador Notas de venta pendientes</caption>
		<thead>
		<tr>
			<th width='30'>&nbsp;</th>
			<th width='140'>Nota de venta</th>
			<th width='95'>Fecha emisión</th>
			<th width='120'>Fecha validación</th>
			<th width='120'>Fecha confirmación</th>
			<th>Ejecutivo</th>
			<th width='130'>Neto ({$ind_cambio_prefix})</th>
		</tr>
		</thead>
		<tbody>");
		
		foreach($indicador_oportunidad as $op){
		$monto += $op['dq_neto']/$ind_cambio;
		$op['dq_neto'] = moneda_local($op['dq_neto']/$ind_cambio,$ind_cambio_decimals);
			if($op['diferencia']/$divisor > $verde){
				$color = '#0F0';
				$bg2 = '#D1FFD1';
			}else if($op['diferencia']/$divisor < $rojo){
				$color = '#F00';
				$bg2 = '#FFD1D1';
			}else{
				$color = '#FF0';
				$bg2 = '#FFFFD1';
			}
			
			if($op['df_validacion'] == '00/00/0000 00:00'){
				$op['df_validacion'] = 'S/V';
			}
			
			if($op['df_confirmacion'] == '00/00/0000 00:00'){
				$op['df_confirmacion'] = 'S/C';
			}
				
			echo("<tr>
				<td style='background:{$color};'></td>
				<td style='background:{$bg2};'>
					<a href='sites/ventas/nota_venta/proc/show_nota_venta.php?id={$op['dc_nota_venta']}'>{$op['dq_nota_venta']}</a>
				</td>
				<td style='background:{$bg2};'>{$op['emision']}</td>
				<td style='background:{$bg2};'>{$op['df_validacion']}</td>
				<td style='background:{$bg2};'>{$op['df_confirmacion']}</td>
				<td style='background:{$bg2};'>{$op['ejecutivo']}</td>
				<td style='background:{$bg2};' align='right'>{$op['dq_neto']}</td>
			<tr>");
		}
		$monto = moneda_local($monto,$ind_cambio_decimals);
		
		echo("</tbody>
		<tfoot>
		<tr>
			<th colspan='6' align='right'>Monto Total</th>
			<th align='right'>{$monto}</th>
		</tr>
		</tfoot></table>
		<div id='cot_loader'></div>
		</div>");
		
	}else{
		$error_man->show_fatal_error("Acceso denegado",array('Detalle','No tiene permisos para acceder al recurso. Consulte con un administrador.'));
	}
?>
<script type="text/javascript">
	$("#indicadores_table_list a").click(function(e){
		e.preventDefault();
		$('#cot_loader').html("<img src='images/ajax-loader.gif' alt='Cargando...' />");
		loadFile($(this).attr('href'),'#cot_loader');
	});
</script>