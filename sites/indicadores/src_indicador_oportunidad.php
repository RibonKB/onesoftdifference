<?php
$ind_op_permisos = $db->select('tb_indicador_op_permisos', 'dg_permisos', "dc_usuario={$idUsuario}");
$cond = '';
if (!count($ind_op_permisos)) {
    $cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
} else {
    if ($ind_op_permisos[0]['dg_permisos'] != 'ALL') {
        $cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
    }
}
$indicador_oportunidad = $db->select("(SELECT * FROM tb_oportunidad WHERE dm_estado='0' {$cond}) op
		JOIN tb_oportunidad_avance av ON av.dm_estado_actividad = '1' AND op.dc_oportunidad = av.dc_oportunidad",
		'UNIX_TIMESTAMP(av.df_proxima_actividad)-UNIX_TIMESTAMP(NOW()) AS diferencia,op.dq_monto');

$indicadores = array(
    'verde' => 0,
    'ambar' => 0,
    'rojo' => 0
);

$divisor = & $empresa_conf['dc_indicador_unidad_op'];
$verde = & $empresa_conf['dc_indicador_verde_op'];
$rojo = & $empresa_conf['dc_indicador_rojo_op'];
$monto = 0;
$monto_verde = 0;
$monto_ambar = 0;
$monto_rojo = 0;

foreach ($indicador_oportunidad as $op) {
    $monto += $op['dq_monto'];
    if ($op['diferencia'] / $divisor > $verde) {
        $indicadores['verde']++;
        $monto_verde += $op['dq_monto'];
    } else if ($op['diferencia'] / $divisor < $rojo) {
        $indicadores['rojo']++;
        $monto_rojo += $op['dq_monto'];
    } else {
        $indicadores['ambar']++;
        $monto_ambar += $op['dq_monto'];
    }
}

$monto = moneda_local($monto / $ind_cambio);
$monto_verde = moneda_local($monto_verde / $ind_cambio);
$monto_ambar = moneda_local($monto_ambar / $ind_cambio);
$monto_rojo = moneda_local($monto_rojo / $ind_cambio);

echo("<div class='indicador'>
<div class='ind_header' onclick='$(this).next().slideToggle(100);'>
	Estado Oportunidades
</div>
<div class='ind_body'>
	<div class='ind_subhead'>
		Actividades Pendientes
	</div>
	<div class='ind_rojo'>
		<a href='sites/indicadores/proc/detalle_oportunidad.php?c=3' class='loadOnOverlay'>
		{$indicadores['rojo']} Opt, {$ind_cambio_prefix} {$monto_rojo}
		</a>
	</div>
	<div class='ind_ambar'>
		<a href='sites/indicadores/proc/detalle_oportunidad.php?c=2' class='loadOnOverlay'>
		{$indicadores['ambar']} Opt, {$ind_cambio_prefix} {$monto_ambar}
		</a>
	</div>
	<div class='ind_verde'>
		<a href='sites/indicadores/proc/detalle_oportunidad.php?c=1' class='loadOnOverlay'>
		{$indicadores['verde']} Opt, {$ind_cambio_prefix} {$monto_verde}
		</a>
	</div>
	<br />
	<div class='ind_foot'>
		<a href='sites/indicadores/proc/detalle_oportunidad.php' class='loadOnOverlay'>
		ver todo
		</a>
	</div>
</div></div>");

unset($ind_op_permisos, $cond, $indicador_oportunidad, $indicadores, $divisor, $verde, $rojo);
?>