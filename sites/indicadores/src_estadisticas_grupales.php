<div id="cllr_content3" class="cllr_content">
	<div class="cllr_title">Estadísticas Grupales</div>
	
	<div class="cllr_toolbar">
		<button class="imgbtn cllr_reload" id="cllr_reload3">Cargar Datos</button>
		<?php if(check_permiso(86)): ?>
		|
		<select id="cllr_others3" class="inputtext">
			<option value="0">Seleccione operador</option>
			<?php foreach($callreg_callers as $c): ?>
			<option value="<?=$c['dc_funcionario'] ?>"><?=$c['dg_nombres'] ?> <?=$c['dg_ap_paterno'] ?></option>
			<?php endforeach; ?>
		</select>
		<?php endif; ?>
	</div>
	
	<div id="cllr_data3" class="cllr_data">
		<div class="info">
			Para ver las estadísticas debe presionar el botón de carga de datos antes.
		</div>
	</div>
</div>