<?php

$indicador_cotizacion = $db->select("tb_nota_venta",
	'UNIX_TIMESTAMP(GREATEST(df_confirmacion,df_validacion,df_fecha_emision))-UNIX_TIMESTAMP(NOW()) AS diferencia,dq_neto',
	"dc_empresa={$empresa} AND (dm_validada = 1 AND dm_confirmada = 1) AND dc_solicitada > dc_comprada AND dm_nula=0");

$indicadores = array(
    'verde' => 0,
    'ambar' => 0,
    'rojo' => 0
);

$divisor = & $empresa_conf['dc_indicador_unidad_cot'];
$verde = & $empresa_conf['dc_indicador_verde_cot'];
$rojo = & $empresa_conf['dc_indicador_rojo_cot'];
$monto = 0;
$monto_verde = 0;
$monto_ambar = 0;
$monto_rojo = 0;

foreach ($indicador_cotizacion as $cot) {
    $monto += $cot['dq_neto'];
    if ($cot['diferencia'] / $divisor > $verde) {
        $indicadores['verde']++;
        $monto_verde += $cot['dq_neto'];
    } else if ($cot['diferencia'] / $divisor < $rojo) {
        $indicadores['rojo']++;
        $monto_rojo += $cot['dq_neto'];
    } else {
        $indicadores['ambar']++;
        $monto_ambar += $cot['dq_neto'];
    }
}

$monto = moneda_local($monto / $ind_cambio);
$monto_verde = moneda_local($monto_verde / $ind_cambio);
$monto_ambar = moneda_local($monto_ambar / $ind_cambio);
$monto_rojo = moneda_local($monto_rojo / $ind_cambio);

echo("<div class='indicador'>
<div class='ind_header' onclick='$(this).next().slideToggle(100);'>
	Notas de venta pend. de compra
</div>
<div class='ind_body'>
	<div class='ind_subhead'>
		Actividades Pendientes
	</div>
	<div class='ind_rojo'>
		<a href='sites/indicadores/proc/detalle_nota_venta.php?c=3&mode=1' class='loadOnOverlay'>
			{$indicadores['rojo']} Cot, {$ind_cambio_prefix} {$monto_rojo}
		</a>
	</div>
	<div class='ind_ambar'>
		<a href='sites/indicadores/proc/detalle_nota_venta.php?c=2&mode=1' class='loadOnOverlay'>
		{$indicadores['ambar']} Cot, {$ind_cambio_prefix} {$monto_ambar}
		</a>
	</div>
	<div class='ind_verde'>
		<a href='sites/indicadores/proc/detalle_nota_venta.php?c=1&mode=1' class='loadOnOverlay'>
		{$indicadores['verde']} Cot, {$ind_cambio_prefix} {$monto_verde}
		</a>
	</div>
	<br />
	<div class='ind_foot'>
		<a href='sites/indicadores/proc/detalle_nota_venta.php?mode=1' class='loadOnOverlay'>
		ver todo
		</a>
	</div>
</div></div>");

unset($ind_cot_permisos, $cond, $indicador_cotizacion, $indicadores, $divisor, $verde, $rojo);
?>