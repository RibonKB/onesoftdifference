<?php
	$callreg_callers = $db->select(
							'tb_funcionario f
							 JOIN tb_ceco c ON c.dc_ceco = f.dc_ceco',
							'f.dc_funcionario, f.dg_nombres, f.dg_ap_paterno, f.dg_anexo',
							"f.dg_anexo > 0 AND f.dc_empresa = {$empresa} AND f.dm_activo = 1 AND c.dm_analisis_estadistico = 1");
?>
<div id="callregindex">

	<div id="cllr_activate_button" class="cllr_activate_button" data-dst="#cllr_content">
		<img src="images/cllr_icon1.png" />
	</div>
	<div id="cllr_activate_button2" class="cllr_activate_button" data-dst="#cllr_content2">
		<img src="images/cllr_icon2.png" />
	</div>
	<div id="cllr_activate_button3" class="cllr_activate_button" data-dst="#cllr_content3">
		<img src="images/cllr_icon3.png" />
	</div>
	
	<?php include(__DIR__.'/src_registro_llamados.php'); ?>
	<?php include(__DIR__.'/src_estadisticas_individuales.php'); ?>
	<?php include(__DIR__.'/src_estadisticas_grupales.php'); ?>
	
</div>