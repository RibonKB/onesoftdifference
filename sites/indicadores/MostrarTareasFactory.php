<?php
	require_once("inc/global.php");
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MostrarTareasFactory
 *
 * @author Eduardo
 */    
        $usuario=$db->select('tb_usuario','dc_funcionario',"dc_usuario={$idUsuario}");
        $cargo = $db->select('tb_funcionario','dc_cargo',"dc_funcionario={$usuario[0]['dc_funcionario']}");
        $cargos = $db->select('tb_funcionario fu LEFT JOIN tb_cargos ca ON ca.dc_cargo=fu.dc_cargo','fu.dc_funcionario id',"ca.dc_evaluador={$cargo[0]['dc_cargo']}");
        $users='';
        if($cargos){
        foreach($cargos as $c){
            if($users==''){
                $users=','.$c['id'];
            }else{
                $users=$users.",".$c['id'];
            }
            
        }
        }
        
        $tareas=$db->select('tb_tarea ta
                LEFT JOIN tb_tipo_tarea tita 
                    ON tita.dc_tipo_tarea=ta.dc_tipo_tarea
                    LEFT JOIN tb_funcionario fu ON fu.dc_funcionario = ta.dc_funcionario',
                'dm_estado Estado_tarea,
                df_fecha Fecha_tarea,
                dg_tipo_tarea Nombre_tarea,
                dg_descripcion Descripcion_tarea,
                CONCAT(dg_nombres," ",dg_ap_paterno," ",dg_ap_materno) responsable',
                "ta.dc_funcionario IN ({$usuario[0]['dc_funcionario']}{$users}) 
                    AND ta.dm_estado IN ('P','A')");
?>
<table class="tab bicolor_tab">
    <thead>
        <tr align="center">
            <th>
              Tarea  
            </th>
            <th>
                Descripción
            </th>
            <th>
                Responsable
            </th>
            <th>
                Fecha Limite
            </th>
            <th>
                Estado
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if($tareas){
        foreach($tareas as $D_Tareas):
            $color='';
        ?>
        <tr align="center">
            <td>
                <?=$D_Tareas['Nombre_tarea']?>
            </td>
            <td>
                <?=$D_Tareas['Descripcion_tarea']?>
            </td>
            <td>
                <?=$D_Tareas['responsable']?>
            </td>
            <td>
                <?=$D_Tareas['Fecha_tarea']?>
            </td>
            <?php 
            switch($D_Tareas['Estado_tarea']):
                case 'P':
                    $color='color: sienna';
                    break;
                case 'A':
                    $color='color : indianred';
                    break;
            endswitch;
            ?>
            <td style="font-weight: bolder ;font-size: 18px;<?=$color?>" align="center">
                <?=$D_Tareas['Estado_tarea']?>
            </td>
            
        </tr>
        <?php 
        endforeach;
        }else{
            ?>
        <tr>
            <td colspan="5" align="center">
                No tiene tareas
            </td>
        </tr>
        <?php
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="5">
                
            </th>
        </tr>
    </tfoot>
</table>