<div id="cllr_content" class="cllr_content">
	<div class="cllr_title">Información de llamados</div>
	<div class="cllr_toolbar">
		<button class="imgbtn cllr_reload" id="cllr_reload">Cargar Datos</button>
		<?php if(check_permiso(86)): ?>
		|
		<select id="cllr_callers" class="inputtext">
			<option value="0">Seleccione operador</option>
			<?php foreach($callreg_callers as $c): ?>
			<option value="<?=$c['dc_funcionario'] ?>"><?=$c['dg_nombres'] ?> <?=$c['dg_ap_paterno'] ?> (<?=$c['dg_anexo'] ?>)</option>
			<?php endforeach; ?>
		</select>
		<?php endif; ?>
	</div>
	<div id="cllr_data" class="cllr_data">
		<div class="info">
			Para ver la información de los llamados debe presionar el botón de carga de datos antes.
		</div>
	</div>
</div>