<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/Factory.class.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div class="secc_bar">Pago de facturas de venta</div>
<div class="panes">
<?php

if(!isset($_POST['dc_factura'])){
	$error_man->showWarning("No ha seleccionado facturas para ser puestas en el proceso de pago");
	exit;
}

//Datos POST pasados a variable
$dc_factura = implode(',',$_POST['dc_factura']);
$dm_modo = $_POST['mode'];


if(!in_array($dm_modo,array('G','P'))){
	$error_man->showWarning("Operación no permitida");
	exit;
}

//Datos de las facturas seleccionadas
$data = $db->select('tb_factura_venta','df_emision, df_vencimiento, dq_total, dq_monto_pagado, dc_cliente, dc_factura, dq_factura, dq_folio',
	"dc_factura IN ({$dc_factura}) AND dc_empresa={$empresa} AND dm_nula = 0 AND dq_monto_pagado < dq_total");

//¿Se encontraron las facturas?
if(!count($data)){
	$error_man->showWarning('No se encontraron datos de cobranza para las facturas seleccionadas.');
	exit;
}

//¿Se encontró información de cobranza para todas las facturas y no fue ninguna anulada?
if(count($_POST['dc_factura']) != count($data)){
	$error_man->showAviso("Hubo un cambio en los datos de las facturas seleccionadas que impiden continuar la gestión, por favor actualice el informe de cobranza y vuelva a intentarlo.");
	exit;
}

$dc_cliente = intval($data[0]['dc_cliente']);

$cliente = $db->select('tb_cliente','dg_razon, dc_contacto_default','dc_cliente = '.$dc_cliente);
$cliente = array_shift($cliente);

require_once("../../../inc/form-class.php");
$form = new Form($empresa);
if($dm_modo == 'P')
	$form->Start(Factory::buildUrl('crearComprobantePago', 'finanzas', 'procesarCrear', 'cobros'),'cr_gestion_cobranza');
else if($dm_modo == 'G')
	$form->Start('sites/finanzas/cobros/proc/crear_gestion_intermedia.php','cr_gestion_cobranza');

$form->Header('<strong>Indique los datos para generar la gestión de cobranza sobre las facturas de venta</strong><br />Los datos marcados con [*] son obligatorios');

if($dm_modo == 'G'){
	$form->Section();
	$form->Textarea('Comentario','gestion_comentario',1);
	$form->Date('Fecha compromiso','gestion_compromiso');
	$form->DateTime('Fecha próxima gestión','gestion_prox_gestion',1,0);
	$form->EndSection();

	$form->Section();
	$form->Listado('Contacto','gestion_contacto','tb_contacto_cliente',array('dc_contacto','dg_contacto'),1,$cliente['dc_contacto_default'],"dc_cliente={$dc_cliente}");
	echo('<br /><div id="contact_data" style="margin:0 10px"></div>');
	
	$form->EndSection();
}

if($dm_modo === 'P'){
	//Acciones con pago
	
	$form->Section();
		echo('<fieldset><table class="tab" width="300" id="mountTable">
		<thead><tr>
			<th>Factura</th>
			<th>Monto Original</th>
			<th>Monto</th>
		</tr></thead><tbody>');
		
		foreach($data as $factura){
			echo('<tr><td>');
				echo '<b>'.$factura['dq_factura'].'</b>-'.$factura['dq_folio'];
			echo("<td align='right'>".
				moneda_local($factura['dq_total']-$factura['dq_monto_pagado'])
			."</td>");
			echo('</td><td align="right">');
				$form->Text('','gestion_dq_monto[]',1,255,$factura['dq_total']-$factura['dq_monto_pagado'],'mountItem inputtext',20);
				$form->Hidden('max_value[]',$factura['dq_total']-$factura['dq_monto_pagado']);
			echo('</td></tr>');
		}
			
		echo('</tbody>
			<tfoot><tr>
				<th align="right" colspan="2">Total</th>
				<th align="right" width="220" id="totalMount">0</th>
			</tr></tfoot></table></fieldset>');
	$form->EndSection();
	
	$form->Section();
		echo('<div id="cobros_container"><fieldset>');
		//$form->Text('Monto (Monto sin cobrar: $ <b>'.moneda_local($data['dq_total']-$data['dq_monto_pagado']).'</b>)','gestion_monto',0,255,0);
		$form->Listado('Medio de cobro','dc_medio_cobro','tb_medio_cobro_cliente',array(
			'CONCAT_WS(",",dc_medio_cobro,dm_requiere_banco,dm_requiere_documento,dm_asiento_intermedio)',
			'dg_medio_cobro'),true);
			
		$form->DateManual('Fecha Contable','df_fecha_contable',1,0);
		
		echo('<div id="cuenta_contable_medio_cobro" class="hidden">');
			$form->Select('Cuenta Pago [*]','dc_cuenta_contable',array());
		echo('</div></fieldset>');
		
		//echo('<br />');
		//$form->Button('Detalles de Pago','id="set_documents_detail" disabled="disabled"','addbtn disbutton');
		
	$form->EndSection();
}

foreach($data as $factura){
	$form->Hidden('gestion_dc_factura[]',$factura['dc_factura']);
	$form->Hidden('gestion_df_emision_factura',$factura['df_emision']);
}

$form->Group();

?>
<div id="PayDetail"></div>
<?php

$form->Hidden('gestion_dc_cliente',$dc_cliente);
$form->End('Crear','addbtn');

//Configurar mensaje de correo para envío al contacto

$emailBody = '\n\nFACTURAS:\n\n';
foreach($data as $f){
	$emailBody .= 'Factura: '.$f['dq_folio'].'\n';
	$emailBody .= 'Fecha Emisión: '.$db->dateLocalFormat($f['df_emision']).'\n';
	$emailBody .= 'Fecha Vencimiento: '.$db->dateLocalFormat($f['df_vencimiento']).'\n';
	$emailBody .= 'Cliente: '.$cliente['dg_razon'].'\n';
	$emailBody .= 'Total: '.moneda_local($f['dq_total']).'\n\n';
}
?>
</div>
<script type="text/javascript" src="jscripts/sites/finanzas/cobros/cr_gestion_cobranza.commons.js?v=0_2b"></script>
<script type="text/javascript" src="jscripts/sites/finanzas/cobros/cr_gestion_cobranza_masiva.js?v=0_11_3b"></script>
<script type="text/javascript">
	js_data.emailBody = '<?php echo $emailBody ?>';
	$('#genOverlay').width($('#genOverlay').width()+50);
</script>