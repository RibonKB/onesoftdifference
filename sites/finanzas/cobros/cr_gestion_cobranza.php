<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_factura_venta_gestion WHERE dc_factura={$_POST['id']}) g
JOIN tb_contacto_cliente cc ON cc.dc_contacto = g.dc_contacto
LEFT JOIN tb_medio_pago mp ON mp.dc_medio_pago = g.dc_medio_pago
LEFT JOIN tb_banco b ON b.dc_banco = g.dc_banco
LEFT JOIN tb_factura_venta_gestion_documento doc ON g.dc_gestion = doc.dc_gestion",
'DATE_FORMAT(g.df_emision,"%d/%m/%Y") AS df_emision, g.dq_monto, DATE_FORMAT(g.df_compromiso,"%d/%m/%Y") AS df_compromiso,
DATE_FORMAT(g.df_proxima_gestion,"%d/%m/%Y %H:%i") AS df_proxima_gestion,cc.dg_contacto,cc.dg_email,cc.dg_fono,
g.dg_comentario,mp.dg_medio_pago,b.dg_banco,GROUP_CONCAT(doc.dg_documento) AS dg_documentos',
'',array('group_by' => 'g.dc_gestion','limit'=>2));

if(count($data)){

	echo("<table class='tab' width='100%'>
	<caption>Últimas gestiones de cobranza realizadas sobre la factura</caption>
	<thead><tr>
		<th width='150'>Contacto</th>
		<th width='90'>Monto</th>
		<th>Fecha Gestión</th>
		<th>Fecha Compromiso</th>
		<th>Fecha próxima gestión</th>
		<th>Medio de pago</th>
		<th width='150'>Banco</th>
		<th>Documentos</th>
		<th>Comentario</th>
	</thead><tbody>");
	foreach($data as $d){
		
		$d['dq_monto'] = moneda_local($d['dq_monto']);
		
		echo("<tr>
			<td>{$d['dg_contacto']}</td>
			<td>{$d['dq_monto']}</td>
			<td>{$d['df_emision']}</td>
			<td>{$d['df_compromiso']}</td>
			<td>{$d['df_proxima_gestion']}</td>
			<td>{$d['dg_medio_pago']}</td>
			<td>{$d['dg_banco']}</td>
			<td>{$d['dg_documentos']}</td>
			<td>{$d['dg_comentario']}</td>
		</tr>");
	}
	echo("</tbody></table>");

}

$data = $db->select('tb_factura_venta','df_emision,df_vencimiento,dq_total,dq_monto_pagado,dc_cliente',"dc_factura={$_POST['id']} AND dc_empresa={$empresa}");

if(!count($data)){
	$error_man->showWarning('La factura de venta especificada no existe.');
	exit;
}
$data = $data[0];

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/finanzas/cobros/proc/crear_gestion_cobranza.php','cr_gestion_cobranza');
$form->Header('<strong>Indique los datos para generar la gestión de cobranza sobre la factura de venta</strong><br />Los datos marcados con [*] son obligatorios');
$form->Section();
$form->Listado('Contacto','gestion_contacto','tb_contacto_cliente',array('dc_contacto','dg_contacto'),1,'',"dc_cliente={$data['dc_cliente']}");
$form->Date('Fecha compromiso','gestion_compromiso',1,$data['df_vencimiento']);
echo('<fieldset>');
$form->Date('Fecha próxima gestión','gestion_prox_gestion',1,0);

//hora próxima gestión
echo("<label>Hora</label>
<select name='gestion_prox_hora' class='inputtext' style='width:50px;'>");
foreach(range(1,12) as $h){
	$h = str_pad($h,2,'0',STR_PAD_LEFT);
	echo("<option value='{$h}'>{$h}</option>");
}
echo("</select> : <select name='gestion_prox_minuto' class='inputtext' style='width:50px;'>");
foreach(range(0,45,15) as $m){
	$m = str_pad($m,2,'0',STR_PAD_LEFT);
	echo("<option value='{$m}'>{$m}</option>");
}
echo("</select> - 
<select name='gestion_prox_meridiano' class='inputtext' style='width:70px;'>
<option value='0'>AM</option>
<option value='12'>PM</option>
</select></fieldset>");

$form->Radiobox('Modo','gestion_modo',array('Gestion','Pago','Pago Completo'));

$form->EndSection();

$form->Section();
echo('<div id="cobros_container" class="hidden">');
$form->Text('Monto (Monto sin cobrar: $ <b>'.moneda_local($data['dq_total']-$data['dq_monto_pagado']).'</b>)','gestion_monto',0,255,0);
$form->Listado('Forma de pago','gestion_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'));
$form->Listado('Banco','gestion_banco','tb_banco',array('dc_banco','dg_banco'));
echo('<fieldset>');
$form->Button('Agregar número de documento','id=add_num_doc','addbtn');
echo('<div id="doc_container"></div></fieldset></div>');
$form->EndSection();

$form->Section();
$form->Textarea('Comentario','gestion_comentario',1);
echo('<br /><div id="contact_data"></div>');
$form->EndSection();

$form->Hidden('gestion_monto_maximo',$data['dq_total']-$data['dq_monto_pagado']);
$form->Hidden('id_factura',$_POST['id']);
$form->Hidden('id_cliente',$data['dc_cliente']);
$form->Hidden('emision_factura',$data['df_emision']);
$form->End('Crear','addbtn');
?>
<script type="text/javascript" src="jscripts/factura_venta/gestion_cobranza.js?v=0_7a"></script>