<?php

require_once 'sites/contabilidad/stuff.class.php';
require_once 'sites/ventas/proc/ventas_functions.php';

/**
 * Description of crearComprobantePagoFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 22-04-2013
 */
class crearComprobantePagoFactory extends Factory{
  
    private $medioCobro = NULL;
    private $dq_monto_pagar;
    
    private $cliente;
    
    private $dc_comprobante_contable;
    private $dq_comprobante_contable;
    private $mesContable;
    private $anhoContable;
    private $matrizFinanciera = array();
    private $parametrosFinancieros;
    private $parametrosAnaliticos;
    
    private $dc_comprobante;
    private $dq_comprobante;
    
    public function procesarCrearAction(){
        $request = self::getRequest();
        $db = $this->getConnection();
        $this->medioCobro = $this->getMedioCobro($request->dc_medio_cobro);
        $this->cliente = $this->getCliente($request->gestion_dc_cliente);
        
        $this->dq_monto_pagar = $this->formatearMontos();
        $this->validarBancos();
        $this->validarDocumentos();
        $this->validarTotalesDocumentos();
        
        $this->validarPeriodoContable();
        
        
        $db->start_transaction();
            $this->dc_comprobante = $this->insertarComprobantePago();
            $this->insertarComprobantePagoDetalle();
            $this->actualizarPagosFactura();
            $this->actualizarAnalisisCliente();
            
            if($this->medioCobro->dm_requiere_banco == 1 || $this->medioCobro->dm_requiere_documento == 1)
                $this->insertarDetallesDocumentos ();
            
            $this->dc_comprobante_contable = $this->insertarComprobanteContable();
            $this->insertarDetallesContablesFinancieros();
            $this->insertarDetallesContablesAnaliticos();
            
        $db->commit();
        
        $this->getErrorMan()->showConfirm("Se ha generado el pago de las facturas correctamente");
        echo $this->getView('contabilidad/general/mostrar_comprobante_creado', array(
              'comprobante' => (object) array(
                  'dc_comprobante' => $this->dc_comprobante_contable,
                  'dg_comprobante' => $this->dq_comprobante_contable
              )
        ));
      
    }
    
    private function insertarComprobantePago(){
      
        $db = $this->getConnection();
      
        $this->initFunctionsService();
        $this->dq_comprobante = Functions::generateDocNumber('tb_comprobante_pago_cliente','dq_comprobante');

        $comprobante_pago = $db->prepare($db->insert('tb_comprobante_pago_cliente',array(
            'dq_comprobante' => '?',
            'dc_cliente' => '?',
            'dc_medio_cobro' => '?',
            'df_emision' => $db->getNow(),
            'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
            'dc_empresa' => $this->getEmpresa()
        )));
        $comprobante_pago->bindValue(1,$this->dq_comprobante,PDO::PARAM_INT);
        $comprobante_pago->bindValue(2,$this->cliente->dc_cliente,PDO::PARAM_INT);
        $comprobante_pago->bindValue(3,$this->medioCobro->dc_medio_cobro,PDO::PARAM_INT);

        $db->stExec($comprobante_pago);

        return $db->lastInsertId();
    }
    
    private function insertarComprobantePagoDetalle(){
        $db = $this->getConnection();
      
        //Insertar detalles del comprobante de pago
        $detalle_comprobante_pago = $db->prepare($db->insert('tb_detalle_factura_pago_cliente',array(
            'dc_comprobante' => $this->dc_comprobante,
            'dc_factura' => '?',
            'dq_monto' => '?'
        )));
        $detalle_comprobante_pago->bindParam(1,$dc_factura,PDO::PARAM_INT);
        $detalle_comprobante_pago->bindParam(2,$dq_monto,PDO::PARAM_STR);
        
        foreach(self::getRequest()->gestion_dc_factura as $i => $f){
          $dc_factura = $f;
          $dq_monto = $this->dq_monto_pagar[$i];
          
          $db->stExec($detalle_comprobante_pago);
        }
    }
    
    private function insertarComprobanteContable(){
        $db = $this->getConnection();
        $request = self::getRequest();
        $this->dq_comprobante_contable = ContabilidadStuff::getCorrelativoComprobante($db, $this->getEmpresa());
        
        //Insertar comprobante
        $comprobante_contable = $db->prepare($db->insert('tb_comprobante_contable',array(
            'dg_comprobante' => $this->dq_comprobante_contable,
            'dc_tipo_movimiento' => '?',
            'df_fecha_contable' => '?',
            'dc_mes_contable' => '?',
            'dc_anho_contable' => '?',
            'dq_cambio' => '?',
            'dg_glosa' => '?',
            'dq_saldo' => array_sum($this->dq_monto_pagar),
            'dc_comprobante_pago' => $this->dc_comprobante,
            'df_fecha_emision' => $db->getNow(),
            'dc_empresa' => $this->getEmpresa(),
            'dc_usuario_creacion' => $this->getUserData()->dc_usuario
        )));
        $comprobante_contable->bindValue(1,$this->medioCobro->dc_tipo_movimiento,PDO::PARAM_INT);
        $comprobante_contable->bindValue(2,$db->sqlDate2($request->df_fecha_contable),PDO::PARAM_STR);
        $comprobante_contable->bindValue(3,$this->mesContable,PDO::PARAM_INT);
        $comprobante_contable->bindValue(4,$this->anhoContable,PDO::PARAM_INT);
        $comprobante_contable->bindValue(5,1,PDO::PARAM_INT);
        $comprobante_contable->bindValue(6,"Cobro cliente: {$this->dq_comprobante} - {$this->cliente->dg_razon}",PDO::PARAM_STR);

        $db->stExec($comprobante_contable);

        //obtener id del comprobante recién creado
        return $db->lastInsertId();
    }
    
    private function insertarDetallesContablesFinancieros(){
        
      $st = $this->prepareDetallesContablesFinancieros();
      $this->matrizFinanciera['facturas'] = $this->insertarDFFacturasPago($st);
      $this->matrizFinanciera['medio_pago'] = $this->insertarDFMedioPago($st);
    }
    
        private function prepareDetallesContablesFinancieros(){
            $db = $this->getConnection();
            //Preparar sentencia para insertar detalle de comprobantes contables
            $detalle_comprobante_contable = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
                'dc_comprobante' => $this->dc_comprobante_contable,
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?'
            )));
            $detalle_comprobante_contable->bindParam(1,$this->parametrosFinancieros->dc_cuenta_contable,PDO::PARAM_INT);
            $detalle_comprobante_contable->bindParam(2,$this->parametrosFinancieros->dq_debe,PDO::PARAM_STR);
            $detalle_comprobante_contable->bindParam(3,$this->parametrosFinancieros->dq_haber,PDO::PARAM_STR);
            $detalle_comprobante_contable->bindParam(4,$this->parametrosFinancieros->dg_glosa,PDO::PARAM_STR);
            
            return $detalle_comprobante_contable;
        }
        
        private function insertarDFFacturasPago($st){
            $this->parametrosFinancieros->dq_debe = 0;
            $this->parametrosFinancieros->dc_cuenta_contable = $this->cliente->dc_cuenta_contable;
            $this->parametrosFinancieros->dq_haber = array_sum($this->dq_monto_pagar);
            $this->parametrosFinancieros->dg_glosa = "Pago Facturas Cliente";
            
            $this->getConnection()->stExec($st);
            
            return $this->getConnection()->lastInsertId();
        }
        
        private function insertarDFMedioPago($st){
          
            $this->parametrosFinancieros->dq_haber = 0;
            $this->parametrosFinancieros->dg_glosa = "Pago Cliente Medio Pago: {$this->medioCobro->dg_medio_cobro}";
          
            //Medio de pago con banco y documento
            if($this->medioCobro->dm_requiere_banco == 1 and
               $this->medioCobro->dm_requiere_documento == 1 and
               $this->medioCobro->dm_asiento_intermedio == 1){
                  return $this->insertarDFMPConBancoConDocumento($st);
            }else
            //Medio de pago con banco y sin documento
            if($this->medioCobro->dm_requiere_banco == 1 and
               $this->medioCobro->dm_requiere_documento == 0 and
               $this->medioCobro->dm_asiento_intermedio == 0){
                  return $this->insertarDFMPConBancoNoDocumento($st);
            }else
            //Medio de pago sin banco ni documento  
            if($this->medioCobro->dm_requiere_banco == 0 and
               $this->medioCobro->dm_requiere_documento == 0 and
               $this->medioCobro->dm_asiento_intermedio == 0){
                  return $this->insertarDFMPNoBancoNoDocumento($st);
            }else{
              //Sin implementar
              $this->getErrorMan()->showWarning("Las opciones del medio de pago son erroneas y no han sido implementadas");
              $this->getConnection()->rollBack();
              exit;
            }
        }
        
            private function insertarDFMPConBancoConDocumento($st){
                $request = self::getRequest();

                $this->parametrosFinancieros->dc_cuenta_contable = $this->medioCobro->dc_cuenta_contable_intermedia;
                $this->parametrosFinancieros->dq_debe = array_sum($request->dq_monto);

                $this->getConnection()->stExec($st);

                return $this->getConnection()->lastInsertId();

            }

            private function insertarDFMPConBancoNoDocumento($st){
                $request = self::getRequest();
                $db = $this->getConnection();

                $group = array();
                foreach($request->dq_monto as $i => $dq_monto){
                    $banco = $db->getRowById('tb_banco', $request->dc_banco[$i], 'dc_banco');

                    if(!isset($group[$banco->dc_cuenta_contable]))
                      $group[$banco->dc_cuenta_contable] = 0;

                    $group[$banco->dc_cuenta_contable] += floatval($dq_monto);
                }

                foreach($group as $dc_cuenta_contable => $dq_debe){
                    $this->parametrosFinancieros->dc_cuenta_contable = $dc_cuenta_contable;
                    $this->parametrosFinancieros->dq_debe = $dq_debe;

                    $db->stExec($st);

                    $group[$dc_cuenta_contable] = $db->lastInsertId();
                }

                return $group;

            }

            private function insertarDFMPNoBancoNoDocumento($st){
                $this->parametrosFinancieros->dc_cuenta_contable = self::getRequest()->dc_cuenta_contable;
                $this->parametrosFinancieros->dq_debe = array_sum($this->dq_monto_pagar);

                $this->getConnection()->stExec($st);
                return $this->getConnection()->lastInsertId();
            }
        
    private function insertarDetallesContablesAnaliticos(){
        $st = $this->prepareDetallesContablesAnaliticos();
        
        $this->parametrosAnaliticos->dc_cliente = $this->cliente->dc_cliente;
        $this->parametrosAnaliticos->dc_tipo_cliente = $this->cliente->dc_tipo_cliente;
        $this->parametrosAnaliticos->dc_mercado_cliente = $this->cliente->dc_mercado;
        $this->parametrosAnaliticos->dc_medio_cobro_cliente = $this->medioCobro->dc_medio_cobro;
        
        $this->insertarDAFacturasPago($st);
        $this->insertarDAMedioPago($st);
    }
    
        private function prepareDetallesContablesAnaliticos(){
            $db = $this->getConnection();
            
            $detalle_analitico = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico', array(
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?',
                'dc_detalle_financiero' => '?',
                'dc_factura_venta' => '?',
                'dc_nota_venta' => '?',
                'dc_orden_servicio' => '?',
                'dc_cliente' => '?',
                'dc_tipo_cliente' => '?',
                'dc_banco' => '?',
                'dc_banco_cobro' => '?',
                'dc_medio_cobro_cliente' => '?',
                'dc_mercado_cliente' => '?',
                'dg_cheque' => '?',
                'df_cheque' => '?',
                'dc_ejecutivo' => '?'
            )));
            $detalle_analitico->bindParam(1 , $this->parametrosAnaliticos->dc_cuenta_contable, PDO::PARAM_INT);
            $detalle_analitico->bindParam(2 , $this->parametrosAnaliticos->dq_debe, PDO::PARAM_STR);
            $detalle_analitico->bindParam(3 , $this->parametrosAnaliticos->dq_haber, PDO::PARAM_STR);
            $detalle_analitico->bindParam(4 , $this->parametrosAnaliticos->dg_glosa, PDO::PARAM_STR);
            $detalle_analitico->bindParam(5 , $this->parametrosAnaliticos->dc_detalle_financiero, PDO::PARAM_INT);
            $detalle_analitico->bindParam(6 , $this->parametrosAnaliticos->dc_factura_venta, PDO::PARAM_INT);
            $detalle_analitico->bindParam(7 , $this->parametrosAnaliticos->dc_nota_venta, PDO::PARAM_INT);
            $detalle_analitico->bindParam(8 , $this->parametrosAnaliticos->dc_orden_servicio, PDO::PARAM_INT);
            $detalle_analitico->bindParam(9 , $this->parametrosAnaliticos->dc_cliente, PDO::PARAM_INT);
            $detalle_analitico->bindParam(10, $this->parametrosAnaliticos->dc_tipo_cliente, PDO::PARAM_INT);
            $detalle_analitico->bindParam(11, $this->parametrosAnaliticos->dc_banco, PDO::PARAM_INT);
            $detalle_analitico->bindParam(12, $this->parametrosAnaliticos->dc_banco_cobro, PDO::PARAM_INT);
            $detalle_analitico->bindParam(13, $this->parametrosAnaliticos->dc_medio_cobro_cliente, PDO::PARAM_INT);
            $detalle_analitico->bindParam(14, $this->parametrosAnaliticos->dc_mercado_cliente, PDO::PARAM_INT);
            $detalle_analitico->bindParam(15, $this->parametrosAnaliticos->dg_cheque, PDO::PARAM_STR);
            $detalle_analitico->bindParam(16, $this->parametrosAnaliticos->df_cheque, PDO::PARAM_STR);
            $detalle_analitico->bindParam(17, $this->parametrosAnaliticos->dc_ejecutivo, PDO::PARAM_INT);
            
            return $detalle_analitico;
            
        }
        
        private function insertarDAFacturasPago($st){
            $db = $this->getConnection();
            $request = self::getRequest();
            
            $this->parametrosAnaliticos->dc_cuenta_contable = $this->cliente->dc_cuenta_contable;
            $this->parametrosAnaliticos->dq_debe = 0;
            $this->parametrosAnaliticos->dg_glosa = "Pago Facturas Cliente";
            $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['facturas'];
            $this->parametrosAnaliticos->dc_banco = NULL;
            $this->parametrosAnaliticos->dc_banco_cobro = NULL;
            $this->parametrosAnaliticos->dg_cheque = NULL;
            $this->parametrosAnaliticos->df_cheque = NULL;
            
            foreach($request->gestion_dc_factura as $i => $f){
              
                $factura = $db->getRowById('tb_factura_venta', $f, 'dc_factura');
                $this->parametrosAnaliticos->dq_haber = $this->dq_monto_pagar[$i];
                $this->parametrosAnaliticos->dc_factura_venta = $f;
                $this->parametrosAnaliticos->dc_nota_venta = $factura->dc_nota_venta;
                $this->parametrosAnaliticos->dc_orden_servicio = $factura->dc_orden_servicio;
                $this->parametrosAnaliticos->dc_ejecutivo = $factura->dc_ejecutivo;
                
                $db->stExec($st);
            }
            
        }
        
        private function insertarDAMedioPago($st){
            $this->parametrosAnaliticos->dq_haber = 0;
            $this->parametrosAnaliticos->dg_glosa = "Pago Cliente Medio Pago: {$this->medioCobro->dg_medio_cobro}";
            $this->parametrosAnaliticos->dc_factura_venta = NULL;
            $this->parametrosAnaliticos->dc_nota_venta = NULL;
            $this->parametrosAnaliticos->dc_orden_servicio = NULL;
            $this->parametrosAnaliticos->dc_ejecutivo = NULL;
            
            
            //Medio de pago con banco y documento
            if($this->medioCobro->dm_requiere_banco == 1 and
               $this->medioCobro->dm_requiere_documento == 1 and
               $this->medioCobro->dm_asiento_intermedio == 1){
                  return $this->insertarDAMPConBancoConDocumento($st);
            }else
            //Medio de pago con banco y sin documento
            if($this->medioCobro->dm_requiere_banco == 1 and
               $this->medioCobro->dm_requiere_documento == 0 and
               $this->medioCobro->dm_asiento_intermedio == 0){
                  return $this->insertarDAMPConBancoNoDocumento($st);
            }else
            //Medio de pago sin banco ni documento  
            if($this->medioCobro->dm_requiere_banco == 0 and
               $this->medioCobro->dm_requiere_documento == 0 and
               $this->medioCobro->dm_asiento_intermedio == 0){
                  return $this->insertarDAMPNoBancoNoDocumento($st);
            }else{
              //Sin implementar
              $this->getErrorMan()->showWarning("Las opciones del medio de pago son erroneas y no han sido implementadas");
              $this->getConnection()->rollBack();
              exit;
            }
        }
        
            private function insertarDAMPConBancoConDocumento($st){
                $request = self::getRequest();
                $db = $this->getConnection();
                $this->parametrosAnaliticos->dc_cuenta_contable = $this->medioCobro->dc_cuenta_contable_intermedia;
                $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['medio_pago'];
                
                foreach($request->dq_monto as $i => $dq_monto){
                    $this->parametrosAnaliticos->dq_debe = floatval($dq_monto);
                    $this->parametrosAnaliticos->dc_banco = $request->dc_banco[$i];
                    $this->parametrosAnaliticos->dc_banco_cobro = $request->dc_banco_origen[$i];
                    $this->parametrosAnaliticos->dg_cheque = $request->dg_documento[$i];
                    $this->parametrosAnaliticos->df_cheque = $db->sqlDate2($request->df_emision[$i]);
                    
                    $db->stExec($st);
                }
            }
            
            private function insertarDAMPConBancoNoDocumento($st){
                $request = self::getRequest();
                $db = $this->getConnection();
                
                $this->parametrosAnaliticos->dg_cheque = NULL;
                $this->parametrosAnaliticos->df_cheque = NULL;
                
                foreach($request->dq_monto as $i => $dq_monto){
                    $banco = $db->getRowById('tb_banco', $request->dc_banco[$i], 'dc_banco');
                    $this->parametrosAnaliticos->dc_cuenta_contable = $banco->dc_cuenta_contable;
                    $this->parametrosAnaliticos->dq_debe = floatval($dq_monto);
                    $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['medio_pago'][$banco->dc_cuenta_contable];
                    $this->parametrosAnaliticos->dc_banco = $banco->dc_banco;
                    $this->parametrosAnaliticos->dc_banco_cobro = $request->dc_banco_origen[$i];
                    
                    $db->stExec($st);
                }
            }
            
            private function insertarDAMPNoBancoNoDocumento($st){
                $request = self::getRequest();
                $db = $this->getConnection();
                $this->parametrosAnaliticos->dc_cuenta_contable = $request->dc_cuenta_contable;
                $this->parametrosAnaliticos->dq_debe = array_sum($this->dq_monto_pagar);
                $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['medio_pago'];
                $this->parametrosAnaliticos->dc_banco = NULL;
                $this->parametrosAnaliticos->dc_banco_cobro = NULL;
                $this->parametrosAnaliticos->dg_cheque = NULL;
                $this->parametrosAnaliticos->df_cheque = NULL;
                
                $db->stExec($st);
            }

    private function actualizarPagosFactura(){
        $db = $this->getConnection();

        //Actualizar pagos en la factura
        $update_factura = $db->prepare($db->update('tb_factura_venta',array(
            'dq_monto_pagado' => 'dq_monto_pagado+?'
        ),'dc_factura = ?'));
        $update_factura->bindParam(1,$dq_monto,PDO::PARAM_STR);
        $update_factura->bindParam(2,$dc_factura,PDO::PARAM_INT);
        
        foreach(self::getRequest()->gestion_dc_factura as $i => $f){
          $dc_factura = $f;
          $dq_monto = $this->dq_monto_pagar[$i];
          
          $db->stExec($update_factura);
        }
    }
    
    private function actualizarAnalisisCliente(){
      
        //Para actualizar los datos del cliente
        $tiempo_pago_cliente = 0;
        $facturas_pagadas = 0;
        
        $db = $this->getConnection();
        
        $request = self::getRequest();
      
        foreach($request->gestion_dc_factura as $i => $f){
            $dq_monto_maximo = $this->toNumber($request->max_value[$i]);
            $dq_monto = $this->dq_monto_pagar[$i];
	
            //Comprobar si con este pago la factura queda completamente saldada
            if($dq_monto == $dq_monto_maximo){
                $df_emision_factura = $request->gestion_df_emision_factura[$i];
                $periodo = $db->doQuery($db->select('dual',"DATEDIFF(NOW(),'{$df_emision_factura}') periodo"));
                $tiempo_pago_cliente += intval($periodo->fetch(PDO::FETCH_OBJ)->periodo);
                $facturas_pagadas++;
            }
        }
        
        /**
        *	En caso en que hayan facturas pagadas completamente se debe actualizar en la tabla de cliente
        *	indicando los días que demoró en pagar y la cantidad de facturas que fueron pagadas completamente
        *	Esto es usado como parámetro en el módulo de análisis del cliente.
        */
        if($facturas_pagadas > 0){
            $db->doExec($db->update('tb_cliente',array(
                'dc_dias_pago' => "dc_dias_pago+$tiempo_pago_cliente",
                'dc_facturas_pagadas' => "dc_facturas_pagadas+{$facturas_pagadas}"
            ),"dc_cliente={$this->cliente->dc_cliente}"));
        }
    }
    
    private function insertarDetallesDocumentos(){
        
        $db = $this->getConnection();
        $request = self::getRequest();
      
        $detalle_pago = $db->prepare($db->insert('tb_detalle_pago_cliente',array(
            'dc_comprobante' => $this->dc_comprobante,
            'dg_documento' => '?',
            'df_vencimiento' => '?',
            'dc_banco' => '?',
            'df_emision' => '?',
            'dq_monto' => '?',
            'dc_banco_cobro' => '?',
            'dg_codigo_seguro' => '?',
            'df_creacion' => $db->getNow()
        )));

        //Asignar Documento
        if($this->medioCobro->dm_requiere_documento == 1){
            $detalle_pago->bindParam(1,$dg_documento,PDO::PARAM_STR);
            $detalle_pago->bindParam(2,$df_vencimiento,PDO::PARAM_STR);
        }else{
            $detalle_pago->bindValue(1,NULL,PDO::PARAM_NULL);
            $detalle_pago->bindValue(2,NULL,PDO::PARAM_NULL);
        }

        if($this->medioCobro->dm_requiere_banco == 1){
            $detalle_pago->bindParam(3,$dc_banco,PDO::PARAM_INT);
        }else{
            $detalle_pago->bindValue(3,NULL,PDO::PARAM_NULL);
        }

        $detalle_pago->bindParam(4,$df_emision,PDO::PARAM_STR);
        $detalle_pago->bindParam(5,$dq_monto,PDO::PARAM_STR);
        $detalle_pago->bindParam(6,$dc_banco_cobro,PDO::PARAM_INT);
        $detalle_pago->bindParam(7,$dg_codigo_seguro,PDO::PARAM_STR);

        foreach($request->dq_monto as $i => $m){

            if($this->medioCobro->dm_requiere_documento == 1){
                $dg_documento = $request->dg_documento[$i];
                $df_vencimiento = $db->sqlDate2($request->df_vencimiento[$i]);
            }

            if($this->medioCobro->dm_requiere_banco == 1){
                $dc_banco = $request->dc_banco[$i];
            }

            $df_emision = $db->sqlDate2($request->df_emision[$i]);
            $dq_monto = floatval($m);
            $dc_banco_cobro = intval($request->dc_banco_origen[$i]);
            $dg_codigo_seguro = $request->dg_codigo_seguro[$i];

            $db->stExec($detalle_pago);

        }
    }
    
    private function validarPeriodoContable(){
      $this->mesContable = substr(self::getRequest()->df_fecha_contable,3,2);
      $this->anhoContable = substr(self::getRequest()->df_fecha_contable,-4);
      $estado = ContabilidadStuff::getEstadoPeriodo(
                        $this->mesContable,
                        $this->anhoContable,
                        ContabilidadStuff::FIELD_ESTADO_CT,
                        $this->getConnection(),
                        $this->getEmpresa(),
                        $this->getUserData()->dc_usuario
                );
      
      switch($estado){
        case ContabilidadStuff::ERROR_PERIODO_INEXISTENTE:
          $this->getErrorMan()->showWarning('No existe el periodo en el que intenta crear el comprobante');
          exit;
        case ContabilidadStuff::ERROR_PERIODO_CERRADO:
          $this->getErrorMan()->showWarning('El periodo contable seleccionado está cerrado');
          exit;
        case ContabilidadStuff::ERROR_PERIODO_USUARIO_DENEGADO:
          $this->getErrorMan()->showWarning('No tiene permisos para generar comprobantes en este periodo, consulte con el encargado');
          exit;
        case ContabilidadStuff::ERROR_PERIODO_GRUPO_DENEGADO:
          $this->getErrorMan()->showWarning('Su grupo de usuario no tiene permiso de generar comprobantes en este periodo, consulte con el encargado');
          exit;
      }
      
      return true;
    }
    
    private function formatearMontos(){
      $request = self::getRequest();
      $db = $this->getConnection();
      //Formatear y validar montos a float
      $dq_monto_pagar = array();
      foreach($request->gestion_dq_monto as $i => $dq_monto){
          $dq_monto_pagar[$i] = $this->toNumber($dq_monto);
          if(!$dq_monto_pagar[$i]){
              $this->getErrorMan()->showWarning('El monto '.$dq_monto.' es inválido, compruebe que sea un número y vuelva a intentarlo.');
              exit;
          }
          
          $factura = $db->getRowById('tb_factura_venta', $request->gestion_dc_factura[$i], 'dc_factura');

          if($factura === false || $factura->dc_empresa != $this->getEmpresa()){
              $this->getErrorMan()->showWarning('Error inesperado, Factura de venta no encontrada');
              exit;
          }

          if($dq_monto_pagar[$i] > floatval($factura->dq_total-$factura->dq_monto_pagado)){
              $this->getErrorMan()->showWarning('El monto '.$dq_monto.' supera el monto máximo a pagar para la factura');
              exit;
          }
      }
      
      return $dq_monto_pagar;
    }
    
    private function validarBancos(){
      //Validar que existan bancos si son necesarios
      if($this->medioCobro->dm_requiere_banco == 1){
          if(!isset(self::getRequest()->dc_banco)){
              $this->getErrorMan()->showWarning('Ocurrió un error inesperado, el medio de cobro requiere banco pero no hay bancos seleccionados en el pago.');
              exit;
          }
      }
    }
    
    private function validarDocumentos(){
      //Validar que los documentos y fechas de vencimientos existan si son necesarios
      if($this->medioCobro->dm_requiere_documento == 1){
          if(!isset(self::getRequest()->dg_documento)){
              $this->getErrorMan()->showWarning('Ocurrió un error inesperado, el medio de cobro requiere documento pero no hay documentos seleccionados en el pago.');
              exit;
          }

          if(!isset(self::getRequest()->df_vencimiento)){
              $this->getErrorMan()->showWarning('Ocurrió un error inesperado, el medio de cobro requiere documento con su fecha de vencimiento y estas no fueron encontradas.');
              exit;
          }
      }
    }
    
    private function validarTotalesDocumentos(){
      if($this->medioCobro->dm_requiere_documento == 1 || $this->medioCobro->dm_requiere_banco == 1){
          if(array_sum($this->dq_monto_pagar) != array_sum(self::getRequest()->dq_monto)){
              $this->getErrorMan()->showWarning('Los montos de los detalles de documentos y bancos no cuadran con los de las facturas a pagar. compruebe los valores y vuelva a intentarlo.');
              exit;
          }
      }
    }
    
    private function getMedioCobro($dc_medio_cobro){
        
        $db = $this->getConnection();
        
        $medio = $db->getRowById('tb_medio_cobro_cliente', $dc_medio_cobro, 'dc_medio_cobro');
        
        if($medio === false || $medio->dc_empresa != $this->getEmpresa()){
            $this->getErrorMan()->showWarning('El medio de cobro seleccionado es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
            exit;
        }
        
        return $medio;
    }
    
    private function getCliente($dc_cliente){
        $db = $this->getConnection();
        
        $cliente = $db->getRowById('tb_cliente', $dc_cliente, 'dc_cliente');
        
        if($cliente === false || $cliente->dc_empresa != $this->getEmpresa()){
          $this->getErrorMan()->showWarning('El cliente en las facturas en invalido, compruebe los datos e entrada y vuelva a intentarlo');
          exit;
        }

	if($cliente->dc_cuenta_contable == 0){
	  $this->getErrorMan()->showWarning('El cliente no tiene asociada una cuenta contable, compruebe los datos y vuelva a intentarlo');
	  exit;
	}
        
        return $cliente;
    }
    
    private function toNumber($number){
        $c = $this->getParametrosEmpresa();
        return floatval(str_replace($c->dm_separador_decimal,'.',str_replace($c->dm_separador_miles,'',$number)));
    }
  
}

?>
