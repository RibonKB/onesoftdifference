<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dias = array(
	1 => 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'
);

?>
<div id="secc_bar">
	Informe de cobranza de facturas
</div>
<div id="main_cont">
	<div class="panes">

	<?php $form->Start('sites/finanzas/cobros/proc/src_informe_cobranza.php','src_informe') ?>
    <?php $form->Header('Indique los filtros para ver los resultados del informe de cobranza') ?>

    <table class="tab" style="text-align:left;" id="form_container" width="100%">
        <tbody>
        	<tr>
            	<td>Fecha Emisión</td>
                <td><?php $form->Date('Desde','df_emision_desde') ?></td>
                <td><?php $form->Date('Hasta','df_emision_hasta') ?></td>
            </tr>
            <tr>
            	<td>Cliente</td>
                <td><?php $form->ListadoMultiple('Clientes','inf_clientes','tb_cliente',array('dc_cliente','dg_razon')) ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
            	<td>Tipo Cliente</td>
                <td><?php $form->ListadoMultiple('Tipo de cliente','inf_tipo_cliente','tb_tipo_cliente',array('dc_tipo_cliente','dg_tipo_cliente')) ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
            	<td>Días de pago "por vencer" [*]</td>
                <td><?php $form->Text('','inf_por_vencer',1,2,7) ?><br /></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
            	<td>Tipo de operación</td>
                <td><?php $form->ListadoMultiple('','inf_tipo_operacion','tb_tipo_operacion',array('dc_tipo_operacion','dg_tipo_operacion')) ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
            	<td>Días atención</td>
                <td><?php $form->Multiselect('','dc_dias_atencion',$dias) ?></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
            	<td>Horario Atención</td>
                <td><?php $form->Time('Desde','dt_horario_desde',false) ?></td>
                <td><?php $form->Time('Hasta','dt_horario_hasta',false) ?></td>
            </tr>
            <tr>
            	<td>Estado informe de cobranza</td>
                <td><?php $form->Radiobox('','dm_cobranza',array(1 => 'Confirmadas', 0 => 'Sin confirmar'),1) ?></td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>

<?php $form->End('Ejecutar') ?>
</div></div>
<script type="text/javascript">
$('#inf_clientes,#inf_tipo_cliente,#inf_tipo_operacion,#dc_dias_atencion').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>