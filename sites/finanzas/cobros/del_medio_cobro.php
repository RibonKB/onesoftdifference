<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_medio_cobro = intval($_POST['id']);

$medio_cobro = $db->prepare($db->select('tb_medio_cobro_cliente',
	'dg_medio_cobro, dm_activo','dc_medio_cobro = ? AND dc_empresa = ?'));
$medio_cobro->bindValue(1,$dc_medio_cobro,PDO::PARAM_INT);
$medio_cobro->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($medio_cobro);

$medio_cobro = $medio_cobro->fetch(PDO::FETCH_OBJ);

if($medio_cobro === false){
	$error_man->showWarning('El medio de cobro seleccionado no fue encontrado');
	exit;
}


?>


<div class="secc_bar" >
	Eliminar medios de cobro
</div>

<div class="panes" >
	<?php $form->Start('sites/finanzas/cobros/proc/delete_medio_cobro.php', 'del_medio_cobro') ?>
    <?php $form->Header('¿Está seguro que desea eliminar el medio de cobro?') ?>
    
    <div class="center" >
    	<?php echo $medio_cobro->dg_medio_cobro ?>
    </div>
    <?php $form->Hidden('dc_medio_cobro_ed', $dc_medio_cobro)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>
