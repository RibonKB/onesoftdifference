<?php
define('MAIN',1);
require_once("../../../inc/global.php");
require_once("../../../inc/form-class.php");

$form = new Form($empresa);
?>
<div id="secc_bar">
	Confirmación de Cobros a Clientes
</div>
<div id="main_cont">
	<div class="panes">
		<?php $form->Start('sites/finanzas/cobros/proc/src_giros.php','src_giros') ?>
		<?php $form->Header('Indique los filtros para buscar cobros a clientes por confirmar') ?>
		<table class="tab" width="100%">
			<tbody>
				<tr>
					<td>Número de comprobante de pago</td>
					<td><?php $form->Text('Desde','dq_comprobante_desde') ?></td>
					<td><?php $form->Text('hasta','dq_comprobante_hasta') ?></td>
				</tr>
				<tr>
					<td>Fecha de Pago</td>
					<td><?php $form->Date('Desde','df_pago_desde') ?></td>
					<td><?php $form->Date('Hasta','df_pago_hasta') ?></td>
				</tr>
				<tr>
					<td>Números de Documento</td>
					<td><?php $form->Text('Separados con coma','dg_documento') ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Fecha emisión comprobante</td>
					<td><?php $form->Date('Desde','df_comprobante_emision_desde',1,'01/'.date('m/Y')) ?></td>
					<td><?php $form->Date('Hasta','df_comprobante_emision_hasta',1,0) ?></td>
				</tr>
				<tr>
					<td>Cliente</td>
					<td><?php $form->ListadoMultiple('','dc_cliente','tb_cliente',array('dc_cliente','dg_razon')); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Banco</td>
					<td><?php $form->ListadoMultiple('','dc_banco','tb_banco',array('dc_banco','dg_banco')); ?></td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
		<?php $form->End('Ejecutar Búsqueda','searchbtn') ?>
	</div>
</div>