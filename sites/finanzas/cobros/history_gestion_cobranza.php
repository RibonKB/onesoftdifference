<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_factura_venta_gestion WHERE dc_factura={$_POST['id']}) g
JOIN tb_contacto_cliente cc ON cc.dc_contacto = g.dc_contacto
LEFT JOIN tb_medio_pago mp ON mp.dc_medio_pago = g.dc_medio_pago
LEFT JOIN tb_banco b ON b.dc_banco = g.dc_banco
LEFT JOIN tb_factura_venta_gestion_documento doc ON g.dc_gestion = doc.dc_gestion",
'DATE_FORMAT(g.df_emision,"%d/%m/%Y") AS df_emision, g.dq_monto, DATE_FORMAT(g.df_compromiso,"%d/%m/%Y") AS df_compromiso,
DATE_FORMAT(g.df_proxima_gestion,"%d/%m/%Y %H:%i") AS df_proxima_gestion,cc.dg_contacto,cc.dg_email,cc.dg_fono,
g.dg_comentario,mp.dg_medio_pago,b.dg_banco,GROUP_CONCAT(doc.dg_documento) AS dg_documentos',
'',array('group_by' => 'g.dc_gestion'));

echo("<div class='panes'>");
if(count($data)){

	echo("<table class='tab' width='100%'>
	<caption>Gestión de cobranza realizadas sobre la factura</caption>
	<thead><tr>
		<th width='150'>Contacto</th>
		<th width='90'>Monto</th>
		<th>Fecha Gestión</th>
		<th>Fecha Compromiso</th>
		<th>Fecha próxima gestión</th>
		<th>Medio de pago</th>
		<th width='150'>Banco</th>
		<th>Documentos</th>
		<th>Comentario</th>
	</thead><tbody>");
	foreach($data as $d){
		
		$d['dq_monto'] = moneda_local($d['dq_monto']);
		
		echo("<tr>
			<td>{$d['dg_contacto']}</td>
			<td>{$d['dq_monto']}</td>
			<td>{$d['df_emision']}</td>
			<td>{$d['df_compromiso']}</td>
			<td>{$d['df_proxima_gestion']}</td>
			<td>{$d['dg_medio_pago']}</td>
			<td>{$d['dg_banco']}</td>
			<td>{$d['dg_documentos']}</td>
			<td>{$d['dg_comentario']}</td>
		</tr>");
	}
	echo("</tbody></table>");

}else{
	$error_man->showAviso('No se han encontrado gestiones realizadas sobre la factura');
}
?>
</div>