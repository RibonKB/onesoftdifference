<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$df_emision_comprobante_desde = $db->sqlDate($_POST['df_comprobante_emision_desde']);
$df_emision_comprobante_hasta = $db->sqlDate($_POST['df_comprobante_emision_hasta'].' 23:59');

$cond = "c.dc_empresa = {$empresa} AND (c.df_emision BETWEEN {$df_emision_comprobante_desde} AND {$df_emision_comprobante_hasta}) AND c.dm_nula = 0";

//Filtrar por número de propuesta.
if($_POST['dq_comprobante_desde']){
	$dq_comprobante_desde = intval($_POST['dq_comprobante_desde']);
	if(!$dq_comprobante_desde){
		$error_man->showWarning('El número de comprobante indicado es inválido');
		exit;
	}
	if($_POST['dq_comprobante_hasta']){
		$dq_comprobante_hasta = intval($_POST['dq_comprobante_hasta']);
		if(!$dq_comprobante_hasta){
			$error_man->showWarning('El número de comprobante indicado es inválido');
			exit;
		}
		$cond .= " AND (c.dq_comprobante BETWEEN {$dq_comprobante_desde} AND {$dq_comprobante_hasta})";
	}else{
		$cond = "c.dq_comprobante = {$dq_comprobante_desde} AND dc_empresa = {$empresa}";
	}
}

//Filtrar por fecha de pago (la que aparece en la administración de comprobantes)
if(trim($_POST['df_pago_desde'])){
	$df_pago_desde = $db->sqlDate(trim($_POST['df_pago_desde']));
	if(trim($_POST['df_pago_desde'])){
		$df_pago_hasta = $db->sqlDate(trim($_POST['df_pago_hasta']));
		$cond .= " AND (p.df_emision BETWEEN {$df_pago_desde} AND {$df_pago_hasta})";
	}else{
		$cond .= " AND p.df_emision = {$df_pago_desde}";
	}
}

//Filtrar por documentos de pago
$dg_documento = trim($_POST['dg_documento']);
if($dg_documento){
	$documentos = explode(',',$dg_documento);
	$cond .= 'p.dg_documento IN(';
	foreach($documentos as $d){
		$cond .= $db->quote($d).',';
	}
	$cond = substr($cond,0,-1).')';
}

//Filtrar por cliente
if(isset($_POST['dc_cliente'])){
	$dc_cliente = implode(',',$_POST['dc_cliente']);
	$cond .= " AND c.dc_cliente IN ({$dc_cliente})";
}

//Filtrar por banco
if(isset($_POST['dc_banco'])){
	$dc_banco = implode(',',$_POST['dc_banco']);
	$cond .= " AND p.dc_banco IN ({$dc_banco})";
}

/*Estado
if(isset($_POST['dm_terminados'])){
	$cond .= ' AND p.dm_estado IN ("C","T")';
}else{
	$cond .= ' AND p.dm_estado = "C"';
}/**/

//Condiciones extra
$cond .= " AND m.dm_asiento_intermedio = '1'";

$data = $db->doQuery($db->select('tb_comprobante_pago_cliente c
JOIN tb_detalle_pago_cliente p ON p.dc_comprobante = c.dc_comprobante
JOIN tb_medio_cobro_cliente m ON m.dc_medio_cobro = c.dc_medio_cobro
JOIN tb_banco b ON b.dc_banco = p.dc_banco
JOIN tb_cliente cl ON cl.dc_cliente = c.dc_cliente',
'c.dq_comprobante, c.df_emision, p.dc_detalle, p.dq_monto, p.dg_documento, p.df_vencimiento, p.df_emision,
m.dg_medio_cobro, b.dg_banco, cl.dg_razon, cl.dg_rut, p.dm_estado, c.dg_comentario',$cond));

?>
<table class="tab" width="100%">
<caption>Pagos pendientes de confirmación</caption>
<thead>
	<tr>
		<th>Documento</th>
		<th>Banco</th>
		<th>Fecha Emisión</th>
		<th>Fecha vencimiento</th>
		<th>N° Propuesta</th>
		<th>Comentario</th>
		<th>Monto</th>
		<th>&nbsp;</th>
	</tr>
</thead>
<tbody>
	<?php while($d = $data->fetch(PDO::FETCH_OBJ)): ?>
	<tr>
		<td><?php echo $d->dg_documento ?></td>
		<td><?php echo $d->dg_banco ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_emision) ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_vencimiento) ?></td>
		<td><?php echo $d->dq_comprobante ?></td>
		<td><?php echo $d->dg_comentario ?></td>
		<td align="right"><?php echo moneda_local($d->dq_monto) ?></td>
		<td>
		<?php if($d->dm_estado == 'L'): ?>
			<a href="sites/finanzas/cobros/confirm_document.php?id=<?php echo $d->dc_detalle ?>" class="loadOnOverlay">
				<img src="images/confirm.png" alt="[C]" title="Confirmar cobro" />
			</a>
		<?php endif; ?>
		</td>
	</tr>
	<?php endwhile; ?>
</tbody>
</table>
<script type="text/javascript">
	pymerp.init($('#src_giros_res'));
</script>