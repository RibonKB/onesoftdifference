<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

//df_proxima_gestion
if($_POST['gestion_prox_gestion_hora'] == 12) $_POST['gestion_prox_gestion_hora'] = 0;
$hora = str_pad($_POST['gestion_prox_gestion_hora']+$_POST['gestion_prox_gestion_meridiano'],2,0,STR_PAD_LEFT).":".$_POST['gestion_prox_gestion_minuto'];
$_POST['gestion_prox_gestion'] = $formatedNextDate = $_POST['gestion_prox_gestion']." ".$hora;
unset($hora);

//Insertar gestión en la factura
$insert_gestion = $db->prepare($db->insert('tb_factura_venta_gestion',array(
	'dc_factura' => '?',
	'dc_contacto' => '?',
	'df_compromiso' => '?',
	'df_proxima_gestion' => '?',
	'dg_comentario' => '?',
	'df_emision' => $db->getNow(),
	'dc_usuario_creacion' => $idUsuario
)));
$insert_gestion->bindParam(1,$dc_factura,PDO::PARAM_INT);
$insert_gestion->bindValue(2,$_POST['gestion_contacto'],PDO::PARAM_INT);
$insert_gestion->bindValue(3,$db->sqlDate2($_POST['gestion_compromiso']),PDO::PARAM_STR);
$insert_gestion->bindValue(4,$db->sqlDate2($_POST['gestion_prox_gestion']),PDO::PARAM_STR);
$insert_gestion->bindValue(5,$_POST['gestion_comentario'],PDO::PARAM_STR);

//Cambiar fecha de próxima gestión para la factura
$update_factura = $db->prepare($db->update('tb_factura_venta',array(
	'df_proxima_gestion' => '?'
),'dc_factura = ?'));
$update_factura->bindValue(1,$db->sqlDate2($_POST['gestion_prox_gestion']),PDO::PARAM_STR);
$update_factura->bindParam(2,$dc_factura,PDO::PARAM_INT);

$db->start_transaction();
	foreach($_POST['gestion_dc_factura'] as $i => $dc_factura){
		$db->stExec($insert_gestion);
		$db->stExec($update_factura);
	}
$db->commit();

$error_man->showConfirm('Se ha almacenado la gestión sobre las facturas seleccionadas');

?>