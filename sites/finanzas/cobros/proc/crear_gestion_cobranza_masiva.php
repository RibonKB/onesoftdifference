<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once("../../../ventas/proc/ventas_functions.php");
require_once("../../../contabilidad/stuff.class.php");

//Validar medio de cobro
$dc_medio_cobro = explode(',',$_POST['dc_medio_cobro']);
$dc_medio_cobro = intval(array_shift($dc_medio_cobro));

$medio_cobro = $db->prepare(
	$db->select('tb_medio_cobro_cliente',
	'dc_tipo_movimiento, dm_requiere_banco, dm_asiento_intermedio, dm_requiere_documento, dc_cuenta_contable_intermedia',
	'dc_medio_cobro = ? AND dc_empresa = ?')
);
$medio_cobro->bindValue(1,$dc_medio_cobro,PDO::PARAM_INT);
$medio_cobro->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($medio_cobro);
$medio_cobro = $medio_cobro->fetch(PDO::FETCH_OBJ);

if($medio_cobro === false){
	$error_man->showWarning('El medio de cobro seleccionado es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//validar cliente
$dc_cliente = intval($_POST['gestion_dc_cliente']);

$cliente = $db->prepare($db->select('tb_cliente','dc_cuenta_contable, dg_razon','dc_cliente = ? AND dc_empresa = ?'));
$cliente->bindValue(1,$dc_cliente,PDO::PARAM_INT);
$cliente->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($cliente);
$cliente = $cliente->fetch(PDO::FETCH_OBJ);

if($cliente === false){
	$error_man->showWarning('El cliente en las facturas en invalido, compruebe los datos e entrada y vuelva a intentarlo');
	exit;
}

if($cliente->dc_cuenta_contable == 0){
	$error_man->showWarning("No se ha configurado la cuenta contable de pagos para el cliente especificado");
	exit;
}

//Validar fecha contable dentro del periodo contable
$mes_contable = substr($_POST['df_fecha_contable'],3,2);
$anho_contable = substr($_POST['df_fecha_contable'],6);
$estado_periodo = ContabilidadStuff::getEstadoPeriodo($mes_contable, $anho_contable, ContabilidadStuff::FIELD_ESTADO_FV);

if($estado_periodo > 0){
	$error_man->showWarning('El periodo contable en el que se quiere realizar el pago no está abierto, contacte con el administrador.');
	exit;
}

//Formatear y validar montos a float
$dq_monto_pagar = array();
foreach($_POST['gestion_dq_monto'] as $i => $dq_monto){
	$dq_monto_pagar[$i] = toNumber($dq_monto);
	if(!$dq_monto_pagar[$i]){
		$error_man->showWarning('El monto '.$dq_monto.' es inválido, compruebe que sea un número y vuelva a intentarlo.');
		exit;
	}
	
	$factura = $db->prepare($db->select('tb_factura_venta','dq_total-dq_monto_pagado dq_maximo','dc_factura = ? AND dc_empresa = ?'));
	$factura->bindValue(1,$_POST['gestion_dc_factura'][$i],PDO::PARAM_INT);
	$factura->bindValue(2,$empresa,PDO::PARAM_INT);
	$db->stExec($factura);
	$factura = $factura->fetch(PDO::FETCH_OBJ);
	
	if($factura === false){
		$error_man->showWarning('Error inesperado, Factura de venta no encontrada');
		exit;
	}
	
	if($dq_monto_pagar[$i] > floatval($factura->dq_maximo)){
		$error_man->showWarning('El monto '.$dq_monto.' supera el monto máximo a pagar para la factura');
		exit;
	}
}

//Validar que existan bancos si son necesarios
if($medio_cobro->dm_requiere_banco == 1){
	if(!isset($_POST['dc_banco'])){
		$error_man->showWarning('Ocurrió un error inesperado, el medio de cobro requiere banco pero no hay bancos seleccionados en el pago.');
		exit;
	}
}

//Validar que los documentos y fechas de vencimientos existan si son necesarios
if($medio_cobro->dm_requiere_documento == 1){
	if(!isset($_POST['dg_documento'])){
		$error_man->showWarning('Ocurrió un error inesperado, el medio de cobro requiere documento pero no hay documentos seleccionados en el pago.');
		exit;
	}
	
	if(!isset($_POST['df_vencimiento'])){
		$error_man->showWarning('Ocurrió un error inesperado, el medio de cobro requiere documento con su fecha de vencimiento y estas no fueron encontradas.');
		exit;
	}
}

if($medio_cobro->dm_requiere_documento == 1 || $medio_cobro->dm_requiere_banco == 1){
	if(array_sum($dq_monto_pagar) != array_sum($_POST['dq_monto'])){
		$error_man->showWarning('Los montos de los detalles de documentos y bancos no cuadran con los de las facturas a pagar. compruebe los valores y vuelva a intentarlo.');
		exit;
	}
}


$db->start_transaction();

//Insertar el comprobante de pago
$dq_comprobante_pago = doc_GetNextNumber('tb_comprobante_pago_cliente','dq_comprobante');

$comprobante_pago = $db->prepare($db->insert('tb_comprobante_pago_cliente',array(
	'dq_comprobante' => '?',
	'dc_cliente' => '?',
	'dc_medio_cobro' => '?',
	'df_emision' => $db->getNow(),
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
)));
$comprobante_pago->bindValue(1,$dq_comprobante_pago,PDO::PARAM_INT);
$comprobante_pago->bindValue(2,$dc_cliente,PDO::PARAM_INT);
$comprobante_pago->bindValue(3,$dc_medio_cobro,PDO::PARAM_INT);

$db->stExec($comprobante_pago);

$dc_comprobante_pago = $db->lastInsertId();

//Insertar detalles del comprobante de pago
$detalle_comprobante_pago = $db->prepare($db->insert('tb_detalle_factura_pago_cliente',array(
	'dc_comprobante' => $dc_comprobante_pago,
	'dc_factura' => '?',
	'dq_monto' => '?'
)));
$detalle_comprobante_pago->bindParam(1,$dc_factura,PDO::PARAM_INT);
$detalle_comprobante_pago->bindParam(2,$dq_monto,PDO::PARAM_STR);

//Actualizar pagos en la factura
$update_factura = $db->prepare($db->update('tb_factura_venta',array(
	'dq_monto_pagado' => 'dq_monto_pagado+?'
),'dc_factura = ?'));
$update_factura->bindParam(1,$dq_monto,PDO::PARAM_STR);
$update_factura->bindParam(2,$dc_factura,PDO::PARAM_INT);

//Para actualizar los datos del cliente
$tiempo_pago_cliente = 0;
$facturas_pagadas = 0;

foreach($_POST['gestion_dc_factura'] as $i => $f){
	$dc_factura = $f;
	$dq_monto = $dq_monto_pagar[$i];
	
	$db->stExec($detalle_comprobante_pago);
	$db->stExec($update_factura);
	
	$dq_monto_maximo = toNumber($_POST['max_value'][$i]);
	
	//Comprobar si con este pago la factura queda completamente saldada
	if($dq_monto == $dq_monto_maximo){
		$df_emision_factura = $_POST['gestion_df_emision_factura'][$i];
		$periodo = $db->doQuery($db->select('dual',"DATEDIFF(NOW(),'{$df_emision_factura}') periodo"));
		$tiempo_pago_cliente += intval($periodo->fetch(PDO::FETCH_OBJ)->periodo);
		$facturas_pagadas++;
	}
}

/**
*	En caso en que hayan facturas pagadas completamente se debe actualizar en la tabla de usuario
*	indicando los días que demoró en pagar y la cantidad de facturas que fueron pagadas completamente
*	Esto es usado como parámetro en el módulo de análisis del cliente.
*/
if($facturas_pagadas > 0){
	$db->doExec($db->update('tb_cliente',array(
		'dc_dias_pago' => "dc_dias_pago+$tiempo_pago_cliente",
		'dc_facturas_pagadas' => "dc_facturas_pagadas+{$facturas_pagadas}"
	),"dc_cliente={$dc_cliente}"));
}

/**
*	En caso en que el medio de cobro requiera el registro de documentos y/o bancos se insertan estos detalles en la tabla de detalles
*/
if($medio_cobro->dm_requiere_banco == 1 || $medio_cobro->dm_requiere_documento == 1){
	$detalle_pago = $db->prepare($db->insert('tb_detalle_pago_cliente',array(
		'dc_comprobante' => $dc_comprobante_pago,
		'dg_documento' => '?',
		'df_vencimiento' => '?',
		'dc_banco' => '?',
		'df_emision' => '?',
		'dq_monto' => '?',
		'dc_banco_cobro' => '?',
		'dg_codigo_seguro' => '?',
		'df_creacion' => $db->getNow()
	)));
	
	//Asignar Documento
	if($medio_cobro->dm_requiere_documento == 1){
		$detalle_pago->bindParam(1,$dg_documento,PDO::PARAM_STR);
		$detalle_pago->bindParam(2,$df_vencimiento,PDO::PARAM_STR);
	}else{
		$detalle_pago->bindValue(1,'',PDO::PARAM_STR);
		$detalle_pago->bindValue(2,NULL,PDO::PARAM_NULL);
	}
	
	if($medio_cobro->dm_requiere_banco == 1){
		$detalle_pago->bindParam(3,$dc_banco,PDO::PARAM_INT);
	}else{
		$detalle_pago->bindValue(3,0,PDO::PARAM_INT);
	}
	
	$detalle_pago->bindParam(4,$df_emision,PDO::PARAM_STR);
	$detalle_pago->bindParam(5,$dq_monto,PDO::PARAM_STR);
	$detalle_pago->bindParam(6,$dc_banco_cobro,PDO::PARAM_INT);
	$detalle_pago->bindParam(7,$dg_codigo_seguro,PDO::PARAM_STR);
	
	foreach($_POST['dq_monto'] as $i => $m){
		
		if($medio_cobro->dm_requiere_documento == 1){
			$dg_documento = $_POST['dg_documento'][$i];
			$df_vencimiento = $db->sqlDate2($_POST['df_vencimiento'][$i]);
		}
		
		if($medio_cobro->dm_requiere_banco == 1){
			$dc_banco = $_POST['dc_banco'][$i];
		}
		
		$df_emision = $db->sqlDate2($_POST['df_emision'][$i]);
		$dq_monto = floatval($m);
		$dc_banco_cobro = intval($_POST['dc_banco_origen'][$i]);
		$dg_codigo_seguro = $_POST['dg_codigo_seguro'][$i];
		
		$db->stExec($detalle_pago);
		
	}
	
}

/**
*	Almacenar asientos contables que realizará el pago
*	Esto comprende crear un comprobante contable con los asientos por factura e indicando el asiento contrario dependiendo del medio de cobro
*/
$dq_comprobante_contable = ContabilidadStuff::getCorrelativoComprobante();

//Insertar comprobante
$comprobante_contable = $db->prepare($db->insert('tb_comprobante_contable',array(
	'dg_comprobante' => $dq_comprobante_contable,
	'dc_tipo_movimiento' => '?',
	'df_fecha_contable' => '?',
	'dc_mes_contable' => '?',
	'dc_anho_contable' => '?',
	'dq_cambio' => '?',
	'dg_glosa' => '?',
	'dq_saldo' => array_sum($dq_monto_pagar),
	'dc_comprobante_pago' => $dc_comprobante_pago,
	'df_fecha_emision' => $db->getNow(),
	'dc_empresa' => $empresa,
	'dc_usuario_creacion' => $idUsuario
)));
$comprobante_contable->bindValue(1,$medio_cobro->dc_tipo_movimiento,PDO::PARAM_INT);
$comprobante_contable->bindValue(2,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
$comprobante_contable->bindValue(3,$mes_contable,PDO::PARAM_INT);
$comprobante_contable->bindValue(4,$anho_contable,PDO::PARAM_INT);
$comprobante_contable->bindValue(5,1,PDO::PARAM_INT);
$comprobante_contable->bindValue(6,"Cobro cliente: {$dq_comprobante_pago} - {$cliente->dg_razon}",PDO::PARAM_STR);

$db->stExec($comprobante_contable);

//obtener id del comprobante recién creado
$dc_comprobante_contable = $db->lastInsertId();

//Preparar sentencia para insertar detalle de comprobantes contables
$detalle_comprobante_contable = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
	'dc_comprobante' => $dc_comprobante_contable,
	'dc_cuenta_contable' => '?',
	'dq_debe' => '?',
	'dq_haber' => '?',
	'dg_glosa' => '?',
	'dg_cheque' => '?',
	'df_fecha_cheque' => '?',
	'dc_cliente' => $dc_cliente,
	'dc_banco' => '?',
	'dc_factura_venta' => '?',
	'dc_banco_cobro' => '?'
)));
$detalle_comprobante_contable->bindParam(1,$dc_cuenta_contable,PDO::PARAM_INT);
$detalle_comprobante_contable->bindParam(2,$dq_debe,PDO::PARAM_STR);
$detalle_comprobante_contable->bindParam(3,$dq_haber,PDO::PARAM_STR);
$detalle_comprobante_contable->bindParam(4,$dg_glosa,PDO::PARAM_STR);
$detalle_comprobante_contable->bindParam(5,$dg_cheque,PDO::PARAM_STR);
$detalle_comprobante_contable->bindParam(6,$df_cheque,PDO::PARAM_STR);
$detalle_comprobante_contable->bindParam(7,$dc_banco,PDO::PARAM_INT);
$detalle_comprobante_contable->bindParam(8,$dc_factura_venta,PDO::PARAM_INT);
$detalle_comprobante_contable->bindParam(9,$dc_banco_cobro,PDO::PARAM_INT);

//Insertar detalles de factura en HABER
/**
*	Van ligados a la cuenta contable del cliente (Deudores por venta).
*	Es una linea por factura de venta
*/
$dc_cuenta_contable = $cliente->dc_cuenta_contable;
$dq_debe = 0;
$dg_cheque = '';
$df_cheque = '';
$dc_banco = 0;

foreach($_POST['gestion_dc_factura'] as $i => $f){
	$dq_haber = $dq_monto_pagar[$i];
	$dc_factura_venta = $f;
	
	$factura = $db->doQuery($db->select('tb_factura_venta','dq_factura, dq_folio','dc_factura = '.$dc_factura_venta.' AND dc_empresa = '.$empresa))->fetch(PDO::FETCH_OBJ);
	
	if($factura === false){
		$error_man->showWarning('Error inesperado, factura erronea.');
		exit;
	}
	
	$dg_glosa = "Factura de venta: {$factura->dq_factura}, folio: {$factura->dq_folio}, cliente: {$cliente->dg_razon}";
	
	$db->stExec($detalle_comprobante_contable);
	
}

$dq_haber = 0;
$dc_factura_venta = 0;


//Insertar detalle de pago
/**
*	Esto depende del medio de cobro
*/

//Con banco y documento
if($medio_cobro->dm_asiento_intermedio == 1){
	
	$dc_cuenta_contable = $medio_cobro->dc_cuenta_contable_intermedia;
	
	if($medio_cobro->dm_requiere_banco == 1 && $medio_cobro->dm_requiere_documento){
		foreach($_POST['dq_monto'] as $i => $dq_monto){
			
			$banco = $db->prepare($db->select('tb_banco_cobro','dg_banco','dc_banco = ? AND dc_empresa = ?'));
			$banco->bindValue(1,$_POST['dc_banco'][$i],PDO::PARAM_INT);
			$banco->bindValue(2,$empresa,PDO::PARAM_INT);
			$db->stExec($banco);
			$banco = $banco->fetch(PDO::FETCH_OBJ);
			
			$dq_debe = floatval($dq_monto);
			$dg_glosa = "Cheque Banco: {$banco->dg_banco}, fecha: {$_POST['df_emision'][$i]}, Cliente {$cliente->dg_razon}";
			$dg_cheque = $_POST['dg_documento'][$i];
			$df_cheque = $db->sqlDate2($_POST['df_emision'][$i]);
			$dc_banco = $_POST['dc_banco'][$i];
			$dc_banco_cobro = $_POST['dc_banco_origen'][$i];
			
			$db->stExec($detalle_comprobante_contable);
			
		}
	}else{
		
		$dg_glosa = "Cobro Factura Cliente: {$cliente->dg_razon}";
		$dq_debe = array_sum($dq_monto_pagar);
		
	}
	
}else{
	
	if($medio_cobro->dm_requiere_banco == 1){
		
		foreach($_POST['dq_monto'] as $i => $dq_monto){
			
			$banco = $db->prepare($db->select('tb_banco','dc_cuenta_contable, dg_banco','dc_banco = ? AND dc_empresa = ?'));
			$banco->bindValue(1,$_POST['dc_banco'][$i],PDO::PARAM_INT);
			$banco->bindValue(2,$empresa,PDO::PARAM_INT);
			$db->stExec($banco);
			$banco = $banco->fetch(PDO::FETCH_OBJ);
			
			$dc_cuenta_contable = $banco->dc_cuenta_contable;
			$dc_banco = $_POST['dc_banco'][$i];
			$dc_banco_cobro = $_POST['dc_banco_origen'][$i];
			$dg_glosa = "Cobro Banco: {$banco->dg_banco}, Fecha: {$_POST['df_emision'][$i]}, Cliente: {$cliente->dg_razon}";
			$dq_debe = floatval($dq_monto);
			
			if($medio_cobro->dm_requiere_documento == 1){
				
				$dg_cheque = $_POST['dg_documento'][$i];
				$df_cheque = $db->sqlDate2($_POST['df_emision'][$i]);
				
			}
			
			$db->stExec($detalle_comprobante_contable);
			
		}
		
	}else{
		
		$dc_cuenta_contable = $_POST['dc_cuenta_contable'];
		
		if(!$dc_cuenta_contable){
			$error_man->showWarning('Error inesperado, no se encontró una cuenta contable en la que hacer el asiento, consulte co n el administrador.');
			$db->rollBack();
			exit;
		}
		
		$dq_debe = array_sum($dq_monto_pagar);
		$dg_glosa = "Cobro cliente: {$cliente->dg_razon}";
		
		$db->stExec($detalle_comprobante_contable);
		
	}
	
}

$db->commit();

$error_man->showConfirm("Se generó el pago correctamente");
$error_man->showInfo("El número del comprobante de pago generado es <h1 style='margin:0;color:#000;'>{$dq_comprobante_pago}</h1>
Además se generó el comprobante contable
<h1 style='margin:0;color:#000;'>{$dq_comprobante_contable}</h1>
haga <a href='sites/contabilidad/ver_comprobante.php?id={$dc_comprobante_contable}' target='_blank'>click aquí</a> para imprimirlo");

/*
//df_compromiso
$df_compromiso = $db->sqlDate($_POST['gestion_compromiso']);

//df_proxima_gestion
if($_POST['gestion_prox_gestion_hora'] == 12) $_POST['gestion_prox_gestion_hora'] = 0;
$hora = str_pad($_POST['gestion_prox_gestion_hora']+$_POST['gestion_prox_gestion_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_prox_gestion_minuto'];
$_POST['gestion_prox_gestion'] = $formatedNextDate = $_POST['gestion_prox_gestion']." ".$hora;
unset($hora);
$df_proxima_gestion = $db->sqlDate($_POST['gestion_prox_gestion']);

$db->start_transaction();

$conPagos = isset($_POST['gestion_dq_monto']);

$insert_gestion = $db->prepare($db->insert('tb_factura_venta_gestion',array(
	'dc_factura' => '?',
	'dc_contacto' => $_POST['gestion_contacto'],
	'df_emision' => $db->getNow(),
	'df_compromiso' => $df_compromiso,
	'df_proxima_gestion' => $df_proxima_gestion,
	'dc_medio_pago' => $conPagos?$_POST['gestion_medio_pago']:0,
	'dc_banco' => $conPagos?$_POST['gestion_banco']:0,
	'dg_comentario' => '?',
	'dc_usuario_creacion' => $idUsuario,
	'dq_monto' => $conPagos?'?':0
)));

$insert_gestion->bindParam(1,$dc_factura,PDO::PARAM_INT);
$insert_gestion->bindValue(2,$_POST['gestion_comentario'],PDO::PARAM_STR);

$to_update_factura = array(
	'df_proxima_gestion' => $df_proxima_gestion
);

if($conPagos)
	$to_update_factura['dq_monto_pagado'] = "dq_monto_pagado+:monto";

$update_factura = $db->prepare($db->update('tb_factura_venta',$to_update_factura,"dc_factura=:idFactura"));

$update_factura->bindParam(':idFactura',$dc_factura,PDO::PARAM_INT);

if($conPagos){
	$insert_gestion->bindParam(3,$dq_monto,PDO::PARAM_STR);
	$update_factura->bindParam(':monto',$dq_monto,PDO::PARAM_STR);

	if(isset($_POST['gestion_docs'])){
		$insert_documento = $db->prepare($db->insert('tb_factura_venta_gestion_documento',array(
			'dc_gestion' => '?',
			'dg_documento' => '?'
		)));
		
		$insert_documento->bindParam(1,$dc_gestion,PDO::PARAM_INT);
		$insert_documento->bindParam(2,$dg_documento,PDO::PARAM_STR);
	}
}

$tiempo_pago_cliente = 0;
$facturas_pagadas = 0;

foreach($_POST['gestion_dc_factura'] as $i => $factura){
	$dc_factura = $factura;
	
	if($conPagos)
		$dq_monto = toNumber($_POST['gestion_dq_monto'][$i]);
		
	$db->stExec($insert_gestion);
	
	if($conPagos && isset($_POST['gestion_docs'])){
		$dc_gestion = $db->lastInsertId();
		
		foreach($_POST['gestion_docs'] as $doc){
			$dg_documento = $doc;
			
			$db->stExec($insert_documento);
		}
	}
	
	$db->stExec($update_factura);
	
	$dq_monto_maximo = toNumber($_POST['max_value'][$i]);
	$df_emision_factura = $_POST['gestion_df_emision_factura'][$i];
	
	if($dq_monto_maximo == $dq_monto && $dq_monto_maximo > 0){
		$periodo = $db->doQuery($db->select('dual',"DATEDIFF(NOW(),'{$df_emision_factura}') periodo"));
		$tiempo_pago_cliente += intval($periodo->fetch(PDO::FETCH_OBJ)->periodo);
		$facturas_pagadas++;
	}
}

$dc_cliente = $_POST['gestion_dc_cliente'];

if($facturas_pagadas > 0){
	$db->doExec($db->update('tb_cliente',array(
		'dc_dias_pago' => "dc_dias_pago+$tiempo_pago_cliente",
		'dc_facturas_pagadas' => "dc_facturas_pagadas+{$facturas_pagadas}"
	),"dc_cliente={$dc_cliente}"));
}

$db->commit();

$error_man->showConfirm("Se han realizado los pagos de las facturas seleccionadas correctamente.");

?>
*/