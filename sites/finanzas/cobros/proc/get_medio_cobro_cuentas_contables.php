<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_medio_cobro = intval($_GET['dc_medio_cobro']);

$medio_cobro = $db->prepare($db->select('tb_medio_cobro_cliente','dc_tipo_movimiento','dc_medio_cobro = ? AND dc_empresa = ?'));
$medio_cobro->bindValue(1,$dc_medio_cobro,PDO::PARAM_INT);
$medio_cobro->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($medio_cobro);
$medio_cobro = $medio_cobro->fetch(PDO::FETCH_OBJ);

if($medio_cobro === false){
	echo json_encode('<not-found>');
	exit;
}

$cuentas = $db->prepare($db->select('tb_cuentas_tipo_movimiento t
JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = t.dc_cuenta_contable',
'c.dc_cuenta_contable, c.dg_cuenta_contable',
't.dc_tipo_movimiento = ? AND c.dc_empresa = ?'));
$cuentas->bindValue(1,$medio_cobro->dc_tipo_movimiento,PDO::PARAM_INT);
$cuentas->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($cuentas);

$cuentas = $cuentas->fetchAll();

if(!count($cuentas)){
	echo json_encode('<empty>');
	exit;
}

echo json_encode($cuentas);
?>