<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

if(!isset($_POST['dc_factura'])){
	$error_man->showWarning("No ha seleccionado facturas para ser habilitadas en el módulo de cobranza");
	exit;
}

$dc_factura = implode(',',$_POST['dc_factura']);

$db->doExec($db->update('tb_factura_venta',array(
	'dm_cobranza' => '"1"'
),"dc_factura IN ({$dc_factura})"));

$error_man->showConfirm('Se han habilitado las facturas seleccionadas para el proceso de cobro');

?>
<script type="text/javascript">
	$('.multipleManCheckbox:checked').each(function(index, element) {
       $(this).closest('tr').remove();
    });
</script>