<?php
define('MAIN',1);
require_once('../../../../inc/init.php');

$dc_tipo_movimiento = intval($_POST['dc_tipo_movimiento']);
$dm_requiere_banco = $_POST['dm_requiere_banco']==1?0:1;
$dm_asiento_intermedio = $_POST['dm_asiento_intermedio']==1?0:1;
$dm_requiere_documento = $_POST['dm_requiere_documento']==1?0:1;

$dc_banco = $dm_requiere_banco?intval($_POST['dc_banco']):0;
$dc_cuenta_intermedia = $dm_asiento_intermedio?intval($_POST['dc_cuenta_intermedia']):0;

$insert_medio_cobro = $db->prepare($db->insert('tb_medio_cobro_cliente',array(
	'dg_medio_cobro' => '?',
	'dc_tipo_movimiento' => '?',
	'dm_requiere_banco' => '?',
	'dm_asiento_intermedio' => '?',
	'dm_requiere_documento' => '?',
	'dc_banco_default' => '?',
	'dc_cuenta_contable_intermedia' => '?',
	'dc_empresa' => $empresa,
	'dc_usuario_creacion' => $idUsuario,
	'df_creacion' => $bd->getNow()
)));
$insert_medio_cobro->bindValue(1,$_POST['dg_medio_cobro'],PDO::PARAM_STR);
$insert_medio_cobro->bindValue(2,$dc_tipo_movimiento,PDO::PARAM_INT);
$insert_medio_cobro->bindValue(3,$dm_requiere_banco,PDO::PARAM_STR);
$insert_medio_cobro->bindValue(4,$dm_asiento_intermedio,PDO::PARAM_STR);
$insert_medio_cobro->bindValue(5,$dm_requiere_documento,PDO::PARAM_STR);
$insert_medio_cobro->bindValue(6,$dc_banco,PDO::PARAM_INT);
$insert_medio_cobro->bindValue(7,$dc_cuenta_intermedia,PDO::PARAM_INT);

$db->stExec($insert_medio_cobro);

$dc_medio_cobro = $db->lastInsertId();

$error_man->showConfirm("Se ha creado el medio de pago <b>{$_POST['dg_medio_cobro']}</b> correctamente.");

$date_now = date('d/m/Y H:i:s');

?>
<table class="hidden">
	<tbody>
		<tr id="item<?php echo $dc_medio_cobro ?>">
			<td><?php echo $_POST['dg_medio_cobro'] ?></td>
			<td><?php echo $date_now ?></td>
			<td>-</td>
			<td><?php echo $dm_requiere_banco==1?'SI':'NO' ?></td>
			<td><?php echo $dm_asiento_intermedio==1?'SI':'NO' ?></td>
			<td>
				<a href='sites/finanzas/pagos/ed_medio_cobro.php?id=<?php echo $dc_medio_cobro ?>' class='loadOnOverlay'>
					<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/finanzas/pagos/del_medio_cobro.php?id=<?php echo $dc_medio_cobro ?>' class='loadOnOverlay'>
					<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
	</tbody>
</table>
<script type="text/javascript">
	$("#item<?php echo $dc_medio_cobro ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});
</script>