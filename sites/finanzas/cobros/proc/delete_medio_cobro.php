<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dc_medio_cobro = intval($_POST['dc_medio_cobro_ed']);

$medio_cobro = $db->prepare($db->select('tb_medio_cobro_cliente','dm_activo','dc_empresa = ? AND dc_medio_cobro = ?'));
$medio_cobro->bindValue(1,$empresa,PDO::PARAM_INT);
$medio_cobro->bindValue(2,$dc_medio_cobro,PDO::PARAM_INT); 
$db->stExec($medio_cobro);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_medio_cobro_cliente',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_medio_cobro =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_medio_cobro,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamento'); 


?>