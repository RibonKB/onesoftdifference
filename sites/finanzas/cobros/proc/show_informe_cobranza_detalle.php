<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$file = "cache/informe_{$empresa}.{$idUsuario}.{$_POST['rid']}.xml";

$dom = new SimpleXMLElement($file,0,1);

$filtred = array();

if($_POST['mode'] == 'total'){
	$p =& $_POST['periodo'];
	foreach($dom->detalle as $d){
		if($p == 'total'){
			$filtred[] = $d;
		}else if($p == 'more'){
			if($d->dc_dias_vencimiento > 90){
				$filtred[] = $d;
			}
		}else if($p == 'non'){
			if($d->dc_dias_vencimiento < $_POST['v']){
				$filtred[] = $d;
			}
		}else if($p < 0){
			if($d->dc_dias_vencimiento > $p && $d->dc_dias_vencimiento < 0){
				$filtred[] = $d;
			}
		}else if($p == '30'){
			if($d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento >= 0){
				$filtred[] = $d;
			}
		}else if($p == '60'){
			if($d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 30){
				$filtred[] = $d;
			}
		}else if($p == '90'){
			if($d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 60){
				$filtred[] = $d;
			}
		}
	}
}else if($_POST['mode'] == 'line'){
	$p =& $_POST['periodo'];
	$c =& $_POST['cliente'];
	foreach($dom->detalle as $d){
		if($p == 'total'){
			if($d->dc_cliente == $c)
				$filtred[] = $d;
		}else if($p == 'more'){
			if($d->dc_cliente == $c && $d->dc_dias_vencimiento > 90){
				$filtred[] = $d;
			}
		}else if($p == 'non'){
			if($d->dc_cliente == $c && $d->dc_dias_vencimiento < $_POST['v']){
				$filtred[] = $d;
			}
		}else if($p < 0){
			if($d->dc_cliente == $c && $d->dc_dias_vencimiento >= $p && $d->dc_dias_vencimiento < 0){
				$filtred[] = $d;
			}
		}else if($p == '30'){
			if($d->dc_cliente == $c && $d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento >= 0){
				$filtred[] = $d;
			}
		}else if($p == '60'){
			if($d->dc_cliente == $c && $d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 30){
				$filtred[] = $d;
			}
		}else if($p == '90'){
			if($d->dc_cliente == $c && $d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 60){
				$filtred[] = $d;
			}
		}
	}
}

?>
<div id="other_options">
	<ul>
    <?php if(intval($_POST['dm_cobranza']) == 1): ?>
		<li>
			<a href="sites/finanzas/cobros/cr_gestion_cobranza_masivo.php" id="gestionMasivo">
				<img src="images/management.png" alt="(M)" />
				Gestión
			</a>
		</li>
		<li>
			<a href="sites/finanzas/cobros/cr_pago_cobranza_masivo.php" id="pagoMasivo">
				<img src="images/monedas-icon.png" alt="($)" />
				Pago
			</a>
		</li>
        <?php if(isset($_POST['cliente'])): ?>
        <li>
			<a href="sites/ventas/nota_credito/index.php?factory=VerSaldoFavor&action=index&dc_cliente=<?php echo $_POST['cliente'] ?>" id="verSaldoFavor">
				<img src="images/monedas-icon.png" alt="($)" />
				Saldos a Favor
			</a>
		</li>
        <?php endif ?>
    <?php else: ?>
    	<li>
			<a href="sites/finanzas/cobros/proc/habilitar_cobranza.php" id="habilitarCobro">
				<img src="images/management.png" alt="(M)" />
				Habilitar cobro de facturas seleccionadas
			</a>
		</li>
    <?php endif; ?>
	</ul>
</div>
<table class="tab" width="100%"><thead><tr>
	<th>
		<input type="checkbox" id="selectAllDetailed" disabled="disabled" />
	</th>
	<th>Número de factura/folio</th>
    <th>Fecha Emisión</th>
	<th>Cliente</th>
	<th>Monto no pagado</th>
	<th>Días</th>
	<th>Próxima Gestión</th>
	<th>Tipo Operación</th>
	<th>Nota de venta</th>
	<th>Orden de servicio</th>
</tr></thead><tbody>

<?php
$total = 0; foreach($filtred as $f): $total += $f->dq_monto_pagar;
?>
	<tr id='factura_<?php echo $f->dc_factura ?>'>
		<td align='center'>
			<input type='checkbox' name='dc_factura[]' class='multipleManCheckbox cli_<?php echo $f->dc_cliente ?>' value='<?php echo $f->dc_factura ?>' />
		</td>
		<td>
        	<a class='onoverlay' href='sites/finanzas/cobros/proc/show_factura_venta.php?id=<?php echo $f->dc_factura ?>'>
            	<b><?php echo $f->dq_factura ?></b> - <?php echo $f->dq_folio ?>
            </a>
        </td>
        <td><?php echo $f->df_emision ?></td>
		<td><?php echo $f->dg_razon ?></td>
		<td class='mountContainer' align='right'><?php echo moneda_local((double)$f->dq_monto_pagar) ?></td>
		<td align="right"><?php echo $f->dc_dias_vencimiento ?></td>
		<td class='nextDateContainer'><?php echo $f->df_proxima_gestion=='00/00/0000 00:00'?'-':$f->df_proxima_gestion; ?></td>
		<td><?php echo $f->dg_tipo_operacion ?></td>
		<td><?php echo $f->dq_nota_venta ?></td>
		<td><?php echo $f->dq_orden_servicio ?></td>
	</tr>
<?php endforeach ?>

</tbody>
<tfoot><tr>
	<th align='right' colspan='4'>Total</th>
	<th id='subTotalVencido' align='right'><?php echo moneda_local($total) ?></th>
	<th colspan='5'>&nbsp;</th>
</tr></tfoot>
</table>

<script type="text/javascript">
js_data.initDetailed();
$('#verSaldoFavor').click(function(e){
	e.preventDefault();
	pymerp.loadOverlay(this.href);
});
</script>