<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['inf_por_vencer'] *= -1;

if(isset($_POST['inf_clientes']))
	$clientes = ' AND cl.dc_cliente IN ('.implode(',',$_POST['inf_clientes']).')';
else
	$clientes = '';
	
if(isset($_POST['inf_tipo_cliente']))
	$tipo_cliente = ' AND cl.dc_tipo_cliente IN ('.implode(',',$_POST['inf_tipo_cliente']).')';
else
	$tipo_cliente = '';
	
if(isset($_POST['inf_tipo_operacion']))
	$factura = 'AND dc_tipo_operacion IN ('.implode(',',$_POST['inf_tipo_operacion']).')';
else
	$factura = '';
	
if($_POST['df_emision_desde']){
	$df_emision_desde = $db->sqlDate($_POST['df_emision_desde']);
	if($_POST['df_emision_hasta']){
		$df_emision_hasta = $db->sqlDate($_POST['df_emision_hasta'].' 23:59');
		$df_emision = "AND (df_emision BETWEEN {$df_emision_desde} AND {$df_emision_hasta})";
	}else{
		$df_emision = "AND DATE(df_emision) = DATE({$df_emision_desde})";
	}
}else{
	$df_emision = '';
}

if(isset($_POST['dc_dias_atencion']) || $_POST['dt_horario_desde_hora'] != 0){
	$horario = '';
	if(isset($_POST['dc_dias_atencion'])){
		$dias_horario = implode(',',$_POST['dc_dias_atencion']);
		$horario .= " AND h.dc_dia IN ({$dias_horario})";
	}
	if($_POST['dt_horario_desde_hora'] != 0){
		$hora_inicio = str_pad(($_POST['dt_horario_desde_hora']+$_POST['dt_horario_desde_meridiano']), 2, 0, STR_PAD_LEFT)
						.':'.str_pad($_POST['dt_horario_desde_minuto'], 2, 0, STR_PAD_LEFT)
						.':00';
		if($_POST['dt_horario_hasta_hora'] != 0){
			$hora_termino = str_pad(($_POST['dt_horario_hasta_hora']+$_POST['dt_horario_hasta_meridiano']), 2, 0, STR_PAD_LEFT)
							.':'.str_pad($_POST['dt_horario_hasta_minuto'], 2, 0, STR_PAD_LEFT)
							.':00';
			$horario .= " AND (
								(h.dt_hora_inicio  BETWEEN '{$hora_inicio}' AND '{$hora_termino}') OR
								(h.dt_hora_termino BETWEEN '{$hora_inicio}' AND '{$hora_termino}') OR
								(h.dt_hora_inicio < '{$hora_inicio}' AND h.dt_hora_termino > '{$hora_termino}'))";
		}else{
			$horario .= " AND h.dt_hora_inicio < '{$hora_inicio}' AND h.dt_hora_termino > '{$hora_inicio}'";
		}
	}
	
	$tb_cliente = "(SELECT icl.* FROM tb_cliente icl
					JOIN tb_cliente_sucursal s ON s.dc_cliente = icl.dc_cliente
					JOIN tb_horario_atencion_sucursal h ON h.dc_sucursal = s.dc_sucursal
					WHERE
					icl.dc_empresa = {$empresa} {$horario}
					GROUP BY icl.dc_cliente)";
}else{
	$tb_cliente = 'tb_cliente';
}

$dm_cobranza = "AND dm_cobranza = '".intval($_POST['dm_cobranza'])."'";
	
$data = $db->select("(SELECT * FROM tb_factura_venta WHERE dq_monto_pagado < dq_total {$factura} {$df_emision} AND dm_nula = 0 AND dc_empresa = {$empresa} {$dm_cobranza}) f
JOIN {$tb_cliente} cl ON cl.dc_cliente = f.dc_cliente {$clientes} {$tipo_cliente}
JOIN tb_tipo_operacion op ON op.dc_tipo_operacion = f.dc_tipo_operacion
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = f.dc_orden_servicio
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = f.dc_nota_venta",
"cl.dc_cliente, cl.dg_razon, DATEDIFF(NOW(),f.df_vencimiento) dc_dias_vencimiento, f.dq_factura, f.dq_folio, f.dc_factura, os.dq_orden_servicio, nv.dq_nota_venta, f.dq_total-f.dq_monto_pagado AS dq_monto_pagar,DATE_FORMAT(f.df_vencimiento,'%d/%m/%Y') df_vencimiento,op.dg_tipo_operacion,
DATE_FORMAT(f.df_proxima_gestion,'%d/%m/%Y %H:%i') df_proxima_gestion, DATE_FORMAT(f.df_emision,'%d/%m/%Y') df_emision");

$dom = new DOMDocument('1.0','UTF-8');
$dom->standalone = true;

$dom_informe = $dom->createElement('informe');
$dom->appendChild($dom_informe);

$per_client = array();
$totales = array(0,0,0,0,0,0,0);
foreach($data as $d){
	$detalle = $dom->createElement('detalle');
	foreach($d as $i => $v){
		$detalle->appendChild($dom->createElement($i,htmlspecialchars($v,ENT_QUOTES)));
	}
	$dom_informe->appendChild($detalle);
	
	$dias = $d['dc_dias_vencimiento'];
	$indice = 0;
	if($dias <= 0){
		if($dias >= $_POST['inf_por_vencer'])
			$indice = 1;
	}else if($dias <= 30){
		$indice = 2;
	}else if($dias <= 60){
		$indice = 3;
	}else if($dias <= 90){
		$indice = 4;
	}else
		$indice = 5;
	
	@$per_client[$d['dc_cliente']][$indice] += $d['dq_monto_pagar'];
	$per_client[$d['dc_cliente']]['razon'] = $d['dg_razon'];
	@$per_client[$d['dc_cliente']]['total'] += $d['dq_monto_pagar'];
	$totales[$indice] += $d['dq_monto_pagar'];
	$totales[6] += $d['dq_monto_pagar'];
}

$cache = uniqid();

$dom->save("cache/informe_{$empresa}.{$idUsuario}.{$cache}.xml");

foreach($totales as $i => $v){
	$totales[$i] = moneda_local($v);
}

$_POST['inf_por_vencer'] *= -1;

echo("<ul id='tipo_informes' class='manual_tab'>
	<li><a href='#'>Informe Completo</a></li>
	<li><a href='#' id='to_detail'>Informe detallado</a></li>
</ul>
<br class='clear'>

<div class='tabpanes'>
<div id='complete_report'>
<table class='tab' width='100%'><thead><tr>
	<th width='30%'>Cliente</th>
	<th width='10%'>Por vencer en {$_POST['inf_por_vencer']} o más días </th>
	<th width='10%'>Por vencer en {$_POST['inf_por_vencer']} o menos días </th>
	<th width='10%'>Vencida 30 días</th>
	<th width='10%'>Vencida 60 días</th>
	<th width='10%'>Vencida 90 días</th>
	<th width='10%'>Vencida 90+ días</th>
	<th width='10%'>Total</th>
</tr></thead><tfoot><tr>
	<th align='right'>Totales</th>
	<th align='right'>
		<a id='farVencer' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=total&periodo=non&v=-{$_POST['inf_por_vencer']}'>
			{$totales[0]}
		</a>
	</th>
	<th align='right'>
		<a id='nearVencer' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=total&periodo=-{$_POST['inf_por_vencer']}'>
			{$totales[1]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer30' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=total&periodo=30'>
			{$totales[2]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer60' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=total&periodo=60'>
			{$totales[3]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer90' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=total&periodo=90'>
			{$totales[4]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer90more' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=total&periodo=more'>
			{$totales[5]}
		</a>
	</th>
	<th align='right'><b>
		<a id='totalVencido' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=total&periodo=total'>
			{$totales[6]}
		</a>
	</b></th>
</tr></tfoot><tbody>");

foreach($per_client as $id => $cliente){
	echo("<tr id='cliente_{$id}'>
		<td><strong>{$cliente['razon']}</strong></td>
		<td align='right'>
			<a class='farVencer' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=line&periodo=non&cliente={$id}&v=-{$_POST['inf_por_vencer']}'>
				".(@$cliente[0]?moneda_local($cliente[0]):0)."</td>
			</a>
		<td align='right'>
			<a class='nearVencer' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=line&periodo=-{$_POST['inf_por_vencer']}&cliente={$id}'>
				".(@$cliente[1]?moneda_local($cliente[1]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer30' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=line&periodo=30&cliente={$id}'>
				".(@$cliente[2]?moneda_local($cliente[2]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer60' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=line&periodo=60&cliente={$id}'>
				".(@$cliente[3]?moneda_local($cliente[3]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer90' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=line&periodo=90&cliente={$id}'>
				".(@$cliente[4]?moneda_local($cliente[4]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer90more' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=line&periodo=more&cliente={$id}'>
				".(@$cliente[5]?moneda_local($cliente[5]):0)."</td>
			</a>
		</td>
		<td align='right'><b>
			<a class='totalCliente' href='sites/finanzas/cobros/proc/show_informe_cobranza_detalle.php?rid={$cache}&mode=line&periodo=total&cliente={$id}'>
				".(@$cliente['total']?moneda_local($cliente['total']):0)."</td>
			</a>
		</b></td>
	</tr>");
}

echo('</tbody></table>
</div><div id="detailed_report">
<div class="info">
	Seleccione un elemento del informe completo para cargar el detalle acá.
</div>
</div>');

?>
<script type="text/javascript" src="jscripts/sites/finanzas/cobros/src_informe_cobranza.js?v=0_16b"></script>
<script type="text/javascript">
	js_data.porVencer = <?php echo $_POST['inf_por_vencer'] ?>;
	js_data.separadorMiles = '<?php echo $empresa_conf['dm_separador_miles'] ?>';
	js_data.separadorDecimal = '<?php echo $empresa_conf['dm_separador_decimal'] ?>';
	js_data.dm_cobranza = <?php echo intval($_POST['dm_cobranza']) ?>;
	$('#complete_report table').first().tableExport();
</script>