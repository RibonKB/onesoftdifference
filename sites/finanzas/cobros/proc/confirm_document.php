<?php
define('MAIN',1);
require_once("../../../../inc/init.php");
require_once("../../../contabilidad/stuff.class.php");

	$dc_detalle = intval($_POST['id']);
	
	if(!$dc_detalle){
		$error_man->showWarning('El pago a confirmar es inválido, compruebe la entrada y vuelva a intentarlo');
		exit;
	}
	
	$data = $db->prepare($db->select('tb_detalle_pago_cliente d
	JOIN tb_comprobante_pago_cliente c ON c.dc_comprobante = d.dc_comprobante
	JOIN tb_medio_cobro_cliente m ON m.dc_medio_cobro = c.dc_medio_cobro
	JOIN tb_cliente cl ON cl.dc_cliente = c.dc_cliente',
	'd.dc_comprobante, d.dc_banco, d.dg_documento, d.dq_monto, d.df_emision, c.dq_comprobante, c.dg_comentario, cl.dg_razon,
	cl.dc_cliente, cl.dg_rut, m.dc_cuenta_contable_intermedia, m.dc_tipo_movimiento, d.df_vencimiento',
	'd.dc_detalle = ? AND c.dc_empresa = ? AND d.dm_estado = ?'));
	$data->bindValue(1,$dc_detalle,PDO::PARAM_INT);
	$data->bindValue(2,$empresa,PDO::PARAM_INT);
	$data->bindValue(3,'L',PDO::PARAM_STR);
	$db->stExec($data);
	$data = $data->fetch(PDO::FETCH_OBJ);
	
	if($data === false){
		$error_man->showWarning('No se encontró el detalle de pago que desea confirmar o este ya fue confirmado.');
		exit;
	}
	
	$data->dc_banco = intval($_POST['dc_banco']);
	
	//Detalles del DEBE
	if($data->dc_banco != 0){
		
		$banco_data = $db->prepare($db->select('tb_banco','dc_cuenta_contable, dg_banco','dc_banco = ?'));
		$banco_data->bindValue(1,$data->dc_banco);
		$db->stExec($banco_data);
		$banco_data = $banco_data->fetch(PDO::FETCH_OBJ);
		
		if($banco_data === false){
			$error_man->showWarning('No se puedo obtener la información del banco, no se puede continuar');
			exit;
		}
		
		$detalle_debe = array(
			'dc_cuenta_contable' => $banco_data->dc_cuenta_contable,
			'dg_glosa' => "Cobro banco: {$banco_data->dg_banco}",
		);
		
	}else{
		$error_man->showAviso('El pago con cargo  cuentas que no sean de banco no está implementado aún, debe hacer los comprobantes contables manualmente.');
		exit;
	}
	
	$dq_comprobante_contable = ContabilidadStuff::getCorrelativoComprobante();
	
	$db->start_transaction();
	
	//Insertar cabecera del comprobante contable
	$insert_comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => '?',
		'dc_tipo_movimiento' => '?',
		'df_fecha_contable' => '?',
		'dc_mes_contable' => date('m'),
		'dc_anho_contable' => date('Y'),
		'dc_tipo_cambio' => '?',
		'dq_cambio' => 1,
		'dg_glosa' => '?',
		'dc_comprobante_pago' => '?',
		'dq_saldo' =>  $data->dq_monto,
		'df_fecha_emision' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario
	)));
	$insert_comprobante->bindValue(1,$dq_comprobante_contable,PDO::PARAM_INT);
	$insert_comprobante->bindValue(2,$data->dc_tipo_movimiento,PDO::PARAM_INT);
	$insert_comprobante->bindValue(3,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
	$insert_comprobante->bindValue(4,0,PDO::PARAM_INT);
	$insert_comprobante->bindValue(5,"Comprobante: {$data->dq_comprobante}\nCliente:{$data->dg_razon}\n\n{$data->dg_comentario}",PDO::PARAM_STR);
	$insert_comprobante->bindValue(6,$data->dc_comprobante,PDO::PARAM_INT);
	
	$db->stExec($insert_comprobante);

	$dc_comprobante = $db->lastInsertId();
	
	//Insertar detalles de comprobante
	
	//Preparar consulta de inserción de detalles
	$insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => $dc_comprobante,
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?',
		'dg_cheque' => '?',
		'df_fecha_cheque' => '?',
		'dg_rut' => '?',
		'dc_banco' => '?',
		'dc_cliente' => $data->dc_cliente
	)));
	$insert_detalle_comprobante->bindParam(1,$dc_cuenta_contable,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindParam(2,$dq_debe,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(3,$dq_haber,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(4,$dg_glosa,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(5,$dg_cheque,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(6,$df_fecha_cheque,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(7,$dg_rut,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(8,$dc_banco,PDO::PARAM_INT);
	
	//HABER: acá van los montos de cobros con la cuenta intermedia asociada
	$dc_cuenta_contable = $data->dc_cuenta_contable_intermedia;
	$dq_debe = 0;
	$dq_haber = $data->dq_monto;
	$dg_glosa = "Cobro cheque: {$data->dg_documento} vencimiento: {$data->df_vencimiento}";
	$dg_cheque = $data->dg_documento;
	$df_fecha_cheque = $data->df_emision;
	$dg_rut = $data->dg_rut;
	$dc_banco = $data->dc_banco;
	
	//Insertar detalle HABER
	$db->stExec($insert_detalle_comprobante);
	
	//DEBE: Acá va el detalle del debe, la cuenta donde se cargan los montos finalmente.
	$dc_cuenta_contable = $detalle_debe['dc_cuenta_contable'];
	$dq_debe = $data->dq_monto;
	$dq_haber = 0;
	$dg_glosa = $detalle_debe['dg_glosa'];
	$dg_cheque = $data->dg_documento;
	$df_fecha_cheque = $data->df_emision;
	$dg_rut = $data->dg_rut;
	$dc_banco = $data->dc_banco;
	
	//Insertar detalle DEBE
	$db->stExec($insert_detalle_comprobante);
	
	//Marcar Propuesta como terminada
	$update_detalle = $db->prepare($db->update('tb_detalle_pago_cliente',array('dm_estado' => '?'),'dc_detalle = ?'));
	$update_detalle->bindValue(1,'T',PDO::PARAM_STR);
	$update_detalle->bindValue(2,$dc_detalle,PDO::PARAM_INT);
	$db->stExec($update_detalle);
	
	$db->commit();
	
	$error_man->showConfirm("Se han confirmado el pago correctamente<br />
	Puede ver la versión de impresión del comprobante haciendo <a href='sites/contabilidad/ver_comprobante.php?id={$dc_comprobante}' target='_blank'>click acá</a>");
	
?>