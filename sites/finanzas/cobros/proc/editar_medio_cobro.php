<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dg_medio_cobro = $_POST['dg_medio_cobro_ed'];
$dc_tipo_movimiento = intval($_POST['dc_tipo_movimiento_ed']);
$dm_requiere_banco = intval($_POST['dm_requiere_banco_ed']);
$dm_asiento_intermedio = intval($_POST['dm_asiento_intermedio_ed']);
$dm_requiere_documento = intval($_POST['dm_requiere_documento_ed']);
$dc_banco_default = intval($_POST['dc_banco_default_ed']);
$dc_cuenta_contable_intermedia = intval($_POST['dc_cuenta_contable_intermedia_ed']);
$dc_medio_cobro = intval($_POST['dc_medio_cobro_ed']);

$medio_cobro = $db->prepare($db->select('tb_medio_cobro_cliente','true','dc_empresa = ? AND dg_medio_cobro = ? AND dc_medio_cobro = ?'));
$medio_cobro->bindValue(1,$empresa,PDO::PARAM_INT);
$medio_cobro->bindValue(2,$dg_medio_cobro,PDO::PARAM_STR);
$medio_cobro->bindValue(3,$dc_medio_cobro,PDO::PARAM_INT);
$db->stExec($medio_cobro);

if($medio_cobro->fetch() !== false){
	$error_man->showWarning("Ya existe un medio de cobro con ese nombre");
	exit; 
}

$db->start_transaction();

$editar = $db->prepare($db->update('tb_medio_cobro_cliente',array(
				'dg_medio_cobro' => '?',
				'dc_tipo_movimiento' => '?',
				'dm_requiere_banco' => '?',
	 			'dm_asiento_intermedio' => '?',
				'dm_requiere_documento' => '?',
				'dc_banco_default' => '?',
	 			'dc_cuenta_contable_intermedia' => '?'),
				'dc_medio_cobro = ? AND dc_empresa = ?'));
$editar->bindValue(1,$dg_medio_cobro,PDO::PARAM_STR);
$editar->bindValue(2,$dc_tipo_movimiento,PDO::PARAM_INT);
$editar->bindvalue(3,$dm_requiere_banco,PDO::PARAM_BOOL);
$editar->bindValue(4,$dm_asiento_intermedio,PDO::PARAM_BOOL);
$editar->bindValue(5,$dm_requiere_documento,PDO::PARAM_BOOL);
$editar->bindValue(6,$dc_banco_default,PDO::PARAM_INT);
$editar->bindValue(7,$dc_cuenta_contable_intermedia,PDO::PARAM_INT);
$editar->bindValue(8,$dc_medio_cobro,PDO::PARAM_INT);
$editar->bindValue(9,$empresa,PDO::PARAM_INT);
$db->stExec($editar);

$db->commit();


$error_man->showConfirm('Se ha modificado correctamente'); 

?>


