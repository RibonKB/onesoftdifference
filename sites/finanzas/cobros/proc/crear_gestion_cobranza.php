<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['gestion_monto'] = str_replace(',','',$_POST['gestion_monto']);

$_POST['gestion_compromiso'] = $db->sqlDate($_POST['gestion_compromiso']);

if($_POST['gestion_prox_hora'] == 12) $_POST['gestion_prox_hora'] = 0;
$hora = str_pad($_POST['gestion_prox_hora']+$_POST['gestion_prox_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_prox_minuto'];
$_POST['gestion_prox_gestion'] = $formatedNextDate = $_POST['gestion_prox_gestion']." ".$hora;
unset($hora);
$_POST['gestion_prox_gestion'] = $db->sqlDate($_POST['gestion_prox_gestion']);

$db->start_transaction();

$modo = $_POST['gestion_modo'] > 0;

$gestion = $db->insert('tb_factura_venta_gestion',array(
	'dc_factura' => $_POST['id_factura'],
	'dc_contacto' => $_POST['gestion_contacto'],
	'df_emision' => 'NOW()',
	'dq_monto' => $modo?$_POST['gestion_monto']:0,
	'df_compromiso' => $_POST['gestion_compromiso'],
	'df_proxima_gestion' => $_POST['gestion_prox_gestion'],
	'dc_medio_pago' => $modo?$_POST['gestion_medio_pago']:0,
	'dc_banco' => $modo?$_POST['gestion_banco']:0,
	'dg_comentario' => $_POST['gestion_comentario'],
	'dc_usuario_creacion' => $idUsuario
));

if(isset($_POST['gestion_docs']) && $modo){
	$_POST['gestion_docs'] = array_unique($_POST['gestion_docs']);
	
	foreach($_POST['gestion_docs'] as $doc){
		$db->insert('tb_factura_venta_gestion_documento',array(
			'dc_gestion' => $gestion,
			'dg_documento' => $doc
		));
	}
}

$to_update_factura = array(
	'df_proxima_gestion' => $_POST['gestion_prox_gestion']
);

if($_POST['gestion_monto'] > 0){
	$to_update_factura['dq_monto_pagado'] = "dq_monto_pagado+{$_POST['gestion_monto']}";
	
}

$db->update('tb_factura_venta',$to_update_factura,"dc_factura={$_POST['id_factura']}");

if($_POST['gestion_monto_maximo'] == $_POST['gestion_monto'] && $_POST['gestion_monto_maximo'] > 0){
	$periodo = $db->select('dual',"DATEDIFF({$_POST['gestion_compromiso']},'{$_POST['emision_factura']}') as periodo");
	$periodo = $periodo[0]['periodo'];
	
	$db->update('tb_cliente',array(
		'dc_dias_pago' => "dc_dias_pago+{$periodo}",
		'dc_facturas_pagadas' => "dc_facturas_pagadas+1"
	),"dc_cliente={$_POST['id_cliente']}");
}

$db->commit();
?>
<script type="text/javascript">
	if(js_data.fromReport){
		<?php if($_POST['gestion_monto_maximo'] == $_POST['gestion_monto'] && $_POST['gestion_monto_maximo'] > 0): ?>
			js_data.deleteItemDetail(<?php echo $_POST['id_factura'] ?>);
		<?php else: ?>
			js_data.editItemDetail(
				'<?php echo $_POST['id_factura'] ?>',
				'<?php echo moneda_local($_POST['gestion_monto_maximo'] - $_POST['gestion_monto']) ?>',
				'<?php echo $formatedNextDate ?>');
		<?php endif; ?>
	}
	$('#genOverlay').remove();
	show_confirm("Se ha almacenado la gestión sobre la factura");
</script>