<?php
define("MAIN",1);
require_once("../../../../inc/global.php");

$dc_medio_cobro = intval($_POST['dc_medio_cobro']);

if(!$dc_medio_cobro){
	$error_man->showWarning('El Medio de cobro seleccionado es inválido, comrpruebe los valores de entrada y vuelva a intentarlo.');
	exit;
}

$dq_total = floatval($_POST['dq_total']);

if(!$dq_total){
	$error_man->showWarning('No se pudo rescatar el total del cobro, consulte con un administrador o vuelva a intentarlo.');
	exit;
}

$medio_cobro = $db->select(
			'tb_medio_cobro_cliente',
			'dg_medio_cobro,dm_requiere_banco,dm_requiere_documento,dc_tipo_movimiento, dc_banco_default',
			"dc_medio_cobro = {$dc_medio_cobro} AND dc_empresa = {$empresa}");
$medio_cobro = array_shift($medio_cobro);

if($medio_cobro == NULL){
	$error_man->showWarning('El Medio de cobro seleccionado no ha sido encontrado, compruebe que exista y vuelva a intentarlo.');
	exit;
}

if($medio_cobro['dm_requiere_banco'] == 0 && $medio_cobro['dm_requiere_documento'] == 0){
	$error_man->showAviso('El medio de cobro seleccionado no requiere detalles de pago');
	exit;
}

require_once("../../../../inc/form-class.php");
$form = new Form($empresa);

?>
	<table class="tab" width="100%">
	<thead>
		<th>(X)</th>
		<?php if($medio_cobro['dm_requiere_documento'] == 1): ?>
			<th>Documento</th>
			<th>Fecha vencimiento</th>
		<?php endif; ?>
		<?php if($medio_cobro['dm_requiere_banco'] == 1): ?>
			<th>Banco</th>
		<?php endif; ?>
        <th>Banco Origen</th>
		<th>Fecha Emisión</th>
		<th>Monto</th>
        <th>Código Seguro</th>
	</thead>
	<tbody id="payDetail"></tbody>
	</table>
	<div id="addPayDetail">
	<?php
		$form->Header('Indique los datos de los detalles de pago para asignarselo al cobro del cliente');
			$form->Section();
				$form->Text('Monto','dq_monto',1,255,$dq_total);
				$form->DateManual('Fecha de emisión','df_emision',1,0);
				$form->Listado('Banco Origen','dc_banco_origen','tb_banco_cobro',array('dc_banco','dg_banco'),1);
				$form->Text('Código seguro','dg_codigo_seguro',true);
			$form->EndSection();
			
			$form->Section();
			
			if($medio_cobro['dm_requiere_documento'] == 1):
				$form->Text('Número de documento','dg_documento',1);
				$form->DateManual('Fecha de vencimiento','df_vencimiento',1);
			endif;
			
			if($medio_cobro['dm_requiere_banco'] == 1):
				$form->Listado('Banco Destino','dc_banco','tb_banco b
				JOIN tb_cuentas_tipo_movimiento tm ON tm.dc_cuenta_contable = b.dc_cuenta_contable AND tm.dc_tipo_movimiento = '.$medio_cobro['dc_tipo_movimiento'],
				array('b.dc_banco','b.dg_banco'),1,$medio_cobro['dc_banco_default']);
			endif;
			
			$form->EndSection();
	?>
	<br class="clear" />
	<div class="center">
		<?php $form->Button('Agregar Detalle','id="addDetail"','addbtn') ?>
	</div>
	</div>
    <hr />