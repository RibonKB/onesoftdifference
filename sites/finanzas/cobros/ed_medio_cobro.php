<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");

$form = new Form($empresa);

$dc_medio_cobro = intval($_POST['id']);

$medio_cobro = $db->prepare($db->select('tb_medio_cobro_cliente',
	'dg_medio_cobro, dc_tipo_movimiento, dm_requiere_banco,dm_asiento_intermedio,
	dm_requiere_documento, dc_banco_default, dc_cuenta_contable_intermedia',
	'dc_medio_cobro = ? AND dc_empresa = ? '));
$medio_cobro->bindValue(1,$dc_medio_cobro,PDO::PARAM_INT);
$medio_cobro->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($medio_cobro);

$medio_cobro = $medio_cobro->fetch(PDO::FETCH_OBJ);

if($medio_cobro === false){
	$error_man->showWarning('El tipo de medio de cobro no fue encontrado');
	exit;
}

?>

<div class="secc_bar">
	Editar medios de cobro
</div>

<div class="panes">
<?php $form->Start('sites/finanzas/cobros/proc/editar_medio_cobro.php', 'ed_medio_cobro') ?>
<?php $form->Header('<b>Ingrese el nombre para actualizar el medio de cobro</b><br />Los campos marcados con [*] son obligatorios') ?>

<?php $form->Section(); 
         	$form->Text('Nombre','dg_medio_cobro_ed',true,Form::DEFAULT_TEXT_LENGTH,$medio_cobro->dg_medio_cobro);    
         	$form->DBSelect('Tipo movimiento contable','dc_tipo_movimiento_ed','tb_tipo_movimiento',array('dc_tipo_movimiento',
			'dg_tipo_movimiento'),true,$medio_cobro->dc_tipo_movimiento); 
		$form->EndSection();
		
		$form->Section();
			$form->Radiobox('Requiere Banco','dm_requiere_banco_ed',array('SI','NO'),$medio_cobro->dm_requiere_banco==1?0:1);
			$form->Radiobox('Asiento Intermedio','dm_asiento_intermedio_ed',array('SI','NO'),$medio_cobro->dm_asiento_intermedio==1?0:1);
			$form->Radiobox('Requiere Documento','dm_requiere_documento_ed',array('SI','NO'),$medio_cobro->dm_requiere_documento==1?0:1);
		$form->EndSection();
		
		$form->Section();
			$form->DBSelect('Banco por defecto','dc_banco_default_ed','tb_banco',array('dc_banco','dg_banco'),$medio_cobro->dc_banco_default);
			$form->DBSelect('Cuenta contable asiento intermedio','dc_cuenta_contable_intermedia_ed','tb_cuenta_contable',
			array('dc_cuenta_contable','dg_cuenta_contable'),true,$medio_cobro->dc_cuenta_contable_intermedia);
		$form->EndSection();
?>

<?php $form->Hidden('dc_medio_cobro_ed',$dc_medio_cobro) ?>
    <?php $form->End('Editar','editbtn') ?>

</div>


