<?php
define('MAIN',1);
require_once("../../../inc/init.php");
require_once("../../../inc/Factory.class.php");

?>
<div class="secc_bar">
	Confirmación de pagos
</div>
<?php

$dc_detalle = $_POST['id'];

if(!$dc_detalle){
	$error_man->showWarning('El pago a confirmar es inválido, compruebe los valores de entrada y vuelva a intentarlo');
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="panes center">
	<?php
    	$form->Start(Factory::buildUrl('ConfirmarGiros', 'finanzas', 'confirmarChequeCobro', 'cobros'),'cr_confirmacion');
		$form->Header('<b>Seleccione la fecha contable que será utilizada para crear el comprobante de giro</b><br />Los campos marcados con [*] son obligatorios');
		$form->Section();
			$form->Date('Fecha contable','df_fecha_contable',1,0);
		$form->EndSection();
		$form->Section();
			$form->DBSelect('Banco deposito','dc_banco','tb_banco',array('dc_banco','dg_banco'),1);
		$form->EndSection();
		$form->Hidden('id',$dc_detalle);
		$form->End('Confirmar','checkbtn');
	?>
</div>