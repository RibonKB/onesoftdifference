<?php

require_once('sites/finanzas/AbstractConfirmarGirosFactory.php');

/**
 * Description of ConfirmarGirosFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 26-04-2013
 */
class ConfirmarGirosFactory extends AbstractConfirmarGirosFactory {

  protected $pago_data;
  private $detalleDebe;
  private $cliente = null;

  public function confirmarChequeCobroAction() {
    $request = self::getRequest();

    $this->pago_data = $this->getPagoData($request->id);

    $this->detalleDebe = $this->getDetalleGiro();

    $this->getConnection()->start_transaction();

    $this->insertarComprobanteContable(array(
        'document_field' => 'dc_comprobante_pago',
        'document_id' => $this->pago_data->dc_comprobante,
        'glosa' => "Comprobante: {$this->pago_data->dq_comprobante}\nCliente:{$this->getCliente()->dg_razon}\n\n{$this->pago_data->dg_comentario}"
    ));
    
    $this->insertarDetallesFinancieros();
    $this->insertarDetallesAnaliticos();
    $this->confirmarGiro();

    $this->getConnection()->commit();

    $this->showEstandarOutput();
  }

  private function getPagoData($dc_pago) {
    $db = $this->getConnection();
    $dc_pago = intval($dc_pago);

    if (!$dc_pago) {
      $this->getErrorMan()->showWarning('El pago a confirmar es inválido, compruebe la entrada y vuelva a intentarlo');
      exit;
    }

    $data = $db->prepare($db->select('tb_detalle_pago_cliente d
          JOIN tb_comprobante_pago_cliente c ON c.dc_comprobante = d.dc_comprobante
          JOIN tb_medio_cobro_cliente m ON m.dc_medio_cobro = c.dc_medio_cobro',
    'd.dc_comprobante, d.dg_documento, d.dq_monto, d.df_emision, c.dq_comprobante, c.dg_comentario, m.dc_medio_cobro,
     c.dc_cliente, m.dc_cuenta_contable_intermedia, m.dc_tipo_movimiento, d.df_vencimiento, d.dc_detalle',
    'd.dc_detalle = ? AND c.dc_empresa = ? AND d.dm_estado = ?'));
    $data->bindValue(1, $dc_pago, PDO::PARAM_INT);
    $data->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
    $data->bindValue(3, 'L', PDO::PARAM_STR);
    $db->stExec($data);
    $data = $data->fetch(PDO::FETCH_OBJ);

    if ($data === false) {
      $this->getErrorMan()->showWarning('No se encontró el detalle de pago que desea confirmar o este ya fue confirmado.');
      exit;
    }
    
    $data->dc_banco = self::getRequest()->dc_banco;

    return $data;
  }

  protected function insertarDetalleChequeFinanciero() {
    //HABER: acá van los montos de cobros con la cuenta intermedia asociada
    $comprobante = $this->getComprobanteService();
    $comprobante->setParametroFinanciero('dc_cuenta_contable', $this->pago_data->dc_cuenta_contable_intermedia);
    $comprobante->setParametroFinanciero('dq_debe', 0);
    $comprobante->setParametroFinanciero('dq_haber', $this->pago_data->dq_monto);
    $comprobante->setParametroFinanciero('dg_glosa', "Cobro cheque: {$this->pago_data->dg_documento} vencimiento: {$this->pago_data->df_vencimiento}");

    return $comprobante->DetalleFinancieroPersist();
  }
  
  protected function insertarDetalleGiroFinanciero() {
    $comprobante = $this->getComprobanteService();

    //DEBE: Acá va el detalle del haber, la cuenta donde se cargan los montos finalmente.
    $comprobante->setParametroFinanciero('dc_cuenta_contable', $this->detalleDebe['dc_cuenta_contable']);
    $comprobante->setParametroFinanciero('dq_debe', $this->pago_data->dq_monto);
    $comprobante->setParametroFinanciero('dq_haber', 0);
    $comprobante->setParametroFinanciero('dg_glosa', $this->detalleDebe['dg_glosa']);

    return $comprobante->DetalleFinancieroPersist();
  }
  
  protected function insertarDetallesAnaliticos() {

    $comprobante = $this->getComprobanteService();
    $comprobante->prepareDetalleAnalitico();

    $comprobante->setParametroAnalitico('dc_cliente', $this->pago_data->dc_cliente);
    $comprobante->setParametroAnalitico('dc_banco', $this->pago_data->dc_banco);
    $comprobante->setParametroAnalitico('dc_medio_cobro_cliente', $this->pago_data->dc_medio_cobro);
    $comprobante->setParametroAnalitico('dg_cheque', $this->pago_data->dg_documento);
    $comprobante->setParametroAnalitico('df_cheque', $this->pago_data->df_emision);
    $comprobante->setDependencias('dc_cliente');

    $this->insertarDetalleChequeAnalitico();
    $this->insertarDetalleGiroAnalitico();
  }
  
      protected function insertarDetalleChequeAnalitico() {
        //DEBE: acá van los montos de cobros con la cuenta intermedia asociada
        $comprobante = $this->getComprobanteService();
        $comprobante->setParametroAnalitico('dc_cuenta_contable', $this->pago_data->dc_cuenta_contable_intermedia);
        $comprobante->setParametroAnalitico('dq_debe', 0);
        $comprobante->setParametroAnalitico('dq_haber', $this->pago_data->dq_monto);
        $comprobante->setParametroAnalitico('dg_glosa', "Cobro cheque: {$this->pago_data->dg_documento} vencimiento: {$this->pago_data->df_vencimiento}");
        $comprobante->setParametroAnalitico('dc_detalle_financiero', $this->matrizFinanciera[0]);

        //Insertar detalle DEBE
        $comprobante->DetalleAnaliticoPersist();
      }

      protected function insertarDetalleGiroAnalitico() {
          //DEBE: Acá va el detalle del haber, la cuenta donde se cargan los montos finalmente.
          $comprobante = $this->getComprobanteService();
          $comprobante->setParametroAnalitico('dc_cuenta_contable', $this->detalleDebe['dc_cuenta_contable']);
          $comprobante->setParametroAnalitico('dq_debe', $this->pago_data->dq_monto);
          $comprobante->setParametroAnalitico('dq_haber', 0);
          $comprobante->setParametroAnalitico('dg_glosa', $this->detalleDebe['dg_glosa']);
          $comprobante->setParametroAnalitico('dc_detalle_financiero', $this->matrizFinanciera[1]);

          //Insertar detalle DEBE
          $comprobante->DetalleAnaliticoPersist();
      }

  private function confirmarGiro() {

    $db = $this->getConnection();

    $update_detalle = $db->prepare($db->update('tb_detalle_pago_cliente', array('dm_estado' => '?'), 'dc_detalle = ?'));
    $update_detalle->bindValue(1, 'T', PDO::PARAM_STR);
    $update_detalle->bindValue(2, $this->pago_data->dc_detalle, PDO::PARAM_INT);
    $db->stExec($update_detalle);
  }

  private function getCliente() {
    if ($this->cliente != null) {
      return $this->cliente;
    }

    $this->cliente = $this->getConnection()->getRowById('tb_cliente', $this->pago_data->dc_cliente, 'dc_cliente');

    return $this->cliente;
  }

}

?>
