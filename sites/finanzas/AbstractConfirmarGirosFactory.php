<?php

require_once('sites/contabilidad/stuff.class.php');

/**
 * Description of AbstractConfirmarGirosFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 26-04-2013
 */
abstract class AbstractConfirmarGirosFactory extends Factory {

  protected $dc_comprobante;
  protected $dq_comprobante;
  protected $parametrosFinancieros;

  protected function showEstandarOutput() {
    $this->getErrorMan()->showConfirm('Se ha confirmado el giro del cheque correctamente');
    echo $this->getComprobanteService()->getStandarCreationOutput();
  }

  protected function getDetalleGiro() {
    if ($this->pago_data->dc_banco != 0) {
      return $this->getGiroBanco();
    } else {
      $this->getErrorMan()->showAviso('El pago con cargo  cuentas que no sean de banco no está implementado aún, debe hacer los comprobantes contables manualmente.');
      exit;
    }
  }

  protected function getGiroBanco() {

    /* @var $db DBConnector */
    $db = $this->getConnection();

    $banco_data = $db->getRowById('tb_banco', $this->pago_data->dc_banco, 'dc_banco', 'dc_cuenta_contable, dg_banco');

    if ($banco_data === false) {
      $this->getErrorMan()->showWarning('No se puedo obtener la información del banco, no se puede continuar');
      exit;
    }

    return array(
        'dc_cuenta_contable' => $banco_data->dc_cuenta_contable,
        'dg_glosa' => "Cobro banco: {$banco_data->dg_banco}",
    );
  }

  protected function insertarComprobanteContable($extraData) {
    $this->dq_comprobante = ContabilidadStuff::getCorrelativoComprobante($this->getConnection(), $this->getEmpresa());
    $db = $this->getConnection();
    $request = self::getRequest();

    $comprobante = $this->getComprobanteService();
    try {
      $comprobante->prepareComprobante($this->pago_data->dc_tipo_movimiento, $db->sqlDate2($request->df_fecha_contable));
    } catch (Exception $e) {
      $this->getErrorMan()->showWarning($e->getMessage());
      exit;
    }
    $comprobante->setComprobanteParam('dg_glosa', $extraData['glosa']);
    $comprobante->setComprobanteParam($extraData['document_field'], $extraData['document_id']);
    $comprobante->setComprobanteParam('dq_saldo', $this->pago_data->dq_monto);
    $comprobante->ComprobantePersist();


    /* $insert_comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
      'dg_comprobante' => '?',
      'dc_tipo_movimiento' => '?',
      'df_fecha_contable' => '?',
      'dc_mes_contable' => 'MONTH(?)',
      'dc_anho_contable' => 'YEAR(?)',
      'dc_tipo_cambio' => '?',
      'dq_cambio' => 1,
      'dg_glosa' => '?',
      $extraData['document_field'] => '?',
      'dq_saldo' =>  $this->pago_data->dq_monto,
      'df_fecha_emision' => $db->getNow(),
      'dc_empresa' => $this->getEmpresa(),
      'dc_usuario_creacion' => $this->getUserData()->dc_usuario
      )));
      $insert_comprobante->bindValue(1,$this->dq_comprobante,PDO::PARAM_INT);
      $insert_comprobante->bindValue(2,$this->pago_data->dc_tipo_movimiento,PDO::PARAM_INT);
      $insert_comprobante->bindValue(3,$db->sqlDate2($request->df_fecha_contable),PDO::PARAM_STR);
      $insert_comprobante->bindValue(4,$db->sqlDate2($request->df_fecha_contable),PDO::PARAM_STR);
      $insert_comprobante->bindValue(5,$db->sqlDate2($request->df_fecha_contable),PDO::PARAM_STR);
      $insert_comprobante->bindValue(6,0,PDO::PARAM_INT);
      $insert_comprobante->bindValue(7,$extraData['glosa'],PDO::PARAM_STR);
      $insert_comprobante->bindValue(8,$extraData['document_id'],PDO::PARAM_INT);

      $db->stExec($insert_comprobante);

      return $db->lastInsertId(); */
  }

  protected function prepareInsertarDetallesFinancieros() {
    $insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle', array(
                'dc_comprobante' => $this->dc_comprobante,
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?'
    )));
    $insert_detalle_comprobante->bindParam(1, $this->parametrosFinancieros->dc_cuenta_contable, PDO::PARAM_INT);
    $insert_detalle_comprobante->bindParam(2, $this->parametrosFinancieros->dq_debe, PDO::PARAM_STR);
    $insert_detalle_comprobante->bindParam(3, $this->parametrosFinancieros->dq_haber, PDO::PARAM_STR);
    $insert_detalle_comprobante->bindParam(4, $this->parametrosFinancieros->dg_glosa, PDO::PARAM_STR);

    return $insert_detalle_comprobante;
  }

  protected function insertarDetallesFinancieros() {
    $this->matrizFinanciera = array();

    $this->getComprobanteService()->prepareDetalleFinanciero();

    $this->matrizFinanciera[] = $this->insertarDetalleChequeFinanciero();
    $this->matrizFinanciera[] = $this->insertarDetalleGiroFinanciero();
    
  }
  
      abstract protected function insertarDetalleChequeFinanciero();
      
      abstract protected function insertarDetalleGiroFinanciero();
      
      abstract protected function insertarDetallesAnaliticos();
  
      abstract protected function insertarDetalleChequeAnalitico();

      abstract protected function insertarDetalleGiroAnalitico();

  /**
   * @return ComprobanteContableService
   */
  protected function getComprobanteService() {
    return $this->getService('ComprobanteContable');
  }

}