<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_banco = intval($_POST['id']);

$datos = $db->select("tb_banco",
	"dg_banco,dg_cuenta_corriente,dc_tipo_cambio,dc_cuenta_contable,dg_ejecutivo,dg_telefono,dg_email,dg_sucursal",
	"dc_banco = {$dc_banco} AND dc_empresa = {$empresa}");

if(!count($datos)){
	$error_man->showWarning("No se encontró el banco específicado, intente nuevamente o contacte con un administrador.");
	exit;
}
$datos = $datos[0];

?>
<div class="secc_bar">Edición bancos</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el banco");
	$form->Start("sites/finanzas/proc/editar_banco.php","ed_banco");
	$form->Section();
		$form->Text("Nombre","ed_dg_banco",true,Form::DEFAULT_TEXT_LENGTH,$datos['dg_banco']);
		$form->Text("Número cuenta corriente",'ed_dg_cuenta_corriente',Form::DEFAULT_MANDATORY_STATUS,Form::DEFAULT_TEXT_LENGTH,$datos['dg_cuenta_corriente']);
		$form->Listado('Tipo de cambio','ed_dc_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'),true,$datos['dc_tipo_cambio']);
		$form->Listado('Cuenta contable','ed_dc_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),true,$datos['dc_cuenta_contable']);
	$form->EndSection();
	$form->Section();
		$form->Text('Ejecutivo','ed_dg_ejecutivo',Form::DEFAULT_MANDATORY_STATUS,Form::DEFAULT_TEXT_LENGTH,$datos['dg_ejecutivo']);
		$form->Text('Telefono','ed_dg_telefono',Form::DEFAULT_MANDATORY_STATUS,Form::DEFAULT_TEXT_LENGTH,$datos['dg_telefono']);
		$form->Text('E-mail','ed_dg_email',Form::DEFAULT_MANDATORY_STATUS,Form::DEFAULT_TEXT_LENGTH,$datos['dg_email']);
		$form->Text('Sucursal','ed_dg_sucursal',Form::DEFAULT_MANDATORY_STATUS,Form::DEFAULT_TEXT_LENGTH,$datos['dg_sucursal']);
	$form->EndSection();
	$form->Hidden("ed_dc_banco",$dc_banco);
	$form->End("Editar","editbtn");
?>
</div>