<?php
define("MAIN",1);
require_once("../../../inc/init.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_banco = intval($_POST['ed_dc_banco']);
$dc_tipo_cambio = intval($_POST['ed_dc_tipo_cambio']);
$dc_cuenta_contable = intval($_POST['ed_dc_cuenta_contable']);

if(!$dc_banco || !$dc_cuenta_contable || !$dc_tipo_cambio){
	$error_man->show_fatal_error('Datos inválidos',array(
		"Campos" => "Banco, Cuenta contable, Tipo de cambio",
		"Mensaje" => "Los datos ingresados son inválidos, no se puede continuar."
	));
}

$insert_data = $db->prepare($db->update('tb_banco',array(
	'dg_banco' => '?',
	'dg_cuenta_corriente' => '?',
	'dc_tipo_cambio' => '?',
	'dc_cuenta_contable' => '?',
	'dg_ejecutivo' => '?',
	'dg_telefono' => '?',
	'dg_email' => '?',
	'dg_sucursal' => '?'
),'dc_banco = ?'));

$insert_data->bindValue(1,$_POST['ed_dg_banco'],PDO::PARAM_STR);
$insert_data->bindValue(2,$_POST['ed_dg_cuenta_corriente'],PDO::PARAM_STR);
$insert_data->bindValue(3,$dc_tipo_cambio,PDO::PARAM_INT);
$insert_data->bindValue(4,$dc_cuenta_contable,PDO::PARAM_INT);
$insert_data->bindValue(5,$_POST['ed_dg_ejecutivo'],PDO::PARAM_STR);
$insert_data->bindValue(6,$_POST['ed_dg_telefono'],PDO::PARAM_STR);
$insert_data->bindValue(7,$_POST['ed_dg_email'],PDO::PARAM_STR);
$insert_data->bindValue(8,$_POST['ed_dg_sucursal'],PDO::PARAM_STR);
$insert_data->bindValue(9,$dc_banco,PDO::PARAM_INT);

$db->stExec($insert_data);

$error_man->showConfirm("Se ha modificado el banco correctamente");

?>
<script type="text/javascript">
	pymerp.loadPage("sites/finanzas/src_banco.php");
</script>