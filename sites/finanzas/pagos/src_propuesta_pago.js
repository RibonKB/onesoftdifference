var js_data = {
	
	tabPropuestas: $('#propuestas_data'),
	
	init: function(){
		pymerp.init($('#src_propuesta_pago_res'));
		js_data.tabPropuestas.tableExport().tableAdjust(10);
	},
	
	setStatus: function(dc_propuesta,dm_estado){
		var tr = $('#pt'+dc_propuesta,js_data.tabPropuestas);
		//tr.find('.validar_propuesta').remove();
		tr.find('.estado_propuesta').text(dm_estado);
	},
	
	initConfirmacion: function(dc_propuesta){
		$('#ver_asientos_confirmacion').click(function(){
			$(this).attr('disabled',true);
			pymerp.loadFile('sites/finanzas/pagos/proc/get_asientos_contables.php','#asientos_confirmacion','id='+dc_propuesta,function(){
				window.setTimeout(function(){
					$('#ver_asientos_confirmacion').attr('disabled',false);
				},1000);
			});
		});
	}
	
};
js_data.init();