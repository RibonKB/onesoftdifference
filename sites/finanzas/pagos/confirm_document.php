<?php
define('MAIN',1);
require_once("../../../inc/global.php");
require_once("../../../inc/Factory.class.php");

?>
<div class="secc_bar">
	Confirmación de cobros
</div>
<?php

$dc_pago = $_POST['id'];

if(!$dc_pago){
	$error_man->showWarning('El pago a confirmar es inválido, compruebe los valores de entrada y vuelva a intentarlo');
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="panes center">
	<?php $form->Start(Factory::buildUrl('ConfirmarGiros', 'finanzas', 'ConfirmarChequePropuesta', 'pagos'),'cr_confirmacion') ?>
	<?php
		$form->Header('<b>Seleccione la fecha contable que será utilizada para crear el comprobante de giro</b><br />Los campos marcados con [*] son obligatorios')
	?>
	<?php $form->Date('Fecha contable','df_fecha_contable',1,0) ?>
	<?php $form->Hidden('id',$dc_pago) ?>
	<?php $form->End('Confirmar','checkbtn') ?>
</div>