<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

//Validar número de propuesta
$dc_propuesta = intval($_POST['dc_propuesta']);
if(!$dc_propuesta){
	$error_man->showWarning('La propuesta de pago es inválida no puede continuar con el proceso de administración de pago.');
	exit;
}

//Valida propuesta de pago de entrada
$propuesta = $db->prepare($db->select('tb_propuesta_pago','dq_saldo_favor','dc_propuesta = ?'));
$propuesta->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($propuesta);
$propuesta = $propuesta->fetch(PDO::FETCH_OBJ);

if($propuesta === false){
	$error_man->showWarning('La propuesta de pago ingresada es inválida, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Validar monto ingresado
$dq_monto = floatval($_POST['dq_monto']);
if(!$dq_monto){
	$error_man->showWarning('El monto a cancelar es inválido, modifiquelo y vuelva a intentarlo');
	exit;
}

//Consultar monto total a pagar propuesta
$dq_total_pagar = $db->doQuery($db->select('tb_propuesta_pago_detalle','SUM(dq_monto) dq_monto_pagar','dc_propuesta = '.$dc_propuesta))->fetch(PDO::FETCH_OBJ)->dq_monto_pagar;
if($dq_total_pagar == NULL){
	$error_man->showWarning('No se encuentran montos por pagar en la propuesta de pago, consulte con un administrador.');
	exit;
}
$dq_total_pagar = $dq_total_pagar - $propuesta->dq_saldo_favor;

//Consultar monto ya pagado de la propuesta
$dq_total_pagado = $db->doQuery($db->select('tb_pago_propuesta','SUM(dq_monto) dq_pagado','dc_propuesta = '.$dc_propuesta))->fetch(PDO::FETCH_OBJ)->dq_pagado;
if($dq_total_pagado == NULL){
	$dq_total_pagado = 0;
}
/*
$verifica_cheque=$db->doQuery($db->select('tb_pago_propuesta papro LEFT JOIN tb_propuesta_pago propa ON propa.dc_propuesta=papro.dc_propuesta','*',"papro.dc_banco=".$_POST['dc_banco']." AND papro.dg_documento=".$_POST['dg_documento']." AND propa.dm_nula=0" ));
if($verifica_cheque != NULL){
    $error_man->showWarning('El documento ya se usó para otra propuesta de pago.');
	exit;
}

$verifica_chequeII=$db->doQuery($db->select('tb_documento_pago_anticipado dpa LEFT JOIN tb_comprobante_pago_anticipado cpa ON cpa.dc_comprobante=dpa.dc_comprobante','*',"dpa.dc_banco=".$_POST['dc_banco']." AND dpa.dg_documento=".$_POST['dg_documento']." cpa.AND dm_nula=0" ));
if($verifica_chequeII != NULL){
    $error_man->showWarning('El documento ya se usó para un pago anticipado.');
	exit;
}
*/
//Validar que monto a pagar no exceda monto máximo propuesto
if(($dq_total_pagar-$dq_total_pagado) < $dq_monto){
	$error_man->showWarning('Está intentando pagar más de lo que está comprometido en la propuesta de pago. compruebe los montos y vuelva a intentarlo.');
	exit;
}

$dq_total_pagado += $dq_monto;

$insert_data = $db->prepare($db->insert('tb_pago_propuesta',array(
	'dc_propuesta' => '?',
	'dc_banco' => '?',
	'dg_documento' => '?',
	'df_vencimiento' => '?',
	'dq_monto' => '?',
	'df_emision' => '?',
	'df_creacion' => $db->getNow(),
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
)));
$insert_data->bindValue(1,$dc_propuesta,PDO::PARAM_INT);

//Asignar el banco solo si es requerido (restrictivo a nivel de medio de pago)
if(isset($_POST['dc_banco'])){
	$dc_banco = intval($_POST['dc_banco']);
	if(!$dc_banco){
		$error_man->showWarning('El banco es obligatorio.');
		exit;
	}
	$insert_data->bindValue(2,$dc_banco,PDO::PARAM_INT);
}else{
	$insert_data->bindValue(2,0,PDO::PARAM_INT);
}

//Asignar numero de documento y la fecha de vencimiento del pago solo si es requerido (restrictivo a nivel de medio de pago)
if(isset($_POST['dg_documento'])){
	if(!$_POST['dg_documento'] || !$_POST['df_vencimiento']){
		$error_man->showWarning("El número de documento y la fecha de vencimiento son campos obligatorios.");
		exit;
	}
	$insert_data->bindValue(3,$_POST['dg_documento'],PDO::PARAM_STR);
	$insert_data->bindValue(4,$db->sqlDate2($_POST['df_vencimiento']),PDO::PARAM_STR);
}else{
	$insert_data->bindValue(3,NULL,PDO::PARAM_NULL);
	$insert_data->bindValue(4,NULL,PDO::PARAM_NULL);
}

$insert_data->bindValue(5,$dq_monto,PDO::PARAM_STR);
$insert_data->bindValue(6,$db->sqlDate2($_POST['df_emision']),PDO::PARAM_STR);


$db->start_transaction();
//cambiar estado a completa en caso de que esté completamente pagada
$pagado = $dq_total_pagado >= $dq_total_pagar;
$dm_estado = 'X';
if($pagado){
	$dm_estado = 'P';
	$update_data = $db->prepare($db->update('tb_propuesta_pago',array(
		'dm_estado' => '?'
	),'dc_propuesta = ?'));
	$update_data->bindValue(1,$dm_estado,PDO::PARAM_STR);
	$update_data->bindValue(2,$dc_propuesta,PDO::PARAM_INT);
	$db->stExec($update_data);
}
$db->stExec($insert_data);
$db->commit();

?>
<script type="text/javascript">
	js_data.reloadDocAdmin(<?php echo $dc_propuesta ?>,'<?php echo $dm_estado ?>');
</script>