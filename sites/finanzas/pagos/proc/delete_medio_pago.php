<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dc_medio_pago = intval($_POST['dc_medio_pago_ed']);

$medio_pago = $db->prepare($db->select('tb_medio_pago_proveedor','dm_activo','dc_empresa = ? AND dc_medio_pago = ?'));
$medio_pago->bindValue(1,$empresa,PDO::PARAM_INT);
$medio_pago->bindValue(2,$dc_medio_pago,PDO::PARAM_INT); 
$db->stExec($medio_pago);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_medio_pago_proveedor',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_medio_pago =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_medio_pago,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamento'); 


?>