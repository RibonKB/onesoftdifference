<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once("../../../ventas/proc/ventas_functions.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

/**
*	Separar detalles por proveedor.
*/
$data = array();
$dq_proveedor = array();
foreach($_POST['propuesta_dc_factura'] as $i => $dc_factura){
	$dc_proveedor = intval($_POST['propuesta_dc_proveedor'][$i]);
	$dc_factura = intval($dc_factura);
	$dq_monto = toNumber($_POST['propuesta_dq_monto'][$i]);
	
	if($dc_proveedor == 0 || $dc_factura == 0 || !is_numeric($dq_monto)){
		$error_man->showWarning("Error fatal, los valores enviados son inválidos, no se puede continuar. consulte con un administrador.");
		exit;
	}
	
	$data[$dc_proveedor][] = array(
		"dc_factura" => $dc_factura,
		"dq_monto" => $dq_monto
	);
	
	if(!isset($dq_proveedor[$dc_proveedor])){
		$dq_proveedor[$dc_proveedor] = 0;
	}
	
	$dq_proveedor[$dc_proveedor] += $dq_monto;
	
}

//Almacena el acumulado de todos los saldos a favor para cada proveedor
$saldo_total = array();
//saldos ocupados agrupados por factura
$saldos_favor = array();
//Agrupa los saldos ocupados para cada nota de credito
$saldos_favor_nc = array();
//Agrupa los saldos ocupados para cada orden de compra
$saldos_favor_oc = array();


/**
*	Obtener detalles saldos a favor factura para cada proveedor
*/
if(isset($_POST['dc_factura_favor'])){
	$dc_factura_favor = implode(',',$_POST['dc_factura_favor']);
	
	$saldo_disponible = $db->doQuery(
		$db->select('tb_factura_compra',
					'dc_factura, dc_proveedor, dq_saldo_favor',
					"dc_factura IN ({$dc_factura_favor}) AND dc_empresa = {$empresa}"
		)
	);
	
	
	while($s = $saldo_disponible->fetch(PDO::FETCH_OBJ)){
		
		if($dq_proveedor[$s->dc_proveedor] > 0){
			$dq_proveedor[$s->dc_proveedor] -= $s->dq_saldo_favor;
			
			if($dq_proveedor[$s->dc_proveedor] < 0){
				$s->dq_saldo_favor = $dq_proveedor[$s->dc_proveedor]+$s->dq_saldo_favor;
			}
			
			$saldos_favor[$s->dc_proveedor][$s->dc_factura] = $s->dq_saldo_favor;
			
			if(!isset($saldo_total[$s->dc_proveedor])){
				$saldo_total[$s->dc_proveedor] = 0;
			}
			
			$saldo_total[$s->dc_proveedor] += $s->dq_saldo_favor;
		}
		
	}
	
}

//Rebajar y obtener detalles de saldo a favor de notas de credito
if(isset($_POST['dc_nota_credito_favor'])){
	$dc_nota_credito_favor = implode(',',$_POST['dc_nota_credito_favor']);
	
	$saldo_disponible = $db->doQuery(
		$db->select('tb_nota_credito_proveedor',
					'dc_nota_credito, dc_proveedor, dq_total-dq_saldado dq_saldo_favor',
					"dc_nota_credito IN ({$dc_nota_credito_favor}) AND dc_empresa = {$empresa}"
		)
	);
	
	while($s = $saldo_disponible->fetch(PDO::FETCH_OBJ)){
		
		if($dq_proveedor[$s->dc_proveedor] > 0){
			$dq_proveedor[$s->dc_proveedor] -= $s->dq_saldo_favor;
			
			if($dq_proveedor[$s->dc_proveedor] < 0){
				$s->dq_saldo_favor = $dq_proveedor[$s->dc_proveedor]+$s->dq_saldo_favor;
			}
			
			$saldos_favor_nc[$s->dc_proveedor][$s->dc_nota_credito] = $s->dq_saldo_favor;
			
			if(!isset($saldo_total[$s->dc_proveedor])){
				$saldo_total[$s->dc_proveedor] = 0;
			}
			
			$saldo_total[$s->dc_proveedor] += $s->dq_saldo_favor;
		}
		
	}
	
}

//Rebajar y obtener detalles de saldos a favor para ordenes de compra
if(isset($_POST['dc_orden_compra_favor'])){
	$dc_orden_compra_favor = implode(',',$_POST['dc_orden_compra_favor']);
	
	$saldo_disponible = $db->doQuery(
		$db->select('tb_orden_compra',
					'dc_orden_compra, dc_proveedor, dq_monto_liberado dq_saldo_favor',
					"dc_orden_compra IN ({$dc_orden_compra_favor}) AND dc_empresa = {$empresa}"
		)
	);
	
	while($s = $saldo_disponible->fetch(PDO::FETCH_OBJ)){
		
		if($dq_proveedor[$s->dc_proveedor] > 0){
			$dq_proveedor[$s->dc_proveedor] -= $s->dq_saldo_favor;
			
			if($dq_proveedor[$s->dc_proveedor] < 0){
				$s->dq_saldo_favor = $dq_proveedor[$s->dc_proveedor]+$s->dq_saldo_favor;
			}
			
			$saldos_favor_oc[$s->dc_proveedor][$s->dc_orden_compra] = $s->dq_saldo_favor;
			
			if(!isset($saldo_total[$s->dc_proveedor])){
				$saldo_total[$s->dc_proveedor] = 0;
			}
			
			$saldo_total[$s->dc_proveedor] += $s->dq_saldo_favor;
		}
		
	}
	
}

/**
*	Preparar inserción de propuestas
*/
$dm_estado = 'L';
if($empresa_conf['dm_valida_propuestas_pago'] == 1){
	if($_POST['propuesta_medio_pago'] != 0){
		$medio_pago = $db->prepare($db->select('tb_medio_pago_proveedor','dm_requiere_banco, dm_requiere_documento','dc_medio_pago = ?'));
		$medio_pago->bindValue(1,$_POST['propuesta_medio_pago'],PDO::PARAM_INT);
		$db->stExec($medio_pago);
		$medio_pago = $medio_pago->fetch(PDO::FETCH_OBJ);
		
		if($medio_pago->dm_requiere_banco == 0 && $medio_pago->dm_requiere_documento == 0)
			$dm_estado = 'P';
		else
			$dm_estado = 'V';
	}else{
		$dm_estado = 'P';
	}
}
$crea_propuesta = $db->prepare($db->insert('tb_propuesta_pago',array(
	"dq_propuesta" => '?',
	"dg_propuesta" => '?',
	"dc_proveedor" => '?',
	"dg_comentario" => '?',
	"df_emision" => $db->getNow(),
	"df_propuesta" => '?',
	"dc_medio_pago" => '?',
	"dc_usuario_creacion" => $idUsuario,
	"dc_empresa" => $empresa,
	"dm_estado" => '?',
	"dq_saldo_favor" => '?'
)));
//doc_GetNextNumber('tb_propuesta_pago','dq_propuesta',2,'df_emision')
$crea_propuesta->bindParam(1,$dq_propuesta,PDO::PARAM_INT);
$crea_propuesta->bindValue(2,$_POST['propuesta_name'],PDO::PARAM_STR);
$crea_propuesta->bindParam(3,$dc_proveedor,PDO::PARAM_INT);
$crea_propuesta->bindValue(4,$_POST['propuesta_comentario'],PDO::PARAM_STR);
$crea_propuesta->bindValue(5,str_replace("'","",$db->sqlDate($_POST['propuesta_fecha'])),PDO::PARAM_STR);
$crea_propuesta->bindValue(6,$_POST['propuesta_medio_pago'],PDO::PARAM_INT);
$crea_propuesta->bindValue(7,$dm_estado,PDO::PARAM_STR);
$crea_propuesta->bindParam(8,$dq_saldo_favor_proveedor,PDO::PARAM_STR);

/**
*	Preparar inserción de detalle de propuesta
*/
$crea_detalle = $db->prepare($db->insert('tb_propuesta_pago_detalle',array(
	"dc_propuesta" => '?',
	"dc_factura" => '?',
	"dq_monto" => '?'
)));
$crea_detalle->bindParam(1,$dc_propuesta,PDO::PARAM_INT);
$crea_detalle->bindParam(2,$dc_factura,PDO::PARAM_INT);
$crea_detalle->bindParam(3,$dq_monto,PDO::PARAM_STR);

/**
*	Preparar Modificar monto pagado de facturas de compra
*/
$edit_factura = $db->prepare($db->update('tb_factura_compra',array(
	"dq_monto_pagado" => "dq_monto_pagado+?"
),'dc_factura = ?'));
$edit_factura->bindParam(1,$dq_monto,PDO::PARAM_STR);
$edit_factura->bindParam(2,$dc_factura,PDO::PARAM_INT);

/**
*	Modificar saldo a favor ocupado por factura
*/
$edit_saldo_favor = $db->prepare($db->update('tb_factura_compra',array(
	'dq_saldo_favor' => 'dq_saldo_favor-?'
),'dc_factura = ?'));
$edit_saldo_favor->bindParam(1,$dq_saldo_favor,PDO::PARAM_STR);
$edit_saldo_favor->bindParam(2,$dc_factura_favor,PDO::PARAM_INT);

/**
*	Modificar saldo a favor ocupado por nota de credito
*/
$edit_saldo_favor_nc = $db->prepare($db->update('tb_nota_credito_proveedor',array(
	'dq_saldado' => 'dq_saldado+?'
),'dc_nota_credito = ? '));
$edit_saldo_favor_nc->bindParam(1,$dq_saldo_favor,PDO::PARAM_STR);
$edit_saldo_favor_nc->bindParam(2,$dc_nota_credito_favor,PDO::PARAM_INT);

/**
*	Modificar saldo a favor ocupado por orden de compra
*/
$edit_saldo_favor_oc = $db->prepare($db->update('tb_orden_compra',array(
	'dq_monto_liberado' => 'dq_monto_liberado-?',
	'dq_pago_usado' => 'dq_pago_usado+?'
),'dc_orden_compra = ?'));
$edit_saldo_favor_oc->bindParam(1,$dq_saldo_favor,PDO::PARAM_STR);
$edit_saldo_favor_oc->bindParam(2,$dq_saldo_favor,PDO::PARAM_STR);
$edit_saldo_favor_oc->bindParam(3,$dc_orden_compra_favor,PDO::PARAM_INT);

/**
*	Agregar detalle de saldo a favr a propuesta de pago
*/
$crea_detalle_favor = $db->prepare($db->insert('tb_saldo_favor_propuesta',array(
	'dc_propuesta' => '?',
	'dc_factura_compra' => '?',
	'dc_nota_credito' => '?',
	'dc_orden_compra' => '?',
	'dq_monto' => '?'
)));
$crea_detalle_favor->bindParam(1,$dc_propuesta,PDO::PARAM_INT);
$crea_detalle_favor->bindParam(2,$dc_factura_favor,PDO::PARAM_INT);
$crea_detalle_favor->bindParam(3,$dc_nota_credito_favor,PDO::PARAM_INT);
$crea_detalle_favor->bindParam(4,$dc_orden_compra_favor,PDO::PARAM_INT);
$crea_detalle_favor->bindParam(5,$dq_saldo_favor,PDO::PARAM_STR);

$dc_factura_favor = NULL;
$dc_nota_credito_favor = NULL;
$dc_orden_compra_favor = NULL;

/**
*	Modificar saldo a favor disponible apra el proveedor
*/
$edit_proveedor_favor = $db->prepare($db->update('tb_proveedor',array(
	'dq_saldo_favor' => 'dq_saldo_favor - ?'
),'dc_proveedor = ?'));
$edit_proveedor_favor->bindParam(1,$dq_saldo_favor_proveedor,PDO::PARAM_STR);
$edit_proveedor_favor->bindParam(2,$dc_proveedor,PDO::PARAM_INT);

/**
*	Por cada proveedor se inserta una propuesta
*/
$db->start_transaction();

$doc_num = array();
foreach($data as $i => $v){
	$dc_proveedor = $i;
	$dq_propuesta = doc_GetNextNumber('tb_propuesta_pago','dq_propuesta',2,'df_emision');
	
	//Modificar saldo a favor del proveedor.
	if(isset($saldo_total[$dc_proveedor])){
		$dq_saldo_favor_proveedor = floatval($saldo_total[$dc_proveedor]);
		$db->stExec($edit_proveedor_favor);
	}else{
		$dq_saldo_favor_proveedor = 0;
	}
	
	$db->stExec($crea_propuesta);
	
	$dc_propuesta = $db->lastInsertId();
	
	/**
	*	Insertar los detalles para la propuesta del proveedor.
	*/
	foreach($v as $detalle){
		$dc_factura = $detalle['dc_factura'];
		$dq_monto = round($detalle['dq_monto'],$empresa_conf['dn_decimales_local']);
		
		$db->stExec($crea_detalle);
		$db->stExec($edit_factura);
	}
	
	/**
	*	Insertar detalles de saldo a favor  modificar saldo a favor disponible por factura de compra
	*/
	if(isset($saldos_favor[$dc_proveedor])){
		foreach($saldos_favor[$dc_proveedor] as $i => $v){
			$dc_factura_favor = $i;
			$dq_saldo_favor = floatval($v);
			
			$db->stExec($edit_saldo_favor);
			$db->stExec($crea_detalle_favor);
		}
		$dc_factura_favor = NULL;
	}
	
	if(isset($saldos_favor_nc[$dc_proveedor])){
		foreach($saldos_favor_nc[$dc_proveedor] as $i => $v){
			$dc_nota_credito_favor = $i;
			$dq_saldo_favor = floatval($v);
			
			$db->stExec($edit_saldo_favor_nc);
			$db->stExec($crea_detalle_favor);
		}
		$dc_nota_credito_favor = NULL;
	}
	
	if(isset($saldos_favor_oc[$dc_proveedor])){
		foreach($saldos_favor_oc[$dc_proveedor] as $i => $v){
			$dc_orden_compra_favor = $i;
			$dq_saldo_favor = floatval($v);
			
			$db->stExec($edit_saldo_favor_oc);
			$db->stExec($crea_detalle_favor);
		}
	}
	
	$doc_num[$dq_propuesta] = $db->doQuery($db->select('tb_proveedor','dg_razon',"dc_proveedor = {$dc_proveedor}"))->fetch(PDO::FETCH_OBJ)->dg_razon;
	
}

$db->commit();

$error_man->showConfirm('Se han generado las propuestas correctamente, los números de propuestas por proveedor son descritas a continuación:');
?>
<div class="info">
	<?php foreach($doc_num as $dq_propuesta => $dg_razon): ?>
		<h2>
			<?php echo $dq_propuesta ?>
			<small> -
				<?php echo $dg_razon ?>
			</small>
		</h2>
	<?php endforeach; ?>
</div>
<script type="text/javascript">
	<?php foreach($_POST['propuesta_dc_factura'] as $f): ?>
	$('#factura_<?php echo $f ?>').remove();
	<?php endforeach; ?>
	<?php if($dm_estado == 'V'): ?>
		js_data.reloadDocAdmin(<?php echo $dc_propuesta ?>,'<?php echo $dm_estado ?>');
	<?php endif; ?>
</script>