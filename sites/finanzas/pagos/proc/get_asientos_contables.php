<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_propuesta = intval($_POST['id']);

//Obtener detalles de cabecera de la propuesta
$cond = $db->prepare($db->select('tb_propuesta_pago p
LEFT JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago',
'm.dm_asiento_intermedio,m.dm_requiere_banco, m.dm_requiere_documento, m.dc_tipo_movimiento, m.dc_cuenta_contable_intermedia,
p.dc_proveedor, p.dg_comentario, p.dg_propuesta, p.dq_propuesta, p.dq_saldo_favor, p.dc_medio_pago',
'p.dc_propuesta = ? AND p.dc_empresa = ?'));
$cond->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$cond->bindValue(2,$empresa);

$db->stExec($cond);
$cond = $cond->fetch(PDO::FETCH_OBJ);

if($cond === false){
	$error_man->showAviso('Error inesperado, no se encontró la propuesta');
	exit;
}

//Obtener datos requeridos del proveedor
$proveedor = $db->prepare($db->select('tb_proveedor p
JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = p.dc_cuenta_contable',
'c.dg_cuenta_contable, c.dg_codigo, p.dc_cuenta_anticipo','p.dc_proveedor = ?'));
$proveedor->bindValue(1,$cond->dc_proveedor,PDO::PARAM_INT);
$db->stExec($proveedor);
$proveedor = $proveedor->fetch(PDO::FETCH_OBJ);

$detalles = array();
$dq_saldo = 0;

	//Facturas a pagar (van en el DEBE de cualquier forma de pago)
	$detalles_factura = $db->prepare($db->select('tb_propuesta_pago_detalle d
	JOIN tb_factura_compra f ON f.dc_factura = d.dc_factura',
	'd.dq_monto, f.dq_folio, f.dq_factura',
	'd.dc_propuesta = ?'));
	$detalles_factura->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
	$db->stExec($detalles_factura);
	
	while($d = $detalles_factura->fetch(PDO::FETCH_OBJ)){
		$detalles[] = array(
			'CUENTA' => $proveedor->dg_cuenta_contable,
			'CODIGO' => $proveedor->dg_codigo,
			'GLOSA' => "Factura compra: {$d->dq_factura}, folio: {$d->dq_folio}",
			'DEBE' => $d->dq_monto,
			'HABER' => 0
		);
		$dq_saldo += $d->dq_monto;
	}

if($cond->dc_medio_pago != 0){
	//Documento y banco
	if($cond->dm_requiere_documento == 1 && $cond->dm_requiere_banco == 1 && $cond->dm_asiento_intermedio == 1){
		$pagos = $db->prepare($db->select('tb_pago_propuesta pp
		LEFT JOIN tb_banco b ON b.dc_banco = pp.dc_banco','pp.dq_monto, pp.df_vencimiento, pp.dg_documento, b.dg_banco','pp.dc_propuesta = ?'));
		$pagos->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
		$db->stExec($pagos);
		
		$cuenta_haber = $db->doQuery($db->select('tb_cuenta_contable','dg_cuenta_contable,dg_codigo','dc_cuenta_contable='.$cond->dc_cuenta_contable_intermedia));
		$cuenta_haber = $cuenta_haber->fetch(PDO::FETCH_OBJ);
		
		while($d = $pagos->fetch(PDO::FETCH_OBJ)){
			$d->df_vencimiento = $db->dateLocalFormat($d->df_vencimiento);
			$detalles[] = array(
				'CUENTA' => $cuenta_haber->dg_cuenta_contable,
				'CODIGO' => $cuenta_haber->dg_codigo,
				'GLOSA' => "Pago cheque: {$d->dg_documento} banco: {$d->dg_banco} vencimiento: {$d->df_vencimiento}",
				'DEBE' => 0,
				'HABER' => $d->dq_monto
			);
			
		}
	}
	//Sin documento con banco
	else if($cond->dm_requiere_banco == 1 && $cond->dm_requiere_documento == 0){
		$pagos = $db->prepare($db->select('tb_pago_propuesta pp
		JOIN tb_banco b ON pp.dc_banco = b.dc_banco
		JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = b.dc_cuenta_contable',
		'pp.dq_monto, c.dg_cuenta_contable, c.dg_codigo, b.dg_banco, pp.df_emision',
		'pp.dc_propuesta = ?'));
		$pagos->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
		$db->stExec($pagos);
		
		while($d = $pagos->fetch(PDO::FETCH_OBJ)){
			$d->df_emision = $db->dateLocalFormat($d->df_emision);
			$detalles[] = array(
				'CUENTA' => $d->dg_cuenta_contable,
				'CODIGO' => $d->dg_codigo,
				'GLOSA' => "Pago transferencia banco: {$d->dg_banco} fecha: {$d->df_emision}",
				'DEBE' => 0,
				'HABER' => $d->dq_monto
			);
		}
	}
	//Sin documento sin banco
	else if($cond->dm_requiere_documento == 0 && $cond->dm_requiere_banco == 0){
		
		$cuentas = $db->prepare($db->select('tb_cuenta_contable cc
							JOIN tb_cuentas_tipo_movimiento tm ON tm.dc_cuenta_contable = cc.dc_cuenta_contable',
							'cc.dc_cuenta_contable, cc.dg_cuenta_contable, cc.dg_codigo',
							'tm.dc_tipo_movimiento = ?'));
		$cuentas->bindValue(1,$cond->dc_tipo_movimiento,PDO::PARAM_INT);
		$db->stExec($cuentas);
		$dg_select = '<select class="inputtext">';
		while($c = $cuentas->fetch(PDO::FETCH_OBJ)){
			$dg_select .= '<option>'.$c->dg_codigo.' - '.$c->dg_cuenta_contable.'</option>';
		}
		$dg_select .= '</select>';
		
		$detalles[] = array(
			'CUENTA' => $dg_select,
			'CODIGO' => '',
			'GLOSA' => 'Pago efectivo',
			'DEBE' => 0,
			'HABER' => $dq_saldo
		);
		
	}
}

if($cond->dq_saldo_favor > 0){
	/*$detalles[] = array(
		'CUENTA' => $dg_select,
		'CODIGO' => '',
		'GLOSA' => 'Pago efectivo',
		'DEBE' => 0,
		'HABER' => $dq_saldo
	);*/
		
	$favor_data = $db->prepare($db->select('tb_saldo_favor_propuesta','dc_factura_compra, dc_orden_compra, dc_nota_credito, dq_monto','dc_propuesta = ?'));
	$favor_data->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
	$db->stExec($favor_data);
	
	while($d = $favor_data->fetch(PDO::FETCH_OBJ)){
		
		if($d->dc_factura_compra != NULL || $d->dc_orden_compra != NULL){
			
			$cuenta_anticipo = $db->doQuery(
					$db->select('tb_cuenta_contable','dg_codigo, dg_cuenta_contable','dc_cuenta_contable = '.$proveedor->dc_cuenta_anticipo)
				)->fetch(PDO::FETCH_OBJ);
				
			if($d->dc_factura_compra != NULL){
				$factura = $db->doQuery($db->select('tb_factura_compra','dq_factura, dq_folio',"dc_factra = {$d->dc_factura_compra}"));
				$factura = $factura->fetch(PDO::FETCH_OBJ);
				
				$to_glosa = "Factura de compra: {$factura->dq_factura_compra} folio: {$factura->dq_folio}";
				
			}else if($d->dc_orden_compra != NULL){
				$orden_compra = $db->doQuery($db->select('tb_orden_compra','dq_orden_compra',"dc_orden_compra = {$d->dc_orden_compra}"));
				$orden_compra = $orden_compra->fetch(PDO::FETCH_OBJ);
				
				$to_glosa = "Orden de compra: {$orden_compra->dq_orden_compra}";
				
			}
			
			$detalles[] = array(
				'CUENTA' => $cuenta_anticipo->dg_cuenta_contable,
				'CODIGO' => $cuenta_anticipo->dg_codigo,
				'GLOSA' => 'Pago con saldo a favor '.$to_glosa,
				'DEBE' => 0,
				'HABER' => $d->dq_monto
			);
			
		}else if($d->dc_nota_credito != NULL){
			
			$nota_credito = $db->doQuery($db->select('tb_nota_credito_proveedor','dq_nota_credito, dq_folio',"dc_nota_credito = {$d->dc_nota_credito}"));
			$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);
				
			$detalles[] = array(
				'CUENTA' => $proveedor->dg_cuenta_contable,
				'CODIGO' => $proveedor->dg_codigo,
				'GLOSA' => "Pago con saldo a favor Nota de credito: {$nota_credito->dq_nota_credito} folio: {$nota_credito->dq_folio}",
				'DEBE' => 0,
				'HABER' => $d->dq_monto
			);
			
		}
	}
}

?>
<table class="tab" width="100%" id="tb_asientos_confirmacion">
<caption>Asiento contable generado al confirmar</caption>
<thead><tr>
	<th>Código</th>
	<th>Cuenta Contable</th>
	<th>Glosa</th>
	<th>DEBE</th>
	<th>HABER</th>
</tr></thead>
<tbody>
	<?php foreach($detalles as $d): ?>
	<tr>
		<?php if($d['CODIGO'] != ''): ?>
			<td><?php echo $d['CODIGO'] ?></td>
			<td><?php echo $d['CUENTA'] ?></td>
		<?php else: ?>
			<td colspan="2"><?php echo $d['CUENTA'] ?></td>
		<?php endif; ?>
		<td><?php echo $d['GLOSA'] ?></td>
		<td align="right"><?php echo moneda_local($d['DEBE']) ?></td>
		<td align="right"><?php echo moneda_local($d['HABER']) ?></td>
	</tr>
	<?php endforeach; ?>
</tbody>
<tfoot>
	<tr>
		<th align="right" colspan="3">Saldo</th>
		<th align="right"><?php echo moneda_local($dq_saldo) ?></th>
		<th align="right"><?php echo moneda_local($dq_saldo) ?></th>
	</tr>
</tfoot>
</table>