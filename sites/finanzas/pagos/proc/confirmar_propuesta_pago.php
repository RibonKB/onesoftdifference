<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_propuesta = intval($_POST['id']);
$dm_estado = 'C';

$cond = $db->prepare($db->select('tb_propuesta_pago p
LEFT JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago',
'm.dm_asiento_intermedio,m.dm_requiere_banco, m.dm_requiere_documento, m.dc_tipo_movimiento, m.dc_cuenta_contable_intermedia,
p.dc_proveedor, p.dg_comentario, p.dg_propuesta, p.dq_propuesta, m.dg_medio_pago, p.dq_saldo_favor, p.dc_medio_pago',
'p.dc_propuesta = ? AND p.dc_empresa = ? AND p.dm_estado = "P"'));
$cond->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$cond->bindValue(2,$empresa);

$db->stExec($cond);
$cond = $cond->fetch(PDO::FETCH_OBJ);

if($cond === false){
	$error_man->showAviso('No se pudo completar la confirmación de la propuesta, las posibles son:<br /><br />
	<ul>
		<li>La propuesta ingresada es inválida</li>
		<li>La propuesta ya ha sido confirmada anteriormente</li>
		<li>No se han generado los comprobantes de pago necesarios previa confirmación</li>
	</ul>');
	exit;
}

$dq_saldo_favor = floatval($cond->dq_saldo_favor);

//Para pago en efectivo
if($cond->dm_requiere_documento == 0 && $cond->dm_requiere_banco == 0 && $cond->dc_medio_pago != 0){
	
	//En caso de que el pago sea en efectivo se debe selccionar la cuenta donde se hará el asiento al haber.
	if(!isset($_POST['dc_cuenta_efectivo'])){
		require_once("../../../../inc/form-class.php");
		$form = new Form($empresa);
		
		echo('<div class="panes center">');
			$form->Start('sites/finanzas/pagos/proc/confirmar_propuesta_pago.php','confirmar_efectivo');
				$form->Header('Indique la cuenta en la cual será registrado el pago');
				
				//Cuentas contables del tipo de movimiento definidio en el tipo de pago de la propuesta de pago
				$cuentas = $db->prepare($db->select('tb_cuenta_contable cc
								JOIN tb_cuentas_tipo_movimiento tm ON tm.dc_cuenta_contable = cc.dc_cuenta_contable',
								'cc.dc_cuenta_contable, cc.dg_cuenta_contable',
								'tm.dc_tipo_movimiento = ?'));
				$cuentas->bindValue(1,$cond->dc_tipo_movimiento,PDO::PARAM_INT);
				$db->stExec($cuentas);
				$arr_cuentas = array();
				while($c = $cuentas->fetch(PDO::FETCH_OBJ)){
					$arr_cuentas[$c->dc_cuenta_contable] = $c->dg_cuenta_contable;
				}
				$form->Select('Cuenta contable','dc_cuenta_efectivo',$arr_cuentas,1);
				$form->Hidden('id',$dc_propuesta);
			$form->End('Confirmar','checkbtn');
		echo('</div>');
		exit;
	}
	
	$dc_cuenta_haber = intval($_POST['dc_cuenta_efectivo']);
	if(!$dc_cuenta_haber){
		$error_man->showWarning('No se ha seleccionado correctamente la cuenta contable del pago en efectivo, por favor contactar con el administrador.');
		exit;
	}
	
}

if($cond->dm_asiento_intermedio == 0){
	$dm_estado = 'T';
}

$db->start_transaction();

//Se cambia el estado dependiendo si tiene o no asiento intermedio
$update_estado = $db->prepare($db->update('tb_propuesta_pago',array(
	'dm_estado' => '?'
),'dc_propuesta = ?'));
$update_estado->bindValue(1,$dm_estado,PDO::PARAM_STR);
$update_estado->bindValue(2,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($update_estado);

//Obtener el número del siguiente comprobante contable
$dg_comprobante = $db->prepare($db->select('tb_comprobante_contable',
			'dg_comprobante',
			'dc_empresa=?',
			array('order_by' => 'df_fecha_emision DESC', 'limit' => 1)));
$dg_comprobante->bindValue(1,$empresa,PDO::PARAM_INT);
$db->stExec($dg_comprobante);
$dg_comprobante = $dg_comprobante->fetch(PDO::FETCH_OBJ);

if($dg_comprobante !== false && substr($dg_comprobante->dg_comprobante,0,6) == date("Ym")){
	$dg_comprobante = date("Ym").((int)substr($dg_comprobante->dg_comprobante,6)+1);
}else{
	$dg_comprobante = date("Ym").'0';
}

//Obtener datos requeridos del proveedor
$proveedor = $db->prepare($db->select('tb_proveedor','dg_razon, dg_rut, dc_cuenta_contable, dc_cuenta_anticipo','dc_proveedor = ?'));
$proveedor->bindValue(1,$cond->dc_proveedor,PDO::PARAM_INT);
$db->stExec($proveedor);
$proveedor = $proveedor->fetch(PDO::FETCH_OBJ);

//Insertar cabecera del comprobante contable
$insert_comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
	'dg_comprobante' => '?',
	'dc_tipo_movimiento' => '?',
	'df_fecha_contable' => '?',
	'dc_mes_contable' => 'MONTH(?)',
	'dc_anho_contable' => 'YEAR(?)',
	'dc_tipo_cambio' => '?',
	'dq_cambio' => 1,
	'dg_glosa' => '?',
	'dc_propuesta_pago' => '?',
	'df_fecha_emision' => $db->getNow(),
	'dc_empresa' => $empresa,
	'dc_usuario_creacion' => $idUsuario
)));
$insert_comprobante->bindValue(1,$dg_comprobante,PDO::PARAM_INT);
$insert_comprobante->bindValue(2,$cond->dc_tipo_movimiento,PDO::PARAM_INT);
$insert_comprobante->bindValue(3,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
$insert_comprobante->bindValue(4,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
$insert_comprobante->bindValue(5,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
$insert_comprobante->bindValue(6,0,PDO::PARAM_INT);
$insert_comprobante->bindValue(7,"Propuesta: {$cond->dq_propuesta} - {$cond->dg_propuesta}\nProveedor:{$proveedor->dg_razon}\n\n{$cond->dg_comentario}",PDO::PARAM_STR);
$insert_comprobante->bindValue(8,$dc_propuesta,PDO::PARAM_INT);

$db->stExec($insert_comprobante);

$dc_comprobante = $db->lastInsertId();

/*
*	Insertar los detalles del comprobante de la siguiente manera dependiendo de las opciones del medio de pago
*
*	Si posee documento y requiere banco (Carga pagos previos):
*		DEBE: Facturas a pagar
*		HABER: Bancos del documento
*
*	Si no posee documento y requiere banco (Carga pagos previos):
*		DEBE: Facturas a pagar
*		HABER: Bancos configurados en acción intermedia
*
*	Si no posee documento y no requiere banco
*		DEBE: Facturas a pagar
*		HABER: Cuenta seleccionada $dc_cuenta_haber en formulario
*/

//Facturas a pagar (van en el DEBE de cualquier forma de pago)
$detalles_factura = $db->prepare($db->select('tb_propuesta_pago_detalle d
JOIN tb_factura_compra f ON f.dc_factura = d.dc_factura',
'd.dc_factura, d.dq_monto, f.dq_folio, f.dq_factura',
'd.dc_propuesta = ?'));
$detalles_factura->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($detalles_factura);

$insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
	'dc_comprobante' => $dc_comprobante,
	'dc_cuenta_contable' => '?',
	'dq_debe' => '?',
	'dq_haber' => '?',
	'dg_glosa' => '?',
	'dg_cheque' => '?',
	'df_fecha_cheque' => '?',
	'dg_rut' => '?',
	'dc_banco' => '?',
	'dg_doc_compra' => '?',
	'dc_proveedor' => '?',
	'dc_factura_compra' => '?',
	'dc_orden_compra' => '?',
	'dc_nota_credito_proveedor' => '?'
)));
$insert_detalle_comprobante->bindParam(1,$dc_cuenta_contable,PDO::PARAM_INT);
$insert_detalle_comprobante->bindParam(2,$dq_debe,PDO::PARAM_STR);
$insert_detalle_comprobante->bindParam(3,$dq_haber,PDO::PARAM_STR);
$insert_detalle_comprobante->bindParam(4,$dg_glosa,PDO::PARAM_STR);
$insert_detalle_comprobante->bindParam(5,$dg_cheque,PDO::PARAM_STR);
$insert_detalle_comprobante->bindParam(6,$df_fecha_cheque,PDO::PARAM_STR);
$insert_detalle_comprobante->bindParam(7,$dg_rut,PDO::PARAM_STR);
$insert_detalle_comprobante->bindParam(8,$dc_banco,PDO::PARAM_INT);
$insert_detalle_comprobante->bindParam(9,$dq_factura,PDO::PARAM_STR);
$insert_detalle_comprobante->bindValue(10,$cond->dc_proveedor,PDO::PARAM_INT);
$insert_detalle_comprobante->bindParam(11,$dc_factura,PDO::PARAM_INT);
$insert_detalle_comprobante->bindParam(12,$dc_orden_compra,PDO::PARAM_INT);
$insert_detalle_comprobante->bindParam(13,$dc_nota_credito,PDO::PARAM_INT);

$dc_cuenta_contable = $proveedor->dc_cuenta_contable;
$dq_haber = '0';
$dg_cheque = '';
$df_fecha_cheque = '';
$dg_rut = $proveedor->dg_rut;
$dc_banco = 0;
$dc_orden_compra = 0;
$dc_nota_credito = 0;

$dq_saldo = 0;

while($d = $detalles_factura->fetch(PDO::FETCH_OBJ)){
	$dq_debe = $d->dq_monto;
	$dg_glosa = "Factura compra: {$d->dq_factura}, folio: {$d->dq_folio}, Proveedor:{$proveedor->dg_razon}";
	$dq_factura = $d->dq_factura;
	$dc_factura = $d->dc_factura;
	
	$db->stExec($insert_detalle_comprobante);
	
	$dq_saldo += $d->dq_monto;
}

$dq_debe = '0';
$dq_factura = '';
$dc_factura = 0;

if($cond->dc_medio_pago != 0):
	//Documento y banco
	if($cond->dm_requiere_documento == 1 && $cond->dm_requiere_banco == 1 && $cond->dm_asiento_intermedio == 1){
		$pagos = $db->prepare($db->select('tb_pago_propuesta pp
		JOIN tb_banco b ON pp.dc_banco = b.dc_banco',
		'pp.dc_banco, pp.dg_documento, pp.df_vencimiento, pp.dq_monto, pp.df_emision, b.dg_banco',
		'pp.dc_propuesta = ?'));
		$pagos->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
		$db->stExec($pagos);
		
		$dc_cuenta_contable = $cond->dc_cuenta_contable_intermedia;
		
		while($d = $pagos->fetch(PDO::FETCH_OBJ)){
			$dq_haber = $d->dq_monto;
			$dg_glosa = "{$cond->dg_medio_pago}: {$d->dg_documento} banco: {$d->dg_banco} vencimiento: {$d->df_vencimiento} Proveedor:{$proveedor->dg_razon}";
			$dg_cheque = $d->dg_documento;
			$df_fecha_cheque = $d->df_emision;
			$dc_banco = $d->dc_banco;
			
			$db->stExec($insert_detalle_comprobante);
			
		}
		
	}
	//Sin documento con banco
	else if($cond->dm_requiere_banco == 1 && $cond->dm_requiere_documento == 0){
		$pagos = $db->prepare($db->select('tb_pago_propuesta pp
		JOIN tb_banco b ON pp.dc_banco = b.dc_banco',
		'pp.dc_banco, pp.dq_monto, pp.df_emision, b.dg_banco, b.dc_cuenta_contable',
		'pp.dc_propuesta = ?'));
		$pagos->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
		$db->stExec($pagos);
		
		$dg_cheque = '';
		$df_fecha_cheque = '';
		
		while($d = $pagos->fetch(PDO::FETCH_OBJ)){
			$dc_cuenta_contable = $d->dc_cuenta_contable;
			$dg_glosa = "{$cond->dg_medio_pago}: {$d->dg_banco} fecha: {$d->df_emision} Proveedor:{$proveedor->dg_razon}";
			$dc_banco = $d->dc_banco;
			$dq_haber = $d->dq_monto;
			
			$db->stExec($insert_detalle_comprobante);
		}
	}
	//Sin documento sin banco
	else if($cond->dm_requiere_documento == 0 && $cond->dm_requiere_banco == 0){
		
		$dg_cheque = '';
		$df_fecha_cheque = '';
		$dc_cuenta_contable = $dc_cuenta_haber;
		$dg_glosa = "{$cond->dg_medio_pago} Proveedor:{$proveedor->dg_razon}";
		$dc_banco = 0;
		$dq_haber = $dq_saldo - $dq_saldo_favor;
		
		$db->stExec($insert_detalle_comprobante);
		
	}
endif; //Fin asientos solo con medio de pago

if($dq_saldo_favor > 0){
	$favor_data = $db->prepare($db->select('tb_saldo_favor_propuesta','dc_factura_compra, dc_orden_compra, dc_nota_credito, dq_monto','dc_propuesta = ?'));
	$favor_data->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
	$db->stExec($favor_data);
	
	$dq_debe = 0;
	$dg_cheque = '';
	$df_fecha_cheque = '';
	$dg_rut = $proveedor->dg_rut;
	$dc_banco = 0;
	$dq_factura = 0;
	
	while($d = $favor_data->fetch(PDO::FETCH_OBJ)){
		
		if($d->dc_factura_compra != NULL || $d->dc_orden_compra != NULL){
			
			if($d->dc_factura_compra != NULL){
				$factura = $db->doQuery($db->select('tb_factura_compra','dq_factura, dq_folio',"dc_factura = {$d->dc_factura_compra}"));
				$factura = $factura->fetch(PDO::FETCH_OBJ);
				
				$to_glosa = "Factura de compra: {$factura->dq_factura} folio: {$factura->dq_folio}";
				
			}else if($d->dc_orden_compra != NULL){
				$orden_compra = $db->doQuery($db->select('tb_orden_compra','dq_orden_compra',"dc_orden_compra = {$d->dc_orden_compra}"));
				$orden_compra = $orden_compra->fetch(PDO::FETCH_OBJ);
				
				$to_glosa = "Orden de compra: {$orden_compra->dq_orden_compra}";
				
			}
			
			$dc_cuenta_contable = $proveedor->dc_cuenta_anticipo;
			$dq_haber = $d->dq_monto;
			$dc_factura = intval($d->dc_factura_compra);
			$dc_orden_compra = intval($d->dc_orden_compra);
			$dc_nota_credito = 0;
			$dg_glosa = 'Pago con saldo a favor '.$to_glosa;
			
			$db->stExec($insert_detalle_comprobante);
		}else if($d->dc_nota_credito != NULL){
			
			$nota_credito = $db->doQuery($db->select('tb_nota_credito_proveedor','dq_nota_credito, dq_folio',"dc_nota_credito = {$d->dc_nota_credito}"));
			$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);
			
			$dc_cuenta_contable = $proveedor->dc_cuenta_contable;
			$dc_factura = 0;
			$dc_orden_compra = 0;
			$dc_nota_credito = intval($d->dc_nota_credito);
			$dg_glosa = "Pago con saldo a favor Nota de credito: {$nota_credito->dq_nota_credito} folio: {$nota_credito->dq_folio}";
			$dq_haber = $d->dq_monto;
			
			$db->stExec($insert_detalle_comprobante);
			
		}
	}
}

//Actualizar saldo en comprobante contable
$db->doExec($db->update('tb_comprobante_contable',array(
	'dq_saldo' => $dq_saldo
),'dc_comprobante='.$dc_comprobante));

$db->commit();

$error_man->showConfirm('Se ha confirmado correctamente el pago de la propuesta <b>'.$cond->dq_propuesta.'</b><br /><br />
<a href="sites/contabilidad/ver_comprobante.php?id='.$dc_comprobante.'" target="_blank" class="button">Imprimir comprobante contable</a><br />');

?>
<script type="text/javascript">
	js_data.refreshListado();
</script>