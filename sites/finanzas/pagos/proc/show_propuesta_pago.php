<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_propuesta = intval($_POST['id']);

if(!$dc_propuesta){
	$error_man->showWarning("No se pudo extraer la información de la propuesta de pago debido, error de entrada.");
	exit;
}

$data = $db->prepare($db->select('tb_propuesta_pago','dq_propuesta, dg_propuesta','dc_propuesta = ? AND dc_empresa = '.$empresa));
$data->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($data);

$data = $data->fetch(PDO::FETCH_OBJ);

if($data === false){
	$error_man->showWarning("No se encontró la propuesta de pago, verificar los datos de entrada.");
	exit;
}

$detail = $db->prepare(
	$db->select(
		'tb_propuesta_pago_detalle pd
		JOIN tb_factura_compra fc ON fc.dc_factura = pd.dc_factura
		LEFT JOIN tb_nota_venta nv ON fc.dc_nota_venta = nv.dc_nota_venta
		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fc.dc_orden_servicio
		LEFT JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
		LEFT JOIN tb_factura_venta fv ON fv.dc_nota_venta = nv.dc_nota_venta',
		'pd.dq_monto, fc.dq_factura, fc.dq_folio, nv.dq_nota_venta, os.dq_orden_servicio, fc.df_emision df_emision_factura_compra,
		fc.df_vencimiento df_vencimiento_factura_compra, cl.dg_razon, DATEDIFF(NOW(),fc.df_vencimiento) dc_dias_vencimiento,
		GROUP_CONCAT(fv.dq_factura,"(<b>",fv.dq_folio,"</b>)" SEPARATOR ",<br />") dq_factura_venta',
		'pd.dc_propuesta = ?',array('group_by' => 'pd.dc_factura')));
$detail->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($detail);


?>
<div class="secc_bar">
	Detalle Propuesta de Pago
</div>
<div class="panes">
<div class="info">
	Detalles propuesta de pago <b><?php echo $data->dq_propuesta ?> - <?php echo $data->dg_propuesta ?></b>
</div>
<table class="tab" width="100%" id="propuesta_detail_data">
<thead>
	<tr>
		<th>FC</th>
		<th>Folio</th>
		<th>F. emisión (FC)</th>
		<th>F. vencimiento</th>
		<th title="Días vencimiento">DV</th>
		<th>Cliente</th>
		<th>NV</th>
		<th>OS</th>
		<th>FV</th>
		<th>Monto a Pagar</th>
	</tr>
</thead>
<tbody>
<?php foreach($detail as $d): ?>
	<tr>
		<td><?php echo $d['dq_factura'] ?></td>
		<td><b><?php echo $d['dq_folio'] ?></b></td>
		<td><?php echo $db->dateLocalFormat($d['df_emision_factura_compra']) ?></td>
		<td><?php echo $db->dateLocalFormat($d['df_vencimiento_factura_compra']) ?></td>
		<td align="right"><?php echo $d['dc_dias_vencimiento'] ?></td>
		<td><?php echo $d['dg_razon'] ?></td>
		<td><?php echo $d['dq_nota_venta'] ?></td>
		<td><?php echo $d['dq_orden_servicio'] ?></td>
		<td><?php echo $d['dq_factura_venta'] ?></td>
		<td align="right"><?php echo moneda_local($d['dq_monto']) ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
<script type="text/javascript">
window.setTimeout(function(){
	$('#propuesta_detail_data').tableExport().tableAdjust(10);
},100);
</script>