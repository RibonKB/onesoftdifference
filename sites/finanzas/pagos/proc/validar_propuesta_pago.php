<div class="secc_bar">
	Validación de propuestas de pago
</div>
<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_propuesta = intval($_POST['id']);
$dm_estado = 'V';

$cond = $db->prepare($db->select('tb_propuesta_pago p
LEFT JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago',
'm.dm_requiere_banco, m.dm_requiere_documento',
'p.dc_propuesta = ? AND p.dc_empresa = ? AND p.dm_estado = "L"'));
$cond->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$cond->bindValue(2,$empresa,PDO::PARAM_INT);

$db->stExec($cond);
$cond = $cond->fetch(PDO::FETCH_OBJ);

if($cond === false){
	$error_man->showAviso('No se pudo completar la validación de la propuesta, las posibles son:<br /><br />
	<ul>
		<li>La propuesta ingresada es inválida</li>
		<li>La propuesta ya ha sido validada anteriormente</li>
	</ul>');
	exit;
}

if($cond->dm_requiere_banco == 0 && $cond->dm_requiere_documento == 0){
	$dm_estado = 'P';
}

$validar = $db->prepare($db->update('tb_propuesta_pago',array(
	'dm_estado' => '?'
),'dc_empresa = ? AND dc_propuesta = ?'));
$validar->bindValue(1,$dm_estado,PDO::PARAM_STR);
$validar->bindValue(2,$empresa,PDO::PARAM_INT);
$validar->bindValue(3,$dc_propuesta,PDO::PARAM_INT);

$db->stExec($validar);

?>
<?php $error_man->showConfirm('Se ha validado la propuesta de pago correctamente.') ?>
<script type="text/javascript">
	js_data.refreshListado();
</script>
