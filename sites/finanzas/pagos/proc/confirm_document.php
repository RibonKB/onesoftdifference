<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

	$dc_pago = intval($_POST['id']);
	
	if(!$dc_pago){
		$error_man->showWarning('El pago a confirmar es inválido, compruebe la entrada y vuelva a intentarlo');
		exit;
	}
	
	$data = $db->prepare($db->select('tb_pago_propuesta pp
	JOIN tb_propuesta_pago p ON p.dc_propuesta = pp.dc_propuesta
	JOIN tb_medio_pago_proveedor m ON p.dc_medio_pago = m.dc_medio_pago
	JOIN tb_proveedor pr ON pr.dc_proveedor = p.dc_proveedor',
	'pp.dc_propuesta, pp.dc_banco, pp.dg_documento, pp.dq_monto, pp.df_emision, p.dq_propuesta, p.dg_propuesta, p.dg_comentario, pr.dg_razon,
	 pr.dg_rut, m.dc_cuenta_contable_intermedia, m.dc_tipo_movimiento, pp.df_vencimiento, pr.dc_proveedor',
	'pp.dc_pago = ? AND p.dc_empresa = ? AND p.dm_estado = ?'));
	$data->bindValue(1,$dc_pago,PDO::PARAM_INT);
	$data->bindValue(2,$empresa,PDO::PARAM_INT);
	$data->bindValue(3,'C',PDO::PARAM_STR);
	$db->stExec($data);
	$data = $data->fetch(PDO::FETCH_OBJ);
	
	if($data === false){
		$error_man->showWarning('No se encontró el detalle de pago que desea confirmar o este ya fue confirmado.');
		exit;
	}
	
	//Detalles del HABER
	if($data->dc_banco != 0){
		
		$banco_data = $db->prepare($db->select('tb_banco','dc_cuenta_contable, dg_banco','dc_banco = ?'));
		$banco_data->bindValue(1,$data->dc_banco);
		$db->stExec($banco_data);
		$banco_data = $banco_data->fetch(PDO::FETCH_OBJ);
		
		if($banco_data === false){
			$error_man->showWarning('No se puedo obtener la información del banco, no se puede continuar');
			exit;
		}
		
		$detalle_haber = array(
			'dc_cuenta_contable' => $banco_data->dc_cuenta_contable,
			'dg_glosa' => "Cobro banco: {$banco_data->dg_banco}",
		);
		
	}else{
		$error_man->showAviso('El pago con cargo  cuentas que no sean de banco no está implementado aún, debe hacer los comprobantes contables manualmente.');
		exit;
	}
	
	//Iniciar transacción para las escrituras
	$db->start_transaction();
	
	//Crear comprobante contable
	
	//Obtener el número del siguiente comprobante contable
	$dg_comprobante = $db->prepare($db->select('tb_comprobante_contable',
				'dg_comprobante',
				'dc_empresa=?',
				array('order_by' => 'df_fecha_emision DESC', 'limit' => 1)));
	$dg_comprobante->bindValue(1,$empresa,PDO::PARAM_INT);
	$db->stExec($dg_comprobante);
	$dg_comprobante = $dg_comprobante->fetch(PDO::FETCH_OBJ);
	
	if($dg_comprobante !== false && substr($dg_comprobante->dg_comprobante,0,6) == date("Ym")){
		$dg_comprobante = date("Ym").((int)substr($dg_comprobante->dg_comprobante,6)+1);
	}else{
		$dg_comprobante = date("Ym").'0';
	}
	
	//Insertar cabecera del comprobante contable
	$insert_comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => '?',
		'dc_tipo_movimiento' => '?',
		'df_fecha_contable' => '?',
		'dc_mes_contable' => 'MONTH(?)',
		'dc_anho_contable' => 'YEAR(?)',
		'dc_tipo_cambio' => '?',
		'dq_cambio' => 1,
		'dg_glosa' => '?',
		'dc_propuesta_pago' => '?',
		'dq_saldo' =>  $data->dq_monto,
		'df_fecha_emision' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario
	)));
	$insert_comprobante->bindValue(1,$dg_comprobante,PDO::PARAM_INT);
	$insert_comprobante->bindValue(2,$data->dc_tipo_movimiento,PDO::PARAM_INT);
	$insert_comprobante->bindValue(3,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
	$insert_comprobante->bindValue(4,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
	$insert_comprobante->bindValue(5,$db->sqlDate2($_POST['df_fecha_contable']),PDO::PARAM_STR);
	$insert_comprobante->bindValue(6,0,PDO::PARAM_INT);
	$insert_comprobante->bindValue(7,"Propuesta: {$data->dq_propuesta} - {$data->dg_propuesta}\nProveedor:{$data->dg_razon}\n\n{$data->dg_comentario}",PDO::PARAM_STR);
	$insert_comprobante->bindValue(8,$data->dc_propuesta,PDO::PARAM_INT);
	
	$db->stExec($insert_comprobante);

	$dc_comprobante = $db->lastInsertId();
	
	//Insertar detalles de comprobante
	
	//Preparar consulta de inserción de detalles
	$insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => $dc_comprobante,
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?',
		'dg_cheque' => '?',
		'df_fecha_cheque' => '?',
		'dg_rut' => '?',
		'dc_banco' => '?',
		'dc_proveedor' => '?'
	)));
	$insert_detalle_comprobante->bindParam(1,$dc_cuenta_contable,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindParam(2,$dq_debe,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(3,$dq_haber,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(4,$dg_glosa,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(5,$dg_cheque,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(6,$df_fecha_cheque,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(7,$dg_rut,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(8,$dc_banco,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindValue(9,$data->dc_proveedor,PDO::PARAM_INT);
	
	//DEBE: acá van los montos de cobros con la cuenta intermedia asociada
	$dc_cuenta_contable = $data->dc_cuenta_contable_intermedia;
	$dq_debe = $data->dq_monto;
	$dq_haber = 0;
	$dg_glosa = "Cobro cheque: {$data->dg_documento} vencimiento: {$data->df_vencimiento}";
	$dg_cheque = $data->dg_documento;
	$df_fecha_cheque = $data->df_emision;
	$dg_rut = $data->dg_rut;
	$dc_banco = $data->dc_banco;
	
	//Insertar detalle DEBE
	$db->stExec($insert_detalle_comprobante);
	
	//HABER: Acá va el detalle del haber, la cuenta donde se cargan los montos finalmente.
	$dc_cuenta_contable = $detalle_haber['dc_cuenta_contable'];
	$dq_debe = 0;
	$dq_haber = $data->dq_monto;
	$dg_glosa = $detalle_haber['dg_glosa'];
	$dg_cheque = $data->dg_documento;
	$df_fecha_cheque = $data->df_emision;
	$dg_rut = $data->dg_rut;
	$dc_banco = $data->dc_banco;
	
	//Insertar detalle HABER
	$db->stExec($insert_detalle_comprobante);
	
	//Marcar Propuesta como terminada
	$update_propuesta = $db->prepare($db->update('tb_propuesta_pago',array('dm_estado' => '?'),'dc_propuesta = ?'));
	$update_propuesta->bindValue(1,'T',PDO::PARAM_STR);
	$update_propuesta->bindValue(2,$data->dc_propuesta,PDO::PARAM_INT);
	$db->stExec($update_propuesta);
	
	//Confirmar transacción
	$db->commit();
	
	$error_man->showConfirm("Se han confirmado el pago correctamente<br />
	Puede ver la versión de impresión del comprobante haciendo <a href='sites/contabilidad/ver_comprobante.php?id={$dc_comprobante}' target='_blank'>click acá</a>");
	
?>
<script type="text/javascript">
	pymerp.loadFile('sites/finanzas/pagos/proc/src_giros.php','src_giros_res',$('#src_giros :input').serialize())
</script>