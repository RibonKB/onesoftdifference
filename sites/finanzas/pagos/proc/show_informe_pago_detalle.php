<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$file = "cache/informe_{$empresa}.{$idUsuario}.{$_POST['rid']}.xml";

$dom = new SimpleXMLElement($file,0,1);

$filtred = array();

if($_POST['mode'] == 'total'){
	$p =& $_POST['periodo'];
	foreach($dom->detalle as $d){
		if($p == 'total'){
			$filtred[] = $d;
		}else if($p == 'more'){
			if($d->dc_dias_vencimiento > 90){
				$filtred[] = $d;
			}
		}else if($p == 'non'){
			if($d->dc_dias_vencimiento < $_POST['v']){
				$filtred[] = $d;
			}
		}else if($p < 0){
			if($d->dc_dias_vencimiento > $p && $d->dc_dias_vencimiento < 0){
				$filtred[] = $d;
			}
		}else if($p == '30'){
			if($d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento >= 0){
				$filtred[] = $d;
			}
		}else if($p == '60'){
			if($d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 30){
				$filtred[] = $d;
			}
		}else if($p == '90'){
			if($d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 60){
				$filtred[] = $d;
			}
		}
	}
}else if($_POST['mode'] == 'line'){
	$p =& $_POST['periodo'];
	$c =& $_POST['proveedor'];
	foreach($dom->detalle as $d){
		if($p == 'total'){
			if($d->dc_proveedor == $c)
				$filtred[] = $d;
		}else if($p == 'more'){
			if($d->dc_proveedor == $c && $d->dc_dias_vencimiento > 90){
				$filtred[] = $d;
			}
		}else if($p == 'non'){
			if($d->dc_proveedor == $c && $d->dc_dias_vencimiento < $_POST['v']){
				$filtred[] = $d;
			}
		}else if($p < 0){
			if($d->dc_proveedor == $c && $d->dc_dias_vencimiento >= $p && $d->dc_dias_vencimiento < 0){
				$filtred[] = $d;
			}
		}else if($p == '30'){
			if($d->dc_proveedor == $c && $d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento >= 0){
				$filtred[] = $d;
			}
		}else if($p == '60'){
			if($d->dc_proveedor == $c && $d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 30){
				$filtred[] = $d;
			}
		}else if($p == '90'){
			if($d->dc_proveedor == $c && $d->dc_dias_vencimiento <= $p && $d->dc_dias_vencimiento > 60){
				$filtred[] = $d;
			}
		}
	}
}

echo('
<div id="other_options">
	<ul>
		<li>
			<a href="#" id="propuestaPago">
				<img src="images/monedas-icon.png" alt="($)" />
				Crear Propuesta de pago
			</a>
		</li>
	</ul>
</div>
<table class="tab" width="100%" id="detailedTable"><thead><tr>
	<th>
		<input type="checkbox" id="selectAllDetailed" disabled="disabled" />
	</th>
	<th>Número de factura/folio</th>
	<th>Proveedor</th>
	<th>Monto no pagado</th>
	<th>Días</th>
	<th>Próxima Gestión</th>
	<th>Tipo Operación</th>
	<th>Nota de venta</th>
	<th>Orden de servicio</th>
</tr></thead><tbody>');

$total = 0;

foreach($filtred as $f){
	$total += $f->dq_monto_pagar;
	$f->dq_monto_pagar = moneda_local((string)$f->dq_monto_pagar);
	$f->df_proxima_gestion = $f->df_proxima_gestion=='00/00/0000 00:00'?'-':$f->df_proxima_gestion;
	echo("<tr id='factura_{$f->dc_factura}'>
		<td align='center'>
			<input type='checkbox' name='dc_factura[]' class='multipleManCheckbox cli_{$f->dc_proveedor}' value='{$f->dc_factura}' />
		</td>
		<td><a class='onoverlay' href='sites/ventas/factura_compra/proc/show_factura_compra.php?id={$f->dc_factura}'><b>{$f->dq_factura}</b> - {$f->dq_folio}</a></td>
		<td>{$f->dg_razon}</td>
		<td class='mountContainer' align='right'>{$f->dq_monto_pagar}</td>
		<td>{$f->dc_dias_vencimiento}</td>
		<td class='nextDateContainer'>{$f->df_proxima_gestion}</td>
		<td>{$f->dg_tipo_operacion}</td>
		<td>{$f->dq_nota_venta}</td>
		<td>{$f->dq_orden_servicio}</td>
	</tr>");
}
$total = moneda_local($total);
echo("</tbody>
<tfoot><tr>
	<th align='right' colspan='3'>Total</th>
	<th id='subTotalVencido' align='right'>{$total}</th>
	<th colspan='6'>&nbsp;</th>
</tr></tfoot>
</table>");

?>
<script type="text/javascript">
js_data.initDetailed();
</script>