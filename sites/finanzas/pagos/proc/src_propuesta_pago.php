<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$conditions = "dc_empresa = {$empresa}";

if($_POST['dq_propuesta_desde']){
	$dq_propuesta_desde = intval($_POST['dq_propuesta_desde']);
	if(!$dq_propuesta_desde){
		$error_man->showWarning("El formato para el número de propuesta no es el esperado, por favor indique un número.");
		exit;
	}
	
	if($_POST['dq_propuesta_hasta']){
		$dq_propuesta_hasta = intval($_POST['dq_propuesta_hasta']);
		
		if(!$dq_propuesta_hasta){
			$error_man->showWarning("El formato para el número de propuesta no es el esperado, por favor indique un número.");
			exit;
		}
		
		$conditions .= " AND (dq_propuesta BETWEEN {$dq_propuesta_desde} AND {$dq_propuesta_hasta})";
	}else{
		$conditions .= " AND dq_propuesta = {$dq_propuesta_desde}";
	}
}else{
	if($_POST['df_propuesta_desde']){
		$df_propuesta_desde = $db->sqlDate($_POST['df_propuesta_desde']);
		if($_POST['df_propuesta_hasta']){
			$df_propuesta_hasta = $db->sqlDate($_POST['df_propuesta_hasta'].' 23:59');
			$conditions .= " AND (df_propuesta BETWEEN {$df_propuesta_desde} AND {$df_propuesta_hasta})";
		}else{
			$conditions .= " AND df_propuesta = {$df_propuesta_desde}";
		}
	}else{
		$df_emision_desde = $db->sqlDate($_POST['df_emision_desde']);
		$df_emision_hasta = $db->sqlDate($_POST['df_emision_hasta'].' 23:59');
		
		$conditions .= " AND (df_emision BETWEEN {$df_emision_desde} AND {$df_emision_hasta})";
	}
}

if(isset($_POST['dc_proveedor'])){
	$dc_proveedor = implode(',',$_POST['dc_proveedor']);
	
	$conditions .= " AND dc_proveedor IN ({$dc_proveedor})";
}

if($_POST['dg_propuesta']){
	$dg_propuesta = mysql_real_escape_string($_POST['dg_propuesta']);
	$conditions .= " AND dg_propuesta LIKE '%{$dg_propuesta}%'";
}

if(isset($_POST['dm_estado'])){
	$dm_estado = implode("','",$_POST['dm_estado']);
	$conditions .= " AND dm_estado IN ('{$dm_estado}')";
}

if(!isset($_POST['dm_nula'])){
	$conditions .= ' AND dm_nula = 0';
}

$data = $db->doQuery(
	$db->select("(SELECT * FROM tb_propuesta_pago WHERE {$conditions}) pp
	LEFT JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = pp.dc_medio_pago
	LEFT JOIN tb_proveedor p ON p.dc_proveedor = pp.dc_proveedor
	JOIN tb_usuario u ON u.dc_usuario = pp.dc_usuario_creacion
	JOIN tb_funcionario f ON f.dc_funcionario = u.dc_funcionario",
	'pp.dc_propuesta, pp.dq_propuesta, pp.dg_propuesta, p.dg_razon, pp.dg_comentario, pp.df_propuesta, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, pp.dm_estado,
	m.dm_requiere_banco, m.dm_requiere_documento, m.dg_medio_pago, pp.dm_nula')
);

?>
<table class="tab" width="100%" id="propuestas_data">
<caption>Propuestas de pago</caption>
<thead>
	<tr>
		<th>(X)</th>
		<th>N° Propuesta</th>
		<th>Propuesta</th>
		<th>Medio de pago</th>
		<th>Proveedor</th>
		<th>Comentario</th>
		<th>Fecha propuesta</th>
		<th>Responsable</th>
		<th>ST</th>
		<th width="100">Workflow</th>
		<th>&nbsp;</th>
	</tr>
</thead>
<tbody>
<?php while($propuesta = $data->fetch(PDO::FETCH_OBJ)): ?>
	<tr id="pt<?php echo $propuesta->dc_propuesta ?>" <?php if($propuesta->dm_nula == 1){ ?>class="null_propuesta"<?php } ?>>
		<td>
			<?php if($propuesta->dm_nula == 0): ?>
			<a href="sites/finanzas/pagos/null_propuesta_pago.php?id=<?php echo $propuesta->dc_propuesta ?>" class="loadOnOverlay">
				<img src="images/delbtn.png" alt="(X)" title="Anular Propuesta" />
			</a>
			<?php endif; ?>
		</td>
		<td>
			<b>
				<a href="sites/finanzas/pagos/proc/show_propuesta_pago.php?id=<?php echo $propuesta->dc_propuesta ?>" class="loadOnOverlay">
					<?php echo $propuesta->dq_propuesta ?>
				</a>
			</b>
		</td>
		<td><?php echo $propuesta->dg_propuesta ?></td>
		<td><?php echo $propuesta->dg_medio_pago ?></td>
		<td><?php echo $propuesta->dg_razon ?></td>
		<td><?php echo $propuesta->dg_comentario ?></td>
		<td><?php echo $db->dateLocalFormat($propuesta->df_propuesta) ?></td>
		<td><?php echo $propuesta->dg_nombres.' '.$propuesta->dg_ap_paterno.' '.$propuesta->dg_ap_materno ?></td>
		<td class="estado_propuesta">
		<?php if($propuesta->dm_nula == 0): ?>
			<?php echo $propuesta->dm_estado ?>
		<?php else: ?>
			NULA
		<?php endif; ?>
		</td>
		<td>
		<?php if($propuesta->dm_estado != 'T' && $propuesta->dm_nula == 0): ?>
			<?php if($propuesta->dm_estado == 'L'): ?>
			<a href="sites/finanzas/pagos/proc/validar_propuesta_pago.php?id=<?php echo $propuesta->dc_propuesta ?>" class="loadOnOverlay validar_propuesta">
				<img src="images/docval.gif" alt="[V]" title="Validar propuesta" />
			</a>
			<?php else: ?>
				<?php if(!($propuesta->dm_requiere_banco == 0 && $propuesta->dm_requiere_documento == 0)): ?>
				<a href="sites/finanzas/pagos/src_admin_pago.php?id=<?php echo $propuesta->dc_propuesta ?>" class="loadOnOverlay src_admin_pago">
					<img src="images/lukas.png" alt="[CH]" title="Administrar cheques" />
				</a>
				<?php endif; ?>
			<a href="sites/finanzas/pagos/confirmar_propuesta_pago.php?id=<?php echo $propuesta->dc_propuesta ?>" class="loadOnOverlay confirmar_propuesta">
				<img src="images/confirm.png" alt="[C]" title="Confirmar Propuesta" />
			</a>
			<?php endif; ?>
		<?php endif; ?>
		</td>
		<td>
			<a href="sites/finanzas/pagos/print_propuesta_pago.php?id=<?php echo $propuesta->dc_propuesta ?>" target="_blank">
				<img src="images/export_icon/pdf.jpg" alt="[P]" title="Versión de impresión" />
			</a>
		</td>
	</tr>
<?php endwhile; ?>
</tbody>
</table>
<style type="text/css">
	.null_propuesta td{
		background-color:#FDEDE8 !important;
	}
</style>
<script type="text/javascript" src="jscripts/sites/finanzas/pagos/src_propuesta_pago.js?v=1_2b"></script>