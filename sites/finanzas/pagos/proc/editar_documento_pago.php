<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_pago = intval($_POST['dc_pago']);

if(!$dc_pago){
	$error_man->showWarning('El pago que intenta modificar es inválido, compruebe los datos de entrada');
	exit;
}

$dc_propuesta = intval($_POST['dc_propuesta']);

//Valida propuesta de pago de entrada
$propuesta = $db->prepare($db->select('tb_propuesta_pago','dq_saldo_favor','dc_propuesta = ?'));
$propuesta->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($propuesta);
$propuesta = $propuesta->fetch(PDO::FETCH_OBJ);

if($propuesta === false){
	$error_man->showWarning('La propuesta de pago ingresada es inválida, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Validar monto ingresado
$dq_monto = floatval($_POST['dq_monto']);
if(!$dq_monto){
	$error_man->showWarning('El monto a cancelar es inválido, modifiquelo y vuelva a intentarlo');
	exit;
}

//Consultar monto total a pagar propuesta
$dq_total_pagar = $db->doQuery($db->select('tb_propuesta_pago_detalle','SUM(dq_monto) dq_monto_pagar','dc_propuesta = '.$dc_propuesta))->fetch(PDO::FETCH_OBJ)->dq_monto_pagar;
if($dq_total_pagar == NULL){
	$error_man->showWarning('No se encuentran montos por pagar en la propuesta de pago, consulte con un administrador.');
	exit;
}
$dq_total_pagar = $dq_total_pagar - $propuesta->dq_saldo_favor;

//Consultar monto ya pagado de la propuesta
$dq_total_pagado = $db->doQuery(
						$db->select('tb_pago_propuesta','SUM(dq_monto) dq_pagado','dc_propuesta = '.$dc_propuesta.' AND dc_pago <> '.$dc_pago)
				   )->fetch(PDO::FETCH_OBJ)->dq_pagado;
if($dq_total_pagado == NULL){
	$dq_total_pagado = 0;
}

//Validar que monto a pagar no exceda monto máximo propuesto
if(($dq_total_pagar-$dq_total_pagado) < $dq_monto){
	$error_man->showWarning('Está intentando pagar más de lo que está comprometido en la propuesta de pago. compruebe los montos y vuelva a intentarlo.');
	exit;
}


$db->start_transaction();
$dq_total_pagado += $dq_monto;

$update_data = $db->prepare($db->update('tb_pago_propuesta',array(
	'dc_banco' => '?',
	'dg_documento' => '?',
	'df_vencimiento' => '?',
	'dq_monto' => '?',
	'df_emision' => '?'
),'dc_pago = ?'));

//Asignar el banco solo si es requerido (restrictivo a nivel de medio de pago)
if(isset($_POST['dc_banco'])){
	$dc_banco = intval($_POST['dc_banco']);
	if(!$dc_banco){
		$error_man->showWarning('El banco es obligatorio.');
		exit;
	}
	$update_data->bindValue(1,$dc_banco,PDO::PARAM_INT);
}else{
	$update_data->bindValue(1,0,PDO::PARAM_INT);
}

//Asignar numero de documento y la fecha de vencimiento del pago solo si es requerido (restrictivo a nivel de medio de pago)
if(isset($_POST['dg_documento'])){
	if(!$_POST['dg_documento'] || !$_POST['df_vencimiento']){
		$error_man->showWarning("El número de documento y la fecha de vencimiento son campos obligatorios.");
		exit;
	}
	$update_data->bindValue(2,$_POST['dg_documento'],PDO::PARAM_STR);
	$update_data->bindValue(3,$db->sqlDate2($_POST['df_vencimiento']),PDO::PARAM_STR);
}else{
	$update_data->bindValue(2,NULL,PDO::PARAM_NULL);
	$update_data->bindValue(3,NULL,PDO::PARAM_NULL);
}

$update_data->bindValue(4,$dq_monto,PDO::PARAM_STR);
$update_data->bindValue(5,$db->sqlDate2($_POST['df_emision']),PDO::PARAM_STR);
$update_data->bindValue(6,$dc_pago,PDO::PARAM_INT);

$db->stExec($update_data);

$db->commit();

$error_man->showConfirm('Se han modificado la información de pago correctamente');
?>