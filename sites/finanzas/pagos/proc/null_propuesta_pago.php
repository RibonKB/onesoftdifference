<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_propuesta = intval($_POST['dc_propuesta']);
if(!$dc_propuesta){
	$error_man->showWarning('El número de propuesta es inválido, no se puede continuar.');
	exit;
}

$propuesta = $db->prepare($db->select('tb_propuesta_pago','dq_propuesta, dm_nula, dm_estado, dq_saldo_favor, dc_proveedor','dc_propuesta = ? AND dc_empresa = ?'));
$propuesta->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$propuesta->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($propuesta);
$propuesta = $propuesta->fetch(PDO::FETCH_OBJ);


if($propuesta === false){
	$error_man->showWarning('No se encontró la propuesta de pago especificada, intentelo denuevo');
	exit;
}

if($propuesta->dm_nula == '1'){
	$error_man->showWarning('La propuesta seleccioanda ya fue anulada anteriormente, no es necesaria esta acción.');
	exit;
}

if(
	!(check_permiso(65) && $propuesta->dm_estado == 'L') &&
	!(check_permiso(66) && in_array($propuesta->dm_estado,array('V','P'))) &&
	!(check_permiso(67) && in_array($propuesta->dm_estado,array('C','T')))){
		
		$error_man->showWarning('No tiene permiso para anular esta propuesta de pago.');
		exit;
		
}

$db->start_transaction();

//Liberar los monstos pagados en las facturas de compra asociadas a la propuesta
$update_factura = $db->prepare('UPDATE tb_factura_compra fc
JOIN tb_propuesta_pago_detalle d ON d.dc_factura = fc.dc_factura
SET fc.dq_monto_pagado = fc.dq_monto_pagado - d.dq_monto
WHERE d.dc_propuesta = ?');
$update_factura->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($update_factura);

//Anular comprobantes contables asociados
//Esto solo en caso de que los debiera haber (Propuestas confirmadas y terminadas)
if(in_array($propuesta->dm_estado,array('C','T'))){
	$update_comprobante = $db->prepare($db->update('tb_comprobante_contable',array('dm_anulado' => '?'),'dc_propuesta_pago = ?'));
	$update_comprobante->bindValue(1,'1',PDO::PARAM_STR);
	$update_comprobante->bindValue(2,$dc_propuesta,PDO::PARAM_INT);
	$db->stExec($update_comprobante);
}

//Anular propuesta de pago
$update_propuesta = $db->prepare($db->update('tb_propuesta_pago',array('dm_nula' => '?'),'dc_propuesta = ?'));
$update_propuesta->bindValue(1,'1',PDO::PARAM_STR);
$update_propuesta->bindValue(2,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($update_propuesta);

if($propuesta->dq_saldo_favor > 0){
	
	$db->doExec($db->update('tb_proveedor',array('dq_saldo_favor' => 'dq_saldo_favor + '.$propuesta->dq_saldo_favor),"dc_proveedor = ".$propuesta->dc_proveedor));
	
	$update_favor_factura = $db->prepare($db->update('tb_saldo_favor_propuesta sf
	JOIN tb_factura_compra fc ON fc.dc_factura = sf.dc_factura_compra',array(
		'fc.dq_saldo_favor' => 'fc.dq_saldo_favor+sf.dq_monto'
	),'sf.dc_propuesta = ?'));
	$update_favor_factura->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
	
	$db->stExec($update_favor_factura);
	
	$update_favor_nota_credito = $db->prepare($db->update('tb_saldo_favor_propuesta sf
	JOIN tb_nota_credito_proveedor nc ON nc.dc_nota_credito = sf.dc_nota_credito',array(
		'nc.dq_saldado' => 'nc.dq_saldado-sf.dq_monto'
	),'sf.dc_propuesta = ?'));
	$update_favor_nota_credito->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
	
	$db->stExec($update_favor_nota_credito);
	
	$update_favor_orden_compra = $db->prepare($db->update('tb_saldo_favor_propuesta sf
	JOIN tb_orden_compra oc ON oc.dc_orden_compra = sf.dc_orden_compra',array(
		'oc.dq_monto_liberado' => 'oc.dq_monto_liberado+sf.dq_monto',
		'oc.dq_pago_usado' => 'oc.dq_pago_usado-sf.dq_monto'
	),'sf.dc_propuesta = ?'));
	$update_favor_orden_compra->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
	
	$db->stExec($update_favor_orden_compra);
	
	
}

$anular = $db->prepare($db->insert('tb_propuesta_pago_anulado', array(
	'dc_propuesta' => '?',
	'dc_motivo' => '?',
	'dg_comentario' => '?',
	'dc_usuario' => $idUsuario,
	'df_anulacion' => $db->getNow())
	));
	
$anular->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$anular->bindValue(2,$_POST['null_motivo'],PDO::PARAM_INT);
$anular->bindValue(3,$_POST['null_comentario'],PDO::PARAM_STR);
	$db->stExec($anular);

$db->commit();

$error_man->showAviso('Atención: se ha anulado la propuesta de pago <b>'.$propuesta->dq_propuesta.'</b>');

?>