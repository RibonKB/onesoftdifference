<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['inf_por_vencer'] *= -1;

if(isset($_POST['inf_clientes']))
	$proveedores = ' AND prov.dc_proveedor IN ('.implode(',',$_POST['inf_proveedores']).')';
else
	$proveedores = '';
	
if(isset($_POST['inf_tipo_cliente']))
	$tipo_proveedor = ' AND cl.dc_tipo_proveedor IN ('.implode(',',$_POST['inf_tipo_proveedor']).')';
else
	$tipo_proveedor = '';
	
if(isset($_POST['inf_tipo_operacion']))
	$factura = 'AND dc_tipo_operacion IN ('.implode(',',$_POST['inf_tipo_operacion']).')';
else
	$factura = '';
	
$data = $db->select("(SELECT * FROM tb_factura_compra WHERE dq_monto_pagado < dq_total {$factura} AND dm_nula = 0 AND dc_empresa = {$empresa}) f
JOIN tb_proveedor prov ON prov.dc_proveedor = f.dc_proveedor {$proveedores} {$tipo_proveedor}
JOIN tb_tipo_factura_compra op ON op.dc_tipo = f.dc_tipo
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = f.dc_orden_servicio
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = f.dc_nota_venta",
"prov.dc_proveedor, prov.dg_razon, DATEDIFF(NOW(),f.df_vencimiento) dc_dias_vencimiento, f.dq_factura, f.dq_folio, f.dc_factura, os.dq_orden_servicio, nv.dq_nota_venta, f.dq_total-f.dq_monto_pagado dq_monto_pagar,DATE_FORMAT(f.df_vencimiento,'%d/%m/%Y') df_vencimiento,op.dg_tipo,
DATE_FORMAT(f.df_proxima_gestion,'%d/%m/%Y %H:%i') df_proxima_gestion");

$dom = new DOMDocument('1.0','UTF-8');
$dom->standalone = true;

$dom_informe = $dom->createElement('informe');
$dom->appendChild($dom_informe);

$per_proveedor = array();
$totales = array(0,0,0,0,0,0,0);
foreach($data as $d){
	if(round($d['dq_monto_pagar'],$empresa_conf['dn_decimales_local']) == 0.0){
		continue;
	}
	$detalle = $dom->createElement('detalle');
	foreach($d as $i => $v){
		$detalle->appendChild($dom->createElement($i,htmlspecialchars($v,ENT_QUOTES)));
	}
	$dom_informe->appendChild($detalle);
	
	$dias = $d['dc_dias_vencimiento'];
	$indice = 0;
	if($dias <= 0){
		if($dias >= $_POST['inf_por_vencer'])
			$indice = 1;
	}else if($dias <= 30){
		$indice = 2;
	}else if($dias <= 60){
		$indice = 3;
	}else if($dias <= 90){
		$indice = 4;
	}else
		$indice = 5;
	
	@$per_proveedor[$d['dc_proveedor']][$indice] += $d['dq_monto_pagar'];
	$per_proveedor[$d['dc_proveedor']]['razon'] = $d['dg_razon'];
	@$per_proveedor[$d['dc_proveedor']]['total'] += $d['dq_monto_pagar'];
	$totales[$indice] += $d['dq_monto_pagar'];
	$totales[6] += $d['dq_monto_pagar'];
}

$cache = uniqid();

$dom->save("cache/informe_{$empresa}.{$idUsuario}.{$cache}.xml");

foreach($totales as $i => $v){
	$totales[$i] = moneda_local($v);
}

$_POST['inf_por_vencer'] *= -1;

echo("<ul id='tipo_informes' class='manual_tab'>
	<li><a href='#'>Informe Completo</a></li>
	<li><a href='#' id='to_detail'>Informe detallado</a></li>
</ul>
<br class='clear'>

<div class='tabpanes'>
<div id='complete_report'>
<table class='tab' width='100%'><thead><tr>
	<th width='30%'>Proveedor</th>
	<th width='10%'>Por vencer en {$_POST['inf_por_vencer']} o más días </th>
	<th width='10%'>Por vencer en {$_POST['inf_por_vencer']} o menos días </th>
	<th width='10%'>Vencida 30 días</th>
	<th width='10%'>Vencida 60 días</th>
	<th width='10%'>Vencida 90 días</th>
	<th width='10%'>Vencida 90+ días</th>
	<th width='10%'>Total</th>
</tr></thead><tfoot><tr>
	<th align='right'>Totales</th>
	<th align='right'>
		<a id='farVencer' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=total&periodo=non&v=-{$_POST['inf_por_vencer']}'>
			{$totales[0]}
		</a>
	</th>
	<th align='right'>
		<a id='nearVencer' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=total&periodo=-{$_POST['inf_por_vencer']}'>
			{$totales[1]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer30' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=total&periodo=30'>
			{$totales[2]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer60' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=total&periodo=60'>
			{$totales[3]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer90' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=total&periodo=90'>
			{$totales[4]}
		</a>
	</th>
	<th align='right'>
		<a id='vencer90more' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=total&periodo=more'>
			{$totales[5]}
		</a>
	</th>
	<th align='right'><b>
		<a id='totalVencido' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=total&periodo=total'>
			{$totales[6]}
		</a>
	</b></th>
</tr></tfoot><tbody>");

foreach($per_proveedor as $id => $proveedor){
	echo("<tr id='proveedor_{$id}'>
		<td><strong>{$proveedor['razon']}</strong></td>
		<td align='right'>
			<a class='farVencer' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=line&periodo=non&proveedor={$id}&v=-{$_POST['inf_por_vencer']}'>
				".(@$proveedor[0]?moneda_local($proveedor[0]):0)."</td>
			</a>
		<td align='right'>
			<a class='nearVencer' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=line&periodo=-{$_POST['inf_por_vencer']}&proveedor={$id}'>
				".(@$proveedor[1]?moneda_local($proveedor[1]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer30' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=line&periodo=30&proveedor={$id}'>
				".(@$proveedor[2]?moneda_local($proveedor[2]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer60' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=line&periodo=60&proveedor={$id}'>
				".(@$proveedor[3]?moneda_local($proveedor[3]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer90' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=line&periodo=90&proveedor={$id}'>
				".(@$proveedor[4]?moneda_local($proveedor[4]):0)."</td>
			</a>
		</td>
		<td align='right'>
			<a class='vencer90more' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=line&periodo=more&proveedor={$id}'>
				".(@$proveedor[5]?moneda_local($proveedor[5]):0)."</td>
			</a>
		</td>
		<td align='right'><b>
			<a class='totalCliente' href='sites/finanzas/pagos/proc/show_informe_pago_detalle.php?rid={$cache}&mode=line&periodo=total&proveedor={$id}'>
				".(@$proveedor['total']?moneda_local($proveedor['total']):0)."</td>
			</a>
		</b></td>
	</tr>");
}

echo('</tbody></table>
</div><div id="detailed_report">
<div class="info">
	Seleccione un elemento del informe completo para cargar el detalle acá.
</div>
</div>');

?>
<script type="text/javascript" src="jscripts/sites/finanzas/pagos/src_informe_pago.js?v=0_15"></script>
<script type="text/javascript">
	js_data.porVencer = <?php echo $_POST['inf_por_vencer'] ?>;
	js_data.separadorMiles = '<?php echo $empresa_conf['dm_separador_miles'] ?>';
	js_data.separadorDecimal = '<?php echo $empresa_conf['dm_separador_decimal'] ?>';
	$('#complete_report table').first().tableExport();
</script>
