<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$cond = "p.dc_empresa = {$empresa} AND p.dm_nula = 0";

//Filtrar por número de propuesta.
if($_POST['dq_propuesta_desde']){
	$dq_propuesta_desde = intval($_POST['dq_propuesta_desde']);
	if(!$dq_propuesta_desde){
		$error_man->showWarning('El número de propuesta indicado es inválido');
		exit;
	}
	if($_POST['dq_propuesta_hasta']){
		$dq_propuesta_hasta = intval($_POST['dq_propuesta_hasta']);
		if(!$dq_propuesta_hasta){
			$error_man->showWarning('El número de propuesta indicado es inválido');
			exit;
		}
		$cond .= " AND (p.dq_propuesta BETWEEN {$dq_propuesta_desde} AND {$dq_propuesta_hasta})";
	}else{
		$cond = "p.dq_propuesta = {$dq_propuesta_desde} AND p.dc_empresa = {$empresa}";
	}
}

//Filtrar por nombre de propuesta
if(trim($_POST['dg_propuesta'])){
	$dg_propuesta = $db->doQuery('%'.trim($_POST['dg_propuesta']).'%');
	$cond .= " AND dg_propuesta LIKE {$dg_propuesta}";
}

//Filtrar por fecha de pago (la que aparece en la administración de comprobantes)
if(trim($_POST['df_pago_desde'])){
	$df_pago_desde = $db->sqlDate(trim($_POST['df_pago_desde']));
	if(trim($_POST['df_pago_hasta'])){
		$df_pago_hasta = $db->sqlDate(trim($_POST['df_pago_hasta']));
		$cond .= " AND (pp.df_emision BETWEEN {$df_pago_desde} AND {$df_pago_hasta})";
	}else{
		$cond .= " AND pp.df_emision = {$df_pago_desde}";
	}
}

//Fecha vencimiento cheque
if(trim($_POST['df_vencimiento_desde'])){
	$df_pago_desde = $db->sqlDate(trim($_POST['df_vencimiento_desde']));
	if(trim($_POST['df_vencimiento_hasta'])){
		$df_pago_hasta = $db->sqlDate(trim($_POST['df_vencimiento_hasta']));
		$cond .= " AND (pp.df_vencimiento BETWEEN {$df_pago_desde} AND {$df_pago_hasta})";
	}else{
		$cond .= " AND pp.df_vencimiento = {$df_pago_desde}";
	}
}

//Filtrar por documentos de pago
$dg_documento = trim($_POST['dg_documento']);
if($dg_documento){
	$documentos = explode(',',$dg_documento);
	$cond .= ' AND pp.dg_documento IN(';
	foreach($documentos as $d){
		$cond .= $db->quote($d).',';
	}
	$cond = substr($cond,0,-1).')';
}

//Filtrar por proveedor
if(isset($_POST['dc_proveedor'])){
	$dc_proveedor = implode(',',$_POST['dc_proveedor']);
	$cond .= " AND p.dc_proveedor IN ({$dc_proveedor})";
}

//Filtrar por banco
if(isset($_POST['dc_banco'])){
	$dc_banco = implode(',',$_POST['dc_banco']);
	$cond .= " AND pp.dc_banco IN ({$dc_banco})";
}

//Estado
if(isset($_POST['dm_terminados'])){
	$cond .= ' AND p.dm_estado IN ("C","T")';
}else{
	$cond .= ' AND p.dm_estado = "C"';
}

//Condiciones extra
$cond .= " AND m.dm_asiento_intermedio = '1'";

$data = $db->doQuery($db->select('tb_propuesta_pago p
JOIN tb_pago_propuesta pp ON pp.dc_propuesta = p.dc_propuesta
JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago
JOIN tb_banco b ON b.dc_banco = pp.dc_banco
JOIN tb_proveedor pr ON pr.dc_proveedor = p.dc_proveedor',
'p.dq_propuesta, p.dg_propuesta, p.df_propuesta, pp.dc_pago, pp.dq_monto, pp.dg_documento, pp.df_vencimiento, pp.df_emision,
m.dg_medio_pago, b.dg_banco, pr.dg_razon, pr.dg_rut, p.dm_estado',$cond));

$anticipos = $db->doQuery($db->select('tb_comprobante_pago_anticipado c
JOIN tb_documento_pago_anticipado p ON p.dc_comprobante = c.dc_comprobante
JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = c.dc_medio_pago
JOIN tb_banco b ON b.dc_banco = p.dc_banco
JOIN tb_proveedor pr ON pr.dc_proveedor = c.dc_proveedor',
'c.dq_comprobante, c.dg_comentario, c.df_pago, p.dc_documento, p.dq_monto, p.dg_documento, p.df_vencimiento, p.df_emision,
m.dg_medio_pago, b.dg_banco, pr.dg_razon, pr.dg_rut, p.dm_estado',"c.dc_empresa = {$empresa} AND p.dm_estado = 'L' AND c.dm_nula = 0 AND p.dg_documento != ''"));

?>
<table class="tab" width="100%">
<caption>Pagos pendientes de confirmación</caption>
<thead>
	<tr>
		<th>Documento</th>
		<th>Banco</th>
		<th>Fecha Emisión</th>
		<th>Fecha vencimiento</th>
		<th>N° Propuesta</th>
		<th>Nombre Propuesta</th>
		<th>Fecha Propuesta</th>
		<th>Monto</th>
		<th>&nbsp;</th>
	</tr>
</thead>
<tbody>
	<?php while($d = $data->fetch(PDO::FETCH_OBJ)): ?>
	<tr>
		<td><?php echo $d->dg_documento ?></td>
		<td><?php echo $d->dg_banco ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_emision) ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_vencimiento) ?></td>
		<td><?php echo $d->dq_propuesta ?></td>
		<td><?php echo $d->dg_propuesta ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_propuesta) ?></td>
		<td align="right"><?php echo moneda_local($d->dq_monto) ?></td>
		<td>
		<?php if($d->dm_estado == 'C'): ?>
			<a href="sites/finanzas/pagos/confirm_document.php?id=<?php echo $d->dc_pago ?>" class="loadOnOverlay">
				<img src="images/confirm.png" alt="[C]" title="Confirmar cobro" />
			</a>
		<?php endif; ?>
		</td>
	</tr>
	<?php endwhile; ?>
</tbody>
</table>

<br />

<table class="tab" width="100%">
<caption>Pagos anticipados pendientes de confirmación</caption>
<thead>
	<tr>
		<th>Documento</th>
		<th>Banco</th>
		<th>Fecha Emisión</th>
		<th>Fecha vencimiento</th>
		<th>Comprobante Pago</th>
		<th>Comentario</th>
		<th>Fecha Comprobante</th>
		<th>Monto</th>
		<th>&nbsp;</th>
	</tr>
</thead>
<tbody>
	<?php while($d = $anticipos->fetch(PDO::FETCH_OBJ)): ?>
	<tr>
		<td><?php echo $d->dg_documento ?></td>
		<td><?php echo $d->dg_banco ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_emision) ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_vencimiento) ?></td>
		<td><?php echo $d->dq_comprobante ?></td>
		<td><?php echo $d->dg_comentario ?></td>
		<td><?php echo $db->dateLocalFormat($d->df_pago) ?></td>
		<td align="right"><?php echo moneda_local($d->dq_monto) ?></td>
		<td>
		<?php if($d->dm_estado == 'L'): ?>
			<a href="sites/finanzas/pagos/confirm_document_anticipado.php?id=<?php echo $d->dc_documento ?>" class="loadOnOverlay">
				<img src="images/confirm.png" alt="[C]" title="Confirmar cobro" />
			</a>
		<?php endif; ?>
		</td>
	</tr>
	<?php endwhile; ?>
</tbody>
</table>
<script type="text/javascript">
	pymerp.init($('#src_giros_res'));
</script>