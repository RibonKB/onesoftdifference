<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");

$form = new Form($empresa);

$dc_propuesta = intval($_POST['id']);

$datos = $db->prepare($db->select('tb_propuesta_pago','dg_propuesta','dc_propuesta = ? AND dc_empresa = ?'));
$datos->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('La propuesta seleccionada no fue encontrada');
	exit;
}

?>

<div class="secc_bar">
	Anular propuesta de pago
</div>

<div class="panes">
<?php
	$form->Start('sites/finanzas/pagos/proc/null_propuesta_pago.php','null_propuesta_pago'); 
	$error_man->showAviso("¿Esta seguro que desea anular la propuesta de pago? <strong>{$datos->dg_propuesta}</strong>"); 
	
	$form->Section();
	$form->DBSelect('Motivo de anulación','null_motivo','tb_motivo_anulacion ma JOIN tb_motivo_anulacion_modulo mm ON ma.dc_motivo = 			 		mm.dc_motivo',array('mm.dc_motivo','ma.dg_motivo'),1,0,"dc_empresa = {$empresa} AND dc_modulo = 3 AND dm_activo = 1 ");
	$form->EndSection();
	
	$form->Section();
	$form->Textarea('Comentario','null_comentario',1);
	$form->EndSection();
	
	
	$form->Hidden('dc_propuesta',$_POST['id']);
	$form->End('Anular','delbtn');
?>  
</div>