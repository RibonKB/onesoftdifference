<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$list = $db->select('tb_medio_pago_proveedor m
					LEFT JOIN tb_tipo_movimiento t ON t.dc_tipo_movimiento = m.dc_tipo_movimiento
					LEFT JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = m.dc_cuenta_contable_intermedia
					LEFT JOIN tb_banco b ON b.dc_banco = m.dc_banco_default',
					'm.dc_medio_pago,m.dg_medio_pago,m.df_creacion,m.dm_requiere_banco, m.dm_asiento_intermedio, m.dm_requiere_documento,
					 t.dg_tipo_movimiento, cc.dg_cuenta_contable, b.dg_banco',
					"m.dc_empresa = {$empresa} AND m.dm_activo = 1");

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">Medios de pago</div>
<div id="main_cont"><div class="panes">

	<?php
		$form->Start("sites/finanzas/pagos/proc/crear_medio_pago.php",'cr_medio_pago');
		$form->Header("<b>Complete los datos requeridos para crear un nuevo medio de pago</b>
		<br />Los campos marcados con [*] son obligatorios
		<br />Puede consultar los que ya están agregados.");
		$form->Section();
			$form->Text('Nombre','dg_medio_pago',true);
			$form->Listado('Tipo de movimiento contable','dc_tipo_movimiento','tb_tipo_movimiento',array('dc_tipo_movimiento','dg_tipo_movimiento'),true);
		$form->EndSection();
		$form->Section();
			$form->Radiobox('Requiere Banco','dm_requiere_banco',array('SI','NO'));
			$form->Radiobox('Asiento intermedio','dm_asiento_intermedio',array('SI','NO'));
			$form->Radiobox('Requiere Documento','dm_requiere_documento',array('SI','NO'));
		$form->EndSection();
		$form->Section();
			$form->Listado('Banco por defecto','dc_banco','tb_banco',array('dc_banco','dg_banco'));
			$form->Listado('Cuenta contable asiento intermedio','dc_cuenta_intermedia','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'));
		$form->EndSection();
		$form->End('Crear','addbtn');
	?>
<table class="tab" width="100%" id="tmedio">
<caption>Medios de pago</caption>
<thead>
	<tr>
		<th>Medio de pago</th>
		<th>Fecha creación</th>
		<th>Tipo de movimiento</th>
		<th>Cuenta intermedia</th>
		<th>Banco por defecto</th>
		<th>Banco</th>
		<th>Asiento intermedio</th>
		<th>Documento</th>
		<th>Opciones</th>
	</tr>
</thead>
<tbody id="list">
<?php foreach($list as $l): ?>
	<tr>
		<td><?php echo $l['dg_medio_pago'] ?></td>
		<td><?php echo $db->dateTimeLocalFormat($l['df_creacion']) ?></td>
		<td><?php echo $l['dg_tipo_movimiento'] ?></td>
		<td><?php echo $l['dg_cuenta_contable'] ?></td>
		<td><?php echo $l['dg_banco'] ?></td>
		<td><?php echo $l['dm_requiere_banco']==1?'SI':'NO' ?></td>
		<td><?php echo $l['dm_asiento_intermedio']==1?'SI':'NO' ?></td>
		<td><?php echo $l['dm_requiere_documento']==1?'SI':'NO' ?></td>
		<td>
			<a href='sites/finanzas/pagos/ed_medio_pago.php?id=<?php echo $l['dc_medio_pago'] ?>' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
			</a>
			<a href='sites/finanzas/pagos/del_medio_pago.php?id=<?php echo $l['dc_medio_pago'] ?>' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
			</a>
		</td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
</div></div>
<script type="text/javascript">
	$('#tmedio').tableExport().tableAdjust(10);
</script>