<?php

require("../../../inc/fpdf.php");

class PDF extends FPDF {

    private $headerData;

    public function PDF($data) {
        $this->headerData = $data;

        parent::__construct('P', 'mm', 'Letter');
    }

    public function Header() {

        $this->SetFont('Arial', 'B', 13);
        $this->MultiCell(150, 9, strtoupper($this->headerData->dg_propuesta));
        $this->SetY(10);

        $this->SetFont('Arial', '', 12);
        $this->SetDrawColor(0, 100, 0);
        $this->SetTextColor(0, 100, 0);
        $this->SetX(160);
        $this->MultiCell(50, 9, "PROPUESTA DE PAGO\nNo {$this->headerData->dq_propuesta}", 1, 'C');
        $this->SetTextColor(0);

        $this->Ln();

        $this->SetDrawColor(150, 150, 150);
        $this->Cell(0, 5, '', 'B');
        $this->Ln();
        $y = $this->GetY();
        $this->SetFontSize(5);
        $this->MultiCell(20, 7, "FECHA:\nPROVEEDOR:\nMEDIO DE PAGO:\nCOMENTARIO: ", 'L');
        $this->Cell(0, 5, '', 'T');
        $this->SetY($y);
        $this->SetX(30);
        $this->SetFontSize(11);
        $this->MultiCell(176, 7, "{$this->headerData->df_propuesta}\n{$this->headerData->dg_razon}\n{$this->headerData->dg_medio_pago}\n{$this->headerData->dg_comentario}", "R");
        $this->Ln();
    }

    public function setDetail(PDOStatement $detail) {

        $this->AddPage();
        $this->SetFontSize(11);

        $w = array(33, 33, 33, 33, 32, 32);

        $this->SetDrawColor(100, 100, 100);
        $this->SetFillColor(200, 200, 200);

        $this->Cell(array_sum($w), 6, 'Detalle de Propuesta', 1, 1, 'C', 1);

        $this->SetDrawColor(150, 150, 150);
        $this->SetFillColor(240, 240, 240);


        $head = array('Factura de compra', 'Folio', 'Fecha Factura', 'NV', 'OS', 'Monto a pagar');
        $this->SetFontSize(7);
        for ($i = 0; $i < 6; $i++) {
            $this->Cell($w[$i], 6, $head[$i], 1, 0, 'L', 1);
        }
        $this->Ln();

        $this->SetDrawColor(200, 200, 200);
        $this->SetFillColor(250, 250, 250);
        $total = 0;
        while ($d = $detail->fetch(PDO::FETCH_OBJ)) {
            $this->Cell($w[0], 6, $d->dq_factura, 1, 0, 'L', 1);
            $this->Cell($w[1], 6, $d->dq_folio, 1, 0, 'L', 1);
            $this->Cell($w[2], 6, $d->df_emision, 1, 0, 'L', 1);
            $this->Cell($w[3], 6, $d->dq_nota_venta, 1, 0, 'L', 1);
            $this->Cell($w[4], 6, $d->dq_orden_servicio, 1, 0, 'L', 1);
            $this->Cell($w[5], 6, moneda_local($d->dq_monto), 1, 0, 'R', 1);
            $total = $total + $d->dq_monto;
            $this->Ln();
        }
         $this->SetFont('', 'B');
        $this->Cell($w[0], 6, '', 3, 0, 'L', 1);
        $this->Cell($w[1], 6, '', 3, 0, 'L', 1);
        $this->Cell($w[2], 6, '', 3, 0, 'L', 1);
        $this->Cell($w[3], 6, '', 3, 0, 'L', 1);
        $this->Cell($w[4], 6, 'Total', 3, 0, 'C', 1);

        $this->Cell($w[5], 6, moneda_local($total), 1, 0, 'R', 1);
        $this->ln();
    }

    public function setPayDetail($detail) {
        if (!count($detail)) {
            return;
        }


        //$this->AddPage();
        $this->Ln(15);

        $w = array(33, 33, 33, 33, 32, 32);
        $this->SetFontSize(11);

        $this->SetDrawColor(100, 100, 100);
        $this->SetFillColor(200, 200, 200);

        $this->Cell(array_sum($w), 6, 'Detalle de Pagos', 1, 1, 'C', 1);

        $this->SetDrawColor(150, 150, 150);
        $this->SetFillColor(240, 240, 240);


        $head = array('Documento', 'Fecha Vencimiento', 'Banco', 'Fecha Emision', 'Responsable', 'Monto');
        $this->SetFontSize(7);
        for ($i = 0; $i < 6; $i++) {
            $this->Cell($w[$i], 6, $head[$i], 1, 0, 'L', 1);
        }
        $this->Ln();

        $this->SetDrawColor(200, 200, 200);
        $this->SetFillColor(250, 250, 250);

        foreach ($detail as $d) {
            $this->Cell($w[0], 6, $d->dg_documento, 1, 0, 'L', 1);
            $this->Cell($w[1], 6, $d->df_vencimiento, 1, 0, 'L', 1);
            $this->Cell($w[2], 6, $d->dg_banco, 1, 0, 'L', 1);
            $this->Cell($w[3], 6, $d->df_emision, 1, 0, 'L', 1);
            $this->Cell($w[4], 6, $d->dg_usuario, 1, 0, 'L', 1);
            $this->Cell($w[5], 6, moneda_local($d->dq_monto), 1, 0, 'R', 1);
            $this->Ln();
        }
    }

}

?>