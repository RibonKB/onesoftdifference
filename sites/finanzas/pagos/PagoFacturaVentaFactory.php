<?php 
	
	class PagoFacturaVentaFactory extends Factory{
		
        protected $title = 'Pago de factura de compra con factura de venta';
		private $comprobanteContable;
		private $montoASaldar;
		private $montoNoPagadoFacturaVenta;
		private $montoNoPagadoFacturaCompra;
		private $idDetalleFinancieroProveedor;
		private $idDetalleFinancieroCliente;
		const GLOSA_CABECERA = 'Pago de factura de compra con factura de venta';
		// private $cuentaContableProveedor;
		// private $cuentaContableCliente
		
		public function indexAction(){
			if(!check_permiso(87)){
                $this->getErrorMan()->showWarning("El usuario actual no posee permisos para acceder a este módulo. Contacte con el encargado.");
				exit;
            }
			$form = $this->getFormView($this->getTemplateUrl('index.form'));
			echo $this->getFullView($form,array(),Factory::STRING_TEMPLATE);
		}
		
		public function procesarAsignacionAction(){
			$db = $this->getConnection();
			$r = self::getRequest();
			$this->comprobanteContable  = $this->getService('ComprobanteContable');
			
			//obtener datos de la factura de venta y validarla, obteniendo el monto no pagado
			$facturaVenta = $db->getRowById('tb_factura_venta',$r->dc_factura_venta,'dc_factura');
			$this->montoNoPagadoFacturaVenta = $this->validarFactura($facturaVenta, 2);
			
			//obtener datos de la factura de compra y luego validarla
			$facturaCompra = $db->getRowById('tb_factura_compra', $r->dc_factura_compra,'dc_factura');
			$this->montoNoPagadoFacturaCompra = $this->validarFactura($facturaCompra, 1);
			
			
			//Validar Cuentas contables
			$cuentaContableCliente = $this->validarCuentacontableCliente($facturaVenta);
			$cuentaContableProveedor = $this->validarCuentacontableProveedor($facturaCompra);
			$configuracionContable = $this->comprobanteContable->getConfiguracionContabilidad();
			if($configuracionContable->dc_tipo_movimiento_pago_con_factura_venta == null){
				$this->getErrorMan()->showWarning('No se ha configurado un tipo de movimiento para el pago de facturas de compra con facturas de venta');
				exit;
			}
			
			//Calcular monto a saldar. Se asignará a la variable de clase automaticamente.
			$this->calcularMonto();
			
			$db->start_transaction();
				
				//Agregar al monto pagado de la factura de compra el saldo
				$this->pagarFactura($facturaCompra, $this->montoASaldar, 1);
				//Lo mismo para la factura de venta
				$this->pagarFactura($facturaVenta, $this->montoASaldar, 2);
				//Generar comprobante pago factura venta en la base de datos
				$this->comprobantePagoFacturaVenta($facturaCompra, $facturaVenta, $this->montoASaldar);
			
				//iniciar Comprobantes Contables
				//Cabecera de comprobante contable
				$this->setCabeceraComprobanteContable($configuracionContable->dc_tipo_movimiento_pago_con_factura_venta);
				
				//Detalle financiero 
				$this->comprobanteContable->prepareDetalleFinanciero();
				
				//Se genera la glosa que se utilizará en la inserción del detalle financiero
				$glosaParametrosFinancieros =  'Pago de factura de compra con factura de venta';
				
				//insercion del detalle financiero para la factura de compra (cuenta proveedor)
				$parametrosFinancierosProveedor = array(
					'dc_cuenta_contable' => $cuentaContableProveedor,
					'dg_glosa' => $glosaParametrosFinancieros,
					'dq_debe' => $this->montoASaldar,
					'dq_haber' => 0
				);
				$this->idDetalleFinancieroProveedor = $this->insertarParametrosFinancieros($parametrosFinancierosProveedor);
				
				//insercion del detalle financiero para la factura de venta (cuenta cliente)
				$parametrosFinancierosCliente = array(
					'dc_cuenta_contable' => $cuentaContableCliente,
					'dg_glosa' => $glosaParametrosFinancieros,
					'dq_debe' => 0,
					'dq_haber' => $this->montoASaldar
				);
				$this->idDetalleFinancieroCliente = $this->insertarParametrosFinancieros($parametrosFinancierosCliente);
				
				//Detalle Analítico
				$this->comprobanteContable->prepareDetalleAnalitico();
				
				//insertar detalle analítico para la factura de compra (cuenta proveedor)
				$valoresAnaliticosProveedor = array(
					'dc_cuenta_contable' => $cuentaContableProveedor,
					'dq_debe' => $this->montoASaldar,
					'dq_haber' => 0,
					'dg_glosa' => 'Factura de compra a pagar con factura de venta',
					'dc_detalle_financiero' => $this->idDetalleFinancieroProveedor,
					'dc_factura_compra' => $facturaCompra->dc_factura,
					'dc_factura_venta' => $facturaVenta->dc_factura
				);
				$this->insertarParametrosAnaliticos($valoresAnaliticosProveedor);
				
				//Setear dependencias del detalle analítico del proveedor
				$dependenciasProveedor = array(
					'dc_factura_compra',
					'dc_factura_venta',
					'dc_proveedor',
					'dc_nota_venta',
					'dc_cliente',
				);
				$this->insertarDependencias($dependenciasProveedor);
				
				//insertar el detalle analítico en base de datos
				$this->comprobanteContable->DetalleAnaliticoPersist();
				
				//limpiar las dependencias
				$this->comprobanteContable->limpiarDependenciasHasta('dc_factura_compra');
				
				//insertar detalle analítico para la factura de Venta (cuenta Cliente)
				$valoresAnaliticosProveedor = array(
					'dc_cuenta_contable' => $cuentaContableCliente,
					'dq_debe' => 0,
					'dq_haber' => $this->montoASaldar,
					'dg_glosa' => 'Factura de venta que cancela la factura de compra',
					'dc_detalle_financiero' => $this->idDetalleFinancieroCliente,
					'dc_factura_venta' => $facturaVenta->dc_factura
				);
				$this->insertarParametrosAnaliticos($valoresAnaliticosProveedor);
				
				//Asignar dependencias al detalle analítico del cliente
				$dependenciasCliente = array(
					'dc_factura_venta',
					'dc_nota_venta',
					'dc_cliente'
				);
				$this->insertarDependencias($dependenciasCliente);
				
				//insertar el detalle analítico en base de datos
				$this->comprobanteContable->DetalleAnaliticoPersist();
				
				//limpiar las dependencias
				$this->comprobanteContable->limpiarDependenciasHasta('dc_factura_venta');
			
			$db->commit();
			$this->getErrorMan()->showConfirm("Se ha utilizado <strong>$ {$this->montoASaldar}</strong> desde la factura de venta: <strong>{$facturaVenta->dq_factura}</strong> (Folio: <strong>{$facturaVenta->dq_folio}</strong>); para saldar el pago de la factura de compra: <strong>{$facturaCompra->dq_factura}</strong> (Folio: <strong>{$facturaCompra->dq_folio}</strong>)");
			echo $this->comprobanteContable->getStandarCreationOutput();
		}
		/**
		 *	function validarFactura
		 *	 - Comprueba que la factura que se va a utilizar no esté nula o que su saldo por pagar no esté completamente pagado
		 *	 - Retorna el saldo disponible no pagado de la factura.
		 *	@param $factura : Objeto de retorno PDO que puede contener el detalle de una Factura de Venta o una Factura de compra.
		 *	@param tipo : INT que determina el tipo de factura. Se utiliza para la salida de mensajes de error.
		 */
		private function validarFactura($factura, $tipo){
			$tipoFactura = array(
				1 => 'factura de compra',
				2 => 'factura de venta'
			);
			$this->validarSetFactura($factura, $tipoFactura[$tipo]);
			//Validar que la factura no esté nula
			if($factura->dm_nula == 1){
				$this->getErrorMan()->showWarning("La {$tipoFactura[$tipo]} seleccionada está nula, seleccione otra.");
				exit;
			}
			
			//Validar que la factura no esté completamente pagada
			$decimales_empresa = $this->getParametrosEmpresa()->dn_decimales_local;
			if(round($factura->dq_total,$decimales_empresa) == round($factura->dq_monto_pagado,$decimales_empresa)){
				$this->getErrorMan()->showWarning("La {$tipoFactura[$tipo]} seleccionada no posee saldos a pagar pendientes, compruebe los datos de entrada y vuelva a intentarlo.");
				exit;
			}
			
			$saldo = round($factura->dq_total,$decimales_empresa) - round($factura->dq_monto_pagado,$decimales_empresa);
			return $saldo;
		}
		
		private function validarSetFactura($factura, $tipo){
			if($factura === false){
				$this->getErrorMan()->showWarning("Se ha ingresado una {$tipo} no valida. Compruebe los valores de entrada e intente nuevamente");
				exit;
			}
		}
		
		//Se valida que el cliente posea una cuenta contable para realizar la operación
		private function validarCuentacontableCliente($factura){
			$db = $this->getConnection();
			$comprobarCuenta = $db->getRowById('tb_cliente', $factura->dc_cliente, 'dc_cliente');
			if($comprobarCuenta === false or $comprobarCuenta->dc_cuenta_contable == 0){
				$this->getErrorMan()->showWarning('El cliente de la factura de venta no está asociado a ninguna cuenta contable.');
				exit;
			}
			return $comprobarCuenta->dc_cuenta_contable;
		}
		
		//Se valida que el proveedor posea una cuenta contable para realizar la operación
		private function validarCuentacontableProveedor($factura){
			$db = $this->getConnection();
			$comprobarCuenta = $db->getRowById('tb_proveedor', $factura->dc_proveedor, 'dc_proveedor');
			if($comprobarCuenta === false or $comprobarCuenta->dc_cuenta_contable == 0){
				$this->getErrorMan()->showWarning('El proveedor de la factura de compra no está asociado a ninguna cuenta contable.');
				exit;
			}
			return $comprobarCuenta->dc_cuenta_contable;
		}
		
		/**
		 *	function calcularMonto
		 *	Esta función se encarga de decidir como será calculado el monto a saldar de la siguiente manera:
		 *	- Determina si se ha enviado algún valor desde la página de preparación
		 *	- Si es enviado, llama a la función que asigna el saldo según el monto solicitado
		 *	- Si no es enviado el campo, entonces procederá a llamar a la función que asigna los montos de manera autommática.
		 *	
		 */
		private function calcularMonto(){
			$r = $this->getRequest();
			if(empty($r->dq_monto_saldar)){
				$this->calculoAuto();
			}
			else{
				if(!is_numeric($r->dq_monto_saldar)){
					$this->getErrorMan()->showWarning('El monto sólo permite números. Verifique los datos ingresados e intente nuevamente.');
					exit;
				}
				$this->calculoMontoAsignado($r->dq_monto_saldar);
			}
			
		}
		
		/**
		 *	function calculoAuto
		 *	Esta función se encarga de calcular automáticamente el monto que se debe utilizar para pagar la factura de venta.
		 *	Se utilizará cuando el monto no sea definido manualmente
		 *	- Si el monto no pagado de la factura de compra es menor que el mayor no pagado en la factura de venta, se utilizará
		 *		el monto no pagado que posee la factura de compra.
		 *	- Si el monto no pagado de la factura de compra es igual al monto no pagado de la factura de venta, se utilizará
		 *		el monto no pagado que posee la factura de compra.
		 *	- Si el monto no pagado de la factura de compra es menor que el monto no pagado de la factura de venta, se utilizará
		 *		el monto no pagado que posee la factura de venta.
		 */
		private function calculoAuto(){
			
			if($this->montoNoPagadoFacturaCompra < $this->montoNoPagadoFacturaVenta || $this->montoNoPagadoFacturaCompra == $this->montoNoPagadoFacturaVenta){
				$this->montoASaldar = $this->montoNoPagadoFacturaCompra;
			}elseif($this->montoNoPagadoFacturaCompra > $this->montoNoPagadoFacturaVenta){
				$this->montoASaldar = $this->montoNoPagadoFacturaVenta;
			}else{
				$this->getErrorMan()->showWarning('Ha ocurrido un error en el cálculo automático. Por favor contacte al administrador.');
				exit;
			}
			
		}
		
		/**
		 *	function calculoMontoAsignado
		 *	Ésta función se utilizará el monto definido manualmente para asignar el saldo en la factura con las siguientes condiciones:
		 *	- Si el monto definido manualmente es negativo, arrojará un error indicando que el valor no puede ser negativo.
		 *	- Si el monto definido manualmente es igual al monto no pagado de la factura de compra lo asignará a la variable de clase $montoASaldar
		 *	- Si el monto definido manualmente es menor o mayor que el monto no pagado de la factura, acusará un error indicando el caso.
		 */
		private function calculoMontoAsignado($montoAsignado){
			$montoAsignado = round($montoAsignado);
			if($montoAsignado <= 0){
				$this->getErrorMan()->showWarning('El monto asignado es negativo. Intente ingresando un valor positivo');
				exit;
			}
			//validar que el monto asignado sea menor que el total sin pagar de la factura de venta
			$this->validarMontoAsignado($montoAsignado);
			
			if($montoAsignado <= $this->montoNoPagadoFacturaCompra){
				$this->montoASaldar = $montoAsignado;
			}elseif($montoAsignado > $this->montoNoPagadoFacturaCompra){
				$this->getErrorMan()->showWarning('El monto asignado es mayor que el de la factura de compra a pagar. Intente con otro valor o deje el campo en blanco para una asignación automática');
				exit;
			}else{
				$this->getErrorMan()->showWarning('Ha ocurrido un error con el monto ingresado o este no es válido. Intente nuevamente o contacte al administrador.');
				exit;
			}
		}
		
		public function validarMontoAsignado($monto){
			//es el monto negativo?
			if($monto < 0){
				$this->getErrorMan()->showWarning('El monto asignado es negativo. Intente ingresando un valor positivo');
				exit;
			}
			//verificar que el monto sea menor o igual que el de la factura de compra
			if($monto > $this->montoNoPagadoFacturaVenta){
				$this->getErrorMan()->showWarning('El monto ingresado supera al monto no pagado de la factura de venta');
				exit;
			}
			
		}
		
		/**
		 *	function pagarFactura
		 *	Ésta funcion se encarga de actualizar la factura, asignando el monto a dq_monto_pagado de esta en la base de datos.
		 *	Recibe como parametro el tipo de factura que se va a operar para seleccionar la tabla correcta
		 *	@param $factura
		 *	@param $monto
		 */
		private function pagarFactura($factura, $monto, $tipo){
			$tipoFactura = array(
			1 => 'tb_factura_compra',
			2 => 'tb_factura_venta'
			);
			$db = $this->getConnection();
			$pagar = $db->prepare($db->update($tipoFactura[$tipo], array('dq_monto_pagado'=>'dq_monto_pagado + ?'), 'dc_factura = ?'));
			$pagar->bindValue(1, $monto, PDO::PARAM_INT);
			$pagar->bindValue(2, $factura->dc_factura, PDO::PARAM_INT);
			$db->stExec($pagar);
			
		}
		
		private function comprobantePagoFacturaVenta($facturaCompra, $facturaVenta, $montoASaldar){
			$db = $this->getConnection();
			
			$valores = array(
				'dc_factura_venta' => $facturaVenta->dc_factura,
				'dc_factura_compra' => $facturaCompra->dc_factura,
				'dq_monto' => '?',
				'df_fecha_creacion' => '?',
				'dc_empresa' => '?',
				'dc_usuario_creacion' => '?',
			);
			
			$comprobante = $db->prepare($db->insert('tb_comprobante_pago_factura_venta', $valores));
			$comprobante->bindValue(1, $montoASaldar, PDO::PARAM_INT);
			$comprobante->bindValue(2, $db->getnow(), PDO::PARAM_INT);
			$comprobante->bindValue(3, $this->getEmpresa(), PDO::PARAM_INT);
			$comprobante->bindValue(4, $this->getUserData()->dc_usuario, PDO::PARAM_INT);
			
			$db->stExec($comprobante);
		}
		
		private function setCabeceraComprobanteContable($tipoMovimiento){
			$r = self::getRequest();
			$db = $this->getConnection();
			//Transformar Fecha en formato sql
			$fechaContable = $db->sqlDate2($r->df_fecha_contable);
			//crear cabecera de comprobante
            try{
                $this->comprobanteContable->prepareComprobante($tipoMovimiento, $fechaContable, 1);
            } catch(Exception $e){
                $this->getErrorMan()->showWarning("Error al validar la fecha contable. Verifique su información e intente nuevamente, o contacte a un administrador.");
				//echo $e->getMessage();
                exit;
            }
			
					$this->comprobanteContable->setComprobanteParam('dg_glosa', self::GLOSA_CABECERA);
					$this->comprobanteContable->setComprobanteParam('dq_saldo', $this->montoASaldar);
			$this->comprobanteContable->ComprobantePersist();
		}
		
		private function insertarParametrosFinancieros($parametros){
			foreach($parametros as $c => $v){
				$this->comprobanteContable->setParametroFinanciero($c, $v);
			}
			$id = $this->comprobanteContable->DetalleFinancieroPersist();
			return $id;
		}
		
		private function insertarParametrosAnaliticos($parametros){
			foreach($parametros as $c => $v){
				$this->comprobanteContable->setParametroAnalitico($c, $v);
			}
		}
		
		private function insertarDependencias($parametros){
			foreach($parametros as $v){
				$this->comprobanteContable->setDependencias($v);
			}
		}
		
		
		public function facturaVentaAutoCompletarAction(){
			require_once(__DIR__ .'/../../../inc/Autocompleter.class.php');
			
			$request = Factory::getRequest();
			$db = $this->getConnection();
			
			$q = $request->q;
			$limit = $request->limit;
			
			$data = $db->prepare($db->select('tb_factura_venta fv
				JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente',
			'fv.dc_factura, fv.dq_factura, fv.dq_folio, fv.dq_neto, cl.dg_razon',
			'fv.dc_empresa = ? AND fv.dm_nula = 0 AND (fv.dq_factura LIKE ? OR fv.dq_folio LIKE ? OR cl.dg_razon LIKE ?)',
			array('limit' => $limit)));
			$data->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
			$data->bindValue(2,'%'.$q.'%',PDO::PARAM_STR);
			$data->bindValue(3,'%'.$q.'%',PDO::PARAM_STR);
			$data->bindValue(4,'%'.$q.'%',PDO::PARAM_STR);
			$db->stExec($data);
			
			while($fv = $data->fetch(PDO::FETCH_OBJ)){
				echo Autocompleter::getItemRow(array(
					$fv->dq_factura,
					$fv->dq_folio,
					$fv->dg_razon,
					$fv->dc_factura
				));
			}
		}
		
		
		public function facturaCompraAutoCompletarAction(){
			require_once(__DIR__ .'/../../../inc/Autocompleter.class.php');
			
			$request = Factory::getRequest();
			$db = $this->getConnection();
			
			$q = $request->q;
			$limit = $request->limit;
			
			$data = $db->prepare($db->select('tb_factura_compra fc
				JOIN tb_proveedor pr ON pr.dc_proveedor = fc.dc_proveedor',
			'fc.dc_factura, fc.dq_factura, fc.dq_folio, fc.dq_neto, pr.dg_razon',
			'fc.dc_empresa = ? AND fc.dm_nula = 0 AND (fc.dq_factura LIKE ? OR fc.dq_folio LIKE ? OR pr.dg_razon LIKE ?)',
			array('limit' => $limit)));
			$data->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
			$data->bindValue(2,'%'.$q.'%',PDO::PARAM_STR);
			$data->bindValue(3,'%'.$q.'%',PDO::PARAM_STR);
			$data->bindValue(4,'%'.$q.'%',PDO::PARAM_STR);
			$db->stExec($data);
			
			while($fc = $data->fetch(PDO::FETCH_OBJ)){
				echo Autocompleter::getItemRow(array(
					$fc->dq_factura,
					$fc->dq_folio,
					$fc->dg_razon,
					$fc->dc_factura
				));
			}
		}
		
		/**
		*	Obtiene los detalles de la factura de venta para mostrarlo como informatico para el usuario
		*	TEMPLATE: ventas/nota_credito/asignar_saldo/show_factura_venta_info
		*/
		public function obtenerDetallesFacturaVentaAction(){
			$db = $this->getConnection();
			$this->initFunctionsService();
			
			$factura = $db->getRowById('tb_factura_venta',self::getRequest()->dc_factura_venta,'dc_factura');
			$cliente = $db->getRowById('tb_cliente',$factura->dc_cliente,'dc_cliente','dg_razon');
			
			echo $this->getView($this->getTemplateUrl('show_factura_venta_info'),array(
				'factura' => $factura,
				'cliente' => $cliente
			));
		}
		
		/**
		*	Obtiene los detalles de la factura de compra para mostrarlo como informativo para el usuario
		*	TEMPLATE: ventas/nota_credito/asignar_saldo/show_factura_venta_info
		*/
		public function obtenerDetallesFacturaCompraAction(){
			$db = $this->getConnection();
			$this->initFunctionsService();
			$r = self::getRequest();
			
			$factura = $db->getRowById('tb_factura_compra',$r->dc_factura_compra,'dc_factura');
			$proveedor = $db->getRowById('tb_proveedor',$factura->dc_proveedor,'dc_proveedor','dg_razon');
			
			echo $this->getView($this->getTemplateUrl('showFacturaCompraInfo'),array(
				'factura' => $factura,
				'proveedor' => $proveedor
			));
		}
		
	}

?>