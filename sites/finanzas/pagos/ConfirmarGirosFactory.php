<?php

require_once('sites/finanzas/AbstractConfirmarGirosFactory.php');

/**
 * Description of ConfirmarGirosFactory
 *
 * @author TomásBorlack
 */
class ConfirmarGirosFactory extends AbstractConfirmarGirosFactory {

  protected $pago_data;
  private $detalleHaber;
  protected $matrizFinanciera;
  private $proveedor = null;

  public function ConfirmarChequePropuestaAction() {
    $request = self::getRequest();

    $this->pago_data = $this->getPagoData($request->id);

    $this->detalleHaber = $this->getDetalleGiro();

    $this->getConnection()->start_transaction();

    $this->insertarComprobanteContable(array(
        'document_field' => 'dc_propuesta_pago',
        'document_id' => $this->pago_data->dc_propuesta,
        'glosa' => "Propuesta: {$this->pago_data->dq_propuesta} - {$this->pago_data->dg_propuesta}\nProveedor:{$this->getProveedor()->dg_razon}\n\n{$this->pago_data->dg_comentario}"
    ));
    $this->insertarDetallesFinancieros();
    $this->insertarDetallesAnaliticos();
    $this->confirmarGiro();
    $this->cerrarPropuesta();

    $this->getConnection()->commit();

    $this->showEstandarOutput();
  }

  public function ConfirmarChequeAnticipadoAction() {
    $request = self::getRequest();

    $this->pago_data = $this->getPagoAnticipadoData($request->id);

    $this->detalleHaber = $this->getDetalleGiro();

    $this->getConnection()->start_transaction();

    $this->dc_comprobante = $this->insertarComprobanteContable(array(
        'document_field' => 'dc_comprobante_pago',
        'document_id' => $this->pago_data->dc_comprobante,
        'glosa' => "Comprobante Pago: {$this->pago_data->dq_comprobante}\nProveedor:{$this->getProveedor()->dg_razon}\n\n{$this->pago_data->dg_comentario}"
    ));
    $this->insertarDetallesFinancieros();
    $this->insertarDetallesAnaliticos();
    $this->confirmarGiroAnticipado();

    $this->getConnection()->commit();

    $this->showEstandarOutput();
  }

  private function getPagoData($dc_pago) {

    $db = $this->getConnection();
    $dc_pago = intval($dc_pago);

    if (!$dc_pago) {
      $this->getErrorMan()->showWarning('El pago a confirmar es inválido, compruebe la entrada y vuelva a intentarlo');
      exit;
    }

    $data = $db->prepare($db->select('tb_pago_propuesta pp
        JOIN tb_propuesta_pago p ON p.dc_propuesta = pp.dc_propuesta
        JOIN tb_medio_pago_proveedor m ON p.dc_medio_pago = m.dc_medio_pago', 'pp.dc_propuesta, pp.dc_banco, pp.dg_documento, pp.dq_monto, pp.df_emision, p.dq_propuesta, p.dg_propuesta, p.dg_comentario,
       m.dc_cuenta_contable_intermedia, m.dc_tipo_movimiento, pp.df_vencimiento, p.dc_proveedor, p.dc_medio_pago, pp.dc_pago', 'pp.dc_pago = ? AND p.dc_empresa = ? AND p.dm_estado = ?'));
    $data->bindValue(1, $dc_pago, PDO::PARAM_INT);
    $data->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
    $data->bindValue(3, 'C', PDO::PARAM_STR);
    $db->stExec($data);
    $data = $data->fetch(PDO::FETCH_OBJ);

    if ($data === false) {
      $this->getErrorMan()->showWarning('No se encontró el detalle de pago que desea confirmar o este ya fue confirmado.');
      exit;
    }

    return $data;
  }

  private function getPagoAnticipadoData($dc_pago) {
    $db = $this->getConnection();
    $dc_pago = intval($dc_pago);

    if (!$dc_pago) {
      $this->getErrorMan()->showWarning('El pago a confirmar es inválido, compruebe la entrada y vuelva a intentarlo');
      exit;
    }

    $data = $db->prepare($db->select('tb_documento_pago_anticipado pp
        JOIN tb_comprobante_pago_anticipado p ON p.dc_comprobante = pp.dc_comprobante
        JOIN tb_medio_pago_proveedor m ON p.dc_medio_pago = m.dc_medio_pago', 'pp.dc_comprobante, pp.dc_banco, pp.dg_documento, pp.dq_monto, pp.df_emision, p.dq_comprobante, p.dg_comentario,
       m.dc_cuenta_contable_intermedia, m.dc_tipo_movimiento, pp.df_vencimiento, p.dc_proveedor, pp.dc_documento, p.dc_medio_pago', 'pp.dc_documento = ? AND p.dc_empresa = ? AND pp.dm_estado = ?'));
    $data->bindValue(1, $dc_pago, PDO::PARAM_INT);
    $data->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
    $data->bindValue(3, 'L', PDO::PARAM_STR);
    $db->stExec($data);
    $data = $data->fetch(PDO::FETCH_OBJ);

    if ($data === false) {
      $this->getErrorMan()->showWarning('No se encontró el detalle de pago que desea confirmar o este ya fue confirmado.');
      exit;
    }

    return $data;
  }

  protected function insertarDetalleChequeFinanciero() {
    //DEBE: acá van los montos de cobros con la cuenta intermedia asociada
    $comprobante = $this->getComprobanteService();
    $comprobante->setParametroFinanciero('dc_cuenta_contable', $this->pago_data->dc_cuenta_contable_intermedia);
    $comprobante->setParametroFinanciero('dq_debe', $this->pago_data->dq_monto);
    $comprobante->setParametroFinanciero('dq_haber', 0);
    $comprobante->setParametroFinanciero('dg_glosa', "Cobro cheque: {$this->pago_data->dg_documento} vencimiento: {$this->pago_data->df_vencimiento}");

    return $comprobante->DetalleFinancieroPersist();
  }

  protected function insertarDetalleGiroFinanciero() {
    $comprobante = $this->getComprobanteService();

    //HABER: Acá va el detalle del haber, la cuenta donde se cargan los montos finalmente.
    $comprobante->setParametroFinanciero('dc_cuenta_contable', $this->detalleHaber['dc_cuenta_contable']);
    $comprobante->setParametroFinanciero('dq_debe', 0);
    $comprobante->setParametroFinanciero('dq_haber', $this->pago_data->dq_monto);
    $comprobante->setParametroFinanciero('dg_glosa', $this->detalleHaber['dg_glosa']);

    return $comprobante->DetalleFinancieroPersist();
  }
  
  protected function insertarDetallesAnaliticos() {

    $comprobante = $this->getComprobanteService();
    $comprobante->prepareDetalleAnalitico();

    $comprobante->setParametroAnalitico('dc_proveedor', $this->pago_data->dc_proveedor);
    $comprobante->setParametroAnalitico('dc_banco', $this->pago_data->dc_banco);
    $comprobante->setParametroAnalitico('dc_medio_pago_proveedor', $this->pago_data->dc_medio_pago);
    $comprobante->setParametroAnalitico('dg_cheque', $this->pago_data->dg_documento);
    $comprobante->setParametroAnalitico('df_cheque', $this->pago_data->df_emision);
    $comprobante->setDependencias('dc_proveedor');

    $this->insertarDetalleChequeAnalitico();
    $this->insertarDetalleGiroAnalitico();
  }

  protected function insertarDetalleChequeAnalitico() {
    //DEBE: acá van los montos de cobros con la cuenta intermedia asociada
    $comprobante = $this->getComprobanteService();
    $comprobante->setParametroAnalitico('dc_cuenta_contable', $this->pago_data->dc_cuenta_contable_intermedia);
    $comprobante->setParametroAnalitico('dq_debe', $this->pago_data->dq_monto);
    $comprobante->setParametroAnalitico('dq_haber', 0);
    $comprobante->setParametroAnalitico('dg_glosa', "Cobro cheque: {$this->pago_data->dg_documento} vencimiento: {$this->pago_data->df_vencimiento}");
    $comprobante->setParametroAnalitico('dc_detalle_financiero', $this->matrizFinanciera[0]);

    //Insertar detalle DEBE
    $comprobante->DetalleAnaliticoPersist();
  }

  protected function insertarDetalleGiroAnalitico() {
    //HABER: Acá va el detalle del haber, la cuenta donde se cargan los montos finalmente.
    $comprobante = $this->getComprobanteService();
    $comprobante->setParametroAnalitico('dc_cuenta_contable', $this->detalleHaber['dc_cuenta_contable']);
    $comprobante->setParametroAnalitico('dq_debe', 0);
    $comprobante->setParametroAnalitico('dq_haber', $this->pago_data->dq_monto);
    $comprobante->setParametroAnalitico('dg_glosa', $this->detalleHaber['dg_glosa']);
    $comprobante->setParametroAnalitico('dc_detalle_financiero', $this->matrizFinanciera[1]);

    //Insertar detalle HABER
    $comprobante->DetalleAnaliticoPersist();
  }

  private function confirmarGiro() {
    $db = $this->getConnection();

    $update_giro = $db->doExec($db->update('tb_pago_propuesta', array(
                'dm_estado' => 1
                    ), "dc_pago = {$this->pago_data->dc_pago}"));
  }

  private function confirmarGiroAnticipado() {

    $db = $this->getConnection();

    //Marcar Propuesta como terminada
    $update_documento = $db->prepare($db->update('tb_documento_pago_anticipado', array('dm_estado' => '?'), 'dc_documento = ?'));
    $update_documento->bindValue(1, 'T', PDO::PARAM_STR);
    $update_documento->bindValue(2, $this->pago_data->dc_documento, PDO::PARAM_INT);
    $db->stExec($update_documento);
  }

  private function cerrarPropuesta() {
    $db = $this->getConnection();

    $giros = $db->doQuery($db->select('tb_pago_propuesta', 'true', "dc_propuesta = {$this->pago_data->dc_propuesta} AND dm_estado = 0"));

    if ($giros->fetch() === false) {
      //Marcar Propuesta como terminada
      $update_propuesta = $db->prepare($db->update('tb_propuesta_pago', array('dm_estado' => '?'), 'dc_propuesta = ?'));
      $update_propuesta->bindValue(1, 'T', PDO::PARAM_STR);
      $update_propuesta->bindValue(2, $this->pago_data->dc_propuesta, PDO::PARAM_INT);
      $db->stExec($update_propuesta);
    }
  }

  private function getProveedor() {
    if ($this->proveedor != null) {
      return $this->proveedor;
    }

    $this->proveedor = $this->getConnection()->getRowById('tb_proveedor', $this->pago_data->dc_proveedor, 'dc_proveedor');

    return $this->proveedor;
  }

}

?>
