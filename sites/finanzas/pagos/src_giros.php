<?php
define('MAIN',1);
require_once("../../../inc/global.php");
require_once("../../../inc/form-class.php");

$form = new Form($empresa);
?>
<div id="secc_bar">
	Confirmación de Pagos de Proveedor
</div>
<div id="main_cont">
	<div class="panes">
		<?php $form->Start('sites/finanzas/pagos/proc/src_giros.php','src_giros') ?>
		<?php $form->Header('Indique los filtros para buscar pagos a proveedores por confirmar') ?>
		<table class="tab" width="100%">
			<tbody>
				<tr>
					<td>Número de propuesta</td>
					<td><?php $form->Text('Desde','dq_propuesta_desde') ?></td>
					<td><?php $form->Text('hasta','dq_propuesta_hasta') ?></td>
				</tr>
				<tr>
					<td>Nombre de propuesta</td>
					<td><br /><?php $form->Text('','dg_propuesta'); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Emisión cheque</td>
					<td><?php $form->Date('Desde','df_pago_desde') ?></td>
					<td><?php $form->Date('Hasta','df_pago_hasta') ?></td>
				</tr>
				<tr>
					<td>Vencimiento cheque</td>
					<td><?php $form->Date('Desde','df_vencimiento_desde') ?></td>
					<td><?php $form->Date('Hasta','df_vencimiento_hasta') ?></td>
				</tr>
				<tr>
					<td>Números de Documento</td>
					<td><?php $form->Text('Separados con coma','dg_documento') ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Proveedor</td>
					<td><?php $form->ListadoMultiple('','dc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon')); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Banco</td>
					<td><?php $form->ListadoMultiple('','dc_banco','tb_banco',array('dc_banco','dg_banco')); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td colspan="2"><?php $form->Combobox('','dm_terminados', array('Mostrar pagos terminados')) ?></td>
				</tr>
			</tbody>
		</table>
		<?php $form->End('Ejecutar Búsqueda','searchbtn') ?>
	</div>
</div>
<script type="text/javascript">
  $('#dc_banco,#dc_proveedor').multiSelect({
            selectAll: true,
            selectAllText: "Seleccionar todos",
            noneSelected: "---",
            oneOrMoreSelected: "% seleccionado(s)"
        });
</script>