<?php

require_once 'sites/contabilidad/stuff.class.php';

/**
 * Description of GestionPropuestaPagoFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 17-04-2013
 */
class GestionPropuestaPagoFactory extends Factory{
  
    private $propuesta;
    private $estado_final;
    
    private $facturasPagadas;
    private $detallesPagoBanco;
    private $saldo_propuesta = null;
    private $saldos_favor_propuesta = null;
    
    private $proveedor = null;
    
    private $dq_comprobante;
    private $dc_comprobante;
    private $matrizFinanciera;
    private $parametrosFinancieros;
    private $parametrosAnaliticos;

    public function confirmarPropuestaPagoAction(){
      $request = self::getRequest();
      
      $this->propuesta = $this->getPropuestaData($request->id);
      $this->estado_final = $this->getEstadoFinalConfirmacion();
      
      $this->getConnection()->start_transaction();
          $this->cambiarEstadoPropuesta($this->estado_final);
          $this->dc_comprobante = $this->insertarComprobanteContable("Propuesta: {$this->propuesta->dq_propuesta} - {$this->propuesta->dg_propuesta}\nProveedor:{$this->getProveedor()->dg_razon}\n\n{$this->propuesta->dg_comentario}");
          $this->insertarDetallesComprobanteConfirmar();
      $this->getConnection()->commit();
      
      $this->getErrorMan()->showConfirm("Se ha confirmado la propuesta de pago correctamente");
      echo $this->getView('contabilidad/general/mostrar_comprobante_creado', array(
            'comprobante' => (object) array(
                'dc_comprobante' => $this->dc_comprobante,
                'dg_comprobante' => $this->dq_comprobante
            )
      ));
      
    }
    
    private function getPropuestaData($dc_propuesta){
      
      $db = $this->getConnection();
      
      $propuesta = $db->getRowById('tb_propuesta_pago', $dc_propuesta, 'dc_propuesta');
      
      //Factory::debug($propuesta);
      
      if($propuesta === false || $propuesta->dc_empresa != $this->getEmpresa()){
          $this->getErrorMan()->showAviso('No se pudo completar la confirmación de la propuesta, las posibles son:<br /><br />
          <ul>
              <li>La propuesta ingresada es inválida</li>
              <li>La propuesta ya ha sido confirmada anteriormente</li>
          </ul>');
          exit;
      }
      
      if ($propuesta->dm_estado != 'P'){
          $this->getErrorMan()->showAviso('No se pudo completar la confirmación de la propuesta, las posibles son:<br /><br />
          <ul>
              <li>No se han generado los documentos de pago necesarios previa confirmación</li>
          </ul>');
          exit;
      }
      
      $medio_pago = $db->getRowById('tb_medio_pago_proveedor', $propuesta->dc_medio_pago, 'dc_medio_pago');
      $propuesta->medio_pago = $medio_pago;
      
      return $propuesta;
    }
    
    private function getEstadoFinalConfirmacion(){
      if($this->propuesta->medio_pago === false ||$this->propuesta->medio_pago->dm_asiento_intermedio == 0){
          return 'T';
      }else{
          return 'C';
      }
    }
    
    private function cambiarEstadoPropuesta($dm_estado){
      
      $db = $this->getConnection();
      
      $update_estado = $db->prepare($db->update('tb_propuesta_pago',array(
          'dm_estado' => '?'
      ),'dc_propuesta = ?'));
      $update_estado->bindValue(1,$dm_estado,PDO::PARAM_STR);
      $update_estado->bindValue(2,$this->propuesta->dc_propuesta,PDO::PARAM_INT);
      $db->stExec($update_estado);
    }
    
    private function insertarComprobanteContable($glosa){
      $this->dq_comprobante = ContabilidadStuff::getCorrelativoComprobante($this->getConnection(), $this->getEmpresa());
      
      $request = self::getRequest();
      $db = $this->getConnection();
      
      if($this->propuesta->medio_pago === false){
        $dc_tipo_movimiento = ContabilidadStuff::getConfiguration($db, $this->getEmpresa())->dc_tipo_movimiento_pago_saldo_favor_proveedor;
      }else{
        $dc_tipo_movimiento = $this->propuesta->medio_pago->dc_tipo_movimiento;
      }
      
      $insert_comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
          'dg_comprobante' => '?',
          'dc_tipo_movimiento' => '?',
          'df_fecha_contable' => '?',
          'dc_mes_contable' => 'MONTH(?)',
          'dc_anho_contable' => 'YEAR(?)',
          'dc_tipo_cambio' => '?',
          'dq_cambio' => 1,
          'dg_glosa' => '?',
          'dc_propuesta_pago' => '?',
          'dq_saldo' => $this->getSaldoPropuesta(),
          'df_fecha_emision' => $db->getNow(),
          'dc_empresa' => $this->getEmpresa(),
          'dc_usuario_creacion' => $this->getUserData()->dc_usuario
      )));
      $insert_comprobante->bindValue(1,$this->dq_comprobante,PDO::PARAM_INT);
      $insert_comprobante->bindValue(2,$dc_tipo_movimiento,PDO::PARAM_INT);
      $insert_comprobante->bindValue(3,$db->sqlDate2($request->df_fecha_contable),PDO::PARAM_STR);
      $insert_comprobante->bindValue(4,$db->sqlDate2($request->df_fecha_contable),PDO::PARAM_STR);
      $insert_comprobante->bindValue(5,$db->sqlDate2($request->df_fecha_contable),PDO::PARAM_STR);
      $insert_comprobante->bindValue(6,NULL,PDO::PARAM_NULL);
      $insert_comprobante->bindValue(7,$glosa,PDO::PARAM_STR);
      $insert_comprobante->bindValue(8,$this->propuesta->dc_propuesta,PDO::PARAM_INT);
      
      $db->stExec($insert_comprobante);
      
      return $db->lastInsertId();
      
    }
    
    private function insertarDetallesComprobanteConfirmar(){
      $this->insertarDetallesFinancierosConfirmar();
      $this->insertarDetallesAnaliticosConfirmar();
    }
    
      private function insertarDetallesFinancierosConfirmar(){
        $this->matrizFinanciera = array();

        $insert_detalle_comprobante = $this->prepareDetallesFinancierosConfirmar();

        //Insertar los detalles de facturas que serán confirmadas
        $this->matrizFinanciera['facturas'] = $this->insertarDFFacturasProveedorConfirmar($insert_detalle_comprobante);

        //Si tiene medio de pago la propuesta se debe rebajar las cuentas dependiendo el medio de pago
        if($this->propuesta->medio_pago != false){
          $this->matrizFinanciera['medio_pago'] = $this->insertarDFMedioPagoConfirmar($insert_detalle_comprobante);
        }

        //Si se utilizó saldo a favor se rebaja de la cuenta de proveedor
        if(floatval($this->propuesta->dq_saldo_favor) > 0){
          $this->matrizFinanciera['saldo_favor'] = $this->insertarDFSaldoFavorConfirmar($insert_detalle_comprobante);
        }

      }

        private function prepareDetallesFinancierosConfirmar(){
          $db = $this->getConnection();
          $this->parametrosFinancieros = new stdClass();

          $insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
              'dc_comprobante' => $this->dc_comprobante,
              'dc_cuenta_contable' => '?',
              'dq_debe' => '?',
              'dq_haber' => '?',
              'dg_glosa' => '?'
          )));
          $insert_detalle_comprobante->bindParam(1,$this->parametrosFinancieros->dc_cuenta_contable,PDO::PARAM_INT);
          $insert_detalle_comprobante->bindParam(2,$this->parametrosFinancieros->dq_debe,PDO::PARAM_STR);
          $insert_detalle_comprobante->bindParam(3,$this->parametrosFinancieros->dq_haber,PDO::PARAM_STR);
          $insert_detalle_comprobante->bindParam(4,$this->parametrosFinancieros->dg_glosa,PDO::PARAM_STR);

          return $insert_detalle_comprobante;
        }

        private function insertarDFFacturasProveedorConfirmar($st){

          $db = $this->getConnection();

          $this->parametrosFinancieros->dq_debe = $this->getSaldoPropuesta();
          $this->parametrosFinancieros->dc_cuenta_contable = $this->getProveedor()->dc_cuenta_contable;
          $this->parametrosFinancieros->dq_haber = 0;
          $this->parametrosFinancieros->dg_glosa = "Saldo Pagado Facturas de Compra";

          $db->stExec($st);

          return $db->lastInsertId();
        }

        private function insertarDFMedioPagoConfirmar($st){
          $db = $this->getConnection();
          $this->parametrosFinancieros->dq_debe = 0;
          $this->parametrosFinancieros->dg_glosa = "Saldo Pagado Medio de Pago: {$this->propuesta->medio_pago->dg_medio_pago}";

          //En caso de que el medio de pago tenga configurado el banco y el documento (Pago con cheque)
          if($this->propuesta->medio_pago->dm_requiere_documento == 1 and
             $this->propuesta->medio_pago->dm_requiere_banco == 1 and
             $this->propuesta->medio_pago->dm_asiento_intermedio == 1)
          {
              return $this->insertarDFMPDocumentoBancoConfirmar($st);
          }else

          //En caso de que el medio de pago tenga banco pero no documento (Pago transferencia)
          if($this->propuesta->medio_pago->dm_requiere_documento == 0 and
             $this->propuesta->medio_pago->dm_requiere_banco == 1)
          {
              return $this->insertarDFMPSoloBancoConfirmar($st);
          }else

          //En caso en que el medio de pago no tenga ni banco ni documento (pago en efectivo)
          if($this->propuesta->medio_pago->dm_requiere_documento == 0 and
             $this->propuesta->medio_pago->dm_requiere_banco == 0)
          {
              return $this->insertarDFMPNoBancoNoDocumento($st);
          }else

          //En caso de que el medio de pago haya sido mal configurado
          {
            $this->getErrorMan()->showWarning("El Medio de pago contiene una configuración que no permite realizar pagos en el sistema, consulte con el administrador");
            $db->rollBack();
            exit;
          }

        }

          private function insertarDFMPDocumentoBancoConfirmar($st){
            $db = $this->getConnection();

            $pagos = $this->getDetallesPagoBancoPropuesta();

            $this->parametrosFinancieros->dc_cuenta_contable = $this->propuesta->medio_pago->dc_cuenta_contable_intermedia;
            $dq_haber = 0;

            foreach($pagos as $d){
              $dq_haber += $d->dq_monto;
            }
            
            $this->parametrosFinancieros->dq_haber = $dq_haber;

            $db->stExec($st);

            return $db->lastInsertId();

          }

          private function insertarDFMPSoloBancoConfirmar($st){
            $pagos = $this->getDetallesPagoBancoPropuesta();
            $db = $this->getConnection();
            $array_financiero = array();

            foreach($pagos as $d){

              if(!isset($array_financiero[$d->dc_cuenta_contable])){
                $array_financiero[$d->dc_cuenta_contable] = 0;
              }

              $array_financiero[$d->dc_cuenta_contable] += $d->dq_monto;

            }

            foreach($array_financiero as $dc_cuenta_contable => $dq_haber){
              $this->parametrosFinancieros->dq_haber = $dq_haber;
              $this->parametrosFinancieros->dc_cuenta_contable = $dc_cuenta_contable;

              $db->stExec($st);

              $array_financiero[$dc_cuenta_contable] = $db->lastInsertId();
            }

            return $array_financiero;

          }

          private function insertarDFMPNoBancoNoDocumento($st){
              $db = $this->getConnection();

              $this->parametrosFinancieros->dc_cuenta_contable = self::getRequest()->dc_cuenta_efectivo;
              $this->parametrosFinancieros->dq_haber = $this->getSaldoPropuesta() - floatval($this->propuesta->dq_saldo_favor);

              $db->stExec($st);

              return $db->lastInsertId();
          }

       private function insertarDFSaldoFavorConfirmar($st){
              $db = $this->getConnection();
              
              $saldos = $this->getSaldosFavorPropuesta();
              
              $group = array();
              $group[$this->getProveedor()->dc_cuenta_contable] = 0;
              $group[$this->getProveedor()->dc_cuenta_anticipo] = 0;
              
              foreach($saldos as $d){
                  if($d->dc_factura_compra != NULL || $d->dc_orden_compra != NULL){
                    $group[$this->getProveedor()->dc_cuenta_anticipo] += $d->dq_monto;
                  }else if($d->dc_nota_credito != NULL){
                    $group[$this->getProveedor()->dc_cuenta_contable] += $d->dq_monto;
                  }
              }
              
              $this->parametrosFinancieros->dg_glosa = "Saldo Pagado Medio de Pago: Saldo a Favor";
              $this->parametrosFinancieros->dq_debe = 0;
              
              foreach($group as $dc_cuenta_contable => $g){
                if($g > 0){
                  $this->parametrosFinancieros->dc_cuenta_contable = $dc_cuenta_contable;
                  $this->parametrosFinancieros->dq_haber = $g;
                  
                  $db->stExec($st);
                  
                  $group[$dc_cuenta_contable] = $db->lastInsertId();
                }else{
                  unset($group[$dc_cuenta_contable]);
                }
              }

              return $group;

          }
    
      private function insertarDetallesAnaliticosConfirmar(){
         $insert_detalle_analitico = $this->prepareDetallesAnaliticosConfirmar();
         $this->parametrosAnaliticos->dc_cuenta_contable = $this->getProveedor()->dc_cuenta_contable;
         $this->parametrosAnaliticos->dc_proveedor = $this->getProveedor()->dc_proveedor;
         $this->parametrosAnaliticos->dc_medio_pago_proveedor = $this->propuesta->dc_medio_pago;
         $this->parametrosAnaliticos->dc_mercado_proveedor = $this->getProveedor()->dc_mercado;
         $this->parametrosAnaliticos->dc_tipo_proveedor = $this->getProveedor()->dc_tipo_proveedor;
         $this->parametrosAnaliticos->dc_orden_compra = NULL;
         
         //Insertar los detalles de facturas que serán confirmadas
         $this->insertarDAFacturasProveedorConfirmar($insert_detalle_analitico);
         
         //Si tiene medio de pago la propuesta se debe rebajar las cuentas dependiendo el medio de pago
         if($this->propuesta->medio_pago != false){
           $this->insertarDAMedioPagoConfirmar($insert_detalle_analitico);
         }
         
         //Si se utilizó saldo a favor se rebaja de la cuenta de proveedor
         if(floatval($this->propuesta->dq_saldo_favor) > 0){
           $this->insertarDASaldoFavorConfirmar($insert_detalle_analitico);
         }
         
      }
      
        private function prepareDetallesAnaliticosConfirmar(){
            $db = $this->getConnection();
            $this->parametrosAnaliticos = new stdClass();
            
            $insert_detalle_analitico = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico', array(
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?',
                'dc_detalle_financiero' => '?',
                'dc_factura_compra' => '?',
                'dc_nota_venta' => '?',
                'dc_orden_servicio' => '?',
                'dc_proveedor' => '?',
                'dc_tipo_proveedor' => '?',
                'dc_cliente' => '?',
                'dc_tipo_cliente' => '?',
                'dc_banco' => '?',
                'dc_medio_pago_proveedor' => '?',
                'dc_mercado_cliente' => '?',
                'dc_mercado_proveedor' => '?',
                'dc_nota_credito_proveedor' => '?',
                'dg_cheque' => '?',
                'df_cheque' => '?',
                'dc_ejecutivo' => '?',
                'dc_orden_compra' => '?'
            )));
            $insert_detalle_analitico->bindParam(1 , $this->parametrosAnaliticos->dc_cuenta_contable, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(2 , $this->parametrosAnaliticos->dq_debe, PDO::PARAM_STR);
            $insert_detalle_analitico->bindParam(3 , $this->parametrosAnaliticos->dq_haber, PDO::PARAM_STR);
            $insert_detalle_analitico->bindParam(4 , $this->parametrosAnaliticos->dg_glosa, PDO::PARAM_STR);
            $insert_detalle_analitico->bindParam(5 , $this->parametrosAnaliticos->dc_detalle_financiero, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(6 , $this->parametrosAnaliticos->dc_factura_compra, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(7 , $this->parametrosAnaliticos->dc_nota_venta, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(8 , $this->parametrosAnaliticos->dc_orden_servicio, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(9 , $this->parametrosAnaliticos->dc_proveedor, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(10, $this->parametrosAnaliticos->dc_tipo_proveedor, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(11, $this->parametrosAnaliticos->dc_cliente, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(12, $this->parametrosAnaliticos->dc_tipo_cliente, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(13, $this->parametrosAnaliticos->dc_banco, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(14, $this->parametrosAnaliticos->dc_medio_pago_proveedor, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(15, $this->parametrosAnaliticos->dc_mercado_cliente, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(16, $this->parametrosAnaliticos->dc_mercado_proveedor, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(17, $this->parametrosAnaliticos->dc_nota_credito_proveedor, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(18, $this->parametrosAnaliticos->dg_cheque, PDO::PARAM_STR);
            $insert_detalle_analitico->bindParam(19, $this->parametrosAnaliticos->df_cheque, PDO::PARAM_STR);
            $insert_detalle_analitico->bindParam(20, $this->parametrosAnaliticos->dc_ejecutivo, PDO::PARAM_INT);
            $insert_detalle_analitico->bindParam(21, $this->parametrosAnaliticos->dc_orden_compra, PDO::PARAM_INT);
            
            return $insert_detalle_analitico;
        }
        
        private function insertarDAFacturasProveedorConfirmar($st){
            $db = $this->getConnection();
            
            $facturas = $this->getFacturasPagadas();
            
            $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['facturas'];
            $this->parametrosAnaliticos->dq_haber = 0;
            $this->parametrosAnaliticos->dc_banco = NULL;
            $this->parametrosAnaliticos->dc_nota_credito_proveedor = NULL;
            $this->parametrosAnaliticos->dg_cheque = NULL;
            $this->parametrosAnaliticos->df_cheque = NULL;
            
            foreach($facturas as $f){
                $this->parametrosAnaliticos->dq_debe = floatval($f->dq_monto);
                $this->parametrosAnaliticos->dg_glosa = "Pago Factura: {$f->dq_factura} folio: {$f->dq_folio}";
                $this->parametrosAnaliticos->dc_factura_compra = $f->dc_factura;
                if($f->dc_nota_venta){
                    $this->parametrosAnaliticos->dc_nota_venta = $f->dc_nota_venta;
                    $this->parametrosAnaliticos->dc_orden_servicio = NULL;
                    $nv = $db->getRowById('tb_nota_venta', $f->dc_nota_venta, 'dc_nota_venta');
                    $this->parametrosAnaliticos->dc_cliente = $nv->dc_cliente;
                    $this->parametrosAnaliticos->dc_ejecutivo = $nv->dc_ejecutivo;
                }else if($f->dc_orden_servicio){
                    $this->parametrosAnaliticos->dc_orden_servicio;
                    $this->parametrosAnaliticos->dc_nota_venta = NULL;
                    $os = $db->getRowById('tb_orden_servicio', $f->dc_orden_servicio, 'dc_orden_servicio');
                    $this->parametrosAnaliticos->dc_cliente = $os->dc_cliente;
                    $this->parametrosAnaliticos->dc_ejecutivo = NULL;
                }else{
                    $this->parametrosAnaliticos->dc_nota_venta = NULL;
                    $this->parametrosAnaliticos->dc_orden_servicio = NULL;
                    $this->parametrosAnaliticos->dc_cliente = NULL;
                    $this->parametrosAnaliticos->dc_ejecutivo = NULL;
                }
                
                if($this->parametrosAnaliticos->dc_cliente){
                    $cliente = $db->getRowById('tb_cliente', $this->parametrosAnaliticos->dc_cliente, 'dc_cliente');
                    $this->parametrosAnaliticos->dc_tipo_cliente = $cliente->dc_tipo_cliente;
                    $this->parametrosAnaliticos->dc_mercado_cliente = $cliente->dc_mercado;
                }else{
                    $this->parametrosAnaliticos->dc_tipo_cliente = NULL;
                    $this->parametrosAnaliticos->dc_mercado_cliente = NULL;
                }
                
                $db->stExec($st);
                
            }
            
        }
        
        private function insertarDAMedioPagoConfirmar($st){
            $db = $this->getConnection();
            $this->parametrosAnaliticos->dq_debe = 0;
            $this->parametrosAnaliticos->dg_glosa = "Saldo Pagado Medio de Pago: {$this->propuesta->medio_pago->dg_medio_pago}";
            $this->parametrosAnaliticos->dc_factura_compra = NULL;
            $this->parametrosAnaliticos->dc_nota_venta = NULL;
            $this->parametrosAnaliticos->dc_orden_servicio = NULL;
            $this->parametrosAnaliticos->dc_cliente = NULL;
            $this->parametrosAnaliticos->dc_tipo_cliente = NULL;
            $this->parametrosAnaliticos->dc_mercado_cliente = NULL;
            $this->parametrosAnaliticos->dc_nota_credito_proveedor = NULL;
            $this->parametrosAnaliticos->dc_ejecutivo = NULL;

            //En caso de que el medio de pago tenga configurado el banco y el documento (Pago con cheque)
            if($this->propuesta->medio_pago->dm_requiere_documento == 1 and
               $this->propuesta->medio_pago->dm_requiere_banco == 1 and
               $this->propuesta->medio_pago->dm_asiento_intermedio == 1)
            {
                $this->insertarDAMPDocumentoBancoConfirmar($st);
            }else

            //En caso de que el medio de pago tenga banco pero no documento (Pago transferencia)
            if($this->propuesta->medio_pago->dm_requiere_documento == 0 and
               $this->propuesta->medio_pago->dm_requiere_banco == 1)
            {
                $this->insertarDAMPSoloBancoConfirmar($st);
            }else

            //En caso en que el medio de pago no tenga ni banco ni documento (pago en efectivo)
            if($this->propuesta->medio_pago->dm_requiere_documento == 0 and
               $this->propuesta->medio_pago->dm_requiere_banco == 0)
            {
                $this->insertarDAMPNoBancoNoDocumento($st);
            }else

            //En caso de que el medio de pago haya sido mal configurado
            {
              $this->getErrorMan()->showWarning("El Medio de pago contiene una configuración que no permite realizar pagos en el sistema, consulte con el administrador");
              $db->rollBack();
              exit;
            }
        }
        
          private function insertarDAMPDocumentoBancoConfirmar($st){
              $db = $this->getConnection();
              $pagos = $this->getDetallesPagoBancoPropuesta();

              $this->parametrosAnaliticos->dc_cuenta_contable = $this->propuesta->medio_pago->dc_cuenta_contable_intermedia;
              $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['medio_pago'];


              foreach($pagos as $d){
                $this->parametrosAnaliticos->dq_haber = $d->dq_monto;
                $this->parametrosAnaliticos->dc_banco = $d->dc_banco;
                $this->parametrosAnaliticos->dg_cheque = $d->dg_documento;
                $this->parametrosAnaliticos->df_cheque = $d->df_emision;

                $db->stExec($st);
              }
          }
          
          private function insertarDAMPSoloBancoConfirmar($st){
              $db = $this->getConnection();
              $pagos = $this->getDetallesPagoBancoPropuesta();

              $this->parametrosAnaliticos->dg_cheque = NULL;
              $this->parametrosAnaliticos->df_cheque = NULL;


              foreach($pagos as $d){
                $this->parametrosAnaliticos->dc_cuenta_contable = $d->dc_cuenta_contable;
                $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['medio_pago'][$d->dc_cuenta_contable];
                $this->parametrosAnaliticos->dq_haber = $d->dq_monto;
                $this->parametrosAnaliticos->dc_banco = $d->dc_banco;

                $db->stExec($st);
              }
          }
          
          private function insertarDAMPNoBancoNoDocumento($st){
              $db = $this->getConnection();

              $this->parametrosAnaliticos->dc_cuenta_contable = self::getRequest()->dc_cuenta_efectivo;
              $this->parametrosAnaliticos->dq_haber = $this->getSaldoPropuesta() - floatval($this->propuesta->dq_saldo_favor);
              $this->parametrosAnaliticos->dg_cheque = NULL;
              $this->parametrosAnaliticos->df_cheque = NULL;
              $this->parametrosAnaliticos->dc_banco = NULL;
              $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['medio_pago'];

              $db->stExec($st);
          }
          
        private function insertarDASaldoFavorConfirmar($st){
          
            $db = $this->getConnection();
            
            $this->parametrosAnaliticos->dq_debe = 0;
            $this->parametrosAnaliticos->dg_glosa = "Saldo Pagado Medio de Pago: Saldo a Favor";
            $this->parametrosAnaliticos->dc_nota_venta = NULL;
            $this->parametrosAnaliticos->dc_orden_servicio = NULL;
            $this->parametrosAnaliticos->dc_cliente = NULL;
            $this->parametrosAnaliticos->dc_tipo_cliente = NULL;
            $this->parametrosAnaliticos->dc_banco = NULL;
            $this->parametrosAnaliticos->dc_mercado_cliente = NULL;
            $this->parametrosAnaliticos->dg_cheque = NULL;
            $this->parametrosAnaliticos->df_cheque = NULL;
            $this->parametrosAnaliticos->dc_ejecutivo = NULL;
          
            $saldos = $this->getSaldosFavorPropuesta();
            
            foreach($saldos as $d){
                
                $this->parametrosAnaliticos->dq_haber = $d->dq_monto;
              
                if($d->dc_factura_compra != NULL || $d->dc_orden_compra != NULL){
                    $this->parametrosAnaliticos->dc_cuenta_contable = $this->getProveedor()->dc_cuenta_anticipo;
                    $this->parametrosAnaliticos->dc_factura_compra = $d->dc_factura_compra;
                    $this->parametrosAnaliticos->dc_orden_compra = $d->dc_orden_compra;
                    $this->parametrosAnaliticos->dc_nota_credito_proveedor = NULL;
                    
                    $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['saldo_favor'][$this->getProveedor()->dc_cuenta_anticipo];
                }else if($d->dc_nota_credito != NULL){
                    $this->parametrosAnaliticos->dc_cuenta_contable = $this->getProveedor()->dc_cuenta_contable;
                    $this->parametrosAnaliticos->dc_nota_credito_proveedor = $d->dc_nota_credito;
                    $this->parametrosAnaliticos->dc_factura_compra = NULL;
                    $this->parametrosAnaliticos->dc_orden_compra = NULL;
                    
                    $this->parametrosAnaliticos->dc_detalle_financiero = $this->matrizFinanciera['saldo_favor'][$this->getProveedor()->dc_cuenta_contable];
                }
                
                $db->stExec($st);
                
            }
        }

    private function getFacturasPagadas(){
        if($this->facturasPagadas != null){
          return $this->facturasPagadas;
        }

        $db = $this->getConnection();

        $this->facturasPagadas = $detalles_factura = $db->prepare($db->select('tb_propuesta_pago_detalle d
          JOIN tb_factura_compra f ON f.dc_factura = d.dc_factura',
        'd.dq_monto, f.*',
        'd.dc_propuesta = ?'));
        $detalles_factura->bindValue(1,$this->propuesta->dc_propuesta,PDO::PARAM_INT);
        $db->stExec($detalles_factura);
        $this->facturasPagadas = $this->facturasPagadas->fetchAll(PDO::FETCH_OBJ);

        return $this->facturasPagadas;
      
    }
    
    private function getProveedor(){
      if($this->proveedor != null){
        return $this->proveedor;
      }
      
      $this->proveedor = $this->getConnection()->getRowById('tb_proveedor', $this->propuesta->dc_proveedor, 'dc_proveedor');
      
      return $this->proveedor;
    }
    
    private function getDetallesPagoBancoPropuesta(){
      
        if($this->detallesPagoBanco != NULL){
            return $this->detallesPagoBanco;
        }
      
        $db = $this->getConnection();
      
        $pagos = $db->prepare($db->select('tb_pago_propuesta pp
          JOIN tb_banco b ON pp.dc_banco = b.dc_banco',
		'pp.dc_banco, pp.dg_documento, pp.df_vencimiento, pp.dq_monto, pp.df_emision, b.dg_banco, b.dc_cuenta_contable',
		'pp.dc_propuesta = ?'));
		$pagos->bindValue(1,$this->propuesta->dc_propuesta,PDO::PARAM_INT);
		$db->stExec($pagos);
        
        $this->detallesPagoBanco = $pagos->fetchAll(PDO::FETCH_OBJ);
        
        return $this->detallesPagoBanco;
    }
    
    private function getSaldoPropuesta(){
        if($this->saldo_propuesta != NULL){
          return $this->saldo_propuesta;
        }
        
        $facturas = $this->getFacturasPagadas();
        $saldo = 0;
        foreach ($facturas as $d){
          $saldo += $d->dq_monto;
        }
        
        $this->saldo_propuesta = $saldo;
        
        return $this->saldo_propuesta;
        
    }
    
    private function getSaldosFavorPropuesta(){
      
        if($this->saldos_favor_propuesta != null){
          return $this->saldos_favor_propuesta;
        }
      
        $db = $this->getConnection();
      
        $favor_data = $db->prepare($db->select('tb_saldo_favor_propuesta','dc_factura_compra, dc_orden_compra, dc_nota_credito, dq_monto','dc_propuesta = ?'));
        $favor_data->bindValue(1,$this->propuesta->dc_propuesta,PDO::PARAM_INT);
        $db->stExec($favor_data);
        
        $this->saldos_favor_propuesta = $favor_data->fetchAll(PDO::FETCH_OBJ);
        
        return $this->saldos_favor_propuesta;
        
    }
  
}

?>
