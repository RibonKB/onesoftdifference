<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div class="secc_bar">Propuesta de pago</div>');

$dc_factura = implode(',',$_POST['dc_factura']);

//Datos de las facturas seleccionadas
$data = $db->select('tb_factura_compra','df_emision,df_vencimiento,dq_total,dq_monto_pagado,dc_proveedor,dc_factura,dq_factura,dq_folio',
	"dc_factura IN ({$dc_factura}) AND dc_empresa={$empresa} AND dm_nula = 0 AND dq_monto_pagado < dq_total");

//¿Se encontraron las facturas?
if(!count($data)){
	$error_man->showWarning('No se encontraron datos de cobranza para las facturas seleccionadas.');
	exit;
}

//¿Se encontró información de cobranza para todas las facturas y no fue ninguna anulada?
if(count($_POST['dc_factura']) != count($data)){
	$error_man->showAviso("Hubo un cambio en los datos de las facturas seleccionadas que impiden continuar la gestión, por favor actualice el informe de pagos y vuelva a intentarlo.");
	exit;
}

$dc_proveedor = $data[0]['dc_proveedor'];
$proveedor = $db->select('tb_proveedor','dq_saldo_favor','dc_proveedor='.$dc_proveedor);
$proveedor = array_shift($proveedor);

if($proveedor == NULL){
	$error_man->showWarning('Los datos del proveedor no pudieron ser obtenidos, compruebe los datos de entrada.');
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/finanzas/pagos/proc/crear_propuesta_pago.php','cr_propuesta_pago');
$form->Header('<strong>Indique los datos para generar la propuesta de pago sobre las facturas de compra</strong><br />Los datos marcados con [*] son obligatorios');
$form->Section();
	$form->Text('Título propuesta','propuesta_name',1);
	$form->Date('Fecha de propuesta','propuesta_fecha',1,0);
	$form->Listado('Medio de pago','propuesta_medio_pago','tb_medio_pago_proveedor',array('dc_medio_pago','dg_medio_pago'),true);
$form->EndSection();
$form->Section();
	$form->Textarea('Comentario','propuesta_comentario',1);
	
	if($proveedor['dq_saldo_favor'] > 0){
	?>
	<br />
	<a href="sites/finanzas/pagos/src_saldo_favor.php?dc_proveedor=<?php echo $dc_proveedor ?>" id="set_saldo_favor" class="button">
		Asignar saldos a favor
	</a>
	<div id="detalle_saldo"></div>
	<?php
	}
	
$form->EndSection();

$form->Group();
$form->Header("Información de pago");
?>
	
	<table class="tab" width="100%" id="mountTable">
		<thead><tr>
			<th>Factura de compra</th>
			<th>Monto Original</th>
			<th>Monto</th>
		</tr></thead><tbody>
		
		<?php foreach($data as $factura): ?>
			<tr>
				<td>
					<b><?php echo $factura['dq_factura'] ?></b> - <?php echo $factura['dq_folio'] ?>
				</td>
				<td align='right'>
					<?php echo moneda_local($factura['dq_total']-$factura['dq_monto_pagado']) ?>
				</td>
				<td align="right">
				<?php
					$form->Text(
						'','propuesta_dq_monto[]',1,255,round($factura['dq_total']-$factura['dq_monto_pagado'],$empresa_conf['dn_decimales_local']),'mountItem inputtext',30
					)
				?>
					<?php $form->Hidden('max_value[]',$factura['dq_total']-$factura['dq_monto_pagado']) ?>
				</td>
			</tr>
		<?php endforeach; ?>
			
		</tbody>
		<tfoot><tr>
			<th align="right" colspan="2">Total</th>
			<th align="right" width="220" id="totalMount">0</th>
		</tr></tfoot>
	</table>
		
<?php
	
	/*$form->Section();
		echo('<div id="cobros_container"><fieldset>');
		//$form->Text('Monto (Monto sin cobrar: $ <b>'.moneda_local($data['dq_total']-$data['dq_monto_pagado']).'</b>)','gestion_monto',0,255,0);
		$form->Listado('Forma de pago','gestion_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'));
		$form->Listado('Banco','gestion_banco','tb_banco',array('dc_banco','dg_banco'));
		echo('<fieldset>');
		$form->Button('Agregar número de documento','id=add_num_doc','addbtn');
		echo('<div id="doc_container"></div></fieldset></fieldset></div>');
	$form->EndSection();*/

$form->Group();

foreach($data as $factura){
	$form->Hidden('propuesta_dc_factura[]',$factura['dc_factura']);
	$form->Hidden('propuesta_dc_proveedor[]',$factura['dc_proveedor']);
}
$form->End('Crear','addbtn');
?>
<script type="text/javascript" src="jscripts/sites/finanzas/pagos/cr_gestion_pago.commons.js?v_6b"></script>
<script type="text/javascript" src="jscripts/sites/finanzas/pagos/cr_gestion_pago_masiva.js?v_8b"></script>
<script type="text/javascript">
	$('#set_saldo_favor').click(function(e){
		e.preventDefault();
		var data = $('#detalle_saldo :input').serialize();
		var href = this.href;
		pymerp.loadOverlay(href,data,true);
	});
</script>