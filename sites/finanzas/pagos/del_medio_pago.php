<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_medio_pago = intval($_POST['id']);

$medio_pago = $db->prepare($db->select('tb_medio_pago_proveedor',
	'dg_medio_pago, dm_activo','dc_medio_pago = ? AND dc_empresa = ?'));
$medio_pago->bindValue(1,$dc_medio_pago,PDO::PARAM_INT);
$medio_pago->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($medio_pago);

$medio_pago = $medio_pago->fetch(PDO::FETCH_OBJ);

if($medio_pago === false){
	$error_man->showWarning('El medio de pago seleccionado no fue encontrado');
	exit;
}


?>


<div class="secc_bar" >
	Eliminar medios de pago
</div>

<div class="panes" >
	<?php $form->Start('sites/finanzas/pagos/proc/delete_medio_pago.php', 'del_medio_pago') ?>
    <?php $form->Header('¿Está seguro que desea eliminar el medio de pago?') ?>
    
    <div class="center" >
    	<?php echo $medio_pago->dg_medio_pago ?>
    </div>
    <?php $form->Hidden('dc_medio_pago_ed', $dc_medio_pago)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>
