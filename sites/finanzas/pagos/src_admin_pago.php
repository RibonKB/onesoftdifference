<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_propuesta = intval($_POST['id']);

$propuesta = $db->select('tb_propuesta_pago p
JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago
JOIN tb_proveedor pr ON pr.dc_proveedor = p.dc_proveedor',
'p.dq_propuesta, p.dg_propuesta, p.dm_estado, m.dc_banco_default, m.dg_medio_pago, p.dq_saldo_favor,
m.dm_requiere_banco, m.dm_asiento_intermedio, m.dm_requiere_documento, m.dc_tipo_movimiento',
"p.dc_propuesta = {$dc_propuesta} AND p.dc_empresa = {$empresa}");
$propuesta = array_shift($propuesta);

if($propuesta === NULL){
	$error_man->showWarning('No se encontró la propuesta de pago seleccionada, compruebe los datos de entrada o consulte con un administrador.');
	exit;
}

if($propuesta['dm_estado'] === 'L'){
	$error_man->showWarning('No se puede agregar documentación a las propuestas de pago sin validar');
	exit;
}

$pagos = $db->select('tb_pago_propuesta p
LEFT JOIN tb_banco b ON b.dc_banco = p.dc_banco
JOIN tb_usuario u ON u.dc_usuario = p.dc_usuario_creacion',
'p.dc_pago, p.dg_documento, p.df_vencimiento, p.dq_monto, p.df_emision, b.dg_banco, u.dg_usuario',
"p.dc_propuesta = {$dc_propuesta} AND p.dc_empresa = {$empresa}");

$dq_pagar = $db->select('tb_propuesta_pago_detalle','SUM(dq_monto) dq_pagar','dc_propuesta = '.$dc_propuesta);
$dq_pagar = array_shift($dq_pagar);
if($dq_pagar == NULL){
	$error_man->showAviso('Ocurrió un error intentando obtener los datos de la propuesta.');
}
$dq_pagar = floatval($dq_pagar['dq_pagar'] - $propuesta['dq_saldo_favor']);

require_once("../../../inc/form-class.php");
$form = new Form($empresa);
?>
<div class="secc_bar">
	Administración de pago de propuesta <b><?php echo $propuesta['dq_propuesta'] ?></b>
</div>
<div class="panes">
	
	<?php if(!count($pagos)): ?>
		<div class="alert">
			No se han realizados gestiones de pago sobre esta propuesta
		</div>
	<?php else: ?>
		<table class="tab" width="100%">
			<thead>
				<tr>
					<th>&nbsp;</th>
				<?php if($propuesta['dm_requiere_documento'] == 1): ?>
					<th>Documento</th>
					<th>Fecha de vencimiento</th>
				<?php endif; ?>
				
				<?php if($propuesta['dm_requiere_banco'] == 1): ?>
					<th>Banco</th>
				<?php endif; ?>
					
					<th>Fecha emisión</th>
					<th>Responsable</th>
					<th>Monto</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($pagos as $p): ?>
				<tr>
					<td>
						<a href="sites/finanzas/pagos/ed_admin_pago.php?id=<?php echo $p['dc_pago'] ?>" class="loadOnOverlay">
						<img src="images/editbtn.png" alt="[ED]" title="Editar Detalle" />
						</a>
					</td>
				<?php if($propuesta['dm_requiere_documento'] == 1): ?>
					<td><?php echo $p['dg_documento'] ?></td>
					<td><?php echo $db->dateLocalFormat($p['df_vencimiento']) ?></td>
				<?php endif; ?>
				
				<?php if($propuesta['dm_requiere_banco'] == 1): ?>
					<td><?php echo $p['dg_banco'] ?></td>
				<?php endif; ?>
				
					<td><?php echo $db->dateLocalFormat($p['df_emision']) ?></td>
					<td><?php echo $p['dg_usuario'] ?></td>
					<td align="right"><?php echo moneda_local($p['dq_monto']) ?></td>
				
				</tr>
			<?php $dq_pagar -= $p['dq_monto'] ?>
			<?php endforeach; ?>
			</tbody>
		</table>
	<?php endif; ?>
	<?php if($dq_pagar > 0): ?>
		<?php 
		$form->Start('sites/finanzas/pagos/proc/crear_documento_pago.php','src_admin_pago');
		$form->Header('Indique los datos para crear un nuevo proceso de pago sobre la propuesta');
		
			$form->Section();
				$form->Text('Monto','dq_monto',1,255,$dq_pagar);
				$form->Date('Fecha de emisión','df_emision',1,0);
			$form->EndSection();
			
			$form->Section();
			
			if($propuesta['dm_requiere_documento'] == 1):
				$form->Text('Número de documento','dg_documento',1);
				$form->Date('Fecha de vencimiento','df_vencimiento',1);
			endif;
			
			if($propuesta['dm_requiere_banco'] == 1):
				$form->Listado('Banco','dc_banco','tb_banco b
				JOIN tb_cuentas_tipo_movimiento tm ON tm.dc_cuenta_contable = b.dc_cuenta_contable AND tm.dc_tipo_movimiento = '.$propuesta['dc_tipo_movimiento'],
				array('b.dc_banco','b.dg_banco'),1,$propuesta['dc_banco_default']);
			endif;
			
			$form->EndSection();
		
		$form->Hidden('dc_propuesta',$dc_propuesta);
		$form->End('Crear','addbtn');
		?>
	<?php endif; ?>
</div>