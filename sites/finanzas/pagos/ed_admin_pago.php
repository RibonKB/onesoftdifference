<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_pago = intval($_POST['id']);

$pago = $db->select('tb_pago_propuesta',
			'dc_propuesta,dc_banco,dg_documento,df_vencimiento,dq_monto,df_emision',
			"dc_pago = {$dc_pago}");
$pago = array_shift($pago);			

if($pago == NULL){
	$error_man->showWarning('El detalle de pago no fue encontrado, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

$propuesta = $db->select('tb_propuesta_pago p
JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago',
'p.dq_propuesta, m.dm_requiere_banco, m.dm_requiere_documento, m.dc_tipo_movimiento',
"p.dc_propuesta = {$pago['dc_propuesta']} AND p.dc_empresa = {$empresa}");
$propuesta = array_shift($propuesta);

if($propuesta == NULL){
	$error_man->showWarning('La propuesta de pago del pago no fue encontrada o no se permiten detalles de pago en ella, compruebe los datos de entrada');
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);
?>
<div class="secc_bar">
	Administración de pago de propuesta <b><?php echo $propuesta['dq_propuesta'] ?></b>
</div>
<div class="panes">
<?php 
	$form->Start('sites/finanzas/pagos/proc/editar_documento_pago.php','ed_admin_pago');
	$form->Header('Indique los datos nuevos para el detalle de pago');
	
		$form->Section();
			$form->Text('Monto','dq_monto',1,255,$pago['dq_monto']);
			$form->Date('Fecha de emisión','df_emision',1,$pago['df_emision']);
		$form->EndSection();
		
		$form->Section();
		
		if($propuesta['dm_requiere_documento'] == 1):
			$form->Text('Número de documento','dg_documento',1,Form::DEFAULT_TEXT_LENGTH,$pago['dg_documento']);
			$form->Date('Fecha de vencimiento','df_vencimiento',1,$pago['df_vencimiento']);
		endif;
		
		if($propuesta['dm_requiere_banco'] == 1):
			$form->Listado('Banco','dc_banco','tb_banco b
			JOIN tb_cuentas_tipo_movimiento tm ON tm.dc_cuenta_contable = b.dc_cuenta_contable AND tm.dc_tipo_movimiento = '.$propuesta['dc_tipo_movimiento'],
			array('b.dc_banco','b.dg_banco'),1,$pago['dc_banco']);
		endif;
		
		$form->EndSection();
	
	$form->Hidden('dc_pago',$dc_pago);
	$form->Hidden('dc_propuesta',$pago['dc_propuesta']);
	$form->End('Editar','editbtn');
?>
</div>