<?php

define('MAIN',1);
require_once("../../../inc/init.php");

$dc_proveedor = intval($_POST['dc_proveedor']);

$proveedor = $db->prepare($db->select('tb_proveedor','dg_razon, dq_saldo_favor',"dc_proveedor = ? AND dc_empresa = ?"));
$proveedor->bindValue(1,$dc_proveedor,PDO::PARAM_INT);
$proveedor->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($proveedor);
$proveedor = $proveedor->fetch(PDO::FETCH_OBJ);

if($proveedor === false){
	$error_man->showWarning('El proveedor es inválido, compruebe los valores de entrada');
	exit;
}

if($proveedor->dq_saldo_favor <= 0){
	$error_man->showAviso('El proveedor no posee saldos a su favor');
	exit;
}


//Saldos a favor de facturas
$facturas_saldo = $db->prepare($db->select('tb_factura_compra','dc_factura, dq_factura, dq_folio, df_emision, dq_saldo_favor','dc_proveedor = ? AND dq_saldo_favor > 0'));
$facturas_saldo->bindValue(1,$dc_proveedor,PDO::PARAM_INT);
$db->stExec($facturas_saldo);
$facturas_saldo = $facturas_saldo->fetchAll(PDO::FETCH_OBJ);

//Saldos a favor de notas de crédito
$nota_credito_saldo = $db->prepare(
						$db->select(
							'tb_nota_credito_proveedor',
							'dc_nota_credito, dq_nota_credito, dq_folio, df_emision, dq_total - dq_saldado dq_saldo_favor',
							'dc_proveedor = ? AND dc_empresa = ? AND dq_total > dq_saldado AND dm_nula = 0'
						)
					);
$nota_credito_saldo->bindValue(1,$dc_proveedor,PDO::PARAM_INT);
$nota_credito_saldo->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_credito_saldo);
$nota_credito_saldo = $nota_credito_saldo->fetchAll(PDO::FETCH_OBJ);

//Saldos a favor de ordenes de compra
$orden_compra_saldo = $db->prepare(
						$db->select(
							'tb_orden_compra',
							'dc_orden_compra, dq_orden_compra, df_fecha_emision, dq_monto_liberado dq_saldo_favor',
							'dc_proveedor = ? AND dc_empresa = ? AND dq_monto_liberado > 0'
						)
					);
$orden_compra_saldo->bindValue(1,$dc_proveedor,PDO::PARAM_INT);
$orden_compra_saldo->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($orden_compra_saldo);
$orden_compra_saldo = $orden_compra_saldo->fetchAll(PDO::FETCH_OBJ);

?>
<div class="secc_bar">
	Saldos a favor <b><?php echo $proveedor->dg_razon ?></b>
</div>
<div class="panes">
	<table class="tab" width="100%">
	<caption>Saldos a Favor Facturas de Compra</caption>
	<thead>
		<tr>
			<th>&nbsp;</th>
			<th>Documento</th>
			<th>Número</th>
			<th>Folio</th>
			<th>Fecha de emisión</th>
			<th>Saldo</th>
		</tr>
	</thead>
	<tbody>
	<?php
		foreach($facturas_saldo as $f):
		if(isset($_POST['dc_factura_favor']) && in_array($f->dc_factura,$_POST['dc_factura_favor'])){
			$checked = 'checked="checked"';
		}else{
			$checked = '';
		}
	?>
		<tr>
			<td>
				<input type="checkbox" class="dc_factura_favor" value="<?php echo $f->dc_factura ?>" <?php echo $checked ?> />
				<input type="hidden" class="dq_saldo_favor_disponible" value="<?php echo round($f->dq_saldo_favor,$empresa_conf['dn_decimales_local']) ?>" />
			</td>
			<td>Factura Compra</td>
			<td><?php echo $f->dq_factura ?></td>
			<td><?php echo $f->dq_folio ?></td>
			<td align="center"><?php echo $db->dateLocalFormat($f->df_emision) ?></td>
			<td align="right"><?php echo moneda_local($f->dq_saldo_favor) ?></td>
		</tr>
	<?php endforeach; ?>
	
	<?php
		foreach($nota_credito_saldo as $nc):
		if(isset($_POST['dc_nota_credito_favor']) && in_array($nc->dc_nota_credito,$_POST['dc_nota_credito_favor'])){
			$checked = 'checked="checked"';
		}else{
			$checked = '';
		}
	?>
		<tr>
			<td>
				<input type="checkbox" class="dc_nota_credito_favor" value="<?php echo $nc->dc_nota_credito ?>" <?php echo $checked ?> />
				<input type="hidden" class="dq_saldo_favor_disponible" value="<?php echo round($nc->dq_saldo_favor,$empresa_conf['dn_decimales_local']) ?>" />
			</td>
			<td>Nota Crédito</td>
			<td><?php echo $nc->dq_nota_credito ?></td>
			<td><?php echo $nc->dq_folio ?></td>
			<td align="center"><?php echo $db->dateLocalFormat($nc->df_emision) ?></td>
			<td align="right"><?php echo moneda_local($nc->dq_saldo_favor) ?></td>
		</tr>
	<?php endforeach; ?>
	
	<?php
		foreach($orden_compra_saldo as $oc):
		if(isset($_POST['dc_orden_compra_favor']) && in_array($oc->dc_orden_compra,$_POST['dc_orden_compra_favor'])){
			$checked = 'checked="checked"';
		}else{
			$checked = '';
		}
	?>
		<tr>
			<td>
				<input type="checkbox" class="dc_orden_compra_favor" value="<?php echo $oc->dc_orden_compra ?>" <?php echo $checked ?> />
				<input type="hidden" class="dq_saldo_favor_disponible" value="<?php echo round($oc->dq_saldo_favor,$empresa_conf['dn_decimales_local']) ?>" />
			</td>
			<td>Orden de Compra</td>
			<td><?php echo $oc->dq_orden_compra ?></td>
			<td>-</td>
			<td align="center"><?php echo $db->dateLocalFormat($oc->df_fecha_emision) ?></td>
			<td align="right"><?php echo moneda_local($oc->dq_saldo_favor) ?></td>
		</tr>
	<?php endforeach; ?>
	
	</tbody>
	</table>
<br />
<div class="center">
	<button class="addbtn" id="add_saldos_button">Agregar Saldos</button>
</div>
</div>
<script type="text/javascript" src="jscripts/sites/finanzas/pagos/src_saldo_favor.js?v=1_4b"></script>