<?php
define("MAIN",1);
require_once("../../../inc/init.php");
$db->query("SET NAMES 'latin1'");

if(file_exists("print_version/modo{$empresa}.php")){
	include_once("print_version/modo{$empresa}.php");
}else{
	include_once("print_version/modo0.php");
}

$dc_propuesta = intval($_GET['id']);
if(!$dc_propuesta){
	$error_man->showWarning('Propuesta inválida.');
	exit;
}

//Información de cabecera de la propuesta de pago
$data = $db->prepare($db->select('tb_propuesta_pago p
LEFT JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago
JOIN tb_proveedor pr ON pr.dc_proveedor = p.dc_proveedor',
'p.dq_propuesta, p.dg_propuesta, p.dg_comentario, p.df_propuesta, m.dg_medio_pago, pr.dg_razon, pr.dg_rut',
'p.dc_propuesta = ? AND p.dc_empresa = ?'));
$data->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$data->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($data);

$data = $data->fetch(PDO::FETCH_OBJ);

if($data === false){
	$error_man->showWarning('No se ha encontrado la propuesta de pago seleccionada');
	exit;
}

//Datos de detalle de propuesta
$detalle = $db->prepare($db->select('tb_propuesta_pago_detalle d
JOIN tb_factura_compra fc ON fc.dc_factura = d.dc_factura
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fc.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fc.dc_orden_servicio',
'd.dq_monto, fc.dq_factura, fc.dq_folio, DATE_FORMAT(fc.df_emision,"%d/%m/%Y") df_emision, nv.dq_nota_venta, os.dq_orden_servicio',
'd.dc_propuesta = ?'));
$detalle->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$db->stExec($detalle);

//Datos de administraciónd e pagos
$pagos = $db->prepare($db->select('tb_pago_propuesta p
LEFT JOIN tb_banco b ON b.dc_banco = p.dc_banco
JOIN tb_usuario u ON u.dc_usuario = p.dc_usuario_creacion',
'p.dg_documento, DATE_FORMAT(p.df_vencimiento,"%d/%m/%Y") df_vencimiento, p.dq_monto, DATE_FORMAT(p.df_emision,"%d/%m/%Y") df_emision, b.dg_banco, u.dg_usuario',
"p.dc_propuesta = ? AND p.dc_empresa = ?"));
$pagos->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$pagos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($pagos);
$pagos = $pagos->fetchAll(PDO::FETCH_OBJ);

$pdf = new PDF($data);
$pdf->setDetail($detalle);
$pdf->setPayDetail($pagos);
$pdf->Output();
?>