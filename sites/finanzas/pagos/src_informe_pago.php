<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Informe de pago de facturas</div>
<div id="main_cont"><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/finanzas/pagos/proc/src_informe_pago.php','src_informe');
$form->Header('Indique los filtros para ver los resultados del informe de pagos');

echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50%">');
$form->ListadoMultiple('Proveedor','inf_proveedores','tb_proveedor',array('dc_proveedor','dg_razon'));
echo('</td><td>');
$form->ListadoMultiple('Tipo de proveedor','inf_tipo_proveedor','tb_tipo_proveedor',array('dc_tipo_proveedor','dg_tipo_proveedor'));
echo('</td></tr><tr><td>');
$form->Text('Días para campo "por vencer"','inf_por_vencer',1,2,7);
echo('<br /></td><td></td></tr><tr><td>');
$form->ListadoMultiple('Tipo de operación (factura)','inf_tipo_operacion','tb_tipo_operacion',array('dc_tipo_operacion','dg_tipo_operacion'));
echo('</td><td></td></tr></table>');

$form->End('Ejecutar');
?>
</div></div>
<script type="text/javascript">
$('#inf_proveedores,#inf_tipo_proveedor,#inf_tipo_operacion').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>