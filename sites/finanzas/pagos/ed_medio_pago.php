<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_medio_pago = intval($_POST['id']);

$medio_pago = $db->prepare($db->select('tb_medio_pago_proveedor',
	'dg_medio_pago,dc_tipo_movimiento , dm_requiere_banco,
	 dm_asiento_intermedio,dm_requiere_documento,dc_banco_default,
	 dc_cuenta_contable_intermedia','dc_medio_pago = ? AND dc_empresa = ?'));
$medio_pago->bindValue(1,$dc_medio_pago,PDO::PARAM_INT);
$medio_pago->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($medio_pago);

$medio_pago = $medio_pago->fetch(PDO::FETCH_OBJ);

if($medio_pago === false){
	$error_man->showWarning('El tipo de medio de pago seleccionado no fue encontrado');
	exit;
}
?>

<div class="secc_bar">  
	Editar medios de pago 
</div>
<div class="pannes">
<?php $form->Start('sites/finanzas/pagos/proc/editar_medio_pago.php', 'ed_medio_pago') ?>
<?php $form->Header('<b>Ingrese el nombre para actualizar el medio de pago</b><br />Los campos marcados con [*] son obligatorios') ?>

		<?php $form->Section(); 
         	$form->Text('Nombre','dg_medio_pago_ed',true,Form::DEFAULT_TEXT_LENGTH,$medio_pago->dg_medio_pago);    
         	$form->DBSelect('Tipo movimiento contable','dc_tipo_movimiento_ed','tb_tipo_movimiento',array('dc_tipo_movimiento',
			'dg_tipo_movimiento'),true,$medio_pago->dc_tipo_movimiento); 
		$form->EndSection();
		
		$form->Section();
			$form->Radiobox('Requiere Banco','dm_requiere_banco_ed',array('SI','NO'),$medio_pago->dm_requiere_banco==1?0:1);
			$form->Radiobox('Asiento Intermedio','dm_asiento_intermedio_ed',array('SI','NO'),$medio_pago->dm_asiento_intermedio==1?0:1);
			$form->Radiobox('Requiere Documento','dm_requiere_documento_ed',array('SI','NO'),$medio_pago->dm_requiere_documento==1?0:1);
		$form->EndSection();
		
		$form->Section();
			$form->DBSelect('Banco por defecto','dc_banco_default_ed','tb_banco',array('dc_banco','dg_banco'),$medio_pago->dc_banco_default);
			$form->DBSelect('Cuenta contable asiento intermedio','dc_cuenta_contable_intermedia_ed','tb_cuenta_contable',
			array('dc_cuenta_contable','dg_cuenta_contable'),true,$medio_pago->dc_cuenta_contable_intermedia);
		$form->EndSection();
		?>
        
             
        <?php $form->Hidden('dc_medio_pago_ed',$dc_medio_pago) ?>
    <?php $form->End('Editar','editbtn') ?>
    
    
</div>
