<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/Factory.class.php");

$dc_propuesta = intval($_POST['id']);

$propuesta = $db->select('tb_propuesta_pago p
LEFT JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago
JOIN tb_proveedor pr ON pr.dc_proveedor = p.dc_proveedor',
'p.dq_propuesta, p.dg_propuesta, p.dm_estado, m.dg_medio_pago, m.dm_requiere_banco, m.dm_asiento_intermedio, m.dm_requiere_documento, m.dc_tipo_movimiento',
"p.dc_propuesta = {$dc_propuesta} AND p.dc_empresa = {$empresa}");
$propuesta = array_shift($propuesta);

if($propuesta === NULL){
	$error_man->showWarning('No se encontró la propuesta de pago seleccionada, compruebe los datos de entrada o consulte con un administrador.');
	exit;
}

if($propuesta['dm_estado'] === 'L'){
	$error_man->showWarning('No se puede agregar documentación a las propuestas de pago sin validar');
	exit;
}

//Detalles de administración de pagos
$pagos = $db->select('tb_pago_propuesta p
LEFT JOIN tb_banco b ON b.dc_banco = p.dc_banco
JOIN tb_usuario u ON u.dc_usuario = p.dc_usuario_creacion',
'p.dg_documento, p.df_vencimiento, p.dq_monto, p.df_emision, b.dg_banco, u.dg_usuario',
"p.dc_propuesta = {$dc_propuesta} AND p.dc_empresa = {$empresa}");

//Datos de detalle de propuesta
$detalle = $db->select(
		'tb_propuesta_pago_detalle pd
		JOIN tb_factura_compra fc ON fc.dc_factura = pd.dc_factura
		LEFT JOIN tb_nota_venta nv ON fc.dc_nota_venta = nv.dc_nota_venta
		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fc.dc_orden_servicio
		LEFT JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
		LEFT JOIN tb_factura_venta fv ON fv.dc_nota_venta = nv.dc_nota_venta',
		'pd.dq_monto, fc.dq_factura, fc.dq_folio, nv.dq_nota_venta, os.dq_orden_servicio, fc.df_emision df_emision_factura_compra,
		fc.df_vencimiento df_vencimiento_factura_compra, cl.dg_razon, DATEDIFF(NOW(),fc.df_vencimiento) dc_dias_vencimiento,
		GROUP_CONCAT(fv.dq_factura,"(<b>",fv.dq_folio,"</b>)" SEPARATOR ",<br />") dq_factura_venta',
		'pd.dc_propuesta = '.$dc_propuesta,array('group_by' => 'pd.dc_factura'));

require_once("../../../inc/form-class.php");
$form = new Form($empresa);
?>
<div class="secc_bar">
	Confirmar propuesta <b><?php echo $propuesta['dq_propuesta'] ?></b>
</div>
<div class="panes">
	<?php if(!count($pagos)): ?>
		<div class="alert">
			No se han realizados gestiones de pago sobre esta propuesta
		</div>
	<?php else: ?>
		<table class="tab" width="100%">
			<thead>
				<tr>
				<?php if($propuesta['dm_requiere_documento'] == 1): ?>
					<th>Documento</th>
					<th>Fecha de vencimiento</th>
				<?php endif; ?>
				
				<?php if($propuesta['dm_requiere_banco'] == 1): ?>
					<th>Banco</th>
				<?php endif; ?>
					
					<th>Fecha emisión</th>
					<th>Responsable</th>
					<th>Monto</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($pagos as $p): ?>
				<tr>
				<?php if($propuesta['dm_requiere_documento'] == 1): ?>
					<td><?php echo $p['dg_documento'] ?></td>
					<td><?php echo $db->dateLocalFormat($p['df_vencimiento']) ?></td>
				<?php endif; ?>
				
				<?php if($propuesta['dm_requiere_banco'] == 1): ?>
					<td><?php echo $p['dg_banco'] ?></td>
				<?php endif; ?>
				
					<td><?php echo $db->dateLocalFormat($p['df_emision']) ?></td>
					<td><?php echo $p['dg_usuario'] ?></td>
					<td align="right"><?php echo moneda_local($p['dq_monto']) ?></td>
				
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	<?php endif; ?>
	<div class="center">
	<?php 
		$form->Start(Factory::buildUrl('GestionPropuestaPago', 'finanzas', 'confirmarPropuestaPago', 'pagos'),'src_confirm_propuesta');
		$form->Header('Indique la acción que desea realizar');
		$form->Section();
			$form->Date('Fecha contable','df_fecha_contable',1,0);
		$form->EndSection();
		if($propuesta['dm_requiere_banco'] == 0 && $propuesta['dm_requiere_documento'] == 0 && $propuesta['dc_tipo_movimiento'] != 0){
			$form->Section();
				
				$form->Listado('Cuenta Contable','dc_cuenta_efectivo',"tb_cuenta_contable cc
						JOIN tb_cuentas_tipo_movimiento tm ON tm.dc_cuenta_contable = cc.dc_cuenta_contable AND tm.dc_tipo_movimiento = {$propuesta['dc_tipo_movimiento']}",
						array('cc.dc_cuenta_contable','cc.dg_cuenta_contable'),1);
				
			$form->EndSection();
		}
		echo('<br class="clear" /><br />');
		$form->Button('Ver asientos contables a realizar','id="ver_asientos_confirmacion"','searchbtn');
		echo('<br />');
		$form->Hidden('id',$dc_propuesta);
		$form->End('Confirmar','checkbtn');
	?>
	</div>
	<div id="asientos_confirmacion"></div>
	<table class="tab" width="100%" id="propuesta_detail_data">
	<thead>
		<tr>
			<th>FC</th>
			<th>Folio</th>
			<th>F. emisión (FC)</th>
			<th>F. vencimiento</th>
			<th title="Días vencimiento">DV</th>
			<th>Cliente</th>
			<th>NV</th>
			<th>OS</th>
			<th>FV</th>
			<th>Monto a Pagar</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($detalle as $d): ?>
		<tr>
			<td><?php echo $d['dq_factura'] ?></td>
			<td><b><?php echo $d['dq_folio'] ?></b></td>
			<td><?php echo $db->dateLocalFormat($d['df_emision_factura_compra']) ?></td>
			<td><?php echo $db->dateLocalFormat($d['df_vencimiento_factura_compra']) ?></td>
			<td align="right"><?php echo $d['dc_dias_vencimiento'] ?></td>
			<td><?php echo $d['dg_razon'] ?></td>
			<td><?php echo $d['dq_nota_venta'] ?></td>
			<td><?php echo $d['dq_orden_servicio'] ?></td>
			<td><?php echo $d['dq_factura_venta'] ?></td>
			<td align="right"><?php echo moneda_local($d['dq_monto']) ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
</div>
<script type="text/javascript">
	js_data.initConfirmacion(<?php echo $dc_propuesta ?>);
	window.setTimeout(function(){
		$('#propuesta_detail_data').tableAdjust(10);
	},100);
</script>