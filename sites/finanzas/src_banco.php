<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$list = $db->select(
	"tb_banco b
	LEFT JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = b.dc_cuenta_contable
	LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = b.dc_tipo_cambio",
	"b.dc_banco, b.dg_banco, b.df_creacion, b.dg_cuenta_corriente, b.dg_ejecutivo, b.dg_telefono, b.dg_email, b.dg_sucursal,
	cc.dg_codigo,cc.dg_cuenta_contable, tc.dg_tipo_cambio",
	"b.dc_empresa={$empresa} AND b.dm_activo='1'",
	array("order_by" => "dg_banco")
);

include_once("../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">
	Bancos
</div>
<div id="main_cont">
<div class="panes">
	<?php
		$form->Start("sites/mantenedores/contabilidad/proc/crear_banco.php","cr_banco");
		$form->Header("<b>Complete los datos requeridos para crear un nuevo banco</b>
						<br />Los campos marcados con [*] son obligatorios
						<br />Puede consultar los que ya están agregados.");
		$form->Section();
			$form->Text("Nombre","banco_name",1);
			$form->Text("Número cuenta corriente",'banco_cuenta_corriente');
			$form->Listado('Tipo de cambio','banco_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'),1);
			$form->Listado('Cuenta contable','banco_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),1);
		$form->EndSection();
		$form->Section();
			$form->Text('Ejecutivo','banco_ejecutivo');
			$form->Text('Telefono','banco_telefono');
			$form->Text('E-mail','banco_email');
			$form->Text('Sucursal','banco_sucursal');
		$form->EndSection();
		$form->End("Crear","addbtn");
	?>
	
	<table class="tab" width="100%" id="tbanco">
	<caption>Bancos</caption>
	<thead>
		<tr>
			<th>Banco</th>
			<th>Fecha de creación</th>
			<th>Cta. cte.</th>
			<th>TC</th>
			<th>Cta. Contable</th>
			<th>Ejecutivo</th>
			<th>Telefono</th>
			<th>E-Mail</th>
			<th>Sucursal</th>
			<th>Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	<?php foreach($list as $l): ?>
		<tr id='item<?php echo $l['dc_banco'] ?>'>
			<td><?php echo $l['dg_banco'] ?></td>
			<td><?php echo $db->dateTimeLocalFormat($l['df_creacion']) ?></td>
			<td><?php echo $l['dg_cuenta_corriente'] ?></td>
			<td><?php echo $l['dg_tipo_cambio'] ?></td>
			<td><b><?php echo $l['dg_codigo'] ?></b> - <?php echo $l['dg_cuenta_contable'] ?></td>
			<td><?php echo $l['dg_ejecutivo'] ?></td>
			<td><?php echo $l['dg_telefono'] ?></td>
			<td><?php echo $l['dg_email'] ?></td>
			<td><?php echo $l['dg_sucursal'] ?></td>
			<td>
				<a href='sites/finanzas/ed_banco.php?id=<?php echo $l['dc_banco'] ?>' class='loadOnOverlay'>
					<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/finanzas/del_banco.php?id=<?php echo $l['dc_banco'] ?>' class='loadOnOverlay'>
					<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody></table>
</div>
</div>
<script type="text/javascript">
	$('#tbanco').tableExport().tableAdjust(10);
</script>