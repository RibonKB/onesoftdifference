<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$funcionario =& $_POST['func_id'];

if(count($db->select("tb_funcionario","dc_funcionario","dc_funcionario={$funcionario} AND dm_activo = '0'"))){
	$error_man->showAviso("El funcionario ya ha sido eliminado");
}
else{
	$db->update("tb_funcionario",array("dm_activo" => '0'),"dc_funcionario = {$funcionario}");
	$db->update("tb_usuario",array("dm_activo" => '0'),"dc_funcionario = {$funcionario}");
	
	$error_man->showConfirm("El funcionario ha sido eliminado correctamente<br /><strong>Los usuarios asignados al funcionario también fueron eliminados</strong>");
	
}

?>