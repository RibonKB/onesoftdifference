<?php
	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	switch($_FILES['fc_foto']['error']){
		case 1:
		case 2: $error_man->showWarning("El tamaño del archivo supera el máximo permitido.");
				exit();
				break;
		case 3: $error_man->showWarning("Se ha cancelado la transferencia del archivo.");
				exit();
				break;
	}
	
	$tipos = array('image/gif','image/pjpeg','image/jpeg','image/jpg','image/x-png','image/png');
	
	if($_FILES['fc_foto']['size']){
		if(!in_array($_FILES['fc_foto']['type'],$tipos)){
			$error_man->showWarning("El archivo especificado no es una imagen válida.<br />Los formatos soportados son gif, jpeg y png");
			exit();
		}
	}
	
	if(count($db->select("tb_funcionario","dc_funcionario","dg_rut = '{$_POST['func_rut']}' AND dc_empresa = {$empresa}"))){
		$error_man->showWarning("Ya existe un funcionario utilizando el <strong>rut</strong> especificado,<br />Pruebe con otro o consulte al administrador");
		exit();
	}
	
	$f_nac = explode("/",$_POST['func_nacimiento']);
	$f_nac = count($f_nac)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$f_nac[1],$f_nac[0],$f_nac[2]))."'":"null";
	
	$f_ini_contr = explode("/",$_POST['func_ini_contr']);
	$f_ini_contr = count($f_ini_contr)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$f_ini_contr[1],$f_ini_contr[0],$f_ini_contr[2]))."'":"null";
	
	$f_fin_contr = explode("/",$_POST['func_fin_contr']);
	$f_fin_contr = count($f_fin_contr)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$f_fin_contr[1],$f_fin_contr[0],$f_fin_contr[2]))."'":"null";
	
	if($_POST['func_sueldo'] == ''){
		$f_sueldo = 0;
	}else{
		$f_sueldo = str_replace(",",".",$_POST['func_sueldo']);
		if(!is_numeric($f_sueldo)){
			$error_man->showWarning("El formato del sueldo es incorrecto");
			exit();
		}
	}
	
	if($_FILES['fc_foto']['size']){
		$imagen = mysql_real_escape_string(fread(fopen($_FILES['fc_foto']['tmp_name'],'r'),$_FILES['fc_foto']['size']));
	}else{
		$imagen = "";
	}
	
	$f_id = $db->insert("tb_funcionario",
	array(
	"dg_rut" => $_POST['func_rut'],
	"dg_nombres" => $_POST['func_nombres'],
	"dg_ap_paterno" => $_POST['func_ap_paterno'],
	"dg_ap_materno" => $_POST['func_ap_materno'],
	"df_nacimiento" => $f_nac,
	"dg_anexo" => $_POST['func_anexo'],
	"dm_sexo" => $_POST['func_sexo'],
	"dc_ceco" => $_POST['func_ceco'],
	"dc_tipo_contrato" => $_POST['func_t_contrato'],
	"df_inicio_contrato" => $f_ini_contr,
	"df_fin_contrato" => $f_fin_contr,
	"dc_isapre" => $_POST['func_salud'],
	"dc_afp" => $_POST['func_afp'],
	"dq_sueldo_bruto" => $f_sueldo,
	"dc_empresa" => $empresa,
	"dr_foto_usuario" => "'{$imagen}'",
	"dg_tipo_foto" => $_FILES['fc_foto']['type']
	));
	
	$error_man->showConfirm("Se ha creado el funcionario correctamente.");
	
?>