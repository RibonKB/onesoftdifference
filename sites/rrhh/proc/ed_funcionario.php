<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de funcionarios
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/rrhh/cr_funcionario.php" class="loader">Creación</a></li>
		<li class="oo_active"><a href="#">Edición</a></li>
		<li><a href="sites/rrhh/del_funcionario.php" class="loader">Eliminación</a></li>
	</ul>
</div>

<?php

$datos = $db->select("tb_funcionario","dc_funcionario,dg_rut,dg_nombres,dg_ap_paterno,dg_ap_materno,df_nacimiento,dm_sexo,dc_ceco,
dc_isapre,dc_afp,dc_tipo_contrato,df_inicio_contrato,df_fin_contrato,dq_sueldo_bruto,dg_anexo",
	"dg_rut = '{$_POST['fc_rut']}' AND dc_empresa = {$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showErrorRedirect("El funcionario indicado no existe","sites/rrhh/ed_funcionario.php");
}
$datos = $datos[0];

?>

<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Ingrese los datos actualizados del funcionario</strong><br />(Los campos marcados con [*] son obligatorios)");
	$form->Start("sites/rrhh/proc/editar_funcionario.php","ed_funcionario","form","method='post' enctype='multipart/form-data'");
	$form->Section();
	$form->Text("RUT","func_rut",1,255,$datos['dg_rut'],"inputrut");
	$form->Text("Apellido Paterno","func_ap_paterno",1,255,$datos['dg_ap_paterno']);
	$form->Date("Fecha nacimiento","func_nacimiento",1,$datos['df_nacimiento']);
	$form->EndSection();
	$form->Section();
	$form->Text("Nombres","func_nombres",1,255,$datos['dg_nombres']);
	$form->Text("Apellido Materno","func_ap_materno",1,255,$datos['dg_ap_materno']);
	$form->Radiobox("Sexo","func_sexo",array("Hombre","Mujer"),$datos['dm_sexo']);
	$form->EndSection();
	$form->Group();
	$form->Section();
	$form->Listado("Centro costo","func_ceco","tb_ceco",array("dc_ceco","dg_ceco"),1,$datos['dc_ceco']);
	$form->Date("Inicio contrato","func_ini_contr",1,$datos['df_inicio_contrato']);
	$form->Listado("Salud","func_salud","tb_isapre",array("dc_isapre","dg_isapre"),1,$datos['dc_isapre']);
	$form->Text("Sueldo bruto (\$)","func_sueldo",0,10,$datos['dq_sueldo_bruto']);
	$form->Text('Anexo','func_anexo',0,20,$datos['dg_anexo']);
	$form->EndSection();
	$form->Section();
	$form->Listado("Tipo de contrato","func_t_contrato","tb_tipo_contrato",array("dc_tipo_contrato","dg_tipo_contrato"),1,$datos['dc_tipo_contrato']);
	$form->Date("Término contrato","func_fin_contr",0,$datos['df_fin_contrato']);
	$form->Listado("AFP","func_afp","tb_afp",array("dc_afp","dg_afp"),1,$datos['dc_afp']);
	$form->File("Foto","fc_foto");
	$form->EndSection();
	$form->Hidden("id_func",$datos['dc_funcionario']);
	$form->End("Editar","editbtn");
?>
<iframe name="edfuncionario_fres" id="edfuncionario_fres" style="display:none;"></iframe>
</div>
</div>

<script type="text/javascript">
$("#ed_funcionario").submit(function(e){
	if(validarForm("#ed_funcionario")){	
		disableForm("#ed_funcionario");
	}else{
		e.preventDefault();
	}
});
$("#edfuncionario_fres").load(function(){
	res = frames['edfuncionario_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#ed_funcionario_res").html(res);
	hide_loader();
});
$("#ed_funcionario").attr("target","edfuncionario_fres");
	
</script>