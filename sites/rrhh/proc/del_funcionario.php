<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de funcionarios
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/rrhh/cr_funcionario.php" class="loader">Creación</a></li>
		<li><a href="sites/rrhh/ed_funcionario.php" class="loader">Edición</a></li>
		<li class="oo_active"><a href="#">Eliminación</a></li>
	</ul>
</div>

<?php

$datos = $db->select("(SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa} AND dg_rut = '{$_POST['fc_rut']}' AND dm_activo='1') f
LEFT JOIN (SELECT * FROM tb_ceco WHERE dc_empresa={$empresa}) cc ON f.dc_ceco = cc.dc_ceco",	
"dc_funcionario,dg_rut,dg_nombres,dg_ap_paterno,dg_ap_materno,df_nacimiento,dm_sexo,
dc_isapre,dc_afp,dc_tipo_contrato,df_inicio_contrato,df_fin_contrato,dq_sueldo_bruto,dg_ceco");

if(!count($datos)){
	$error_man->showErrorRedirect("El funcionario indicado no existe","sites/rrhh/del_funcionario.php");
}

$datos = $datos[0];

?>

<div id="main_cont">
	<div class="panes">
	<form action="sites/rrhh/proc/delete_funcionario.php" class="validar" id="del_func">
	<fieldset>
		<div class="title">Datos del funcionario</div>
		<div class="left">
		 <img src="images/avatar/foto_funcionario.php?fid=<?=$datos['dc_funcionario'] ?>" alt="<?=$datos['dg_nombres'] ?>" style="width:120px;border:1px solid #DDD;padding:3px;" />
		</div>
		
		<div class="left">
			<label>Nombre funcionario:<br /></label>
			<?="{$datos['dg_nombres']} {$datos['dg_ap_paterno']} {$datos['dg_ap_materno']}" ?>
			<br /><br />
			<label>Centro costo:<br /></label>
			<?=$datos['dg_ceco'] ?>
		</div>
		<hr class="clear" />
		<div align="center">
			¿Está seguro de querer eliminar el funcionario?<br />
				<input type="hidden" name="func_id" value="<?=$datos['dc_funcionario'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
		</div>
	</fieldset>
	</form>
	<div id="del_func_res"></div>
	</div>
</div>