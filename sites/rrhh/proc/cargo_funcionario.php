<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$modulo = intval($_POST['modulo']);
$ceco = $_POST['ceco'];


if(!isset($_POST['ceco'])){
	$error_man->showWarning('Seleccione módulo ceco');
	exit;
}

$db->start_transaction();

$delete = $db->prepare('DELETE FROM tb_cargo_funcionario WHERE dc_empresa = ? AND dc_modulo = ?');
$delete->bindValue(1,$empresa,PDO::PARAM_INT);
$delete->bindValue(2,$modulo,PDO::PARAM_INT);
$db->stExec($delete);

foreach($ceco as $cc){
	$db->doExec($db->insert('tb_cargo_funcionario',array(
	'dc_modulo'=>$modulo,
	'dc_ceco'=>$cc,
	'dc_empresa'=>$empresa
	)));
}

$db->commit();

$error_man->showConfirm('Se ha modificado correctamente');

?>