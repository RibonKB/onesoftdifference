<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of rrhh
 *
 * @author Eduardo
 */
class cargo_func  extends Factory {

    public function indexAction() {
        echo $this->getFormView($this->getTemplateURL('cargo_funcionario'));
    }

    private function datos() {
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_ceco', 'dc_ceco, dg_ceco', 'dc_empresa = ?'));
        $datos->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($datos);

        $datos = $datos->fetch(PDO::FETCH_OBJ);

        if ($datos === false) {
            $this->getErrorMan()->showWarning('El centro de costo no fue encontrado');
            exit;
        }
    }

}

?>
