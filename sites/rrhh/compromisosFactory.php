<?php

class compromisosFactory extends Factory {

    public $title='Compromisos';
    
    public function indexAction() {
        $this->IdentificarAutorizacion();
    }

    private function IdentificarAutorizacion() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $identifica = $db->prepare($db->select('tb_usuario_autoriza', 'dc_usuario', "dc_usuario={$this->getUserData()->dc_usuario}"
                )
        );

        $db->stExec($identifica);
        $identifica = $identifica->fetch(PDO::FETCH_OBJ);
        if ($identifica != false) {
            $mostrar = $this->getFormView($this->getTemplateURL('compromisosAutoriza'),
                                          array(
                                                'mis_compromisos'=>$this->compromisos_ver(),
                                                'evaluar'=>$this->compromisos_evaluar(),
                                                'en_espera'=>$this->compromisos_Autorizar(),
                                                'aprobados'=>$this->compromisos_aprobados(),
                                                'rechazados'=>$this->compromisos_rechazados(),
                                                'evaluado'=>$this->compromisos_evaluado_si(),
                                                'evaluadon'=>$this->compromisos_evalua_no(),
                                                'mis_compromisos_estado'=>$this->compromisos_ver_estado()
                                               )
                                         );
            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        } else {
            $mostrar = $this->getFormView($this->getTemplateURL('compromisosUsuario'),
                                          array(
                                                'mis_compromisos'=>$this->compromisos_ver()
                                          ));
            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        }
    }

    /**
     * 
     * Mustra los compromisos que debe evaluar
     * 
     * @return type
     * 
     */
    private function compromisos_evaluar() {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        $dc_user_o = $this->dc_funcionario($this->getUserData()->dc_usuario);
        $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario', 
                                              "com.dc_creador={$this->getUserData()->dc_usuario}
                                                    AND com.dc_usuario_funcionario=us.dc_usuario
                                                        AND us.dc_funcionario = fu.dc_funcionario
                                                            AND com.dc_cumple=1
                                                                AND com.dc_estado =2"
        ));

        $db->stExec($compromisos);
        while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
            if ($llena->dg_compromiso_empresa == null) {
                $llena->dg_compromiso_empresa = "─═☆☆═─";
            }
            $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple] = $llena->dc_estado;
        }
        return $arr1;
    }
    
    private function compromisos_evaluado_si() {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        $dc_user_o = $this->dc_funcionario($this->getUserData()->dc_usuario);
        $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario', 
                                              "com.dc_creador={$this->getUserData()->dc_usuario}
                                                    AND com.dc_usuario_funcionario=us.dc_usuario
                                                        AND us.dc_funcionario = fu.dc_funcionario
                                                            AND com.dc_cumple=2
                                                                AND com.dc_estado =2"
        ));

        $db->stExec($compromisos);
        while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
            if ($llena->dg_compromiso_empresa == null) {
                $llena->dg_compromiso_empresa = "─═☆☆═─";
            }
            $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple] = $llena->dc_estado;
        }
        return $arr1;
    }
    
    private function compromisos_evalua_no() {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        $dc_user_o = $this->dc_funcionario($this->getUserData()->dc_usuario);
        $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario', 
                                              "com.dc_creador={$this->getUserData()->dc_usuario}
                                                    AND com.dc_usuario_funcionario=us.dc_usuario
                                                        AND us.dc_funcionario = fu.dc_funcionario
                                                            AND com.dc_cumple=3
                                                                AND com.dc_estado =2"
        ));

        $db->stExec($compromisos);
        while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
            if ($llena->dg_compromiso_empresa == null) {
                $llena->dg_compromiso_empresa = "─═☆☆═─";
            }
            $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple] = $llena->dc_estado;
        }
        return $arr1;
    }

    /**
     * 
     * Muestra los compromisos que debe aprobar y/o rechazar
     * 
     * @return type
     */
    private function compromisos_Autorizar() {
        $arr1 = '';
        $db = $this->getConnection();

        $dc_user_o = $this->dc_user($this->getUserData()->dc_usuario);

        $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 
                                              'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,
                                               com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               com.df_creacion,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario', 
                                               "com.dc_creador IN ({$dc_user_o})
                                                   AND com.dc_estado=1
                                                        AND com.dc_usuario_funcionario=us.dc_usuario
                                                            AND us.dc_funcionario=fu.dc_funcionario"
        ));

        $db->stExec($compromisos);
        while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
            //aeleccionar al creador del compromiso
            $select_funcionario=$db->prepare($db->select('tb_funcionario fu JOIN tb_usuario us JOIN tb_compromisos com',
                                                        'CONCAT(fu.dg_nombres," ",
                                                                fu.dg_ap_paterno," ",
                                                                fu.dg_ap_materno) AS nombre_creador',
                                                        "com.df_creacion='{$llena->df_creacion}'
                                                             AND com.dc_creador=us.dc_usuario
                                                                     AND fu.dc_funcionario=us.dc_funcionario"));
              $db->stExec($select_funcionario);
              $creador=$select_funcionario->fetch(PDO::FETCH_OBJ);
            if ($llena->dg_compromiso_empresa == null) {
                $llena->dg_compromiso_empresa = "─═☆☆═─";
            }
            $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple][$llena->dc_estado][$llena->df_creacion] = $creador->nombre_creador;
        }
        return $arr1;
    }
    
    /**
     * 
     * Muestra los compromisos aprobados
     * 
     * @return type
     */
    private function compromisos_aprobados() {
        $arr1 = '';
        $db = $this->getConnection();

        $dc_user_o = $this->dc_user($this->getUserData()->dc_usuario);

        $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 
                                              'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               com.df_creacion,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario', 
                                              "com.dc_creador IN ({$dc_user_o})
                                                    AND us.dc_usuario=com.dc_usuario_funcionario
                                                        AND fu.dc_funcionario=us.dc_funcionario
                                                            AND com.dc_estado=2
                                                                AND (SELECT (MONTH( NOW( ) ) - MONTH( com.df_compromiso )))>=0
                                                                    AND (TIMESTAMPDIFF(YEAR,NOW(),com.df_compromiso))=0"
        ));

        $db->stExec($compromisos);
        while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
            //aeleccionar al creador del compromiso
            $select_funcionario=$db->prepare($db->select('tb_funcionario fu JOIN tb_usuario us JOIN tb_compromisos com',
                                                        'CONCAT(fu.dg_nombres," ",
                                                                fu.dg_ap_paterno," ",
                                                                fu.dg_ap_materno) AS nombre_creador',
                                                        "com.df_creacion='{$llena->df_creacion}'
                                                             AND com.dc_creador=us.dc_usuario
                                                                     AND fu.dc_funcionario=us.dc_funcionario"));
              $db->stExec($select_funcionario);
              $creador=$select_funcionario->fetch(PDO::FETCH_OBJ);
            if ($llena->dg_compromiso_empresa == null) {
                $llena->dg_compromiso_empresa = "─═☆☆═─";
            }
            $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple][$llena->dc_estado][$llena->df_creacion] = $creador->nombre_creador;
        }
        return $arr1;
    }
    
    /**
     * 
     * Muestra los compromisos rechazados
     * 
     * @return type
     */
    private function compromisos_rechazados() {
        $arr1 = '';
        $db = $this->getConnection();

        $dc_user_o = $this->dc_user($this->getUserData()->dc_usuario);

        $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 
                                              'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               com.df_creacion,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario', 
                                               "com.dc_creador IN ({$dc_user_o})
                                                    AND com.dc_usuario_funcionario=us.dc_usuario
                                                        AND us.dc_funcionario=fu.dc_funcionario
                                                            AND com.dc_estado=3
                                                                AND (SELECT (MONTH( NOW( ) ) - MONTH( com.df_compromiso )))>=0
                                                                    AND (TIMESTAMPDIFF(YEAR,NOW(),com.df_compromiso))=0"
        ));

        $db->stExec($compromisos);
        while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
            //aeleccionar al creador del compromiso
            $select_funcionario=$db->prepare($db->select('tb_funcionario fu JOIN tb_usuario us JOIN tb_compromisos com',
                                                        'CONCAT(fu.dg_nombres," ",
                                                                fu.dg_ap_paterno," ",
                                                                fu.dg_ap_materno) AS nombre_creador',
                                                        "com.df_creacion='{$llena->df_creacion}'
                                                             AND com.dc_creador=us.dc_usuario
                                                                     AND fu.dc_funcionario=us.dc_funcionario"));
              $db->stExec($select_funcionario);
              $creador=$select_funcionario->fetch(PDO::FETCH_OBJ);
            if ($llena->dg_compromiso_empresa == null) {
                $llena->dg_compromiso_empresa = "─═☆☆═─";
            }
            $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple][$llena->dc_estado][$llena->df_creacion] = $creador->nombre_creador;
        }
        return $arr1;
    }

    /**
     * 
     * Muestra los compromisos que tiene el usuario
     * 
     * @return type
     * 
     */
    private function compromisos_ver() {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
            $dc_user_func = $this->dc_funcionario($this->getUserData()->dc_usuario);
            $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario',
                                              "com.dc_usuario_funcionario={$this->getUserData()->dc_usuario}
                                                            AND com.dc_cumple IN (1,2,3)
                                                                AND com.dc_estado=2
                                                                    AND com.dm_activo=1
                                                                        AND us.dc_usuario=com.dc_usuario_funcionario
                                                                            AND fu.dc_funcionario=us.dc_funcionario
                                                                                   AND (SELECT (MONTH( NOW( ) ) - MONTH( com.df_compromiso )))>=0
                                                                                        AND (TIMESTAMPDIFF(YEAR,NOW(),com.df_compromiso))=0"
            ));

            $db->stExec($compromisos);
            while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
                if ($llena->dg_compromiso_empresa == null) {
                    $llena->dg_compromiso_empresa = "─═☆☆═─";
                }
                $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple] = $llena->dc_estado;
            }
        return $arr1;
    }

    
    private function compromisos_ver_estado() {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
            $dc_user_func = $this->dc_funcionario($this->getUserData()->dc_usuario);
            $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 
                                              'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               CONCAT(fu.dg_nombres," ",
                                                      fu.dg_ap_paterno," ",
                                                      fu.dg_ap_materno) AS nombre_funcionario',
                                              "com.dc_creador={$this->getUserData()->dc_usuario} 
                                                   AND com.dm_activo=1
                                                     
                                                          
                                                       AND us.dc_usuario=com.dc_usuario_funcionario
                                                           AND fu.dc_funcionario=us.dc_funcionario
                                                           AND fu.dc_cargo IN (SELECT dc_cargo FROM tb_usuario_autoriza WHERE dc_usuario={$this->getUserData()->dc_usuario} AND dm_activo=1)
                                              AND (SELECT (MONTH( NOW( ) ) - MONTH( com.df_compromiso )))>=-1 AND (SELECT (MONTH( NOW( ) ) - MONTH( com.df_compromiso )))<=0 
                                                                    AND (TIMESTAMPDIFF(YEAR,NOW(),com.df_compromiso))=0"
            ));

            $db->stExec($compromisos);
            while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
                if ($llena->dg_compromiso_empresa == null) {
                    $llena->dg_compromiso_empresa = "─═☆☆═─";
                }
                $arr1[$llena->dc_usuario][$llena->nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple] = $llena->dc_estado;
            }
        return $arr1;
    }
    
    
    /**
     * 
     * Entrega el id del funcionario
     * 
     * 
     * @param type $user_p
     * @return string
     * 
     */
    private function dc_funcionario($user_p) {
        $rr = '';
        $db = $this->getConnection();
        $select = $db->prepare($db->select('tb_usuario_autoriza us_a 
                                                JOIN tb_funcionario fu', 
                                           'fu.dc_funcionario', 
                                           "us_a.dc_usuario={$user_p}
                                               AND us_a.dm_activo=1 
                                                 AND fu.dc_cargo=us_a.dc_cargo"));
        $db->stExec($select);
        while ($llena = $select->fetch(PDO::FETCH_OBJ)) {
            if ($rr == '') {
                $rr = $llena->dc_funcionario;
            } else {
                $rr = $rr . "," . $llena->dc_funcionario;
            }
        }
        return $rr;
    }
    
    private function dc_user($user_p)
    {
        $rr = '';
        $db = $this->getConnection();
        $select = $db->prepare($db->select('tb_usuario_autoriza us_a 
                                                JOIN tb_funcionario fu
                                                    JOIN tb_usuario us', 
                                            'us.dc_usuario', 
                                            "us_a.dc_usuario={$user_p} 
                                                  AND us_a.dm_activo=1
                                                           AND fu.dc_cargo=us_a.dc_cargo
                                                                  AND us.dc_funcionario=fu.dc_funcionario"));
        $db->stExec($select);
        while ($llena = $select->fetch(PDO::FETCH_OBJ)) {
            if ($rr == '') {
                $rr = $llena->dc_usuario;
            } else {
                $rr = $rr . "," . $llena->dc_usuario;
            }
        }
        return $rr;
    }
    
    public function CompromisoCumpleSaveAction() {
        $r = self::getRequest();
        $db = $this->getConnection();

        for ($i = 1; $i <= $r->Cant_sino; $i++) {
            $nom = 'sino' . $i;
            if (isset($r->$nom)) {
                $spl = split('_', $r->$nom);
                $fecha = $spl[1];
                $dc_user = $spl[0];
                $sino = $spl[2];
                $up = $db->prepare($db->update('tb_compromisos', 
                                                array('dc_cumple' => "{$sino}"), 
                                                "dc_usuario_funcionario={$dc_user} 
                                                    AND df_compromiso='{$fecha}'"
                                               ));
                $db->stExec($up);
            }
        }
    }
    
    
    public function CompromisoEstadoSaveAction() {
        $r = self::getRequest();
        $db = $this->getConnection();

        for ($i = 1; $i <= $r->Cant_sino; $i++) {
            $nom = 'sino' . $i;
            if (isset($r->$nom)) {
                $spl = split('_', $r->$nom);
                $fecha = $spl[1];
                $dc_user = $spl[0];
                $sino = $spl[2];
                $up = $db->prepare($db->update('tb_compromisos', 
                                                array('dc_estado' => "{$sino}"), 
                                                "dc_usuario_funcionario={$dc_user} 
                                                    AND df_compromiso='{$fecha}'"
                                               ));
                $db->stExec($up);
            }
        }
    }
    
}

?>
