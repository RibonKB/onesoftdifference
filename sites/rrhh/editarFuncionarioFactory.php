<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of editarFuncionarioFactory
 *
 * @author Eduardo
 */
class editarFuncionarioFactory extends Factory {

    public function indexAction() {
        echo $this->getFormView($this->getTemplateURL('editar'));
    }

    public function FormEditarAction() {
        echo $this->getFormView($this->getTemplateURL('EditorFuncionario'), array('datos' => $this->llenaForm(), 'cargos' => $this->Select(),
            'afp' => $this->afp(), 'tipoCont' => $this->tcontrato(), 'ceco' => $this->ceco(), 'isapre' => $this->isapre()));
    }

    Public function edicionAction() {

        $this->verificarFotoTa();
        $this->verificarFotoTi();
        $this->cargoVsCeco();
        $this->insertDatos();
    }

    /**
     * Se encarga de entregar todos los datos
     * del funcionario el cual se editará
     * 
     * @return
     */
    private function llenaForm() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $r->datos = $db->prepare($db->select("tb_funcionario", "dc_funcionario,dg_rut,dg_nombres,dg_ap_paterno,dg_ap_materno,df_nacimiento,dm_sexo,dc_ceco,
dc_isapre,dc_afp,dc_tipo_contrato,df_inicio_contrato,df_fin_contrato,dq_sueldo_bruto,dg_anexo,dc_cargo,dm_evaluacion_ticket", "dg_rut = '{$r->fc_rut}' AND dc_empresa = {$this->getEmpresa()} AND dm_activo = '1'"));

        $db->stExec($r->datos);

        $r->datos = $r->datos->fetch(PDO::FETCH_OBJ);
        if ($r->datos == false) {
            $this->getErrorMan()->showErrorRedirect("El funcionario indicado no existe", Factory::buildActionUrl('index'));
        }
        return $r->datos;
    }

    /**
     * 
     * entrega todos los cargos diponibles
     * 
     * @return string
     */
    private function Select() {
        $ar1 = '';
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_cargos', '*', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $ar1[$crea->dc_cargo] = $crea->dg_cargo . " - " .$this->area($crea->dc_ceco);
        }
        return $ar1;
    }
    
    private function area($dc_ceco)
    {
        $db=$this->getConnection();
        $s=$db->prepare($db->select('tb_ceco','dg_ceco',"dc_ceco={$dc_ceco}"));
        $db->stExec($s);
        $s=$s->fetch(PDO::FETCH_OBJ);
        $s=$s->dg_ceco;
        return $s;
    }

    /**
     * Entrega todas las afp´s disponibles.
     * 
     * @return type
     */
    private function afp() {
        $ar1 = '';
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_afp', 'dc_afp,dg_afp',"dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $ar1[$crea->dc_afp] = $crea->dg_afp;
        }
        return $ar1;
    }

    /**
     * Entrega todos los tipos de contratos.
     * 
     * @return type
     */
    private function tcontrato() {
        $ar1 = '';
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_tipo_contrato', 'dc_tipo_contrato,dg_tipo_contrato',"dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $ar1[$crea->dc_tipo_contrato] = $crea->dg_tipo_contrato;
        }
        return $ar1;
    }

    /**
     * Entrega todas la isapres disponibles.
     * 
     * @return type
     */
    private function isapre() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_isapre', 'dc_isapre , dg_isapre',"dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_isapre] = $crea->dg_isapre;
        }
        return $ar1;
    }

    /**
     * Entrega todos los CeCo(Centro de Costo) disponibles.
     * 
     * @return type
     */
    private function ceco() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_ceco', 'dc_ceco , dg_ceco',"dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_ceco] = $crea->dg_ceco;
        }
        return $ar1;
    }

    /**
     * Verifica que la imagen no pase el peso permitido
     */
    private function verificarFotoTa() {
        switch ($_FILES['fc_foto']['error']) {
            case 1:
            case 2: $this->getErrorMan()->showWarning("El tamaño del archivo supera el máximo permitido.");
                exit();
                break;
            case 3: $this->getErrorMan()->showWarning("Se a cancelado la transferencia del archivo.");
                exit();
                break;
        }
    }

    /**
     * verifica que el tipo de imagen sea soportado
     */
    private function verificarFotoTi() {
        $tipos = array('image/gif', 'image/pjpeg', 'image/jpeg', 'image/jpg', 'image/x-png', 'image/png');

        if ($_FILES['fc_foto']['size']) {
            if (!in_array($_FILES['fc_foto']['type'], $tipos)) {
                $this->getErrorMan()->showWarning("El archivo especificado no es una imagen válida.<br />Los formatos soportados son gif, jpeg y png");
                exit();
            }
            $f_ext = substr($_FILES['fc_foto']['name'], strrpos($_FILES['fc_foto']['name'], ".") + 1);
        }
    }

    /**
     * 
     * Inserta los datos modificados
     * a la base de datos.
     * 
     */
    private function insertDatos() {

        $r = self::getRequest();
        $f_nac = explode("/", $r->func_nacimiento);
        $f_nac = $f_nac[2] . "-" . $f_nac[1] . "-" . $f_nac[0] . " 00:00:00";
        $f_ini_contr = explode("/", $r->func_ini_contr);
        $f_ini_contr = $f_ini_contr[2] . "-" . $f_ini_contr[1] . "-" . $f_ini_contr[0] . " 00:00:00";

         if($r->func_fin_contr)
         {
        $f_fin_contr = explode("/", $r->func_fin_contr);
        $f_fin_contr = $f_fin_contr[2] . "-" . $f_fin_contr[1] . "-" . $f_fin_contr[0] . " 00:00:00";
         }else
         {
             $f_fin_contr="0000-00-00 00:00:00";
         }

        $f_sueldo = str_replace(",", ".", $r->func_sueldo);
        if (!is_numeric($f_sueldo)) {
            $this->getErrorMan()->showWarning("El formato del sueldo es incorrecto");
            exit();
        }
        $f_values = array("dg_nombres" => '?',
            "dg_ap_paterno" => '?',
            "dg_ap_materno" => '?',
            "df_nacimiento" => '?',
            "dg_anexo" => '?',
            "dm_sexo" => '?',
            "dc_ceco" => '?',
            "dc_isapre" => '?',
            "dc_afp" => '?',
            "dc_tipo_contrato" => '?',
            "df_inicio_contrato" => '?',
            "df_fin_contrato" => '?',
            "dq_sueldo_bruto" => '?',
            "dc_cargo" => '?',
	    "dm_evaluacion_ticket" => '?');


        if ($_FILES['fc_foto']['size']) {
            $f_values["dr_foto_usuario"] = "'" . mysql_real_escape_string(fread(fopen($_FILES['fc_foto']['tmp_name'], 'r'), $_FILES['fc_foto']['size'])) . "'";
        }
        $db = $this->getConnection();
        $update = $db->prepare($db->update("tb_funcionario", $f_values, "dc_funcionario='{$r->id_func}'"));

        $update->bindValue(1, $r->func_nombres, PDO::PARAM_STR);
        $update->bindValue(2, $r->func_ap_paterno, PDO::PARAM_STR);
        $update->bindValue(3, $r->func_ap_materno, PDO::PARAM_STR);
        $update->bindValue(4, $f_nac, PDO::PARAM_STR);
        $update->bindValue(5, $r->func_anexo, PDO::PARAM_STR);
        $update->bindValue(6, $r->func_sexo, PDO::PARAM_INT);
        $update->bindValue(7, $r->func_ceco, PDO::PARAM_INT);
        $update->bindValue(8, $r->func_salud, PDO::PARAM_INT);
        $update->bindValue(9, $r->func_afp, PDO::PARAM_INT);
        $update->bindValue(10, $r->func_t_contrato, PDO::PARAM_INT);
        $update->bindValue(11, $f_ini_contr, PDO::PARAM_STR);
        $update->bindValue(12, $f_fin_contr, PDO::PARAM_STR);
        $update->bindValue(13, $f_sueldo, PDO::PARAM_STR);
        $update->bindValue(14, $r->cargo, PDO::PARAM_INT);
	$update->bindValue(15, $r->dm_evaluacion_ticket, PDO::PARAM_BOOL);
        //$update->bindValue(15, $r->compromisos ? 0:1,PDO::PARAM_BOOL);

        $db->stExec($update);
        $this->getErrorMan()->showConfirm("Se han actualizado los datos del funcionario correctamente.");
    }

    /**
     * 
     * Verifica que el cargo y el ceco coincidan
     * 
     * ejemplo:
     *          Si el ceco el contabilidad, el cargo no puede ser de servicios.
     * 
     */
    private function cargoVsCeco() {
        $r = self::getRequest();
        $r->func_ceco;
        $c=$r->cargo;
        if($c != 0)
        {
        $db = $this->getConnection();
        $ceco = $db->prepare($db->select("tb_ceco", "dg_ceco", "dc_ceco='{$r->func_ceco}'"));
        $db->stExec($ceco);
        $ceco = $ceco->fetch(PDO::FETCH_OBJ);
        $ceco = $ceco->dg_ceco;

        $cargo = $db->prepare($db->select("tb_cargos", "dg_area", "dc_cargo='{$r->cargo}'"));
        $db->stExec($cargo);
        $cargo = $cargo->fetch(PDO::FETCH_OBJ);
        $cargo = $cargo->dg_area;

        if ($ceco != $cargo) {
            $this->getErrorMan()->showWarning("Centro de costo y Cargos no coinciden");
            exit();
        }
    }
    }

}

?>
