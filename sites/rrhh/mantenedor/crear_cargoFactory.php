<?php

class crear_cargoFactory extends Factory {

    //put your code here
    public $title='Crear Cargo';
    public function indexAction() {
        $mostrar=$this->getFormView($this->getTemplateURL('crear'), array('sel' => $this->Select(), 'cargos' => $this->cargos(),'cargos_ocup'=>$this->muestra_cargos2()));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        
    }

    /**
     * 
     * Verifica el el cargo no tenga evaluador asignado.
     * 
     */
    private function Verificar_evaluador_evaluado() {
        $pass = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        if (isset($r->evaluador)) {
            for ($i = 1; $i < $r->valorf; $i++) {
                $nom = 'evalua' . $i;
                if (isset($r->$nom)) {
                    $ver = $db->prepare($db->select('tb_cargos', 'dc_evaluador', "dc_cargo={$r->$nom}"));
                    $db->stExec($ver);
                    $ver = $ver->fetch(PDO::FETCH_OBJ);
                    $ver1 = $ver->dc_evaluador;
                    if ($ver1 != null) {
                        $this->getErrorMan()->showAviso('Uno o mas de los cargos seleccionados ya tienen un evaluador asignado');
                        exit();
                    }
                }
            }
        }
    }
    
     /**
     * 
     * Muestra todos los cargos que ya tengan evaluador asignado 
     * 
     */
    private function muestra_cargos2() {
        $r = self::getRequest();
        $arr1 = '';
        $db = $this->getConnection();
        $muestra = $db->prepare($db->select('tb_cargos', 'dc_cargo,dg_cargo,dg_area,dc_evaluador', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($muestra);
        while ($llena = $muestra->fetch(PDO::FETCH_OBJ)) {
            if ($llena->dc_evaluador != null || $llena->dc_evaluador != 0)
            {
            $arr1[$llena->dg_area][$llena->dc_cargo] = $llena->dg_cargo;
            }
        }
        return $arr1;
    }

    /**
     * 
     * Se encarga de guardar el evaluador del cargo 
     * mediante la función up().
     * 
     * @param type $ls
     */
    private function guardar_cargos_evaluador($ls) {
        $r = self::getRequest();
        $db = $this->getConnection();
        if (isset($r->evaluador)) {
            for ($i = 1; $i < $r->valorf; $i++) {
                $nom = 'evalua' . $i;

                if (isset($r->$nom)) {
                    $this->up($db, $r->$nom, $ls);
                }
            }
        }
    }

    /**
     * Actualiza el campo dc_evaluador de la tabla tb_cargos
     * 
     * @param type $db
     * @param type $cargo
     * @param type $evaluador
     * 
     */
    private function up($db, $cargo, $evaluador) {
        $up = $db->prepare($db->update('tb_cargos', array('dc_evaluador' => '?'), "dc_cargo={$cargo}"));
        $up->bindvalue(1, $evaluador, PDO::PARAM_STR);
        $db->stExec($up);
    }

    /**
     * 
     * Guarda el cargo creado en la basede datos
     * 
     */
    public function crearAction() {

        $this->verificaCargo();

        $e = '';
        $r = self::getRequest();
        $fecha = date("Y-m-d H:i:s");
        $user_creacion = $this->getUserData()->dc_usuario;
        $db = $this->getConnection();

        $insert = $db->prepare($db->insert('tb_cargos', array('dg_cargo' => '?', 'dc_ceco' => '?', 'dc_evaluador' => '?', 'dc_usuario_creacion' => '?', 'df_creacion' => '?', 'dm_activo' => 1, 'dm_evalua' => '?','dc_empresa'=>'?','dg_area'=>'?')));
        $insert->bindValue(1, $r->cargo, PDO::PARAM_STR);
        $insert->bindValue(2, $r->ceco, PDO::PARAM_INT);
        $insert->bindValue(3, '', PDO::PARAM_INT);
        $insert->bindValue(4, $user_creacion, PDO::PARAM_INT);
        $insert->bindvalue(5, $fecha, PDO::PARAM_STR);
        $insert->bindValue(6, isset($r->evaluador) ? 1 : 0, PDO::PARAM_BOOL);
        $insert->bindValue(7,$this->getEmpresa(),PDO::PARAM_STR);
        $insert->bindValue(8,$this->ident_ceco($r->ceco),PDO::PARAM_STR);
        if(isset($r->evaluador))
        {
        if (!isset($r->continuar)) {
            $this->Verificar_evaluador_evaluado();
        }
        }
        $db->stExec($insert);
        $ls = $db->lastInsertId('dc_cargo');
        $this->guardar_cargos_evaluador($ls);
        $this->getErrorMan()->showConfirm('Cargo creado con exito!');
    }

    private function ident_ceco($id){
        $db=$this->getConnection();
        $select=$db->prepare($db->select('tb_ceco','dg_ceco',"dc_ceco={$id}"));
        $db->stExec($select);
        $select=$select->fetch(PDO::FETCH_OBJ);
        $select=$select->dg_ceco;
        return $select;
    }
        
    /**
     * Muestra todos los CeCo (Centro de Costo), diponibles
     * 
     * @return type
     */
    private function Select() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_ceco', 'dg_ceco,dc_ceco', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_ceco] = $crea->dg_ceco;
        }
        return $ar1;
    }

    /**
     * Varifica que el cargo no exista en la base de datos
     */
    private function verificaCargo() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $select = $db->prepare($db->select('tb_cargos', "1", "dg_cargo = '{$r->cargo}' and dc_ceco = '{$r->ceco}'"));
        $db->stexec($select);
        if ($select->fetch() != false) {
            $this->getErrorMan()->showWarning('El cargo ingresado ya existe');
            exit();
        }
    }

    /**
     * 
     * Muestra los cargos que ya han sido creados
     * @return type
     * 
     * 
     */
    private function cargos() {
        $a = '';
        $b = $this->getConnection();
        $c = $b->prepare($b->select('tb_cargos ca JOIN tb_ceco ce', 'ca.dc_ceco ,ca.dg_cargo,ce.dg_ceco,ca.dc_cargo', "ca.dm_activo=1 AND ca.dc_empresa={$this->getEmpresa()} AND ca.dc_ceco=ce.dc_ceco"));
        $b->stExec($c);
        while ($d = $c->fetch(PDO::FETCH_OBJ)) {
            $a[$d->dg_ceco][$d->dc_cargo."_".$d->dc_ceco] = $d->dg_cargo;
        }
        return $a;
    }
    
    

}

?>
