<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AutorizaCompFactory
 *
 * @author Eduardo
 */
class AutorizaCompFactory extends Factory {

    //put your code here
public $title='Autorizar aprobación de compromisos';

    public function indexAction() {
        $r = self::getRequest();
    $mostrar = $this->getFormView($this->getTemplateURL('Autoriza'),
                                   array('cargos'=>$this->muestra_cargos(),
                                         'cargos_ocup'=>$this->cargos_ocupados(),
                                         'usuario'=>$this->ver_evaluadores()
                                         )
                                 );
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function SaveAction() {
        $this->Desactivar();
        $this->Reactivar_Guardar();
    }

    /*
     * 
     * Mostrar Datos
     * 
     */

    private function ver_evaluadores() {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        $mostrar = $db->prepare($db->select('tb_cargos ca 
                                                JOIN tb_funcionario fu
                                                    JOIN tb_usuario us', 
                                          'CONCAT(
                                                  fu.dg_nombres," ",
                                                  fu.dg_ap_paterno," ",
                                                  fu.dg_ap_materno
                                                  ) 
                                                  AS nombre_completo,
                                           us.dc_usuario,
                                           ca.dc_cargo,
                                           CONCAT(
                                                  ca.dg_cargo," / ",
                                                  ca.dg_area
                                                  ) 
                                                  AS cargo', 
                                           "ca.dm_evalua=1
                                                AND ca.dc_cargo=fu.dc_cargo
                                                    AND fu.dc_funcionario=us.dc_funcionario
                                                        AND fu.dc_empresa={$this->getEmpresa()}
                                                        AND ca.dm_activo=1
                                                            "
                )
        );
        $db->stExec($mostrar);
        while ($llenar = $mostrar->fetch(PDO::FETCH_OBJ)) {
            $arr1[$llenar->dc_usuario . "_" . $llenar->dc_cargo] = $llenar->nombre_completo." -- ".$llenar->cargo;
        }
        return $arr1;
    }

    private function muestra_cargos() {
        $arr1 = '';
        $db = $this->getConnection();
        $muestra = $db->prepare($db->select('tb_cargos', 'dc_cargo,
                                             dg_cargo,
                                             dg_area', 
                                            "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"
                                           )
                               );
        $db->stExec($muestra);
        while ($llena = $muestra->fetch(PDO::FETCH_OBJ)) {

            $arr1[$llena->dg_area][$llena->dc_cargo] = $llena->dg_cargo;
        }
        return $arr1;
    }

    private function cargos_ocupados() {
        $arr1 = '';
        $db = $this->getConnection();
        $cargos_ocup = $db->prepare($db->select('tb_cargos ca 
                                                    JOIN tb_usuario_autoriza us_a', 
                                                'ca.dg_area,
                                                 ca.dc_cargo,
                                                 ca.dg_cargo,
                                                 us_a.dc_usuario', 
                                                'us_a.dc_cargo=ca.dc_cargo AND us_a.dm_activo=1=ca.dm_activo'
                )
        );
        $db->stExec($cargos_ocup);
        while ($llena = $cargos_ocup->fetch(PDO::FETCH_OBJ)) {
            $arr1[$llena->dg_area][$llena->dc_cargo] [$llena->dc_usuario]= $llena->dg_cargo;
        }
        return $arr1;
    }

    /*
     * 
     * Fin Mostrar Datos
     * 
     */

    
    /*
     * 
     * Guardar o Reactivar relaciones
     * 
     */
    private function Desactivar() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $user=split("_",$r->usuario);
        $up = $db->prepare($db->update('tb_usuario_autoriza', array('dm_activo' => 0), "dc_usuario={$user[0]}"));
        $db->stExec($up);
    }

    private function Reactivar_Guardar() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $user=split("_",$r->usuario);
        for ($i = 1; $i <= $r->valorf; $i++) {

            $nom = 'autoriza' . $i;
            if (isset($r->$nom)) {
                $this->verificar($r->$nom, $user[0]);
            }
        }
        $msg='...';
        echo $this->getFormView($this->getTemplateURL('listo'),array('msg'=>$msg));
    }

    private function verificar($dc_cargo, $dc_user) {
        $db = $this->getConnection();
        $verifica = $db->prepare($db->select('tb_usuario_autoriza', '*', "dc_usuario={$dc_user} AND dc_cargo={$dc_cargo}"));
        $db->stExec($verifica);
        $verifica = $verifica->fetch(PDO::FETCH_OBJ);
        if ($verifica != null) {
            if ($verifica->dm_activo == 0) {
                $this->reactivar($dc_user, $dc_cargo, $db);
            }
        } else {
            $this->guardar($dc_user, $dc_cargo, $db);
        }
    }

    private function reactivar($dc_user, $dc_cargo, $db) {
        $reactivar = $db->prepare($db->update('tb_usuario_autoriza', array('dm_activo' => 1), "dc_usuario={$dc_user} 
                                                        AND 
                                                           dc_cargo={$dc_cargo}"
                )
        );
        $db->stExec($reactivar);
    }

    private function guardar($dc_user, $dc_cargo, $db) {
        $save = $db->prepare($db->insert('tb_usuario_autoriza', array(
                    'dc_cargo' => '?',
                    'dc_usuario' => '?',
                    'dm_activo' => 1
                        )
                )
        );
        $save->bindvalue(1, $dc_cargo, PDO::PARAM_INT);
        $save->bindvalue(2, $dc_user, PDO::PARAM_INT);
        $db->stExec($save);
    }

    /*
     * 
     * Fin Guardar o Reactivar relaciones
     * 
     */
}

?>
