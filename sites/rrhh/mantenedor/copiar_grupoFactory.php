<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of copiar_grupoFactory
 *
 * @author Eduardo
 */
class copiar_grupoFactory extends Factory {

    //put your code here

    public function indexAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('copiar'), array('cargos' => $this->muestra_cargos(),
            'grupo' => $this->grupo_a_copiar(),
            'Cargo' => $this->cargo_grupo($r->id_cargo)
        ));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function SaveAndCopyAction() {
        $r = self::getRequest();
        $cant=0;
        for ($i = 1; $i <= $r->valorf; $i++) {
            $nom = "id_cargo_copia" . $i;
            if (isset($r->$nom)) {
                $cant++;
                $this->save($r->$nom);
            }
        }
        $msg='Espere mientras se copian los grupos';
        $tiempo=($cant*0.03)*1000;
        echo $this->getFormView($this->getTemplateURL('listo'),array('msg'=>$msg,'time'=>$tiempo));
    }

    private function save($cargo) {
        $r = self::getRequest();
        $db = $this->getConnection();
        $save = $db->prepare($db->insert('tb_grupo_cargo_relacion', array(
                    'dc_grupo_pregunta' => '?',
                    'dc_cargo' => '?',
                    'dc_empresa'=> '?',
                    'dm_activo'=>1
        )));
        $save->bindValue(1, $r->id_grupo_c, PDO::PARAM_INT);
        $save->bindValue(2, $cargo, PDO::PARAM_INT);
        $save->bindValue(3, $this->getEmpresa(), PDO::PARAM_INT);
        $inserta=$this->verifica_relacion($cargo, $r->id_grupo_c);
        if ($inserta == true) {
            $db->stExec($save);
        }
    }

    private function verifica_relacion($cargo, $grupo) {
        $db = $this->getConnection();
        $ver = $db->prepare($db->select('tb_grupo_cargo_relacion', '*', "dc_cargo={$cargo} AND dc_grupo_pregunta={$grupo}"));
        $db->stExec($ver);
        $ver = $ver->fetch(PDO::FETCH_OBJ);
        if ($ver != false) {
            if ($ver->dm_activo == 0) {
                $activar = $db->prepare($db->update('tb_grupo_cargo_relacion', array('dm_activo' => 1), "dc_cargo={$cargo} AND dc_grupo_pregunta={$grupo}"));
                $db->stExec($activar);
                return false;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private function muestra_cargos() {
        $r = self::getRequest();
        $arr1 = '';
        $db = $this->getConnection();
        $muestra = $db->prepare($db->select('tb_cargos', 'dc_cargo,dg_cargo,dg_area', "dc_cargo !={$r->id_cargo} AND dc_empresa={$this->getEmpresa()} AND dm_activo=1"));
        $db->stExec($muestra);
        while ($llena = $muestra->fetch(PDO::FETCH_OBJ)) {

            $arr1[$llena->dg_area][$llena->dc_cargo] = $llena->dg_cargo;
        }
        return $arr1;
    }

    private function grupo_a_copiar() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $grup = $db->prepare($db->select('tb_grupo_preguntas', 'dg_grupo_pregunta,dc_grupo_pregunta', "dc_grupo_pregunta={$r->id_grupo}"));
        $db->stExec($grup);
        $r->grup = $grup->fetch(PDO::FETCH_OBJ);

        return $r->grup;
    }

    private function cargo_grupo($id_cargo) {

        $db = $this->getConnection();
        $select = $db->prepare($db->select('tb_cargos', 'dg_cargo', "dc_cargo={$id_cargo}"));
        $db->stExec($select);
        $select = $select->fetch(PDO::FETCH_OBJ);
        $select = $select->dg_cargo;
        return $select;
    }

}

?>
