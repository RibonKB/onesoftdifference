<?php
require_once('sites/rrhh/FuncionesEvaluaciones.php');

class CompromisosFactory extends FuncionesEvaluaciones{
   public $title='Compromisos';
     
     public function indexAction()
     {
         $mostrar = $this->getFormView($this->getTemplateURL('AsignarCompromiso'),
                 array(
                     'funcionario'=>$this->user_cargo_area(),
                     'compromisos_creados'=>$this->ver_compromisos_creados2()
                 )
                 );
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
     }
     
     public function crearCompromisoAction()
     {
         $this->grabar();
     }
     
     
     private function user_cargo_area() {
        $ar1 = '';
        $db = $this->getConnection();
        $mostrar = $db->prepare($db->select('tb_funcionario fu 
                                                    JOIN tb_cargos ca
                                                        JOIN tb_usuario us', 
                                            'fu.dg_nombres,
                                             fu.dg_ap_paterno,
                                             fu.dg_ap_materno,
                                             ca.dg_cargo,
                                             ca.dg_area,
                                             ca.dc_cargo,
                                             fu.dc_funcionario,
                                             us.dc_usuario', 
                                            "ca.dc_evaluador={$this->getUserData()->dc_cargo} 
                                                    AND ca.dc_cargo = fu.dc_cargo
                                                        AND us.dc_funcionario=fu.dc_funcionario
                                                            AND fu.dc_empresa={$this->getEmpresa()}"
        ));
        $db->stExec($mostrar);

        while ($ss = $mostrar->fetch(PDO::FETCH_OBJ)) {
                /*$ar1[$ss->dc_funcionario] =
                        $ss->dg_nombres . " " .
                        $ss->dg_ap_paterno . " " .
                        $ss->dg_ap_materno . " / " .
                        $ss->dg_cargo . "-" .
                        $ss->dg_area;*/
                $ar1[$ss->dc_usuario."_".$ss->dc_funcionario] =
                        $ss->dg_nombres . " " .
                        $ss->dg_ap_paterno . " " .
                        $ss->dg_ap_materno;
            }
        return $ar1;
    }
    
    private function grabar()
    {
        $r=self::getRequest();
        $db=$this->getConnection();
        $fecha_hoy = date('Y-m-d H:i:s');
       $dc=split("_",$r->funcionario);
        $dc_user_func=$dc[0];
        $fecha_compromiso=$this->fecha($r->fecha_compromiso);
        $save=$db->prepare($db->insert('tb_compromisos',array(
                            'dg_compromiso'=>'?',
                            'dg_compromiso_empresa'=>'?',
                            'dc_creador'=>'?',
                            'dc_usuario_funcionario'=>'?',
                            'dc_empresa'=>'?',
                            'df_creacion'=>'?',
                            'df_compromiso'=>'?',
                            'dm_activo'=>1,
                            'dc_estado'=>1,
                            'dc_cumple'=>1
            )));             //dc_cumple y dc_estado: 1=en espera, 2=positivo=en cumple es 'cumple' y en estado es 'aceptado' )
                             // 3=negativo=en cumple es 'no cumple' y en estado es 'rechazado'.
        
        $save->bindValue(1,$r->compromiso_func ,PDO::PARAM_STR);
        $save->bindValue(2,$r->compromiso_emp ,PDO::PARAM_STR);
        $save->bindValue(3,$this->getUserData()->dc_usuario ,PDO::PARAM_INT);
        $save->bindValue(4,$dc_user_func ,PDO::PARAM_INT);
        $save->bindValue(5,$this->getEmpresa() ,PDO::PARAM_INT);
        $save->bindValue(6,$fecha_hoy ,PDO::PARAM_STR);
        $save->bindValue(7,$fecha_compromiso ,PDO::PARAM_STR);
        $db->stExec($save);
    }
    
    private function ver_compromisos_creados2()
    {
        $arr1='';
        $r=self::getRequest();
        $db=$this->getConnection();
        $arr2=$this->user_cargo_area();
        $i=0;
        if($arr2)
        {
        foreach($arr2 as $dc_funcionario=>$nombre_funcionario)
        {
            $dc=split("_",$dc_funcionario);
        $dc_user_func=$dc[1];
        $compromisos=$db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us',
                                              'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple',
                                              "com.dc_creador={$this->getUserData()->dc_usuario}
                                                    AND com.dc_usuario_funcionario=us.dc_usuario
                                                        AND us.dc_funcionario={$dc_user_func}
                                                            AND com.dm_activo=1"
                ));
                                    
        $db->stExec($compromisos);
        while($llena=$compromisos->fetch(PDO::FETCH_OBJ))
        {
            if($llena->dg_compromiso_empresa==null) 
            {
                $llena->dg_compromiso_empresa="─═☆☆═─";
            }
            $arr1[$llena->dc_usuario][$nombre_funcionario][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple]=$llena->dc_estado;
        }
        }
        }else{
             $arr1['---']['---']['---']['---']['---']['---']='---';
        }
        return $arr1;
    }
    
    private function fecha($fecha) {
        $f = explode("/", $fecha);
        if ($f[2]) {
            $f = $f[2] . "-" . $f[1] . "-" . $f[0] ." ". date("H:i:s");
        } else {
            $f = "0000-00-00 00:00:00";
        }
        return $f;
    }
    
     
     
}

?>
