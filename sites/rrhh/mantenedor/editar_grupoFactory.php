<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of editar_grupoFactory
 *
 * @author Eduardo
 */
class editar_grupoFactory extends Factory {

    public $title='Editar Grupo';
    //put your code here

    public function indexAction() {
         $mostrar=$this->getFormView($this->getTemplateURL('editar'),array('grupo'=>$this->grupo(),'cargos'=>$this->cargos(),'cargo'=>$this->identificar_cargo_grupo()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }
    
    public function editCargoAction()
    {
        $r=self::getRequest();
        $mostrar=$this->getFormView($this->getTemplateURL('editar_cgp'),array('grupo'=>$this->grupo(),'cargos'=>$this->cargos(),'cargo'=>$this->identificar_cargo_grupo(),'id_cargo'=>$r->id_cargo));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function editNomAction() {
        $this->upNom();
        $msg='Grupo editado con exito';
        echo $this->getFormView($this->getTemplateURL('listo'),array('msg'=>$msg));
    }

    private function upNom() {
        $db = $this->getConnection();
        $r = self::getRequest();
        $up=$db->prepare($db->update('tb_grupo_preguntas',array('dg_grupo_pregunta'=>'?'),"dc_grupo_pregunta={$r->id_grupo}"));
        $up->bindvalue(1,$r->grupo,PDO::PARAM_STR);
            $this->comprobar_disp();
        $db->stExec($up);
    }
    
    private function comprobar_disp(){
        $db = $this->getConnection();
        $r = self::getRequest();
        $sel=$db->prepare($db->select('tb_grupo_preguntas','dg_grupo_pregunta',"dg_grupo_pregunta='{$r->grupo}'"));
        $db->stExec($sel);
        $sel=$sel->fetch(PDO::FETCH_OBJ);
        if($sel != false){
            $this->getErrorMan()->showWarning('Ya existe un grupo con este nombre');
            exit();
        }
    }

    private function grupo() {
        $r=self::getRequest();
        $db=$this->getConnection();
        $grupo=$db->prepare($db->select('tb_grupo_preguntas','dc_grupo_pregunta,dg_grupo_pregunta',"dc_grupo_pregunta={$r->id_grupo}"));
        $db->stExec($grupo);
        $r->grupo=$grupo->fetch(PDO::FETCH_OBJ);
        return $r->grupo;
    }
    
    private function cargos(){
         $r=self::getRequest();
         $arr1='';
        $db=$this->getConnection();
        $grupo=$db->prepare($db->select('tb_cargos','dc_cargo,dg_area,dg_cargo',"dm_activo=1"));
        $db->stExec($grupo);
        while($completa=$grupo->fetch(PDO::FETCH_OBJ))
        {
            $arr1[$completa->dc_cargo]=$completa->dg_cargo." - ".$completa->dg_area;
        }
        return $arr1;
        
    }
    
    private function identificar_cargo_grupo()
    {
         $r=self::getRequest();
         $arr1='';
        $db=$this->getConnection();
        $car_gru=$db->prepare($db->select('tb_cargos ','dc_cargo',"dc_cargo={$r->id_cargo} "));
        $db->stExec($car_gru);
        $car_gru=$car_gru->fetch(PDO::FETCH_OBJ);
        if($car_gru != false)
        {
            $car_gru=$car_gru->dc_cargo;
        }else
        {
            $car_gru='no';
        }
        
        return $car_gru;
    }
    
    
    public function editarCarAction() {
        $this->upCar();
        $msg='Grupo Editado correctamente';
        echo $this->getFormView($this->getTemplateURL('listo'),array('msg'=>$msg));
    }

    private function upCar() {
        $db = $this->getConnection();
        $r = self::getRequest();
        $up=$db->prepare($db->update('tb_grupo_cargo_relacion',array('dc_cargo'=>'?','dc_grupo_pregunta'=>'?'),"dc_grupo_pregunta={$r->id_grupo} AND dc_cargo={$r->id_cargo}"));
        $up->bindValue(1,$r->cargo,PDO::PARAM_INT);
        $up->bindvalue(2,$r->id_grupo,PDO::PARAM_INT);
        $this->verifica_relacion($r->id_grupo,$r->cargo);
        $db->stExec($up);
    }
    
    private function verifica_relacion($gp,$pr)
    {
        $db=$this->getConnection();
        $ver=$db->prepare($db->select('tb_grupo_cargo_relacion','*',"dc_grupo_pregunta={$gp} AND dc_cargo={$pr}"));
        $db->stExec($ver);
        $ver=$ver->fetch(PDO::FETCH_OBJ);
        if($ver != false)
        {
            $this->getErrorMan()->showWarning('La relacion Grupo-Cargo ya existe');
            exit();
        }
    }
    
   

}

?>
