<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gruposFactory
 *
 * @author Eduardo
 */
class gruposFactory extends Factory {

    public $title = 'Crear Grupo de pregunta';

    public function indexAction() {
        $mostrar = $this->getFormView($this->getTemplateURL('grupo'), array('cargo' => $this->mostrar_cargos(), 'grupos' => $this->cargo_cargo(), 'preguntas' => $this->mostrar_preguntas_asoc()));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    /**
     * Inserta el grupo a la base de datos.
     * 
     * antes de ser insertado se eliminana los caracteres especiales
     * contenidos en @param array() $ce.
     * 
     * luego se transforma a lowcase.
     */
    public function insertarAction() {
        $this->verificarGrupo();
        $r = self::getRequest();
        $db = $this->getConnection();
        $user_creacion = $this->getUserData()->dc_usuario;
        $fecha = date("Y-m-d H:i:s");
        $empresa = $this->getEmpresa();
        $insert = $db->prepare($db->insert("tb_grupo_preguntas", array(
                    "dg_grupo_pregunta" => '?',
                    "dc_empresa" => '?',
                    "dc_usuario" => '?',
                    "df_creacion" => '?',
                    "dm_activo" => '1',
        )));

        $ce = array('/', '?', '¿', '!', '¡', ':', '$', '#', '|', '°', '¬', '^', '{', '}', '[', ']', '%', '\'', '´', '+', '¨', '`', '=');
        $gr_lc = str_replace($ce, "", $r->grupo);

        $insert->bindValue(1, $gr_lc, PDO::PARAM_STR);
        $insert->bindValue(2, $empresa, PDO::PARAM_INT);
        $insert->bindValue(3, $user_creacion, PDO::PARAM_INT);
        $insert->bindValue(4, $fecha, PDO::PARAM_STR);
        $db->stExec($insert);
        $last = $db->lastInsertId('dc_grupo_pregunta');
        $insertaR = $db->prepare($db->insert('tb_grupo_cargo_relacion', array(
                    "dc_grupo_pregunta" => '?',
                    "dc_cargo" => '?',
                    "dm_activo"=>1,
                    "dc_empresa"=>'?'
        )));
        $insertaR->bindvalue(1, $last, PDO::PARAM_INT);
        $insertaR->bindvalue(2, $r->cargo, PDO::PARAM_INT);
    $insertaR->bindValue(3,$this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($insertaR);

        $this->getErrorMan()->showConfirm('Grupo creado de forma exitosa');
    }

    /**
     * 
     * Verifica que el grupo ingresado no exista en la base de datos.
     * 
     * @param string $lc mediante el metodo @str_replace() convierte el nombre del grupo a Lowcase.
     * 
     * @param array() $ce Contiene todos los caracteres especiales
     * que serán eliminados antes de insertar el grupo en la base de datos.
     */
    private function verificarGrupo() {

        $r = self::getRequest();
        $ce = array('/', '?', '¿', '!', '¡', ':', '$', '#', '|', '°', '¬', '^', '{', '}', '[', ']', '%', '\'', '´', '+', '¨', '`', "=");
        $gr_lc = str_replace($ce, "", $r->grupo);
        $r = self::getRequest();
        $db = $this->getConnection();
        $select = $db->prepare($db->select("tb_grupo_preguntas", "dc_grupo_pregunta", "dg_grupo_pregunta='{$gr_lc}'"));
        $db->stExec($select);
        $select = $select->fetch(PDO::FETCH_OBJ);
        if ($select != false) {
            $sel = $db->prepare($db->select('tb_grupo_cargo_relacion', '*', "dc_grupo_pregunta={$select->dc_grupo_pregunta} AND dc_cargo={$r->cargo}"));
            $db->stExec($sel);
            $sel = $sel->fetch(PDO::FETCH_OBJ);
            echo $sel->dc_grupo_pregunta."..".$sel->dc_cargo;
            if ($sel != false) {
                if($sel->dm_activo == 0)
                {
                    
                  $update=$db->prepare($db->update('tb_grupo_cargo_relacion',array('dm_activo'=>1),"dc_grupo_pregunta={$select->dc_grupo_pregunta} AND dc_cargo={$r->cargo}")); 
                  $db->stExec($update);
                  $this->getErrorMan()->showConfirm('Grupo creado de forma exitosa');
                  exit();
                }else
                {
                $this->getErrorMan()->showWarning('El grupo ingresado ya existe');
                exit();
                }
            }
        }
    }

    private function grupos() {
        $a = '';
        $c = $this->getConnection();
        $d = $c->prepare($c->select('tb_grupo_preguntas', 'dc_grupo_pregunta,dg_grupo_pregunta', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $c->stExec($d);
        while ($s = $d->fetch(PDO::FETCH_OBJ)) {
            $a[$s->dc_grupo_pregunta] = $s->dg_grupo_pregunta;
        }
        return $a;
    }

    private function mostrar_cargos()
    {
        $arr1='';
        $db=$this->getConnection();
        $s=$db->prepare($db->select('tb_cargos ca JOIN tb_ceco ce','ca.dc_cargo,ca.dg_cargo,ce.dg_ceco',"ca.dc_empresa={$this->getEmpresa()} AND ca.dc_ceco=ce.dc_ceco AND ca.dm_activo=1"));
        $db->stExec($s);
        while ($llena = $s->fetch(PDO::FETCH_OBJ)) {
                $arr1[$llena->dc_cargo] = $llena->dg_cargo." - ".$llena->dg_ceco;
            }
        if($arr1=='')
        {
            $arr1['----'] = '°----°';
        }
        return $arr1;
    }
    
    private function ident_dc_cargo() {
        $rr = $this->grupos();
        $arr1 = '';
        if($rr)
        {
        foreach ($rr as $dc_grupo => $dg_grupo) {

            $db = $this->getConnection();
            $dc_cargo = $db->prepare($db->select('tb_grupo_cargo_relacion', 'dc_cargo', "dc_grupo_pregunta={$dc_grupo} AND dc_empresa={$this->getEmpresa()}"));
            $db->stExec($dc_cargo);
            while ($llena = $dc_cargo->fetch(PDO::FETCH_OBJ)) {
                $arr1[$llena->dc_cargo] = $this->ident_cargo($llena->dc_cargo);
            }
        }
        }else{
            
            $arr1['----'] = '°----°';
        }
        return $arr1;
    }

    private function cargo_cargo() {
        $rr = $this->grupos();
        $arr1 = '';
        if($rr)
        {
        foreach ($rr as $dc_grupo => $dg_grupo) {

            $db = $this->getConnection();
            $dc_cargo = $db->prepare($db->select('tb_grupo_cargo_relacion', 'dc_cargo', "dc_grupo_pregunta={$dc_grupo} AND dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
            $db->stExec($dc_cargo);
            while ($llena = $dc_cargo->fetch(PDO::FETCH_OBJ)) {
                $arr1[$dc_grupo . "_" . $llena->dc_cargo][$this->ident_cargo($llena->dc_cargo)] = $dg_grupo;
            }
        }
        }else{
            $arr1['---']['---'] = '---';
        }
        return $arr1;
    }

    private function ident_cargo($id_cargo) {
        $db = $this->getConnection();
        $cargo = $db->prepare($db->select('tb_cargos ca JOIN tb_ceco ce', 'ca.dg_cargo,ce.dg_ceco', "ca.dm_activo=1 AND ca.dc_cargo={$id_cargo} AND ca.dc_ceco=ce.dc_ceco AND ca.dc_empresa={$this->getEmpresa()}"));
        $db->stExec($cargo);
        $cargo = $cargo->fetch(PDO::FETCH_OBJ);
        if($cargo != false)
        {
        $cargo = $cargo->dg_cargo . " - " . $cargo->dg_ceco;
        }

        return $cargo;
    }

    private function mostrar_preguntas_asoc() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $arr1='';
        $m_preguntas = $db->prepare($db->select('tb_preguntas', 'dg_pregunta,dc_grupo_pregunta,dc_pregunta', "dm_activo=1"));
        $db->stExec($m_preguntas);
        while ($pregunta = $m_preguntas->fetch(PDO::FETCH_OBJ)) {
            
            $arr1[$pregunta->dc_grupo_pregunta][$pregunta->dc_pregunta] = $pregunta->dg_pregunta; {
                
            }
        }

        return $arr1;
    }

}

?>
