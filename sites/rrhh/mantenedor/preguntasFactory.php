<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of preguntasFactory
 *
 * @author Eduardo
 */
class preguntasFactory extends Factory {

    public $title = 'Crear pregunta';

    public function indexAction() {
        $mostrar = $this->getFormView($this->getTemplateURL('pregunta'), array("gr" => $this->ident_dc_cargo(), 'preguntas' => $this->preguntas(),'cargos'=>$this->cargo_preguntas()));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }


    /**
     * Se inserta la pregunta a la base de datos.
     * 
     * Todas las preguntas ingresadas serán transformadas a lowcase (minusculas)
     * y se eliminaran los caracteres "¿?".
     */
    public function insertarAction() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $user_creacion = $this->getUserData()->dc_usuario;
        $fecha = date("Y-m-d H:i:s");
        $insert = $db->prepare($db->insert("tb_preguntas", array(
                    "dg_pregunta" => '?',
                    "dc_empresa" => '?',
                    "dc_usuario" => '?',
                    "df_creacion_pregunta" => '?',
                    "dm_activo" => '1',
                    "dc_grupo_pregunta" => '?'
        )));

        $sig_p = array('!', '¡', ':', '$', '#', '|', '°', '¬', '^', '{', '}', '[', ']', '\'', '+', '`', '=');
        $pr_sS = str_replace($sig_p, "", $r->pregunta);
        $grupo=split('_',$r->grupo);
        $insert->bindValue(1, $pr_sS, PDO::PARAM_STR);
        $insert->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
        $insert->bindValue(3, $user_creacion, PDO::PARAM_INT);
        $insert->bindValue(4, $fecha, PDO::PARAM_STR);
        $insert->bindValue(5, $grupo[0], PDO::PARAM_INT);

        $db->stExec($insert);
    }

    /**
     * 
     * Función encargada de mostrar la lista de grupos existentes
     * para seleccionar a que grupo perteneserá la pregunta.
     * 
     * @return array() $ar1.
     * 
     * El array() retornado contiene todos los grupos existentes que esten activos
     */
    private function grupo() {
        $ar1 = '';
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_grupo_preguntas', 'dc_grupo_pregunta,dg_grupo_pregunta', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $ar1[$crea->dc_grupo_pregunta]=$crea->dg_grupo_pregunta;
        }
        return $ar1;
    }
    
    
    private function ident_dc_cargo()
    {
        $rr=$this->grupo();
        $arr1='';
        if($rr)
        {
        foreach($rr as $dc_grupo=>$dg_grupo)
        {
        
        $db=$this->getConnection();
        $dc_cargo=$db->prepare($db->select('tb_grupo_cargo_relacion','dc_cargo',"dc_grupo_pregunta={$dc_grupo} AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($dc_cargo);
        while($llena=$dc_cargo->fetch(PDO::FETCH_OBJ))
        {
            $arr1[$dc_grupo."_".$llena->dc_cargo]=$dg_grupo." / ".$this->identifica_cargo($llena->dc_cargo);
        }
        }
        }else{
            
            $arr1['----'] = '----';
        }
        return $arr1;
    }
    
    
    private function identifica_cargo($cargo)
    {
        $db=$this->getConnection();
        $ident=$db->prepare($db->select('tb_cargos ca JOIN tb_ceco ce','ca.dg_cargo,ce.dg_ceco',"ca.dc_cargo={$cargo} AND ca.dc_ceco=ce.dc_ceco"));
        $db->stExec($ident);
        $ident=$ident->fetch(PDO::FETCH_OBJ);
           $ident=$ident->dg_cargo." - ".$ident->dg_ceco;
        return $ident;
    }
    

    /**
     * Verifica que no se ingrese la misma prgunta mas de una vez.
     */
    private function preguntas() {
        $d = '';
        $a = $this->getConnection();
        $b = $a->prepare($a->select('tb_preguntas pr JOIN tb_grupo_preguntas grp', 'pr.dg_pregunta,pr.dc_pregunta,grp.dg_grupo_pregunta,grp.dc_grupo_pregunta', "pr.dc_grupo_pregunta=grp.dc_grupo_pregunta AND grp.dc_empresa={$this->getEmpresa()} and pr.dm_activo=1  ORDER BY grp.dc_grupo_pregunta"
        ));
        $a->stExec($b);
        while ($c = $b->fetch(PDO::FETCH_OBJ)) {
            $d[$c->dc_pregunta . "-" . $c->dc_grupo_pregunta][$c->dg_grupo_pregunta] = $c->dg_pregunta;
        }
        return $d;
    }

    private function cargo_preguntas() {
        $db = $this->getConnection();
        $arr1 = '';
        $cargo_pregunta = $db->prepare($db->select('tb_grupo_preguntas grp 
                                                        JOIN tb_cargos ca
                                                            JOIN tb_grupo_cargo_relacion grc
                                                                JOIN tb_ceco ce',
                'grc.dc_grupo_pregunta,
                    ca.dg_cargo,ce.dg_ceco,
                    grc.dc_cargo', 
                "grc.dc_cargo =ca.dc_cargo 
                    AND grc.dc_grupo_pregunta=grp.dc_grupo_pregunta 
                    AND grp.dc_empresa={$this->getEmpresa()}
                        AND ca.dc_ceco=ce.dc_ceco"
        ));
        $db->stExec($cargo_pregunta);
        
            while ($llena = $cargo_pregunta->fetch(PDO::FETCH_OBJ)) {
                    $arr1[$llena->dc_grupo_pregunta][$llena->dc_cargo]=$llena->dg_cargo." - ".$llena->dg_ceco;
            }
        
        return $arr1;
    }

}

?>
