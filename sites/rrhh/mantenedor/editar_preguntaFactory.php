<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of editar_preguntaFactory
 *
 * @author Eduardo
 */
class editar_preguntaFactory extends Factory {
    //put your code here
    
    public $title='Edición';
    public function indexAction()
    {
        $mostrar=$this->getFormView($this->getTemplateURL('editar'),array('pregunta'=>$this->editar_pregunta_pregunta()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }
    
    public function GrupoAction()
    {
        $mostrar=$this->getFormView($this->getTemplateURL('editar_gp'),array('pregunta'=>$this->editar_pregunta_pregunta(),'grupos'=>$this->grupos(),'grupo'=>$this->grupo()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function delAction()
    {
        $mostrar=$this->getFormView($this->getTemplateURL('eliminar'),array('pregunta'=>$this->preg()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }
    
        private function editar_pregunta_pregunta()
    {
        $r=self::getRequest();
        $db=$this->getConnection();
        $pregunta=$db->prepare($db->select('tb_preguntas','dg_pregunta,dc_pregunta',"dc_pregunta={$r->id_pregunta}"));
        $db->stExec($pregunta);
        $r->pregunta=$pregunta->fetch(PDO::FETCH_OBJ);
        return $r->pregunta;
    }
    
    public function preguntaAction()
    {
        $r=self::getRequest();
        $db=$this->getConnection();
        $edicion=$db->prepare($db->update('tb_preguntas',array('dg_pregunta'=>'?'),"dc_pregunta={$r->id_pregunta}"));
        $edicion->bindValue(1,$r->pregunta,PDO::PARAM_STR);
        $db->stExec($edicion);        
        $this->getErrorMan()->showConfirm('La pregunta se modifico correctamente');
    }
    
    private function grupos()
    {
        $arr1='';
        $r=self::getRequest();
        $db=$this->getConnection();
        $grupos=$db->prepare($db->select('tb_grupo_preguntas','dg_grupo_pregunta,dc_grupo_pregunta'));
        $db->stExec($grupos);
        while($llena=$grupos->fetch(PDO::FETCH_OBJ))
        {
            $arr1[$llena->dc_grupo_pregunta]=$llena->dg_grupo_pregunta;
        }
        return $arr1;
        
    }
    
    private function grupo()
    {
        $arr1='';
        $r=self::getRequest();
        $db=$this->getConnection();
        $grupos=$db->prepare($db->select('tb_grupo_preguntas grp JOIN tb_preguntas pr','grp.dc_grupo_pregunta',"grp.dc_grupo_pregunta={$r->grupo} AND pr.dc_pregunta={$r->id_pregunta}"));
        $db->stExec($grupos);
        $grupos=$grupos->fetch(PDO::FETCH_OBJ);
        return $grupos;
        
    }
    
    private function grupo2($gr)
    {
        $arr1='';
        $r=self::getRequest();
        $db=$this->getConnection();
        $grupos=$db->prepare($db->select('tb_grupo_preguntas grp JOIN tb_preguntas pr','grp.dg_grupo_pregunta',"grp.dc_grupo_pregunta={$gr} AND pr.dc_pregunta={$r->id_pregunta}"));
        $db->stExec($grupos);
        
        $grupos=$grupos->fetch(PDO::FETCH_OBJ);
        return $grupos->dg_grupo_pregunta;
        
    }
    
    public function grupoEditAction()
    {
         $r=self::getRequest();
        $db=$this->getConnection();
        $edicion=$db->prepare($db->update('tb_preguntas',array('dc_grupo_pregunta'=>'?'),"dc_pregunta={$r->id_pregunta}"));
        $edicion->bindValue(1,$r->grupo,PDO::PARAM_STR);
        $db->stExec($edicion);        
        $this->getErrorMan()->showConfirm('La pregunta se modifico correctamente');
    }
    
    private function preg()
    {
        $arr1='';
        $r=self::getRequest();
        $db=$this->getConnection();
        $pregunta=$db->prepare($db->select('tb_preguntas','dg_pregunta,dc_pregunta,dc_grupo_pregunta',"dc_pregunta={$r->id_pregunta}"));
        $db->stExec($pregunta);
       while($llena=$pregunta->fetch(PDO::FETCH_OBJ))
       {
           $arr1[$llena->dc_pregunta][$this->grupo2($llena->dc_grupo_pregunta)]=$llena->dg_pregunta;
       }
        
        return $arr1;
    }
    
    public function eliminarAction()
    {
        $r=self::getRequest();
        $db=$this->getConnection();
        $del=$db->prepare($db->update('tb_preguntas',array('dm_activo'=>0),"dc_pregunta={$r->id_pregunta}"));
        $db->stExec($del);
        $this->getErrorMan()->showConfirm('La pregunta fue eliminada correctamente');
    }
}

?>
