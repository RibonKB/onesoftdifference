<?php

class editar_cargoFactory extends Factory {

    public $title = 'Edición de cargo';

    public function indexAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView(
                $this->getTemplateURL('editar'), array(
            'checkEv' => $this->Ev_s_n(),
            'checkCargo' => $this->muestra_cargos_a_cargo_XD(), 'cargos' => $this->muestra_cargos(),
            'sel' => $this->ceco(), 'datos' => $this->muestra_datos(), 'cargo' => $this->muestra_nom_cargo(),
            'ev' => $r->id_edit,'cargos_ocup'=>$this->muestra_cargos2()
                )
        );
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function editarCecoAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView(
                $this->getTemplateURL('editar_ceco'), array(
            'sel' => $this->ceco(), 'datos' => $this->muestra_datos(), 'cargo' => $this->muestra_nom_cargo(),
            'ev' => $r->id_edit
                )
        );
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function editarAction() {
        $this->edita();
    }

    private function edita() {
        $r = self::getRequest();
        $this->up_de_cargos();
        if (!isset($r->continuar)) {
            $this->Verificar_evaluador_evaluado();
        }
        $this->borrar();
        $this->guardar_cargos_evaluador();
        $this->getErrorMan()->showConfirm('Cargo actualizado correctamente');
    }

    public function editarNombreAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('editar_nombre'), array('cargo' => $this->muestra_nom_cargo()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function editarenombreAction() {
        $this->verificaCargo();
        $this->up_cargo();
        $this->getErrorMan()->showConfirm('Cargo actualizado correctamente');
    }

    private function up_de_cargos() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $up = $db->prepare($db->update('tb_cargos', array('dm_evalua' => '?'), "dc_cargo={$r->id_evaluador}"));
        $up->bindValue(1, isset($r->evaluador) ? 1 : 0, PDO::PARAM_BOOL);
        if (!isset($r->evaluador)) {
            $this->borrar_evaluados();
        }
        $db->stExec($up);
    }

    private function borrar_evaluados() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $up = $db->prepare($db->update('tb_cargos', array('dc_evaluador' => '?'), "dc_cargo={$r->id_evaluador}"));
        $up->bindValue(1, '', PDO::PARAM_INT);
        $db->stExec($up);
    }

    /**
     * 
     * Verifica el el cargo no tenga evaluador asignado.
     * 
     */
    private function Verificar_evaluador_evaluado() {
        $pass = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        if (isset($r->evaluador)) {
            for ($i = 1; $i < $r->valorf; $i++) {
                $nom = 'evalua' . $i;
                if (isset($r->$nom)) {
                    $ver = $db->prepare($db->select('tb_cargos', 'dc_evaluador', "dc_cargo={$r->$nom} AND dc_evaluador != {$r->id_evaluador}"));
                    $db->stExec($ver);
                    $ver = $ver->fetch(PDO::FETCH_OBJ);
                    if ($ver != false) {
                        $ver1 = $ver->dc_evaluador;
                        if ($ver1 != null) {
                            $this->getErrorMan()->showWarning('Uno o mas de los cargos seleccionados ya tienen un evaluador asignado');
                            exit();
                        }
                    }
                }
            }
        }
    }

    public function editarccAction() {
        $this->verificaCargo2();
        $this->up_cargo2();
        $this->getErrorMan()->showConfirm('Cargo actualizado correctamente');
    }

    /**
     * Varifica que el cargo no exista en la base de datos
     */
    private function verificaCargo() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $select = $db->prepare($db->select('tb_cargos', "*", "dg_cargo = '{$r->cargo}' and dg_area = '{$this->ident_ceco($r->id_cargo)}'"));
        $db->stExec($select);
        $select = $select->fetch(PDO::FETCH_OBJ);

        if ($select != false) {
            $this->getErrorMan()->showWarning('El cargo ingresado ya existe');
            exit();
        }
    }

    /**
     * Varifica que el cargo no exista en la base de datos
     */
    private function verificaCargo2() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $select = $db->prepare($db->select('tb_cargos', "*", "dg_cargo = '{$r->cargo}' and dg_area = '{$this->ident_ceco_cc($r->ceco)}'"));
        $db->stExec($select);
        $select = $select->fetch(PDO::FETCH_OBJ);

        if ($select != false) {
            $this->getErrorMan()->showWarning('El cargo ingresado ya existe');
            exit();
        }
    }

    /**
     * 
     * Deja el campo dc_evaluador en blanco antes de guardar.
     * 
     * 
     */
    private function borrar() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $borrar = $db->prepare($db->update('tb_cargos', array('dc_evaluador' => '?'), "dc_evaluador={$r->id_evaluador}"));
        $borrar->bindValue(1, '', PDO::PARAM_INT);
        $db->stExec($borrar);
    }

    private function guardar_cargos_evaluador() {
        $r = self::getRequest();
        $db = $this->getConnection();
        if (isset($r->evaluador)) {
            for ($i = 1; $i <= $r->valorf; $i++) {
                $nom = 'evalua' . $i;

                if (isset($r->$nom)) {
                    $this->up($db, $r->$nom, $r->id_evaluador);
                }
            }
        }
    }

    /**
     * Actualiza el campo dc_evaluador de la tabla tb_cargos
     * 
     * @param type $db
     * @param type $cargo
     * @param type $evaluador
     * 
     */
    private function up($db, $cargo, $evaluador) {
        $up = $db->prepare($db->update('tb_cargos', array('dc_evaluador' => '?'), "dc_cargo={$cargo}"));
        $up->bindvalue(1, $evaluador, PDO::PARAM_STR);
        $db->stExec($up);
    }

    private function up_cargo() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $up = $db->prepare($db->update('tb_cargos', array('dg_cargo' => '?'), "dc_cargo={$r->id_cargo}"));
        $up->bindValue(1, $r->cargo, PDO::PARAM_STR);
        $db->stExec($up);
    }

    private function up_cargo2() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $up = $db->prepare($db->update('tb_cargos', array('dg_area' => '?'), "dc_cargo={$r->id_cargo}"));

        $up->bindValue(1, $this->ident_ceco_cc($r->ceco), PDO::PARAM_STR);
        $db->stExec($up);
    }

    private function ident_ceco($ceco) {
        $db = $this->getConnection();
        $ceco = $db->prepare($db->select('tb_ceco ce JOIN tb_cargos ca', 'ce.dg_ceco', "ce.dg_ceco = ca.dg_area AND ca.dc_cargo ={$ceco}"));
        $db->stExec($ceco);
        $ceco = $ceco->fetch(PDO::FETCH_OBJ);
        $ceco = $ceco->dg_ceco;
        return $ceco;
    }

    private function ident_ceco_cc($ceco) {
        $db = $this->getConnection();
        $ceco1 = $db->prepare($db->select('tb_ceco ', 'dg_ceco', "dc_ceco={$ceco}"));
        $db->stExec($ceco1);
        $ceco1 = $ceco1->fetch(PDO::FETCH_OBJ);
        $ceco1 = $ceco1->dg_ceco;
        return $ceco1;
    }

    /**
     * enlista los Centros de Costo (CeCo) 
     * 
     * @return type
     * 
     */
    private function ceco() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_ceco', 'dc_ceco , dg_ceco', 'dm_activo=1'));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_ceco] = $crea->dg_ceco;
        }
        return $ar1;
    }

    /**
     * 
     * Mustra los datos del cargo(nombre y CeCo)
     * 
     */
    private function muestra_datos() {

        $r = self::getRequest();
        $db = $this->getConnection();
        $muestra = $db->prepare($db->select('tb_cargos ca JOIN tb_ceco ce', 'ce.dc_ceco', "ca.dc_cargo ={$r->id_edit} AND ce.dg_ceco=ca.dg_area AND ca.dc_empresa={$this->getEmpresa()}"));
        $db->stExec($muestra);
        $r->muestra = $muestra->fetch(PDO::FETCH_OBJ);
        return $r->muestra;
    }

    private function muestra_nom_cargo() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $mu = $db->prepare($db->select('tb_cargos', 'dg_cargo,dc_cargo', "dc_cargo={$r->id_edit}"));
        $db->stExec($mu);
        $r->mu = $mu->fetch(PDO::FETCH_OBJ);

        return $r->mu;
    }

    /**
     * 
     * Muestra todos los cargos 
     * 
     */
    private function muestra_cargos() {
        $r = self::getRequest();
        $arr1 = '';
        $db = $this->getConnection();
        $muestra = $db->prepare($db->select('tb_cargos', 'dc_cargo,dg_cargo,dg_area', "dc_cargo !={$r->id_edit} AND dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($muestra);
        while ($llena = $muestra->fetch(PDO::FETCH_OBJ)) {

            $arr1[$llena->dg_area][$llena->dc_cargo] = $llena->dg_cargo;
        }
        return $arr1;
    }
    
    /**
     * 
     * Muestra todos los cargos que ya tengan evaluador asignado 
     * 
     */
    private function muestra_cargos2() {
        $r = self::getRequest();
        $arr1 = '';
        $db = $this->getConnection();
        $muestra = $db->prepare($db->select('tb_cargos', 'dc_cargo,dg_cargo,dg_area,dc_evaluador', "dc_cargo !={$r->id_edit} AND dc_evaluador != {$r->id_edit} AND dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($muestra);
        while ($llena = $muestra->fetch(PDO::FETCH_OBJ)) {
            if ($llena->dc_evaluador != null || $llena->dc_evaluador != 0)
            {
            $arr1[$llena->dg_area][$llena->dc_cargo] = $llena->dg_cargo;
            }
        }
        return $arr1;
    }

    /**
     * 
     * Indica cuales son los cargos que evalua, en caso de ser evaluador.
     * 
     */
    private function muestra_cargos_a_cargo_XD() {

        $r = self::getRequest();
        $arr1['ck'] = ' ';
        $db = $this->getConnection();
        $muestra = $db->prepare($db->select('tb_cargos', 'dc_cargo', "dc_evaluador ={$r->id_edit} AND dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($muestra);
        while ($llena = $muestra->fetch(PDO::FETCH_OBJ)) {
            $arr1[$llena->dc_cargo] = $llena->dc_cargo;
        }
        return $arr1;
    }

    private function Ev_s_n() {
        $r = self::getRequest();
        $arr1 = '';
        $db = $this->getConnection();
        $select_check = $db->prepare($db->select('tb_cargos', 'dm_evalua', "dc_cargo={$r->id_edit} AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($select_check);
        $select_check = $select_check->fetch(PDO::FETCH_OBJ);
        $select_check = $select_check->dm_evalua;
        if ($select_check != 1) {
            $arr1['check'] = ' ';
        } else {
            $arr1['check'] = 1;
        }

        return $arr1;
    }

}

?>
