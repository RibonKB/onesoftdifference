<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of eliminarPregunta
 *
 * @author Eduardo
 */
class eliminarPreguntaFactory extends Factory {
    //put your code here
    
    public $title='Eliminar Pregunta';
    
    public function indexAction()
    {
        $mostrar =$this->getFormView($this->getTemplateURL('del'),array('pr'=>$this->pregunta()));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }
    public function eliminaAction()
    {
        $this->elimina();
    }
    
        private function pregunta() {
            $r=self::getRequest();
        $ar1 = '';
        $db = $this->getConnection();
        $datos = $db->prepare($db->select(
                'tb_preguntas pr JOIN tb_grupo_preguntas gr ON pr.dc_grupo_pregunta=gr.dc_grupo_pregunta', 
                'pr.dc_pregunta,pr.dg_pregunta,gr.dg_grupo_pregunta', 
                'pr.dm_activo=1  ORDER BY gr.dg_grupo_pregunta'
                ));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $ar1[$crea->dc_pregunta] = $crea->dg_pregunta." - ".$crea->dg_grupo_pregunta;
        }
        return $ar1;
    }
    
    
    private function elimina()
    {
        $r=self::getRequest();
        
        $db=$this->getConnection();
        $des_pr=$db->prepare($db->update('tb_preguntas',array('dm_activo'=>0),"dc_pregunta='{$r->pregunta}'"));
        $db->stExec($des_pr);
        $this->getErrorMan()->showErrorRedirect('La pregunta se elimino correctamente', $this->buildActionUrl('index'));
    }
}

?>
