<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of eliminar_grupoFactory
 *
 * @author Eduardo
 */
class eliminar_grupoFactory extends Factory {

    public $title='Eliminar Grupo';

    public function indexAction() {

        $r=self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('eliminar'), array('grupo' => $this->mostrar_grupo(), 'preguntas' => $this->mostrar_preguntas_asoc(),'id_c'=>$r->id_cargo));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }
    
    public function relacionAction()
    {
        $r=self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('relacion_del'), array('grupo' => $this->mostrar_grupo(), 'preguntas' => $this->mostrar_preguntas_asoc(),'id_c'=>$r->id_cargo));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function delRelAction()
    {
        $this->eliminar_relacion();
        $msg='Se elimino la relación';
        echo $this->getFormView($this->getTemplateURL('listo'),array('msg'=>$msg));
    }
    
    private function eliminar_relacion()
    {
        $r = self::getRequest();
        $db = $this->getConnection();
        $del_grupo_rel=$db->prepare($db->update('tb_grupo_cargo_relacion',array('dm_activo'=>0),"dc_grupo_pregunta={$r->id_grupo} AND dc_cargo={$r->id_cargo}"));
        $db->stExec($del_grupo_rel);
    }
    
    public function eliminarAction() {
        $this->eliminar_preguntas_grupo();
        $this->eliminar_Relaciones();
        $this->eliminar_grupo();
        $this->getErrorMan()->showConfirm('Grupo elimando');
        $msg='Se esta eliminando el grupo y sus relaciones existentes';
        echo $this->getFormView($this->getTemplateURL('listo'),array('msg'=>$msg));
    }


    private function eliminar_grupo() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $del_grupo=$db->prepare($db->update('tb_grupo_preguntas',array('dm_activo'=>0),"dc_grupo_pregunta={$r->id_grupo}"));
        $db->stExec($del_grupo);
    }
    
    private function eliminar_Relaciones()
    {
        $r = self::getRequest();
        $db = $this->getConnection();
        $del_grupo_rel=$db->prepare($db->update('tb_grupo_cargo_relacion',array('dm_activo'=>0),"dc_grupo_pregunta={$r->id_grupo}"));
        $db->stExec($del_grupo_rel);
    }

    private function eliminar_preguntas_grupo()
    {
         $r = self::getRequest();
        $db = $this->getConnection();
        $del_grupo=$db->prepare($db->update('tb_preguntas',array('dc_grupo_pregunta'=>0),"dc_grupo_pregunta={$r->id_grupo}"));
        $db->stExec($del_grupo);
    }

    private function mostrar_grupo() {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        $m_grupo = $db->prepare($db->select('tb_grupo_preguntas', 'dg_grupo_pregunta,dc_grupo_pregunta', "dc_grupo_pregunta={$r->id_grupo}"));
        $db->stExec($m_grupo);
        while ($grupo = $m_grupo->fetch(PDO::FETCH_OBJ)) {
            $arr1[$grupo->dc_grupo_pregunta][$this->mostrar_cargo_asoc($r->id_cargo)] = $grupo->dg_grupo_pregunta;
        }
        return $arr1;
    }

    private function mostrar_cargo_asoc($id_cargo) {
        $r = self::getRequest();
        $db = $this->getConnection();

        $m_cargo = $db->prepare($db->select('tb_cargos', 'dg_cargo,dg_area', "dc_cargo={$id_cargo}"));
        if ($id_cargo != 999) {
            if($id_cargo != 0){
                $db->stExec($m_cargo);
            $m_cargo = $m_cargo->fetch(PDO::FETCH_OBJ);
            $m_cargo = $m_cargo->dg_cargo . " - " . $m_cargo->dg_area;
            }else
            {
                $m_cargo='Sin cargo asignado';
            }
            
        } else {
            $m_cargo='Todos los cargos';
        }
        return $m_cargo;
    }

    private function mostrar_preguntas_asoc() {
        $arr1['pr']='--';
        $r = self::getRequest();
        $db = $this->getConnection();
        $m_preguntas = $db->prepare($db->select('tb_preguntas', 'dg_pregunta,dc_pregunta', "dc_grupo_pregunta={$r->id_grupo} AND dm_activo=1"));
        $db->stExec($m_preguntas);
        while ($pregunta = $m_preguntas->fetch(PDO::FETCH_OBJ)) {
            
            $arr1[$pregunta->dc_pregunta] = $pregunta->dg_pregunta;
            
        {
            
        }
        }
        
        return $arr1;
    }

}

?>
