<?php

class eliminar_cargoFactory extends Factory {
    //put your code here
    public $title='Eliminar cargo';
    public function indexAction()
    {
        $mostrar=$this->getFormView($this->getTemplateURL('eliminar'),array('cargo'=>$this->Cargo_eliminar()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }
    
    public function eliminarAction()
    {
        $r=self::getRequest();
        $db=$this->getConnection();
        $del=$db->prepare($db->update('tb_cargos',array('dm_activo'=>0),"dc_cargo={$r->id_cargo}"));
        $db->stExec($del);
        $this->getErrorMan()->showConfirm('Cargo eliminado');
    }
    
    private function Cargo_eliminar()
    {
        $r=self::getRequest();
        $db=$this->getConnection();
        $ver_cargo=$db->prepare($db->select('tb_cargos','dg_cargo,dg_area,dc_cargo',"dc_cargo={$r->id_del}"));
        $db->stExec($ver_cargo);
        $ver_cargo=$ver_cargo->fetch(PDO::FETCH_OBJ);
        $cargo[$ver_cargo->dc_cargo][$ver_cargo->dg_area]=$ver_cargo->dg_cargo;
        return $cargo;
    }
}

?>
