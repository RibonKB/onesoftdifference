<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of eliminarFuncionarioFactory
 *
 * @author Eduardo
 */
class eliminarFuncionarioFactory extends Factory {
    //put your code here
    
    public function indexAction(){
        echo $this->getFormView($this->getTemplateURL('eliminar'));
    }
    public function eliminarAction(){
        $this->elimina();
    }
    
    private function elimina(){
        $r=self::getRequest();
        $db=$this->getConnection();
        $funcionario = $this->identifica_funcionario($r->fc_rut);
        $elimina=$db->prepare($db->select("tb_funcionario","dc_funcionario","dc_funcionario={$funcionario} AND dm_activo = '0'"));
        $db->stExec($elimina);
if($elimina->fetch() != false){
	$this->getErrorMan()->showAviso("El funcionario ya ha sido eliminado");
}
else{
	$des_func=$db->prepare($db->update("tb_funcionario",array("dm_activo" => '0'),"dc_funcionario = {$funcionario}"));
	$des_user=$db->prepare($db->update("tb_usuario",array("dm_activo" => '0'),"dc_funcionario = {$funcionario}"));
	$db->stExec($des_func);
        $db->stExec($des_user);
	$this->getErrorMan()->showConfirm("El funcionario ha sido eliminado correctamente<br /><strong>Los usuarios asignados al funcionario también fueron eliminados</strong>");
	
}
    }
    
    private function identifica_funcionario($rut){
        $select=$this->coneccion('select','tb_funcionario','dc_funcionario',"dg_rut='{$rut}' AND dc_empresa={$this->getEmpresa()}");
        $select=$select->fetch(PDO::FETCH_OBJ);
        return $select->dc_funcionario;
    }
}

?>
