<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de funcionarios
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/rrhh/cr_funcionario.php" class="loader">Creación</a></li>
		<li class="oo_active"><a href="#">Edición</a></li>
		<li><a href="sites/rrhh/del_funcionario.php" class="loader">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont">
<div class="panes" align="center">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Ingrese el RUT del funcionario que desea editar.</strong><br />(Los criterios de búsqueda son nombre del funcionario, rut del funcionario)");
	$form->Start("sites/rrhh/proc/ed_funcionario.php","ed_func","cValidar");
	$form->Text("RUT funcionario","fc_rut",1,20,"","inputrut");
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[0]+" ( "+row[1]+" )";
}
$("#fc_rut").autocomplete('sites/proc/autocompleter/funcionario.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>
