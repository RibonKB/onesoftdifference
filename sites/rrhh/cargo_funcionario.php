<?php
define("MAIN",1);
require_once("../../inc/init.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$datos = $db->prepare($db->select('tb_ceco','dc_ceco, dg_ceco','dc_empresa = ?'));
$datos->bindValue(1,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('El centro de costo no fue encontrado');
	exit;
}
?>

<div id="secc_bar"> 
	Asignación módulo a ceco
</div>

<div id="main_cont">
<div class="panes">

<?php

$form->Start('sites/rrhh/proc/cargo_funcionario.php','cargo_funcionario');
$form->Header("Seleccione");

$form->Section();
	$form->Select('Modulo','modulo',array(
		'1'=>'Ventas',
		'2'=>'Contabilidad',
		'3'=>'Finanzas',
		'4'=>'Area de servicios',
		'5'=>'Logistica'
	));
$form->EndSection();

$form->Section();
	$form->DBMultiSelect('CECO','ceco','tb_ceco',array('dc_ceco','dg_ceco'));
$form->EndSection();

$form->End('Crear','addbtn');
?>

</div>
</div>

<script type="text/javascript">
$('#ceco').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>