<?php

class CrearFuncionarioFactory extends Factory {

    //put your code here
    public function indexAction() {
        echo $this->getFormView($this->getTemplateURL('crear'), array('cargo' => $this->cargos(), 'afp' => $this->afp()
            , 'tcontr' => $this->tcontrato(), 'isapre' => $this->isapre(), 'ceco' => $this->ceco()));
    }

    public function crearAction() {
        $this->verificarImagenTa();
        $this->verificarImagenTi();
        $this->verificarFuncionario();
        $this->cargoVsCeco();
        $this->crearFunc();
    }

    private function verificarImagenTa() {
        switch ($_FILES['fc_foto']['error']) {
            case 1:
            case 2: $this->getErrorMan()->showWarning("El tamaño del archivo supera el máximo permitido.");
                exit();
                break;
            case 3: $this->getErrorMan()->showWarning("Se ha cancelado la transferencia del archivo.");
                exit();
                break;
        }
    }

    private function verificarImagenTi() {
        $tipos = array('image/gif', 'image/pjpeg', 'image/jpeg', 'image/jpg', 'image/x-png', 'image/png');

        if ($_FILES['fc_foto']['size']) {
            if (!in_array($_FILES['fc_foto']['type'], $tipos)) {
                $this->getErrorMan()->showWarning("El archivo especificado no es una imagen válida.<br />Los formatos soportados son gif, jpeg y png");
                exit();
            }
        }
    }

    private function verificarFuncionario() {
        $db = $this->getConnection();
        $r = self::getRequest();
        $func = $db->prepare($db->select("tb_funcionario", "dc_funcionario", "dg_rut = '{$r->func_rut}' AND dc_empresa = {$this->getEmpresa()}"));
        $db->stExec($func);
        if ($func->fetch() != false) {
            $this->getErrorMan()->showWarning("Ya existe un funcionario utilizando el <strong>rut</strong> especificado,<br />Pruebe con otro o consulte al administrador");
            exit();
        }
    }

    public function crearFunc() {
        $db = $this->getConnection();
        $r = self::getRequest();
        $f_nac = $this->f($r->func_nacimiento);
        $f_ini_contr = $this->f($r->func_ini_contr);
        $f_fin_contr = $this->f($r->func_fin_contr);
        
        if ($r->func_sueldo == '') {
            $f_sueldo = 0;
        } else {
            $f_sueldo = str_replace(",", ".", $r->func_sueldo);
            if (!is_numeric($f_sueldo)) {
                $this->getErrorMan()->showWarning("El formato del sueldo es incorrecto");
                exit();
            }
        }

        if ($_FILES['fc_foto']['size']) {
            $imagen = mysql_real_escape_string(fread(fopen($_FILES['fc_foto']['tmp_name'], 'r'), $_FILES['fc_foto']['size']));
        } else {
            $imagen = "";
        }

        $f_id = $db->prepare($db->insert("tb_funcionario", array(
                    "dg_rut" => '?',
                    "dg_nombres" => '?',
                    "dg_ap_paterno" => '?',
                    "dg_ap_materno" => '?',
                    "df_nacimiento" => '?',
                    "dg_anexo" => '?',
                    "dm_sexo" => '?',
                    "dc_ceco" => '?',
                    "dc_tipo_contrato" => '?',
                    "df_inicio_contrato" => '?',
                    "df_fin_contrato" => '?',
                    "dc_isapre" => '?',
                    "dc_afp" => '?',
                    "dq_sueldo_bruto" => '?',
                    "dc_empresa" => $this->getEmpresa(),
                    "dr_foto_usuario" => "'{$imagen}'",
                    "dg_tipo_foto" => '?',
                    "dc_cargo" => '?',
		    "dm_evaluacion_ticket" => '?'
        )));

        $f_id->bindValue(1, $r->func_rut, PDO::PARAM_STR);
        $f_id->bindValue(2, $r->func_nombres, PDO::PARAM_STR);
        $f_id->bindValue(3, $r->func_ap_paterno, PDO::PARAM_STR);
        $f_id->bindValue(4, $r->func_ap_materno, PDO::PARAM_STR);
        $f_id->bindValue(5, $f_nac, PDO::PARAM_STR);
        $f_id->bindValue(6, $r->func_anexo, PDO::PARAM_INT);
        $f_id->bindValue(7, $r->func_sexo, PDO::PARAM_INT);
        $f_id->bindValue(8, $r->func_ceco, PDO::PARAM_INT);
        $f_id->bindValue(9, $r->func_t_contrato, PDO::PARAM_STR);
        $f_id->bindValue(10, $f_ini_contr, PDO::PARAM_STR);
        $f_id->bindValue(11, $f_fin_contr, PDO::PARAM_STR);
        $f_id->bindValue(12, $r->func_salud, PDO::PARAM_INT);
        $f_id->bindValue(13, $r->func_afp, PDO::PARAM_INT);
        $f_id->bindValue(14, $f_sueldo, PDO::PARAM_INT);
        $f_id->bindValue(15, $_FILES['fc_foto']['type'], PDO::PARAM_STR);
        $f_id->bindValue(16, $r->cargo, PDO::PARAM_INT);
	$f_id->bindValue(17, $r->dm_evaluacion_ticket, PDO::PARAM_BOOL);

        $db->stExec($f_id);
        $this->getErrorMan()->showConfirm("Se ha creado el funcionario correctamente.");
    }

    private function cargos() {
        $ar1 = '';
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_cargos', '*', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $ar1[$crea->dc_cargo] = $crea->dg_cargo . " - " . $crea->dg_area;
        }
        return $ar1;
    }

    private function afp() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_afp', 'dc_afp,dg_afp', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_afp] = $crea->dg_afp;
        }
        return $ar1;
    }

    private function tcontrato() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_tipo_contrato', 'dc_tipo_contrato,dg_tipo_contrato', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_tipo_contrato] = $crea->dg_tipo_contrato;
        }
        return $ar1;
    }

    private function isapre() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_isapre', 'dc_isapre , dg_isapre', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_isapre] = $crea->dg_isapre;
        }
        return $ar1;
    }

    private function ceco() {
        $ar1 = '';
        $n = 0;
        $db = $this->getConnection();
        $datos = $db->prepare($db->select('tb_ceco', 'dc_ceco , dg_ceco', "dm_activo=1 AND dc_empresa={$this->getEmpresa()}"));
        $db->stExec($datos);
        while ($crea = $datos->fetch(PDO::FETCH_OBJ)) {
            $n++;
            $ar1[$crea->dc_ceco] = $crea->dg_ceco;
        }
        return $ar1;
    }

    private function cargoVsCeco() {
        $r = self::getRequest();
        $r->func_ceco;
        $r->cargo;
        $db = $this->getConnection();
        $ceco = $db->prepare($db->select("tb_ceco", "dg_ceco", "dc_ceco='{$r->func_ceco}'"));
        $db->stExec($ceco);
        $ceco = $ceco->fetch(PDO::FETCH_OBJ);
        $ceco = $ceco->dg_ceco;

        $cargo = $db->prepare($db->select("tb_cargos", "dg_area", "dc_cargo='{$r->cargo}'"));
        $db->stExec($cargo);
        $cargo = $cargo->fetch(PDO::FETCH_OBJ);
        $cargo = $cargo->dg_area;

        if ($ceco != $cargo) {
            $this->getErrorMan()->showWarning("Centro de costo y Cargos no coinciden");
            exit();
        }
    }

    private function f($fecha) {
        $f = explode("/", $fecha);
        if (isset($f[2])) {
            $f = $f[2] . "-" . $f[1] . "-" . $f[0] . " 00:00:00";
        } else {
            $f = "0000-00-00 00:00:00";
        }
        return $f;
    }

}

?>
