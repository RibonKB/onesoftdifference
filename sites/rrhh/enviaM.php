    <?php

    require_once("inc/mail/class.phpmailer.php");

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of envia_mail
     *
     * @author Eduardo
     */
    class enviaM extends Factory{

        public function Enviar_mail($destinatario, $subjet,$mensaje) {
            $mail=new PHPMailer();
            $mail->IsSMTP();
            $mail->Host = 'smtp.office365.com';
            $mail->SMTPSecure = 'tls';
            $mail->SMTPKeepAlive = true;
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->Username = 'evaluacion@adischile.cl';
            $mail->Password = '1softP4ssW4rD';
            //$mail->Password = '1softP1ssW4rD';
            $mail->CharSet = 'UTF-8';
            $mail->SetFrom('evaluacion@adischile.cl', 'Evaluación Funcionarios');

            $mail->AddAddress($destinatario, '');

            $mail->IsHTML(true);
            $mail->Subject = $subjet;
            $mail->Body = $mensaje;
            if (!$mail->Send()) {
                $this->getErrorMan()->showWarning("No se pudo enviar el correo a los destinatarios.<br />
    compruebe los datos de acceso al servidor SMTP");
            }
        }

        public function subjet($typo_eval,$nombre)
        {
            $subjet=$typo_eval." de ".$nombre;
            return $subjet;
        }

        public function getMensaje($tipo_evaluacion, $user_nom,$promedio, $comentario=null) {
            $coment='';
            if($comentario)
            {
                $coment=$comentario;
            }else
            {
                $coment='El Usuario no ingresó comentario';
            }
            $mensaje = 'Resultado de la ' . $tipo_evaluacion . " del funcionario: " . $user_nom . "</br></br>
            <table border='1'>
            <tr style='font-weight:bolder' align='center'><td>Nombre Funcionario</td><td>Promedio obtenido</td><td>Comentario de la evaluación</td></tr>
            <tr><td  align='center'>" . $user_nom . "</td><td align='center'>" . $promedio . "</td><td align='center'>" . $coment . "</td> </tr>
            </table></br></br>";
            return $mensaje;
        }

        public function Tipo_evaluacion_mensaje($tipo_evaluacion) {
            $mensaje_t_eval = '';
            if ($tipo_evaluacion == 1) {
                $mensaje_t_eval = 'Evaluación';
            } else if ($tipo_evaluacion == 2) {
                $mensaje_t_eval = 'Auto-Evaluación';
            } else {
                $mensaje_t_eval = 'Evaluación en consenso';
            }
            return $mensaje_t_eval;
        }

      
    }

    ?>
