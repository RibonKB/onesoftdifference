<?php

require_once('sites/rrhh/FuncionesEvaluaciones.php');

/**
 * Description of Evaulacion_consensoFactory
 *
 * @author Eduardo
 */
class Evaluacion_consensoFactory extends FuncionesEvaluaciones {

    public $title = 'Evaluación en Consenso';

    public function indexAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('evaluacion'), array("evalua" => $this->preguntas($this->get_cargo($r->user_a_ev_cons),1,$this->dc_user($r->user_a_ev_cons)), "dc_user_ev" => $this->dc_user($r->user_a_ev_cons), 'fun' => $this->funcionario(), 'compromisos_creados' => $this->ver_compromisos_creados($r->user_a_ev)));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function guardarAction() {
        $r = self::getRequest();
        $this->guardar(3, $this->getUserData()->dc_usuario, $r->dc_us_eva);
    }

    private function funcionario() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $func_nom = $db->prepare($db->select('tb_funcionario', 'dg_nombres,dg_ap_paterno', "dc_funcionario={$r->user_a_ev_cons}"));
        $db->stExec($func_nom);
        $func_nom = $func_nom->fetch(PDO::FETCH_OBJ);
        $func_nom = $func_nom->dg_nombres . " " . $func_nom->dg_ap_paterno;
        return $func_nom;
    }

}

?>
