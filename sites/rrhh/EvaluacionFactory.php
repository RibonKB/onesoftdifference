<?php

require_once('sites/rrhh/FuncionesEvaluaciones.php');

/**
 * Description of EvaluacionFactory
 *
 * @author Eduardo
 */
class EvaluacionFactory extends FuncionesEvaluaciones {

    public $title = 'Evaluación';
    private $dias=28;

    //put your code here

    public function indexAction() {
        $this->evalua01();
    }

    public function autoEvaAction() {
        $r=self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('evaluacion'), 
                array('preguntas' => $this->preguntas($this->getUserData()->dc_cargo),
            'compromisos_creados' => $this->ver_compromisos_creados($this->getUserData()->dc_usuario)));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function guardarAction() {
        $this->guardar(2, $this->getUserData()->dc_usuario);
    }

    private function user_cargo_area_2() {
        $ar1 = null;
        $db = $this->getConnection();
        $mostrar = $db->prepare($db->select('tb_funcionario fu 
                                                    JOIN tb_cargos ca', 'fu.dg_nombres,
                                             fu.dg_ap_paterno,
                                             fu.dg_ap_materno,
                                             ca.dg_cargo,
                                             ca.dg_area,
                                             ca.dc_cargo,
                                             fu.dc_funcionario', "ca.dc_evaluador={$this->getUserData()->dc_cargo} 
                                                    AND ca.dc_cargo = fu.dc_cargo"
        ));
        $db->stExec($mostrar);

        while ($ss = $mostrar->fetch(PDO::FETCH_OBJ)) {
            $f = $this->fecha_evaluacion($ss->dc_funcionario);
            if ($f == 'no') {
                $g = $this->ev($ss->dc_funcionario);
                if ($g == 'no') {
                    $c = $this->fecha_consenso($ss->dc_funcionario);
                    if ($c == 'si') {
                        $ar1[$ss->dc_funcionario] =
                                $ss->dg_nombres . " " .
                                $ss->dg_ap_paterno . " " .
                                $ss->dg_ap_materno . " / " .
                                $ss->dg_cargo . "-" .
                                $ss->dg_area;
                    }
                }
            }
        }
        return $ar1;
    }

    /**
     * 
     * Verifica si el usuario puede evaluar a otro usuario o no.
     * 
     * Si no puede, es enviado automaticamente a la auto-evaluación.
     * 
     */
    private function evalua01() {
        $cargo_usuario =
                        $this->
                        getUserData()->
                dc_cargo;
        $db = $this->getConnection();
        $eva = $db->prepare($db->select("tb_cargos", "dm_evalua", "dc_cargo={$cargo_usuario}"));
        $db->stExec($eva);
        $c = $eva->fetch(PDO::FETCH_OBJ);
        if ($c != false) {

            if ($c->dm_evalua == 1) {

                if ($this->user_cargo_area() != null) {
                    if ($this->ev($this->getUserData()->dc_funcionario) == 'no') {//no realiza auto-evaluacion
                        if ($this->user_cargo_area_2() != null) {
                            //realiza consenso
                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'si', 'conse' => 'si', 'ev_fu' => 'si', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom())); //lstEv = listo auto-evaluacion    conse=consenso (si muestra o no la opcion)          ev_fu=evaluacion funcionario(si muestra o no la opcion)
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        } else {

                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'si', 'conse' => 'no', 'ev_fu' => 'si', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom()));
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        }
                    } else {

                        if ($this->user_cargo_area_2() != null) {//realiza consenso
                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'no', 'conse' => 'si', 'ev_fu' => 'si', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom()));
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        } else {

                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'no', 'conse' => 'no', 'ev_fu' => 'si', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom()));
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        }
                    }
                } else {
                    if ($this->ev($this->getUserData()->dc_funcionario) == 'no') {
                        if ($this->user_cargo_area_2() != null) {//realiza consenso
                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'si', 'conse' => 'si', 'ev_fu' => 'no', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom()));
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        } else {
                            //$msg = "La auto evaluación ya fue realizada";
                            // $this->getErrorMan()->showAviso('La auto-evaluación ya fue realizada.');

                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'si', 'conse' => 'no', 'ev_fu' => 'no', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom()));
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                            //$mostrar = $this->getFormView($this->getTemplateURL('listo'), array("msg" => $msg, 'd' => $this->calcular_dias()));
                            //echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        }
                    } else {

                        if ($this->user_cargo_area_2() != null) {//realiza consenso
                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'no', 'conse' => 'si', 'ev_fu' => 'no', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom()));
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        } else {
                            $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'no', 'conse' => 'si', 'ev_fu' => 'no', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom()));
                            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                        }
                    }
                }
            } else {
                if (!check_permiso(77)) {
                    $re = $this->ev($this->getUserData()->dc_funcionario);
                    if ($re == 'no') {
                        //$msg = "La auto evaluación ya fue realizada";
                        $this->getErrorMan()->showAviso('La auto-evaluación ya fue realizada.');
                        //$mostrar = $this->getFormView($this->getTemplateURL('listo'), array("msg" => $msg, 'd' => $this->calcular_dias()));
                        //echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                    } else {
                        $mostrar = $this->getFormView($this->getTemplateURL('evaluacion'), array('preguntas' => $this->preguntas($this->getUserData()->dc_cargo), 'compromisos_creados' => $this->ver_compromisos_creados($this->getUserData()->dc_usuario)));
                        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                    }
                } else {
                    $re = $this->ev($this->getUserData()->dc_funcionario);
                    if ($re == 'no') {
                        $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'si', 'conse' => 'no', 'ev_fu' => 'no', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom(), 'solo_ve' => 1));
                        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                    } else {
                        $mostrar = $this->getFormView($this->getTemplateURL('evaluacion2'), array('usr' => $this->user_cargo_area(), 'lstEv' => 'no', 'conse' => 'no', 'ev_fu' => 'no', 'user_cons' => $this->user_cargo_area_2(), 'tbl_prom' => $this->funcionarios_prom(), 'solo_ve' => 1));
                        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
                    }
                }
            }
        
        }
    }

    /*
     * ---------------------------------------
     * Verificacion de fechas de evaluaciones.
     * ---------------------------------------
     */

    /**
     * 
     * Verifica el tiempo de la ultima auto-evaluación
     * si son mas de 20 dias se evalua.
     * 
     * @return boolean
     */
    private function ev($user) {
        $auto_evalua = '';
        $db = $this->getConnection();
        $d = $db->prepare($db->select('tb_funcionario', '*', "dc_funcionario = {$user}  AND df_U_A_evaluacion > (DATE_SUB(NOW(),INTERVAL {$this->dias} day))"));
        $db->stExec($d);
        $d = $d->fetch(PDO::FETCH_OBJ);

        if ($d != false) {

            $auto_evalua = 'no'; //no se cumplen los 20 dias
        } else {

            $auto_evalua = 'si'; //ya se cumplieron los 20 dias
        }
        return $auto_evalua;
    }

    /**
     * 
     * Verifica la fecha de la ultima evaluacion del funcionario.
     * 
     * @param type $user
     * @return boolean
     * 
     */
    private function fecha_evaluacion($user) {

        $evalua = '';
        $db = $this->getConnection();
        $f = $db->prepare($db->select('tb_funcionario', '*', "dc_funcionario={$user} AND df_ultima_evaluacion >(DATE_SUB(NOW(),INTERVAL {$this->dias} day))"
        ));    //selecciona todo de la tabla funcionario cuando el ID del funcionario corresponda a lo contenido en la variable $user 
        //y cuando la ultima evaluacion sea mayor a la fecha actual menos 20 dias.
        //
            $db->stExec($f);
        $f = $f->fetch(PDO::FETCH_OBJ);
        if ($f != false) {//si lo encuentra, quiere decir que aun no es tiempo de realizar la evaluacion del funcionario
            $evalua = 'no'; //aun no se cumplen los 20 dias
        } else {

            $evalua = 'si'; //se cumplieron los 20 dias
        }
        return $evalua;
    }

    /**
     * 
     * Verifica las fechas de la ultima evaluacion 
     * y de la ultima auto-evaluacion.
     * 
     * si ambas fueron realizadas dentro en una perido de 20
     * dias a la fecha actual.
     * 
     * se habilitará la evaluacion en consenso.
     * 
     * @param type $user
     * 
     * El parametro $user contiene el dc_funcionario el cual corresponde
     * al ID del duncionario.
     * 
     * @return boolean
     * 
     * Si ambas fechas estan dentro del lapso de 20 dias, retorna una booleano TRUE,
     * de lo contrario, retornará FALSE.
     */
    private function fecha_consenso($user) {
        $consenso = '';
        $db = $this->getConnection();
        $f = $db->prepare($db->select('tb_funcionario', '*', "dc_funcionario={$user} AND df_evaluacion_consenso >(DATE_SUB(NOW(),INTERVAL {$this->dias} day))"
        ));    //selecciona todo de la tabla funcionario cuando el ID del funcionario corresponda a lo contenido en la variable $user 
        //y cuando la ultima evaluacion sea mayor a la fecha actual menos 20 dias.
        //
            $db->stExec($f);
        $f = $f->fetch(PDO::FETCH_OBJ);
        if ($f != false) {//si lo encuentra, quiere decir que aun no es tiempo de realizar la evaluacion del funcionario
            $consenso = 'no'; //aun no se cumplen los 20 dias
        } else {
            $consenso = 'si'; //se cumplieron los 20 dias
        }
        return $consenso;
    }

    /*
     * -----------------------------------------
     * Fin verificacion de fechas de evaluaciones.
     * -----------------------------------------
     */

    /**
     * 
     * muestra en pantalla los promedios de los funcionarios a evaluar
     * 
     * 
     * @return int
     */
    private function funcionarios_prom() {
        $ar1 = '';
        $permiso = check_permiso(77);
        $condicion = "AND ca.dc_evaluador={$this->getUserData()->dc_cargo} 
                                                    AND ca.dc_cargo = fu.dc_cargo ";
        if ($permiso != FALSE):
            $condicion = '';
        endif;
        $db = $this->getConnection();
        $mostrar = $db->prepare($db->select('tb_funcionario fu 
                                                    JOIN tb_cargos ca', 'fu.dg_nombres,
                                             fu.dg_ap_paterno,
                                             fu.dg_ap_materno,
                                             fu.dc_funcionario,
                                             ca.dg_cargo,
                                             ca.dg_area,
                                             ca.dc_cargo,
                                             fu.dc_funcionario', "fu.dm_activo=1 {$condicion} AND fu.dc_empresa={$this->getEmpresa()}", array('order_by' => 'fu.dg_nombres')
        ));

        $db->stExec($mostrar);

        while ($ss = $mostrar->fetch(PDO::FETCH_OBJ)) {
            $condicion2 = "ev.dc_usuario_quien={$this->getUserData()->dc_usuario} 
                            AND ";
            if ($permiso != FALSE):
                $condicion2 = '';
            endif;
            
            $f = $this->fecha_evaluacion($ss->dc_funcionario);
            if ($f == 'no') {
                $nota_evaluacion = $db->prepare($db->select('tb_evaluacion ev JOIN tb_funcionario fu', 'ev.dc_promedio,fu.df_ultima_evaluacion,ev.df_creacion', "  ev.dc_usuario_a_quien={$this->dc_user($ss->dc_funcionario)} 
                                AND fu.df_ultima_evaluacion=ev.df_creacion AND ((DAY(fu.df_ultima_evaluacion)-DAY(NOW()))*-1)<{$this->dias} AND ev.dc_tipo=1"
                ));
                $db->stExec($nota_evaluacion);
                $nota_evaluacion = $nota_evaluacion->fetch(PDO::FETCH_OBJ);
                if ($nota_evaluacion != false) {
                    $nota_evaluacion = $nota_evaluacion->dc_promedio;
                    $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['evaluacion'] = $nota_evaluacion;
                } else {
                    $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['evaluacion'] = 0;
                }
            } else {
                $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['evaluacion'] = 0;
            }
            $g = $this->ev($ss->dc_funcionario);
            if ($g == 'no') {
                $nota_auto_evaluacion = $db->prepare($db->select('tb_evaluacion ev JOIN tb_funcionario fu', 'ev.dc_promedio', "ev.dc_usuario_quien={$this->dc_user($ss->dc_funcionario)} 
                            AND ev.dc_usuario_a_quien={$this->dc_user($ss->dc_funcionario)} 
                                AND fu.df_U_A_evaluacion=ev.df_creacion AND ((DAY(fu.df_U_A_evaluacion)-DAY(NOW()))*-1)<{$this->dias} AND ev.dc_tipo=2"
                ));
                $db->stExec($nota_auto_evaluacion);
                $nota_auto_evaluacion = $nota_auto_evaluacion->fetch(PDO::FETCH_OBJ);
                if ($nota_auto_evaluacion != false) {
                    $nota_auto_evaluacion = $nota_auto_evaluacion->dc_promedio;
                    $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['auto-evaluacion'] = $nota_auto_evaluacion;
                } else {
                    $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['auto-evaluacion'] = 0;
                }
            } else {
                $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['auto-evaluacion'] = 0;
            }

            if ($ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['auto-evaluacion'] != 0 && $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['evaluacion'] != 0) {
                $nota_evaluacion_cons = $db->prepare($db->select('tb_evaluacion ev JOIN tb_funcionario fu', 'ev.dc_promedio', " $condicion2 ev.dc_usuario_a_quien={$this->dc_user($ss->dc_funcionario)} 
                                AND fu.df_evaluacion_consenso=ev.df_creacion AND ((DAY(fu.df_evaluacion_consenso)-DAY(NOW()))*-1)<{$this->dias} AND ev.dc_tipo=3"
                ));
                $db->stExec($nota_evaluacion_cons);
                $nota_evaluacion_cons = $nota_evaluacion_cons->fetch(PDO::FETCH_OBJ);
                if ($nota_evaluacion_cons != false) {
                    $nota_evaluacion_cons = $nota_evaluacion_cons->dc_promedio;
                    $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['evaluacion_consenso'] = $nota_evaluacion_cons;
                } else {
                    $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['evaluacion_consenso'] = 0;
                }
            } else {
                $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['evaluacion_consenso'] = 0;
            }
            $ar1[$ss->dg_nombres . " " . $ss->dg_ap_paterno . " " . $ss->dg_ap_materno]['id_funcionario'] = $ss->dc_funcionario;
        }
        return $ar1;
    }

    /**
     * 
     * Retorna array con los funcionarios a evaluar.
     * 
     * @return string
     */
    private function user_cargo_area() {
        $ar1 = '';
        $db = $this->getConnection();
        $mostrar = $db->prepare($db->select('tb_funcionario fu 
                                                    JOIN tb_cargos ca', 'fu.dg_nombres,fu.dg_ap_paterno,fu.dg_ap_materno,ca.dg_cargo,ca.dg_area,ca.dc_cargo,fu.dc_funcionario', "ca.dc_evaluador={$this->getUserData()->dc_cargo} AND ca.dc_cargo = fu.dc_cargo"
        ));
        $db->stExec($mostrar);

        while ($ss = $mostrar->fetch(PDO::FETCH_OBJ)) {
            $f = $this->fecha_evaluacion($ss->dc_funcionario);
            if ($f == 'si') {

                $ar1[$ss->dc_funcionario] =
                        $ss->dg_nombres . " " .
                        $ss->dg_ap_paterno . " " .
                        $ss->dg_ap_materno . " / " .
                        $ss->dg_cargo . "-" .
                        $ss->dg_area;
            }
        }

        return $ar1;
    }
    //----------------------------------------------------------
    //----------------------------------------------------------

    public function VerEvaluacionAction() {
        $request = $this->getRequest();
        $funcionario_nombre = $request->funcionario_nombre;
        $funcionario_id = $request->funcionario_id;
        $tipo_eval = $request->tipo_eval;
        $evaluacion=$this->Get_evaluacion($funcionario_id, $tipo_eval);
        $mostrar = $this->getFormView($this->getTemplateURL('evaluacion_lista'), array(
            'nombre_funcionario' => $funcionario_nombre,
            
            'datos' => $evaluacion['datos'],
            'tipo_evaluacion' => $this->get_tipo_eval($tipo_eval),
            'funcionario_id' => $funcionario_id,
            'tipo_eval' => $tipo_eval
        ));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    private function Get_evaluacion($funcionario_id, $tipo_eval) {
        $arr1 = '';
        $f;
        $datos = $this->coneccion('select', 'tb_evaluacion ev
                                 LEFT JOIN tb_respuestas re ON re.dc_evaluacion=ev.dc_evaluacion
                                 LEFT JOIN tb_preguntas pr ON pr.dc_pregunta=re.dc_pregunta
                                 LEFT JOIN tb_grupo_preguntas gr ON gr.dc_grupo_pregunta=pr.dc_grupo_pregunta', 'gr.dg_grupo_pregunta,
                                 pr.dg_pregunta,
                                 re.dc_calificacion,
                                 re.dg_razon,
                                 ev.df_creacion', "ev.dc_evaluacion={$this->GetLast_Evaluacion($this->dc_user($funcionario_id), $tipo_eval)} 
                                 AND pr.dm_activo=1");
         while($llena=$datos->fetch(PDO::FETCH_OBJ)){
             if(!$llena->dg_razon){
                 $llena->dg_razon='Sin Comentario';
            }
            $arr1[$llena->dg_grupo_pregunta][$llena->dg_pregunta][$llena->dc_calificacion] = $llena->dg_razon;
            $f=$llena->df_creacion;
        }
        return array('datos'=>$arr1,'fecha'=>$f);
    }

    private function get_tipo_eval($tipo_eval){
        $evaluacion='';
        switch ($tipo_eval):
            case 1:
               $evaluacion='Evaluación';
                break;
            case 2:
               $evaluacion='Auto-Evaluación';
                break;
            case 3:
               $evaluacion='Evaluación Consenso';
                break;
        endswitch;
        return $evaluacion;
    }

    public function PrintVersionAction() {
        $request = $this->getRequest();
        $funcionario_nombre = $request->funcionario_nombre;
        $funcionario_id = $request->funcionario_id;
        $tipo_eval = $request->tipo_eval;
        $evaluacion=$this->Get_evaluacion($funcionario_id, $tipo_eval);
        echo $mostrar = $this->getFormView($this->getTemplateURL('ver_imp'), array(
            'rut'=>$this->rutFun($funcionario_id),
    'nombre_funcionario' => $funcionario_nombre,
    'datos' => $evaluacion['datos'],
    'tipo_eval' => $this->get_tipo_eval($tipo_eval),
            'fecha'=>$evaluacion['fecha'],
    'db' => $this->getConnection(),
    'empresa' => $this->getEmpresa(),
    'promedio' => $this->Prom($funcionario_id, $request->tipo_eval)
        ));
}

    private function Prom($id,$te){
        $campo='';
        switch($te):
            case 1:
                $campo='df_ultima_evaluacion';
                break;
            case 2:
                $campo='df_U_A_evaluacion';
                break;
            case 3:
                $campo='df_evaluacion_consenso';
                break;
        endswitch;
        
        $select=$this->coneccion('select',"tb_funcionario fu LEFT JOIN tb_evaluacion ev ON ev.df_creacion=fu.{$campo}",'dc_promedio,dg_comentario',"dc_funcionario={$id} AND dc_tipo={$te}");
        $select=$select->fetch(PDO::FETCH_OBJ);
        if(!$select->dg_comentario){
            $select->dg_comentario='Sin Comentario';
        }
        return $select;
    }
    
    private function rutFun($id){
        $s=$this->coneccion('select','tb_funcionario','dg_rut',"dc_funcionario={$id}");
        $s=$s->fetch(PDO::FETCH_OBJ);
        return $s->dg_rut;
    }
    
}

?>
