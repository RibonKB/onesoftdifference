<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">
	Gestión de funcionarios
</div>
<div id="main_cont">

<br />
<br />
<div align="center" class="opciones">
	<a href="sites/rrhh/cr_funcionario.php" class="loader"><img src="images/submenu/creacionfuncionarios.jpg" alt="Creaci&oacute;n Funcionarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Creación</div></a>
	<a href="sites/rrhh/ed_funcionario.php" class="loader"><img src="images/submenu/edicionfuncionarios.jpg" alt="Edici&oacute;n Funcionarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Edición</div></a>
	<a href="sites/rrhh/del_funcionario.php" class="loader"><img src="images/submenu/eliminarfuncionarios.jpg" alt="Eliminar Funcionarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Eliminación</div></a>
</div>

</div>