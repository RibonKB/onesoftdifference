

<?php

require_once('sites/rrhh/enviaM.php');

class FuncionesEvaluaciones extends Factory {

    private function tb_evaluacion($fecha, $tipo_eval, $db, $user1, $coment = '', $user2 = null) {
        $tb_evaluacion = $db->prepare($db->insert('tb_evaluacion', array(
                    'df_creacion' => '?',
                    'dc_usuario_quien' => '?',
                    'dc_usuario_a_quien' => '?',
                    'dg_comentario' => '?',
                    'dc_tipo' => '?'
        )));

        $tb_evaluacion->bindValue(1, $fecha, PDO::PARAM_STR);
        $tb_evaluacion->bindValue(2, $user1, PDO::PARAM_INT);
        if ($user2) {
            $tb_evaluacion->bindValue(3, $user2, PDO::PARAM_INT);
        } else {
            $tb_evaluacion->bindValue(3, $user1, PDO::PARAM_INT);
        }

        $tb_evaluacion->bindValue(4, $coment, PDO::PARAM_STR);
        $tb_evaluacion->bindValue(5, $tipo_eval, PDO::PARAM_INT);
        $db->stExec($tb_evaluacion);
        $eva = $db->lastInsertId('dc_evaluacion');
        return $eva;
    }

    private function get_dc_funcionario($user1, $db, $user2 = null) {
        $dc_user = '';
        if ($user2) {
            $dc_user = $user2;
        } else {
            $dc_user = $user1;
        }
        $s = $db->prepare($db->select('tb_usuario', "dc_funcionario", "dc_usuario={$dc_user}"));
        $db->stExec($s);
        $s = $s->fetch(PDO::FETCH_OBJ);
        $s = $s->dc_funcionario;
        return $s;
    }

    private function UpdateFuncionario($dc_funcionario, $fecha, $db, $df_ev) {
        $up = $db->prepare($db->update('tb_funcionario', array($df_ev => '?'), "dc_funcionario={$dc_funcionario}"));
        $up->bindValue(1, $fecha, PDO::PARAM_STR);
        $db->stExec($up);
    }

    private function tb_respuesta($p, $n, $ra, $eva, $db) {
        $tb_respuesta = $db->prepare($db->insert('tb_respuestas', array(
                    'dc_pregunta' => '?',
                    'dc_calificacion' => '?',
                    'dg_razon' => '?',
                    'dc_evaluacion' => '?'
        )));
        $tb_respuesta->bindValue(1, $p, PDO::PARAM_INT);
        $tb_respuesta->bindValue(2, $n, PDO::PARAM_INT);
        $tb_respuesta->bindValue(3, $ra, PDO::PARAM_STR);
        $tb_respuesta->bindValue(4, $eva, PDO::PARAM_INT);

        $db->stExec($tb_respuesta);
    }

    private function promedio($last_insert, $promedio, $db) {
        $guarda_promedio = $db->prepare($db->update('tb_evaluacion', array('dc_promedio' => '?'), "dc_evaluacion={$last_insert}"));
        $guarda_promedio->bindvalue(1, number_format($promedio, 1), PDO::PARAM_STR);
        $db->stExec($guarda_promedio);
    }

    private function Tipo_evaluacion($tipo_evaluacion) {
        $evaluacion = '';
        if ($tipo_evaluacion == 1) {
            $evaluacion = 'df_ultima_evaluacion';
        } else if ($tipo_evaluacion == 2) {
            $evaluacion = 'df_U_A_evaluacion';
        } else if ($tipo_evaluacion == 3) {
            $evaluacion = 'df_evaluacion_consenso';
        }
        return $evaluacion;
    }

    private function Calcula_promedio($db, $r, $last_insert) {
        $cant_notas = 0;
        $suma_notas = 0;
        for ($i = 1; $i <= $r->cant_n; $i++) {
            $n = "nota_" . $i;
            $p = "pregunta" . $i;
            $ra = "razon" . $i;
            if (isset($r->$n)) {
                $suma_notas = $suma_notas + $r->$n;
                $cant_notas++;
                $this->tb_respuesta($r->$p, $r->$n, $r->$ra, $last_insert, $db);
            }
        }
        $promedio = $suma_notas / $cant_notas;
        return $promedio;
    }

    /**
     * Guarda los datos de la base de datos
     * 
     */
    protected function guardar($tipo_evaluacion, $user1, $user2 = null) {
        $r = self::getRequest();
        $db = $this->getConnection();
        $fecha = date('Y-m-d H:i:s');
        $last_insert = $this->tb_evaluacion($fecha, $tipo_evaluacion, $db, $user1, $r->comentario, $user2);
        $dc_funcionario = $this->get_dc_funcionario($user1, $db, $user2);
        $evaluacion = $this->Tipo_evaluacion($tipo_evaluacion);
        $this->UpdateFuncionario($dc_funcionario, $fecha, $db, $evaluacion);
        $promedio = $this->Calcula_promedio($db, $r, $last_insert);
        $this->promedio($last_insert, $promedio, $db);
        $e = new enviaM();
        $e->Enviar_mail(
                $this->get_detinatario($dc_funcionario), $e->subjet(
                        $e->Tipo_evaluacion_mensaje($tipo_evaluacion), $this->nombre_funcionario($dc_funcionario)
                ), $e->getMensaje(
                        $e->Tipo_evaluacion_mensaje($tipo_evaluacion), $this->nombre_funcionario($dc_funcionario), number_format($promedio)
                )
        );
        $msg = "Datos enviados correctamente.... El promedio de la Evaluación es: " . number_format($promedio, 1);
        if ($user2) {
            $mostrar = $this->getFormView($this->getTemplateURL('listo'), array('msg' => $msg, 'compromisos_creados' => $this->ver_compromisos_creados($user2)));
            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        } else {
            $mostrar = $this->getFormView($this->getTemplateURL('listo'), array('msg' => $msg));
            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        }
    }

    protected function preguntas($dc_cargo,$c=0,$us=NULL) {
    
        $a = '';
        $db = $this->getConnection();
        $grupo = $this->Get_GrupoPreguntas($dc_cargo, $db);
        while ($crea = $grupo->fetch(PDO::FETCH_OBJ)) {
            $sel_pregunta =
                    $db->prepare(
                            $db->select(
                                    'tb_preguntas pr JOIN 
                                        tb_grupo_preguntas grp', 
                                    'pr.dc_pregunta,
                                        pr.dg_pregunta,
                                        grp.dg_grupo_pregunta', 
                                    "grp.dc_grupo_pregunta={$crea->dc_grupo_pregunta} 
                                        AND pr.dc_grupo_pregunta={$crea->dc_grupo_pregunta} 
                                            AND grp.dc_empresa={$this->getEmpresa()}
                                                AND pr.dm_activo=1"
                                            )
                                );
            $db->stExec($sel_pregunta);
            while ($crea2 = $sel_pregunta->fetch(PDO::FETCH_OBJ)) {
                $a[$crea2->dg_grupo_pregunta][$crea2->dc_pregunta] = $crea2->dg_pregunta;
                if($c):
                    $a['notas_AE']=$this->Get_Notas_AutoEval($us);
                    $a['notas_Eval']=$this->Get_Notas_Evaluacion($us);
                endif;
                
            }
        }
        return $a;
    }

    private function Get_Notas_AutoEval($us){
        $ultima_evaluacion=$this->GetLast_Evaluacion($us, 2);
        $notas=$this->get_notas($ultima_evaluacion);
        return $notas;
    }
    private function Get_Notas_Evaluacion($us){
        $ultima_evaluacion=$this->GetLast_Evaluacion($us, 1);
        $notas=$this->get_notas($ultima_evaluacion);
        return $notas;
    }
    private function get_notas($L_E){
        $arr1='';
        $notas=$this->coneccion('select','tb_respuestas', 'dc_pregunta,dc_calificacion', "dc_evaluacion={$L_E}");
        while($llena=$notas->fetch(PDO::FETCH_OBJ)){
            $arr1[$llena->dc_pregunta]=$llena->dc_calificacion;
        }
        return $arr1;
    }
    public function GetLast_Evaluacion($funcionario_id,$tipo_eval) {
        $ID = $this->coneccion('select', 'tb_evaluacion', 'dc_evaluacion AS id', 
                "dc_usuario_a_quien={$funcionario_id} 
                    AND dc_tipo={$tipo_eval} 
                        AND dc_evaluacion IS NOT NULL 
                GROUP BY dc_evaluacion 
                ORDER BY dc_evaluacion DESC
                LIMIT 1");
        $ID = $ID->fetch(PDO::FETCH_OBJ);

        if ($ID != false) {
            return $ID->id;
        }
    }

    private function Get_GrupoPreguntas($dc_cargo, $db) {
        $r = self::getRequest();
        $grupo =
                $db->prepare(
                $db->select(
                        'tb_grupo_cargo_relacion', 'dc_grupo_pregunta', "dc_cargo = {$dc_cargo}"
        ));

        $db->stExec($grupo);
        $r->grupo = $grupo;

        return $r->grupo;
    }

    protected function get_cargo($dc_funcionario) {
        $db = $this->getConnection();
        $dc = $this->dc_user($dc_funcionario);
        $cargo = $db->prepare($db->select('tb_funcionario fu 
                                                JOIN tb_usuario us', 'fu.dc_cargo', "fu.dc_funcionario=us.dc_funcionario and us.dc_usuario ={$dc}"
        ));
        $db->stExec($cargo);
        $cargo = $cargo->fetch(PDO::FETCH_OBJ);
        $ccs = $cargo->dc_cargo;
        return $ccs;
    }

    protected function dc_user($dc_funcionario) {
        $db = $this->getConnection();
        $id_user = $db->prepare($db->select("tb_usuario", "dc_usuario", "dc_funcionario={$dc_funcionario}"));
        $db->stExec($id_user);
        $id_user = $id_user->fetch(PDO::FETCH_OBJ);
        if($id_user != false){
            $id_user = $id_user->dc_usuario;
            return $id_user;
        }else{
            $this->getErrorMan()->showAviso('El funcionario seleccionado no tiene un usuario creado <br/> Solo funcionarios que tengan usuario creado pueden ser evaluados.');
            exit;            
        }
    }

    protected function nombre_funcionario($dc_funcionario) {
        $db = $this->getConnection();
        $user_name = $db->prepare($db->select('tb_funcionario', 'dg_nombres,dg_ap_paterno,dg_ap_materno', "dc_funcionario={$dc_funcionario}"));
        $db->stExec($user_name);
        $user_name = $user_name->fetch(PDO::FETCH_OBJ);
        $user_name = $user_name->dg_ap_paterno . " " . $user_name->dg_ap_materno . " " . $user_name->dg_nombres;
        return $user_name;
    }

    protected function get_detinatario($dc_funcionario) {
        $db = $this->getConnection();
        $email = $db->prepare($db->select('tb_funcionario', 'dg_email', "dc_funcionario={$dc_funcionario}"));
        $db->stExec($email);
        $email = $email->fetch(PDO::FETCH_OBJ);
        $email = $email->dg_email;
        return $email;
    }

    private function Comparar_fechas($dc_user) {
        $db = $this->getConnection();
        $rr = '';
        $compara = $db->prepare($db->select('tb_compromisos', 'df_compromiso', "dc_usuario_funcionario={$dc_user} "));
        $db->stExec($compara);
        while ($ff = $compara->fetch(PDO::FETCH_OBJ)) {
            $spl_f_t = split(' ', $ff->df_compromiso);
            $fecha_spl = split('-', $spl_f_t[0]);
            $fecha_a_m = $fecha_spl[0] . "-" . $fecha_spl[1];
            $fecha_actual = date("Y-m");

            if ($fecha_a_m == $fecha_actual) {
                if ($rr == '') {
                    $rr = "'" . $ff->df_compromiso . "'";
                } else {
                    $rr = $rr . ",'" . $ff->df_compromiso . "'";
                }
            }
        }
        return $rr;
    }

    public function ver_compromisos_creados($dc_user) {
        $arr1 = '';
        $r = self::getRequest();
        $db = $this->getConnection();
        $fecha = $this->Comparar_fechas($dc_user);
        if ($fecha != null) {

            $compromisos = $db->prepare($db->select('tb_compromisos com 
                                                    JOIN tb_funcionario fu
                                                        JOIN tb_usuario us', 'com.dg_compromiso,
                                               com.df_compromiso,
                                               com.dg_compromiso_empresa, 
                                               com.dc_estado,com.dc_cumple,
                                               us.dc_usuario,
                                               com.dc_estado,
                                               com.dc_cumple,
                                               fu.dg_nombres,
                                               fu.dg_ap_paterno,
                                               fu.dg_ap_materno', "com.dc_usuario_funcionario={$dc_user}
                                                            AND com.df_compromiso IN ({$fecha}) 
                                                                AND us.dc_usuario={$dc_user}
                                                                    AND us.dc_funcionario = fu.dc_funcionario
                                                                        AND com.dc_estado=2"
            ));

            $db->stExec($compromisos);
            while ($llena = $compromisos->fetch(PDO::FETCH_OBJ)) {
                if ($llena->dg_compromiso_empresa == null) {
                    $llena->dg_compromiso_empresa = "─═☆☆═─";
                }
                $arr1[$llena->dc_usuario][$llena->dg_nombres . " " . $llena->dg_ap_paterno . " " . $llena->dg_ap_materno][$llena->dg_compromiso][$llena->df_compromiso][$llena->dg_compromiso_empresa][$llena->dc_cumple] = $llena->dc_estado;
            }
        } else {
            $arr1["--"]["--"]["--"]["--"]["--"]["--"] = "--";
        }

        return $arr1;
    }

    public function CompromisoCumpleSave() {
        $r = self::getRequest();
        $db = $this->getConnection();

        for ($i = 1; $i <= $r->Cant_sino; $i++) {
            $nom = 'sino' . $i;
            if (isset($r->$nom)) {
                $spl = split('_', $r->$nom);
                $fecha = $spl[1];
                $dc_user = $spl[0];
                $sino = $spl[2];
                $up = $db->prepare($db->update('tb_compromisos', 
                                                array('dc_cumple' => "{$sino}"), 
                                                "dc_usuario_funcionario={$dc_user} 
                                                    AND df_compromiso='{$fecha}'"
                                               ));
                $db->stExec($up);
            }
        }
    }

}

?>
