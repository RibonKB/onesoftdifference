<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Creación de funcionarios</div>
<div id="other_options">
	<ul>
		<li class="oo_active"><a href="#">Creación</a></li>
		<li><a href="sites/rrhh/ed_funcionario.php" class="loader">Edición</a></li>
		<li><a href="sites/rrhh/del_funcionario.php" class="loader">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Complete los datos para la creación del funcionario</strong><br />(Los campos marcados con [*] son obligatorios)");
	$form->Start("sites/rrhh/proc/crear_funcionario.php","func_crear","form","method='post' enctype='multipart/form-data'");
	$form->Section();
	$form->Text("RUT","func_rut",1,255,"","inputrut");
	$form->Text("Apellido Paterno","func_ap_paterno",1);
	$form->Date("Fecha nacimiento","func_nacimiento",1);
	$form->EndSection();
	$form->Section();
	$form->Text("Nombres","func_nombres",1);
	$form->Text("Apellido Materno","func_ap_materno",1);
	$form->Radiobox("Sexo","func_sexo",array("Hombre","Mujer"),0);
	$form->EndSection();
	$form->Group();
	$form->Section();
	$form->Listado("Centro costo","func_ceco","tb_ceco",array("dc_ceco","dg_ceco"),1);
	$form->Date("Inicio contrato","func_ini_contr",1,date("d/m/Y"));
	$form->Listado("Salud","func_salud","tb_isapre",array("dc_isapre","dg_isapre"),1);
	$form->Text("Sueldo bruto (\$)","func_sueldo");
	$form->Text('Anexo','func_anexo',0,20);
	$form->EndSection();
	$form->Section();
	$form->Listado("Tipo de contrato","func_t_contrato","tb_tipo_contrato",array("dc_tipo_contrato","dg_tipo_contrato"),1);
	$form->Date("Término contrato","func_fin_contr");
	$form->Listado("AFP","func_afp","tb_afp",array("dc_afp","dg_afp"),1);
	$form->File("Foto","fc_foto");
	$form->EndSection();
	$form->End("Crear","addbtn");
?>
<iframe name="addfuncionario_fres" id="addfuncionario_fres" class="hidden"></iframe>
</div>
</div>

<script type="text/javascript">
$("#func_crear").attr("target","addfuncionario_fres");
$("#func_crear").submit(function(e){
	if(validarForm("#func_crear")){	
		disableForm("#func_crear");
	}else{
		e.preventDefault();
	}
});
$("#addfuncionario_fres").load(function(){
	res = frames['addfuncionario_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#func_crear_res").html(res);
	hide_loader();
});

</script>