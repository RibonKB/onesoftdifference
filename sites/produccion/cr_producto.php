<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div class='secc_bar'>Creación de productos</div><div class='panes'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/crear_producto.php','cr_costeo','confirmValidar');
$form->Header("Indique los datos para la creación de un producto<br />
El producto se asignará automáticamente al costeo actual (Generándolo de ser necesario)");

$form->Section();
$form->Text('Nombre','prod_name',1);
$form->EndSection();

$form->Section();
$form->Text('Días de fabricación','prod_fabricacion',1,5);
$form->EndSection();

$form->Section();
$form->Textarea('Descripción','prod_descripcion');
$form->EndSection();

echo("<br class='clear'/><hr /><br />");

echo("<table class='tab' width='700'>
<thead>
	<tr>
		<th width='25'>&nbsp;</th>
		<th>Material/Servicio</th>
		<th width='80'>U. Medida</th>
		<th width='120'>Cantidad</th>
		<th width='120'>Precio</th>
		<th width='120'>SubTotal</th>
	</tr>
</thead>
<tbody id='producto_detalle'>
	
</tbody>
</table>");
$form->Button("Agregar material","onclick='add_cost_material(\"#producto_detalle\");'",'addbtn');
$form->Button("Agregar servicio","onclick='add_cost_service(\"#producto_detalle\");'",'addbtn');
echo('<hr />');
$form->Hidden('prod_proyecto',$_POST['id']);

$form->End('Crear producto','addbtn');

echo("</div>");
?>