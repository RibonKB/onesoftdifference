<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_produccion_producto",'dg_producto',
"dc_producto={$_POST['id']} AND dc_empresa={$empresa}");

if(!count($data)){
	$error_man->showWarning("Producto no encontrado, verifique que no haya accedido desde un sitio indebido.");
	exit();
}
$data = $data[0];

echo("<div class='secc_bar'>Eliminar productos</div><div class='panes'>");
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/delete_producto.php','del_producto');
$error_man->showAviso("Confirma que desea eliminar el producto <b>{$data['dg_producto']}</b> del costeo?");
$form->Hidden('prod_id',$_POST['id']);
$form->End('Eliminar','delbtn');

echo('</div>');


?>