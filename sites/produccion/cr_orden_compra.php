<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div class='secc_bar'>Crear costeo</div><div class='panes'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/cr_orden_compra.php','cr_orden_compra','cValidar');
$form->Header('Indique el proveedor a quien se le hará la orden de compra');
$form->Text('RUT Proveedor','pro_proveedor',1,255,'','inputrut');
$form->Hidden('pro_number',$_POST['pro_number']);
$form->End('Crear orden de compra');

?>
<script type="text/javascript">
$('#pro_proveedor').autocomplete('sites/proc/autocompleter/proveedor.php',{
	formatItem: function(row){	return row[1]+" ("+row[0]+") "+row[2]; },
	minChars: 2,
	width:300}
);
</script>