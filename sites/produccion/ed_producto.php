<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_produccion_producto",'dg_producto,dc_dias_fabricacion,dc_costeo,dc_usuario_creacion',
"dc_producto={$_POST['id']} AND dc_empresa={$empresa}");

if(!count($data)){
	$error_man->showWarning("Producto no encontrado, verifique que no haya accedido desde un sitio indebido.");
	exit();
}
$data = $data[0];

if($data['dc_usuario_creacion'] != $idUsuario){
	$error_man->showWarning("No tiene permiso para editar este producto. solo puede modificae los que haya creado usted mismo.");
	exit();
}

echo("<div class='secc_bar'>Editar productos</div><div class='panes'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/editar_producto.php','ed_producto');
$form->Header("Editando <b>{$data['dg_producto']}</b>");
echo("<br />");
$form->Section();
$form->Text('Nombre','prod_name',1,255,$data['dg_producto']);
$form->EndSection();
$form->Section();
$form->Text('Días de fabricación','prod_fabricacion',1,5,$data['dc_dias_fabricacion']);
$form->EndSection();

echo("<table class='tab' width='700'>
<thead>
	<tr>
		<th width='25'>&nbsp;</th>
		<th>Material/Servicio</th>
		<th width='80'>U. Medida</th>
		<th width='120'>Cantidad</th>
		<th width='120'>Precio</th>
		<th width='120'>SubTotal</th>
	</tr>
</thead>
<tbody id='producto_detalle'>
</tbody>
</table>");
$form->Button("Agregar material","onclick='add_cost_material(\"#producto_detalle\");'",'addbtn');
$form->Button("Agregar servicio","onclick='add_cost_service(\"#producto_detalle\");'",'addbtn');
echo('<hr />');
$form->Hidden('prod_id',$_POST['id']);
$form->End('Editar','editbtn');

echo("</div>");

$cost_materials = $db->select("(SELECT * FROM tb_produccion_producto_detalle WHERE dc_producto = {$_POST['id']}) d
JOIN tb_produccion_material m ON m.dc_material = d.dc_material
LEFT JOIN tb_unidad_medida u ON u.dc_unidad_medida = m.dc_unidad_medida",
'm.dc_material,m.dg_material,d.dc_cantidad,d.dc_detalle,d.dq_precio,d.dq_total,u.dg_unidad_medida');

$cost_services = $db->select("(SELECT * FROM tb_produccion_producto_detalle_servicio WHERE dc_producto={$_POST['id']}) d
JOIN (SELECT * FROM tb_produccion_servicio WHERE dc_empresa={$empresa}) s ON s.dc_servicio = d.dc_servicio",
's.dc_servicio,s.dg_servicio,d.dc_cantidad,d.dc_detalle,d.dq_precio,d.dq_total');

?>
<script type="text/javascript">
var ed_material = <?=json_encode($cost_materials); ?>;
var ed_servicio = <?=json_encode($cost_services); ?>;

for(i in ed_material){
	var m = ed_material[i];
	var tr = $("#template_detail tr.templ_material").clone(true);
	mat_result.call(tr.find('.src_material'),{precio:m.dq_precio},[m.dg_material,null,null,m.dc_material]);
	tr.appendTo('#producto_detalle');
}

for(i in ed_servicio){
	var s = ed_servicio[i];
	var tr = $("#template_detail tr.templ_service").clone(true);
	srv_result.call(tr.find('.src_service'),null,[s.dg_servicio,null,s.dc_servicio,s.dq_precio]);
	tr.appendTo('#producto_detalle');
}
</script>