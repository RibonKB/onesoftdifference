<?php
define("MAIN",1);
require_once("../../inc/global.php");
$name = 'files/'.$_GET['file'];

if(!preg_match("/^\d+-{$empresa}-./",$_GET['file'])){
die('<h1>Archivo inválido</h1>
<script type="text/javascript">
alert("El archivo es inválido");
</script>');
}

if(!is_file($name)){
die('
<h1>Archivo no encontrado</h1>
<script type="text/javascript">
alert("El archivo es inválido");
</script>');
}

header("Content-disposition: attachment; filename=$name");
header("Content-type: application/octet-stream");
readfile($name);
?>