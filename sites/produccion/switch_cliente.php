<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$contactos = $db->select("(SELECT * FROM tb_domicilio_cliente WHERE dc_cliente = {$_POST['id']}) dc
	  LEFT JOIN tb_comuna co ON dc.dc_comuna = co.dc_comuna
	  JOIN (SELECT * FROM tb_domicilio_cliente_tipo_direccion WHERE dm_activo ='1') td ON dc.dc_domicilio = td.dc_domicilio
	  JOIN tb_contacto_cliente cc ON td.dc_contacto = cc.dc_contacto",
	  'DISTINCT cc.dc_contacto,dc.dg_direccion,co.dg_comuna,cc.dg_contacto');
	  
echo("<option value='0'></option>");
foreach($contactos as $c){
	echo("<option value='{$c['dc_contacto']}'>{$c['dg_contacto']} - {$c['dg_direccion']} {$c['dg_comuna']}</option>");
}
?>