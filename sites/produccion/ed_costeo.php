<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_produccion_costeo",'dg_costeo,dc_dias_fabricacion,dq_total,dc_proyecto,dc_usuario_creacion',
"dc_costeo={$_POST['id']} AND dc_empresa={$empresa}");

if(!count($data)){
	$error_man->showWarning("Costeo no encontrado, verifique que no haya accedido desde un sitio indebido.");
	exit();
}
$data = $data[0];

if($data['dc_usuario_creacion'] != $idUsuario){
	$error_man->showWarning("No tiene permiso para editar este costeo. solo puede modificar costeos haya creado usted mismo.");
	exit();
}

echo("<div class='secc_bar'>Editar costeos</div><div class='panes'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/editar_costeo.php','ed_costeo');
$form->Header("Editar el costeo {$data['dg_costeo']}");
echo("<br />");
$form->Section();
$form->Text("Nombre",'ed_cost_name',1,255,$data['dg_costeo']);
$form->EndSection();
$form->Section();
$form->Text("Días de fabricación de producto",'ed_cost_fabricacion',1,255,$data['dc_dias_fabricacion']);
$form->EndSection();
echo("<br class='clear' />");
echo("<table class='tab' width='100%'>
<thead>
	<tr>
		<th>Material/Servicio</th>
		<th width='80'>U. Medida</th>
		<th width='120'>Cantidad</th>
		<th width='120'>Precio</th>
		<th width='120'>SubTotal</th>
	</tr>
</thead>
<tbody id='ed_costeo_detalle'>");

$cost_materials = $db->select("(SELECT * FROM tb_produccion_costeo_detalle WHERE dc_costeo = {$_POST['id']}) d
JOIN (SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa}) m ON m.dc_material = d.dc_material
LEFT JOIN (SELECT * FROM tb_unidad_medida WHERE dc_empresa={$empresa}) u ON u.dc_unidad_medida = m.dc_unidad_medida",
'm.dc_material,m.dg_material,d.dc_cantidad,d.dc_detalle,d.dq_precio,d.dq_total,u.dg_unidad_medida');

foreach($cost_materials as $d){
	echo("<tr class='templ_material'><td class='material_list'>");
	echo("<input type='text' class='src_material searchbtn' size='10' />
	<div class='right' style='width:240px;background:#EEE;color:#555;padding:3px;border:1px solid #CCC;margin:3px;'>
	{$d['dg_material']}</div>");
	$form->Hidden('cost_material[]',$d['dc_material']);
	echo("</td><td>{$d['dg_unidad_medida']}</td><td>");
	$form->Text('','cost_cantidad[]',1,11,$d['dc_cantidad'],'inputtext',10);
	echo("</td><td>");
	$form->Text('','cost_precio[]',1,22,$d['dq_precio'],'inputtext',10);
	echo("</td><td align='right' class='cost_price'>{$d['dq_total']}</td></tr>");
}

$cost_services = $db->select("(SELECT * FROM tb_produccion_costeo_detalle_servicio WHERE dc_costeo={$_POST['id']}) d
JOIN (SELECT * FROM tb_produccion_servicio WHERE dc_empresa={$empresa}) s ON s.dc_servicio = d.dc_servicio",
's.dc_servicio,s.dg_servicio,d.dc_cantidad,d.dc_detalle,d.dq_precio,d.dq_total');

foreach($cost_services as $d){
	echo("<tr class='templ_service'><td>");
	echo("<input type='text' class='src_service searchbtn' size='10' />
	<div class='right' style='width:240px;background:#EEE;color:#555;padding:3px;border:1px solid #CCC;margin:3px;'>
	{$d['dg_servicio']}</div>");
	$form->Hidden('cost_service[]',$d['dc_servicio']);
	echo("</td><td>HH</td><td>");
	$form->Text('','cost_cantidad2[]',1,11,$d['dc_cantidad'],'inputtext',10);
	echo("</td><td>");
	$form->Text('','cost_precio2[]',1,22,$d['dq_precio'],'inputtext',10);
	echo("</td><td align='right'>{$d['dq_total']}</td></tr></table>");
}

echo("</tbody>
</table>");
$form->Button("Agregar material","onclick='add_cost_material(\"#ed_costeo_detalle\");'",'addbtn');
$form->Button("Agregar servicio","onclick='add_cost_service(\"#ed_costeo_detalle\");'",'addbtn');
echo('<hr />');
$form->Hidden('ed_cost_proyecto',$data['dc_proyecto']);
$form->Hidden('ed_cost_total',$data['dq_total']);
$form->Hidden('ed_cost_id',$_POST['id']);
$form->End('Emitir costeo','addbtn');

echo("</div>");
?>
<script type="text/javascript">
	$('.overlay .src_material').autocomplete('sites/proc/autocompleter/produccion_material.php',{
		formatItem: function(row){
			return "<b>"+row[0]+"</b> "+row[1]+" "+row[2];
		},
		minChars: 2,
		width:300
		}).result(function(e,row){
			$(this).val('').next().show().text(row[0]).next().val(row[3]);
			$(this).parent().next().text(unidades_medida[row[3]]);
		});
		
	$('.overlay .src_service').autocomplete('sites/proc/autocompleter/produccion_servicio.php',{
		formatItem: function(row){
			return "<b>"+row[0]+"</b> "+row[1];
		},
		minChars: 2,
		width:300
		}).result(function(e,row){
			$(this).val('').next().show().text(row[0]).next().val(row[2]);
			$(this).parent().next().next().next().children('input').val(row[3]).trigger('change');
		});
</script>