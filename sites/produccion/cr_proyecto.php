<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Crear {$lang['op_doc_may']}</div>
<div id='main_cont'><div class='panes'>");

require_once('../../inc/form-class.php');
$form = new Form($empresa);

$form->Start('sites/produccion/proc/crear_proyecto.php','cr_proyecto','form',"method='post' enctype='multipart/form-data'");
$form->Header("<b>Indique los datos para crear {$lang['op_artic']} {$lang['op_doc']}</b><br />Los campos marcados con [*] son obligatorios");
echo('<hr />');
$form->Section();
$form->Text('Nombre del proyecto','op_name',1);
echo("<br /><fieldset>");
$form->Listado('Cliente','op_cliente','tb_cliente',array('dc_cliente','dg_razon'),1);
echo("<label><input type='checkbox' name='op_other_cli'/>Otro</label>
<input type='text' name='op_cliente_txt' disabled='disabled' class='inputtext' size='30' /></fieldset><br />");
$form->Date('Fecha estimada de cierre','op_termino');

$form->EndSection();
$form->Section();
$form->Textarea("Descripción",'op_descripcion');
echo('<label><input type="checkbox" name="req_design" value="1"> Requiere Diseño</label>
<label><input type="checkbox" name="req_cost" value="1"> Requiere Costeo</label>');
//$form->Combobox('Opciones','op_required',array('Requiere costeo','Requiere diseño'));
$form->EndSection();
$form->Section();
echo("<br /><fieldset>
<legend><b>Archivos del proyecto</b> <small>(Recursos extra adjuntos al proyecto)</small></legend>");
$form->Button('Añadir archivo al proyecto','id="add_file"','addbtn');
echo('</fieldset>');
$form->EndSection();
$form->End('Crear','addbtn');

?>
<iframe name="cr_proyecto_fres" id="cr_proyecto_fres" class='hidden'></iframe>
<iframe name="upload_file" id="upload_file" class="hidden"></iframe>
</div></div>
<script type="text/javascript">
$('#cr_proyecto').attr('target','cr_proyecto_fres');
$("#cr_proyecto").submit(function(e){
	if(validarForm("#cr_proyecto")){
		$('<input>').multiAttr({
			type:'hidden',
			name:'asinc'
		}).val(1).appendTo('#cr_proyecto');
		disableForm("#cr_proyecto");
	}else{
		e.preventDefault();
	}
});
$("#cr_proyecto_fres").load(function(){
	res = frames['cr_proyecto_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#cr_proyecto_res").html(res);
	hide_loader();
});
$('input[name=op_other_cli]').click(function(){
	if($(this).attr('checked')){
		$(this).parent().next().attr('disabled',false).attr('required',true).focus();
		$('#op_cliente').attr('required',false);
	}else{
		$(this).parent().next().attr('disabled',true).attr('required',false);
		$('#op_cliente').attr('required',true);
	}
});
$('#add_file').click(function(){
	var div = $('<div>');
	var delbtn = $('<img>').multiAttr({
		src:'images/delbtn.png',
		alt:'[X]',
		title:'Quitar archivo'
	}).click(function(){$(this).parent().remove();});;
	var file = $('<input>').multiAttr({
		type:'file',
		name:'adfile[]'
	});
	div.append(delbtn).append(file);
	$(this).after(div);
});
</script>