<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div class='secc_bar'>Crear costeo</div><div class='panes'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/crear_costeo.php','cr_costeo');
$form->Header("Indique los datos para la creación de un costeo");
echo("<br />");
$form->Text("Nombre",'cost_name',1);
$form->Text("Días de fabricación de producto",'cost_fabricacion',1);
$form->Hidden('cost_proyecto',$_POST['id']);
$form->End('Agregar costeo','addbtn');

echo("</div>");
?>