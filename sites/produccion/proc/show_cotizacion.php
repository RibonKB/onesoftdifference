<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_produccion_cotizacion WHERE dc_empresa={$empresa} AND dc_cotizacion = {$_POST['id']}) cot
LEFT JOIN tb_termino_comercial tc ON tc.dc_termino_comercial = cot.dc_termino_comercial
LEFT JOIN tb_modo_envio me ON me.dc_modo_envio = cot.dc_modo_envio
LEFT JOIN (SELECT distinct cl_c.dc_contacto,cl_d.dg_direccion,cl_com.dg_comuna,cl_r.dg_region FROM tb_contacto_cliente cl_c
	JOIN tb_domicilio_cliente_tipo_direccion cl_t ON cl_c.dc_contacto = cl_t.dc_contacto
	JOIN tb_domicilio_cliente cl_d ON cl_t.dc_domicilio = cl_d.dc_domicilio
	LEFT JOIN tb_comuna cl_com ON cl_d.dc_comuna = cl_com.dc_comuna
	LEFT JOIN tb_region cl_r ON cl_com.dc_region = cl_r.dc_region) c ON cot.dc_contacto = c.dc_contacto
LEFT JOIN tb_cliente cl ON cl.dc_cliente = cot.dc_cliente
LEFT JOIN tb_cotizacion_estado est ON est.dc_estado = cot.dc_estado
LEFT JOIN tb_produccion_proyecto pro ON pro.dc_proyecto = cot.dc_proyecto",
"cot.dc_cotizacion,cot.dq_cotizacion,tc.dg_termino_comercial,c.dg_direccion,c.dg_comuna,c.dg_region,me.dg_modo_envio,cl.dg_razon,pro.dg_proyecto,
cl.dg_rut,DATE_FORMAT(cot.df_fecha_emision,'%d/%m/%Y') as df_emision,cot.dq_iva,cot.dq_neto,cot.dg_observacion,cot.dc_estado,est.dg_estado,cot.dc_costeo");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la cotización especificada");
	exit();
}
$data = $data[0];
$data['dq_total'] = moneda_local($data['dq_iva']+$data['dq_neto']);
$data['dq_neto'] = moneda_local($data['dq_neto']);
$data['dq_iva'] = moneda_local($data['dq_iva']);
$data['dq_cotizacion'] = str_pad($data['dq_cotizacion'],4,'0',STR_PAD_LEFT);

echo("<div class='title center'>Cotización nº {$data['dq_cotizacion']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong>({$data['dg_rut']}) {$data['dg_razon']}</strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td>{$data['df_emision']}</td>
	<td rowspan='9' width='250'>
		<label>Estado de cotización</label><br />
		<h3 class='info' style='margin:0'>{$data['dg_estado']}</h3>
	</td>
</tr><tr>
	<td>Domicilio cliente</td>
	<td><b>{$data['dg_direccion']}</b> {$data['dg_comuna']} <label>{$data['dg_region']}</label></td>
</tr><tr>
	<td>Proyecto:</td>
	<td><b>{$data['dg_proyecto']}</b></td>
</tr><tr>
	<td>Término Comercial</td>
	<td>{$data['dg_termino_comercial']}</td>
</tr><tr>
	<td>Modo de envio</td>
	<td>{$data['dg_modo_envio']}</td>
</tr><tr>
	<td>Observación</td>
	<td>{$data['dg_observacion']}</td>
</tr></table>");

$detalle = $db->select("tb_produccion_producto","dg_producto,dq_precio,dq_precio_venta,dg_descripcion,dc_cantidad,dc_cantidad*dq_precio_venta AS dq_total",
"dc_costeo={$data['dc_costeo']}");

/*$detalle = $db->select("(SELECT * FROM tb_produccion_cotizacion_detalle WHERE dc_cotizacion = {$data['dc_cotizacion']}) det
LEFT JOIN tb_produccion_costeo c ON det.dc_costeo = c.dc_costeo",
'c.dq_costeo,det.dg_descripcion,det.dq_cantidad,det.dq_precio_venta,det.dq_precio_compra');*/

echo("<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
<tr>
	<th>Cantidad</th>
	<th>Costeo</th>
	<th>Descripción</th>
	<th>Precio</th>
	<th>Margen</th>
	<th>Total</th>
</tr>
</thead>
<tbody>");

$margen_total = 0;
foreach($detalle as $d){
	$margen = moneda_local($d['dq_precio_venta']-$d['dq_precio']);
	$margen_p = number_format(($d['dq_precio_venta']-$d['dq_precio'])*100/$d['dq_precio'],1);
	$margen_total += $d['dq_precio_venta']-$d['dq_precio'];
	$d['dq_total'] = moneda_local($d['dq_precio_venta']*$d['dc_cantidad']);
	$d['dq_precio_venta'] = moneda_local($d['dq_precio_venta']);
	$d['dq_cantidad'] = number_format($d['dc_cantidad'],2,$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
	echo("
	<tr>
		<td width='80'>{$d['dq_cantidad']}</td>
		<td align='left' width='120'><b>{$d['dg_producto']}</b></td>
		<td align='left'>{$d['dg_descripcion']}</td>
		<td width='120' align='right'>{$d['dq_precio_venta']}</td>
		<td width='120' align='right'><b>{$margen}</b> ({$margen_p}%)</td>
		<td width='120' align='right'>{$d['dq_total']}</td>
	</tr>");
}

$margen_total = moneda_local($margen_total);

echo("
</tbody>
<tfoot>
<tr>
	<th colspan='5' align='right'>Total Neto</th>
	<th align='right'>{$data['dq_neto']}</th>
</tr>
<tr>
	<th colspan='5' align='right'>Margen Total</th>
	<th align='right'>{$margen_total}</th>
</tr>
<tr>
	<th colspan='5' align='right'>IVA</th>
	<th align='right'>{$data['dq_iva']}</th>
</tr>
<tr>
	<th colspan='5' align='right'>Total a Pagar</th>
	<th align='right'>{$data['dq_total']}</th>
</tr>
</tfoot>");

?>
<script type="text/javascript">
$('#print_version').unbind('click');
$('#print_version').click(function(){
	window.open("sites/produccion/ver_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>",'print_cotizacion','width=800;height=600');
});
$('#edit_cotizacion').unbind('click');
$('#edit_cotizacion').click(function(){
	$('#res_list').slideUp();
	$('.panes').width(1140);
	loadFile("sites/ventas/ed_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>",'#show_cotizacion','',globalFunction);
});
$('#gestion_cotizacion').unbind('click');
$('#gestion_cotizacion').click(function(){
	loadOverlay("sites/ventas/src_gestion_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>");
});
$('#historial_actividad').unbind('click');
$('#historial_actividad').click(function(){
	loadOverlay("sites/ventas/history_gestion_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>");
});
</script>