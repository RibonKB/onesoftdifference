<?php
/**
*	Ingresa una nueva cotización
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_cotizacion']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$estado = $db->select('tb_valor_defecto','dc_valor',"dg_tabla = 'tb_cotizacion_estado' AND dc_empresa={$empresa}",array('order_by'=> 'df_creacion DESC'));

if(count($estado))
	$estado = $estado[0]['dc_valor'];
else
	$estado = 0;

$db->start_transaction();
$last_date = $db->select('tb_produccion_cotizacion','MAX(df_fecha_emision) fecha',"dc_empresa={$empresa}");

if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_produccion_cotizacion','dq_cotizacion',"dc_empresa={$empresa} AND df_fecha_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_cotizacion'],0,$largo_prefix) == $cot_prefix || $empresa_conf['dc_correlativo_cotizacion'] == '3'){
		$cot_num = substr($last[0]['dq_cotizacion'],$largo_prefix)+1;
	}else{
		$cot_num = $empresa_conf['dg_correlativo_cotizacion'];
	}
}else{
	$cot_num = $empresa_conf['dg_correlativo_cotizacion'];
}

$cotizacion = $db->insert("tb_produccion_cotizacion",
array(
	'dq_cotizacion' => $cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT),
	'dg_observacion' => $_POST['cot_observacion'],
	'df_envio' => $db->sqlDate($_POST['cot_envio']),
	'dc_termino_comercial' => $_POST['cot_termino'],
	'dc_modo_envio' => $_POST['cot_modo_envio'],
	'dc_empresa' => $empresa,
	'dc_contacto' => $_POST['cot_contacto'],
	'dc_usuario_creacion' => $idUsuario,
	'dc_cliente' => $_POST['cli_id'],
	'df_fecha_emision' => 'NOW()',
	'dq_iva' => $_POST['cot_iva'],
	'dq_neto' => $_POST['cot_neto'],
	'dc_proyecto' => $_POST['cot_proyecto'],
	'dc_costeo' => $_POST['cot_costeo'],
	'dc_estado' => $estado
));

if(isset($_POST['prod'])){
	foreach($_POST['prod'] as $i => $v){
		
		$det_item = array(
			'dc_cotizacion' => $cotizacion,
			'dc_empresa' => $empresa,
			'dg_descripcion' => $_POST['desc'][$i],
			'dc_cantidad' => $_POST['cant'][$i],
			'dq_precio_venta' => str_replace(',','',$_POST['precio'][$i]),
			'dq_precio_compra' => str_replace(',','',$_POST['costo'][$i])
		);
		
		if($_POST['type_prod'][$i] == 1)
			$det_item['dc_producto'] = $v;
		else
			$det_item['dg_producto'] = $v;
		
		
		$db->insert('tb_produccion_cotizacion_detalle',$det_item);
	}
}

$cant_det = count($_POST['prod']);

$db->commit();

$error_man->showConfirm("Se ha generado la cotización");
echo("<div class='title'>El número de cotización es <h1 style='margin:0;color:#000;'>".$cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT)."</h1></div>");
$error_man->showConfirm("Fueron agregados {$cant_det} elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/produccion/ver_cotizacion.php?id={$cotizacion}','cotizacion','width=800,height=600')\"> Haga clic aquí para ver la cotización</a>");

$contacto = $db->select('tb_contacto_cliente','dg_contacto',"dc_contacto={$_POST['cot_contacto']}");
if(count($contacto)){
	$contacto = $contacto[0]['dg_contacto'];
}else{
	$contacto = 'N/A';
}

$db->insert('tb_cotizacion_avance',
array(
	"dc_cotizacion" => $cotizacion,
	"dg_contacto" => $contacto,
	"dg_gestion" => 'Creación',
	"df_creacion" => 'NOW()',
	"df_proxima_actividad" => "ADDDATE(NOW(),{$empresa_conf['dn_cotizacion_dias_actividad']})",
	"dc_estado_cotizacion" => $estado
));

if($_POST['pro_permiso']!=2){
	$db->update('tb_produccion_proyecto',
	array(
		"dc_costeo_actual" => 0,
		"dm_permiso" => 1
	),"dc_proyecto={$_POST['cot_proyecto']}");
	
	$db->update('tb_produccion_costeo',
	array(
		"dc_cotizacion" => $cotizacion
	),"dc_costeo={$_POST['cot_costeo']}");
}

if($_POST['cot_termino'])
	$db->update('tb_cliente',array("dc_termino_comercial" => $_POST['cot_termino']),"dc_cliente={$_POST['cli_id']}");

?>