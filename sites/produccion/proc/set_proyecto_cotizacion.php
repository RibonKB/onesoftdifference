<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div class="secc_bar">Asignación de cotización a proyecto</div><div class="panes">');

$data = $db->select('tb_produccion_cotizacion','dc_proyecto,dq_cotizacion',"dc_cotizacion={$_POST['id']} AND dc_empresa={$empresa}");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la cotización especificada");
	echo('</div>');
	exit();
}
$data = $data[0];

$db->update('tb_produccion_proyecto',array('dc_cotizacion' => $_POST['id']),"dc_proyecto={$data['dc_proyecto']}");

$error_man->showConfirm("Se ha asignado la cotización <b>{$data['dq_cotizacion']}</b> como la cotización definitiva para el proyecto.");
?>
</div>
<script type="text/javascript">
	$('#show_proyecto').html("<img src='images/ajax-loader.gif' alt='' /> cargando proyecto ...");
	loadFile("sites/produccion/proc/show_proyecto.php?id="+id_proyecto,'#show_proyecto','',globalFunction);
</script>