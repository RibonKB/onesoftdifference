<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->escape($_POST['pro_number']);

$data_proy = $db->select("(SELECT * FROM tb_produccion_proyecto WHERE dc_empresa = {$empresa} AND dc_proyecto = {$_POST['pro_number']} AND dm_permiso<>'1') pr
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON cl.dc_cliente = pr.dc_cliente",
'pr.dg_proyecto,pr.dc_proyecto,pr.dc_cliente,pr.dg_cliente,pr.dc_costeo_actual,cl.dg_razon,cl.dc_contacto_default,cl.dc_termino_comercial,pr.dm_permiso');

if(!count($data_proy)){
	$error_man->showErrorRedirect("No se encontró el proyecto especificado o este està bloqueado para ser cotizado.","sites/produccion/src_proyecto.php");
}

$data_proy = $data_proy[0];

if($data_proy['dc_costeo_actual'] == 0 && $data_proy['dm_permiso'] != 2){
	$error_man->showErrorRedirect("El proyecto <b>{$data_proy['dg_proyecto']}</b> debe ser costeado antes de cotizarlo","sites/produccion/src_proyecto.php");
}

if($data_proy['dc_cliente'] == 0){
	$display_client = $data_proy['dg_cliente'];
	$sel_client = true;
}else{
	$display_client = $data_proy['dg_razon'];
	$sel_client = false;
}

$detalle = $db->select("tb_produccion_producto",
'dc_producto,dg_producto,dq_precio,dg_descripcion',"dc_empresa={$empresa} AND dc_costeo={$data_proy['dc_costeo_actual']}");

if(!count($detalle) && $data_proy['dm_permiso'] != 2 ){
	$error_man->showErrorRedirect("El proyecto <b>{$data_proy['dg_proyecto']}</b> No ha sido costeado","sites/produccion/src_proyecto.php");
}

echo("<div id='secc_bar'>Creación de cotización</div>
<div id='main_cont'>
<div class='panes' style='width:1140px;'>
<div class='title center'>");

if($data_proy['dc_cliente'] != 0)
echo("<button class='right button' id='cli_switch'>Cambiar Cliente</button>");


echo("Generando cotización para <strong id='cli_razon' style='color:#000;'>{$display_client}</strong>
</div>
<hr class='clear' />");

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/produccion/proc/crear_cotizacion.php","cr_cotizacion",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la cotización</strong><br />Los datos marcados con [*] son obligatorios");
	if($data_proy['dc_cliente'] == 0){
		echo("<fieldset class='center'>");
		$error_man->showAviso("<b>Atención:</b> Al momento de crear el proyecto no se especificó un cliente del sistema, para cotizar debe hacerlo.");
		$form->Listado('Seleccione el cliente','cli_id','tb_cliente',array('dc_cliente','dg_razon'),1);
		$_SESSION['redirect_on_success'] = "sites/produccion/proc/cr_cotizacion.php?pro_number={$_POST['pro_number']}";
		echo("
		<a href='sites/clientes/cr_cliente.php' class='loader' style='color:#55F;font-size:17px;text-decoration:underline;'>Crear cliente</a>
		<br /></fieldset>");
	}else{
		$form->Hidden("cli_id",$data_proy['dc_cliente']);
	}
	$form->Section();
	$form->Listado('Contacto','cot_contacto',
	"(SELECT * FROM tb_domicilio_cliente WHERE dc_cliente = {$data_proy['dc_cliente']}) dc
	  LEFT JOIN tb_comuna co ON dc.dc_comuna = co.dc_comuna
	  JOIN (SELECT * FROM tb_domicilio_cliente_tipo_direccion WHERE dm_activo ='1') td ON dc.dc_domicilio = td.dc_domicilio
	  JOIN tb_contacto_cliente cc ON td.dc_contacto = cc.dc_contacto",
	array('DISTINCT cc.dc_contacto','dc.dg_direccion','co.dg_comuna','cc.dg_contacto'),1,$data_proy['dc_contacto_default'],'');
	
	$form->Listado("Términos comerciales","cot_termino","tb_termino_comercial",array("dc_termino_comercial","dg_termino_comercial"),0,$data_proy['dc_termino_comercial']);
	$form->EndSection();
	
	$form->Section();
	$form->Date("Fecha envio","cot_envio",0,1);
	$form->Listado("Modo de envio","cot_modo_envio","tb_modo_envio",array("dc_modo_envio","dg_modo_envio"));
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','cot_observacion');
	$form->EndSection();
	
	echo("<hr class='clear' />
	<div id='prods'>
	<br />
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th>&nbsp;</th>
		<th width='250'>Producto</th>
		<th>Descripción</th>
		<th width='63'>Cantidad</th>
		<th width='85'>Precio</th>
		<th width='85'>Total</th>
		<th width='85'>Costo</th>
		<th width='85'>Costo total</th>
		<th width='85'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'>");
	foreach($detalle as $d){
		echo("<tr class='main'>
		<td colspan='2'><b>{$d['dg_producto']}</b>");
		$form->Hidden('prod[]',$d['dc_producto']);
		$form->Hidden('type_prod[]',1);
		echo("</td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='35' value='{$d['dg_descripcion']}' /></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>
			<td align='right'>
			<input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' value='{$d['dq_precio']}' readonly='readonly' />
		</td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
		</tr>");
	}
	echo("</tbody>
	<tfoot>
	<tr>
		<th colspan='4' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center' id='total_margen_p'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>");
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('cot_proyecto',$data_proy['dc_proyecto']);
	$form->Hidden('cot_costeo',$data_proy['dc_costeo_actual']);
	$form->Hidden('pro_permiso',$data_proy['dm_permiso']);
	$form->End('Crear','addbtn');
	
	echo("<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td>
		<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
		</td><td>
		<input type='text' name='prod[]' class='prod_codigo searchbtn' size='35' />
		<input type='hidden' name='type_prod[]' value='0' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='35' required='required' /></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'>
		<input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' />
		</td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
	</tr>
	</table>");
?>
</div></div>
<script type="text/javascript">
	$("select#cli_id").change(function(){
		$('#cot_contacto').attr('disabled',true);
		loadFile('sites/produccion/switch_cliente.php?id='+$(this).val(),'#cot_contacto','',function(){
			$('#cot_contacto').attr('disabled',false);
		});
	});
	$('#cli_switch').click(function(){
		loadOverlay('sites/ventas/switch_cliente.php');
	});
	empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;
</script>
<script type="text/javascript" src="jscripts/product_manager/produccion_cotizacion.js?v3"></script>
<script type="text/javascript">
	tc = 1;
	tc_dec = 0;
	actualizar_detalles();
	actualizar_totales();
</script>