<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$proy_costeos = $db->select('tb_produccion_costeo','1',"dc_proyecto={$_POST['p']}");
$costeo = $db->insert('tb_produccion_costeo',
array(
	'dc_correlativo' => count($proy_costeos)+1,
	'dc_proyecto' => $_POST['p'],
	'dc_empresa' => $empresa,
	'df_emision' => 'NOW()',
	'dc_usuario_creacion' => $idUsuario
));

$db->update('tb_produccion_proyecto',
array(
	'dc_costeo_actual' => $costeo
),"dc_proyecto={$_POST['p']}");

$prod_data = $db->select('tb_produccion_producto','dc_producto,dg_producto,dc_dias_fabricacion,dq_precio',"dc_costeo={$_POST['c']}");

foreach($prod_data as $p){
	$producto = $db->insert('tb_produccion_producto',array(
		"dg_producto" => $p['dg_producto'],
		"dc_costeo" => $costeo,
		"df_creacion" => 'NOW()',
		"dc_empresa" => $empresa,
		"dc_usuario_creacion" => $idUsuario,
		"dc_dias_fabricacion" => $p['dc_dias_fabricacion'],
		"dq_precio" => $p['dq_precio']
	));
	
	$db->query("INSERT INTO tb_produccion_producto_detalle
	(dc_producto,dc_material,dc_cantidad,dq_precio,dq_total)
	SELECT {$producto},dc_material,dc_cantidad,dq_precio,dq_total
	FROM tb_produccion_producto_detalle
	WHERE dc_producto={$p['dc_producto']}");
	
	$db->query("INSERT INTO tb_produccion_producto_detalle_servicio
	(dc_producto,dc_servicio,dc_cantidad,dq_precio,dq_total)
	SELECT {$producto},dc_servicio,dc_cantidad,dq_precio,dq_total
	FROM tb_produccion_producto_detalle_servicio
	WHERE dc_producto={$p['dc_producto']}");
}

$db->update('tb_produccion_proyecto',array("dc_costeo_actual" => $costeo),"dc_proyecto={$_POST['p']}");

$db->commit();

$error_man->showConfirm("Costeo duplicado y asignado como actual correctamente");

?>
<script type="text/javascript">
	$('#show_proyecto').html("<img src='images/ajax-loader.gif' alt='' /> cargando proyecto ...");
	loadFile("sites/produccion/proc/show_proyecto.php?id="+id_proyecto,'#show_proyecto','',globalFunction);
</script>
