<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_produccion_proyecto WHERE dc_empresa={$empresa} AND dc_proyecto={$_POST['id']}) pr
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON pr.dc_cliente = cl.dc_cliente",
"pr.dg_cliente,pr.dc_proyecto,pr.dq_proyecto,pr.dg_proyecto,DATE_FORMAT(pr.df_emision,'%d/%m/%Y') as df_emision,cl.dg_razon,
DATE_FORMAT(pr.df_termino,'%d/%m/%Y') as df_termino,dg_descripcion,dm_permiso");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado el proyecto especificada");
	exit();
}

$data = $data[0];

$cotizado = $db->select('tb_produccion_cotizacion','1',"dc_proyecto={$data['dc_proyecto']}");

if(count($cotizado))
	$cotizado = 1;
else
	$cotizado = 0;

$data['dq_proyecto'] = str_pad($data['dq_proyecto'],4,'0',STR_PAD_LEFT);

echo("<div class='title center'>Proyecto &quot;{$data['dg_proyecto']}&quot;</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>");

if($data['dm_permiso'] == '1'){
	echo("<div id='make_cotizable' class='right'>
	<button type='button' class='button' style='background-image:url(images/doc.png);background-repeat:no-repeat;'>
		Finalizar costeo
	</button></div>");
}

echo("Cliente:<br /><strong> {$data['dg_cliente']}{$data['dg_razon']}</strong></caption>
<tr>
	<td width='160'>Número</td>
	<td><b>{$data['dq_proyecto']}</td>
</tr>
<tr>
	<td width='160'>Fecha emision</td>
	<td><b>{$data['df_emision']}</td>
</tr><tr>
	<td>Cierre estimado</td>
	<td><b>{$data['df_termino']}</b></td>
</tr><tr>
	<td><b>Descripción</b></td>
	<td>{$data['dg_descripcion']}</td>
</tr></table>");

$costeos = $db->select('tb_produccion_costeo',
'dc_costeo,dc_correlativo,DATE_FORMAT(df_emision,"%d/%m/%Y") as df_emision,dc_cotizacion',
"dc_empresa = {$empresa} AND dc_proyecto={$_POST['id']}");

if(!count($costeos))
	$error_man->showInfo("El proyecto especificado no posee ningún costeo a la fecha, puede agregar nuevos costeos haciendo click en el botón &quot;Agregar Costeo&quot;");
else{
$error_man->showInfo("Costeos realizados sobre el proyecto");
foreach($costeos as $costeo){
	
	if($costeo['dc_cotizacion'] == 0){
		$cot_button = "<button class='button' disabled='disabled'>No cotizado aún</button>";
	}else{
		$cot_button = "<button class='button' onclick='show_cost_cotizacion({$costeo['dc_costeo']})'>Ver cotización</button>";
	}
	
	echo("<div style='background:url(images/form-bg.jpg);border:1px solid #BBB;padding:5px;margin:3px;'>
	<b>Costeo {$costeo['dc_correlativo']}</b>
	<button class='button' onclick='show_cost_detail({$costeo['dc_costeo']})'>Mostrar detalle</button>
	{$cot_button}
	</div>");
}
}

echo("<br />");



/*$productos = $db->select("tb_produccion_producto","dc_producto,dg_producto,dc_dias_fabricacion","dc_proyecto={$_POST['id']} AND dc_costeo=0");

if(!count($productos)){
	$error_man->showInfo('No hay productos en proceso de costeo');
}else{
	$error_man->showInfo('Productos en proceso de costeo');
}

foreach($productos as $p){
	echo("<table class='tab' width='80%' align='center'>
	<caption>
	<span class='right'>Días de fabricación: {$p['dc_dias_fabricacion']}</span>
	{$p['dg_producto']}</caption>
	<thead><tr>
	<th>Material</th>
	<th>U.Medida</th>
	<th>Cantidad</th>
	<th>Precio Unidad</th>
	<th>Total</th>
	</tr></thead><tbody>");
	
	echo("</tbody></table><br />");
}*/

/*if(!count($costeos))
	$error_man->showInfo("El proyecto especificado no posee ningún costeo a la fecha, puede agregar nuevos costeos haciendo click en el botón &quot;Agregar Costeo&quot;");
else{
$error_man->showInfo("Costeos realizados sobre el proyecto");
echo('<br />');
	foreach($costeos as $costeo){
		echo("<br /><fieldset><legend>{$costeo['dg_costeo']} ({$costeo['dc_dias_fabricacion']} días de fabricación)</legend>");
		
		$cost_productos = $db->select('tb_produccion_producto',
		'dc_producto,dg_producto,dq_precio,DATE_FORMAT(df_creacion,"%d/%m/%Y") as df_creacion',
		"dc_costeo={$costeo['dc_costeo']} AND dc_empresa={$empresa}");
		
		if(!count($cost_productos))
			$error_man->showInfo("El costeo no posee productos asignados");
		else
			foreach($cost_productos as $p){
				$p['dq_precio'] = moneda_local($p['dq_precio']);
				echo("<div style='border:1px solid #005;background:#D3DCFD;padding:5px;margin:2px;'>
				<span class='right'>{$p['df_creacion']}</span>
				<h2 style='margin:0;padding:0;'>{$p['dg_producto']}</h2>
				<div style='background:#ABBDFA;color:#006;'>precio: $ <b>{$p['dq_precio']}</b></div>
				</div>");
			}
		
		echo("<button id='add_producto' class='addbtn' onclick='add_producto({$costeo['dc_costeo']})'>Agregar producto</button>");
		echo("</fieldset><br />");
	}
}*/

?>
<script type="text/javascript">

var id_proyecto = <?=$_POST['id'] ?>;

$('#print_version').unbind('click');
$('#print_version').click(function(){
	window.open("sites/produccion/ver_proyecto.php?id=<?=$_POST['id'] ?>",'print_proyecto','width=800;height=600');
});
$('#edit_proyecto').unbind('click');
$('#edit_proyecto').click(function(){
	loadOverlay("sites/produccion/ed_proyecto.php?id=<?=$_POST['id'] ?>");
});
$('#add_cost').unbind('click');
$('#add_cost').click(function(){
	loadOverlay("sites/produccion/cr_costeo.php?id=<?=$_POST['id'] ?>");
});
$('#cotizar_proyecto').unbind('click');
$('#cotizar_proyecto').click(function(){
	loadpage("sites/produccion/proc/cr_cotizacion.php?pro_number=<?=$data['dq_proyecto'] ?>");
});
$('#make_cotizable button').click(function(){
	if(confirm('Está seguro que desea continuar')){
		$('#cr_costeo').remove();
		$('#make_cotizable').html('<img src="images/ajax-loader.gif" alt="cargando..." />');
		loadFile('sites/produccion/proc/set_cotizable.php','#make_cotizable','id=<?=$_POST['id'] ?>');
	}
});
</script>