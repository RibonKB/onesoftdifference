<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->escape($_POST['pro_number']);

$data_proy = $db->select("(SELECT * FROM tb_produccion_proyecto WHERE dc_empresa = {$empresa} AND dq_proyecto = {$_POST['pro_number']} AND dc_cotizacion<>0) pr
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON cl.dc_cliente = pr.dc_cliente",
'pr.dg_proyecto,pr.dc_proyecto,pr.dc_cliente,pr.dg_cliente,cl.dg_razon,cl.dc_contacto_default,pr.dc_cotizacion');

if(!count($data_proy)){
	$error_man->showErrorRedirect("No se encontró el proyecto especificado o no ha seleccionado una cotización definitiva para el proyecto.",
	"sites/produccion/src_proyecto.php");
}

$data_proy = $data_proy[0];

if($data_proy['dc_cliente'] == 0){
	$display_client = $data_proy['dg_cliente'];
	$sel_client = true;
}else{
	$display_client = $data_proy['dg_razon'];
	$sel_client = false;
}

$detalle = $db->select("(SELECT * FROM tb_produccion_cotizacion_detalle WHERE dc_cotizacion={$data_proy['dc_cotizacion']}) d
JOIN tb_produccion_producto p ON p.dc_producto = d.dc_producto",'p.dc_producto,p.dg_producto,d.dc_cantidad,d.dg_descripcion');

echo("<div id='secc_bar'>Creación de orden de trabajo</div>
<div id='main_cont'>
<div class='panes'>
<div class='title center'>");

if($data_proy['dc_cliente'] != 0)
echo("<a href='sites/ventas/switch_cliente.php' class='right button loadOnOverlay'>Cambiar Cliente</a>");

echo("Generando orden de trabajo para <strong id='cli_razon' style='color:#000;'>{$display_client}</strong>
</div>
<hr class='clear' />");

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/produccion/proc/crear_orden_trabajo.php","cr_orden_trabajo");
	$form->Header("<strong>Indique los datos para la generación de la orden de trabajo</strong><br />Los datos marcados con [*] son obligatorios");
	if($data_proy['dc_cliente'] == 0){
		echo("<fieldset class='center'>");
		$error_man->showAviso("<b>Atención:</b> Al momento de crear el proyecto no se especificó un cliente del sistema, para crear nota de venta debe hacerlo.");
		$form->Listado('Seleccione el cliente','cli_id','tb_cliente',array('dc_cliente','dg_razon'),1);
		$_SESSION['redirect_on_success'] = "sites/produccion/proc/cr_orden_trabajo.php?pro_number={$_POST['pro_number']}";
		echo("
		<a href='sites/clientes/cr_cliente.php' class='loader' style='color:#55F;font-size:17px;text-decoration:underline;'>Crear cliente</a>
		<br /></fieldset>");
	}else{
		$form->Hidden("cli_id",$data_proy['dc_cliente']);
	}
	$form->Section();
	$form->Listado('Contacto','ot_contacto',
	"(SELECT * FROM tb_domicilio_cliente WHERE dc_cliente = {$data_proy['dc_cliente']}) dc
	  LEFT JOIN tb_comuna co ON dc.dc_comuna = co.dc_comuna
	  JOIN (SELECT * FROM tb_domicilio_cliente_tipo_direccion WHERE dm_activo ='1') td ON dc.dc_domicilio = td.dc_domicilio
	  JOIN tb_contacto_cliente cc ON td.dc_contacto = cc.dc_contacto",
	array('DISTINCT cc.dc_contacto','dc.dg_direccion','co.dg_comuna','cc.dg_contacto'),1,$data_proy['dc_contacto_default'],'');
	$form->Listado("Encargado","ot_encargado","tb_funcionario",array("dc_funcionario","dg_nombres","dg_ap_paterno","dg_ap_materno"));
	$form->EndSection();
	
	$form->Section();
	$form->Date("Fecha de entrega","ot_entrega",0,1);
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','ot_observacion');
	$form->EndSection();
	
	echo("<hr class='clear' />
	<div id='prods'>
	<br />
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='30%'>Producto</th>
		<th width='50%'>Descripción</th>
		<th width='20%'>Cantidad</th>
	</tr>
	</thead>
	<tbody id='prod_list'>");
		foreach($detalle as $d){
			$d['dc_cantidad'] = moneda_local($d['dc_cantidad']);
			echo("<tr class='main'>
				<td><strong>{$d['dg_producto']}</strong>");
				$form->Hidden('prod[]',$d['dc_producto']);
				$form->Hidden('cant[]',$d['dc_cantidad']);
				echo("</td>
				<td>{$d['dg_descripcion']}</td>
				<td>{$d['dc_cantidad']}</td>
			</tr>");
		}
	echo("</tbody></table>
	</div>");
	$form->Hidden('ot_proyecto',$data_proy['dc_proyecto']);
	$form->End('Crear','addbtn');

?>
</div></div>
<script type="text/javascript">
$("select#cli_id").change(function(){
	$('#cot_contacto').attr('disabled',true);
	loadFile('sites/produccion/switch_cliente.php?id='+$(this).val(),'#cot_contacto','',function(){
		$('#cot_contacto').attr('disabled',false);
	});
});
</script>