<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$proyecto =& $_POST['prod_proyecto'];
$costeo = $db->select('tb_produccion_proyecto','dc_costeo_actual',"dc_proyecto={$proyecto}");

if(!count($costeo)){
	$error_man->showWarning("Ocurrió un error inesperado, el proyecto al que desea agregar el producto no existe.");
	exit();
}

$costeo = $costeo[0]['dc_costeo_actual'];
if($costeo == 0){
	
	$db->start_transaction();
	
	$proy_costeos = $db->select('tb_produccion_costeo','1',"dc_proyecto={$proyecto}");
	$costeo = $db->insert('tb_produccion_costeo',
	array(
		'dc_correlativo' => count($proy_costeos)+1,
		'dc_proyecto' => $proyecto,
		'dc_empresa' => $empresa,
		'df_emision' => 'NOW()',
		'dc_usuario_creacion' => $idUsuario
	));
	
	$db->update('tb_produccion_proyecto',
	array(
		'dc_costeo_actual' => $costeo
	),"dc_proyecto={$proyecto}");
	
	$db->commit();
	
}

$producto = $db->insert('tb_produccion_producto',
array(
	"dg_producto" => $_POST['prod_name'],
	"dc_costeo" => $costeo,
	"dg_descripcion" => $_POST['prod_descripcion'],
	"dc_dias_fabricacion" => $_POST['prod_fabricacion'],
	"dc_empresa" => $empresa,
	"df_creacion" => "NOW()",
	"dc_usuario_creacion" => $idUsuario
));

	$total = 0;
	if(isset($_POST['cost_material'])){
		foreach($_POST['cost_material'] as $i => $v){
			$db->insert('tb_produccion_producto_detalle',
			array(
				'dc_producto' => $producto,
				'dc_material' => $v,
				'dq_precio' => $_POST['cost_precio'][$i],
				'dc_cantidad' => $_POST['cost_cantidad'][$i],
				'dq_total' => $_POST['cost_cantidad'][$i]*$_POST['cost_precio'][$i]
			));
			
			$total += $_POST['cost_cantidad'][$i]*$_POST['cost_precio'][$i];
		}
	}
	
	if(isset($_POST['cost_service'])){
		foreach($_POST['cost_service'] as $i => $v){
			$db->insert('tb_produccion_producto_detalle_servicio',
			array(
				'dc_producto' => $producto,
				'dc_servicio' => $v,
				//'dc_funcionario' => $_POST['cost_funcionario'][$i],
				'dq_precio' => $_POST['cost_precio2'][$i],
				'dc_cantidad' => $_POST['cost_cantidad2'][$i],
				'dq_total' => $_POST['cost_cantidad2'][$i]*$_POST['cost_precio2'][$i]
			));
			
			$total += $_POST['cost_cantidad2'][$i]*$_POST['cost_precio2'][$i];
		}
	}
	
	$db->update('tb_produccion_producto',array(
		'dq_precio' => $total
	),"dc_producto = {$producto}");
	
	$error_man->showConfirm("Producto creado correctamente");
	
?>
<script type="text/javascript">
	$('#show_proyecto').html("<img src='images/ajax-loader.gif' alt='' /> cargando proyecto ...");
	$('#genOverlay').remove();
	loadFile("sites/produccion/proc/show_proyecto.php?id="+id_proyecto,'#show_proyecto','',globalFunction);
</script>