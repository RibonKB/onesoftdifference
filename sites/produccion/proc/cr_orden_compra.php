<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->escape($_POST['pro_number']);

$prov_data = $db->select('tb_proveedor','dc_proveedor,dg_razon,dc_contacto_default',"dg_rut='{$_POST['pro_proveedor']}'");

if(!count($prov_data)){
	$error_man->showErrorRedirect("No se encontró el proveedor especificado","sites/produccion/src_proyecto.php");
}

$prov_data = $prov_data[0];

/*$data_proy = $db->select("(SELECT * FROM tb_produccion_proyecto WHERE dc_empresa = {$empresa} AND ) pr
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON cl.dc_cliente = pr.dc_cliente",
'pr.dg_proyecto,pr.dc_proyecto,pr.dc_cliente,pr.dg_cliente,pr.dc_costeo_actual,cl.dg_razon,cl.dc_contacto_default,pr.dm_permiso');*/

$data_proy = $db->select('tb_produccion_proyecto','dg_proyecto,dc_proyecto,dc_costeo_actual_oc,dm_permiso',
"dc_empresa={$empresa} AND dq_proyecto = {$_POST['pro_number']} AND dm_permiso<>'1'");

if(!count($data_proy)){
	$error_man->showErrorRedirect("No se encontró el proyecto especificado o este està bloqueado para ser cotizado.","sites/produccion/src_proyecto.php");
}

$data_proy = $data_proy[0];

if($data_proy['dc_costeo_actual_oc'] == 0 && $data_proy['dm_permiso'] != 2){
	$error_man->showErrorRedirect("El proyecto <b>{$data_proy['dg_proyecto']}</b> debe ser costeado antes de poder crear una orden de compra",
	"sites/produccion/src_proyecto.php");
}

$detalle = $db->select("(SELECT * FROM tb_produccion_producto WHERE dc_costeo={$data_proy['dc_costeo_actual_oc']}) p
JOIN tb_produccion_producto_detalle d ON d.dc_producto = p.dc_producto
JOIN tb_produccion_material m ON m.dc_material = d.dc_material",
'p.dg_producto,d.dc_cantidad,d.dq_precio,d.dq_total,m.dc_material,m.dg_material');

if(!count($detalle) && $data_proy['dm_permiso'] != 2 ){
	$error_man->showErrorRedirect("El proyecto <b>{$data_proy['dg_proyecto']}</b> No ha sido costeado","sites/produccion/src_proyecto.php");
}

echo("<div id='secc_bar'>Creación de orden de compra</div>
<div id='main_cont'>
<div class='panes' style='width:80%;'>
<div class='title center'>");

/*if($data_proy['dc_cliente'] != 0)
echo("<!button class='right button' id='cli_switch'>Cambiar Proveedor</button>");*/

echo("Generando orden de compra para <strong id='prov_razon' style='color:#000;'>{$prov_data['dg_razon']}</strong>
</div>
<hr class='clear' />");

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/produccion/proc/crear_orden_compra.php","cr_orden_compra",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la orden de compra</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Listado('Contacto','oc_contacto',
	"(SELECT * FROM tb_domicilio_proveedor WHERE dc_proveedor = {$prov_data['dc_proveedor']}) dc
	  LEFT JOIN tb_comuna co ON dc.dc_comuna = co.dc_comuna
	  JOIN (SELECT * FROM tb_domicilio_proveedor_tipo_direccion WHERE dm_activo ='1') td ON dc.dc_domicilio = td.dc_domicilio
	  JOIN tb_contacto_proveedor cc ON td.dc_contacto = cc.dc_contacto",
	array('DISTINCT cc.dc_contacto','dc.dg_direccion','co.dg_comuna','cc.dg_contacto'),1,$prov_data['dc_contacto_default'],'');

	$form->Textarea('Observaciones','oc_observacion');
	
	echo("<hr class='clear' />
	<div id='prods'>
	<br />
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='20'>Opciones</th>
		<th>Producto</th>
		<th>Material</th>
		<th width='250'>Descripción</th>
		<th width='63'>Cantidad</th>
		<th width='85'>Costo</th>
		<th width='85'>Costo total</th>
	</tr>
	</thead>
	<tbody id='prod_list'>");
	foreach($detalle as $d){
	echo("<tr class='main'>
		<td><img src='images/delbtn.png' class='deldetail' /></td>
		<td><b>{$d['dg_producto']}</b>");
		$form->Hidden('prod[]',$d['dc_material']);
		echo("</td>
		<td>{$d['dg_material']}</td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='35' /></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' value='{$d['dc_cantidad']}' style='text-align:right;' required='required' /></td>

		<td align='right'>
			<input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' value='{$d['dq_precio']}' readonly='readonly' />
		</td>
		<td class='costo_total' align='right'>0</td>
	</tr>");
		}
	echo("</tbody>
	<tfoot>
	<tr>
		<th colspan='5' align='right'>Totales</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
	</tr>
	</tfoot>
	</table>
	<!-- div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div -->
	</div>");
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('oc_proyecto',$data_proy['dc_proyecto']);
	$form->Hidden('oc_costeo',$data_proy['dc_costeo_actual_oc']);
	$form->Hidden('pro_permiso',$data_proy['dm_permiso']);
	$form->Hidden('oc_proveedor',$prov_data['dc_proveedor']);
	$form->End('Crear','addbtn');
	
	echo("<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td>
		<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
		<input type='text' name='prod2[]' class='prod_codigo searchbtn' size='15' /></td>
		<td><input type='text' name='desc2[]' class='prod_desc inputtext' size='35' required='required' /></td>
		<td><input type='text' name='cant2[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio2[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'>
		<input type='text' name='costo2[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' />
		</td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
	</tr>
	</table>");

?>
</div></div>
<script type="text/javascript">
$('.deldetail').click(function(){
	$(this).parent().parent().remove();
});
	$("select#cli_id").change(function(){
		$('#cot_contacto').attr('disabled',true);
		loadFile('sites/produccion/switch_cliente.php?id='+$(this).val(),'#cot_contacto','',function(){
			$('#cot_contacto').attr('disabled',false);
		});
	});
	$('#cli_switch').click(function(){
		loadOverlay('sites/ventas/switch_cliente.php');
	});
	empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;
</script>
<script type="text/javascript" src="jscripts/product_manager/orden_compra.min.js?v10"></script>	
<script type="text/javascript">
	tc = 1;
	tc_dec = 0;
	actualizar_detalles();
	actualizar_totales();
</script>