<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_produccion_proyecto WHERE dc_empresa={$empresa} AND dc_proyecto={$_POST['id']}) pr
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON pr.dc_cliente = cl.dc_cliente",
"pr.dg_cliente,pr.dc_proyecto,pr.dq_proyecto,pr.dg_proyecto,DATE_FORMAT(pr.df_emision,'%d/%m/%Y') as df_emision,cl.dg_razon,
DATE_FORMAT(pr.df_termino,'%d/%m/%Y') as df_termino,dg_descripcion,dm_permiso,dc_costeo_actual,dc_cotizacion,dc_orden_trabajo,dc_cubicacion");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado el proyecto especificada");
	exit();
}

$data = $data[0];

$cotizado = $db->select('tb_produccion_cotizacion','1',"dc_proyecto={$data['dc_proyecto']}");

if(count($cotizado))
	$cotizado = 1;
else
	$cotizado = 0;

$data['dq_proyecto'] = str_pad($data['dq_proyecto'],4,'0',STR_PAD_LEFT);

echo("<div class='title center'>Proyecto &quot;{$data['dg_proyecto']}&quot;</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>");

if($data['dm_permiso'] == '1' && $data['dc_costeo_actual'] != 0){
	echo("<div id='make_cotizable' class='right'>
	<button type='button' class='imgbtn' style='background-image:url(images/doc.png);'>
		Finalizar costeo
	</button></div>");
}

$files=opendir('../files');
$valid_files = array();
while($file = readdir($files)){
	if(preg_match("/^{$data['dq_proyecto']}-{$empresa}/",$file))
		$valid_files[] = $file;
}

echo("Cliente:<br /><strong> {$data['dg_cliente']}{$data['dg_razon']}</strong></caption>
<tr>
	<td width='160'>Número</td>
	<td><b>{$data['dq_proyecto']}</td>
	<td rowspan='3' valign='top'>Archivos del proyecto:
	");
if(count($valid_files)){
echo("<ul>");
foreach($valid_files as $f){
	echo("<li><a href='sites/produccion/getfile.php?file={$f}' target='_blank'><b>{$f}</b></a></li>");
}
echo("</ul>");
}else{
	echo('<b>Sin archivos asociados</b>');
}

echo("</td>
</tr>
<tr>
	<td width='160'>Fecha emision</td>
	<td><b>{$data['df_emision']}</td>
</tr><tr>
	<td>Cierre estimado</td>
	<td><b>{$data['df_termino']}</b></td>
</tr><tr>
	<td><b>Descripción</b></td>
	<td colspan='2'>{$data['dg_descripcion']}</td>
</tr></table><hr />");

if($data['dm_permiso'] == '1' && $data['dc_cotizacion'] == 0)
echo("<div class='center' id='add_producto_cont'>
<button type='button' class='addbtn' onclick='add_producto({$_POST['id']})'>Agregar producto</button>
<hr />
</div>");

$costeos = $db->select("(SELECT * FROM tb_produccion_costeo WHERE dc_proyecto={$_POST['id']}) cos
LEFT JOIN tb_produccion_cotizacion cot ON cot.dc_cotizacion = cos.dc_cotizacion",
'cos.dc_costeo,cos.dc_correlativo,cot.dc_cotizacion,DATE_FORMAT(cos.df_emision,"%d/%m/%Y") AS df_emision,cot.dq_cotizacion',
"",array('order_by' => "dc_correlativo ASC"));

foreach($costeos as $c){

	if($c['dc_costeo'] == $data['dc_costeo_actual']){
		echo("<div style='border:1px solid #E6D875;color:#222;background:#F3EDBE;padding:5px;margin:3px;'>
		<span class='right'>Fecha emisión: <b>{$c['df_emision']}</b></span>
			<strong>Costeo {$c['dc_correlativo']}</strong> <small>(actual)</small>");
			
		$cost_prods = $db->select('tb_produccion_producto',
		'dc_producto,dg_producto,DATE_FORMAT(df_creacion,"%d/%m/%Y") as df_creacion,dc_dias_fabricacion,dq_precio',
		"dc_costeo = {$c['dc_costeo']}");
		
		foreach($cost_prods as $p){
			
			$p['dq_precio'] = moneda_local($p['dq_precio']);
			
			echo("<table class='tab' width='100%'>
			<caption>
				<span class='right'><small>Días fabricación: </small>{$p['dc_dias_fabricacion']}</span>
				<a href='sites/produccion/ed_producto.php?id={$p['dc_producto']}' class='loadOnOverlay left'>
					<img src='images/editbtn.png' alt='EDITAR' />
				</a>
				<a href='sites/produccion/del_producto.php?id={$p['dc_producto']}' class='loadOnOverlay left'>
					<img src='images/delbtn.png' alt='EDITAR' />
				</a>
				{$p['dg_producto']}
			</caption>
			<thead><tr>
				<th>Material/Servicio</th>
				<th>U. de Medida</th>
				<th>Cantidad</th>
				<th>Precio</th>
				<th>SubTotal</th>
			</tr></thead>
			<tfoot><tr>
				<th align='right' colspan='4'>Total</th>
				<th align='right'>{$p['dq_precio']}</th>
			</tr></tfoot>
			<tbody>");
			
			$det_material = $db->select("(SELECT * FROM tb_produccion_producto_detalle WHERE dc_producto = {$p['dc_producto']}) p
			JOIN (SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa}) m ON p.dc_material = m.dc_material
			JOIN (SELECT * FROM tb_unidad_medida WHERE dc_empresa={$empresa}) u ON u.dc_unidad_medida = m.dc_unidad_medida",
			'p.dc_cantidad,p.dq_precio,p.dq_total,m.dg_material,u.dg_unidad_medida');
			
			if(count($det_material)){
				echo("<tr><th colspan='5' style='background:#BBB;color:#555;font-size:8px;'>MATERIALES</td></tr>");
				foreach($det_material as $d){
				$d['dq_precio'] = moneda_local($d['dq_precio']);
				$d['dq_total'] = moneda_local($d['dq_total']);
					echo("<tr>
						<td><b>{$d['dg_material']}</b></td>
						<td>{$d['dg_unidad_medida']}</td>
						<td>{$d['dc_cantidad']}</td>
						<td align='right'>{$d['dq_precio']}</td>
						<td align='right'>{$d['dq_total']}</td>
					</tr>");
				}
			}
			
			$det_servicio = $db->select("(SELECT * FROM tb_produccion_producto_detalle_servicio WHERE dc_producto = {$p['dc_producto']}) p
			JOIN (SELECT * FROM tb_produccion_servicio WHERE dc_empresa = {$empresa}) s ON p.dc_servicio = s.dc_servicio",
			'p.dc_cantidad,p.dq_precio,p.dq_total,s.dg_servicio');
			
			if(count($det_servicio)){
				echo("<tr><th colspan='5' style='background:#BBB;color:#555;font-size:8px;'>SERVICIOS</td></tr>");
				foreach($det_servicio as $d){
				$d['dq_precio'] = moneda_local($d['dq_precio']);
				$d['dq_total'] = moneda_local($d['dq_total']);
					echo("<tr>
						<td><b>{$d['dg_servicio']}</b></td>
						<td>HH</td>
						<td>{$d['dc_cantidad']}</td>
						<td align='right'>{$d['dq_precio']}</td>
						<td align='right'>{$d['dq_total']}</td>
					</tr>");
				}
			}
			
			echo("<br />");
		}
		
		echo("</div>");
	}else{
		
		if($c['dc_cotizacion'] == 0){
			$cotbtn = "<button class='button' disabled='disabled'>Sin cotización asignada</button>";
		}else{
			$cotbtn = "<button class='button' onclick='ver_cotizacion({$c['dc_cotizacion']})'>Ver Cotización (<b>{$c['dq_cotizacion']}</b>)</button>";
			if($data['dc_cotizacion'] == 0 && $data['dc_costeo_actual'] == 0)
				$cotbtn .= "<a href='sites/produccion/proc/set_proyecto_cotizacion.php?id={$c['dc_cotizacion']}' class='loadOnOverlay button'>Seleccionar como definitiva</a>";
		}
		
		if($data['dc_costeo_actual'] == 0 && $data['dc_cotizacion'] == 0)
			$templbtn = "<button class='button' onclick='templ_costeo({$c['dc_costeo']})'>Duplicar como actual</button>";
		else
			$templbtn = '';
			
		if($c['dc_cotizacion'] == $data['dc_cotizacion'])
			$bgcolor = "ADFF95";
		else
			$bgcolor = "F6F6F6";
		
		echo("<div style='border:1px solid #DDD;color:#222;background:#{$bgcolor};padding:5px;margin:3px;'>
		<span class='right'>Fecha emisión: <b>{$c['df_emision']}</b></span>
			<strong>Costeo {$c['dc_correlativo']}</strong>
			<button class='searchbtn' onclick='ver_detalle_costeo({$c['dc_costeo']})'>Ver Detalle Costeo</button>
			{$cotbtn}
			{$templbtn}
		</div>");
	}
	
	if($c['dc_costeo'] == $data['dc_costeo_actual'])
		$current = $c;

}

if($data['dm_permiso'] == 2)
	$cotizaciones = $db->select("tb_produccion_cotizacion","dc_cotizacion,dq_cotizacion,DATE_FORMAT(df_fecha_emision,'%d/%m/%Y') AS df_fecha_emision","dc_proyecto={$_POST['id']}");
else
	$cotizaciones = array();
	
foreach($cotizaciones as $cot){
	
	if($cot['dc_cotizacion'] == $data['dc_cotizacion'])
		$bgcolor = "ADFF95";
	else
		$bgcolor = "F6F6F6";
	
	echo("<div style='border:1px solid #DDD;color:#222;background:#{$bgcolor};padding:5px;margin:3px;'>
	<span class='right'>Fecha emisión: <b>{$cot['df_fecha_emision']}</b></span>
	Cotización N° <strong>{$cot['dq_cotizacion']}</strong>
	<button type='button' class='button' onclick='ver_cotizacion({$cot['dc_cotizacion']});'>Versión de impresión</button>");
	
	if($data['dc_cotizacion'] == 0)
		echo("<a href='sites/produccion/proc/set_proyecto_cotizacion.php?id={$cot['dc_cotizacion']}' class='loadOnOverlay button'>Seleccionar como definitiva</a>");
	
	echo("</div>");
}

?>
<script type="text/javascript">
var id_proyecto = <?=$_POST['id'] ?>;

<?php if($data['dc_cotizacion'] != 0 && $data['dc_orden_trabajo'] == 0): ?>
	enable_button('#orden_trabajo_proyecto');
	$('#orden_trabajo_proyecto').unbind('click').click(function(){
		loadpage("sites/produccion/proc/cr_orden_trabajo.php?pro_number=<?=$data['dq_proyecto'] ?>");
	});
<?php else: ?>
	disable_button('#orden_trabajo_proyecto');
	$('#orden_trabajo_proyecto').unbind('click');
<?php endif; ?>

<?php if($data['dc_orden_trabajo'] != 0 && $data['dc_cubicacion'] == 0): ?>
	enable_button('#cubica_proyecto');
	$('#cubica_proyecto').unbind('click').click(function(){
		loadpage("sites/produccion/proc/cr_cubicacion.php?pro_number=<?=$data['dq_proyecto'] ?>");
	});
<?php else: ?>
	disable_button('#cubica_proyecto');
	$('#cubica_proyecto').unbind('click');
<?php endif; ?>


$('#edit_proyecto').unbind('click').click(function(){
	loadOverlay("sites/produccion/ed_proyecto.php?id=<?=$_POST['id'] ?>");
});
$('#add_cost').unbind('click').click(function(){
	loadOverlay("sites/produccion/cr_costeo.php?id=<?=$_POST['id'] ?>");
});
$('#orden_compra_proyecto').unbind('click').click(function(){
	loadOverlay("sites/produccion/cr_orden_compra.php?pro_number=<?=$data['dq_proyecto'] ?>");
});

<?php if($data['dm_permiso'] != 1): ?>
enable_button('#cotizar_proyecto');
$('#cotizar_proyecto').unbind('click').click(function(){
	loadpage("sites/produccion/proc/cr_cotizacion.php?pro_number=<?=$data['dc_proyecto'] ?>");
});
<?php else: ?>
disable_button('#cotizar_proyecto');
$('#cotizar_proyecto').unbind('click');
<?php endif; ?>


$('#make_cotizable button').click(function(){
	if(confirm('Está seguro que desea continuar')){
		$('#add_producto_cont').remove();
		$('#make_cotizable').html('<img src="images/ajax-loader.gif" alt="cargando..." />');
		loadFile('sites/produccion/proc/set_cotizable.php','#make_cotizable','id=<?=$_POST['id'] ?>');
	}
});
</script>