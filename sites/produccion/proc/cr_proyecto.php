<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

	$db->escape($_POST['cli_rut']);
	$data = $db->select("(SELECT * FROM tb_cliente WHERE dg_rut = '{$_POST['cli_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1') cl
	LEFT JOIN tb_contacto_cliente c ON c.dc_contacto = dc_contacto_default",
	"cl.dc_cliente,cl.dg_razon,cl.dg_giro,c.dg_contacto,c.dg_fono,cl.dg_rut");
	
	if(!count($data))
		$error_man->showErrorRedirect("No se encontró el cliente especificado, intentelo nuevamente.","sites/produccion/cr_proyecto.php");
	$data = $data[0];
	echo("<div id='secc_bar'>Creación de proyecto</div>
	<div id='main_cont'><div class='panes'>");
	
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/produccion/proc/crear_proyecto.php","cr_proyecto");
	$form->Header("Indique los datos para la creación del nuevo proyecto.");
	echo("<div class='right'>");
	$form->Date('Fecha emisión','proy_emision',1,0);
	echo("</div>");
	$form->Text("Indique el nombre del proyecto","proy_name",1);
	echo("<fieldset><legend>Datos del cliente al que se le hará el proyecto.</legend>");
	$form->Section();
	echo("<dl>
	<dt>Razón social</dt>
	<dd><b>{$data['dg_razon']}</b></dd>
	<dt>RUT</dt>
	<dd><b>{$data['dg_rut']}</b></dd>
	<dt>Giro</dt>
	<dd><b>{$data['dg_giro']}</b></dd>
	</dl>");
	$form->EndSection();
	$form->Section();
	echo("<fieldset style='min-width:300px;'><legend>Datos de contacto</legend>
	<dl>
	<dt>Domicilio</dt>
		<dd><b>{$data['dg_contacto']}</b></dd>
	<dt>Contacto</dt>
		<dd><b>{$data['dg_contacto']}</b></dd>
	<dt>Teléfono</dt>
		<dd><b>{$data['dg_fono']}</b></dd>
	</dl>
	</fieldset>");
	$form->EndSection();
	echo("</fieldset>");
	$form->Hidden('proy_cli',$data['dc_cliente']);
	$form->End("Crear",'addbtn');
	
	echo("</div></div>");
	
?>