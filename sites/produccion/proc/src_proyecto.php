<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['pr_emision_desde'] = $db->sqlDate($_POST['pr_emision_desde']);
$_POST['pr_emision_hasta'] = $db->sqlDate($_POST['pr_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['pr_emision_desde']} AND {$_POST['pr_emision_hasta']})";

if($_POST['pr_numero_desde']){
	if($_POST['pr_numero_hasta'])
		$conditions .= " AND (dq_proyecto BETWEEN {$_POST['pr_numero_desde']} AND {$_POST['pr_numero_hasta']})";
	else
		$conditions .= " AND dq_proyecto = {$_POST['pr_numero_desde']}";
}

if($_POST['pr_cierre_desde']){
$_POST['pr_cierre_desde'] = $db->sqlDate($_POST['pr_cierre_desde']);
	if($_POST['pr_cierre_hasta']){
		$_POST['pr_cierre_hasta'] = $db->sqlDate($_POST['pr_cierre_hasta']);
		$conditions .= " AND (df_termino BETWEEN {$_POST['pr_cierre_desde']} AND {$_POST['pr_cierre_hasta']})";
	}else
		$condition .= " AND df_termino = {$_POST['pr_cierre_desde']}";
}

if(isset($_POST['pr_client'])){
	$_POST['pr_client'] = implode(',',$_POST['pr_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['pr_client']})";
}

$data = $db->select('tb_produccion_proyecto','dc_proyecto,dq_proyecto',$conditions,array('order_by' => 'df_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron proyectos con los criterios especificados");
	exit();
}

echo("<div id='show_proyecto'></div>");

echo('<hr>
<b>Leyenda</b><br />
<div style="border:1px solid #DDD;width:15px;height:15px;background:#F3EDBE;margin:3px;" class="left"></div> Costeo actual<br class="clear" />
<div style="border:1px solid #DDD;width:15px;height:15px;background:#ADFF95;margin:3px;" class="left"></div> Cotización definitiva<br class="clear" />');

echo("
<div style='position:absolute;left:180px;top:45px;background:#EEE;padding:0 20px;'>
<div id='res_list' style='display:none;position:absolute;top:26px;left:24px;height:400px;overflow:auto;width:180px;'>
<table class='tab sortable' width='100%'>
<caption>Proyectos<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Proyecto</th>
	</tr>
</thead>
<tbody>");

foreach($data as $o){
$o['dq_proyecto'] = str_pad($o['dq_proyecto'],4,'0',STR_PAD_LEFT);
echo("
<tr>
	<td align='left'>
		<a href='sites/produccion/proc/show_proyecto.php?id={$o['dc_proyecto']}' class='pr_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$o['dq_proyecto']}</a>
	</td>
</tr>");
}

echo("
</tbody>
</table>
</div>

<button class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='editbtn' id='edit_proyecto'>Editar</button>
<button type='button' id='cotizar_proyecto' class='imgbtn' style='background-image:url(images/doc.png)'>Cotizar</button>
<button type='button' id='orden_trabajo_proyecto' class='imgbtn' style='background-image:url(images/doc.png)'>Crear Orden de trabajo</button>
<button type='button' id='cubica_proyecto' class='imgbtn' style='background-image:url(images/doc.png)'>Cubicar</button>
<button type='button' id='orden_compra_proyecto' class='imgbtn' style='background-image:url(images/doc.png)'>Crear Orden de compra</button>
</div>
");

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

echo("<table class='hidden' id='template_detail'><tr class='templ_material'>
<td><img src='images/delbtn.png' class='del_detail' alt='[X]' /></td>
<td class='material_list'>");
echo("<input type='text' class='src_material searchbtn' />");
$form->Hidden('cost_material[]',0);
echo("</td><td></td><td>");
$form->Text('','cost_cantidad[]',1,11,1,'inputtext',10);
echo("</td><td>");
$form->Text('','cost_precio[]',1,22,1,'inputtext',10);
echo("</td><td align='right' class='cost_price'>1</td></tr><tr class='templ_service'>
<td><img src='images/delbtn.png' class='del_detail' alt='[X]' /></td>
<td>");
echo("<input type='text' class='src_service searchbtn' size='10' />");
$form->Hidden('cost_service[]',0);
echo("</td><td>HH</td><td>");
$form->Text('','cost_cantidad2[]',1,11,1,'inputtext',10);
echo("</td><td>");
$form->Text('','cost_precio2[]',1,22,1,'inputtext',10);
echo("</td><td align='right'>0</td></tr></table>");

$unidades_medida = array();
$mats = $db->select("(SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa}) m
LEFT JOIN tb_unidad_medida u ON u.dc_unidad_medida = m.dc_unidad_medida",
"m.dc_material,u.dg_unidad_medida");
foreach($mats as $u){
	$unidades_medida[$u['dc_material']] = $u['dg_unidad_medida'];
}
?>

<script type="text/javascript">
var unidades_medida = <?=json_encode($unidades_medida) ?>;
</script>
<script type="text/javascript" src="jscripts/product_manager/proyecto.js?v1_1"></script>