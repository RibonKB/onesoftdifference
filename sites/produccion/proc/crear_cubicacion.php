<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$proy_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_proyecto']){
	case '1':$proy_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$proy_prefix = date("Ym"); $largo_prefix = 6; break;
}

$db->start_transaction();

$last_date = $db->select('tb_produccion_cubicacion','MAX(df_emision) fecha',"dc_empresa={$empresa}");

if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_produccion_cubicacion','dq_cubicacion',"dc_empresa={$empresa} AND df_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_cubicacion'],0,$largo_prefix) == $proy_prefix || $empresa_conf['dc_correlativo_proyecto'] == '3'){
		$proy_num = substr($last[0]['dq_cubicacion'],$largo_prefix)+1;
	}else{
		$proy_num = $empresa_conf['dg_correlativo_proyecto'];
	}
}else{
	$proy_num = $empresa_conf['dg_correlativo_proyecto'];
}

$cubicacion = $db->insert('tb_produccion_cubicacion',array(
	'dq_cubicacion' => $proy_prefix.str_pad($proy_num,4,'0',STR_PAD_LEFT),
	'dc_proyecto' => $_POST['ot_proyecto'],
	'dc_empresa' => $empresa,
	'df_emision' => 'NOW()',
	'dc_usuario_creacion' => $idUsuario
));

foreach($_POST['cub_prod'] as $i => $v){
	$db->insert('tb_produccion_cubicacion_detalle',array(
		'dc_cubicacion' => $cubicacion,
		'dc_material' => $_POST['cub_material'][$i],
		'dc_producto' => $v,
		'dc_cantidad' => $_POST['cub_cantidad'][$i]
	));
}

$db->commit();

$cant_det = count($_POST['cub_prod']);

$error_man->showConfirm('Se ha generado la cubicación correctamente');
echo("<div class='title'>El número de cubicación es <h1 style='margin:0;color:#000;'>".$proy_prefix.str_pad($proy_num,4,'0',STR_PAD_LEFT)."</h1></div>");
$error_man->showConfirm("Fueron agregados {$cant_det} elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/produccion/ver_cotizacion.php?id={$cubicacion}','cubicacion','width=800,height=600')\"> Haga clic aquí para ver la cubicación</a>");


?>