<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$proy_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_proyecto']){
	case '1':$proy_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$proy_prefix = date("Ym"); $largo_prefix = 6; break;
}

$estado = $db->select('tb_valor_defecto','dc_valor',"dg_tabla = 'tb_cotizacion_estado' AND dc_empresa={$empresa}",array('order_by'=> 'df_creacion DESC'));

$db->start_transaction();
$last_date = $db->select('tb_produccion_proyecto','MAX(df_emision) fecha',"dc_empresa={$empresa}");

if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_produccion_proyecto','dq_proyecto',"dc_empresa={$empresa} AND df_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_proyecto'],0,$largo_prefix) == $proy_prefix || $empresa_conf['dc_correlativo_proyecto'] == '3'){
		$proy_num = substr($last[0]['dq_proyecto'],$largo_prefix)+1;
	}else{
		$proy_num = $empresa_conf['dg_correlativo_proyecto'];
	}
}else{
	$proy_num = $empresa_conf['dg_correlativo_proyecto'];
}

$proyecto = $db->insert('tb_produccion_proyecto',
array(
	"dg_proyecto" => $_POST['op_name'],
	"dg_descripcion" => $_POST['op_descripcion'],
	"dc_cliente" => !isset($_POST['op_cliente_txt'])?$_POST['op_cliente']:0,
	"dg_cliente" => isset($_POST['op_cliente_txt'])?$_POST['op_cliente_txt']:'',
	"dm_requiere_diseno" => isset($_POST['req_design'])?1:0,
	"dm_permiso" => isset($_POST['req_cost'])?1:2,
	"df_emision" => 'NOW()',
	"df_termino" => $db->sqlDate($_POST['op_termino']),
	"dc_empresa" => $empresa,
	"dq_proyecto" => $proy_prefix.str_pad($proy_num,4,'0',STR_PAD_LEFT)
));

$db->commit();

$number = $proy_prefix.str_pad($proy_num,4,'0',STR_PAD_LEFT);

if(!isset($_POST['req_cost']))
	$cot_btn = "<button type='button' class='button' id='cot_proyecto'>Cotizar proyecto</button>";
else
	$cot_btn = "";
	
if(isset($_FILES['adfile'])){
	foreach($_FILES['adfile']['error'] as $i => $v){
		if($v != UPLOAD_ERR_OK)
			continue;
		$fname = str_replace(' ','_',$_FILES['adfile']['name'][$i]);
     	$fname = ereg_replace('[^\.A-Za-z0-9_]','',$fname);
		$fname = "{$number}-{$empresa}-{$fname}";
		
		if(!move_uploaded_file($_FILES['adfile']['tmp_name'][$i],"../files/$fname")){
			$error_man->showWarning("No se pudo subir el archivo {$_FILES['adfile']['name'][$i]}");
		}
	}
}

$error_man->showConfirm("Se ha generado el proyecto correctamente.
<div class='title'>El número de proyecto es {$number}</div>{$cot_btn}");

?>
<script type="text/javascript">
	$('#cot_proyecto').click(function(){
		loadpage("sites/produccion/proc/cr_cotizacion.php?pro_number=<?=$number ?>");
	});
</script>