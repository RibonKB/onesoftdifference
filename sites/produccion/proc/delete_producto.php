<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$db->query("DELETE FROM tb_produccion_producto WHERE dc_producto={$_POST['prod_id']}");
$db->query("DELETE FROM tb_produccion_producto_detalle WHERE dc_producto={$_POST['prod_id']}");
$db->query("DELETE FROM tb_produccion_producto_detalle_servicio WHERE dc_producto={$_POST['prod_id']}");

$db->commit();

?>
<script type="text/javascript">
	$('#show_proyecto').html("<img src='images/ajax-loader.gif' alt='' /> cargando proyecto ...");
	$('#genOverlay').remove();
	loadFile("sites/produccion/proc/show_proyecto.php?id="+id_proyecto,'#show_proyecto','',globalFunction);
</script>