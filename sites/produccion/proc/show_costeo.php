<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<button onclick='window.open(\"sites/produccion/ver_costeo.php?id={$_POST['id']}\",\"print_costeo\",\"width=800;height=600\");' class='button'>Versión de impresión</button>");

$cost_prods = $db->select('tb_produccion_producto',
		'dc_producto,dg_producto,DATE_FORMAT(df_creacion,"%d/%m/%Y") as df_creacion,dc_dias_fabricacion,dq_precio',
		"dc_costeo = {$_POST['id']}");
		
		foreach($cost_prods as $p){
			
			$p['dq_precio'] = moneda_local($p['dq_precio']);
			
			echo("<table class='tab' width='100%'>
			<caption>
				<span class='right'><small>Días fabricación: </small>{$p['dc_dias_fabricacion']}</span>
				{$p['dg_producto']}
			</caption>
			<thead><tr>
				<th>Material/Servicio</th>
				<th>U. de Medida</th>
				<th>Cantidad</th>
				<th>Precio</th>
				<th>SubTotal</th>
			</tr></thead>
			<tfoot><tr>
				<th align='right' colspan='4'>Total</th>
				<th align='right'>{$p['dq_precio']}</th>
			</tr></tfoot>
			<tbody>");
			
			$det_material = $db->select("(SELECT * FROM tb_produccion_producto_detalle WHERE dc_producto = {$p['dc_producto']}) p
			JOIN (SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa}) m ON p.dc_material = m.dc_material
			JOIN (SELECT * FROM tb_unidad_medida WHERE dc_empresa={$empresa}) u ON u.dc_unidad_medida = m.dc_unidad_medida",
			'p.dc_cantidad,p.dq_precio,p.dq_total,m.dg_material,u.dg_unidad_medida');
			
			if(count($det_material)){
				echo("<tr><th colspan='5' style='background:#BBB;color:#555;font-size:8px;'>MATERIALES</td></tr>");
				foreach($det_material as $d){
				$d['dq_precio'] = moneda_local($d['dq_precio']);
				$d['dq_total'] = moneda_local($d['dq_total']);
					echo("<tr>
						<td><b>{$d['dg_material']}</b></td>
						<td>{$d['dg_unidad_medida']}</td>
						<td>{$d['dc_cantidad']}</td>
						<td align='right'>{$d['dq_precio']}</td>
						<td align='right'>{$d['dq_total']}</td>
					</tr>");
				}
			}
			
			$det_servicio = $db->select("(SELECT * FROM tb_produccion_producto_detalle_servicio WHERE dc_producto = {$p['dc_producto']}) p
			JOIN (SELECT * FROM tb_produccion_servicio WHERE dc_empresa = {$empresa}) s ON p.dc_servicio = s.dc_servicio",
			'p.dc_cantidad,p.dq_precio,p.dq_total,s.dg_servicio');
			
			if(count($det_servicio)){
				echo("<tr><th colspan='5' style='background:#BBB;color:#555;font-size:8px;'>SERVICIOS</td></tr>");
				foreach($det_servicio as $d){
				$d['dq_precio'] = moneda_local($d['dq_precio']);
				$d['dq_total'] = moneda_local($d['dq_total']);
					echo("<tr>
						<td><b>{$d['dg_servicio']}</b></td>
						<td>HH</td>
						<td>{$d['dc_cantidad']}</td>
						<td align='right'>{$d['dq_precio']}</td>
						<td align='right'>{$d['dq_total']}</td>
					</tr>");
				}
			}
			
			echo("<br />");
		}
		
		echo("</div>");

?>