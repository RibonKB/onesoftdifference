<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['pr_emision_desde'] = $db->sqlDate($_POST['pr_emision_desde']);
$_POST['pr_emision_hasta'] = $db->sqlDate($_POST['pr_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['pr_emision_desde']} AND {$_POST['pr_emision_hasta']})";

if($_POST['pr_numero_desde']){
	if($_POST['pr_numero_hasta'])
		$conditions .= " AND (dq_cotizacion BETWEEN {$_POST['pr_numero_desde']} AND {$_POST['pr_numero_hasta']})";
	else
		$conditions .= " AND dq_cotizacion = {$_POST['pr_numero_desde']}";
}

if(isset($_POST['pr_client'])){
	$_POST['pr_client'] = implode(',',$_POST['pr_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['pr_client']})";
}

$data = $db->select('tb_produccion_cotizacion','dc_cotizacion,dq_cotizacion',$conditions,array('order_by' => 'df_fecha_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron cotizaciones con los criterios especificados");
	exit();
}

echo("<div id='show_cotizacion'></div>");

echo("
<div style='position:absolute;left:180px;top:45px;background:#EEE;padding:0 20px;'>
<div id='res_list' style='display:none;position:absolute;top:26px;left:24px;height:400px;overflow:auto;width:180px;'>
<table class='tab sortable' width='100%'>
<caption>Proyectos<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Cotización</th>
	</tr>
</thead>
<tbody>");

foreach($data as $o){
$o['dq_cotizacion'] = str_pad($o['dq_cotizacion'],4,'0',STR_PAD_LEFT);
echo("
<tr>
	<td align='left'>
		<a href='sites/produccion/proc/show_cotizacion.php?id={$o['dc_cotizacion']}' class='cot_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$o['dq_cotizacion']}</a>
	</td>
</tr>");
}

echo("
</tbody>
</table>
</div>

<button class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='editbtn' id='edit_cotizacion'>Editar</button>
</div>
");
?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".cot_load").click(function(e){
		e.preventDefault();
		$('#show_cotizacion').html("<img src='images/ajax-loader.gif' alt='' /> cargando cotización ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width(900);
		loadFile($(this).attr('href'),'#show_cotizacion','',globalFunction);
	}).first().trigger('click');
	
	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>