<?php
/**
*	Ingresa una nueva orden de trabajo
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_nota_venta']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$db->start_transaction();
$last_date = $db->select('tb_produccion_orden_trabajo','MAX(df_fecha_emision) fecha',"dc_empresa={$empresa}");

if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_produccion_orden_trabajo','dq_orden_trabajo',"dc_empresa={$empresa} AND df_fecha_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_orden_trabajo'],0,$largo_prefix) == $cot_prefix || $empresa_conf['dc_correlativo_nota_venta'] == '3'){
		$cot_num = substr($last[0]['dq_orden_trabajo'],$largo_prefix)+1;
	}else{
		$cot_num = $empresa_conf['dg_correlativo_nota_venta'];
	}
}else{
	$cot_num = $empresa_conf['dg_correlativo_nota_venta'];
}

$orden_trabajo = $db->insert("tb_produccion_orden_trabajo",
array(
	'dq_orden_trabajo' => $cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT),
	'dg_observacion' => $_POST['ot_observacion'],
	'df_entrega' => $db->sqlDate($_POST['ot_entrega']),
	'dc_encargado' => $_POST['ot_encargado'],
	'dc_empresa' => $empresa,
	'dc_contacto' => $_POST['ot_contacto'],
	'dc_usuario_creacion' => $idUsuario,
	'dc_cliente' => $_POST['cli_id'],
	'df_fecha_emision' => 'NOW()',
	'dc_proyecto' => $_POST['ot_proyecto']
));

if(isset($_POST['prod'])){
	foreach($_POST['prod'] as $i => $v){
		$db->insert('tb_produccion_orden_trabajo_detalle',array(
			'dc_orden_trabajo' => $orden_trabajo,
			'dc_cantidad' => $_POST['cant'][$i],
			'dc_producto' => $v
		));
	}
}

$db->update('tb_produccion_proyecto',array("dc_orden_trabajo" => $orden_trabajo),"dc_proyecto={$_POST['ot_proyecto']}");

$cant_det = count($_POST['prod']);

$db->commit();


$error_man->showConfirm("Se ha generado la orden de trabajo");
echo("<div class='title'>El número de orden de trabajo es <h1 style='margin:0;color:#000;'>".$cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT)."</h1></div>");
$error_man->showConfirm("Fueron agregados {$cant_det} elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/produccion/ver_orden_trabajo.php?id={$orden_trabajo}','orden_trabajo','width=800,height=600')\"> Haga clic aquí para ver la orden de trabajo</a>");


?>