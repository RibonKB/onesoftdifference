<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$db->query("DELETE FROM tb_produccion_producto_detalle WHERE dc_producto={$_POST['prod_id']}");
$db->query("DELETE FROM tb_produccion_producto_detalle_servicio WHERE dc_producto={$_POST['prod_id']}");

$total = 0;
if(isset($_POST['cost_material'])){
	foreach($_POST['cost_material'] as $i => $v){
		$db->insert('tb_produccion_producto_detalle',
		array(
			'dc_producto' => $_POST['prod_id'],
			'dc_material' => $v,
			'dq_precio' => $_POST['cost_precio'][$i],
			'dc_cantidad' => $_POST['cost_cantidad'][$i],
			'dq_total' => $_POST['cost_cantidad'][$i]*$_POST['cost_precio'][$i]
		));
		
		$total += $_POST['cost_cantidad'][$i]*$_POST['cost_precio'][$i];
	}
}

if(isset($_POST['cost_service'])){
	foreach($_POST['cost_service'] as $i => $v){
		$db->insert('tb_produccion_producto_detalle_servicio',
		array(
			'dc_producto' => $_POST['prod_id'],
			'dc_servicio' => $v,
			'dq_precio' => $_POST['cost_precio2'][$i],
			'dc_cantidad' => $_POST['cost_cantidad2'][$i],
			'dq_total' => $_POST['cost_cantidad2'][$i]*$_POST['cost_precio2'][$i]
		));
		
		$total += $_POST['cost_cantidad2'][$i]*$_POST['cost_precio2'][$i];
	}
}

$db->update('tb_produccion_producto',array(
	"dg_producto" => $_POST['prod_name'],
	"dc_dias_fabricacion" => $_POST['prod_fabricacion'],
	"dq_precio" => $total
),"dc_producto={$_POST['prod_id']}");

$db->commit();

?>
<script type="text/javascript">
	$('#show_proyecto').html("<img src='images/ajax-loader.gif' alt='' /> cargando proyecto ...");
	$('#genOverlay').remove();
	loadFile("sites/produccion/proc/show_proyecto.php?id="+id_proyecto,'#show_proyecto','',globalFunction);
</script>