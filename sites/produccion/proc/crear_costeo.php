<?php
	define('MAIN',1);
	require_once("../../../inc/global.php");
	if(!isset($_POST['asinc'])){
		$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
		$error_man->show_fatal_error("Acceso Denegado",$options);
	}
	
	$costeo = $db->insert('tb_produccion_costeo',
	array(
		"dg_costeo" => $_POST['cost_name'],
		"dc_dias_fabricacion" => $_POST['cost_fabricacion'],
		"dc_proyecto" => $_POST['cost_proyecto'],
		"dc_usuario_creacion" => $idUsuario,
		"dc_empresa" => $empresa,
		"df_emision" => 'NOW()'
	));
	
	/*$total = 0;
	if(isset($_POST['cost_material'])){
		foreach($_POST['cost_material'] as $i => $v){
			$db->insert('tb_produccion_costeo_detalle',
			array(
				'dc_costeo' => $costeo,
				'dc_material' => $v,
				'dq_precio' => $_POST['cost_precio'][$i],
				'dc_cantidad' => $_POST['cost_cantidad'][$i],
				'dq_total' => $_POST['cost_cantidad'][$i]*$_POST['cost_precio'][$i]
			));
			
			/$total += $_POST['cost_cantidad'][$i]*$_POST['cost_precio'][$i];
		}
	}
	
	if(isset($_POST['cost_service'])){
		foreach($_POST['cost_service'] as $i => $v){
			$db->insert('tb_produccion_costeo_detalle_servicio',
			array(
				'dc_costeo' => $costeo,
				'dc_servicio' => $v,
				//'dc_funcionario' => $_POST['cost_funcionario'][$i],
				'dq_precio' => $_POST['cost_precio2'][$i],
				'dc_cantidad' => $_POST['cost_cantidad2'][$i],
				'dq_total' => $_POST['cost_cantidad2'][$i]*$_POST['cost_precio2'][$i]
			));
			
			$total += $_POST['cost_cantidad2'][$i]*$_POST['cost_precio2'][$i];
		}
	}
	
	$db->update('tb_produccion_costeo',array(
		'dq_total' => $total
	),"dc_costeo = {$costeo}");*/
	
	$error_man->showConfirm('Se ha creado el costeo');
?>
<script type="text/javascript">
	alert('Se ha creado el costeo');
	loadFile('sites/produccion/proc/show_proyecto.php?id=<?=$_POST['cost_proyecto'] ?>','#show_proyecto');
</script>
