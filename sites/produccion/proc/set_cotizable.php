<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_produccion_proyecto','dc_costeo_actual',"dc_proyecto={$_POST['id']}");

if(!count($data)){
	$error_man->showWarning("Ocurrió un error inesperado, el proyecto no existe.");
	exit();
}

if($data[0]['dc_costeo_actual'] == 0){
	$error_man->showWarning("<span style='color:#000;'>No se ha costeado</span>");
	exit();
}

$db->update('tb_produccion_proyecto',array('dm_permiso' => 0),"dc_proyecto={$_POST['id']}");

$error_man->showConfirm('HECHO!');
?>
<script type="text/javascript">
enable_button('#cotizar_proyecto');
$('#cotizar_proyecto').unbind('click').click(function(){
	loadpage("sites/produccion/proc/cr_cotizacion.php?pro_number=<?=$_POST['id'] ?>");
});
</script>