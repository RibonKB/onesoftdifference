<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_orden_compra']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$db->start_transaction();
$last_date = $db->select('tb_produccion_orden_compra','MAX(df_fecha_emision) fecha',"dc_empresa={$empresa}");

if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_produccion_orden_compra','dq_orden_compra',"dc_empresa={$empresa} AND df_fecha_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_orden_compra'],0,$largo_prefix) == $cot_prefix || $empresa_conf['dc_correlativo_orden_compra'] == '3'){
		$cot_num = substr($last[0]['dq_orden_compra'],$largo_prefix)+1;
	}else{
		$cot_num = $empresa_conf['dg_correlativo_orden_compra'];
	}
}else{
	$cot_num = $empresa_conf['dg_correlativo_orden_compra'];
}

$orden_compra = $db->insert("tb_produccion_orden_compra",
array(
	'dq_orden_compra' => $cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT),
	'dc_empresa' => $empresa,
	'dc_usuario_creacion' => $idUsuario,
	'dq_cambio' => 1,
	'dc_proveedor' => $_POST['oc_proveedor'],
	'df_fecha_emision' => 'NOW()',
	'dq_neto' => $_POST['cot_neto'],
	'dq_iva' => $_POST['cot_iva'],
	'dg_observacion' => $_POST['oc_observacion']
));

foreach($_POST['prod'] as $i => $v){
	$db->insert('tb_produccion_orden_compra_detalle',
	array(
		'dc_orden_compra' => $orden_compra,
		'dc_material' => $v,
		'dq_precio' => str_replace(',','',$_POST['costo'][$i]),
		'dq_cantidad' => $_POST['cant'][$i],
		'dg_descripcion' => $_POST['desc'][$i]
	));
}

$db->commit();

$error_man->showConfirm("Se ha generado la orden de compra");
echo("<div class='title'>El número de cotización es <h1 style='margin:0;color:#000;'>".$cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT)."</h1></div>");
$error_man->showConfirm("Fueron agregados ".count($_POST['prod'])." elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/produccion/ver_orden_compra.php?id={$orden_compra}','orden_compra','width=800,height=600')\"> Haga clic aquí para ver la orden de compra</a>");

if($_POST['pro_permiso']!=2){
	$db->update('tb_produccion_proyecto',
	array(
		"dm_permiso" => 1,
		"dc_costeo_actual_oc" => 0
	),"dc_proyecto={$_POST['oc_proyecto']}");
	
	$db->update('tb_produccion_costeo',
	array(
		"dc_orden_compra" => $orden_compra
	),"dc_costeo={$_POST['oc_costeo']}");
}

?>