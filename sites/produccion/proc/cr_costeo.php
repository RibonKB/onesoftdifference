<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->escape($_POST['proy_num']);

$data = $db->select("(SELECT * FROM tb_produccion_proyecto WHERE dq_proyecto = '{$_POST['proy_num']}' AND dc_empresa={$empresa}) pr
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON pr.dc_cliente = cl.dc_cliente",
"pr.dg_cliente,pr.dc_proyecto,pr.dq_proyecto,pr.dg_proyecto,DATE_FORMAT(pr.df_emision,'%d/%m/%Y') as df_emision,cl.dg_razon");

if(!count($data))
	$error_man->showErrorRedirect("No se encontró el proyecto especificado, intentelo nuevamente.","sites/produccion/cr_costeo.php");
$data = $data[0];
echo("<div id='secc_bar'>Costeo de proyecto</div>
<div id='main_cont'><div class='panes'>
<div class='title center'>Costeando proyecto &quot;{$data['dg_proyecto']}&quot;</div>
<br />
<table class='tab' width='70%' align='center'>
<thead>
	<tr>
		<th width='33%'>Número de proyecto</th>
		<th width='33%'>Cliente</th>
		<th>Fecha Emisión</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>{$data['dq_proyecto']}</td>
		<td>{$data['dg_razon']} {$data['dg_cliente']}</td>
		<td>{$data['df_emision']}</td>
	</tr>
</tbody></table><br />");

$id_proy =& $data['dc_proyecto'];

$costeos = $db->select('tb_produccion_costeo',
'dc_costeo,dg_costeo,DATE_FORMAT(df_emision,"%d/%m/%Y") as df_emision,dq_total,dc_dias_fabricacion',
"dc_empresa = {$empresa} AND dc_proyecto={$id_proy}");

if(!count($costeos))
	$error_man->showInfo("El proyecto especificado no posee ningún costeo a la fecha, puede agregar nuevos costeos haciendo click en el botón &quot;Agregar Costeo&quot;");
else{
$error_man->showInfo("Costeos realizados sobre el proyecto");
echo('<br />');
	foreach($costeos as $costeo){
		echo("<div style='width:80%;margin:auto;'>
				<table class='tab' width='100%'>
				<caption>
					<b class='left'>{$costeo['dg_costeo']}</b>
					<span class='right'>Fecha emisión: <b>{$costeo['df_emision']}</b><br />
					Días de fabricación: {$costeo['dc_dias_fabricacion']}</span>
					<br class='clear' />
				</caption>
				<thead>
					<tr>
						<th>Material / Servicio</th>
						<th>Unidad de medida</th>
						<th>Cantidad</th>
						<th>Precio / HH</th>
						<th>Sub Total</th>
					</tr>
				</thead>
					<tbody>");
$detalles = $db->select("tb_produccion_costeo_detalle d
LEFT JOIN tb_produccion_material p ON p.dc_material = d.dc_material
LEFT JOIN tb_unidad_medida u ON u.dc_unidad_medida = p.dc_unidad_medida","*",
"d.dc_costeo = {$costeo['dc_costeo']}");

$detalles_servicio = $db->select("tb_produccion_costeo_detalle_servicio d
LEFT JOIN tb_produccion_servicio p ON p.dc_servicio = d.dc_servicio","*",
"d.dc_costeo = {$costeo['dc_costeo']}");

if(count($detalles)){
	echo("<tr><th colspan='5' style='background:#BBB;color:#555;font-size:8px;'>MATERIALES</td></tr>");
	foreach($detalles as $d){
	$d['dq_precio'] = moneda_local($d['dq_precio']);
	$d['dq_total'] = moneda_local($d['dq_total']);
		echo("<tr>
			<td><b>{$d['dg_material']}</b></td>
			<td>{$d['dg_unidad_medida']}</td>
			<td>{$d['dc_cantidad']}</td>
			<td align='right'>{$d['dq_precio']}</td>
			<td align='right'>{$d['dq_total']}</td>
		</tr>");
	}
}

if(count($detalles_servicio)){
	echo("<tr><th colspan='5' style='background:#BBB;color:#555;font-size:8px;'>SERVICIOS</td></tr>");
	foreach($detalles_servicio as $d){
	$d['dq_precio'] = moneda_local($d['dq_precio']);
	$d['dq_total'] = moneda_local($d['dq_total']);
		echo("<tr>
			<td colspan='2'>{$d['dg_servicio']}</td>
			<td>{$d['dc_cantidad']}</td>
			<td align='right'>{$d['dq_precio']}</td>
			<td align='right'>{$d['dq_total']}</td>
		</tr>");
	}
}

$costeo['dq_total'] = moneda_local($costeo['dq_total']);
					echo("</tbody>
					<tfoot>
						<tr>
							<th colspan='4' align='right'>Total</th>
							<th align='right'>{$costeo['dq_total']}</th>
						</tr>
					</tfoot>
				</table></div><br /><hr /><br />");
	}
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/crear_costeo.php','cr_costeo');
echo("<br />");
$form->Section();
$form->Text("Nombre",'cost_name',1);
$form->EndSection();
$form->Section();
$form->Text("Días de fabricación de producto",'cost_fabricacion',1);
$form->EndSection();
$form->Section();
echo("<br /><button type='button' class='addbtn' id='import_detail'>Ocupar costeo existente</button>");
$form->EndSection();
echo("<br class='clear' />");
echo("<table class='tab' width='940'>
<thead>
	<tr>
		<th width='290'>Material/Servicio</th>
		<th width='290'>Unidad de medida/Funcionario</th>
		<th width='120'>Cantidad</th>
		<th width='120'>Precio</th>
		<th width='120'>SubTotal</th>
	</tr>
</thead>
<tbody id='costeo_detalle'>
	
</tbody>
</table>");
$form->Button("Agregar material","onclick='add_cost_material();'",'addbtn');
$form->Button("Agregar servicio","onclick='add_cost_service();'",'addbtn');
echo('<hr />');
$form->Hidden('cost_proyecto',$id_proy);
$form->Hidden('cost_total',0);
$form->Hidden('proy_num',$_POST['proy_num']);
$form->End('Emitir costeo','addbtn');

echo('</div></div>');

echo("<table class='hidden' id='template_detail'><tr class='templ_material'><td class='material_list'>");
$form->Listado('','cost_material[]','tb_produccion_material',array('dc_material','dg_material'),1);
echo("</td><td></td><td>");
$form->Text('','cost_cantidad[]',1,11,1,'inputtext',10);
echo("</td><td>");
$form->Text('','cost_precio[]',1,22,1,'inputtext',10);
echo("</td><td align='right' class='cost_price'>0</td></tr><tr class='templ_service'><td>");
$services = $db->select("(SELECT * FROM tb_produccion_servicio WHERE dc_empresa = {$empresa}) s
LEFT JOIN (SELECT * FROM tb_produccion_categoria WHERE dc_empresa={$empresa}) c ON c.dc_categoria = s.dc_categoria",
'c.dc_categoria,c.dg_categoria,s.dc_servicio,s.dg_servicio');
$grouped = array();
foreach($services as $s){
	$grouped[$s['dc_categoria']][] = $s;
}

echo("<select name='cost_service[]' class='inputtext' required='required' ><option></option>");
foreach($grouped as $cat){
	echo("<optgroup label='{$cat[0]['dg_categoria']}'>");
	foreach($cat as $svc){
		echo("<option value='{$svc['dc_servicio']}'>{$svc['dg_servicio']}</option>");
	}
	echo('</optgroup>');
}
echo("</select></td><td>");
//$form->Listado('','cost_funcionario[]','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
echo("</td><td>");
$form->Text('','cost_cantidad2[]',1,11,1,'inputtext',10);
echo("</td><td>");
$form->Text('','cost_precio2[]',1,22,1,'inputtext',10);
echo("</td><td align='right'>0</td></tr></table>");

$unidades_medida = array();
$mats = $db->select("(SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa}) m
LEFT JOIN tb_unidad_medida u ON u.dc_unidad_medida = m.dc_unidad_medida",
"m.dc_material,u.dg_unidad_medida");
foreach($mats as $u){
	$unidades_medida[$u['dc_material']] = $u['dg_unidad_medida'];
}

?>
<script type="text/javascript">
unidades_medida = <?=json_encode($unidades_medida) ?>;

add_cost_material = function(){
	$("#template_detail tr.templ_material").clone(true).appendTo('#costeo_detalle');
}
add_cost_service = function(){
	$("#template_detail tr.templ_service").clone(true).appendTo('#costeo_detalle');
}
$('input[name=cost_cantidad[]],input[name=cost_cantidad2[]]').change(function(){
	price = $(this).parent().next().find('input').val();
	cant = $(this).val();
	$(this).parent().next().next().text(mil_format(cant*price));
});
$('input[name=cost_precio[]],input[name=cost_precio2[]]').change(function(){
	cant = $(this).parent().prev().find('input').val();
	price = $(this).val();
	$(this).parent().next().text(mil_format(cant*price));
});
$("#import_detail").click(function(){
	alert('sadas');
});
$('.material_list select').change(function(){
	v = $(this).val();
	if(v != 0)
	$(this).parent().next().text(unidades_medida[$(this).val()]);
	else
	$(this).parent().next().text('');
});
</script>