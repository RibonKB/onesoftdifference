<?php
define("MAIN",1);
require_once("../../inc/global.php");
$db->query("SET NAMES 'latin1'");

include("template_costeo/modo{$empresa}.php");

		
	$cabecera = $db->select("(SELECT * FROM tb_produccion_costeo WHERE dc_costeo={$_GET['id']}) c
	LEFT JOIN tb_usuario u ON u.dc_usuario = c.dc_usuario_creacion
	LEFT JOIN tb_funcionario f ON f.dc_funcionario = u.dc_funcionario",
	'dc_correlativo,DATE_FORMAT(df_emision,"%d/%m/%Y") as df_emision,CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS dg_responsable');
	
	if(!count($cabecera)){
		$error_man->showWarning("No se ha encontrado el costeo especificado.");
		exit();
	}
	$cabecera = $cabecera[0];
	
	$datosCosteo = $db->select('tb_produccion_producto',
	'dc_producto,dg_producto,DATE_FORMAT(df_creacion,"%d/%m/%Y") as df_creacion,dc_dias_fabricacion,dq_precio',
	"dc_costeo = {$_GET['id']}");
	
	if(!count($datosCosteo)){
		$error_man->showWarning("El detalle del costeo está vacio");
		exit();
	}
	
	$detalle = array();
	foreach($datosCosteo as $p){
		$detalle[] = array($p,
		$db->select("(SELECT * FROM tb_produccion_producto_detalle WHERE dc_producto = {$p['dc_producto']}) p
			JOIN (SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa}) m ON p.dc_material = m.dc_material
			JOIN (SELECT * FROM tb_unidad_medida WHERE dc_empresa={$empresa}) u ON u.dc_unidad_medida = m.dc_unidad_medida",
			'p.dc_cantidad,p.dq_precio,p.dq_total,m.dg_material,u.dg_unidad_medida'),
		$db->select("(SELECT * FROM tb_produccion_producto_detalle_servicio WHERE dc_producto = {$p['dc_producto']}) p
			JOIN (SELECT * FROM tb_produccion_servicio WHERE dc_empresa = {$empresa}) s ON p.dc_servicio = s.dc_servicio",
			'p.dc_cantidad,p.dq_precio,p.dq_total,s.dg_servicio'));
	}
	
	$pdf = new PDF();
	$pdf->AddDetalle($detalle);
	$pdf->Output();	
?>
