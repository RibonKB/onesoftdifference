<?php
require("../../inc/fpdf.php");

class PDF extends FPDF{
private $datosEmpresa;
private $blank = false;
private $docWidth = 830;

public function  PDF(){
	global $empresa,$db;
	
	$datosE = $db->select("(SELECT * FROM tb_empresa WHERE dc_empresa={$empresa}) e
	JOIN tb_empresa_configuracion ec ON e.dc_empresa=ec.dc_empresa
	LEFT JOIN tb_comuna c ON c.dc_comuna = e.dc_comuna
	LEFT JOIN tb_region r ON r.dc_region = c.dc_region",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','pt',array($this->docWidth,640));
	
}

function Header(){
	global $datosCosteo,$cabecera;

	$this->SetFont('Arial','',12);
	$this->SetDrawColor(110,110,110);
	$this->SetTextColor(110,110,110);
	$this->MultiCell(0,20,"COSTEO {$cabecera['dc_correlativo']}\nFecha emisi�n: {$cabecera['df_emision']}\nResponsable: {$cabecera['dg_responsable']}",1);
	$this->SetTextColor(0);
}

function AddDetalle($detalle){
	global $datosCotizacion;
	
	$this->AddPage();

	$this->SetFont('Helvetica','I',11);
	$this->SetTextColor(0);
	$this->SetDrawColor(90,90,90);
	
	foreach($detalle as $d){
		$this->Ln(30);
		$this->SetFillColor(220,220,220);
		$this->Cell(0,20,$d[0]['dg_producto'],1,1,'C',1);
		$this->SetFillColor(245,245,245);
		$this->Cell(0,20,"D�as fabricaci�n: ".$d[0]['dc_dias_fabricacion'],1,1,'L',1);
		$w = array(293.5,110,110,130,130);
		$p = array('L','C','C','R','R');
		$head = array("Material/Servicio","U. de Medida","Cantidad","Precio","Subtotal");
		for($i=0;$i<5;$i++)
			$this->Cell($w[$i],18,$head[$i],1,0,$p[$i]);
		$this->Ln();
			
		if(count($d[1])){
			$this->SetFillColor(220,220,220);
			$this->Cell(0,11,"Materiales",1,1,'C',1);
			foreach($d[1] as $v){
				$v['dq_precio'] = moneda_local($v['dq_precio']);
				$v['dq_total'] = moneda_local($v['dq_total']);
				$this->Cell($w[0],18,$v['dg_material'],'LR',0,$p[0]);
				$this->Cell($w[1],18,$v['dg_unidad_medida'],'LR',0,$p[1]);
				$this->Cell($w[2],18,$v['dc_cantidad'],'LR',0,$p[2]);
				$this->Cell($w[3],18,$v['dq_precio'],'LR',0,$p[3]);
				$this->Cell($w[4],18,$v['dq_total'],'LR',0,$p[4]);
				$this->Ln();
			}
		}
		
		if(count($d[2])){
			$this->SetFillColor(220,220,220);
			$this->Cell(0,11,"Servicios",1,1,'C',1);
			foreach($d[2] as $v){
				$v['dq_precio'] = moneda_local($v['dq_precio']);
				$v['dq_total'] = moneda_local($v['dq_total']);
				$this->Cell($w[0],18,$v['dg_servicio'],'LR',0,$p[0]);
				$this->Cell($w[1],18,'HH','LR',0,$p[1]);
				$this->Cell($w[2],18,$v['dc_cantidad'],'LR',0,$p[2]);
				$this->Cell($w[3],18,$v['dq_precio'],'LR',0,$p[3]);
				$this->Cell($w[4],18,$v['dq_total'],'LR',0,$p[4]);
				$this->Ln();
			}
		}
		
		/*<tfoot><tr>
				<th align='right' colspan='4'>Total</th>
				<th align='right'>{$p['dq_precio']}</th>
			</tr></tfoot>*/
			
		$this->SetFillColor(245,245,245);
		$this->Cell($w[0]+$w[1]+$w[2]+$w[3],18,'Total',1,0,'R',1);
		$v['dq_precio'] = moneda_local($d[0]['dq_precio']);
		$this->Cell($w[4],18,$v['dq_precio'],1,0,'R',1);
		
		$this->Ln(30);
		
	}
	
	
	/*foreach($detalle as $i => $det){
		$y1 = $this->GetY();
			$this->SetFont('','B');
		if(isset($det['dg_producto'])){
			$this->MultiCell($w[0],14,$det['dg_producto']);
			$this->SetFont('','');
		}
		if($det['dg_descripcion'])
			$this->MultiCell($w[0],14,$det['dg_descripcion']);
			
		$this->SetFont('','');
		
		if(isset($datosCotizacion['sd_material'][$det['dc_costeo']])){
			foreach($datosCotizacion['sd_material'][$det['dc_costeo']] as $sd){
				$this->SetX(200);
				$this->Cell($w[0],12,"{$sd['dc_cantidad']}{$sd['dg_unidad_medida']} {$sd['dg_material']}");
				$this->Ln();
			}
		}
		
		if(isset($datosCotizacion['sd_servicio'][$det['dc_costeo']])){
			foreach($datosCotizacion['sd_servicio'][$det['dc_costeo']] as $sd){
				$this->SetX(200);
				$this->Cell($w[0],12,"{$sd['dc_cantidad']}  {$sd['dg_servicio']}");
				$this->Ln();
			}
		}
		
		$y2 = $this->GetY();
		$this->SetY($y1);
		$this->SetX(180+$w[0]);
		$this->Cell($w[1],14,moneda_local($det['dc_cantidad']),0,2,'C');
		$this->SetY($y1);
		$this->SetX(180+$w[0]+$w[1]);
		$this->Cell($w[2],14,"$ ".moneda_local($det['dq_precio_venta']),0,2,'R');
		$this->SetY($y1);
		$this->SetX(180+$w[0]+$w[1]+$w[2]);
		$this->Cell($w[3],14,"$ ".moneda_local($det['dq_total']),0,2,'R');
		$this->SetY($y2);
		$this->Ln(20);
	}
	
	$this->Ln(20);
	$y = $this->GetY();
	$this->MultiCell($w[0]+$w[1]+$w[2],20,"Total Neto\nI.V.A.\nTotal",0,'R');
	$this->SetY($y);
	$this->SetX(180+$w[0]+$w[1]+$w[2]);
	$this->SetFont('','B');
	$this->MultiCell($w[2],20,
		"$ ".moneda_local($datosCotizacion['dq_total'])
		."\n$ ".moneda_local($datosCotizacion['dq_iva'])
		."\n$ ".moneda_local($datosCotizacion['dq_total']+$datosCotizacion['dq_iva']),0,'R');*/
}

function setBlank(){
	$this->blank = true;
}

function getWidth(){
	return $this->docWidth;
}

}
?>