<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Cotizaciones</div>
<div id='main_cont'><br /><br /><div class='panes'>");

include_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/produccion/proc/src_cotizacion.php','src_cotizacion');
$form->Header("<strong>Indicar los parámetros de búsqueda de cotizaciones</strong>");

	echo('<table class="tab" style="text-align:left;" width="100%" id="form_container"><tr><td>Número de cotización</td><td>');
	$form->Text("Desde","pr_numero_desde");
	echo('</td><td>');
	$form->Text('Hasta','pr_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','pr_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','pr_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','pr_client','tb_cliente',array('dc_cliente','dg_razon'));
	echo('</td><td>&nbsp;</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');


?>
</div></div>
<script type="text/javascript">
$('#pr_client').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>