<?php
define("MAIN",1);
require_once("../../inc/global.php");
$db->query("SET NAMES 'latin1'");

include("../ventas/template_cotizacion/modo{$empresa}.php");

	$datosCotizacion = $db->select(
	"(SELECT * FROM tb_produccion_cotizacion WHERE dc_cotizacion = {$_GET['id']} AND dc_empresa={$empresa}) c
	LEFT JOIN (SELECT * FROM tb_produccion_proyecto WHERE dc_empresa = {$empresa}) pr ON pr.dc_proyecto = c.dc_proyecto
	LEFT JOIN (SELECT * FROM tb_termino_comercial WHERE dc_empresa = {$empresa}) ter ON c.dc_termino_comercial = ter.dc_termino_comercial
	JOIN (SELECT * FROM tb_usuario WHERE dc_empresa = {$empresa}) u ON c.dc_usuario_creacion = u.dc_usuario
	JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa = {$empresa}) f ON u.dc_funcionario = f.dc_funcionario
	LEFT JOIN (SELECT * FROM tb_modo_envio WHERE dc_empresa={$empresa}) me ON c.dc_modo_envio = me.dc_modo_envio",
	"c.dc_cliente, c.dc_contacto, c.dq_cotizacion, DATE_FORMAT(c.df_fecha_emision,'%d/%m/%Y') as df_fecha_emision, ter.dg_termino_comercial,
	CONCAT_WS(' ',f.dg_nombres,f.dg_ap_paterno) as dg_ejecutivo, DATE_FORMAT(c.df_envio,'%d/%m/%Y') as df_fecha_envio, me.dg_modo_envio,
	c.dg_observacion, c.dq_neto, c.dq_iva, pr.dg_proyecto, pr.dq_proyecto, c.dc_costeo");
	
	if(!count($datosCotizacion)){
		$error_man->showWarning("La cotizacion especificada no existe");
		exit();
	}
	$datosCotizacion = $datosCotizacion[0];
	
	$datosCotizacion['dq_total'] =& $datosCotizacion['dq_neto'];
	$datosCotizacion['dq_proyecto'] = str_pad($datosCotizacion['dq_proyecto'],4,'0',STR_PAD_LEFT);
	
	/*$datosCotizacion['detalle'] = $db->select("tb_produccion_producto","dg_producto,dq_precio_venta,dg_descripcion,dc_cantidad,dc_cantidad*dq_precio_venta AS dq_total",
	"dc_costeo={$datosCotizacion['dc_costeo']}");
	
	$datosCotizacion['detalle_extra'] = $db->select("tb_produccion_cotizacion_detalle","dg_descripcion,dc_cantidad,dq_precio_venta,dc_cantidad*dq_precio_venta AS dq_total",
	"dc_cotizacion={$_GET['id']}");*/
	
	$datosCotizacion['detalle'] = $db->select("(SELECT * FROM tb_produccion_cotizacion_detalle WHERE dc_cotizacion={$_GET['id']}) d
	LEFT JOIN tb_produccion_producto p ON p.dc_producto = d.dc_producto",
	"IF(d.dc_producto,p.dg_producto,d.dg_producto) AS dg_producto,d.dq_precio_venta,d.dg_descripcion,d.dc_cantidad,d.dc_cantidad*d.dq_precio_venta AS dq_total");
	
	$datosCliente = $db->select(
	"(SELECT * FROM tb_cliente WHERE dc_cliente = {$datosCotizacion['dc_cliente']}) cl
	JOIN tb_domicilio_cliente d ON cl.dc_cliente = d.dc_cliente
	LEFT JOIN tb_comuna co ON d.dc_comuna = co.dc_comuna
	JOIN tb_domicilio_cliente_tipo_direccion td ON d.dc_domicilio = td.dc_domicilio
	JOIN (SELECT * FROM tb_contacto_cliente WHERE dc_contacto = {$datosCotizacion['dc_contacto']}) c ON td.dc_contacto = c.dc_contacto",
	"cl.dg_razon, d.dg_direccion, cl.dg_giro, co.dg_comuna, c.dg_contacto");
	
	$datosCliente = $datosCliente[0];
	
	$pdf = new PDF();
	$pdf->AddPage();
	$pdf->AddDetalle($datosCotizacion['detalle']);
	
	if(isset($_GET['img'])){
		$pdf->setBlank();
		
		$files=opendir('files');
		while($file = readdir($files)){
			if(preg_match("/^{$datosCotizacion['dq_proyecto']}-{$empresa}.+(jpg)|(jpeg)|(gif)$/",$file)){
				//Image(string file [, float x [, float y [, float w [, float h [, string type [, mixed link]]]]]])
				$filename = 'files/'.$file;
				$pdf->AddPage();
				$pdf->Image($filename);
			}
		}

	}
	$pdf->Output();	
?>
