<?php

class CrearProductoFactory extends Factory {

    protected $title = 'Productos';

    /*
     * Muestra el formulario de cracion de producto.
     */

    public function indexAction() {

        $mostrar = $this->getFormView($this->getTemplateURL('index.form'));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    /*
     * Se procesa la informacion que envia el formulario 
     */

    public function procesarFormAction() {


        $this->ComprobarTamañoImagen();
        $this->validarImagen();
        $this->ValidarCodigo();
        $this->ValidarValores();
        $this->insertarDatos();
        $this->SubirImagen();

        $this->getErrorMan()->showConfirm("Se ha creado el producto correctamente.");
    }

    /*
     * Se valida que la imagen cumpla con los formatos permitidos.
     * de no ser hasi se le enviara un mensaje al usuario.
     */

    private function validarImagen() {
        $r = self::getRequest();
        $tipos = array('image/gif', 'image/pjpeg', 'image/jpeg', 'image/jpg', 'image/x-png', 'image/png');

        if ($_FILES['prod_foto']['size']) {
            if (!in_array($_FILES['prod_foto']['type'], $tipos)) {
                $this->getErrorMan()->showWarning("El archivo especificado no es una imagen válida.<br />Los formatos soportados son gif, jpeg y png");
                exit();
            }
            $r->p_ext = substr($_FILES['prod_foto']['name'], strrpos($_FILES['prod_foto']['name'], "."));
        }
    }

    /*
     * Verifica que el codigo ingresado no exista en la Base de Datos.
     */

    private function ValidarCodigo() {
        $r = self::getRequest();
        $con = $this->getConnection();

        $sel = $con->prepare($con->select("tb_producto", "1", "dg_codigo = '{$r->prod_codigo}' AND dc_empresa = {$this->getEmpresa()} AND dm_activo='1'"));
        $con->stExec($sel);

        if ($sel->fetch() != false) {
            $this->getErrorMan()->showWarning("Ya hay un producto utilizando el código indicado, pruebe otro o consulte a un administrador");
            exit();
        }
    }

    /*
     * Verifica que los valores ingresados son numeros validos
     */

    private function ValidarValores() {
        $r = self::getRequest();

        $r->prod_precio_venta = str_replace(',', "", $r->prod_precio_venta);
        $r->prod_precio_compra = str_replace(',', "", $r->prod_precio_compra);
        if ((!is_numeric($r->prod_precio_venta)) || (!is_numeric($r->prod_precio_compra))) {
            $this->getErrorMan()->showWarning("El precio no es un número válido");
            exit();
        }
        $r->prod_tipo_cambio = explode('|', $r->prod_tipo_cambio);
        $r->cambio = $r->prod_tipo_cambio[1];
        $r->prod_tipo_cambio = $r->prod_tipo_cambio[0];
    }

    /*
     * Se insertan los datos entregados por el usuario en el formulario.
     */

    private function insertarDatos() {
        $r = self::getRequest();

        $db = $this->getConnection();
        $user_creacion=$this->getUserData()->dc_usuario;
        $fecha=date("Y-m-d H:i:s");
        $insertar = $db->prepare($db->insert("tb_producto", array(
                    "dg_producto" => '?',
                    "dg_codigo" => '?',
                    "dq_precio_venta" => '?',
                    "dq_precio_compra" => '?',
                    "dc_tipo_cambio" => '?',
                    "dc_marca" => '?',
                    "dc_tipo_producto" => '?',
                    "dc_linea_negocio" => '?',
                    "dc_empresa" => $this->getEmpresa(),
                    "dc_cebe" => '?',
                    "dg_unidad_medida" => '?',
                    "dm_requiere_serie" => '?',
                    "dc_usuario_creacion"=>'?',
                    "df_creacion"=> '?'
        )));

        $insertar->bindvalue(1, $r->prod_nombre, PDO::PARAM_STR);
        $insertar->bindvalue(2, $r->prod_codigo, PDO::PARAM_INT);
        $insertar->bindvalue(3, $r->prod_precio_venta * $r->cambio, PDO::PARAM_INT);
        $insertar->bindvalue(4, $r->prod_precio_compra * $r->cambio, PDO::PARAM_INT);
        $insertar->bindValue(5, $r->cambio, PDO::PARAM_INT);
        $insertar->bindvalue(6, $r->prod_marca, PDO::PARAM_INT);
        $insertar->bindvalue(7, $r->prod_tipo, PDO::PARAM_INT);
        $insertar->bindvalue(8, $r->prod_lnegocio, PDO::PARAM_INT);
        $insertar->bindvalue(9, $r->prod_cebe, PDO::PARAM_INT); //????
        $insertar->bindvalue(10, $r->prod_unidad, PDO::PARAM_INT);
        $insertar->bindvalue(11, isset($r->prod_serie) ? 1 : 0, PDO::PARAM_BOOL);
        $insertar->bindvalue(12, $user_creacion, PDO::PARAM_INT);
        $insertar->bindvalue(13, $fecha, PDO::PARAM_STR);

        $db->stExec($insertar);

        $r->p_id = $db->lastInsertId();
    }

    /*
     * Se sube la imagen al servidor
     */

    private function SubirImagen() {
        $r = self::getRequest();
        if ($_FILES['prod_foto']['size']) {
            if (!move_uploaded_file($_FILES['prod_foto']['tmp_name'], "../fotos/prod_{$r->p_id}{$r->p_ext}")) {
                $this->getErrorMan()->showWarning("No se pudo subir la foto del producto, intentelo más tarde editandolo");
            }
        }
    }

    /*
     * Se comprueba que el tamaño de la imagen no exceda el tamaño permitido
     */

    private function ComprobarTamañoImagen() {
        switch ($_FILES['prod_foto']['error']) {
            case 1:
            case 2: $this->getErrorMan()->showWarning("El tamaño del archivo supera el máximo permitido.");
                exit();
                break;
            case 3: $this->getErrorMan()->showWarning("Se ha cancelado la transferencia del archivo.");
                exit();
                break;
        }
    }

}

?>
