<?php

class EditarProductoFactory extends Factory {

    protected $title = 'Edicion Producto';

    /*
     * Se muestra el formulario para modificar informacion de producto
     */

    public function indexAction() {
        $mostrar = $this->getFormView($this->getTemplateURL('edit.form'));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    /* public function index2Acction() {
      $r=self::getRequest();

      $mostrar = $this->getFormView($this->getTemplateURL('editar.form'),array('producto'=>$r->datos));
      //$this->BuscaProducto();
      echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
      } */

    /*
     * Se procesan los datos a modificar.
     */

    public function procesarFormAction() {



        $this->ComprobarTamañoImagen();
        $this->validarImagen();
        $this->ValidarValores();
        $this->modificarDatos();
        $this->SubirImagen();

        $this->getErrorMan()->showConfirm("Se ha editado el producto correctamente.");
    }

    /*
     * Se valida que la imagen cumpla con los formatos permitidos.
     */

    private function validarImagen() {
        $r = self::getRequest();
        $tipos = array('image/gif', 'image/pjpeg', 'image/jpeg', 'image/jpg', 'image/x-png', 'image/png');

        if ($_FILES['prod_foto']['size']) {
            if (!in_array($_FILES['prod_foto']['type'], $tipos)) {
                $this->getErrorMan()->showWarning("El archivo especificado no es una imagen válida.<br />Los formatos soportados son gif, jpeg y png");
                exit();
            }
            $r->p_ext = substr($_FILES['prod_foto']['name'], strrpos($_FILES['prod_foto']['name'], "."));
        }
    }

    /*
     * Verifica que el codigo ingresado no exista en la Base de Datos.
     */

    private function ValidarCodigo() {
        $r = self::getRequest();
        $con = $this->getConnection();

        $sel = $con->prepare($con->select("tb_producto", "1", "dg_codigo = '{$r->prod_codigo}' AND dc_empresa = {$this->getEmpresa()} AND dm_activo='1'"));
        $con->stExec($sel);

        if ($sel->fetch() != false) {
            $this->getErrorMan()->showWarning("Ya hay un producto utilizando el código indicado, pruebe otro o consulte a un administrador");
            exit();
        }
    }

    /*
     * Verifica que los valores ingresados son numeros validos
     */

    private function ValidarValores() {
        $r = self::getRequest();

        $r->prod_precio_venta = str_replace(',', "", $r->prod_precio_venta);
        $r->prod_precio_compra = str_replace(',', "", $r->prod_precio_compra);
        if ((!is_numeric($r->prod_precio_venta)) || (!is_numeric($r->prod_precio_compra))) {
            $this->getErrorMan()->showWarning("El precio no es un número válido");
            exit();
        }
        $db = $this->getConnection();
        $r->cambio = $db->prepare($db->select('tb_tipo_cambio', 'dq_cambio', "dc_tipo_cambio={$r->prod_tipo_cambio}"));
        $db->stExec($r->cambio);
        $r->cambio = $r->cambio->fetch(PDO::FETCH_OBJ);
        if ($r->cambio == false) {
            $r->cambio = 1;
        }
    }

    /*
     * Se comprueba que el tamaño de la imagen no exceda el tamaño permitido
     */

    private function ComprobarTamañoImagen() {
        switch ($_FILES['prod_foto']['error']) {
            case 1:
            case 2: $this->getErrorMan()->showWarning("El tamaño del archivo supera el máximo permitido.");
                exit();
                break;
            case 3: $this->getErrorMan()->showWarning("Se ha cancelado la transferencia del archivo.");
                exit();
                break;
        }
    }

    /*
     * Se sube la imagen al servidor.
     */

    private function SubirImagen() {
        $r = self::getRequest();
        if ($_FILES['prod_foto']['size']) {
            if (!move_uploaded_file($_FILES['prod_foto']['tmp_name'], "../fotos/prod_{$r->id_prod}{$r->p_ext}")) {
                $this->getErrorMan()->showWarning("No se pudo subir la foto del producto, intentelo más tarde editandolo");
            }
        }
    }

    /*
     * Se modifican los datos del producto medianta Update.
     */

    private function modificarDatos() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $modificar = $db->prepare($db->update("tb_producto", array(
                    "dc_marca" => '?',
                    "dc_tipo_producto" => '?',
                    "dc_linea_negocio" => '?',
                    "dg_producto" => '?',
                    "dg_codigo" => '?',
                    "dq_precio_venta" => '?',
                    "dq_precio_compra" => '?',
                    "dc_tipo_cambio" => '?'
                        ), "dc_producto = ? "));

        $modificar->bindValue(1, $r->prod_marca, PDO::PARAM_INT);
        $modificar->bindValue(2, $r->prod_tipo, PDO::PARAM_INT);
        $modificar->bindValue(3, $r->prod_lnegocio, PDO::PARAM_INT);
        $modificar->bindValue(4, $r->prod_nombre, PDO::PARAM_STR);
        $modificar->bindValue(5, $r->prod_codigo, PDO::PARAM_INT);
        $modificar->bindValue(6, $r->prod_precio_venta * $r->cambio->dq_cambio, PDO::PARAM_INT);
        $modificar->bindValue(7, $r->prod_precio_compra * $r->cambio->dq_cambio, PDO::PARAM_INT);
        $modificar->bindValue(8, $r->prod_tipo_cambio, PDO::PARAM_INT);
        $modificar->bindValue(9, $r->id_prod, PDO::PARAM_INT);

        $db->stExec($modificar);
    }

    public function BuscaProductoAction() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $r->datos = $db->prepare($db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$this->getEmpresa()} AND dm_activo='1' AND dg_codigo = '{$r->prod_codigo}') p
LEFT JOIN tb_tipo_producto t ON p.dc_tipo_producto = t.dc_tipo_producto
LEFT JOIN tb_marca m ON p.dc_marca = m.dc_marca
LEFT JOIN tb_linea_negocio l ON p.dc_linea_negocio = l.dc_linea_negocio
LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = p.dc_tipo_cambio", "p.dc_producto,p.dg_producto,p.dg_codigo,p.dq_precio_venta,p.dq_precio_compra,t.dg_tipo_producto,m.dg_marca,l.dg_linea_negocio,
	p.dc_tipo_producto,p.dc_marca,p.dc_linea_negocio,p.dg_unidad_medida,p.dc_cebe,p.dm_requiere_serie,p.dc_tipo_cambio,tc.dq_cambio"));
        $db->stExec($r->datos);
        $r->datos = $r->datos->fetch(PDO::FETCH_OBJ);
        if ($r->datos == false) {
            $this->getErrorMan()->showErrorRedirect("No se encontró el producto especificado", Factory::buildActionUrl('index'));
        } else {

            $this->MostrarImagen();
            $mostrar = $this->getFormView($this->getTemplateURL('editar.form'), array('producto' => $r->datos));
            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        }
    }

    private function MostrarImagen() {
        $r = self::getRequest();
        $extensions = array("gif", "png", "jpg", "jpeg");

        $r->prod_foto = "sites/productos/fotos/prod_default.png";
        foreach ($extensions as $ext) {
            if (file_exists("../fotos/prod_{$r->datos->dc_producto}.{$ext}")) {
                $r->prod_foto = "sites/productos/fotos/prod_{$r->datos->dc_producto}.{$ext}";
            }
        }
    }

}

?>
