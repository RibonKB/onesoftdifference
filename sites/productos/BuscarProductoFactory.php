<?php

class BuscarProductoFactory extends Factory {

    protected $title = 'buscar Producto';

    /*
     * Nos muestra el formulario principal 
     * donde ingresamos el producto a buscar
     */

    public function indexAction() {
        $mostrar = $this->getFormView($this->getTemplateURL('index.encuentra'));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    /*
     * Busca los datos necesarios para llenar los campos de la vista y a la vez tambien nos muestra la vista
     */

    public function BuscarAction() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $empresa = $this->getParametrosEmpresa();
        $r->datos = $db->prepare($db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$this->getEmpresa()} ANd dm_activo='1' AND dg_codigo = '{$r->prod_codigo}') p
LEFT JOIN (SELECT * FROM tb_tipo_producto WHERE dc_empresa={$this->getEmpresa()} AND dm_activo = '1') t ON p.dc_tipo_producto = t.dc_tipo_producto
LEFT JOIN (SELECT * FROM tb_marca WHERE dc_empresa={$this->getEmpresa()} AND dm_activo='1') m ON p.dc_marca = m.dc_marca
LEFT JOIN (SELECT * FROM tb_linea_negocio WHERE dc_empresa={$this->getEmpresa()} AND dm_activo='1') l ON p.dc_linea_negocio = l.dc_linea_negocio", "p.dc_producto,p.dg_producto,p.dg_codigo,p.dq_precio_venta,p.dq_precio_compra,t.dg_tipo_producto,m.dg_marca,l.dg_linea_negocio"));

        $db->stExec($r->datos);
        $r->datos = $r->datos->fetch(PDO::FETCH_OBJ);
        if ($r->datos == false) {
            $this->getErrorMan()->showErrorRedirect("No se encontró el producto especificado", Factory::buildActionUrl('index'));
        } else {
            $this->mostrarImagen();
            $mostrar = $this->getFormView($this->getTemplateURL('muestra'), array('datos' => $r->datos, 'foto' => $r->prod_foto, 'empresa_conf' => $empresa));
            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        }
    }

    /*
     * Nos muestra la imagen del producto, si este no tien imagen, la imagen que se verá sera prod_defaul.png
     */

    private function mostrarImagen() {
        $r = self::getRequest();
        $extensions = array("gif", "png", "jpg", "jpeg");

        $r->prod_foto = "sites/productos/fotos/prod_default.png";
        foreach ($extensions as $ext) {
            if (file_exists("../fotos/prod_{$r->datos->dc_producto}.{$ext}")) {
                $r->prod_foto = "sites/productos/fotos/prod_{$r->datos->dc_producto}.{$ext}";
            }
        }
    }

}

?>
