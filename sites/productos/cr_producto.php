<?php
/**
*	Mantenedor de usuarios que utilizarán el sistema
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Creación de productos</div>

<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Complete los datos para la creación del producto</strong><br />(Los campos marcados con [*] son obligatorios)");
	$form->Start("sites/productos/proc/crear_producto.php","prod_crear","form","method='post' enctype='multipart/form-data'");
	$form->Section();
	$form->Text("Nombre","prod_nombre",1);
	$form->Listado("Linea de negocio","prod_lnegocio","tb_linea_negocio",array("dc_linea_negocio","dg_linea_negocio"),1);
	$form->Listado("Tipo de producto","prod_tipo","tb_tipo_producto",array("dc_tipo_producto","dg_tipo_producto"),1);
	$form->Text("Unidad de medida","prod_unidad",1,15,"un.");
	$form->File("Imagen del producto","prod_foto");
	$form->Combobox('','prod_serie',array('Requiere serie'));
	$form->EndSection();
	$form->Section();
	$form->Text("Código","prod_codigo",1);
	$form->Text("Precio venta [$] (Referencia)","prod_precio_venta",1,10);
	$form->Text("Precio compra [$] (Referencia)","prod_precio_compra",1,10);
	$form->Listado("Tipo de cambio",'prod_tipo_cambio','tb_tipo_cambio',array('CONCAT_WS("|",dc_tipo_cambio,dq_cambio)','dg_tipo_cambio'),1);
	$form->Listado("Marca","prod_marca","tb_marca",array("dc_marca","dg_marca"),1);
	$form->Listado("Centro beneficio","prod_cebe","tb_cebe",array("dc_cebe","dg_cebe"),1);
	$form->EndSection();
	$form->End("Crear","addbtn");
	
?>
<iframe name="addproducto_fres" id="addproducto_fres" style="display:none;"></iframe>
</div>

</div>

<script type="text/javascript">
$("#prod_crear").attr("target","addproducto_fres");
$("#prod_crear").submit(function(e){
	if(validarForm("#prod_crear")){	
		disableForm("#prod_crear");
	}else{
		e.preventDefault();
	}
});
$("#addproducto_fres").load(function(){
	res = frames['addproducto_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#prod_crear_res").html(res);
	hide_loader();
});
$('#prod_precio_venta,#prod_precio_compra').change(function(){
if(parseFloat($(this).val()))
	$(this).val(mil_format(toNumber($(this).val())));
else
	$(this).val('');
});
</script>