<?php

/*
 * 
 * Clase para eliminar producto
 * 
 */

class EliminarProductoFactory extends Factory {

    protected $title = 'Eliminar Producto';

    /*
     * Muestra el formulario principal para ingresar el producto a eliminar
     */

    public function indexAction() {

        $mostrar = $this->getFormView($this->getTemplateURL('index.form'));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    /*
     * Se consultanm los datops para luego llenar la vista con ellos, y muestra la vista.
     */

    public function ProcesarAction() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $r->datos = $db->prepare($db->select("tb_producto p,tb_tipo_producto t,tb_marca m,tb_linea_negocio l", "p.dc_producto,p.dg_producto,p.dg_codigo,p.dq_precio_compra,p.dq_precio_venta,t.dg_tipo_producto,m.dg_marca,l.dg_linea_negocio", "p.dc_tipo_producto = t.dc_tipo_producto AND
	p.dc_marca = m.dc_marca AND
	p.dc_linea_negocio = l.dc_linea_negocio AND
	p.dm_activo = '1' AND
	p.dc_empresa = {$this->getEmpresa()} AND
	p.dg_codigo = '{$r->prod_codigo}'
	"
        ));
        $db->stExec($r->datos);
        $r->datos = $r->datos->fetch(PDO::FETCH_OBJ);
        if ($r->datos == false) {
            $this->getErrorMan()->howErrorRedirect("No se encontró el producto especificado", Factory::buildActionUrl('index'));
        } else {
            $this->mostrarImagen();
            $mostrar = $this->getFormView($this->getTemplateURL('eliminar'), array('datos' => $r->datos, 'foto' => $r->prod_foto));
            echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
        }
    }

    /*
     * Una ves el cliente decide eliminar el producto, esta funcion la busca en la base de datos y la desactiva
     * poniendo el atributo dm_activo = 0
     */

    public function eliminarAction() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $r->eliminar = $db->prepare($db->select("tb_producto", "1", "dc_producto={$r->id_prod} AND dm_activo = '1'"));
        $db->stExec($r->eliminar);
        $r->eliminar = $r->eliminar->fetch(PDO::FETCH_OBJ);

        if ($r->eliminar == false) {
            $this->getErrorMan()->showAviso("El producto ya ha sido eliminado");
        } else {
            $r->actualiza = $db->prepare($db->update("tb_producto", array("dm_activo" => '0'), "dc_producto = {$r->id_prod}"));
            $db->stExec($r->actualiza);
            $this->getErrorMan()->showAviso("El producto ha sido eliminado correctamente");
        }
    }

    /*
     * Se encarga de mostrarnos la imagen del producto
     */

    private function mostrarImagen() {

        $r = self::getRequest();

        $extensions = array("gif", "png", "jpg", "jpeg");

        $r->prod_foto = "sites/productos/fotos/prod_default.png";
        foreach ($extensions as $ext) {
            if (file_exists("../fotos/prod_{$r->datos->dc_producto}.{$ext}")) {
                $r->prod_foto = "sites/productos/fotos/prod_{$r->datos->dc_producto}.{$ext}";
            }
        }
    }

}

?>
