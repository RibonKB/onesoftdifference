<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de productos
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/productos/src_producto.php" class="loader">Consulta</a></li>
		<li><a href="sites/productos/cr_producto.php" class="loader">Creación</a></li>
		<li class="oo_active"><a href="#">Edición</a></li>
		<li><a href="sites/productos/del_producto.php" class="loader">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Ingrese el código del producto que quiere editar.</strong><br />(Los criterios de búsqueda son codigo,nombre del producto, tipo de producto,marca, línea de negocio)");
	$form->Start("sites/productos/proc/ed_producto.php","busqueda_prod","cValidar");
	$form->text("Código del producto","prod_codigo",1);
	$form->End("Buscar","searchbtn");
?>
</div>
<script type="text/javascript">
format = function(row){
	return row[0]+" ( "+row[1]+" )";
}
$("#prod_codigo").autocomplete('sites/proc/autocompleter/producto.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>