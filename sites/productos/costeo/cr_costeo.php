<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Costeo de productos</div>
<div id='main_cont'><div class='panes center'>");

	include("../../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Header("<strong>Indique el código del producto a costear</strong>
	<br />(Los criterios de búsqueda son el código, nombre, tipo, marca o linea de negocio del producto)");
	$form->Start("sites/productos/costeo/proc/cr_costeo.php","ed_costeo","cValidar");
	$form->Text("Código de producto","prod_codigo",1);
	$form->End("Editar","editbtn");

echo("</div></div>");
?>
<script type="text/javascript">
format = function(row){
	return row[0]+" ( "+row[1]+" )";
}
$("#prod_codigo").autocomplete('sites/proc/autocompleter/producto.php?cond=cost',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>