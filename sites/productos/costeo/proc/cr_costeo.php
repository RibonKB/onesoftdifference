<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_producto','dg_producto',"dg_codigo = '{$_POST['prod_codigo']}'");
if(!count($data)){
	$error_man->showErrorRedirect("No se encontró el producto especificado","sites/productos/costeo/cr_costeo.php");
}
$data = $data[0];

echo("<div id='secc_bar'>Creación costeo de producto</div>
<div id='main_cont'><div class='panes'>");

require_once("../../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start("sites/productos/costeo/proc/crear_costeo.php");
$form->Header("Creación de costeo asignado a {}");
$form->End("Crear",'addbtn');

echo("</div></div>");
?>