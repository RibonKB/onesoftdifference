<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Consulta de productos
</div>
<div id="other_options">
	<ul>
		<li class="oo_active"><a href="#">Consulta</a></li>
		<li><a href="sites/productos/cr_producto.php" class="loader">Creación</a></li>
		<li><a href="sites/productos/ed_producto.php" class="loader">Edición</a></li>
		<li><a href="sites/productos/del_producto.php" class="loader">Eliminación</a></li>
	</ul>
</div>

<?php

$datos = $db->select("tb_producto p,tb_tipo_producto t,tb_marca m,tb_linea_negocio l",
	"p.dc_producto,p.dg_producto,p.dg_codigo,p.dq_precio_compra,p.dq_precio_venta,t.dg_tipo_producto,m.dg_marca,l.dg_linea_negocio",
	"p.dc_tipo_producto = t.dc_tipo_producto AND
	p.dc_marca = m.dc_marca AND
	p.dc_linea_negocio = l.dc_linea_negocio AND
	p.dm_activo = '1' AND
	p.dc_empresa = {$empresa} AND
	p.dg_codigo = '{$_POST['prod_codigo']}'
	"
);

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el producto especificado","sites/productos/src_producto.php");
}
$datos = $datos[0];

$extensions = array("gif","png","jpg","jpeg");

$prod_foto = "sites/productos/fotos/prod_default.png";
foreach($extensions as $ext){
	if(file_exists("../fotos/prod_{$datos['dc_producto']}.{$ext}")){
		$prod_foto = "sites/productos/fotos/prod_{$datos['dc_producto']}.{$ext}";
	}
}

?>

<div id="main_cont">
	<div class="panes">
	<fieldset>
		<div class="left">
			<img src="<?php echo($prod_foto); ?>" alt="" style="width:90px;border:1px solid #CCC;background:#EEE;padding:3px;margin:5px;" />
		</div>
		<div class="left">
			<label>Código: </label><?=$datos['dg_codigo'] ?><br />
			<label>Nombre: </label><?=$datos['dg_producto'] ?><br />
			<label>Precio de venta: </label>$<?=number_format($datos['dq_precio_venta'],0,",",".") ?><br />
			<label>Precio de compra: </label>$<?=number_format($datos['dq_precio_compra'],0,",",".") ?><br />
			<label>Marca: </label><?=$datos['dg_marca'] ?><br />
			<label>Línea de Negocio: </label><?=$datos['dg_linea_negocio'] ?><br />
		</div>
		<hr class="clear" />
		<div class="center">
		¿Está seguro que quiere eliminar el producto seleccionado?<br />
		<form action="sites/productos/proc/delete_producto.php" class="confirmValidar" id="del_prod">
			<input type="hidden" name="id_prod" value="<?php echo($datos['dc_producto']); ?>" />
			<input type="submit" class="delbtn" value="Eliminar" />
		</form>
		<div id="del_prod_res"></div>
		</div>
	</fieldset>
	</div>
</div>