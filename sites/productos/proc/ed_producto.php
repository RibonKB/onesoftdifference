<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">Edición de productos</div>

<?php

$datos = $db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_codigo = '{$_POST['prod_codigo']}') p
LEFT JOIN tb_tipo_producto t ON p.dc_tipo_producto = t.dc_tipo_producto
LEFT JOIN tb_marca m ON p.dc_marca = m.dc_marca
LEFT JOIN tb_linea_negocio l ON p.dc_linea_negocio = l.dc_linea_negocio
LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = p.dc_tipo_cambio",
	"p.dc_producto,p.dg_producto,p.dg_codigo,p.dq_precio_venta,p.dq_precio_compra,t.dg_tipo_producto,m.dg_marca,l.dg_linea_negocio,
	p.dc_tipo_producto,p.dc_marca,p.dc_linea_negocio,p.dg_unidad_medida,p.dc_cebe,p.dm_requiere_serie,p.dc_tipo_cambio,tc.dq_cambio");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el producto especificado","sites/productos/ed_producto.php");
}
$datos = $datos[0];

$extensions = array("gif","png","jpg","jpeg");

$prod_foto = "sites/productos/fotos/prod_default.png";
foreach($extensions as $ext){
	if(file_exists("../fotos/prod_{$datos['dc_producto']}.{$ext}")){
		$prod_foto = "sites/productos/fotos/prod_{$datos['dc_producto']}.{$ext}";
	}
}

?>

<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Complete los datos para la edición del producto</strong><br />(Los campos marcados con [*] son obligatorios)");
	$form->Start("sites/productos/proc/editar_producto.php","prod_edit","form","method='post' enctype='multipart/form-data'");
	$form->Section();
	$form->Text("Nombre","prod_nombre",1,255,$datos['dg_producto']);
	$form->Listado("Linea de negocio","prod_lnegocio","tb_linea_negocio",array("dc_linea_negocio","dg_linea_negocio"),1,$datos['dc_linea_negocio']);
	$form->Listado("Tipo de producto","prod_tipo","tb_tipo_producto",array("dc_tipo_producto","dg_tipo_producto"),1,$datos['dc_tipo_producto']);
	$form->Text("Unidad de medida","prod_unidad",1,15,$datos['dg_unidad_medida']);
	$form->File("Imagen del producto","prod_foto");
	$form->Combobox('','prod_serie',array('Requiere serie'),$datos['dm_requiere_serie']==1?array(0):array());
	$form->EndSection();
	$form->Section();
	$form->Text("Código","prod_codigo",1,255,$datos['dg_codigo']);
	$form->Text("Precio venta [$] (Referencia)","prod_precio_venta",1,10,$datos['dq_precio_venta']/$datos['dq_cambio']);
	$form->Text("Precio compra [$] (Referencia)","prod_precio_compra",1,10,$datos['dq_precio_compra']/$datos['dq_cambio']);
	$form->Listado("Tipo de cambio",'prod_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'),1,$datos['dc_tipo_cambio']);
	$form->Listado("Marca","prod_marca","tb_marca",array("dc_marca","dg_marca"),1,$datos['dc_marca']);
	$form->Listado("Centro beneficio","prod_cebe","tb_cebe",array("dc_cebe","dg_cebe"),1,$datos['dc_cebe']);
	$form->EndSection();
	$form->Hidden("id_prod",$datos['dc_producto']);
	$form->End("Crear","addbtn");
	
?>
<iframe name="edproducto_fres" id="edproducto_fres" style="display:none;"></iframe>
</div>
</div>

<script type="text/javascript">
$("#prod_edit").attr("target","edproducto_fres");
$("#prod_edit").submit(function(e){
	if(validarForm("#prod_edit")){	
		disableForm("#prod_edit");
	}else{
		e.preventDefault();
	}
});
$("#edproducto_fres").load(function(){
	res = frames['edproducto_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#prod_edit_res").html(res);
});
</script>