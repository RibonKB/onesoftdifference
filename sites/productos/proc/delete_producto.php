<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$producto =& $_POST['id_prod'];

if(count($db->select("tb_producto","1","dc_producto={$producto} AND dm_activo = '0'"))){
	$error_man->showAviso("El producto ya ha sido eliminado");
}
else{
	$db->update("tb_producto",array("dm_activo" => '0'),"dc_producto = {$producto}");
	$error_man->showConfirm("El producto ha sido eliminado correctamente");
}
?>