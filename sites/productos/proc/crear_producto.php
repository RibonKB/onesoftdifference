<?php
	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	switch($_FILES['prod_foto']['error']){
		case 1:
		case 2: $error_man->showWarning("El tamaño del archivo supera el máximo permitido.");
				exit();
				break;
		case 3: $error_man->showWarning("Se ha cancelado la transferencia del archivo.");
				exit();
				break;
	}
	
	$tipos = array('image/gif','image/pjpeg','image/jpeg','image/jpg','image/x-png','image/png');
	
	if($_FILES['prod_foto']['size']){
		if(!in_array($_FILES['prod_foto']['type'],$tipos)){
			$error_man->showWarning("El archivo especificado no es una imagen válida.<br />Los formatos soportados son gif, jpeg y png");
			exit();
		}
		$p_ext = substr($_FILES['prod_foto']['name'],strrpos($_FILES['prod_foto']['name'],"."));
	}
	
	$_POST['prod_codigo'] = trim($_POST['prod_codigo']);
	
	if(count($db->select("tb_producto","1","dg_codigo = '{$_POST['prod_codigo']}' AND dc_empresa = {$empresa} AND dm_activo='1'"))){
		$error_man->showWarning("Ya hay un producto utilizando el código indicado, pruebe otro o consulte a un administrador");
		exit();
	}

	$p_precio_venta = str_replace(',',"",$_POST['prod_precio_venta']);
	$p_precio_compra = str_replace(',',"",$_POST['prod_precio_compra']);
	if((!is_numeric($p_precio_venta)) || (!is_numeric($p_precio_compra))){
		$error_man->showWarning("El precio no es un número válido");
		exit();
	}
	
	$tipo_cambio = explode('|',$_POST['prod_tipo_cambio']);
	$cambio = $tipo_cambio[1];
	$tipo_cambio = $tipo_cambio[0];
	
	$p_id = $db->insert("tb_producto",
	array(
		"dg_producto" => $_POST['prod_nombre'],
		"dg_codigo" => $_POST['prod_codigo'],
		"dq_precio_venta" => $p_precio_venta*$cambio,
		"dq_precio_compra" => $p_precio_compra*$cambio,
		"dc_tipo_cambio" => $tipo_cambio,
		"dc_marca" => $_POST['prod_marca'],
		"dc_tipo_producto" => $_POST['prod_tipo'],
		"dc_linea_negocio" => $_POST['prod_lnegocio'],
		"dc_empresa" => $empresa,
		"dc_cebe" => $_POST['prod_cebe'],
		"dg_unidad_medida" => $_POST['prod_unidad'],
		"dm_requiere_serie" => isset($_POST['prod_serie'])?1:0,
		"dc_usuario_creacion" => $idUsuario,
		'df_creacion' => 'NOW()'
	));
	
	if($_FILES['prod_foto']['size']){
		if(!move_uploaded_file($_FILES['prod_foto']['tmp_name'],"../fotos/prod_{$p_id}{$p_ext}"))
		{
			$error_man->showWarning("No se pudo subir la foto del producto, intentelo más tarde editandolo");
		}
	}
	
	$error_man->showConfirm("Se ha creado el producto correctamente.");
	
?>