<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} ANd dm_activo='1' AND dg_codigo = '{$_POST['prod_codigo']}') p
LEFT JOIN (SELECT * FROM tb_tipo_producto WHERE dc_empresa={$empresa} AND dm_activo = '1') t ON p.dc_tipo_producto = t.dc_tipo_producto
LEFT JOIN (SELECT * FROM tb_marca WHERE dc_empresa={$empresa} AND dm_activo='1') m ON p.dc_marca = m.dc_marca
LEFT JOIN (SELECT * FROM tb_linea_negocio WHERE dc_empresa={$empresa} AND dm_activo='1') l ON p.dc_linea_negocio = l.dc_linea_negocio",
	"p.dc_producto,p.dg_producto,p.dg_codigo,p.dq_precio_venta,p.dq_precio_compra,t.dg_tipo_producto,m.dg_marca,l.dg_linea_negocio");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el producto especificado","sites/productos/src_producto.php");
}
$datos = $datos[0];

$extensions = array("gif","png","jpg","jpeg");

$prod_foto = "sites/productos/fotos/prod_default.png";
foreach($extensions as $ext){
	if(file_exists("../fotos/prod_{$datos['dc_producto']}.{$ext}")){
		$prod_foto = "sites/productos/fotos/prod_{$datos['dc_producto']}.{$ext}";
	}
}

?>

<div id="secc_bar">Consulta de productos</div><div id="main_cont">
<div id="options_menu">
	<a href="sites/logistica/proc/src_stock_report.php?st_productos[]=<?=$datos['dc_producto'] ?>" class="loadOnOverlay button">Detalle del Stock</a>
	<a href="sites/logistica/proc/src_producto_cartola.php?id=<?=$datos['dc_producto'] ?>" class="loadOnOverlay button">Cartola de existencias</a>
</div>
<br /><br />
	<div class="panes">
	<fieldset>
		<div class="left">
			<img src="<?=$prod_foto ?>" alt="" style="width:90px;border:1px solid #CCC;background:#EEE;padding:3px;margin:5px;" />
		</div>
		<div class="left">
			<label>Código: </label><?=$datos['dg_codigo'] ?><br />
			<label>Nombre: </label><?=$datos['dg_producto'] ?><br />
			<label>Precio de venta (Referencial): </label>$<?=number_format($datos['dq_precio_venta'],$empresa_conf['dn_decimales_local'],$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']) ?><br />
			<label>Precio de compra (Referencial): </label>$<?=number_format($datos['dq_precio_compra'],$empresa_conf['dn_decimales_local'],$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']) ?><br />
			<label>Marca: </label><?=$datos['dg_marca'] ?><br />
			<label>Línea de Negocio: </label><?=$datos['dg_linea_negocio'] ?><br />
		</div>
	</fieldset>
	</div>
</div>