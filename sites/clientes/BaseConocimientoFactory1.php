<?php

	require_once 'inc/PHPExcel.php';

	class BaseConocimientoFactory extends Factory{

		private $datoAntiguo;

		//todo que permite obtener un informe en excel sobre la base de conocimiento de clientes
		public function indexAction(){

			//Establecer conexion y obtencion de datos
			$db = $this->getConnection();
			$st1 = $db->prepare($db->select('tb_campo_personalizado'));/*, '*', 'dc_empresa = ?'));
			$st1 -> bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);*/
			$db -> stExec($st1);

			/*$st2 = $db->prepare("select cp.dg_campo, cte.dg_razon, ccp.dg_valor, cte.dc_empresa, concat(tfun.dg_nombres, ' ', dg_ap_paterno, ' ',dg_ap_materno) Ejecutivo, csuc.dg_direccion
					from tb_cliente cte LEFT JOIN tb_cliente_campo_personalizado ccp ON ccp.dc_cliente = cte.dc_cliente
					LEFT JOIN tb_campo_personalizado cp ON ccp.dc_campo = cp.dc_campo
					LEFT JOIN tb_funcionario tfun ON cte.dc_ejecutivo = tfun.dc_funcionario
					LEFT JOIN tb_cliente_sucursal csuc ON  cte.dc_casa_matriz = csuc.dc_sucursal
					where cte.dc_empresa = ? order by cte.dg_razon");
			$st2 -> bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
			*/
			/*$st2 = $db->prepare("SELECT cp.dg_campo, cte.dg_razon, ccp.dg_valor, cte.dc_empresa,
					concat(tfun.dg_nombres, ' ', dg_ap_paterno, ' ',dg_ap_materno) Ejecutivo, csuc.dg_direccion,
					ccte.dg_contacto, ccte.dg_fono, ccte.dg_email
					FROM ((tb_campo_personalizado cp, tb_cliente cte)
					LEFT JOIN tb_cliente_campo_personalizado ccp on ccp.dc_campo = cp.dc_campo AND ccp.dc_cliente = cte.dc_cliente)
					LEFT JOIN tb_funcionario tfun on cte.dc_ejecutivo = tfun.dc_funcionario
					LEFT JOIN tb_cliente_sucursal csuc ON  cte.dc_casa_matriz = csuc.dc_sucursal
					LEFT JOIN tb_contacto_cliente ccte ON ccte.dc_cliente = cte.dc_cliente
					where cte.dc_empresa = ? order by cte.dg_razon");
			$st2 -> bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
			$db -> stExec($st2);
			//tb_campo_personalizado cp
			//ccp.dc_campo = cp.dc_campo AND*/
			$st2 = $db->prepare("SELECT cp.dg_campo, cte.dg_razon, ccp.dg_valor, cte.dc_empresa,
					concat(tfun.dg_nombres, ' ', dg_ap_paterno, ' ',dg_ap_materno) Ejecutivo, csuc.dg_direccion,
					ccte.dg_contacto, ccte.dg_fono, ccte.dg_email
					FROM tb_cliente cte
					LEFT JOIN tb_cliente_campo_personalizado ccp on ccp.dc_cliente = cte.dc_cliente
					LEFT JOIN tb_campo_personalizado cp ON cp.dc_campo = ccp.dc_campo
					LEFT JOIN tb_funcionario tfun on cte.dc_ejecutivo = tfun.dc_funcionario
					LEFT JOIN tb_cliente_sucursal csuc ON  cte.dc_casa_matriz = csuc.dc_sucursal
					LEFT JOIN tb_contacto_cliente ccte ON ccte.dc_cliente = cte.dc_cliente
					where cte.dc_empresa = ? order by cte.dg_razon");
			$st2 -> bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
			$db -> stExec($st2);

			//Crear objeto PHPExcel
			$obPHPExcel = new PHPExcel();

			//Propiedades del Excel
			$obPHPExcel->getProperties()
				->setCreator("onesoft")
				->setLastModifiedBy("onesoft")
				->setTitle("Base Conocimiento")
				->setSubject("Base Conocimiento")
				->setDescription("Base de conocimiento de clientes")
				->setKeywords("Excel Office 2007 openxml php")
				->setCategory("Ventas");

			//Set de parámetros, declaración de contador para formatear celdas al final del documento
			$columna = 'A';
			$fila = 1;
			$contador = 0;

			//Llenar las primera fila del documento
			$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Nombre Empresa");
			$obPHPExcel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
			$columna++;
			$contador++;
			$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Direccion Casa Matriz");
			$obPHPExcel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
			$columna++;
			$contador++;
			$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Ejecutivo Asignado");
			$obPHPExcel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
			$columna++;
			$contador++;
			$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Contacto");
			$obPHPExcel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
			$columna++;
			$contador++;
			$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Teléfono");
			$obPHPExcel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
			//atencion, se setea la columna que corresponderá a la casilla teléfono
			$columnaTelefono = $columna;
			$columna++;
			$contador++;
			$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Email");
			$obPHPExcel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
			$columna++;
			$contador++;

			while($row = $st1->fetch(PDO::FETCH_ASSOC)){
				$data = $row['dg_campo'];
				$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$data");
				$obPHPExcel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
				$columna++;
				$contador++;
			}
			//Variable que determinará el total de filas
			$totalFilas = 0;
			//Llenar el resto del documento
			while($row = $st2->fetch(PDO::FETCH_ASSOC)){
				$dato1 = $row['dg_razon'];
				$dato2 = $row['dg_valor'];
				$dato3 = $row['Ejecutivo'];
				$dato4 = $row['dg_direccion'];
				$contacto = $row['dg_contacto'];
				$fono = $row['dg_fono'];
				$email = $row['dg_email'];
				if($this->datoAntiguo != $dato1){
					$fila++;
					$columna='A';
					$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$dato1");
					$this->datoAntiguo = $dato1;
					$columna++;
					$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$dato4");
					$columna++;
					$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$dato3");
					$columna++;
					$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$contacto");
					$columna++;
					$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$fono");
					$columna++;
					$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$email");
					$totalFilas++;
				}
				$columna++;
				$obPHPExcel->setActiveSheetIndex(0)->setCellValue("$columna$fila","$dato2");

			}

			//Establecer el tamaño de las celdas automáticamente
			$columna = 'A';
			for($i = 1; $i <=$contador; $i++){
				$obPHPExcel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
				$columna++;
			}

			//Renombrar hoja
			$obPHPExcel->getActiveSheet()->setTitle('Knowledge base');
			//Establecer hoja activa para que se muestre al abrir el excel
			$obPHPExcel->setActiveSheetIndex(0);
			//Indicar al navegador que muestre el dialogo de descarga
			header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
			//Indicar que se esta devolviendo un archivo
			header("Content-Disposition: attachment; filename=Knowledge_base.xlsx");
			//Evitar que quede en cache
			header('Cache-control: max-age=0');
			//Dar salida al documento
			$obPHPExcel = PHPExcel_IOFactory::createWriter($obPHPExcel, 'Excel2007');
			$obPHPExcel->save('php://output');
			exit;

		}


		//todo que permite obtener un informe en excel sobre la base de conocimiento de clientes
		public function informeAntiguoAction(){


			$db = $this->getConnection();
			$st1 = $db->prepare($db->select('tb_campo_personalizado'));/*, '*', 'dc_empresa = ?'));
			$st1 -> bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);*/
			$db -> stExec($st1);


			$st2 = $db->prepare("select cp.dg_campo, cte.dg_razon, ccp.dg_valor, cte.dc_empresa
					from (tb_campo_personalizado cp, tb_cliente cte)left join tb_cliente_campo_personalizado ccp
					on ccp.dc_campo = cp.dc_campo
					and ccp.dc_cliente = cte.dc_cliente where cte.dc_empresa = ? order by cte.dg_razon");
			$st2 -> bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
			$db -> stExec($st2);




			//Declaracion de encabezados
			$tablaExcel =  "<html>
								<table>
								<header>
									<tr>
										<th></th>";


			while($row = $st1->fetch(PDO::FETCH_ASSOC)){
				$data = utf8_encode($row['dg_campo']);
				$tablaExcel.="<th>$data</th>";
			}

			$tablaExcel .="</tr>
							</header>";

			//Contenido de la tabla

			$contador = 0;
			$tablaExcel .="<tr>";
			while($row = $st2->fetch(PDO::FETCH_ASSOC)){
				$dato1 = $row['dg_razon'];
				$dato2 = $row['dg_valor'];
				if($this->datoAntiguo != $dato1){
					if($contador != 0){
						$tablaExcel .=" </tr><tr>";
					}
					$tablaExcel .= "<td>$dato1</td>";
					$this->datoAntiguo = $dato1;
				}
				$tablaExcel .= "<td>$dato2</td>";
				$contador++;

				//$tablaExcel.="<th>$data</th>";
			}


			//Finalizar tabla
			$tablaExcel .="</tr></table>";
			$tablaExcel .="</html>";

			//Indicar al navegador que muestre el dialogo de descarga
			header("Content-type: application/octet-stream; charset=UTF-8");
			//Indicar que se esta devolviendo un archivo
			header("Content-Disposition: attachment; filename=Knowledge_base.xls");
			//Evitar que quede en cache
			header("Pragma: no-cache");
			header("Expires: 0");
			//Dar salida a la tabla
			echo $tablaExcel;

		}

	}

?>
