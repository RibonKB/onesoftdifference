<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div id="secc_bar">Creación de clientes</div>
<div id="main_cont"><div class="panes">');

$form->Start('sites/clientes/proc/crear_cliente.php','cr_cliente');
$form->Header('<b>Ingrese los datos para la creación del cliente</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();
$form->Text("RUT","dg_rut",1,12,"","inputrut");
$form->Text("Giro","dg_giro",1);
$form->Listado("Mercado","dc_mercado","tb_mercado",array("dc_mercado","dg_mercado"),1);
$form->Text("Linea de crédito","dq_linea_credito",1,10);
$form->Listado('Ejecutivo','dc_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
$form->EndSection();

$form->Section();
$form->Text("Razon social","dg_razon",1,100);
$form->Text("Sitio web","dg_sitioweb",0,50);
$form->Listado("Tipo de cliente","dc_tipo_cliente","tb_tipo_cliente",array("dc_tipo_cliente","dg_tipo_cliente"),1);
$form->ListadoMultiple("Medios de pago",'dc_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'));
$form->Listado("Cuenta Contable","dc_cuenta_contable","tb_cuenta_contable",array("dc_cuenta_contable","dg_cuenta_contable"),1);
$form->EndSection();

$form->Section();
if(check_permiso(62))
	echo("<br /><label>
		<input type='checkbox' name='cli_val_nv' value='1' /> Validar Notas de venta automáticamente
	</label><br />");
	
if(check_permiso(63))
	echo("<br /><label>
		<input type='checkbox' name='cli_conf_nv' value='1' /> Confirmar Notas de venta automáticamente
	</label><br />");
$form->EndSection();

$form->Group();
$form->Header('<b>Casa Matriz</b><br /><small>(Luego podrá agregar nuevas sucursales)</small>');

$form->Section();	 
$form->Text('Nombre','dg_sucursal',1);
$form->Text('Dirección','dg_direccion',1);
$form->Listado('Región','dc_region','tb_region',array('dc_region','dg_region'),0,0,'');
$form->Select('Comuna','dc_comuna',array(),1);
$form->EndSection();

$form->Section();
$form->Text('Teléfono','dg_telefono_sucursal');
$form->Text('Fax','dg_fax_sucursal');
$form->Text('Código Postal','dg_postal');
//$form->Text('Horario atención','cli_suc_horario');
echo('<br /><div id="horario_atencion_data"></div>');
$form->Button('Horario de atención','id="setup_horario_atencion"');

$form->EndSection();

$form->Section();
$aux = $db->select("tb_tipo_direccion","dc_tipo_direccion,dg_tipo_direccion","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by"=>"dg_tipo_direccion"));
$tcontactos = array();
foreach($aux as $t)
	$tcontactos[$t['dc_tipo_direccion']] = $t['dg_tipo_direccion'];
$form->Combobox("Tipo de sucursal","dc_tipo_sucursal",$tcontactos);
$form->EndSection();

$form->Group();
$form->Header('<b>Contacto</b><br /><small>(Este será seleccionado automáticamente como el contacto por defecto)</small>');

$form->Section();	 
$form->Text('Nombre contacto','dg_contacto',1);
$form->Text('Teléfono','dg_telefono_contacto');
$form->Text('Teléfono movil','dg_movil_contacto');
$form->EndSection();

$form->Section();
$form->Text('E-Mail','dg_email');
$form->Text('Fax','dg_fax_contacto');
$form->Listado('Cargo contacto','dc_cargo_contacto','tb_cargo_contacto',array('dc_cargo_contacto','dg_cargo_contacto'));
$form->EndSection();

$form->Group();
$form->End('Crear','addbtn');

$aux = $db->select('tb_comuna','dc_comuna,dg_comuna,dc_region');
$comunas = array();
foreach($aux as $comuna){
	$comunas[$comuna['dc_region']][] = array($comuna['dc_comuna'],$comuna['dg_comuna']);
}

?>
</div></div>
<script type="text/javascript" src="jscripts/clientes/cr_cliente.js?v=0_2_2b"></script>
<script type="text/javascript">
js_data.comunas = <?php echo json_encode($comunas) ?>;
js_data.init();
</script>
