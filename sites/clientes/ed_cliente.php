<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_cliente',
'dg_rut,dg_razon,dg_giro,dg_sitio_web,dc_empresa,dc_tipo_cliente,dc_mercado,dq_linea_credito,dc_ejecutivo,dm_validar_nota_venta,dm_confirmar_nota_venta,dc_cuenta_contable',
"dc_cliente={$_POST['id']} AND dc_empresa={$empresa}");

if(!count($data)){
	$error_man->showAviso("No se encontró ningún cliente con los criterios especificados");
	exit();
}
$data = $data[0];

$aux = $db->select('tb_cliente_medio_pago','dc_medio_pago',"dc_cliente={$_POST['id']}");
$medios_pago = array();
foreach($aux as $m){
	$medios_pago[] = $m['dc_medio_pago'];
}

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div class="secc_bar">Edición de clientes</div><div class="panes">');

$form->Start('sites/clientes/proc/editar_cliente.php','ed_cliente');
$form->Header('<b>Ingrese los datos para editar del cliente</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();
$form->Text("RUT","cli_rut",1,12,$data['dg_rut'],"inputrut");
$form->Text("Giro","cli_giro",1,255,$data['dg_giro']);
$form->Listado("Mercado","cli_mercado","tb_mercado",array("dc_mercado","dg_mercado"),1,$data['dc_mercado']);
$form->Text("Linea de crédito","cli_linea",1,10,$data['dq_linea_credito']);
$form->Listado('Ejecutivo','cli_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'),0,$data['dc_ejecutivo']);
$form->EndSection();

$form->Section();
$form->Text("Razon social","cli_razon",1,100,$data['dg_razon']);
$form->Text("Sitio web","cli_web",0,50,$data['dg_sitio_web']);
$form->Listado("Tipo de cliente","cli_tipo","tb_tipo_cliente",array("dc_tipo_cliente","dg_tipo_cliente"),1,$data['dc_tipo_cliente']);
$form->ListadoMultiple("Medios de pago",'cli_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'),$medios_pago);
$form->Listado("Cuenta Contable","dc_cuenta_contable","tb_cuenta_contable",array("dc_cuenta_contable","dg_cuenta_contable"),1,$data['dc_cuenta_contable']);
$form->EndSection();

$form->Section();
if(check_permiso(62)){
	$checked = $data['dm_validar_nota_venta']==1?'checked="checked"':'';
	echo("<br /><label>
		<input type='checkbox' name='cli_val_nv' value='1' {$checked} /> Validar Notas de venta automáticamente
	</label><br />");
}else{
	$form->Hidden('dm_validar_nota_venta',$data['dm_validar_nota_venta']);
}
	
if(check_permiso(63)){
	$checked = $data['dm_confirmar_nota_venta']==1?'checked="checked"':'';
	echo("<br /><label>
		<input type='checkbox' name='cli_conf_nv' value='1' {$checked} /> Confirmar Notas de venta automáticamente
	</label><br />");
}else{
	$form->Hidden('dm_confirmar_nota_venta',$data['dm_confirmar_nota_venta']);
}
$form->EndSection();

$form->Hidden('cli_id',$_POST['id']);
$form->End('Editar','editbtn');

echo('</div>');
?>
<script type="text/javascript">
js_data.editInit();
</script>
