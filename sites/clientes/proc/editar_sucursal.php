<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$db->update('tb_cliente_sucursal',array(
	'dg_sucursal' => $_POST['cli_suc_name'],
	'dg_direccion' => $_POST['cli_suc_direccion'],
	'dg_fono' => $_POST['cli_suc_fono'],
	'dg_fax' => $_POST['cli_suc_fax'],
	'dg_codigo_postal' => $_POST['cli_suc_postal'],
	'dg_horario_atencion' => $_POST['cli_suc_horario'],
	'dc_comuna' => $_POST['cli_suc_comuna']
),"dc_sucursal={$_POST['sucursal_id']}");

if(isset($_POST['cli_suc_tipo']) && implode(',',$_POST['cli_suc_tipo']) != $_POST['tipos_change']){
	$db->query("DELETE FROM tb_cliente_sucursal_tipo where dc_sucursal={$_POST['sucursal_id']}");
	
	foreach($_POST['cli_suc_tipo'] as $tipo){
		$db->insert('tb_cliente_sucursal_tipo',array(
			'dc_sucursal' => $_POST['sucursal_id'],
			'dc_tipo_direccion' => $tipo
		));
	}
}else if(!isset($_POST['cli_suc_tipo']) && $_POST['tipos_change'] != ''){
	$db->query("DELETE FROM tb_cliente_sucursal_tipo where dc_sucursal={$_POST['sucursal_id']}");
}

$db->commit();
?>
<script type="text/javascript">
$('#genOverlay').remove();
show_confirm('Se han modificado los datos de la sucursal correctamente.');
</script>