<?php
/**
*
*	Contiene el formulario completo para editar un contacto.
*	Similar a cotactoForm.php con la diferencia de que rellena con los datos del cliente especificado por $datos['dc_cliente']
*
**/
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
//prepara los datos (de tablas, campos y condiciones) para traer los datos de contactos del cliente especificado

$tables = "(SELECT * FROM tb_contacto_cliente WHERE dm_activo = '1') c
JOIN tb_domicilio_cliente_tipo_direccion dtd ON c.dc_contacto = dtd.dc_contacto
JOIN (SELECT * FROM tb_domicilio_cliente WHERE dc_cliente = {$datos['dc_cliente']}) d ON d.dc_domicilio = dtd.dc_domicilio
LEFT JOIN tb_comuna co ON d.dc_comuna = co.dc_comuna
LEFT JOIN tb_region re ON re.dc_region = co.dc_region
LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto";

$fields = "distinct re.dc_region, dg_region, co.dc_comuna, dg_comuna, d.dc_domicilio, dg_direccion, dg_fono, dg_fax, dg_correo,
dg_comentario, dg_horario_atencion, cc.dc_cargo_contacto, c.dc_contacto, dg_contacto, dg_cargo_contacto, c.dm_default";

$contacto = $db->select($tables,$fields);
unset($tables,$fields);

//Para cada contacto del cliente mostrar los datos en el formulario
foreach($contacto as $c){
$default = "";
if($c['dm_default'] == '1'){
	$default = 'checked="checked"';
}

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	echo("<fieldset id='old_{$c['dc_contacto']}'>");
	$form->Header("
	<div class='right'>
		<img src='images/ocultar.png' alt='ocultar' class='tip' onclick='hidContacto(\"old\",{$c['dc_contacto']})' />
			<div class='tooltip'>Mostrar/Ocultar</div>
		<img src='images/cerrar.png' alt='eliminar' class='tip' onclick='deshContacto(\"old\",{$c['dc_contacto']},\"clientes\")' />
			<div class='tooltip'>Eliminar el contacto</div>
	</div>
	contacto: <strong>{$c['dg_contacto']}</strong>");
	echo("
	<div class='right'>
		Contacto Default: 
		<input type='radio' name='cont_default' value='{$c['dc_contacto']}o' {$default} />
	</div>
	<br class='clear' />
	<div class='contacto_cont' style='display:none;'>");
	$form->Section();
	$form->Text("Nombre contacto","econtacto[]",1,99,$c['dg_contacto']);
	$form->Listado("Cargo del contacto","ecargo[]","tb_cargo_contacto",array("dc_cargo_contacto","dg_cargo_contacto"),0,$c['dc_cargo_contacto'],"dc_cargo_contacto > 0 AND dc_empresa=$empresa");
	$form->Text("Dirección","edir[]",1,99,$c['dg_direccion']);
	
	$regiones = $db->select("tb_region");
	echo("<label>Region<br />
		<select onchange='showComunas(\"ecomuna_{$c['dc_contacto']}\",this)' class='inputtext' id='eregion_{$c['dc_contacto']}'>");
	foreach($regiones as $r){
		echo("<option value='{$r['dc_region']}'>{$r['dg_region']}</option>");
	}
	echo("</select></label><br />
	<label>Comuna[*]<br />
	<select name='ecomuna[]' id='ecomuna_{$c['dc_contacto']}' class='inputtext' required='required'>
		<option></option>
	</select>
	</label>
	<span id='comuna_{$_POST['indice']}_comloader'></span>
	<br />
	");
	
	$form->Textarea("Comentario","ecomm[]",0,$c['dg_comentario']);
	$form->EndSection();
	$form->Section();
	$form->Text("Teléfono","etfono[]",1,99,$c['dg_fono']);
	$form->Text("Correo electrónico","eemail[]",0,99,$c['dg_correo']);
	$form->Text("Fax","efax[]",0,99,$c['dg_fax']);
	$form->Text("Horario de atención","ehorario[]",0,255,$c['dg_horario_atencion']);
	
	$tiposcontacto = $db->select("tb_tipo_direccion","dc_tipo_direccion,dg_tipo_direccion","dc_empresa={$empresa}",array("order_by"=>"dg_tipo_direccion"));
	$seleccionados = $db->select("tb_domicilio_cliente_tipo_direccion","dc_tipo_direccion","dm_activo = '1' AND dc_contacto={$c['dc_contacto']}");
	$sel = array();
	$tipos = array();
	foreach($seleccionados as $s){
		$sel[] = $s['dc_tipo_direccion'];
	}
	foreach($tiposcontacto as $t){
		$tipos[$t['dc_tipo_direccion']] = $t['dg_tipo_direccion'];
	}
	$form->Combobox('',"etipo{$c['dc_contacto']}",$tipos,$sel);
	$form->EndSection();
	$form->Hidden("eindice[]",$c['dc_contacto']);
	$form->Hidden("edomicilio[]",$c['dc_domicilio']);
	echo("
	</div>
	</fieldset>");
?>
<script type="text/javascript">
//Asigna la región del contacto en caso de que se haya especificado, para luego seleccionar la comuna
$("#eregion_<?=$c['dc_contacto'] ?>").val(<?=$c['dc_region'] ?>);
showComunas("ecomuna_<?=$c['dc_contacto'] ?>",document.getElementById("eregion_<?=$c['dc_contacto'] ?>"),<?=$c['dc_comuna'] ?>);
</script>
<?php
}
?>