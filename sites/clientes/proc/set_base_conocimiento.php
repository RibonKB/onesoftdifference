<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if($_POST['set_edit'] != 0){
	$db->query("DELETE FROM tb_cliente_campo_personalizado WHERE dc_cliente={$_POST['cli_id']}");
}

foreach($_POST['field'] as $i => $v){
	if(is_array($v)){
		$v = implode('###',$v);
	}
	
	$db->insert('tb_cliente_campo_personalizado',array(
		'dc_cliente' => $_POST['cli_id'],
		'dc_campo' => $i,
		'dg_valor' => str_replace('###','<',htmlentities($v, ENT_QUOTES))
	));
}

$error_man->showConfirm('Se han asignado los datos a la base de conocimientos del cliente.');
?>