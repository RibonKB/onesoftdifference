<?php
/**
*	Deshabilita el contacto especificado por el id $_POST['id']
**/

define("MAIN",1);
require_once("../../../inc/global.php");
//comprueba que el archivo haya sido llamado utilizando la función loadFile() de onesoft.js
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//modifica el campo db_visible para el contacto especificado
$db->update("tb_contacto_cliente",array("dm_activo" => '0'),"dc_contacto={$_POST['id']}");

//muestra un mensaje especificando que se "eliminó" el contacto
$error_man->showConfirm("Se ha eliminado el contacto");

?>