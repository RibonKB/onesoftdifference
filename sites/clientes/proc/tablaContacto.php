<?php
/**
*	Muestra de manera ordenada todos los contactos para un tipo especificado por $t['dc_tipo_direccion'] y para el cliente dado por $datos['dc_cliente'] ambos provenientes desde el archivo "mostrarcliente.php"
**/

//prepara los datos (tablas, campos y condiciones) para la consulta que traerá todos los contactos del tipo especificado
	
$tables = "(SELECT * FROM tb_contacto_cliente WHERE dm_activo = '1') c
JOIN (SELECT * FROM tb_domicilio_cliente_tipo_direccion WHERE dm_activo = '1' AND dc_tipo_direccion = {$t['dc_tipo_direccion']}) dtd ON c.dc_contacto = dtd.dc_contacto
JOIN (SELECT * FROM tb_domicilio_cliente WHERE dc_cliente = {$datos['dc_cliente']}) d ON d.dc_domicilio = dtd.dc_domicilio
JOIN tb_comuna co ON d.dc_comuna = co.dc_comuna
JOIN tb_region re ON co.dc_region = re.dc_region
JOIN tb_cargo_contacto cc ON c.dc_cargo_contacto = cc.dc_cargo_contacto";
	
$fields = "dg_contacto, dg_fono, dg_fax, dg_correo, dg_comentario, dg_direccion, dg_horario_atencion, dg_comuna, dg_region, dg_cargo_contacto";
	
//trae desde la base de datos todos los contactos del tipo especificado
$contacto = $db->select($tables,$fields);
unset($tables,$fields);

echo('<div>');

//en caso de que ningún contacto para ese tipo haya sido rescatado se muestra un mensaje especificandolo
if(!count($contacto)){
	$error_man->showInfo("No existen contactos del tipo <strong>{$t['dg_tipo_direccion']}</strong> para el cliente");
}

//en caso contrario se recorren todos los registros recuperados y se muestran de manera ordenada en una tabla
foreach($contacto as $c){ ?>

<fieldset>
<table class="tab" width="100%">
<tbody align="left">

<tr>
	<th scope="col" width="150">
	<label>Nombre Contacto:</label>
	</th>
	<td>
	<?= $c['dg_contacto'] ?>
	</td>
</tr>

<tr>
	<th>
	<label>Teléfono:</label>
	</th>
	<td>
	<?= $c['dg_fono'] ?>
	</td>
</tr>

<tr>
	<th>
	<label>Correo Electrónico:</label>
	</th>
	<td>
	<?= $c['dg_correo'] ?>
	</td>
</tr>

<tr>
	<th>
	<label>Fax:</label>
	</th>
	<td>
	<?= $c['dg_fax'] ?>
	</td>
</tr>

<tr>
	<th>
	<label>Dirección:</label>
	</th>
	<td>
	<?="<strong>{$c['dg_direccion']}</strong> {$c['dg_comuna']} <label>{$c['dg_region']}</label>" ?>
	</td>
</tr>

<tr>
	<th>
	<label>Horario de Atención:</label>
	</th>
	<td>
	<?php echo($c['dg_horario_atencion']); ?>
	</td>
</tr>

<tr>
	<th>
	<label>Comentario:</label>
	</th>
	<td>
	<?php echo($c['dg_comentario']); ?>
	</td>
</tr>

</tbody>
</table>
</fieldset>

<?php } 

//luego de mostrados todos los contactos para ese usuario (o el mensaje informando que no se encontró) se muestra un boton que permite agregar nuevos contactos
?>
<div id="contacto_<?php echo($t['dc_tipo_direccion']); ?>"></div>
<div class="center">
<button type="button" class="button" id="button_<?php echo($t['dc_tipo_direccion']); ?>">Agregar nuevo contacto <?php echo($t['dg_tipo_direccion']); ?></button>
</div>

</div>
<script type="text/javascript">
	$("#button_<?php echo($t['dc_tipo_direccion']); ?>").click(function(){
		addContacto('contacto_<?php echo($t['dc_tipo_direccion']); ?>','clientes');
		$('#contacto_<?php echo($t['dc_tipo_direccion']); ?>').before("<div id='c_"+contacto+"_res'></div>");
	});
</script>