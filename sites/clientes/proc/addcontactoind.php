<?php
/**
*	Agrega contactos individuales al cliente especificado por $_POST['cli']
**/
	define("MAIN",1);
	require_once("../../../inc/global.php");
	require_once("../../../inc/contacto_class.php");
	if(!isset($_POST['asinc'])){
		$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
		$error_man->show_fatal_error("Acceso Denegado",$options);
	}
	
	$ind =& $_POST['ind'];
	$cli =& $_POST['cli'];
	
	//se comprueba que al menos un checkbox de tipo de contacto haya sido marcado. en caso de no marcar ninguno se muestra error
	if(!isset($_POST['tipo'.$ind])){
		$error_man->showWarning("Debe especificar al menos 1 tipo de contacto en el formulario");
	}else{
	
		//rescata los tipos de contacto seleccionados para asignarselos al contacto.
		$tipos = array($_POST["tipo{$ind}"]);
		
		//inicialización del objeto de contacto
		$contactos = new Contacto();
		
		$contactos->setDefault(array(0));
		$contactos->setNombreContacto($_POST['contacto']);
		$contactos->setCargo($_POST['cargo']);
		$contactos->setFono($_POST['tfono']);
		$contactos->setFax($_POST['fax']);
		$contactos->setCorreo($_POST['email']);
		$contactos->setComentario($_POST['comm']);
		$contactos->setComuna($_POST['comuna']);
		$contactos->setDireccion($_POST['dir']);
		$contactos->setHorario($_POST['horario']);
		$contactos->setTipo($tipos);
		
		//inserta el contacto en la base de datos para el cliente especificado
		$contactos->insertarNuevos($cli);
		
		//Mensaje de confirmación
		$error_man->showConfirm("Se ha agregado el nuevo contacto al cliente");
		
	}
	
?>