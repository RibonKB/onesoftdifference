<?php
/**
*	Muestra el formulario de edición de clientes permitiendo al usuario agregar nueva información o editar la existente.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$tables = "(SELECT * FROM tb_cliente WHERE dm_activo = '1' AND dg_rut = '{$_POST['cli_rut']}') cl
LEFT JOIN tb_mercado m ON cl.dc_mercado = m.dc_mercado
LEFT JOIN tb_tipo_cliente tc ON cl.dc_tipo_cliente = tc.dc_tipo_cliente
LEFT JOIN tb_cliente_estado ce ON cl.dc_cliente_estado = ce.dc_cliente_estado";

$fields = "dc_cliente,dg_rut,dg_razon,dg_giro,dg_sitioweb,dg_cliente_estado,
dq_linea_credito,tc.dc_tipo_cliente,dg_tipo_cliente,m.dc_mercado,dg_mercado";

//Obtiene los datos del cliente para rellenar el formulario con estos.
$datos = $db->select($tables,$fields);
unset($tables,$fields);

if(!count($datos)){
	$error_man->showErrorRedirect("El cliente especificado no se ha encontrado","sites/clientes/ed_cliente.php");
}
$datos = $datos[0];

?>

<div id="secc_bar">Edición de los datos del cliente</div>
<div id="other_options">
	<ul>
		<li><a href="sites/clientes/src_cliente.php?idCliente=<?=$datos['dc_cliente'] ?>" class="loader">Consultar cliente</a></li>
		<li><a href="sites/clientes/cr_cliente.php" class="loader">Crear clientes</a></li>
		<li class="oo_active"><a href="sites/clientes/ed_cliente.php" class="loader">Editar otro Cliente</a></li>
		<li><a href="sites/clientes/val_cliente.php?idCliente=<?=$datos['dc_cliente'] ?>" class="loader">Validar Cliente</a></li>
	</ul>
</div>

<div id="main_cont">
<div class="panes">
<div class="title">
Editando - <strong><?=$datos['dg_razon'] ?></strong> ( <?=$datos['dg_rut'] ?> )
</div>
<?php
	include("../../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Header("<strong>Ingrese los datos actualizados del cliente</strong><br />Los campos marcados con [*] son obligatorios");
	$form->Start("sites/clientes/proc/editar_cliente.php","ed_cliente");
	$form->Section();
	$form->Text("Giro","cli_giro",1,250,$datos['dg_giro']);
	$form->Listado("Mercado","cli_mercado","tb_mercado",array("dc_mercado","dg_mercado"),1,$datos['dc_mercado']);
	$form->Text("Linea de crédito","cli_linea",1,10,$datos['dq_linea_credito']);
	$form->EndSection();
	$form->Section();
	$form->Text("Razón social","cli_razon",1,100,$datos['dg_razon']);
	$form->Text("Sitio web","cli_web",0,50,$datos['dg_sitioweb']);
	$form->Listado("Tipo de cliente","cli_tipo","tb_tipo_cliente",array("dc_tipo_cliente","dg_tipo_cliente"),1,$datos['dc_tipo_cliente']);
	$form->EndSection();
	$form->Group();
	$form->Header("<strong>Agregar o editar contacto</strong>");
	echo("<div id='contactos'>");
	include("contactoForm_edit.php");
	echo("</div><div class='center'>");
	$form->Button("Agregar nuevo contacto","onClick='addContacto(\"contactos\",\"clientes\");'","addbtn");
	echo("</div>");
	$form->Hidden("cli_id",$datos['dc_cliente']);
	$form->End("Editar","editbtn");
?>
</div>
<script type="text/javascript">
var contacto = 0;
var addc_inline = false;
</script>