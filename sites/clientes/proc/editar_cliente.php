<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$db->update('tb_cliente',array(
	'dg_rut' => $_POST['cli_rut'],
	'dc_ejecutivo' => $_POST['cli_ejecutivo'],
	'dg_razon' => $_POST['cli_razon'],
	'dg_giro' => $_POST['cli_giro'],
	'dg_sitio_web' => $_POST['cli_web'],
	'dm_validar_nota_venta' => isset($_POST['cli_val_nv'])?1:isset($_POST['dm_validar_nota_venta'])?$_POST['dm_validar_nota_venta']:0,
	'dm_confirmar_nota_venta' => isset($_POST['cli_conf_nv'])?1:isset($_POST['dm_confirmar_nota_venta'])?$_POST['dm_confirmar_nota_venta']:0,
	'dg_password' => md5($_POST['cli_rut']),
	'dc_tipo_cliente' => $_POST['cli_tipo'],
	'dc_mercado' => $_POST['cli_mercado'],
	'dq_linea_credito' => $_POST['cli_linea'],
	'dc_cuenta_contable' => $_POST['dc_cuenta_contable']
),"dc_cliente={$_POST['cli_id']}");

$db->query("DELETE FROM tb_cliente_medio_pago WHERE dc_cliente = {$_POST['cli_id']}");

if(isset($_POST['cli_pago'])){
	foreach($_POST['cli_pago'] as $mp){
		$db->insert('tb_cliente_medio_pago',array(
			'dc_cliente' => $_POST['cli_id'],
			'dc_medio_pago' => $mp
		));
	}
}

$db->commit();

$error_man->showConfirm('Se han modificado los datos del cliente correctamente');
?>
<script type="text/javascript">
	js_data.refreshClient();
</script>
