<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_cliente WHERE dg_rut='{$_POST['cli_rut']}' AND dc_empresa={$empresa}) cl
LEFT JOIN tb_tipo_cliente t ON t.dc_tipo_cliente = cl.dc_tipo_cliente
LEFT JOIN tb_funcionario f ON f.dc_funcionario = cl.dc_ejecutivo
JOIN tb_mercado m ON m.dc_mercado = cl.dc_mercado
JOIN tb_contacto_cliente c ON c.dc_contacto = cl.dc_contacto_default
JOIN tb_cliente_sucursal s ON s.dc_sucursal = cl.dc_casa_matriz
	LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
	LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region",
'cl.dc_cliente,cl.dg_rut,cl.dg_razon,cl.dg_giro,cl.dg_sitio_web,cl.dc_cliente_estado,cl.dq_linea_credito,DATE_FORMAT(cl.df_creacion,"%d/%m/%Y") AS df_creacion,
m.dg_mercado,t.dg_tipo_cliente,c.dg_contacto,c.dg_fono,c.dg_movil,c.dg_email,c.dg_fax,s.dg_sucursal,s.dg_direccion,s.dg_fono AS dg_fono_matriz,com.dg_comuna,reg.dg_region,
CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS dg_ejecutivo');

if(!count($data)){
	$error_man->showAviso("No se encontró ningún cliente con los criterios especificados");
	exit();
}
$data = $data[0];

//http://192.168.0.202/callto/manager.php?call=&ext=6804&call=90978581368&Submit+now.x=0&Submit+now.y=0

$anexo = $db->select('tb_funcionario','dg_anexo',"dc_funcionario={$userdata['dc_funcionario']}");
$anexo = $anexo[0]['dg_anexo'];

echo("<div id='options_menu'>");
if(check_permiso(44))
echo("<a href='sites/clientes/ed_cliente.php?id={$data['dc_cliente']}' class='loadOnOverlay editbtn'>Editar</a>");
if(check_permiso(46) && $data['dc_cliente_estado'] < 2)
echo("<a href='sites/clientes/proc/validar_cliente.php?id={$data['dc_cliente']}' id='valida_cliente' class='checkbtn'>Validar</a>");
echo("<a href='sites/clientes/cr_sucursal.php?id={$data['dc_cliente']}' class='loadOnOverlay addbtn'>Crear sucursal</a>");
echo("<a href='sites/clientes/base_conocimiento.php?id={$data['dc_cliente']}' class='loadOnOverlay button'>Base de conocimiento</a>");
echo("<a href='sites/clientes/src_analisis_cliente.php?id={$data['dc_cliente']}' class='loadOnOverlay button'>Análisis Cliente</a>");
echo("</div><br />");

$data['dq_linea_credito'] = moneda_local($data['dq_linea_credito']);
if($data['dg_movil']) $data['dg_movil'] = '<i>móvil:</i> '.$data['dg_movil'];
if($data['dg_fax']) $data['dg_fax'] = '<i>fax:</i> '.$data['dg_fax'];

echo("<div id='show_cliente'>
<table width='100%' class='tab'>
<caption>Razón social: <b>{$data['dg_razon']}</b></caption>
<tbody>
	<tr>
		<td>RUT</td>
		<td>{$data['dg_rut']}</td>
	</tr>
	<tr>
		<td>Giro</td>
		<td>{$data['dg_giro']}</td>
	</tr>
	<tr>
		<td>Casa Matriz</td>
		<td><b>{$data['dg_sucursal']}</b> {$data['dg_fono_matriz']} <address>{$data['dg_direccion']} {$data['dg_comuna']} {$data['dg_region']}</address></td>
	</tr>
	<tr>
		<td>Ejecutivo</td>
		<td>{$data['dg_ejecutivo']}</td>
	</tr>
	<tr>
		<td>Contacto</td>
		<td><b>{$data['dg_contacto']}</b> <i>fono:</i> {$data['dg_fono']} {$data['dg_movil']} {$data['dg_fax']}</td>
	</tr>
	<tr>
		<td>Sitio web</td>
		<td>{$data['dg_sitio_web']}</td>
	</tr>
	<tr>
		<td>Linea de crédito</td>
		<td>$ {$data['dq_linea_credito']}</td>
	</tr>
	<tr>
		<td>Tipo de cliente</td>
		<td>{$data['dg_tipo_cliente']}</td>
	</tr>
	<tr>
		<td>Mercado</td>
		<td>{$data['dg_mercado']}</td>
	</tr>
	<tr>
		<td>Fecha de ingreso</td>
		<td>{$data['df_creacion']}</td>
	</tr>
</tbody>
</table>");

$error_man->showInfo("Sucursales y contactos de <b>{$data['dg_razon']}</b>");

$sucursales = $db->select("(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente={$data['dc_cliente']} AND dm_activo=1) s
LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region",'dc_sucursal,dg_sucursal,dg_direccion,com.dg_comuna,reg.dg_region');

$aux_contactos = $db->select("(SELECT * FROM tb_contacto_cliente WHERE dc_cliente={$data['dc_cliente']} AND dm_activo=1) c
LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",'dc_contacto,dg_contacto,dg_fono,dg_fax,dg_movil,dg_email,dg_cargo_contacto,dc_sucursal');
$contactos = array();
foreach($aux_contactos as $c){
	$s = $c['dc_sucursal'];
	unset($c['dc_sucursal']);
	$contactos[$s][] = $c;
}
unset($aux_contactos);

foreach($sucursales as $s){
echo("<div style='border:1px solid #777;padding:6px;background:#F6F6F6;margin:3px;'>
	<a href='sites/clientes/cr_contacto.php?id={$s['dc_sucursal']}&cli_id={$data['dc_cliente']}' class='loadOnOverlay addbtn right'>Añadir Contacto</a>
	<button type='button' class='show_contactos button right'>Ver/Ocultar contactos</button>
	<a href='sites/clientes/ed_sucursal.php?id={$s['dc_sucursal']}' class='loadOnOverlay editbtn right'>Editar</a>
	<a href='sites/clientes/cr_pre_horario_atencion.php?dc_sucursal={$s['dc_sucursal']}' class='loadOnOverlay editbtn right'>Horarios de atención</a>
	<strong>{$s['dg_sucursal']} </strong><address>{$s['dg_direccion']}, <b>{$s['dg_comuna']}</b> {$s['dg_region']}</address><br class='clear' />
	<div class='cont_body hidden'>");
	
if(!isset($contactos[$s['dc_sucursal']])){
	$error_man->showInfo("La sucursal <b>{$s['dg_sucursal']}</b> no posee contactos que se puedan mostrar");
}else{
	echo('<hr /><table class="tab" width="100%"><caption>Contactos</caption>
	<thead><tr>
		<th></th>
		<th>Contacto</th>
		<th>Teléfono</th>
		<th>Móvil</th>
		<th>Fax</th>
		<th>E-mail</th>
		<th>Cargo</th>
	</tr></thead><tbody>');
		
	$razon = substr($data['dg_razon'],0,15);
	
	foreach($contactos[$s['dc_sucursal']] as $c){
		//http://192.168.0.202/callto/manager.php?call=&ext=6804&call=912314212312&Submit+now.x=0&Submit+now.y=0
		//http://192.168.0.202/callto/manager.php?call=&ext={$anexo}&call=9{$c['dg_movil']}&Submit+now.x=0&Submit+now.y=0
		if($anexo != 0){
			
			
			if($c['dg_movil'] && is_numeric($c['dg_movil'])){
				$query_data = http_build_query(array(
					'call' => $c['dg_movil'],
					'ext' => $anexo,
					'razon' => $data['dg_razon']
				));
				
				$c['dg_movil'] = "<a target='callframe' class='right' href='http://192.168.0.202/phone/asterisk_call_trigger.php?{$query_data}'>
					<img src='images/call.png' alt='[LLAMAR]' title='Llamar al número' />
				</a>".$c['dg_movil'];
			}
			
			if(is_numeric($c['dg_fono'])){
				$query_data = http_build_query(array(
					'call' => $c['dg_fono'],
					'ext' => $anexo,
					'razon' => $data['dg_razon']
				));
				
				$c['dg_fono'] = "<a target='callframe' class='right' href='http://192.168.0.202/phone/asterisk_call_trigger.php?{$query_data}'>
					<img src='images/call.png' alt='[LLAMAR]' title='Llamar al número' />
				</a>".$c['dg_fono'];
			}
		}
		
		echo("<tr>
			<td><a href='sites/clientes/ed_contacto.php?id={$c['dc_contacto']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='editar' />
			</a></td>
			<td><b>{$c['dg_contacto']}</b></td>
			<td>{$c['dg_fono']}</td>
			<td>{$c['dg_movil']}</td>
			<td>{$c['dg_fax']}</td>
			<td>{$c['dg_email']}</td>
			<td>{$c['dg_cargo_contacto']}</td>
		</tr>");
	}
	echo('</tbody></table>');
}

	echo("</div>
</div>");
}

echo('</div>
<iframe name="callframe" class="hidden" />');
?>
<script type="text/javascript" src="jscripts/clientes/src_cliente.js?v0_4"></script>
<script type="text/javascript">
	js_data.cli_rut = '<?=$_POST['cli_rut'] ?>';
</script>
