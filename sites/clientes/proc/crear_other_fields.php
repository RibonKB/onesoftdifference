<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['of_codigo'] = str_replace(' ','',$_POST['of_codigo']);

if(count($db->select('tb_campo_personalizado','1',"dg_codigo = 'dc_empresa={$empresa} AND {$_POST['of_codigo']}'"))){
	$error_man->showWarning('El código ya está siendo utilizado por otro campo personalizado utilice otro');
	exit;
}

$db->start_transaction();

$campo = $db->insert('tb_campo_personalizado',array(
	'dg_campo' => $_POST['of_name'],
	'dg_codigo' => $_POST['of_codigo'],
	'dg_tipo' => $_POST['of_tipo'],
	'dc_empresa' => $empresa,
	'dc_tipo_objetivo' => 1
));

if($_POST['of_tipo'] != 'text'){
	$_POST['of_values'] = array_unique(explode("\n",htmlentities($_POST['of_values'],ENT_QUOTES,'UTF-8')));
	
	foreach($_POST['of_values'] as $i => $v){
		$db->insert('tb_campo_personalizado_valor',array(
			'dg_valor' => htmlentities($v, ENT_QUOTES),
			'dc_orden' => $i,
			'dc_campo' => $campo
		));
	}
}

$db->commit();

$error_man->showConfirm('Se ha creado el campo personalizado con éxito');
?>
<script type="text/javascript">
	actualizar_base();
</script>