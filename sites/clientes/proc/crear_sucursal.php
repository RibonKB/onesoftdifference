<?php
define("MAIN",1);
require_once("../../../inc/init.php");

require_once("crear_horario_atencion.php");
limpiar_horarios_repetidos();
validar_horario_atencion();

$db->start_transaction();

$sucursal = $db->prepare($db->insert('tb_cliente_sucursal',array(
	'dg_sucursal' => '?',
	'dg_direccion' => '?',
	'dg_fono' => '?',
	'dg_fax' => '?',
	'dg_codigo_postal' => '?',
	'dc_cliente' => '?',
	'df_creacion' => $db->getNow(),
	'dc_comuna' => '?'
)));
$sucursal->bindValue(1,$_POST['cli_suc_name'],PDO::PARAM_STR);
$sucursal->bindValue(2,$_POST['cli_suc_direccion'],PDO::PARAM_STR);
$sucursal->bindValue(3,$_POST['cli_suc_fono'],PDO::PARAM_STR);
$sucursal->bindValue(4,$_POST['cli_suc_fax'],PDO::PARAM_STR);
$sucursal->bindValue(5,$_POST['cli_suc_postal'],PDO::PARAM_STR);
$sucursal->bindValue(6,$_POST['cli_id'],PDO::PARAM_INT);
$sucursal->bindValue(7,$_POST['cli_suc_comuna'],PDO::PARAM_INT);
$db->stExec($sucursal);

$dc_sucursal = $db->lastInsertId();

if(isset($_POST['cli_suc_tipo'])){
	
	$insert_tipo = $db->prepare($db->insert('tb_cliente_sucursal_tipo',array(
		'dc_sucursal' => $dc_sucursal,
		'dc_tipo_direccion' => '?'
	)));
	$insert_tipo->bindParam(1,$dc_tipo,PDO::PARAM_INT);
	
	foreach($_POST['cli_suc_tipo'] as $dc_tipo){
		$db->stExec($insert_tipo);
	}
}

insertar_horarios_atencion();

$db->commit();

$error_man->showConfirm('Se ha creado la sucursal correctamente');
?>
<script type="text/javascript">
js_data.refreshClient();
</script>