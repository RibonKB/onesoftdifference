<?php
define('MAIN',1);
require_once("../../../inc/init.php");

$dc_campo = intval($_POST['dc_campo_ed']);

$datos = $db->prepare($db->select('tb_campo_personalizado','dm_activo','dc_empresa = ? AND dc_campo = ?'));
$datos->bindValue(1,$empresa,PDO::PARAM_INT);
$datos->bindValue(2,$dc_campo,PDO::PARAM_INT); 
$db->stExec($datos);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_campo_personalizado',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_campo =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_campo,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamento'); 


?>
<script type="text/javascript">
	actualizar_base();
</script>