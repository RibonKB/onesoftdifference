<?php
/**
*	Contiene el formulario completo para agregar un nuevo contacto.
*	se importa [ prioritariamente usando la funcion javascript loadFile() ] mandando como parámetro POST ( $_POST['indice'] ) el identificador sufijo para la identificación de los campos.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

?>
<div class="info">
<div class="right">
	<img src="images/ocultar.png" alt="ocultar" id="hidd_<?=$_POST['indice'] ?>" class="tip" onClick="hidContacto('<?=$_POST['target'] ?>',<?=$_POST['indice'] ?>)" />
	<div class="tooltip">Mostrar/Ocultar</div>
	<img src="images/cerrar.png" alt="ocultar" class="tip" onClick="delContacto('<?=$_POST['target'] ?>',<?=$_POST['indice'] ?>)" />
	<div class="tooltip">Eliminar el contacto</div>
</div>
Contacto número <?=$_POST['indice'] ?>
</div>
<div class="right">
Contacto Default: <input type="radio" name="cont_default" value="<?=$_POST['indice'] ?>" />
</div>
<br class="clear" />
<div class="contacto_cont">
<?php
if(isset($_POST['il'])){
	//En caso de que sea llamado por la creación individual de contactos desde la consulta de clientes
	//se agrega el tag de formulario para permitir inserciones individuales.
	echo("<form action='sites/clientes/proc/addcontactoind.php' class='validar' id='c_{$_POST['indice']}' >");
}

	include("../../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Section();
	$form->Text("Nombre contacto","contacto[]",1,99);
	$form->Listado("Cargo del contacto","cargo[]","tb_cargo_contacto",array("dc_cargo_contacto","dg_cargo_contacto"),1,'',"dc_cargo_contacto > 0 AND dc_empresa=$empresa");
	$form->Text("Dirección","dir[]",1,99);
	
	$regiones = $db->select("tb_region");
	echo("<label>Region<br />
		<select onchange='showComunas(\"comuna_{$_POST['indice']}\",this)' class='inputtext'>
		<option></option>
		");
	foreach($regiones as $r){
		echo("<option value='{$r['dc_region']}'>{$r['dg_region']}</option>");
	}
	echo("</select></label><br />
	<label>Comuna[*]<br />
	<select name='comuna[]' id='comuna_{$_POST['indice']}' class='inputtext' required='required'>
		<option></option>
	</select>
	</label>
	<span id='comuna_{$_POST['indice']}_comloader'></span>
	<br />
	");
	
	$form->Textarea("Comentario","comm[]");
	$form->EndSection();
	$form->Section();
	$form->Text("Teléfono","tfono[]",1,99);
	$form->Text("Correo electrónico","email[]",0,99);
	$form->Text("Fax","fax[]",0,99);
	$form->Text("Horario de atención","horario[]");
	$aux = $db->select("tb_tipo_direccion","dc_tipo_direccion,dg_tipo_direccion","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by"=>"dg_tipo_direccion"));
	$tcontactos = array();
	foreach($aux as $t){
		$tcontactos[$t['dc_tipo_direccion']] = $t['dg_tipo_direccion'];
	}
	$form->Combobox("Tipo de contacto","tipo{$_POST['indice']}",$tcontactos);
	$form->EndSection();
	$form->Hidden("indice[]",$_POST['indice']);

//Se agrega el botón para realizar la inserción individual del contacto.
if(isset($_POST['il'])){
	echo("<div class='left' style='width:600px;margin:auto;'>
	<div class='center'>");
	$form->Button("Agregar contacto",'','addbutton','submit');
	$form->EndSection();
	$form->EndSection();
	$form->Hidden("ind",$_POST['indice']);
	$form->Hidden("cli",$_POST['il']);
	echo("</form>");
?>
	<script type="text/javascript">
	$(".validar").submit(function(e){
	e.preventDefault();
	enviarForm('#c_<?=$_POST['indice'] ?>','#c_<?=$_POST['indice'] ?>_res');
	});
	</script>
<?php
}
?>
</div>
<script type="text/javascript">
$(".tip").tooltip({position:'bottom center',effect:'slide'});
</script>