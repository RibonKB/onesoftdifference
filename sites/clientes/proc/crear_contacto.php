<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->insert('tb_contacto_cliente',array(
	'dg_contacto' => $_POST['cli_cont_name'],
	'dg_fono' => $_POST['cli_cont_fono'],
	'dg_fax' => $_POST['cli_cont_fax'],
	'dg_movil' => $_POST['cli_cont_movil'],
	'dg_email' => $_POST['cli_cont_mail'],
	'dc_cargo_contacto' => $_POST['cli_cont_cargo'],
	'dc_cliente' => $_POST['cli_id'],
	'dc_sucursal' => $_POST['cli_cont_sucursal']
));

$error_man->showConfirm('Se ha creado el contacto correctamente y se a asignado a la sucursal');

?>
<script type="text/javascript">
	js_data.refreshClient();
</script>