<?php
define('MAIN',1);
require_once("../../../inc/init.php");

$dc_campo = intval($_POST['dc_campo_ed']);
$dg_campo = $_POST['dg_campo_ed'];
$dg_codigo = ($_POST['dg_codigo_ed']);
$dg_tipo = $_POST['dg_tipo_ed'];
$dg_valor = $_POST['dg_valor_ed'];


$datos = $db->prepare($db->select('tb_campo_personalizado','true','dc_empresa = ? AND dg_campo = ? AND dc_campo = ?'));
$datos->bindValue(1,$empresa,PDO::PARAM_INT);
$datos->bindValue(2,$dg_campo,PDO::PARAM_STR);
$datos->bindValue(3,$dc_campo,PDO::PARAM_INT);
$db->stExec($datos);

$datoscampo = $db->prepare($db->select('tb_campo_personalizado_valor','true','dc_campo = ?'));
$datoscampo->bindValue(1,$dc_campo,PDO::PARAM_INT);
$db->stExec($datoscampo);



/*if($datos->fetch() !== false){
	$error_man->showWarning("Ya existe ese nombre");
	exit; 
}*/

$db->start_transaction();

$editar = $db->prepare($db->update('tb_campo_personalizado',array(
				'dg_campo' => '?',
				'dg_codigo' => '?',
				'dg_tipo' => '?'),
				'dc_campo = ? AND dc_empresa = ?'));
$editar->bindValue(1,$dg_campo,PDO::PARAM_STR);
$editar->bindValue(2,$dg_codigo,PDO::PARAM_STR);
$editar->bindvalue(3,$dg_tipo,PDO::PARAM_STR);
$editar->bindValue(4,$dc_campo,PDO::PARAM_INT);
$editar->bindValue(5,$empresa,PDO::PARAM_INT);
$db->stExec($editar);

$eliminar = $db->prepare('DELETE FROM tb_campo_personalizado_valor WHERE dc_campo = ?');
$eliminar->bindValue(1,$dc_campo,PDO::PARAM_INT);
$db->stExec($eliminar);



if($dg_tipo == 'select' || $dg_tipo == 'multiple'){
	$dg_valor = array_unique(explode("\n",htmlentities($dg_valor,ENT_QUOTES,'UTF-8')));
	
	foreach($dg_valor as $i => $v){
		$inser = $db->prepare($db->insert('tb_campo_personalizado_valor',array(
			'dg_valor' => '?',
			'dc_campo' => '?',
			'dc_orden' => $i
		)));
		$inser->bindValue(1,$v,PDO::PARAM_STR);
		$inser->bindValue(2,$dc_campo,PDO::PARAM_INT);
		$db->stExec($inser);
	}
}

/*$insertar = $db->prepare($db->insert('tb_campo_personalizado_valor',array(
						'dg_valor' => '?'),
						'dc_campo = ?'));
$insertar->bindValue(1,$dc_campo,PDO::PARAM_INT);
$db->stExec($insertar);

*/
$db->commit();


$error_man->showConfirm('Se ha modificado correctamente'); 

?>
<script type="text/javascript">
	actualizar_base();
</script>