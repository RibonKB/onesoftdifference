<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_sucursal = intval($_POST['dc_sucursal']);

require_once('crear_horario_atencion.php');

limpiar_horarios_repetidos();
validar_horario_atencion();

$db->start_transaction();

	$db->doExec("DELETE FROM tb_horario_atencion_sucursal WHERE dc_sucursal = {$dc_sucursal}");
	
	insertar_horarios_atencion();

$db->commit();

?>
<div class="secc_bar">
	Horarios de atención sucursal
</div>
<div class="panes">
	<?php $error_man->showConfirm('Se han asignado los horarios de atención a la sucursal correctamente') ?>
</div>