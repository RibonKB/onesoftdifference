<?php
define("MAIN",1);
require_once("../../../inc/init.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['dc_tipo_sucursal'])){
	$error_man->showWarning('Debe seleccionar un tipo de dirección para la sucursal');
	exit();
}

$dg_rut = $_POST['dg_rut'];
$cliente = $db->prepare($db->select('tb_cliente','1',"dg_rut=? AND dc_empresa = ?"));
$cliente->bindValue(1,$dg_rut,PDO::PARAM_STR);
$cliente->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($cliente);

if($cliente->fetch() !== false){
	$error_man->showWArning("El cliente con el rut <b>{$dg_rut}</b> ya a sido ingresado anteriormente, compruebe que sea el que quiere ingresar ahora");
	exit;
}

$dq_linea_credito = floatval($_POST['dq_linea_credito']);
if(!$dq_linea_credito){
	$error_man->showWarning("La linea de crédito contiene un valor no válido compruebe la información y vuelva a intentarlo.");
	exit;
}

require_once('crear_horario_atencion.php');
limpiar_horarios_repetidos();
validar_horario_atencion();

$db->start_transaction();

$cliente = $db->prepare($db->insert('tb_cliente',array(
	'dg_rut' => '?',
	'dc_ejecutivo' => '?',
	'dg_razon' => '?',
	'dg_giro' => '?',
	'dg_sitio_web' => '?',
	'dg_password' => '?',
	'dc_tipo_cliente' => '?',
	'dc_mercado' => '?',
	'dq_linea_credito' => '?',
	'dc_cuenta_contable' => '?',
	'dc_empresa' => $empresa,
	'dm_validar_nota_venta' => isset($_POST['cli_val_nv'])?1:0,
	'dm_confirmar_nota_venta' => isset($_POST['cli_conf_nv'])?1:0,
	'df_creacion' => $db->getNow()
)));
$cliente->bindValue(1,$dg_rut,PDO::PARAM_STR);
$cliente->bindValue(2,$_POST['dc_ejecutivo'],PDO::PARAM_INT);
$cliente->bindValue(3,$_POST['dg_razon'],PDO::PARAM_STR);
$cliente->bindValue(4,$_POST['dg_giro'],PDO::PARAM_STR);
$cliente->bindValue(5,$_POST['dg_sitioweb'],PDO::PARAM_STR);
$cliente->bindValue(6,md5($dg_rut),PDO::PARAM_STR);
$cliente->bindValue(7,$_POST['dc_tipo_cliente'],PDO::PARAM_INT);
$cliente->bindValue(8,$_POST['dc_mercado'],PDO::PARAM_INT);
$cliente->bindValue(9,floatval($_POST['dq_linea_credito']),PDO::PARAM_STR);
$cliente->bindValue(10,$_POST['dc_cuenta_contable'],PDO::PARAM_INT);
$db->stExec($cliente);

$dc_cliente = $db->lastInsertId();

if(isset($_POST['dc_medio_pago'])){
	
	$insert_medio_pago = $db->prepare(
		$db->insert('tb_cliente_medio_pago',array(
			'dc_cliente' => $dc_cliente,
			'dc_medio_pago' => '?'
		))
	);
	$insert_medio_pago->bindParam(1,$dc_medio_pago,PDO::PARAM_INT);
	
	foreach($_POST['dc_medio_pago'] as $dc_medio_pago){
		$db->stExec($insert_medio_pago);
	}
}

$sucursal = $db->prepare($db->insert('tb_cliente_sucursal',array(
	'dg_sucursal' => '?',
	'dg_direccion' => '?',
	'dg_fono' => '?',
	'dg_fax' => '?',
	'dg_codigo_postal' => '?',
	'dc_comuna' => '?',
	'dc_cliente' => $dc_cliente,
	'df_creacion' => $db->getNow()
)));
$sucursal->bindValue(1,$_POST['dg_sucursal'],PDO::PARAM_STR);
$sucursal->bindValue(2,$_POST['dg_direccion'],PDO::PARAM_STR);
$sucursal->bindValue(3,$_POST['dg_telefono_sucursal'],PDO::PARAM_STR);
$sucursal->bindValue(4,$_POST['dg_fax_sucursal'],PDO::PARAM_STR);
$sucursal->bindValue(5,$_POST['dg_postal'],PDO::PARAM_STR);
$sucursal->bindValue(6,$_POST['dc_comuna'],PDO::PARAM_INT);
$db->stExec($sucursal);

$dc_sucursal = $db->lastInsertId();

insertar_horarios_atencion();

$insert_tipo_sucursal = $db->prepare($db->insert('tb_cliente_sucursal_tipo',array(
		'dc_sucursal' => $dc_sucursal,
	 	'dc_tipo_direccion' => '?'
	)));
$insert_tipo_sucursal->bindParam(1,$dc_tipo,PDO::PARAM_INT);

foreach($_POST['dc_tipo_sucursal'] as $dc_tipo){
	$db->stExec($insert_tipo_sucursal);
}

$contacto = $db->prepare($db->insert('tb_contacto_cliente',array(
	'dg_contacto' => '?',
	'dg_fono' => '?',
	'dg_fax' => '?',
	'dg_movil' => '?',
	'dg_email' => '?',
	'dc_cargo_contacto' => '?',
	'dc_cliente' => $dc_cliente,
	'dc_sucursal' => $dc_sucursal
)));
$contacto->bindValue(1,$_POST['dg_contacto'],PDO::PARAM_STR);
$contacto->bindValue(2,$_POST['dg_telefono_contacto'],PDO::PARAM_STR);
$contacto->bindValue(3,$_POST['dg_fax_contacto'],PDO::PARAM_STR);
$contacto->bindValue(4,$_POST['dg_movil_contacto'],PDO::PARAM_STR);
$contacto->bindValue(5,$_POST['dg_email'],PDO::PARAM_STR);
$contacto->bindValue(6,$_POST['dc_cargo_contacto'],PDO::PARAM_INT);
$db->stExec($contacto);

$dc_contacto = $db->lastInsertId();

$db->doExec($db->update('tb_cliente',array(
	'dc_contacto_default' => $dc_contacto,
	'dc_casa_matriz' => $dc_sucursal
),"dc_cliente = ".$dc_cliente));

$db->commit();

$error_man->showConfirm("Se ha creado el cliente correctamente, pero no se encuentra validado para utilizarlo en los procesos de la empresa.");

?>
