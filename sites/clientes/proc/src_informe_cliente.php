<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$condition = "dm_activo = 1 AND dc_empresa = {$empresa}";

if(isset($_POST['dc_ejecutivo'])){
	$dc_ejecutivo = implode(',',$_POST['dc_ejecutivo']);
	$condition .= " AND dc_ejecutivo IN ({$dc_ejecutivo})";
}

if(isset($_POST['dc_tipo_cliente'])){
	$dc_tipo_cliente = implode(',',$_POST['dc_tipo_cliente']);
	$condition .= " AND dc_tipo_cliente IN ({$dc_tipo_cliente})";
}

if(isset($_POST['dc_mercado'])){
	$dc_mercado = implode(',',$_POST['dc_mercado']);
	$condition .= " AND dc_mercado IN ({$dc_mercado})";
}

if(isset($_POST['dc_region'])){
	$dc_region = implode(',',$_POST['dc_region']);
	$region_condition = "AND com.dc_region IN ({$dc_region})";
}else{
	$region_condition = '';
}

if($_POST['df_creacion_desde']){
	$_POST['df_creacion_desde'] = $db->sqlDate($_POST['df_creacion_desde']);
	if($_POST['df_creacion_hasta']){
		$_POST['df_creacion_hasta'] = $db->sqlDate($_POST['df_creacion_hasta']);
		$date_condition = "AND (df_creacion BETWEEN {$_POST['df_creacion_desde']} AND {$_POST['df_creacion_hasta']})";
	}else{
		$date_condition = "AND df_creacion = {$_POST['df_creacion_desde']}";
	}
}else{
	$date_condition = '';
}

$data = $db->doQuery($db->select("(SELECT * FROM tb_cliente WHERE {$condition} {$date_condition}) cl
JOIN tb_cliente_sucursal s ON s.dc_sucursal = cl.dc_casa_matriz
JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna {$region_condition}
JOIN tb_region r ON com.dc_region = r.dc_region
LEFT JOIN tb_funcionario f ON f.dc_funcionario = cl.dc_ejecutivo
LEFT JOIN tb_tipo_cliente tc ON tc.dc_tipo_cliente = cl.dc_tipo_cliente
LEFT JOIN tb_mercado m ON m.dc_mercado = cl.dc_mercado
LEFT JOIN (SELECT dc_cliente, MAX(df_fecha_emision) ultima_cotizacion FROM tb_cotizacion GROUP BY dc_cliente) cot ON cl.dc_cliente = cot.dc_cliente
LEFT JOIN (SELECT dc_cliente, MAX(df_fecha_emision) ultima_nota_venta FROM tb_nota_venta GROUP BY dc_cliente) nv ON cl.dc_cliente = nv.dc_cliente
LEFT JOIN (SELECT dc_cliente, MAX(df_emision) ultima_factura FROM tb_factura_venta GROUP BY dc_cliente) fv ON cl.dc_cliente = fv.dc_cliente",
'cl.dc_cliente, cl.dg_razon, cl.dg_rut, s.dg_sucursal, s.dg_direccion, com.dg_comuna, r.dg_region, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, tc.dg_tipo_cliente, m.dg_mercado,
cot.ultima_cotizacion, nv.ultima_nota_venta, fv.ultima_factura'));

if($data->rowCount() == 0){
	$error_man->showAviso("No se han encontrado clientes que cumplan con los criterios especificados en los filtros, intentelo denuevo");
	exit;
}

?>
<table class="tab" width="100%">
	<thead>
		<tr>
			<th>id</th>
			<th width="350">Cliente</th>
			<th width="100">Rut</th>
			<th>Sucursal</th>
			<th>Ejecutivo</th>
			<th>Tipo de cliente</th>
			<th>Mercado</th>
			<th>Fecha Última Cotización</th>
			<th>Fecha Última Venta</th>
			<th>Fecha Última Factura</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($data as $c): ?>
		<tr>
			<td><?php echo $c['dc_cliente'] ?></td>
			<td><?php echo $c['dg_razon'] ?></td>
			<td><?php echo $c['dg_rut'] ?></td>
			<td><b><?php echo $c['dg_direccion'] ?></b> <?php echo $c['dg_comuna'] ?> <?php $c['dg_region'] ?></td>
			<td><?php echo $c['dg_nombres'].' '.$c['dg_ap_paterno'].' '.$c['dg_ap_materno'] ?></td>
			<td><?php echo $c['dg_tipo_cliente'] ?></td>
			<td><?php echo $c['dg_mercado'] ?></td>
			<td><?php echo $db->dateTimeLocalFormat($c['ultima_cotizacion']) ?></td>
			<td><?php echo $db->dateTimeLocalFormat($c['ultima_nota_venta']) ?></td>
			<td><?php echo $db->dateTimeLocalFormat($c['ultima_factura']) ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
