<?php
/**
*	Muestra el formulario de validación de clientes permitiendo al usuario agregar nueva información o editar la existente.
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//faltarían los medios de pagos, se agregarían al estar listo este proceso.

$tables = "(SELECT * FROM tb_cliente WHERE dm_activo = '1' AND dg_rut = '{$_POST['cli_rut']}') cl
LEFT JOIN tb_tipo_cliente tc ON cl.dc_tipo_cliente = tc.dc_tipo_cliente";

$fields = "dc_cliente,dg_rut,dg_razon,dg_giro,dg_tipo_cliente";


//Obtiene los datos del cliente para rellenar el formulario con estos.
$datos = $db->select($tables,$fields);
unset($tables,$fields);

if(!count($datos)){
	$error_man->showErrorRedirect("El cliente especificado no se ha encontrado","sites/clientes/ed_cliente.php");
}

$datos = $datos[0];

?>

<div id="secc_bar">Validación de clientes</div>
<div id="other_options">
	<ul>
		<li><a href="sites/clientes/src_cliente.php?idCliente=<?=$datos['dc_cliente'] ?>" class="loader">Consultar</a></li>
		<li><a href="sites/clientes/cr_cliente.php" class="loader">Crear</a></li>
		<li><a href="sites/clientes/ed_cliente.php?idCliente=<?=$datos['dc_cliente'] ?>" class="loader">Editar</a></li>
		<li class="oo_active"><a href="sites/clientes/val_cliente.php" class="loader">Validar otro cliente</a></li>
	</ul>
</div>

<div id="main_cont">
	<div class="panes">
	<fieldset>
		<div class="title">Razón social: <?=$datos['dg_razon'] ?></div>
		<div class="left"><br />
			<label>RUT: </label><?=$datos['dg_rut'] ?><br />
			<label>Giro: </label><?=$datos['dg_giro'] ?><br />
			<label>Tipo de cliente: </label><?=$datos['dg_tipo_cliente'] ?><br />
		</div><br class="clear" /><br />
		<hr />
		<div class="center">
		¿Está seguro que quiere validar el cliente seleccionado?
		<div id="cli_validar_res"></div>
		<form action="sites/clientes/proc/validar_cliente.php" class="validar" id="cli_validar">
			<input type="hidden" name="id_cli" value="<?=$datos['dc_cliente'] ?>" />
			<input type="submit" class="editbtn" value="Validar" />
		</form>
		</div>
	</fieldset>
	</div>
</div>