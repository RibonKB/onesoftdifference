<?php

function limpiar_horarios_repetidos(){
	$_POST['horario_atencion'] = array_unique($_POST['horario_atencion']);
}

function validar_horario_atencion(){
	if(isset($_POST['horario_atencion'])){
		foreach($_POST['horario_atencion'] as $horario){
			if(!preg_match('/^[0-9]\|[0-9]{1,2}:[0-9]{2}\|[0-9]{1,2}:[0-9]{2}$/', $horario)){
				$error_man->showWarning('No se pudieron procesar los horarios de atención, compruebe los datos de entrada y vuelva a intentarlo.');
				exit;
			}
		}
	}
}

function insertar_horarios_atencion(){
	
	global $dc_sucursal, $db;
	
	if(!isset($_POST['horario_atencion'])){
		return;
	}
	
	$insert = $db->prepare($db->insert('tb_horario_atencion_sucursal',array(
		'dc_sucursal' => $dc_sucursal,
		'dc_dia' => '?',
		'dt_hora_inicio' => '?',
		'dt_hora_termino' => '?'
	)));
	$insert->bindParam(1,$dc_dia,PDO::PARAM_INT);
	$insert->bindParam(2,$dt_hora_inicio,PDO::PARAM_STR);
	$insert->bindParam(3,$dt_hora_termino,PDO::PARAM_STR);
	
	foreach($_POST['horario_atencion'] as $horario){
		$horario = explode('|',$horario);
		$dc_dia = intval($horario[0]);
		$dt_hora_inicio = str_pad($horario[1], 5, 0, STR_PAD_LEFT);
		$dt_hora_termino = str_pad($horario[2], 5, 0, STR_PAD_LEFT);
		
		$db->stExec($insert);
	}
	
}
?>