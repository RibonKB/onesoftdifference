<?php
define("MAIN",1);
require_once("../../inc/init.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_cliente = intval($_POST['id']);

//Montos facturados agrupados por año
$facturas_anho = $db->prepare($db->select('tb_factura_venta fv
JOIN tb_factura_venta_detalle d ON d.dc_factura = fv.dc_factura',
				'YEAR(fv.df_emision) dq_anho, SUM(d.dq_precio*d.dc_cantidad) dq_monto',
				'dc_cliente = ? AND dm_nula = 0',
				array('group_by' => 'dq_anho', 'order_by' => 'dq_anho DESC')));
$facturas_anho->bindValue(1,$dc_cliente,PDO::PARAM_INT);
$db->stExec($facturas_anho);

$total_anual = 0;

//montos facturados agrupados por linea de negocio
$facturas_linea_negocio = $db->prepare($db->select('tb_factura_venta fv
JOIN tb_factura_venta_detalle d ON d.dc_factura = fv.dc_factura
JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = ?
JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio',
'ln.dg_linea_negocio, SUM(d.dq_precio*d.dc_cantidad) dq_monto','fv.dc_cliente = ? AND fv.dm_nula = 0',
array('group_by' => 'ln.dc_linea_negocio')));
$facturas_linea_negocio->bindValue(1,$empresa,PDO::PARAM_INT);
$facturas_linea_negocio->bindValue(2,$dc_cliente,PDO::PARAM_INT);
$db->stExec($facturas_linea_negocio);

$total_linea = 0;

//Comportamiento de pago
$cliente_data = $db->prepare($db->select('tb_cliente','dc_dias_pago,dc_facturas_pagadas,dq_linea_credito','dc_cliente = ?'));
$cliente_data->bindValue(1,$dc_cliente,PDO::PARAM_INT);
$db->stExec($cliente_data);
$cliente_data = $cliente_data->fetch(PDO::FETCH_OBJ);
if($cliente_data->dc_facturas_pagadas != 0){
	$cliente_data->dc_dias_promedio = $cliente_data->dc_dias_pago/$cliente_data->dc_facturas_pagadas;
	if($cliente_data->dc_dias_promedio < 120)
		$cliente_data->dc_bar_position = intval(($cliente_data->dc_dias_promedio*300)/120);
	else
		$cliente_data->dc_bar_position = 300;
}else{
	$cliente_data->dc_dias_promedio = 'N/A';
}

//Cantidad de Facturas vigentes
$facturas_vigentes = $db->prepare($db->select('tb_factura_venta','count(dc_factura) dq_cantidad','dc_cliente = ? AND dm_nula = 0'));
$facturas_vigentes->bindValue(1,$dc_cliente,PDO::PARAM_INT);
$db->stExec($facturas_vigentes);
$facturas_vigentes = $facturas_vigentes->fetch(PDO::FETCH_OBJ)->dq_cantidad;

//Cantidad de Notas de crédito
$notas_vigentes = $db->prepare($db->select('tb_nota_credito','count(dc_nota_credito) dq_cantidad','dc_cliente = ? AND dm_nula = 0'));
$notas_vigentes->bindValue(1,$dc_cliente,PDO::PARAM_INT);
$db->stExec($notas_vigentes);
$notas_vigentes = $notas_vigentes->fetch(PDO::FETCH_OBJ)->dq_cantidad;

//Monto pendiente de pago de facturas
$pendiente_pago = $db->prepare($db->select('tb_factura_venta',
			'SUM(dq_neto-dq_monto_pagado) dq_monto,
			 SUM(DATEDIFF(NOW(),df_vencimiento)) dc_dias_vencimiento,
			 COUNT(dc_factura) dc_cantidad_facturas',
			'dc_cliente = ? AND dm_nula = 0 AND dq_neto > dq_monto_pagado'));
$pendiente_pago->bindValue(1,$dc_cliente,PDO::PARAM_INT);
$db->stExec($pendiente_pago);
$pendiente_pago = $pendiente_pago->fetch(PDO::FETCH_OBJ);
if($pendiente_pago->dc_cantidad_facturas){
	$dias_promedio_pagar = intval($pendiente_pago->dc_dias_vencimiento/$pendiente_pago->dc_cantidad_facturas);
}else{
	$dias_promedio_pagar = 'N/A';
}
$pendiente_pago = $pendiente_pago->dq_monto;

//Saldo disponible linea crédito
$saldo_disponible = $cliente_data->dq_linea_credito - $pendiente_pago;

?>
<div class="secc_bar">Análisis de cliente</div>
<div class="panes">
<br />
	<div class="left">
	<!-- montos facturados agrupados por año -->
	<table class="tab" width="220" id="factura_anual">
	<caption>Facturación por año</caption>
		<thead><tr>
				<th>Año</th>
				<th>Monto</th>
		</tr></thead>
		<tbody><?php while($anual = $facturas_anho->fetch(PDO::FETCH_OBJ)): $total_anual += $anual->dq_monto; ?>
			<tr>
				<td><?php echo $anual->dq_anho ?></td>
				<td align="right"><?php echo moneda_local($anual->dq_monto) ?></td>
			</tr>
		<?php endwhile; ?></tbody>
		<tfoot><tr>
			<th align="right">Total:</th>
			<th align="right"><?php echo moneda_local($total_anual) ?></th>
		</tr></tfoot>
	</table>
	<br />
	<!-- Montos facturados agrupados por linea de negocio -->
	<table class="tab" width="220" id="factura_linea_negocio">
	<caption>Facturación por linea de negocio</caption>
		<thead><tr>
			<th>Linea Negocio</th>
			<th>Monto</th>
		</tr></thead>
		<tbody><?php while($linea = $facturas_linea_negocio->fetch(PDO::FETCH_OBJ)): $total_linea += $linea->dq_monto; ?>
			<tr>
				<td><?php echo $linea->dg_linea_negocio ?></td>
				<td align="right"><?php echo moneda_local($linea->dq_monto) ?></td>
			</tr>
		<?php endwhile; ?></tbody>
		<tfoot><tr>
			<th align="right">Total:</th>
			<th align="right"><?php echo moneda_local($total_linea) ?></th>
		</tr></tfoot>
	</table>
	</div>
	
	<div class="left">
	<?php if($cliente_data->dc_dias_promedio != 'N/A'): ?>
	<table class="tab" width="90%" style="margin-left:30px;">
	<caption>Comportamiento de pago</caption>
	<tbody>
	<tr>
		<td width="320" align="center">
			<div style="width:320px;height:50px;background:url(images/level-bar.png);position:relative;">
				<img src="images/level-indicator-level.png" align="[LVL]" style="position:absolute;top:6px;left:<?php echo $cliente_data->dc_bar_position ?>px;" />
			</div>
		</td>
	</tr>
	<tr>
		<td align="right">
			Días de pago promedio: <strong><?php echo $cliente_data->dc_dias_promedio; ?></strong>
		</td>
	</tr>
	</tbody></table>
	<?php else: ?>
		<div class="info" style="margin-left:30px;">Sin información de Pagos</div>
	<?php endif; ?>
	<br />
	
	<table class="tab" width="90%" style="margin-left:30px;">
		<caption>Datos extra de faturación</caption>
		<tbody>
			<tr>
				<td>Cantidad facturas vigentes</td>
				<td align="right"><?php echo $facturas_vigentes ?></td>
			</tr>
			<tr>
				<td>Cantidad nota de crédito vigentes</td>
				<td align="right"><?php echo $notas_vigentes ?></td>
			</tr>
			<tr>
				<td>Monto pendiente pago</td>
				<td align="right"><?php echo moneda_local($pendiente_pago)  ?></td>
			</tr>
			<tr>
				<td>Días promedio por pagar<br />
				<small>(Números positivos índican días retrasados y negativos días por pagar)</small></td>
				<td align="right"><?php echo $dias_promedio_pagar ?></td>
			</tr>
			<tr>
				<td>Linea de crédito actual</td>
				<td align="right"><?php echo moneda_local($cliente_data->dq_linea_credito) ?></td>
			</tr>
			<tr>
				<td>Saldo disponible línea de crédito</td>
				<td align="right">
				<?php if($saldo_disponible < 0): ?>
					<span style="color:red;"><?php echo moneda_local($saldo_disponible) ?></span>
				<?php else: ?>
					<?php echo moneda_local($saldo_disponible) ?>
				<?php endif; ?>
				</td>
			</tr>
		</tbody>
	</table>
	
	</div>
	
<br class="clear" />
</div>
<script type="text/javascript">
window.setTimeout(function(){
	$("#factura_anual").tableAdjust(5);
	$("#factura_linea_negocio").tableAdjust(5);
},100);
</script>