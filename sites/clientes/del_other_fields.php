<?php
define("MAIN",1);
require_once("../../inc/init.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$dc_campo = intval($_POST['id']);

$datos = $db->prepare($db->select('tb_campo_personalizado',
	'dc_campo, dg_campo, dg_campo','dc_campo = ? AND dc_empresa = ?'));
$datos->bindValue(1,$dc_campo,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('El campo personalizado no fue encontrado');
	exit;
}


?>


<div class="secc_bar" >
	Eliminar campo personalizado
</div>

<div class="panes" >
	<?php $form->Start('sites/clientes/proc/delete_other_fields.php', 'del_other_fields') ?>
    <?php $error_man->showAviso("¿Está seguro que desea eliminar el campo perzonalizado? <strong>{$datos->dg_campo}</strong>") ?>

    <?php $form->Hidden('dc_campo_ed', $dc_campo)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>
