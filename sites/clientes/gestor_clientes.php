<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">
	Gesti&oacute;n de clientes
</div>
<div id="main_cont">

<br />
<br />
<div align="center" class="opciones">
	<a href="sites/clientes/src_cliente.php" class="loader">
		<img src="images/submenu/consultaclientes.jpg" alt="Consulta Clientes" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" />
		<div class="tooltip">Consulta</div>
	</a>
	
	<a href="sites/clientes/cr_cliente.php" class="loader">
		<img src="images/submenu/creacionclientes.jpg" alt="Creaci&oacute;n Clientes" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" />
		<div class="tooltip">Creaci&oacute;n</div>
	</a>
	
	<a href="sites/clientes/ed_cliente.php" class="loader">
		<img src="images/submenu/edicionclientes.jpg" alt="Edici&oacute;n Clientes" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" />
		<div class="tooltip">Edici&oacute;n</div>
	</a>
	
	<a href="sites/clientes/val_cliente.php" class="loader">
		<img src="images/submenu/validacionclientes.jpg" alt="Validaci&oacute;n Clientes" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" />
		<div class="tooltip">Validaci&oacute;n</div>
	</a>
</div>

</div>