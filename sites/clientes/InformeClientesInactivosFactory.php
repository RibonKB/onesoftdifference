<?php
class InformeClientesInactivosFactory extends Factory{

	protected $title = "Informe de clientes inactivos";
	const PERIODO_DEFAULT = 3;
	const FILTRAR_POR_COTIZACION = 0;
	const FILTRAR_POR_LLAMADOS = 1;
	private $clientes = array();
	private $fechaObjetivo;
	
	private $local_prefix = array('9','92','922');
	private $mobile_prefix = array('909','99');
	private $minDuration = 30;
	
	public function indexAction(){
		$form = $this->getFormView($this->getTemplateUrl('index.form'));
		echo $this->getFullView($form,array(),Factory::STRING_TEMPLATE);
	}
	
	public function verInformeAction(){
		$this->validarFiltros();
		
		$this->fechaObjetivo = $this->getFechaObjetivo();
		$this->clientes = $this->getBaseClientes();
		$this->filtrarClientesCotizacion();
		$this->filtrarClientesLlamados();
		
		if(!count($this->clientes)):
			$this->getErrorMan()->showConfirm("No existen clientes inactivos con los parámetros ingresados");
			exit;
		endif;
		
		echo $this->getView($this->getTemplateUrl('result'),array(
			'clientes' => $this->clientes
		));
		
	}
	
	private function getFechaObjetivo(){
		$r = self::getRequest();
		$date = new DateTime();
		$date->sub(new DateInterval('P'.intval($r->dq_periodo).'M'));
		return $date->getTimestamp();
	}
	
	private function getBaseClientes(){
		$r = self::getRequest();
		if(isset($r->dc_ejecutivo)):
			return $this->getEjecutivosClientes();
		else:
			if(isset($r->dm_sin_ejecutivo)):
				return $this->getSinEjecutivosClientes();
			else:
				return $this->getTodosClientes();
			endif;
		endif;
	}
	
	private function getEjecutivosClientes(){
		$r = self::getRequest();
		$db = $this->getConnection();
		$cantidadEjecutivos = count($r->dc_ejecutivo);
		$questions = substr(str_repeat(',?',$cantidadEjecutivos),1);
		
		$clientes = $db->prepare(
							$db->select('tb_cliente cl'.
										' JOIN tb_funcionario f ON f.dc_funcionario = cl.dc_ejecutivo',
										'cl.dc_cliente, cl.dg_razon, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, f.dg_anexo',
										"cl.dc_ejecutivo IN ({$questions}) AND cl.dc_empresa = ? AND cl.dm_activo"));
		
		foreach($r->dc_ejecutivo as $i => $dc_ejecutivo):
			$clientes->bindValue($i+1,$dc_ejecutivo,PDO::PARAM_INT);
		endforeach;
		
		$clientes->bindValue($cantidadEjecutivos+1,$this->getEmpresa(),PDO::PARAM_INT);
		
		$db->stExec($clientes);
		
		return $clientes->fetchAll(PDO::FETCH_OBJ);
	}
	
	private function getSinEjecutivosClientes(){
		$db = $this->getConnection();
		
		$clientes = $db->prepare(
						$db->select('tb_cliente cl','cl.dc_cliente, cl.dg_razon','cl.dc_empresa = ? AND dm_activo = 1 AND dc_ejecutivo = 0'));
		$clientes->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($clientes);
		
		return $clientes->fetchAll(PDO::FETCH_OBJ);
		
	}
	
	private function getTodosClientes(){
		$db = $this->getConnection();
		
		$clientes = $db->prepare(
						$db->select('tb_cliente cl'.
									' LEFT JOIN tb_funcionario f ON f.dc_funcionario = cl.dc_ejecutivo',
									'cl.dc_cliente, cl.dg_razon, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, f.dg_anexo',
									'cl.dc_empresa = ? AND cl.dm_activo = 1'));
		$clientes->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$db->stExec($clientes);
		
		return $clientes->fetchAll(PDO::FETCH_OBJ);
	}
	
	private function validarFiltros(){
		$r = self::getRequest();
		if(!isset($r->dq_periodo) or empty($r->dq_periodo) or !intval($r->dq_periodo) or intval($r->dq_periodo) < 0){
			$this->getErrorMan()->showWarning('El <b>periodo</b> debe ser un número válido superior a cero');
			exit;
		}
		
		if(!isset($r->dc_parametros_seleccion)){
			$this->getErrorMan()->showWarning('Debe indicar al menos uno de los <b>parámetros de selección</b>');
			exit;
		}
		
		if(isset($r->dm_sin_ejecutivo) && isset($r->dc_ejecutivo)):
			$this->getErrorMan()->showAviso("Si selecciona un ejecutivo del listado se ignora la opción de selección de clientes sin ejecutivo");
		endif;
		
	}
	
	private function filtrarClientesCotizacion(){
		$r = self::getRequest();
		$todo = in_array(self::FILTRAR_POR_COTIZACION,$r->dc_parametros_seleccion);
		
		foreach($this->clientes as $i => &$cliente):
			$cliente->cotizacion =  $this->getUltimaCotizacion($cliente);
			if($todo):
				if($cliente->cotizacion != false and ($cliente->cotizacion->df_fecha_emision->getTimestamp() - $this->fechaObjetivo) > 0):
					unset($this->clientes[$i]);
				endif;
			endif;
		endforeach;
		
	}
	
	private function filtrarClientesLlamados(){
		$r = self::getRequest();
		$todo = in_array(self::FILTRAR_POR_LLAMADOS,$r->dc_parametros_seleccion);
		
		foreach($this->clientes as $i => &$cliente):
			$cliente->ultimo_llamado = $this->getUltimoLLamado($cliente);
			if($todo):
				if($cliente->ultimo_llamado != false and ($cliente->ultimo_llamado->calldate->getTimestamp() - $this->fechaObjetivo) > 0):
					unset($this->clientes[$i]);
				endif;
			endif;
		endforeach;
		
	}
	
	private function getUltimaCotizacion($cliente){
		$db = $this->getConnection();
		$data = $db->prepare($db->select('tb_cotizacion','dq_cotizacion, df_fecha_emision','dc_cliente = ?',array(
			'order_by' => 'df_fecha_emision DESC',
			'limit' => '1'
		)));
		$data->bindValue(1,$cliente->dc_cliente,PDO::PARAM_INT);
		$db->stExec($data);
		$data = $data->fetch(PDO::FETCH_OBJ);
		
		if($data != false):
			$data->df_fecha_emision = new DateTime($data->df_fecha_emision);
		endif;
		
		return $data;
	}
	
	private function getUltimoLLamado($cliente){
		$db = $this->getConnection();
		$numeros = $this->getNumerosCliente($cliente);
		$like = substr(str_repeat(',?',count($numeros)),1);
		
		if(!count($numeros)){
			return false;
		}
		
		if(isset($cliente->dg_anexo) and $cliente->dg_anexo):
			$data = $db->prepare($db->select('cdr','dst, calldate, src',"src = ? AND billsec > {$this->minDuration} AND dst IN ({$like})",array(
				'order_by' => 'calldate DESC',
				'limit' => '1'
			)));
			$data->bindValue(1,$cliente->dg_anexo,PDO::PARAM_STR);
			$hop = 2;
		else:
			$data = $db->prepare($db->select('cdr','dst, calldate, src',"billsec > {$this->minDuration} AND dst IN ({$like})",array(
				'order_by' => 'calldate DESC',
				'limit' => '1'
			)));
			$hop = 1;
		endif;
		
		for($j = 0; $j < count($numeros); $j++):
			$data->bindValue($j+$hop,$numeros[$j],PDO::PARAM_STR);
		endfor;
		
		$db->stExec($data);
		
		$llamada = $data->fetch(PDO::FETCH_OBJ);
		
		if($llamada != false){
			$llamada->calldate = new DateTime($llamada->calldate);
		}
		
		return $llamada;
	}
	
	private function getNumerosCliente($cliente){
		$db = $this->getConnection();
		$contactos = $db->prepare($db->select('tb_contacto_cliente','dg_fono, dg_movil','dc_cliente = ? AND dm_activo = 1'));
		$contactos->bindValue(1,$cliente->dc_cliente,PDO::PARAM_INT);
		$db->stExec($contactos);
		
		$numeros = array();
		
		foreach($contactos->fetchAll(PDO::FETCH_OBJ) as $c):
			if($c->dg_fono):
				foreach($this->local_prefix as $p):
					$numeros[] = $p.$c->dg_fono;
				endforeach;
			endif;
			
			if($c->dg_movil):
				foreach($this->mobile_prefix as $p):
					$numeros[] = $p.$c->dg_movil;
				endforeach;
			endif;
		endforeach;
		
		return $numeros;
		
	}
	
}