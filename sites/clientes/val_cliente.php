<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">Validación de clientes</div>

<div id="other_options">

<ul>
	<li><a href="sites/clientes/src_cliente.php" class="loader">Consultar</a></li>
	<li><a href="sites/clientes/cr_cliente.php" class="loader">Crear</a></li>
	<li><a href="sites/clientes/ed_cliente.php" class="loader">Editar</a></li>
	<li class="oo_active"><a href="#">Validar</a></li>
</ul>

</div>

<div id="main_cont" align="center">
<div class="panes">
<?php
	include("../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Header("<strong>Indique el RUT del cliente a validar</strong><br />
	Los criterios de busqueda son el RUT, Razón social o giro del cliente");
	$form->Start("sites/clientes/proc/val_cliente.php","val_cliente","cValidar");
	$form->Text("RUT","cli_rut",1,20,'',"inputrut");
	$form->End("Validar");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[1]+" ("+row[0]+") "+row[2];
}
$("#cli_rut").autocomplete('sites/proc/autocompleter/cliente.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>

<?php
	//Comprueba si se ha realizado una busqueda de cliente
	if(isset($_POST['idCliente'])){
	$rut = $db->select("tb_cliente","dg_rut","dm_activo=1 AND dc_cliente={$_POST['idCliente']}");
	if(count($rut)){
?>
<script type="text/javascript">
loadpage("sites/clientes/proc/val_cliente.php?cli_rut=<?php echo($rut[0]['dg_rut']); ?>");
</script>
<?php
}}
?>