<?php
/**
*	<b>intro:</b> Utilizando el RUT de un cliente muestra toda la información de este.
*	El RUT puede ser ingresado directamente o utilizando el asistente de busqueda en Overlay.
**/
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Consulta de clientes</div><div id="main_cont"><br /><div class="panes">
<?php
	//Obtiene los tipos de dirección
	$aux = $db->select("tb_tipo_direccion","dg_tipo_direccion,dc_tipo_direccion","dc_empresa={$empresa} AND dm_activo = '1'");
	$tipos = array();
	$checked = array();
	foreach($aux as $t){
		$tipos[$t['dc_tipo_direccion']] = $t['dg_tipo_direccion'];
		$checked[] = $t['dc_tipo_direccion'];
	}
	unset($aux);

	include("../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Start("sites/clientes/proc/src_cliente.php","src_cliente");
	echo("<div class='center'>");
	$form->Header("<strong>Ingrese el RUT del cliente para mostrar su información</strong>");
	$form->Text("RUT","cli_rut",1,20,'',"inputrut");
	$form->Combobox('','tipo',$tipos,$checked,'');
	unset($tipos,$checked);
	echo("</div>");
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
$("#cli_rut").autocomplete('sites/proc/autocompleter/cliente.php',
{
formatItem: function(row){
	return row[1]+" ("+row[0]+") "+row[2];
},
minChars: 2,
width:300
}
);
</script>

<?php
	
	//Comprueba si se ha realizado una busqueda de cliente
	if(isset($_POST['idCliente'])){
	$rut = $db->select("tb_cliente","dg_rut","dm_activo=1 AND dc_cliente={$_POST['idCliente']}");
	if(count($rut)){
?>
<script type="text/javascript">
$("#frut").attr("value","<?php echo($rut[0]['dg_rut']); ?>");
completeEnviarForm("#busqueda_cli");
</script>
<?php
}}
?>