<?php
define("MAIN",1);
require_once("../../inc/init.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$dc_campo = intval($_POST['id']);

$datos = $db->prepare($db->select(
	"tb_campo_personalizado cp LEFT JOIN tb_campo_personalizado_valor cpv ON cpv.dc_campo = cp.dc_campo",
	"cp.dc_campo, cp.dg_campo, cp.dg_codigo, cp.dg_tipo, cp.dc_empresa, cp.dc_tipo_objetivo,
	 GROUP_CONCAT(cpv.dg_valor ORDER BY cpv.dc_orden SEPARATOR '\\n') dg_valor","cpv.dc_campo = ? AND dc_empresa = ?"));
$datos->bindValue(1,$dc_campo,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('No se encontro el campo personalizado');
	exit();
}

?>

<div class="secc_bar">  
	Editar base conocimiento 
</div>
<div class="panes">
<?php $form->Start('sites/clientes/proc/editar_other_fields.php', 'ed_other_fields') ?>
<?php $form->Header('<b>Ingrese el nombre para actualizar la base de conocimiento</b><br />Los campos marcados con [*] son obligatorios') ?>

		<?php $form->Section(); 
         		$form->Text('Nombre','dg_campo_ed',true,Form::DEFAULT_TEXT_LENGTH,$datos->dg_campo); 
				$form->Text('Código','dg_codigo_ed',true,Form::DEFAULT_TEXT_LENGTH,$datos->dg_codigo);   
        	$form->EndSection(); 	
		
		$form->Section();
			$form->Select('Tipo de campo','dg_tipo_ed',array(
			'text' => 'Texto',
			'select' => 'Selección',
			'date' => 'Fecha',
			'multiple' => 'Selección múltiple'),1,$datos->dg_tipo); 
		$form->EndSection();
		
		
		$form->Section();
		 	$form->Textarea('Valores posibles (solo select y multiselect)<br />separados por linea','dg_valor_ed',0,$datos->dg_valor);
		$form->EndSection();		
		?>
        
             
        <?php $form->Hidden('dc_campo_ed',$dc_campo) ?>
    <?php $form->End('Editar','editbtn') ?>
    
    
</div>
