<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_contacto_cliente','dg_contacto,dg_fono,dg_fax,dg_movil,dg_email,dc_cargo_contacto,dc_sucursal,dc_cliente',"dc_contacto={$_POST['id']}");

if(!count($data)){
	$error_man->showWarning('Contacto no encontrado');
	exit;
}
$data = $data[0];

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div class="secc_bar">Edición de contactos</div><div class="panes">');

$form->Start('sites/clientes/proc/editar_contacto.php','ed_contacto');
$form->Header('<b>Ingrese los datos para la edición del contacto</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();
$form->Listado('Sucursal','cli_cont_sucursal','tb_cliente_sucursal',array('dc_sucursal','dg_sucursal'),1,$data['dc_sucursal'],"dc_cliente={$data['dc_cliente']}");
$form->Text('Nombre contacto','cli_cont_name',1,255,$data['dg_contacto']);
$form->Text('Teléfono','cli_cont_fono',0,255,$data['dg_fono']);
$form->Text('Teléfono movil','cli_cont_movil',0,255,$data['dg_movil']);
$form->EndSection();

$form->Section();
$form->Text('E-Mail','cli_cont_mail',0,255,$data['dg_email']);
$form->Text('Fax','cli_cont_fax',0,255,$data['dg_fax']);
$form->Listado('Cargo contacto','cli_cont_cargo','tb_cargo_contacto',array('dc_cargo_contacto','dg_cargo_contacto'),0,$data['dc_cargo_contacto']);
$form->EndSection();

$form->Hidden('id_contacto',$_POST['id']);
$form->End('Crear','addbtn');

?>