<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$list = $db->select(
	"tb_campo_personalizado cp LEFT JOIN tb_campo_personalizado_valor cpv ON cpv.dc_campo = cp.dc_campo",
	"cp.dc_campo, cp.dg_campo, cp.dg_codigo, cp.dg_tipo, cp.dc_empresa, cp.dc_tipo_objetivo , cp.dm_activo,
	 GROUP_CONCAT(cpv.dg_valor ORDER BY cpv.dc_orden SEPARATOR '<br>') dg_valor","dm_activo = 1 AND dc_empresa = {$empresa}", 
	array("order_by" => "dg_campo","group_by" => "cp.dc_campo"));



echo('<div id="secc_bar">Base de conocimiento de clientes</div>
<div id="main_cont"><div class="panes">');

require_once("../../inc/form-class.php");

$form = new Form($empresa);


$form->Start('sites/clientes/proc/crear_other_fields.php','cr_other_field');
$form->Header('<b>Indique los datos para crear nuevos campos para la creación de clientes</b><br />Los campos marcados con [*] son obligatorios.');

$form->Section();
$form->Text('Nombre de campo personalizado','of_name',1);
$form->Text('Código','of_codigo',1);
$form->Select('Tipo de campo','of_tipo',array(
	'text' => 'Texto',
	'select' => 'Selección',
	'date' => 'Fecha',
	'multiple' => 'Selección múltiple'
),1);
$form->EndSection();

$form->Section();

$form->Textarea('Valores posibles (solo select y multiselect)<br />separados por linea','of_values',0);
$form->EndSection();

$form->End('Crear','addbtn');



?>

<table class="tab" width="100%">
	<caption>Tipos de nombre de cargos personalizados ya agregados</caption>
	<thead>
		<tr>
			<th>Tipo de nombre</th>
			<th>Código</th>
			<th>Tipo de campo</th>
			<th>Datos (Tipo de campo)</th>
            <th>Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	
	<?php foreach($list as $l): ?>
		<tr id='item{$l['dc_campo']}'>
			<td><?php echo $l['dg_campo'] ?></td>
			<td><?php echo $l['dg_codigo'] ?></td>
			<td><?php echo $l['dg_tipo']?></td>
            <td><?php echo $l['dg_valor'] ?></td>
			<td>
				<a href='sites/clientes/ed_other_fields.php?id=<?php echo $l['dc_campo'] ?>' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/clientes/del_other_fields.php?id=<?php echo $l['dc_campo'] ?>' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
	
	</tbody></table>



</div></div>
<script type="text/javascript">
$('#of_codigo').change(function(){
	$(this).val($(this).val().replace(/ /g,''));
});

$('#of_values').change(function(){
	if($('#of_tipo').val() == 'text'){
		var div = $('<div>').addClass('alert').attr('id','tempmsgalert')
		.html('Los valores posibles no son válidos en campos de texto.<br />no será almacenado.')
		.insertBefore(this);
		
		$(this).css({backgroundColor:'#FFFFBF'});
	}else{
		$('#tempmsgalert').remove();
		$(this).css({backgroundColor:'#FFF'});
	}
});

$('#of_tipo').change(function(){
	if($('#of_values').val() != '')
		$('#of_values').trigger('change');
});

var actualizar_base = function(){
	pymerp.loadPage('sites/clientes/src_other_fields.php');
}
</script>