<?php
require_once 'inc/PHPExcel.php';

class BaseConocimientoFactory extends Factory {
    
    private $clientes = array();
    private $campoPersonalizado = array();
    
    
    public function indexAction(){
        $this->clientes = $this->getClientes();
        $this->campoPersonalizado = $this->getCampoPersonalizado();
        $this->setDatosCliente();
        $this->excel = new PHPExcel();
        $this->setPropiedadesDocumentoExcel();
        $this->setCabecerasExcel();
        $this->setContenidoExcel();
        $this->setSizeCeldas();
        $this->lanzarExcel();
    }
    
    private function getClientes(){
        $db = $this->getConnection();
        $table = 'tb_cliente';
        $fields = 'dc_cliente, dg_rut, dg_razon, dg_giro, dc_contacto_default, dc_casa_matriz, dc_ejecutivo';
        $conditions = 'dc_empresa = :empresa AND dm_activo = 1 ORDER BY dg_razon';
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($st);
        return $st->fetchAll(PDO::FETCH_OBJ);
    }
    
    private function getCampoPersonalizado(){
        $db = $this->getConnection();
        $table = 'tb_campo_personalizado';
        $fields = 'dc_campo, dg_campo, dg_codigo';
        $conditions = 'dc_empresa = :empresa AND dm_activo = 1';
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($st);
        $cp = array();
        foreach($st->fetchAll(PDO::FETCH_OBJ) as $v){
            $cp[$v->dc_campo] = $v;
        }
        return $cp;
    }
    
    private function setdatosCliente(){
        foreach($this->clientes as $k => $v){
            $this->clientes[$k]->casaMatriz = $this->getCasaMatriz($v->dc_casa_matriz);
            $this->clientes[$k]->ejecutivo = $this->getEjecutivo($v->dc_ejecutivo);
            $this->clientes[$k]->contacto = $this->getContacto($v->dc_contacto_default);
            $this->clientes[$k]->camposPersonalizados = $this->getCamposPersonalizados($v->dc_cliente);
        }
    }
    
    private function getCasaMatriz($id){
        $empty = new stdClass();
        $empty->dc_sucursal = '';
        $empty->dg_direccion = '';
        $empty->dg_fono = '';
        if(empty($id)){
            return $empty;
        }
        $db = $this->getConnection();
        $row = $db->getRowById('tb_cliente_sucursal', $id, 'dc_sucursal', 'dc_sucursal, dg_direccion, dg_fono');
        if($row === false){
            return $empty;
        }
        return $row;
    }
    
    private function getEjecutivo($id){
        $empty = new stdClass();
        $empty->dc_functionario = '';
        $empty->dg_ejecutivo = '';
        if(empty($id)){
            return $empty;
        }
        $db = $this->getConnection();
        $row = $db->getRowById('tb_funcionario',$id,'dc_funcionario', 
                    "dc_funcionario, CONCAT(dg_nombres, ' ', dg_ap_paterno, ' ', dg_ap_materno) dg_ejecutivo");
        if($row === false){
            return $empty;
        }
        return $row;
    }
    
    private function getContacto($id){
        $empty = new stdClass();
        $empty->dc_contacto = '';
        $empty->dg_contacto = '';
        $empty->dg_fono = '';
        $empty->dg_email = '';
        if(empty($id)){
            return $empty;
        }
        $db = $this->getConnection();
        $row = $db->getRowById('tb_contacto_cliente',$id,'dc_contacto', 'dc_contacto, dg_contacto, dg_fono, dg_email');
        if($row === false){
            return $empty;
        }
        return $row;
    }
    
    private function getCamposPersonalizados($dc_cliente){
        $campoPersonalizadoCliente = $this->getCampoPersonalizadoCliente($dc_cliente);
        return $this->getMatrizCampoPersonalizadoCliente($campoPersonalizadoCliente);
    }
    
    private function getCampoPersonalizadoCliente($dc_cliente){
        $db = $this->getConnection();
        $st = $db->prepare($db->select('tb_cliente_campo_personalizado', 'dc_cliente, dc_campo, dg_valor', 'dc_cliente = :cliente'));
        $st->bindValue(':cliente', $dc_cliente, PDO::PARAM_INT);
        $db->stExec($st);
        return $st->fetchAll(PDO::FETCH_OBJ);
    }
    
    private function getMatrizCampoPersonalizadoCliente($cpc){
        
        $camposPersonalizados = array();
        $cpk = array_keys($this->campoPersonalizado);
        foreach($cpk as $v){
            $camposPersonalizados[$v] = '';
        }
        foreach($cpc as $v){
            if(isset($camposPersonalizados[$v->dc_campo])){
                $camposPersonalizados[$v->dc_campo] = $v->dg_valor;
            }
        }
        return $camposPersonalizados;
    }
    
    private function setPropiedadesDocumentoExcel(){
        $this->excel->getProperties()
                    ->setCreator("onesoft")
                    ->setLastModifiedBy("onesoft")
                    ->setTitle("Base Conocimiento")
                    ->setSubject("Base Conocimiento")
                    ->setDescription("Base de conocimiento de clientes")
                    ->setKeywords("Excel Office 2007 openxml php")
                    ->setCategory("Ventas");
    }
    
    private function setCabecerasExcel(){
        $columna = 'A';
        $fila = 1;
        $titulos = ['Empresa','Direccion Casa Matriz','Ejecutivo','Contacto','Telefono','E-mail'];
        foreach($titulos as $v){
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$v);
            $this->excel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
            $columna++;
        }
        
        foreach($this->campoPersonalizado as $v){
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$v->dg_campo);
            $this->excel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
            $columna++;
        }
    }
    
    private function setContenidoExcel(){
        $columna = 'A';
        $fila = 2;
        foreach($this->clientes as $cte){
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$cte->dg_razon);
            $columna++;
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$cte->casaMatriz->dg_direccion);
            $columna++;
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$cte->ejecutivo->dg_ejecutivo);
            $columna++;
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$cte->contacto->dg_contacto);
            $columna++;
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$cte->contacto->dg_fono);
            $columna++;
            $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$cte->contacto->dg_email);
            $columna++;
            foreach($cte->camposPersonalizados as $cp){
                $this->excel->setActiveSheetIndex(0)->setCellValue("$columna$fila",$cp);
                $columna++;
            }
            $columna = 'A';
            $fila++;
        }
    }
    
    private function setSizeCeldas(){
        $columna = 'A';
        for($i = 1; $i <= 50; $i++){
            $this->excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
            $columna++;
        }
    }
    
    private function lanzarExcel(){
        $fecha = new DateTime();
        $name =  $fecha->format('Ymd');
        $name .= '_Base_Conocimiento_Clientes';
        $this->excel->getActiveSheet()->setTitle('Base Conocimiento Clientes');
        $this->excel->setActiveSheetIndex(0);
        header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
        header("Content-Disposition: attachment; filename={$name}.xlsx");
        header('Cache-control: max-age=0');
        $excelSalida = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $excelSalida->save('php://output');
    }
    
}
