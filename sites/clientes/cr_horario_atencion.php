<?php
define("MAIN",1);
require_once("../../inc/init.php");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$dias = array(
	1 => 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'
);

?>
<div class="secc_bar">
	Horarios de atención sucursal
</div>
<div class="panes">
	<?php
		if(!isset($_POST['dc_sucursal']))
    		$form->Start('#','cr_horario_atencion','no_submit');
		else{
			$form->Start('sites/clientes/proc/crear_prepare_horario_atencion.php','crear_horario_atencion','overlayValidar');
			$form->Hidden('dc_sucursal',$_POST['dc_sucursal']);
		}
	?>
    	<?php
			$form->Combobox('Días','dc_dia',$dias,array(),' ');
			$form->Time('Hora desde','dt_hora_desde');
			$form->Time('Hora hasta','dt_hora_hasta');
		?>
        <br class="clear" />
        <?php $form->Button('Agregar Horario','id="btn_add_horario_atencion"','addbtn') ?>
        <?php $form->Group() ?>
        	<table class="tab" width="80%" align="center">
            	<thead>
                	<tr>
                    	<th width="30">&nbsp;</th>
                        <th>Día</th>
                        <th>Hora Inicio</th>
                        <th>Hora Término</th>
                    </tr>
                </thead>
                <tbody id="day_list">
                	<tr id="alert_day_list">
                    	<td colspan="4">
                        	<div class="info">
                        		Indique los días y horarios de atención para agregar a la sucursal
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php $form->Group() ?>
    <?php $form->End('Terminar','checkbtn') ?>
</div>
<script type="text/javascript" src="jscripts/sites/clientes/cr_horario_atencion.js?v=0_1b"></script>
<script type="text/javascript">
	cr_horario.dias = <?php echo json_encode($dias) ?>;
<?php if(isset($_POST['horario_atencion'])): ?>
	<?php
		foreach($_POST['horario_atencion'] as $horario):
		$horario = explode('|',$horario);
		$dia = $horario[0];
		$inicio = $horario[1];
		$termino = $horario[2];
	?>
	cr_horario.addHorarioTr('<?php echo $dia ?>','<?php echo $inicio ?>','<?php echo $termino ?>');
	<?php endforeach; ?>
<?php endif; ?>
</script>