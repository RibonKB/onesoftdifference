<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$base = $db->select('tb_campo_personalizado','dc_campo,dg_campo,dg_tipo',"dc_empresa={$empresa} AND dc_tipo_objetivo=1 AND dm_activo = 1");

if(!count($base)){
	$error_man->showAviso('No hay campos definidos para realizar base de conocimiento, estos son dispuestos por lo administradores del sistema.');
	exit;
}

$data = $db->select('tb_cliente_campo_personalizado','dc_campo,dg_valor',"dc_cliente={$_POST['id']}");

$valor = array();
foreach($data as $v){
	$valor[$v['dc_campo']] = $v['dg_valor'];
}
unset($data);

require_once("../../inc/form-class.php");
$form = new Form($empresa);

echo('<div class="secc_bar">Base de conocimientos cliente</div>
<div class="panes">');

$form->Start('sites/clientes/proc/set_base_conocimiento.php','set_knowledge_base');
$form->Header('Indique los datos a ser asignados como la base de conocimiento del cliente');

$index = 0;
foreach($base as $b){
	
	if(isset($valor[$b['dc_campo']])){
		$val = $valor[$b['dc_campo']];
	}else{
		$val = '';
	}
	
	$form->Section();
	switch($b['dg_tipo']){
		case 'text': 
			$form->Text($b['dg_campo'],"field[{$b['dc_campo']}]",0,255,$val);
			break;
		case 'select': 
			$form->Listado($b['dg_campo'],"field[{$b['dc_campo']}]",'tb_campo_personalizado_valor',array('dg_valor','dg_valor'),0,$val,"dc_campo={$b['dc_campo']}",array('order_by' => 'dc_orden'));
			break;
		case 'date':
			$form->Date($b['dg_campo'],"field[{$b['dc_campo']}]",0,$val);
			break;
		case 'multiple': 
			$val = $val!=''?explode('<',$val):array();
			$form->ListadoMultiple($b['dg_campo'],"field[{$b['dc_campo']}]",'tb_campo_personalizado_valor',array('dg_valor','dg_valor'),$val,"dc_campo={$b['dc_campo']}");
			break;
	}
	$form->EndSection();
	
	$index++;
	if($index % 2 == 0){
		echo('<br class="clear">');
	}
	
}

echo('<br class="clear">');
$form->Hidden('set_edit',count($valor));
$form->Hidden('cli_id',$_POST['id']);
$form->End('Asignar base','editbtn');
?>
</div>
<script type="text/javascript">
$('#set_knowledge_base select.multiple').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>