<?php
define("MAIN",1);
require_once("../../inc/init.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$dc_sucursal = intval($_POST['dc_sucursal']);

$sucursal = $db->prepare($db->select('tb_cliente_sucursal','true','dc_sucursal = ?'));
$sucursal->bindValue(1,$dc_sucursal,PDO::PARAM_INT);
$db->stExec($sucursal);

if($sucursal->fetch() === false){
	$error_man->showWarning('La sucursal seleccionada no es válida compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

$horarios = $db->prepare($db->select('tb_horario_atencion_sucursal','dc_dia, dt_hora_inicio, dt_hora_termino','dc_sucursal = ?'));
$horarios->bindValue(1,$dc_sucursal,PDO::PARAM_INT);
$db->stExec($horarios);

$form->Start('sites/clientes/cr_horario_atencion.php','cr_pre_horario','overlayValidar');
	$form->Hidden('dc_sucursal',$dc_sucursal);
	while($h = $horarios->fetch(PDO::FETCH_OBJ)):
		$inicio = substr($h->dt_hora_inicio,0,5);
		$fin = substr($h->dt_hora_termino,0,5);
		$form->Hidden('horario_atencion[]',"{$h->dc_dia}|{$inicio}|{$fin}");
	endwhile;
$form->End();

?>
<script type="text/javascript">
	pymerp.init();
	$('#cr_pre_horario').trigger('submit');
</script>