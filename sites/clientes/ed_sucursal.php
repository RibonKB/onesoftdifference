<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_cliente_sucursal WHERE dc_sucursal={$_POST['id']} AND dm_activo=1) s
JOIN tb_comuna c ON c.dc_comuna = s.dc_comuna
JOIN tb_region r ON r.dc_region = c.dc_region",
'dg_sucursal,dg_direccion,dg_fono,dg_fax,dg_codigo_postal,dg_horario_atencion,s.dc_comuna,r.dc_region');
					
if(!count($data)){
	$error_man->showWarning('Sucursal no encontrada');
	exit;
}

$data = $data[0];

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div class="secc_bar">Creación de sucursales</div><div class="panes">');

$form->Start('sites/clientes/proc/editar_sucursal.php','ed_sucursal');
$form->Header('<b>Ingrese los datos para la creación del contacto</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();	 
$form->Text('Nombre','cli_suc_name',1,255,$data['dg_sucursal']);
$form->Text('Dirección','cli_suc_direccion',1,255,$data['dg_direccion']);
$form->Listado('Región','cli_region','tb_region',array('dc_region','dg_region'),0,$data['dc_region'],'');
$form->Listado('Comuna','cli_suc_comuna','tb_comuna',array('dc_comuna','dg_comuna'),1,$data['dc_comuna'],"dc_region={$data['dc_region']}");
//$form->Select('Comuna','cli_suc_comuna',array(),1);
$form->EndSection();

$form->Section();
$form->Text('Teléfono','cli_suc_fono',0,255,$data['dg_fono']);
$form->Text('Fax','cli_suc_fax',0,255,$data['dg_fax']);
$form->Text('Código Postal','cli_suc_postal',0,255,$data['dg_codigo_postal']);
$form->Text('Horario atención','cli_suc_horario',0,255,$data['dg_horario_atencion']);
$form->EndSection();

$form->Section();

//se obtienen todos los tipos de direccion parar enlistarlos con checkboxs
$aux = $db->select("tb_tipo_direccion","dc_tipo_direccion,dg_tipo_direccion","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by"=>"dg_tipo_direccion"));
$tcontactos = array();
foreach($aux as $t)
	$tcontactos[$t['dc_tipo_direccion']] = $t['dg_tipo_direccion'];
	
//se traen los tipos de direccion asignados a la sucursal para marcarlos como activos
$aux = $db->select('tb_cliente_sucursal_tipo','dc_tipo_direccion',"dc_sucursal={$_POST['id']}");
$checked = array();
foreach($aux as $c)
	$checked[] = $c['dc_tipo_direccion'];

$form->Combobox("Tipo de sucursal","cli_suc_tipo",$tcontactos,$checked);
$form->EndSection();

$form->Hidden('tipos_change',implode(',',$checked));
$form->Hidden('sucursal_id',$_POST['id']);
$form->End('Editar','editbtn');

$aux = $db->select('tb_comuna','dc_comuna,dg_comuna,dc_region');
$comunas = array();
foreach($aux as $comuna){
	$comunas[$comuna['dc_region']][] = array($comuna['dc_comuna'],$comuna['dg_comuna']);
}
?>
<script type="text/javascript">
$('#cli_region').change(js_data.changeRegion);
js_data.comunas = <?=json_encode($comunas) ?>;
</script>