<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div class="secc_bar">Creación de contactos</div><div class="panes">');

$form->Start('sites/clientes/proc/crear_contacto.php','cr_contacto');
$form->Header('<b>Ingrese los datos para la creación del contacto</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();
$form->Listado('Sucursal','cli_cont_sucursal','tb_cliente_sucursal',array('dc_sucursal','dg_sucursal'),1,$_POST['id'],"dc_cliente={$_POST['cli_id']}");
$form->Text('Nombre contacto','cli_cont_name',1);
$form->Text('Teléfono','cli_cont_fono');
$form->Text('Teléfono movil','cli_cont_movil');
$form->EndSection();

$form->Section();
$form->Text('E-Mail','cli_cont_mail');
$form->Text('Fax','cli_cont_fax');
$form->Listado('Cargo contacto','cli_cont_cargo','tb_cargo_contacto',array('dc_cargo_contacto','dg_cargo_contacto'));
$form->EndSection();

$form->Hidden('cli_id',$_POST['cli_id']);
$form->End('Crear','addbtn');
?>
</div>