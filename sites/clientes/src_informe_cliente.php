<?php
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);
?>
<div id="secc_bar">
	Informe de clientes
</div>
<div id="main_cont"><div class="panes">
	<?php
		$form->Start('sites/clientes/proc/src_informe_cliente.php','src_informe_cliente');
		$form->Header("Indique los filtros que se aplicarán al informe de clientes<br />
		Los datos arcados con [*] son obligatorios.");
	?>
	<table class="tab" style="text-align:left;" id="form_container" width="100%">
		<tr>
			<td width="100">Ejecutivo</td>
			<td>
				<?php $form->ListadoMultiple('','dc_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno')) ?>
			</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tipo de Cliente</td>
			<td>
				<?php $form->ListadoMultiple('','dc_tipo_cliente','tb_tipo_cliente',array('dc_tipo_cliente','dg_tipo_cliente')); ?>
			</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td>Mercado</td>
			<td>
				<?php $form->ListadoMultiple('','dc_mercado','tb_mercado',array('dc_mercado','dg_mercado')); ?>
			</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td>Región</td>
			<td>
				<?php $form->ListadoMultiple('','dc_region','tb_region',array('dc_region','dg_region'),0,''); ?>
			</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td>Fecha de creación</td>
			<td>
				<?php $form->Date('Desde','df_creacion_desde'); ?>
			</td>
			<td>
				<?php $form->Date('Hasta','df_creacion_hasta',0,0); ?>
			</td>
		</tr>
	</table>
	<?php $form->End('Ejecutar consulta','searchbtn'); ?>
</div></div>
<script type="text/javascript" src="jscripts/sites/clientes/src_informe_cliente.js"></script>