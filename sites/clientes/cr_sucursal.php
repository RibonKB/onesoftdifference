<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div class="secc_bar">Creación de sucursales</div><div class="panes">');

$form->Start('sites/clientes/proc/crear_sucursal.php','cr_sucursal');
$form->Header('<b>Ingrese los datos para la creación del contacto</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();	 
$form->Text('Nombre','cli_suc_name',1);
$form->Text('Dirección','cli_suc_direccion',1);
$form->Listado('Región','cli_region','tb_region',array('dc_region','dg_region'),0,0,'');
$form->Select('Comuna','cli_suc_comuna',array(),1);
$form->EndSection();

$form->Section();
$form->Text('Teléfono','cli_suc_fono');
$form->Text('Fax','cli_suc_fax');
$form->Text('Código Postal','cli_suc_postal');
//$form->Text('Horario atención','cli_suc_horario');
echo('<br /><div id="horario_atencion_data"></div>');
$form->Button('Horario de atención','id="setup_horario_atencion"');
echo('<br /><br />');
$form->EndSection();

$form->Section();
$aux = $db->select("tb_tipo_direccion","dc_tipo_direccion,dg_tipo_direccion","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by"=>"dg_tipo_direccion"));
$tcontactos = array();
foreach($aux as $t)
	$tcontactos[$t['dc_tipo_direccion']] = $t['dg_tipo_direccion'];
$form->Combobox("Tipo de sucursal","cli_suc_tipo",$tcontactos);
$form->EndSection();

$form->Hidden('cli_id',$_POST['id']);
$form->End('Crear','addbtn');

$aux = $db->select('tb_comuna','dc_comuna,dg_comuna,dc_region');
$comunas = array();
foreach($aux as $comuna){
	$comunas[$comuna['dc_region']][] = array($comuna['dc_comuna'],$comuna['dg_comuna']);
}

?></div>
<script type="text/javascript">
$('#cli_region').change(js_data.changeRegion);
js_data.comunas = <?php echo json_encode($comunas) ?>;
$('#setup_horario_atencion').click(function(){
	var data = $(':input',$('#horario_atencion_data')).serialize();
	pymerp.loadOverlay('sites/clientes/cr_horario_atencion.php?',data,true);
});
</script>