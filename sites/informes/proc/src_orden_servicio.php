<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/time-functions.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$conditions = "YEAR(df_fecha_emision) = '{$_POST['os_ann']}' AND dc_empresa={$empresa}";

if(isset($_POST['os_month'])){
	$month = implode("','",$_POST['os_month']);
	$conditions .= " AND MONTH(df_fecha_emision) IN ('{$month}')";
	unset($month);
}

if(isset($_POST['os_tecnico'])){
	$tecnico = implode(",",$_POST['os_tecnico']);
	$conditions .= " AND dc_tecnico IN ({$tecnico})";
	unset($tecnico);
}

if(isset($_POST['os_client'])){
	$cli = implode(',',$_POST['os_client']);
	$conditions .= " AND dc_cliente IN ({$cli})";
	unset($cli);
}

if(isset($_POST['os_stat'])){
	$stat = implode(',',$_POST['os_stat']);
	$conditions .= " AND dc_estado IN ({$stat})";
	unset($stat);
}

if($_POST['os_solucionado'] == 2){
	$_POST['os_solucionado'] = "0','1";
}

$data = $db->select("(SELECT * FROM tb_orden_servicio WHERE {$conditions} AND dm_estado IN('{$_POST['os_solucionado']}')) os
JOIN tb_funcionario f ON f.dc_funcionario = os.dc_tecnico
LEFT JOIN tb_cliente_sucursal suc ON suc.dc_sucursal = os.dc_sucursal
LEFT JOIN tb_estado_orden_servicio es ON es.dc_estado = os.dc_estado
LEFT JOIN tb_cliente cl ON cl.dc_cliente = os.dc_cliente
LEFT JOIN (SELECT * FROM tb_orden_servicio_avance WHERE dm_estado_actividad = '1' AND dm_estado_orden = '0') gs ON gs.dc_orden_servicio = os.dc_orden_servicio",
"os.dc_orden_servicio,os.dq_orden_servicio,es.dg_estado,os.dg_solicitante,cl.dg_razon,os.dg_requerimiento,os.dg_solucion,os.dm_estado, suc.dg_sucursal,
TIMESTAMPDIFF(HOUR,NOW(),gs.df_proxima_actividad) AS df_rango_fecha,DATE_FORMAT(df_proxima_actividad,'%d/%m/%Y %H:%i') as df_compromiso,
TIMESTAMPDIFF(MINUTE,os.df_fecha_emision,NOW()) AS df_rango_fecha_inicio, CONCAT_WS(' ',f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS dg_tecnico",
'',array('order_by' => 'df_rango_fecha'));

if(!count($data)){
	$error_man->showAviso("No se encontraron ordenes de servicio con los filtros indicados");
	exit;
}

$no_terminados = array();
$terminados = array();
foreach($data as $os){
	if($os['dm_estado'] == '0')
		$no_terminados[] = $os['dc_orden_servicio'];
	else
		$terminados[] = $os['dc_orden_servicio'];
}

echo("<table class='tab' id='os_monitor' width='100%'>
<thead>
<tr>
	<th></th>
	<th>Tiempo</th>
	<th width='80'>Orden</th>
	<th width='100'>Estado</th>
	<th width='150'>Solicitante</th>
	<th width='150'>Técnico</th>
	<th>Fecha de compromiso</th>
	<th>Cliente</th>
	<th>Sucursal</th>
	<th>Requerimiento</th>
	<th>Solución</th>
</tr>
</thead>
<tbody>");

foreach($data as $os){
if($os['df_rango_fecha'] != NULL){
	if($os['df_rango_fecha'] <= $empresa_conf['dc_indicador_rojo_os'])
		$luz = '<img src="images/circle_offline.gif" alt="[RJ]" />';
	else if($os['df_rango_fecha'] >= $empresa_conf['dc_indicador_verde_os'])
		$luz = '<img src="images/circle_online.gif" alt="[VD]" />';
	else
		$luz = '<img src="images/circle_onffline.gif" alt="[AM]" />';
		
	$horas = MinToHour($os['df_rango_fecha_inicio']);
}else{
	$luz = '-';
	$horas = '-';
}

$requeriment = substr($os['dg_requerimiento'],0,50);
echo("<tr>
<td>{$luz}</td>
<td>{$horas}</td>
<td>{$os['dq_orden_servicio']}</td>
<td>{$os['dg_estado']}</td>
<td>{$os['dg_solicitante']}</td>
<td>{$os['dg_tecnico']}</td>
<td>{$os['df_compromiso']}</td>
<td>{$os['dg_razon']}</td>
<td>{$os['dg_sucursal']}</td>
<td title='{$os['dg_requerimiento']}'>{$requeriment}</td>
<td>{$os['dg_solucion']}</td>
</tr>");
}

echo("</tbody>
</table>");
?>
<script type="text/javascript">
window.clearTimeout(os_timeout);
var os_timeout = window.setTimeout(function(){
	if($('#src_orden_servicio_res').size()){
		$('#src_orden_servicio').trigger('submit').prev('input[type=button]').remove();
	}
},60000);
$("#os_monitor").tableExport();
</script>