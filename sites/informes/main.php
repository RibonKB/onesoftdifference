<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once("../../inc/form-class.php");
$form = new Form($empresa);

echo("<div id='secc_bar'>Monitor de informes</div>
<div id='main_cont'>
<ul id='tabs'>
<li><a href='#'>Monitor</a></li>
</ul>
<br class='clear' />
<div class='tabpanes'>
<div>");

$form->Start('sites/informes/proc/src_orden_servicio.php','src_orden_servicio');
$form->Header('Indique los filtros que aplicará al informe');
$form->Section();

$meses = array(
'1' => 'Enero',
'2' => 'Febrero',
'3' => 'Marzo',
'4' => 'Abril',
'5' => 'Mayo',
'6' => 'Junio',
'7' => 'Julio',
'8' => 'Agosto',
'9' => 'Septiembre',
'10' => 'Octubre',
'11' => 'Noviembre',
'12' => 'Diciembre');

$form->MultiSelect('Mes','os_month',$meses);
$form->Radiobox('Solucionado','os_solucionado',array(1=> 'SI', 0=> 'NO', 2=> 'TODAS'),0,'');
$form->EndSection();
$form->Section();
$form->Text('Año','os_ann',1,4,date('Y'));
$form->ListadoMultiple('Estado','os_stat','tb_estado_orden_servicio',array('dc_estado','dg_estado'));
$form->EndSection();
$form->Section();
$form->ListadoMultiple('Cliente','os_client','tb_cliente',array('dc_cliente','dg_razon'));
$form->EndSection();
$form->Section();
if(check_permiso(54))
	$form->ListadoMultiple('Técnico','os_tecnico','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 4");

else
	$form->Hidden('os_tecnico[]',$userdata['dc_funcionario']);
$form->EndSection();
$form->End('Filtrar','searchbtn');

echo("</div>
</div>");



echo("</div>");

?>
<script type="text/javascript">
$('#os_month,#os_client,#os_stat,#os_tecnico').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});

window.clearTimeout(os_timeout);
</script>