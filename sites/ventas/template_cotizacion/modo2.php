<?php
require("../../inc/fpdf.php");

class PDF extends FPDF{
private $datosEmpresa;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa=3 AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','pt',array(830,640));
	
}

function Header(){
	global $datosCotizacion;

	$this->SetFillColor(129,197,100);
	$this->Rect(0,0,160,640,'F');
	$this->SetLeftMargin(5);
	$this->Image("../../images/logos_empresa/3.png",0,0,150);
	$this->SetY(150);
	$this->SetFont('Helvetica','',10);
	$this->SetTextColor(215,237,205);
	$this->MultiCell(150,10,"{$this->datosEmpresa['dg_giro']}");
	$this->Ln(30);
	$this->SetFont('','B');
	$this->MultiCell(150,10,"Proyecto:\nNombre del proyecto");
	$this->Ln(10);
	$this->MultiCell(150,10,"Fecha de Cotización\n{$datosCotizacion['df_fecha_emision']}");
	$this->Ln(120);
	
	$this->SetFont('','',8);
	$this->MultiCell(150,10,$this->datosEmpresa['dg_pie_cotizacion']);
	
	$this->SetY(10);
	$this->SetLeftMargin(180);
}

function AddDetalle($detalle){
	global $datosCotizacion;

	$this->AddPage();
	$this->SetFont('Helvetica','I',12);
	$this->SetTextColor(0);
	$this->Ln(30);
	$w = array(350,120,120);
	$head = array("Descripción","Valor Unitario","Valor total");
	for($i=0;$i<3;$i++){
		$this->Cell($w[$i],15,$head[$i],0,0,'L');
	}
	$this->Ln(40);
	
	foreach($detalle as $i => $det){
		$y1 = $this->GetY();
		$this->MultiCell($w[0],10,$det['dg_descripcion']);
		$this->SetY($y1);
		$this->SetX(180+$w[0]);
		$this->Cell($w[1],10,"$ ".moneda_local($det['dq_precio_venta']),0,2,'R');
		$this->SetY($y1);
		$this->SetX(180+$w[0]+$w[1]);
		$this->Cell($w[2],10,"$ ".moneda_local($det['dq_total']),0,2,'R');
		$this->Ln();
	}
	
	$this->Ln(20);
	$y = $this->GetY();
	$this->MultiCell($w[0]+$w[1],10,"Total");
	$this->SetY($y);
	$this->SetX(180+$w[0]+$w[1]);
	$this->Cell($w[2],10,"$ ".moneda_local($datosCotizacion['dq_total']),0,2,'R');
}


}
?>