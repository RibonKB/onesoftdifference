<?php
require("../../inc/fpdf.php");

class PDF extends FPDF{

private $datosEmpresa;
private $logo;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	$this->logo = "../../images/logos_empresa/1.jpg";
	
	unset($datosE);
	
	parent::__construct('P','mm','Letter');
	
}

function Header(){
	global $datosCotizacion,$opcionesCotizacion;
	
	$this->SetFont('Arial','',12);
	$this->SetDrawColor(0,100,0);
	$this->SetTextColor(0,100,0);
	$this->SetX(160);
	$this->MultiCell(50,9,"R.U.T. {$this->datosEmpresa['dg_rut']}\nCOTIZACION\nN� {$datosCotizacion['dq_cotizacion']}",1,'C');
	$this->SetY(10);
	$this->SetTextColor(0);
	
	//$this->SetFont('','B',12);
	//$this->Cell(145,5,$this->datosEmpresa['dg_razon']);
	//Image(string file [, float x [, float y [, float w [, float h [, string type [, mixed link]]]]]])
	$this->Image($this->logo,NULL,NULL,0,20);
	$this->SetFont('Arial','',7);
	$this->MultiCell(145,2.4,"{$this->datosEmpresa['dg_giro']}\n{$this->datosEmpresa['dg_direccion']}, {$this->datosEmpresa['dg_comuna']} {$this->datosEmpresa['dg_region']}\n{$this->datosEmpresa['dg_fono']}");
	$this->Ln(3);
	
	$lineas = 0;
	$this->SetFontSize(9);
	$lineas += ceil($this->GetStringWidth($datosCotizacion['dg_razon'])/100);
	$lineas += ceil($this->GetStringWidth($datosCotizacion['dg_giro'])/100);
	
	$this->SetDrawColor(150,150,150);
	$this->SetY(46);
	$this->Cell(0,5,'','B');
	$this->Ln();
	$y = $this->GetY();
	$this->SetFontSize(5);
	$this->MultiCell(14,7,"SE�ORES:\nDIRECCI�N:\nGIRO: ".str_repeat("\n ",$lineas),'L');
	$this->SetY($y);
	$this->SetX(138);
	$this->MultiCell(14,7,"CONTACTO:\nCOMUNA:");
	$this->SetFontSize(9);
	$this->SetY($y);
	$this->SetX(150);
	$this->MultiCell(56,7," {$datosCotizacion['dg_contacto']}\n{$datosCotizacion['dg_comuna']} \n ".str_repeat("\n ",$lineas),"R");
	$this->SetY($y);
	$this->SetX(22);
	$this->MultiCell(110,7,"{$datosCotizacion['dg_razon']}\n{$datosCotizacion['dg_direccion']}\n{$datosCotizacion['dg_giro']}");
	$this->Cell(0,5,'','T');
	$this->Ln();	
	
	$this->SetFillColor(210,210,210);
	$this->SetFontSize(9);
	$y = $this->GetY();
	$this->Cell(35,7,"T�rmino comercial",'B',2,'C',1);
	$this->Cell(35,5,$datosCotizacion['dg_termino_comercial'],0,0,'R');
	$this->SetY($y);
	
	$this->SetX(50);
	$this->Cell(40,7,"Tipo Cambio",'B',0,'L',1);
	$this->SetX(50);
	$this->Cell(40,7,$this->datosEmpresa['dg_moneda_local'],0,1,'R');
	$this->SetX(50);
	$this->Cell(40,5,$datosCotizacion['dg_tipo_cambio'],0,0,'L');
	$this->SetX(50);
	$this->Cell(40,5,$datosCotizacion['dq_cambio'],0,0,'R');
	$this->SetY($y);
	
	$this->SetX(95);
	$this->Cell(46,7,"Ejecutivo",'B',2,'C',1);
	$this->Cell(46,7,$datosCotizacion['dg_ejecutivo'],0,0,'R');
	$this->SetY($y);
	
	$this->SetX(146);
	$this->Cell(25,7,"Fecha envio",'B',2,'C',1);
	$this->Cell(25,7,$datosCotizacion['df_fecha_envio'],0,0,'R');
	$this->SetY($y);
	
	$this->SetX(176);
	$this->Cell(30,7,"Modo envio",'B',2,'C',1);
	$this->Cell(30,7,$datosCotizacion['dg_modo_envio'],0,0,'R');
	$this->SetY($y+13);
	
	if(count($opcionesCotizacion['contactoExtra']))
	{
		$this->SetDrawColor(150,150,150);
		$this->Cell(0,2,'','B');
		$this->Ln();
		$y = $this->GetY();
		$contactosExtra = "";
		$dir_contactosExtra = "";
		$sizes = array();
		foreach($opcionesCotizacion['contactoExtra'] as $i => $v){
			$contactosExtra .= $v." :\n";
			$dir_contactosExtra .= $opcionesCotizacion['dir_contactoExtra'][$i]."\n";
			$sizes[] = $this->GetStringWidth($v);
		}
		
		$this->SetFontSize(5);
		$this->MultiCell(max($sizes),5,strtoupper($contactosExtra),'LB');
		$this->SetY($y);
		$this->SetX(max($sizes)+5);
		$this->SetFontSize(11);
		$this->MultiCell(201-max($sizes),5,$dir_contactosExtra,'RB');
		$this->Ln(3);
	}
	
	$w = array(18,29,93,28,28);
	$head = array("CANTIDAD","C�DIGO","DESCRIPCION","PRECIO","TOTAL");
	$this->SetFontSize(7);
	for($i=0;$i<5;$i++){
		$this->Cell($w[$i],6,$head[$i],1,0,'L',1);
	}
	$this->Ln();
	
}

function Footer()
{
	$pie = explode("\n",$this->datosEmpresa['dg_pie_cotizacion']);
    //Posici?n: a 1,5 cm del final
    $this->SetY((count($pie)*-3)-20);
    //Arial italic 8
    $this->SetFont('Arial','I',7);
    //N?mero de p?gina
	$this->AliasNbPages();
	$this->MultiCell(150,3,$this->datosEmpresa['dg_pie_cotizacion']);
	$this->SetY(-15);
    $this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'R');
}

function AddDetalle($detalle){
	global $datosCotizacion,$empresa_conf;
	
	$this->AddPage();
	$this->SetFont('Times','',9);
	$this->SetDrawColor(150,150,150);
	$this->SetFillColor(240,240,240);
	$fill = false;
	
	$lineas = 1;
	
	$w = array(18,29,93,28,28);
	$lm = 10;
	$xs = array($lm+$w[0],$lm+$w[0]+$w[1],$lm+$w[0]+$w[1]+$w[2],$lm+$w[0]+$w[1]+$w[2]+$w[3]);
	
	foreach($detalle as $i => $det){
		
		$hdesc = ceil($this->GetStringWidth($det['dg_descripcion'])/91);
		
		$lineas += $hdesc;
		$append = str_repeat("\n ",$hdesc-1);
		if($lineas > 20){
			$lineas = 0;
		}
		
		if(($lineas)%21 == 0){
			$this->Cell(0,6,'','T');
			$this->AddPage();
		}
		
		
		/*$this->Cell(25,6,moneda_local($det['dq_cantidad']),'LR',0,'R',$fill);
		$this->Cell(91,6,$det['dg_descripcion'],'LR',0,'L',$fill);
		$this->Cell(40,6,moneda_local($det['dq_precio_venta'],$datosCotizacion['dn_cantidad_decimales']),'LR',0,'R',$fill);
		$this->Cell(40,6,moneda_local($det['dq_total'],$datosCotizacion['dn_cantidad_decimales']),'LR',0,'R',$fill);
		$this->Ln();*/
		
		$y = $this->GetY();
		$this->MultiCell(18,5,moneda_local($det['dq_cantidad']).$append,'LR','R',$fill);
		$this->SetY($y);
		$this->SetX($xs[0]);
		$this->MultiCell(29,5,$det['dg_codigo'].$append,'LR','L',$fill);
		$this->SetY($y);
		$this->SetX($xs[1]);
		$this->MultiCell(93,5,$det['dg_descripcion'],'LR','L',$fill);
		$this->SetY($y);
		$this->SetX($xs[2]);
		$this->MultiCell(28,5,moneda_local($det['dq_precio_venta'],$datosCotizacion['dn_cantidad_decimales']).$append,'LR','R',$fill);
		$this->SetY($y);
		$this->SetX($xs[3]);
		$this->MultiCell(28,5,moneda_local($det['dq_cantidad']*$det['dq_precio_venta'],$datosCotizacion['dn_cantidad_decimales']).$append,'LR','R',$fill);
		
		$fill = !$fill;
	}
	
	for($i=0;$i<(10 -$lineas%9);$i++){
		$this->Cell(18,5,"",'LR',0,'R',$fill);
		$this->Cell(29,5,"",'LR',0,'R',$fill);
		$this->Cell(93,5,"",'LR',0,'L',$fill);
		$this->Cell(28,5,"",'LR',0,'R',$fill);
		$this->Cell(28,5,"",'LR',0,'R',$fill);
		$this->Ln();
		$fill = !$fill;
	}
	$this->Cell(0,6,'','T');
	$this->Ln(0);
	
	$y = $this->GetY();
	$this->SetFont('Arial','',7);
	$this->MultiCell(116,3,"Observaciones: \n".$datosCotizacion['dg_observacion'],1);
	
	$this->SetFillColor(200,200,200);
	$this->SetY($y);
	$this->SetX(126);
	$this->SetFont('','B');
	$this->Cell(40,5,"TOTAL ({$datosCotizacion['dg_tipo_cambio']})",1,2,'R',1);
	$this->Cell(40,5,"TOTAL NETO",1,2,'R',1);
	$this->Cell(40,5,"I.V.A.",1,2,'R',1);
	$this->Cell(40,5,"TOTAL PAGAR",1,2,'R',1);
	
	$this->SetY($y);
	$this->SetX(166);
	$this->SetFont('Times','',9);
	$this->Cell(40,5,moneda_local($datosCotizacion['dq_total'],$datosCotizacion['dn_cantidad_decimales']),1,2,'R');
	$this->Cell(40,5,moneda_local($datosCotizacion['dq_neto']),1,2,'R');
	$this->Cell(40,5,moneda_local($datosCotizacion['dq_iva']),1,2,'R');
	$this->Cell(40,5,moneda_local($datosCotizacion['dq_neto']+$datosCotizacion['dq_iva']),1,2,'R');
	
}

}
?>