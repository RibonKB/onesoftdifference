<?php
require("../../inc/fpdf.php");

class PDF extends FPDF{
private $datosEmpresa;
private $blank = false;
private $docWidth = 830;

public function  PDF(){
	global $empresa,$db;
	
	$datosE = $db->select("(SELECT * FROM tb_empresa WHERE dc_empresa={$empresa}) e
	JOIN tb_empresa_configuracion ec ON e.dc_empresa=ec.dc_empresa
	LEFT JOIN tb_comuna c ON c.dc_comuna = e.dc_comuna
	LEFT JOIN tb_region r ON r.dc_region = c.dc_region",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','pt',array($this->docWidth,640));
	
}

function Header(){
	global $datosCotizacion;
	
	if($this->blank)
		return;

	$this->SetFillColor(129,197,100);
	$this->Rect(0,0,160,640,'F');
	$this->SetLeftMargin(5);
	$this->Image("../../images/logos_empresa/3.png",0,0,150);
	$this->SetY(150);
	$this->SetFont('Helvetica','',10);
	$this->SetTextColor(215,237,205);
	$this->MultiCell(150,10,"{$this->datosEmpresa['dg_giro']}");
	$this->Ln(30);
	$this->SetFont('','B');
	$this->MultiCell(150,12,"Proyecto N� {$datosCotizacion['dq_proyecto']}:\n{$datosCotizacion['dg_proyecto']}");
	$this->Ln(10);
	$this->MultiCell(150,12,"Fecha de Cotizaci�n N� {$datosCotizacion['dq_cotizacion']}\n{$datosCotizacion['df_fecha_emision']}");
	$this->Ln(10);
	$this->MultiCell(150,12,"Medio de pago:\n{$datosCotizacion['dg_termino_comercial']}");
	$this->Ln(110);
	
	$this->SetFont('','',8);
	$this->MultiCell(150,10,$this->datosEmpresa['dg_pie_cotizacion']);
	
	$this->SetY(10);
	$this->SetLeftMargin(180);
}

function AddDetalle($detalle){
	global $datosCotizacion;

	$this->SetFont('Helvetica','I',12);
	$this->SetTextColor(0);
	$this->Ln(30);
	$w = array(350,30,110,110);
	$p = array('L','C','R','R');
	$head = array("Descripci�n","Cantidad","Valor Unitario","Valor total");
	for($i=0;$i<4;$i++){
		$this->Cell($w[$i],15,$head[$i],0,0,$p[$i]);
	}
	$this->Ln(40);
	
	foreach($detalle as $i => $det){
		$y1 = $this->GetY();
			$this->SetFont('','B');
		if(isset($det['dg_producto'])){
			$this->MultiCell($w[0],14,$det['dg_producto']);
			$this->SetFont('','');
		}
		if($det['dg_descripcion'])
			$this->MultiCell($w[0],14,$det['dg_descripcion']);
			
		$this->SetFont('','');
		
		/*if(isset($datosCotizacion['sd_material'][$det['dc_costeo']])){
			foreach($datosCotizacion['sd_material'][$det['dc_costeo']] as $sd){
				$this->SetX(200);
				$this->Cell($w[0],12,"{$sd['dc_cantidad']}{$sd['dg_unidad_medida']} {$sd['dg_material']}");
				$this->Ln();
			}
		}
		
		if(isset($datosCotizacion['sd_servicio'][$det['dc_costeo']])){
			foreach($datosCotizacion['sd_servicio'][$det['dc_costeo']] as $sd){
				$this->SetX(200);
				$this->Cell($w[0],12,"{$sd['dc_cantidad']}  {$sd['dg_servicio']}");
				$this->Ln();
			}
		}*/
		
		$y2 = $this->GetY();
		$this->SetY($y1);
		$this->SetX(180+$w[0]);
		$this->Cell($w[1],14,moneda_local($det['dc_cantidad']),0,2,'C');
		$this->SetY($y1);
		$this->SetX(180+$w[0]+$w[1]);
		$this->Cell($w[2],14,"$ ".moneda_local($det['dq_precio_venta']),0,2,'R');
		$this->SetY($y1);
		$this->SetX(180+$w[0]+$w[1]+$w[2]);
		$this->Cell($w[3],14,"$ ".moneda_local($det['dq_total']),0,2,'R');
		$this->SetY($y2);
		$this->Ln(20);
	}
	
	$this->Ln(20);
	$y = $this->GetY();
	$this->MultiCell($w[0]+$w[1]+$w[2],20,"Total Neto\nI.V.A.\nTotal",0,'R');
	$this->SetY($y);
	$this->SetX(180+$w[0]+$w[1]+$w[2]);
	$this->SetFont('','B');
	$this->MultiCell($w[2],20,
		"$ ".moneda_local($datosCotizacion['dq_total'])
		."\n$ ".moneda_local($datosCotizacion['dq_iva'])
		."\n$ ".moneda_local($datosCotizacion['dq_total']+$datosCotizacion['dq_iva']),0,'R');
}

function setBlank(){
	$this->blank = true;
}

function getWidth(){
	return $this->docWidth;
}

}
?>