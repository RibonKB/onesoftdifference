<?php
class CrearNotaCreditoFactory extends Factory{

  private $factura, $proveedor, $tipo, $detalles, $dq_total, $dq_neto, $dq_exento, $dq_iva, $contactos, $cuentas_contables;
  protected $dq_nota_credito, $dc_nota_credito;
  protected $title = "Creación de nota de Crédito de Proveedor";

  public function crearFormAction(){
    $this->initCrearFormData();

    $form = $this->getFormView($this->getTemplateUrl('crear.form'),array(
      'proveedor' => $this->proveedor,
      'tipo' => $this->tipo,
      'contactos' => $this->contactos,
      'moneda' => $this->getParametrosEmpresa()->dg_moneda_local,
      'iva_empresa' => $this->getParametrosEmpresa()->dq_iva,
      'decimales_empresa' => $this->getParametrosEmpresa()->dn_decimales_local,
      "cuentas_contables" => $this->cuentas_contables
    ));

    echo $this->getFullView($form,array(),Factory::STRING_TEMPLATE);

  }

  public function procesarCrearAction(){
    $this->initCrear();

    $this->getConnection()->start_transaction();
      $this->insertarCabeceraNotaCredito();
      $this->insertarDetallesNotaCredito();
    $this->getConnection()->commit();

    echo $this->getView($this->getTemplateURL('datosCreacion'), array(
        'dc_nota_credito' => $this->dc_nota_credito,
        'dq_nota_credito' => $this->dq_nota_credito,
        'dq_folio' => self::getRequest()->dq_folio
    ));

  }

  private function initCrearFormData(){
    $r = self::getRequest();

    $this->initProveedorByRut($r);
    $this->initTipoNotaCredito($r);
    $this->initContactosProveedor();
    $this->initCuentasContablesCentralizacion();

  }

  private function initCrear(){
    $r = self::getRequest();

    $this->initFactura($r);
    $this->initProveedor($r);
    $this->initTipoNotaCredito($r);
    $this->initDetalles($r);
    $this->initTotales($r);

  }

  private function initFactura($r){

    if(!isset($r->dc_factura_simple) or !preg_match("/^\d+$/",$r->dc_factura_simple)):
      $this->getErrorMan()->showWarning("Es necesario que toda Nota de crédito esté relacionada con una factura de compra");
      exit;
    endif;

    $db = $this->getConnection();
    $this->factura = $db->getRowById("tb_factura_compra",$r->dc_factura_simple,"dc_factura");

    if($this->factura === false or $this->factura->dm_nula == 1):
      $this->getErrorMan()->showWarning("La factura seleccionada es inválida o esta ha sido anulada,".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

  }

  private function initProveedor($r){
    if(!isset($r->dc_proveedor) or !preg_match('/^\d+$/',$r->dc_proveedor)):
      $this->getErrorMan()->showWarning("El proveedor en la Nota de crédito es inválido y debe ser revisado,".
                                        "favor verificar los datos de entrada y volver a intentarlo.");
      exit;
    endif;

    $db = $this->getConnection();
    $this->proveedor = $db->getRowById('tb_proveedor',$r->dc_proveedor,'dc_proveedor');

    if($this->proveedor === false or $this->proveedor->dm_activo == 0):
      $this->getErrorMan()->showWarning("El proveedor indicado no fue encontrado o este fue eliminado,".
                                        "favor veriicar los datos de entrada y volver a intentarlo");
      exit;
    endif;

  }

  private function initProveedorByRut($r){
    $this->initFunctionsService();
    if(!Functions::validaRut($r->dg_rut)):
      $this->getErrorMan()->showErrorRedirect(
        "El rut del proveedor es inválido y no se puede utilizar para crear la Nota de Crédito.",
        "sites/ventas/nota_credito_proveedor/cr_nota_credito.php");
      exit;
    endif;

    $db = $this->getConnection();
    $select = $db->prepare($db->select('tb_proveedor','*','dg_rut = ? AND dc_empresa = ? AND dm_activo = 1'));
    $select->bindValue(1, $r->dg_rut, PDO::PARAM_STR);
    $select->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($select);

    $this->proveedor = $select->fetch(PDO::FETCH_OBJ);

    if($this->proveedor === false):
      $this->getErrorMan()->showErrorRedirect(
        "No se encontró el proveedor especificado, intentelo nuevamente.",
        "sites/ventas/nota_credito_proveedor/cr_nota_credito.php");
      exit;
    endif;

  }

  private function initContactosProveedor(){
    $db = $this->getConnection();

    $select = $db->prepare($db->select('tb_contacto_proveedor','dc_contacto_proveedor, dg_contacto','dc_proveedor = ?'));
    $select->bindValue(1, $this->proveedor->dc_proveedor, PDO::PARAM_INT);
    $db->stExec($select);

    $this->contactos = array();

    while($r = $select->fetch(PDO::FETCH_OBJ)):
      $this->contactos[$r->dc_contacto_proveedor] = $r->dg_contacto;
    endwhile;

  }

  private function initCuentasContablesCentralizacion(){

    if($this->tipo->dm_retorna_stock == 1):
      return;
    endif;

    $db = $this->getConnection();

    $select = $db->prepare(
                $db->select('tb_tipo_nota_credito_proveedor_cuenta_contable t'.
                            ' JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = t.dc_cuenta_contable',
                            't.dc_cuenta_contable, cc.dg_codigo, cc.dg_cuenta_contable',
                            't.dc_tipo_nota_credito = ?'));

    $select->bindValue(1, $this->tipo->dc_tipo, PDO::PARAM_INT);
    $db->stExec($select);

    $data = $select->fetchAll(PDO::FETCH_OBJ);

    if(count($data) == 0):
      $this->getErrorMan()->showErrorRedirect(
        "El tipo de Nota de crédito requiere tener al menos una cuenta contable para poder ser utilizada.",
        "sites/ventas/nota_credito_proveedor/cr_nota_credito.php"
      );
      exit;
    endif;

    $this->cuentas_contables = array();
    while($cc = array_shift($data)):
      $this->cuentas_contables[$cc->dc_cuenta_contable] = $cc->dg_cuenta_contable;
    endwhile;

  }

  private function initBodegasDetalle($r){

    if(!isset($r->dc_bodega) or count($r->dc_bodega) != count($this->detalles)):
      $this->getErrorMan()->showWarning("No se han indicado bodegas o estas no corresponden a los detalles que fueron ingresados,".
                                        " verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    foreach($r->dc_bodega as &$b):
      if(empty($b) or !preg_match('/^\d{1,11}$/',$b)):
        $this->getErrorMan()->showWarning("Una de las bodegas indicadas es inválida,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;
    endforeach;

    $db = $this->getConnection();
    foreach($r->dc_bodega as $i => &$b):
      $bodega = $db->getRowById('tb_bodega',$b,'dc_bodega');

      if($bodega === false or $bodega->dm_activo == 0):
        $this->getErrorMan()->showWarning("No se encontró una de las bodegas o esta fue eliminada.".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;

      $this->detalles[$i]->bodega = $bodega;

    endforeach;

  }

  private function initCuentasContablesCentralizacionDetalles($r){

    if(!isset($r->dc_cuenta_contable) or count($r->dc_cuenta_contable) != count($this->detalles)):
      $this->getErrorMan()->showWarning("No se han indicado cuentas contables o estas no corresponden a los detalles que fueron ingresados,".
                                        " verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    foreach($r->dc_cuenta_contable as &$cc):
      if(empty($cc) or !preg_match('/^\d{1,11}$/',$cc)):
        $this->getErrorMan()->showWarning("Uno de las cuentas contables indicadas es inválida,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;
    endforeach;

    $db = $this->getConnection();
    foreach($r->dc_cuenta_contable as $i => &$cc):
      $cuenta = $db->getRowById('tb_cuenta_contable',$cc,'dc_cuenta_contable');

      if($cuenta === false or $cuenta->dm_activo == 0):
        $this->getErrorMan()->showWarning("No se encontró una de las cuentas contables o esta fue eliminada.".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;

      $this->detalles[$i]->cuenta_contable = $cuenta;

    endforeach;

  }

  private function initTipoNotaCredito($r){
    if(!isset($r->dc_tipo) or !preg_match('/^\d+$/',$r->dc_tipo)):
      $this->getErrorMan()->showWarning("El tipo en la Nota de crédito es inválido y debe ser revisado,".
                                        "favor verificar los datos de entrada y volver a intentarlo.");
      exit;
    endif;

    $db = $this->getConnection();
    $this->tipo = $db->getRowById('tb_tipo_nota_credito_proveedor',$r->dc_tipo,'dc_tipo');

    if($this->tipo === false or $this->tipo->dm_activo == 0):
      $this->getErrorMan()->showWarning("El tipo de nota de crédito indicado no fue encontrado o este fue eliminado,".
                                        "favor veriicar los datos de entrada y volver a intentarlo");
      exit;
    endif;

  }

  private function initDetalles($r){
    $this->initDetailsObject($r);
    $this->initCantidades($r);
    $this->initPrecios($r);
    $this->initProductos($r);
    if($this->tipo->dm_retorna_stock == 0):
      $this->initCuentasContablesCentralizacionDetalles($r);
    else:
      $this->initBodegasDetalle($r);
    endif;
  }

  private function initDetailsObject($r){
    $this->detalles = array();

    if(
        !isset($r->prod) or empty($r->prod) //Se agregaron productos?
        or !isset($r->desc) or empty($r->desc) //Fueron especificados las descripciones?
        or count($r->prod) != count($r->desc) //La cantidad de productos es la misma que la de descripciones?
      ):
      $this->getErrorMan()->showWarning("No se han encontrado detalles en la nota de crédito".
                                        "verificar los datos de entrada y volver a intentarlo");
      exit;
    endif;

    foreach($r->prod as $i => &$p):
      $this->detalles[$i] = (object)array(
        "dg_codigo" => $p,
        "dg_descripcion" => $r->desc[$i],
        "despachos" => array()
      );
    endforeach;

  }

  private function initCantidades($r){

    if(!isset($r->cant) or empty($r->cant) or count($r->cant) != count($this->detalles)):
      $this->getErrorMan()->showWarning("Las cantidades indicadas son inválidas o estas no fueron indicadas,".
                                        "verificar los datos de entrada y volver a intentarlo.");
      exit;
    endif;

    foreach($r->cant as &$c):
      if(empty($c) or !preg_match("/^\d{1,11}$/",$c)):
          $this->getErrorMan()->showWarning("<strong>".htmlentities($c)."</strong> No es una cantidad válida y debe ser corregida,".
                                            "verificar los datos y volver a intentarlo.");
          exit;
      endif;
    endforeach;

    foreach($r->cant as $i => &$c):
      $this->detalles[$i]->dc_cantidad = intval($c);
    endforeach;

  }

  private function initPrecios($r){

    //Verificar que el array de precios exista y no sea vacio
    if(!isset($r->precio) or empty($r->precio) or count($r->precio) != count($this->detalles)){
      $this->getErrorMan()->showWarning("Se deben especificar los detalles con precio para poder".
                                        "continuar con la creación de la nota de crédito,".
                                        "verificar los datos de entrada y volver a intentarlo");
      exit;
    }

    foreach($r->precio as &$p):
      //Verificar que el precio no sea vacío y posea el formato correcto (p.e: "100,000.28"; "999.12"; "1,000,000"; "999")
      if(empty($p) or !preg_match("/^(\d+,)*\d+(\.\d+)?$/",$p)):
        $this->getErrorMan()->showWarning("El precio en uno de los detalles es inválido,".
                                          "Error encontrado en el precio: <strong>".htmlentities($p)."</strong>");
        exit;
      endif;
    endforeach;

    foreach($r->precio as $i => &$p):
      //Quitar comas separadoras de mil y convertir al tipo float el número
      $this->detalles[$i]->dq_precio_unitario = floatval(str_replace(',','',$p));
      $this->detalles[$i]->dq_precio_total = $this->detalles[$i]->dq_precio_unitario * $this->detalles[$i]->dc_cantidad;
    endforeach;

  }

  private function initProductos($r){
    $db = $this->getConnection();

    $dg_codigo = null;
    $select = $db->prepare($db->select('tb_producto','*','dg_codigo = ? AND dc_empresa = ?'));
    $select->bindParam(1, $dg_codigo, PDO::PARAM_STR);
    $select->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);

    foreach($this->detalles as $i => &$d):
        $dg_codigo = $d->dg_codigo;
        $db->stExec($select);

        $producto = $select->fetchAll(PDO::FETCH_OBJ);

        if(count($producto) != 1):
          $this->getErrorMan()->showWarning("El código de producto <strong>".htmlentities($dg_codigo)."<strong>".
                                            "no ha sido encontrado o es ambiguo,".
                                            "verifique los datos de entrada y vuelva a intentarlo");
          exit;
        endif;

        $this->detalles[$i]->producto = array_shift($producto);

    endforeach;

    $this->initTiposProducto();

  }



  private function initTiposProducto(){

    $db = $this->getConnection();

    foreach($this->detalles as $i => &$d):

      $tipo = $db->getRowById('tb_tipo_producto',$this->detalles[$i]->producto->dc_tipo_producto,'dc_tipo_producto');

      if($tipo === false):
        $this->getErrorMan()->showWarning("No se logró encontrar el tipo de producto asociado al detalle".
                                          "<strong>".htmlentities($this->detalles[$i]->dg_descripcion)."</strong>");
        exit;
      endif;

      if($this->tipo->dm_retorna_stock == 1 and $tipo->dm_controla_inventario == 1):
        $this->getErrorMan()->showWarning("No puede incluir productos que no muevan stock en Notas de crédito con tipo".
                                          " <strong>{$this->tipo->dg_tipo}</strong>.<br /> Error encontrado en el producto ".
                                          " <strong>{$this->detalles[$i]->producto->dg_producto}</strong>");
        exit;
      elseif($this->tipo->dm_retorna_stock == 0 and $tipo->dm_controla_inventario == 0):
        $this->getErrorMan()->showWarning("No puede incluir productos que muevan stock en Notas de Crédito con tipo".
                                          " <strong>{$this->tipo->dg_tipo}</strong><br /> Error encontrado en el producto".
                                          " <strong>{$this->detalles[$i]->producto->dg_producto}</strong>");
        exit;
      endif;

      $this->detalles[$i]->producto->tipo = $tipo;
    endforeach;
  }

  private function initTotales($r){
    if(!isset($r->dq_neto) or !preg_match("/^(\d+,)*\d+(\.\d+)?$/",$r->dq_neto)):
      $this->getErrorMan()->showWarning("El precio neto indicado no posee un formato válido,".
                                        "verificar los datos de entrada y volver a intentar.");
      exit;
    endif;
    $this->dq_neto = floatval(str_replace(',','',$r->dq_neto));

    if(!isset($r->dq_exento) or !preg_match("/^(\d+,)*\d+(\.\d+)?$/",$r->dq_exento)):
      $this->getErrorMan()->showWarning("El precio exento indicado no posee un formato válido,".
                                        "verificar los datos de entrada y volver a intentar.");
      exit;
    endif;
    $this->dq_exento = floatval(str_replace(',','',$r->dq_exento));

    if(!isset($r->dq_iva) or !preg_match("/^(\d+,)*\d+(\.\d+)?$/",$r->dq_iva)):
      $this->getErrorMan()->showWarning("El precio iva indicado no posee un formato válido,".
                                        "verificar los datos de entrada y volver a intentar.");
      exit;
    endif;
    $this->dq_iva = floatval(str_replace(',','',$r->dq_iva));

    if(!isset($r->dq_total) or !preg_match("/^(\d+,)*\d+(\.\d+)?$/",$r->dq_total)):
      $this->getErrorMan()->showWarning("El precio total indicado no posee un formato válido,".
                                        "verificar los datos de entrada y volver a intentar.");
      exit;
    endif;
    $this->dq_total = floatval(str_replace(',','',$r->dq_total));

  }

  private function insertarCabeceraNotaCredito(){
    $db = $this->getConnection();
    $doc = $this->getService('DocumentCreation');
    $this->dq_nota_credito = $doc->getCorrelativo('tb_nota_credito_proveedor','dq_nota_credito');
    $r = self::getRequest();

    $insertar = $db->prepare($db->insert('tb_nota_credito_proveedor',array(
      'dq_nota_credito' => $this->dq_nota_credito,
    	'dq_folio' => '?',
    	'dc_tipo' => '?',
    	'dc_medio_pago' => '?',
    	'dg_comentario' => '?',
    	'df_emision' => '?',
    	'df_vencimiento' => '?',
    	'dc_proveedor' => '?',
    	'df_libro_compra' => '?',
    	'dq_neto' => '?',
    	'dq_exento' => '?',
    	'dq_iva' => '?',
    	'dq_total' => '?',
    	'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
    	'df_creacion' => $db->getNow(),
    	'dc_empresa' => $this->getEmpresa()
    )));

    $insertar->bindValue(1, $r->dq_folio, PDO::PARAM_INT);
    $insertar->bindValue(2, $this->tipo->dc_tipo, PDO::PARAM_INT);
    $insertar->bindValue(3, $r->fv_medio_pago, PDO::PARAM_INT);
    $insertar->bindValue(4, $r->dg_comentario, PDO::PARAM_STR);
    $insertar->bindValue(5, $db->sqlDate2($r->df_emision), PDO::PARAM_STR);
    $insertar->bindValue(6, $db->sqlDate2($r->df_vencimiento),PDO::PARAM_STR);
    $insertar->bindValue(7, $this->proveedor->dc_proveedor, PDO::PARAM_INT);
    $insertar->bindValue(8, $db->sqlDate2($r->df_libro_compra),PDO::PARAM_STR);
    $insertar->bindValue(9, $this->dq_neto, PDO::PARAM_STR);
    $insertar->bindValue(10, $this->dq_exento, PDO::PARAM_STR);
    $insertar->bindValue(11, $this->dq_iva, PDO::PARAM_STR);
    $insertar->bindValue(12, $this->dq_total, PDO::PARAM_STR);

    $db->stExec($insertar);

    $this->dc_nota_credito = $db->lastInsertId();

  }

  private function insertarDetallesNotaCredito(){
    $db = $this->getConnection();

    $insert_detalle = $db->prepare($db->insert('tb_nota_credito_proveedor_detalle',array(
    	'dc_nota_credito' => $this->dc_nota_credito,
    	'dg_producto' => '?',
    	'dg_descripcion' => '?',
    	'dq_cantidad' => '?',
    	'dq_precio' => '?',
      'dc_cuenta_contable_centralizacion' => '?',
      'dc_bodega' => '?'
    )));
    $insert_detalle->bindParam(1,$dg_producto,PDO::PARAM_STR);
    $insert_detalle->bindParam(2,$dg_descripcion,PDO::PARAM_STR);
    $insert_detalle->bindParam(3,$dq_cantidad,PDO::PARAM_INT);
    $insert_detalle->bindParam(4,$dq_precio,PDO::PARAM_STR);

    if($this->tipo->dm_retorna_stock == 0):
      $insert_detalle->bindParam(5, $dc_cuenta_contable_centralizacion, PDO::PARAM_INT);
      $insert_detalle->bindValue(6, NULL, PDO::PARAM_NULL);
    else:
      $insert_detalle->bindValue(5, NULL, PDO::PARAM_NULL);
      $insert_detalle->bindParam(6, $dc_bodega, PDO::PARAM_INT);
    endif;

    foreach($this->detalles as $d):

      $dg_producto = $d->producto->dg_codigo;
      $dg_descripcion = $d->dg_descripcion;
      $dq_cantidad = $d->dc_cantidad;
      $dq_precio = $d->dq_precio_unitario;

      if($this->tipo->dm_retorna_stock == 0):
        $dc_cuenta_contable_centralizacion = $d->cuenta_contable->dc_cuenta_contable;
      else:
        $dc_bodega = $d->bodega->dc_bodega;
      endif;

      $db->stExec($insert_detalle);

    endforeach;

  }

}
