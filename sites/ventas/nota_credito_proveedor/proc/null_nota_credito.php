<?php
define('MAIN',1);
include('../../../../inc/init.php');

$dc_nota_credito = intval($_POST['dc_nota_credito']);

//Obtener los datos de la nota de crédito
$nota_credito = $db->prepare($db->select('tb_nota_credito_proveedor','dq_nota_credito, dm_centralizada, dc_tipo, dq_total, dq_saldado, dm_nula','dc_nota_credito = ?'));
$nota_credito->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$db->stExec($nota_credito);
$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);

if($nota_credito === false){
	$error_manr->showWarning('La nota de crédito es inválida, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Comprobar que no esté ya nula
if($nota_credito->dm_nula == 1){
	$error_man->showWarning("La nota de crédito ya ha sido anulada anteriormente");
	exit;
}

//Comprobar centralización
if($nota_credito->dm_centralizada == 1){
	$error_man->showWarning("La nota de crédito ha sido incluida en una centralización, se debe anular los comprobantes de centralización antes");
	exit;
}

//Comprobar nota de crédito sin saldos utilizados
if($nota_credito->dq_saldado > 0){
	$error_man->showWarning('La nota de crédito ha sido utilizada para realizar pagos de facturas, libere de pagos estas facturas para poder anular la orden de compra');
	exit;
}

/*Obtener los datos del tipo de nota de crédito
$tipo_nota_credito = $db->prepare($db->select('tb_tipo_nota_credito_proveedor','dm_retorna_stock, dm_multiple_factura','dc_tipo = ?'));
$tipo_nota_credito->bindValue(1,$nota_credito->dc_tipo,PDO::PARAM_INT);
$db->stExec($tipo_nota_credito);
$tipo_nota_credito = $tipo_nota_credito->fetch(PDO::FETCH_OBJ);

if($tipo_nota_credito === false){
	$error_man->showWarning('Ha ocurrido un error recuperando la información del tipo de documento, no se puede continuar.');
	exit;
}*/

$db->start_transaction();

//Modificar estado de la nota de crédito
$update_nota_credito = $db->prepare($db->update('tb_nota_credito_proveedor',array('dm_nula' => 1),'dc_nota_credito = ?'));
$update_nota_credito->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$db->stExec($update_nota_credito);

//Almacenar log de la anulación
$insert_log = $db->prepare($db->insert('tb_nota_credito_proveedor_anulada',array(
	'dc_nota_credito' => '?',
	'dc_motivo' => '?',
	'dg_comentario' => '?',
	'dc_usuario' => $idUsuario,
	'df_anulacion' => $db->getNow()
)));
$insert_log->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$insert_log->bindValue(2,$_POST['dc_motivo'],PDO::PARAM_INT);
$insert_log->bindValue(3,$_POST['dg_comentario'],PDO::PARAM_STR);
$db->stExec($insert_log);

$db->commit();

$error_man->showAviso('Atención: ha anulado la nota de crédito <b>'.$nota_credito->dq_nota_credito.'</b>');

?>