<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once('../../proc/ventas_functions.php');

$dc_proveedor = intval($_POST['dc_proveedor']);
$dc_tipo = intval($_POST['dc_tipo_nota_credito']);

//Formatear montos como número
$dq_precio_form = array();
$dq_precio_cant = array();
foreach($_POST['precio'] as $i => $p):
	$dq_precio_form[$i] = floatval(str_replace(',','',$p));
	$dq_precio_cant[] = $dq_precio_form[$i]*intval($_POST['cant'][$i]);
endforeach;

//Validar Monto propuesto igual que monto total Nota de Crédito
if(isset($_POST['dq_monto_propuesto']))
	$dq_monto_propuesto = floatval($_POST['dq_monto_propuesto']);
$dq_total_neto = floatval(str_replace(',','',$_POST['dq_neto']));
$dq_exento = floatval(str_replace(',','',$_POST['dq_exento']));
$dq_iva = floatval(str_replace(',','',$_POST['dq_iva']));
$dq_total_nota_credito = floatval(str_replace(',','',$_POST['dq_total']));

/*if((!$dq_total_neto || !$dq_iva || !$dq_total_nota_credito) && !$dq_exento){
	$error_man->showWarning('El formato de los totales en la nota de crédito es incorrecto, corrija los datos y vuelva a intentarlo.');
	exit;
}/**/

/*if(isset($_POST['dq_monto_propuesto']) && $dq_monto_propuesto > $dq_total_nota_credito){
  $error_man->showWarning('El monto propuesto no puede ser superior al monto que incluyó en los detalles de la nota de crédito. compruebe los valores y vuelva a intentarlo.');
  exit;
}/**/

//Obtener las opciones asociadas al tipo de nota de crédito
$tipo_nota_credito = $db->prepare(
	$db->select(
		'tb_tipo_nota_credito_proveedor',
		'dc_tipo, dg_tipo, dm_retorna_stock, dm_multiple_factura',
		'dc_tipo = ? AND dc_empresa = ?'
	)
);
$tipo_nota_credito->bindValue(1,$dc_tipo,PDO::PARAM_INT);
$tipo_nota_credito->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($tipo_nota_credito);
$tipo_nota_credito = $tipo_nota_credito->fetch(PDO::FETCH_OBJ);

//Validar tipo de nota de crédito con datos de entrada
/*
*	- Sin devolución y FV múltiple debe tener al menos 1 factura
*	- Con devolución de stock, validar que el stock esté disponible
*/
if($tipo_nota_credito->dm_retorna_stock == 0 && $tipo_nota_credito->dm_multiple_factura == 1):
	
	if(!isset($_POST['dc_factura']) || !isset($_POST['dq_monto_pagar'])){
		$error_man->showAviso('Debe seleccionar al menos una factura de compra para este tipo de nota de crédito');
		exit;
	}
	
endif;

if($tipo_nota_credito->dm_retorna_stock == 1):
	if($_POST['gdp']=='si'){
	if(!isset($_POST['dc_guia_despacho'])){
		$error_man->showWarning('Debe seleccionar guías de despacho asociadas a la nota de crédito para poder continuar.');
		exit;
	}
        }

	/*if(!isset($_POST['dc_factura_simple']) && intval($_POST['dc_factura_simple']) > 0){
		$error_man->showWarning('Debe seleccionar una factua de compra para hacer el retorno de stock');
		exit;
	}
	$dc_factura = intval($_POST['dc_factura_simple']);
	
	$questions = substr(str_repeat(',?',count($_POST['prod'])),1);
	$search_stock = $db->prepare($db->select('tb_producto p
	JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
	'dc_producto, dg_codigo',
	'p.dc_empresa = '.$empresa.' AND p.dm_activo = 1 AND p.dg_codigo IN ('.$questions.') AND t.dm_controla_inventario = 0'));
	
	$prod_cant = array();
	
	$i = 1;
	foreach($_POST['prod'] as $index => $p):
		$search_stock->bindValue($i,$p,PDO::PARAM_STR);
		$i++;
		
		//Rescatar la cantidad por código en el detalle
		$prod_cant[$p]  = intval($_POST['cant'][$index]);
	endforeach;
	
	$db->stExec($search_stock);
	
	//Estructura de datos para los productos que mueven stock
	$prod_data = array();
	while($s = $search_stock->fetch(PDO::FETCH_OBJ)):
		$prod_data[$s->dc_producto]['dg_codigo'] = $s->dg_codigo;
		$prod_data[$s->dc_producto]['dq_cantidad'] = $prod_cant[$s->dg_codigo];
	endwhile;
	
	//Obtener los datos de la factura de compra
	$factura = $db->prepare($db->select('tb_factura_compra','dc_guia_recepcion','dc_factura = ? AND dc_empresa = ?'));
	$factura->bindValue(1,$dc_factura,PDO::PARAM_INT);
	$factura->bindValue(2,$empresa,PDO::PARAM_INT);
	$db->stExec($factura);
	$factura = $factura->fetch(PDO::FETCH_OBJ);
	
	//Factura no encontrada
	if($factura === false){
		$error_man->showWarning('La Factura de compra que seleccionó no ha sido encontrada.');
		exit;
	}
	
	//Factura sin guía de recepción
	if($factura->dc_guia_recepcion == 0){
		$error_man->showWarning('La factura de compra seleccionada no ha sido vinculada a una guía de recepción para poder hacer un retorno de stock.');
		exit;
	}
	
	//Obtener los datos de la guía de recepción
	$guia_recepcion = $db->prepare($db->select('tb_guia_recepcion','dc_nota_venta,dc_orden_compra','dc_guia_recepcion = ? AND dc_empresa = ?'));
	$guia_recepcion->bindValue(1,$factura->dc_guia_recepcion,PDO::PARAM_INT);
	$guia_recepcion->bindValue(2,$empresa,PDO::PARAM_INT);
	$db->stExec($guia_recepcion);
	$guia_recepcion = $guia_recepcion->fetch(PDO::FETCH_OBJ);
	
	//Guía de recepción no encontrada
	if($guia_recepcion === false){
		$error_man->showWarning('No se ha encontrado la Guía de recepción vinculada a la factura, compruebe que sea valida y vuelva a intentarlo.');
		exit;
	}
	
	//Tipo de retorno: Libre o Reservada
	if($guia_recepcion->dc_nota_venta == 0){
		$tipo_retorno = 'L';
		
		//Traer detalles de guía de recepción para reflejarlos en los de la nota de crédito
		$detalle_gr = $db->prepare($db->select('tb_guia_recepcion_detalle d
		LEFT JOIN tb_stock s ON s.dc_producto = d.dc_producto AND s.dc_bodega = d.dc_bodega',
		'd.dc_producto, d.dc_bodega, (s.dq_stock - s.dq_stock_reservado - s.dq_stock_reservado_os) dq_stock_libre',
		'd.dc_guia_recepcion = ?'));
		$detalle_gr->bindValue(1,$factura->dc_guia_recepcion,PDO::PARAM_INT);
		$db->stExec($detalle_gr);
		
		//Para cada detalle de la guía de recepción comprobar si está en la nota de crédito y validar stock libre.
		$count_coincidences = 0;
		while($d = $detalle_gr->fetch(PDO::FETCH_OBJ)):
			if(isset($prod_data[$d->dc_producto])){
				$count_coincidences++;
				if($prod_data[$d->dc_producto]['dq_cantidad'] > intval($d->dq_stock_libre)){
					
					$bodega = $db->prepare($db->select('tb_bodega','dg_bodega','dc_bodega = ?'));
					$bodega->bindValue(1,$d->dc_bodega,PDO::PARAM_INT);
					$db->stExec($bodega);
					$bodega = $bodega->fetch(PDO::FETCH_OBJ)->dg_bodega;
					
					$error_man->showWarning('No hay stock libre suficiente para quitar de la bodega.<br />
					<ul>
						<li>Problemas con el producto: <b>'.$prod_data[$d->dc_producto]['dg_codigo'].'</b></li>
						<li>En la bodega: <b>'.$bodega.'</b></li>
					</ul>');
					exit;
				}
				
				$prod_data[$d->dc_producto]['dc_bodega'] = $d->dc_bodega;
				
			}
		endwhile;
		
	}else{
		$tipo_retorno = 'R';
		
		//Traer detalles de guía de recepción junto a los de la nota de venta correspondiente para reflejarlos en los de la nota de crédito
		$detalle_gr = $db->prepare($db->select('tb_guia_recepcion_detalle d
		JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
		LEFT JOIN tb_stock s ON s.dc_producto = d.dc_producto AND s.dc_bodega = d.dc_bodega',
		'd.dc_producto, d.dc_bodega, s.dq_stock_reservado, (nvd.dc_recepcionada-nvd.dc_despachada) dq_stock_ocupado',
		'd.dc_guia_recepcion = ?'));
		$detalle_gr->bindValue(1,$factura->dc_guia_recepcion,PDO::PARAM_INT);
		$db->stExec($detalle_gr);
		
		//Para cada detalle de la nota de crédito validar que esté disponible el stock reservado y que además no se haya ocupado para despacho.
		$count_coincidences = 0;
		while($d = $detalle_gr->fetch(PDO::FETCH_OBJ)):
			if(isset($prod_data[$d->dc_producto])){
				$count_coincidences++;
				
				//
				if(
					$prod_data[$d->dc_producto]['dq_cantidad'] > intval($d->dq_stock_reservado) ||
					$prod_data[$d->dc_producto]['dq_cantidad'] > intval($d->dq_stock_ocupado)
				){
					
					$bodega = $db->prepare($db->select('tb_bodega','dg_bodega','dc_bodega = ?'));
					$bodega->bindValue(1,$d->dc_bodega,PDO::PARAM_INT);
					$db->stExec($bodega);
					$bodega = $bodega->fetch(PDO::FETCH_OBJ)->dg_bodega;
					
					$error_man->showWarning('No se puede devolver el stock de la factura, estas son las posibles razones:<br />
					<ul>
						<li>No hay stock reservado suficiente en la bodega</li>
						<li>Los productos en la nota de venta estaban disponibles pero fueron despachados.</li>
						<li>El problema se produjo en el producto: <b>'.$prod_data[$d->dc_producto]['dg_codigo'].'</b></li>
						<li>En la bodega: '.$bodega.'</li>
					</ul>');
					exit;
				}
				
				$prod_data[$d->dc_producto]['dc_bodega'] = $d->dc_bodega;
				
			}
		endwhile;
		
	}
	
	//Comprobar que todos los productos estén disponibles en la guía de recepción.
	if($count_coincidences != count($prod_data)){
		$error_man->showWarning('Hay productos que no están disponibles en el detalle de la nota de crédito,
		esto es debido a que la guía de recepción ligada no posee alguno de los productos del detalle.');
		exit;
	}*/
	
endif;

//Calcular Monto saldado
/**
*	- En caso de ser multi factura, se debe calcular a partir de los montos pagados para esa factura (traidos en el $_POST)
*	- En caso de ser de factura simple, se trae el monto no pagado de la factura y se diferencia con el monto total de la nota de crédito
*	- En otro caso el monto saldado es saldado
**/
if($tipo_nota_credito->dm_multiple_factura == 1){
	$saldar = floatval(array_sum($_POST['dq_monto_pagar']));
}else if(isset($_POST['dc_factura_simple']) && intval($_POST['dc_factura_simple']) > 0){
	$dc_factura = intval($_POST['dc_factura_simple']);
	$factura_pago = $db->prepare($db->select('tb_factura_compra','(dq_total-dq_monto_pagado) dq_monto_pagar','dc_factura = ? AND dc_empresa = ?'));
	$factura_pago->bindValue(1,$dc_factura,PDO::PARAM_INT);
	$factura_pago->bindValue(2,$empresa,PDO::PARAM_INT);
	$db->stExec($factura_pago);
	
	$factura_pago = $factura_pago->fetch(PDO::FETCH_OBJ);
	
	if($factura_pago === false){
		$error_man->showWarning('No se ha encontrado la factura que ha especificado para pagar, compruebe la información de entrada y vuelva a intentarlo.');
		exit;
	}
	
	$saldar = floatval($factura_pago->dq_monto_pagar);
	
	if($saldar > $dq_total_nota_credito){
		$saldar = $dq_total_nota_credito;
	}
}else{
	$saldar = 0.0;
}

//Iniciar transacción en la base de datos
$db->start_transaction();

//Almacenar cabecera Nota de crédito
$dq_nota_credito = doc_GetNextNumber('tb_nota_credito_proveedor','dq_nota_credito',2);
$insert_nota_credito = $db->prepare($db->insert('tb_nota_credito_proveedor',array(
	'dq_nota_credito' => $dq_nota_credito,
	'dq_folio' => '?',
	'dc_tipo' => '?',
	'dc_medio_pago' => '?',
	'dg_comentario' => '?',
	'df_emision' => '?',
	'df_vencimiento' => '?',
	'dq_saldado' => '?',
	'dc_proveedor' => '?',
	'df_libro_compra' => '?',
	'dq_neto' => $dq_total_neto,
	'dq_exento' => $dq_exento,
	'dq_iva' => $dq_iva,
	'dq_total' => $dq_total_nota_credito,
	'dc_usuario_creacion' => $idUsuario,
	'df_creacion' => $db->getNow(),
	'dc_empresa' => $empresa
)));
$insert_nota_credito->bindValue(1,$_POST['nc_number'],PDO::PARAM_STR);
$insert_nota_credito->bindValue(2,$dc_tipo,PDO::PARAM_INT);
$insert_nota_credito->bindValue(3,$_POST['fv_medio_pago'],PDO::PARAM_INT);
$insert_nota_credito->bindValue(4,$_POST['fv_comentario'],PDO::PARAM_STR);
$insert_nota_credito->bindValue(5,$db->sqlDate2($_POST['fv_emision']),PDO::PARAM_STR);
$insert_nota_credito->bindValue(6,$db->sqlDate2($_POST['fv_vencimiento']),PDO::PARAM_STR);
$insert_nota_credito->bindValue(7,$saldar,PDO::PARAM_STR);
$insert_nota_credito->bindValue(8,$_POST['dc_proveedor'],PDO::PARAM_INT);
$insert_nota_credito->bindValue(9,$db->sqlDate2($_POST['df_libro_compra']),PDO::PARAM_STR);

$db->stExec($insert_nota_credito);

$dc_nota_credito = $db->lastInsertId();

//Almacenar Detalle Nota de crédito
$insert_detalle = $db->prepare($db->insert('tb_nota_credito_proveedor_detalle',array(
	'dc_nota_credito' => $dc_nota_credito,
	'dg_producto' => '?',
	'dg_descripcion' => '?',
	'dq_cantidad' => '?',
	'dq_precio' => '?'
)));
$insert_detalle->bindParam(1,$dg_producto,PDO::PARAM_STR);
$insert_detalle->bindParam(2,$dg_descripcion,PDO::PARAM_STR);
$insert_detalle->bindParam(3,$dq_cantidad,PDO::PARAM_INT);
$insert_detalle->bindParam(4,$dq_precio,PDO::PARAM_STR);

foreach($_POST['prod'] as $i => $p){
	$dg_producto = $p;
	$dg_descripcion = $_POST['desc'][$i];
	$dq_cantidad = $_POST['cant'][$i];
	$dq_precio = $dq_precio_form[$i];
	
	$db->stExec($insert_detalle);
}

//Rebajar stock en Nota de Crédito simple con retorno
if($tipo_nota_credito->dm_retorna_stock == 1):

	$update_guia_despacho = $db->prepare($db->update('tb_guia_despacho_proveedor',array(
		'dc_nota_credito' => $dc_nota_credito
	),'dc_guia_despacho = ?'));
	$update_guia_despacho->bindParam(1,$dc_guia_despacho,PDO::PARAM_INT);
	if($_POST['gdp']=='si'){
	foreach($_POST['dc_guia_despacho'] as $gd){
		$dc_guia_despacho = $gd;
		
		$db->stExec($update_guia_despacho);
	}
        }

	/**$dc_tipo_movimiento = $db->doQuery(
			$db->select('tb_configuracion_logistica','dc_tipo_movimiento_salida_nota_credito_proveedor dc_tipo_movimiento','dc_empresa = '.$empresa)
		)->fetch(PDO::FETCH_OBJ)->dc_tipo_movimiento;

	$movimiento_bodega = $db->prepare($db->insert('tb_movimiento_bodega',array(
		'dc_proveedor' => '?',
		'dc_tipo_movimiento' => '?',
		'dc_bodega_salida' => '?',
		'dc_guia_recepcion' => '?',
		'dc_orden_compra' => '?',
		'dc_factura' => '?',
		'dc_nota_venta' => '?',
		'dc_producto' => '?',
		'dq_cantidad' => '?',
		'dc_empresa' => '?',
		'df_creacion' => $db->getNow(),
		'dc_usuario_creacion' => '?'
	)));
	$movimiento_bodega->bindValue(1,$dc_proveedor,PDO::PARAM_INT);
	$movimiento_bodega->bindValue(2,$dc_tipo_movimiento,PDO::PARAM_INT);
	$movimiento_bodega->bindParam(3,$dc_bodega,PDO::PARAM_INT);
	$movimiento_bodega->bindValue(4,$factura->dc_guia_recepcion,PDO::PARAM_INT);
	$movimiento_bodega->bindValue(5,$guia_recepcion->dc_orden_compra,PDO::PARAM_INT);
	$movimiento_bodega->bindValue(6,$dc_factura,PDO::PARAM_INT);
	$movimiento_bodega->bindValue(7,$guia_recepcion->dc_nota_venta,PDO::PARAM_INT);
	$movimiento_bodega->bindParam(8,$dc_producto,PDO::PARAM_INT);
	$movimiento_bodega->bindParam(9,$dq_cantidad,PDO::PARAM_INT);
	$movimiento_bodega->bindValue(10,$empresa,PDO::PARAM_INT);
	$movimiento_bodega->bindValue(11,$idUsuario,PDO::PARAM_INT);

	if($tipo_retorno == 'L'):
		foreach($prod_data as $dc_producto => $p){
			$moved = bodega_RebajarStockLibre($dc_producto,$p['dq_cantidad'],$p['dc_bodega']);
			if($moved != 1){
				
				$bodega = $db->prepare($db->select('tb_bodega','dg_bodega','dc_bodega = ?'));
				$bodega->bindValue(1,$p['dc_bodega'],PDO::PARAM_INT);
				$db->stExec($bodega);
				$bodega = $bodega->fetch(PDO::FETCH_OBJ)->dg_bodega;
				
				$error_man->showWarning('Lo sentimos, el stock que se iba a rebajar fue utilizado mientras se intentaba guardar la nota de crédito.<br />
						<ul>
							<li>Problemas con el producto: <b>'.$p['dg_codigo'].'</b></li>
							<li>En la bodega: <b>'.$bodega.'</b></li>
						</ul>');
				$db->rollBack();
				exit;
			}
			
			$dc_bodega = $p['dc_bodega'];
			$dq_cantidad = $p['dq_cantidad'];
			$db->stExec($movimiento_bodega);
		}
	elseif($tipo_retorno == 'R'):
		foreach($prod_data as $dc_producto => $p){
			$moved = bodega_RebajarStockReservado($dc_producto,$p['dq_cantidad'],$p['dc_bodega']);
			
			if($moved != 1){
				
				$bodega = $db->prepare($db->select('tb_bodega','dg_bodega','dc_bodega = ?'));
				$bodega->bindValue(1,$p['dc_bodega'],PDO::PARAM_INT);
				$db->stExec($bodega);
				$bodega = $bodega->fetch(PDO::FETCH_OBJ)->dg_bodega;
				
				$error_man->showWarning('Lo sentimos, el stock que se iba a rebajar fue utilizado mientras se intentaba guardar la nota de crédito.<br />
						<ul>
							<li>Problemas con el producto: <b>'.$p['dg_codigo'].'</b></li>
							<li>En la bodega: <b>'.$bodega.'</b></li>
						</ul>');
				$db->rollBack();
				exit;
			}
			
			$dc_bodega = $p['dc_bodega'];
			$dq_cantidad = $p['dq_cantidad'];
			$db->stExec($movimiento_bodega);
		}
	else:
		$db->rollBack();
		exit;
	endif;*/
endif; //Fin rebajar stock

//Rebajar Pagos en factura de compra
$saldar_factura = $db->prepare($db->update('tb_factura_compra',array(
	'dq_monto_pagado' => 'dq_monto_pagado+?'
),'dc_factura = ?'));
$saldar_factura->bindParam(1,$dq_monto_pagado,PDO::PARAM_STR);
$saldar_factura->bindParam(2,$dc_factura,PDO::PARAM_INT);

$relacionar_factura = $db->prepare($db->insert('tb_nota_credito_proveedor_factura',array(
	'dc_factura' => '?',
	'dc_nota_credito' => $dc_nota_credito,
	'dq_monto_saldado' => '?'
)));
$relacionar_factura->bindParam(1,$dc_factura,PDO::PARAM_INT);
$relacionar_factura->bindParam(2,$dq_monto_pagado,PDO::PARAM_STR);

if($tipo_nota_credito->dm_multiple_factura == 1){
	foreach($_POST['dc_factura'] as $i => $f){
		$dq_monto_pagado = floatval($_POST['dq_monto_pagar'][$i]);
		$dc_factura = intval($f);
		
		$db->stExec($saldar_factura);
		//Almacenar relación con facturas de compra
		$db->stExec($relacionar_factura);
	}
}else if(isset($_POST['dc_factura_simple']) && intval($_POST['dc_factura_simple']) > 0){
	$dq_monto_pagado = $saldar;
	$dc_factura = intval($_POST['dc_factura_simple']);
	
	$db->stExec($saldar_factura);
	//Almacenar relación con facturas de compra
	$db->stExec($relacionar_factura);
}

if($saldar < $dq_total_nota_credito){
	$update_proveedor = $db->prepare($db->update('tb_proveedor',array(
		'dq_saldo_favor' => 'dq_saldo_favor+?'
	),'dc_proveedor = ?'));
	$update_proveedor->bindValue(1,floatval($dq_total_nota_credito-$saldar),PDO::PARAM_STR);
	$update_proveedor->bindValue(2,$dc_proveedor,PDO::PARAM_INT);
	
	$db->stExec($update_proveedor);
}

/**
*	Ingresar comprobante contable "proveedor" -> "proveedor"
*	Esto solo en caso de facturación múltiple con ingreso de pagos
*/
if($tipo_nota_credito->dm_multiple_factura == 1 || (isset($_POST['dc_factura_simple']) && intval($_POST['dc_factura_simple']) > 0)):
	require_once('../../../contabilidad/stuff.class.php');
	
	//Obtener datos de configuración de la contabilidad
	$contabilidad_conf = ContabilidadStuff::getConfiguration();
	
	//Obtener información del proveedor para los datos de la glosa
    $proveedor = $db->getRowById('tb_proveedor',$dc_proveedor,'dc_proveedor');
	
	$dg_comprobante = ContabilidadStuff::getCorrelativoComprobante();
	
	$insert_comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => $dg_comprobante,
		'dc_tipo_movimiento' => $contabilidad_conf->dc_tipo_movimiento_nota_credito_proveedor,
		'df_fecha_contable' => '?',
		'dc_mes_contable' => '?',
		'dc_anho_contable' => '?',
		'dc_tipo_cambio' => 0,
		'dq_cambio' => 1,
		'dg_glosa' => '?',
		'dq_saldo' => '?',
		'dc_nota_credito_proveedor' => $dc_nota_credito,
		'df_fecha_emision' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario
	)));
	$insert_comprobante->bindValue(1,$db->sqlDate2($_POST['df_libro_compra']),PDO::PARAM_STR);
	$insert_comprobante->bindValue(2,substr($_POST['df_libro_compra'],3,2),PDO::PARAM_INT);
	$insert_comprobante->bindValue(3,substr($_POST['df_libro_compra'],-4),PDO::PARAM_INT);
	$insert_comprobante->bindValue(4,"Aplicación saldo nota de crédito {$dq_nota_credito}",PDO::PARAM_STR);
	$insert_comprobante->bindValue(5,$saldar,PDO::PARAM_STR);
	$db->stExec($insert_comprobante);
	
	$dc_comprobante = $db->lastInsertId();
	
	$insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => $dc_comprobante,
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?'
	)));
	$insert_detalle_comprobante->bindValue(1,$proveedor->dc_cuenta_contable,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindParam(2,$dq_debe,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(3,$dq_haber,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(4,$dg_glosa,PDO::PARAM_STR);
    
    $insert_detalle_comprobante_analitico = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico',array(
        'dc_cuenta_contable' => '?',
        'dq_debe' => '?',
        'dq_haber' => '?',
        'dg_glosa' => '?',
        'dc_detalle_financiero' => '?',
        'dc_factura_compra' => '?',
        'dc_proveedor' => '?',
        'dc_tipo_proveedor' => '?',
        'dc_mercado_proveedor' => '?',
        'dc_nota_credito_proveedor' => '?'
    )));
    $insert_detalle_comprobante_analitico->bindValue(1 , $proveedor->dc_cuenta_contable, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindParam(2 , $dq_debe_analitico, PDO::PARAM_STR);
    $insert_detalle_comprobante_analitico->bindParam(3 , $dq_haber_analitico, PDO::PARAM_STR);
    $insert_detalle_comprobante_analitico->bindParam(4 , $dg_glosa_analitico, PDO::PARAM_STR);
    $insert_detalle_comprobante_analitico->bindParam(5 , $dc_detalle_financiero, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindParam(6 , $dc_factura_compra, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(7 , $proveedor->dc_proveedor, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(8 , $proveedor->dc_tipo_proveedor, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(9 , $proveedor->dc_mercado, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(10, $dc_nota_credito, PDO::PARAM_INT);
    
    $dq_haber = 0;
    $dq_haber_analitico = 0;
    $dq_debe = $saldar;
    $dg_glosa = "Saldo Nota de crédito";
    
    $db->stExec($insert_detalle_comprobante);
    
    $dc_detalle_financiero = $db->lastInsertId();
	
	//Si es pago factura multiple
	if($tipo_nota_credito->dm_multiple_factura == 1){
		foreach($_POST['dc_factura'] as $i => $f){
			$dq_debe_analitico = floatval($_POST['dq_monto_pagar'][$i]);
			$dc_factura_compra = intval($f);
			
			//Obtener los datos de la factura de compra
            $factura = $db->getRowById('tb_factura_compra',$dc_factura_compra,'dc_factura');
			
			$dg_glosa_analitico = "Factura compra: {$factura->dq_factura}, folio: {$factura->dq_folio}, Proveedor: {$proveedor->dg_razon}";
			
			$db->stExec($insert_detalle_comprobante_analitico);
			
		}
	}else{
		
		$dq_debe_analitico = $saldar;
		$dc_factura_compra = intval($_POST['dc_factura_simple']);
		
		//Obtener los datos de la factura de compra
		$factura = $db->getRowById('tb_factura_compra',$dc_factura_compra,'dc_factura');
		
		$dg_glosa_analitico = "Factura compra: {$factura->dq_factura}, folio: {$factura->dq_folio}, Proveedor: {$proveedor->dg_razon}";
		
		$db->stExec($insert_detalle_comprobante_analitico);
		
	}
		
	$dq_debe = 0;
	$dq_haber = $saldar;
	$dg_glosa = "Pago con saldo a favor Nota de credito: {$dq_nota_credito} folio: {$_POST['nc_number']}";
	
	$db->stExec($insert_detalle_comprobante);
    
    $dq_debe_analitico = 0;
    $dq_haber_analitico = $saldar;
    $dg_glosa_analitico = "Pago con saldo a favor Nota de credito: {$dq_nota_credito} folio: {$_POST['nc_number']}";
    $dc_factura_compra = NULL;
    $dc_detalle_financiero = $db->lastInsertId();
    
    $db->stExec($insert_detalle_comprobante_analitico);
	
endif;

$db->commit();

$error_man->showConfirm("Se ha creado la Nota de crédito de proveedor correctamente");
$error_man->showInfo("El número de folio para nota de crédito física es el <strong>{$_POST['nc_number']}</strong><br />
Además de eso el número de identificación interno es el <h2 style='margin:0;color:#000;'>{$dq_nota_credito}</h2>");

if($tipo_nota_credito->dm_multiple_factura == 1):
	$error_man->showInfo("Se generó el comprobante contable <h2 style='margin:0;color:#000;'>{$dg_comprobante}</h2><br />
	<a href='sites/contabilidad/ver_comprobante.php?id={$dc_comprobante}' target='_blank'>Haga click aquí para ver la versión impresión</a>");
endif;

?>