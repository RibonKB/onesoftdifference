<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

if($_POST['dm_retorna_stock'] == '1' && $_POST['dm_multiple_factura'] != '0'){
	$error_man->showWarning('Si escoge la opción de retorno de stock no puede seleccionar documentos múltiples');
	exit;
}


$insert_tipo = $db->prepare($db->insert('tb_tipo_nota_credito_proveedor',array(
	'dg_tipo' => '?',
	'dg_descripcion' => '?',
	'dm_retorna_stock' => '?',
	'dc_cuenta_contable_centralizacion' => '?',
	'dc_empresa' => $empresa,
	'df_creacion' => $db->getNow(),
	'dc_usuario_creacion' => $idUsuario
)));
$insert_tipo->bindValue(1,$_POST['dg_tipo'],PDO::PARAM_STR);
$insert_tipo->bindValue(2,$_POST['dg_descripcion'],PDO::PARAM_STR);
$insert_tipo->bindValue(3,$_POST['dm_retorna_stock'],PDO::PARAM_STR);
$insert_tipo->bindValue(4,$_POST['dc_cuenta_contable'], PDO::PARAM_INT);

$db->stExec($insert_tipo);

$dc_tipo = $db->lastInsertId();

$error_man->showConfirm("Se ha creado el tipo de nota de crédito <strong>{$_POST['dg_tipo']}</strong> correctamente");

$date_now = date('d/m/Y H:i');
?>
<table class="hidden">
	<tr id="item<?php echo $dc_tipo ?>">
		<td><?php echo $_POST['dg_tipo'] ?></td>
		<td><?php echo $date_now ?></td>
		<td><?php echo $_POST['dg_descripcion'] ?></td>
		<td><?php echo $_POST['dm_retorna_stock']=='1'?'SI':'NO' ?></td>
		<td><?php echo $_POST['dm_multiple_factura']=='1'?'SI':'NO' ?></td>
		<td>
			<a href='sites/ventas/nota_credito_proveedor/ed_tipo_nota_credito.php?id=<?php echo $dc_tipo ?>' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
			</a>
			<a href='sites/ventas/nota_credito_proveedor/del_tipo_nota_credito.php?id=<?php echo $dc_tipo ?>' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
			</a>
		</td>
	</tr>
</table>
<script type="text/javascript">
	$("#item<?php echo $dc_tipo ?>").appendTo("#list")
	.find("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>
