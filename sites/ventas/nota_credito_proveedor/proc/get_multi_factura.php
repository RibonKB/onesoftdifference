<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_proveedor = intval($_POST['dc_proveedor']);
$dq_monto = floatval($_POST['dq_monto']);

$data = $db->prepare($db->select('tb_factura_compra fc',
'fc.dc_factura, fc.dq_factura, fc.dq_folio, fc.df_emision, fc.dq_monto_pagado, fc.dq_total',
"fc.dc_proveedor = ? AND dc_empresa = ? AND (fc.dq_total-fc.dq_monto_pagado) > 0"));
$data->bindValue(1,$dc_proveedor,PDO::PARAM_INT);
$data->bindValue(2,$empresa,PDO::PARAM_INT);

$db->stExec($data);

?>
<div class="secc_bar">Asignación de Facturas a Nota de crédito</div>
<div class="panes">
	<b class="right">Saldo Disponible:</b><br />
	<input type="text" class="inputtext right" readonly="readonly" id="dq_monto_disponible" />
	<table class="tab" width="100%" id="tb_multi_factura_data">
	<caption>Facturas no pagadas</caption>
	<thead>
		<tr>
			<th>SEL</th>
			<th>Factura de compra</th>
			<th>Folio</th>
			<th>Fecha de emisión</th>
			<th>Saldo Actual</th>
			<th>Saldo Final</th>
			<th>Saldo Ocupado</th>
		</tr>
	</thead>
	<tbody>
	<?php while($factura = $data->fetch(PDO::FETCH_OBJ)): ?>
		<tr>
			<td>
				<input type="checkbox" class="dc_selected_factura" value="<?php echo $factura->dc_factura ?>" />
				<input type="hidden" class="dq_monto_actual" value="<?php echo round($factura->dq_total-$factura->dq_monto_pagado,$empresa_conf['dn_decimales_local']) ?>" />
				<input type="hidden" class="dq_monto_saldo" value="0" />
			</td>
			<td><?php echo $factura->dq_factura ?></td>
			<td><?php echo $factura->dq_folio ?></td>
			<td><?php echo $db->dateLocalFormat($factura->df_emision) ?></td>
			<td align="right"><?php echo moneda_local($factura->dq_total-$factura->dq_monto_pagado) ?></td>
			<td align="right" class="dq_monto_final dq_no_num">-</td>
			<td align="right" class="dq_saldo_ocupado dq_no_num">-</td>
		</tr>
	<?php endwhile; ?>
	</tbody>
	</table>
	<div class="center">
		<button class="addbtn" id="setMultiFactura">Asignar Facturas</button>
	</div>
</div>
<script type="text/javascript">
	js_data.setDqMonto(<?php echo $dq_monto ?>);
	js_data.initMultiFactura();
</script>