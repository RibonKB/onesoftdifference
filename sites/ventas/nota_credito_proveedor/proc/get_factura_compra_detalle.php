<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['dc_factura']);

if(!is_numeric($_GET['dc_factura'])){
	echo json_encode('<not-found>');
	exit;
}

$dc_factura = intval($_GET['dc_factura']);

$id = $db->select('tb_factura_compra',
'dc_factura',
"dc_empresa = {$empresa} AND dc_factura={$dc_factura} AND dm_nula=0");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

$detalle = $db->select("tb_factura_compra_detalle d
JOIN tb_producto p ON p.dc_producto = d.dc_producto",
"d.dc_detalle,p.dg_codigo,p.dg_producto dg_descripcion,d.dc_cantidad dq_cantidad,d.dq_precio dq_precio_venta",
"dc_factura = {$dc_factura}");


if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>