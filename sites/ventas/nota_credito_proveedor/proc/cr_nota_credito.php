<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../../../inc/Factory.class.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

/**
*	Validar proveedor
*/
if(isset($_POST['dg_rut'])){
	//Se escapan los caracteres por seguridad
	$db->escape($_POST['dg_rut']);

	$proveedor = $db->select("tb_proveedor","dc_proveedor,dg_razon,dc_contacto_default",
	"dg_rut = '{$_POST['dg_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");
	$proveedor = array_shift($proveedor);

}else if(isset($_POST['dc_proveedor'])){

	$dc_proveedor = intval($_POST['dc_proveedor']);

	$proveedor = $db->select("tb_proveedor",'dc_proveedor,dg_razon,dc_contacto_default',
	"dc_proveedor={$dc_proveedor} AND dc_empresa={$empresa} AND dm_activo='1'");
	$proveedor = array_shift($proveedor);

}else
	exit;

if($proveedor == NULL){
	$error_man->showErrorRedirect("No se encontró el proveedor especificado, intentelo nuevamente.","sites/ventas/nota_credito_proveedor/cr_nota_credito.php");
}


/**
*	Validar tipo de nota de crédito
*/
$dc_tipo = intval($_POST['dc_tipo']);
$tipo_nota_credito = $db->select('tb_tipo_nota_credito_proveedor',
	'dc_tipo, dg_tipo, dm_retorna_stock, dm_multiple_factura',
	"dc_tipo = {$dc_tipo} AND dc_empresa = {$empresa} AND dm_activo = 1");
$tipo_nota_credito = array_shift($tipo_nota_credito);

if($tipo_nota_credito == NULL){
	$error_man->showWarning('El tipo de nota de crédito es inválido, seleccione otro y vuelva a intentarlo');
	exit;
}

include_once("../../../../inc/form-class.php");
$form = new Form($empresa);

$dq_nota_credito = $db->select('tb_nota_credito_proveedor','MAX(dq_folio)+1 AS number',"dc_empresa={$empresa}");
$dq_nota_credito = $dq_nota_credito[0]['number'];

?>
<div id='secc_bar'>Creación de nota de crédito</div>
<div id='main_cont'>
<input type='text' class='hidden' />
<div class='panes'>
<div class='title center'>
Generando nota de crédito de <strong style='color:#000;'><?php echo $proveedor['dg_razon'] ?></strong>
<br />
<small>tipo: <b><?php echo $tipo_nota_credito['dg_tipo'] ?></b></small>
</div>
	<?php $form->Start(Factory::buildUrl('CrearNotaCredito','ventas','procesarCrear','nota_credito_proveedor'),"cr_nota_credito",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la nota de crédito</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();

	$form->Date('Fecha emisión','fv_emision',1,0);

	//Contacto principal de la nota de crédito, aparecerá en la cabecera como principal contacto del proveedor
	$form->Listado('Contacto','fv_contacto',"tb_contacto_proveedor",
				array('dc_contacto_proveedor','dg_contacto'),1,$proveedor['dc_contacto_default'],'dc_proveedor='.$proveedor['dc_proveedor']);

	$form->Listado('Medio de pago','fv_medio_pago','tb_medio_pago_proveedor',array('dc_medio_pago','dg_medio_pago'),1);
	/*$form->Listado('Tipo de nota de crédito','nc_tipo','tb_tipo_nota_credito_proveedor',
	array('CONCAT_WS("|",dc_tipo_nota_credito,dm_retorna_stock,dm_retorna_libre,dc_bodega_entrada)','dg_tipo_nota_credito'),1);*/

	$form->EndSection();
	$form->Section();

	$form->Text("Número de nota de crédito","nc_number",1,20,$dq_nota_credito);
	$form->Date('Fecha vencimiento','fv_vencimiento',1);
	$form->Date('Fecha contable','df_libro_compra',1,0);

	//Selección de factura dependiendo el tipo de nota de crédito
	if($tipo_nota_credito['dm_multiple_factura'] == 1){
		echo('<br /><div id="selected_facturas"></div>');
		$form->Button('Asignar Facturas','id="get_factura_venta"','addbtn');
	}else if($tipo_nota_credito['dm_retorna_stock'] == 1){
		echo('<br /><div id="selected_guias"></div>');
		$form->Button('Asignar guías de despacho','id="get_guia_despacho"','addbtn');
                echo ('<br />');
	}
	$form->Text('Factura de Compra','dq_factura');
	$form->Hidden('dc_factura_simple',0);
	//Fin selección de factura

	$form->EndSection();
	$form->Section();

	$form->Textarea('Comentario','fv_comentario',0,isset($num_doc_list)?$num_doc_list:'');
	if($tipo_nota_credito['dm_multiple_factura'] == 1){
		$form->Text('Monto Propuesto','dq_monto_propuesto',1);
		echo '<div id="neto_propuesto"></div>';
	}

	$form->EndSection();

	if($tipo_nota_credito['dm_multiple_factura'] == 1):
		$form->Section();
	?>
		<div class="hidden" id="facturas_list_div" style="border:1px solid #BBB;background-color:#FFF;padding:5px;margin:5px;width:200px;">
			<div class="info">Facturas a saldar</div>
			<table class="tab" width="100%">
				<thead>
					<tr>
						<th>FC</th>
						<th>Folio</th>
						<th>Saldo Ocupado</th>
					</tr>
				</thead>
				<tbody id="facturas_checklist">
				</tbody>
			</table>
		</div>
	<?php
		$form->EndSection();
	endif;

?>

	<hr class='clear' />
	<div id='prods'>
	<div class='info'>Detalle (Valores en <b><?php echo $empresa_conf['dg_moneda_local'] ?></b>)</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th>Opciones</th>
		<th>Código</th>
		<th>Descripción</th>
		<th>Cantidad</th>
		<th>Precio</th>
		<th width="120">Total</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th align='right' colspan='4'>Total Neto</th>
		<th align='right' colspan='2'>
        	<?php
				$form->Text('','dq_neto',1,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35);
			?>
        </th>
	</tr>
    <tr>
		<th align='right' colspan='4'>Total Exento</th>
		<th align='right' colspan='2'>
        	<?php
				$form->Text('','dq_exento',1,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35);
			?>
        </th>
	</tr>
	<tr>
		<th align='right' colspan='4'>IVA</th>
		<th align='right' colspan='2'>
            <?php
				$form->Text('','dq_iva',1,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35);
			?>
        </th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total a pagar</th>
		<th align='right' colspan='2'>
        	<?php
				$form->Text('','dq_total',1,Form::DEFAULT_TEXT_LENGTH,0,'inputtext',35);
			?>
        </th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>

	</div>
<?php
        $r=$db->select('tb_guia_despacho_proveedor','*',"dc_proveedor={$proveedor['dc_proveedor']} AND dm_nula=0");
        $m='si';
        $r=array_shift($r);
        if($r == NULL){
            $m='no';
        }
        $form->Hidden('gdp',$m);
	$form->Hidden('dc_proveedor',$proveedor['dc_proveedor']);
	$form->Hidden('dc_tipo',$dc_tipo);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('nc_max_neto',0);
	$form->End('Emitir Nota de crédito','addbtn');
?>

	<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='Eliminar detalle' class='del_detail' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='15' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='50' required='required' /></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='15' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='15' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>
	</tr>
	</table>

</div></div>
<script type="text/javascript">
	var empresa_iva = <?php echo $empresa_conf['dq_iva'] ?>;
	var empresa_dec = <?php echo $empresa_conf['dn_decimales_local'] ?>;
	$('#dq_neto, #dq_exento, #dq_iva, #dq_total').css({'textAlign':'right'});
</script>
<script type="text/javascript" src="jscripts/product_manager/nota_credito.js?v1_12a"></script>
<script type="text/javascript" src="jscripts/sites/ventas/nota_credito_proveedor/cr_nota_credito.js?v=1_14b"></script>
<script type="text/javascript">
	js_data.dc_proveedor = <?php echo $proveedor['dc_proveedor'] ?>;
	js_data.init();
</script>
