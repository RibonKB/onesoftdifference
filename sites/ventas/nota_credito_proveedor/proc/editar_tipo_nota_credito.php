<?php
define("MAIN",1);
require_once("../../../../inc/init.php");


$dg_tipo = $_POST['dg_tipo_ed'];
$dg_descripcion = $_POST['dg_descripcion_ed'];
$dm_retorna_stock = $_POST['dm_retorna_stock_ed'];
$dm_multiple_factura = $_POST['dm_multiple_factura_ed'];
$dc_cuenta_contable_centralizacion = intval($_POST['dc_cuenta_contable_centralizacion_ed']);
$dc_tipo = intval($_POST['dc_tipo_ed']);

$datos = $db->prepare($db->select('tb_tipo_nota_credito_proveedor','true','dc_tipo = ? AND dc_empresa = ?'));
$datos->bindValue(1,$dc_tipo,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

/*if($datos->fetch() !== false){
	$error_man->showWarning("Ya existe un tipo de nota de credito con ese nombre");
	exit;
}*/

$db->start_transaction();

$editar = $db->prepare($db->update('tb_tipo_nota_credito_proveedor',array(
	'dg_tipo' => '?',
	'dg_descripcion' => '?',
	'dm_retorna_stock' => '?',
	'dm_multiple_factura' => '?',
	'dc_cuenta_contable_centralizacion' => '?'),
	'dc_tipo = ? AND dc_empresa = ?'));
$editar->bindValue(1,$dg_tipo,PDO::PARAM_STR);
$editar->bindValue(2,$dg_descripcion,PDO::PARAM_STR);
$editar->bindValue(3,$dm_retorna_stock,PDO::PARAM_BOOL);
$editar->bindValue(4,$dm_multiple_factura,PDO::PARAM_BOOL);
$editar->bindValue(5,$dc_cuenta_contable_centralizacion,PDO::PARAM_INT);
$editar->bindValue(6,$dc_tipo,PDO::PARAM_INT);
$editar->bindValue(7,$empresa,PDO::PARAM_INT);
$db->stExec($editar);
	
$db->commit();

$error_man->showAviso('Se ha modificado correctamente');

?>