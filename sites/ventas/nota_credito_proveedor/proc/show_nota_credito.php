<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_nota_credito = intval($_POST['id']);

$data = $db->select("(SELECT * FROM tb_nota_credito_proveedor WHERE dc_empresa={$empresa} AND dc_nota_credito = {$dc_nota_credito}) nc
JOIN tb_medio_pago_proveedor mp ON mp.dc_medio_pago = nc.dc_medio_pago
LEFT JOIN tb_tipo_nota_credito_proveedor top ON top.dc_tipo = nc.dc_tipo
LEFT JOIN tb_proveedor pr ON pr.dc_proveedor = nc.dc_proveedor",
"nc.dq_nota_credito,DATE_FORMAT(nc.df_emision,'%d/%m/%Y') AS df_emision,DATE_FORMAT(nc.df_vencimiento,'%d/%m/%Y') AS df_vencimiento,nc.dg_comentario,
nc.dq_neto,nc.dq_iva,nc.dq_total,mp.dg_medio_pago,nc.dm_nula,nc.dc_nota_credito,nc.dc_proveedor, (nc.dq_total - nc.dq_saldado) dq_saldo_favor,
top.dg_tipo,pr.dg_razon,pr.dg_rut,pr.dg_giro,nc.dq_folio,nc.df_libro_compra,nc.dm_centralizada");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la nota de crédito especificada");
	exit();
}
$data = $data[0];
$data['dg_comentario'] = str_replace("\n",'<br />',$data['dg_comentario']);

$nula = '';
if($data['dm_nula'] == '1'){
	/*$null_data = $db->select("(SELECT * FROM tb_nota_credito_anulada WHERE dc_factura = {$dc_nota_credito}) n
	JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo",'ma.dg_motivo,n.dm_fisica,n.dg_comentario');*/
	$nula_info = '';
	/*if(count($null_data)){
		$null_data = $null_data[0];
		$null_data['dm_fisica'] = $null_data['dm_fisica']==1?'SI':'NO';
		$nula_info = "<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
		<div class='info'>Info anulación</div>
		Motivo: <b>{$null_data['dg_motivo']}</b><br />
		Facturada fisicamente: <b>{$null_data['dm_fisica']}</b><br />
		Comentario: <b>{$null_data['dg_comentario']}</b>";
	}*/
	$nula = "<td rowspan='11' valign='middle'><h3 class='alert'>NOTA DE CRÉDITO NULA</h3>{$nula_info}</td>";
}

$detalle = $db->select("tb_nota_credito_proveedor_detalle",
'dg_producto,dq_cantidad,dg_descripcion,dq_precio',
"dc_nota_credito={$data['dc_nota_credito']}");

$facturas_relacionadas = $db->select('tb_nota_credito_proveedor_factura d
JOIN tb_factura_compra fc ON fc.dc_factura = d.dc_factura',
'd.dq_monto_saldado, d.dm_estado, fc.dq_factura, fc.dq_folio',
"d.dc_nota_credito = {$dc_nota_credito}");

?>

<div class='title center'>Nota de crédito Nº <?php echo $data['dq_nota_credito'] ?></div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Proveedor:<br /><strong>(<?php echo $data['dg_rut'] ?>) <?php $data['dg_razon'] ?></strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td><?php echo $data['df_emision'] ?></td>
	<?php echo $nula ?>
	<?php if(count($facturas_relacionadas)): ?>
		<td rowspan="11">
			<table class="tab" width="100%">
			<caption>Facturas relacionadas</caption>
				<thead>
					<tr>
						<th>Factura</th>
						<th>Folio</th>
						<th>Monto Saldado</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($facturas_relacionadas as $f): ?>
					<tr>
						<td><?php echo $f['dq_factura'] ?></td>
						<td><?php echo $f['dq_folio'] ?></td>
						<td><?php echo moneda_local($f['dq_monto_saldado']) ?></td>
						<td><?php echo $f['dm_estado'] ?></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</td>
	<?php endif; ?>
</tr><tr>
	<td>Fecha Contable</td>
	<td><b><?php echo $db->dateLocalFormat($data['df_libro_compra']) ?></b></td>
</tr><tr>
	<td>Folio</td>
	<td><b><?php echo $data['dq_folio'] ?></b></td>
</tr><tr>
	<td>Medio de pago:</td>
	<td><b><?php echo $data['dg_medio_pago'] ?></b></td>
</tr><tr>
	<td>Tipo de nota de crédito</td>
	<td><?php echo $data['dg_tipo'] ?></td>
</tr><tr>
	<td>Fecha de vencimiento</td>
	<td><?php echo $data['df_vencimiento'] ?></td>
</tr><tr>
	<td>Comentario</td>
	<td><?php echo $data['dg_comentario'] ?></td>
</tr></table>

<?php if($data['dm_centralizada']): ?>
	<div class="confirm">NOTA DE CRÉDITO CENTRALIZADA</div>
<?php endif ?>

<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
<tr>
	<th width='60'>Código</th>
	<th width='60'>Cantidad</th>
	<th>Descripción</th>
	<th width='100'>Precio</th>
	<th width='100'>Total</th>
</tr>
</thead>
<tbody>
<?php

foreach($detalle as $d){
	$d['dq_total'] = moneda_local($d['dq_precio']*$d['dq_cantidad']);
	$d['dq_cantidad'] = intval($d['dq_cantidad']);
	$d['dq_precio'] = moneda_local($d['dq_precio']);
	echo("
	<tr>
		<td>{$d['dg_producto']}</td>
		<td>{$d['dq_cantidad']}</td>
		<td align='left'>{$d['dg_descripcion']}</td>
		<td align='right'>{$d['dq_precio']}</td>
		<td align='right'>{$d['dq_total']}</td>
	</tr>");
}

$data['dq_neto'] = moneda_local($data['dq_neto']);
$data['dq_iva'] = moneda_local($data['dq_iva']);
$data['dq_total'] = moneda_local($data['dq_total']);

echo("</tbody>
<tfoot>
<tr>
	<th colspan='4' align='right'>Total Neto</th>
	<th align='right'>{$data['dq_neto']}</th>
</tr>
<tr>
	<th colspan='4' align='right'>IVA</th>
	<th align='right'>{$data['dq_iva']}</th>
</tr>
<tr>
	<th colspan='4' align='right'>Total a Pagar</th>
	<th align='right'>{$data['dq_total']}</th>
</tr>
</tfoot></table>");

?>
<script type="text/javascript">
$("#tabs").tabs("div.tabpanes > div",{effect:'default'});
<?php if($data['dm_nula'] == 1): ?>
disable_button('#print_version,#null_nota_credito,#pagar_facturas,#liberar_facturas,#asignar_guias');
<?php else: ?>
 enable_button('#print_version,#null_nota_credito,#pagar_facturas,#liberar_facturas,#asignar_guias');
$('#print_version').unbind('click').click(function(){
	window.open('sites/ventas/nota_credito/ver_nota_credito.php?id=<?php echo $dc_nota_credito ?>&v=0','width=800;height=600');
});
$('#null_nota_credito').unbind('click').click(function(){
	loadOverlay('sites/ventas/nota_credito_proveedor/null_nota_credito.php?id=<?php echo $dc_nota_credito ?>');
});
$('#pagar_facturas').unbind('click').click(function(){
	var dc_proveedor = <?php echo $data['dc_proveedor'] ?>;
	var dq_monto = <?php echo $data['dq_saldo_favor'] ?>;
	pymerp.loadOverlay('sites/ventas/nota_credito_proveedor/proc/get_multi_factura.php','dc_proveedor='+dc_proveedor+'&dq_monto='+dq_monto);
});
$('#liberar_facturas').unbind('click').click(function(){
	loadOverlay('sites/ventas/nota_credito_proveedor/liberar_facturas_compra.php?id=<?php echo $dc_nota_credito ?>');
});
$('#asignar_guias').unbind('click').click(function(){
	loadOverlay('sites/ventas/nota_credito_proveedor/src_asignar_guias.php?id=<?php echo $dc_nota_credito ?>');
});
<?php endif; ?>
</script>