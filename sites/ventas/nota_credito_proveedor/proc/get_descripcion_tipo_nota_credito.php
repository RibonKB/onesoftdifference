<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_tipo = intval($_POST['dc_tipo']);

$data = $db->prepare($db->select('tb_tipo_nota_credito_proveedor','dg_tipo, dg_descripcion, dm_retorna_stock, dm_multiple_factura','dc_tipo = ? AND dc_empresa = ?'));
$data->bindValue(1,$dc_tipo,PDO::PARAM_INT);
$data->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($data);

$data = $data->fetch(PDO::FETCH_OBJ);

if($data === false){
	exit;
}
?>
<div style="background-color:#D5F9A4; width:250px; margin:auto; text-align:left;">
	<h3><small>tipo:</small> <?php echo $data->dg_tipo ?></h3>
	<p style="padding:0 10px;">
		<?php echo $data->dg_descripcion ?>
		<ul>
			<?php if($data->dm_retorna_stock == 1): ?>
			<li>Retorna Stock</li>
			<?php endif; ?>
			<?php if($data->dm_multiple_factura): ?>
			<li>Para facturas múltiples</li>
			<?php else: ?>
			<li>Para factura simple</li>
			<?php endif; ?>
		</ul>
	</p>
</div>