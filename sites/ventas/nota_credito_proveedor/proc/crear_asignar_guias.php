<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_nota_credito = intval($_POST['dc_nota_credito']);

$nota_credito = $db->prepare($db->select('tb_nota_credito_proveedor','dc_proveedor','dc_nota_credito = ? AND dc_empresa = ?'));
$nota_credito->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$nota_credito->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_credito);
$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);

if($nota_credito === false){
	$error_man->showWarning('No se ha encontrado la nota de crédito especificada, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

//Obtener detalles de nota de crédito
$detalles_nc = $db->prepare($db->select('tb_nota_credito_proveedor_detalle','dg_producto, dq_cantidad','dc_nota_credito = ?'));
$detalles_nc->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$db->stExec($detalles_nc);

$dnc = array();
while($d = $detalles_nc->fetch(PDO::FETCH_OBJ)){
	$dnc[$d->dg_producto] = $d->dq_cantidad;
}

//Obtener detalles de guías de despacho ya asignadas a la nota de credito
$detalle_gd = $db->prepare($db->select('tb_guia_despacho_proveedor gd
										JOIN tb_guia_despacho_proveedor_detalle d ON d.dc_guia_despacho = gd.dc_guia_despacho',
										'd.dg_producto, d.dq_cantidad',
										'gd.dc_nota_credito = ? AND gd.dc_empresa = ?'));
$detalle_gd->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$detalle_gd->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($detalle_gd);

//Rebajar las cantidades en la nota de crédito para ajustarlas a lo que está disponible para despachar
while($d = $detalle_gd->fetch(PDO::FETCH_OBJ)):
	$dnc[$d->dg_producto] -= $d->dq_cantidad;
endwhile;

//Obtener detalles de guías de despacho seleccionadas en el formulario
$question = substr(str_repeat(',?',count($_POST['dc_guia_despacho'])),1);

$detalle_gd = $db->prepare($db->select('tb_guia_despacho_proveedor gd
										JOIN tb_guia_despacho_proveedor_detalle d ON d.dc_guia_despacho = gd.dc_guia_despacho',
										'd.dg_producto, d.dq_cantidad, gd.dq_guia_despacho, gd.dq_folio, d.dg_descripcion',
										"gd.dc_empresa = ? AND gd.dc_nota_credito = 0 AND gd.dc_guia_despacho IN({$question})"));
$detalle_gd->bindValue(1,$empresa,PDO::PARAM_INT);

foreach($_POST['dc_guia_despacho'] as $i => $gd){
	$detalle_gd->bindValue($i+2,$gd,PDO::PARAM_INT);
}

$db->stExec($detalle_gd);

//Comprobar si los detalles están disponibles para ser rebajados de la nota de crédito
while($d = $detalle_gd->fetch(PDO::FETCH_OBJ)){
	
	if(!isset($dnc[$d->dg_producto])){
		$error_man->showWarning("La guía de despacho <b>{$d->dq_guia_despacho}</b> folio <b>{$d->dq_folio}</b>
								 posee el producto <b>{$d->dg_producto}</b> - <i>{$d->dg_descripcion}</i> No disponibl en la nota de crédito");
		exit;
	}
	
	$dnc[$d->dg_producto] -= $d->dq_cantidad;
	
	if($dnc[$d->dg_producto] < 0){
		$error_man->showWarning("Las cantidades para el producto <b>{$d->dg_producto}</b> <i>{$d->dg_descripcion}</i> superan lo disponible para despacho en la nota de crédito");
		exit;
	}
	
}

//Asignar nota de crédito a las guías de despacho seleccionadas
$update_gd = $db->prepare($db->update('tb_guia_despacho_proveedor',array(
	'dc_nota_credito' => '?'
),"dc_empresa = ? AND dc_guia_despacho IN ({$question})"));
$update_gd->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$update_gd->bindValue(2,$empresa,PDO::PARAM_INT);

foreach($_POST['dc_guia_despacho'] as $i => $gd){
	$update_gd->bindValue($i+3,$gd,PDO::PARAM_INT);
}

$db->stExec($update_gd);

$error_man->showConfirm('Se han asignado las guías de despacho proveedor a la nota de crédito correctamente');

?>