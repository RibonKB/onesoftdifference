<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['df_emision_desde'] = $db->sqlDate($_POST['df_emision_desde']);
$_POST['df_emision_hasta'] = $db->sqlDate($_POST['df_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['df_emision_desde']} AND {$_POST['df_emision_hasta']})";

if($_POST['dq_nota_credito_desde']){
	if($_POST['dq_nota_credito_hasta']){
		$conditions .= " AND (dq_nota_credito BETWEEN {$_POST['dq_nota_credito_desde']} AND {$_POST['dq_nota_credito_hasta']})";
	}else{
		$conditions .= " AND dq_nota_credito = {$_POST['dq_nota_credito_desde']}";
	}
}

if(isset($_POST['dc_proveedor'])){
	$_POST['dc_proveedor'] = implode(',',$_POST['dc_proveedor']);
	$conditions .= " AND dc_proveedor IN ({$_POST['dc_proveedor']})";
}

$data = $db->select('tb_nota_credito_proveedor','dc_nota_credito,dq_nota_credito,dq_folio,dm_nula',$conditions,array('order_by' => 'dq_nota_credito DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron notas de crédito con los criterios especificados");
	exit();
}

?>

<div id='show_nota_credito'></div>

<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Notas de crédito<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Nota de Crédito</th>
	</tr>
</thead>
<tbody>

<?php foreach($data as $c): ?>
<tr>
	<td align='left'>
		<?php if($c['dm_nula'] == 1): ?>
			<img src="images/warning.png" alt="NULA" class="right" width="20" title="Nota de Crédito Anulada" />
		<?php endif; ?>
		<a href='sites/ventas/nota_credito_proveedor/proc/show_nota_credito.php?id=<?php echo $c['dc_nota_credito'] ?>' class='fv_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' /><?php echo $c['dq_nota_credito'] ?> - <b><?php echo $c['dq_folio'] ?></b></a>
	</td>
</tr>
<?php endforeach; ?>

</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type="button" class="button" id="pagar_facturas">Saldar Facturas de compra</button>
<button type="button" class="button" id="liberar_facturas">Liberar Facturas de compra</button>
<button type="button" class="addbtn" id="asignar_guias">Asignar Guías de despacho</button>
<button type='button' class='delbtn' id='null_nota_credito'>Anular</button>

</div>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".fv_load").click(function(e){
		e.preventDefault();
		$('#show_factura').html("<img src='images/ajax-loader.gif' alt='' /> cargando nota de crédito ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('#main_cont .panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_nota_credito');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>