<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_nota_credito = intval($_POST['dc_nota_credito']);

//Obtener datos de la nota de crédito
$nota_credito = $db->prepare($db->select('tb_nota_credito','dq_nota_credito','dc_nota_credito = ?'));
$nota_credito->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$db->stExec($nota_credito);
$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);

if($nota_credito === false){
	$error_man->showWarning('La nota de crédito es inválida, compruebe los datos de entrada.');
	exit;
}

$db->start_transaction();

$update_saldos = $db->prepare(
					$db->update('tb_nota_credito_proveedor_factura p
					JOIN tb_nota_credito_proveedor nc ON nc.dc_nota_credito = p.dc_nota_credito
					JOIN tb_factura_compra fc ON fc.dc_factura = p.dc_factura',array(
						'fc.dq_monto_pagado' => 'fc.dq_monto_pagado-p.dq_monto_saldado',
						'p.dm_estado' => '?',
						'nc.dq_saldado' => 'nc.dq_saldado-p.dq_monto_saldado'
					),'p.dc_nota_credito = ? AND p.dm_estado = ?')
				 );
$update_saldos->bindValue(1,'L',PDO::PARAM_STR);
$update_saldos->bindValue(2,$dc_nota_credito,PDO::PARAM_INT);
$update_saldos->bindValue(3,'A',PDO::PARAM_STR);

$db->stExec($update_saldos);

$db->commit();

$error_man->showAviso('Atención se han liberado los pagos hechos por la nota de crédito');

?>