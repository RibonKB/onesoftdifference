<?php
define('MAIN',1);
include('../../../inc/global.php');

$dc_nota_credito = intval($_POST['id']);

$data = $db->select('tb_nota_credito_proveedor','dq_nota_credito, dm_nula',"dc_nota_credito = {$dc_nota_credito}");
$data = array_shift($data);

if($data == NULL){
	$error_man->showWarning('La nota de credito no fue encontrada, no se puede continuar.');
	exit;
}

if($data['dm_nula'] == 1){
	$error_man->showAviso('La nota de credito ya fue anulada, no se puede continuar.');
	exit;
}

$dq_nota_credito = $data['dq_nota_credito'];

echo('<div class="secc_bar">Anular Nota de Crédito</div><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/nota_credito_proveedor/proc/null_nota_credito.php','null_nota_credito_form');
$form->Header('Indique las razones por las que se anulará la nota de crédito');
$error_man->showAviso("Atención, está a punto de anular la nota de crédito N° <b>{$dq_nota_credito}</b>");
$form->Section();
$form->Listado('Motivo de anulación','dc_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','dg_comentario',1);
$form->EndSection();
$form->Hidden('dc_nota_credito',$dc_nota_credito);
$form->End('Anular','delbtn');

?>
</div>