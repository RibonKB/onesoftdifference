<?php
define("MAIN",1);
require_once("../../../inc/global.php");

include_once("../../../inc/form-class.php");
$form = new Form($empresa);
?>
<div id="secc_bar">
	Notas de crédito proveedor
</div>
<div id="main_cont"><br /><br />
	<div class="panes">
		<?php $form->Start('sites/ventas/nota_credito_proveedor/proc/src_nota_credito.php','src_nota_credito') ?>
		<?php $form->Header('Índique los filtros de búsqueda para las notas de crédito de proveedor') ?>
		<table class="tab" width="100%">
		<tbody>
			<tr>
				<td width="170">Número de Nota de crédito</td>
				<td><?php $form->Text('Desde','dq_nota_credito_desde') ?></td>
				<td><?php $form->Text('Hasta','dq_nota_credito_hasta') ?></td>
			</tr>
			<tr>
				<td>Fecha de emisión</td>
				<td><?php $form->Date('Desde','df_emision_desde',1,"01/".date("m/Y")) ?></td>
				<td><?php $form->Date('Hasta','df_emision_hasta',1,0) ?></td>
			</tr>
			<tr>
				<td>Proveedor</td>
				<td><?php $form->ListadoMultiple('','dc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon')) ?></td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
		</table>
		<?php $form->End('Ejecutar Búsqueda','searchbtn'); ?>
	</div>
</div>
<script type="text/javascript">
$('#dc_proveedor').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>
