<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/Factory.class.php");

$list = $db->select('tb_tipo_nota_credito_proveedor',
	'dc_tipo, dg_tipo, dg_descripcion, df_creacion, dm_retorna_stock, dm_multiple_factura',
	"dc_empresa = {$empresa} AND dm_activo = 1");

include_once("../../../inc/form-class.php");
$form = new Form($empresa);
?>
<div id="secc_bar">
	Tipos de nota de crédito de proveedor
</div>
<div id="main_cont">
<div class="panes">
	<?php
		$form->Start('sites/ventas/nota_credito_proveedor/proc/crear_tipo_nota_credito.php','cr_tipo_nota_credito');
		$form->Header('<b>Complete los datos requeridos para crear un nuevo tipo de nota de crédito</b>
						<br />Los campos marcados con [*] son obligatorios
						<br />Puede consultar los que ya están agregados.');
		$form->Section();
			$form->Text('Nombre','dg_tipo',1);
			$form->Textarea('Descripción','dg_descripcion',1);
		$form->EndSection();
		$form->Section();
			$form->Radiobox('Retorna stock','dm_retorna_stock',array('NO','SI'),0);
			//$form->Radiobox('Múltiples documentos','dm_multiple_factura',array('NO','SI'),0);
		$form->EndSection();
		$form->Section();
			$form->Listado('Cuenta contable centralizacion','dc_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable',		 			  						   'dg_cuenta_contable'),true);
		$form->EndSection();
		$form->End('Crear','addbtn');
	?>

	<table class="tab" width="100%" id="ttipo">
	<caption>Tipos de Nota de crédito proveedor</caption>
	<thead>
		<tr>
			<th>Tipo de nota de crédito</th>
			<th>Fecha de creación</th>
			<th>Descripción</th>
			<th>Retorna Stock</th>
			<th>Múltiples Documentos</th>
			<th>Opciones</th>
		</tr>
	</thead>
	<tbody id="list">
	<?php foreach($list as $l): ?>
		<tr id='item<?php echo $l['dc_banco'] ?>'>
			<td><?php echo $l['dg_tipo'] ?></td>
			<td><?php echo $db->dateTimeLocalFormat($l['df_creacion']) ?></td>
			<td><?php echo $l['dg_descripcion'] ?></td>
			<td><?php echo $l['dm_retorna_stock']=='1'?'SI':'NO' ?></td>
			<td><?php echo $l['dm_multiple_factura']=='1'?'SI':'NO' ?></td>
			<td>
				<a href='sites/ventas/nota_credito_proveedor/ed_tipo_nota_credito.php?id=<?php echo $l['dc_tipo'] ?>' class='loadOnOverlay'>
					<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/ventas/nota_credito_proveedor/del_tipo_nota_credito.php?id=<?php echo $l['dc_tipo'] ?>' class='loadOnOverlay'>
					<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
				<a href="<?php echo Factory::buildUrl("TipoNotaCredito","ventas","cuentaCentralizacion","nota_credito_proveedor",array('dc_tipo' => $l['dc_tipo'])) ?>" class="loadOnOverlay">
					<img src="images/inventario.png" title="Asignar cuentas contables" />
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>

</div>
</div>
<script type="text/javascript">
	$('#ttipo').tableExport().tableAdjust(10);
</script>
