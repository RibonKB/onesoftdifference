<?php
define("MAIN",1);
require_once('../../../inc/init.php');
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_nota_credito = intval($_POST['id']);

//Obtener datos de la nota de crédito
$nota_credito = $db->prepare($db->select('tb_nota_credito_proveedor','dc_proveedor','dc_nota_credito = ? AND dc_empresa = ?'));
$nota_credito->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$nota_credito->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_credito);
$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);

//Comprobar existencia de nota de crédito
if($nota_credito === false){
	$error_man->showWarning('No se ha encontrado la nota de crédito especificada, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

//Consultar información del proveedor
$proveedor = $db->prepare($db->select('tb_proveedor','dg_razon, dg_rut','dc_proveedor = ? AND dc_empresa = ?'));
$proveedor->bindValue(1,$nota_credito->dc_proveedor,PDO::PARAM_INT);
$proveedor->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($proveedor);
$proveedor = $proveedor->fetch(PDO::FETCH_OBJ);

if($proveedor === false){
	$error_man->showWarning('El proveedor de la nota de crédito es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Obtener guías de despacho disponibles para el proveedor
$guias_despacho = $db->prepare(
					$db->select(
						'tb_guia_despacho_proveedor gd
						 JOIN tb_tipo_guia_despacho_proveedor t ON t.dc_tipo = gd.dc_tipo_guia',
						'gd.dc_guia_despacho, gd.dq_guia_despacho, gd.df_emision, gd.dq_folio, gd.dq_total, t.dg_tipo',
						'gd.dc_proveedor = ? AND gd.dc_empresa = ? AND dc_nota_credito = 0 AND dm_nula = 0')
					);
$guias_despacho->bindValue(1,$nota_credito->dc_proveedor,PDO::PARAM_INT);
$guias_despacho->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($guias_despacho);
$guias_despacho = $guias_despacho->fetchAll(PDO::FETCH_OBJ);


?>
<div class="secc_bar">
	Guías de despacho proveedor <strong><?php echo $proveedor->dg_razon ?></strong>
</div>
<div class="panes">
<?php if(!count($guias_despacho)): //Si no se encontraron guías ?>
	<?php $error_man->showAviso('No se han encontrado guías de despacho para el proveedor seleccionado.'); exit; ?>
	</div>
<?php endif; ?>
	<?php $form->Start('sites/ventas/nota_credito_proveedor/proc/crear_asignar_guias.php','asignar_guias_form') ?>
    <?php $form->Header('Indique las guías de despacho asociadas a la nota de crédito') ?>
        <table class="tab" width="100%">
            <thead>
                <th width="20">
                    <input type="checkbox" id="select_all_gd" />
                </th>
                <th>Número de Guía</th>
                <th>Folio</th>
                <th>Emisión</th>
                <th>Tipo</th>
                <th>Total</th>
            </thead>
            <tbody>
            <?php while($gd = array_shift($guias_despacho)): ?>
                <tr>
                    <td align="center">
                        <input type="checkbox" class="gd_item" value="<?php echo $gd->dc_guia_despacho ?>" name="dc_guia_despacho[]" />
                    </td>
                    <td><?php echo $gd->dq_guia_despacho ?></td>
                    <td><?php echo $gd->dq_folio ?></td>
                    <td><?php echo $db->dateLocalFormat($gd->df_emision) ?></td>
                    <td><?php echo $gd->dg_tipo ?></td>
                    <td><?php echo moneda_local($gd->dq_total) ?></td>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>
     <?php $form->Hidden('dc_nota_credito',$dc_nota_credito) ?>
     <?php $form->End('Asignar','addbtn') ?>
</div>