<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo = intval($_POST['id']);

$datos = $db->prepare($db->select('tb_tipo_nota_credito_proveedor',
	'dc_tipo, dg_tipo','dc_tipo = ? AND dc_empresa = ?'));
$datos->bindValue(1,$dc_tipo,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('No se encontro el tipo de nota de credito');
	exit;
}


?>


<div class="secc_bar" >
	Eliminar tipo nota de credito
</div>

<div class="panes" >
	<?php $form->Start('sites/ventas/nota_credito_proveedor/proc/delete_tipo_nota_credito.php', 'del_tipo_nota_credito') ?>
    <?php $error_man->showAviso("¿Está seguro que desea eliminar el tipo de nota de credito? <strong>{$datos->dg_tipo}</strong>") ?>

    <?php $form->Hidden('dc_tipo_ed', $dc_tipo)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>
