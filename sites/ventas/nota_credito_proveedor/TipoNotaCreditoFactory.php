<?php
class TipoNotaCreditoFactory extends Factory{

  private $tipo, $cuentas_contables, $cuenta_contable;
  protected $title;

  public function eliminarCuentaContableCentralizacionAction(){
    $this->initEliminarCuentaContableCentralizacionData();

    $this->getConnection()->start_transaction();
      $aff = $this->eliminarCuentaContableCetralizacion();
    $this->getConnection()->commit();

    if($aff > 0):
      $this->getErrorMan()->showConfirm("Se ha quitado la cuenta contable {$this->cuenta_contable->dg_cuenta_contable} del tipo de Nota de Crédito.");
    else:
      $this->getErrorMan()->showAviso("No se encontró la cuenta contable asociada al tipo de Nota de Crédito, compruebe que no haya sido eliminada anteriormente.");
    endif;

  }

  public function cuentaCentralizacionAction(){
    $this->title = "Cuentas contable Centralización";
    $this->initCuentaCentralizacionFormData();

    $form = $this->getFormView($this->getTemplateUrl("cuentaCentralizacion.form"),array(
      "tipo" => $this->tipo,
      "cuentas_contables" => $this->cuentas_contables
    ));

    echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);

  }

  public function procesarCuentaCentralizacionAction(){
    $this->initProcesarCuentaCentralizacionFormData();

    $this->getConnection()->start_transaction();
      $this->insertarCuentaContableCentralizacion();
    $this->getConnection()->commit();

    echo $this->getView($this->getTemplateUrl('confirmarCuentaContableCentralizacion'),array(
      'cuenta_contable' => $this->cuenta_contable
    ));

  }

  private function initCuentaCentralizacionFormData(){
    $r = self::getRequest();
    $this->initTipoNotaCredito($r, true);
    $this->initCuentasContables($this->tipo->dc_tipo);
  }

  private function initProcesarCuentaCentralizacionFormData(){
    $r = self::getRequest();
    $this->initTipoNotaCredito($r, true);
    $this->initCuentaContableSeleccionada($r);
    $this->validaCuentaCentralizacion($this->tipo->dc_tipo, $this->cuenta_contable->dc_cuenta_contable);
  }

  private function initEliminarCuentaContableCentralizacionData(){
    $r = self::getRequest();

    $this->initTipoNotaCredito($r);
    $this->initCuentaContableSeleccionada($r);
  }

  private function initTipoNotaCredito($r, $validaSinMovimientoStock = false){

    if(!isset($r->dc_tipo) or !preg_match("/^\d+$/",$r->dc_tipo)){
      $this->getErrorMan()->showWarning("El formato del tipo de nota crédito especificado ".
                                        "es inválido o este no fue indicado,".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    }

    $db = $this->getConnection();
    $this->tipo = $db->getRowById("tb_tipo_nota_credito_proveedor",$r->dc_tipo,"dc_tipo");

    if($this->tipo === false or $this->tipo->dm_activo == 0){
      $this->getErrorMan()->showWarning("El tipo de nota de crédito seleccionado no fue encontrado o este fue eliminado,".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    }

    if($validaSinMovimientoStock):
      if($this->tipo->dm_retorna_stock == 1):
        $this->getErrorMan()->showWarning("El tipo de nota de crédito seleccionado tiene habilitado el movimiento de stock.".
                                          " Este tipo de nota de crédito posee la habilidad de seleccionar la cuenta contable a partir de".
                                          " los productos seleccionados por lo que no requiere asociación de cuentas directa.");
        exit;
      endif;
    endif;

  }

  private function initCuentasContables($dc_tipo){
    $db = $this->getConnection();

    $select = $db->prepare(
                $db->select('tb_tipo_nota_credito_proveedor_cuenta_contable t'.
                            ' JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = t.dc_cuenta_contable',
                            'cc.dc_cuenta_contable, cc.dg_codigo, cc.dg_cuenta_contable',
                            't.dc_tipo_nota_credito = ?'));
    $select->bindValue(1, $dc_tipo, PDO::PARAM_INT);
    $db->stExec($select);

    $this->cuentas_contables = $select->fetchAll(PDO::FETCH_OBJ);

  }

  private function initCuentaContableSeleccionada($r){

    if(!isset($r->dc_cuenta_contable) or !preg_match("/^\d+$/",$r->dc_cuenta_contable)):
      $this->getErrorMan()->showWarning("El formato de la cuenta contable seleccionada ".
                                        "es inválido o este no fue indicado, ".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    $db = $this->getConnection();
    $this->cuenta_contable = $db->getRowById("tb_cuenta_contable",$r->dc_cuenta_contable,"dc_cuenta_contable");

    if($this->cuenta_contable === false or $this->cuenta_contable->dm_activo == 0):
      $this->getErrorMan->showWarning("La cuenta contable seleccionada es inválida o esta no fue encontrada, ".
                                      "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

  }

  private function validaCuentaCentralizacion($dc_tipo, $dc_cuenta_contable){
    $db = $this->getConnection();

    $select = $db->prepare($db->select('tb_tipo_nota_credito_proveedor_cuenta_contable','true','dc_tipo_nota_credito = ? AND dc_cuenta_contable = ?'));
    $select->bindValue(1, $dc_tipo, PDO::PARAM_INT);
    $select->bindValue(2, $dc_cuenta_contable, PDO::PARAM_INT);

    $db->stExec($select);

    if($select->fetch() != false):
      $this->getErrorMan()->showWarning("La cuenta contable que intenta agregar ya fue agregada anteriormente, ".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

  }

  private function insertarCuentaContableCentralizacion(){
    $db = $this->getConnection();

    $insertar = $db->prepare($db->insert('tb_tipo_nota_credito_proveedor_cuenta_contable',array(
      'dc_cuenta_contable' => '?',
      'dc_tipo_nota_credito' => '?'
    )));
    $insertar->bindValue(1, $this->cuenta_contable->dc_cuenta_contable, PDO::PARAM_INT);
    $insertar->bindValue(2, $this->tipo->dc_tipo, PDO::PARAM_INT);

    $db->stExec($insertar);

  }

  private function eliminarCuentaContableCetralizacion(){
    $db = $this->getConnection();

    $delete = $db->prepare($db->delete('tb_tipo_nota_credito_proveedor_cuenta_contable','dc_tipo_nota_credito = ? AND dc_cuenta_contable = ?'));
    $delete->bindValue(1, $this->tipo->dc_tipo, PDO::PARAM_INT);
    $delete->bindValue(2, $this->cuenta_contable->dc_cuenta_contable, PDO::PARAM_INT);
    $db->stExec($delete);

    return $delete->rowCount();

  }

}
