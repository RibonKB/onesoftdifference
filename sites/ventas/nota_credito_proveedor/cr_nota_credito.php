<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/Factory.class.php");
?>
<div id="secc_bar">Creación de Nota de crédito</div>
<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Start(Factory::buildUrl('CrearNotaCredito','ventas','crearForm','nota_credito_proveedor'),"cr_nota_credito","cValidar");
	$form->Header("<strong>Indique el RUT del proveedor del que se le hará la nota de crédito</strong>
	<br />Los criterios de búsqueda son el RUT, Razon social o giro del proveedor");
	$form->Text("RUT Proveedor","dg_rut",1,20);
	echo('<br />');
	$form->Listado('Tipo de Nota de Crédito','dc_tipo','tb_tipo_nota_credito_proveedor',array('dc_tipo','dg_tipo'),1);
	echo('<div id="tipo_data"></div>');
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
$("#dg_rut").autocomplete('sites/proc/autocompleter/proveedor.php',{
	formatItem: function(row){ return row[1]+" ("+row[0]+") "+row[2]; },
	minChars: 2,
	width:300
});

$('#dc_tipo').change(function(){
	var dc_tipo = parseInt($(this).val());
	if(dc_tipo){
		pymerp.loadFile('sites/ventas/nota_credito_proveedor/proc/get_descripcion_tipo_nota_credito.php','#tipo_data','dc_tipo='+dc_tipo);
	}else{
		$('#tipo_data').html('');
	}
});
</script>
