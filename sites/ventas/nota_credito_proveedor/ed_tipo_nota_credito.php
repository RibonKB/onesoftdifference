<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo = intval($_POST['id']);

$datos = $db->prepare($db->select('tb_tipo_nota_credito_proveedor','dg_tipo, dg_descripcion, dm_retorna_stock, dm_multiple_factura, dc_cuenta_contable_centralizacion', 'dc_tipo = ? AND dc_empresa = ?'));
$datos->bindValue(1,$dc_tipo,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('No se encontro el tipo de nota de crédito');
	exit;
}

?>

<div class="secc_bar">  
	Editar tipo de nota de crédito 
</div>

<div class="panes">
<?php $form->Start('sites/ventas/nota_credito_proveedor/proc/editar_tipo_nota_credito.php', 'ed_tipo_nota_credito') ?>
<?php $form->Header('<b>Ingrese los datos para actualizar el tipo nota de crédito</b><br />Los campos marcados con [*] son obligatorios') ?>

	<?php $form->Section(); 
    		$form->Text('Nombre','dg_tipo_ed',true,Form::DEFAULT_TEXT_LENGTH,$datos->dg_tipo); 
			$form->Textarea('Descripción','dg_descripcion_ed',0,$datos->dg_descripcion);   
        $form->EndSection(); 	
		
	$form->Section();
			$form->Radiobox('Retorna Stock','dm_retorna_stock_ed',array('SI','NO'),$datos->dm_retorna_stock==1?0:1);
			$form->Radiobox('Múltiples facturas','dm_multiple_factura_ed',array('SI','NO'),$datos->dm_multiple_factura==1?0:1);
	$form->EndSection();
		
	$form->Section();
		 	$form->DBSelect('Cuenta contable','dc_cuenta_contable_centralizacion_ed','tb_cuenta_contable',array('dc_cuenta_contable',		'dg_cuenta_contable'),1,$datos->dc_cuenta_contable_centralizacion);
	$form->EndSection();		
		?>
        
             
        <?php $form->Hidden('dc_tipo_ed',$dc_tipo) ?>
    <?php $form->End('Editar','editbtn') ?>
    
    
</div>
