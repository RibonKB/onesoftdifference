<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_nota_credito = intval($_POST['id']);

$nota_credito = $db->prepare($db->select('tb_nota_credito_proveedor','dq_nota_credito','dc_nota_credito = ?'));
$nota_credito->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$db->stExec($nota_credito);
$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);

if($nota_credito === false){
	$error_man->showWarning('No se ha encontrado la nota de crédito, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

$data = $db->prepare(
				$db->select('tb_nota_credito_proveedor_factura p
				JOIN tb_factura_compra fc ON fc.dc_factura = p.dc_factura',
				'p.dq_monto_saldado, fc.dq_factura, fc.dq_folio',
				'p.dc_nota_credito = ? AND dm_estado = ?')
		);
$data->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$data->bindValue(2,'A',PDO::PARAM_STR);
$db->stExec($data);

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Liberación Facturas de compra pagadas por nota de crédito <b><?php echo $nota_credito->dq_nota_credito ?></b>
</div>
<div class="panes">
	<?php $form->Start('sites/ventas/nota_credito_proveedor/proc/liberar_facturas_compra.php','form_liberar_facturas') ?>
	<div class="info">
		Se van a liberar las siguientes facturas de compra
	</div>
	<div class="alert">
		Atención: solo se incluyen las facturas asignadas al crear la nota de crédito,
		<br />
		para liberar los otros saldos ocupados anule las propuestas de pago relacionadas.
	</div>
	<br />
	<table class="tab" width="90%" align="center">
		<thead>
			<th>Factura de compra</th>
			<th>Folio</th>
			<th>Monto Saldado</th>
		</thead>
		<tbody>
		<?php while($factura = $data->fetch(PDO::FETCH_OBJ)): ?>
			<tr>
				<td><b><?php echo $factura->dq_factura ?></b></td>
				<td><?php echo $factura->dq_folio ?></td>
				<td align="right"><?php echo moneda_local($factura->dq_monto_saldado) ?></td>
			</tr>
		<?php endwhile; ?>
		</tbody>
	</table>
	<?php
		$form->Hidden('dc_nota_credito',$dc_nota_credito);
		$form->End('Liberar');
	?>
</div>