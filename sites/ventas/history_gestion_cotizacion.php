<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/cotizacion.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_cotizacion_avance WHERE dc_cotizacion = {$_POST['id']} ORDER BY df_creacion DESC) a
LEFT JOIN (SELECT * FROM tb_cotizacion_estado WHERE dc_empresa={$empresa}) e ON a.dc_estado_cotizacion = e.dc_estado",
'a.dg_gestion,a.dg_contacto,a.dc_estado_cotizacion,e.dg_estado,a.dc_avance,
DATE_FORMAT(a.df_creacion,"%d/%m/%Y %H:%i") as df_creacion,
DATE_FORMAT(a.df_proxima_actividad,"%d/%m/%Y") as df_estimada,
DATE_FORMAT(a.df_cierre_actividad,"%d/%m/%Y") as df_cierre');

echo("<div class='panes' style='width:850px;'>");
if(!count($data)){
	$error_man->showAviso("La {$lang['cot_doc']} especificada no posee actividades en su proceso");
	exit;
}

echo("<table class='tab' width='100%'>
<caption>Últimas actividades realizadas sobre la {$lang['cot_doc']}</caption>
<thead><tr>
	<th>Gestion</th>
	<th width='150'>Contacto</th>
	<th width='120'>Estado</th>
	<th width='90'>Fecha creación</th>
	<th width='90'>Cierre estimado</th>
	<th width='90'>Fecha cierre</th>
</thead><tbody>");

foreach($data as $d){
	echo("<tr>
			<td>{$d['dg_gestion']}</td>
			<td>{$d['dg_contacto']}</td>
			<td>{$d['dg_estado']}</td>
			<td>{$d['df_creacion']}</td>
			<td>{$d['df_estimada']}</td>
			<td>{$d['df_cierre']}</td>
		</tr>");
}
echo("</tbody></table><hr />");

?>
</div>