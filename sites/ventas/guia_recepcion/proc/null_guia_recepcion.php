<?php
define('MAIN',1);
include('../../../../inc/init.php');

/**
*	Verificar guía de recepción, obtener nota de venta u orden de servicio
*	
*	IN: dc_guia_recepcion
*	OUT: dc_nota_venta, dc_orden_servicio
**/
$dc_guia_recepcion = $_POST['id_guia_recepcion'];

$tb_guia_recepcion = $db->prepare($db->select('tb_guia_recepcion',
	'dc_nota_venta,dc_orden_servicio,dc_orden_compra,dq_guia_recepcion',
	'dc_guia_recepcion = ? AND dc_empresa = ?'));

$tb_guia_recepcion->bindValue(1,$dc_guia_recepcion,PDO::PARAM_INT);
$tb_guia_recepcion->bindValue(2,$empresa,PDO::PARAM_INT);

$db->stExec($tb_guia_recepcion);
$tb_guia_recepcion = $tb_guia_recepcion->fetch(PDO::FETCH_OBJ);

if($tb_guia_recepcion === false){
	$error_man->showWarning("La guía de recepción seleccionada es inválida, compruebe que no a intentado acceder desde un lugar indebido");
	exit;
}

$dc_nota_venta = $tb_guia_recepcion->dc_nota_venta;
$dc_orden_servicio = $tb_guia_recepcion->dc_orden_servicio;
$dc_orden_compra = $tb_guia_recepcion->dc_orden_compra;
$dq_guia_recepcion = $tb_guia_recepcion->dq_guia_recepcion;

	
/**
*	Obtener Detalle guía repepción con nota de venta
*	
*	IN: dc_guia_recepcion
*	OUT: detalle
**/
if($dc_nota_venta != 0){
	$detalle = $db->prepare($db->select("tb_guia_recepcion_detalle grd
	JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = grd.dc_detalle_nota_venta
	LEFT JOIN tb_producto p ON p.dc_producto = grd.dc_producto",
	'grd.dc_producto, (grd.dq_precio*grd.dq_cantidad) dq_monto, grd.dc_detalle_nota_venta, grd.dq_cantidad, grd.dc_detalle_orden_compra,
	grd.dc_bodega, (nvd.dc_recepcionada-nvd.dc_despachada) dc_disponible, p.dg_codigo, p.dg_producto, p.dq_precio_compra',
	"grd.dc_guia_recepcion = ?"));
}
/**
*	Obtener Detalle guía repepción con orden de servicio
*	
*	IN: dc_guia_recepcion
**/
else if($dc_orden_servicio != 0){
	$detalle = $db->prepare($db->select("tb_guia_recepcion_detalle grd
	JOIN tb_orden_servicio_factura_detalle osd ON osd.dc_detalle = grd.dc_detalle_nota_venta
	LEFT JOIN tb_producto p ON p.dc_producto = grd.dc_producto",
	'grd.dc_producto, (grd.dq_precio*grd.dq_cantidad) dq_monto, grd.dc_detalle_orden_servicio, grd.dq_cantidad, grd.dc_detalle_orden_compra,
	grd.dc_bodega, (osd.dc_recepcionada-osd.dq_despachado) dc_disponible, p.dg_codigo, p.dg_producto, p.dq_precio_compra',
	"grd.dc_guia_recepcion = ?"));
}

/**
*	Obtener Detalle guía repepción
*	
*	IN: dc_guia_recepcion
**/
else{
	$detalle = $db->prepare($db->select('tb_guia_recepcion_detalle grd
	LEFT JOIN tb_producto p ON p.dc_producto = grd.dc_producto',
	'grd.dc_producto, (grd.dq_precio*grd.dq_cantidad) dq_monto, grd.dq_cantidad, grd.dc_bodega, p.dg_codigo, p.dg_producto, grd.dc_detalle_orden_compra, p.dq_precio_compra',
	"grd.dc_guia_recepcion = ?"));
}


$detalle->bindValue(1,$dc_guia_recepcion,PDO::PARAM_INT);	
$db->stExec($detalle);
$detalle = $detalle->fetchAll(PDO::FETCH_OBJ);

/**
*	Comprobar cantidades no despachadas
*	
*	IN: detalles
**/

if($dc_nota_venta != 0 || $dc_orden_servicio != 0){
	foreach($detalle as $d){
		
		if($d->dq_cantidad > $d->dc_disponible){
			$error_man->showWarning("No puede anularse la guía de recepción debido a que se han despachado los productos reservados.<br />
			Anule las guías de despacho asociadas o consulte con un administrador");
			exit;
		}
		
	}
}


/**
*	Obtener tipo de movimiento por anulación de guia de recepción
*	
*	IN: dc_empresa
*	OUT: dc_tipo_movimiento
**/

$dc_tipo_movimiento = $db->doQuery($db->select('tb_configuracion_logistica','dc_tipo_movimiento_anula_guia_recepcion',"dc_empresa = {$empresa}"))
							->fetch(PDO::FETCH_OBJ)
							->dc_tipo_movimiento_anula_guia_recepcion;
							
if($dc_tipo_movimiento == 0){
	$error_man->showAviso("No se ha configurado un tipo de movimiento para salida de stocks por anulación, consulte con un administrador para continuar.");
	exit;
}

require_once('../../proc/ventas_functions.php');
$db->start_transaction();

$db->doExec($db->update('tb_guia_recepcion',array(
	'dm_nula' => '"1"'
),'dc_guia_recepcion = '.$dc_guia_recepcion));

/*
*	Rebajar Stock Reservado Nota de venta
*	
*	IN: detalles
*/

if($dc_nota_venta != 0){
	foreach($detalle as $d){
		$valida = bodega_RebajarStockReservado($d->dc_producto,$d->dq_cantidad,$d->dc_bodega);
		if($valida != 1){
			$error_man->showWarning("El producto <b>{$d->dg_codigo} <i>{$d->dg_producto}</i></b> No fue encontrado en la bodega correspondiente,
			por favor verifique la disponibilidad de stock");
			exit;
		}
	}
}

/*
*	Rebajar Stock Reservado Orden de servicio
*	
*	IN: detalles
*/

else if($dc_orden_servicio != 0){
	foreach($detalle as $d){
		$valida = bodega_RebajarStockReservadoOS($d->dc_producto,$d->dq_cantidad,$d->dc_bodega);
		if($valida != 1){
			$error_man->showWarning("El producto <b>{$d->dg_codigo} <i>{$d->dg_producto}</i></b> No fue encontrado en la bodega correspondiente,
			por favor verifique la disponibilidad de stock");
			exit;
		}
	}
}

/*
*	Rebajar Stock Libre
*	
*	IN: detalles
*/

else{
	foreach($detalle as $d){
		$valida = bodega_RebajarStock($d->dc_producto,$d->dq_cantidad,$d->dc_bodega);
		if($valida != 1){
			$error_man->showWarning("El producto <b>{$d->dg_codigo} <i>{$d->dg_producto}</i></b> No fue encontrado en la bodega correspondiente,
			por favor verifique la disponibilidad de stock");
			exit;
		}
	}
}

/**
*	Rebajar cantidad recepcionada detalle nota de venta
*	
*	IN: detalle
**/

if($dc_nota_venta != 0){
	
	$update_recepcionado = $db->prepare($db->update('tb_nota_venta_detalle',array(
		"dc_recepcionada" => "dc_recepcionada - ?"
	),"dc_nota_venta_detalle = ?"));
	
	$update_recepcionado->bindParam(1,$cantidad,PDO::PARAM_INT);
	$update_recepcionado->bindParam(2,$id_detalle,PDO::PARAM_INT);
	
	foreach($detalle as $d){
		$cantidad = $d->dq_cantidad;
		$id_detalle = $d->dc_detalle_nota_venta;
		
		$db->stExec($update_recepcionado);
		
		$db->doExec($db->update('tb_nota_venta',array(
			"dc_recepcionada" => "dc_recepcionada - ".$cantidad
		),'dc_nota_venta = '.$dc_nota_venta));
	}
}

/**
*	Rebajar cantidad recepcionada detalle orden de servicio
*	
*	IN: detalle
**/

else if($dc_nota_venta != 0){
	
	$update_recepcionado = $db->prepare($db->update('tb_orden_servicio_factura_detalle',array(
		"dc_recepcionada" => "dc_recepcionada - ?"
	),"dc_detalle = ?"));
	
	$update_recepcionado->bindParam(1,$cantidad,PDO::PARAM_INT);
	$update_recepcionado->bindParam(2,$id_detalle,PDO::PARAM_INT);
	
	foreach($detalle as $d){
		$cantidad = $d->dq_cantidad;
		$id_detalle = $d->dc_detalle_orden_servicio;
		
		$db->stExec($update_recepcionado);
	}
}

/**
*	Rebajar cantidad recepcionada detalle orden de compra
*	
*	IN: detalle
**/

$update_recepcionado_oc = $db->prepare($db->update('tb_orden_compra_detalle',array(
	"dc_recepcionada" => "dc_recepcionada - ?"
),"dc_detalle = ?"));

$update_recepcionado_oc->bindParam(1,$cantidad,PDO::PARAM_INT);
$update_recepcionado_oc->bindParam(2,$id_detalle,PDO::PARAM_INT);

foreach($detalle as $d){
	$cantidad = $d->dq_cantidad;
	$id_detalle = $d->dc_detalle_orden_compra;
	
	$db->stExec($update_recepcionado_oc);
}

/**
*	Registrar movimiento de bodega
*	
*	IN: dc_guia_recepcion, dc_nota_venta, dc_tipo_movimiento, dc_cantidad, dc_producto, dc_bodega, dq_monto
**/

$registrar_movimiento = $db->prepare($db->insert('tb_movimiento_bodega',array(
	"dc_tipo_movimiento" => $dc_tipo_movimiento,
	"dc_bodega_salida" => '?',
	"dq_monto" => '?',
	"dc_guia_recepcion" => $dc_guia_recepcion,
	"dc_orden_compra" => $dc_orden_compra,
	"dc_nota_venta" => $dc_nota_venta,
	"dc_orden_servicio" => $dc_orden_servicio,
	"dc_producto" => '?',
	"dq_cantidad" => '?',
	"dc_empresa" => $empresa,
	"df_creacion" => $db->getNow(),
	"dc_usuario_creacion" => $idUsuario
)));

$registrar_movimiento->bindParam(1,$bodega_salida,PDO::PARAM_INT);
$registrar_movimiento->bindParam(2,$monto,PDO::PARAM_STR);
$registrar_movimiento->bindParam(3,$producto,PDO::PARAM_INT);
$registrar_movimiento->bindParam(4,$cantidad,PDO::PARAM_INT);

foreach($detalle as $d){
	$bodega_salida = $d->dc_bodega;
	$monto = $d->dq_precio_compra;
	$producto = $d->dc_producto;
	$cantidad = $d->dq_cantidad;
	
	$db->stExec($registrar_movimiento);
}

/**
*	Anular factura de compra
*	
*	IN: dc_guia_recepcion
**/

$anular_factura_compra = $db->prepare($db->update('tb_factura_compra',array(
	'dm_nula' => 1
),"dc_guia_recepcion = ?"));
$anular_factura_compra->bindValue(1,$dc_guia_recepcion,PDO::PARAM_INT);
$db->stExec($anular_factura_compra);

/**
*	Anular guía de compra
*	
*	IN: dc_guia_recepcion
**/

$anular_guia_compra = $db->prepare($db->update('tb_guia_compra',array(
	'dm_nula' => 1
),"dc_guia_recepcion = ?"));
$anular_guia_compra->bindValue(1,$dc_guia_recepcion,PDO::PARAM_INT);
$db->stExec($anular_guia_compra);

/**
*	En caso de no quedar compras vigentes para los productos se debe reiniciar el valor costo a cero
*/

$contar_compras = $db->prepare($db->select('tb_guia_recepcion_detalle d
									JOIN tb_guia_recepcion g ON g.dc_guia_recepcion = d.dc_guia_recepcion',
									'COUNT(*) cantidad',
									'd.dc_producto = ? AND g.dm_nula = 0'));
$contar_compras->bindParam(1,$dc_producto,PDO::PARAM_INT);

$editar_costo = $db->prepare($db->update('tb_producto',array('dq_precio_compra'=>0),'dc_producto = ?'));
$editar_costo->bindParam(1,$dc_producto,PDO::PARAM_INT);

foreach($detalle as $d){
	$dc_producto = $d->dc_producto;
	
	$db->stExec($contar_compras);
	
	if($contar_compras->fetch(PDO::FETCH_OBJ)->cantidad == 0){
		$db->stExec($editar_costo);
	}
	
	$contar_compras->closeCursor();
	
}

/**
*	Confirmar anulación de guía de recepción
**/

$db->commit();
$error_man->showAviso("Atención: se ha anulado la guia de recepción <b>{$dq_guia_recepcion}</b>");
?>