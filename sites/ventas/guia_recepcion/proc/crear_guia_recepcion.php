<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../proc/ventas_functions_old.php");

if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_orden_compra = intval($_POST['gr_oc_id']);

$orden_compra = $db->select('tb_orden_compra','dq_orden_compra, dq_monto_pagado, dq_pago_usado, dq_monto_liberado, dc_proveedor',"dc_empresa = {$empresa} AND dc_orden_compra = {$dc_orden_compra}");
$orden_compra = array_shift($orden_compra);

if($orden_compra == NULL){
	$error_man->showWarning('La orden de compra que se intenta procesar es inválida');
	exit;
}

if($orden_compra['dq_monto_pagado'] >= $orden_compra['dq_pago_usado']){
	if($empresa_conf['dc_tipo_movimiento_pago_anticipado'] == 0){
		$error_man->showWarning('No puede continuar con la recepción debido a que no está configurado una cuenta para anticipos.');
		exit;
	}
}

if($_POST['gr_tipo_entrada'] == 'factura'){
	$tipo_documento = 'tb_factura_compra';
	$campo_numero = 'dq_factura';
	$campo_id_documento = 'dc_factura';
}else if($_POST['gr_tipo_entrada'] == 'guia'){
	$tipo_documento = 'tb_guia_compra';
	$campo_numero = 'dq_guia_compra';
	$campo_id_documento = 'dc_guia_compra';
}

/*if(isset($tipo_documento)){
	$valida_folio = $db->select($tipo_documento,'true',"dq_folio = {$_POST['gr_factura_compra']} AND dm_nula = 0");
	if(count($valida_folio)){
		$error_man->showWarning('Ya existe un documento con el folio ingresado, compruebe los datos de entrada y vuelva a intentarlo');
		exit;
	}
}*/

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_guia_recepcion']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$tipo_mov_guia = explode('|',$_POST['gr_tipo']);
$_POST['gr_tipo'] = $tipo_mov_guia[0];
$gr_bodega = $tipo_mov_guia[2];
$tipo_mov_guia = $tipo_mov_guia[1];

$detalles_pendientes_oc	= 	$db->select('tb_orden_compra_detalle d
										 JOIN tb_producto p ON p.dc_producto = d.dc_producto
										 JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
										'd.dc_detalle,d.dc_producto,d.dq_precio,d.dc_proveedor,d.dq_cantidad,d.dc_recepcionada,
										 p.dq_precio_compra, t.dm_recalcula_precio, t.dm_controla_inventario',
										'dc_orden_compra='.$dc_orden_compra.' AND dq_cantidad > dc_recepcionada');
										
$detalle_oc = array();
foreach($detalles_pendientes_oc as $d){
	$detalle_oc[$d['dc_detalle']] = $d;
}
//echo'<pre>';var_dump($detalle_oc);echo'</pre>';
unset($detalles_pendientes_oc);


$db->start_transaction();

$dq_guia_recepcion = doc_GetNextNumber('tb_guia_recepcion','dq_guia_recepcion',2,'df_fecha_emision');

$guia_recepcion = $db->insert('tb_guia_recepcion',array(
	"dq_guia_recepcion" => $dq_guia_recepcion,
	"dc_tipo_guia" => $_POST['gr_tipo'],
	"dc_orden_compra" => $_POST['gr_oc_id'],
	"dc_nota_venta" => $_POST['gr_nv_id'],
	"dc_orden_servicio" => $_POST['gr_os_id'],
	"df_fecha_emision" => 'NOW()',
	"dc_usuario_creacion" => $idUsuario,
	"dc_empresa" => $empresa,
	"dg_observacion" => $_POST['gr_observacion']
));

$recepcionada = 0;
foreach($_POST['prod'] as $i => $v){
	$v = explode('|',$v);
	$det_oc = $v[0];
	$ceco = $v[2];
	if($_POST['gr_nv_id'] != 0){
		$det_nv = $v[1];
		$det_os = 0;
	}else if($_POST['gr_os_id'] != 0){
		$det_nv = 0;
		$det_os = $v[1];
	}else{
		$det_nv = 0;
		$det_os = 0;
	}
	
	$_POST['bodega'][$i] = $_POST['bodega'][$i]!=0?$_POST['bodega'][$i]:$gr_bodega;
	
	$db->insert('tb_guia_recepcion_detalle',array(
		"dc_guia_recepcion" => $guia_recepcion,
		"dc_detalle_orden_compra" => $det_oc,
		"dc_detalle_nota_venta" => $det_nv,
		"dc_detalle_orden_servicio" => $det_os,
		'dc_producto' => $_POST['prod_id'][$i],
		"dq_cantidad" => $_POST['cant'][$i],
		'dq_precio' => $_POST['precio'][$i],
		"dc_bodega" => $_POST['bodega'][$i],
		"dc_responsable" => $_POST['responsable'][$i],
		"dc_ceco" => $ceco
	));
	
	if(!isset($detalle_oc[$det_oc])){
		$error_man->showWarning("Se ha encontrado un error mientras se intentaba asignar el stock a la recepción, no se han efectuado cambios.");
		$db->rollback();
		exit();
	}
	$det_data = $detalle_oc[$det_oc];
	
	//Si el detalle se salda por completo se elimina del listado
	if(($det_data['dq_cantidad']-$det_data['dc_recepcionada']-$_POST['cant'][$i]) <= 0){
		unset($detalle_oc[$det_oc]);
	}
	
	//Calcular PMP
	if($det_data['dm_recalcula_precio'] == 1){
		$dc_stock_actual = bodega_ComprobarStock($det_data['dc_producto'],$_POST['bodega'][$i]);
		$dq_pmp_actual = floatval($det_data['dq_precio_compra']);
		
		if($dq_pmp_actual != 0){
		  $dq_costo_stock = (($dc_stock_actual*$dq_pmp_actual)+($_POST['cant'][$i]*$det_data['dq_precio']))/($_POST['cant'][$i]+$dc_stock_actual);
		
		  $diff = (abs($dq_pmp_actual-$det_data['dq_precio'])*100)/$dq_pmp_actual;
		  
		  //echo'<pre>';var_dump('$dq_pmp_actual:',$dq_pmp_actual,'$dq_costo_stock:',$dq_costo_stock,'$diff:',$diff);echo'</pre>';
		  
		  if($diff > 30){
			$error_man->showWarning('No se puede procesar la solicitud de compra debido a que el precio indicado para el producto excede al precio anterior en un gran porcentaje');
			$db->rollback();
			exit;
		  }
		}else{
			$dq_costo_stock = $det_data['dq_precio'];
		}
		
		$db->update('tb_producto',array(
			'dq_precio_compra' => $dq_costo_stock
		),"dc_producto = {$det_data['dc_producto']}");
	}else{
		$dq_costo_stock = $det_data['dq_precio'];
	}
	
    if($det_data['dm_controla_inventario'] == 0){
        if($_POST['gr_nv_id'] != 0){
            $db->update('tb_stock',array(
                'dq_stock' => "dq_stock+{$_POST['cant'][$i]}",
                'dq_stock_reservado' => "dq_stock_reservado+{$_POST['cant'][$i]}"
            ),"dc_bodega={$_POST['bodega'][$i]} AND dc_producto={$det_data['dc_producto']}");

            if($db->affected_rows() == 0){

                $db->insert('tb_stock',array(
                    'dc_producto' => $det_data['dc_producto'],
                    'dc_bodega' => $_POST['bodega'][$i],
                    'dq_stock' => $_POST['cant'][$i],
                    'dq_stock_reservado' => $_POST['cant'][$i]
                ));

            }
        }else if($_POST['gr_os_id'] != 0){
            $db->update('tb_stock',array(
                'dq_stock' => "dq_stock+{$_POST['cant'][$i]}",
                'dq_stock_reservado_os' => "dq_stock_reservado_os+{$_POST['cant'][$i]}"
            ),"dc_bodega={$_POST['bodega'][$i]} AND dc_producto={$det_data['dc_producto']}");

            if($db->affected_rows() == 0){

                $db->insert('tb_stock',array(
                    'dc_producto' => $det_data['dc_producto'],
                    'dc_bodega' => $_POST['bodega'][$i],
                    'dq_stock' => $_POST['cant'][$i],
                    'dq_stock_reservado_os' => $_POST['cant'][$i]
                ));

            }

        }else{
            $db->update('tb_stock',array(
                'dq_stock' => "dq_stock+{$_POST['cant'][$i]}"
            ),"dc_bodega={$_POST['bodega'][$i]} AND dc_producto={$det_data['dc_producto']}");

            if($db->affected_rows() == 0){

                $db->insert('tb_stock',array(
                    'dc_producto' => $det_data['dc_producto'],
                    'dc_bodega' => $_POST['bodega'][$i],
                    'dq_stock' => $_POST['cant'][$i]
                ));

            }
        }

        $db->insert('tb_movimiento_bodega',array(
            'dc_proveedor' => $orden_compra['dc_proveedor'],
            'dc_tipo_movimiento' => $tipo_mov_guia,
            'dc_bodega_entrada' => $_POST['bodega'][$i],
            'dq_monto' => $dq_costo_stock,
            'dc_guia_recepcion' => $guia_recepcion,
            'dc_orden_compra' => $_POST['gr_oc_id'],
            'dc_nota_venta' => $_POST['gr_nv_id'],
            'dc_orden_servicio' => $_POST['gr_os_id'],
            'dc_producto' => $det_data['dc_producto'],
            'dq_cantidad' => $_POST['cant'][$i],
            'dc_empresa' => $empresa,
            'df_creacion' => 'NOW()',
            'dc_usuario_creacion' => $idUsuario
        ));
    
    }
	
	$recepcionada += $_POST['cant'][$i];
	
	$db->update('tb_orden_compra_detalle',array('dc_recepcionada' => "dc_recepcionada+{$_POST['cant'][$i]}"),"dc_detalle={$det_oc}");
	if($det_nv != 0){
		$db->update('tb_nota_venta_detalle',array('dc_recepcionada' => "dc_recepcionada+{$_POST['cant'][$i]}"),"dc_nota_venta_detalle={$det_nv}");
	}else if($det_os != 0){
		$db->update('tb_orden_servicio_factura_detalle',array('dc_recepcionada' => "dc_recepcionada+{$_POST['cant'][$i]}"),"dc_detalle={$det_os}");
	}
}

//Si se eliminar todos los detalles pendientes de recepción entonces la orden de compra quedó completamente saldada
if(!count($detalle_oc)){
	$oc_completa = true;
}else{
	$oc_completa = false;
}

//Calcular el monto a pagar inicial de la factura.
$total_pagar = round(floatval($orden_compra['dq_monto_pagado'])-floatval($orden_compra['dq_pago_usado'])-floatval($orden_compra['dq_monto_liberado']),$empresa_conf['dn_decimales_local']);

if($total_pagar > 0){
	$total_factura = round(floatval(str_replace(',','',$_POST['gr_total'])),$empresa_conf['dn_decimales_local']);
	
	if($oc_completa){
		
		if($total_factura <= $total_pagar){
			$dq_monto_pagado_factura = $total_factura;
			$dq_saldo_favor_factura = $total_pagar-$total_factura;
			
			$db->update('tb_proveedor',array('dq_saldo_favor' => 'dq_saldo_favor+'.$dq_saldo_favor_factura),"dc_proveedor = ".$orden_compra['dc_proveedor']);
		}else{
			$dq_monto_pagado_factura = $total_pagar;
			$dq_saldo_favor_factura = 0;
		}
		
		$dq_pago_usado = $dq_monto_pagado_factura;
		
	}else{
		
		if($total_factura >= $total_pagar){
			$dq_monto_pagado_factura = $total_pagar;
			$dq_saldo_favor_factura = 0;
		}else{
			$dq_monto_pagado_factura = $total_factura;
			$dq_saldo_favor_factura = 0;
		}
		
		$dq_pago_usado = $dq_monto_pagado_factura;
		
	}//Fin pago inicial factura de compra
	
	$db->update('tb_orden_compra',array('dq_pago_usado' => 'dq_pago_usado+'+$dq_pago_usado),'dc_orden_compra = '.$dc_orden_compra);
}else{
	$dq_monto_pagado_factura = 0;
	$dq_saldo_favor_factura = 0;
}

if($_POST['gr_nv_id'] != 0){
	$db->update('tb_nota_venta',array('dc_recepcionada' => "dc_recepcionada+{$recepcionada}"),"dc_nota_venta={$_POST['gr_nv_id']}");
}

/*
*	Ingreso de factura de compra
*/

if($_POST['gr_tipo_entrada'] == 'existente'){
	$factura_compra = intval($_POST['dc_factura_old']);
	
	$db->update('tb_factura_compra',array(
		'dq_monto_pagado' => 'dq_monto_pagado+'.$dq_monto_pagado_factura
	),"dc_factura = {$factura_compra}");
	
}else{

	$exento = isset($_POST['gr_exento'])?str_replace(',','',$_POST['gr_exento']):0;
	
	$factura_data = array(
		'dc_nota_venta' => $_POST['gr_nv_id'],
		'dc_orden_servicio' => $_POST['gr_os_id'],
		'dc_proveedor' => $orden_compra['dc_proveedor'],
		'dc_medio_pago' => $_POST['gr_fc_medio_pago'],
		'dc_tipo' => $_POST['gr_fc_tipo_factura'],
		$campo_numero => doc_GetNextNumber($tipo_documento,$campo_numero,4),
		'dq_folio' => $_POST['gr_factura_compra'],
		'dg_referencia' => $_POST['dg_referencia'],
		'dg_comentario' => $_POST['gr_observacion'],
		'df_emision' => $db->sqlDate($_POST['gr_fc_emision']),
		'df_creacion' => 'NOW()',
		'df_vencimiento' => $db->sqlDate($_POST['gr_fc_vencimiento']),
		'df_libro_compra' => $db->sqlDate($_POST['gr_fc_fecha_contable']),
		'dc_orden_compra' => $_POST['gr_oc_id'],
		'dq_exento' => $exento,
		'dq_descuento' => round(floatval(str_replace(',','',$_POST['gr_descuento'])),$empresa_conf['dn_decimales_local']),
		'dq_neto' => round(floatval(str_replace(',','',$_POST['gr_neto'])),$empresa_conf['dn_decimales_local']),
		'dq_iva' => round(floatval(str_replace(',','',$_POST['gr_iva'])),$empresa_conf['dn_decimales_local']),
		'dq_total' => round(floatval(str_replace(',','',$_POST['gr_total'])),$empresa_conf['dn_decimales_local']),
		'dc_usuario_creacion' => $idUsuario,
		'dc_empresa' => $empresa
	);
	
	if($tipo_documento == 'tb_factura_compra'){
		$factura_data['dq_monto_pagado'] = $dq_monto_pagado_factura;
		$factura_data['dq_saldo_favor'] = $dq_saldo_favor_factura;
	}
	
	$factura_compra = $db->insert($tipo_documento,$factura_data);
	
	
	
	foreach($_POST['prod'] as $i => $v){
		$v = explode('|',$v);
		$det_oc = $v[0];
		$ceco = $v[2];
		if($_POST['gr_nv_id'] != 0){
			$det_nv = $v[1];
			$det_os = 0;
		}else if($_POST['gr_os_id'] != 0){
			$det_nv = 0;
			$det_os = $v[1];
		}else{
			$det_nv = 0;
			$det_os = 0;
		}
		
		$db->insert($tipo_documento.'_detalle',array(
			$campo_id_documento => $factura_compra,
			'dc_producto' => $_POST['prod_id'][$i],
			'dc_detalle_nota_venta' => $det_nv,
			'dc_detalle_orden_servicio' => $det_os,
			'dc_detalle_orden_compra' => $det_oc,
			'dc_cantidad' => $_POST['cant'][$i],
			'dq_precio' => $_POST['precio'][$i],
			'dq_total' => $_POST['cant'][$i]*$_POST['precio'][$i],
			'dc_ceco' => $ceco
		));
	}

}

if($_POST['gr_tipo_entrada'] != 'guia'){
	$db->update('tb_guia_recepcion',array(
		'dc_factura' => $factura_compra
	),"dc_guia_recepcion = {$guia_recepcion}");
}

/**
*	Crear comprobante contable pago automatico de facturas de compra
*	En caso de existir pago inicial de la factura
*/
if($total_pagar > 0){
	
	$proveedor = $db->select('tb_proveedor','*',"dc_proveedor=".$orden_compra['dc_proveedor']);
	$proveedor = array_shift($proveedor);
	
	require_once("../../../contabilidad/stuff.class.php");
	
	$dq_comprobante = ContabilidadStuff::getCorrelativoComprobante_OLD();
	
	$dc_comprobante = $db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => $dq_comprobante,
		'dc_tipo_movimiento' => $empresa_conf['dc_tipo_movimiento_pago_anticipado'],
		'df_fecha_contable' => 'NOW()',
		'dc_mes_contable' => date('m'),
		'dc_anho_contable' => date('Y'),
		'dc_tipo_cambio' => 0,
		'dq_cambio' => 1,
		'dg_glosa' => "Uso saldo por pago orden de compra: {$orden_compra['dq_orden_compra']} Proveedor: {$proveedor['dg_razon']}",
		'dq_saldo' => $dq_pago_usado,
		'df_fecha_emision' => 'NOW()',
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario
	));
	
	//Detalle DEBE
	//Cuenta del proveedor
	
	$df1 = $db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => $dc_comprobante,
		'dc_cuenta_contable' => $proveedor['dc_cuenta_contable'],
		'dq_debe' => $dq_pago_usado,
		'dq_haber' => 0,
		'dg_glosa' => "Pago Anticipado proveedor: {$proveedor['dg_razon']}"
	));
        
    $db->insert('tb_comprobante_contable_detalle_analitico',array(
        'dc_cuenta_contable' => $proveedor['dc_cuenta_contable'],
        'dq_debe' => $dq_pago_usado,
        'dq_haber' => 0,
        'dg_glosa' => 'Pago Anticipado proveedor',
        'dc_detalle_financiero' => $df1,
        'dc_factura_compra' => $factura_compra,
        'dc_nota_venta' => $_POST['gr_nv_id'],
        'dc_orden_servicio' => $_POST['gr_os_id'],
        'dc_proveedor' => $proveedor['dc_proveedor'],
        'dc_tipo_proveedor' => $proveedor['dc_tipo_proveedor'],
        'dc_orden_compra' => $dc_orden_compra,
        'dc_guia_recepcion' => $guia_recepcion
    ));
	
	//Detalle HABER
	//Cuenta de anticipo Proveedor
	$df2 = $db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => $dc_comprobante,
		'dc_cuenta_contable' => $proveedor['dc_cuenta_anticipo'],
		'dq_debe' => 0,
		'dq_haber' => $dq_pago_usado,
		'dg_glosa' => "Pago Anticipado proveedor: {$proveedor['dg_razon']}"
	));
        
    $db->insert('tb_comprobante_contable_detalle_analitico',array(
        'dc_cuenta_contable' => $proveedor['dc_cuenta_anticipo'],
        'dq_debe' => 0,
        'dq_haber' => $dq_pago_usado,
        'dg_glosa' => 'Pago Anticipado proveedor',
        'dc_detalle_financiero' => $df2,
        'dc_factura_compra' => $factura_compra,
        'dc_nota_venta' => $_POST['gr_nv_id'],
        'dc_orden_servicio' => $_POST['gr_os_id'],
        'dc_proveedor' => $proveedor['dc_proveedor'],
        'dc_tipo_proveedor' => $proveedor['dc_tipo_proveedor'],
        'dc_orden_compra' => $dc_orden_compra,
        'dc_guia_recepcion' => $guia_recepcion
    ));
	
}

$db->commit();

$error_man->showConfirm("Se ha generado la guía de recepción");
echo("<div class='title'>El número de guía de recepción es <h1 style='margin:0;color:#000;'>{$dq_guia_recepcion}</h1></div>");

$error_man->showConfirm("Fueron agregados ".count($_POST['prod'])." elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/ventas/guia_recepcion/ver_guia_recepcion.php?id={$guia_recepcion}','guia_recepcion','width=800,height=600')\"> Haga clic aquí para ver la guía de recepción</a>");

?>