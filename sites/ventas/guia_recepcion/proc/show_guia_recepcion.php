<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_guia_recepcion WHERE dc_guia_recepcion = {$_POST['id']} AND dc_empresa={$empresa}) gr
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = gr.dc_nota_venta
JOIN tb_orden_compra oc ON oc.dc_orden_compra = gr.dc_orden_compra
JOIN tb_proveedor prov ON prov.dc_proveedor = oc.dc_proveedor
JOIN tb_tipo_guia_recepcion t ON t.dc_tipo = gr.dc_tipo_guia
LEFT JOIN tb_factura_compra fc ON fc.dc_factura = gr.dc_factura AND fc.dm_nula = 0",
'gr.dm_nula,gr.dq_guia_recepcion,DATE_FORMAT(gr.df_fecha_emision,"%d/%m/%Y %H:%i") df_fecha_emision,gr.dg_observacion,t.dg_tipo,oc.dq_orden_compra,nv.dq_nota_venta,
fc.dq_factura, fc.dq_folio, prov.dg_razon, prov.dg_rut');

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la guía de recepción especificada");
	exit();
}
$data = $data[0];

if($data['dm_nula'] == 1){
	$nula_info = '';
/*$null_data = $db->select("(SELECT * FROM tb_guia_recepcion_anulada WHERE dc_guia_recepcion = {$_POST['id']}) n
	JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo",'ma.dg_motivo,n.dg_comentario');
	if(count($null_data)){
		$null_data = $null_data[0];
		$nula_info = "<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
		<div class='info'>Info anulación</div>
		Motivo: <b>{$null_data['dg_motivo']}</b><br />
		Comentario: <b>{$null_data['dg_comentario']}</b>";
	}*/
$anulada = "<td rowspan='7'>
	<h3 class='alert'>GUIA RECEPCION ANULADA</h3>{$nula_info}
</td>";
}else{
	$anulada = '';
}

echo("<div class='title center'>Guía de Recepción Nº {$data['dq_guia_recepcion']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>
	Proveedor: (<b>{$data['dg_rut']}</b>) {$data['dg_razon']}
</caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td><b>{$data['df_fecha_emision']}</b></td>
	{$anulada}
</tr><tr>
	<td>Factura de Compra</td>
	<td>Interno: <b>{$data['dq_factura']}</b> folio: <b>{$data['dq_folio']}</b></td>
</tr><tr>
	<td>Tipo de guía</td>
	<td><b>{$data['dg_tipo']}</b></td>
</tr><tr>
	<td>Orden de compra</td>
	<td><b>{$data['dq_orden_compra']}</b></td>
</tr><tr>
	<td>Nota de Venta</td>
	<td><b>{$data['dq_nota_venta']}</b></td>
</tr><tr>
	<td>Observacion</td>
	<td>{$data['dg_observacion']}</td>
</tr></table>");

$detalle = $db->select("(SELECT * FROM tb_guia_recepcion_detalle WHERE dc_guia_recepcion={$_POST['id']}) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto
JOIN tb_bodega b ON b.dc_bodega = d.dc_bodega
LEFT JOIN tb_funcionario f ON f.dc_funcionario = d.dc_responsable
LEFT JOIN tb_ceco c ON c.dc_ceco = d.dc_ceco",
'p.dg_codigo,p.dg_producto,d.dq_precio,d.dq_cantidad,b.dg_bodega,f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno,c.dg_ceco');

echo("<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
<tr>
	<th width='60'>Código</th>
	<th>Producto</th>
	<th width='60'>Cantidad</th>
	<th width='100'>Precio</th>
	<th width='100'>Total</th>
	<th>Bodega</th>
	<th>Responsable</th>
</tr>
</thead>
<tbody>");

$total = 0;
foreach($detalle as $d){
	
	$total += $d['dq_precio']*$d['dq_cantidad'];
	
	$d['dq_total'] = moneda_local($d['dq_precio']*$d['dq_cantidad']);
	$d['dq_precio'] = moneda_local($d['dq_precio']);
	echo("<tr>
		<td align='left'>{$d['dg_codigo']}</td>
		<td align='left'>{$d['dg_producto']}</td>
		<td>{$d['dq_cantidad']}</td>
		<td align='right'>{$d['dq_precio']}</td>
		<td align='right'>{$d['dq_total']}</td>
		<td align='left'>{$d['dg_bodega']}</td>
		<td align='left'>{$d['dg_nombres']} {$d['dg_ap_paterno']} {$d['dg_ap_materno']}</td>
	</tr>");
	
	
}

$total = moneda_local($total);

echo("</tbody>
<tfoot>
<tr>
	<th colspan='4' align='right'>Total</th>
	<th align='right'>{$total}</th>
	<th colspan='2'></th>
</tr>
</tfoot></table>");

?>
<script type="text/javascript">
$('#print_version').unbind('click').click(function(){
	window.open("sites/ventas/orden_compra/ver_orden_compra.php?id=<?=$_POST['id'] ?>&v=0",'print_orden_compra','width=800;height=600');
});
$('#edit_orden_compra').unbind('click').click(function(){
	loadpage("sites/ventas/orden_compra/ed_orden_compra.php?id=<?=$_POST['id'] ?>");
});
$('#null_orden_compra').unbind('click').click(function(){
	loadOverlay("sites/ventas/guia_recepcion/null_guia_recepcion.php?id=<?=$_POST['id'] ?>");
});
</script>