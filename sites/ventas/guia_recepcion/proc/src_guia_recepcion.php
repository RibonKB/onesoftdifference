<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}


$_POST['gr_emision_desde'] = $db->sqlDate($_POST['gr_emision_desde']);
$_POST['gr_emision_hasta'] = $db->sqlDate($_POST['gr_emision_hasta']." 23:59");

$conditions = "gr.dc_empresa = {$empresa} AND (gr.df_fecha_emision BETWEEN {$_POST['gr_emision_desde']} AND {$_POST['gr_emision_hasta']})";

if($_POST['gr_nota_venta'] != ''){
	if(!is_numeric($_POST['gr_nota_venta'])){
		$error_man->showAviso('El formato de número de la nota de venta es incorrecto, corrijalo y vuelva a intentarlo');
		exit();
	}
	$nota_venta = $db->select('tb_nota_venta','dc_nota_venta',"dq_nota_venta={$_POST['gr_nota_venta']} AND dc_empresa = {$empresa}");
	if(!count($nota_venta)){
		$error_man->showWarning('La nota de venta especificada no existe');
		exit();
	}
	
	$conditions .= " AND gr.dc_nota_venta = {$nota_venta[0]['dc_nota_venta']}";
	
}

if($_POST['gr_orden_compra'] != ''){
	if(!is_numeric($_POST['gr_orden_compra'])){
		$error_man->showAviso('El formato de número de la orden de compra es incorrecto, corríjalo y vuelva a intentarlo');
		exit();
	}
	$orden_compra = $db->select('tb_orden_compra','dc_orden_compra',"dq_orden_compra={$_POST['gr_orden_compra']} AND dc_empresa = {$empresa}");
	if(!count($orden_compra)){
		$error_man->showWarning('La orden de compra especificada no existe');
		exit();
	}
	
	$conditions .= " AND gr.dc_orden_compra = {$orden_compra[0]['dc_orden_compra']}";
	
}

if($_POST['dq_factura']){
	if(!is_numeric($_POST['dq_factura'])){
		$error_man->showAviso('El formato de número de la factura de compra es incorrecto, corríjalo y vuelva a intentarlo');
		exit();
	}
	$factura = $db->select('tb_factura_compra','dc_factura',"(dq_factura = {$_POST['dq_factura']} OR dq_folio = {$_POST['dq_factura']}) AND dc_empresa = {$empresa}");
	var_dump($factura);
	if(!count($factura)){
		$error_man->showWarning('La factura de compra especificada no existe');
		exit();
	}
	
	$conditions .= " AND gr.dc_factura = {$factura[0]['dc_factura']}";
}

if($_POST['gr_numero_desde']){
	if($_POST['gr_numero_hasta']){
		$conditions .= " AND (dq_guia_recepcion BETWEEN {$_POST['gr_numero_desde']} AND {$_POST['gr_numero_hasta']})";
	}else{
		$conditions .= " AND dq_guia_recepcion = {$_POST['gr_numero_desde']}";
	}
}

if(isset($_POST['gr_tipo'])){
	$_POST['gr_tipo'] = implode(',',$_POST['gr_tipo']);
	$conditions .= " AND gr.dc_tipo_guia IN ({$_POST['gr_tipo']})";
}

if(isset($_POST['dc_proveedor'])){
	$_POST['dc_proveedor'] = implode(',',$_POST['dc_proveedor']);
	$conditions .= " AND oc.dc_proveedor IN ({$_POST['dc_proveedor']})";
}

$data = $db->select('tb_guia_recepcion gr
JOIN tb_orden_compra oc ON oc.dc_orden_compra = gr.dc_orden_compra',
'gr.dc_guia_recepcion,gr.dq_guia_recepcion',$conditions,array('order_by' => 'gr.df_fecha_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron guías de recepción con los criterios especificados");
	exit();
}

echo("<div id='show_guia_recepcion'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Guías de recepción<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Guía</th>
	</tr>
</thead>
<tbody>
");

foreach($data as $c){
echo("
<tr>
	<td align='left'>
		<a href='sites/ventas/guia_recepcion/proc/show_guia_recepcion.php?id={$c['dc_guia_recepcion']}' class='oc_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_guia_recepcion']}</a>
	</td>
</tr>");
}

echo("</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='delbtn' id='null_orden_compra'>Anular</button>
</div>");

?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".oc_load").click(function(e){
		e.preventDefault();
		$('#show_guia_recepcion').html("<img src='images/ajax-loader.gif' alt='' /> cargando guía de recepción ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_guia_recepcion');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>