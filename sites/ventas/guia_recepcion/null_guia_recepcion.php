<?php
define('MAIN',1);
include('../../../inc/global.php');

$data = $db->select('tb_guia_recepcion','dq_guia_recepcion',"dc_guia_recepcion = {$_POST['id']}");

if(!count($data)){
	$error_man->showWarning('La guia de recepción especificada es inválido');
	exit;
}

$dq_guia_recepcion = $data[0]['dq_guia_recepcion'];
unset($data);

echo('<div class="secc_bar">Anular Guía de Recepción</div><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/guia_recepcion/proc/null_guia_recepcion.php','null_guia_form');
$form->Header('Indique las razones por las que se anulará la guia de recepción');
$error_man->showAviso("Atención, está a punto de anular la guia de recepción N° <b>{$dq_guia_recepcion}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('id_guia_recepcion',$_POST['id']);
$form->End('Anular','delbtn');

?>
</div>