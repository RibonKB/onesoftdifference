<?php

/**
 * Description of CrearGuiaRecepcionFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 09-08-2013
 */
 
class CrearGuiaRecepcionFactory extends Factory{

    private $orden_compra;
    private $tipo_movimiento;
    private $detalles_pendientes;
    private $idOnDetail;

    private $dc_guia_recepcion;
    private $dq_guia_recepcion;
    private $dc_factura_compra;
    private $df_fecha_recepcion;

    private $dq_monto_pagado_factura = 0;
    private $dq_saldo_favor_factura = 0;
    private $dq_pago_usado = 0;

    private $comprobante_contable;
    private $dc_detalle_debe;
    private $dc_detalle_haber;
    
    private $asociaFacturaCompra = false;
    

    public function procesarCrearAction(){
        $request = self::getRequest();
        $this->orden_compra = $this->validaOrdenCompra($request->gr_oc_id);
        $this->tipo_movimiento = $this->getTipoGuiaRecepcion($request->gr_tipo);
        $this->detalles_pendientes = $this->getDetallesPendientesOCProcesar();
        $this->idOnDetail = $this->getIdsOnDetail();
        $this->validarPendientesProcesar();
        $this->validarPMPIngreso();
        $this->initMontosPagados();
        //Validación de los detalles de la guía de recepción en comparación con una factura de compra existente.
        $this->validaDetallesConFacturaCompra();

        $this->getConnection()->start_transaction();

          $this->dc_factura_compra = $this->relacionarFacturaCompra();
          $this->df_fecha_recepcion = $this->getFechaRecepcion();

          $this->insertarGuiaRecepcion();
          $this->insertarDetalleGuiaRecepcion();
          $this->actualizaPMP();
          $this->cargaStockBodega();
          $this->rebajaDetalles();
          $this->rebajaPagos();

          if($this->dq_pago_usado > 0):
            $this->insertarComprobanteContable();
          endif;

        $this->getConnection()->commit();

        echo $this->getView($this->getTemplateURL('confirmarCrear'), array(
            'error_man' => $this->getErrorMan(),
            'dq_guia_recepcion' => $this->dq_guia_recepcion,
            'dc_guia_recepcion' => $this->dc_guia_recepcion
        ));

        if($this->dq_pago_usado):
          echo $this->getContabilidadService()->getStandarCreationOutput();
        endif;

    }

    private function insertarGuiaRecepcion(){
      $r = self::getRequest();
      $db = $this->getConnection();

      $this->dq_guia_recepcion = $this->getDocCreateService()->getCorrelativo('tb_guia_recepcion', 'dq_guia_recepcion', 2, 'df_fecha_emision');

      $guia = $db->prepare($db->insert('tb_guia_recepcion', array(
          "dq_guia_recepcion" => $this->dq_guia_recepcion,
          "dc_tipo_guia" => '?',
          "dc_orden_compra" => '?',
          "dc_nota_venta" => '?',
          "dc_orden_servicio" => '?',
          "df_fecha_emision" => $db->getNow(),
          "dc_usuario_creacion" => $this->getUserData()->dc_usuario,
          "dc_empresa" => $this->getEmpresa(),
          "dg_observacion" => '?',
          'dc_factura' => '?',
          'df_recepcion' => '?'
      )));
      $guia->bindValue(1, $this->tipo_movimiento->dc_tipo, PDO::PARAM_INT);
      $guia->bindValue(2, $this->orden_compra->dc_orden_compra, PDO::PARAM_INT);
      $guia->bindValue(3, $r->gr_nv_id, PDO::PARAM_INT);
      $guia->bindValue(4, $r->gr_os_id, PDO::PARAM_INT);
      $guia->bindValue(5, $r->gr_observacion, PDO::PARAM_STR);
      $guia->bindValue(6, $this->dc_factura_compra, PDO::PARAM_INT);
      $guia->bindValue(7, $db->sqlDate2($this->df_fecha_recepcion), PDO::PARAM_STR);
      $db->stExec($guia);

      $this->dc_guia_recepcion = $db->lastInsertId();

    }

    private function insertarDetalleGuiaRecepcion(){
        $r = self::getRequest();
        $db = $this->getConnection();

        $detalle = $db->prepare($db->insert('tb_guia_recepcion_detalle', array(
            "dc_guia_recepcion" => $this->dc_guia_recepcion,
            "dc_detalle_orden_compra" => '?',
            "dc_detalle_nota_venta" => '?',
            "dc_detalle_orden_servicio" => '?',
            'dc_producto' => '?',
            "dq_cantidad" => '?',
            'dq_precio' => '?',
            "dc_bodega" => '?',
            "dc_responsable" => '?',
            "dc_ceco" => '?',
            "dc_detalle_factura_compra" => '?',
        )));
        $detalle->bindParam(1, $dc_detalle_orden_compra, PDO::PARAM_INT);
        $detalle->bindParam(2, $dc_detalle_nota_venta, PDO::PARAM_INT);
        $detalle->bindParam(3, $dc_detalle_orden_servicio, PDO::PARAM_INT);
        $detalle->bindParam(4, $dc_producto, PDO::PARAM_INT);
        $detalle->bindParam(5, $dq_cantidad, PDO::PARAM_INT);
        $detalle->bindParam(6, $dq_precio, PDO::PARAM_STR);
        $detalle->bindParam(7, $dc_bodega, PDO::PARAM_INT);
        $detalle->bindParam(8, $dc_responsable, PDO::PARAM_INT);
        $detalle->bindParam(9, $dc_ceco, PDO::PARAM_INT);
        $detalle->bindParam(10, $dc_detalle_factura_compra, PDO::PARAM_INT);

        foreach($this->idOnDetail as $i => $det):
            $dc_detalle_orden_compra = $det->dc_detalle_orden_compra;
            $dc_detalle_nota_venta = $det->dc_detalle_nota_venta;
            $dc_detalle_orden_servicio = $det->dc_detalle_orden_servicio;
            $dc_producto = $r->prod_id[$i];
            $dq_cantidad = $r->cant[$i];
            $dq_precio = floatval($r->precio[$i]);
            $dc_bodega = $det->dc_bodega;
            $dc_responsable = $r->responsable[$i];
            $dc_ceco = $det->dc_ceco;
            $detalleFactura = 0;
            if(isset($this->detalles_pendientes[$det->dc_detalle_orden_compra]->datosAsignacion)){
                $detalleFactura = $this->detalles_pendientes[$det->dc_detalle_orden_compra]->datosAsignacion->detalle_factura;
            }
            $dc_detalle_factura_compra = $detalleFactura;

            $db->stExec($detalle);
        endforeach;

    }

    private function actualizaPMP(){
        $db = $this->getConnection();
        $r = self::getRequest();

        foreach($this->idOnDetail as $i => $detalle):
          $data = $this->detalles_pendientes[$detalle->dc_detalle_orden_compra];
          if($data->dm_recalcula_precio == 1 and $data->dm_controla_inventario == 0):

              $update_producto = $db->prepare($db->update('tb_producto', array(
                  'dq_precio_compra' => '?'
              ), 'dc_producto = ?'));
              $update_producto->bindValue(1, $detalle->dq_pmp, PDO::PARAM_STR);
              $update_producto->bindValue(2, $data->dc_producto, PDO::PARAM_INT);
              $db->stExec($update_producto);

              if($detalle->dc_detalle_nota_venta):

                  $update_nota_venta = $db->prepare($db->update('tb_nota_venta_detalle', array(
                      'dq_pmp_compra' => '?',
                      'dq_precio_compra_final' => '?'
                  ), 'dc_nota_venta_detalle = ?'));
                  $update_nota_venta->bindValue(1, $detalle->dq_pmp, PDO::PARAM_STR);
                  $update_nota_venta->bindValue(2, floatval($r->precio[$i]), PDO::PARAM_STR);
                  $update_nota_venta->bindValue(3, $detalle->dc_detalle_nota_venta, PDO::PARAM_INT);
                  $db->stExec($update_nota_venta);

              elseif($detalle->dc_detalle_orden_servicio):

                  $update_orden_servicio = $db->prepare($db->update('tb_orden_servicio_factura_detalle', array(
                      'dq_pmp_compra' => '?',
                      'dq_precio_compra_final' => '?'
                  ),'dc_detalle = ?'));
                  $update_orden_servicio->bindValue(1, $detalle->dq_pmp, PDO::PARAM_STR);
                  $update_orden_servicio->bindValue(2, floatval($r->precio[$i]), PDO::PARAM_STR);
                  $update_orden_servicio->bindValue(3, $detalle->dc_detalle_orden_servicio, PDO::PARAM_INT);
                  $db->stExec($update_orden_servicio);

              endif;

          endif;
        endforeach;
    }

    private function cargaStockBodega(){

      $r = self::getRequest();
      $db = $this->getConnection();
      $logistica = $this->getLogisticaService();

      $movimiento = $db->prepare($db->insert('tb_movimiento_bodega',array(
            'dc_proveedor' => $this->orden_compra->dc_proveedor,
            'dc_tipo_movimiento' => $this->tipo_movimiento->dc_tipo_movimiento,
            'dc_bodega_entrada' => '?',
            'dq_monto' => '?',
            'dc_guia_recepcion' => $this->dc_guia_recepcion,
            'dc_orden_compra' => $this->orden_compra->dc_orden_compra,
            'dc_nota_venta' => '?',
            'dc_orden_servicio' => '?',
            'dc_producto' => '?',
            'dq_cantidad' => '?',
            'dc_empresa' => $this->getEmpresa(),
            'df_creacion' => $db->getNow(),
            'dc_usuario_creacion' => $this->getUserData()->dc_usuario
      )));
      $movimiento->bindParam(1, $dc_bodega, PDO::PARAM_INT);
      $movimiento->bindParam(2, $dq_monto, PDO::PARAM_STR);
      $movimiento->bindValue(3, $r->gr_nv_id, PDO::PARAM_INT);
      $movimiento->bindValue(4, $r->gr_os_id, PDO::PARAM_INT);
      $movimiento->bindParam(5, $dc_producto, PDO::PARAM_INT);
      $movimiento->bindParam(6, $dc_cantidad, PDO::PARAM_INT);

      foreach($this->idOnDetail as $i => $detalle):
          $data = $this->detalles_pendientes[$detalle->dc_detalle_orden_compra];

          if($data->dm_controla_inventario != 0):
            continue;
          endif;

          $dc_bodega = $detalle->dc_bodega;
          $dq_monto = $detalle->dq_pmp;
          $dc_producto = $data->dc_producto;
          $dc_cantidad = $r->cant[$i];

          if($detalle->dc_detalle_nota_venta):
            $logistica->cargarStockNV($data->dc_producto, $r->cant[$i], $detalle->dc_bodega);
          elseif($detalle->dc_detalle_orden_servicio):
            $logistica->cargarStockOS($data->dc_producto, $r->cant[$i], $detalle->dc_bodega);
          else:
            $logistica->cargarStockLibre($data->dc_producto, $r->cant[$i], $detalle->dc_bodega);
          endif;

          $db->stExec($movimiento);

      endforeach;
    }

    private function rebajaDetalles(){

      $db = $this->getConnection();
      $r = self::getRequest();

      $update_oc = $db->prepare($db->update('tb_orden_compra_detalle', array(
         'dc_recepcionada' => 'dc_recepcionada + ?'
      ),'dc_detalle = ?'));
      $update_oc->bindParam(1, $dc_cantidad, PDO::PARAM_INT);
      $update_oc->bindParam(2, $dc_detalle_orden_compra, PDO::PARAM_INT);

      $update_nv = $db->prepare($db->update('tb_nota_venta_detalle', array(
          'dc_recepcionada' => 'dc_recepcionada + ?'
      ), 'dc_nota_venta_detalle = ?'));
      $update_nv->bindParam(1, $dc_cantidad, PDO::PARAM_INT);
      $update_nv->bindParam(2, $dc_detalle_nota_venta, PDO::PARAM_INT);

      $update_os = $db->prepare($db->update('tb_orden_servicio_factura_detalle', array(
          'dc_recepcionada' => 'dc_recepcionada + ?'
      ), 'dc_detalle = ?'));
      $update_os->bindParam(1, $dc_cantidad, PDO::PARAM_INT);
      $update_os->bindParam(2, $dc_detalle_orden_servicio, PDO::PARAM_INT);

      foreach($this->idOnDetail as $i => $detalle):

        $dc_cantidad = intval($r->cant[$i]);
        $dc_detalle_orden_compra = $detalle->dc_detalle_orden_compra;
        $dc_detalle_nota_venta = $detalle->dc_detalle_nota_venta;
        $dc_detalle_orden_servicio = $detalle->dc_detalle_orden_servicio;

        $db->stExec($update_oc);

        if($dc_detalle_nota_venta):
          $db->stExec($update_nv);
        endif;

        if($dc_detalle_orden_servicio):
          $db->stExec($update_os);
        endif;

      endforeach;

    }

    private function rebajaPagos(){

      $db = $this->getConnection();

      if($this->dq_pago_usado > 0):
          $orden_compra = $db->prepare($db->update('tb_orden_compra', array(
              'dq_pago_usado' => 'dq_pago_usado+?'
          ), 'dc_orden_compra = ?'));
          $orden_compra->bindValue(1, $this->dq_pago_usado, PDO::PARAM_STR);
          $orden_compra->bindValue(2, $this->orden_compra->dc_orden_compra, PDO::PARAM_INT);
          $db->stExec($orden_compra);
      endif;

      if($this->dq_saldo_favor_factura > 0):
          $proveedor = $db->prepare($db->update('tb_proveedor',array(
              'dq_saldo_favor' => 'dq_saldo_favor+?'
          ),"dc_proveedor = ?"));
          $proveedor->bindValue(1, $this->dq_saldo_favor_factura, PDO::PARAM_STR);
          $proveedor->bindValue(2, $this->orden_compra->dc_proveedor, PDO::PARAM_INT);
          $db->stExec($proveedor);
      endif;
    }

    private function relacionarFacturaCompra(){
      $r = self::getRequest();

      if($r->gr_tipo_entrada == 'existente'):
        return $this->relacionaFacturaExistente();
      elseif($r->gr_tipo_entrada == 'factura'):
        return $this->insertarFacturaCompra();
      else:
        $this->insertarGuiaCompra();
        return 0;
      endif;
    }

    private function getFechaRecepcion(){
      $r = self::getRequest();

      if(in_array($r->gr_tipo_entrada, array('existente','factura'))):
        return $r->gr_fc_emision;
      else:
        return $r->gr_fecha_recepcion;
      endif;
    }

    private function relacionaFacturaExistente(){
      $r = self::getRequest();
      $db = $this->getConnection();

      $factura = $db->prepare($db->update('tb_factura_compra', array(
          'dq_monto_pagado' => 'dq_monto_pagado+?',
          'dq_saldo_favor' => 'dq_saldo_favor+?'
      ), 'dc_factura = ?'));
      $factura->bindValue(1, $this->dq_monto_pagado_factura, PDO::PARAM_STR);
      $factura->bindValue(2, $this->dq_saldo_favor_factura, PDO::PARAM_STR);
      $factura->bindValue(3, $r->dc_factura_old, PDO::PARAM_INT);
      $db->stExec($factura);
      
      $this->prepararActualizacionDetalleFacturaCompra();

      return $r->dc_factura_old;
    }

    private function insertarFacturaCompra(){
      $r = self::getRequest();
      $dq_exento = isset($r->gr_exento)?floatval(str_replace(',','',$r->gr_exento)):0;
      $db = $this->getConnection();
      $decimales = $this->getParametrosEmpresa()->dn_decimales_local;

      $factura = $db->prepare($db->insert('tb_factura_compra', array(
          'dc_nota_venta' => $this->orden_compra->dc_nota_venta,
          'dc_orden_servicio' => $this->orden_compra->dc_orden_servicio,
          'dc_proveedor' => $this->orden_compra->dc_proveedor,
          'dc_medio_pago' => '?',
          'dc_tipo' => '?',
          'dq_factura' => $this->getDocCreateService()->getCorrelativo('tb_factura_compra', 'dq_factura', 4, 'df_creacion'),
          'dq_folio' => '?',
          'dg_referencia' => '?',
          'dg_comentario' => '?',
          'df_emision' => '?',
          'df_creacion' => $db->getNow(),
          'df_vencimiento' => '?',
          'df_libro_compra' => '?',
          'dc_orden_compra' => $this->orden_compra->dc_orden_compra,
          'dq_exento' => $dq_exento,
          'dq_descuento' => '?',
          'dq_neto' => '?',
          'dq_iva' => '?',
          'dq_total' => '?',
          'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
          'dq_monto_pagado' => $this->dq_monto_pagado_factura,
          'dq_saldo_favor' => $this->dq_saldo_favor_factura,
          'dc_empresa' => $this->getEmpresa()
      )));
      $factura->bindValue(1, $r->gr_fc_medio_pago, PDO::PARAM_INT);
      $factura->bindValue(2, $r->gr_fc_tipo_factura, PDO::PARAM_INT);
      $factura->bindValue(3, $r->gr_factura_compra, PDO::PARAM_INT);
      $factura->bindValue(4, $r->dg_referencia, PDO::PARAM_STR);
      $factura->bindValue(5, $r->gr_observacion, PDO::PARAM_STR);
      $factura->bindValue(6, $db->sqlDate2($r->gr_fc_emision), PDO::PARAM_STR);
      $factura->bindValue(7, $db->sqlDate2($r->gr_fc_vencimiento), PDO::PARAM_STR);
      $factura->bindValue(8, $db->sqlDate2($r->gr_fc_fecha_contable), PDO::PARAM_STR);
      $factura->bindValue(9, round(floatval(str_replace(',','',$r->gr_descuento)),$decimales), PDO::PARAM_STR);
      $factura->bindValue(10, round(floatval(str_replace(',','',$r->gr_neto)),$decimales), PDO::PARAM_STR);
      $factura->bindValue(11, round(floatval(str_replace(',','',$r->gr_iva)),$decimales), PDO::PARAM_STR);
      $factura->bindValue(12, round(floatval(str_replace(',','',$r->gr_total)),$decimales), PDO::PARAM_STR);
      $db->stExec($factura);

      $dc_factura = $db->lastInsertId();

      $this->insertarDetalleFacturaCompra($dc_factura);

      return $dc_factura;

    }

    private function insertarDetalleFacturaCompra($dc_factura){
        $r = self::getRequest();
        $db = $this->getConnection();

        $detalle = $db->prepare($db->insert('tb_factura_compra_detalle', array(
            'dc_factura' => $dc_factura,
			'dc_producto' => '?',
			'dc_detalle_nota_venta' => '?',
			'dc_detalle_orden_servicio' => '?',
			'dc_detalle_orden_compra' => '?',
			'dc_cantidad' => '?',
			'dq_precio' => '?',
			'dq_total' => '?',
			'dc_ceco' => '?',
            'dg_descripcion' => '?'
        )));
        $detalle->bindParam(1, $dc_producto, PDO::PARAM_INT);
        $detalle->bindParam(2, $dc_detalle_nota_venta, PDO::PARAM_INT);
        $detalle->bindParam(3, $dc_detalle_orden_servicio, PDO::PARAM_INT);
        $detalle->bindParam(4, $dc_detalle_orden_compra, PDO::PARAM_INT);
        $detalle->bindParam(5, $dc_cantidad, PDO::PARAM_INT);
        $detalle->bindParam(6, $dq_precio, PDO::PARAM_STR);
        $detalle->bindParam(7, $dq_total, PDO::PARAM_STR);
        $detalle->bindParam(8, $dc_ceco, PDO::PARAM_INT);
        $detalle->bindParam(9, $dg_descripcion, PDO::PARAM_STR);

        foreach($this->idOnDetail as $i => $d):
            $data = $this->detalles_pendientes[$d->dc_detalle_orden_compra];

            $dc_producto = $data->dc_producto;
            $dc_detalle_nota_venta = $d->dc_detalle_nota_venta;
            $dc_detalle_orden_servicio = $d->dc_detalle_orden_servicio;
            $dc_detalle_orden_compra = $d->dc_detalle_orden_compra;
            $dc_cantidad = intval($r->cant[$i]);
            $dq_precio = floatval($r->precio[$i]);
            $dq_total = $dc_cantidad*$dq_precio;
            $dc_ceco = $d->dc_ceco;
            $dg_descripcion = $data->dg_descripcion;

            $db->stExec($detalle);
            
            $this->detalles_pendientes[$d->dc_detalle_orden_compra]->datosAsignacion = new stdClass();
            $this->detalles_pendientes[$d->dc_detalle_orden_compra]->datosAsignacion->cantidad_recepcion = $r->cant[$i];
            $this->detalles_pendientes[$d->dc_detalle_orden_compra]->datosAsignacion->detalle_factura = $db->lastInsertId();

        endforeach;

    }

    private function insertarGuiaCompra(){
      $r = self::getRequest();
      $dq_exento = isset($r->gr_exento)?floatval(str_replace(',','',$r->gr_exento)):0;
      $db = $this->getConnection();
      $decimales = $this->getParametrosEmpresa()->dn_decimales_local;

      $guiaCompra = $db->prepare($db->insert('tb_guia_compra', array(
          'dc_nota_venta' => $this->orden_compra->dc_nota_venta,
          'dc_orden_servicio' => $this->orden_compra->dc_orden_servicio,
          'dc_proveedor' => $this->orden_compra->dc_proveedor,
          'dc_medio_pago' => '?',
          'dc_tipo' => '?',
          'dq_guia_compra' => $this->getDocCreateService()->getCorrelativo('tb_factura_compra', 'dq_factura', 4, 'df_creacion'),
          'dq_folio' => '?',
          'dg_referencia' => '?',
          'dg_comentario' => '?',
          'df_emision' => '?',
          'df_creacion' => $db->getNow(),
          'df_vencimiento' => '?',
          'df_libro_compra' => '?',
          'dc_orden_compra' => $this->orden_compra->dc_orden_compra,
          'dq_exento' => $dq_exento,
          'dq_descuento' => '?',
          'dq_neto' => '?',
          'dq_iva' => '?',
          'dq_total' => '?',
          'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
          'dc_empresa' => $this->getEmpresa()
      )));
      $guiaCompra->bindValue(1, $r->gr_fc_medio_pago, PDO::PARAM_INT);
      $guiaCompra->bindValue(2, $r->gr_fc_tipo_factura, PDO::PARAM_INT);
      $guiaCompra->bindValue(3, $r->gr_factura_compra, PDO::PARAM_INT);
      $guiaCompra->bindValue(4, $r->dg_referencia, PDO::PARAM_STR);
      $guiaCompra->bindValue(5, $r->gr_observacion, PDO::PARAM_STR);
      $guiaCompra->bindValue(6, $db->sqlDate2($r->gr_fc_emision), PDO::PARAM_STR);
      $guiaCompra->bindValue(7, $db->sqlDate2($r->gr_fc_vencimiento), PDO::PARAM_STR);
      $guiaCompra->bindValue(8, $db->sqlDate2($r->gr_fc_fecha_contable), PDO::PARAM_STR);
      $guiaCompra->bindValue(9, round(floatval(str_replace(',','',$r->gr_descuento)),$decimales), PDO::PARAM_STR);
      $guiaCompra->bindValue(10, round(floatval(str_replace(',','',$r->gr_neto)),$decimales), PDO::PARAM_STR);
      $guiaCompra->bindValue(11, round(floatval(str_replace(',','',$r->gr_iva)),$decimales), PDO::PARAM_STR);
      $guiaCompra->bindValue(12, round(floatval(str_replace(',','',$r->gr_total)),$decimales), PDO::PARAM_STR);
      $db->stExec($guiaCompra);

      $dc_guia_compra = $db->lastInsertId();

      $this->insertarDetalleGuiaCompra($dc_guia_compra);
    }

    private function insertarDetalleGuiaCompra($dc_guia_compra){
      $r = self::getRequest();
        $db = $this->getConnection();

        $detalle = $db->prepare($db->insert('tb_guia_compra_detalle', array(
            'dc_guia_compra' => $dc_guia_compra,
			'dc_producto' => '?',
			'dc_detalle_nota_venta' => '?',
			'dc_detalle_orden_servicio' => '?',
			'dc_detalle_orden_compra' => '?',
			'dc_cantidad' => '?',
			'dq_precio' => '?',
			'dq_total' => '?',
			'dc_ceco' => '?',
            'dg_descripcion' => '?'
        )));
        $detalle->bindParam(1, $dc_producto, PDO::PARAM_INT);
        $detalle->bindParam(2, $dc_detalle_nota_venta, PDO::PARAM_INT);
        $detalle->bindParam(3, $dc_detalle_orden_servicio, PDO::PARAM_INT);
        $detalle->bindParam(4, $dc_detalle_orden_compra, PDO::PARAM_INT);
        $detalle->bindParam(5, $dc_cantidad, PDO::PARAM_INT);
        $detalle->bindParam(6, $dq_precio, PDO::PARAM_STR);
        $detalle->bindParam(7, $dq_total, PDO::PARAM_STR);
        $detalle->bindParam(8, $dc_ceco, PDO::PARAM_INT);
        $detalle->bindParam(9, $dg_descripcion, PDO::PARAM_STR);

        foreach($this->idOnDetail as $i => $d):
            $data = $this->detalles_pendientes[$d->dc_detalle_orden_compra];

            $dc_producto = $data->dc_producto;
            $dc_detalle_nota_venta = $d->dc_detalle_nota_venta;
            $dc_detalle_orden_servicio = $d->dc_detalle_orden_servicio;
            $dc_detalle_orden_compra = $d->dc_detalle_orden_compra;
            $dc_cantidad = intval($r->cant[$i]);
            $dq_precio = floatval($r->precio[$i]);
            $dq_total = $dc_cantidad*$dq_precio;
            $dc_ceco = $d->dc_ceco;
            $dg_descripcion = $data->dg_descripcion;

            $db->stExec($detalle);

        endforeach;
    }

    private function getFechaLibroCompra(){
        $r = self::getRequest();
      if($r->gr_tipo_entrada == 'existente'){
            $df_fecha_contable=$this->getFecha();
            return $df_fecha_contable;
        }else{
           return $r->gr_fc_fecha_contable;
        }
    }

    private function getFecha(){
        $r=self::getRequest();
        $fecha=$this->coneccion('select', 'tb_factura_compra', 'df_libro_compra',"dc_factura={$r->dc_factura_old}");
        $fecha=$fecha->fetch(PDO::FETCH_OBJ);
        return $fecha->df_libro_compra;
        }

    private function insertarComprobanteContable(){
      $comprobante = $this->getContabilidadService();

      $db = $this->getConnection();
      $r = self::getRequest();

      $proveedor = $db->getRowById('tb_proveedor', $this->orden_compra->dc_proveedor, 'dc_proveedor');

      try {
        $comprobante->prepareComprobante($this->getParametrosEmpresa()->dc_tipo_movimiento_pago_anticipado, $db->sqlDate2($this->getFechaLibroCompra()));
      } catch (Exception $e) {
        $this->getErrorMan()->showWarning($e->getMessage());
        exit;
      }

      $comprobante->setComprobanteParam('dg_glosa', "Uso saldo por pago orden de compra: {$this->orden_compra->dq_orden_compra} Proveedor: {$proveedor->dg_razon}");
      $comprobante->setComprobanteParam('dq_saldo', $this->dq_pago_usado);
      $comprobante->ComprobantePersist();

      $this->comprobante_contable = $comprobante->getComprobanteInfo();

      $this->insertarDetallesFinancierosContables();
      $this->insertarDetallesAnaliticosContables();

    }

    private function insertarDetallesFinancierosContables(){
        $this->getContabilidadService()->prepareDetalleFinanciero();
        $this->dc_detalle_debe = $this->insertarDFCProveedor();
        $this->dc_detalle_haber = $this->insertarDFCAnticipo();
    }

        private function insertarDFCProveedor(){
            $service = $this->getContabilidadService();
            $proveedor = $this->getConnection()->getRowById('tb_proveedor', $this->orden_compra->dc_proveedor, 'dc_proveedor');

            $service->setParametroFinanciero('dc_cuenta_contable', $proveedor->dc_cuenta_contable);
            $service->setParametroFinanciero('dq_debe', $this->dq_pago_usado);
            $service->setParametroFinanciero('dq_haber', 0);
            $service->setParametroFinanciero('dg_glosa', "Pago Anticipado proveedor: {$proveedor->dg_razon}");

            return $service->DetalleFinancieroPersist();
        }

        private function insertarDFCAnticipo(){
            $service = $this->getContabilidadService();
            $proveedor = $this->getConnection()->getRowById('tb_proveedor', $this->orden_compra->dc_proveedor, 'dc_proveedor');

            $service->setParametroFinanciero('dc_cuenta_contable', $proveedor->dc_cuenta_anticipo);
            $service->setParametroFinanciero('dq_haber', $this->dq_pago_usado);
            $service->setParametroFinanciero('dq_debe', 0);
            $service->setParametroFinanciero('dg_glosa', "Pago Anticipado proveedor: {$proveedor->dg_razon}");

            return $service->DetalleFinancieroPersist();
        }

    private function insertarDetallesAnaliticosContables(){
        $service = $this->getContabilidadService();

        $service->prepareDetalleAnalitico();
        $service->setParametroAnalitico('dc_proveedor', $this->orden_compra->dc_proveedor);
        $service->setDependencias('dc_proveedor');

        if($this->orden_compra->dc_nota_venta){
          $nota_venta = $this->getConnection()->getRowById('tb_nota_venta', $this->orden_compra->dc_nota_venta, 'dc_nota_venta');
          $dc_cliente = $nota_venta->dc_cliente;

          $service->setParametroAnalitico('dc_nota_venta', $nota_venta->dc_nota_venta);
          $service->setDependencias('dc_nota_venta');
          $service->setDependencias('dc_cliente');
        }elseif($this->orden_compra->dc_orden_servicio){
          $orden_servicio = $this->getConnection()->getRowById('tb_orden_servicio', $this->orden_compra->dc_orden_servicio, 'dc_orden_servicio');
          $dc_cliente = $orden_servicio->dc_cliente;

          $service->setParametroAnalitico('dc_orden_servicio', $orden_servicio->dc_orden_servicio);
          $service->setDependencias('dc_orden_servicio');
          $service->setDependencias('dc_nota_venta');
          $service->setDependencias('dc_cliente');
        }

        $service->setParametroAnalitico('dc_factura_compra', $this->dc_factura_compra);
        $service->setParametroAnalitico('dc_orden_compra', $this->orden_compra->dc_orden_compra);
        $service->setParametroAnalitico('dc_guia_recepcion', $this->dc_guia_recepcion);
        $service->setParametroAnalitico('dg_glosa', 'Pago Anticipado proveedor');

        $this->insertarDACProveedor();
        $this->insertarDACAnticipo();

    }

        private function insertarDACProveedor(){
            $service = $this->getContabilidadService();
            $proveedor = $this->getConnection()->getRowById('tb_proveedor', $this->orden_compra->dc_proveedor, 'dc_proveedor');

            $service->setParametroAnalitico('dc_cuenta_contable', $proveedor->dc_cuenta_contable);
            $service->setParametroAnalitico('dq_debe', $this->dq_pago_usado);
            $service->setParametroAnalitico('dq_haber', 0);
            $service->setParametroAnalitico('dc_detalle_financiero', $this->dc_detalle_debe);

            $service->DetalleAnaliticoPersist();
        }

        private function insertarDACAnticipo(){
            $service = $this->getContabilidadService();
            $proveedor = $this->getConnection()->getRowById('tb_proveedor', $this->orden_compra->dc_proveedor, 'dc_proveedor');

            $service->setParametroAnalitico('dc_cuenta_contable', $proveedor->dc_cuenta_anticipo);
            $service->setParametroAnalitico('dq_debe', 0);
            $service->setParametroAnalitico('dq_haber', $this->dq_pago_usado);
            $service->setParametroAnalitico('dc_detalle_financiero', $this->dc_detalle_haber);

            $service->DetalleAnaliticoPersist();
        }

    private function initMontosPagados(){

        $decimales = $this->getParametrosEmpresa()->dn_decimales_local;
        $r = self::getRequest();

        $total_pagar = round(
                         floatval($this->orden_compra->dq_monto_pagado)
                        -floatval($this->orden_compra->dq_pago_usado)
                        -floatval($this->orden_compra->dq_monto_liberado)
                      ,$decimales);

        if($total_pagar == 0):
          return;
        endif;

        $completa = $this->obtenerEstadoCompletaOC();
        $totalFactura = round(floatval(str_replace(',','',$r->gr_total)),$decimales);

        if($completa):

          if($totalFactura <= $total_pagar){
              $this->dq_monto_pagado_factura = $totalFactura;
              $this->dq_saldo_favor_factura = $total_pagar-$totalFactura;

              //$db->update('tb_proveedor',array('dq_saldo_favor' => 'dq_saldo_favor+'.$dq_saldo_favor_factura),"dc_proveedor = ".$orden_compra['dc_proveedor']);
          }else{
              $this->dq_monto_pagado_factura = $total_pagar;
          }

        else:

          if($totalFactura >= $total_pagar){
              $this->dq_monto_pagado_factura = $total_pagar;
          }else{
              $this->dq_monto_pagado_factura = $totalFactura;
          }

        endif;

        $this->dq_pago_usado = $this->dq_monto_pagado_factura;

    }

    private function validaOrdenCompra($dc_orden_compra){
        $orden_compra = $this->getOrdenCompra($dc_orden_compra);

        if($orden_compra === false):
            $this->getErrorMan()->showWarning('La orden de compra que se intenta procesar es inválida');
            exit;
        endif;

        if($orden_compra->dq_monto_pagado >= $orden_compra->dq_pago_usado):
            if($this->getParametrosEmpresa()->dc_tipo_movimiento_pago_anticipado == 0){
              $this->getErrorMan()->showWarning('No puede continuar con la recepción debido a que no está configurado una cuenta para anticipos.');
              exit;
            }
        endif;

        return $orden_compra;
    }

    private function getOrdenCompra($dc_orden_compra){
        return $this->getConnection()->getRowById('tb_orden_compra', $dc_orden_compra, 'dc_orden_compra');
    }

    private function getTipoGuiaRecepcion($gr_tipo){
        $gr_tipo = explode('|',$gr_tipo);
        return $this->getConnection()->getRowById('tb_tipo_guia_recepcion', $gr_tipo[0], 'dc_tipo');
    }

    private function getDetallesPendientesOCProcesar(){
        $detalles = $this->getDetallesPendientesOC();

        $detalle_oc = array();
        foreach($detalles as $d){
            $detalle_oc[$d->dc_detalle] = $d;
        }

        return $detalle_oc;

    }

    private function getDetallesPendientesOC(){
      $db = $this->getConnection();
      $detalles	= 	$db->prepare($db->select('tb_orden_compra_detalle d
						JOIN tb_producto p ON p.dc_producto = d.dc_producto
						JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
					'd.dc_detalle,d.dc_producto,d.dq_precio,d.dc_proveedor,d.dq_cantidad,d.dc_recepcionada,
					 p.dq_precio_compra, t.dm_recalcula_precio, t.dm_controla_inventario, d.dg_descripcion',
					'dc_orden_compra = ? AND dq_cantidad > dc_recepcionada AND t.dm_controla_inventario = 0'));
      $detalles->bindValue(1, $this->orden_compra->dc_orden_compra, PDO::PARAM_INT);
      $db->stExec($detalles);

      return $detalles->fetchAll(PDO::FETCH_OBJ);

    }

    private function getIdsOnDetail(){
      $r = self::getRequest();
      $data = array();
      foreach($r->prod as $i => $v){
        $v = explode('|',$v);
        $data[$i] = new stdClass();
        $data[$i]->dc_detalle_orden_compra = $v[0];
        $data[$i]->dc_ceco = $v[2];

        if($r->bodega[$i] == 0){
          $data[$i]->dc_bodega = $this->tipo_movimiento->dc_bodega;
        }else{
          $data[$i]->dc_bodega = $r->bodega[$i];
        }

        if($r->gr_nv_id != 0){
            $data[$i]->dc_detalle_nota_venta = $v[1];
            $data[$i]->dc_detalle_orden_servicio = 0;
        }else if($_POST['gr_os_id'] != 0){
            $data[$i]->dc_detalle_nota_venta = 0;
            $data[$i]->dc_detalle_orden_servicio = $v[1];
        }else{
            $data[$i]->dc_detalle_nota_venta = 0;
            $data[$i]->dc_detalle_orden_servicio = 0;
        }
      }

      return $data;
    }

    private function validarPendientesProcesar(){
        $r = self::getRequest();

        foreach($this->idOnDetail as $i => $detalle):
            if(!isset($this->detalles_pendientes[$detalle->dc_detalle_orden_compra])):
              $this->getErrorMan()->showWarning('No se puede procesar la guía de recepción debido a que uno o más detalles de la orden de compra seleccionados ya fue recepcionado');
              exit;
            endif;

            $p = $this->detalles_pendientes[$detalle->dc_detalle_orden_compra];

            if($r->cant[$i] > ($p->dq_cantidad-$p->dc_recepcionada)):
              $this->getErrorMan()->showWarning('No se puede procesar la guía de recepción debido a que uno o más detalles en esta supera el limite a recepcionar de la orden de compra');
              exit;
            endif;

        endforeach;
    }

    private function validarPMPIngreso(){
      $r = self::getRequest();
        foreach($this->idOnDetail as $i => $detalle):
          $data = $this->detalles_pendientes[$detalle->dc_detalle_orden_compra];
          if($data->dm_controla_inventario == 0):
              $pmp = $this->getLogisticaService()->getNewPMP($data->dc_producto, $detalle->dc_bodega, $r->cant[$i], $r->precio[$i]);

              if($pmp === false):
                $this->getErrorMan()->showWarning('No se puede procesar la solicitud de compra debido a que el precio indicado para el producto excede al precio anterior en un gran porcentaje');
                exit;
              endif;

              $this->idOnDetail[$i]->dq_pmp = $pmp;

          endif;
        endforeach;
    }

    private function obtenerEstadoCompletaOC(){
      $r = self::getRequest();
      $completas = array();

      foreach($this->idOnDetail as $i => $detalle):
        $data = $this->detalles_pendientes[$detalle->dc_detalle_orden_compra];
        $dq_cantidad = intval($r->cant[$i]);
        $dq_pendiente = intval($data->dq_cantidad)-intval($data->dc_recepcionada);

        if($dq_pendiente > $dq_cantidad):
          return false;
        endif;

        $completas[] = $detalle->dc_detalle_orden_compra;

      endforeach;

      if(count($completas) == count($this->detalles_pendientes)):
        return true;
      endif;

      return false;
    }

    /**
     * @return DocumentCreationService
     */
    private function getDocCreateService(){
      return $this->getService('DocumentCreation');
    }

    /**
     * @return LogisticaStuffService
     */
    private function getLogisticaService(){
      return $this->getService('LogisticaStuff');
    }

    /**
     * @return ComprobanteContableService
     */
    private function getContabilidadService(){
      return $this->getService('ComprobanteContable');
    }
    
    /*
     * Creación de método que verificará si los detalles de la orden de compra son compatibles
     * con la Factura de compra Existente. Esto lo realizará comparando los códigos y las cantidades de los
     * detalles de la orden de compra
     */
    private function validaDetallesConFacturaCompra() {
        $facturaExistente = $this->getFacturaCompraExistente();
        if(!$facturaExistente){
            return;
        }
        $facturaExistente->detalles = $this->getDetallesFacturaExistente($facturaExistente);
        $detallesOrdenCompra = $this->getDetallesOrdenCompraRecepcionar();
        $this->compararDetalles($facturaExistente, $detallesOrdenCompra);
    }
    
    private function getFacturaCompraExistente() {
        $r = self::getRequest();
        if($r->gr_tipo_entrada != 'existente') {
            return false;
        }
        if(!isset($r->dc_factura_old)) {
            $this->getErrorMan()->showWarning('No se ha ingresado una factura existente.');
            exit;
        }
        $db = $this->getConnection();
        return $db->getRowById('tb_factura_compra', $r->dc_factura_old, 'dc_factura');
    }
    
    private function getDetallesFacturaExistente($facturaExistente) {
        if(empty($facturaExistente)){
            return false;
        }
        $db = $this->getConnection();
        $table = 'tb_factura_compra_detalle';
        $fields = 'dc_detalle, dc_factura, dc_producto, dc_cantidad, dc_recepcionada';
        $conditions = 'dc_factura = :factura';
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':factura', $facturaExistente->dc_factura, PDO::PARAM_INT);
        $db->stExec($st);
        $arr = array();
        while ($v = $st->fetch(PDO::FETCH_OBJ)) {
            $arr[$v->dc_detalle] = $v;
        }
        return $arr;
    }
    
    private function getDetallesOrdenCompraRecepcionar() {
        $r = $this->getRequest();
        $arr = array();
        foreach($this->idOnDetail as $c => $v){
            $obj = new stdClass();
            foreach($v as $k => $d){
                $obj->$k = $d;
            }
            $obj->cantidad_recepcion = $r->cant[$c];
            $arr[] = $obj;
        }
        unset($c, $v, $k, $d);
        $detalles = array();
        foreach ($arr as $c) {
            $detalles[$c->dc_detalle_orden_compra] = $this->detalles_pendientes[$c->dc_detalle_orden_compra];
            $detalles[$c->dc_detalle_orden_compra]->datosAsignacion = $c;
        }
        return $detalles;
    }
    
    private function compararDetalles(&$facturaCompra, &$detallesOrdenCompra) {
        foreach($detallesOrdenCompra as &$v){
            $this->recorreDetalleFacturaCompra($v, $facturaCompra);
            if(!isset($v->datosAsignacion->detalle_factura)){
                $producto = substr($v->dg_descripcion, 30);
                $this->getErrorMan()->showWarning("No se ha encontrado en la factura de compra cantidad suficiente para recepcionar el detalle {$v->dg_descripcion}");
                exit;
            }
        }
        
    }
    
    private function recorreDetalleFacturaCompra(&$detalleODC, &$facturaCompra) {
        foreach($facturaCompra->detalles as &$v){
            if($detalleODC->dc_producto != $v->dc_producto){
                continue;
            }
            $cantidadDisponibleFacturaCompra = $v->dc_cantidad - $v->dc_recepcionada;
            if($cantidadDisponibleFacturaCompra < $detalleODC->datosAsignacion->cantidad_recepcion){
                continue;
            }
            $detalleODC->datosAsignacion->detalle_factura = $v->dc_detalle;
        }
    }
    
    private function prepararActualizacionDetalleFacturaCompra() {
        foreach($this->idOnDetail as &$v){
            $this->updateDetalleFacturaCompra($this->detalles_pendientes[$v->dc_detalle_orden_compra]);
        }
    }
    
    private function updateDetalleFacturaCompra($detalle){
        $db = $this->getConnection();
        $table = 'tb_factura_compra_detalle';
        $fields = array('dc_recepcionada' => 'dc_recepcionada + :recepcionada',);
        $conditions = 'dc_detalle = :detalle';
        $st = $db->prepare($db->update($table, $fields, $conditions));
        $st->bindValue(':recepcionada', $detalle->datosAsignacion->cantidad_recepcion, PDO::PARAM_INT);
        $st->bindValue(':detalle', $detalle->datosAsignacion->detalle_factura, PDO::PARAM_INT);
        $db->stExec($st);
    }
    
}
