<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/Factory.class.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Crear Guía de recepción</div>
<div id='main_cont'><div class='panes'>");

require_once('../../../inc/form-class.php');
$form = new Form($empresa);

$form->Start(Factory::buildUrl('CrearGuiaRecepcion', 'ventas', 'procesarCrear', 'guia_recepcion'),'cr_guia_recepcion','ventas_form');
$form->Header("<b>Indique una orden de compra para recepcionar</b><br />Los campos marcados con [*] son obligatorios");
echo('<hr />');

$form->Section();
	$form->Text('Orden de compra','gr_orden_compra',1,20);
	$form->Listado('Tipo de guía de recepción','gr_tipo','tb_tipo_guia_recepcion',array('CONCAT_WS("|",dc_tipo,dc_tipo_movimiento,dc_bodega)','dg_tipo'),1);
$form->EndSection();

$form->Section();
	$form->Textarea('Observacion','gr_observacion');
$form->EndSection();

$form->Section();
	$form->Radiobox('Tipo de documento de entrada','gr_tipo_entrada',array(
		'factura' => 'Factura de compra',
		'existente' => 'Factura Existente',
		'guia' => 'Guía de compra'
	),'factura');
$form->EndSection();

echo('<br class="clear" /><br /><div id="oc_data_container"></div>');

$form->Group('id="new_factura"');
	$form->Header("Datos de factura de compra anexo a la recepción");
	$form->Section();
		$form->Text('Número de factura de compra','gr_factura_compra',1);
		$form->Date('Fecha de emisión','gr_fc_emision',1);
		$form->Date('Fecha de vencimiento','gr_fc_vencimiento',1);
		$form->Date('Fecha Contable','gr_fc_fecha_contable',1);
	$form->EndSection();

	$form->Section();
		$form->Listado('Medio de pago','gr_fc_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'));
		$form->Listado(
				'Tipo de factura',
				'gr_fc_tipo_factura',
				'tb_tipo_factura_compra',
				array('dc_tipo','dg_tipo'),
				1,0,
				"dc_empresa = {$empresa} AND dm_activo = 1 AND dm_factura_manual = 0");
		$form->Text('Referencia','dg_referencia');
	$form->EndSection();

	$form->Section();
		$form->Radiobox('Factura electrónica','gr_fc_electronica',array(1=>'SI',0=>'NO'),1);
	$form->EndSection();
$form->Group('id="old_factura" class="hidden"');
	$form->Header('Datos de Factura existente');
	$form->Section();
		$form->Text('Factura de Compra '.Form::MANDATORY_ICON,'dq_factura_existente',0);
		$form->Hidden('dc_factura_old',0);
	$form->EndSection();

	//Agregación de fecha de recepción en bodega
	$form->Section();
		$form->Date('Fecha de recepción de productos '.Form::MANDATORY_ICON,'gr_fecha_recepcion',0);
	$form->EndSection();

$form->Group();

echo('<br /><div id="prods">
<table width="100%" class="tab">
<caption>Detalle a recepcionar</caption>
<thead>
	<tr>
		<th width="25"><input type="checkbox" id="all_products" disabled="disabled" title="seleccionar todos"></th>
		<th width="120">Código</th>
		<th>Descripción</th>
		<th width="80">Cantidad [*]</th>
		<th width="90">Precio</th>
		<th width="100">Precio Total</th>
		<th width="160">Responsable</th>
		<th width="160">Bodega</th>
	</tr>
</thead>
<tfoot>

	<tr>
		<th colspan="5" align="right">Exento</th>
		<th>
			<input type="text" class="inputtext gr_txt_right" id="gr_exento" name="gr_exento" disabled="disabled" />
			<input type="hidden" class="max_val" value="0" />
		</th>
		<th colspan="2" align="left">
			<label><input type="checkbox" id="activate_exent" /> Habilitar exento</label>
		</th>
	</tr>

	<tr>
		<th colspan="5" align="right">Descuento</th>
		<th>
			<input type="text" class="inputtext gr_txt_right" id="gr_descuento" name="gr_descuento" value="0" />
		</th>
		<th colspan="2">&nbsp;</th>
	</tr>

	<tr>
		<th colspan="5" align="right">Neto</th>
		<th>
			<input type="text" class="inputtext gr_txt_right" id="gr_neto" name="gr_neto" />
			<input type="hidden" class="max_val" value="0" />
		</th>
		<th colspan="2"></th>
	</tr>

	<tr>
		<th colspan="5" align="right">IVA</th>
		<th>
			<input type="text" class="inputtext gr_txt_right" id="gr_iva" name="gr_iva" />
		</th>
		<th colspan="2"></th>
	</tr>

	<tr>
		<th colspan="5" align="right">Total</th>
		<th>
			<input type="text" class="inputtext gr_txt_right" id="gr_total" name="gr_total" />
		</th>
		<th colspan="2"></th>
	</tr>

</tfoot>
<tbody id="prod_list">
	<tr>
		<td colspan="8" align="center">
			<div class="info">Indique una orden de compra para cargar el detalle</div>
		</td>
	</tr>
</tbody>
</table>
</div>
<input type="hidden" id="gr_total_detalle" value="0" />');
$form->Hidden('gr_oc_id',0);
$form->Hidden('gr_nv_id',0);
$form->Hidden('gr_os_id',0);
$form->Hidden('gr_fc_contacto',0);
$form->End('Crear','addbtn');

$rlist = $db->select('tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco','tf.dc_funcionario,tf.dg_nombres,tf.dg_ap_paterno,tf.dg_ap_materno',"tf.dc_empresa={$empresa} AND dc_modulo = 1",array('order_by' => 'dg_nombres'));

$responsables = '<select class="prod_responsable inputtext" style="width:155px;" disabled="disabled" name="responsable[]"><option value="0"></option>';
foreach($rlist as $r){
	$responsables .= "<option value='{$r['dc_funcionario']}'>{$r['dg_nombres']} {$r['dg_ap_paterno']} {$r['dg_ap_materno']}</option>";
}
$responsables .= '</select>';

$blist = $db->select('tb_bodega','dc_bodega,dg_bodega',"dc_empresa={$empresa} AND dm_tipo='0'");
$bodegas = '<select class="prod_bodega inputtext" style="width:155px;" disabled="disabled" name="bodega[]"><option value="0"></option>';
foreach($blist as $b){
	$bodegas .= "<option value='{$b['dc_bodega']}'>{$b['dg_bodega']}</option>";
}
$bodegas .= '</select>';

echo('<table id="prods_form" class="hidden">
<tr class="main">
	<td align="center">
		<input type="checkbox" name="prod[]" class="prod_id" />
		<input type="hidden" class="det_prod" value="0" disabled="disabled" name="prod_id[]" />
	</td>
	<td class="prod_codigo"></td>
	<td class="prod_descripcion"></td>
	<td><input type="text" size="5" name="cant[]" class="prod_cant" disabled="disabled" required="required" /></td>
	<td class="prod_precio" align="right">
		<input type="text" size="10" name="precio[]" disabled="disabled" class="det_price" />
	</td>
	<td class="prod_precio_total" align="right"></td>
	<td>'.$responsables.'</td>
	<td>'.$bodegas.'</td>
</tr>
</table>');

?>
</div></div>
<style type="text/css">
.gr_active_prod{
	background:#9AEE4F !important;
}
.gr_txt_right{
	text-align:right;
}
</style>
<script type="text/javascript" src="jscripts/product_manager/guia_recepcion.js?v2_11a"></script>
