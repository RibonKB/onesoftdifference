<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">Ingreso Facturas de compra</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Indique el RUT del proveedor al que se le hará la factura de compra</strong><br />Los criterios de búsqueda son el RUT, Razón social o giro del proveedor");
	$form->Start("sites/ventas/proc/cr_fac_compra.php","cr_fac_compra","cValidar");
	$form->Text("RUT proveedor","prov_rut",1,20);
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[1]+" ("+row[0]+") "+row[2];
}
$("#prov_rut").autocomplete('sites/proc/autocompleter/proveedor.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>