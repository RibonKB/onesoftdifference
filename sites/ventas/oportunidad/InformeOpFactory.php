<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once('sites/ventas/oportunidad/Oportunidad.php');

/**
 * Description of InformeOp
 *
 * @author Eduardo
 */
class InformeOpFactory extends Oportunidad {

    public $title = 'Informe Oportunidad';
    private $puntos_bant = ' dc_pts_bant <=100';
    private $request;
    private $sep_fecha = "/";

    public function indexAction($filt, $mes, $marca = null) {
        $mostrar = $this->getFormView($this->getTemplateURL('Informe_op'), array('datos' => $this->MostrarPor_Ejecutivo(),
            'meses' => $this->meses(),
            'filt' => $filt,
            'primer_mes' => $mes,
            'marcas' => $this->Mostrar_Marcas(),
            'marca' => $marca));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function mostrarPorFiltroAction() {
        $this->request = self::getRequest();
        $filt = '';
        $mes = 1;
        $filtro_marca = '';
        $filtro_f = null;
        $filtro_ln = '';
        $cambio = $this->cambioE();
        $cambio_sel = $this->cambioE();
        $anho = date('Y');
        $array = '';
        if (isset($this->request->filtro_anho)) {
            if ($this->request->filtro_anho != 0):
                $anho = $this->request->filtro_anho;
            endif;
        }

        $condicion = new stdClass;
        $condicion->linea_negocio = '';
        $condicion->marca = '';
        $condicion->funcionario = '';
		$condicion->televenta = '';
        $condicion->cliente = '';
        $condicion->estado = '';
        $condicion->clienteII = '';
        $condicion->tipo_producto = '';
        $estadoArr = '';
        $marcaArr = '';
        $lineaArr = '';
        $ejecutivoArr = '';
		$televentaArr = '';
        $clienteArr = '';
        $clienteIIArr = '';
        $tipo_prodArr = '';
        $dd = NULL;
        $datos2 = NULL;
        $ttlM2 = NULL;
        $ver_por = 'dq_monto_marca';
        $con = 0;

        if (isset($this->request->filtro_cliente)):
            $clienteArr = $this->get_condicion_multiple($this->request->filtro_cliente, 1);
            $condicion->cliente = $this->get_condicion_multiple($this->request->filtro_cliente);
        endif;

        if (isset($this->request->tipo_cambio)) {
            if ($this->request->tipo_cambio != 0) {
                $cambio_sel = $this->request->tipo_cambio;
                $cambio = $this->request->tipo_cambio;
            }
        }

        if (isset($this->request->Filtro_LN)):
            $lineaArr = $this->get_condicion_multiple($this->request->Filtro_LN, 1);
            $condicion->linea_negocio = $this->get_condicion_multiple($this->request->Filtro_LN);
        endif;

        if (isset($this->request->Filtro_funcionario)) {
            $ejecutivoArr = $this->get_condicion_multiple($this->request->Filtro_funcionario, 1);
            $condicion->funcionario = $this->get_condicion_multiple($this->request->Filtro_funcionario);
        }
		if(isset($this->request->Filtro_televenta)){
			$televentaArr = $this->get_condicion_multiple($this->request->Filtro_televenta, 1);
			$condicion->televenta = $this->get_condicion_multiple($this->request->Filtro_televenta);
		}
        if (isset($this->request->filtro_marca)) {
            $marcaArr = $this->get_condicion_multiple($this->request->filtro_marca, 1);
            $condicion->marca = $this->get_condicion_multiple($this->request->filtro_marca);
        }

        if (isset($this->request->filtro_cliente_otro)):
            $clienteIIArr = $this->get_condicion_multiple($this->request->filtro_cliente_otro, 1);
            $condicion->clienteII = $this->get_condicion_multiple($this->request->filtro_cliente_otro, 2);
        endif;

        if (isset($this->request->filtro_tipo_producto)):
            $tipo_prodArr = $this->get_condicion_multiple($this->request->filtro_tipo_producto, 1);
            $condicion->tipo_producto = $this->get_condicion_multiple($this->request->filtro_tipo_producto);
        endif;

        if (isset($this->request->filtro_mes)):
            if ($this->request->filtro_mes != 0):
                $mes = $this->request->filtro_mes;
            endif;
            if (isset($this->request->Ver_Por)):
                $ver_por = $this->request->Ver_Por;
                if ($ver_por != 'dq_monto_marca') {
                    $con = 1;
                }

            endif;
        endif;
        if (isset($this->request->Filtro_estado)):
            $estadoArr = $this->get_condicion_multiple($this->request->Filtro_estado, 1);
            $condicion->estado = $this->get_condicion_multiple($this->request->Filtro_estado);
        endif;
        if (isset($this->request->filtro)) {
            if ($this->request->filtro == 'ejecutivo') {
                $this->puntos_bant = '  AND op.dc_pts_bant <=100';
                $this->puntos('hola');
                $filt = 'Ejecutivo';
                $datos2 = NULL;
                $datos = $this->MostrarPor_Ejecutivo($condicion, $mes, $anho, $this->puntos_bant, $ver_por);
                $ttlM = $this->TotalPor_Mes($datos, $mes, '', $filtro_f);
            } elseif ($this->request->filtro == 'linea de negocio') {
                $this->puntos_bant = '  AND op.dc_pts_bant <=100';
                $this->puntos('hola');
                $filt = 'Linea de Negocio';
                $datos2 = NULL;
                $datos = $this->MostrarPor_lineaNegocio($condicion, $mes, $anho, $this->puntos_bant, $ver_por);
                $ttlM = $this->TotalPor_Mes($datos, $mes, '', $filtro_ln);
            } elseif ($this->request->filtro == 'marca') {
                $this->puntos_bant = ' AND op.dc_pts_bant <=100';
                $this->puntos('hola');
                $filt = 'Marca';
                $datos2 = NULL;
                $datos = $this->MostrarPor_Marca($condicion, $mes, $anho, $this->puntos_bant, $ver_por);
                $ttlM = $this->TotalPor_Mes($datos, $mes, '', $filtro_marca);
            } elseif ($this->request->filtro == 'oportunidad') {
                $filt = 'Oportunidad';
                $this->puntos();
                $datos2 = NULL;
                $datos = $this->MostrarPor_Oportunidad($this->puntos_bant, $mes, $anho, $condicion, $ver_por);
                $ttlM = $this->TotalPor_Mes($datos, $mes, 2);
                $dd = 12;
            } elseif ($this->request->filtro == 'cliente') {
                $this->puntos_bant = ' AND op.dc_pts_bant <=100';
                $this->puntos('hola');
                $filt = 'Cliente';
                $datos2 = $this->MostrarOtros_Clientes($condicion, $mes, $anho, $this->puntos_bant, $ver_por);
                $datos = $this->MostrarPor_Cliente($condicion, $mes, $anho, $this->puntos_bant, $ver_por);
                $ttlM = $this->TotalPor_Mes($datos, $mes, '', $filtro_f);
                $ttlM2 = $this->TotalPor_Mes($datos2, $mes, '', $filtro_f);
            } elseif ($this->request->filtro == 'tipo producto') {
                $this->puntos_bant = ' AND op.dc_pts_bant <=100';
                $this->puntos('hola');
                $filt = 'Tipo Producto';
                $datos2 = NULL;
                $datos = $this->MostrarPor_TipoProducto($condicion, $mes, $anho, $this->puntos_bant, $ver_por);
                $ttlM = $this->TotalPor_Mes($datos, $mes);
            }
        } else {
            $this->puntos_bant = ' AND op.dc_pts_bant <=100';
            $this->puntos('hola');
            $filt = 'Ejecutivo';
            $datos2 = NULL;
            $datos = $this->MostrarPor_Ejecutivo($condicion, $mes, $anho, $this->puntos_bant, $ver_por);
            $ttlM = $this->TotalPor_Mes($datos, $mes, '', $filtro_f);
        }
        $array = array('datos' => $datos,
            'datos2' => $datos2,
            'dd' => $dd,
            'meses' => $this->meses(),
            'filt' => $filt,
            'primer_mes' => $mes,
            'cambio' => $cambio,
            'cambios' => $this->cambio2(),
            'cambio_sel' => $cambio_sel,
            'funcionario' => $this->Mostrar_Ejecutivo(),
            'sele1' => $ejecutivoArr,
            'TtlMes' => $ttlM,
            'TtlMes2' => $ttlM2,
            'TtlQ' => $this->TXQ($ttlM),
            'TtlQ2' => $this->TXQ($ttlM2),
            'anho' => $anho,
            'marcas' => $this->Mostrar_Marcas(),
            'lineas' => $this->Mostrar_LineaNegocio(),
            'clientes' => $this->Mostrar_Cliente(),
            'sele2' => $lineaArr,
            'sele3' => $marcaArr,
            'cliente_select' => $clienteArr,
            'puntos_b' => $this->puntos_bant,
            'estados' => $this->get_Estados(),
            'estado_select' => $estadoArr,
            'clientes_otro' => $this->Mostrar_Cliente_Otro(),
            'cli_otro_sel' => $clienteIIArr,
            'ver_por_sel' => $ver_por,
            'cambio_s_n' => $con,
            'tipo_prod' => $this->Mostrar_tipo_producto(),
            'tipo_p_select' => $tipo_prodArr);
        $mostrar = $this->getFormView($this->getTemplateURL('Informe_op'), $array);
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function VerDetalleAction() {

        $this->request = self::getRequest();
        $marca = '';
        $ln = '';
        $ejec = '';
        $estado = '';
        $cliente = '';
        $cliente2 = '';
        $prod_tipo = '';
        if ($this->request->ln):
            $ln = $this->get_condicion_multiple($this->request->ln);
        endif;
        if ($this->request->otro):

            $marca = $this->get_condicion_multiple($this->request->otro);
        endif;
        if ($this->request->otro2):

            $ejec = $this->get_condicion_multiple($this->request->otro2);
        endif;
        if ($this->request->estado) {
            $estado = $this->get_condicion_multiple($this->request->estado);
        }
        if ($this->request->cliente):
            $cliente = $this->get_condicion_multiple($this->request->cliente);
        endif;
        if ($this->request->cliente2):
            $cliente2 = $this->get_condicion_multiple($this->request->cliente2, 2);
        endif;
        if ($this->request->tipo_prod):
            $prod_tipo = $this->get_condicion_multiple($this->request->tipo_prod, 2);
        endif;
        $detalle = '';
        $array = '';
        if ($this->request->tipo == 'Ejecutivo') {
            $this->title = 'Detalle del ' . $this->request->tipo;
            $detalle = $this->
                    getDetalle_Ejecutivo("op.dc_ejecutivo=" . $this->request->id, $this->request->mes,$this->request->anho, $this->request->puntos, $ln, $marca, '', $estado, $cliente, $cliente2, $prod_tipo);
            $array = array('detalle' => $detalle,
                'empresa' => $this->getEmpresa(),
                'cambio' => $this->request->cambio,
                'f' => 2,
                'tipo' => $this->request->tipo,
                'total' => $this->request->total);
        } elseif ($this->request->tipo == 'Marca') {

            $this->title = 'Detalle de la ' . $this->request->tipo;
            $detalle = $this->
                    getDetalle("opd.dc_marca=" . $this->request->id, $this->request->mes,$this->request->anho, $this->request->puntos, $ln, '', $ejec, $estado, $cliente, $cliente2, $prod_tipo);
            $array = array('detalle' => $detalle,
                'empresa' => $this->getEmpresa(),
                'cambio' => $this->request->cambio,
                'tipo' => $this->request->tipo,
                'total' => $this->request->total);
        } elseif ($this->request->tipo == 'Linea de Negocio') {
            $this->title = 'Detalle de la ' . $this->request->tipo;
            $detalle = $this->
                    getDetalle("opd.dc_linea_negocio=" . $this->request->id, $this->request->mes,$this->request->anho, $this->request->puntos, '', $marca, $ejec, $estado, $cliente, $cliente2, $prod_tipo);
            $array = array('detalle' => $detalle,
                'empresa' => $this->getEmpresa(),
                'cambio' => $this->request->cambio,
                'tipo' => $this->request->tipo,
                'total' => $this->request->total);
        } elseif ($this->request->tipo == 'Oportunidad') {
            $this->title = 'Detalle de la ' . $this->request->tipo . ' N°';
            $detalle = $this->
                    getDetalle("opd.dc_oportunidad=" . $this->request->id, $this->request->mes,$this->request->anho, $this->request->puntos, $ln, $marca, $ejec, $estado, $cliente, $cliente2, $prod_tipo);
            $array = array('detalle' => $detalle,
                'empresa' => $this->getEmpresa(),
                'cambio' => $this->request->cambio,
                'tipo' => $this->request->tipo,
                'total' => $this->request->total);
        } elseif ($this->request->tipo == 'Cliente') {
            $this->title = 'Detalle de ' . $this->request->tipo;
            $detalle = $this->
                    getDetalle_Ejecutivo("op.dc_cliente=" . $this->request->id, $this->request->mes,$this->request->anho, $this->request->puntos, $ln, $marca, $ejec, $estado, $cliente, $cliente2, $prod_tipo);
            $array = array('detalle' => $detalle,
                'empresa' => $this->getEmpresa(),
                'cambio' => $this->request->cambio,
                'f' => 2,
                'tipo' => $this->request->tipo,
                'total' => $this->request->total);
        } elseif ($this->request->tipo == 'Cliente NR') {
            $this->title = 'Detalle de ' . $this->request->tipo;
            $detalle = $this->
                    getDetalle_Ejecutivo("op.dg_cliente='" . $this->request->id . "'", $this->request->mes,$this->request->anho, $this->request->puntos, $ln, $marca, $ejec, $estado, $cliente, $cliente2, $prod_tipo);
            $array = array('detalle' => $detalle,
                'empresa' => $this->getEmpresa(),
                'cambio' => $this->request->cambio,
                'f' => 2,
                'tipo' => $this->request->tipo,
                'total' => $this->request->total);
        } elseif ($this->request->tipo == 'Tipo Producto') {
            $this->title = 'Detalle de ' . $this->request->tipo;
            $detalle = $this->
                    getDetalle("opd.dc_tipo_producto=" . $this->request->id, $this->request->mes,$this->request->anho, $this->request->puntos, $ln, $marca, $ejec, $estado, $cliente, $cliente2, $prod_tipo);
            $array = array('detalle' => $detalle,
                'empresa' => $this->getEmpresa(),
                'cambio' => $this->request->cambio,
                'tipo' => $this->request->tipo,
                'total' => $this->request->total);
        }

        $mostrar = $this->getFormView($this->getTemplateURL('Detalle_informe'), $array);
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    /*
     * ////////////////////////////////////////////////////////////////////
     * ////////////////////////////////////////////////////////////////////
     * ////////////////////////FILTRO CABECERA/////////////////////////////
     * ////////////////////////////////////////////////////////////////////
     * ////////////////////////////////////////////////////////////////////
     */

    private function MostrarPor_lineaNegocio($condicion, $mes, $anho, $puntos, $ver) {
        $arr1 = '';
        $linea_negocio = '';
        if ($condicion->linea_negocio):
            $linea_negocio = "AND dc_linea_negocio IN ({$condicion->linea_negocio})";
        endif;
        $ln = $this->coneccion('select', 'tb_linea_negocio', 'dc_linea_negocio,
                    dg_linea_negocio', "dc_empresa={$this->getEmpresa()} 
                    AND dm_activo=1 $linea_negocio
                    ORDER BY dg_linea_negocio");
        while ($linea = $ln->fetch(PDO::FETCH_OBJ)):
            $arr1[$linea->dc_linea_negocio][$linea->dg_linea_negocio] = $this->
                    Llenar_arr1(
                    'det.dc_linea_negocio', $linea->dc_linea_negocio, $anho, $mes, $puntos, $condicion->marca, $condicion->linea_negocio, $condicion->funcionario, $condicion->televenta, $condicion->estado, $condicion->cliente, $condicion->clienteII, $condicion->tipo_producto, $ver
            );
        endwhile;
        return $arr1;
    }

    private function MostrarPor_Cliente($condicion, $mes, $anho, $puntos, $ver) {
        $arr1 = '';
        $cliente = '';
        if ($condicion->cliente):
            $cliente = "AND dc_cliente IN ({$condicion->cliente})";
        endif;
        $ln = $this->coneccion('select', 'tb_cliente', 'dc_cliente,
                    dg_razon', "dc_empresa={$this->getEmpresa()} 
                    AND dm_activo=1 $cliente
                    ORDER BY dg_razon");
        while ($linea = $ln->fetch(PDO::FETCH_OBJ)):
            $arr1[$linea->dc_cliente][$linea->dg_razon] = $this->
                    Llenar_arr1(
                    'op.dc_cliente', $linea->dc_cliente, $anho, $mes, $puntos, $condicion->marca, $condicion->linea_negocio, $condicion->funcionario, $condicion->televenta, $condicion->estado, $condicion->cliente, 0, $condicion->tipo_producto, $ver
            );
        endwhile;
        return $arr1;
    }

    private function MostrarOtros_Clientes($condicion, $mes, $anho, $puntos, $ver) {
        $arr1 = '';
        $ot_cl = $this->coneccion('select', 'tb_oportunidad', 'dg_cliente', "dc_empresa={$this->getEmpresa()} AND dg_cliente <> '' ORDER BY dg_cliente");
        while ($llena = $ot_cl->fetch(PDO::FETCH_OBJ)) {
            $arr1[$llena->dg_cliente][$llena->dg_cliente] = $this->
                    Llenar_arr1(
                    'op.dg_cliente', "'" . $llena->dg_cliente . "'", $anho, $mes, $puntos, $condicion->marca, $condicion->linea_negocio, $condicion->funcionario, $condicion->televenta, $condicion->estado, '', $condicion->clienteII, $condicion->tipo_producto, $ver
            );
        }


        return $arr1;
    }

    private function MostrarPor_Marca($condicion, $mes, $anho, $puntos, $ver) {
        $arr1 = '';
        $marca = '';
        if ($condicion->marca):
            $marca = "AND dc_marca IN ({$condicion->marca})";
        endif;
        $m = $this->coneccion('select', 'tb_marca', 'dc_marca,dg_marca', "dc_empresa={$this->getEmpresa()} 
                    AND dm_activo=1 $marca 
                    ORDER BY dg_marca"
        );
        while ($marca = $m->fetch(PDO::FETCH_OBJ)):
            $arr1[$marca->dc_marca][$marca->dg_marca] = $this->
                    Llenar_arr1(
                    'det.dc_marca', $marca->dc_marca, $anho, $mes, $puntos, $condicion->marca, $condicion->linea_negocio, $condicion->funcionario, $condicion->televenta, $condicion->estado, $condicion->cliente, $condicion->clienteII, $condicion->tipo_producto, $ver
            );
        endwhile;
        return $arr1;
    }

    private function MostrarPor_Ejecutivo($condicion, $mes, $anho, $puntos, $ver) {
        $arr1 = '';
        $funcionario = '';
        if ($condicion->funcionario) {
            $funcionario = " AND fu.dc_funcionario IN ({$condicion->funcionario})";
        }
        $fu = $this->coneccion('select', 'tb_funcionario fu 
                    LEFT JOIN tb_cargo_funcionario cf ON cf.dc_modulo = 1', 'dc_funcionario,
                    CONCAT(dg_nombres," ",dg_ap_paterno," ",dg_ap_materno) as Nombre_func', "fu.dc_empresa={$this->getEmpresa()} 
                    AND fu.dm_activo=1 $funcionario ORDER BY dg_nombres");
        while ($funcionario = $fu->fetch(PDO::FETCH_OBJ)):
            $arr1[$funcionario->dc_funcionario][$funcionario->Nombre_func] = $this->
                    Llenar_arr1(
                    'op.dc_ejecutivo', $funcionario->dc_funcionario, $anho, $mes, $puntos, $condicion->marca, $condicion->linea_negocio, $condicion->funcionario, $condicion->televenta, $condicion->estado, $condicion->cliente, $condicion->clienteII, $condicion->tipo_producto, $ver
            );
        endwhile;
        return $arr1;
    }

    private function MostrarPor_Oportunidad($puntos, $mes, $anho, $condicion, $ver) {
        $arr1 = '';
        $op = $this->coneccion('select', 'tb_oportunidad', 'dc_oportunidad,
                    dq_oportunidad,
                    dm_estado,
                    dc_pts_bant,
                    dc_pregunta1_bant,
                    dc_pregunta2_bant,
                    dc_pregunta3_bant,
                    dc_pregunta4_bant', "dc_empresa={$this->getEmpresa()} {$puntos}");
        while ($oportunidad = $op->fetch(PDO::FETCH_OBJ)):
            $arr1[$oportunidad->dc_oportunidad]
                    [$oportunidad->dq_oportunidad]
                    [$oportunidad->dm_estado]
                    [$oportunidad->dc_pts_bant]
                    [$oportunidad->dc_pregunta1_bant . "_" .
                    $oportunidad->dc_pregunta2_bant . "_" .
                    $oportunidad->dc_pregunta3_bant . "_" .
                    $oportunidad->dc_pregunta4_bant] = $this->
                    Llenar_arr1(
                    'det.dc_oportunidad', $oportunidad->dc_oportunidad, $anho, $mes, '', $condicion->marca, $condicion->linea_negocio, $condicion->funcionario, $condicion->televenta, $condicion->estado, $condicion->cliente, $condicion->clienteII, $condicion->tipo_producto, $ver
            );
        endwhile;
        return $arr1;
    }

    private function MostrarPor_TipoProducto($condicion, $mes, $anho, $puntos, $ver) {
        $arr1 = '';
        $tipo_prod = '';
        if ($condicion->tipo_producto):
            $tipo_prod = " AND dc_tipo_producto IN ({$condicion->tipo_producto})";
        endif;
        $m = $this->coneccion('select', 'tb_tipo_producto', 'dc_tipo_producto,dg_tipo_producto', "dc_empresa={$this->getEmpresa()} 
                    AND dm_activo=1 $tipo_prod 
                    ORDER BY dg_tipo_producto"
        );
        while ($tp = $m->fetch(PDO::FETCH_OBJ)):
            $arr1[$tp->dc_tipo_producto][$tp->dg_tipo_producto] = $this->
                    Llenar_arr1(
                    'det.dc_tipo_producto', $tp->dc_tipo_producto, $anho, $mes, $puntos, $condicion->marca, $condicion->linea_negocio, $condicion->funcionario, $condicion->televenta, $condicion->estado, $condicion->cliente, $condicion->clienteII, $condicion->tipo_producto, $ver
            );
        endwhile;
        return $arr1;
    }

    /*
     * ////////////////////////////////////////////////////////////////
     * ////////////////////////////////////////////////////////////////
     * ///////////////////////FIN FILTRO CABECERA//////////////////////
     * ////////////////////////////////////////////////////////////////
     * ////////////////////////////////////////////////////////////////
     */

    /**
     * 
     * $tipo:
     * 1 = retorna array();
     * 2= retorna cadena de texto, separada por "," y " ' " ej: 'hola','hola2','n'....
     * vacio = retorna cadena de texto separada solo por "," ej: hola,hola2,n.....
     * 
     */
    private function cambio2() {
        $arr1 = '';
        $cambio = $this->coneccion('select', 'tb_tipo_cambio', 'dq_cambio,dg_tipo_cambio', "dc_empresa={$this->getEmpresa()}");
        while ($c = $cambio->fetch(PDO::FETCH_OBJ)) {
            if ($c->dg_tipo_cambio == 'USD' || $c->dg_tipo_cambio == 'usd' || $c->dg_tipo_cambio == 'Dolar' || $c->dg_tipo_cambio == 'dolar') {
                $c->dq_cambio = $this->cambioE();
            }
            $arr1[$c->dq_cambio] = $c->dg_tipo_cambio;
        }
        return $arr1;
    }

    private function cambioE() {
        $r = self::getRequest();
        $ind_cambio = $this->coneccion('select', 'tb_tipo_cambio', 'dq_cambio,dg_prefijo', "dc_tipo_cambio={$this->getParametrosEmpresa()->dc_indicador_tipo_cambio}");
        $ind_cambio = $ind_cambio->fetch(PDO::FETCH_OBJ);
        if ($ind_cambio != false) {
            $r->ind_cambio_prefix = $ind_cambio->dg_prefijo;
            $r->ind_cambio = $ind_cambio->dq_cambio;
        } else {
            $r->ind_cambio_prefix = '$';
            $r->ind_cambio = 1;
        }
        return $r->ind_cambio;
    }

    private function GetLast_Oportunidad_Avance_Id($id_oportunidad) {
        $ID = $this->coneccion(
                'select', 'tb_oportunidad_avance', 'dc_avance AS id', "dc_oportunidad={$id_oportunidad} AND df_creacion IS NOT NULL 
                GROUP BY df_creacion 
                ORDER BY df_creacion DESC
                LIMIT 1");
        $ID = $ID->fetch(PDO::FETCH_OBJ);

        if ($ID != false) {
            return $ID->id;
        }
    }

    private function GetOportunidad_Avance_Datos($id_oportunidad = '0') {

        $id_avance = $this->GetLast_Oportunidad_Avance_Id($id_oportunidad);
        if ($id_avance) {
            $avance = $this->coneccion(
                    'select', 'tb_oportunidad_avance', 'dg_gestion comentario_avance,df_creacion creacion_avance', "dc_avance={$id_avance}");
            return $avance;
        }
    }

    /*
     * ///////////////////////////////////////////////////
     * /////////////////////FILTROS///////////////////////
     * ///////////////////////////////////////////////////
     */
    /*
     * ///////////////////////////////////////////////////
     * /////////////////FIN FILTROS///////////////////////
     * ///////////////////////////////////////////////////
     */

    private function Fecha($fecha) {
        $f1 = explode(" ", $fecha);
        $f1_1 = explode("-", $f1[0]);
        $f1[0] = $f1_1[2] . $this->sep_fecha . $f1_1[1] . $this->sep_fecha . $f1_1[0];
        return $f1[0];
    }

    private function ident_cliente($id) {
        $cliente = $this->coneccion('select', 'tb_cliente', 'dg_razon', "dc_cliente={$id}");
        $cliente = $cliente->fetch(PDO::FETCH_OBJ);
        return $cliente->dg_razon;
    }

    private function meses() {
        $arr1[1] = 'Enero';
        $arr1[2] = 'Febrero';
        $arr1[3] = 'Marzo';
        $arr1[4] = 'Abril';
        $arr1[5] = 'Mayo';
        $arr1[6] = 'Junio';
        $arr1[7] = 'Julio';
        $arr1[8] = 'Agosto';
        $arr1[9] = 'Septiembre';
        $arr1[10] = 'Octubre';
        $arr1[11] = 'Noviembre';
        $arr1[12] = 'Diciembre';
        return $arr1;
    }

    public function TotalPor_Mes($datos, $mes, $tipo = 0, $ids = null) {
        for ($i = 1; $i <= 12; $i++):
            $sumaQXM[$i] = 0;
        endfor;
        $id_s = '';
        if ($tipo) {
            if ($datos) {
                foreach ($datos as $id => $ar1):
                    $id_s = $id;
                    if ($ids):
                        $id_s = $ids;
                    endif;
                    if ($id == $id_s):
                        foreach ($ar1 as $nombre => $ar2):
                            foreach ($ar2 as $estado => $ar3):
                                foreach ($ar3 as $puntos => $ar4):
                                    foreach ($ar4 as $preguntas => $MesYTotal):
                                        for ($i = 1; $i <= 12; $i++):
                                            $sumaQXM[$i]+=$MesYTotal[$mes];
                                            if ($mes == 12):
                                                $mes = 0;
                                            endif;
                                            $mes++;
                                        endfor;
                                    endforeach;
                                endforeach;
                            endforeach;
                        endforeach;
                    endif;
                endforeach;
            }
        }else {
            foreach ($datos as $id => $ar1):
                $id_s = $id;
                if ($ids):
                    $id_s = $ids;
                endif;
                if ($id == $id_s):
                    foreach ($ar1 as $nombre => $MesYTotal2):
                        for ($i = 1; $i <= 12; $i++):
                            $sumaQXM[$i]+=$MesYTotal2[$mes];
                            if ($mes == 12):
                                $mes = 0;
                            endif;
                            $mes++;
                        endfor;
                    endforeach;
                endif;
            endforeach;
        }
        return $sumaQXM;
    }

    private function TXQ($totalXMes) {
        $n = 1;
        for ($r = 1; $r <= 4; $r++):
            $ttlQ[$r] = 0;
        endfor;
        for ($i = 1; $i <= 4; $i++):
            $suma = 0;
            for ($l = 1; $l <= 3; $l++):
                $suma+=$totalXMes[$n];
                $n++;
            endfor;
            $ttlQ[$i] = $suma;
        endfor;

        return $ttlQ;
    }

    /*
     * /////////////////////////////////////////////////
     * ////////////////////QUERY´S//////////////////////
     * /////////////////////////////////////////////////
     */

    private function AnalisaCondicion_pers($cond) {
        $cond = strtolower($cond);
        $separar = explode(" ", $cond);
        if (isset($separar[4])) {
            $junta = $separar[0] . " " . $separar[1] . " " . $separar[2] . " " . $separar[3];
            if ($junta == 'mayor o igual a' || $junta == 'mayor o igual que') {

                $condicion = " >=" . $separar[4];
            } else if ($junta == 'menor o igual que' || $junta == 'menor o igual a') {

                $condicion = " <=" . $separar[4];
            }
        } else if (isset($separar[3])) {

            $junta2 = $separar[0] . " " . $separar[2];
            $junta3 = $separar[0] . " " . $separar[1] . " " . $separar[2];

            if ($junta2 == 'entre y') {

                $condicion = " between " . $separar[1] . " AND " . $separar[3];
            } elseif ($junta3 == 'mayor o igual' || $junta3 == 'mayor o =') {

                $condicion = " <=" . $separar[3];
            } else if ($junta3 == 'menor o igual' || $junta3 == 'menor o =') {

                $condicion = " >=" . $separar[3];
            }
        } else if (isset($separar[2])) {

            $junta1 = $separar[0] . " " . $separar[1];

            if ($junta1 == 'mayor a' || $junta1 == 'mayor que' ||
                    $junta1 == 'superior a' || $junta1 == 'superior que') {

                $condicion = " >" . $separar[2];
            } else if ($junta1 == 'menor a' || $junta1 == 'menor que' ||
                    $junta1 == 'inferior a' || $junta1 == 'inferior que') {

                $condicion = " <" . $separar[2];
            } else if ($junta1 == 'igual a' || $junta1 == 'igual que' ||
                    $junta1 == '= a' || $junta1 == '= que') {

                $condicion = " =" . $separar[2];
            } else if ($separar[1] == 'y' || $separar[1] == "and" || $separar[1] == "&") {

                $condicion = " between " . $separar[0] . " AND " . $separar[2];
            } else
            if ($junta1 == 'mayor igual' ||
                    $junta1 == 'mayor =') {

                $condicion = " >=" . $separar[2];
            } else if ($junta1 == 'menor igual' ||
                    $junta1 == 'menor =') {

                $condicion = " <=" . $separar[2];
            }
        } else if (isset($separar[1])) {
            if ($separar[0]) {

                $this->getErrorMan()->showAviso('Comando no reconocido');
                exit;
            }
        } else {

            $this->getErrorMan()->showAviso('Comando no reconocido');
            exit;
        }
        return $condicion;
    }

    private function Condicion_preguntas($m = null) {
        $condicion = '';
        $tipo_busqueda = isset($this->request->tipo_busqueda_preguntas) ? 1 : 0;
        $pr[1] = isset($this->request->sino_1) ? 1 : 0;
        $pr[2] = isset($this->request->sino_2) ? 1 : 0;
        $pr[3] = isset($this->request->sino_3) ? 1 : 0;
        $pr[4] = isset($this->request->sino_4) ? 1 : 0;
        if ($m) {
            if ($tipo_busqueda == 1) {
                $condicion = " AND op.dc_pregunta1_bant={$pr[1]} 
                AND op.dc_pregunta2_bant={$pr[2]} 
                    AND op.dc_pregunta3_bant={$pr[3]} 
                        AND op.dc_pregunta4_bant={$pr[4]} ";
            } else {
                for ($i = 1; $i <= 4; $i++) {
                    if ($pr[$i] == 1) {
                        if ($condicion == '') {
                            $condicion = " AND (op.dc_pregunta{$i}_bant=1 ";
                        } else {
                            $condicion = $condicion . " || op.dc_pregunta{$i}_bant=1 ";
                        }
                    }
                }
                $condicion = $condicion . ")";
            }
        } else {
            if ($tipo_busqueda == 1) {
                $condicion = " dc_pregunta1_bant={$pr[1]} 
                AND dc_pregunta2_bant={$pr[2]} 
                    AND dc_pregunta3_bant={$pr[3]} 
                        AND dc_pregunta4_bant={$pr[4]} ";
            } else {
                for ($i = 1; $i <= 4; $i++) {
                    if ($pr[$i] == 1) {
                        if ($condicion == '') {
                            $condicion = " (dc_pregunta{$i}_bant=1 ";
                        } else {
                            $condicion = $condicion . " || dc_pregunta{$i}_bant=1 ";
                        }
                    }
                }
                $condicion = $condicion . ")";
            }
        }
        return $condicion;
    }

    private function get_condicion_multiple($condicion, $tipo = 0) {
        $cond = "";
        $i = 1;
        foreach ($condicion as $num => $est):
            if ($tipo == 1):
                if ($i == 1) {
                    $cond[] = $est;
                    $i++;
                } else {
                    $cond[] = $est;
                }
            else:
                if ($tipo == 2):
                    if ($i == 1) {
                        $cond = "'" . $est . "'";
                        $i++;
                    } else {
                        $cond = $cond . ",'" . $est . "'";
                    }
                else:
                    if ($i == 1) {
                        $cond = $est;
                        $i++;
                    } else {
                        $cond = $cond . "," . $est;
                    }
                endif;

            endif;

        endforeach;

        return $cond;
    }

    private function Llenar_arr1($campo, $valorCampo, $anho, $month = 1, $condPuntos = '', $condMarca = NULL, $condLinea = NULL, $condEjecutivo = NULL, $condTeleventa = NULL, $condEstado = NULL, $condCliente = NULL, $condCliente2 = NULL, $condTipoP = NULL, $verPor = "dq_monto_marca") {
        $suma = 0;
        $arr1 = '';
        $ValorAnual = 0;
        $estad = "";
        $linea = "";
        $marca = "";
        $ejecutivo = "";
		$televenta = "";
        $cliente = "";
        $cliente2 = "";
        $prod_tipo = "";
        $sel = "";
        if ($condEstado) {
            $estad = "AND op.dm_estado IN ({$condEstado})";
        }
        if ($condLinea) {
            $linea = "AND det.dc_linea_negocio IN ({$condLinea})";
        }
        if ($condMarca) {
            $marca = "AND det.dc_marca IN ({$condMarca})";
        }
        if ($condEjecutivo) {
            $ejecutivo = "AND op.dc_ejecutivo IN ({$condEjecutivo})";
        }
		if($condTeleventa){
			$televenta = "AND op.dc_tele_venta IN ({$condTeleventa})";
		}
        if ($condCliente):
            $cliente = "AND op.dc_cliente IN ({$condCliente})";
        endif;
        if ($condCliente2):
            $cliente2 = "AND op.dg_cliente IN ({$condCliente2})";
        endif;

        if ($condTipoP) {
            $prod_tipo = "AND det.dc_tipo_producto IN ({$condTipoP})";
        }

        for ($mes = 1; $mes <= 12; $mes++):

            if ($condCliente2 && $condCliente) {
                $valores = $this->UNION_SELECT("tb_oportunidad_detalle det
                                                JOIN tb_oportunidad op
                                                ON op.dc_oportunidad=det.dc_oportunidad
                                                LEFT JOIN tb_estados st ON st.dc_estado=op.dm_estado", 
                                                $verPor, 
                                                NULL, 
                                                NULL, 
                                                "{$campo}={$valorCampo}
                                                AND Month(op.df_proyeccion_cierre)={$month} 
                                                AND Year(op.df_proyeccion_cierre)={$anho}
                                                {$condPuntos}
                                                {$marca}
                                                {$linea}
                                                {$ejecutivo}
												{$televenta}
                                                {$cliente2}
                                                {$prod_tipo}
                                                {$estad}
                                                AND det.dm_activo=1", 
                                                "{$campo}={$valorCampo}
                                                AND Month(op.df_proyeccion_cierre)={$month} 
                                                AND Year(op.df_proyeccion_cierre)={$anho}
                                                {$condPuntos}
                                                {$marca}
                                                {$linea}
                                                {$ejecutivo}
												{$televenta}
                                                {$cliente}
                                                {$estad}
                                                AND det.dm_activo=1");
            } else {
                $valores = $this->coneccion('select', 
                        "tb_oportunidad_detalle det
                        JOIN tb_oportunidad op
                        ON op.dc_oportunidad=det.dc_oportunidad
                        LEFT JOIN tb_estados st ON st.dc_estado=op.dm_estado", 
                        "$verPor", 
                        "{$campo}={$valorCampo}
                        AND Month(op.df_proyeccion_cierre)={$month} 
                        AND Year(op.df_proyeccion_cierre)={$anho}
                        {$condPuntos}
                        {$marca}
                        {$linea}
                        {$ejecutivo}
						{$televenta}
                        {$cliente}
                        {$cliente2}
                        {$prod_tipo}
                        {$estad}
                        AND det.dm_activo=1");
            }
            //var_dump("<pre>",$valores,"</pre>");
            //exit;
            while ($valor = $valores->fetch(PDO::FETCH_OBJ)):
                $suma+=$valor->$verPor;
                $ValorAnual+=$suma;
            endwhile;
            $arr1[$month] = $suma;
            $suma = 0;
            if ($month == 12) {
                $month = 0;
                $anho+=1;
            }
            if ($mes == 12) {
                if ($ValorAnual == 0) {
                    for ($i = 1; $i <= 12; $i++) {
                        $arr1[$i] = 'n';
                    }
                }
                $ValorAnual = 0;
            }
            $month++;
        endfor;
        return $arr1;
    }

    public function getDetalle($id, $mes,$anho, $pts = '', $ln = null, $ot = null, $ot2 = null, $CondEstado = null, $condCliente = NULL, $client2 = NULL, $prod_tipo = NULL) {
        $cliente = '';
        $marca = '';
        $linea = '';
        $ejecutivo = '';
        $estado = '';
        $cliente = '';
        $tipo_prod = '';
        if ($ot):
            $marca = "opd.dc_marca IN ({$ot}) AND ";
        endif;
        if ($ot2):
            $ejecutivo = "op.dc_ejecutivo IN ({$ot2}) AND ";
        endif;
        if ($ln):
            $linea = "opd.dc_linea_negocio IN({$ln}) AND ";
        endif;
        if ($CondEstado):
            $estado = "AND op.dm_estado IN ({$CondEstado})  ";
        endif;
        if ($condCliente):
            $cliente = "op.dc_cliente IN ({$condCliente}) AND ";
        endif;

        if ($prod_tipo):
            $tipo_prod = "opd.dc_tipo_producto IN ({$prod_tipo}) AND ";
        endif;
        $marcaDet = $this->coneccion('select', "tb_oportunidad_detalle opd 
                    LEFT JOIN tb_oportunidad op ON opd.dc_oportunidad=op.dc_oportunidad
                    LEFT JOIN tb_marca ma ON ma.dc_marca=opd.dc_marca
                    LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio=opd.dc_linea_negocio
                    LEFT JOIN tb_funcionario fu ON fu.dc_funcionario=op.dc_ejecutivo 
                    LEFT JOIN tb_estados st ON st.dc_estado=op.dm_estado
                    LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto=opd.dc_tipo_producto
					LEFT JOIN tb_funcionario tv ON tv.dc_funcionario = op.dc_tele_venta", 'op.dq_oportunidad,
                    op.dc_oportunidad,
                    ln.dg_linea_negocio,
                    ma.dg_marca,
                    CONCAT(fu.dg_nombres," ",fu.dg_ap_paterno," ",fu.dg_ap_materno) nombre,
					CONCAT_WS(" ",tv.dg_nombres,tv.dg_ap_paterno,tv.dg_ap_materno) televenta,
                    op.df_fecha_emision,
                    op.dc_empresa,
                    op.df_proyeccion_cierre,
                    opd.dm_activo,
                    op.dc_cliente,
                    op.dg_cliente,
                    op.dc_pregunta1_bant,
                    op.dc_pregunta2_bant,
                    op.dc_pregunta3_bant,
                    op.dc_pregunta4_bant,
                    opd.dq_monto_marca,
                    opd.dq_cantidad,
                    tp.dg_tipo_producto,
                    st.dg_estado,
                    op.dm_estado', "{$id} AND
                       {$ejecutivo} {$marca}{$linea}{$cliente}{$tipo_prod}
                        op.dc_empresa={$this->getEmpresa()}
                             {$pts} {$estado}
                        AND Month(op.df_proyeccion_cierre)={$mes} 
                            AND Year(op.df_proyeccion_cierre)={$anho}
                            AND opd.dm_activo=1 
                            ORDER BY op.dq_oportunidad");
        $ll = $marcaDet->fetchAll(PDO::FETCH_OBJ);
        if ($ll == FALSE) {
            $cliente = "op.dg_cliente IN ({$client2}) AND ";
            $marcaDet = $this->coneccion('select', "tb_oportunidad_detalle opd 
                    LEFT JOIN tb_oportunidad op ON opd.dc_oportunidad=op.dc_oportunidad
                    LEFT JOIN tb_marca ma ON ma.dc_marca=opd.dc_marca
                    LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio=opd.dc_linea_negocio
                    LEFT JOIN tb_funcionario fu ON fu.dc_funcionario=op.dc_ejecutivo 
                    LEFT JOIN tb_estados st ON st.dc_estado=op.dm_estado
                    LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto=opd.dc_tipo_producto
					LEFT JOIN tb_funcionario tv ON tv.dc_funcionario = op.dc_tele_venta", 'op.dq_oportunidad,
                    op.dc_oportunidad,
                    ln.dg_linea_negocio,
                    ma.dg_marca,
                    CONCAT(fu.dg_nombres," ",fu.dg_ap_paterno," ",fu.dg_ap_materno) nombre,
					CONCAT_WS(" ",tv.dg_nombres,tv.dg_ap_paterno,tv.dg_ap_materno) televenta,
                    op.df_fecha_emision,
                    op.dc_empresa,
                    op.df_proyeccion_cierre,
                    opd.dm_activo,
                    op.dc_cliente,
                    op.dg_cliente,
                    op.dc_pregunta1_bant,
                    op.dc_pregunta2_bant,
                    op.dc_pregunta3_bant,
                    op.dc_pregunta4_bant,
                    opd.dq_monto_marca,
                    opd.dq_cantidad,
                    tp.dg_tipo_producto,
                    st.dg_estado,
                    op.dm_estado', "{$id} AND
                       {$ejecutivo} {$marca}{$linea}{$cliente}{$tipo_prod}
                        op.dc_empresa={$this->getEmpresa()}
                            {$pts} {$estado}
                        AND Month(op.df_proyeccion_cierre)={$mes} 
                            AND opd.dm_activo=1 
                            ORDER BY op.dq_oportunidad");

            $ll = $marcaDet->fetchAll(PDO::FETCH_OBJ);
        }

        foreach ($ll as $c) {
            if ($c->dc_cliente == 0) {
                $c->dg_cliente = $c->dg_cliente;
            } else {
                $c->dg_cliente = $this->ident_cliente($c->dc_cliente);
            }
            $c->df_fecha_emision = $this->Fecha($c->df_fecha_emision);
            $c->df_proyeccion_cierre = $this->Fecha($c->df_proyeccion_cierre);
            $avance = $this->GetOportunidad_Avance_Datos($c->dc_oportunidad);
            while ($d = $avance->fetch(PDO::FETCH_OBJ)) {
                $c->comentario_avance = $d->comentario_avance;
                $d->creacion_avance = $this->Fecha($d->creacion_avance);
                $c->creacion_avance = $d->creacion_avance;
            }
        }

        return $ll;
    }

    public function getDetalle_Ejecutivo($id, $mes,$anho, $pts = '', $ln = NULL, $ot = NULL, $ot2 = NULL, $CondEstado = NULL, $condCliente = NULL, $client2 = NULL, $prod_tipo = NULL) {
        $cliente = '';
        $marca = '';
        $linea = '';
        $ejecutivo = '';
        $estado = '';
        $cliente = '';
        $tipo_prod = '';
        $m = 0;
        if ($ot):
            $marca = " opd.dc_marca IN ({$ot}) AND ";
        endif;
        if ($ot2):
            $ejecutivo = " op.dc_ejecutivo IN ({$ot2})AND ";
        endif;
        if ($ln):
            $linea = " opd.dc_linea_negocio IN ({$ln}) AND ";
        endif;
        if ($CondEstado):
            $estado = " AND op.dm_estado IN ({$CondEstado}) ";
        endif;
        if ($condCliente):
            $cliente = "op.dc_cliente IN ({$condCliente}) AND ";
        endif;
        if ($prod_tipo):
            $tipo_prod = "opd.dc_tipo_producto IN ({$prod_tipo}) AND ";
        endif;
        $marcaDet = $this->coneccion('select', "tb_oportunidad op
                    LEFT JOIN tb_oportunidad_detalle opd ON op.dc_oportunidad=opd.dc_oportunidad
                    LEFT JOIN tb_funcionario fu ON fu.dc_funcionario=op.dc_ejecutivo
                    LEFT JOIN tb_marca ma ON ma.dc_marca=opd.dc_marca
                    LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio=opd.dc_linea_negocio
                    LEFT JOIN tb_estados st ON st.dc_estado=op.dm_estado
                    LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto=opd.dc_tipo_producto
					LEFT JOIN tb_funcionario tv ON tv.dc_funcionario = op.dc_tele_venta", 'opd.dc_oportunidad_detalle, op.dq_oportunidad,
                    CONCAT(fu.dg_nombres," ",fu.dg_ap_paterno," ",fu.dg_ap_materno) nombre,
					CONCAT_WS(" ",tv.dg_nombres,tv.dg_ap_paterno,tv.dg_ap_materno) televenta,
                    op.df_fecha_emision,
                    op.dc_empresa,
                    op.df_proyeccion_cierre,
                    op.dc_cliente,
                    op.dg_cliente,
                    op.dm_estado,
                    op.dq_monto,
                    opd.dq_cantidad,
                    opd.dq_monto_marca,
                    op.dc_pregunta1_bant,
                    op.dc_pregunta2_bant,
                    op.dc_pregunta3_bant,
                    op.dc_pregunta4_bant,
                    op.dc_oportunidad,
                    st.dg_estado,
                    tp.dg_tipo_producto', 
                    "{$id} AND
                    {$ejecutivo}{$marca}{$linea}{$cliente}{$tipo_prod}
                    op.dc_empresa={$this->getEmpresa()}
                    {$pts}{$estado}
                    AND Month(op.df_proyeccion_cierre)={$mes} 
                    AND Year(op.df_proyeccion_cierre)={$anho}
                    AND opd.dm_activo=1 
                    ORDER BY op.dq_monto");
        $ll = $marcaDet->fetchAll(PDO::FETCH_OBJ);
        if ($ll == FALSE) {
            $cliente = "op.dg_cliente IN ({$client2}) AND ";
            $marcaDet = $this->coneccion('select', "tb_oportunidad op
                    LEFT JOIN tb_oportunidad_detalle opd ON op.dc_oportunidad=opd.dc_oportunidad
                    LEFT JOIN tb_funcionario fu ON fu.dc_funcionario=op.dc_ejecutivo
                    LEFT JOIN tb_marca ma ON ma.dc_marca=opd.dc_marca
                    LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio=opd.dc_linea_negocio
                    LEFT JOIN tb_estados st ON st.dc_estado=op.dm_estado
                    LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto=opd.dc_tipo_producto
					LEFT JOIN tb_funcionario tv ON tv.dc_funcionario = op.dc_tele_venta", 'opd.dc_oportunidad_detalle, op.dq_oportunidad,
                    CONCAT(fu.dg_nombres," ",fu.dg_ap_paterno," ",fu.dg_ap_materno) nombre,
					CONCAT_WS(" ",tv.dg_nombres,tv.dg_ap_paterno,tv.dg_ap_materno) televenta,
                    op.df_fecha_emision,
                    op.dc_empresa,
                    op.df_proyeccion_cierre,
                    op.dc_cliente,
                    op.dg_cliente,
                    op.dm_estado,
                    op.dq_monto,
                    opd.dq_monto_marca,
                    opd.dq_cantidad,
                    op.dc_pregunta1_bant,
                    op.dc_pregunta2_bant,
                    op.dc_pregunta3_bant,
                    op.dc_pregunta4_bant,
                    op.dc_oportunidad,
                    st.dg_estado,
                    tp.dg_tipo_producto', 
                    "{$id} AND
                    {$ejecutivo}
                    {$marca}
                    {$linea}
                    {$cliente}
                    {$tipo_prod}
                    op.dc_empresa={$this->getEmpresa()}
                    {$pts}
                    {$estado}
                    AND Month(op.df_proyeccion_cierre)={$mes} 
                    AND Year(op.df_proyeccion_cierre)={$anho}
                    AND opd.dm_activo=1 
                    ORDER BY op.dq_monto");

            $ll = $marcaDet->fetchAll(PDO::FETCH_OBJ);
        }
        foreach ($ll as $c) {
            $m = 0;

            if ($marca || $linea) {
                $c->dq_monto = $m+=$c->dq_monto_marca;
            }



            if ($c->dc_cliente == 0) {
                $c->dg_cliente = $c->dg_cliente;
            } else {
                $c->dg_cliente = $this->ident_cliente($c->dc_cliente);
            }

            $c->df_fecha_emision = $this->Fecha($c->df_fecha_emision);
            $c->df_proyeccion_cierre = $this->Fecha($c->df_proyeccion_cierre);
            $avance = $this->GetOportunidad_Avance_Datos($c->dc_oportunidad);

            while ($d = $avance->fetch(PDO::FETCH_OBJ)) {

                $c->comentario_avance = $d->comentario_avance;
                $d->creacion_avance = $this->Fecha($d->creacion_avance);
                $c->creacion_avance = $d->creacion_avance;
            }
        }
        return $ll;
    }

    private function puntos($m = '') {
        if (isset($this->request->por_puntos)) {
            if (isset($this->request->f_perso)) {
                if ($this->request->filtro_pts_perso):
                    if ($m) {
                        $this->puntos_bant = " AND op.dc_pts_bant" .
                                $this->AnalisaCondicion_pers($this->request->filtro_pts_perso);
                    } else {
                        $this->puntos_bant = " AND dc_pts_bant" .
                                $this->AnalisaCondicion_pers($this->request->filtro_pts_perso);
                    }
                endif;
            } else {
                if ($m) {
                    $this->puntos_bant = " AND op.dc_pts_bant" . $this->request->Filtro_bant_puntos;
                } else {
                    $this->puntos_bant = " AND dc_pts_bant" . $this->request->Filtro_bant_puntos;
                }
            }
        } else if (isset($this->request->por_preguntas)) {

            $this->puntos_bant = $this->Condicion_preguntas($m);
        }
    }

}

?>
