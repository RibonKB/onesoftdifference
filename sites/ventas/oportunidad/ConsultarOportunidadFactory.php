<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConsultarOportunidadFactory
 *
 * @author Eduardo
 */
class ConsultarOportunidadFactory extends Factory {

    //put your code here

    public $title = 'Oportunidades';

    public function indexAction() {
        $mostrar = $this->getFormView($this->getTemplateURL('consulta'), array(
            'clientes' => $this->Mostrar_clientes(),
            'funcionarios' => $this->Mostrar_funcionario(),
            'linea_negocio' => $this->Mostrar_linea_negocio(),
            'marca' => $this->Mostrar_marca(),
			'userdata' => $this->getUserData()
        ));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function BuscaOportunidadInformeAction() {
        $mostrar = $this->getFormView($this->getTemplateURL('buscar_Informe'));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function MostrarOportunidadAction() {
        echo $this->getFormView($this->getTemplateURL('mostrar_op'), array(
            'datos' => $this->buscar()
        ));
    }

    public function MostrarDetAction() {
        $r = self::getRequest();
        if (isset($r->bant)) {
            echo $this->getFormView($this->getTemplateURL('mostrar_det'), array('datos' => $this->busca_oportunidad($r->id), 'bant' => $r->bant, 'cambio' => $this->cambio(), 'prefijo' => $this->prefix(), 'detalle' => $this->detalle_oportunidad(), 'id' => $r->id));
        } else {
            echo $this->getFormView($this->getTemplateURL('mostrar_det'), array('datos' => $this->busca_oportunidad($r->id), 'cambio' => $this->cambio(), 'prefijo' => $this->prefix(), 'detalle' => $this->detalle_oportunidad(), 'id' => $r->id));
        }
    }

    public function VerOpAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('ver_oportunidad'), array('datos' => $this->busca_oportunidad($r->ids), 'empres' => $this->getEmpresa(), 'dbs' => $this->getConnection(), 'cambio' => $this->cambio()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function EditarOpAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('editar'), array(
            'minuto' => $this->Mostrar_minutos(),
            'hora' => $this->Mostrar_hora(),
            'datos' => $this->busca_oportunidad($r->ids),
            'marca' => $this->Mostrar_marca(),
            'clientes' => $this->Mostrar_clientes(),
            'tipo_producto' => $this->Mostrar_tipo_producto(),
            'cambio' => $this->cambio(),
            'funcionarios' => $this->Mostrar_funcionario(),
            'linea_negocio' => $this->Mostrar_linea_negocio(),
            'idsa' => $r->ids,
            'detalle_op' => $this->getDetalle($r->ids)
        ));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function ActualizaOpAction() {
        $this->ActualizarOportunidadDetalle();
        $this->ActualizarOportunidad();
        $this->getErrorMan()->showConfirm('Oportunidad modificada correctamente');
    }

    public function bantAction() {
        $r = self::getRequest();
        $mostrar = $this->getFormView($this->getTemplateURL('bant'), array('id_op' => $r->ids, 'marcados' => $this->BuscaBant()));
        echo $this->getOverlayView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function GBantAction() {
        $this->guardaBant();
        $this->getErrorMan()->showConfirm('El BANT se actualizo correctamente');
    }

    //========================================================================
    //========================================================================
    //========================================================================

    /**
     * 
     * para array simple $arr1[dato1]=dato2;
     * 
     * @param type $consulta
     * @param type $campos_retorno
     * @return type
     * 
     */
    private function llena_arr1($consulta, $campo1, $campo2, $campo3 = null) {
        $arr1 = '';
        while ($llena = $consulta->fetch(PDO::FETCH_OBJ)) {
            if ($campo3) {
                $arr1[$llena->$campo1][$llena->$campo2] = $llena->$campo3;
            } else {
                $arr1[$llena->$campo1] = $llena->$campo2;
            }
        }
        return $arr1;
    }

    //==================================================================
    //==================================================================
    //==================================================================

    private function Mostrar_minutos() {
        $min = '';
        foreach (range(0, 45, 15) as $m) {
            $m = str_pad($m, 2, '0', STR_PAD_LEFT);
            $min[$m] = $m;
        }
        return $min;
    }

    private function Mostrar_hora() {
        $hora = '';
        foreach (range(1, 12) as $h) {
            $h = str_pad($h, 2, '0', STR_PAD_LEFT);
            $hora[$h] = $h;
        }
        return $hora;
    }

    private function Mostrar_clientes() {
        $cliente = $this->coneccion('select', 'tb_cliente', 'dc_cliente,dg_razon', "dc_empresa={$this->getEmpresa()} AND dm_activo=1");
        return $this->llena_arr1($cliente, 'dc_cliente', 'dg_razon');
    }

    private function Mostrar_linea_negocio() {
        $linea = $this->coneccion('select', 'tb_linea_negocio', 'dc_linea_negocio,dg_linea_negocio', "dc_empresa={$this->getEmpresa()} AND dm_activo=1");
        return $this->llena_arr1($linea, 'dc_linea_negocio', 'dg_linea_negocio');
    }

    private function Mostrar_funcionario() {
        $funcionario = $this->coneccion('select', 'tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco', 'tf.dc_funcionario,CONCAT(tf.dg_nombres," ",tf.dg_ap_paterno," ",tf.dg_ap_materno) AS Nombre_funcionario', "tf.dc_empresa = {$this->getEmpresa()} AND dc_modulo = 1 AND dm_activo=1");
        return $this->llena_arr1($funcionario, 'dc_funcionario', 'Nombre_funcionario');
    }

    private function Mostrar_marca() {
        $marca = $this->coneccion('select', 'tb_marca', 'dc_marca,dg_marca', "dc_empresa={$this->getEmpresa()} AND dm_activo=1");
        return $this->llena_arr1($marca, 'dc_marca', 'dg_marca');
    }

    private function Mostrar_tipo_producto() {
        $tipo_producto = $this->coneccion('select', 'tb_tipo_producto', 'dc_tipo_producto,dg_tipo_producto', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_tipo_producto");
        return $this->llena_arr1($tipo_producto, 'dc_tipo_producto', 'dg_tipo_producto');
    }

    //==================================================================
    //==================================================================
    //==================================================================

    private function Condicion() {
        $r = self::getRequest();
        $estado = '';
        /*foreach ($r->op_status as $d) {
            if ($estado == '') {
                $estado = $d;
            } else {
                $estado.=',' . $d;
            }
        }*/
        $conditions = "dc_empresa = {$this->getEmpresa()} 
                          AND (df_fecha_emision BETWEEN '{$this->Ordenar_fecha($r->op_emision_desde)}' 
                              AND '{$this->Ordenar_fecha($r->op_emision_hasta)}') 
                                  ";//AND dm_estado IN ({$estado})";
        if ($r->op_numero_desde) {
            if ($r->op_numero_hasta) {
                $conditions .= " AND (dq_oportunidad BETWEEN {$r->op_numero_desde} AND {$r->op_numero_hasta})";
            } else {
                $conditions .= " AND dq_oportunidad = {$r->op_numero_desde}";
            }
        }

        if (isset($r->op_client)) {
            $r->op_client = implode(',', $r->op_client);
            $conditions .= " AND dc_cliente IN ({$r->op_client})";
        }

        if (isset($r->op_executive)) {
            $r->op_executive = implode(',', $r->op_executive);
            $conditions .= " AND dc_ejecutivo IN ({$r->op_executive})";
        }
		
		if (isset($r->op_televenta)) {
            $r->op_televenta = implode(',', $r->op_televenta);
            $conditions .= " AND dc_tele_venta IN ({$r->op_televenta})";
        }

        if (isset($r->op_buss_line)) {
            $r->op_buss_line = implode(',', $r->op_buss_line);
            $conditions .= " AND dc_linea_negocio IN ({$r->op_buss_line})";
        }

        if (isset($r->dc_marca)) {
            $r->dc_marca = implode(',', $r->dc_marca);
            $conditions .= " AND dc_marca IN ({$r->dc_marca})";
        }
        return $conditions;
    }

    private function buscar() {
        $r = self::getRequest();
        $condicion = $this->Condicion();
        $fields = 'dc_oportunidad,dq_oportunidad';
        if (isset($r->op_bant)) {
            $fields .= ",dc_pts_bant";
        }
        $datos = $this->coneccion('select', 'tb_oportunidad', $fields, $condicion." ORDER BY dq_oportunidad DESC");
        $datos = $datos->fetchAll(PDO::FETCH_OBJ);
        if ($datos == false) {
            $this->getErrorMan()->showAviso("No se encontraron oportunidades con los criterios especificados");
            exit();
        }
        return $datos;
    }

    private function Ordenar_fecha($fecha) {
        $f = explode('/', $fecha);
        $ff = $f[2] . '-' . $f[1] . '-' . $f[0] . ' 00:00:00';
        return $ff;
    }

    private function verificar_Montos() {
        $r = self::getRequest();
        $total_est = $r->op_monto;
        $total_suma_mpm = 0;
        $item = $r->item_id;
        $marca = $r->dc_marca;
        $valor = $r->valor;
        foreach ($item as $c => $v) {
            if ($marca[$c]) {
                $total_suma_mpm+=$valor[$c];
            }
        }
        if ($total_est != $total_suma_mpm):
            $this->getErrorMan()->showWarning('El monto estimado y el monto total de los productos no coinciden');
            exit;
        endif;
    }

    //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    //-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    private function BuscaBant() {
        $r = self::getRequest();
        $bant = $this->coneccion('select', 'tb_oportunidad', 'dc_pts_bant,dc_pregunta1_bant,dc_pregunta2_bant,dc_pregunta3_bant,dc_pregunta4_bant', "dc_oportunidad={$r->ids}");
        $bant = $bant->fetchAll(PDO::FETCH_OBJ);
        return $bant;
    }

    private function busca_oportunidad($id) {
        $r = self::getRequest();
        $empresa = $this->getEmpresa();
        $datos = $this->coneccion('select', "(SELECT * FROM tb_oportunidad WHERE dc_empresa={$empresa} AND dc_oportunidad={$id}) op
LEFT JOIN (SELECT * FROM tb_linea_negocio WHERE dc_empresa={$empresa}) l ON op.dc_linea_negocio = l.dc_linea_negocio
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) c ON op.dc_cliente = c.dc_cliente
LEFT JOIN (SELECT * FROM tb_cotizacion WHERE dc_empresa={$empresa}) cot ON op.dc_cotizacion=cot.dc_cotizacion
LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) e ON op.dc_ejecutivo=e.dc_funcionario
LEFT JOIN (SELECT * FROM tb_usuario WHERE dc_empresa={$empresa}) u ON op.dc_usuario_creacion=u.dc_usuario
LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) r ON r.dc_funcionario = op.dc_tele_venta
LEFT JOIN tb_marca m ON m.dc_marca = op.dc_marca
LEFT JOIN tb_estados es ON es.dc_estado=op.dm_estado", 'op.dq_oportunidad,
            op.dc_oportunidad,
            dc_pts_bant,
            dg_razon,
            dq_cotizacion,
            dg_comentario,
            dg_linea_negocio,
            dg_estado,
            dq_monto,
            dg_fono,
            dg_nombre_contacto,
            df_proyeccion_cierre,
            dc_tele_venta,
            dg_deal_id,
            DATE_FORMAT(df_llamada,"%d/%m/%Y %I:%i %p") AS df_llamada,
            dg_correo,
            CONCAT_WS(" ",e.dg_nombres," ",e.dg_ap_paterno," ",e.dg_ap_materno) AS dg_ejecutivo,
            dm_estado,
            dg_cliente, 
            c.dc_cliente,
            m.dg_marca,
            op.dc_ejecutivo,
            CONCAT_WS(" ",r.dg_nombres," ",r.dg_ap_paterno," ",r.dg_ap_materno) AS dg_responsable,
            DATE_FORMAT(op.df_fecha_emision,"%d/%m/%Y") AS df_emision');
        $datos = $datos->fetchAll(PDO::FETCH_OBJ);
        if ($datos == false) {
            $this->getErrorMan()->showWarning("No se ha encontrado la Oportunidad especificada");
            exit();
        }
        return $datos;
    }

    private function cambio() {
        $r = self::getRequest();
        $ind_cambio = $this->coneccion('select', 'tb_tipo_cambio', 'dq_cambio,dg_prefijo', "dc_tipo_cambio={$this->getParametrosEmpresa()->dc_indicador_tipo_cambio}");

        $ind_cambio = $ind_cambio->fetch(PDO::FETCH_OBJ);

        if ($ind_cambio != false) {
            $r->ind_cambio_prefix = $ind_cambio->dg_prefijo;

            $r->ind_cambio = $ind_cambio->dq_cambio;
        } else {
            $r->ind_cambio_prefix = '$';
            $r->ind_cambio = 1;
        }



        return $r->ind_cambio;
    }

    private function prefix() {
        $r = self::getRequest();
        $ind_cambio = $this->coneccion('select', 'tb_tipo_cambio', 'dq_cambio,dg_prefijo', "dc_tipo_cambio={$this->getParametrosEmpresa()->dc_indicador_tipo_cambio}");

        $ind_cambio = $ind_cambio->fetch(PDO::FETCH_OBJ);
        return $ind_cambio->dg_prefijo;
    }

    private function detalle_oportunidad() {
        $r = self::getRequest();
        $arr1 = '';
        $detalle = $this->coneccion('select', 'tb_oportunidad_detalle od 
            LEFT JOIN tb_marca m ON m.dc_marca=od.dc_marca 
            LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio=od.dc_linea_negocio
            LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto=od.dc_tipo_producto', 'od.dq_monto_marca,ln.dg_linea_negocio,m.dg_marca,tp.dg_tipo_producto,od.dq_cantidad, od.dq_margen', "od.dc_oportunidad={$r->id} AND od.dc_marca=m.dc_marca AND od.dc_linea_negocio=ln.dC_linea_negocio AND od.dm_activo=1", 0, 0, 1);

        return $detalle;
    }

    private function getDetalle($id) {
        $datos = $this->coneccion('select', 'tb_oportunidad_detalle', 'dc_marca,dc_linea_negocio,dc_tipo_producto,dq_monto_marca,dq_cantidad, dq_margen', "dc_oportunidad={$id} AND dm_activo=1", 0, 0, 1);
        return $datos;
    }

    private function Verifica_cantidad($id) {
        $cant = $this->coneccion('select', 'tb_oportunidad_detalle', 'dc_marca,dq_cantidad', "dc_oportunidad={$id} AND dm_activo=1");

        return $this->llena_arr1($cant, 'dc_marca', 'dq_cantidad');
    }

    private function verifica_det() {
        $r = self::getRequest();
        if (!isset($r->item_id[0])) {
            $this->getErrorMan()->showWarning('La oportunidad debe tener almenos Un producto');
            exit;
        }
    }

    private function verificar_mpm($id) {

        $mpm = $this->coneccion('select', 'tb_oportunidad_detalle', 'dc_marca,dc_linea_negocio,dq_monto_marca', "dc_oportunidad={$id} AND dm_activo=1");

        return $this->llena_arr1($mpm, 'dc_marca', 'dc_linea_negocio');
    }

    private function tipo_prod($id) {
        $mpm = $this->coneccion('select', 'tb_oportunidad_detalle', 'dc_marca,dc_tipo_producto', "dc_oportunidad={$id} AND dm_activo=1");

        return $this->llena_arr1($mpm, 'dc_marca', 'dc_tipo_producto');
    }

    private function Valor($id) {
        $mpm = $this->coneccion('select', 'tb_oportunidad_detalle', 'dc_marca,dc_linea_negocio,dq_monto_marca', "dc_oportunidad={$id} AND dm_activo=1");

        return $this->llena_arr1($mpm, 'dc_marca', 'dc_linea_negocio', 'dq_monto_marca');
    }

    private function cliente_txt() {
        $r = self::getRequest();
        $cliente_txt = '';
        if (isset($r->op_other_cli)) {
            $r->op_cliente = 0;
            $cliente_txt = $r->op_cliente_txt;
        }
        return $cliente_txt;
    }

    //==================================================================
    //==================================================================
    //==================================================================

    private function ActualizarOportunidadDetalle() {
        $this->verifica_det();
        $this->verificar_Montos();
        $r = self::getRequest();
        $this->coneccion('update', 'tb_oportunidad_detalle', array('dm_activo' => 0), "dc_oportunidad={$r->op_id}");
        $item = $r->item_id;
        foreach ($item as $c => $v):
            if ($r->dc_marca[$c]):
                $this->coneccion('insert', 'tb_oportunidad_detalle', array(
                    'dc_marca' => '?',
                    'dq_monto_marca' => '?',
                    'dc_linea_negocio' => '?',
                    'dc_oportunidad' => '?',
                    'dq_cantidad' => '?',
                    'dc_tipo_producto' => '?',
                    'dm_activo' => 1,
					'dq_margen' => '?'), 0, array(
                    'a::' . $r->dc_marca[$c] => 'INT',
                    'c::' . $r->valor[$c] * $this->cambio() => 'INT',
                    'b::' . $r->op_linea_negocio[$c] => 'INT',
                    'd::' . $r->op_id => 'INT',
                    'F::' . $r->cantidad[$c] => 'INT',
                    'T::' . $r->op_tipo_producto[$c] => 'INT',
					'M::' . $r->margen[$c] / 100 => 'INT'
                ));
            endif;
        endforeach;
    }

    private function ActualizarOportunidad() {
        $r = self::getRequest();
        $this->coneccion('update', 'tb_oportunidad', array(
            'dg_cliente' => '?',
            'dq_monto' => '?',
            'dg_correo' => '?',
            'dc_ejecutivo' => '?',
            'dc_cliente' => '?',
            'dg_comentario' => '?',
            'dg_nombre_contacto' => '?',
            'dg_fono' => '?',
            'dg_deal_id' => '?'
                ), "dc_oportunidad={$r->op_id}", array(
            'a::' . $this->cliente_txt() => 'STR',
            'e::' . $r->op_monto * $this->cambio() => 'INT',
            'c::' . $r->op_email => 'STR',
            'd::' . $r->op_ejecutivo => 'INT',
            'b::' . $r->op_cliente => 'INT',
            'f::' . $r->op_comentario => 'STR',
            't::' . $r->op_nom_contacto => 'STR',
            'h::' . $r->op_fono_contacto => 'STR',
            'z::' . $r->deal_id_op => 'STR'
        ));
    }

    private function guardaBant() {
        $r = self::getRequest();
        $pr1 = isset($r->sino_1) ? 1 : 0;
        $pr2 = isset($r->sino_2) ? 1 : 0;
        $pr3 = isset($r->sino_3) ? 1 : 0;
        $pr4 = isset($r->sino_4) ? 1 : 0;
        $this->coneccion('update', 'tb_oportunidad', array(
            'dc_pregunta1_bant' => '?',
            'dc_pregunta2_bant' => '?',
            'dc_pregunta3_bant' => '?',
            'dc_pregunta4_bant' => '?',
            'dc_pts_bant' => '?'
                ), "dc_oportunidad={$r->oportunidad_id}", array(
            'z::' . $pr1 => 'INT',
            'r::' . $pr2 => 'INT',
            'y::' . $pr3 => 'INT',
            'x::' . $pr4 => 'INT',
            'a::' . $r->pts => 'INT'
                )
        );
    }

}

?>
