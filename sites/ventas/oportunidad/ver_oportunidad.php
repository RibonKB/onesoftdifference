<?php
	define("MAIN",1);
	include_once("../../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	include("template_oportunidad/modo1.php");
	
	$datosOportunidad = $db->select("(SELECT * FROM tb_oportunidad WHERE dc_empresa={$empresa} AND dc_oportunidad={$_GET['id']}) op
	LEFT JOIN (SELECT * FROM tb_linea_negocio WHERE dc_empresa={$empresa}) l ON op.dc_linea_negocio = l.dc_linea_negocio
	LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) c ON op.dc_cliente = c.dc_cliente
	LEFT JOIN (SELECT * FROM tb_cotizacion WHERE dc_empresa={$empresa}) cot ON op.dc_cotizacion=cot.dc_cotizacion
	LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) e ON op.dc_ejecutivo=e.dc_funcionario
	LEFT JOIN (SELECT * FROM tb_usuario WHERE dc_empresa={$empresa}) u ON op.dc_usuario_creacion=u.dc_usuario
	LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) r ON u.dc_funcionario = r.dc_funcionario",
	'op.dq_oportunidad,dg_razon,dq_cotizacion,dg_comentario,dg_linea_negocio,dq_monto,DATE_FORMAT(df_llamada,"%d/%m/%Y") AS df_llamada,
	dg_correo,CONCAT_WS(" ",e.dg_nombres,e.dg_ap_paterno,e.dg_ap_materno) AS dg_ejecutivo,dm_estado,dg_cliente,
	CONCAT_WS(" ",r.dg_nombres,r.dg_ap_paterno,r.dg_ap_materno) AS dg_responsable,DATE_FORMAT(op.df_fecha_emision,"%d/%m/%Y") AS df_emision');
	
	if(!count($datosOportunidad)){
		$error_man->show_fatal_error('Oportunidad no encontrada. detalles:',
		array(
			'Acceso Denegado'=> 'No tiene permiso de acceso al recurso',
			'No encontrado' => 'La oportunidad no existe'
		));
	}
	
	$datosOportunidad = $datosOportunidad[0];
	
	$pdf = new PDF();
	$pdf->SetBody();
	$pdf->setFoot();
	$pdf->Output();
?>