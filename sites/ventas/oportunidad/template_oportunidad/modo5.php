<?php
require("../../../inc/fpdf.php");

class PDF extends FPDF{

private $datosEmpresa;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"(SELECT * FROM tb_empresa WHERE dc_empresa={$empresa}) e
	JOIN (SELECT * FROM tb_empresa_configuracion WHERE dc_empresa={$empresa}) ec ON e.dc_empresa = ec.dc_empresa
	LEFT JOIN tb_comuna c ON e.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region r ON c.dc_region = r.dc_region",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','mm','Letter');
	
}

function Header(){
	global $datosOportunidad;
	
	$this->SetFont('Arial','',12);
	$this->SetDrawColor(0,100,0);
	$this->SetTextColor(0,100,0);
	$this->SetX(160);
	$this->MultiCell(50,7,"\nOPORTUNIDAD\nN� {$datosOportunidad['dq_oportunidad']}\n\n",1,'C');
	$this->SetY(10);
	$this->SetTextColor(0);
	
	$this->SetFont('','B',12);
	$this->Cell(145,5,$this->datosEmpresa['dg_razon']);
	$this->ln();
	$this->SetFont('Arial','',7);
	$this->SetTextColor(110);
	$this->MultiCell(120,2.4,"{$this->datosEmpresa['dg_giro']}\n{$this->datosEmpresa['dg_direccion']}, {$this->datosEmpresa['dg_comuna']} {$this->datosEmpresa['dg_region']}\n{$this->datosEmpresa['dg_fono']}");
	$this->SetTextColor(0);
	$this->Ln(4);
	
}

function setBody(){
	global $datosOportunidad;
	if($datosOportunidad['dg_cliente'])
		$datosOportunidad['dg_razon'] = $datosOportunidad['dg_cliente'];
	$datosOportunidad['dq_monto'] = moneda_local($datosOportunidad['dq_monto']);
	$this->AddPage();
	$this->SetFont('Arial','',7);
	$this->SetDrawColor(200);
	$this->SetTextColor(50);
	$this->Cell(0,5,'','B');
	$this->Ln();
	$y = $this->GetY();
	$this->MultiCell(26,6,"CLIENTE\nFECHA EMISI�N\nCOTIZACION\nLINEA NEGOCIO\nMONTO ESTIMADO\nCORREO\nEJECUTIVO\nLLAMADA\nRESPONSABLE",'L');
	$this->Cell(0,5,'','T');
	$this->SetY($y);
	$this->SetX(40);
	$this->SetFont('','B',10);
	$this->MultiCell(166,6,
	"{$datosOportunidad['dg_razon']}\n{$datosOportunidad['df_emision']}\n{$datosOportunidad['dq_cotizacion']}\n{$datosOportunidad['dg_linea_negocio']}\n$ {$datosOportunidad['dq_monto']}\n{$datosOportunidad['dg_correo']}\n{$datosOportunidad['dg_ejecutivo']}\n{$datosOportunidad['df_llamada']}\n{$datosOportunidad['dg_responsable']}",
	'R');
	$this->SetFillColor(180);
	$this->Cell(0,6,'Comentario:',1,1,'L',1);
	$this->MultiCell(0,6,$datosOportunidad['dg_comentario'],1);
}

function setFoot(){
}

}

?>