<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("(SELECT * FROM tb_oportunidad WHERE dc_empresa={$empresa} AND dc_oportunidad={$_POST['id']}) op
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa} AND dm_activo='1') cl ON op.dc_cliente = cl.dc_cliente",
"op.dq_oportunidad,cl.dc_cliente");

if(!count($datos)){
	$error_man->showWarning('Error: Oportunidad inválida, intentelo nuevamente.');
	exit();
}
$datos = $datos[0];

echo("<div class='panes center'>");

include_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Header('Indique datos extra para la creación de la cotización');
$form->Start('sites/ventas/proc/cr_cotizacion.php','cr_cotizacion','cValidar');
if($datos['dc_cliente'] == NULL){
	$error_man->showAviso('Atención: la oportunidad posee un cliente ingresado manualmente o inválido<br />
	Para crear la cotización debe seleccionar un cliente validado');
	$form->Listado('Cliente','cli_id','tb_cliente',array('dc_cliente','dg_razon'),1);
}else{
	$form->Hidden('cli_id',$datos['dc_cliente']);
}
echo("<br /><fieldset style='display:inline;padding:0 20px;background:#E5E5E5;'><legend>Utilizar cotización como plantilla (Opcional)</legend>");
$form->Text("Indique el número de la cotización",'cot_base_num');
echo("<br /></fieldset>");
$form->Hidden('op_numero',$datos['dq_oportunidad']);
$form->End('Cotizar','button');

echo("</div>");
?>