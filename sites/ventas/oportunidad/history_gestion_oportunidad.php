<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_oportunidad_avance opa
    LEFT JOIN tb_estados es ON opa.dm_estado_oportunidad=es.dc_estado",
'opa.dg_gestion,opa.dg_contacto,es.dg_estado,opa.dc_avance,dm_nula,
DATE_FORMAT(opa.df_creacion,"%d/%m/%Y %H:%i") as df_creacion,
DATE_FORMAT(opa.df_proxima_actividad,"%d/%m/%Y") as df_estimada,
DATE_FORMAT(opa.df_cierre_actividad,"%d/%m/%Y") as df_cierre,
DATE_FORMAT(opa.df_proyeccion_cierre,"%d/%m/%Y") as df_cierre_estimado',
"opa.dc_oportunidad = {$_POST['id']}",array('order_by'=>'opa.df_creacion DESC'));

echo("<div class='panes' style='width:850px;'>");
if(!count($data)){
	$error_man->showAviso('La cotización especificada no posee actividades en su proceso');
	exit;
}

echo("<table class='tab' width='100%'>
<caption>Últimas actividades realizadas sobre la cotización</caption>
<thead><tr>
	<th>Gestion</th>
	<th width='150'>Contacto</th>
	<th width='120'>Estado Oportunidad</th>
	<th width='90'>Fecha creación</th>
        <th wicth='90'>Proxima Gestion</th>
	<th width='90'>Cierre estimado</th>
	<th width='90'>Fecha cierre</th>
</thead><tbody>");

foreach($data as $d){
	$estado = 'Abierto';
	/*if($d['dm_estado_oportunidad'] == '1'){
        $estado = 'Cerrado';}elseif($d['dm_nula']==1){
            $estado='Lost';
        }*/
	echo("<tr>
			<td>{$d['dg_gestion']}</td>
			<td>{$d['dg_contacto']}</td>
			<td>{$d['dg_estado']}</td>
			<td>{$d['df_creacion']}</td>
			<td>{$d['df_estimada']}</td>
                        <td>{$d['df_cierre_estimado']}</td>
			<td>{$d['df_cierre']}</td>
		</tr>");
}
echo("</tbody></table><hr />");

?>
</div>