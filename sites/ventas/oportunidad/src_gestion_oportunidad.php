<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_oportunidad_avance opd LEFT JOIN tb_estados st ON st.dc_estado=opd.dm_estado_oportunidad",
'opd.dg_gestion,opd.dg_contacto,opd.dm_estado_oportunidad,opd.dc_avance,opd.dm_nula,st.dg_estado,
DATE_FORMAT(opd.df_creacion,"%d/%m/%Y %H:%i") as df_creacion,
DATE_FORMAT(opd.df_proxima_actividad,"%d/%m/%Y") as df_estimada,
DATE_FORMAT(opd.df_cierre_actividad,"%d/%m/%Y") as df_cierre,
DATE_FORMAT(opd.df_proyeccion_cierre,"%d/%m/%Y") as df_cierre_estimado',
"dc_oportunidad = {$_POST['id']}",array('order_by'=>'df_creacion DESC','limit'=>2));

$estados=$db->select('tb_estados','dc_estado,dg_estado');

echo("<div class='panes' style='width:850px;'>");
if(count($data)){

	echo("<table class='tab' width='100%'>
	<caption>Últimas actividades realizadas sobre la oportunidad</caption>
	<thead><tr>
		<th>Gestión</th>
		<th width='150'>Contacto</th>
		<th width='120'>Estado Oportunidad</th>
		<th width='90'>Fecha creación</th>
                <th width='90'>Proxima Gestion</th>
		<th width='90'>Cierre estimado</th>
		<th width='90'>Fecha cierre</th>
	</thead><tbody>");
		foreach($data as $d){
		
               
			echo("<tr>
				<td>{$d['dg_gestion']}</td>
				<td>{$d['dg_contacto']}</td>
                                <td>{$d['dg_estado']}</td>
				<td>{$d['df_creacion']}</td>
				<td>{$d['df_estimada']}</td>
                                <td>{$d['df_cierre_estimado']}</td>
				<td>{$d['df_cierre']}</td>
			</tr>");
		}
	echo("</tbody></table><hr />");

}
$estado='';
foreach ($estados as $e):
    $estado[$e['dc_estado']]=$e['dg_estado'];
endforeach;

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/oportunidad/proc/crear_gestion_oportunidad.php','cr_gestion');
$form->Header('Nueva actividad');
$form->Section();
$form->Text('Contacto','gestion_contacto',1);
$form->Select('Estado oportunidad','gestion_estado',$estado,0,0);
$form->Date('Fecha próxima gestión','gestion_fecha');

//Hora de la gestión
echo("<label>Hora</label>
<select name='gestion_hora' class='inputtext' style='width:50px;'>");
foreach(range(1,12) as $h){
	$h = str_pad($h,2,'0',STR_PAD_LEFT);
	echo("<option value='{$h}'>{$h}</option>");
}
echo("</select> : <select name='gestion_minuto' class='inputtext' style='width:50px;'>");
foreach(range(0,45,15) as $m){
	$m = str_pad($m,2,'0',STR_PAD_LEFT);
	echo("<option value='{$m}'>{$m}</option>");
}
echo("</select> - 
<select name='gestion_meridiano' class='inputtext' style='width:70px;'>
<option value='0'>AM</option>
<option value='12'>PM</option>
</select><br/>");

$form->Date('Fecha de Cierre','fecha_cierre');
$form->EndSection();
$form->Section();
$form->Textarea('Gestión','gestion_gestion',1);

$form->EndSection();
$form->Hidden('op_id',$_POST['id']);
if(count($data))
	$form->Hidden('gestion_avance',$data[0]['dc_avance']);
        
$form->End('Crear','addbtn');
?>
</div>
<script type="text/javascript">
$('#gestion_estado').change(function(){
	$('input[name=gestion_cambio]').val($(this).val());
});
$("#gestion_fecha").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});

$("#fecha_cierre").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});
</script>