<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$op_editable = '';
if($empresa_conf['dm_editar_op_cerradas'] == '0'){
	$op_editable = "AND dm_estado = '0'";
}

$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
if(count($ind_cambio)){
	$ind_cambio = $ind_cambio[0]['dq_cambio'];
}else{
	$ind_cambio = 1;
}

$data = $db->select('tb_oportunidad',"dq_oportunidad,dc_cliente,dg_cliente,dg_comentario,dc_linea_negocio,(dq_monto/{$ind_cambio}) AS dq_monto,dg_correo,dc_ejecutivo",
"dc_oportunidad = {$_POST['id']} {$op_editable}");

if(!count($data)){
	$error_man->showWarning("No se encontró {$lang['op_artic']} {$lang['op_doc']} indicada o está se encuentra en un estado cerrado");
	exit();
}
$data = $data[0];

echo("<div class='secc_bar'>Editar {$lang['op_doc_may']} {$data['dq_oportunidad']}</div>
<div class='panes'>");

require_once('../../../inc/form-class.php');
$form = new Form($empresa);

$form->Start('sites/ventas/oportunidad/proc/editar_oportunidad.php','ed_oportunidad','op_form');
$form->Header("<b>Indique los datos actualizados de {$lang['op_artic']} {$lang['op_doc']}</b><br />Los campos marcados con [*] son obligatorios");
echo('<hr />');
$form->Section();
echo("<fieldset>");

$checked = $data['dg_cliente']?'checked="checked"':'';
$disable = !$data['dg_cliente']?"disabled='disabled'":'';

$form->Listado('Cliente','op_cliente','tb_cliente',array('dc_cliente','dg_razon'),1,$data['dc_cliente']);
echo("<label><input type='checkbox' name='op_other_cli' {$checked}/>Otro</label>
<input type='text' name='op_cliente_txt' {$disable} class='inputtext' value='{$data['dg_cliente']}' size='30' /></fieldset><br />");
$form->Listado('Linea de negocio','op_linea_negocio','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio'),1,$data['dc_linea_negocio']);
$form->Text("Monto aproximado (USD)",'op_monto',1,255,$data['dq_monto']);

$form->EndSection();
$form->Section();
$form->Textarea("Comentario",'op_comentario',1,$data['dg_comentario']);
$form->Text("E-Mail de contacto",'op_email',1,255,$data['dg_correo']);
$form->Listado('Ejecutivo','op_ejecutivo',"tb_funcionario",array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'),0,$data['dc_ejecutivo']);
$form->EndSection();
$form->Hidden('op_id',$_POST['id']);
$form->End('Editar','editbtn');

?>
</div>
<script type="text/javascript">
$('input[name=op_other_cli]').click(function(){
	if($(this).attr('checked')){
		$(this).parent().next().attr('disabled',false).attr('required',true);
		$('#op_cliente').attr('required',false);
	}else{
		$(this).parent().next().attr('disabled',true).attr('required',false);
		$('#op_cliente').attr('required',true);
	}
});

$('.op_form').submit(function(e){
	e.preventDefault();
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/.test($('#op_email').val()))){
		$('#op_email').addClass('invalid');
		$('#op_email').focus();
		return;
	}
	monto = parseFloat($('#op_monto').val());
	if(!monto){
		$('#op_monto').addClass('invalid');
		$('#op_monto').focus();
		return;
	}
	$('#op_monto').val(monto);
	$('#op_email,#op_monto').removeClass('invalid');
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
	
});
</script>