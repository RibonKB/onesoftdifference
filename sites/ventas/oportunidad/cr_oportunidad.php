<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Crear {$lang['op_doc_may']}</div>
<div id='main_cont'><div class='panes'>");

require_once('../../../inc/form-class.php');
$form = new Form($empresa);

$form->Start('sites/ventas/oportunidad/proc/crear_oportunidad.php','cr_oportunidad','op_form');
$form->Header("<b>Indique los datos para crear {$lang['op_artic']} {$lang['op_doc']}</b><br />Los campos marcados con [*] son obligatorios");
echo('<hr />');
$form->Section();
echo("<fieldset>");
$form->Listado('Cliente','op_cliente','tb_cliente',array('dc_cliente','dg_razon'),1);
echo("<label><input type='checkbox' name='op_other_cli'/>Otro</label>
<input type='text' name='op_cliente_txt' disabled='disabled' class='inputtext' size='30' /></fieldset><br />");
$form->Listado('Linea de negocio','op_linea_negocio','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio'),1);
$form->Text("Monto aproximado (USD)",'op_monto',1);

$form->EndSection();

$form->Section();
  $form->Textarea("Comentario",'op_comentario',1);
  $form->Text("E-Mail de contacto",'op_email',1);
  $form->Listado('Ejecutivo','op_ejecutivo','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),1,0,"tf.dc_empresa = {$empresa} AND dc_modulo = 1");
  
$form->EndSection();

$form->Section();
  $form->Listado('Marca', 'dc_marca', 'tb_marca', array('dc_marca','dg_marca'));

  $form->Date('Llamar día','op_llamada');

  //Hora de la gestión
  echo("<label>Hora</label>
  <select name='gestion_hora' class='inputtext' style='width:50px;'>");
  foreach(range(1,12) as $h){
      $h = str_pad($h,2,'0',STR_PAD_LEFT);
      echo("<option value='{$h}'>{$h}</option>");
  }
  echo("</select> : <select name='gestion_minuto' class='inputtext' style='width:50px;'>");
  foreach(range(0,45,15) as $m){
      $m = str_pad($m,2,'0',STR_PAD_LEFT);
      echo("<option value='{$m}'>{$m}</option>");
  }
  echo("</select> - 
  <select name='gestion_meridiano' class='inputtext' style='width:70px;'>
  <option value='0'>AM</option>
  <option value='12'>PM</option>
  </select>");
$form->EndSection();

$form->End('Crear','addbtn');

?>
</div></div>
<script type="text/javascript">
$('input[name=op_other_cli]').click(function(){
	if($(this).attr('checked')){
		$(this).parent().next().attr('disabled',false).attr('required',true);
		$('#op_cliente').attr('required',false);
	}else{
		$(this).parent().next().attr('disabled',true).attr('required',false);
		$('#op_cliente').attr('required',true);
	}
});

$('.op_form').submit(function(e){
	e.preventDefault();
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w+)+$/.test($('#op_email').val()))){
		$('#op_email').addClass('invalid');
		$('#op_email').focus();
		return;
	}
	monto = parseFloat($('#op_monto').val());
	if(!monto){
		$('#op_monto').addClass('invalid');
		$('#op_monto').focus();
		return;
	}
	$('#op_monto').val(monto);
	$('#op_email,#op_monto').removeClass('invalid');
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
	
});
</script>