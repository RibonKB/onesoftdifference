<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GestionarOportunidadFactory
 *
 * @author Eduardo
 */
class GestionarOportunidadFactory extends Factory {

    //put your code here

    public function indexAction() {
        $r = self::getRequest();
	$r->op_id = $r->id;
        $mostrar = $this->getFormView($this->getTemplateURL('gestion_form'), array(
            'id' => $r->id,
            'data' => $this->getGestion(),
            'estados' => $this->getEstados(),
            'aviso'=>$this->verifica_aviso(),
            'tarea'=>$this->verifica_tarea()
                )
        );
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function GestionarAction() {
        $r = self::getRequest();
        $this->verifica_estado();
        $this->verifica_ingreso_fecha($r->gestion_fecha);
        $this->verifica_ingreso_fecha($r->fecha_cierre, 1);
        $t=$this->verifica_tarea();
        if($t == false){
            
        }
        $this->actualiza_oportunidad();
        $this->actualiza_avance();
        $this->getErrorMan()->showConfirm("Se ha generado una nueva actividad sobre la Oportunidad");
    }

    private function getGestion() {
        $r = self::getRequest();
        $data = $this->coneccion('select', 'tb_oportunidad_avance opd LEFT JOIN tb_estados st ON st.dc_estado=opd.dm_estado_oportunidad', 'opd.dg_gestion,opd.dg_contacto,opd.dm_estado_oportunidad,opd.dc_avance,opd.dm_nula,st.dg_estado,
                    DATE_FORMAT(opd.df_creacion,"%d/%m/%Y %H:%i") as df_creacion,
                    DATE_FORMAT(opd.df_proxima_actividad,"%d/%m/%Y") as df_estimada,
                    DATE_FORMAT(opd.df_cierre_actividad,"%d/%m/%Y") as df_cierre,
                    DATE_FORMAT(opd.df_proyeccion_cierre,"%d/%m/%Y") as df_cierre_estimado', "dc_oportunidad = {$r->id}", NULL, NULL, NULL, array(
            'order_by' => 'opd.df_creacion DESC',
            'limit' => 2
                )
        );
        $data = $data->fetchAll(PDO::FETCH_OBJ);
        return $data;
    }

    private function getEstados() {
        $estados = $this->coneccion('select', 'tb_estados', 'dc_estado,dg_estado');
        $estados = $estados->fetchAll(PDO::FETCH_OBJ);
        return $estados;
    }

    private function requiere() {
        $r = self::getRequest();
        $requiere = $this->coneccion('select', 'tb_estados', 'dm_requiere', "dc_estado={$r->gestion_estado}");
        $requiere = $requiere->fetch(PDO::FETCH_OBJ);
        return $requiere->dm_requiere;
    }

    private function verifica_aviso(){
        $r=self::getRequest();
        $t=$this->getService('AdministradorTarea');
        $o=$t->Verificar_aviso($this->getUserData()->dc_funcionario,'1,2',"'dc_oportunidad'","'".$r->id."'");
        $k='no';
        if($o == true){
            $k='si';
        }
        return $k;
    }
    
    private function verifica_tarea(){
        $r=self::getRequest();
        $t=$this->getService('AdministradorTarea');
        $m=$t->verifica_tarea_Existe(array('dc_oportunidad'=>$r->op_id),'1,2');
        return $m;
    }
    
    private function verifica_ingreso_fecha($fecha = NULL, $t = 0) {
        $r = self::getRequest();
        $tipo = 'fecha para la próxima actividad';
        if ($t) {
            $tipo = 'fecha estimada de cierre';
        }
        if (!$fecha) {
            if ($r->gestion_estado == 0 || $this->requiere() == 1) {
                $this->getErrorMan()->showWarning("Si no indica una $tipo debe seleccionar un estado de Oportunidad que cierre el proceso.");
                exit;
            }
        }
    }

    private function actualiza_oportunidad() {
        $r = self::getRequest();
        $req = $this->requiere();
        $db = $this->getConnection();
        $funcionario = $this->getEjecutivoOp($r->op_id);
        $dq = $this->getDq_op($r->op_id);
        $tarea = $this->getService('AdministradorTarea');
        $this->coneccion('update', 'tb_oportunidad', array(
            "dm_estado" => $r->gestion_estado
                ), "dc_oportunidad = {$r->op_id}");

        if ($req == 1) {
            $this->coneccion('update', 'tb_oportunidad', array(
                'df_proyeccion_cierre' => "'".$this->Fecha_proyeccion()."'"
                    ), "dc_oportunidad={$r->op_id}");
                    $tar=$this->verifica_tarea();
                    if($tar != false){
                        $tarea->Actualiza_tarea(array('dc_oportunidad' => $r->op_id), 1);
                    }
            $tarea->Crear_tarea($funcionario, 1, $this->Fecha_proyeccion(), array('dc_oportunidad' => $r->op_id), $dq, 1);
           
        }
    }

    private function Fecha_proyeccion() {
        $r = self::getRequest();
        $h = $r->gestion_hora;
        $m = $r->gestion_minuto;
        $Me = $r->gestion_meridiano;

        if ($r->gestion_fecha) {
            if ($Me == 12 && $h == 12) {
                $hora = "00:" . $m . ":00";
            } else {
                $hora = str_pad($h + $Me, 2, '0', STR_PAD_LEFT) . ":" . $m . ":00";
            }
        }
        $f = explode("/", $r->fecha_cierre);
        $fe = $f[2] . "-" . $f[1] . "-" . $f[0] . " " . $hora;
        return $fe;
    }

    private function getDq_op($id_op) {
        $dq = $this->coneccion('select', 'tb_oportunidad', 'dq_oportunidad', "dc_oportunidad={$id_op}");
        $dq = $dq->fetch(PDO::FETCH_OBJ);
        return $dq->dq_oportunidad;
    }

    private function getEjecutivoOp($id_op) {
        $f = $this->coneccion('select', 'tb_oportunidad', 'dc_ejecutivo', "dc_oportunidad={$id_op}");
        $f = $f->fetch(PDO::FETCH_OBJ);
        return $f->dc_ejecutivo;
    }

    private function Hora_fecha_llamar() {
        $r = self::getRequest();
        $h = $r->gestion_hora;
        $m = $r->gestion_minuto;
        $Me = $r->gestion_meridiano;

        if ($r->gestion_fecha) {
            if ($Me == 12 && $h == 12) {
                $hora = "00:" . $m . ":00";
            } else {
                $hora = str_pad($h + $Me, 2, '0', STR_PAD_LEFT) . ":" . $m . ":00";
            }
            $f = explode("/", $r->gestion_fecha);
            $fe = $f[2] . "-" . $f[1] . "-" . $f[0] . " " . $hora;
        }
        return $fe;
    }

    private function actualiza_avance() {
        $r = self::getRequest();
        $req = $this->requiere();
        $db = $this->getConnection();
        $funcionario = $this->getEjecutivoOp($r->op_id);
        $dq = $this->getDq_op($r->op_id);
        $tarea = $this->getService('AdministradorTarea');
        if ($req == 0) {
            $this->coneccion('update', 'tb_oportunidad_avance', array(
                "dm_estado_actividad" => 0,
                "df_cierre_actividad" => 'NOW()'
                    ), "dc_avance = {$r->gestion_avance}");
                    $t2014=$this->verifica_tarea();
                    if($t2014 == 'si'){
                        $tarea->Actualiza_tarea(array('dc_oportunidad' => $r->op_id), 2);
            $tarea->Actualiza_tarea(array('dc_oportunidad' => $r->op_id), 1);
                    }
            
        }
	$r = self::getRequest();
        $arr=array(
            "dc_oportunidad" => $r->op_id,
            "dg_gestion" => "'" . $r->gestion_gestion . "'",
            "df_creacion" => 'NOW()',
            "dm_estado_oportunidad" => $r->gestion_estado,
            "dm_estado_actividad" => 1,
            "dg_contacto" => "'" . $r->gestion_contacto . "'"
        );
        if($req == 1 ){
                $arr=array(
            "dc_oportunidad" => $r->op_id,
            "dg_gestion" => "'" . $r->gestion_gestion . "'",
            "df_creacion" => 'NOW()',
            "df_proxima_actividad"=>"'".$this->Hora_fecha_llamar()."'",
            "df_proyeccion_cierre"=>"'".$this->Fecha_proyeccion()."'",
            "dm_estado_oportunidad" => $r->gestion_estado,
            "dm_estado_actividad" => 1,
            "dg_contacto" => "'" . $r->gestion_contacto . "'"
        );
        }
         if(isset($r->aviso_op)){
                $tarea->Usuario_TB_Inserta(array('dc_oportunidad'=>$r->op_id),$this->getUserData()->dc_funcionario,1);
                $tarea->Usuario_TB_Inserta(array('dc_oportunidad'=>$r->op_id),$this->getUserData()->dc_funcionario,2);
            }
        $this->coneccion('insert', 'tb_oportunidad_avance', $arr);
    }

    private function verifica_estado() {
        $r = self::getRequest();
        if ($r->gestion_estado == 0) {
            $this->getErrorMan()->showWarning("debe seleccionar un estado de Oportunidad.");
            exit;
        }
    }

}

?>
