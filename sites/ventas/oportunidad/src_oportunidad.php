<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Oportunidades</div>
<div id='main_cont'><br /><br /><div class='panes'>");

include_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/oportunidad/proc/src_oportunidad.php','src_oportunidad');
$form->Header("<strong>Indicar los parámetros de búsqueda de oportunidades</strong>");

	echo('<table class="tab" style="text-align:left;" width="100%" id="form_container"><tr><td>Número de oportunidad</td><td>');
	$form->Text("Desde","op_numero_desde");
	echo('</td><td>');
	$form->Text('Hasta','op_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','op_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','op_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','op_client','tb_cliente',array('dc_cliente','dg_razon'));
	echo('</td><td>&nbsp;</td></tr><tr><td>Ejecutivo</td><td>');
	
	$form->ListadoMultiple('','op_executive','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 1 AND dm_activo = 1");
	
	
	echo('</td><td>&nbsp;</td></tr><tr><td>Linea de negocio</td><td>');
	$form->ListadoMultiple('','op_buss_line','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio'));
    
    echo('</td><td>&nbsp;</td></tr><tr><td>Marca</td><td>');
	$form->ListadoMultiple('','dc_marca','tb_marca',array('dc_marca','dg_marca'));
    
	echo('</td><td>&nbsp;</td></tr><tr><td colspan="3" align="center">');
	$form->Combobox('Incluir','op_status',array('Oportunidades abiertas','Oportunidades Cerradas'),array(0,1)," ");
	echo('</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');


?>
</div></div>
<script type="text/javascript">
$('#op_client,#op_executive,#op_buss_line,#dc_marca').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});

$(':checkbox[name="op_status[]"]').click(function(e){
	if($(':checked[name="op_status[]"]').size() < 1){
		e.preventDefault();
	}
});
</script>