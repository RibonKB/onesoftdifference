<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CrearOportunidadFactory
 *
 * @author Eduardo
 */
require_once('sites/ventas/enviaM.php');

class CrearOportunidadFactory extends Factory {

    public $title = 'Crear Oportunidad';
    private $op;

    public function indexAction() {
        $mostrar = $this->getFormView($this->getTemplateURL('cr_oportunidad'), array('minuto' => $this->Mostrar_minutos(),
            'hora' => $this->Mostrar_hora(),
            'cliente' => $this->Mostrar_clientes(),
            'linea_negocio' => $this->Mostrar_linea_negocio(),
            'funcionario' => $this->Mostrar_funcionario(),
            'marca' => $this->Mostrar_marca(),
            'marcaII' => $this->Mostrar_marcaII(),
            'tipo_producto' => $this->Mostrar_tipo_producto(),
            'tv' => $this->getUserData()->dc_funcionario,
            'tele_venta' => $this->Mostrar_funcionario()
        ));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }

    public function grabarAction() {
        $this->Guardar_oportunidad();
        $this->Guardar_detalle();
        $this->Guardar_oportunidad_avance();
        $this->EnviarMail();
        $this->getErrorMan()->showConfirm("Oportunidad generada correctamente");
        echo $this->getFormView($this->getTemplateURL('op_lista'), array('numero' => $this->op));
    }

    //========================================================================
    //========================================================================
    //========================================================================



    private function E() {
        return $this->getParametrosEmpresa()->dc_correlativo_oportunidad;
    }

    private function F() {
        return $this->getParametrosEmpresa()->dn_oportunidad_dias_actividad;
    }

    private function G() {
        return $this->getParametrosEmpresa()->dc_indicador_tipo_cambio;
    }

    /**
     * 
     * para array simple $arr1[dato1]=dato2;
     * 
     * @param type $consulta
     * @param type $campos_retorno
     * @return type
     * 
     */
    private function llena_arr1($consulta, $campo1, $campo2) {
        $arr1 = '';
        while ($llena = $consulta->fetch(PDO::FETCH_OBJ)) {
            $arr1[$llena->$campo1] = $llena->$campo2;
        }
        return $arr1;
    }

    //========================================================================
    //========================================================================
    //========================LOS DATOS QUE SE MOSTRARÁN======================
    //========================================================================
    //========================================================================

    private function Mostrar_minutos() {
        $min = '';
        foreach (range(0, 45, 15) as $m) {
            $m = str_pad($m, 2, '0', STR_PAD_LEFT);
            $min[$m] = $m;
        }
        return $min;
    }

    private function Mostrar_hora() {
        $hora = '';
        foreach (range(1, 12) as $h) {
            $h = str_pad($h, 2, '0', STR_PAD_LEFT);
            $hora[$h] = $h;
        }
        return $hora;
    }

    private function Mostrar_clientes() {
        $cliente = $this->coneccion('select', 'tb_cliente', 'dc_cliente,dg_razon', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_razon");
        return $this->llena_arr1($cliente, 'dc_cliente', 'dg_razon');
    }

    private function Mostrar_linea_negocio() {
        $linea = $this->coneccion('select', 'tb_linea_negocio', 'dc_linea_negocio,dg_linea_negocio', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_linea_negocio");
        return $this->llena_arr1($linea, 'dc_linea_negocio', 'dg_linea_negocio');
    }

    private function Mostrar_funcionario() {
        $funcionario = $this->coneccion('select', 'tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco', 'tf.dc_funcionario,CONCAT(tf.dg_nombres," ",tf.dg_ap_paterno," ",tf.dg_ap_materno) AS Nombre_funcionario', "tf.dc_empresa = {$this->getEmpresa()} AND tc.dc_modulo = 1 ORDER BY tf.dg_nombres");
        return $this->llena_arr1($funcionario, 'dc_funcionario', 'Nombre_funcionario');
    }

    private function Mostrar_marca() {
        $marca = $this->coneccion('select', 'tb_marca', 'dc_marca,dg_marca', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_marca");
        return $this->llena_arr1($marca, 'dc_marca', 'dg_marca');
    }

    private function Mostrar_marcaII() {
        $marca = $this->coneccion('select', 'tb_marca', 'dc_marca,dg_marca', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dc_marca");
        return $this->llena_arr1($marca, 'dc_marca', 'dg_marca');
    }

    private function Mostrar_tipo_producto() {
        $tipo_producto = $this->coneccion('select', 'tb_tipo_producto', 'dc_tipo_producto,dg_tipo_producto', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_tipo_producto");
        return $this->llena_arr1($tipo_producto, 'dc_tipo_producto', 'dg_tipo_producto');
    }

    //========================================================================
    //========================================================================
    //========================================================================

    private function Numero_oportunidad() {
        $cot_prefix = "";
        $largo_prefix = 0;
        switch ($this->getParametrosEmpresa()->dc_correlativo_oportunidad) {
            case '1':$cot_prefix = date("Y");
                $largo_prefix = 4;
                break;
            case '2':$cot_prefix = date("Ym");
                $largo_prefix = 6;
                break;
        }
        return $cot_prefix;
    }

    private function Numero_oportunidad2() {
        $cot_prefix = "";
        $largo_prefix = 0;
        switch ($this->getParametrosEmpresa()->dc_correlativo_oportunidad) {
            case '1':$cot_prefix = date("Y");
                $largo_prefix = 4;
                break;
            case '2':$cot_prefix = date("Ym");
                $largo_prefix = 6;
                break;
        }
        return $largo_prefix;
    }

    private function cliente_txt() {
        $r = self::getRequest();
        $cliente_txt = '';
        if (isset($r->op_other_cli)) {
            $r->op_cliente = 0;
            $v=  trim($r->op_cliente_txt);
            if($v != ''){
                $cliente_txt = $r->op_cliente_txt;
            }else{
                $this->getErrorMan()->showWarning('Debe indicar Cliente');
                exit;
            }
            
        }
        return $cliente_txt;
    }

    private function last_date() {
        $cot_num = '';
        $last_date = $this->coneccion('select', 'tb_oportunidad', 'MAX(df_fecha_emision) fecha', "dc_empresa={$this->getEmpresa()}");
        $last_date = $last_date->fetch(PDO::FETCH_OBJ);
        if ($last_date->fecha != NULL) {
            $last = $this->coneccion('select', 'tb_oportunidad', 'dq_oportunidad', "dc_empresa={$this->getEmpresa()} AND df_fecha_emision = '{$last_date->fecha}'");
            $last = $last->fetch(PDO::FETCH_OBJ);
            if (substr($last->dq_oportunidad, 0, $this->Numero_oportunidad2()) == $this->Numero_oportunidad() || $this->E() == '3') {
                $cot_num = substr($last->dq_oportunidad, $this->Numero_oportunidad2()) + 1;
            } else {
                $cot_num = $this->E();
            }
        } else {
            $cot_num = $this->E();
        }
        return $cot_num;
    }

    private function cambio() {
        $ind_cambio = $this->coneccion('select', 'tb_tipo_cambio', 'dq_cambio', "dc_tipo_cambio={$this->G()}");
        $ind_cambio = $ind_cambio->fetch(PDO::FETCH_OBJ);
        if (count($ind_cambio->dq_cambio)) {
            $ind_cambio = $ind_cambio->dq_cambio;
        } else {
            $ind_cambio = 1;
        }
        return $ind_cambio;
    }

    private function Hora_fecha_llamar() {
        $r = self::getRequest();
        $h = $r->hora;
        $m = $r->minuto;
        $Me = $r->meridiano;

        if ($r->op_llamada) {
            if ($Me == 12 && $h==12) {
                $hora = "00:" . $m . ":00";
            } else {
                $hora = str_pad($h + $Me, 2, '0', STR_PAD_LEFT) . ":" . $m . ":00";
            }
            $f = explode("/", $r->op_llamada);
            $fe = $f[2] . "-" . $f[1] . "-" . $f[0];
        }
        return $fe . " " . $hora;
    }

    private function verifica_MtVsM() {
        $r = self::getRequest();
        $monto_total_1 = $r->op_monto;
        $monto_total_2 = 0;
        $item = '';
        if (isset($r->item_id)):
            $item = $r->item_id;
        else:
            $this->getErrorMan()->showWarning("Debe agregar por lo menos 1 producto a la oportunidad");
                exit();
        endif;

        foreach ($item as $c => $v):
            if ($r->dc_marca[$c]) {
                $monto_total_2+=$r->valor[$c];
            }
        endforeach;
        if ($monto_total_1 != $monto_total_2) {
            if ($monto_total_2 != 0) {
                $this->getErrorMan()->showWarning("El total del M.P.M debe ser igual al Monto aproximado");
                exit();
            }
        }
    }

    private function Fecha_proyeccion() {
        $r = self::getRequest();
        $h = $r->hora;
        $m = $r->minuto;
        $Me = $r->meridiano;

        if ($r->op_llamada) {
            if ($Me == 12 && $h==12) {
                $hora = "00:" . $m . ":00";
            } else {
                $hora = str_pad($h + $Me, 2, '0', STR_PAD_LEFT) . ":" . $m . ":00";
            }
        }
        $f = explode("/", $r->op_proyeccion);
        $fe = $f[2] . "-" . $f[1] . "-" . $f[0] . " ". $hora;
        return $fe;
    }

    //========================================================================
    //========================================================================
    //========================================================================

    private function Guardar_oportunidad() {
        $this->verifica_MtVsM();
        $r = self::getRequest();
        $db = $this->getConnection();
        /*
         * dc_marca y dc_linea_negocio se deben eliminar de la tabla
         * tb_oportunidad y pasan a la tabla
         * tb_oportunidad_detalle.
         * 
         * Se debe agregar el campo df_proyeccion_cierre
         */
        $pr1 = isset($r->sino_1) ? 1 : 0;
        $pr2 = isset($r->sino_2) ? 1 : 0;
        $pr3 = isset($r->sino_3) ? 1 : 0;
        $pr4 = isset($r->sino_4) ? 1 : 0;
        $this->op = $this->Numero_oportunidad() . str_pad($this->last_date(), 4, '0', STR_PAD_LEFT);
        $fecha_llamar=$this->Hora_fecha_llamar();
        $lop = $this->coneccion('insert', 'tb_oportunidad', array(
            'dq_oportunidad' => '?',
            'dg_cliente' => '?',
            'dc_cliente' => '?',
            'dg_comentario' => '?',
            'dq_monto' => '?',
            'df_llamada' => '?',
            'dg_correo' => '?',
            'dc_ejecutivo' => '?',
            'dc_empresa' => '?',
            'dc_usuario_creacion' => '?',
            'dg_fono' => '?',
            'dg_nombre_contacto' => '?',
            'df_proyeccion_cierre' => '?',
            'dc_pts_bant' => '?',
            'df_fecha_emision' => 'NOW()',
            'dg_deal_id' => '?',
            'dc_pregunta1_bant' => '?',
            'dc_pregunta2_bant' => '?',
            'dc_pregunta3_bant' => '?',
            'dc_pregunta4_bant' => '?',
            'dc_tele_venta' => '?',
            'dm_estado' => 1
                ), 0, array(
            'a::' . $this->op => 'INT',
            'b::' . $this->cliente_txt() => 'STR',
            'c::' . $r->op_cliente => 'INT',
            'd::' . $r->op_comentario => 'STR',
            'f::' . $r->op_monto * $this->cambio() => 'INT',
            'g::' . $fecha_llamar => 'STR',
            'h::' . trim($r->op_email) => 'STR',
            'i::' . $r->op_ejecutivo => 'INT',
            'k::' . $this->getEmpresa() => 'INT',
            'l::' . $this->getUserData()->dc_usuario => 'INT',
            'm::' . $r->op_contacto_fono => 'STR',
            'n::' . $r->op_nombre_contacto => 'STR',
            'o::' . $this->Fecha_proyeccion() => 'STR',
            'p::' . $r->pts => 'INT',
            'q::' . $r->deal_id_op => 'STR',
            's::' . $pr1 => 'INT',
            'r::' . $pr2 => 'INT',
            'y::' . $pr3 => 'INT',
            'x::' . $pr4 => 'INT',
            'z::' . $r->op_tele_venta => 'INT'
                )
        );
        $r->dc_op = $db->lastInsertId('dc_oportunidad');
        $tarea=$this->getService('AdministradorTarea');
        $tarea->Crear_tarea($r->op_ejecutivo,1, $fecha_llamar,array('dc_oportunidad'=>$r->dc_op),$this->op);
        $tarea->Crear_tarea($r->op_ejecutivo,2, $this->Fecha_proyeccion(),array('dc_oportunidad'=>$r->dc_op),$this->op);
    }
    private function Guardar_oportunidad_avance() {
        $r = self::getRequest();
        $db = $this->getConnection();
        $prox_actividad = '';
        if ($r->op_llamada) {
            $prox_actividad = $this->Hora_fecha_llamar();
        } else {
            $prox_actividad = "ADDDATE(NOW(),{$this->F()})";
        }
        /**
         * Se deben agregar los campos dg_fono_contacto y dg_nombre_contacto
         */
        $this->coneccion('insert', 'tb_oportunidad_avance', array(
            'dc_oportunidad' => '?',
            'dg_contacto' => '?',
            'dg_gestion' => '?',
            'df_creacion' => 'NOW()',
            'df_proxima_actividad' => '?',
            'dm_estado_oportunidad' => 0
                ), 0, array(
            'a::' . $r->dc_op => 'INT',
            's::' . $r->op_email => 'STR',
            'd::' . 'Creacion' => 'STR',
            'f::' . $prox_actividad => 'STR'
        ));
    }
	
	//Se modifico para que permita almacenar también el margen.
    private function Guardar_detalle() {
        $r = self::getRequest();
        $item = $r->item_id;
        foreach ($item as $c => $v):
            if ($r->dc_marca[$c]) {
                $this->coneccion('insert', 'tb_oportunidad_detalle', array(
                    'dc_marca' => '?',
                    'dc_linea_negocio' => '?',
                    'dc_tipo_producto' => '?',
                    'dq_monto_marca' => '?',
                    'dc_oportunidad' => '?',
                    'dq_cantidad' => '?',
                    'dm_activo' => 1,
					'dq_margen' => '?'
                        ), 0, array(
                    'j::' . $r->dc_marca[$c] => 'INT',
                    'e::' . $r->op_linea_negocio[$c] => 'INT',
                    'g::' . $r->op_tipo_producto[$c] => 'INT',
                    'O::' . $r->valor[$c] * $this->cambio() => 'INT',
                    'R::' . $r->dc_op => 'INT',
                    'T::' . $r->cantidad[$c] => 'INT',
					'M::' . $r->margen[$c] / 100 => 'INT'
                ));
            }
        endforeach;
    }
    
    private function EnviarMail(){
        $r=self::getRequest();
        $e = new enviaM();
        $e->Enviar_mail($this->funcionario_Mail($r->op_ejecutivo), $e->subjet($this->op), $e->getMensaje($this->Ident_func($r->op_tele_venta), $this->Ident_func($r->op_ejecutivo), $this->op));
    }
    
    private function funcionario_Mail($funcionario){
        $select=$this->coneccion('select','tb_funcionario','dg_email',"dc_funcionario={$funcionario}");
        $select=$select->fetch(PDO::FETCH_OBJ);
        return $select->dg_email;
    }
    
    private function Ident_func($id){
        $select=$this->coneccion('select','tb_funcionario','CONCAT(dg_nombres," ",dg_ap_paterno," ",dg_ap_materno) nombre',"dc_funcionario={$id}");
        $select=$select->fetch(PDO::FETCH_OBJ);
        return $select->nombre;
    }

}

?>
