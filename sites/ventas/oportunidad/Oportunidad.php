<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Oportunidad
 *
 * @author Eduardo
 */
class Oportunidad extends Factory{

    
    public function Correlativo_op() {
        return $this->getParametrosEmpresa()->dc_correlativo_oportunidad;
    }

    public function Dias_actividadOp() {
        return $this->getParametrosEmpresa()->dn_oportunidad_dias_actividad;
    }
    
    public function Mostrar_Marcas() {
        $marcas = $this->coneccion('select', 'tb_marca', 'dg_marca,dc_marca', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_marca");
        return $this->llena_arr1($marcas, 'dc_marca', 'dg_marca');
    }

    public function Mostrar_Ejecutivo() {
        $funcionario = $this->coneccion('select', 'tb_funcionario fu 
                    LEFT JOIN tb_cargo_funcionario cf 
                    ON cf.dc_modulo = 1', 'dc_funcionario,
                    CONCAT(dg_nombres," ",dg_ap_paterno," ",dg_ap_materno) as Nombre_func', "fu.dc_empresa={$this->getEmpresa()} 
                    AND fu.dm_activo=1  
                    ORDER BY dg_nombres");
        return $this->llena_arr1($funcionario, 'dc_funcionario', 'Nombre_func');
    }

    public function Mostrar_LineaNegocio() {
        $ln = $this->coneccion('select', 'tb_linea_negocio', 'dc_linea_negocio,dg_linea_negocio', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_linea_negocio");
       return $this->llena_arr1($ln, 'dc_linea_negocio', 'dg_linea_negocio');
    }

    public function Mostrar_tipo_producto() {
        $ln = $this->coneccion('select', 'tb_tipo_producto', 'dc_tipo_producto,dg_tipo_producto', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_tipo_producto");
        return $this->llena_arr1($ln, 'dc_tipo_producto', 'dg_tipo_producto');
    }

    public function Mostrar_Cliente() {
        $arr1 = '';
        $cl = $this->coneccion('select', 'tb_cliente', 'dc_cliente,dg_razon', "dc_empresa={$this->getEmpresa()} AND dm_activo=1 ORDER BY dg_razon");
        return $this->llena_arr1($cl, 'dc_cliente', 'dg_razon');;
    }

    public function Mostrar_Cliente_Otro() {
        $cl = $this->coneccion('select', 'tb_oportunidad', 'dg_cliente', "dc_empresa={$this->getEmpresa()} AND dg_cliente <> '' ORDER BY dg_cliente");
        return $this->llena_arr1($cl, 'dg_cliente', 'dg_cliente');
    }
    
    public function get_Estados() {
        $estados = $this->coneccion('select', 'tb_estados', '*');
        return $this->llena_arr1($estados, 'dc_estado', 'dg_estado');
    }
    
    public function cliente_txt() {
        $r = self::getRequest();
        $cliente_txt = ' ';
        if (isset($r->op_other_cli)) {
            $r->op_cliente = 0;
            $cliente_txt = $r->op_cliente_txt;
        }
        return $cliente_txt;
    }
    
     public function cambio() {
        $ind_cambio = $this->coneccion('select', 'tb_tipo_cambio', 'dq_cambio', "dc_tipo_cambio={$this->Correlativo_op()}");
        $ind_cambio = $ind_cambio->fetch(PDO::FETCH_OBJ);
        if (count($ind_cambio->dq_cambio)) {
            $ind_cambio = $ind_cambio->dq_cambio;
        } else {
            $ind_cambio = 1;
        }
        return $ind_cambio;
    }
    
    /**
     * 
     * para array simple $arr1[dato1]=dato2;
     * 
     * @param type $consulta
     * @param type $campos_retorno
     * @return type
     * 
     */
    private function llena_arr1($consulta, $campo1, $campo2, $campo3 = null) {
        while ($llena = $consulta->fetch(PDO::FETCH_OBJ)) {
            if ($campo3) {
                $arr1[$llena->$campo1][$llena->$campo2] = $llena->$campo3;
            } else {
                $arr1[$llena->$campo1] = $llena->$campo2;
            }
        }
        return $arr1;
    }
    
}

?>
