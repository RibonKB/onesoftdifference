<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_oportunidad WHERE dc_empresa={$empresa} AND dc_oportunidad={$_POST['id']}) op
LEFT JOIN (SELECT * FROM tb_linea_negocio WHERE dc_empresa={$empresa}) l ON op.dc_linea_negocio = l.dc_linea_negocio
LEFT JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) c ON op.dc_cliente = c.dc_cliente
LEFT JOIN (SELECT * FROM tb_cotizacion WHERE dc_empresa={$empresa}) cot ON op.dc_cotizacion=cot.dc_cotizacion
LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) e ON op.dc_ejecutivo=e.dc_funcionario
LEFT JOIN (SELECT * FROM tb_usuario WHERE dc_empresa={$empresa}) u ON op.dc_usuario_creacion=u.dc_usuario
LEFT JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa}) r ON u.dc_funcionario = r.dc_funcionario
LEFT JOIN tb_marca m ON m.dc_marca = op.dc_marca
LEFT JOIN tb_estados st ON st.dc_estado = op.dm_estado",
'op.dq_oportunidad,dg_razon,dq_cotizacion,dg_comentario,dg_linea_negocio,dq_monto,DATE_FORMAT(df_llamada,"%d/%m/%Y %I:%i %p") AS df_llamada,
dg_correo,CONCAT_WS(" ",e.dg_nombres,e.dg_ap_paterno,e.dg_ap_materno) AS dg_ejecutivo,st.dg_estado,dg_cliente, m.dg_marca,
CONCAT_WS(" ",r.dg_nombres,r.dg_ap_paterno,r.dg_ap_materno) AS dg_responsable,DATE_FORMAT(op.df_fecha_emision,"%d/%m/%Y") AS df_emision');

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la {$lang['op_doc']} especificada");
	exit();
}

$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio,dg_prefijo',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
if(count($ind_cambio)){
	$ind_cambio_prefix = $ind_cambio[0]['dg_prefijo'];
	$ind_cambio = $ind_cambio[0]['dq_cambio'];
}else{
	$ind_cambio_prefix = '$';
	$ind_cambio = 1;
}

$data = $data[0];
$monto = moneda_local($data['dq_monto']/$ind_cambio);

if($data['dg_cliente'] == '')
	$data['dg_cliente'] = $data['dg_razon'];

echo("<div class='title center'>{$lang['op_doc_may']} nº {$data['dq_oportunidad']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong> {$data['dg_cliente']}</strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td><b>{$data['df_emision']}</td>
	<td rowspan='8' width='250'>
		<label>Estado de {$lang['op_doc']}</label><br />
		<h3 class='info' style='margin:0'>{$data['dg_estado']}</h3>
	</td>
</tr><tr>
	<td>Cotizaciones asignada</td>
	<td id='load_cotizaciones'><a href='#'>Ver</a></td>
</tr><tr>
	<td>Linea de negocio:</td>
	<td><b>{$data['dg_linea_negocio']}</b></td>
</tr><tr>
	<td>Marca:</td>
	<td><b>{$data['dg_marca']}</b></td>
</tr><tr>
	<td>Monto estimado:</td>
	<td>{$ind_cambio_prefix} <b>{$monto}</b></b></td>
</tr><tr>
	<td>Llamada</td>
	<td><b>{$data['df_llamada']}</b></td>
</tr><tr>
	<td>Correo</td>
	<td><b>{$data['dg_correo']}</b></td>
</tr><tr>
	<td>Ejecutivo</td>
	<td><b>{$data['dg_ejecutivo']}</b></td>
</tr><tr>
	<td>Responsable</td>
	<td><b>{$data['dg_responsable']}</b></td>
</tr><tr>
	<td><b>Comentario</b></td>
	<td colspan='2'>{$data['dg_comentario']}</td>
</tr></table>");

?>
<script type="text/javascript">
$('#print_version').unbind('click');
$('#print_version').click(function(){
	window.open("sites/ventas/oportunidad/ver_oportunidad.php?id=<?=$_POST['id'] ?>",'print_oportunidad','width=800;height=600');
});
$('#edit_oportunidad').unbind('click');
$('#edit_oportunidad').click(function(){
	loadOverlay("sites/ventas/oportunidad/ed_oportunidad.php?id=<?=$_POST['id'] ?>");
});
$('#gestion_oportunidad').unbind('click');
$('#gestion_oportunidad').click(function(){
	loadOverlay("sites/ventas/oportunidad/src_gestion_oportunidad.php?id=<?=$_POST['id'] ?>");
});
$('#historial_actividad').unbind('click');
$('#historial_actividad').click(function(){
	loadOverlay("sites/ventas/oportunidad/history_gestion_oportunidad.php?id=<?=$_POST['id'] ?>");
});
$('#cotizar_oportunidad').unbind('click');
$('#cotizar_oportunidad').click(function(){
	loadOverlay("sites/ventas/oportunidad/cotizar_oportunidad.php?id=<?=$_POST['id'] ?>");
});
$('#load_cotizaciones a').click(function(e){
	e.preventDefault();
	loadFile('sites/ventas/oportunidad/proc/get_cotizaciones.php?id=<?=$_POST['id'] ?>','#load_cotizaciones');
});
</script>
