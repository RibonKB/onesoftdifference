<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['op_emision_desde'] = $db->sqlDate($_POST['op_emision_desde']);
$_POST['op_emision_hasta'] = $db->sqlDate($_POST['op_emision_hasta']." 23:59");

$_POST['op_status'] = implode("','",$_POST['op_status']);

$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['op_emision_desde']} AND {$_POST['op_emision_hasta']})')";

if($_POST['op_numero_desde']){
	if($_POST['op_numero_hasta']){
		$conditions .= " AND (dq_oportunidad BETWEEN {$_POST['op_numero_desde']} AND {$_POST['op_numero_hasta']})";
	}else{
		$conditions .= " AND dq_oportunidad = {$_POST['op_numero_desde']}";
	}
}

if(isset($_POST['op_client'])){
	$_POST['op_client'] = implode(',',$_POST['op_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['op_client']})";
}

if(isset($_POST['op_executive'])){
	$_POST['op_executive'] = implode(',',$_POST['op_executive']);
	$conditions .= " AND dc_ejecutivo IN ({$_POST['op_executive']})";
}

if(isset($_POST['op_buss_line'])){
	$_POST['op_buss_line'] = implode(',',$_POST['op_buss_line']);
	$conditions .= " AND dc_linea_negocio IN ({$_POST['op_buss_line']})";
}

if(isset($_POST['dc_marca'])){
	$_POST['dc_marca'] = implode(',',$_POST['dc_marca']);
	$conditions .= " AND dc_marca IN ({$_POST['dc_marca']})";
}

$data = $db->select('tb_oportunidad','dc_oportunidad,dq_oportunidad',$conditions,array('order_by' => 'df_fecha_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron oportunidades con los criterios especificados");
	exit();
}

echo("<div id='show_oportunidad'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Oportunidades<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Oportunidad</th>
	</tr>
</thead>
<tbody>");

foreach($data as $o){
echo("
<tr>
	<td align='left'>
		<a href='sites/ventas/oportunidad/proc/show_oportunidad.php?id={$o['dc_oportunidad']}' class='op_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$o['dq_oportunidad']}</a>
	</td>
</tr>");
}

echo("
</tbody>
</table>
</div>

<button class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='editbtn' id='edit_oportunidad'>Editar</button>
<button type='button' id='gestion_oportunidad' class='imgbtn' style='background-image:url(images/management.png);'>Gestión</button>
<button type='button' id='historial_actividad' class='imgbtn' style='background-image:url(images/archive.png);'>Historial</button>
<button type='button' id='cotizar_oportunidad' class='imgbtn' style='background-image:url(images/doc.png);'>Cotizar</button>
</div>
");
?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".op_load").click(function(e){
		e.preventDefault();
		$('#show_oportunidad').html("<img src='images/ajax-loader.gif' alt='' /> cargando oportunidad ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_oportunidad');
	}).first().trigger('click');
	
	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>
