<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../../../inc/lang/cotizacion.lang.global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$cliente_txt = '';
if(isset($_POST['op_other_cli'])){
	$_POST['op_cliente'] = 0;
	$cliente_txt = $_POST['op_cliente_txt'];
}

$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
if(count($ind_cambio)){
	$ind_cambio = $ind_cambio[0]['dq_cambio'];
}else{
	$ind_cambio = 1;
}

$oportunidad = $db->update("tb_oportunidad",
array(
	"dc_cliente" => $_POST['op_cliente'],
	"dg_cliente" => $cliente_txt,
	"dg_comentario" => $_POST['op_comentario'],
	"dc_linea_negocio" => $_POST['op_linea_negocio'],
	"dq_monto" => $_POST['op_monto']*$ind_cambio,
	"dg_correo" => $_POST['op_email'],
	"dc_ejecutivo" => $_POST['op_ejecutivo']
),"dc_oportunidad = {$_POST['op_id']}");

$error_man->showConfirm("Se han modificado los datos de la {$lang['op_doc']} correctamente");

?>
<script type="text/javascript">
	alert('Se han modificado los datos de la <?=$lang['op_doc'] ?> correctamente');
	$('#genOverlay').remove();
</script>