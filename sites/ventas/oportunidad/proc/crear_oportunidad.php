<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../../../inc/lang/{$empresa}/oportunidad.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_oportunidad']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$cliente_txt = '';
if(isset($_POST['op_other_cli'])){
	$_POST['op_cliente'] = 0;
	$cliente_txt = $_POST['op_cliente_txt'];
}

$db->start_transaction();
$last_date = $db->select('tb_oportunidad','MAX(df_fecha_emision) fecha',"dc_empresa={$empresa}");

if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_oportunidad','dq_oportunidad',"dc_empresa={$empresa} AND df_fecha_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_oportunidad'],0,$largo_prefix) == $cot_prefix || $empresa_conf['dc_correlativo_oportunidad'] == '3'){
		$cot_num = substr($last[0]['dq_oportunidad'],$largo_prefix)+1;
	}else{
		$cot_num = $empresa_conf['dg_correlativo_oportunidad'];
	}
}else{
	$cot_num = $empresa_conf['dg_correlativo_oportunidad'];
}

$ind_cambio = $db->select('tb_tipo_cambio','dq_cambio',"dc_tipo_cambio={$empresa_conf['dc_indicador_tipo_cambio']}");
if(count($ind_cambio)){
	$ind_cambio = $ind_cambio[0]['dq_cambio'];
}else{
	$ind_cambio = 1;
}

if($_POST['op_llamada']){
	$hora = str_pad($_POST['gestion_hora']+$_POST['gestion_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_minuto'];
	$_POST['op_llamada'] = $_POST['op_llamada']." ".$hora;
	unset($hora);
}

$oportunidad = $db->insert("tb_oportunidad",
array(
	"dq_oportunidad" => $cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT),
	"dc_cliente" => $_POST['op_cliente'],
	"dg_cliente" => $cliente_txt,
	"dg_comentario" => $_POST['op_comentario'],
	"dc_linea_negocio" => $_POST['op_linea_negocio'],
	"dq_monto" => $_POST['op_monto']*$ind_cambio,
	"df_llamada" => $db->sqlDate($_POST['op_llamada']),
	"dg_correo" => $_POST['op_email'],
	"dc_ejecutivo" => $_POST['op_ejecutivo'],
    "dc_marca" => $_POST['dc_marca'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_fecha_emision" => 'NOW()'
));

$error_man->showConfirm("{$lang['op_doc']} generada/o correctamente");
echo("<div class='title'>El número de {$lang['op_doc']} es <h1 style='margin:0;color:#000;'>".$cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT)."</h1></div>");

if($_POST['op_llamada'])
	$prox_actividad = $db->sqlDate($_POST['op_llamada']);
else
	$prox_actividad = "ADDDATE(NOW(),{$empresa_conf['dn_oportunidad_dias_actividad']})";

$db->insert('tb_oportunidad_avance',
array(
	"dc_oportunidad" => $oportunidad,
	"dg_contacto" => $_POST['op_email'],
	"dg_gestion" => 'Creación',
	"df_creacion" => 'NOW()',
	"df_proxima_actividad" => $prox_actividad,
	"dm_estado_oportunidad" => 0
));

$db->commit();

?>