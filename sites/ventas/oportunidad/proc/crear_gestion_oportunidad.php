<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
    $options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if($_POST['gestion_estado']==0){
    $error_man->showWarning("debe seleccionar un estado de Oportunidad.");
		exit;
}

$requiere=$db->select('tb_estados','dm_requiere',"dc_estado=".$_POST['gestion_estado']);

if(!$_POST['gestion_fecha']){
	if($_POST['gestion_estado']==0 || $requiere[0]['dm_requiere']==1){
		$error_man->showWarning("Si no indica una fecha para la próxima actividad debe seleccionar un estado de Oportunidad que cierre el proceso.");
		exit;
	}
}
if(!$_POST['fecha_cierre']){
	if($_POST['gestion_estado']==0 || $requiere[0]['dm_requiere']==1){
		$error_man->showWarning("Si no indica una fecha estimada de cierre debe seleccionar un estado de {$lang['op_doc']} que cierre el proceso.");
		exit;
	}
}

$db->update('tb_oportunidad',
array(
    "dm_estado" => $_POST['gestion_estado']
),"dc_oportunidad = {$_POST['op_id']}");

if($requiere[0]['dm_requiere']==0){
	$db->update('tb_oportunidad_avance',
	array(
        "dm_estado_actividad" => 0,
        "df_cierre_actividad" => 'NOW()'
	),"dc_avance = {$_POST['gestion_avance']}");
}
if($_POST['gestion_fecha']){
	$hora = str_pad($_POST['gestion_hora']+$_POST['gestion_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_minuto'];
	$_POST['gestion_fecha'] = $_POST['gestion_fecha']." ".$hora;
    unset($hora);
}

  $db->insert('tb_oportunidad_avance',
array(
    "dc_oportunidad" => $_POST['op_id'],
    "dg_gestion" => $_POST['gestion_gestion'],
    "df_creacion" => 'NOW()',
    "df_proxima_actividad" => $db->sqlDate($_POST['gestion_fecha']),
    "dm_estado_oportunidad" => $_POST['gestion_estado'],
        "df_proyeccion_cierre"=>$db->sqlDate($_POST['fecha_cierre']),
    "dm_estado_actividad" => 1,
    "dg_contacto" => $_POST['gestion_contacto']
));


if($requiere[0]['dm_requiere']==1){
$db->update('tb_oportunidad',
        array(
            'df_proyeccion_cierre'=>$db->sqlDate($_POST['fecha_cierre'])
        ),"dc_oportunidad={$_POST['op_id']}");
    }
$error_man->showConfirm("Se ha generado una nueva actividad sobre la Oportunidad");

?>