<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT dq_oportunidad FROM tb_oportunidad WHERE dc_oportunidad={$_POST['id']} AND dc_empresa = {$empresa}) op
JOIN (SELECT dq_oportunidad,dq_cotizacion FROM tb_cotizacion WHERE dc_empresa={$empresa}) cot ON op.dq_oportunidad=cot.dq_oportunidad",
'dq_cotizacion');

if(!count($data)){
	$error_man->showInfo('Sin cotizaciones asignadas');
	exit();
}

echo("<ul>");
foreach($data as $d){
	echo("<li>{$d['dq_cotizacion']}</li>");
}
echo("</ul>");
?>