<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Obtener valor tipo de cambio
$db->escape($_POST['sel_tipo_cambio']);
$dq_cambio = $db->select('tb_tipo_cambio','dg_tipo_cambio,dq_cambio,dn_cantidad_decimales',"dc_tipo_cambio={$_POST['sel_tipo_cambio']}");
if(!count($dq_cambio)){
	$error_man->showAviso("El tipo de cambio especificado es inválido, en su lugar se utiliza moneda local");
	$dm_decimales = $empresa_conf['dn_decimales_local'];
	$dg_tipo_cambio = $empresa_conf['dg_moneda_local'];
	$dq_cambio = 1;
}else{
	$dm_decimales = $dq_cambio[0]['dn_cantidad_decimales'];
	$dg_tipo_cambio = $dq_cambio[0]['dg_tipo_cambio'];
	$dq_cambio = $dq_cambio[0]['dq_cambio'];
}

$db->escape($_POST['sel_inim']);
$db->escape($_POST['sel_finm']);
$db->escape($_POST['sel_anho']);
$date_condition = "MONTH(df_fecha_emision) >= {$_POST['sel_inim']} AND MONTH(df_fecha_emision) <= {$_POST['sel_finm']} AND YEAR(df_fecha_emision) = {$_POST['sel_anho']}";

$sqlCotizacion = $db->select("(SELECT * FROM tb_oportunidad WHERE dc_empresa = {$empresa} AND {$date_condition}) op
LEFT JOIN tb_cotizacion cot ON cot.dc_cotizacion = op.dc_cotizacion
LEFT JOIN (SELECT dc_oportunidad, MAX(df_creacion) as df_ultima_gestion FROM tb_oportunidad_avance GROUP BY dc_oportunidad) av ON av.dc_oportunidad = op.dc_oportunidad
JOIN tb_funcionario f ON f.dc_funcionario = op.dc_ejecutivo
JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = op.dc_linea_negocio
LEFT JOIN tb_cliente cl ON cl.dc_cliente = op.dc_cliente
JOIN tb_usuario u ON u.dc_usuario = op.dc_usuario_creacion
JOIN tb_funcionario tv ON tv.dc_funcionario = u.dc_funcionario",
"op.dq_oportunidad, cl.dc_cliente, cl.dg_razon, op.dg_cliente, cot.dq_cotizacion, op.dg_comentario, ln.dg_linea_negocio, op.dq_monto, op.dg_correo, op.dc_ejecutivo,
CONCAT_WS(' ',f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) dg_ejecutivo, CONCAT_WS(' ',tv.dg_nombres,tv.dg_ap_paterno,tv.dg_ap_materno) dg_televenta, op.dm_estado,
op.dc_usuario_creacion dc_televenta, op.dc_linea_negocio, UNIX_TIMESTAMP(op.df_fecha_emision) uts_op_creacion, UNIX_TIMESTAMP(cot.df_fecha_emision) uts_cot_creacion, 
op.df_fecha_emision, av.df_ultima_gestion",'',
array('order_by' => 'dq_monto'));

$ejecutivos = array();
$lineas_negocio = array();
$televentas = array();

$periodo_cotizacion = array();

foreach($sqlCotizacion as $i => $cot){
	
	$sqlCotizacion[$i]['dq_monto'] = $sqlCotizacion[$i]['dq_monto']/$dq_cambio;
	$dq_monto = $sqlCotizacion[$i]['dq_monto'];
	
	if($cot['dc_cliente'] == NULL){
		$sqlCotizacion[$i]['dc_cliente'] = 0;
		$sqlCotizacion[$i]['dg_razon'] = $cot['dg_cliente'];
	}
	
	if($cot['dq_cotizacion'] == NULL){
		$sqlCotizacion[$i]['dq_cotizacion'] = '-';
	}else{
		$periodo_cotizacion[$cot['dc_ejecutivo']]['nombre_parcial'] = $cot['dg_ejecutivo'];
		$periodo_cotizacion[$cot['dc_ejecutivo']]['timediff'][] = $cot['uts_cot_creacion']-$cot['uts_op_creacion'];
	}
	
	$ejecutivos[$cot['dc_ejecutivo']]['nombre_parcial'] = $cot['dg_ejecutivo'];
	$ejecutivos[$cot['dc_ejecutivo']]['monto'][] = $dq_monto;
	
	$lineas_negocio[$cot['dc_linea_negocio']]['nombre'] = $cot['dg_linea_negocio'];
	$lineas_negocio[$cot['dc_linea_negocio']]['monto'][] = $dq_monto;
	
	$televentas[$cot['dc_televenta']]['nombre'] = $cot['dg_televenta'];
	$televentas[$cot['dc_televenta']]['monto'][] = $dq_monto;
}

$percentage = array();
foreach($ejecutivos as $ex){
	$percentage[] = array(
		'label' => $ex['nombre_parcial'],
		'data' => array_sum($ex['monto'])
	);
}

$p_televenta = array();
foreach($televentas as $tv){
	$p_televenta[] = array(
		'label' => $tv['nombre'],
		'data' => array_sum($tv['monto'])
	);
}

$p_linea_negocio = array();
foreach($lineas_negocio as $ln){
	$p_linea_negocio[] = array(
		'label' => $ln['nombre'],
		'data' => array_sum($ln['monto'])
	);
}

$p_periodo_cotizacion = array();
foreach($periodo_cotizacion as $p){
	$p_periodo_cotizacion[] = array(
		'label' => $p['nombre_parcial'],
		'data' => round((array_sum($p['timediff'])/count($p['timediff']))/3600,2)
	);
}

$sqlPeriodos = $db->select('tb_oportunidad op
JOIN tb_funcionario f ON f.dc_funcionario = op.dc_ejecutivo',
'op.dc_ejecutivo , f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno ,MONTH(op.df_fecha_emision) df_mes_emision, SUM(op.dq_monto) dq_neto_total',
"op.dc_empresa = {$empresa} AND YEAR(op.df_fecha_emision) = {$_POST['sel_anho']}",array('group_by' => 'op.dc_ejecutivo, df_mes_emision'));

$periodos = array();
foreach($sqlPeriodos as $p){
	$periodos[$p['dc_ejecutivo']]['puntos'][] = array(intval($p['df_mes_emision']),floatval($p['dq_neto_total']));
	$periodos[$p['dc_ejecutivo']]['nombre_completo'] = $p['dg_nombres'].' '.substr($p['dg_ap_paterno'],0,1).'.';
}
unset($sqlPeriodos);

?>

<div id="ex_list"></div><br />
<div id="grafico_puntos" style="width:450px;height:300px;float:left;"></div>

<div id="grafico_torta" style="width:450px;height:300px;float:left;"></div>

<div id="grafico_torta_televenta" style="width:450px;height:300px;float:left;"></div>

<div id="grafico_torta_linea_negocio" style="width:450px;height:300px;float:left;"></div>

<hr class="clear" />
<div id="data_table">
<?php if(count($p_periodo_cotizacion)): ?>
	<div id="grafico_torta_avgtime" style="width:450px;height:300px;float:left;"></div>
	<table width="50%" class="tab">
	<caption>Tiempo promedio de cotización</caption>
	<thead><tr>
		<th>Ejecutivo</th>
		<th>Promedio de cotización</th>
	</tr></thead>
	<tbody>
	<?php foreach($p_periodo_cotizacion as $p): ?>
		<tr>
			<td><?php echo $p['label'] ?></td>
			<td><?php echo $p['data'] ?> hrs</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
<?php endif; ?>
</div>
<br class="clear" />
<div id="detail_data_table">
<table class="tab" width="100%">
<caption>Detalle oportunidades</caption>
<thead><tr>
	<th>Oportunidad</th>
	<th>Fecha emisión</th>
	<th>Cotización</th>
	<th>Periodo de cotización</th>
	<th>Estado</th>
	<th>Última Gestión</th>
	<th>Televenta</th>
	<th>Ejecutivo</th>
	<th>Cliente</th>
	<th>Observación</th>
	<th>Monto</th>
</tr></thead>
<tbody>
<?php foreach($sqlCotizacion as $op): ?>
	<tr>
		<td><?php echo $op['dq_oportunidad'] ?></td>
		<td><?php echo $db->dateTimeLocalFormat($op['df_fecha_emision']) ?></td>
		<td><?php echo $op['dq_cotizacion'] ?></td>
		<td><?php echo $op['dq_cotizacion']!='-'?round(($cot['uts_cot_creacion']-$cot['uts_op_creacion'])/3600,2).' hrs':'-' ?></td>
		<td><?php echo $op['dm_estado']==1?'Abierta':'Cerrada' ?></td>
		<td><?php echo $db->dateTimeLocalFormat($op['df_ultima_gestion']) ?></td>
		<td><?php echo $op['dg_televenta'] ?></td>
		<td><?php echo $op['dg_ejecutivo'] ?></td>
		<td><?php echo $op['dg_razon'] ?></td>
		<td><?php echo $op['dg_comentario'] ?></td>
		<td><?php echo moneda_local($op['dq_monto']) ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>

<script type="text/javascript">
	js_data.piePlotData = <?php echo json_encode($percentage) ?>;
	js_data.piePlotTeleventa = <?php echo json_encode($p_televenta) ?>;
	js_data.piePlotLineaNegocio = <?php echo json_encode($p_linea_negocio) ?>;
	js_data.periodPlotData = <?php echo json_encode($periodos) ?>;
	
	js_data.oportunidadData = <?php echo json_encode($sqlCotizacion) ?>;
	js_data.tcDecimals = <?php echo $dm_decimales ?>;
</script>