<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

include_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">Contratos</div>
<div id="main_cont">
    <div class="panes">
        <?php $form->Start("sites/ventas/contrato/proc/src_factura_masiva.php","src_factura_masiva") ?>
        <?php $form->Header("<strong>Indicar los parámetros de búsqueda de los contratos</strong>") ?>
        	<table class="tab" width="100%">
        		<tbody>
                	<tr>
                    	<td>Número Contrato</td>
                        <td><?php $form->Text('Desde','dq_contrato_desde') ?></td>
                        <td><?php $form->Text('Hasta','dq_contrato_hasta') ?></td>
                    </tr>
                    <tr>
                    	<td>Número Nota Venta</td>
                        <td><?php $form->Text('Desde','dq_nota_venta_desde') ?></td>
                        <td><?php $form->Text('Hasta','dq_nota_venta_hasta') ?></td>
                    </tr>
                    <tr>
                    	<td>Fecha de corte</td>
                        <td><?php $form->Text('Mes','dc_mes_corte',1,2,date('m')) ?></td>
                        <td><?php $form->Text('Año','dc_anho_corte',1,4,date('Y')) ?></td>
                    </tr>
                    <tr>
                    	<td>Fecha Emisión</td>
                        <td><?php $form->Date('Desde','df_emision_desde') ?></td>
                        <td><?php $form->Date('Hasta','df_emision_hasta') ?></td>
                    </tr>
                    <tr>
                    	<td>Tasa de Interés</td>
                        <td><?php $form->Text('Desde','dc_interes_desde') ?></td>
                        <td><?php $form->Text('Hasta','dc_interes_hasta') ?></td>
                    </tr>
                    <tr>
                    	<td>Cliente</td>
                        <td><?php $form->ListadoMultiple('','dc_cliente','tb_cliente',array('dc_cliente','dg_razon')) ?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td>Financiamiento</td>
                        <td><?php $form->Combobox('','dm_financiamiento',array('Directo','Indirecto'),array(0,1)) ?></td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        <?php $form->End('Ejecutar consulta','searchbtn') ?>
    </div>
</div>
<script type="text/javascript">
$('#dc_cliente').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>