<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_nota_venta = intval($_POST['dc_nota_venta']);

//Datos de la nota de venta
$nota_venta = $db->prepare($db->select('tb_nota_venta','dc_tipo_nota_venta, dq_cambio, dc_tipo_cambio, dc_facturada','dc_nota_venta = ? AND dc_empresa = ?'));
$nota_venta->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$nota_venta->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_venta);
$nota_venta = $nota_venta->fetch(PDO::FETCH_OBJ);

if($nota_venta === false){
	$error_man->showWarning('No se ha encontrado la Nota de Venta especificada, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

//Datos base del contrato
$contrato = $db->prepare(
				$db->select(
					'tb_nota_venta_contrato',
					'dc_contrato,dq_contrato,df_inicio,df_termino,dc_interes,dc_cuotas,dq_cuota,dm_tasa_cambio,dg_patron_facturacion,dm_financiamiento,
					 PERIOD_DIFF(DATE_FORMAT(df_termino,"%Y%m"),DATE_FORMAT(df_inicio,"%Y%m"))/dc_cuotas dc_periodo',
					'dc_nota_venta = ? AND dc_empresa = ?'));
$contrato->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$contrato->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($contrato);
$contrato = $contrato->fetch(PDO::FETCH_OBJ);

if($contrato === false){
	$error_man->showWarning('No se ha encontrado un contrato asociado a la nota de venta especificada, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Datos de financiamiento
if($contrato->dm_financiamiento == 1){
	$financiamiento = $db->prepare($db->select('tb_contrato_financiamiento_banco','dc_financiamiento,dc_banco,dq_cuota,df_inicio,df_termino,dc_interes','dc_contrato = ?'));
	$financiamiento->bindValue(1,$contrato->dc_contrato);
	$db->stExec($financiamiento);
	
	$financiamiento = $financiamiento->fetch(PDO::FETCH_OBJ);
}

//Datos de cuotas y conceptos
$conceptos = $db->prepare($db->select('tb_nota_venta_contrato_detalle','dc_tipo, dq_cuota','dc_contrato = ?',array('group_by' => 'dc_tipo')));
$conceptos->bindValue(1,$contrato->dc_contrato,PDO::PARAM_INT);
$db->stExec($conceptos);
$conceptos = $conceptos->fetchAll(PDO::FETCH_OBJ);

//Detalles de la nota de venta
$detalle_facturacion = $db->prepare(
							$db->select(
								'tb_nota_venta_detalle d
								 JOIN tb_producto p ON p.dc_producto = d.dc_producto',
								'd.dc_nota_venta_detalle, d.dc_producto, d.dg_descripcion, d.dq_precio_compra, d.dq_precio_venta,
								 p.dg_codigo',
								'dc_nota_venta = ? AND dm_tipo = 0'));
$detalle_facturacion->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$db->stExec($detalle_facturacion);

//Datos del tipo de cambio
$tipo_cambio = $db->prepare($db->select('tb_tipo_cambio','dg_tipo_cambio, dn_cantidad_decimales','dc_tipo_cambio = ?'));
$tipo_cambio->bindValue(1,$nota_venta->dc_tipo_cambio,PDO::PARAM_INT);
$db->stExec($tipo_cambio);
$tipo_cambio = $tipo_cambio->fetch(PDO::FETCH_OBJ);

?>
<div class="secc_bar">
	Editar Contrato <?php echo $contrato->dq_contrato ?>
</div>
<div class="panes">
	<?php $form->Start('sites/ventas/contrato/proc/editar_contrato.php','ed_contrato') ?>
    <?php $form->Header('<b>Indique los datos actualizados del contrato</b><br />
	Los datos marcados con [*] son obligatorios<br />
	Los valores están representados en <b>'.$tipo_cambio->dg_tipo_cambio.'</b> y ajustados al cambio en la nota de venta') ?>
    <input type="hidden" value="<?php echo $nota_venta->dc_tipo_nota_venta ?>" id="nv_tipo_venta" />
    <?php
			$form->Section();
				$form->Text('Número de cuotas','dc_cuotas',1,Form::DEFAULT_TEXT_LENGTH,$contrato->dc_cuotas);
				$form->Hidden('dc_cuotas_old',$contrato->dc_cuotas);
				$form->Text('Valor cuota','dq_cuota',1,Form::DEFAULT_TEXT_LENGTH,$contrato->dq_cuota/$nota_venta->dq_cambio);
				$form->Text('Tasa de interés','dc_interes_interno',1,Form::DEFAULT_TEXT_LENGTH,$contrato->dc_interes);
				$form->Select('Tasa de cambio','dm_tasa_cambio',array(
				1 =>'Último día hábil del mes',
					'Primer día hábil mes siguiente',
					'Último día del mes',
					'Primer día del mes siguiente',
					'Día de facturación',
					'Pactado en Nota de Venta'
				),1,$contrato->dm_tasa_cambio);
				if($nota_venta->dc_facturada == 0){
					$form->Select('Periodo','dc_periodo',array(
						1 => 'Mensual',
						3 => 'Trimestral',
						6 => 'Semestral',
						7 => 'Anual'
					),1,$contrato->dc_periodo);
				}
			$form->EndSection();
			$form->Section();
				if($nota_venta->dc_facturada == 0){
					$form->Date('Fecha de inicio','df_inicio',1,$contrato->df_inicio);
					$form->Hidden('df_inicio_old',$contrato->df_inicio);
				}
				//$form->Date('Fecha de término','df_termino',1);
				$form->Textarea('Patrón de facturación','dg_patron',0,$contrato->dg_patron_facturacion);
				?>
                <div style="border:1px solid #CCC; background-color:#FAFAFA; padding:3px;">
                	Máscaras para el patrón:
                    <table width="280">
                    	<tbody>
                            <tr>
                                <td width="140" align="right">
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{TC}}')" style="width:120px;text-align:center">
                                    	Tipo de Cambio
                                    </button>
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{QC}}')" style="width:120px;text-align:center">
                                    	Valor Tipo cambio
                                    </button>
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{CS}}')" style="width:120px;text-align:center">
                                    	Total Cuotas
                                    </button>
                                </td>
                                <td valign="top">
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{CT}}')" style="width:120px;text-align:center">
                                    	Cuota actual
                                    </button>
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{CTO}}')" style="width:120px;text-align:center">
                                    	Número Contrato
                                    </button>
									<button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{TTC}}')" style="width:120px;text-align:center">
                                    	Total en Tipo Cambio
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php
			$form->EndSection();
			
			$form->Group();
			$form->Section();
				echo('<div style="width:280px;height:1px"></div>');
				$form->Radiobox('Financiamiento','dm_financiamiento',array('Directo','Indirecto'),$contrato->dm_financiamiento,' ');
			$form->EndSection();
			$form->Section();
			echo('<div id="financiamiento_data" class="hidden">');
				$form->DBSelect('Banco '.Form::MANDATORY_ICON,'dc_banco','tb_banco',
							array('dc_banco','dg_banco'),
							Form::DEFAULT_MANDATORY_STATUS,
							isset($financiamiento->dc_banco)?$financiamiento->dc_banco:'');
				$form->Text('Valor cuota '.Form::MANDATORY_ICON,'dq_cuota_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							Form::DEFAULT_TEXT_LENGTH,
							isset($financiamiento->dq_cuota)?$financiamiento->dq_cuota/$nota_venta->dq_cambio:'');
				$form->Date('Fecha inicio '.Form::MANDATORY_ICON,'df_inicio_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							isset($financiamiento->df_inicio)?$financiamiento->df_inicio:'');
				$form->Date('Fecha término '.Form::MANDATORY_ICON,'df_termino_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							isset($financiamiento->df_termino)?$financiamiento->df_termino:'');
				$form->Text('Tasa interés '.Form::MANDATORY_ICON,'dc_interes_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							Form::DEFAULT_TEXT_LENGTH,
							isset($financiamiento->interes)?$financiamiento->interes:'');
			echo('</div>');
			$form->EndSection();
			
			$form->Group();
			
			$form->Header('Detalles de facturación');
		?>
        <table class="tab" width="100%">
        	<thead>
            	<tr>
                	<th width="50">Código</th>
                    <th>Descripción</th>
                    <th width="100">Costo</th>
                    <th width="100">Precio</th>
                </tr>
            </thead>
            <tbody>
            <?php while($d = $detalle_facturacion->fetch(PDO::FETCH_OBJ)): ?>
            	<tr>
                	<td>
						<?php echo $d->dg_codigo ?>
                        <?php $form->Hidden('dc_detalle_nota_venta[]',$d->dc_nota_venta_detalle) ?>
                    </td>
                    <td><?php echo $d->dg_descripcion ?></td>
                    <td>
						<input type="text" class="inputtext" size="20" style="text-align:right" name="dq_costo[]" value="<?php echo round($d->dq_precio_compra/$nota_venta->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?>" />
                    </td>
                    <td>
						<input type="text" class="inputtext" size="20" style="text-align:right" name="dq_precio[]"  value="<?php echo round($d->dq_precio_venta/$nota_venta->dq_cambio,$tipo_cambio->dn_cantidad_decimales) ?>" />
                    </td>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>
	<div id="distribucion_data">
		<?php
			foreach($conceptos as $c):
				$form->Hidden('dq_cuota_concepto[]',$c->dq_cuota/$nota_venta->dq_cambio);
				$form->Hidden('dc_detalle_concepto[]',$c->dc_tipo);
			endforeach;
		?>
	</div>
    <?php
		foreach($conceptos as $c):
			$form->Hidden('dc_detalle_concepto_old[]',$c->dc_tipo);
		endforeach;
		$form->Hidden('dc_contrato',$contrato->dc_contrato);
		$form->Hidden('dc_nota_venta',$dc_nota_venta);
		if($contrato->dm_financiamiento == 1){
			$form->Hidden('dc_financiamiento',$financiamiento->dc_financiamiento);
		}
		$form->Group();
    	$form->End('Editar','editbtn');
	?>
</div>
<script type="text/javascript" src="jscripts/sites/ventas/nota_venta/cr_contrato.js"></script>
<script type="text/javascript">
	cr_contrato.fieldNames.contratoForm = '#ed_contrato';
	cr_contrato.submitContratoForm = function(){return;};
	cr_contrato.initCreation();
</script>