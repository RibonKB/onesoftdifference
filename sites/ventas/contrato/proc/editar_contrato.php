<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_nota_venta = intval($_POST['dc_nota_venta']);
$dc_contrato = intval($_POST['dc_contrato']);

//Datos de la nota de venta
$nota_venta = $db->prepare($db->select('tb_nota_venta','dc_tipo_nota_venta, dq_cambio, dc_tipo_cambio, dc_facturada','dc_nota_venta = ? AND dc_empresa = ?'));
$nota_venta->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$nota_venta->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_venta);
$nota_venta = $nota_venta->fetch(PDO::FETCH_OBJ);

if($nota_venta === false){
	$error_man->showWarning('No se ha encontrado la Nota de Venta especificada, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

//validar que las cuotas del contrato no sean menores a las facturadas
if(intval($_POST['dc_cuotas']) <= $nota_venta->dc_facturada){
	$error_man->showWarning('El contrato tiene letras facturadas superiores a la cantidad de cuotas que desea modificar, '.
	 						'primero anule las facturas necesarias antes de poder cambiar la cantidad de cuotas');
	exit;
}

//Validar que el valor cuota no supere el valor detalle de la nota de venta
if($_POST['dq_cuota'] != array_sum($_POST['dq_precio'])){
	$error_man->showWarning('El valor cuota debe ser igual que el valor configurado como detalle de facturación.');
	exit;
}

	$dm_financiamiento = intval($_POST['dm_financiamiento']);

	$contrato_data = array(
		'dc_interes' => '?',
		'dq_cuota' => '?',
		'dm_tasa_cambio' => '?',
		'dm_financiamiento' => '?',
		'dg_patron_facturacion' => '?'
	);
	
	$params = array(
		array(floatval($_POST['dc_interes_interno']), PDO::PARAM_STR),
		array(floatval($_POST['dq_cuota'])*$nota_venta->dq_cambio, PDO::PARAM_STR),
		array($_POST['dm_tasa_cambio'], PDO::PARAM_STR),
		array($dm_financiamiento, PDO::PARAM_INT),
		array($_POST['dg_patron'], PDO::PARAM_STR)
	);
	
	if($nota_venta->dc_facturada == 0){
		$df_inicio = $db->sqlDate2($_POST['df_inicio']);
		$dc_periodo = intval($_POST['dc_periodo']);
		$dc_meses_contrato = $dc_periodo*intval($_POST['dc_cuotas']);
		
		$contrato_data['df_termino'] = "DATE_ADD(?, INTERVAL {$dc_meses_contrato} MONTH)";
		$params[] = array($df_inicio, PDO::PARAM_STR);
		
		if($df_inicio != $_POST['df_inicio_old']){
			$contrato_data['df_inicio'] = '?';
			
			$params[] = array($df_inicio, PDO::PARAM_STR);
			
		}
		
	}else{
		$contrato = $db->prepare(
						$db->select('tb_nota_venta_contrato',
									'df_inicio, PERIOD_DIFF(DATE_FORMAT(df_termino,"%Y%m"),DATE_FORMAT(df_inicio,"%Y%m"))/dc_cuotas dc_periodo',
									'dc_contrato = ?'));
		$contrato->bindValue(1,$dc_contrato,PDO::PARAM_INT);
		$db->stExec($contrato);
		$contrato = $contrato->fetch(PDO::FETCH_OBJ);
		
		if($contrato === false){
			$error_man->showWarning('Error irrecuperable, no se pudo obtener la información del contrato');
			exit;
		}
		
		$df_inicio = $contrato->df_inicio;
		$dc_periodo = $contrato->dc_periodo;
		
	}
	
	if(intval($_POST['dc_cuotas']) != intval($_POST['dc_cuotas_old'])){
		
		$contrato_data['dc_cuotas'] = '?';
		$params[] = array(intval($_POST['dc_cuotas']), PDO::PARAM_INT);
		
	}
	
	$cuota_facturada = array();
	if($nota_venta->dc_facturada > 1){
		$cuotas = $db->prepare($db->select(
									'tb_nota_venta_contrato_detalle',
									'dc_numero_cuota',
									'dc_contrato = ? AND dc_factura IS NOT NULL',
									array('group_by' => 'dc_numero_cuota')));
		$cuotas->bindValue(1,$dc_contrato,PDO::PARAM_INT);
		$db->stExec($cuotas);
		
		while($c = $cuotas->fetch(PDO::FETCH_OBJ)):
			$cuota_facturada[] = $c->dc_numero_cuota;
		endwhile;
		
	}
	
$db->start_transaction();

	$update_contrato = $db->prepare($db->update('tb_nota_venta_contrato',$contrato_data,'dc_contrato = ?'));
	foreach($params as $i => $p){
		$update_contrato->bindValue($i+1,$p[0],$p[1]);
	}
	$update_contrato->bindValue(count($params)+1,$dc_contrato,PDO::PARAM_INT);
	$db->stExec($update_contrato);
	
	if($dm_financiamiento == 1){
		
		$financiamiento_data = array(
			'dc_banco' => '?',
			'dq_cuota' => '?',
			'df_inicio' => '?',
			'df_termino' => '?',
			'dc_interes' => '?'
		);
		$financiamiento_param = array(
			array($_POST['dc_banco'],PDO::PARAM_INT),
			array($_POST['dq_banco'],PDO::PARAM_STR),
			array($db->sqlDate2($_POST['df_inicio_banco']),PDO::PARAM_STR),
			array($db->sqlDate2($_POST['df_termino_banco']),PDO::PARAM_STR),
			array($_POST['dc_interes_banco'],PDO::PARAM_STR),
			array($dc_contrato,PDO::PARAM_INT)
		);
		
		//Actualizar datos del financimiento
		$update_financiamiento = $db->prepare($db->update('tb_contrato_financiamiento_banco',$financiamiento_data,'dc_contrato = ?'));
		
		foreach($financiamiento_param as $i => $p){
			$update_financiamiento->bindValue($i+1,$p[0],$p[1]);
		}
		$db->stExec($update_financiamiento);
		
		//En caso de no haber actualizado se inserta el nuevo registro del financiamiento
		if($update_financiamiento->rowCount() < 1){
			
			$financiamiento_data['dc_contrato'] = '?';
			$insert_financiamiento = $db->prepare($db->insert('tb_contrato_financiamiento_banco',$financiamiento_data));
			
			foreach($financiamiento_param as $i => $p){
				$insert_financiamiento->bindValue($i+1,$p[0],$p[1]);
			}
			
			$db->stExec($insert_financiamiento);
			
		}
		
		
	}else{
		$delete_financiamiento = $db->prepare('DELETE FROM tb_contrato_financiamiento_banco WHERE dc_contrato = ?');
		$delete_financiamiento->bindValue(1,$dc_contrato,PDO::PARAM_INT);
		$db->stExec($delete_financiamiento);
	}
	
	//Actualizar detalles de cuotas del contrato
	$dc_cuotas = intval($_POST['dc_cuotas']);
	$dc_cuotas_old = intval($_POST['dc_cuotas_old']);
	
	$insert_detalle = $db->prepare($db->insert('tb_nota_venta_contrato_detalle',array(
		'dc_tipo' => '?',
		'dc_contrato' => '?',
		'dq_cuota' => '?',
		'dc_numero_cuota' => '?',
		'df_vencimiento' => 'DATE_ADD(?, INTERVAL ? MONTH)'
	)));
	$insert_detalle->bindParam(1,$dc_concepto,PDO::PARAM_INT);
	$insert_detalle->bindValue(2,$dc_contrato,PDO::PARAM_INT);
	$insert_detalle->bindParam(3,$dq_cuota_detalle,PDO::PARAM_STR);
	$insert_detalle->bindParam(4,$dc_cuota,PDO::PARAM_INT);
	$insert_detalle->bindValue(5,$df_inicio,PDO::PARAM_STR);
	$insert_detalle->bindParam(6,$df_vencimiento,PDO::PARAM_INT);
	
	if($dc_cuotas != $dc_cuotas_old){
		
		if($dc_cuotas > $dc_cuotas_old){
			for($dc_cuota = $dc_cuotas_old+1; $dc_cuota <= $dc_cuotas; $dc_cuota++){
				foreach($_POST['dc_detalle_concepto'] as $i => $dc_concepto){
					$dq_cuota_detalle = floatval($_POST['dq_cuota_concepto'][$i])*$nota_venta->dq_cambio;
					$df_vencimiento = $dc_periodo*$dc_cuota;
					
					$db->stExec($insert_detalle);
					
				}
			}
			
		}else{
			$db->doExec("DELETE FROM tb_nota_venta_contrato_detalle WHERE dc_numero_cuota > {$dc_cuotas} AND dc_contrato = {$dc_contrato}");
		}
	}
	
	//Borrar conceptos de pago quitados (solo para cuotas no facturadas)
	foreach($_POST['dc_detalle_concepto_old'] as $dc_concepto_old){
		if(!in_array($dc_concepto_old, $_POST['dc_detalle_concepto'])){
			$db->doExec("DELETE FROM tb_nota_venta_contrato_detalle WHERE dc_tipo = {$dc_concepto_old} AND dc_contrato = {$dc_contrato} AND dc_factura IS NULL");
		}
	}
	
	//Actualizar montos en detalles antiguos
	$update_detalle = $db->prepare($db->update('tb_nota_venta_contrato_detalle',array(
		'dq_cuota' => '?',
		'df_vencimiento' => 'DATE_ADD(?, INTERVAL dc_numero_cuota*? MONTH)'
	),'dc_contrato = ? AND dc_numero_cuota <= ? AND dc_tipo = ? AND dc_factura IS NULL'));
	$update_detalle->bindParam(1,$dq_cuota_detalle,PDO::PARAM_STR);
	$update_detalle->bindValue(2,$df_inicio,PDO::PARAM_STR);
	$update_detalle->bindValue(3,$dc_periodo,PDO::PARAM_INT);
	$update_detalle->bindValue(4,$dc_contrato,PDO::PARAM_INT);
	$update_detalle->bindValue(5,$dc_cuotas_old,PDO::PARAM_INT);
	$update_detalle->bindParam(6,$dc_concepto,PDO::PARAM_INT);
	
	foreach($_POST['dc_detalle_concepto'] as $i => $dc_concepto){
		$dq_cuota_detalle = floatval($_POST['dq_cuota_concepto'][$i])*$nota_venta->dq_cambio;
		$db->stExec($update_detalle);
	}
	
	//Verificar la agregación de nuevos conceptos de pago al contrato
	if($dc_cuotas < $dc_cuotas_old){
		$hasta = $dc_cuotas;
	}else{
		$hasta = $dc_cuotas_old;
	}
	foreach($_POST['dc_detalle_concepto'] as $i => $dc_concepto){
		if(!in_array($dc_concepto,$_POST['dc_detalle_concepto_old'])){
			for($dc_cuota = 1; $dc_cuota <= $hasta; $dc_cuota++){
				
				//Verificar que la cuota no se encuentre facturada
				if(in_array($dc_cuota,$cuota_facturada)){
					continue;
				}
				
				$dq_cuota_detalle = floatval($_POST['dq_cuota_concepto'][$i])*$nota_venta->dq_cambio;
				$df_vencimiento = $dc_periodo*$dc_cuota;
				
				$db->stExec($insert_detalle);
			}
		}
	}
	
	//Modificar detalles de la nota de venta
	$update_detalle_nota_venta = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dq_precio_compra' => '?',
		'dq_precio_venta' => '?'
	),'dc_nota_venta_detalle = ?'));
	$update_detalle_nota_venta->bindParam(1,$dq_costo,PDO::PARAM_STR);
	$update_detalle_nota_venta->bindParam(2,$dq_precio,PDO::PARAM_STR);
	$update_detalle_nota_venta->bindParam(3,$dc_detalle_nota_venta,PDO::PARAM_INT);
	
	foreach($_POST['dc_detalle_nota_venta'] as $i => $dc_detalle_nota_venta){
		$dq_costo = floatval($_POST['dq_costo'][$i])*$nota_venta->dq_cambio;
		$dq_precio = floatval($_POST['dq_precio'][$i])*$nota_venta->dq_cambio;
		
		$db->stExec($update_detalle_nota_venta);
	}
	
	$detalles_nota_venta = implode(',',$_POST['dc_detalle_nota_venta']);
	
	$db->doExec("DELETE FROM tb_nota_venta_detalle WHERE dc_nota_venta_detalle NOT IN({$detalles_nota_venta}) AND dc_nota_venta = {$dc_nota_venta} AND dm_tipo = 0");
	
	//En caso en que se hayan agregado más detalles
	if(count($_POST['dc_detalle_nota_venta']) < count($_POST['dq_precio'])){
		//Insertar los detalles a la nota de venta
		//NOT-IMPLEMENTED-YET
	}
	
	$update_nota_venta = $db->prepare($db->update('tb_nota_venta',array(
		'dq_neto' => '?',
		'dq_iva' => '?',
		'dc_solicitada' => '?'
	),'dc_nota_venta = ?'));
	$dq_neto = floatval(array_sum($_POST['dq_precio']))*$nota_venta->dq_cambio;
	$update_nota_venta->bindValue(1,$dq_neto,PDO::PARAM_STR);
	$update_nota_venta->bindValue(2,round($dq_neto*$empresa_conf['dq_iva']/100,$empresa_conf['dn_decimales_local']),PDO::PARAM_STR);
	$update_nota_venta->bindValue(3,$dc_cuotas,PDO::PARAM_INT);
	$update_nota_venta->bindValue(4,$dc_nota_venta,PDO::PARAM_INT);
	
	$db->stExec($update_nota_venta);
	

$db->commit();

$error_man->showConfirm('Se han modificado los detalles del contrato correctamente.');
?>