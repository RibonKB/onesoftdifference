<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

//Función que devuelve el siguiente folio disponible a partir del que se reciba por parámetro
function getNextFolio($dq_folio){
	global $db;
	
	while($db->doQuery($db->select('tb_factura_venta','true',"dm_nula = 0 AND dq_folio = ".$dq_folio))->fetch() !== false)
		$dq_folio++;
	
	return $dq_folio;
}

//Formateo datos entrada
$dq_folio = intval($_POST['dq_folio']);
$df_emision = $db->sqlDate2($_POST['df_emision']);
$df_vencimiento = $db->sqlDate2($_POST['df_vencimiento']);

//Validación Folio
if(!$dq_folio){
	$error_man->showWarning('El formato del folio es incorrecto, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Preparar inserción de cabecera de factura.
$insert_factura = $db->prepare($db->insert('tb_factura_venta',array(
	'dc_nota_venta' => '?',
	'dc_cliente' => '?',
	'dc_contacto' => '?',
	'dc_ejecutivo' => '?',
	'dc_contacto_entrega' => '?',
	'dc_medio_pago' => '?',
	'dc_tipo_cargo' => '?',
	'dc_tipo_operacion' => '?',
	'dq_factura' => '?',
	'dq_folio' => '?',
	'dg_comentario' => '?',
	'df_emision' => '?',
	'df_creacion' => $db->getNow(),
	'df_vencimiento' => '?',
	'df_libro_venta' => '?',
	'dq_neto' => '?',
	'dq_exento' => '?',
	'dq_iva' => '?',
	'dq_total' => '?',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa,
	'dm_anticipada' => '?',
	'dm_tipo_impresion' => '?'
)));
$insert_factura->bindParam(1 ,$dc_nota_venta,PDO::PARAM_INT);
$insert_factura->bindParam(2 ,$dc_cliente,PDO::PARAM_INT);
$insert_factura->bindParam(3 ,$dc_contacto,PDO::PARAM_INT);
$insert_factura->bindParam(4 ,$dc_ejecutivo,PDO::PARAM_INT);
$insert_factura->bindParam(5 ,$dc_contacto_entrega,PDO::PARAM_INT);
$insert_factura->bindValue(6 ,$_POST['dc_medio_pago'],PDO::PARAM_INT);
$insert_factura->bindValue(7 ,$_POST['dc_tipo_cargo'],PDO::PARAM_INT);
$insert_factura->bindValue(8 ,$_POST['dc_tipo_operacion'],PDO::PARAM_INT);
$insert_factura->bindParam(9 ,$dq_factura,PDO::PARAM_INT);
$insert_factura->bindParam(10,$dq_folio,PDO::PARAM_INT);
$insert_factura->bindValue(11,$_POST['dg_comentario'],PDO::PARAM_STR);
$insert_factura->bindValue(12,$df_emision,PDO::PARAM_STR);
$insert_factura->bindValue(13,$df_vencimiento,PDO::PARAM_STR);
$insert_factura->bindValue(14,$df_emision,PDO::PARAM_STR);
$insert_factura->bindParam(15,$dq_neto,PDO::PARAM_STR);
$insert_factura->bindParam(16,$dq_exento,PDO::PARAM_STR);
$insert_factura->bindParam(17,$dq_iva,PDO::PARAM_STR);
$insert_factura->bindParam(18,$dq_total,PDO::PARAM_STR);
$insert_factura->bindValue(19,'N',PDO::PARAM_STR);
$insert_factura->bindParam(20,$dm_tipo_impresion,PDO::PARAM_STR);

//Preparar inserción detalle factura
$insert_detalle_factura = $db->prepare($db->insert('tb_factura_venta_detalle',array(
	'dc_factura' => '?',
	'dg_producto' => '?',
	'dc_detalle_nota_venta' => '?',
	'dg_descripcion' => '?',
	'dm_tipo' => '?',
	'dc_cantidad' => '?',
	'dq_precio' => '?',
	'dq_costo' => '?',
	'dq_total' => '?'
)));
$insert_detalle_factura->bindParam(1,$dc_factura,PDO::PARAM_INT);
$insert_detalle_factura->bindParam(2,$dg_producto,PDO::PARAM_STR);
$insert_detalle_factura->bindParam(3,$dc_detalle_nota_venta,PDO::PARAM_INT);
$insert_detalle_factura->bindParam(4,$dg_descripcion,PDO::PARAM_STR);
$insert_detalle_factura->bindParam(5,$dm_tipo_detalle,PDO::PARAM_STR);
$insert_detalle_factura->bindValue(6,1,PDO::PARAM_INT);
$insert_detalle_factura->bindParam(7,$dq_precio,PDO::PARAM_STR);
$insert_detalle_factura->bindParam(8,$dq_costo,PDO::PARAM_STR);
$insert_detalle_factura->bindParam(9,$dq_total_detalle,PDO::PARAM_STR);

//Preparar edición factura en detalle de contrato
$update_detalle_contrato = $db->prepare($db->update('tb_nota_venta_contrato_detalle',array(
	'dc_factura' => '?'
),'dc_contrato = ? AND dc_numero_cuota = ?'));
$update_detalle_contrato->bindParam(1,$dc_factura,PDO::PARAM_INT);
$update_detalle_contrato->bindParam(2,$dc_contrato,PDO::PARAM_INT);
$update_detalle_contrato->bindParam(3,$dc_cuota,PDO::PARAM_INT);

//Preparar edición cantidades facturadas detalle nota de venta
$update_detalle_nota_venta = $db->prepare($db->update('tb_nota_venta_detalle',array(
	'dc_facturada' => 'dc_facturada+1'
),'dc_nota_venta_detalle = ?'));
$update_detalle_nota_venta->bindParam(1,$dc_detalle_nota_venta,PDO::PARAM_INT);

//Preparar edición cantidades facturadas cabecera nota de venta
$update_nota_venta = $db->prepare($db->update('tb_nota_venta',array(
	'dc_facturada' => 'dc_facturada+?'
),'dc_nota_venta = ?'));
$update_nota_venta->bindParam(1,$dc_facturada,PDO::PARAM_INT);
$update_nota_venta->bindParam(2,$dc_nota_venta,PDO::PARAM_INT);

include_once('../../proc/ventas_functions.php');

$db->start_transaction();

$detalle_facturas = array();
foreach($_POST['dc_cuota'] as $c){
	
	//Configurar datos para la cabecera de la factura
	$c = explode(',',$c);
	$dc_cuota = $c[0];
	$dc_contrato = $c[1];
	$i = $c[2];
	
	$dq_factura = doc_GetNextNumber('tb_factura_venta','dq_factura',2,'df_creacion');
	$dq_folio = getNextFolio($dq_folio);
	
	$contrato = $db->prepare($db->select('tb_nota_venta_contrato c
									JOIN tb_nota_venta nv ON nv.dc_nota_venta = c.dc_nota_venta
									JOIN (SELECT dc_nota_venta, SUM(dq_precio_venta*dq_cantidad) dq_neto FROM tb_nota_venta_detalle WHERE dm_tipo = 0 GROUP BY dc_nota_venta) d ON d.dc_nota_venta = nv.dc_nota_venta
									JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente',
									'nv.dc_nota_venta, nv.dc_cliente, nv.dc_contacto, nv.dc_ejecutivo, d.dq_neto, nv.dq_iva, cl.dg_razon,
									 c.dg_patron_facturacion, c.dq_contrato, c.dc_cuotas, nv.dc_tipo_cambio, c.dm_tasa_cambio, nv.dq_cambio dq_cambio_nota_venta',
									'c.dc_contrato = ? AND nv.dc_empresa = ?'));
	$contrato->bindValue(1,$dc_contrato,PDO::PARAM_INT);
	$contrato->bindValue(2,$empresa,PDO::PARAM_INT);
	$db->stExec($contrato);
	$contrato = $contrato->fetch(PDO::FETCH_OBJ);
	
	//validar valor tipo de cambio en la base de datos
	if($contrato->dm_tasa_cambio != 6){
		$df_cambio = $_POST['df_cambio'][$i];
		$df_cambio_real = $db->sqlDate2($_POST['df_cambio_real'][$i]);
		
		if($df_cambio != $df_cambio_real){
			$tipo_cambio_real = $db->prepare($db->select('tb_tipo_cambio_mensual','dq_cambio','dc_tipo_cambio = ? AND df_cambio = ?'));
			$tipo_cambio_real->bindValue(1,$contrato->dc_tipo_cambio,PDO::PARAM_INT);
			$tipo_cambio_real->bindValue(2,$df_cambio_real,PDO::PARAM_STR);
			$db->stExec($tipo_cambio_real);
			$tipo_cambio_real = $tipo_cambio_real->fetch(PDO::FETCH_OBJ);
			
			if($tipo_cambio_real === false){
				$error_man->showWarning("Error al intentar obtener el valor del tipo de cambio para la cuota <b>{$dc_cuota}</b> del contrato <b>{$contrato->dq_contrato}</b>");
				$db->rollBack();
				exit;
			}
			
			$dq_cambio = floatval($tipo_cambio_real->dq_cambio);
			
		}else{
			$dq_cambio = floatval($_POST['dq_cambio'][$i]);
		}
	}else{
		$dq_cambio = floatval($_POST['dq_cambio'][$i]);
	}
	
	$dc_nota_venta = $contrato->dc_nota_venta;
	$dc_cliente = $contrato->dc_cliente;
	$dc_contacto = $contrato->dc_contacto;
	$dc_ejecutivo = $contrato->dc_ejecutivo;
	$dc_contacto_entrega = $contrato->dc_contacto;
	$dq_neto = round(floatval($contrato->dq_neto/$contrato->dc_cuotas)*($dq_cambio/$contrato->dq_cambio_nota_venta),$empresa_conf['dn_decimales_local']);
	$dq_exento = 0;
	$dq_iva = round(floatval(($contrato->dq_neto*$empresa_conf['dq_iva']/100)/$contrato->dc_cuotas)*($dq_cambio/$contrato->dq_cambio_nota_venta),$empresa_conf['dn_decimales_local']);
	$dq_total = floatval($dq_neto+$dq_iva);
	
	if(trim($contrato->dg_patron_facturacion) == ''){
		$dm_tipo_impresion = '1';
	}else{
		$dm_tipo_impresion = '0';
	}
	
	$db->stExec($insert_factura);
	
	$dc_factura = $db->lastInsertId();
	
	//Actualizar factura en detalle de contrato
	$db->stExec($update_detalle_contrato);
	
	//Obtener detalles de la nota de venta para almacenar como detalle de la factura
	$detalle_nota_venta = $db->doQuery($db->select('tb_nota_venta_detalle d
								JOIN tb_producto p ON p.dc_producto = d.dc_producto',
							'd.dc_nota_venta_detalle, d.dg_descripcion, d.dq_precio_compra, d.dq_precio_venta, p.dg_codigo',
							'd.dc_nota_venta = '.$dc_nota_venta));
	
	//Almacenar detalles de Factura
	$dm_tipo_detalle = 0;
	$dc_facturada = 0;
	$dq_total_precio = $dq_total_costo = 0;
	while($dnv = $detalle_nota_venta->fetch(PDO::FETCH_OBJ)){
		$dg_producto = $dnv->dg_codigo;
		$dc_detalle_nota_venta = $dnv->dc_nota_venta_detalle;
		$dg_descripcion = $dnv->dg_descripcion;
		$dq_precio = $dq_total_detalle = $dnv->dq_precio_venta*($dq_cambio/$contrato->dq_cambio_nota_venta);
		$dq_costo = $dnv->dq_precio_compra*($dq_cambio/$contrato->dq_cambio_nota_venta);
		
		$db->stExec($insert_detalle_factura);
		$db->stExec($update_detalle_nota_venta);
		
		$dc_facturada++;
		$dq_total_precio += $dq_precio;
		$dq_total_costo += $dq_costo;
	}
	
	$db->stExec($update_nota_venta);
	
	if($dm_tipo_impresion == '0'){
		$dm_tipo_detalle = 1;
		
		$tipo_cambio = $db->doQuery($db->select('tb_tipo_cambio','dg_tipo_cambio, dq_cambio','dc_tipo_cambio = '.$contrato->dc_tipo_cambio))->fetch(PDO::FETCH_OBJ);
		
		$contrato->dg_patron_facturacion = str_replace('{{TC}}',$tipo_cambio->dg_tipo_cambio,$contrato->dg_patron_facturacion);
		$contrato->dg_patron_facturacion = str_replace('{{QC}}',moneda_local($tipo_cambio->dq_cambio),$contrato->dg_patron_facturacion);
		$contrato->dg_patron_facturacion = str_replace('{{CS}}',$contrato->dc_cuotas,$contrato->dg_patron_facturacion);
		$contrato->dg_patron_facturacion = str_replace('{{CT}}',$dc_cuota,$contrato->dg_patron_facturacion);
		$contrato->dg_patron_facturacion = str_replace('{{CTO}}',$contrato->dq_contrato,$contrato->dg_patron_facturacion);
		$contrato->dg_patron_facturacion = str_replace('{{TTC}}',moneda_local($dq_total/$tipo_cambio->dq_cambio,2),$contrato->dg_patron_facturacion);
		
		$dg_producto = '';
		$dc_detalle_nota_venta = 0;
		$dg_descripcion = $contrato->dg_patron_facturacion;
		$dq_precio = $dq_total_precio;
		$dq_costo = $dq_total_costo;
		$dq_total_detalle = $dq_total_precio;
		
		$db->stExec($insert_detalle_factura);
		
	}
	
	
	$detalle_facturas[] =  array(
		'dq_contrato' => $contrato->dq_contrato,
		'dc_cuota' => $dc_cuota,
		'dc_cuotas' => $contrato->dc_cuotas,
		'dc_factura' => $dc_factura,
		'dq_factura' => $dq_factura,
		'dq_folio' => $dq_folio,
		'dg_cliente' => $contrato->dg_razon,
		'dq_neto' => $dq_neto,
		'dq_iva' => $dq_iva,
		'dq_total' => $dq_total
	);
	
	$dq_folio++;
	
}
$db->commit();

$error_man->showConfirm('Se han generado las facturas asociadas para cada uno de los contratos seleccionados');
?>
<table class="tab" width="100%">
<caption>Datos de facturas generadas</caption>
<thead>
	<tr>
    	<th>Contrato</th>
        <th>Cuota</th>
        <th>Factura</th>
        <th>Folio</th>
        <th>Cliente</th>
        <th>Neto</th>
        <th>IVA</th>
        <th>Total</th>
        <th>&nbsp;</th>
    </tr>
</thead>
<tbody>
	<?php while($d = array_shift($detalle_facturas)): ?>
    <tr>
    	<td><?php echo $d['dq_contrato'] ?></td>
        <td><?php echo $d['dc_cuota'] ?>/<?php echo $d['dc_cuotas'] ?></td>
        <td><?php echo $d['dq_factura'] ?></td>
        <td><?php echo $d['dq_folio'] ?></td>
        <td><?php echo $d['dg_cliente'] ?></td>
        <td align="right"><?php echo moneda_local($d['dq_neto']) ?></td>
        <td align="right"><?php echo moneda_local($d['dq_iva']) ?></td>
        <td align="right"><?php echo moneda_local($d['dq_total']) ?></td>
        <td align="center">
        	<a href="sites/ventas/factura_venta/ver_factura_venta.php?id=<?php echo $d['dc_factura'] ?>" target="_blank">[IMPRIMIR]</a>
        </td>
    </tr>
    <?php endwhile; ?>
</tbody>
</table>
