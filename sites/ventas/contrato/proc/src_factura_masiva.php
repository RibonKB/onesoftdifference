<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_mes_corte = intval($_POST['dc_mes_corte']);
if($dc_mes_corte < 1 || $dc_mes_corte > 12){
	$error_man->showWarning('El mes de corte es inválido, compruebe el dato y vuelva a intentarlo');
	exit;
}

$dc_anho_corte = intval($_POST['dc_anho_corte']);
if(!$dc_anho_corte){
	$error_man->showWarning('El año de corte es inválido, compruebe el dato y vuelva a intentarlo');
	exit;
}

if($dc_mes_corte == 12){
	$dc_mes_corte = '01';
	$dc_anho_corte++;
}else{
	$dc_mes_corte = str_pad($dc_mes_corte+1, 2, 0, STR_PAD_LEFT);
}

$df_corte = $db->sqlDate("01/{$dc_mes_corte}/{$dc_anho_corte}");

$conditions = "c.dc_empresa = {$empresa} AND d.dc_factura IS NULL AND d.df_vencimiento < {$df_corte} AND nv.dm_nula = 0";

$dq_contrato_desde = intval($_POST['dq_contrato_desde']);
$dq_contrato_hasta = intval($_POST['dq_contrato_hasta']);
if($dq_contrato_desde != 0){
	
	if($dq_contrato_hasta != 0){
		$conditions .= " AND (c.dq_contrato BETWEEN {$dq_contrato_desde} AND {$dq_contrato_hasta})";
	}else{
		$conditions .= " AND c.dq_contrato = {$dq_contrato_desde}";
	}
	
}

if($_POST['df_emision_desde'] != ''){
	$df_emision_desde = $db->sqlDate($_POST['df_emision_desde']);
	if($_POST['df_emision_hasta'] != ''){
		$df_emision_hasta = $db->sqlDate($_POST['df_emision_hasta']);
		$conditions .= " AND (c.df_emision BETWEEN {$df_emision_desde} AND {$df_emision_hasta})";
	}else{
		$conditions .= " AND c.df_emision = {$df_emision_desde}";
	}
}

$dq_nota_venta_desde = intval($_POST['dq_nota_venta_desde']);
$dq_nota_venta_hasta = intval($_POST['dq_nota_venta_hasta']);
if($dq_nota_venta_desde != 0){
	
	if($dq_nota_venta_hasta != 0){
		$conditions .= " AND (nv.dq_nota_venta BETWEEN {$dq_nota_venta_desde} AND {$dq_nota_venta_hasta})";
	}else{
		$conditions .= " AND nv.dq_nota_venta = {$dq_nota_venta_desde}";
	}
	
}

$dc_interes_desde = floatval($_POST['dc_interes_desde']);
$dc_interes_hasta = floatval($_POST['dc_interes_hasta']);
if($dc_interes_desde != 0.0){
	
	if($dc_interes_hasta != 0.0){
		$conditions .= " AND (c.dc_interes BETWEEN {$dc_interes_desde} AND {$dc_interes_hasta})";
	}else{
		$conditions .= " AND c.dc_interes = {$dc_interes_desde}";
	}
	
}

if(isset($_POST['dc_cliente'])){
	$dc_cliente = implode(',',$_POST['dc_cliente']);
	$conditions .= " AND cl.dc_cliente IN ({$dc_cliente})";
}

if(isset($_POST['dm_financiamiento']) && count($_POST['dm_financiamiento']) != 2){
	$dm_financiamiento = implode(',',$_POST['dm_financiamiento']);
	$conditions .= " c.dm_financiamiento IN ({$dm_financiamiento})";
}

$conditions .= " AND nv.dm_nula = 0";

$data = $db->doQuery($db->select('tb_nota_venta_contrato c
	JOIN tb_nota_venta nv ON nv.dc_nota_venta = c.dc_nota_venta
	JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = nv.dc_tipo_cambio
	JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
	JOIN tb_nota_venta_contrato_detalle d ON d.dc_contrato = c.dc_contrato',
'c.dq_contrato, c.df_emision, c.df_inicio, c.df_termino, c.dc_interes, c.dc_cuotas, c.dm_financiamiento, nv.dq_cambio dq_cambio_nota_venta,
nv.dq_nota_venta, cl.dg_razon, SUM(d.dq_cuota) dq_cuota, d.dc_numero_cuota, d.df_vencimiento, c.dc_contrato, c.dm_tasa_cambio,
tc.dg_tipo_cambio, nv.dq_cambio, tc.dn_cantidad_decimales, tc.dc_tipo_cambio, MONTH(d.df_vencimiento) dc_mes_vencimiento, YEAR(d.df_vencimiento) dc_anho_vencimiento',
$conditions,array(
	'group_by' => 'c.dc_contrato, d.dc_numero_cuota',
	'order_by' => 'cl.dg_razon ASC, c.dq_contrato, d.df_vencimiento'
)))->fetchAll(PDO::FETCH_OBJ);

//Mostrar aviso en caso de no encontrar ninguna
if(!count($data)){
	$error_man->showAviso('No se encontraron contratos pendientes de facturación con los filtros proporcionados');
	exit;
}

//Comprobar el mínimo mes de vencimiento para cada contrato.
$mes = array();
foreach($data as $c):
	
	$c->dc_mes_vencimiento = intval($c->dc_mes_vencimiento);
	$c->dc_anho_vencimiento = intval($c->dc_anho_vencimiento);
	
	if(!isset($mes[$c->dc_contrato])){
		$mes[$c->dc_contrato] = array($c->dc_mes_vencimiento,$c->dc_anho_vencimiento);
	}else{
		if($mes[$c->dc_contrato][1] > $c->dc_anho_vencimiento && $mes[$c->dc_contrato][0] > $c->dc_mes_vencimiento){
			$mes[$c->dc_contrato] = array($c->dc_mes_vencimiento,$c->dc_anho_vencimiento);
		}
	}
	
endforeach;

//Obtener la tasa de cambio para la divisa.
$tipo_cambio = array();

foreach($data as $c):
	
	switch($c->dm_tasa_cambio){
		case 1: //Tipo de cambio al último día hábil del mes
		
			if($mes[$c->dc_contrato][0] == 1){
				$mes[$c->dc_contrato][0] == 12;
			}else{
				$mes[$c->dc_contrato][0] -= 1;
			}
		
			$tc = $db->prepare(
					$db->select(
						'tb_tipo_cambio_mensual',
						'dq_cambio, df_cambio',
						'dc_tipo_cambio = ? AND dm_habil = 1 AND MONTH(df_cambio) = ? AND YEAR(df_cambio) = ?',
						array('order_by' => 'df_cambio DESC', 'limit' => 1)
					)
				);
			$tc->bindValue(1,$c->dc_tipo_cambio,PDO::PARAM_INT);
			$tc->bindValue(2,$mes[$c->dc_contrato][0],PDO::PARAM_INT);
			$tc->bindValue(3,$mes[$c->dc_contrato][1],PDO::PARAM_INT);
			$db->stExec($tc);
			$tc = $tc->fetch(PDO::FETCH_OBJ);
			
			if($tc === false){
				$m = str_pad($mes[$c->dc_contrato][0],2,0,STR_PAD_LEFT).'/'.$mes[$c->dc_contrato][1];
				$error_man->showWarning('No se han configurados los valores de cambio para el tipo de cambio <b>'.$c->dg_tipo_cambio.'</b> en el mes <b>'.$m.'</b>');
				$error_man->showWarning('Error encontrado en el contrato <b>'.$c->dq_contrato.'</b> de la nota de venta <b>'.$c->dq_nota_venta.'</b>');
				exit;
			}
			
			$tc = array($tc->dq_cambio,$tc->df_cambio,'Último día hábil del mes');
			
			break;
		
		case 2: //Primer día hábil del mes siguiente
			$m = $mes[$c->dc_contrato][0]+1;
			$y = $mes[$c->dc_contrato][1];
			if($m > 12){
				$m = 1;
				$y++;
			}
			
			$tc = $db->prepare(
					$db->select(
						'tb_tipo_cambio_mensual',
						'dq_cambio, df_cambio',
						'dc_tipo_cambio = ? AND dm_habil = 1 AND MONTH(df_cambio) = ? AND YEAR(df_cambio) = ?',
						array('order_by' => 'df_cambio ASC', 'limit' => 1)
					)
				);
			$tc->bindValue(1,$c->dc_tipo_cambio,PDO::PARAM_INT);
			$tc->bindValue(2,$m,PDO::PARAM_INT);
			$tc->bindValue(3,$y,PDO::PARAM_INT);
			$db->stExec($tc);
			$tc = $tc->fetch(PDO::FETCH_OBJ);
			
			if($tc === false){
				$m = str_pad($m,2,0,STR_PAD_LEFT).'/'.$y;
				$error_man->showWarning('No se han configurados los valores de cambio para el tipo de cambio <b>'.$c->dg_tipo_cambio.'</b> en el mes <b>'.$m.'</b>');
				exit;
			}
			
			$tc = array($tc->dq_cambio,$tc->df_cambio,'Primer día hábil mes siguiente');
			
			break;
		case 3: //Último día del mes
		
			$tc = $db->prepare(
					$db->select(
						'tb_tipo_cambio_mensual',
						'dq_cambio, df_cambio',
						'dc_tipo_cambio = ? AND MONTH(df_cambio) = ? AND YEAR(df_cambio) = ?',
						array('order_by' => 'df_cambio DESC', 'limit' => 1)
					)
				);
			$tc->bindValue(1,$c->dc_tipo_cambio,PDO::PARAM_INT);
			$tc->bindValue(2,$mes[$c->dc_contrato][0],PDO::PARAM_INT);
			$tc->bindValue(3,$mes[$c->dc_contrato][1],PDO::PARAM_INT);
			$db->stExec($tc);
			$tc = $tc->fetch(PDO::FETCH_OBJ);
			
			if($tc === false){
				$m = str_pad($mes[$c->dc_contrato][0],2,0,STR_PAD_LEFT).'/'.$mes[$c->dc_contrato][1];
				$error_man->showWarning('No se han configurados los valores de cambio para el tipo de cambio <b>'.$c->dg_tipo_cambio.'</b> en el mes <b>'.$m.'</b>');
				exit;
			}
			
			$tc = array($tc->dq_cambio,$tc->df_cambio,'Último día del mes');
			
			break;
			
		case 4: //Primer día del mes siguiente
			$m = $mes[$c->dc_contrato][0]+1;
			$y = $mes[$c->dc_contrato][1];
			if($m > 12){
				$m = 1;
				$y++;
			}
			
			$tc = $db->prepare(
					$db->select(
						'tb_tipo_cambio_mensual',
						'dq_cambio, df_cambio',
						'dc_tipo_cambio = ? AND MONTH(df_cambio) = ? AND YEAR(df_cambio) = ?',
						array('order_by' => 'df_cambio ASC', 'limit' => 1)
					)
				);
			$tc->bindValue(1,$c->dc_tipo_cambio,PDO::PARAM_INT);
			$tc->bindValue(2,$m,PDO::PARAM_INT);
			$tc->bindValue(3,$y,PDO::PARAM_INT);
			$db->stExec($tc);
			$tc = $tc->fetch(PDO::FETCH_OBJ);
			
			if($tc === false){
				$m = str_pad($m,2,0,STR_PAD_LEFT).'/'.$y;
				$error_man->showWarning('No se han configurados los valores de cambio para el tipo de cambio <b>'.$c->dg_tipo_cambio.'</b> en el mes <b>'.$m.'</b>');
				exit;
			}
			
			$tc = array($tc->dq_cambio,$tc->df_cambio,'Primer día mes siguiente');
			
			break;
		
		case 5: //Al día de facturación
			
			$tc = $db->prepare($db->select('tb_tipo_cambio_mensual','dq_cambio, df_cambio','dc_tipo_cambio = ? AND DATE(df_cambio) = DATE(?)'));
			$tc->bindValue(1,$c->dc_tipo_cambio,PDO::PARAM_INT);
			$tc->bindValue(2,$c->df_vencimiento,PDO::PARAM_STR);
			$db->stExec($tc);
			$tc = $tc->fetch(PDO::FETCH_OBJ);
			
			if($tc === false){
				$m = $db->dateLocalFormat($c->df_vencimiento);
				$error_man->showWarning('No se han configurados los valores de cambio para el tipo de cambio <b>'.$c->dg_tipo_cambio.'</b> en el día <b>'.$m.'</b>');
				exit;
			}
			
			$tc = array($tc->dq_cambio,$tc->df_cambio,'Día de facturación');
			
			break;
		case 6: //Pactado
			
			$tc = array($c->dq_cambio_nota_venta,0,'Pactado');
			
			break;
			
		default: 
			$error_man->showWarning('Error inesperado, la tasa de cambio para el contrato <b>'.$c->dq_contrato.'</b> es inválida y no se puede continuar');
			exit;
	}
	$tipo_cambio[] = $tc;
	
endforeach;

require_once("../../../../inc/form-class.php");
$form = new Form($empresa);

?>
<?php
	$form->Start('sites/ventas/contrato/proc/crear_factura_masiva.php','cr_factura_masiva');
	$form->Header('<b>Complete los datos para generar las facturas de venta para el cobro de contratos</b><br />Los campos marcados con [*] son obligatorios');
	$form->Section();
		$form->Text('Número Documento inicial','dq_folio',1);
		$form->Date('Fecha Emisión','df_emision',1,0);
		$form->Date('Fecha Vencimiento','df_vencimiento',1);
	$form->EndSection();
	$form->Section();
		$form->DBSelect('Medio Pago','dc_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'),1);
		$form->DBSelect('Tipo Cargo','dc_tipo_cargo','tb_tipo_cargo',array('dc_tipo_cargo','dg_tipo_cargo'));
		$form->DBSelect('Tipo Operación','dc_tipo_operacion','tb_tipo_operacion',array('dc_tipo_operacion','dg_tipo_operacion'),1);
	$form->EndSection();
	$form->Section();
		$form->Textarea('Comentario','dg_comentario');
	$form->EndSection();
	$form->Group();
?>
    <table class="tab bicolor_tab" width="100%" id="contratos_details">
        <thead>
            <th align="left"><input type="checkbox" id="select_all" /></th>
            <th>Cuota</th>
            <th>Contrato</th>
            <th>NV</th>
            <th>Cliente</th>
            <th>Valor</th>
            <th>TC</th>
            <th>Modo TC</th>
            <th>Fecha TC</th>
            <th>Vencimiento</th>
            <th>Valor TC</th>
            <th>Neto</th>
        </thead>
        <tbody>
        <?php $i = 0 ?>
        <?php while($c = array_shift($data)): ?>
        	<?php $tc = array_shift($tipo_cambio) ?>
            <tr>
                <td>
                	<input type="checkbox" name="dc_cuota[]" class="dc_cuota" value="<?php echo $c->dc_numero_cuota ?>,<?php echo $c->dc_contrato ?>,<?php echo $i ?>" />
                </td>
                <td><b><?php echo $c->dc_numero_cuota ?>/<?php echo $c->dc_cuotas ?></b></td>
                <td><b><?php echo $c->dq_contrato ?></b></td>
                <td><?php echo $c->dq_nota_venta ?></td>
                <td><?php echo $c->dg_razon ?></td>
                <td align="right"><?php echo moneda_local($c->dq_cuota/$c->dq_cambio,$c->dn_cantidad_decimales) ?></td>
                <td><b><?php echo $c->dg_tipo_cambio ?></b></td>
                <td><?php echo $tc[2] ?></td>
                <?php if($c->dm_tasa_cambio == 6): ?>
                	<td>
                    	-
                        <input type="hidden" name="df_cambio_real[]" value="<?php echo $tc[1] ?>" />
                        <input type="hidden" name="dq_cambio[]" value="<?php echo $tc[0] ?>" />
                        <input type="hidden" name="df_cambio[]" value="<?php echo $tc[1] ?>" />
                    </td>
                <?php else: ?>
                	<td>
                    	<input type="text" name="df_cambio_real[]" class="date inputtext" value="<?php echo $tc[1] ?>" size="7" />
                        <input type="hidden" name="dq_cambio[]" value="<?php echo $tc[0] ?>" />
                        <input type="hidden" name="df_cambio[]" value="<?php echo $tc[1] ?>" />
                    </td>
                <?php endif; ?>
                <td align="center"><b><?php echo $db->dateLocalFormat($c->df_vencimiento) ?></b></td>
                <td align="right"><?php echo moneda_local($tc[0],$c->dn_cantidad_decimales) ?></td>
                <td align="right"><?php echo moneda_local($c->dq_cuota*($tc[0]/$c->dq_cambio)) ?></td>
            </tr>
        <?php $i++ ?>
        <?php endwhile; ?>
        </tbody>
    </table>
<?php
	$form->Group();
	$form->End('Facturar','addbtn');
?>
<script type="text/javascript">
	pymerp.init('#src_factura_masiva_res');
	$('#contratos_details').tableAdjust(7);
</script>