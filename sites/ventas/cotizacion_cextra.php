<?php
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/cotizacion.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Contactos extra de <?=$lang['cot_plural_may'] ?></div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Elija el tipo de contacto que irá en la {$lang['cot_doc']} y elija una etiqueta.<br />
		<a href='#' class='ol' rel='#ex1'>Haga click aquí para ver un ejemplo</a>");
	$form->Start("sites/ventas/proc/crear_cotizacion_cextra.php","cr_cextra");
	$form->Section();
	$form->Text("Etiqueta: (ej: 'Factura enviar a')","cextra_label",1);
	$form->EndSection();
	$form->Section();
	$form->Listado("Tipo de contacto","cextra_tipo","tb_tipo_direccion",array("dc_tipo_direccion","dg_tipo_direccion"),1);
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	//Se obtienen los contactos extra de cotizacion para mostrarlos en un listado
	$cextra = $db->select(
		"tb_cotizacion_contacto_extra ce, tb_tipo_direccion td",
		"ce.dc_contacto_extra, ce.dg_etiqueta, td.dg_tipo_direccion",
		"ce.dc_tipo_direccion = td.dc_tipo_direccion AND ce.dc_empresa={$empresa} AND ce.dm_activo='1'"
		);
		
	echo("<table class='tab' width='100%'>
	<caption>Contactos extra para {$lang['cot_doc']} ya agregados</caption>
	<thead>
		<tr>
			<th width='40%'>Etiqueta</th>
			<th width='40%'>Tipo de dirección</th>
			<th width='20%'>Opciones</th>
		</tr>
	</thead>
	<tbody id='list'>");
	
	foreach($cextra as $c){
		echo("
		<tr id='item{$c['dc_contacto_extra']}'>
			<td>{$c['dg_etiqueta']}</td>
			<td>{$c['dg_tipo_direccion']}</td>
			<td>
				<a href='sites/ventas/ed_cotizacion_cextra.php?id={$c['dc_contacto_extra']}' class='loadOnOverlay'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/ventas/del_cotizacion_cextra.php?id={$c['dc_contacto_extra']}' class='loadOnOverlay'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>
		");
	}
	
	echo('</tbody>');
?>
</div>
</div>
<div id="ex1" class="overlay"><img src="sites/ventas/examples/ex_1.jpg" alt="" /></div>