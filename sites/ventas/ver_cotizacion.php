<?php
	define("MAIN",1);
	include_once("../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	include("template_cotizacion/modo{$empresa}.php");
	
	$datosCotizacion = $db->select("(SELECT * FROM tb_cotizacion WHERE dc_empresa={$empresa} AND dc_cotizacion = {$_GET['id']}) cot
	LEFT JOIN tb_termino_comercial term ON term.dc_termino_comercial = cot.dc_termino_comercial
	LEFT JOIN tb_contacto_cliente c ON c.dc_contacto = cot.dc_contacto
	LEFT JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
	LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
	LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
	LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = cot.dc_tipo_cambio
	LEFT JOIN tb_modo_envio me ON me.dc_modo_envio = cot.dc_modo_envio
	LEFT JOIN tb_cliente cl ON cl.dc_cliente = cot.dc_cliente
	LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = cot.dc_ejecutivo
	LEFT JOIN tb_funcionario tv ON tv.dc_funcionario = cot.dc_ejecutivo_tv
	LEFT JOIN tb_cotizacion_estado ce ON ce.dc_estado = cot.dc_estado",
	"cl.dc_cliente,cl.dg_giro,cot.dc_cotizacion,cot.dq_cotizacion,term.dg_termino_comercial,s.dg_direccion,com.dg_comuna,reg.dg_region,tc.dg_tipo_cambio,
	tc.dn_cantidad_decimales,cot.dq_cambio,me.dg_modo_envio,cl.dg_razon,cl.dg_rut,cot.dq_iva,cot.dq_neto,c.dg_contacto,tc.dn_cantidad_decimales,
	DATE_FORMAT(cot.df_fecha_emision,'%d/%m/%Y') as df_fecha_emision,UNIX_TIMESTAMP(cot.df_fecha_envio) as df_fecha_envio,
	(cot.dq_iva+cot.dq_neto) as dq_total,cot.dg_observacion,CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno,ex.dg_ap_materno) as dg_ejecutivo,
	CONCAT_WS(' ',tv.dg_nombres,tv.dg_ap_paterno,tv.dg_ap_materno) as dg_televenta,cot.dc_estado,ce.dg_estado");
	
	if(!count($datosCotizacion)){
		$error_man->showWarning("La cotizacion especificada no existe");
		exit();
	}
	$datosCotizacion = $datosCotizacion[0];
	
	setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
	$datosCotizacion['df_fecha_envio'] = strftime("%d de %B de %Y",$datosCotizacion['df_fecha_envio']);
	
	$datosCotizacion['detalle'] = $db->select("tb_cotizacion_detalle",
	"dg_producto dg_codigo,dq_cantidad,(dq_precio_venta/{$datosCotizacion['dq_cambio']}) AS dq_precio_venta,dg_descripcion,
	CAST(dq_cantidad*dq_precio_venta/{$datosCotizacion['dq_cambio']} as DECIMAL(20,{$datosCotizacion['dn_cantidad_decimales']})) as dq_total",
	"dc_cotizacion = {$_GET['id']}");
	
	$datosCotizacion['dq_total'] = 0;
	foreach($datosCotizacion['detalle'] as $d){
		$datosCotizacion['dq_total'] += $d['dq_precio_venta']*$d['dq_cantidad'];
	}
	
	$datosCotizacion['dq_neto'] = $datosCotizacion['dq_total']*$datosCotizacion['dq_cambio'];
	
	//arreglar
	$datosCotizacion['dq_iva'] = $datosCotizacion['dq_neto']*0.19;
	
	$opcionesCotizacion['contactoExtra'] = array();
	$opcionesCotizacion['dir_contactoExtra'] = array();
	
	$contactosExtra = $db->select(
	"(SELECT * FROM tb_cotizacion_contactos_extra_cotizacion WHERE dc_cotizacion = {$_GET['id']}) ex
	JOIN tb_cotizacion_contacto_extra ce ON ce.dc_contacto_extra = ex.dc_contacto_extra
	JOIN tb_contacto_cliente c ON c.dc_contacto = ex.dc_contacto
	JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal",
	"ce.dg_etiqueta,s.dg_direccion,c.dg_contacto");
	
	foreach($contactosExtra as $c){
		$opcionesCotizacion['contactoExtra'][] =& $c['dg_etiqueta'];
		$opcionesCotizacion['dir_contactoExtra'][] = " {$c['dg_contacto']} - {$c['dg_direccion']}";
	}
	
	$pdf = new PDF();
	$pdf->AddDetalle($datosCotizacion['detalle']);
	$pdf->Output();
	
	
?>