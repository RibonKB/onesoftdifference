<?php
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/cotizacion.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_cotizacion_contacto_extra",
"dg_etiqueta,dc_tipo_direccion",
"dc_contacto_extra = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el contacto extra");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Contactos extra de <?=$lang['cot_doc'] ?></div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados para el contacto extra");
	$form->Start("sites/ventas/proc/editar_cotizacion_cextra.php","ed_cextra");
	$form->Section();
	$form->Text("Etiqueta","ed_cextra_label",1,255,$datos['dg_etiqueta']);
	$form->EndSection();
	$form->Section();
	$form->Listado("Tipo de contacto","ed_cextra_tipo","tb_tipo_direccion",array("dc_tipo_direccion","dg_tipo_direccion"),1,$datos['dc_tipo_direccion']);
	$form->EndSection();
	$form->Hidden("ed_cextra_id",$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>