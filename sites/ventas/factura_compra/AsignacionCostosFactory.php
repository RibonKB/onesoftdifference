<?php

class AsignacionCostosFactory extends Factory {
    
    protected $title = 'Asignacion de Costos';
    private $facturaCompra;
    private $detallesOriginales;
    
    public function indexAction(){
        $this->setNotFound();
    }
    
    public function searchAction(){
        $this->initFunctionsService();
        $factura = $this->getFacturaCompra();
        $this->validateShowData($factura);
        $cebe = $this->getCebe();
        $selectDetalles = $this->getSelectDetalles($factura->detalles);
        $costosAsignados = $this->getDetallesAsignacionCostos($factura);
        $datos = array(
            'factura' => $factura, 
            'cebe' => $cebe,
            'costosAsignados' => $costosAsignados,
            'selectDetalles' => $selectDetalles,
        );
		
        $form = $this->getFormView($this->getTemplateUrl('show'), $datos);
        echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
    }
    
    private function getFacturaCompra(){
        $r = Factory::getRequest();
        $db = $this->getConnection();
        $table = ' tb_factura_compra fc ';
        $table .= ' JOIN tb_proveedor pv ON fc.dc_proveedor = pv.dc_proveedor ';
        $table .= ' LEFT JOIN tb_orden_compra oc ON fc.dc_orden_compra =  oc.dc_orden_compra';
        $table .= ' LEFT JOIN tb_guia_recepcion gr ON fc.dc_guia_recepcion = gr.dc_guia_recepcion ';
        
        $fields = 'fc.dc_factura, fc.dq_factura, fc.dq_folio, fc.dq_exento, fc.dq_neto, fc.dq_iva, fc.dq_total';
        $fields .= ',fc.dm_nula,pv.dg_razon, oc.dc_orden_compra';
        
        $conditions = 'fc.dc_empresa = :empresa AND fc.dc_factura = :facturaCompra ';
        $conditions .= ' AND (fc.dc_nota_venta = 0 or fc.dc_nota_venta IS NULL)';
        $conditions .= ' AND (oc.dc_nota_venta  = 0 or oc.dc_nota_venta IS NULL)';
        $conditions .= ' AND (gr.dc_nota_venta  = 0 or gr.dc_nota_venta IS NULL)';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':empresa', $this->getEmpresa() ,PDO::PARAM_INT);
        $st->bindValue(':facturaCompra', $r->dc_factura,PDO::PARAM_INT);
        $db->stExec($st);
        $factura =  $st->fetch(PDO::FETCH_OBJ);
        $detalleFacturas = $this->getDetallesFactura($factura);
        $factura->detalles = $this->fixDescripcionDetallesFacturaCompra($detalleFacturas);
        return $factura;
    }
    
    private function getDetallesFactura($factura){
        $db = $this->getConnection();
        $table = 'tb_factura_compra_detalle';
        $fields = 'dc_detalle, dc_producto, dg_descripcion, dq_total';
        $conditions = 'dc_factura = :factura';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':factura', $factura->dc_factura, PDO::PARAM_INT);
        $db->stExec($st);
        return $st->fetchAll(PDO::FETCH_OBJ);
    }
    
    private function fixDescripcionDetallesFacturaCompra($detalles){
        $db = $this->getConnection();
        
        foreach($detalles as $c => $v){
            if(empty($v->dg_descripcion)){
                $producto = clone $db->getRowById('tb_producto', $v->dc_producto, 'dc_producto');
                $detalles[$c]->dg_descripcion = $producto->dg_producto;
            }
        }
        
        return $detalles;
    }
    
    private function getDetallesAsignacionCostos($facturaCompra){
        if($facturaCompra === false){
            return false;
        }
        
        $db = $this->getConnection();
        
        $table = ' tb_factura_compra_asignacion_costos ac ';
        $table .= ' JOIN tb_factura_compra_detalle fcd ON ac.dc_detalle_factura_compra = fcd.dc_detalle ';
        $table .= ' JOIN tb_nota_venta nv ON ac.dc_nota_venta = nv.dc_nota_venta ';
        $table .= ' JOIN tb_cebe cb ON ac.dc_cebe = cb.dc_cebe ';
        $table .= ' LEFT JOIN tb_comprobante_asignacion_costos cac ON ac.dc_asignacion = cac.dc_asignacion_costo ';
        $table .= ' LEFT JOIN tb_factura_venta fv ON cac.dc_factura_venta = fv.dc_factura ';
        
        $fields = 'ac.dc_factura_compra, ac.dc_asignacion, ac.dc_nota_venta, cac.dc_factura_venta, ac.dm_contrato, ';
        $fields .= ' ac.dq_monto, ac.dg_descripcion, ac.dc_cebe, ac.df_fecha_asignacion,  ac.dc_cantidad_cuotas, ';
        $fields .= ' ac.dc_cuotas_saldadas, ac.dm_nula, ac.dc_detalle_factura_compra, nv.dq_nota_venta, cb.dg_cebe, fv.dm_marge_centralizada ';
        
        $conditions = 'ac.dc_factura_compra = :facturaCompra AND ac.dc_empresa = :empresa AND ac.dm_nula = 0';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':facturaCompra', $facturaCompra->dc_factura, PDO::PARAM_INT);
        $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        
        $db->stExec($st);
        $detalles = array();
        foreach($st->fetchAll(PDO::FETCH_OBJ) as $k => $v){
            $contrato = $this->getDatosContrato($v->dc_nota_venta);
            $detalles[$k] = $v;
            $detalles[$k]->dc_contrato = $contrato->dc_contrato;
            $detalles[$k]->dc_cuotas = $contrato->dc_cuotas;
            $detalles[$k]->dc_facturadas = $contrato->dc_facturadas;
            $detalles[$k]->facturasVenta = $this->getFacturasVenta($v->dc_nota_venta);
        }
        
        return $detalles;
        
    }
    
    private function validateShowData($facturaCompra){
        if($facturaCompra === false){
            $msg = 'Se ha producido uno de los siguientes errores: ';
            $msg .= '<ul>';
            $msg .= '<li> La Factura de compra está asociada a una nota de venta</li>';
            $msg .= '<li> La Guía de Recepcion relacionada con esta factura está asociada a una nota de venta</li>';
            $msg .= '<li> La Orden de Compra relacionada con esta factura está asociada a una nota de venta</li>';
            $msg .= '</ul>';
            $this->getErrorMan()->showAviso($msg);
            exit;
        }
        $this->validateGuiaRecepcion($facturaCompra);
    }
    
    private function validateGuiaRecepcion($facturaCompra){
        $db = $this->getConnection();
        
        $table = 'tb_guia_recepcion';
        $fields = 'dq_guia_recepcion, dc_nota_venta';
        $conditions = 'dc_orden_compra = :ordenCompra OR dc_factura = :factura';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':ordenCompra', $facturaCompra->dc_orden_compra, PDO::PARAM_INT);
        $st->bindValue(':factura', $facturaCompra->dc_factura, PDO::PARAM_INT);
        
        $db->stExec($st);
        foreach($st->fetchAll(PDO::FETCH_OBJ) as $v){
            if(!empty($v->dc_nota_venta)){
                $msg = 'No se puede asignar costos por que la factura de compra está relacionada con la guía de recepción';
                $msg = " {$v->dq_guia_recepcion}, y ésta está asociada a una nota de venta.";
                $this->getErrorMan()->showAviso($msg);
                exit;
            }
        }
    }
    
    private function getSelectDetalles($detalles){
        $select = array();
        foreach($detalles as $v){
            $select[$v->dc_detalle] = $v->dg_descripcion;
        }
        return $select;
    }
    
    private function getCebe(){
        $db = $this->getConnection();
        $values = 'dc_cebe, dg_cebe';
        $conditions = 'dc_empresa = :empresa AND dm_activo = :activo';
        $st = $db->prepare($db->select('tb_cebe', $values, $conditions));
        $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        $st->bindValue(':activo', 1, PDO::PARAM_INT);
        $db->stExec($st);
        $result = $st->fetchAll(PDO::FETCH_OBJ);
        $cebes = array();
        foreach($result as $v){
            $cebes[$v->dc_cebe] = $v->dg_cebe;
        }
        
        return $cebes;
        
    }
    
    public function notaVentaAutocompleterAction(){
        Factory::getAutocompleterClass();
        $r = Factory::getRequest();
        $db = $this->getConnection();
        
        $table = 'tb_nota_venta nv';
        $table .= ' JOIN tb_cliente cte ON nv.dc_cliente_final =  cte.dc_cliente ';
        $fields = 'nv.dc_nota_venta, nv.dq_nota_venta, nv.dq_neto, cte.dg_razon';
        $conditions = ' nv.dc_empresa = :empresa AND nv.dm_nula = 0 ';
        $conditions .= ' AND (nv.dq_nota_venta LIKE :q OR cte.dg_razon LIKE :q) ';
        $conditions .= ' ORDER BY nv.df_fecha_emision DESC ';
        $conditions .= ' LIMIT :limit ';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        
        $st->bindValue(':empresa',$this->getEmpresa(),PDO::PARAM_INT);
        $st->bindValue(':q','%'.$r->q.'%',PDO::PARAM_STR);
        $st->bindValue(':limit',10,PDO::PARAM_INT);
        $db->stExec($st);
        $results = $st->fetchAll(PDO::FETCH_OBJ);
        
        foreach ($results as $k => $v) {
            $contrato = $this->getDatosContrato($v->dc_nota_venta);
            $results[$k]->dc_contrato = $contrato->dc_contrato;
            $results[$k]->dc_cuotas = $contrato->dc_cuotas;
            $results[$k]->dc_facturadas = $contrato->dc_facturadas;
            $results[$k]->facturas = json_encode($this->getFacturasVenta($v->dc_nota_venta, true));
        }
        
        foreach ($results as $fc){
            echo Autocompleter::getItemRow(array(
                $fc->dq_nota_venta,
                $fc->dg_razon,
                $fc->dq_neto,
                $fc->dc_nota_venta,
                $fc->dc_contrato,
                $fc->dc_cuotas,
                $fc->dc_facturadas,
                $fc->facturas,
            ));
        }
    }
	
    /*
     * Obtiene la factura de compra, para ser asignada al campo de texto
     * 
     */
    // public function facturaCompraAutocompleterAction(){
        // Factory::getAutocompleterClass();
        // $r = Factory::getRequest();
        // $db = $this->getConnection();
        
        // $table = ' tb_factura_compra fc ';
        // $table .= ' JOIN tb_proveedor pv ON fc.dc_proveedor = pv.dc_proveedor ';
        // $table .= ' LEFT JOIN tb_orden_compra oc ON fc.dc_orden_compra =  oc.dc_orden_compra';
        // $table .= ' LEFT JOIN tb_guia_recepcion gr ON fc.dc_guia_recepcion = gr.dc_guia_recepcion ';
        
        // $fields = 'fc.dc_factura, fc.dq_factura, fc.dq_folio, fc.dq_neto, pv.dg_razon';
        // $fields .= ',fc.dm_nula,pv.dg_razon, oc.dc_orden_compra';
        
        // $conditions = 'fc.dc_empresa = :empresa AND fc.dm_nula = 0 ';
        // $conditions .= ' AND (fc.dq_factura LIKE :q OR fc.dq_folio LIKE :q OR pv.dg_razon LIKE :q) ';
        // $conditions .= ' AND (fc.dc_nota_venta = 0 or fc.dc_nota_venta IS NULL) ';
        // $conditions .= ' AND (oc.dc_nota_venta  = 0 or oc.dc_nota_venta IS NULL) ';
        // $conditions .= ' AND (gr.dc_nota_venta  = 0 or gr.dc_nota_venta IS NULL) ';
        // $conditions .= ' ORDER BY fc.df_emision DESC ';
        
        // $st = $db->prepare($db->select($table,$fields,$conditions));
        // $st->bindValue(':empresa', $this->getEmpresa(),PDO::PARAM_INT);
        // $st->bindValue(':q', '%' . $r->q . '%' ,PDO::PARAM_INT);
        
        // $db->stExec($st);
        // $results = $st->fetchAll(PDO::FETCH_OBJ);
        
        // foreach ($results as $fc){
            // echo Autocompleter::getItemRow(array(
                // $fc->dq_factura,
                // $fc->dq_folio,
                // $fc->dg_razon,
                // $fc->dc_factura,
                // $fc->dq_neto,
            // ));
        // }
    // }
    
    private function getDatosContrato($dc_nota_venta){
        // $db = $this->getConnection();
        // $table = ' tb_nota_venta_contrato nvc ';
        // $table .= ' JOIN tb_nota_venta_contrato_detalle nvd ON nvc.dc_contrato = nvd.dc_contrato ';
        // $fields = 'nvc.dc_contrato, nvc.dc_cuotas, count(nvd.dc_factura) dc_facturadas';
        // $conditions = 'nvc.dc_nota_venta = :notaVenta AND nvc.dc_empresa = :empresa';
        // $conditions .= ' GROUP BY nvc.dc_contrato, nvc.dc_cuotas ';
        // $st = $db->prepare($db->select($table, $fields, $conditions));
        // $st->bindValue(':notaVenta', $dc_nota_venta, PDO::PARAM_INT);
        // $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        // $db->stExec($st);
        // $result = $st->fetch(PDO::FETCH_OBJ);
        // if($result !== false){
            // $datos['dc_contrato'] = $result->dc_contrato;
            // $datos['dc_cuotas'] = $result->dc_cuotas;
            // $datos['dc_facturadas'] = $result->dc_facturadas;
        // }
		$cabeceraContrato = $this->getCabeceraContrato($dc_nota_venta);
		$facturasContrato = false;
		
		if($cabeceraContrato !== false){
			$facturasContrato = $this->getFacturasContrato($cabeceraContrato->dc_contrato);
		}
		
        $datos = array(
                'dc_contrato' => $cabeceraContrato ? $cabeceraContrato->dc_contrato:0,
                'dc_cuotas' => $cabeceraContrato ? $cabeceraContrato->dc_cuotas:1,
                'dc_facturadas' => !empty($facturasContrato) ? count($facturasContrato):0,
                'facturasContrato' => $facturasContrato === false ? array():$facturasContrato,
            );
        return (object)$datos;
    }
	
	private function getCabeceraContrato($dc_nota_venta){
		$db = $this->getConnection();
		
		$table = 'tb_nota_venta_contrato';
		$fields = 'dc_contrato, dq_contrato, dc_nota_venta, dc_cuotas';
		$conditions = 'dc_nota_venta = :dc_nota_venta AND dc_empresa = :empresa';
		
		$st = $db->prepare($db->select($table, $fields, $conditions));
		$st->bindValue(':dc_nota_venta', $dc_nota_venta, PDO::PARAM_INT);
		$st->bindValue(':empresa', $this->getEmpresa(),PDO::PARAM_INT);
		
		$db->stExec($st);
		
		return $st->fetch(PDO::FETCH_OBJ);
	}
	
	private function getFacturasContrato($dc_contrato){
		$db = $this->getConnection();
		
		$table = 'tb_nota_venta_contrato_detalle';
		$fields = 'dc_contrato, dc_factura, dc_numero_cuota';
		$conditions = 'dc_contrato = :dc_contrato AND dc_factura IS NOT NULL ';
		$conditions .= ' GROUP BY  dc_factura  ORDER BY dc_numero_cuota ';
		
		$st = $db->prepare($db->select($table, $fields, $conditions));
		$st->bindValue(':dc_contrato', $dc_contrato, PDO::PARAM_INT);
		
		$db->stExec($st);
		
		return $st->fetchAll(PDO::FETCH_OBJ);
	}
    
    public function asignarCostosAction(){
        $detalles = $this->validateParameters();
        //$this->prepararAsignaciones($detalles);
        $this->insertarAsignaciones($detalles);
    }
    
    private function validateParameters(){
        $this->facturaCompra = $this->getFacturaCompra();
        $this->detallesOriginales = $this->getDetallesAsignacionCostos($this->facturaCompra);
        $detallesAsignacionCostos = array();
        if($this->validateIssetFields()){
            $this->validateLength();
            $this->validateMontos();
            $this->validateNotaVenta();
            $this->validateCebe();
            $this->validateFecha();
            $this->validateCuotas();
            $this->validateFacturaVenta();
            $this->validateContrato();
            $detallesAsignacionCostos = $this->crearObjetosDetallesCostos();
            $this->validateMontosDetallesFacturaCompra($detallesAsignacionCostos);
        } else {
            $this->getErrorMan()->showWarning('No se han enviado todos los datos necesarios para la asignacion de costos
                . Verifique su información o contacte al administrador.');
        }
		
		$detalles = $this->agregarDatosContrato($detallesAsignacionCostos);
        
        return $detallesAsignacionCostos;
        
    }
    
    private function validateIssetFields(){
        $r = Factory::getRequest();
        return isset($r->dq_monto, $r->dg_descripcion, $r->dc_cebe, $r->df_fecha_asignacion, $r->dc_cuotas, $r->dc_detalle_factura_compra);
    }
    
    private function validateLength(){
        $r = Factory::getRequest();
        $totalItems = count($r->dc_nota_venta);
        $campos = array();
        $campos[] = count($r->dq_monto);
        $campos[] = count($r->dg_descripcion);
        $campos[] = count($r->dc_cebe);
        $campos[] = count($r->df_fecha_asignacion);
        $campos[] = count($r->dc_cuotas);
        $campos[] = count($r->dc_detalle_factura_compra);
		
        foreach($campos as $v){
            if($v != $totalItems){
                $this->getErrorMan()->showWarning('Existe un error en la cantidad de asignaciones. Contacte al administrador.');
                exit;
            }
        }
    }
    
    private function validateMontos(){
        $r = Factory::getRequest();
        $sumaMontos = 0;
        foreach($r->dq_monto as $v){
            if($v <= 0){
                $this->getErrorMan()->showWarning('Se ha asignado un costo negativo o de valor 0. Verifique su información e intente nuevamente');
                exit;
            }
            $sumaMontos += $v;
        }
        if($sumaMontos > $this->facturaCompra->dq_neto){
            $this->getErrorMan()->showWarning('La suma de los montos excede al valor neto de la factura. Verifique su información e intente nuevamente');
            exit;
        }
    }
    
    private function validateNotaVenta(){
        $r = Factory::getRequest();
        $db = $this->getConnection();
        $table = 'tb_nota_venta';
        $fields = 'dc_nota_venta, dq_nota_venta';
        $conditions = 'dc_nota_venta = :notaVenta AND dc_empresa = :empresa AND dm_nula = 0';
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':empresa',$this->getEmpresa(), PDO::PARAM_INT);
        $notasVenta = array();
        foreach($r->dc_nota_venta as $v){
            $st->bindParam(':notaVenta', $v, PDO::PARAM_INT);
            $db->stExec($st);
            $notaVenta = $st->fetch(PDO::FETCH_OBJ);
            if($notaVenta === false){
                $this->getErrorMan()->showWarning('Existe un error al consultar la nota de venta, o ésta se encuentra nula. Verifique su información o contacte al administrador');
                exit;
            }
            $datosContrato = $this->getDatosContrato($notaVenta->dc_nota_venta);
            $nv = array_merge((array)$notaVenta, (array)$datosContrato);
            $notasVenta[] = (object)$nv;
        }
        
        $this->notasVenta = $notasVenta;
        
    }
    
    private function validateCebe(){
        $r = Factory::getRequest();
        $cebes = $this->getCebe();
        $idCebes = array_keys($cebes);
        
        foreach($r->dc_cebe as $v){
            if(!in_array($v, $idCebes)){
                $this->getErrorMan()->showWarning('Existe un valor erroneo uno o varios CeBes seleccionados. Contacte al administrador.');
                exit;
            }
        }
    }
    
    private function validateFecha(){
        $r = Factory::getRequest();
        foreach($r->df_fecha_asignacion as $v){
            if(!$this->validateDate($v, 'd/m/Y')){
                $this->getErrorMan()->showWarning('Una de las fechas posee un formato no válido. Verifique su información e intente nuevamente');
                exit;
            }
        }
    }
    
    private function validateDate($date, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    
    private function validateCuotas(){
        $r = Factory::getRequest();
        foreach($r->dc_cuotas as $c => $v){
            if($v < 1){
                $this->getErrorMan()->showWarning('El número de cuotas no puede ser menor a 1. Verifique su información e intente nuevamente.');
                exit;
            }
			//Verisón antigua, comprueba solo las cuotas restantes
            // $numeroCuotas = $this->notasVenta[$c]->dc_cuotas - $this->notasVenta[$c]->dc_facturadas;
            // if($v > $numeroCuotas){
                // $this->getErrorMan()->showWarning('El número de cuotas exceder a las cuotas restantes del contrato. Verifique su información e intente nuevamente.');
                // exit;
            // }
			//nueva versión verifica que no exceda al total de cuotas, puesto que se validará a parte las facturas anteriores.
            if($v > $this->notasVenta[$c]->dc_cuotas){
                $this->getErrorMan()->showWarning('El número de cuotas exceder al total de cuotas del contrato. Verifique su información e intente nuevamente.');
                exit;
            }
        }
    }
    
    private function validateFacturaVenta(){
        $r = Factory::getRequest();
        $db = $this->getConnection();
        $table = 'tb_factura_venta';
        $fields = 'dc_factura, dq_factura, dq_folio';
        $conditions = 'dc_factura = :facturaVenta AND dc_empresa = :empresa AND dm_nula = 0';
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':empresa',$this->getEmpresa(), PDO::PARAM_INT);
        $facturaVenta = array();
        foreach($r->dc_factura_venta as $v){
            if(empty($v) || $v == '0'){
                continue;
            }
            $st->bindParam(':facturaVenta', $v, PDO::PARAM_INT);
            $db->stExec($st);
            $facturaVenta = $st->fetch(PDO::FETCH_OBJ);
            if($facturaVenta === false){
                $this->getErrorMan()->showWarning('Existe un error al registrar la factura de venta, o ésta se encuentra nula. Verifique su información o contacte al administrador');
                exit;
            }
        }
    }
    
    private function validateContrato(){
        $r = Factory::getRequest();
        foreach($r->dm_contrato as $v){
            if($v < 0 || $v > 1){
                $this->getErrorMan()->showWarning('Existe un error al determinar si la nota de venta es por contrato. Contacte al administrador.');
                exit;
            }
        }
    }
    
    private function crearObjetosDetallesCostos(){
        $r = $this->getRequest();
        $detalles = array();
        foreach($r->dc_asignacion as $k => $v){
            $detail = new stdClass();
            $detail->dc_factura_compra = $r->dc_factura;
            $detail->dc_asignacion = $v;
            $detail->dc_nota_venta = $r->dc_nota_venta[$k];
            $detail->dc_factura_venta = empty($r->dc_factura_venta[$k]) ? null:$r->dc_factura_venta[$k];
            $detail->dm_contrato = $r->dm_contrato[$k];
            $detail->dq_monto = $r->dq_monto[$k];
            $detail->dg_descripcion = $r->dg_descripcion[$k];
            $detail->dc_cebe = $r->dc_cebe[$k];
            $detail->df_fecha_asignacion = $this->getConnection()->sqlDate2($r->df_fecha_asignacion[$k]);
            $detail->dc_cantidad_cuotas = $r->dc_cuotas[$k];
            $detail->dc_cuotas_saldadas = $r->dc_cuotas_saldadas[$k];
            $detail->dm_nula = 0;
            $detail->dc_detalle_factura_compra = $r->dc_detalle_factura_compra[$k];
            $detalles[] = $detail;
        }
        return $detalles;
    }
    
    private function validateMontosDetallesFacturaCompra($detalles){
        $totalesDetallesFactura = array();
        $descripcionProductoDetalleFactura = array();
        $idsDetallesFactura = array();
        foreach($this->facturaCompra->detalles as $v){
            $totalesDetallesFactura[$v->dc_detalle] = floatval($v->dq_total);
            $descripcionProductoDetalleFactura[$v->dc_detalle] = substr($v->dg_descripcion, 0, 35);
            $idsDetallesFactura[] = $v->dc_detalle;
        }
        
        $totalesDetallesAsignacion = array();
        foreach ($detalles as $v) {
            if(!in_array($v->dc_detalle_factura_compra,$idsDetallesFactura)){
                $this->getErrorMan()->showWarning('Uno de los detalles de la factura de compra no corresponde. Contacte al administrador.');
                exit;
            }
            if (!isset($totalesDetallesAsignacion[$v->dc_detalle_factura_compra])) {
                $totalesDetallesAsignacion[$v->dc_detalle_factura_compra] = 0;
            }
            $totalesDetallesAsignacion[$v->dc_detalle_factura_compra] += floatval($v->dq_monto);
        }
        
        foreach ($totalesDetallesAsignacion as $c => $v) {
            if($totalesDetallesFactura[$c] < $v) {
                $this->getErrorMan()->showWarning("Se han excedido el monto utilizado para el detalle 
                    '{$descripcionProductoDetalleFactura[$c]}...' (Monto Máximo: {$totalesDetallesFactura[$c]}. Monto utilizado: {$v}). 
                    Verifique su información e intente nuevamente.");
                exit;
            }
        }
        
    }
	
	private function agregarDatosContrato($detalles){
		foreach($detalles as $k => $v){
			if($v->dm_contrato != 1){
				continue;
			}
			
			$cabeceraContrato = $this->getCabeceraContrato($v->dc_nota_venta);
			
			if ($cabeceraContrato === false){
				$this->getErrorMan()->showWarning('Error al obtener los datos del contrato. Contacte al administrador');
				exit;
			}
			
			$facturasContrato = $this->getFacturasContrato($cabeceraContrato->dc_contrato);
			
			if ($facturasContrato === false){
				$this->getErrorMan()->showWarning('Error al obtener los datos del contrato. Contacte al administrador');
				exit;
			}
			
			$detalles[$k]->dc_contrato = $cabeceraContrato->dc_contrato;
			$detalles[$k]->dc_cuotas = $cabeceraContrato->dc_cuotas;
			$detalles[$k]->dc_facturadas = count($facturasContrato);
			$detalles[$k]->facturasContrato = $facturasContrato;
			
			return $detalles;
			
		}
	}
    
    private function insertarAsignaciones($detalles){
        $detallesActualizados = $this->getUpdateDetails($detalles);
        $detallesAnulados = $this->getNullsetDetails($detalles);
        $detallesNuevos = $this->getNewDetails($detalles);
		//validar los detalles
		$this->validarDetallesActualizados($detallesActualizados);
		$this->validarDetallesAnulados($detallesAnulados);
		$this->validarDetallesnuevos($detallesNuevos);
        
		$updateDetails = array_merge($detallesActualizados, $detallesAnulados);
        $this->validarCantidadDetallesOriginales($updateDetails);
        
        $this->procesarAsignaciones($detallesNuevos, $updateDetails);
        
    }
    
    private function getUpdateDetails($detalles){
        $up = array();
        $idDetalleOriginal = array();
        
        foreach($this->detallesOriginales as $v){
            $idDetalleOriginal[] = $v->dc_asignacion;
        }
        
        foreach($detalles as $v){
            if(in_array($v->dc_asignacion, $idDetalleOriginal)){
                $up[] = $v;
            }
        }
        
        return $up;
    }
    
    private function getNullsetDetails($detalles){
        $ids = array();
        foreach($detalles as $v){
            if(!empty($v->dc_asignacion)){
                $ids[] = $v->dc_asignacion;
            }
        }
        $up = array();
        
        foreach ($this->detallesOriginales as $v){
            if(!in_array($v->dc_asignacion, $ids)){
                $v->dm_nula = 1;
                $up[] = $v;
            }
        }
        
        return $up;
        
    }
    
    private function getNewDetails($detalles){
        $new = array();
        foreach ($detalles as $v){
            if(empty($v->dc_asignacion)){
                $new[] = $v;
            }
        }
        return $new;
    }
	
	private function validarDetallesActualizados($detalles){
		foreach($detalles as $det){
			foreach($this->detallesOriginales as $dor){
				if($det->dc_asignacion != $dor->dc_asignacion){
					continue;
				}
				
				$diff = array_diff_assoc((array)$det, (array)$dor);
				
				if(count($diff) > 0){
					$msg = 'La edición de detalles anteriormente ingresados no está disponible.';
					$msg .= ' Para ello, elimine el detalle e ingrese los datos nuevamente.';
					
					$this->getErrorMan()->showWarning($msg);
					exit;	
				}
			}
		}
	}
	
	private function validarDetallesAnulados($detalles){
		foreach($detalles as $v){
			if($this->validarCentralizacionFactura($v->dc_factura_venta)){
				$msg = 'No es posible eliminar el costo asignado debido a que el costo de la factura de venta ya fue centralizado.';
				$msg .= ' Verifique su información o contacte al administrador. ';
				
				$this->getErrorMan()->showWarning($msg);
				exit;
			}
		}
	}
	
	private function validarCentralizacionFactura($dc_factura){
		if(!isset($dc_factura)){
			return false;
		}
		$db = $this->getConnection();
		return $db->getRowById('tb_factura_venta', $dc_factura, 'dc_factura')->dm_margen_centralizada;
	}
	
	private function validarDetallesNuevos($detalles){
		foreach($detalles as $v){
			if($this->validarCentralizacionFactura($v->dc_factura_venta)){
				$msg = 'No es posible asignar el costo por que se ha seleccionado una factura de venta que ha sido centralizada por sus costos.';
				$msg .= ' Verifique su información o contacte al administrador. ';
				
				$this->getErrorMan()->showWarning($msg);
				exit;
			}
			
			if($v->dm_contrato){
				$this->validarFacturasContrato($v);
			}
			
		}
	}
	
	private function validarFacturasContrato($detalle){
		
		if(!$this->validarCentralizacionFactura($detalle->dc_factura_venta)){
			$bool = true;
			$cuotas = 0;
			
			foreach($detalle->facturasContrato as $v){
				if($detalle->dc_factura_venta == $v->dc_factura){
					$bool = false;
					$cuotasRestantes = $detalle->dc_cuotas - $v->dc_numero_cuota;
					$this->validarCuotasRestantes($cuotasRestantes, $detalle->dc_cantidad_cuotas);
				}
				
				if($bool){
					continue;
				} else {
					$cuotas++;
				}
				
				if($this->validarCentralizacionFactura($v->dc_factura)){
					$msg = 'No es posible asignar el costo debido a que la factura seleccionada o una de las siguientes';
					$msg .= ' se encuentra centralizada. Verifique su información o contacte al administrador. ';
					
					$this->getErrorMan()->showWarning($msg);
					exit;
				}
				
				if($cuotas == $detalle->dc_cantidad_cuotas){
					break;
				}
			}
		} else {
			$msg = 'No es posible asignar el costo debido a que la factura seleccionada o una de las siguientes';
			$msg .= ' se encuentra centralizada. Verifique su información o contacte al administrador. ';
			
			$this->getErrorMan()->showWarning($msg);
			exit;
		}
	}
	
	private function validarCuotasRestantes($cuotasRestantes, $cuotas){
		if($cuotasRestantes < $cuotas){
			$msg = 'No es posible asignar un costo debido a que el numero de cuotas asignadas supera al numero de cuotas';
			$msg .= " restantes a partir de la factura seleccionada ({$cuotasRestantes}). Verifique su información o contacte al administrador. ";
			
			$this->getErrorMan()->showWarning($msg);
			exit;
		}
	}
    
    private function validarCantidadDetallesOriginales($updateDetails){
        if(count($updateDetails) != count($this->detallesOriginales)){
            $this->getErrorMan()->showWarning('Se ha detectado un error con el conteo de detalles. Contacte al administrador.');
        }
    }
    
    private function procesarAsignaciones($detallesNuevos, $updateDetails) {
        $this->getConnection()->start_transaction();
        $this->actualizarDetalles($updateDetails);
		$this->actualizarComprobantes($updateDetails);
        $detallesNuevosConId = $this->insertarDetalles($detallesNuevos);
		$this->insertarComprobantes($detallesNuevosConId);
        $this->getConnection()->commit();
        $this->getErrorMan()->showConfirm('Se han procesado las asignaciones correctamente.');
    }
    
    private function actualizarDetalles($detalles){
        $db = $this->getConnection();
        $table = 'tb_factura_compra_asignacion_costos';
        $fields = array(
            'dc_nota_venta' => ':dc_nota_venta',
            'dm_contrato' => ':dm_contrato',
            'dq_monto' => ':dq_monto',
            'dg_descripcion' => ':dg_descripcion',
            'dc_cebe' => ':dc_cebe',
            'df_fecha_asignacion' => ':df_fecha_asignacion',
            'dc_cantidad_cuotas' => ':dc_cantidad_cuotas',
            'dm_nula' => ':dm_nula',
        );
        $conditions = 'dc_asignacion = :dc_asignacion AND dc_factura_compra = :dc_factura_compra';
        
        $st = $db->prepare($db->update($table, $fields, $conditions));
        
        foreach($detalles as $v){
            $st->bindParam(':dc_nota_venta', $v->dc_nota_venta, PDO::PARAM_INT);
            $st->bindParam(':dm_contrato', $v->dm_contrato, PDO::PARAM_STR);
            $st->bindParam(':dq_monto', $v->dq_monto, PDO::PARAM_STR);
            $st->bindParam(':dg_descripcion', $v->dg_descripcion, PDO::PARAM_STR);
            $st->bindParam(':dc_cebe', $v->dc_cebe, PDO::PARAM_INT);
            $st->bindParam(':df_fecha_asignacion', $v->df_fecha_asignacion, PDO::PARAM_STR);
            $st->bindParam(':dc_cantidad_cuotas', $v->dc_cantidad_cuotas, PDO::PARAM_INT);
            $st->bindParam(':dm_nula', $v->dm_nula, PDO::PARAM_INT);
            $st->bindParam(':dc_asignacion', $v->dc_asignacion, PDO::PARAM_INT);
            $st->bindParam(':dc_factura_compra', $v->dc_factura_compra, PDO::PARAM_INT);
            
            $db->stExec($st);
        }
        
    }
	
	private function actualizarComprobantes($detalles){
		foreach($detalles as $v){
			if($v->dm_nula == 1 || $v->dc_factura_venta === null){
				$this->eliminarComprobanteAsignacionCosto($v);
			}
		}
	}
	
	private function eliminarComprobanteAsignacionCosto($asignacionCosto){
		$db = $this->getConnection();
		
		$table = 'tb_comprobante_asignacion_costos';
		$conditions = 'dc_asignacion_costo = :asignacion';
		
		$st = $db->prepare($db->delete($table, $conditions));
		$st->bindValue(':asignacion', $asignacionCosto->dc_asignacion, PDO::PARAM_INT);
		
		$db->stExec($st);
	}
	
	private function insertarComprobantes($detalles){
		foreach($detalles as $v){
			if($v->dc_factura_venta === null) {
				continue;
			} elseif($v->dm_contrato == 1) {
				$this->insertarComprobanteAsignacionCostoContrato($v);
			} else {
				$this->insertarComprobanteAsignacionCosto($v);
			}
		}
	}
	
	private function insertarComprobanteAsignacionCostoContrato($asignacionCosto){
		
		if(!isset($asignacionCosto->dc_factura_venta)){
			return;
		}
		
		$bool = false;
		
		foreach($asignacionCosto->facturasContrato as $v){
			if($v->dc_factura == $asignacionCosto->dc_factura_venta){
				$bool = true;
			}
			
			if($bool){
				$this->insertarComprobanteAsignacionCosto($asignacionCosto, $v->dc_factura);
			}
		}
		
	}
	
	private function insertarComprobanteAsignacionCosto($asignacionCosto, $facturaVenta = null){
		$db = $this->getConnection();
		
		$table = 'tb_comprobante_asignacion_costos';
		$fields = array(
			'dc_asignacion_costo' => ':dc_asignacion_costo',
			'dc_factura_compra' => ':dc_factura_compra',
			'dc_factura_venta' => ':dc_factura_venta',
			'dq_monto' => ':dq_monto',
		);
		
		$facturaVenta = isset($facturaVenta) ? $facturaVenta:$asignacionCosto->dc_factura_venta;
		
		$monto = round($asignacionCosto->dq_monto / $asignacionCosto->dc_cantidad_cuotas , 2); 
		
		$st = $db->prepare($db->insert($table, $fields));
		$st->bindValue(':dc_asignacion_costo', $asignacionCosto->dc_asignacion, PDO::PARAM_INT);
		$st->bindValue(':dc_factura_compra', $asignacionCosto->dc_factura_compra, PDO::PARAM_INT);
		$st->bindValue(':dc_factura_venta', $facturaVenta, PDO::PARAM_INT);
		$st->bindValue(':dq_monto', $monto, PDO::PARAM_STR);
		
		$db->stExec($st);
		$updateAsignacionCosto = array(
			'dc_cuotas_saldadas' => 'dc_cuotas_saldadas + 1'
		);
		$uac = $db->prepare($db->update('tb_factura_compra_asignacion_costos', $updateAsignacionCosto, 'dc_asignacion = :asignacion'));
		$uac->bindValue(':asignacion', $asignacionCosto->dc_asignacion, PDO::PARAM_INT);
		$db->stExec($uac);
		
	}
    
    private function insertarDetalles($detalles){
        $db = $this->getConnection();
        $table = 'tb_factura_compra_asignacion_costos';
        $fields = array(
            'dc_factura_compra' => ':dc_factura_compra',
            'dc_nota_venta' => ':dc_nota_venta',
            'dm_contrato' => ':dm_contrato',
            'dq_monto' => ':dq_monto',
            'dg_descripcion' => ':dg_descripcion',
            'dc_detalle_factura_compra' => ':dc_detalle_factura_compra',
            'dc_cebe' => ':dc_cebe',
            'df_fecha_asignacion' => ':df_fecha_asignacion',
            'dc_cantidad_cuotas' => ':dc_cantidad_cuotas',
            'dc_empresa' => ':dc_empresa',
            'df_fecha_creacion' => $db->getNow(),
            'dc_usuario_creacion' => ':dc_usuario_creacion',
            'dm_nula' => ':dm_nula',
        );
        
        $st = $db->prepare($db->insert($table, $fields));
        
        foreach($detalles as $c => $v){
            $st->bindParam(':dc_factura_compra', $v->dc_factura_compra, PDO::PARAM_INT);
            $st->bindParam(':dc_nota_venta', $v->dc_nota_venta, PDO::PARAM_INT);
            $st->bindParam(':dm_contrato', $v->dm_contrato, PDO::PARAM_STR);
            $st->bindParam(':dq_monto', $v->dq_monto, PDO::PARAM_STR);
            $st->bindParam(':dg_descripcion', $v->dg_descripcion, PDO::PARAM_STR);
            $st->bindParam(':dc_detalle_factura_compra', $v->dc_detalle_factura_compra, PDO::PARAM_STR);
            $st->bindParam(':dc_cebe', $v->dc_cebe, PDO::PARAM_INT);
            $st->bindParam(':df_fecha_asignacion', $v->df_fecha_asignacion, PDO::PARAM_STR);
            $st->bindParam(':dc_cantidad_cuotas', $v->dc_cantidad_cuotas, PDO::PARAM_INT);
            $st->bindParam(':dc_empresa', $this->getEmpresa(), PDO::PARAM_INT);
            $st->bindParam(':dc_usuario_creacion', $this->getUserData()->dc_usuario, PDO::PARAM_INT);
            $st->bindParam(':dm_nula', $v->dm_nula, PDO::PARAM_INT);
            
            $db->stExec($st);
			$detalles[$c]->dc_asignacion = $db->lastInsertId();
        }
		
		return $detalles;
    }
    
    private function getFacturasVenta($dc_nota_venta, $dm_margen_centralizada = false){
        $db = $this->getConnection();
        $table = 'tb_factura_venta';
        $fields = 'dc_factura, dq_factura, dq_folio, dm_margen_centralizada';
        $conditions = 'dc_nota_venta = :notaVenta AND dc_empresa = :empresa AND dm_nula = 0 ';
        $conditions .= $dm_centralizada ? ' AND dm_margen_centralizada = 0 ':'';
        $st =$db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':notaVenta', $dc_nota_venta, PDO::PARAM_INT);
        $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($st);
        
        return $st->fetchAll(PDO::FETCH_OBJ);
    }
}
