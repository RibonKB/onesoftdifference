<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_detalle = intval($_POST['id']);

$detalle = $db->prepare($db->select('tb_factura_compra_detalle d
JOIN tb_producto p ON p.dc_producto = d.dc_producto',
'd.dc_cantidad, d.dq_precio, p.dg_codigo, p.dg_producto',
'd.dc_detalle = ?'));
$detalle->bindValue(1,$dc_detalle,PDO::PARAM_INT);
$db->stExec($detalle);
$detalle = $detalle->fetch(PDO::FETCH_OBJ);

if($detalle === false){
	$error_man->showWarning('El detalle seleccionado es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Edición detalles factura de compra
</div>
<div class="panes">
	<?php $form->Start('sites/ventas/factura_compra/proc/editar_detalle_factura.php','ed_detalle_factura') ?>
    <?php $form->Header('<strong>Indique los datos actualizados del detalle a modificar</strong><br />Los campos marcados con [*] son obligatorios') ?>
    	<table class="tab" width="500" align="center">
        	<caption>Datos del detalle</caption>
            <tbody>
            	<tr>
                	<td width="100">Código</td>
                    <td><b><?php echo $detalle->dg_codigo ?></b></td>
                </tr>
                <tr>
                	<td>Producto</td>
                    <td><b><?php echo $detalle->dg_producto ?></b></td>
                </tr>
                <tr>
                	<td>Cantidad</td>
                    <td><b><?php echo $detalle->dc_cantidad ?></b></td>
                </tr>
                <tr>
                	<td>Precio actual</td>
                    <td><b><?php echo moneda_local($detalle->dq_precio) ?></b></td>
                </tr>
            </tbody>
        </table>
    <?php $form->Group() ?>
    	<?php $form->Text('Nuevo precio para el detalle','dq_precio',1,Form::DEFAULT_TEXT_LENGTH,$detalle->dq_precio) ?>
        <?php $form->Hidden('dc_detalle',$dc_detalle) ?>
    <?php $form->End('Editar','editbtn') ?>
</div>