<?php

require_once('sites/contabilidad/stuff.class.php');

/**
 * Description of AnularFacturaCompraFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 28-08-2013
 */
class AnularFacturaCompraFactory extends Factory{
  
    protected $title = "Anular Factura de Compra";
    private $factura;
    
    public function indexAction(){
        $this->factura = $this->getFacturaCompra(self::getRequest()->id);
        
        if($this->factura === false || $this->factura->dc_empresa != $this->getEmpresa()):
          $this->getErrorMan()->showWarning('La factura de compra especificada es inválida');
          exit;
        endif;
        
        $this->validarPeriodoContable();
        $this->validarGuiasRecepcion();
        $this->validarPagos();
        
        $form = $this->getFormView($this->getTemplateURL('index.form'), array(
            'factura' => $this->factura
        ));
        
        echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
        
    }
    
    private function getFacturaCompra($dc_factura){
        return $this->getConnection()->getRowById('tb_factura_compra', $dc_factura, 'dc_factura');
    }
    
    private function validarPeriodoContable(){
      $df_libro_compra = new DateTime($this->factura->df_libro_compra);
      $dc_mes = $df_libro_compra->format('m');
      $dc_anho = $df_libro_compra->format('Y');
      $dm_estado_periodo_contable = ContabilidadStuff::getEstadoPeriodo(
              $dc_mes,
              $dc_anho,
              ContabilidadStuff::FIELD_ESTADO_FC,
              $this->getConnection(),
              $this->getEmpresa(),
              $this->getUserData()->dc_usuario);
      
      if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_CERRADO):
        $this->getErrorMan()->showWarning('No puede anular facturas de compra que se encuentran en periodos contables cerrados.');
        exit;
      endif;
      
    }
    
    private function validarGuiasRecepcion(){
        $db = $this->getConnection();
        
        $guias = $db->prepare($db->select('tb_guia_recepcion', 'dq_guia_recepcion, df_fecha_emision', 'dc_factura = ? AND dm_nula = 0'));
        $guias->bindValue(1, $this->factura->dc_factura, PDO::PARAM_INT);
        $db->stExec($guias);
        $guias_collection = $guias->fetchAll(PDO::FETCH_OBJ);
        
        if(count($guias_collection)):
          echo $this->getOverlayView($this->getTemplateURL('listadoGuiasVigentes.error'), array(
              'guias' => $guias_collection
          ));
          exit;
        endif;
        
    }
    
    private function validarPagos(){
      if($this->factura->dq_monto_pagado > 0):
        $this->getErrorMan()->showWarning('No se puede anular la factura de compra debido a que a esta se le han realizado gestiones de pago');
        exit;
      endif;
    }
    
}

?>
