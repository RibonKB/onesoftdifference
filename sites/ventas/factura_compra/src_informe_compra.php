<?php
define("MAIN",1);
require_once("../../../inc/init.php");

include_once("../../../inc/form-class.php");
$form = new Form($empresa);
?>
<div id="secc_bar">Libro de compras</div>
<div id="main_cont">
	<div class="panes">
		<?php
            $form->Start("sites/ventas/factura_compra/proc/src_informe_compra.php","src_informe_compra");
            $form->Header("<strong>Indicar los parámetros de consulta del libro de compras</strong>");
        ?>

		<table class="tab" id="form_container" width="100%">
        	<tbody>
            	<tr>
                	<td width="50">Número de factura</td>
                    <td width="280"><?php $form->Text("Desde","dq_factura_desde") ?></td>
                    <td width="280"><?php $form->Text('Hasta','dq_factura_hasta') ?></td>
                </tr>
                <tr>
                	<td>Fecha emisión</td>
                    <td><?php $form->Date('Desde','df_emision_desde',1,"01/".date("m/Y")) ?></td>
                    <td><?php $form->Date('Hasta','df_emision_hasta',1,0) ?></td>
                </tr>
                <tr>
                	<td>Proveedor</td>
                    <td><?php $form->DBMultiSelect('','dc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon')) ?></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td>Marca</td>
                    <td><?php $form->DBMultiSelect('','dc_marca','tb_marca',array('dc_marca','dg_marca')) ?></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td>Linea de negocio</td>
                    <td><?php $form->DBMultiSelect('','dc_linea_negocio','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio')) ?></td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
	<?php $form->End('Ejecutar consulta','searchbtn') ?>
</div>
</div>
<script type="text/javascript">
$('#dc_proveedor, #dc_marca, #dc_linea_negocio').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>