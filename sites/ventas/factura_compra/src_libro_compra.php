<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Libro de compras</div>
<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/factura_compra/proc/src_libro_compra.php","src_libro_compra",'overlayValidar');
	$form->Header("<strong>Indicar los parámetros de consulta del libro de compras</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50">Número de factura</td><td width="280">');
	$form->Text("Desde","fc_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','fc_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','fc_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','fc_emision_hasta',1,0);
	echo('</td></tr><tr><td>Proveedor</td><td>');
	$form->ListadoMultiple('','fc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon'));
	echo('</td><td>&nbsp;</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#fc_proveedor').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>