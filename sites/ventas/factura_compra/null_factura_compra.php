<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div class="secc_bar">Anular factura de compra</div><div class="panes">');

$dc_factura = intval($_POST['id']);

$data = $db->select('tb_factura_compra','dq_factura, MONTH(df_libro_compra) dc_mes_contable, YEAR(df_libro_compra) dc_anho_contable',"dc_factura = {$dc_factura}");
$data = array_shift($data);

if($data == NULL){
	$error_man->showWarning('El número de factura de compra especificado es inválido');
	exit;
}

require_once("../../contabilidad/stuff.class.php");

/**
*	Comprobar Estado del periodo contable en el que se está la factura de venta
**/

$dm_estado_periodo_contable = ContabilidadStuff::getEstadoPeriodo_OLD($data['dc_mes_contable'],$data['dc_anho_contable'],ContabilidadStuff::FIELD_ESTADO_FC);

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_CERRADO){
	$error_man->showWarning('No puede anular facturas de compra que se encuentran en periodos contables cerrados.');
	exit;
}

//END ESTADO CONTABLE

$dq_factura = $data['dq_factura'];

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/factura_compra/proc/null_factura_compra.php','null_factura_form');
$form->Header('Indique las razones por las que se anulará la factura de compra');
$error_man->showAviso("Atención, está a punto de anular la factura N° <b>{$dq_factura}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('id_factura',$dc_factura);
$form->End('Anular','delbtn');

?>
</div></div>