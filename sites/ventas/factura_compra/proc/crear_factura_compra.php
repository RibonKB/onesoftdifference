<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once('../../proc/ventas_functions.php');
require_once("../../../contabilidad/stuff.class.php");

if(!is_numeric($_POST['fv_number'])){
	$error_man->showWarning("El folio de la factura debe ser un número válido");
	exit;
}

$valida_numero = $db->doQuery(
					$db->select('tb_factura_compra','1',
					"dq_folio={$_POST['fv_number']} AND dc_proveedor = {$_POST['prov_id']} AND dc_empresa={$empresa} AND dm_nula = '0'")
				 )->fetch();

if($valida_numero !== false){
	$error_man->showWarning("El número de factura <b>{$_POST['fv_number']}</b> es inválido pues ya existe una factura de compra con dicho número.");
	exit;
}

/**
*	Comprobar Estado del periodo contable en el que se agregará la factura de compra
**/

$mes_contable = substr($_POST['fv_fecha_contable'],3,2);
$anho_contable = substr($_POST['fv_fecha_contable'],6);

$dm_estado_periodo_contable = ContabilidadStuff::getEstadoPeriodo($mes_contable,$anho_contable,ContabilidadStuff::FIELD_ESTADO_FC);

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_INEXISTENTE){
	$error_man->showWarning('El periodo contable en el que intenta agregar la factura de compra no existe');
	exit;
}

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_CERRADO){
	$error_man->showWarning('El periodo contable en el que intenta agregar la factura de compra se encuentra cerrado.');
	exit;
}

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_GRUPO_DENEGADO){
	$error_man->showWarning('Su grupo de usuario no tiene permiso para generar facturas de compra en el periodo contable seleccionado.');
	exit;
}

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_USUARIO_DENEGADO){
	$error_man->showWarning('No tiene permiso para generar facturas de compra en el periodo contable seleccionado.');
	exit;
}

//END ESTADO CONTABLE

$codes = implode("','",$_POST['prod']);

$db_prod = $db->doQuery($db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_codigo IN ('{$codes}')) p
JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto",
'dc_producto,dg_codigo'));

$ids = array();
foreach($db_prod as $p){
	$ids[$p['dg_codigo']] = $p['dc_producto'];
}
unset($db_prod);

$exento = isset($_POST['fc_exento'])?str_replace(',','',$_POST['fc_exento']):0;

$db->start_transaction();

$fc_num = doc_GetNextNumber('tb_factura_compra','dq_factura',4,'df_creacion');
$tipo_factura = explode('|',$_POST['fc_tipo_factura']);

$factura_compra = $db->prepare($db->insert('tb_factura_compra',array(
	'dc_proveedor' => $_POST['prov_id'],
	'dc_medio_pago' => $_POST['fv_medio_pago'],
	'dc_tipo' => $tipo_factura[0],
	'dq_factura' => $fc_num,
	'dq_folio' => '?',
	'dg_comentario' => '?',
	'dg_referencia' => '?',
	'dc_guia_recepcion' => '?',
	'df_emision' => $db->sqlDate($_POST['fv_emision']),
	'df_creacion' => $db->getNow(),
	'df_vencimiento' => $db->sqlDate($_POST['fv_vencimiento']),
	'df_libro_compra' => $db->sqlDate($_POST['fv_fecha_contable']),
	'dq_exento' => $exento,
	'dq_descuento' => str_replace(',','',$_POST['fc_descuento']),
	'dq_neto' => str_replace(',','',$_POST['fc_neto']),
	'dq_iva' => str_replace(',','',$_POST['fc_iva']),
	'dq_total' => str_replace(',','',$_POST['fc_total']),
	'dc_empresa' => $empresa
)));

$factura_compra->bindValue(1,$_POST['fv_number'],PDO::PARAM_INT);
$factura_compra->bindValue(2,$_POST['fv_comentario'],PDO::PARAM_STR);
$factura_compra->bindValue(3,$_POST['fv_referencia'],PDO::PARAM_STR);
$factura_compra->bindValue(4,$_POST['fv_id_guia_recepcion'],PDO::PARAM_INT);

$db->stExec($factura_compra);

$factura_compra = $db->lastInsertId();

$detalle_st = $db->prepare($db->insert('tb_factura_compra_detalle',array(
		'dc_factura' => $factura_compra,
		'dc_producto' => '?',
		'dc_cantidad' => '?',
		'dq_precio' => '?',
		'dq_total' => '?',
		'dc_ceco' => '?',
		'dc_cuenta_servicio' => '?',
		'dg_descripcion' => '?',
	)));
	
$detalle_st->bindParam(1,$producto,PDO::PARAM_INT);
$detalle_st->bindParam(2,$cantidad,PDO::PARAM_INT);
$detalle_st->bindParam(3,$precio,PDO::PARAM_STR);
$detalle_st->bindParam(4,$total,PDO::PARAM_STR);
$detalle_st->bindParam(5,$ceco,PDO::PARAM_STR);
$detalle_st->bindParam(7,$descripcion,PDO::PARAM_STR);
$detallesFactura = array();
foreach($_POST['prod'] as $i => $v){
	if(!isset($ids[$v])){
		$error_man->showWarning("El producto <b>{$v}</b> no ha sido creado, puede quitarlo y volver a agregarlo.");
		$db->rollBack();
		exit;
	}
	
	$producto = $ids[$v];
	$cantidad = $_POST['cant'][$i];
	$precio = str_replace(',','',$_POST['precio'][$i]);
	$total = $precio*$cantidad;
	$ceco = $_POST['ceco'][$i];
	$descripcion = $_POST['desc'][$i];
	
	if($tipo_factura[1] == 0){
		$detalle_st->bindValue(6,NULL,PDO::PARAM_NULL);
	}else{
		$detalle_st->bindValue(6,$_POST['dc_cuenta_contable_servicio'][$i],PDO::PARAM_INT);
	}
	
	$db->stExec($detalle_st);
	$detallesFactura[] = $db->getRowById('tb_factura_compra_detalle', $db->lastInsertId(), 'dc_detalle');
}

/**
 * Validación de las guías de recepción y si los detalles de estas se encuentran en la factura de compra y en la misma cantidad.
 */
if(isset($_POST['dc_guia_recepcion'])){
    //obtención de la factura. 
    $facturaCompra = $db->getRowById('tb_factura_compra', $factura_compra, 'dc_factura');
    $facturaCompra->detalles = $detallesFactura;

    //recorrer las guías de recepción
    $guiasRecepcion = array();
    $dgrTable = 'tb_guia_recepcion_detalle';
    $dgrFields = 'dc_detalle, dc_guia_recepcion, dc_producto, dc_detalle_factura_compra, dq_cantidad';
    $dgrConditions = 'dc_guia_recepcion = ?';
    $dgrSt = $db->prepare($db->select($dgrTable, $dgrFields, $dgrConditions));
    $idGuia = null;
    $dgrSt->bindParam(1, $idGuia, PDO::PARAM_INT);
    foreach($_POST['dc_guia_recepcion'] as $c => $v){
        $guiasRecepcion[$c] = $db->getRowById('tb_guia_recepcion', $v,'dc_guia_recepcion');
        $idGuia = $v;
        $db->stExec($dgrSt);
        $guiasRecepcion[$c]->detalles = $dgrSt->fetchAll(PDO::FETCH_OBJ);
    }
    $asignaciones = array();
    foreach($guiasRecepcion as $gr){
        foreach($gr->detalles as $dgr){
            $obj = new stdClass();
            $obj->detalleGuiaRecepcion = $dgr->dc_detalle;
            $obj->idGuiaRecepcion = $gr->dc_guia_recepcion;
            $obj->numeroGuiaRecepcion = $gr->dq_guia_recepcion;
            foreach($facturaCompra->detalles as $dfc) {
                if($dfc->dc_producto != $dgr->dc_producto){
                    continue;
                }
                $obj->detalleFacturaCompra = $dfc->dc_detalle;
                if($dgr->dq_cantidad > $dfc->dc_cantidad){
                    continue;
                }
                $obj->cantidad = $dgr->dq_cantidad;
            }
            $asignaciones[$dgr->dc_detalle] = $obj;
        }
    }
    
    //verificar las asignaciones
    $detalleGuiaRecepcion = null;
    $detalleFacturaCompra = null;
    $cantidad = null;
    
    $udgrTable = 'tb_guia_recepcion_detalle';
    $udgrFields = array('dc_detalle_factura_compra' => '?');
    $udgrConditions = 'dc_detalle = ?';
    $udgrSt = $db->prepare($db->update($udgrTable, $udgrFields, $udgrConditions));
    $udgrSt->bindParam(1, $detalleFacturaCompra, PDO::PARAM_INT);
    $udgrSt->bindParam(2, $detalleGuiaRecepcion, PDO::PARAM_INT);
    
    
    $udfcTable = 'tb_factura_compra_detalle';
    $udfcFields = array('dc_recepcionada' => 'dc_recepcionada + ?');
    $udfcConditions = 'dc_detalle = ?';
    $udfcSt = $db->prepare($db->update($udfcTable, $udfcFields, $udfcConditions));
    $udfcSt->bindParam(1, $cantidad, PDO::PARAM_INT);
    $udfcSt->bindParam(2, $detalleFacturaCompra, PDO::PARAM_INT);
    
    $guiasRecepcion = array();
    
    foreach($asignaciones as $v) {
        if(!isset($v->detalleFacturaCompra)){
            $error_man->showWarning("La guia de recepción {$v->dq_guia_recepcion} no posee un detalle que se pueda 
                relacionar con los de la factura recien creada. Verifique su información e intente nuevamente");
            exit;
        }
        if(!isset($v->cantidad)){
            $error_man->showWarning("La guia de recepción {$v->dq_guia_recepcion} está recepcionando una cantidad de producto 
                mayor a la que se está asignando en la factura recien creada. Verifique su información");
            exit;
        }
        
        //Actualizar Detalles Guia Recepción y factura
        $detalleGuiaRecepcion = $v->detalleGuiaRecepcion;
        $detalleFacturaCompra = $v->detalleFacturaCompra;
        $cantidad = $v->cantidad;
        
        $db->stExec($udgrSt);
        $db->stExec($udfcSt);
        
        $guiasRecepcion[$v->idGuiaRecepcion] = $v->idGuiaRecepcion;
    }
    
    //Actualizar las guias de recepcion
    $ugrTable = 'tb_guia_recepcion';
    $ugrFields = array('dc_factura' => '?');
    $ugrConditions = 'dc_guia_recepcion = ?';
    $ugrSt = $db->prepare($db->update($ugrTable, $ugrFields, $ugrConditions));
    $fc = null;
    $gr = null;
    $ugrSt->bindParam(1, $fc, PDO::PARAM_INT);
    $ugrSt->bindParam(2, $gr, PDO::PARAM_INT);
    foreach($guiasRecepcion as $v) {
        $fc = $facturaCompra->dc_factura;
        $gr = $v;
        $db->stExec($ugrSt);
    }
}

$db->commit();

$error_man->showConfirm("Se ha almacenado la factura de compra correctamente.");
$error_man->showInfo("El número de folio para factura física es el <strong>{$_POST['fv_number']}</strong><br />
Además de eso el número de identificación interno es el <h1 style='margin:0;color:#000;'>{$fc_num}</h1>");

?>
