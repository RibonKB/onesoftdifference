<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['fv_emision_desde'] = $db->sqlDate($_POST['fv_emision_desde']);
$_POST['fv_emision_hasta'] = $db->sqlDate($_POST['fv_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['fv_emision_desde']} AND {$_POST['fv_emision_hasta']})";

if($_POST['fv_numero_desde']){
	if($_POST['fv_numero_hasta']){
		$conditions .= " AND (dq_factura BETWEEN {$_POST['fv_numero_desde']} AND {$_POST['fv_numero_hasta']})";
	}else{
		$conditions = "(dq_factura = {$_POST['fv_numero_desde']} OR dq_folio = {$_POST['fv_numero_desde']}) AND dc_empresa = {$empresa}";
	}
}

if($_POST['fv_nota_venta'] != ''){
	$dg_nota_venta = $_POST['fv_nota_venta'];
	if(!is_numeric($dg_nota_venta)){
		$error_man->showWarning("El campo nota venta debe ser numérico. Verifique su información e intente nuevamente");
		exit();
	} 
	$nv = $db->select('tb_nota_venta', 'dc_nota_venta, dq_nota_venta', "dq_nota_venta = {$dg_nota_venta} AND dc_empresa = {$empresa}");
	if(!count($nv)){
		$error_man->showAviso("No se encontraron facturas de compra con los criterios especificados");
		exit();
	} else {
		$conditions .=  " AND dc_nota_venta = {$nv[0]['dc_nota_venta']} ";
	}
}

if(isset($_POST['fv_proveedor'])){
	$_POST['fv_proveedor'] = implode(',',$_POST['fv_proveedor']);
	$conditions .= " AND dc_proveedor IN ({$_POST['fv_proveedor']})";
}

$data = $db->select('tb_factura_compra','dc_factura,dq_factura,dq_folio,dm_nula',$conditions,array('order_by' => 'dq_factura DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron facturas de compra con los criterios especificados");
	exit();
}

echo("<div id='show_factura'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Facturas<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Factura</th>
	</tr>
</thead>
<tbody>
");

foreach($data as $c){
$null_icon = '';
if($c['dm_nula'] == 1){
	$null_icon = '<img src="images/warning.png" alt="NULA" class="right" width="20" title="Factura Anulada" />';
}
echo("<tr>
	<td align='left'>
		{$null_icon}
		<a href='sites/ventas/factura_compra/proc/show_factura_compra.php?id={$c['dc_factura']}' class='fv_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_factura']} - <b>{$c['dq_folio']}</b></a>
	</td>
</tr>");
}

?>
</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='imgbtn' id='pago_factura' style='background-image:url(images/lukas.png);'>Generar Pago</button>
<button type="button" class="imgbtn" id="asignar_guia_recepcion" style="background-image: url(images/doc.png)">Asignar Guía Recepción</button>
<button type="button" class="editbtn" id="edit_folio">Editar Folio</button>
<button type='button' class='delbtn' id='null_factura'>Anular</button>
<?php if(check_permiso(100) || true): ?>
<button type='button' class='imgbtn' id='asigna_costos' style='background-image:url(images/doc.png);'>Asignar a Costos</button>
<?php endif;?>
</div>

<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".fv_load").click(function(e){
		e.preventDefault();
		$('#show_factura').html("<img src='images/ajax-loader.gif' alt='' /> cargando factura ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('#main_cont .panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_factura');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>