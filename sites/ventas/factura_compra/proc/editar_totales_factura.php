<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_factura = intval($_POST['dc_factura']);

$factura = $db->prepare($db->select('tb_factura_compra','dq_monto_pagado','dc_factura = ? AND dc_empresa = ?'));
$factura->bindValue(1,$dc_factura,PDO::PARAM_INT);
$factura->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($factura);
$factura = $factura->fetch(PDO::FETCH_OBJ);

if($factura === false){
	$error_man->showWarning('La Factura seleccionada es inválida, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

$dq_total = floatval($_POST['dq_total']);
$dq_monto_pagado = floatval($factura->dq_monto_pagado);

if($dq_total < $dq_monto_pagado){
	$error_man->showWarning('El monto total no puede ser inferior al monto ya pagado de la factura, verifique los pagos y vuelva a intentarlo.');
	exit;
}

$db->start_transaction();

$update_factura = $db->prepare($db->update('tb_factura_compra',array(
	'dq_exento' => '?',
	'dq_neto' => '?',
	'dq_iva' => '?',
	'dq_total' => '?'
),'dc_factura = ?'));
$update_factura->bindValue(1,floatval($_POST['dq_exento']),PDO::PARAM_STR);
$update_factura->bindValue(2,floatval($_POST['dq_neto']),PDO::PARAM_STR);
$update_factura->bindValue(3,floatval($_POST['dq_iva']),PDO::PARAM_STR);
$update_factura->bindValue(4,$dq_total,PDO::PARAM_INT);
$update_factura->bindValue(5,$dc_factura,PDO::PARAM_INT);
$db->stExec($update_factura);

$db->commit();

$error_man->showConfirm('Se han cambiados los montos totales de la factura correctamente.');
?>