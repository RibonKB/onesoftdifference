<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_factura = intval($_POST['dc_factura']);
$dc_medio_pago = intval($_POST['dc_medio_pago']);

$medio_pago = $db->prepare($db->select('tb_medio_pago_proveedor',
'dc_tipo_movimiento, dm_requiere_banco, dm_asiento_intermedio, dm_requiere_documento, dc_cuenta_contable_intermedia, dg_medio_pago',
"dc_empresa = ? AND dc_medio_pago = ?"));
$medio_pago->bindValue(1,$empresa,PDO::PARAM_INT);
$medio_pago->bindValue(2,$dc_medio_pago,PDO::PARAM_INT);
$db->stExec($medio_pago);
$medio_pago = $medio_pago->fetch(PDO::FETCH_OBJ);

if($medio_pago === false){
	$error_man->showWarning('No se ha encontrado el medio de pago utilizado para hacer el pago.');
	exit;
}

//Validar que el medio de pago no haya cambiado en el formulario anterior
if(
	$medio_pago->dc_tipo_movimiento != $_POST['dc_tipo_movimiento'] ||
	$medio_pago->dm_requiere_banco != $_POST['dm_requiere_banco'] ||
	$medio_pago->dm_requiere_documento != $_POST['dm_requiere_documento']
){
	$error_man->showWarning('La configuración de Medios de pago ha cambiado, no se puede continuar debido a problemas de integridad.<br />Consulte con el administrador.');
	exit;
}

//Obtener monto adeudado factura de compra
$factura = $db->prepare($db->select('tb_factura_compra','dq_total-dq_monto_pagado dq_monto, dq_factura, dq_folio, dc_proveedor','dc_empresa = ? AND dc_factura = ?'));
$factura->bindValue(1,$empresa,PDO::PARAM_INT);
$factura->bindValue(2,$dc_factura,PDO::PARAM_INT);
$db->stExec($factura);
$factura = $factura->fetch(PDO::FETCH_OBJ);

if($factura === false){
	$error_man->showWarning('Error obteniendo la información de pago de la factura de compra');
	exit;
}

$dq_monto = floatval($factura->dq_monto);

/**
*	Obtener montos de saldos a favor para asignar al pago.
*/
if(isset($_POST['dc_factura_favor'])){
	$dc_factura_favor = implode(',',$_POST['dc_factura_favor']);
	$saldos_favor = $db->doQuery($db->select('tb_factura_compra','dc_factura, dq_saldo_favor',"dc_factura IN ({$dc_factura_favor})"));
	
	$saldo_total = 0;
	$saldo_detalle = array();
	
	while($s = $saldos_favor->fetch(PDO::FETCH_OBJ)){
		$dq_monto -= $s->dq_saldo_favor;
		
		if($dq_monto < 0){
			$saldo_final = abs($dq_monto);
			$dq_monto = 0;
		}else{
			$saldo_final = 0;
		}
		
		$saldo_total += $s->dq_saldo_favor-$saldo_final;
		
		//Lo que era menos lo que quedó es lo que se ocupó
		$saldo_detalle[$s->dc_factura] = $s->dq_saldo_favor-$saldo_final;
		
		if($dq_monto == 0){
			break;
		}
		
	}
}

/**
*	Validar valor adeudado menor o igual al total pagar
**/
if($medio_pago->dm_requiere_banco == 0 && $medio_pago->dm_requiere_documento == 0){
	$dq_monto_pagar = floatval($_POST['dq_monto_pagar']);
	if(!$dq_monto_pagar){
		$error_man->showWarning('El monto a pagar es inválido, compruebelo y vuelva a intentarlo');
		exit;
	}
	
	if($dq_monto < $dq_monto_pagar){
		$error_man->showWarning('El monto a pagar debe ser inferior o igual al monto adeudado.');
		exit;
	}
	
}else{
	
	$dq_monto_pagar = 0;
	
	foreach($_POST['dq_monto'] as $d){
		$dq = floatval($d);
		if(!$dq){
			$d = htmlentities($d);
			$error_man->showWarning("El monto {$d} es inválido y no puede usarse como monto.");
			exit;
		}
		
		$dq_monto_pagar += $dq;
	}
	
	if(!$dq_monto_pagar){
		$error_man->showWarning('No se encontraron detalles de pago, intentelo denuevo.');
		exit;
	}
	
	if($dq_monto < $dq_monto_pagar){
		$error_man->showWarning('El monto a pagar debe ser inferior o igual al monto adeudado.');
		exit;
	}
	
}

/**
*	Luego de validado el monto a pagar se modifica sumándole el monto que se ocupará como saldo a favor, este saldo será el total real a pagar por la propuesta.
*/
if(isset($saldo_total) && $saldo_total > 0){
	$dq_monto_pagar += $saldo_total;
}

//Obtener datos requeridos del proveedor
$proveedor = $db->prepare($db->select('tb_proveedor','dg_razon, dg_rut, dc_cuenta_contable, dc_cuenta_anticipo','dc_proveedor = ?'));
$proveedor->bindValue(1,$factura->dc_proveedor,PDO::PARAM_INT);
$db->stExec($proveedor);
$proveedor = $proveedor->fetch(PDO::FETCH_OBJ);

/**
*	Crear una propuesta para agrupar todos los detalles de la 
*/

//Estado final de la propuesta
if(!check_permiso(69)){
	$dm_estado = 'P';
}else if($medio_pago->dm_asiento_intermedio == 1){
	$dm_estado = 'C';
}else{
	$dm_estado = 'T';
}

//Iniciar transacción
$db->start_transaction();

require_once('../../proc/ventas_functions.php');
$insert_propuesta = $db->prepare($db->insert('tb_propuesta_pago',array(
	'dq_propuesta' => '?',
	'dg_propuesta' => '?',
	'dc_medio_pago' => '?',
	'dc_proveedor' => '?',
	'dg_comentario' => '?',
	'df_emision' => $db->getNow(),
	'df_propuesta' => '?',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa,
	'dm_estado' => '?',
	'dq_saldo_favor' => '?'
)));
$dq_propuesta = doc_GetNextNumber('tb_propuesta_pago','dq_propuesta',2);
$insert_propuesta->bindValue(1,$dq_propuesta,PDO::PARAM_INT);
$insert_propuesta->bindValue(2,"Pago factura: {$factura->dq_factura} folio: {$factura->dq_folio}",PDO::PARAM_STR);
$insert_propuesta->bindValue(3,$dc_medio_pago,PDO::PARAM_INT);
$insert_propuesta->bindValue(4,$factura->dc_proveedor,PDO::PARAM_INT);
$insert_propuesta->bindValue(5,$_POST['dg_comentario'],PDO::PARAM_STR);
$insert_propuesta->bindValue(6,$db->sqlDate2($_POST['df_propuesta']),PDO::PARAM_STR);
$insert_propuesta->bindValue(7,$dm_estado,PDO::PARAM_STR);
$insert_propuesta->bindValue(8,isset($saldo_total)?$saldo_total:0,PDO::PARAM_STR);

$db->stExec($insert_propuesta);

$dc_propuesta = $db->lastInsertId();

/**
*	Insertar detalle de la propuesta indicando la factura que será pagada
*/
$ins_det_propuesta = $db->prepare($db->insert('tb_propuesta_pago_detalle',array(
	'dc_propuesta' => '?',
	'dc_factura' => '?',
	'dq_monto' => '?'
)));
$ins_det_propuesta->bindValue(1,$dc_propuesta,PDO::PARAM_INT);
$ins_det_propuesta->bindValue(2,$dc_factura,PDO::PARAM_INT);
$ins_det_propuesta->bindValue(3,$dq_monto_pagar,PDO::PARAM_INT);
$db->stExec($ins_det_propuesta);

/**
*	Insertar detalles de pago (Si lo requiere)
*/
if($medio_pago->dm_requiere_banco == 1 || $medio_pago->dm_requiere_documento == 1){
	$ins_det_pago = $db->prepare($db->insert('tb_pago_propuesta',array(
		'dc_propuesta' => $dc_propuesta,
		'dc_banco' => '?',
		'dg_documento' => '?',
		'df_vencimiento' => '?',
		'dq_monto' => '?',
		'df_emision' => '?',
		'df_creacion' => $db->getNow(),
		'dc_usuario_creacion' => $idUsuario,
		'dc_empresa' => $empresa
	)));
	
	//Asignar sin banco en caso de no ser requerido por el medio de pago
	if($medio_pago->dm_requiere_banco == 1)
		$ins_det_pago->bindParam(1,$dc_banco,PDO::PARAM_INT);
	else
		$ins_det_pago->bindValue(1,0,PDO::PARAM_INT);
	
	//Insertar sin número de documento en caso de no requerirlo
	if($medio_pago->dm_requiere_documento){
		$ins_det_pago->bindParam(2,$dg_documento,PDO::PARAM_STR);
		$ins_det_pago->bindParam(3,$df_vencimiento,PDO::PARAM_STR);
	}else{
		$ins_det_pago->bindValue(2,NULL,PDO::PARAM_NULL);
		$ins_det_pago->bindValue(3,NULL,PDO::PARAM_NULL);
	}
	
	$ins_det_pago->bindParam(4,$dq_monto,PDO::PARAM_STR);
	$ins_det_pago->bindParam(5,$df_emision,PDO::PARAM_STR);
	
	foreach($_POST['dq_monto'] as $i => $d){
		if($medio_pago->dm_requiere_banco == 1)
			$dc_banco = $_POST['dc_banco'][$i];
		if($medio_pago->dm_requiere_documento == 1){
			$dg_documento = $_POST['dg_documento'][$i];
			$df_vencimiento = $db->sqlDate2($_POST['df_vencimiento'][$i]);
		}
		$dq_monto = floatval($_POST['dq_monto'][$i]);
		$df_emision = $db->sqlDate2($_POST['df_emision'][$i]);
		
		$db->stExec($ins_det_pago);
	}
	
}

/**
*	Insertar detalles de saldo a favor si posee
*/
if(isset($saldo_detalle) && count($saldo_detalle)){
	//Insertar detalles ligados a la propuesta
	$ins_det_saldos_favor = $db->prepare($db->insert('tb_saldo_favor_propuesta',array(
		'dc_propuesta' => $dc_propuesta,
		'dc_factura_compra' => '?',
		'dq_monto' => '?'
	)));
	$ins_det_saldos_favor->bindParam(1,$dc_factura_favor,PDO::PARAM_INT);
	$ins_det_saldos_favor->bindParam(2,$dq_monto_favor,PDO::PARAM_STR);
	
	//Rebajar saldos a favor de las facturas
	$edit_saldo_factura = $db->prepare($db->update('tb_factura_compra',array('dq_saldo_favor' => 'dq_saldo_favor-?'),'dc_factura = ?'));
	$edit_saldo_factura->bindParam(1,$dq_monto_favor,PDO::PARAM_STR);
	$edit_saldo_factura->bindParam(2,$dc_factura_favor,PDO::PARAM_INT);
	
	foreach($saldo_detalle as $i => $v){
		$dc_factura_favor = $i;
		$dq_monto_favor = floatval($v);
		
		$db->stExec($ins_det_saldos_favor);
		$db->stExec($edit_saldo_factura);
	}
	
	
	
}
$dg_comprobante='';
/**
*	Insertar comprobante contable
*/
if(check_permiso(69)){
	//Obtener el número del siguiente comprobante contable
	$dg_comprobante = $db->prepare($db->select('tb_comprobante_contable',
				'dg_comprobante',
				'dc_empresa=?',
				array('order_by' => 'df_fecha_emision DESC', 'limit' => 1)));
	$dg_comprobante->bindValue(1,$empresa,PDO::PARAM_INT);
	$db->stExec($dg_comprobante);
	$dg_comprobante = $dg_comprobante->fetch(PDO::FETCH_OBJ);
	
	if($dg_comprobante !== false && substr($dg_comprobante->dg_comprobante,0,6) == date("Ym")){
		$dg_comprobante = date("Ym").((int)substr($dg_comprobante->dg_comprobante,6)+1);
	}else{
		$dg_comprobante = date("Ym").'0';
	}
	
	//Insertar cabecera del comprobante contable
	$insert_comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => '?',
		'dc_tipo_movimiento' => '?',
		'df_fecha_contable' => '?',
		'dc_mes_contable' => date('m'),
		'dc_anho_contable' => date('Y'),
		'dc_tipo_cambio' => '?',
		'dq_cambio' => 1,
		'dg_glosa' => '?',
		'dc_propuesta_pago' => '?',
		'dq_saldo' => '?',
		'df_fecha_emision' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario
	)));
	$insert_comprobante->bindValue(1,$dg_comprobante,PDO::PARAM_INT);
	$insert_comprobante->bindValue(2,$medio_pago->dc_tipo_movimiento,PDO::PARAM_INT);
	$insert_comprobante->bindValue(3,$db->sqlDate2($_POST['df_propuesta']),PDO::PARAM_STR);
	$insert_comprobante->bindValue(4,0,PDO::PARAM_INT);
	$insert_comprobante->bindValue(5,"Propuesta: {$dq_propuesta}\nProveedor: {$proveedor->dg_razon}\nmedio de pago: {$medio_pago->dg_medio_pago}\n\n{$_POST['dg_comentario']}",PDO::PARAM_STR);
	$insert_comprobante->bindValue(6,$dc_propuesta,PDO::PARAM_INT);
	$insert_comprobante->bindValue(7,$dq_monto_pagar,PDO::PARAM_STR);
	
	$db->stExec($insert_comprobante);
	
	$dc_comprobante = $db->lastInsertId();
	
	/**
	*	Insertar detalles de comprobante contable
	*/
	$insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => $dc_comprobante,
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?',
		'dg_cheque' => '?',
		'df_fecha_cheque' => '?',
		'dg_rut' => '?',
		'dc_banco' => '?',
		'dg_doc_compra' => '?',
		'dc_proveedor' => '?',
		'dc_factura_compra' => '?'
	)));
	$insert_detalle_comprobante->bindParam(1,$dc_cuenta_contable,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindParam(2,$dq_debe,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(3,$dq_haber,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(4,$dg_glosa,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(5,$dg_cheque,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(6,$df_fecha_cheque,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(7,$dg_rut,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(8,$dc_banco,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindParam(9,$dq_factura,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindValue(10,$factura->dc_proveedor,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindParam(11,$dc_factura_compra,PDO::PARAM_INT);
	
	//Detalle al DEBE, monto factura.
	$dc_cuenta_contable = $proveedor->dc_cuenta_contable;
	$dq_debe = $dq_monto_pagar;
	$dq_haber = 0;
	$dg_glosa = "Factura compra: {$factura->dq_factura}, folio: {$factura->dq_folio}, Proveedor:{$proveedor->dg_razon}";
	$dg_cheque = '';
	$df_fecha_cheque = '';
	$dg_rut = $proveedor->dg_rut;
	$dc_banco = 0;
	$dq_factura = $factura->dq_factura;
	$dc_factura_compra = $dc_factura;
	
	$db->stExec($insert_detalle_comprobante);
	
	$dq_debe = 0;
	
	//Detalle al HABER, dependiento el medio de pago
	if($medio_pago->dm_requiere_banco == 0 && $medio_pago->dm_requiere_documento == 0){
		$dc_cuenta_contable = $_POST['dc_cuenta_contable'];
		$dq_haber = $dq_monto_pagar;
		if(isset($saldo_total)){
			$dq_haber -= $saldo_total;
		}
		$dg_glosa = "{$medio_pago->dg_medio_pago} Proveedor:{$proveedor->dg_razon}";
		$dg_rut = $proveedor->dg_rut;
		$dq_factura = $factura->dq_factura;
		
		$db->stExec($insert_detalle_comprobante);
		
	}else if($medio_pago->dm_requiere_banco == 1 && $medio_pago->dm_requiere_documento == 0){
		foreach($_POST['dq_monto'] as $i => $d){
			
			$cc_banco = $db->prepare($db->select('tb_banco','dg_banco,dc_cuenta_contable','dc_banco = ?'));
			$cc_banco->bindValue(1,$_POST['dc_banco'][$i],PDO::PARAM_INT);
			$db->stExec($cc_banco);
			$cc_banco = $cc_banco->fetch(PDO::FETCH_OBJ);
			
			$dc_cuenta_contable = $cc_banco->dc_cuenta_contable;
			$dq_haber = floatval($d);
			$dg_glosa = "{$medio_pago->dg_medio_pago} banco: {$cc_banco->dg_banco} fecha: {$_POST['df_emision'][$i]} Proveedor:{$proveedor->dg_razon}";
			$dg_rut = $proveedor->dg_rut;
			$dc_banco = $_POST['dc_banco'][$i];
			$dq_factura = $factura->dq_factura;
			
			$db->stExec($insert_detalle_comprobante);
		}
	}else if($medio_pago->dm_requiere_banco == 1 && $medio_pago->dm_requiere_documento == 1 && $medio_pago->dm_asiento_intermedio == 1){
		foreach($_POST['dq_monto'] as $i => $d){
			
			$cc_banco = $db->prepare($db->select('tb_banco','dg_banco,dc_cuenta_contable','dc_banco = ?'));
			$cc_banco->bindValue(1,$_POST['dc_banco'][$i],PDO::PARAM_INT);
			$db->stExec($cc_banco);
			$cc_banco = $cc_banco->fetch(PDO::FETCH_OBJ);
			
			$dc_cuenta_contable = $medio_pago->dc_cuenta_contable_intermedia;
			$dq_haber = floatval($d);
			$dg_glosa = "{$medio_pago->dg_medio_pago} banco: {$cc_banco->dg_banco} fecha: {$_POST['df_emision'][$i]} Proveedor:{$proveedor->dg_razon}";
			$dg_rut = $proveedor->dg_rut;
			$dg_cheque = $_POST['dg_documento'][$i];
			$df_fecha_cheque = $db->sqlDate2($_POST['df_vencimiento'][$i]);
			$dc_banco = $_POST['dc_banco'][$i];
			$dq_factura = $factura->dq_factura;
			
			$db->stExec($insert_detalle_comprobante);
		}
	}
	
	if(isset($saldo_detalle) && count($saldo_detalle)){
		
		foreach($saldo_detalle as $i => $v){
			$dc_cuenta_contable = $proveedor->dc_cuenta_anticipo;
			$dq_debe = 0;
			$dq_haber = floatval($v);
			$dg_glosa = "Pago con saldo a favor";
			$dg_cheque = '';
			$df_fecha_cheque = '';
			$dg_rut = $proveedor->dg_rut;
			$dc_banco = 0;
			$dq_factura = '';
			$dc_factura_compra = $i;
			
			$db->stExec($insert_detalle_comprobante);
		}
		
	}
	
}//Tiene permiso para confirmar propuesta

/**
*	Actualizar monto pagado en la factura
*/
$update_factura = $db->prepare($db->update('tb_factura_compra',array(
	'dq_monto_pagado' => 'dq_monto_pagado+?'
),'dc_factura = ?'));
$update_factura->bindValue(1,$dq_monto_pagar,PDO::PARAM_STR);
$update_factura->bindValue(2,$dc_factura,PDO::PARAM_INT);
$db->stExec($update_factura);

$db->commit();

$error_man->showConfirm('Se ha realizado el pago de la factura correctamente, los documentos generados son los siguientes:');
?>
<div class="info">
	<h2>
		<small>Propuesta de pago</small><br />
		<strong><?php echo $dq_propuesta ?></strong><br /><br />
		<small>Comprobante contable</small><br />
		<strong><?php echo $dg_comprobante ?></strong>
	</h2>
</div>