<?php
define('MAIN',1);
require_once("../../../../inc/init.php");

$dq_guia_recepcion = intval($_GET['dq_guia_recepcion']);

if(!$dq_guia_recepcion){
	echo json_encode('<not-found>');
	exit;
}

$guia_recepcion = $db->prepare($db->select('tb_guia_recepcion gr','dc_guia_recepcion','dc_empresa = ? AND dq_guia_recepcion = ?'));
$guia_recepcion->bindValue(1,$empresa,PDO::PARAM_INT);
$guia_recepcion->bindValue(2,$dq_guia_recepcion,PDO::PARAM_INT);
$db->stExec($guia_recepcion);
$guia_recepcion = $guia_recepcion->fetch();

if($guia_recepcion === false){
	echo json_encode('<not-found>');
	exit;
}

$detalle = $db->doQuery($db->select('tb_guia_recepcion_detalle d
JOIN tb_producto p ON p.dc_producto = d.dc_producto',
'p.dc_producto, p.dg_producto dg_descripcion, d.dq_cantidad, d.dq_precio dq_precio_venta, p.dg_codigo, p.dm_requiere_serie',
'dc_guia_recepcion = '.$guia_recepcion['dc_guia_recepcion']));

echo json_encode(array(
	$detalle->fetchAll(),
	$guia_recepcion
));

?>