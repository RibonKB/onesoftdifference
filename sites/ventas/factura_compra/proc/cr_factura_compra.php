<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se escapan los caracteres por seguridad
$db->escape($_POST['prov_rut']);
$datos = $db->select("tb_proveedor","dc_proveedor,dg_razon,dc_contacto_default",
"dg_rut = '{$_POST['prov_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el proveedor especificado, intentelo nuevamente.","sites/ventas/factura_compra/cr_factura_compra.php");
}

$datos = $datos[0];


echo("<div id='secc_bar'>Creación de factura de compra</div>
<div id='main_cont'>
<input type='text' class='hidden' />
<div class='panes' style='width:1140px;'>
<div class='title center'>
Generando factura de compra para <strong id='prov_razon' style='color:#000;'>{$datos['dg_razon']}</strong>
</div>");

	include_once("../../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/factura_compra/proc/crear_factura_compra.php","cr_factura_compra",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la factura de compra</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();
	
	$form->Date('Fecha emisión','fv_emision',1,0);
	
	//Contacto principal de la factura, aparecerá en la cabecera como principal contacto del cliente
	$form->Listado('Contacto','fv_contacto',
	"(SELECT * FROM tb_contacto_proveedor WHERE dc_proveedor = {$datos['dc_proveedor']}) c
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto_proveedor','c.dg_contacto','c.dg_direccion','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');
	
	//Dirección a la que se despachará el pedido
	$form->Listado('Contacto de entrega','fv_cont_entrega',
	"(SELECT * FROM tb_contacto_proveedor WHERE dc_proveedor = {$datos['dc_proveedor']}) c
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto_proveedor','c.dg_contacto','c.dg_direccion','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');
	
	$form->Listado('Tipo Factura de compra','fc_tipo_factura',"tb_tipo_factura_compra",array('CONCAT_WS("|",dc_tipo,dm_factura_manual)','dg_tipo'),1);
	
	$form->EndSection();
	$form->Section();
	
	$form->Text("Número de factura","fv_number",1,20);
	$form->Date('Fecha vencimiento','fv_vencimiento',1);
	$form->Listado('Medio de pago','fv_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'),1);
	//$form->Text('Guía de recepción','fv_guia_recepcion');
	echo('<br /><div id="guia_recepcion_data"></div>');
	$form->Button('Asignar guías de recepción','id="set_guia_recepcion"','addbtn');
	$form->Hidden('fv_id_guia_recepcion',0);
	
	$form->EndSection();
	$form->Section();
	
	$form->Textarea('Comentario','fv_comentario',0,isset($num_doc_list)?$num_doc_list:'');
	$form->Text('Referencia','fv_referencia');
	$form->Date('Fecha Contable','fv_fecha_contable',1,0);
	
	$form->EndSection();
	
	echo("<hr class='clear' />");
	
	echo("<div id='prods'>
	<div class='info'>Detalle (Valores en <b>{$empresa_conf['dg_moneda_local']}</b>)</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='70'>Opciones</th>
		<th width='120'>Código</th>
		<th>Descripción</th>
		<th width='120'>Cantidad</th>
		<th width='120'>Precio</th>
		<th width='120'>Total</th>
		<th width='200'>Ceco</th>
		<th width='200' class='hidden cuenta_servicio'>Cuenta Servicio</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='4' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2'><input type='hidden' id='fc_total_detalle' value='0' /></th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Exento</th>
		<th align='right' colspan='2'>
			<input type='text' name='fc_exento' id='fc_exento' size='33' disabled='disabled' />
			<input type='hidden' class='max_val' value='0' />
		</th>
		<th align='left' colspan='2'>
			<label><input type='checkbox' id='activate_exent' /> Habilitar exento</label>
		</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total Neto</th>
		<th align='right' colspan='2'>
			<input type='text' name='fc_neto' id='fc_neto' size='33' readonly='readonly' />
			<input type='hidden' class='max_val' value='0' />
		</th>
		<th colspan='2'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Descuento</th>
		<th align='right' colspan='2'>
			<input type='text' name='fc_descuento' id='fc_descuento' size='33' value='0' />
		</th>
		<th colspan='2'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>IVA</th>
		<th align='right' colspan='2'>
			<input type='text' name='fc_iva' id='fc_iva' size='33' />
		</th>
		<th colspan='2'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total a pagar</th>
		<th align='right' colspan='2'>
			<input type='text' name='fc_total' id='fc_total' size='33' />
		</th>
		<th colspan='2'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div></div>");
	
	$form->Hidden('prov_id',$datos['dc_proveedor']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	if(isset($doc_list)){
		$form->Hidden('doc_list',$doc_list);
		$form->Hidden('doc_type',$_POST['tipo_doc']);
	}
	$form->End('Facturar','addbtn');
	
	$cecos = $db->select('tb_ceco','dc_ceco,dg_ceco',"dc_empresa = {$empresa} AND dm_activo = '1'");
	
	echo("<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<!-- img src='images/doc.png' alt='' title='Asignar Series' class='set_series' / -->
			<!-- input type='hidden' class='prod_series' name='serie[]' -->
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='13' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='50' required='required' /></td>
		<td>
			<input type='text' name='cant[]' class='prod_cant inputtext' size='12' style='text-align:right;' required='required' />
			<input type='hidden' name='despachada[]' value='0' class='despach_cant' />
			<input type='hidden' name='recepcionada[]' value='0' class='recep_cant' />
		</td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='12' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>
		<td>
		<select name='ceco[]' class='inputtext' style='width:200px;' required='required'><option value='0'></option>");
		foreach($cecos as $c)
			echo("<option value='{$c['dc_ceco']}'>{$c['dg_ceco']}</option>");
		echo("</select>
		</td>
		<td class='hidden cuenta_servicio'><select class='inputtext' style='width:200px;' name='dc_cuenta_contable_servicio[]' /></td>
	</tr>
	</table>");

?>
</div></div>
<script type="text/javascript">
	var empresa_iva = <?php echo $empresa_conf['dq_iva'] ?>;
	var empresa_dec = <?php echo $empresa_conf['dn_decimales_local'] ?>;
	var anticipa = <?php echo check_permiso(28)?1:0 ?>;
</script>
<script type="text/javascript" src="jscripts/product_manager/factura_venta.js?v0_10_7b"></script>
<script type="text/javascript" src="jscripts/product_manager/factura_compra.js?v0_11_0b"></script>