<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$_POST['fc_emision_desde'] = $db->sqlDate($_POST['fc_emision_desde']);
$_POST['fc_emision_hasta'] = $db->sqlDate($_POST['fc_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_libro_compra BETWEEN {$_POST['fc_emision_desde']} AND {$_POST['fc_emision_hasta']}) AND dm_nula = 0";

$params = array();

if($_POST['fc_numero_desde']){
	if($_POST['fc_numero_hasta']){
		$conditions .= " AND (dq_factura BETWEEN ? AND ?)";
		$params[] = $_POST['fc_numero_desde'];
		$params[] = $_POST['fc_numero_hasta'];
	}else{
		$conditions = "dq_factura = ?";
		$params[] = $_POST['fc_numero_desde'];
	}
}

if(isset($_POST['fc_proveedor'])){
	$proveedores = substr(str_repeat(',?',count($_POST['fc_proveedor'])),1);
	$conditions .= " AND dc_proveedor IN ({$proveedores})";
	$params = array_merge($params,$_POST['fc_proveedor']);
}

$tables = "(SELECT * FROM tb_factura_compra WHERE {$conditions}) fc
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fc.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fc.dc_orden_servicio
JOIN tb_proveedor pr ON pr.dc_proveedor = fc.dc_proveedor
JOIN tb_tipo_proveedor t ON t.dc_tipo_proveedor = pr.dc_tipo_proveedor AND t.dm_incluido_libro_compra = 1";

$fields = 'GROUP_CONCAT(nv.dq_nota_venta SEPARATOR "<br />") dq_nota_venta,GROUP_CONCAT(os.dq_orden_servicio SEPARATOR "<br />") dq_orden_servicio,
DATE_FORMAT(fc.df_emision,"%d/%m/%Y") df_emision, pr.dg_rut, pr.dg_razon,
fc.dq_exento,SUM(fc.dq_neto) dq_neto,SUM(fc.dq_iva) dq_iva,SUM(fc.dq_total) dq_total,
GROUP_CONCAT(fc.dq_factura SEPARATOR "<br />") dq_factura,fc.dq_folio,fc.dm_nula';

/*'nv.dq_nota_venta, CONCAT_WS(" ",ej.dg_nombres,ej.dg_ap_paterno,ej.dg_ap_materno) dg_vendedor,
DATE_FORMAT(fv.df_emision,"%d/%m/%Y") df_emision, cl.dg_rut, cl.dg_razon, fv.dq_exento, fv.dq_neto, fv.dq_iva, fv.dq_total,
fv.dq_factura,fv.dq_folio';*/

$st = $db->prepare($db->select($tables,$fields,'',array('group_by' => 'fc.dq_folio, fc.dc_proveedor, fc.dm_nula')));

foreach($params as $i => $v){
	$st->bindValue($i+1,$v,PDO::PARAM_INT);
}

$db->stExec($st);

echo('<div class="secc_bar">Libro de compra</div><div class="panes">
<table id="result_libro_compra" class="tab bicolor_tab" width="100%">
<thead><tr>
	<th width="100">Factura</th>
	<th width="100">Folio</th>
	<th>Fecha emisión</th>
	<th>DP</th>
	<th width="100">Documento Relacionado</th>
	<th width="100">RUT Proveedor</th>
	<th>Proveedor</th>
	<th width="100">Exento</th>
	<th width="100">Neto</th>
	<th width="100">IVA</th>
	<th width="100">Total</th>
</tr></thead><tbody>');

$total_exento = 0;
$total_neto = 0;
$total_iva = 0;
$total = 0;

foreach($st as $d){
	
	/*$total_exento += $d['dq_exento'];
	$total_neto += $d['dq_neto'];
	$total_iva += $d['dq_iva'];
	$total += $d['dq_total'];*/
	
	$exento = moneda_local($d['dq_exento']);
	$neto = moneda_local($d['dq_neto']);
	$iva = moneda_local($d['dq_iva']);
	$subtotal = moneda_local($d['dq_total']);
	
	$DP = '';
	if($d['dq_nota_venta'])
		$DP = '<b>NV</b>';
	if($d['dq_orden_servicio'])
		$DP = '<b>OS</b>';
		
	if($d['dm_nula'] == 0){
		$total_exento += $d['dq_exento'];
		$total_neto += $d['dq_neto'];
		$total_iva += $d['dq_iva'];
		$total += $d['dq_total'];
	}else{
		$exento =	'<span style="color:#666">'.moneda_local($d['dq_exento']).'</span>';
		$neto = 	'<span style="color:#666">'.moneda_local($d['dq_neto']).'</span>';
		$iva = 		'<span style="color:#666">'.moneda_local($d['dq_iva']).'</span>';
		$subtotal = '<span style="color:#666">'.moneda_local($d['dq_total']).'</span>';
	}
	
	echo("<tr>
		<td><b>{$d['dq_factura']}</b></td>
		<td><b>{$d['dq_folio']}</b></td>
		<td>{$d['df_emision']}</td>
		<td>{$DP}</td>
		<td>{$d['dq_nota_venta']}{$d['dq_orden_servicio']}</td>
		<td>{$d['dg_rut']}</td>
		<td>{$d['dg_razon']}</td>
		<td align='right'><b>{$exento}</b></td>
		<td align='right'><b>{$neto}</b></td>
		<td align='right'><b>{$iva}</b></td>
		<td align='right'><b>{$subtotal}</b></td>
	</tr>");
}

$total_exento = moneda_local($total_exento);
$total_neto = moneda_local($total_neto);
$total_iva = moneda_local($total_iva);
$total = moneda_local($total);

echo("</tbody><tfoot>
	<tr>
		<th colspan='7' align='right'>Totales</th>
		<th align='right'>{$total_exento}</th>
		<th align='right'>{$total_neto}</th>
		<th align='right'>{$total_iva}</th>
		<th align='right'>{$total}</th>
	</tr>
</tfoot></table></div>");
?>
<script type="text/javascript">
	window.setTimeout(function(){
		$('#result_libro_compra').tableExport().tableAdjust(20);
	},100);
</script>