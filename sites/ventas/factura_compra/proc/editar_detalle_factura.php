<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_detalle = intval($_POST['dc_detalle']);

$detalle = $db->prepare($db->select('tb_factura_compra_detalle','dq_precio, dc_detalle_orden_compra, dc_factura','dc_detalle = ?'));
$detalle->bindValue(1,$dc_detalle,PDO::PARAM_INT);
$db->stExec($detalle);
$detalle = $detalle->fetch(PDO::FETCH_OBJ);

if($detalle === false){
	$error_man->showWarning('El detalle seleccionado es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

$factura = $db->prepare($db->select('tb_factura_compra','dc_guia_recepcion, dq_monto_pagado, dq_total','dc_factura = ?'));
$factura->bindValue(1,$detalle->dc_factura,PDO::PARAM_INT);
$db->stExec($factura);
$factura = $factura->fetch(PDO::FETCH_OBJ);

$dq_diferencia = round(floatval($_POST['dq_precio'])-floatval($detalle->dq_precio),$empresa_conf['dn_decimales_local']);

$dq_iva = round($dq_diferencia*($empresa_conf['dq_iva']/100),$empresa_conf['dn_decimales_local']);

$dq_monto_pagar = floatval($factura->dq_total)-floatval($factura->dq_monto_pagado);

if($dq_diferencia+$dq_iva > $dq_monto_pagar){
	$error_man->showWarning('Los montos pagados de la factura superarían el monto total de la factura con este cambio de precio, no se puede continuar');
	exit;
}

$db->start_transaction();

	$edit_detalle = $db->prepare($db->update('tb_factura_compra_detalle',array(
		'dq_precio' => '?',
		'dq_total' => 'dc_cantidad*?'
	),'dc_detalle = ?'));
	$edit_detalle->bindValue(1,floatval($_POST['dq_precio']),PDO::PARAM_STR);
	$edit_detalle->bindValue(2,floatval($_POST['dq_precio']),PDO::PARAM_STR);
	$edit_detalle->bindValue(3,$dc_detalle,PDO::PARAM_INT);
	$db->stExec($edit_detalle);

	$edit_factura = $db->prepare($db->update('tb_factura_compra',array(
		'dq_neto' => 'dq_neto+?',
		'dq_iva' => 'dq_iva+?',
		'dq_total' => 'dq_total+?'
	),'dc_factura = ?'));
	$edit_factura->bindValue(1,$dq_diferencia,PDO::PARAM_STR);
	$edit_factura->bindValue(2,$dq_iva,PDO::PARAM_STR);
	$edit_factura->bindValue(3,$dq_diferencia+$dq_iva,PDO::PARAM_STR);
	$edit_factura->bindValue(4,$detalle->dc_factura,PDO::PARAM_INT);
	$db->stExec($edit_factura);
	
	if($factura->dc_guia_recepcion != 0 && $detalle->dc_detalle_orden_compra != 0){
		$edit_detalle_guia = $db->prepare($db->update('tb_guia_recepcion_detalle',array(
			'dq_precio' => '?'
		),'dc_guia_recepcion = ? AND dc_detalle_orden_compra = ?'));
		$edit_detalle_guia->bindValue(1,floatval($_POST['dq_precio']),PDO::PARAM_STR);
		$edit_detalle_guia->bindValue(2,$factura->dc_guia_recepcion,PDO::PARAM_INT);
		$edit_detalle_guia->bindValue(3,$detalle->dc_detalle_orden_compra,PDO::PARAM_INT);
		$db->stExec($edit_detalle_guia);
	}

$db->commit();

$error_man->showConfirm('Se ha modificado el valor para el detalle correctamente.');

?>