<?php
define("MAIN",1);
require_once("../../../../inc/global.php");

$dc_factura = intval($_POST['dc_factura']);
$dc_medio_pago = intval($_POST['dc_medio_pago']);

if(!$dc_factura || !$dc_medio_pago){
	$error_man->showWarning('Los datos de entrada son inválidos, compruebe la información y vuelva a intentarlo.');
	exit;
}

$medio_pago = $db->select('tb_medio_pago_proveedor',
'dc_tipo_movimiento, dm_requiere_banco, dm_asiento_intermedio, dm_requiere_documento, dc_banco_default',
"dc_empresa = {$empresa} AND dc_medio_pago = {$dc_medio_pago}");
$medio_pago = array_shift($medio_pago);

if($medio_pago === NULL){
	$error_man->showWarning('No se encuentra el medio de pago ingresado, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

$factura = $db->select('tb_factura_compra fc
JOIN tb_proveedor pr ON pr.dc_proveedor = fc.dc_proveedor',
'fc.dq_factura, fc.dq_folio, (fc.dq_total-fc.dq_monto_pagado) dq_monto_pagar, pr.dc_proveedor, pr.dg_razon, pr.dq_saldo_favor',
"fc.dc_factura = {$dc_factura} AND fc.dc_empresa = {$empresa}");
$factura = array_shift($factura);

if($factura === NULL){
	$error_man->showWarning('No se encuentra la factura de compra asociada, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

require_once("../../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Comprobantes de pago de Factura
</div>
<div class="panes">
	<?php
		$form->Start('sites/ventas/factura_compra/proc/crear_pago_factura_compra.php','cr_pago');
			$form->Header('<b>Indique los datos para los comprobantes de pago de factura</b><br />
			Los datos marcados con [*] son obligatorios');
			$form->Section();
				
				//En caso de que el pago sea en efectivo se muestra la cuenta contable en que se cargará el pago
				if($medio_pago['dm_requiere_banco'] == 0 && $medio_pago['dm_requiere_documento'] == 0):

						$form->Text('Monto a pagar','dq_monto_pagar',1);
					
						$form->Listado('Cuenta Destino','dc_cuenta_contable','tb_cuenta_contable cc
						JOIN tb_cuentas_tipo_movimiento t ON t.dc_cuenta_contable = cc.dc_cuenta_contable AND t.dc_tipo_movimiento = '.$medio_pago['dc_tipo_movimiento']
						,array('cc.dc_cuenta_contable','cc.dg_cuenta_contable'),1);

				endif;
				
				$form->Textarea('Comentario','dg_comentario',1);
				
				if($factura['dq_saldo_favor'] > 0):
				?>
					<br />
					<a href="sites/finanzas/pagos/src_saldo_favor.php?dc_proveedor=<?php echo $factura['dc_proveedor'] ?>" id="set_saldo_favor" class="button">
						Asignar saldos a favor
					</a>
					<div id="detalle_saldo"></div>
				<?php
				endif;
				
			$form->EndSection();
			$form->Section();
				$form->Date('Fecha de pago','df_propuesta',1,0);
				?>
				<fieldset>
					<div class="info">
						Datos Factura
					</div>
					<div>
						Número: <b><?php echo $factura['dq_factura'] ?></b><br />
						Folio: <b><?php echo $factura['dq_folio'] ?></b><br />
						Proveedor:<br /><b><?php echo $factura['dg_razon'] ?></b><br />
						Monto en Deuda: <b><?php echo moneda_local($factura['dq_monto_pagar']) ?></b>
					</div>
				</fieldset>
				<?php
			$form->EndSection();
			//En caso de que admita banco o documento el medio, el pago ódrá ser realizado en varios detalles.
			if($medio_pago['dm_requiere_banco'] == 1 || $medio_pago['dm_requiere_documento'] == 1):
			
				$bancos = $db->select('tb_banco b
				JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = b.dc_cuenta_contable
				JOIN tb_cuentas_tipo_movimiento t ON t.dc_cuenta_contable = c.dc_cuenta_contable',
				'b.dc_banco, b.dg_banco',
				"b.dc_empresa = {$empresa} AND t.dc_tipo_movimiento = {$medio_pago['dc_tipo_movimiento']}");
			
				$form->Group('id="payDetail"');
					$form->Button('Agregar detalle de pago','id="addPayDetail"','addbtn');
				$form->Group();
			endif;
		$form->Hidden('dm_requiere_banco',$medio_pago['dm_requiere_banco']);
		$form->Hidden('dm_requiere_documento',$medio_pago['dm_requiere_documento']);
		$form->Hidden('dm_asiento_intermedio',$medio_pago['dm_asiento_intermedio']);
		$form->Hidden('dc_tipo_movimiento',$medio_pago['dc_tipo_movimiento']);
		$form->Hidden('dc_factura',$dc_factura);
		$form->Hidden('dc_medio_pago',$dc_medio_pago);
		$form->End('Realizar pago','addbtn');
	?>
</div>
<?php /* Plantilla de detalles */if($medio_pago['dm_requiere_banco'] == 1 || $medio_pago['dm_requiere_documento'] == 1): ?>
	<div id="detailContainer" class="hidden">
	<fieldset class="payDetail">
		<div class="left">
			<label>
				Monto[*]<br />
				<input type="text" class="inputtext" name="dq_monto[]" required="required" size="41" maxlength="255" />
			</label><br />
			<label>
				Fecha Emisión[*]<br />
				<input type="text" class="date inputtext" name="df_emision[]" required="required" size="38" maxlength="255" />
			</label><br />
		</div>
		<div class="left">
			<?php /*Requiere documento*/ if($medio_pago['dm_requiere_documento'] == 1): ?>
			<label>
				Número de Documento[*]<br />
				<input type="text" class="inputtext" name="dg_documento[]" required="required" size="41" maxlength="255" />
			</label><br />
			<label>
				Fecha Vencimiento[*]<br />
				<input type="text" class="date inputtext" name="df_vencimiento[]" required="required" size="38" maxlength="255" />
			</label><br />
			<?php endif; ?>
			<?php /*Requiere Banco*/ if($medio_pago['dm_requiere_banco'] == 1): ?>
			<label>
				Banco[*]<br />
				<select class="inputtext dc_banco" name="dc_banco[]" required="required">
				<option value="0"></option>
				<?php foreach($bancos as $b): ?>
					<option value="<?php echo $b['dc_banco'] ?>"><?php echo $b['dg_banco'] ?></option>
				<?php endforeach; ?>
				</select>
			</label>
			<?php endif; ?>
		</div>
	</fieldset>
	</div>
	<script type="text/javascript" src="jscripts/sites/finanzas/pagos/cr_pago_individual.js"></script>
	<script type="text/javascript">
		js_data.init(<?php echo $medio_pago['dc_banco_default'] ?>);
	</script>
<?php endif; /* fin plantilla de detalles */ ?>
<script type="text/javascript">
	pymerp.init($('#cr_pago'));
	$('#set_saldo_favor').click(function(e){
		e.preventDefault();
		var href = this.href;
		pymerp.loadOverlay(href,'',true);
	});
</script>