<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_tipo = intval($_GET['dc_tipo']);

$cuentas = $db->prepare(
				$db->select('tb_tipo_factura_compra_cuenta_contable t
							 JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = t.dc_cuenta_contable',
							'c.dc_cuenta_contable, c.dg_cuenta_contable',
							't.dc_tipo_factura = ?'));
$cuentas->bindValue(1,$dc_tipo,PDO::PARAM_INT);
$db->stExec($cuentas);

echo json_encode($cuentas->fetchAll(PDO::FETCH_OBJ));