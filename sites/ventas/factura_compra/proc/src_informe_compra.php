<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$df_emision_desde = $db->sqlDate($_POST['df_emision_desde']);
$df_emision_hasta = $db->sqlDate($_POST['df_emision_hasta']." 23:59");

$conditions = "fc.dc_empresa = {$empresa} AND (fc.df_emision BETWEEN {$df_emision_desde} AND {$df_emision_hasta}) AND fc.dm_nula = 0";
$params = array();

if($_POST['dq_factura_desde']){
	if($_POST['dq_factura_hasta']){
		$conditions .= " AND (fc.dq_factura BETWEEN ? AND ?)";
		$params[] = $_POST['dq_factura_desde'];
		$params[] = $_POST['dq_factura_hasta'];
	}else{
		$conditions = "fc.dq_factura = ?";
		$params[] = $_POST['dq_factura_desde'];
	}
}

if(isset($_POST['dc_proveedor'])){
	$proveedores = substr(str_repeat(',?',count($_POST['dc_proveedor'])),1);
	$conditions .= " AND fc.dc_proveedor IN ({$proveedores})";
	$params = array_merge($params,$_POST['dc_proveedor']);
}

if(isset($_POST['dc_marca'])){
	$marcas = substr(str_repeat(',?',count($_POST['dc_marca'])),1);
	$conditions .= " AND p.dc_marca IN ({$marcas})";
	$params = array_merge($params,$_POST['dc_marca']);
}

if(isset($_POST['dc_linea_negocio'])){
	$lineas = substr(str_repeat(',?',count($_POST['dc_linea_negocio'])),1);
	$conditions .= " AND p.dc_linea_negocio IN ({$lineas})";
	$params = array_merge($params,$_POST['dc_linea_negocio']);
}

$data = $db->prepare($db->select('tb_factura_compra_detalle d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	JOIN tb_marca m ON m.dc_marca = p.dc_marca
	JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio
	JOIN tb_factura_compra fc ON fc.dc_factura = d.dc_factura
	JOIN tb_proveedor pr ON pr.dc_proveedor = fc.dc_proveedor',
'd.dc_cantidad, d.dq_precio, p.dg_codigo, p.dg_producto, m.dg_marca, ln.dg_linea_negocio, fc.dq_factura, fc.dq_folio, pr.dg_razon',
$conditions, array( 'order_by' => 'fc.dq_factura' )));

foreach($params as $i => $v){
	$data->bindValue($i+1,$v,PDO::PARAM_INT);
}

$db->stExec($data);

?>
<table class="tab bicolor_tab" id="result_informe" width="100%">
	<thead>
    	<tr>
        	<th>Factura</th>
            <th>Folio</th>
            <th>Código</th>
            <th>Producto</th>
            <th>Marca</th>
            <th>Linea_negocio</th>
            <th>Cantidad</th>
            <th>Valor</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
    <?php $total = 0; ?>
    <?php while($d = $data->fetch(PDO::FETCH_OBJ)): ?>
    	<?php $total += $d->dq_precio*$d->dc_cantidad; ?>
        <tr>
        	<td><?php echo $d->dq_factura ?></td>
            <td><?php echo $d->dq_folio ?></td>
            <td><?php echo $d->dg_codigo ?></td>
            <td><?php echo $d->dg_producto ?></td>
            <td><?php echo $d->dg_marca ?></td>
            <td><?php echo $d->dg_linea_negocio ?></td>
            <td align="right"><?php echo $d->dc_cantidad ?></td>
            <td align="right"><?php echo moneda_local($d->dq_precio) ?></td>
            <td align="right"><?php echo moneda_local($d->dq_precio*$d->dc_cantidad) ?></td>
        </tr>
    <?php endwhile; ?>
    </tbody>
    <tfoot>
    	<tr>
        	<th colspan="8" align="right">Total</th>
            <th align="right"><?php echo moneda_local($total) ?></th>
        </tr>
    </tfoot>
</table>