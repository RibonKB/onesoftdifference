<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_proveedor = intval($_POST['dc_proveedor']);

$guia_recepcion = $db->prepare(
					$db->select(
						'tb_guia_recepcion gr
						JOIN tb_orden_compra oc ON oc.dc_orden_compra = gr.dc_orden_compra
						LEFT JOIN tb_tipo_guia_recepcion t ON t.dc_tipo = gr.dc_tipo_guia',
						'gr.dc_guia_recepcion, gr.dq_guia_recepcion, gr.df_fecha_emision, oc.dq_orden_compra, t.dg_tipo',
						'oc.dc_proveedor = ? AND gr.dc_empresa = ? AND gr.dc_factura = 0 AND gr.dm_nula = 0')
				  );
$guia_recepcion->bindValue(1,$dc_proveedor,PDO::PARAM_INT);
$guia_recepcion->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($guia_recepcion);
$guia_recepcion = $guia_recepcion->fetchAll(PDO::FETCH_OBJ);

//Configurar las guias de recepcion seleccionadas inicialmente
$grs = array();
if(isset($_POST['dc_guia_recepcion'])){
	$grs = $_POST['dc_guia_recepcion'];
}

require_once("../../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Asignar Guías de Recepción a Factura de Compra
</div>
<div class="panes">
	<?php $form->Start('#','get_guia_recepcion','get_guias') ?>
    	<?php $form->Header('Seleccione las guías de recepción que quiere asignar a la factura de compra') ?>
        <table class="tab" width="100%" id="guias_data">
        	<thead>
            	<tr>
                	<th>&nbsp;</th>
                    <th>Guía de Recepción</th>
                    <th>Fecha Emisión</th>
                    <th>Tipo Guía</th>
                    <th>Orden de compra</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($guia_recepcion as $gr): ?>
            	<tr>
                	<td>
                    	<input
                        	type="checkbox"
                            name="dc_guia_recepcion[]"
                            value="<?php echo $gr->dc_guia_recepcion ?>"
                            <?php if(in_array($gr->dc_guia_recepcion,$grs)): ?>
                            checked="checked"
                            <?php endif; ?>
                        />
                    </td>
                    <td><b><?php echo $gr->dq_guia_recepcion ?></b></td>
                    <td><?php echo $db->dateLocalFormat($gr->df_fecha_emision) ?></td>
                    <td><?php echo $gr->dg_tipo ?></td>
                    <td><?php echo $gr->dq_orden_compra ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php $form->End('Asignar','addbtn') ?>
</div>
<script type="text/javascript">
	window.setTimeout(function(){
		$('#guias_data').tableExport().tableAdjust(10);
	},300);
	$('#get_guia_recepcion').submit(initSetGuiaRecepcion);
</script>