<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_factura = intval($_POST['dc_factura']);
$df_libro_compra = $db->sqlDate2($_POST['df_libro_compra']);

$update_factura = $db->prepare($db->update('tb_factura_compra',array(
	'df_libro_compra' => '?'
),'dc_factura = ? AND dc_empresa = '.$empresa));
$update_factura->bindValue(1,$df_libro_compra,PDO::PARAM_STR);
$update_factura->bindValue(2,$dc_factura,PDO::PARAM_INT);

$db->stExec($update_factura);

$error_man->showConfirm('Se ha modificado la fecha contable de la factura correctamente');

?>
<script type="text/javascript">
	$('#res_list .confirm .fv_load').trigger('click');
</script>