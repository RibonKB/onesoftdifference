<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../../../inc/Factory.class.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_factura = intval($_POST['id']);

$data = $db->select("(SELECT * FROM tb_factura_compra WHERE dc_empresa={$empresa} AND dc_factura = {$dc_factura}) fv
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fv.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fv.dc_orden_servicio
JOIN tb_medio_pago mp ON mp.dc_medio_pago = fv.dc_medio_pago
LEFT JOIN tb_contacto_proveedor c ON c.dc_contacto_proveedor = fv.dc_contacto
	LEFT JOIN tb_comuna com ON com.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
LEFT JOIN tb_tipo_cargo tc ON tc.dc_tipo_cargo = fv.dc_tipo_cargo
LEFT JOIN tb_tipo_factura_compra top ON top.dc_tipo = fv.dc_tipo
LEFT JOIN tb_proveedor cl ON cl.dc_proveedor = fv.dc_proveedor",
"fv.dq_factura,fv.df_emision, fv.df_vencimiento ,fv.dg_comentario,
nv.dq_nota_venta,os.dq_orden_servicio,fv.dq_neto,fv.dq_iva,fv.dq_total,fv.dc_factura,mp.dg_medio_pago,fv.dm_nula,c.dg_telefono,fv.dq_exento,fv.dm_centralizada,
c.dg_contacto,c.dg_direccion,reg.dg_region,com.dg_comuna,tc.dg_tipo_cargo,top.dg_tipo,cl.dg_razon,cl.dg_rut,cl.dg_giro, fv.dq_monto_pagado, fv.df_libro_compra");

$data = array_shift($data);

if($data == NULL){
	$error_man->showWarning("No se ha encontrado la factura especificada");
	exit;
}
$data['dg_comentario'] = str_replace("\n",'<br />',$data['dg_comentario']);

$detalle = $db->select("tb_factura_compra_detalle d
JOIN tb_producto p ON p.dc_producto = d.dc_producto
LEFT JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = d.dc_cuenta_servicio",
'd.dc_detalle,p.dg_codigo,d.dc_cantidad,p.dg_producto,d.dq_precio,dq_total,dm_tipo,d.dg_descripcion, cc.dg_cuenta_contable, cc.dg_codigo dg_codigo_cuenta_contable',
"dc_factura={$data['dc_factura']}");

$nula = '';
if($data['dm_nula'] == '1'){
	$null_data = $db->select("(SELECT * FROM tb_factura_compra_anulada WHERE dc_factura = {$dc_factura}) n
	JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo",'ma.dg_motivo,n.dm_fisica,n.dg_comentario');
	$nula_info = '';
	if(count($null_data)){
		$null_data = $null_data[0];
		$null_data['dm_fisica'] = $null_data['dm_fisica']==1?'SI':'NO';
		$nula_info = "<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
		<div class='info'>Info anulación</div>
		Motivo: <b>{$null_data['dg_motivo']}</b><br />
		Facturada fisicamente: <b>{$null_data['dm_fisica']}</b><br />
		Comentario: <b>{$null_data['dg_comentario']}</b>";
	}
	$nula = "<td rowspan='11' valign='middle'><h3 class='alert'>FACTURA ANULADA</h3>{$nula_info}</td>";
}

if($data['dm_centralizada'] == 1){
	$centralizacion = $db->select('tb_comprobante_contable_detalle_analitico da
			JOIN tb_comprobante_contable_detalle d ON d.dc_detalle = da.dc_detalle_financiero
			JOIN tb_comprobante_contable c ON c.dc_comprobante = d.dc_comprobante',
		'c.dg_comprobante, c.df_fecha_contable',
		"da.dc_factura_compra ={$dc_factura} AND c.dm_anulado = 0 AND c.dm_tipo_comprobante = 3");
	$centralizacion = array_shift($centralizacion);
}

?>

<div class='title center'>Factura Nº <?php echo $data['dq_factura'] ?> <?php echo empty($data['dq_nota_venta']) ? 'vacio':'lleno';?></div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Proveedor:<br /><strong>(<?php echo $data['dg_rut'] ?>) <?php echo $data['dg_razon'] ?></strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td><?php echo $db->dateLocalFormat($data['df_emision']) ?></td>
	<?php echo $nula ?>
    <?php if($data['dq_monto_pagado'] > 0): ?>
    	<td rowspan="9" width="350" valign="top">
        	<div class="confirm">
            	CON PAGO
        	<?php if($data['dq_monto_pagado'] < $data['dq_total']): ?>
            	PARCIAL
            <?php else: ?>
            	COMPLETO
            <?php endif; ?>
            </div>
            <div style="border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;">
        		<div class="info">Información de pago</div>
                Monto Pagado: <b><?php echo moneda_local($data['dq_monto_pagado']) ?></b>
                <br />
                Monto Pendiente de pago: <b><?php echo moneda_local($data['dq_total']-$data['dq_monto_pagado']) ?></b>
            </div>
            <div class="center">
	            <button id="historial_pago" class="button">Ver información de pagos</button>
            </div>
        </td>
    <?php endif; ?>
    <?php if($data['dm_centralizada']): ?>
    	<td rowspan="13" valign="top">
            <h3 class="confirm">FACTURA CENTRALIZADA</h3>
               <?php if($centralizacion !== null): ?>
               	<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
               	<div class='info'>Info centralización</div>
        	       	Comprobante centralización: <b><?php echo $centralizacion['dg_comprobante'] ?></b><br />
            	    Periodo Contable: <b><?php $db->dateLocalFormat($centralizacion['df_fecha_contable']) ?></b>
               </div>
           <?php endif ?>
        </td>
    <?php endif ?>
</tr><tr>
	<td>Fecha contable</td>
	<td>
		<span class="left"><?php echo $db->dateLocalFormat($data['df_libro_compra']) ?>&nbsp;&nbsp;</span>
		<a href="sites/ventas/factura_compra/ed_fecha_libro_compra.php?id=<?php echo $_POST['id'] ?>" class="loadOnOverlay">
			<img src="images/editbtn.png" align="[ED]" title="Editar fecha contable" width="15" />
		</a>
	</td>
</tr><tr>
	<td>Domicilio proveedor</td>
	<td><b><?php echo $data['dg_direccion'] ?></b> <?php echo $data['dg_comuna'] ?> <label><?php echo $data['dg_region'] ?></label></td>
</tr><tr>
	<td>Medio de pago:</td>
	<td><b><?php echo $data['dg_medio_pago'] ?></b></td>
</tr><tr>
	<td>Tipo de cargo:</td>
	<td><b><?php echo $data['dg_tipo_cargo'] ?></b></td>
</tr><tr>
	<td>Tipo de factura</td>
	<td><?php echo $data['dg_tipo'] ?></td>
</tr><tr>
	<td>Fecha de vencimiento</td>
	<td><?php echo $db->dateLocalFormat($data['df_vencimiento']) ?></td>
</tr><tr>
	<td>Nota de venta</td>
	<td><?php echo $data['dq_nota_venta'] ?></td>
</tr><tr>
	<td>Orden de servicio</td>
	<td><?php echo $data['dq_orden_servicio'] ?></td>
</tr><tr>
	<td>Comentario</td>
	<td><?php echo $data['dg_comentario'] ?></td>
</tr></table>

<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
	<tr>
		<th>&nbsp;</th>
		<th width='100'>Código</th>
		<th width='60'>Cantidad</th>
		<th>Descripción</th>
		<th>Cuenta Contable</th>
		<th width='100'>Precio</th>
		<th width='100'>Total</th>
	</tr>
</thead>
<tbody>

<?php foreach($detalle as $d): ?>
	<tr>
    	<td>
        	<a href="sites/ventas/factura_compra/ed_detalle_factura.php?id=<?php echo $d['dc_detalle'] ?>" class="loadOnOverlay">
          	<img src="images/editbtn.png" alt="[ED]" title="Editar detalle" />
          </a>
					<a href="<?php echo Factory::buildUrl('Centralizacion', 'ventas', 'asignarCuentaDetalle', 'factura_compra', array('dc_factura' => $dc_factura,'dc_detalle' => $d['dc_detalle'])) ?>" class="loadOnOverlay">
						<img src="images/inventario.png" alt="[CST]" title="Editar cuenta centralización" />
					</a>
        </td>
		<td><?php echo $d['dg_codigo'] ?></td>
		<td><?php echo $d['dc_cantidad'] ?></td>
		<td>
        	<?php if($d['dg_descripcion']): ?>
				<?php echo $d['dg_descripcion'] ?>
            <?php else: ?>
				<?php echo $d['dg_producto'] ?>
            <?php endif; ?>
        </td>
		<td width="250">
			<?php if($d['dg_cuenta_contable']): ?>
				<strong>(<?php echo $d['dg_codigo_cuenta_contable'] ?>)</strong>
				<?php echo $d['dg_cuenta_contable'] ?>
			<?php endif ?>
		</td>
		<td align='right'><?php echo moneda_local($d['dq_precio']) ?></td>
		<td align='right'><?php echo moneda_local($d['dq_total']) ?></td>
	</tr>
<?php endforeach; ?>

</tbody>
<tfoot>
<tr>
	<th colspan="5" rowspan="4" align="right">
    	<button type="button" class="editbtn" id="edit_totales">Editar Totales</button>
    </th>
	<th align='right'>Total Exento</th>
	<th align='right'><?php echo moneda_local($data['dq_exento']) ?></th>
</tr>
<tr>
	<th align='right'>Total Neto</th>
	<th align='right'><?php echo moneda_local($data['dq_neto']) ?></th>
</tr>
<tr>
	<th align='right'>IVA</th>
	<th align='right'><?php echo moneda_local($data['dq_iva']) ?></th>
</tr>
<tr>
	<th align='right'>Total a Pagar</th>
	<th align='right'><?php echo moneda_local($data['dq_total']) ?></th>
</tr>
</tfoot></table>

<script type="text/javascript">
<?php if($data['dm_nula']): ?>
disable_button('#print_version,#print_glosa_version,#null_factura,#pago_factura,#asigna_costos');
<?php else: ?>
pymerp.init('#show_factura');
enable_button('#print_version,#print_glosa_version,#null_factura,#pago_factura,#asigna_costos');
$('#print_version').unbind('click').click(function(){
	window.open("sites/ventas/factura_compra/ver_factura_venta.php?id=<?php echo $dc_factura ?>",'print_factura_compra','width=800;height=600');
});
$('#null_factura').unbind('click').click(function(){
	pymerp.loadOverlay('<?php echo Factory::buildUrl('AnularFacturaCompra', 'ventas', 'index', 'factura_compra', array('id' => $dc_factura)) ?>');
});
$('#gestion_factura').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/factura_compra/cr_gestion_cobranza.php?id=<?php echo $dc_factura ?>');
});
$('#edit_totales').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/factura_compra/ed_totales_factura.php?id=<?php echo $dc_factura ?>');
});
$('#edit_folio').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/factura_compra/ch_folio.php?id=<?php echo $dc_factura ?>');
});
$('#historial_pago').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/factura_compra/src_historial_pago.php?id=<?php echo $dc_factura ?>');
});
	<?php if($data['dq_total'] > $data['dq_monto_pagado']):  ?>
		$('#pago_factura').unbind('click').click(function(){
			loadOverlay('sites/ventas/factura_compra/cr_pago_factura_compra.php?id=<?php echo $dc_factura ?>');
		});
	<?php else: ?>
		disable_button('#pago_factura');
	<?php endif; ?>
    <?php if(empty($data['dq_nota_venta'])):?>
        $('#asigna_costos').unbind('click').click(function(){
            pymerp.loadOverlay('<?php echo Factory::buildUrl('AsignacionCostos','ventas','search','factura_compra',array('dc_factura' => $dc_factura)); ?>');
        });
    <?php else: ?>
		disable_button('#asigna_costos');
    <?php endif; ?>
<?php endif; ?>
</script>
