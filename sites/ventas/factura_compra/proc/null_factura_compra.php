<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}


//51

if(check_permiso(51))
	$month_cond = '';
else
	$month_cond = 'AND MONTH(df_emision) = MONTH(NOW())';

$data = $db->select('tb_factura_compra','dc_nota_venta,dc_orden_servicio,dm_nula, dm_centralizada',"dc_factura={$_POST['id_factura']} {$month_cond}");
if(!count($data)){
	$error_man->showWarning('No se ha encontrado la factura especificada o esta no puede anularse por no encontrarse en el periodo actual.');
	exit;
}
$data = $data[0];

//Comprobar que no esté ya nula
if($data['dm_nula'] == 1){
	$error_man->showWarning("La factura de compra ya ha sido anulada anteriormente");
	exit;
}

//Comprobar centralización
if($data['dm_centralizada'] == 1){
	$error_man->showWarning("La factura de compra ha sido incluida en una centralización, se debe anular los comprobantes de centralización antes");
	exit;
}

$db->start_transaction();

//Comprobar que no se esté utilizando la ordende servicio o nota de venta asignada a la factura
$db->update('tb_factura_compra',array('dm_nula'=>1),"dc_factura={$_POST['id_factura']}");

$db->insert('tb_factura_compra_anulada',array(
	'dc_factura' => $_POST['id_factura'],
	'dc_motivo' => $_POST['null_motivo'],
	'dg_comentario' => $_POST['null_comentario'],
	'dc_usuario' => $idUsuario,
	'df_anulacion' => 'NOW()'
));

$db->commit();
$error_man->showAviso("Atención a anulado la factura de compra y las relaciones con otros documentos");
?>
<script type="text/javascript">
$('#res_list .confirm .fv_load').trigger('click');
</script>