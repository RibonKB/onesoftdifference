<?php
define("MAIN",1);
require_once("../../../inc/global.php");

$dc_factura = intval($_POST['id']);

if(!$dc_factura){
	$error_man->showWarning('La factura ingresada es inválida, compruebe los valores de entrada');
	exit;
}

$data = $db->select('tb_factura_compra','dq_factura','dc_factura = '.$dc_factura);
$data = array_shift($data);

if($data === NULL){
	$error_man->showWarning('No se encontró la factura de compra, compruebe los datos de entrada');
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Pagar Factura <b><?php echo $data['dq_factura'] ?></b>
</div>
<div class="panes center">
	<?php
		$form->Start('sites/ventas/factura_compra/proc/cr_pago_factura_compra.php','cr_pago','overlayValidar');
			$form->Header('Indique el medio de pago con el que se realizará la gestión de pago sobre la factura');
			$form->Listado('Medio de pago','dc_medio_pago','tb_medio_pago_proveedor',array('dc_medio_pago','dg_medio_pago'),1);
			$form->Hidden('dc_factura',$dc_factura);
		$form->End('Iniciar Pago','addbtn');
	?>
</div>