<?php
class CentralizacionFactory extends Factory{

  private $fc;
  protected $title = "Centralización de compras";

  public function asignarCuentaDetalleAction(){
    $this->setFactura();
    $this->fc->tipo = $this->getTipoFacturaCC();
    $detalle = $this->getDetalleComprometido();
    $cuentas = $this->formatoCuentasSelect($this->getCuentasSeleccionables());
    $this->title = "Asignación de cuenta centralización de factura";

    $form = $this->getFormView($this->getTemplateUrl('asignarCuentaDetalle.form'),array(
      'fc' => $this->fc,
      'detalle' => $detalle,
      'cuentas' => $cuentas
    ));

    echo $this->getOverlayView($form,array(),Factory::STRING_TEMPLATE);

  }

  public function procesarAsignarCuentaDetalleAction(){
    $this->setFactura();
    $this->fc->tipo = $this->getTipoFacturaCC();
    $detalle = $this->getDetalleComprometido();

    $rows = $this->updateDetalleNuevaCuentaContable($detalle);

    if($rows == 0):
      $this->getErrorMan()->showAviso("No se ha modificado la cuenta en el detalle debido a que esta es la misma que poseía");
    else:
      $this->getErrorMan()->showConfirm("La cuenta contable asociada al detalle fue actualizada correctamente");
    endif;

  }

  private function updateDetalleNuevaCuentaContable($detalle){
    $db = $this->getConnection();
    $r = self::getRequest();

    $update = $db->prepare($db->update('tb_factura_compra_detalle',array(
      'dc_cuenta_servicio' => '?'
    ),'dc_detalle = ?'));
    $update->bindValue(1, $r->dc_cuenta_contable, PDO::PARAM_INT);
    $update->bindValue(2, $detalle->dc_detalle, PDO::PARAM_INT);

    $db->stExec($update);

    return $update->rowCount();

  }

  private function setFactura(){
    $r = self::getRequest();
    $db = $this->getConnection();

    $this->fc = $db->getRowById('tb_factura_compra',$r->dc_factura,'dc_factura');

    if($this->fc === false){
      $this->getErrorMan()->showWarning("Ocurrió un error al intentar acceder a la factura de compra. compruebe los datos de entrada y vuelva a intentarlo.");
      exit;
    }

    if($this->fc->dm_nula == 1){
      $this->getErrorMan()->showWarning("La factura de compra accedida se encuentra nula. Favor verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    }

  }

  private function getDetalleComprometido(){
    $r = self::getRequest();
    $db = $this->getConnection();

    $detalle = $db->getRowById('tb_factura_compra_detalle',intval($r->dc_detalle),'dc_detalle');

    if($detalle === false){
      $this->getErrorMan()->showWarning("El detalle seleccionado o ingresado no se encontró. compruebe los datos de entrada y vuelva a intentarlo.");
      exit;
    }

    if($this->fc->dc_factura != $detalle->dc_factura){
      $this->getErrorMan()->showWarning("El detalle que intenta modificar no corresponde a un detalle en la factura de compra. compruebe los datos de entrada y vuelva a intentarlo.");
      exit;
    }

    $detalle->producto = $db->getRowById('tb_producto',$detalle->dc_producto,'dc_producto');

    return $detalle;
  }

  private function getCuentasSeleccionables(){
    $db = $this->getConnection();

    $cuentas = $db->prepare(
               $db->select('tb_tipo_factura_compra_cuenta_contable tcc
                            JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = tcc.dc_cuenta_contable'
                            ,'cc.dc_cuenta_contable, cc.dg_cuenta_contable, cc.dg_codigo'
                            ,'tcc.dc_tipo_factura = ? AND cc.dm_activo = 1'));
    $cuentas->bindValue(1,$this->fc->dc_tipo,PDO::PARAM_INT);
    $db->stExec($cuentas);

    return $cuentas->fetchAll(PDO::FETCH_OBJ);
  }

  private function formatoCuentasSelect($cuentas){
    $arr = array();
    foreach ($cuentas as $c) {
      $arr[$c->dc_cuenta_contable] = $c->dg_cuenta_contable;
    }

    return $arr;
  }

  private function getTipoFacturaCC(){
    $db = $this->getConnection();

    $tipo = $db->getRowById('tb_tipo_factura_compra',$this->fc->dc_tipo,'dc_tipo');

    if($tipo->dm_factura_manual != 1){
      $this->getErrorMan()->showWarning("el tipo de factura asociado no permite la asociación con cuentas contables.");
      exit;
    }

    return $tipo;

  }

}
