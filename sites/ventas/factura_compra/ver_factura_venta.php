<?php
	define("MAIN",1);
	include_once("../../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	include("template_factura_compra/modo1.php");
	
	$datosFactura = $db->select("tb_factura_compra fv
	    LEFT JOIN tb_nota_venta  nv  ON nv.dc_nota_venta = fv.dc_nota_venta
	LEFT JOIN tb_orden_servicio  os  ON os.dc_orden_servicio = fv.dc_orden_servicio
	         JOIN tb_medio_pago_proveedor  mp  ON mp.dc_medio_pago = fv.dc_medio_pago
               JOIN tb_orden_compra  oc  ON oc.dc_orden_compra=fv.dc_orden_compra
               LEFT JOIN tb_orden_compra_detalle ocd ON ocd.dc_orden_compra=oc.dc_orden_compra
	    LEFT JOIN tb_tipo_cargo  tc  ON tc.dc_tipo_cargo = fv.dc_tipo_cargo
	     LEFT JOIN tb_proveedor  cl  ON cl.dc_proveedor = fv.dc_proveedor
	   LEFT JOIN tb_funcionario  ex  ON ex.dc_funcionario =fv.dc_usuario_creacion",
                
	"fv.dq_folio dq_factura,UNIX_TIMESTAMP(fv.df_emision) AS df_emision,DATE_FORMAT(fv.df_vencimiento,'%d/%m/%Y') AS df_vencimiento,
	oc.dq_orden_compra,nv.dq_nota_venta,os.dq_orden_servicio,fv.dq_neto,fv.dq_iva,fv.dq_total,fv.dc_factura,mp.dg_medio_pago,
	tc.dg_tipo_cargo,cl.dg_razon,cl.dg_rut,cl.dg_giro,
	CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno) dg_ejecutivo,fv.dg_comentario",
	" fv.dc_empresa={$empresa} AND fv.dc_factura = {$_GET['id']}");
	if(!count($datosFactura)){
		$error_man->showWarning("La factura de compra especificada no existe");
		exit();
	}
	$datosFactura = $datosFactura[0];
	
	setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
	$datosFactura['df_emision'] = strftime("%d de %B de %Y",$datosFactura['df_emision']);
	
	
	$datosFactura['detalle'] = $db->select("(SELECT * FROM tb_factura_compra_detalle WHERE dc_factura={$datosFactura['dc_factura']}) d
	LEFT JOIN tb_producto p ON p.dg_codigo = d.dc_producto AND p.dc_empresa = {$empresa}
	LEFT JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto",
	'p.dg_producto,d.dc_cantidad,d.dg_descripcion,d.dg_serie,d.dq_precio,d.dq_descuento,d.dq_total,t.dm_controla_inventario');
	
	foreach($datosFactura['detalle'] as $i => $v){
		if($v['dm_controla_inventario'] != 0)
			$datosFactura['detalle'][$i]['dg_producto'] = '';
	}
	
	$pdf = new PDF();
	$pdf->AddDetalle($datosFactura['detalle']);
        
	$pdf->Output();
	
	
?>