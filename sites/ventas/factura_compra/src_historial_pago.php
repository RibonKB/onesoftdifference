<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_factura = intval($_POST['id']);

//Datos de la factura
$factura = $db->prepare($db->select('tb_factura_compra fc','fc.dq_factura, fc.dq_folio, fc.dq_monto_pagado','fc.dc_factura = ? AND fc.dc_empresa = ?'));
$factura->bindValue(1,$dc_factura,PDO::PARAM_INT);
$factura->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($factura);
$factura = $factura->fetch(PDO::FETCH_OBJ);

if($factura === false){
	$error_man->showWarning('La factura seleccionada es inválida, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Obtener pagos por propuesta de pago
$propuesta = $db->prepare($db->select('tb_propuesta_pago_detalle d
	JOIN tb_propuesta_pago p ON p.dc_propuesta = d.dc_propuesta
	LEFT JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = p.dc_medio_pago',
	'd.dq_monto, p.dq_propuesta, p.df_propuesta, m.dg_medio_pago',
	'd.dc_factura = ? AND p.dc_empresa = ? AND p.dm_nula = 0'));
$propuesta->bindValue(1,$dc_factura,PDO::PARAM_INT);
$propuesta->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($propuesta);
$propuesta = $propuesta->fetchAll(PDO::FETCH_OBJ);

//Obtener pagos por nota de crédito
$nota_credito = $db->prepare($db->select('tb_nota_credito_proveedor_factura d
	JOIN tb_nota_credito_proveedor nc ON nc.dc_nota_credito = d.dc_nota_credito',
	'd.dq_monto_saldado, d.dm_estado, nc.dq_nota_credito, nc.dq_folio, nc.df_emision',
	'd.dc_factura = ? AND nc.dc_empresa = ?'));
$nota_credito->bindValue(1,$dc_factura,PDO::PARAM_INT);
$nota_credito->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_credito);
$nota_credito = $nota_credito->fetchAll(PDO::FETCH_OBJ);

?>
<div class="secc_bar">
	Información de pagos de factura: <b><?php echo $factura->dq_factura ?></b> folio: <b><?php echo $factura->dq_folio ?></b>
</div>
<div class="panes">
	<?php if(count($propuesta)): ?>
    <table class="tab" width="100%">
    	<caption>Pago realizados con propuestas de pago</caption>
        <thead>
            <tr>
            	<th>Número de propuesta</th>
                <th>Medio de pago</th>
                <th>Fecha</th>
                <th>Monto</th>
            </tr>
        </thead>
        <tbody>
    	<?php while($p = array_shift($propuesta)): $factura->dq_monto_pagado -= $p->dq_monto; ?>
        	<tr>
            	<td><b><?php echo $p->dq_propuesta ?></b></td>
                <td><?php echo $p->dg_medio_pago ?></td>
                <td><?php echo $db->dateLocalFormat($p->df_propuesta) ?></td>
                <td align="right"><?php echo moneda_local($p->dq_monto) ?></td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
    <br />
    <?php endif; ?>
    
    <?php if(count($nota_credito)): ?>
    <table class="tab" width="100%">
    	<caption>Descuento con Nota de Crédito</caption>
        <thead>
        	<tr>
            	<th>Número Nota de Crédito</th>
                <th>Folio</th>
                <th>Emisión</th>
                <th>Estado</th>
                <th>Monto</th>
            </tr>
        </thead>
        <tbody>
        <?php 
			while($nc = array_shift($nota_credito)):
				if($nc->dm_estado == 'A')
					$factura->dq_monto_pagado -= $nc->dq_monto_saldado;
		?>
        	<tr>
            	<td><b><?php echo $nc->dq_nota_credito ?></b></td>
                <td><b><?php echo $nc->dq_folio ?></b></td>
                <td><?php echo $db->dateLocalFormat($nc->df_emision) ?></td>
                <td><?php echo $nc->dm_estado ?></td>
                <td align="right"><?php echo moneda_local($nc->dq_monto_saldado) ?></td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
    <br />
    <?php endif; ?>
    
    <?php if($factura->dq_monto_pagado > 0): ?>
    	<table class="tab" width="60%" align="center">
        	<caption>Otros Pagos</caption>
            <tbody>
            	<tr>
                	<td>Pagos anticipados</td>
                    <td align="right"><b><?php echo moneda_local($factura->dq_monto_pagado) ?></b></td>
                </tr>
            </tbody>
        </table>
        <br />
    <?php endif; ?>
</div>