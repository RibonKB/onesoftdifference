<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_factura = intval($_POST['id']);

//Verificar factura y obtener detalles
$factura = $db->prepare($db->select('tb_factura_compra','dq_factura, dm_nula, dq_total, dq_neto, dq_iva, dq_exento','dc_factura = ? AND dc_empresa = ?'));
$factura->bindValue(1,$dc_factura,PDO::PARAM_INT);
$factura->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($factura);
$factura = $factura->fetch(PDO::FETCH_OBJ);

if($factura === false){
	$error_man->showWarning('La factura seleccionada es inválida, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Edición Totales Factura de compra
</div>
<div class="panes">
	<?php $form->Start('sites/ventas/factura_compra/proc/editar_totales_factura.php','editar_totales') ?>
    <?php $form->Header('<strong>Indique los datos actualizados de la factura</strong><br />Los campos marcados con [*] son obligatorios') ?>
    	<table class="tab" width="100%">
        	<tbody>
            	<tr>
                	<td align="right" width="100"><b>Exento</b></td>
                    <td><?php $form->Text('','dq_exento',1,Form::DEFAULT_TEXT_LENGTH,$factura->dq_exento) ?></td>
                </tr>
            	<tr>
                	<td align="right" width="100"><b>Neto</b></td>
                    <td><?php $form->Text('','dq_neto',1,Form::DEFAULT_TEXT_LENGTH,$factura->dq_neto) ?></td>
                </tr>
                <tr>
                	<td align="right"><b>IVA</b></td>
                    <td><?php $form->Text('','dq_iva',1,Form::DEFAULT_TEXT_LENGTH,$factura->dq_iva) ?></td>
                </tr>
                <tr>
                	<td align="right"><b>Total</b></td>
                    <td><?php $form->Text('','dq_total',1,Form::DEFAULT_TEXT_LENGTH,$factura->dq_total) ?></td>
                </tr>
            </tbody>
        </table>
    <?php $form->Hidden('dc_factura',$dc_factura) ?>
    <?php $form->End('Editar','editbtn') ?>
</div>