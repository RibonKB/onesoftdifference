<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$dc_factura = intval($_POST['id']);

$factura = $db->select('tb_factura_compra','dq_factura, dq_folio, df_libro_compra',"dc_factura = {$dc_factura} AND dc_empresa = {$empresa}");
$factura = array_shift($factura);

if($factura == NULL){
	$error_man->showWarning('No se encontró la factura especificada, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

?>
<div class="secc_bar">Edición Fecha Contable Factura de compra <u><?php echo $factura['dq_factura'] ?></u> folio: <u><?php echo $factura['dq_folio'] ?></u></div>
<div class="panes center">
	<?php
		$form->Start('sites/ventas/factura_compra/proc/editar_fecha_libro_compra.php','ed_fecha_contable');
		$form->Header("<b>Indique los datos para editar la fecha de contabilización de la factura de compra</b><br />
						Los campos marcados con [*] son obligatorios.");
		$form->Date('Fecha contable','df_libro_compra',1,$factura['df_libro_compra']);
		$form->Hidden('dc_factura',$dc_factura);
		$form->End('Editar','editbtn');
	?>
</div>