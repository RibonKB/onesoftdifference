<?php


/**
 * Description of AsociarGuiaRecepcionFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 13-11-2013
 */
class AsociarGuiaRecepcionFactory extends Factory{
  
    protected $title = "Asociar Guía de Recepcion";
    
    public function indexAction(){
        $form = $this->getFormView($this->getTemplateURL('index.form'),array(
            'factura' => $this->getConnection()->getRowById('tb_factura_compra', self::getRequest()->dc_factura, 'dc_factura')
        ));
        
        echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
    }
    
    public function procesarAsignacionAction(){
        $guia_recepcion = $this->getGuiaRecepcion(self::getRequest()->dq_guia_recepcion);
        
        if($guia_recepcion->dc_factura != NULL):
          if($guia_recepcion->dc_factura == self::getRequest()->dc_factura):
            $this->getErrorMan()->showAviso('La Guía de recepción ya fué asociada a la factura de compra anteriormente');
          else:
            $this->getErrorMan()->showWarning('La Guía de Recepción ya está asociada a otra factura de compra');
          endif;
          
          exit;
        endif;
        
        $this->actualizarFacturaGuia($guia_recepcion->dc_guia_recepcion, self::getRequest()->dc_factura);
        
        $this->getErrorMan()->showConfirm('La Guía de Recepción ha sido relacionada correctamente a la Factura de Compra');
    }
    
    private function getGuiaRecepcion($dq_guia_recepcion){
      $db = $this->getConnection();
      
      $data = $db->prepare($db->select('tb_guia_recepcion', '*', 'dq_guia_recepcion = ? AND dc_empresa = ?'));
      $data->bindValue(1, $dq_guia_recepcion, PDO::PARAM_INT);
      $data->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
      $db->stExec($data);
      
      $data = $data->fetch(PDO::FETCH_OBJ);
      
      if($data === false):
        $this->getErrorMan()->showWarning('La guía de recepción seleccionada no existe, compruebe los datos de entrada y vuelva a intentarlo');
        exit;
      endif;
      
      return $data;
      
    }
    
    private function actualizarFacturaGuia($dc_guia_recepcion, $dc_factura){
      $db = $this->getConnection();
      
      $update = $db->prepare($db->update('tb_guia_recepcion', array(
          'dc_factura' => '?'
      ), 'dc_guia_recepcion  = ?'));
      $update->bindValue(1, $dc_factura, PDO::PARAM_INT);
      $update->bindValue(2, $dc_guia_recepcion, PDO::PARAM_INT);
      
      $db->stExec($update);
    }
    
    
  
}

?>
