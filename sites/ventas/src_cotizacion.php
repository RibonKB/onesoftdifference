<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/cotizacion.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar"><?=$lang['cot_plural_may'] ?></div>
<div id="main_cont" class="center"><br /><br />
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/proc/src_cotizacion.php","src_cotizacion");
	$form->Header("<strong>Indicar los parámetros de búsqueda de la {$lang['cot_doc']}</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50">Número de cotización</td><td width="280">');
	$form->Text("Desde","cot_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','cot_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','cot_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','cot_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','cot_client','tb_cliente',array('dc_cliente','dg_razon'));
	if(check_permiso(35)){
		echo('</td><td>&nbsp;</td></tr><tr><td>Ejecutivo</td><td>');
		$form->ListadoMultiple('','cot_executive','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 1 AND dm_activo = 1");
	}else
		$form->Hidden('cot_executive[]',$userdata['dc_funcionario']);
	if(check_permiso(58)){
	echo('</td><td>&nbsp;</td></tr><tr><td>Ejecutivo televenta</td><td>');
		$form->ListadoMultiple('','cot_tv','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 1 AND dm_activo = 1");
	}else
		$form->Hidden('cot_tv[]',$userdata['dc_funcionario']);
	echo('</td><td>&nbsp;</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#cot_client,#cot_executive,#cot_tv').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>