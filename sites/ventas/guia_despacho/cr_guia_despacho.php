<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/lang/{$empresa}/nota_venta.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Creación de Guía de Despacho</div>
<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/guia_despacho/proc/cr_guia_despacho.php","cr_factura_venta","cValidar");
	$form->Header("<strong>Indique el RUT del cliente al que se le hará la guía de despacho</strong>
	<br />Los criterios de búsqueda son el RUT, Razon social o giro del cliente");
	$form->Text("RUT cliente","cli_rut",1,20);
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
$("#cli_rut").autocomplete('sites/proc/autocompleter/cliente.php',{
	formatItem: function(row){ return row[1]+" ("+row[0]+") "+row[2]; },
	minChars: 2,
	width:300
});
</script>