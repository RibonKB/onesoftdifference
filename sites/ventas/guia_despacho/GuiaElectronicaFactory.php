<?php
require_once("Numbers/Words.php");

class GuiaElectronicaFactory extends Factory{

	protected $title = "Guía de despacho Electrónica";
	private $tipoDocumento = '52';
	private $actividadEconomica = '523924';
	private $sucursal = '1';
	private $servidorFTP = "192.168.0.149";
	
	public function indexAction(){
		$form = $this->getFormView($this->getTemplateURL('index.form'),array(
			'dc_guia_despacho' => self::getRequest()->dc_guia_despacho
		));
		echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
	}
	
	public function emitirGuiaAction(){
		$this->getErrorMan()->showConfirm('Se ha subido el archivo al servidor BSOFT Correctamente (Mentira)');
		// $guia = $this->getGuia();
		// $this->formatoDatosDocumento($guia);
		
		// $ruta = $this->crearArchivoXML($guia);
		// $this->subirArchivoFTP($ruta);
		
	}
	
	private function getGuia(){
		$r = self::getRequest();
		$db = $this->getConnection();
		
		$factura = $db->getRowById('tb_guia_despacho',$r->dc_guia_despacho,'dc_guia_despacho');
		
		if($factura === false):
			$this->getErrorMan->showWarning('La guía de despacho especificada no pudo ser obtenida, favor verificar los datos de entrada y reintentar');
			exit;
		endif;
		
		$factura->nota_venta = $this->getNotaVentaFactura($factura);
		$factura->cliente = $this->getClienteFactura($factura);
		//$factura->ejecutivo = $this->getEjecutivoFactura($factura);
		//$factura->medio_pago = $this->getMedioPagoFactura($factura);
		//$factura->tipo_operacion = $this->getTipoOperacionFactura($factura);
		$factura->detalle = $this->getFacturaDetalle($factura);
		
		return $factura;
		
	}
	
	private function formatoDatosDocumento(&$factura){
		$factura->df_emision = $this->formatearFecha($factura->df_emision);
		//$factura->df_vencimiento = $this->formatearFecha($factura->df_vencimiento);
		$factura->cliente->dg_rut = $this->formatearRut($factura->cliente->dg_rut);
		$factura->cliente->dg_giro = substr($factura->cliente->dg_giro,0,40);
		$factura->dq_neto = intval($factura->dq_neto);
		$factura->dq_iva = intval($factura->dq_iva);
		$factura->dq_total = intval($factura->dq_total);
		
		$factura->cliente->dg_razon = substr(str_replace('&','and',$factura->cliente->dg_razon),0,80);
		$factura->cliente->dg_razon = substr($factura->cliente->dg_razon,0,100);
		$factura->cliente->contacto->dg_contacto = substr($factura->cliente->contacto->dg_contacto,0,80);
		$factura->cliente->contacto->sucursal->dg_direccion = substr($factura->cliente->contacto->sucursal->dg_direccion,0,70);
		$factura->cliente->contacto->sucursal->comuna->dg_comuna = substr($factura->cliente->contacto->sucursal->comuna->dg_comuna,0,20);
		$factura->cliente->contacto->sucursal->ciudad->dg_region = substr($factura->cliente->contacto->sucursal->ciudad->dg_region,0,20);
		
		
		foreach($factura->detalle as &$d):
			$d->dq_precio = intval($d->dq_precio);
			//$d->dq_total = intval($d->dq_total);
			$d->dg_producto = substr(str_replace('&','and',$d->dg_producto),0,10);
			$d->dg_nombre_producto = substr(str_replace('&','and',$d->dg_descripcion),0,70);
			$d->dg_descripcion = str_replace('&','and',$d->dg_descripcion);
		endforeach;
		
	}
	
	private function getFacturaDetalle($factura){
		$db = $this->getConnection();
		
		$detalle = $db->prepare($db->select('tb_guia_despacho_detalle','*','dc_guia_despacho = ?'));
		$detalle->bindValue(1,$factura->dc_guia_despacho,PDO::PARAM_INT);
		$db->stExec($detalle);
		
		return $detalle->fetchAll(PDO::FETCH_OBJ);
	}
	
	private function getNotaVentaFactura($factura){
		$db = $this->getConnection();
		return $db->getRowById('tb_nota_venta',$factura->dc_nota_venta,'dc_nota_venta');
	}
	
	private function getClienteFactura($factura){
		$db = $this->getConnection();
		$cliente = $db->getRowById('tb_cliente',$factura->dc_cliente,'dc_cliente');
		$cliente->contacto = $this->getContactoCliente($factura);
		return $cliente;
	}
	
	private function getEjecutivoFactura($factura){
		$db = $this->getConnection();
		return $db->getRowById('tb_funcionario',$factura->dc_ejecutivo,'dc_funcionario');
	}
	
	private function getMedioPagoFactura($factura){
		$db = $this->getConnection();
		return $db->getRowById('tb_medio_pago',$factura->dc_medio_pago,'dc_medio_pago');
	}
	
	private function getTipoOperacionFactura($factura){
		$db = $this->getConnection();
		return $db->getRowById('tb_tipo_operacion',$factura->dc_tipo_operacion,'dc_tipo_operacion');
	}
	
	private function getContactoCliente($factura){
		$db = $this->getConnection();
		$contacto = $db->getRowById('tb_contacto_cliente',$factura->dc_contacto,'dc_contacto');
		$contacto->sucursal = $db->getRowById('tb_cliente_sucursal',$contacto->dc_sucursal,'dc_sucursal');
		$contacto->sucursal->comuna = $db->getRowById('tb_comuna',$contacto->sucursal->dc_comuna,'dc_comuna');
		$contacto->sucursal->ciudad = $db->getRowById('tb_region',$contacto->sucursal->comuna->dc_region,'dc_region');
		return $contacto;
	}
	
	private function crearArchivoXML($factura){
		$dom = new DOMDocument('1.0','UTF-8');
		$dom->xmlStandalone = true;
		
		//Crear la raiz DTE
		$dte = $dom->createElement('DTE');
		$dte_version = $dom->createAttribute('version');
		$dte_version->value = '1.0';
		$dte->appendChild($dte_version);
		$dom->appendChild($dte);
		
		//Crear el documento como tal
		$documento = $dom->createElement('Documento');
		$id_documento = $dom->createAttribute('ID');
		$id_documento->value = $this->crearIdDocumento($factura);
		$documento->appendChild($id_documento);
		$dte->appendChild($documento);
		
		//Crear el encabezado
		$encabezado = $dom->createElement('Encabezado');
		$documento->appendChild($encabezado);
		
		//Identificador del documento
		$id_doc = $dom->createElement('IdDoc');
		$id_doc->appendChild($dom->createElement('TipoDTE',$this->tipoDocumento));
		$id_doc->appendChild($dom->createElement('Folio',$factura->dq_folio));
		$id_doc->appendChild($dom->createElement('FchEmis',$factura->df_emision));
		//$id_doc->appendChild($dom->createElement('FmaPago',$factura->medio_pago->dg_codigo_sii));
		//$id_doc->appendChild($dom->createElement('TermPagoGlosa',$factura->medio_pago->dg_medio_pago));
		//$id_doc->appendChild($dom->createElement('FchVenc',''));
		$encabezado->appendChild($id_doc);
		
		//Emisor del documento
		$empresa = $this->getConnection()->getRowById('tb_empresa',$this->getEmpresa(),'dc_empresa');
		$emisor = $dom->createElement('Emisor');
		$emisor->appendChild($dom->createElement('RUTEmisor',$this->getRutEmpresa()));
		$emisor->appendChild($dom->createElement('RznSoc',strtoupper($empresa->dg_fantasia)));
		$emisor->appendChild($dom->createElement('GiroEmis',substr(strtoupper($empresa->dg_giro),0,80)));
		$emisor->appendChild($dom->createElement('Acteco',$this->actividadEconomica));
		$emisor->appendChild($dom->createElement('Sucursal',$this->sucursal));
		$emisor->appendChild($dom->createElement('DirOrigen',$empresa->dg_direccion));
		$emisor->appendChild($dom->createElement('CmnaOrigen','PROVIDENCIA'));
		$emisor->appendChild($dom->createElement('CiudadOrigen','SANTIAGO'));
		$emisor->appendChild($dom->createElement('CdgVendedor',''));
		$encabezado->appendChild($emisor);
		
		//Receptor del documento
		$receptor = $dom->createElement('Receptor');
		$receptor->appendChild($dom->createElement('RUTRecep',$factura->cliente->dg_rut));
		$receptor->appendChild($dom->createElement('RznSocRecep',$factura->cliente->dg_razon));
		$receptor->appendChild($dom->createElement('GiroRecep',$factura->cliente->dg_giro));
		
		//Datos extra del receptor
		$receptor->appendChild($dom->createElement('DirRecep',$factura->cliente->contacto->sucursal->dg_direccion));
		$receptor->appendChild($dom->createElement('CmnaRecep',$factura->cliente->contacto->sucursal->comuna->dg_comuna));
		
		$encabezado->appendChild($receptor);
		
		//Totales documento
		/*
			<Totales> 
				<MntNeto>XXXXXXX</MntNeto> 
				<TasaIVA>19</TasaIVA> 
				<IVA>XXXXX</IVA> 
				<MntTotal>XXXXXXXXX</MntTotal> 
			</Totales>
		*/
		$totales = $dom->createElement('Totales');
		$totales->appendChild($dom->createElement('MntNeto',$factura->dq_neto));
		$totales->appendChild($dom->createElement('TasaIVA',19));
		$totales->appendChild($dom->createElement('IVA',$factura->dq_iva));
		$totales->appendChild($dom->createElement('MntTotal',$factura->dq_total));
		$encabezado->appendChild($totales);
		
		//Detalles
		/*
			<Detalles>
				<Detalle>
					<NroLinDet>1</NroLinDet> 
					<CdgItem>XXXXXXXXX</CdgItem>
					<NmbItem>XXXXXXXXXXXXXXXX</NmbItem> 
					<DscItem>XXXXXXXXXXXXXXXX</DscItem> 
					<QtyItem>XX</QtyItem> 
					<UnmdItem>--</UnmdItem> 
					<PrcItem>XXXXXXX</PrcItem> 
					<MontoItem>XXXXX</MontoItem>
				</Detalle>
				<Detalle>
					<NroLinDet>2</NroLinDet> 
					<CdgItem>XXXXXXXXX</CdgItem>
					<NmbItem>XXXXXXXXXXXXXXXX</NmbItem> 
					<DscItem>XXXXXXXXXXXXXXXX</DscItem> 
					<QtyItem>XX</QtyItem> 
					<UnmdItem>--</UnmdItem> 
					<PrcItem>XXXXXXX</PrcItem> 
					<MontoItem>XXXXX</MontoItem>
				</Detalle>
			</Detalles>
		*/
		$detalles = $dom->createElement('Detalles');
		foreach($factura->detalle as $i => $d):
			$detalle = $dom->createElement('Detalle');
			$detalle->appendChild($dom->createElement('NroLinDet',$i+1));
			
			$cdgitem = $dom->createElement('CdgItem');
			$cdgitem->appendChild($dom->createElement('TpoCodigo','INT1'));
			$cdgitem->appendChild($dom->createElement('VlrCodigo',$d->dg_producto));
			
			//$detalle->appendChild($dom->createElement('CdgItem',$d->dg_producto));
			$detalle->appendChild($cdgitem);
			
			$detalle->appendChild($dom->createElement('NmbItem',$d->dg_nombre_producto));
			$detalle->appendChild($dom->createElement('DscItem',$d->dg_descripcion));
			$detalle->appendChild($dom->createElement('QtyItem',$d->dq_cantidad));
			$detalle->appendChild($dom->createElement('UnmdItem','Un.'));
			// El SII indica que para casos en los que no se use el precio del ítem, 
			// debe ser informado en Blanco
			// if($d->dq_precio != 0):
				// $detalle->appendChild($dom->createElement('PrcItem',$d->dq_precio));
			// endif;
			$detalle->appendChild($dom->createElement('PrcItem',$d->dq_precio));
			$detalle->appendChild($dom->createElement('MontoItem',$d->dq_precio*$d->dq_cantidad));
			
			$documento->appendChild($detalle);
		endforeach;
		//$documento->appendChild($detalles);
		
		//Parametros personalizados
		/*
			<Parametros>
				<MontoEscrito>-------------------</MontoEscrito>
				<VendedorGlosa>nombre vendedor</VendedorGlosa>
				<TipoOperacion>nombre tipo operación</TipoOperacion>
				<GlosaCabecera>-------------</GlosaCabecera>
				<OrdenCompra>XXXXXXX</OrdenCompra>
				<NotaVenta>XXXXXXX</Notaventa>
			</Parametros>
		*/
		$parametros = $dom->createElement('Parametros');
		$nw = new Numbers_Words();
		$parametros->appendChild($dom->createElement('MontoEscrito',strtoupper($nw->toWords($factura->dq_total,'es'))));
		$parametros->appendChild($dom->createElement('VendedorGlosa',''));
		$parametros->appendChild($dom->createElement('TipoOperacion',''));
		$parametros->appendChild($dom->createElement('GlosaCabecera',$factura->dg_comentario));
		$parametros->appendChild($dom->createElement('OrdenCompra',$factura->dg_orden_compra));
		$parametros->appendChild($dom->createElement('FechaEmisGlosa',$this->getFechaGlosa($factura->df_emision)));
		$parametros->appendChild($dom->createElement('FechaVencGlosa',''));
		
		if($factura->nota_venta != false):
			$parametros->appendChild($dom->createElement('NotaVenta',$factura->nota_venta->dq_nota_venta));
		else:
			$parametros->appendChild($dom->createElement('NotaVenta',''));
		endif;
		
		//Datos extra del receptor
		$parametros->appendChild($dom->createElement('ContactoRecep',$factura->cliente->contacto->dg_contacto));
		$parametros->appendChild($dom->createElement('DirRecep',$factura->cliente->contacto->sucursal->dg_direccion));
		$parametros->appendChild($dom->createElement('DirEntregaRecep',$factura->cliente->contacto->sucursal->dg_direccion));
		$parametros->appendChild($dom->createElement('CmnaRecep',$factura->cliente->contacto->sucursal->comuna->dg_comuna));
		$parametros->appendChild($dom->createElement('CiudadRecep',$factura->cliente->contacto->sucursal->ciudad->dg_region));
		$parametros->appendChild($dom->createElement('Telefono',$factura->cliente->contacto->dg_fono));
		
		foreach($factura->detalle as $i => $d):
			$especs = $dom->createElement('Especificaciones');
			$especs->appendChild($dom->createElement('EspecItem','1'));
			$especs->appendChild($dom->createElement('EspecItem0','\n'));
			$especs->appendChild($dom->createElement('EspecItem0','\n'));
			$parametros->appendChild($especs);
		endforeach;
		
		$dte->appendChild($parametros);
		
		$filename = 'E'.$this->getEmpresa().'GD'.$factura->dq_folio.'.xml';
		
		$xml_text = $this->quitarTildes($dom->saveXML());
		
		$file = fopen('/tmp/'.$filename,'w');
		fwrite($file,$xml_text);
		fclose($file);
		//$dom->save('/tmp/'.$filename);
		
		return $filename;
		
	}
	
	private function crearIdDocumento($factura){
		
		return 'R'.$this->getRutEmpresa().'T'.$this->tipoDocumento.'F'.$factura->dq_folio;
	}
	
	private function getRutEmpresa(){
		$empresa = $this->getConnection()->getRowById('tb_empresa',$this->getEmpresa(),'dc_empresa');
		return $this->formatearRut($empresa->dg_rut);
	}
	
	private function formatearFecha($fecha){
		return substr($fecha,0,10);
	}
	
	private function formatearRut($rut){
		return strtoupper(str_replace('.','',$rut));
	}
	
	private function subirArchivoFTP($ruta){
		$ftp = ftp_connect($this->servidorFTP) or die("No se pudo conectar");
		
		$login_result = ftp_login($ftp, "administrator", "Soporte632");
		ftp_pasv($ftp, true);
		
		if(ftp_put($ftp, $ruta, '/tmp/'.$ruta, FTP_ASCII)){
			$this->getErrorMan()->showConfirm('Se ha subido el archivo al servidor BSOFT Correctamente');
		}else{
			$this->getErrorMan()->showWarning('No se ha podido subir el archivo al servidor de BSOFT');
		}

		ftp_close($ftp);
		
	}
	
	private function getFechaGlosa($fecha){
		$meses = array("NO-DATE", "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		
		$date = new DateTime($fecha);
		return $date->format("d")." de ".$meses[$date->format("n")]. " de ".$date->format("Y");
	}
	
	private function quitarTildes($cadena) {
		$no_permitidas	= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas		= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","A","I","O","U","a" ,"A","A" ,"A" ,"A" ,"A" ,"C","C","A" ,"e","A" ,"A","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
		$texto = str_replace($no_permitidas, $permitidas ,$cadena);
		return $texto;
	}
	
}
