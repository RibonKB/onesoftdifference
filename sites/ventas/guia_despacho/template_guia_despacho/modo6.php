<?php
require("../../../inc/fpdf.php");
//require_once("Numbers/Words.php");

class PDF extends FPDF{

private $datosEmpresa;
private $margenY = 1.0;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','cm',array(21.5,28.2));
	
}

function Header(){
	global $datosGuia;
	
	//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
	//$this->SetXY(x,y);
	//MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
	
	$this->SetFont('Arial','',12);
	$this->SetTextColor(0,0,0);
	
	$this->SetXY(12.5,$this->margenY);//4.2);
	$this->Cell(8,0.5,$datosGuia['dq_guia_despacho'],0,0,'C');
	
	$this->SetFont('Arial','',9);
	$this->SetXY(3,$this->margenY+3.2);
	$this->MultiCell(10,0.44,
	"{$datosGuia['dg_razon']}\n{$datosGuia['dg_direccion']}\n{$datosGuia['dg_region']}\n{$datosGuia['dg_codigo_postal']}\n{$datosGuia['dg_rut']}\n".
	"{$datosGuia['dg_direccion_entrega']} {$datosGuia['dg_comuna_entrega']} {$datosGuia['dg_region_entrega']}\n{$datosGuia['dg_contacto_entrega']}");
	
	$this->setXY(9,4.5+$this->margenY);
	$this->MultiCell(4,0.44,"{$datosGuia['dg_comuna']}\n{$datosGuia['dg_fono']}");
	
	$this->SetXY(16.5,5.5+$this->margenY);
	$this->MultiCell(4,0.5,"{$datosGuia['df_emision']}\n{$datosGuia['dg_orden_compra']}\n{$datosGuia['dq_nota_venta']}\n\n{$datosGuia['dg_guia_referencia']}");
	
}

function Footer(){
	/*global $datosGuia;
	
	$nw = new Numbers_Words();
	$this->SetXY(1.7,20.7+$this->margenY);
	$this->Cell(14.5,0.6,ucfirst($nw->toWords($datosGuia['dq_total'],"es")));
	
	$this->SetFont('Arial','',12);
	$this->SetXY(18.3,20.7+$this->margenY);
	$this->Cell(2.5,0.83,moneda_local($datosGuia['dq_neto']),0,2,'R');
	$this->Cell(2.5,0.83,moneda_local($datosGuia['dq_iva']),0,2,'R');
	$this->Cell(2.5,0.83,moneda_local($datosGuia['dq_total']),0,2,'R');*/
}

function AddDetalle($detalle){
	global $datosGuia,$empresa_conf;
	
	$this->AddPage();
	$this->SetFont('Arial','',9);
	$this->SetFillColor(255,255,255);
	
	$this->SetY(11.3+$this->margenY);
	foreach($detalle as $det){
		$det['dq_total'] = moneda_local($det['dq_precio']*$det['dq_cantidad']);
		$det['dq_precio'] = moneda_local($det['dq_precio']*$det['dq_cantidad']);
		$this->SetX(1.7);
		$this->Cell(1,0.476,$det['dq_cantidad']);
		$code_large = $this->GetStringWidth($det['dg_producto']);
		if($code_large > 2.3){
			$desc_margin = $code_large-2.3;
		}else{
			$desc_margin = 0;
		}
		$this->SetFont('','B');
		$this->Cell(2.5+$desc_margin,0.476,$det['dg_producto']);
		$this->SetFont('','');
		$this->Cell(10-$desc_margin,0.476,substr($det['dg_descripcion'],0,49).'.');
		$y = $this->GetY();
		$x = $this->GetX()+3.3;
		
		$this->Cell(2.5,0.476,$det['dg_serie'],0,2);
		
		$y2 = $this->GetY();
		
		$this->SetY($y);
		$this->SetX($x);
		
		$this->Cell(1.8,0.476,$det['dq_precio'],0,0,'R');
		$this->SetY($y2);
		
	}
	
	$this->Ln(2);
	$this->SetX(4);
	$this->MultiCell(15,0.5,$datosGuia['dg_comentario']);
	
	/*$y = $this->GetY();
	if($datosOrden['dg_observacion']){
		$this->SetFont('Arial','',7);
		$this->MultiCell(116,3,"Observaciones: \n".$datosOrden['dg_observacion'],1);
	}*/
	
}

}
?>