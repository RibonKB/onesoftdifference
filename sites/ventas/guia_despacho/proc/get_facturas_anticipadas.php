<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_nota_venta = intval($_GET['dc_nota_venta']);
$dm_facturable = intval($_GET['facturable']);

if(!is_numeric($dc_nota_venta)){
	echo json_encode('<not-found>');
	exit;
}

if($dm_facturable == 1){
	$dm_anticipada = "AND dm_anticipada = 'A'";
}else
	$dm_anticipada = '';

$st = $db->prepare($db->select(
"(SELECT * FROM tb_factura_venta WHERE dc_nota_venta = ? {$dm_anticipada} AND dc_empresa = {$empresa} AND dm_nula = 0) fv
JOIN tb_factura_venta_detalle d ON fv.dc_factura = d.dc_factura
LEFT JOIN tb_guia_despacho gd ON gd.dc_factura = fv.dc_factura
LEFT JOIN tb_guia_despacho_detalle dgd ON gd.dc_guia_despacho = dgd.dc_guia_despacho AND d.dc_detalle_nota_venta = dgd.dc_detalle_nota_venta",
	'fv.dc_factura,fv.dq_factura,d.dg_producto,SUM(d.dc_cantidad) dq_facturado,SUM(dgd.dq_cantidad) dq_despachado, fv.dm_anticipada',
	'',array('group_by'=>'fv.dc_factura,d.dg_producto')));
	
$st->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$db->stExec($st);

$detalles = array();
$dc_factura = array();

while($det = $st->fetch(PDO::FETCH_OBJ)){
	if($det->dq_despachado == NULL)
		$det->dq_despachado = 0;
	$detalles[$det->dq_factura][] = array($det->dg_producto,$det->dq_facturado,$det->dq_despachado);
	$dc_factura[$det->dq_factura] = $det->dc_factura;
}

if(!count($detalles)){
	echo json_encode('<not-found>');
	exit;
}

echo json_encode(array($detalles,$dc_factura));
?>