<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once('../../proc/ventas_functions_old.php');

$dc_guia_despacho = $_POST['dc_guia_despacho'];

$data = $db->select('tb_guia_despacho','dc_nota_venta,dc_orden_servicio,dc_cliente,dm_nula',"dc_guia_despacho={$dc_guia_despacho}");

if(!count($data)){
	$error_man->showWarning("No se encuentra la guía de despacho a anular");
	exit;
}

if($data[0]['dm_nula'] == 1){
	$error_man->showWarning('La guía de despacho ya ha sido anulada, no puede continuar.');
	exit;
}

$dc_nota_venta = $data[0]['dc_nota_venta'];
$dc_orden_servicio = $data[0]['dc_orden_servicio'];
$dc_cliente = $data[0]['dc_cliente'];

/*
*	Descontar cantidad en nota de venta (en cabecera y detalle)
*/

$detalles = $db->select("tb_guia_despacho_detalle d
JOIN tb_producto p ON d.dg_producto = p.dg_codigo AND p.dc_empresa = {$empresa}",
	'd.dc_detalle_nota_venta,d.dc_detalle_orden_servicio,SUM(d.dq_cantidad) dq_cantidad,d.dc_bodega_salida,d.dc_bodega_entrada,p.dc_producto,p.dq_precio_compra',
	"d.dc_guia_despacho = {$dc_guia_despacho}",
	array('group_by' => 'd.dg_producto'));
	
$db->start_transaction();
	
$total_despachado = 0;
if($dc_nota_venta > 0){
	foreach($detalles as $d){
		$db->update('tb_nota_venta_detalle',array(
			'dc_despachada' => "dc_despachada-{$d['dq_cantidad']}"
		),"dc_nota_venta_detalle = {$d['dc_detalle_nota_venta']}");
		
		$total_despachado += $d['dq_cantidad'];
	}
}else if($dc_orden_servicio > 0){
	foreach($detalles as $d){
		$db->update('tb_orden_servicio_factura_detalle',array(
			'dq_despachado' => "dq_despachado-{$d['dq_cantidad']}"
		),"dc_detalle = {$d['dc_detalle_orden_servicio']}");
	}
}

if($total_despachado > 0){
	if($dc_nota_venta > 0)
		$db->update('tb_nota_venta',array(
			'dc_despachada' => "dc_despachada-{$total_despachado}"
		),"dc_nota_venta = {$dc_nota_venta}");
}

/*
*	ingreso de stock en las bodegas
*/

$tipo_movimiento = $db->select('tb_configuracion_logistica','dc_tipo_movimiento_devolucion_gd dc_tipo',"dc_empresa={$empresa}");
$tipo_movimiento = $tipo_movimiento[0]['dc_tipo'];

foreach($detalles as $d){
	
	//carga de stock
	if($dc_nota_venta > 0){
		$db->update('tb_stock',array(
			'dq_stock' => "dq_stock+{$d['dq_cantidad']}",
			'dq_stock_reservado' => "dq_stock_reservado+{$d['dq_cantidad']}"
		),"dc_producto = {$d['dc_producto']} AND dc_bodega = {$d['dc_bodega_salida']}");
	}else if($dc_orden_servicio > 0){
		$db->update('tb_stock',array(
			'dq_stock' => "dq_stock+{$d['dq_cantidad']}",
			'dq_stock_reservado_os' => "dq_stock_reservado_os+{$d['dq_cantidad']}"
		),"dc_producto = {$d['dc_producto']} AND dc_bodega = {$d['dc_bodega_salida']}");
	}else{
		$db->update('tb_stock',array(
			'dq_stock' => "dq_stock+{$d['dq_cantidad']}"
		),"dc_producto = {$d['dc_producto']} AND dc_bodega = {$d['dc_bodega_salida']}");
	}
	
	//movimiento entrada
	$db->insert('tb_movimiento_bodega',array(
		"dc_cliente" => $dc_cliente,
		"dc_tipo_movimiento" => $tipo_movimiento,
		"dc_bodega_entrada" => $d['dc_bodega_salida'],
		"dc_guia_despacho" => $dc_guia_despacho,
		"dc_nota_venta" => $dc_nota_venta,
		"dc_orden_servicio" => $dc_orden_servicio,
		"dc_producto" => $d['dc_producto'],
		"dq_cantidad" => $d['dq_cantidad'],
		"dq_monto" => $d['dq_precio_compra'],
		"dc_empresa" => $empresa,
		"df_creacion" => 'NOW()',
		"dc_usuario_creacion" => $idUsuario
	));
	
	if($d['dc_bodega_entrada'] > 0){
		//salida stock traslado
		bodega_RebajarStockLibre($d['dc_producto'],$d['dq_cantidad'],$d['dc_bodega_entrada']);
		
		//movimiento de salida
		$db->insert('tb_movimiento_bodega',array(
			"dc_cliente" => $dc_cliente,
			"dc_tipo_movimiento" => $tipo_movimiento,
			"dc_bodega_salida" => $d['dc_bodega_entrada'],
			"dc_guia_despacho" => $dc_guia_despacho,
			"dc_nota_venta" => $dc_nota_venta,
			"dc_orden_servicio" => $dc_orden_servicio,
			"dc_producto" => $d['dc_producto'],
			"dq_cantidad" => -$d['dq_cantidad'],
			"dq_monto" => $d['dq_precio_compra'],
			"dc_empresa" => $empresa,
			"df_creacion" => 'NOW()',
			"dc_usuario_creacion" => $idUsuario
		));
	}
	
}

/*
*	registrar motivo de anulacion
*/

$db->insert('tb_guia_despacho_anulada',array(
	'dc_guia_despacho' => $dc_guia_despacho,
	'dc_motivo' => $_POST['null_motivo'],
	//'dm_fisica' => $_POST['null_fisica'],
	'dg_comentario' => $_POST['null_comentario'],
	'dc_usuario' => $idUsuario,
	'df_anulacion' => 'NOW()'
));

$db->update('tb_guia_despacho',array(
	'dm_nula' => 1
),"dc_guia_despacho = {$dc_guia_despacho}");

$db->commit();
$error_man->showAviso("Atención a anulado la guía de despacho");
?>
<script type="text/javascript">
$('#res_list .confirm .fv_load').trigger('click');
</script>