<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

if(!is_numeric($_POST['dq_folio'])){
	$error_man->showWarning("El folio es inválido, debe indicar un número válido.");
	exit;
}

$update = $db->prepare($db->update('tb_guia_despacho',array(
	"dq_folio" => '?'
),"dc_guia_despacho = ?"));
$update->bindValue(1,$_POST['dq_folio'],PDO::PARAM_INT);
$update->bindValue(2,$_POST['dc_guia_despacho'],PDO::PARAM_INT);

$db->stExec($update);

$error_man->showConfirm("Se ha cambiado el Folio de la guía de despacho por <b>{$_POST['dq_folio']}</b>");
?>
<script type="text/javascript">
$('#res_list .confirm b').text('<?php echo $_POST['dq_folio'] ?>');
</script>