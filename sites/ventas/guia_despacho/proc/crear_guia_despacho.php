<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once('../../proc/ventas_functions_old.php');

if(isset($_POST['dc_factura_anticipada']) && $_POST['dc_factura_anticipada'] == 0){
	unset($_POST['dc_factura_anticipada']);
	unset($_POST['dc_factura_anticipada']);
}

if(count($db->select('tb_guia_despacho','1',"dq_folio={$_POST['gd_number']} AND dc_empresa={$empresa} AND dm_nula = 0"))){
	$error_man->showWarning("El número de la guía de despacho <b>{$_POST['gd_number']}</b> es inválido pues ya existe una guía con dicho número.");
	exit();
}

//Obtener los primary key de los productos que realizan movimientos en bodega a través de los códigos.
$codes = implode("','",$_POST['prod']);
$ids = array();
$dq_costo_stock = array();
$db_prod = $db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_codigo IN ('{$codes}')) p
JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto AND dm_controla_inventario = '0'",
'dc_producto ,dg_codigo, dq_precio_compra');
foreach($db_prod as $p){
	$ids[$p['dg_codigo']] = $p['dc_producto'];
	$dq_costo_stock[$p['dc_producto']] = floatval($p['dq_precio_compra']);
}
unset($db_prod);

if($_POST['gd_id_nota_venta'] != 0){
	foreach($_POST['prod'] as $i => $p){
		if(isset($ids[$p])){
			$pendiente = $_POST['cant'][$i];
			if($pendiente > 0){
				$disponible = $_POST['recepcionada'][$i]-$_POST['despachada'][$i];
				
				if($pendiente > $disponible){
					$error_man->showWarning("No hay stock reservado suficiente para la nota de venta<br />
					Error detectado en el detalle <b>{$p}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />
					Cantidad necesaria : <b>{$pendiente}</b><br />
					Cantidad Disponible: <b>{$disponible}</b><br /><br />
					Debe hacer las compras, recepciones y/o traspasos necesarios para satisfacer la necesidad de stock");
					exit;
				}//END if pendiente > disponible
				
				$to_guia[] = $i;
			}//END if pendiente
		}//END if manipula stock
	}//END foreach products
}//END if $_POST['fv_id_nota_venta']
else{
	foreach($_POST['prod'] as $i => $p){
		if(isset($ids[$p])){
			$to_guia[] = $i;
		}
	}
}

$db->start_transaction();
//$db->query("SET autocommit=0");
//$db->query("LOCK TABLES tb_stock WRITE, tb_guia_despacho WRITE, tb_nota_venta_detalle WRITE, tb_guia_despacho_detalle WRITE,tb_movimiento_bodega WRITE, tb_nota_venta WRITE");

$dc_tipo_guia = intval($_POST['gd_tipo_guia']);
$tipo_guia_q = $db->select('tb_tipo_guia_despacho','dm_stock_reservado',"dc_tipo={$dc_tipo_guia}");
$tipo_guia_q = $tipo_guia_q[0];

foreach($_POST['prod'] as $i => $p){
	if(isset($ids[$p])){
		
		//La bodega de salida es obligatoria para productos que mueven stock
		if($_POST['bodega_salida'][$i] == 0){
			$error_man->showWarning("La bodega de salida para productos que controlan inventario es obligatoria<br />
			Error encontrado en producto con código <b>{$p}</b> ({$_POST['desc'][$i]})");
			exit();
		}
		
		$bodega = $_POST['bodega_salida'][$i];

		/*Se intenta quitar stock desde la bodega de salida
		$db->update('tb_stock',array(
			'dq_stock' => "dq_stock-{$_POST['cant'][$i]}"
		),"dc_producto={$ids[$p]} AND dc_bodega={$_POST['bodega_salida'][$i]} AND dq_stock >= {$_POST['cant'][$i]}");
		
		//Si no había stock suficiente se procede a restaurar la base de datos y mostrar un error
		if($db->affected_rows() != 1){
			$error_man->showWarning("No hay stock disponible para: <b>{$_POST['desc'][$i]}</b> código: <b>{$p}</b> En la bodega especificada");
			$db->rollback();
			//$db->query("UNLOCK TABLES");
			exit();
		}*/
		
		/* Rebajar stock */
		
		if($_POST['gd_id_nota_venta'] != 0 || $tipo_guia_q['dm_stock_reservado'] == 1){
			$cantidad = $_POST['cant'][$i];
			
			$existencia = bodega_RebajarStockReservado($ids[$_POST['prod'][$i]],$cantidad,$bodega);
			
			if($existencia != 1){
				
				$nombre_bodega = $db->select('tb_bodega','dg_bodega',"dc_bodega={$bodega}");
				
				$error_man->showWarning("Hay stock reservado suficiente para la nota de venta ".
				"pero no se encontró stock disponible en bodega {$nombre_bodega[0]['dg_bodega']}<br />".
				"Error detectado en el detalle <b>{$_POST['prod'][$i]}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />".
				"Cantidad necesaria : <b>{$cantidad}</b><br />".
				"Bodega en que se busca: <b>{$nombre_bodega[0]['dg_bodega']}</b>");
				
				$db->rollBack();
				exit;
			}
		}//END if nota venta
		else{
			//Movimiento de stock si no hubiese asignación de nota de venta
				$existencia = bodega_RebajarStockLibre($ids[$_POST['prod'][$i]],$_POST['cant'][$i],$bodega);
				
				if($existencia != 1){
					$nombre_bodega = $db->select('tb_bodega','dg_bodega',"dc_bodega={$bodega}");
					
					$error_man->showWarning("No hay stock libre suficiente en la bodega <b>{$nombre_bodega[0]['dg_bodega']}</b><br />
					Error detectado en el detalle <b>{$_POST['prod'][$i]}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />
					Cantidad necesaria : <b>{$_POST['cant'][$i]}</b><br />
					Bodega en que se busca: <b>{$nombre_bodega[0]['dg_bodega']}</b>");
					
					$db->rollBack();
					exit;
				}
			
		}
		
		/* END Rebajar stock */
		
		//En el caso en que la guía de despacho sea por traslado, se suma el stock correspondiete que se sacó de la bodega de salida
		if(isset($_POST['bodega_entrada']) && !isset($_POST['dc_factura_anticipada'])){
			
			if($_POST['bodega_entrada'][$i] == 0){
				$error_man->showWarning("La bodega de entrada para productos que controlan inventario es obligatoria en el tipo de guía seleccionado<br />
				Error encontrado en producto con código <b>{$p}</b> ({$_POST['desc'][$i]})");
				exit();
			}
			
			if($tipo_guia_q['dm_stock_reservado'] == 1){
				$db->update('tb_stock',array(
					'dq_stock' => "dq_stock+{$_POST['cant'][$i]}",
					'dq_stock_reservado' => "dq_stock_reservado+{$_POST['cant'][$i]}"
				),"dc_bodega={$_POST['bodega_entrada'][$i]} AND dc_producto={$ids[$p]}");
				
				if($db->affected_rows() == 0){
					
					$db->insert('tb_stock',array(
						'dc_producto' => $ids[$p],
						'dc_bodega' => $_POST['bodega_entrada'][$i],
						'dq_stock' => $_POST['cant'][$i],
						'dq_stock_reservado' => $_POST['cant'][$i]
					));
					
				}
				
			}else{
			
				$db->update('tb_stock',array(
					'dq_stock' => "dq_stock+{$_POST['cant'][$i]}"
				),"dc_bodega={$_POST['bodega_entrada'][$i]} AND dc_producto={$ids[$p]}");
				
				//Si es primera vez que se mueve stock para ese producto en esa bodega, entonces se crea.
				if($db->affected_rows() == 0){
					
					$db->insert('tb_stock',array(
						'dc_producto' => $ids[$p],
						'dc_bodega' => $_POST['bodega_entrada'][$i],
						'dq_stock' => $_POST['cant'][$i]
					));
					
				}
			}
		}
		
	}
}

//Si la guía de despacho no realiza traslado a otra bodega de entrada entonces se setean las bodegas en 0
if(!isset($_POST['bodega_entrada']))
	$_POST['bodega_entrada'] = array_fill(0,count($_POST['prod']),0);

$sum = 0;
foreach($_POST['serie'] as $i => $s){
	$sum += count(explode(',',$s));
	if(count(explode(',',$s)) < $_POST['cant'][$i] && !empty($s)){
		$sum++;
	}
}

//la cantidad de guías de despacho pueden ser muchas para permitir imprimiarlas sin problema a sobrepasar los límites de la hoja.
$cantidad_guias = ceil($sum/20);

$guias = array();
$guias_number = array();
$actual = $_POST['gd_number'];
for($i = 0; $i <$cantidad_guias; $i++){
	while(count($db->select('tb_guia_despacho','1',"dq_folio={$actual} AND dc_empresa={$empresa}"))){
		$actual++;
	}
	$guias[] = $db->insert('tb_guia_despacho',array(
		'dc_nota_venta' => $_POST['gd_id_nota_venta'],
		'dc_tipo_guia' => $_POST['gd_tipo_guia'],
		'dc_orden_servicio' => $_POST['gd_id_orden_servicio'],
		'dc_cliente' => $_POST['cli_id'],
		'dc_contacto' => $_POST['gd_contacto'],
		'dc_contacto_entrega' => $_POST['gd_cont_entrega'],
		'dq_guia_despacho' => doc_GetNextNumber('tb_guia_despacho','dq_guia_despacho'),
		'dq_folio' => $actual,
		'dg_guia_referencia' => $_POST['gd_guia_referencia'],
		'dg_comentario' => $_POST['gd_comentario'],
		'df_emision' => $db->sqlDate($_POST['gd_emision']),
		'dg_orden_compra' => $_POST['gd_orden_compra'],
		'dq_neto' => $_POST['cot_neto'],
		'dq_iva' => $_POST['cot_iva'],
		'dq_total' => $_POST['cot_neto']+$_POST['cot_iva'],
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario,
		'dc_factura' => isset($_POST['dc_factura_anticipada'])?$_POST['dc_factura_anticipada']:0
	));
	
	$guias_number[] = $actual;
	
	$actual++;
}

$guia_actual = 0;
$det_actual = 0;
$cantidad_despachada = 0;
$movements = array();
foreach($_POST['cant'] as $i => $v){

	$series = explode(',',$_POST['serie'][$i]);
	$_POST['precio'][$i] = str_replace(',','',$_POST['precio'][$i]);
	$_POST['costo'][$i] = str_replace(',','',$_POST['costo'][$i]);
	$nosum = false;
	
	if(count($series) == 1 && $series[0] == ''){
		$series = array();
	}
			
	$detail_data = array(
		"dg_producto" => $_POST['prod'][$i],
		"dg_descripcion" => $_POST['desc'][$i],
		"dq_precio" => $_POST['precio'][$i],
		"dq_costo" => $_POST['costo'][$i],
		"dc_bodega_salida" => $_POST['bodega_salida'][$i],
		"dc_bodega_entrada" => $_POST['bodega_entrada'][$i]
	);
	
	if(isset($ids[$_POST['prod'][$i]])){
		$movements[$_POST['prod'][$i]] = array();
		$movements[$_POST['prod'][$i]]['price'] = $dq_costo_stock[$ids[$_POST['prod'][$i]]];
		$movements[$_POST['prod'][$i]]['cant'] = $v;
		$movements[$_POST['prod'][$i]]['series'] = $_POST['serie'][$i];
		$movements[$_POST['prod'][$i]]['bodega_salida'] = $_POST['bodega_salida'][$i];
		$movements[$_POST['prod'][$i]]['bodega_entrada'] = $_POST['bodega_entrada'][$i];
	}
	
	if(isset($_POST['id_detail'])){
		$detail_data['dc_detalle_nota_venta'] = $_POST['id_detail'][$i];
		$db->update('tb_nota_venta_detalle',array(
			"dc_despachada" => "dc_despachada+{$v}"
		),"dc_nota_venta_detalle = {$_POST['id_detail'][$i]}");
		
		$cantidad_despachada += $v;
	}else if(isset($_POST['id_os_detail'])){
		$detail_data['dc_detalle_orden_servicio'] = $_POST['id_os_detail'][$i];
		$db->update('tb_orden_servicio_factura_detalle',array(
			"dq_despachado" => "dq_despachado+{$v}"
		),"dc_detalle = {$_POST['id_os_detail'][$i]}");
	}
		
	
	foreach($series as $s){
		$detail_data['dg_serie'] = $s;
		
		$detail_data['dc_guia_despacho'] = $guias[$guia_actual];
		
		if(isset($ids[$_POST['prod'][$i]]))
			$movements[$_POST['prod'][$i]]['guias'][$guias[$guia_actual]][] = $s;
		
		$db->insert('tb_guia_despacho_detalle',$detail_data);
		
		$det_actual++;
		
		if($det_actual%20 == 0){
			$guia_actual++;
		}
	}
	
	$leftovers = $v - count($series);
	if($leftovers > 0){
		$detail_data['dg_serie'] = '';
		$detail_data['dq_cantidad'] = $leftovers;
		$detail_data['dc_guia_despacho'] = $guias[$guia_actual];
		
		if(isset($ids[$_POST['prod'][$i]]))
			for($c = 0; $c < $leftovers; $c++)
			$movements[$_POST['prod'][$i]]['guias'][$guias[$guia_actual]][] = '';
		
		$db->insert('tb_guia_despacho_detalle',$detail_data);
		
		$det_actual++;
		
		if($det_actual%20 == 0){
			$guia_actual++;
		}
	}
	
	/*for($j = 0; $j<$v; $j++){
		
		if(isset($ids[$_POST['prod'][$i]]))
			$movements[$_POST['prod'][$i]]['guias'][$guias[$guia_actual]][] = isset($series[$j])?$series[$j]:'';
			
		$detail_data['dc_guia_despacho'] = $guias[$guia_actual];

		if(isset($series[$j])){
			$detail_data['dg_serie'] = $series[$j];
		}else if(!$nosum){
			$detail_data['dg_serie'] = '';
			$nosum = true;
		}
		
		$db->insert('tb_guia_despacho_detalle',$detail_data);
		
		if(!$nosum){
			$det_actual++;
			
		}
		
		if($det_actual%20 == 0){
			$guia_actual++;
			if($det_actual != 0)
				$det_actual++;
		}
	}
	
	if($nosum)
		$det_actual++;*/
	
}

if($cantidad_despachada && isset($_POST['id_detail'])){
	$db->update('tb_nota_venta',array("dc_despachada" => "dc_despachada+{$cantidad_despachada}"),"dc_nota_venta={$_POST['gd_id_nota_venta']}");
}

foreach($movements as $codigo => $v){
	
	foreach($v['guias'] as $guia => $arr){
		$series = $v['series'];
		$cantidad = count($arr);
		
		$db->insert('tb_movimiento_bodega',array(
			'dc_cliente' => $_POST['cli_id'],
			'dc_tipo_movimiento' => $_POST['gd_tipo_mov'],
			'dc_bodega_salida' => $v['bodega_salida'],
			'dq_monto' => $v['price'],
			'dc_guia_despacho' => $guia,
			'dg_series' => $series,
			'dc_nota_venta' => $_POST['gd_id_nota_venta'],
			'dc_producto' => $ids[$codigo],
			'dq_cantidad' => -1*$cantidad,
			'dc_empresa' => $empresa,
			'df_creacion' => 'NOW()',
			'dc_usuario_creacion' => $idUsuario
		));
		
		if($v['bodega_entrada'] != 0){
			$db->insert('tb_movimiento_bodega',array(
				'dc_cliente' => $_POST['cli_id'],
				'dc_tipo_movimiento' => $_POST['gd_tipo_mov'],
				'dc_bodega_entrada' => $v['bodega_entrada'],
				'dq_monto' => $v['price'],
				'dc_guia_despacho' => $guia,
				'dg_series' => $series,
				'dc_nota_venta' => $_POST['gd_id_nota_venta'],
				'dc_producto' => $ids[$codigo],
				'dq_cantidad' => $cantidad,
				'dc_empresa' => $empresa,
				'df_creacion' => 'NOW()',
				'dc_usuario_creacion' => $idUsuario
			));
		}
	}
}

/**
*	Asignar a Facturación anticipada
**/

if(isset($_POST['dc_factura_anticipada'])){
	if(!isset($_POST['to_complete'])){
	//En caso de que la factura anticipada que se haya seleccionada no esté completa aún
	//se comprueban las cantidades de los detalles y se comprueba si la factura se encuentra completa
		foreach($_POST['prod'] as $i => $p){
			if(isset($_POST['ant_code'][$p])){
				$_POST['ant_code'][$p] -= $_POST['cant'][$i];
				if($_POST['ant_code'][$p] < 0){
					$error_man->showWarning("Ha ocurrido un error al intentar asignar la cantidad despachada a la factura anticipada<br />
					Este error se gatillo debido a que la cantidad del producto <b>{$p}</b> es mayor al anticipado.");
					$db->rollback();
					exit;
				}
			}
		}
		
		if(array_sum($_POST['ant_code']) == 0){
			$db->update('tb_factura_venta',array(
				'dm_anticipada' => 'E'
			),"dc_factura = {$_POST['dc_factura_anticipada']}");
		}
	}else{
	//en caso en que la factura anticipada esté completa los detalles de la guía de despacho pasan a ser parte de la factura
	//Esto solo en caso de que el tipo de guía no permita facturación directa
		foreach($_POST['prod'] as $i => $p){
			
			$db->insert('tb_factura_venta_detalle',array(
				"dc_factura" => $_POST['dc_factura_anticipada'],
				"dg_producto" => $p,
				"dc_detalle_nota_venta" => $_POST['id_detail'][$i],
				"dg_descripcion" => $_POST['desc'][$i],
				"dm_tipo" => 0,
				"dc_cantidad" => $_POST['cant'][$i],
				"dq_precio" => 0,
				"dq_descuento" => $_POST['descuento'][$i],
				"dq_total" => 0
			));
			
		}
	}
	
	if(isset($_POST['bodega_entrada'])){
	//En caso en que la guía de despacho sea por traslado los productos trasladados son despachados automaticamente
	//por lo uq ese debe hacer movimiento de salida para cada uno de estos
	
		$tipo_movimiento = $db->select('tb_configuracion_logistica','dc_tipo_movimiento_salida_anticipada dc_tipo',"dc_empresa = {$empresa}");
		$tipo_movimiento = $tipo_movimiento[0]['dc_tipo'];
		
		if($tipo_movimiento == 0){
			$error_man->showWarning("No es posible continuar porque ocurrió un error al intentar generar un registro de salida por la facturación anticipada con traslados<br />"
			."Compruebe que esté configurado el tipo de movimiento de salida por facturación anticipada.");
			$db->rollback();
			exit;
		}
			
		foreach($movements as $codigo => $v){
		
			foreach($v['guias'] as $guia => $arr){
				$series = $v['series'];
				$cantidad = count($arr);
				
				$db->insert('tb_movimiento_bodega',array(
					'dc_cliente' => $_POST['cli_id'],
					'dc_tipo_movimiento' => $tipo_movimiento,
					'dc_bodega_salida' => $v['bodega_entrada'],
					'dq_monto' => $v['price'],
					'dc_guia_despacho' => $guia,
					'dg_series' => $series,
					'dc_nota_venta' => $_POST['gd_id_nota_venta'],
					'dc_producto' => $ids[$codigo],
					'dq_cantidad' => -1*$cantidad,
					'dc_empresa' => $empresa,
					'df_creacion' => 'NOW()',
					'dc_usuario_creacion' => $idUsuario,
					'dc_factura' => $_POST['dc_factura_anticipada']
				));
				
			}
		}
		
	}
}

$db->rollback();
//$db->query("UNLOCK TABLES");

if(count($guias) == 1){
	$lang = array('ha','guía','El número','es');
}else{
	$lang = array('han','guías','Los número','son');
}

$error_man->showConfirm("Se {$lang[0]} generado ".count($guias)." {$lang[1]} de despacho");
echo("<div class='title'>{$lang[2]} de guía {$lang[3]} <h1 style='margin:0;color:#000;'>".implode('<br />',$guias_number)."</h1></div>");
?>