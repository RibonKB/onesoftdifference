<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select('tb_orden_servicio','dc_orden_servicio',
"dc_empresa = {$empresa} AND dq_orden_servicio={$_GET['number']}");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}
$id = $id[0];

$detalle = $db->select("(SELECT * FROM tb_orden_servicio_factura_detalle WHERE dc_orden_servicio = {$id['dc_orden_servicio']} AND dq_cantidad > dq_despachado) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto",
'd.dc_detalle,p.dg_codigo,p.dg_producto AS dg_descripcion,(d.dq_cantidad-d.dq_despachado) dq_cantidad,
d.dq_precio_venta,d.dq_precio_compra,p.dc_producto,d.dg_serie,p.dm_requiere_serie,d.dc_recepcionada,d.dq_despachado');

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>