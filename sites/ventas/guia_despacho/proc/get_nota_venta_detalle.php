<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select('tb_nota_venta nv
JOIN tb_tipo_nota_venta tnv ON tnv.dc_tipo = nv.dc_tipo_nota_venta',
'nv.dc_nota_venta,nv.dq_cambio,nv.dc_tipo_cambio,nv.dm_al_dia,tnv.dc_tipo_guia_default',
"nv.dc_empresa = {$empresa} AND nv.dq_nota_venta={$_GET['number']} AND nv.dm_validada = '1' AND nv.dm_confirmada = '1' AND dm_nula='0'");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

$detalle = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$id['dc_nota_venta']} AND dm_tipo=1) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto",
"d.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,p.dc_producto,d.dc_despachada,d.dc_comprada,d.dc_recepcionada,p.dm_requiere_serie");

if(!count($detalle)){
	$detalle = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$id['dc_nota_venta']} AND dm_tipo=0) d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto",
	"d.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,
	p.dc_producto,d.dc_despachada,d.dc_comprada,d.dc_recepcionada,p.dm_requiere_serie");
	
	if(!count($detalle)){
		echo json_encode(array('<empty>',$id));
		exit;
	}
}


echo json_encode(array($detalle,$id));
?>