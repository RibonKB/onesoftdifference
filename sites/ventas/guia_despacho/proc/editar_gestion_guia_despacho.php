<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$st_edit = $db->prepare($db->update('tb_guia_despacho',array(
	'dc_responsable' => '?',
	'dg_receptor' => '?',
	'df_fecha_recepcion' => $db->sqlDate($_POST['df_fecha_recepcion'])
),"dc_guia_despacho = {$_POST['dc_guia_despacho']}"));

$st_edit->bindValue(1,$_POST['dc_responsable'],PDO::PARAM_INT);
$st_edit->bindValue(2,$_POST['dg_receptor'],PDO::PARAM_STR);

$db->stExec($st_edit);

if($st_edit->rowCount() != 1){
	$error_man->showAviso("No se modificó ninguna guía de despacho, compruebe que esta existe");
	exit;
}

$error_man->showConfirm("Los datos de gestión sobre la guía de despacho han sido actualizados correctamente.");
?>