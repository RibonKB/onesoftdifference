<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select('tb_nota_venta','dc_nota_venta,dq_cambio,dc_tipo_cambio,dm_al_dia',
"dc_empresa = {$empresa} AND dq_nota_venta={$_GET['number']} AND dm_validada = '1' AND dm_confirmada = '1'");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}
$id = $id[0];

$detalle = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$id['dc_nota_venta']}) d
LEFT JOIN tb_producto p ON p.dc_producto = d.dc_producto",
"d.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,p.dc_producto,d.dc_despachada,d.dc_comprada,d.dc_recepcionada,p.dm_requiere_serie");

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>