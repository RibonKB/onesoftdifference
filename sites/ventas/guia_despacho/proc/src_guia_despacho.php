<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['gd_emision_desde'] = $db->sqlDate($_POST['gd_emision_desde']);
$_POST['gd_emision_hasta'] = $db->sqlDate($_POST['gd_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['gd_emision_desde']} AND {$_POST['gd_emision_hasta']})";

if($_POST['gd_numero_desde']){
	if($_POST['gd_numero_hasta']){
		$conditions .= " AND
							((dq_guia_despacho BETWEEN {$_POST['gd_numero_desde']} AND {$_POST['gd_numero_hasta']})
							OR
							(dq_folio BETWEEN {$_POST['gd_numero_desde']} AND {$_POST['gd_numero_hasta']}))";
	}else{
		$conditions = "(dq_guia_despacho = {$_POST['gd_numero_desde']} OR dq_folio = {$_POST['gd_numero_desde']}) AND dc_empresa = {$empresa}";
	}
}

if(isset($_POST['gd_cliente'])){
	$_POST['gd_cliente'] = implode(',',$_POST['gd_cliente']);
	$conditions .= " AND dc_cliente IN ({$_POST['gd_cliente']})";
}

$data = $db->select('tb_guia_despacho','dc_guia_despacho,dq_guia_despacho,dq_folio',$conditions,array('order_by' => 'df_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron guías de despacho de venta con los criterios especificados");
	exit();
}

echo("<div id='show_guia_despacho'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Guías de Despacho<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Guía</th>
	</tr>
</thead>
<tbody>
");


foreach($data as $c){
	if($c['dq_folio'] == 0){
		$c['dq_folio'] = '[V]';
	}
	
	echo("<tr>
		<td align='left'>
			<a href='sites/ventas/guia_despacho/proc/show_guia_despacho.php?id={$c['dc_guia_despacho']}' class='gd_load'>
			<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_guia_despacho']} - <b>{$c['dq_folio']}</b></a>
		</td>
	</tr>");
}

echo("
</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='imgbtn' id='gestion_guia' style='background-image:url(images/management.png);'>Gestión</button>
<button type='button' class='editbtn' id='edit_folio'>Editar Folio</button>
<button type='button' class='imgbtn' id='guia_electronica' style='background-image:url(images/archive.png);'>Guía de despacho electrónica</button>
<button type='button' class='delbtn' id='null_guia'>Anular</button>
</div>
");

?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".gd_load").click(function(e){
		e.preventDefault();
		$('#show_factura').html("<img src='images/ajax-loader.gif' alt='' /> cargando guía de despacho ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_guia_despacho');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>