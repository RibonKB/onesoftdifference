<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(isset($_POST['cli_rut'])){
	//Se escapan los caracteres por seguridad
	$db->escape($_POST['cli_rut']);
	$datos = $db->select("tb_cliente","dc_cliente,dg_razon,dc_contacto_default",
	"dg_rut = '{$_POST['cli_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");
	
	if(!count($datos)){
		$error_man->showErrorRedirect("No se encontró el cliente especificado, intentelo nuevamente.","sites/ventas/guia_despacho/cr_guia_despacho.php");
	}
	
	$poblacion['nota_venta'] = '';
	$poblacion['orden_servicio'] = '';
}else if(isset($_POST['cli_id'])){
	$datos = $db->select("tb_cliente",'dc_cliente,dg_razon,dc_contacto_default',
	"dc_cliente={$_POST['cli_id']} AND dc_empresa={$empresa} AND dm_activo='1'");
	
	if(!count($datos)){
		$error_man->showErrorRedirect("Error: Cliente inválido, intentelo nuevamente.","sites/ventas/guia_despacho/cr_guia_despacho.php");
	}
	
	$poblacion['nota_venta'] = isset($_POST['nv_numero'])?$_POST['nv_numero']:'';
	$poblacion['orden_servicio'] = isset($_POST['os_numero'])?$_POST['os_numero']:'';
}
$datos = $datos[0];


echo("<div id='secc_bar'>Creación de factura de venta</div>
<div id='main_cont'>
<div class='panes' style='width:1140px;'>
<div class='title center'>
Emitiendo Guía de despacho para <strong id='cli_razon' style='color:#000;'>{$datos['dg_razon']}</strong>
</div>");

	include_once("../../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dq_cambio",
	"dc_empresa={$empresa} AND dm_activo='1'");
	$cambio_list = array();
	foreach($list_seg as $c){
		$form->Hidden("cambio{$c['dc_tipo_cambio']}",$c['dq_cambio']);
	}
	
	$num_guia = $db->select('tb_guia_despacho','MAX(dq_guia_despacho)+1 AS number',"dc_empresa={$empresa}");
	$num_guia = $num_guia[0]['number'];
	
	$form->Start("sites/ventas/guia_despacho/proc/crear_guia_despacho.php","cr_guia_despacho",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la guía de despacho</strong><br />Los datos marcados con [*] son obligatorios");
	
	echo('<div style="width:580px;float:left;">');
	$form->Section();
	
	$form->Text("Número de guia","gd_number",1,20,$num_guia);
	
	if(isset($_POST['os_numero']))
		$form->Listado('Tipo de guía de despacho','gd_tipo_guia','tb_tipo_guia_despacho',array('dc_tipo','dg_tipo'),1,0,"dc_empresa={$empresa} AND dm_activo='1' AND dc_bodega_entrada > 0");
	else
		$form->Listado('Tipo de guía de despacho','gd_tipo_guia','tb_tipo_guia_despacho',array('dc_tipo','dg_tipo'),1);
	
	//Contacto principal de la factura, aparecerá en la cabecera como principal contacto del cliente
	$form->Listado('Contacto','gd_contacto',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');
	
	//Dirección a la que se despachará el pedido
	$form->Listado('Contacto de entrega','gd_cont_entrega',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');
	
	
	$form->EndSection();
	$form->Section();
	
	$form->Text('Guía referencia','gd_guia_referencia');
	$form->Text('Orden de compra cliente','gd_orden_compra');
	$form->Text('Nota de venta','gd_nota_venta',0,255,$poblacion['nota_venta']);
	$form->Hidden('gd_id_nota_venta',0);
	$form->Text('Orden de servicio','gd_orden_servicio',0,255,$poblacion['orden_servicio']);
	$form->Hidden('gd_id_orden_servicio',0);
	
	$form->EndSection();
	
	echo('<br class="clear" id="after_header">');
	echo('</div>');
	
	
	$form->Section();
	$form->Date('Fecha Emisión','gd_emision',1,0);
	$form->Textarea('Comentario','gd_comentario');
	echo('<div id="factura_anticipada_container"></div>');
	$form->EndSection();

	echo("<hr class='clear' />");
	
	echo("<div id='prods'>
	<div class='info'>Detalle (Valores en <b>{$empresa_conf['dg_moneda_local']}</b>)</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='80'>Opciones</th>
		<th width='95'>Código</th>
		<th>Descripción</th>
		<th>Bodega Salida</th>
		<th>Bodega entrada</th>
		<th width='63'>Cantidad</th>
		<th width='82'>Precio</th>
		<th width='82'>Total</th>
		<th width='82'>Costo</th>
		<th width='82'>Costo total</th>
		<th width='82'>Descuento</th>
		<th width='82'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='6' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th>&nbsp;</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center' id='total_margen_p'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='6'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th colspan='5'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='6'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='5'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='6'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='5'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div></div>");
	
	$form->Hidden('cli_id',$datos['dc_cliente']);
	$form->Hidden('gd_tipo_mov',0);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('facturable',0);
	$form->End('Emitir','addbtn');
	
	$bodegas = "<option value='0'></option>";
	foreach($db->select('tb_bodega','dc_bodega,dg_bodega',"dc_empresa={$empresa} AND dm_activo='1'") as $b){
		$bodegas .= "<option value='{$b['dc_bodega']}'>{$b['dg_bodega']}</option>";
	}
	$bodegas .= '</select>';
	
	echo("<table id='prods_form' style='display:none;'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/doc.png' alt='' title='Asignar Series' class='set_series' />
			<input type='hidden' class='prod_series' name='serie[]' value=''>
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='9' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' /></td>
		<td>
			<select name='bodega_salida[]' style='width:110px;' class='prod_bodega_salida inputtext' required='required'>
			{$bodegas}
		</td>
		<td>
			<select name='bodega_entrada[]' style='width:110px;' class='prod_bodega_entrada inputtext'>
			{$bodegas}
		</td>
		<td>
			<input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' />
			<input type='hidden' name='despachada[]' value='0' class='despach_cant' />
			<input type='hidden' name='recepcionada[]' value='0' class='recep_cant' />
		</td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td><input type='text' name='descuento[]' class='prod_descuento inputtext' size='7' style='text-align:right;' value='0' required='required' /></td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
	</tr>
	</table>");
	
	$bodega_default = array();
	$tipo_doc_mov = array();
	foreach($db->select('tb_tipo_guia_despacho','dc_tipo,dc_bodega,dc_bodega_entrada,dc_tipo_movimiento,dm_facturable',"dc_empresa={$empresa} AND dm_activo='1'") as $t){
		$bodega_salida[$t['dc_tipo']] = $t['dc_bodega'];
		$bodega_entrada[$t['dc_tipo']] = $t['dc_bodega_entrada'];
		$tipo_doc_mov[$t['dc_tipo']] = $t['dc_tipo_movimiento'];
		$tipo_doc_fac[$t['dc_tipo']] = $t['dm_facturable'];
	}
	
?>
</div></div>
<script type="text/javascript">
	var empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	var empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;
	
	var out_bod = <?=json_encode($bodega_salida) ?>;
	var in_bod = <?=json_encode($bodega_entrada) ?>;
	var tipo_mov = <?=json_encode($tipo_doc_mov) ?>;
	var facturable = <?=json_encode($tipo_doc_fac) ?>;
	
	var anticipa = <?=check_permiso(28)?1:0 ?>;

</script>
<script type="text/javascript" src="jscripts/product_manager/guia_despacho.js?v2_11b"></script>
<script type="text/javascript">
<?php
	if(isset($detalles_base))
		echo("var detalles_base = ".json_encode($detalles_base).";
		$('#prod_tipo_cambio').change();
		set_detail(detalles_base);");
?>
if($('#gd_nota_venta').val() != '')
	$('#gd_nota_venta').change();
if($('#gd_orden_servicio').val() != '')
	$('#gd_orden_servicio').change();
</script>