<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_guia_despacho WHERE dc_guia_despacho = {$_POST['id']} AND dc_empresa={$empresa}) gd
LEFT JOIN tb_tipo_guia_despacho t ON t.dc_tipo = gd.dc_tipo_guia
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = gd.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = gd.dc_orden_servicio
JOIN tb_contacto_cliente c ON c.dc_contacto = gd.dc_contacto
	JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
	LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
	LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
JOIN tb_contacto_cliente ce ON ce.dc_contacto = gd.dc_contacto_entrega
	JOIN tb_cliente_sucursal s2 ON s2.dc_sucursal = c.dc_sucursal
	LEFT JOIN tb_comuna com2 ON com2.dc_comuna = s2.dc_comuna
	LEFT JOIN tb_region reg2 ON reg2.dc_region = com2.dc_region
JOIN tb_cliente cl ON cl.dc_cliente = gd.dc_cliente",
'gd.dq_guia_despacho,gd.dg_guia_referencia,gd.dg_comentario,DATE_FORMAT(gd.df_emision,"%d/%m/%Y") AS df_emision,gd.dg_orden_compra,gd.dq_neto,gd.dq_iva,gd.dq_total,
nv.dq_nota_venta,os.dq_orden_servicio,c.dg_contacto,s.dg_sucursal,s.dg_direccion,com.dg_comuna,reg.dg_region,ce.dg_contacto AS dg_contacto_entrega,gd.dm_nula,
s2.dg_sucursal AS dg_sucursal_entrega,s2.dg_direccion AS dg_direccion_entrega,com2.dg_comuna AS dg_comuna_entrega,reg2.dg_region AS dg_region_entrega,
cl.dg_razon,cl.dg_rut, t.dg_tipo');

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la guia de despacho especificada");
	exit();
}
$data = $data[0];

echo("<div class='title center'>Guía de Despacho Nº {$data['dq_guia_despacho']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong>({$data['dg_rut']}) {$data['dg_razon']}</strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td>{$data['df_emision']}</td>
</tr><tr>
	<td>Tipo de Guía</td>
	<td>{$data['dg_tipo']}</td>
</tr><tr>
	<td>Domicilio cliente</td>
	<td><b>{$data['dg_direccion']}</b> {$data['dg_comuna']} <label>{$data['dg_region']}</label></td>
</tr><tr>
	<td>Domicilio entrega</td>
	<td><b>{$data['dg_direccion_entrega']}</b> {$data['dg_comuna_entrega']} <label>{$data['dg_region_entrega']}</label></td>
</tr><tr>
	<td>Guía de Referencia</td>
	<td>{$data['dg_guia_referencia']}</td>
</tr><tr>
	<td>Orden de compra</td>
	<td>{$data['dg_orden_compra']}</td>
</tr><tr>
	<td>Nota de venta</td>
	<td>{$data['dq_nota_venta']}</td>
</tr><tr>
	<td>Orden de servicio</td>
	<td>{$data['dq_orden_servicio']}</td>
</tr><tr>
	<td>Comentario</td>
	<td>{$data['dg_comentario']}</td>
</tr></table>");

if($data['dm_nula'] == 1):
$error_man->showAviso('GUÍA DE DESPACHO NULA');
endif;

$detalle = $db->select("tb_guia_despacho_detalle",
'dg_producto,dg_descripcion,dg_serie,dq_precio,dq_costo,dq_cantidad',
"dc_guia_despacho={$_POST['id']}");

/*$detalle = array();
foreach($detalle_aux as $d){
	$detalle[$d['dg_producto']]['series'][] = $d['dg_serie'];
	$detalle[$d['dg_producto']]['descripcion'] = $d['dg_descripcion'];
	$detalle[$d['dg_producto']]['precio'] = $d['dq_precio'];
	$detalle[$d['dg_producto']]['costo'] = $d['dq_costo'];
}
unset($detalle_aux);*/

echo("<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
<tr>
	<th width='60'>Código</th>
	<th width='60'>Cantidad</th>
	<th>Descripción</th>
	<th width='60'>series</th>
	<th width='100'>Precio</th>
	<th width='100'>Costo</th>
	<th width='100'>Total</th>
</tr>
</thead>
<tbody>");

foreach($detalle as $i => $d){
	$d['dq_total'] = moneda_local($d['dq_precio']*$d['dq_cantidad']);
	$d['dq_precio'] = moneda_local($d['dq_precio']);
	$d['dq_costo'] = moneda_local($d['dq_costo']);
	$series = '';
	/*foreach($d['series'] as $s){
		if($s){
			$series .= $s.'<br />';
		}
	}*/
	echo("
	<tr>
		<td>{$d['dg_producto']}</td>
		<td>{$d['dq_cantidad']}</td>
		<td align='left'>{$d['dg_descripcion']}</td>
		<td>{$series}</td>
		<td align='right'>{$d['dq_precio']}</td>
		<td align='right'>{$d['dq_costo']}</td>
		<td align='right'>{$d['dq_total']}</td>
	</tr>");
}

$data['dq_neto'] = moneda_local($data['dq_neto']);
$data['dq_iva'] = moneda_local($data['dq_iva']);
$data['dq_total'] = moneda_local($data['dq_total']);

echo("</tbody>
<tfoot>
<tr>
	<th colspan='6' align='right'>Total Neto</th>
	<th align='right'>{$data['dq_neto']}</th>
</tr>
<tr>
	<th colspan='6' align='right'>IVA</th>
	<th align='right'>{$data['dq_iva']}</th>
</tr>
<tr>
	<th colspan='6' align='right'>Total a Pagar</th>
	<th align='right'>{$data['dq_total']}</th>
</tr>
</tfoot></table>");

?>
<script type="text/javascript">
$('#print_version').unbind('click').click(function(){
	window.open("sites/ventas/guia_despacho/ver_guia_despacho.php?id=<?=$_POST['id'] ?>&v=0",'print_guia_despacho','width=800;height=600');
});
$('#gestion_guia').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/guia_despacho/ed_gestion_guia_despacho.php?id=<?=$_POST['id'] ?>');
});
$('#null_guia').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/guia_despacho/null_guia_despacho.php?id=<?=$_POST['id'] ?>');
});
$('#edit_folio').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/guia_despacho/ch_folio.php?id=<?=$_POST['id'] ?>');
});
$('#guia_electronica').unbind('click').click(function(){
	pymerp.loadOverlay('god.php?modulo=ventas&submodulo=guia_despacho&factory=GuiaElectronica&dc_guia_despacho=<?php echo $_POST['id'] ?>');
});
</script>