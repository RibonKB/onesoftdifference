<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$_POST['df_emision_desde'] = $db->sqlDate($_POST['df_emision_desde']);
$_POST['df_emision_hasta'] = $db->sqlDate($_POST['df_emision_hasta']." 23:59");

$conditions = "dc_empresa={$empresa} AND dq_folio > 0 AND (df_emision BETWEEN {$_POST['df_emision_desde']} AND {$_POST['df_emision_hasta']})";

if(isset($_POST['dc_cliente'])){
	$dc_cliente = implode(',',$_POST['dc_cliente']);
	$conditions .= " AND dc_cliente IN ({$dc_cliente})";
}

if($_POST['dm_tipo'] == 0){
	$conditions .= " AND dc_factura = 0";
	$fv_condition = '';
}else if($_POST['dm_tipo'] == 1){
	$fv_condition = "fv.dm_anticipada IN ('A','E')";
}else{
    $fv_condition = '';
}

$data = $db->doQuery($db->select("(SELECT * FROM tb_guia_despacho WHERE {$conditions}) gd
    LEFT JOIN tb_tipo_guia_despacho tgd ON tgd.dc_tipo = gd.dc_tipo_guia
    LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = gd.dc_nota_venta
    LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = gd.dc_orden_servicio
    LEFT JOIN tb_factura_venta fv ON fv.dc_factura = gd.dc_factura
    LEFT JOIN tb_cliente cl ON cl.dc_cliente = gd.dc_cliente
    JOIN tb_guia_despacho_detalle gdd ON gdd.dc_guia_despacho = gd.dc_guia_despacho",
'gd.dq_guia_despacho, gd.dq_folio dq_folio_guia, gd.df_emision, gd.dm_nula,
tgd.dg_tipo, nv.dq_nota_venta, os.dq_orden_servicio, fv.dq_factura, fv.dq_folio dq_folio_factura, fv.dm_anticipada,
cl.dg_razon, cl.dg_rut, SUM(gdd.dq_cantidad*gdd.dq_precio) dq_precio_neto, SUM(gdd.dq_cantidad*gdd.dq_costo) dq_costo_neto',
$fv_condition,array('group_by' => 'gd.dq_guia_despacho')));

$gd = $data->fetch(PDO::FETCH_OBJ);
if($gd === false || $gd->dq_guia_despacho == NULL){
	$error_man->showAviso("No se han encontrado guias de despacho que cumplan con las condiciones puestas en los filtros");
	exit;
}

$precio_total = 0;
$costo_total = 0;

?>
<table class="tab" width="100%" id="gd_table_data">
<thead>
	<tr>
		<th>Guía de despacho</th>
        <th>Folio</th>
		<th>Emisión</th>
		<th>Cliente</th>
		<th>Tipo de guía</th>
		<th>Nota Venta</th>
		<th>Orden Servicio</th>
		<th>Factura</th>
        <th title="Estado Anticipo">EA</th>
        <th>Folio Factura</th>
		<th>Precio Neto</th>
		<th>Costo Neto</th>
	</tr>
</thead>
<tbody>
<?php do{
	$precio_total += $gd->dq_precio_neto;
	$costo_total += $gd->dq_costo_neto;
	?>
	<tr <?php if($gd->dm_nula == 1): ?>style="color:#F00;"<?php endif; ?>>
		<td><?php echo $gd->dq_guia_despacho ?></td>
        <td><b><?php echo $gd->dq_folio_guia ?></b></td>
		<td><?php echo $db->dateLocalFormat($gd->df_emision) ?></td>
		<td><?php echo $gd->dg_razon ?></td>
		<td><?php echo $gd->dg_tipo ?></td>
		<td><?php echo $gd->dq_nota_venta ?></td>
		<td><?php echo $gd->dq_orden_servicio ?></td>
		<?php if($gd->dq_factura != NULL): ?>
			<td><?php echo $gd->dq_factura ?></td>
            <td><?php echo $gd->dm_anticipada ?></td>
            <td><b><?php echo $gd->dq_folio_factura ?></b></td>
		<?php else: ?>
			<td>-</td>
            <td>-</td>
            <td>-</td>
		<?php endif ?>
		<td align="right"><?php echo moneda_local($gd->dq_precio_neto) ?></td>
		<td align="right"><?php echo moneda_local($gd->dq_costo_neto) ?></td>
	</tr>
<?php }while($gd = $data->fetch(PDO::FETCH_OBJ)); ?>
</tbody>
<tfoot>
	<tr>
		<th colspan="10" align="right">Totales</th>
		<th align="right"><?php echo moneda_local($precio_total) ?></th>
		<th align="right"><?php echo moneda_local($costo_total) ?></th>
	</tr>
</tfoot>
</table>
<script type="text/javascript">
    $('#gd_table_data').tableExport().tableAdjust(15);
</script>