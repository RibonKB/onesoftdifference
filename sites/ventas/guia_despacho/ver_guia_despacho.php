<?php
	define("MAIN",1);
	include_once("../../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	include("template_guia_despacho/modo{$empresa}.php");
	
	$datosGuia = $db->select("(SELECT * FROM tb_guia_despacho WHERE dc_guia_despacho = {$_GET['id']} AND dc_empresa={$empresa}) gd
	LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = gd.dc_nota_venta
	LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = gd.dc_orden_servicio
	JOIN tb_contacto_cliente c ON c.dc_contacto = gd.dc_contacto
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
		LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
		LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
	JOIN tb_contacto_cliente ce ON ce.dc_contacto = gd.dc_contacto_entrega
		JOIN tb_cliente_sucursal s2 ON s2.dc_sucursal = c.dc_sucursal
		LEFT JOIN tb_comuna com2 ON com2.dc_comuna = s2.dc_comuna
		LEFT JOIN tb_region reg2 ON reg2.dc_region = com2.dc_region
	JOIN tb_cliente cl ON cl.dc_cliente = gd.dc_cliente",
	'gd.dq_folio dq_guia_despacho,gd.dg_guia_referencia,gd.dg_comentario,UNIX_TIMESTAMP(gd.df_emision) AS df_emision,gd.dg_orden_compra,gd.dq_neto,gd.dq_iva,gd.dq_total,
	nv.dq_nota_venta,os.dq_orden_servicio,c.dg_contacto,s.dg_sucursal,s.dg_direccion,com.dg_comuna,reg.dg_region,ce.dg_contacto AS dg_contacto_entrega,
	s2.dg_sucursal AS dg_sucursal_entrega,s2.dg_direccion AS dg_direccion_entrega,com2.dg_comuna AS dg_comuna_entrega,reg2.dg_region AS dg_region_entrega,
	cl.dg_razon,cl.dg_rut,s.dg_codigo_postal,ce.dg_fono');
	
	if(!count($datosGuia)){
		$error_man->showWarning("La guia de despacho especificada no existe");
		exit();
	}
	$datosGuia = $datosGuia[0];
	
	setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
	$datosGuia['df_emision'] = strftime("%d de %B de %Y",$datosGuia['df_emision']);
	
	$db->escape($_GET['v']);
	
	$detalle = $db->select("tb_guia_despacho_detalle",
	'dq_cantidad,dg_producto,dg_descripcion,dg_serie,dq_precio',
	"dc_guia_despacho={$_GET['id']}");
	
	/*$detalle = array();
	foreach($detalle_aux as $d){
		$detalle[$d['dg_producto']]['series'][] = $d['dg_serie'];
		$detalle[$d['dg_producto']]['descripcion'] = $d['dg_descripcion'];
		$detalle[$d['dg_producto']]['precio'] = $d['dq_precio'];
	}*/
	$datosGuia['detalle'] = $detalle;
	unset($detalle);

	/*$datosGuia['detalle'] = $db->select("(SELECT * FROM tb_factura_venta_detalle WHERE dc_factura={$datosGuia['dc_factura']} AND dm_tipo='{$_GET['v']}') d
	LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto
	LEFT JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto",
	'd.dg_producto,d.dc_cantidad,d.dg_descripcion,d.dg_serie,d.dq_precio,d.dq_descuento,d.dq_total,t.dm_controla_inventario');*/
	
	$pdf = new PDF();
	$pdf->AddDetalle($datosGuia['detalle']);
	$pdf->Output();
	
	
?>