<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_guia_despacho','dc_responsable,dg_receptor,df_fecha_recepcion',"dc_guia_despacho = {$_POST['id']} AND dc_empresa = {$empresa}");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la guía de despacho especificada, compruebe que accedió desde el lugar indicado.");
	exit;
}
$data = $data[0];

if($data['df_fecha_recepcion'] == '0000-00-00 00:00:00'){
	$data['df_fecha_recepcion'] = '';
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/guia_despacho/proc/editar_gestion_guia_despacho.php','gestion_guia_despacho');
$form->Header("Indique los datos de gestión sobre la guía de despacho");

$form->Section();
	$form->Listado('Responsable','dc_responsable','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'),0,$data['dc_responsable']);
	$form->Text('Recepcionada por','dg_receptor',0,255,$data['dg_receptor']);
$form->EndSection();

$form->Section();
	$form->Date('Fecha de recepcion','df_fecha_recepcion',0,$data['df_fecha_recepcion']);
$form->EndSection();

$form->Hidden('dc_guia_despacho',$_POST['id']);
$form->End('Editar','editbtn');
?>
<script type="text/javascript">
$("#df_fecha_recepcion").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});
</script>