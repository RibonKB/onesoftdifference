<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Guía Despacho</div>
<div id="main_cont" class="center"><br /><br />
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/guia_despacho/proc/src_guia_despacho.php","src_guia_despacho");
	$form->Header("<strong>Indicar los parámetros de búsqueda de la guía de despacho</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50">Número de guía de despacho</td><td width="280">');
	$form->Text("Desde","gd_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','gd_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','gd_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','gd_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','gd_cliente','tb_cliente',array('dc_cliente','dg_razon'));
	echo('</td><td>&nbsp;</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#gd_cliente').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>