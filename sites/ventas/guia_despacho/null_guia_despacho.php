<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_guia_despacho','dq_guia_despacho,dc_factura,dm_nula',"dc_guia_despacho = {$_POST['id']}");

if(!count($data)){
	$error_man->showWarning('El número de guía de despacho especificado es inválido');
	exit;
}

$dc_factura = $data[0]['dc_factura'];

if($data[0]['dc_factura'] != 0){
	$factura_data = $db->select('tb_factura_venta','1',"dc_factura = {$dc_factura} AND dm_nula = 0");
	if(count($factura_data)){
		$error_man->showWarning("No puede anular la guía de despacho porque tiene una factura vigente asignada, anule primero la factura.");
		exit;
	}
}

if($data[0]['dm_nula'] == 1){
	$error_man->showAviso("La guía de despacho seleccionada ya está anulada");
	exit;
}

$dq_guia_despacho = $data[0]['dq_guia_despacho'];
unset($data);

echo('<div class="secc_bar">Anular guía de despacho</div><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/guia_despacho/proc/null_guia_despacho.php','null_guia_form');
$form->Header('Indique las razones por las que se anulará la guia de despacho');
$error_man->showAviso("Atención, está a punto de anular la guía de despacho N° <b>{$dq_guia_despacho}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
//$form->Radiobox('Facturada fisicamente','null_fisica',array('NO','SI'),0);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('dc_guia_despacho',$_POST['id']);
$form->End('Anular','delbtn');

?>