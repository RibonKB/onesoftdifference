<?php
define("MAIN",1);
require_once("../../../inc/global.php");
?>
<div id="secc_bar">
	Informe de Guías de Despacho
</div>
<div id="main_cont"><div class="panes">
	<?php
		require_once('../../../inc/form-class.php');
		$form = new Form($empresa);
		$form->Start('sites/ventas/guia_despacho/proc/src_informe_guia_despacho.php','informe_guias');
		$form->Header("Indique los filtros que se aplicarán al informe de guías de despacho.<br />
		Los campos marcados con [*] son obligatorios.");
	?>
	<table class="tab" style="text-align:left;" id="form_container" width="100%">
		<tr>
			<td width="130">Fecha de emisión</td>
			<td>
				<?php $form->Date('Desde','df_emision_desde',1,"01/".date("m/Y")); ?>
			</td>
			<td>
				<?php $form->Date('Hasta','df_emision_hasta',1,0); ?>
			</td>
		</tr>
		<tr>
			<td>Cliente</td>
			<td>
				<?php $form->ListadoMultiple('','dc_cliente','tb_cliente',array('dc_cliente','dg_razon')); ?>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Opciones</td>
			<td>
				<?php $form->Radiobox('Mostrar solo','dm_tipo',array('Pendiente de Facturación','Por facturación anticipada','Todas')) ?>
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<?php $form->End('Ejecutar consulta','searchbtn'); ?>
</div></div>
<script type="text/javascript" src="jscripts/sites/ventas/guia_despacho/src_informe_guia_despacho.js?v=0_1b"></script>