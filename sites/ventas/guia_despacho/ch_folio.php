<?php
define("MAIN",1);
require_once("../../../inc/global.php");

$db->escape($_POST['id']);
$guia_despacho = $db->select('tb_guia_despacho','dq_guia_despacho',"dc_guia_despacho = {$_POST['id']}");

if(!count($guia_despacho)){
	$error_man->showWarning('No se ha encontrado la guía de despacho especificada, intentelo denuevo.');
	exit;
}
$guia_despacho = $guia_despacho[0]['dq_guia_despacho'];

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

echo('<div class="secc_bar">Cambiar folio de Guías de Despacho</div><div class="panes center">');
$form->Start('sites/ventas/guia_despacho/proc/change_folio_guia_despacho.php','ch_folio_guia');
$form->Header("Indique el nuevo folio para la guía de despacho <b>{$guia_despacho}</b>");
$form->Text('Nuevo Folio','dq_folio',1);
$form->Hidden('dc_guia_despacho',$_POST['id']);
$form->End('Editar','editbtn');
echo('</div></div>');
?>