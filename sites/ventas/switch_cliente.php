<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['sw'])){
echo("<div class='panes center'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/switch_cliente.php?sw=1','switch_cliente');
$form->Header("Indique el nuevo cliente que utilizar para la cotización");
$error_man->showAviso("Aviso: La información del cliente como contactos, etc. ya insertado en la cotización se perderá");
$form->Listado('Seleccione cliente','sw_cliente','tb_cliente',array('dc_cliente','dg_razon'),1);
$form->End('Actualizar','editbtn');

echo("</div>");
exit();
}

$datos = $db->select('tb_cliente','dc_cliente,dg_razon',"dc_cliente = {$_POST['sw_cliente']} AND dc_empresa={$empresa} AND dm_activo='1'");

if(!count($datos)){
	$error_man->showWarning("Los datos del cliente son inválidos, consulte con el encargado sobre el error");
	exit();
}
$datos = $datos[0];

echo("<div id='selects'>");

$contactos = $db->select("(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
'c.dc_contacto,c.dg_contacto,s.dg_sucursal,cc.dg_cargo_contacto');
	  
echo("<select class='sw_contactos'>
<option value='0'></option>");
foreach($contactos as $c){
	echo("<option value='{$c['dc_contacto']}'>{$c['dg_contacto']} {$c['dg_sucursal']} {$c['dg_cargo_contacto']}</option>");
}
echo("</select>");

echo('</div>');
?>
<script type="text/javascript">
	$('#cli_razon').text('<?=$datos['dg_razon'] ?>');
	$(':hidden[name=cli_id]').val(<?=$_POST['sw_cliente'] ?>);
	$('#lst_contacto').html($('#selects .sw_contactos').html());
</script>
<div class="confirm">Los datos del cliente se han actualizado correctamente puede cerrar el asistente.</div>