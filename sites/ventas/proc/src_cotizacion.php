<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['cot_emision_desde'] = $db->sqlDate($_POST['cot_emision_desde']);
$_POST['cot_emision_hasta'] = $db->sqlDate($_POST['cot_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['cot_emision_desde']} AND {$_POST['cot_emision_hasta']})";

if($_POST['cot_numero_desde']){
	if($_POST['cot_numero_hasta']){
		$conditions .= " AND (dq_cotizacion BETWEEN {$_POST['cot_numero_desde']} AND {$_POST['cot_numero_hasta']})";
	}else{
		$conditions .= " AND dq_cotizacion = {$_POST['cot_numero_desde']}";
	}
}

if(isset($_POST['cot_client'])){
	$_POST['cot_client'] = implode(',',$_POST['cot_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['cot_client']})";
}

if(isset($_POST['cot_executive'])){
	$_POST['cot_executive'] = implode(',',$_POST['cot_executive']);
	$conditions .= " AND dc_ejecutivo IN ({$_POST['cot_executive']})";
}

if(isset($_POST['cot_tv'])){
	$_POST['cot_tv'] = implode(',',$_POST['cot_tv']);
	$conditions .= " AND dc_ejecutivo IN ({$_POST['cot_tv']})";
}

$data = $db->select('tb_cotizacion','dc_cotizacion,dq_cotizacion',$conditions,array('order_by' => 'df_fecha_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron cotizaciones con los criterios especificados");
	exit();
}

echo("<div id='show_cotizacion'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Cotizaciones<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Cotizacion</th>
	</tr>
</thead>
<tbody>
");

foreach($data as $c){
echo("
<tr>
	<td align='left'>
		<a href='sites/ventas/proc/show_cotizacion.php?id={$c['dc_cotizacion']}' class='cot_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_cotizacion']}</a>
	</td>
</tr>");
}

echo("
</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='editbtn' id='edit_cotizacion'>Editar</button>
<button type='button' id='gestion_cotizacion' class='imgbtn' style='background-image:url(images/management.png);'>Gestión</button>
<button type='button' id='historial_actividad' class='imgbtn' style='background-image:url(images/archive.png);'>Historial</button>
<button type='button' id='crear_nota_venta' class='imgbtn' style='background-image:url(images/doc.png);'>Crear Nota Venta</button>
</div>
");

?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".cot_load").click(function(e){
		e.preventDefault();
		$('#show_cotizacion').html("<img src='images/ajax-loader.gif' alt='' /> cargando cotizacion ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_cotizacion');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>