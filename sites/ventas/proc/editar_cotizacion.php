<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->update('tb_cotizacion',
array(
	"dc_termino_comercial" => $_POST['cot_termino'],
	"dc_contacto" => $_POST['cot_contacto'],
	"dc_tipo_cambio" => $_POST['prod_tipo_cambio'],
	"dq_cambio" => $_POST['cot_cambio'],
	"dc_modo_envio" => $_POST['cot_modo_envio'],
	"df_fecha_envio" => $db->sqlDate($_POST['cot_envio']),
	"dq_iva" => $_POST['cot_iva'],
	"dq_neto" => $_POST['cot_neto'],
	"dg_observacion" => $_POST['cot_observacion'],
	"dc_ejecutivo" => $_POST['cot_executive'],
	"dc_ejecutivo_tv" => $_POST['cot_televenta']
),"dc_cotizacion = {$_POST['cot_id']} AND dc_empresa={$empresa}");

$db->query("DELETE FROM tb_cotizacion_detalle WHERE dc_cotizacion = {$_POST['cot_id']}");

foreach($_POST['prod'] as $i => $v){
	$db->insert('tb_cotizacion_detalle',
	array(
		"dc_empresa" => $empresa,
		"dc_cotizacion" => $_POST['cot_id'],
		"dg_producto" => $_POST['prod'][$i],
		"dg_descripcion" => $_POST['desc'][$i],
		"dq_cantidad" => $_POST['cant'][$i],
		"dq_precio_venta" => str_replace(',','',$_POST['precio'][$i])*$_POST['cot_cambio'],
		"dq_precio_compra" => str_replace(',','',$_POST['costo'][$i])*$_POST['cot_cambio'],
		"dc_proveedor" => $_POST['proveedor'][$i]
	));
}

$error_man->showConfirm("La cotización fue modificada correctamente con ".count($_POST['prod'])." elementos en el detalle.<br />
<a href=\"#\" onclick=\"window.open('sites/ventas/ver_cotizacion.php?id={$_POST['cot_id']}','cotizacion','width=800,height=600')\"> Haga clic aquí para ver la cotización</a>");

?>