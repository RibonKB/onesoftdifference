<?php
/**
*	Ingresa una nueva cotización
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if($_POST['cot_oportunidad']){
if(!is_numeric($_POST['cot_oportunidad'])){
	$error_man->showWarning('Formato no esperado en el número de oportunidad');
	exit();
}
	if($empresa_conf['dm_cliente_op_cot'] == '1'){
	$op_data = $db->select('tb_oportunidad','dc_oportunidad',"dq_oportunidad={$_POST['cot_oportunidad']} AND dc_empresa={$empresa} AND dc_cliente={$_POST['cli_id']}");
		if(!count($op_data)){
			$error_man->showWarning("No puede relacionar la <b>cotización</b> con una <b>oportunidad</b> que no pertenezca al mismo cliente");
			exit();
		}
	}else{
	$op_data = $db->select('tb_oportunidad','dc_oportunidad',"dq_oportunidad={$_POST['cot_oportunidad']} AND dc_empresa={$empresa}");
		if(!count($op_data)){
			$error_man->showWarning("La oportunidad especificada no existe, compruebela o consulte con el encargado.");
			exit();
		}
	}
}else{
	$_POST['cot_oportunidad'] = 'NULL';
}

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_cotizacion']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$estado = $db->select('tb_valor_defecto','dc_valor',"dg_tabla = 'tb_cotizacion_estado' AND dc_empresa={$empresa}",array('order_by'=> 'df_creacion DESC'));

if(count($estado))
	$estado = $estado[0]['dc_valor'];
else
	$estado = 0;

$db->start_transaction();
$last_date = $db->select('tb_cotizacion','MAX(df_fecha_emision) fecha',"dc_empresa={$empresa}");

if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_cotizacion','dq_cotizacion',"dc_empresa={$empresa} AND df_fecha_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_cotizacion'],0,$largo_prefix) == $cot_prefix || $empresa_conf['dc_correlativo_cotizacion'] == '3'){
		$cot_num = substr($last[0]['dq_cotizacion'],$largo_prefix)+1;
	}else{
		$cot_num = $empresa_conf['dg_correlativo_cotizacion'];
	}
}else{
	$cot_num = $empresa_conf['dg_correlativo_cotizacion'];
}

$cotizacion = $db->insert("tb_cotizacion",
array(
	"dc_termino_comercial" => $_POST['cot_termino'],
	"dc_contacto" => $_POST['lst_contacto'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"dc_tipo_cambio" => $_POST['prod_tipo_cambio'],
	"dq_cambio" => $_POST['cot_cambio'],
	"dc_modo_envio" => $_POST['cot_modo_envio'],
	"dc_cliente" => $_POST['cli_id'],
	"dq_cotizacion" => $cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT),
	"df_fecha_emision" => "NOW()",
	"df_fecha_envio" => $db->sqlDate($_POST['cot_envio']),
	"dq_iva" => $_POST['cot_iva'],
	"dq_neto" => $_POST['cot_neto'],
	"dg_observacion" => substr($_POST['cot_observacion'],0,255),
	"dc_ejecutivo" => $_POST['cot_executive'],
	"dc_ejecutivo_tv" => $_POST['cot_televenta'],
	"dc_estado" => $estado,
	"dq_oportunidad" => $_POST['cot_oportunidad']
));

if(isset($_POST['contacto_extra'])){
foreach($_POST['contacto_extra'] as $i => $v){
	if($v != '0'){
		$db->insert("tb_cotizacion_contactos_extra_cotizacion",
		array(
			"dc_cotizacion" => $cotizacion,
			"dc_contacto" => $v,
			"dc_contacto_extra" => $_POST['tipo_cextra'][$i]
		));
	}
}}

$error_man->showConfirm("Se ha generado la cotización
<div class='title'>El número de cotización es <h1 style='margin:0;color:#000;'>".$cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT)."</h1></div>");

foreach($_POST['prod'] as $i => $v){
	$_POST['desc'][$i] = str_replace('<', '(lt)', $_POST['desc'][$i]);
	$_POST['desc'][$i] = str_replace('>', '(gt)', $_POST['desc'][$i]);
	$db->insert('tb_cotizacion_detalle',
	array(
		"dc_empresa" => $empresa,
		"dc_cotizacion" => $cotizacion,
		"dg_producto" => $_POST['prod'][$i],
		"dg_descripcion" => $_POST['desc'][$i],
		"dq_cantidad" => $_POST['cant'][$i],
		"dq_precio_venta" => str_replace(',','',$_POST['precio'][$i])*$_POST['cot_cambio'],
		"dq_precio_compra" => str_replace(',','',$_POST['costo'][$i])*$_POST['cot_cambio'],
		"dc_proveedor" => $_POST['proveedor'][$i]
	));
}

$error_man->showConfirm("Fueron agregados ".count($_POST['prod'])." elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/ventas/ver_cotizacion.php?id={$cotizacion}','cotizacion','width=800,height=600')\"> Haga clic aquí para ver la cotización</a>");

$contacto = $db->select('tb_contacto_cliente','dg_contacto',"dc_contacto={$_POST['lst_contacto']}");
if(count($contacto)){
	$contacto = $contacto[0]['dg_contacto'];
}else{
	$contacto = 'N/A';
}

$db->insert('tb_cotizacion_avance',
array(
	"dc_cotizacion" => $cotizacion,
	"dg_contacto" => $contacto,
	"dg_gestion" => 'Creación',
	"df_creacion" => 'NOW()',
	"df_proxima_actividad" => "ADDDATE(NOW(),{$empresa_conf['dn_cotizacion_dias_actividad']})",
	"dc_estado_cotizacion" => $estado
));

if($_POST['cot_oportunidad'] != 'NULL'){

	$db->update('tb_oportunidad_avance',
	array(
		'dm_estado_actividad' => 0,
		'df_cierre_actividad' => 'NOW()'
	),"dc_oportunidad={$op_data[0]['dc_oportunidad']} AND dm_estado_actividad = '1'");
	
	$db->insert('tb_oportunidad_avance',
	array(
		'dc_oportunidad' => $op_data[0]['dc_oportunidad'],
		'dg_gestion' => "Generada cotización enlazada número: ".$cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT),
		'df_creacion' => 'NOW()',
		'dm_estado_oportunidad' => '1',
		'dm_estado_actividad' => '0',
		'dg_contacto' => $contacto
	));
	
	$db->update('tb_oportunidad',array(
		'dm_estado' => 1,
		'dc_cotizacion' => $cotizacion
	),"dc_oportunidad={$op_data[0]['dc_oportunidad']}");
}

$db->commit();

?>