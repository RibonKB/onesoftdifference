<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$new = $db->insert("tb_cotizacion_contacto_extra",
array(
	"dg_etiqueta" => ucwords($_POST['cextra_label']),
	"dc_tipo_direccion" => $_POST['cextra_tipo'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"df_fecha_creacion" => "NOW()"
));

$error_man->showConfirm("Se ha creado el contacto para cotización <strong>{$_POST['cextra_label']}</strong> correctamente");

$new = $db->select(
		"tb_cotizacion_contacto_extra ce, tb_tipo_direccion td",
		"ce.dc_contacto_extra, ce.dg_etiqueta, td.dg_tipo_direccion",
		"ce.dc_tipo_direccion = td.dc_tipo_direccion AND ce.dc_empresa={$empresa} AND dc_contacto_extra={$new}"
		);
$new = $new[0];

echo("
<table style='display:none;'>
<tr id='item{$new['dc_contacto_extra']}'>
	<td>{$new['dg_etiqueta']}</td>
	<td>{$new['dg_tipo_direccion']}</td>
	<td>
		<a href='sites/ventas/ed_cotizacion_cextra.php?id={$new['dc_contacto_extra']}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/ventas/del_cotizacion_cextra.php?id={$new['dc_contacto_extra']}' class='loadOnOverlay'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>
");

?>
<script type="text/javascript">
	$("#item<?php echo($new['dc_contacto_extra']); ?>").appendTo("#list");
	
	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

</script>