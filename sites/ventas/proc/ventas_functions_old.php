<?php

$rebajaReservado = false;
$rebajaLibre = false;
$rebaja = false;
$comprueba = false;

function bodega_RebajarStockReservado($producto, $cantidad, $bodega){
	global $db;
	
	$db->update('tb_stock',array(
		'dq_stock' => "dq_stock-{$cantidad}",
		'dq_stock_reservado' => "dq_stock_reservado-{$cantidad}"
	),"dc_producto={$producto} AND dc_bodega={$bodega} AND dq_stock >= {$cantidad} AND dq_stock_reservado >= {$cantidad}");
	
	return $db->affected_rows();
}

function bodega_RebajarStockLibre($producto, $cantidad, $bodega){
	global $db;

	$db->update('tb_stock',array(
		'dq_stock' => "dq_stock-{$cantidad}"
	),"dc_producto={$producto} AND dc_bodega={$bodega} AND (dq_stock-dq_stock_reservado) >= {$cantidad}");
	
	return $db->affected_rows();
}

/*function bodega_RebajarStock($producto, $cantidad, $bodega){
	global $db,$rebaja;

	if($rebaja === false){
		$rebaja = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock-:cant"
		),"dc_producto=:whereProduct AND dc_bodega=:whereBodega AND dq_stock >= :whereCant"));
	}
	
	$rebaja->bindValue(':cant',$cantidad,PDO::PARAM_INT);
	$rebaja->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
	$rebaja->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);
	$rebaja->bindValue(':whereCant',$cantidad,PDO::PARAM_INT);
	
	$db->stExec($rebaja);
	
	return $rebaja->rowCount();
}*/

function bodega_ComprobarStock($producto, $bodega){
	global $db;
	
	$stock = $db->select('tb_stock','dq_stock',"dc_producto={$producto} AND dc_bodega={$bodega}");
	$stock = array_shift($stock);
	
	if($stock == NULL){
		return 0;
	}else{
		return intval($stock['dq_stock']);
	}
}

function doc_GetNextNumber($table, $campo_number, $format=2, $emision = 'df_emision') {
    global $db,$empresa;

    $doc_prefix = '';
    $largo_prefix = 0;

    switch ($format) {
        case 1: $doc_prefix = date('Y');
            $largo_prefix = 4;
            break;
        case 2: $doc_prefix = date('Ym');
            $largo_prefix = 6;
            break;
		case 4: $doc_prefix = date('m');
			$largo_prefix = 2;
			break;
    }

    if ($format != 3) {
        $Mactual = date('m');
        $Yactual = date('Y');
        $last = $db->select($table, "MAX({$campo_number}) AS dq", "MONTH({$emision}) = '{$Mactual}' AND YEAR({$emision}) = '{$Yactual}' AND dc_empresa = {$empresa}");
    } else {
        $last = $db->select($table, "MAX({$campo_number}) AS dq AND dc_empresa = {$empresa}");
    }

    $last = $last[0]['dq'];

    if ($last != NULL) {
        $doc_num = substr($last, $largo_prefix) + 1;
    } else {
        $doc_num = 1;
    }

    return $doc_prefix . str_pad($doc_num, 4, 0, STR_PAD_LEFT);
}

//Suma de numero de documento a prueba de desborde
function doc_GetRealNext($number, $format=2){
	switch ($format) {
        case 1: $largo_prefix = 4;
            break;
        case 2: $largo_prefix = 6;
            break;
		default: $largo_prefix = 0;
            break;
    }
	
	$doc_prefix = substr($number,0,$largo_prefix);
	$doc_num = substr($number,$largo_prefix);
	
	 return $doc_prefix . str_pad(++$doc_num, 4, 0, STR_PAD_LEFT);
}


?>