<?php

$rebajaReservado = false;
$rebajaReservadoOS = false;

$rebajaLibre = false;
$cargaReservado = false;
$cargaLibre = false;
$cargaLibreInsert = false;

$cargaNV = false;
$cargaNVInsert = false;

$cargaOS = false;
$cargaOSInsert = false;

$rebaja = false;
$comprueba = false;

function bodega_RebajarStockReservado($producto, $cantidad, $bodega){
	global $db,$rebajaReservado;

	if($rebajaReservado === false){
		$rebajaReservado = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock-?",
			'dq_stock_reservado' => "dq_stock_reservado-?"
		),"dc_producto=? AND dc_bodega=? AND dq_stock >= ? AND dq_stock_reservado >= ?"));
	}
	
	$rebajaReservado->bindValue(1,$cantidad,PDO::PARAM_INT);
	$rebajaReservado->bindValue(2,$cantidad,PDO::PARAM_INT);
	$rebajaReservado->bindValue(3,$producto,PDO::PARAM_INT);
	$rebajaReservado->bindValue(4,$bodega,PDO::PARAM_INT);
	$rebajaReservado->bindValue(5,$cantidad,PDO::PARAM_INT);
	$rebajaReservado->bindValue(6,$cantidad,PDO::PARAM_INT);
	
	$db->stExec($rebajaReservado);
	
	return $rebajaReservado->rowCount();
}

function bodega_RebajarStockReservadoOS($producto, $cantidad, $bodega){
	global $db,$rebajaReservadoOS;

	if($rebajaReservadoOS === false){
		$rebajaReservadoOS = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock-?",
			'dq_stock_reservado_os' => "dq_stock_reservado_os-?"
		),"dc_producto=? AND dc_bodega=? AND dq_stock >= ? AND dq_stock_reservado >= ?"));
	}
	
	$rebajaReservado->bindValue(1,$cantidad,PDO::PARAM_INT);
	$rebajaReservado->bindValue(2,$cantidad,PDO::PARAM_INT);
	$rebajaReservado->bindValue(3,$producto,PDO::PARAM_INT);
	$rebajaReservado->bindValue(4,$bodega,PDO::PARAM_INT);
	$rebajaReservado->bindValue(5,$cantidad,PDO::PARAM_INT);
	$rebajaReservado->bindValue(6,$cantidad,PDO::PARAM_INT);
	
	$db->stExec($rebajaReservadoOS);
	
	return $rebajaReservadoOS->rowCount();
}

function bodega_RebajarStockLibre($producto, $cantidad, $bodega){
	global $db,$rebajaLibre;

	if($rebajaLibre === false){
		$rebajaLibre = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock-:cant"
		),"dc_producto=:whereProduct AND dc_bodega=:whereBodega AND (dq_stock-dq_stock_reservado) >= :whereCant"));
	}
	
	$rebajaLibre->bindValue(':cant',$cantidad,PDO::PARAM_INT);
	$rebajaLibre->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
	$rebajaLibre->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);
	$rebajaLibre->bindValue(':whereCant',$cantidad,PDO::PARAM_INT);
	
	$db->stExec($rebajaLibre);
	
	return $rebajaLibre->rowCount();
}

function bodega_RebajarStock($producto, $cantidad, $bodega){
	global $db,$rebaja;

	if($rebaja === false){
		$rebaja = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock-:cant"
		),"dc_producto=:whereProduct AND dc_bodega=:whereBodega AND dq_stock >= :whereCant"));
	}
	
	$rebaja->bindValue(':cant',$cantidad,PDO::PARAM_INT);
	$rebaja->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
	$rebaja->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);
	$rebaja->bindValue(':whereCant',$cantidad,PDO::PARAM_INT);
	
	$db->stExec($rebaja);
	
	return $rebaja->rowCount();
}

function bodega_CargarStockLibre($producto, $cantidad, $bodega){
	global $db,$cargaLibre,$cargaLibreInsert;
	
	if($cargaLibre === false){
		$cargaLibre = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock+:cant"
		),"dc_producto=:whereProduct AND dc_bodega=:whereBodega"));
	}
	
	$cargaLibre->bindValue(':cant',$cantidad,PDO::PARAM_INT);
	$cargaLibre->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
	$cargaLibre->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);
	
	$db->stExec($cargaLibre);
	
	if($cargaLibre->rowCount() == 0){
		if($cargaLibreInsert === false){
			$cargaLibreInsert = $db->prepare($db->insert('tb_stock',array(
				'dq_stock' => ':cant',
				'dc_producto' => ':producto',
				'dc_bodega' => ':bodega'
			)));
		}
		
		$cargaLibreInsert->bindValue(':cant',$cantidad,PDO::PARAM_INT);
		$cargaLibreInsert->bindValue(':producto',$producto,PDO::PARAM_INT);
		$cargaLibreInsert->bindValue(':bodega',$bodega,PDO::PARAM_INT);
		
		$db->stExec($cargaLibreInsert);
	}
}

//reservado a Nota de venta
function bodega_CargarStockNV($producto, $cantidad, $bodega){
	global $db,$cargaNV,$cargaNVInsert;
	
	if($cargaNV === false){
		$cargaNV = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock+:cant",
			'dq_stock_reservado' => "dq_stock_reservado+:cantNV"
		),"dc_producto=:whereProduct AND dc_bodega=:whereBodega"));
	}
	
	$cargaNV->bindValue(':cant',$cantidad,PDO::PARAM_INT);
	$cargaNV->bindValue(':cantNV',$cantidad,PDO::PARAM_INT);
	$cargaNV->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
	$cargaNV->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);
	
	$db->stExec($cargaNV);
	
	if($cargaNV->rowCount() == 0){
		if($cargaNVInsert === false){
			$cargaNVInsert = $db->prepare($db->insert('tb_stock',array(
				'dq_stock' => ':cant',
				'dq_stock_reservado' => ':cantNV',
				'dc_producto' => ':producto',
				'dc_bodega' => ':bodega'
			)));
		}
		
		$cargaNVInsert->bindValue(':cant',$cantidad,PDO::PARAM_INT);
		$cargaNVInsert->bindValue(':cantNV',$cantidad,PDO::PARAM_INT);
		$cargaNVInsert->bindValue(':producto',$producto,PDO::PARAM_INT);
		$cargaNVInsert->bindValue(':bodega',$bodega,PDO::PARAM_INT);
		
		$db->stExec($cargaNVInsert);
	}
}
//

//reservado a Orden de servicio
function bodega_CargarStockOS($producto, $cantidad, $bodega){
	global $db,$cargaOS,$cargaOSInsert;
	
	if($cargaOS === false){
		$cargaOS = $db->prepare($db->update('tb_stock',array(
			'dq_stock' => "dq_stock+:cant",
			'dq_stock_reservado_os' => "dq_stock_reservado_os+:cantOS"
		),"dc_producto=:whereProduct AND dc_bodega=:whereBodega"));
	}
	
	$cargaOS->bindValue(':cant',$cantidad,PDO::PARAM_INT);
	$cargaOS->bindValue(':cantOS',$cantidad,PDO::PARAM_INT);
	$cargaOS->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
	$cargaOS->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);
	
	$db->stExec($cargaOS);
	
	if($cargaOS->rowCount() == 0){
		if($cargaOSInsert === false){
			$cargaOSInsert = $db->prepare($db->insert('tb_stock',array(
				'dq_stock' => ':cant',
				'dq_stock_reservado_os' => ':cantOS',
				'dc_producto' => ':producto',
				'dc_bodega' => ':bodega'
			)));
		}
		
		$cargaOSInsert->bindValue(':cant',$cantidad,PDO::PARAM_INT);
		$cargaOSInsert->bindValue(':cantOS',$cantidad,PDO::PARAM_INT);
		$cargaOSInsert->bindValue(':producto',$producto,PDO::PARAM_INT);
		$cargaOSInsert->bindValue(':bodega',$bodega,PDO::PARAM_INT);
		
		$db->stExec($cargaOSInsert);
	}
}
//

function bodega_ComprobarStock($producto, $bodega){
	global $db;
	
	$stock = $db->doQuery($db->select('tb_stock','dq_stock',"dc_producto={$producto} AND dc_bodega={$bodega}"))->fetch(PDO::FETCH_ASSOC);
	
	if($stock === false){
		return 0;
	}else{
		return $stock['dq_stock'];
	}
}

function bodega_ComprobarTotalStock($producto, $bodega){
	global $db;
	
	$stock = $db->doQuery($db->select('tb_stock','dq_stock,dq_stock_reservado,dq_stock_reservado_os',
	"dc_producto={$producto} AND dc_bodega={$bodega}"))->fetch(PDO::FETCH_OBJ);
	
	if($stock === false){
		return array(0,0,0);
	}else{
		return array($stock->dq_stock-$stock->dq_stock_reservado-$stock->dq_stock_reservado_os,$stock->dq_stock_reservado,$stock->dq_stock_reservado_os);
	}
}

function doc_GetNextNumber($table, $campo_number, $format=2, $emision = 'df_emision') {
    global $db,$empresa;

    $doc_prefix = '';
    $largo_prefix = 0;

    switch ($format) {
        case 1: $doc_prefix = date('Y');
            $largo_prefix = 4;
            break;
        case 2: $doc_prefix = date('Ym');
            $largo_prefix = 6;
            break;
		case 4: $doc_prefix = date('m');
			$largo_prefix = 2;
			break;
    }

    if ($format != 3) {
        $Mactual = date('m');
        $Yactual = date('Y');
        $last = $db->doQuery($db->select($table, "MAX({$campo_number}) AS dq", "MONTH({$emision})='{$Mactual}' AND YEAR({$emision})='{$Yactual}' AND dc_empresa={$empresa}"));
    } else {
        $last = $db->doQuery($db->select($table, "MAX({$campo_number}) AS dq AND dc_empresa={$empresa}"));
    }

    $last = $last->fetch(PDO::FETCH_OBJ)->dq;

    if ($last != NULL) {
        $doc_num = substr($last, $largo_prefix) + 1;
    } else {
        $doc_num = 1;
    }

    return $doc_prefix . str_pad($doc_num, 4, 0, STR_PAD_LEFT);
}

//Suma de numero de documento a prueba de desborde
function doc_GetRealNext($number, $format=2){
	switch ($format) {
        case 1: $largo_prefix = 4;
            break;
        case 2: $largo_prefix = 6;
            break;
		default: $largo_prefix = 0;
            break;
    }
	
	$doc_prefix = substr($number,0,$largo_prefix);
	$doc_num = substr($number,$largo_prefix);
	
	 return $doc_prefix . str_pad(++$doc_num, 4, 0, STR_PAD_LEFT);
}


?>