<?php
/**
*	Edicion de contacto de cotizacion extra
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se modifican los datos en la base de datos
$db->update("tb_cotizacion_contacto_extra",
array(
	"dg_etiqueta" => trim($_POST['ed_cextra_label']),
	"dc_tipo_direccion" => $_POST['ed_cextra_tipo']
),"dc_contacto_extra = {$_POST['ed_cextra_id']} AND dc_empresa={$empresa}");

?>
<script type="text/javascript">
	loadpage("sites/ventas/cotizacion_cextra.php");
</script>