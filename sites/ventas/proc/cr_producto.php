<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div class="secc_bar">Creación de productos</div>
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Complete los datos para la creación del producto</strong><br />(Los campos marcados con [*] son obligatorios)");
	$form->Start("sites/ventas/proc/crear_producto.php","prod_crear");
	$form->Section();
	$form->Text("Nombre","prod_nombre",1,255,$_POST['d']);
	$form->Text("Precio venta [$] (Referencia)","prod_precio_venta",1,10,$_POST['v']);
	$form->Listado("Tipo de producto","prod_tipo","tb_tipo_producto",array("dc_tipo_producto","dg_tipo_producto"),1);
	$form->Text("Unidad de medida","prod_unidad",1,15,"un.");
	$form->Listado("Linea de negocio","prod_lnegocio","tb_linea_negocio",array("dc_linea_negocio","dg_linea_negocio"),1);
	$form->EndSection();
	$form->Section();
	$form->Text("Código","prod_codigo",1,255,$_POST['c']);
	$form->Text("Precio compra [$] (Referencia)","prod_precio_compra",1,10,$_POST['q']);
	$form->Listado("Marca","prod_marca","tb_marca",array("dc_marca","dg_marca"),1);
	$form->Listado("Centro beneficio","prod_cebe","tb_cebe",array("dc_cebe","dg_cebe"),1);
	$form->EndSection();
	$form->End("Crear","addbtn");
	
?>
</div>

<script type="text/javascript">
$('#prod_precio_venta,#prod_precio_compra').change(function(){
if(parseFloat($(this).val()))
	$(this).val(mil_format(toNumber($(this).val())));
else
	$(this).val('');
}).change();
</script>