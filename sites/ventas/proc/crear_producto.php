<?php
	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	if(count($db->select("tb_producto","1","dg_codigo = '{$_POST['prod_codigo']}' AND dc_empresa = {$empresa}"))){
		$error_man->showWarning("Ya hay un producto utilizando el código indicado, pruebe otro o consulte a un administrador");
		exit();
	}

	$p_precio_venta = str_replace(',',"",$_POST['prod_precio_venta']);
	$p_precio_compra = str_replace(',',"",$_POST['prod_precio_compra']);
	if((!is_numeric($p_precio_venta)) || (!is_numeric($p_precio_compra))){
		$error_man->showWarning("El precio no es un número válido");
		exit();
	}
	
	$p_id = $db->insert("tb_producto",array(
		"dg_producto" => $_POST['prod_nombre'],
		"dg_codigo" => $_POST['prod_codigo'],
		"dq_precio_venta" => $p_precio_venta,
		"dq_precio_compra" => $p_precio_compra,
		"dc_marca" => $_POST['prod_marca'],
		"dc_tipo_producto" => $_POST['prod_tipo'],
		"dc_linea_negocio" => $_POST['prod_lnegocio'],
		"dc_empresa" => $empresa,
		"dc_cebe" => $_POST['prod_cebe'],
		"dg_unidad_medida" => $_POST['prod_unidad'],
		"dc_usuario_creacion" => $idUsuario
	));
	
	$error_man->showConfirm("Se ha creado el producto correctamente.");
	
	$detline = json_encode(array(array(
		"dc_producto" => $p_id,
		"dg_descripcion" => $_POST['prod_nombre'],
		"dc_proveedor" => 0,
		"dq_cantidad" => 1,
		"dq_precio_compra" => $p_precio_compra,
		"dq_precio_venta" => $p_precio_venta,
		"dg_producto" => $_POST['prod_codigo']
	)));
	
?>
<script type="text/javascript">
	$('#genOverlay').remove();
	var detalle_producto = <?=$detline ?>;
	set_detail(detalle_producto);
</script>