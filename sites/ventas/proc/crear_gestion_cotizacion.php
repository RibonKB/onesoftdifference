<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!$_POST['gestion_fecha']){
	if(!$_POST['gestion_cambio']){
		$error_man->showWarning('Si no indica una fecha para la próxima actividad debe seleccionar un estado de cotización que cierre el proceso.');
		exit;
	}else if(!count($db->select('tb_cotizacion_estado','1',"dc_estado={$_POST['gestion_cambio']} AND dm_fin_proceso='1'"))){
		$error_man->showWarning('Si no indica una fecha para la próxima actividad debe seleccionar un estado de cotización que cierre el proceso.');
		exit;
	}
}

if($_POST['gestion_cambio']){
	$db->update('tb_cotizacion',
	array(
		"dc_estado" => $_POST['gestion_cambio']
	),"dc_cotizacion = {$_POST['cot_id']}");
}

if(isset($_POST['gestion_avance'])){
	$db->update('tb_cotizacion_avance',
	array(
		"dm_estado_actividad" => 0,
		"df_cierre_actividad" => 'NOW()'
	),"dc_avance = {$_POST['gestion_avance']}");
}

if($_POST['gestion_fecha']){
	$hora = str_pad($_POST['gestion_hora']+$_POST['gestion_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_minuto'];
	$_POST['gestion_fecha'] = $_POST['gestion_fecha']." ".$hora;
	unset($hora);
}

$dg_contacto = $db->select('tb_contacto_cliente','dg_contacto',"dc_contacto={$_POST['dc_contacto']}");
$dg_contacto = $dg_contacto[0]['dg_contacto'];

$db->insert('tb_cotizacion_avance',
array(
	"dc_cotizacion" => $_POST['cot_id'],
	"dg_gestion" => $_POST['gestion_gestion'],
	"df_creacion" => 'NOW()',
	"df_proxima_actividad" => $db->sqlDate($_POST['gestion_fecha']),
	"dc_estado_cotizacion" => $_POST['gestion_estado'],
	"dm_estado_actividad" => 1,
	"dg_contacto" => $dg_contacto,
	"dc_usuario_creacion" => $idUsuario,
	"dc_motivo_gestion" => intval($_POST['dc_motivo_estado']),
	"dc_marca_referencia" => intval($_POST['dc_marca_referencia'])
));

$error_man->showConfirm("Se ha generado una nueva actividad sobre la cotización");

?>