<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$poblacion = array(
	'termino' => '',
	'tipo_cambio' => '',
	'cambio' => '',
	'modo_envio' => '',
	'observacion' => '',
	'ejecutivo' => $userdata['dc_funcionario'],
	'televenta' => $userdata['dc_funcionario']
);

if($_POST['cot_base_num']){
	//Se escapan los caracteres por seguridad
	$db->escape($_POST['cot_base_num']);
	
	$base = $db->select('tb_cotizacion',
	'dc_cotizacion,dc_termino_comercial,dc_tipo_cambio,dq_cambio,dc_modo_envio,dg_observacion,dc_ejecutivo,dc_ejecutivo_tv',
	"dq_cotizacion={$_POST['cot_base_num']} AND dc_empresa = {$empresa}");
	
	if(!count($base)){
		$error_man->showErrorRedirect("No se encontró la cotización especificada como plantilla, intentelo nuevamente.","sites/ventas/cr_cotizacion.php");
	}
	$base = $base[0];
	
	$poblacion = array(
		'termino' => $base['dc_termino_comercial'],
		'tipo_cambio' => $base['dc_tipo_cambio'],
		'cambio' => number_format($base['dq_cambio'],$empresa_conf['dn_decimales_local']),
		'modo_envio' => $base['dc_modo_envio'],
		'observacion' => $base['dg_observacion'],
		'ejecutivo' => $base['dc_ejecutivo'],
		'televenta' => $base['dc_ejecutivo_tv']
	);
	
	$detalles_base = $db->select('tb_cotizacion_detalle',
	'dg_producto,dq_cantidad,dg_descripcion,dq_precio_venta,dq_precio_compra,dc_proveedor',
	"dc_cotizacion={$base['dc_cotizacion']}");
}

if(isset($_POST['cli_rut'])){
	//Se escapan los caracteres por seguridad
	$db->escape($_POST['cli_rut']);
	if($idUsuario == 145){
		$ejecutivo_cond = 'AND dc_ejecutivo = 182';
	}else{
		$ejecutivo_cond = '';
	}
	$datos = $db->select("tb_cliente","dc_cliente,dg_razon,dc_contacto_default",
	"dg_rut = '{$_POST['cli_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1' {$ejecutivo_cond}");
	
	if(!count($datos)){
		$error_man->showErrorRedirect("No se encontró el cliente especificado, intentelo nuevamente.","sites/ventas/cr_cotizacion.php");
	}
	$datos = $datos[0];
	$poblacion['oportunidad'] = '';
}else if(isset($_POST['cli_id'])){
	$datos = $db->select("tb_cliente",'dc_cliente,dg_razon,dc_contacto_default',
	"dc_cliente={$_POST['cli_id']} AND dc_empresa={$empresa} AND dm_activo='1'");
	
	if(!count($datos)){
		$error_man->showErrorRedirect("Error: Cliente inválido, intentelo nuevamente.","sites/ventas/oportunidad/src_oportunidad.php");
	}
	$datos = $datos[0];

	$poblacion['oportunidad'] =& $_POST['op_numero'];
}

echo("<div id='secc_bar'>Creación de cotización</div>
<div id='main_cont'>
<div class='panes' style='width:1140px;'>
<div class='title center'>
<button class='right button' id='cli_switch'>Cambiar Cliente</button>
Generando cotización para <strong id='cli_razon' style='color:#000;'>{$datos['dg_razon']}</strong>
</div>");

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio,dn_cantidad_decimales",
	"dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$c['dq_cambio'] = number_format($c['dq_cambio'],$c['dn_cantidad_decimales'],'.',',');
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |{$c['dq_cambio']}|";
		$form->Hidden("decimal{$c['dc_tipo_cambio']}",$c['dn_cantidad_decimales']);
	}
	
	$form->Start("sites/ventas/proc/crear_cotizacion.php","cr_cotizacion",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la cotización</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();
	$form->Listado('Contacto','lst_contacto',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');
	
	$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1,$poblacion['tipo_cambio']);
	$form->Text('Cambio','cot_cambio',1,255,$poblacion['cambio']);
	$form->Listado("Términos comerciales","cot_termino","tb_termino_comercial",array("dc_termino_comercial","dg_termino_comercial"),0,$poblacion['termino']);
	$form->EndSection();
	
	$form->Section();
	$form->Date("Fecha envio","cot_envio",0,1);
	
	$form->Listado("Modo de envio","cot_modo_envio","tb_modo_envio",array("dc_modo_envio","dg_modo_envio"),0,$poblacion['modo_envio']);
	
	$form->Listado('Ejecutivo','cot_executive','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),1,$poblacion['ejecutivo'],"tf.dc_empresa = {$empresa} AND dc_modulo = 1 AND dm_activo = 1");	
	$form->Listado('Ejecutivo televenta','cot_televenta','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),0,$poblacion['televenta'],"tf.dc_empresa = {$empresa} AND dc_modulo = 1 AND dm_activo = 1");
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','cot_observacion',0,$poblacion['observacion']);
	$form->Text('Oportunidad Número','cot_oportunidad',$empresa_conf['dm_wf_op_requerida'],255,$poblacion['oportunidad']);
	$form->EndSection();
	
	echo("<hr class='clear' />");
	
	$c_extra = $db->select("tb_cotizacion_contacto_extra",
	"dg_etiqueta,dc_tipo_direccion,dc_contacto_extra",
	"dc_empresa = {$empresa} AND dm_activo = '1'");
	
	foreach($c_extra as $ex){
		$form->Section();
		$form->Hidden('tipo_cextra[]',$ex['dc_contacto_extra']);
		$form->Listado($ex['dg_etiqueta'],'contacto_extra[]',"(SELECT * FROM tb_cliente_sucursal_tipo WHERE dc_tipo_direccion = {$ex['dc_tipo_direccion']}) st
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = st.dc_sucursal
		JOIN tb_contacto_cliente c ON c.dc_sucursal = s.dc_sucursal
		LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),0,0,'');
		$form->EndSection();
	}
	echo("<br class='clear' /><br />");
	$form->Header("Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la cotización",'id="prod_info"');
	echo("<div id='prods' style='display:none;'>
	<br />
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='60'>Opciones</th>
		<th width='95'>Código (Autocompleta)</th>
		<th>Descripción</th>
		<th width='158'>Proveedor</th>
		<th width='63'>Cantidad</th>
		<th width='85'>Precio</th>
		<th width='85'>Total</th>
		<th width='85'>Costo</th>
		<th width='85'>Costo total</th>
		<th width='85'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'>");
	if(isset($detalles_base)){
		foreach($detalles_base as $d){
		$d['dq_precio_venta'] /= $base['dq_cambio'];
		$d['dq_precio_compra'] /= $base['dq_cambio'];
			echo("
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo inputtext' size='7' value='{$d['dg_producto']}' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='25' value='{$d['dg_descripcion']}' required='required' /></td>
		<td><select name='proveedor[]' style='width:155px;' class='inputtext' required='required'><option></option>");
		$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'");
		foreach($proveedores as $p){
			$sel = $p['dc_proveedor']==$d['dc_proveedor']?'selected="selected"':'';
			echo("<option value='{$p['dc_proveedor']}' {$sel}>{$p['dg_razon']}</option>");
		}
		echo("</select></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' value='{$d['dq_cantidad']}' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' value='{$d['dq_precio_venta']}' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'>
			<input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' value='{$d['dq_precio_compra']}' required='required' />
		</td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td class='margen_p' align='center'>0</td>
	</tr>
	<tr style='display:none;'>
		<td colspan='5' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_price' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_margen' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>");
		}
	}
	echo("</tbody>
	<tfoot>
	<tr>
		<th colspan='5' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center' id='total_margen_p'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>");
	$form->Hidden('cli_id',$datos['dc_cliente']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->End('Crear','addbtn');
	
	echo("<table id='prods_form' style='display:none;'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo inputtext' size='7' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' /></td>
		<td><select name='proveedor[]' style='width:155px;' class='inputtext' required='required'><option></option>");
		$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'");
		foreach($proveedores as $p){
			echo("<option value='{$p['dc_proveedor']}'>{$p['dg_razon']}</option>");
		}
		echo("</select></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td class='margen_p'><input type='text' class='margen_p inputtext' size='7' /></td>
	</tr>
	<tr style='display:none;'>
		<td colspan='5' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_price' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_margen' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>
	</table>");
?>
</div></div>
<script type="text/javascript">    
	$('#cli_switch').click(function(){
		loadOverlay('sites/ventas/switch_cliente.php');
	});
	empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;
</script>
<script type="text/javascript" src="jscripts/product_manager/cotizacion.js?v=1_5c"></script>
<script type="text/javascript">
	init_autocomplete();
	$('#prod_tipo_cambio').change();
</script>