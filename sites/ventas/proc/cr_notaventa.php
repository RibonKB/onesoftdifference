<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("tb_cliente","dc_cliente,dg_razon","dg_rut = '{$_POST['cli_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el cliente especificado, intentelo nuevamente.","sites/ventas/cr_notaventa.php");
}
$datos = $datos[0];

$contactos = $db->select(
"tb_domicilio_cliente dc, tb_comuna co, tb_domicilio_cliente_tipo_direccion td, tb_contacto_cliente cc",
"DISTINCT cc.dc_contacto, dc.dg_direccion, co.dg_comuna, cc.dg_contacto",
"dc.dc_domicilio = td.dc_domicilio AND dc.dc_comuna = co.dc_comuna AND cc.dc_contacto = td.dc_contacto AND dc.dc_cliente = {$datos['dc_cliente']} AND td.dm_activo = '1'",
array("order_by"=>"cc.dm_default","order_dir"=>"DESC")
);

?>
<div id="secc_bar">Creación de nota de venta</div>
<div id="main_cont">
<div class="panes">
<div class="title center">Generando nota de venta para <strong><?=$datos['dg_razon'] ?></strong></div>
<hr class="clear" />
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Indique los datos para la genración de la nota de venta</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Start("sites/ventas/proc/crear_notaventa.php","cr_notaventa","form","method='post' enctype='multipart/form-data'");
	$form->Section();
	echo("<label>Contacto<br />
	<select name='nv_contacto' class='inputtext' style='width:285px;' required='required'>");
	foreach($contactos as $c){
		echo("
		<option value=\"{$c['dc_contacto']}\">
			{$c['dg_contacto']}
			({$c['dg_direccion']} {$c['dg_comuna']})
		</option>
		");
	}
	echo("</select>
	</label><br />");
	
	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio","dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |{$c['dq_cambio']}|";
	}
	$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1);
	$form->Listado("Términos comerciales","nv_termino","tb_termino_comercial",array("dc_termino_comercial","dg_termino_comercial"));
	$form->EndSection();
	
	$form->Section();
	$form->Date("Fecha envio","nv_envio");
	$form->Listado("Modo de envio","nv_modo_envio","tb_modo_envio",array("dc_modo_envio","dg_modo_envio"));
	$form->EndSection();
	
	echo("<hr class='clear' />");
	
	$c_extra = $db->select(
	"tb_nota_venta_contacto_extra",
	"dg_etiqueta,dc_tipo_direccion,dc_contacto_extra",
	"dc_empresa = {$empresa} AND dm_activo = '1'"
	);
	foreach($c_extra as $ex){
		$con_extras = $db->select(
		"tb_domicilio_cliente dc, tb_comuna co, tb_domicilio_cliente_tipo_direccion td, tb_contacto_cliente cc",
		"cc.dc_contacto, dc.dg_direccion, co.dg_comuna, cc.dg_contacto",
		"dc.dc_domicilio = td.dc_domicilio AND dc.dc_comuna = co.dc_comuna AND cc.dc_contacto = td.dc_contacto AND dc.dc_cliente = {$datos['dc_cliente']} AND td.dm_activo = '1' AND td.dc_tipo_direccion = {$ex['dc_tipo_direccion']}"
		);
				
		echo("
		<div class='left'>
		<input type='hidden' name='tipo_cextra[]' value='{$ex['dc_contacto_extra']}' />
		<label>{$ex['dg_etiqueta']}<br />
		<select name='contacto_extra[]' class='inputtext' style='width:280px;'>
		<option value='0'></option>
		");
		foreach($con_extras as $e){
			echo("
			<option value='{$e['dc_contacto']}'>{$e['dg_contacto']} ({$e['dg_direccion']} {$e['dg_comuna']})</option>");
		}
		echo("
		</select>
		<label><br />
		</div>
		");
			}
		?>
	<br class="clear" />
	<fieldset>
	<legend>Productos en la nota de venta</legend>
	<div class="info" id="prod_info">Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la nota de venta</div>
	<div id="prods" style="display:none;">
	<table width="100%" class="tab">
		<thead>
			<tr>
				<th width="13%">Código (Autocompleta)</th>
				<th width="33%">Nombre</th>
				<th width="10%">Cantidad</th>
				<th width="10%">Precio (Base)</th>
				<th width="12%">Total (Base)</th>
				<th width="10%">Precio (T/C)</th>
				<th width="12%">Total (T/C)</th>
			</tr>
		</thead>
		<tbody id="prod_list">
			<tr>
				<td style="position:absolute;">
				<input type="text" name="prod[]" class="prod_codigo inputtext" size="7" required="required" />
				</td>
				<td class="desc">-</td>
				<td><input type="text" name="cant[]" class="prod_cant inputtext" size="5" readonly="readonly" /></td>
				<td><input type="text" name="precio[]" value="0" class="pr1 inputtext" size="5" readonly="readonly" /></td>
				<td class="to1">0</td>
				<td><input type="text" value="0" class="pr2 inputtext" size="5" readonly="readonly" /></td>
				<td class="to2">0</td>
			</tr>
		</tbody>
	</table>
		
	<div class="center">
		<input type="button" class="addbtn" id="prod_add" value="Agregar otro producto" />
	</div>
		
		</div>
		
		</fieldset>

<?php
	echo("<div class='center'>");
	$form->Textarea("Observaciones","nv_observacion");
	echo("<hr />");
	$form->File("Orden de compra adjunta<br />(Formatos soportados: pdf, xls, xlsx, jpg, bmp, doc, png, gif, odf)","nv_orden_compra");
	echo("</div>");
	$form->Hidden("cli_id",$datos['dc_cliente']);
	$form->End("Crear","addbtn");
?>
<iframe name="cr_notaventa_fres" id="cr_notaventa_fres" style="display:none;"></iframe>
</div>
</div>
<script type="text/javascript" src="jscripts/products_manager.js"></script>
<script type="text/javascript">
$("#cr_notaventa").attr("target","cr_notaventa_fres");
$("#cr_notaventa").submit(function(e){
	if(validarForm("#cr_notaventa")){	
		disableForm("#cr_notaventa");
	}else{
		e.preventDefault();
	}
});
$("#cr_notaventa_fres").load(function(){
	res = frames['cr_notaventa_fres'].document.getElementsByTagName("body")[0].innerHTML;
	$("#cr_notaventa_res").html(res);
});
</script>