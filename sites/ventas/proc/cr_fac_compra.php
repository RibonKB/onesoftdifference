<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("tb_proveedor","dc_proveedor,dg_razon","dg_rut='{$_POST['prov_rut']}' AND dc_empresa={$empresa} AND dm_activo='1'");
if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el proveedor especificado, intentelo nuevamente.","sites/ventas/cr_fac_compra.php");
}
$datos=$datos[0];

include_once("../../../inc/form-class.php");
$form = new Form($empresa);

echo("
<div id='secc_bar'>Ingreso Facturas de compra</div>
<div id='main_cont'>
<div class='panes'>
<div class='title center'>Factura de compra para <strong>{$datos['dg_razon']}</strong></div>
<hr />
");

$form->Header("<strong>Indique los datos para el ingreso de la facturas de compra</strong><br />Los datos marcados con [*] son obligatorios");
$form->Start("sites/ventas/proc/crear_fac_compra.php","cr_factura_compra");
$form->Section();
$form->Text("Número de factura","fac_numero",1);
$form->Date("Fecha emisión","fac_fecha",1,date("d/m/Y"));
$form->EndSection();
$form->Section();
$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio","dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |{$c['dq_cambio']}|";
	}
$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1);
$form->Text("Orden de compra","fac_ocompra",1);
$form->EndSection();

?>
<br class="clear" />
<fieldset>
<legend>Productos en la factura</legend>
<div class="info" id="prod_info">Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la factura</div>
<div id="prods" style="display:none;">
<table width="100%" class="tab">
	<thead>
		<tr>
			<th width="13%">Código (Autocompleta)</th>
			<th width="33%">Nombre</th>
			<th width="10%">Cantidad</th>
			<th width="10%">Precio (Base)</th>
			<th width="12%">Total (Base)</th>
			<th width="10%">Precio (T/C)</th>
			<th width="12%">Total (T/C)</th>
		</tr>
	</thead>
	<tbody id="prod_list">
		<tr>
			<td style="position:absolute;">
			<input type="text" name="prod[]" class="prod_codigo inputtext" size="7" required="required" />
			</td>
			<td class="desc">-</td>
			<td><input type="text" name="cant[]" class="prod_cant inputtext" size="5" readonly="readonly" /></td>
			<td><input type="text" name="precio[]" value="0" class="pr1 inputtext" size="5" readonly="readonly" /></td>
			<td class="to1">0</td>
			<td><input type="text" value="0" class="pr2 inputtext" size="5" readonly="readonly" /></td>
			<td class="to2">0</td>
		</tr>
	</tbody>
</table>
<div class="center">
	<input type="button" class="addbtn" id="prod_add" value="Agregar otro producto" />
</div>
</div>
</fieldset>
<?php

$form->Hidden("prov_id",$datos['dc_proveedor']);
$form->End("Ingresar","addbtn");

echo("
</div>
</div>");

?>
<script type="text/javascript" src="jscripts/products_manager.js"></script>