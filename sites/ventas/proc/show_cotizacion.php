<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_cotizacion WHERE dc_empresa={$empresa} AND dc_cotizacion = {$_POST['id']}) cot
LEFT JOIN tb_termino_comercial term ON term.dc_termino_comercial = cot.dc_termino_comercial
LEFT JOIN tb_contacto_cliente c ON c.dc_contacto = cot.dc_contacto
	LEFT JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
	LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
	LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = cot.dc_tipo_cambio
LEFT JOIN tb_modo_envio me ON me.dc_modo_envio = cot.dc_modo_envio
LEFT JOIN tb_cliente cl ON cl.dc_cliente = cot.dc_cliente
LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = cot.dc_ejecutivo
LEFT JOIN tb_funcionario tv ON tv.dc_funcionario = cot.dc_ejecutivo_tv
LEFT JOIN tb_cotizacion_estado ce ON ce.dc_estado = cot.dc_estado",
"cl.dc_cliente,cot.dc_cotizacion,cot.dq_cotizacion,term.dg_termino_comercial,s.dg_direccion,com.dg_comuna,reg.dg_region,tc.dg_tipo_cambio,
tc.dn_cantidad_decimales,cot.dq_cambio,me.dg_modo_envio,cl.dg_razon,cl.dg_rut,cot.dq_iva,cot.dq_neto,c.dg_contacto,
DATE_FORMAT(cot.df_fecha_emision,'%d/%m/%Y') as df_emision,DATE_FORMAT(cot.df_fecha_envio,'%d/%m/%Y') as df_envio,s.dg_sucursal,
(cot.dq_iva+cot.dq_neto) as dq_total,cot.dg_observacion,CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno,ex.dg_ap_materno) as dg_ejecutivo,
CONCAT_WS(' ',tv.dg_nombres,tv.dg_ap_paterno,tv.dg_ap_materno) as dg_televenta,cot.dc_estado,ce.dg_estado");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la cotización especificada");
	exit();
}
$data = $data[0];
$cambio = moneda_local($data['dq_cambio']);

echo("<div class='title center'>Cotización nº {$data['dq_cotizacion']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong>({$data['dg_rut']}) {$data['dg_razon']}</strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td>{$data['df_emision']}</td>
	<td rowspan='9' width='250'>
		<label>Estado de cotización</label><br />
		<h3 class='info' style='margin:0'>{$data['dg_estado']}</h3>
	</td>
</tr><tr>
	<td>Contacto</td>
	<td><b>{$data['dg_contacto']}</b> - {$data['dg_sucursal']}</td>
</tr><tr>
	<td>Domicilio</td>
	<td><b>{$data['dg_direccion']}</b> {$data['dg_comuna']} <label>{$data['dg_region']}</label></td>
</tr><tr>
	<td>Tipo de cambio</td>
	<td><b>{$data['dg_tipo_cambio']}</b> ({$cambio})</td>
</tr><tr>
	<td>Ejecutivo</td>
	<td><b>{$data['dg_ejecutivo']}</b></td>
</tr><tr>
	<td>Ejecutivo televenta</td>
	<td><b>{$data['dg_televenta']}</b></td>
</tr><tr>
	<td>Término Comercial</td>
	<td>{$data['dg_termino_comercial']}</td>
</tr><tr>
	<td>Modo de envio</td>
	<td>{$data['dg_modo_envio']}</td>
</tr><tr>
	<td>Fecha de envio</td>
	<td>{$data['df_envio']}</td>
</tr><tr>
	<td>Observación</td>
	<td>{$data['dg_observacion']}</td>
</tr></table>");

$detalle = $db->select("(SELECT * FROM tb_cotizacion WHERE dc_cotizacion = {$data['dc_cotizacion']}) cot
JOIN(SELECT * FROM tb_cotizacion_detalle WHERE dc_cotizacion={$data['dc_cotizacion']}) d ON d.dc_cotizacion = cot.dc_cotizacion
LEFT JOIN (SELECT * FROM tb_proveedor WHERE dc_empresa={$empresa}) p ON d.dc_proveedor = p.dc_proveedor",
'dq_cantidad,d.dg_descripcion,(d.dq_precio_venta/cot.dq_cambio) as dq_precio_venta,p.dg_razon,(d.dq_precio_venta/cot.dq_cambio*d.dq_cantidad) as dq_total,(d.dq_precio_compra/cot.dq_cambio) as dq_precio_compra');

echo("<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
<tr>
	<th>Cantidad</th>
	<th>Descripción</th>
	<th>Proveedor</th>
	<th>Costo</th>
	<th>Total Costo</th>
	<th>Precio</th>
	<th>Total</th>
	<th>Margen</th>
	<th>Margen (%)</th>
</tr>
</thead>
<tbody>");
$data['dq_total_costo'] = 0;
$data['dq_neto'] = 0;
foreach($detalle as $d){
	$data['dq_total_costo'] += $d['dq_precio_compra']*$d['dq_cantidad'];
	$data['dq_neto'] += $d['dq_precio_venta']*$d['dq_cantidad'];
	$costo_total = moneda_local($d['dq_precio_compra']*$d['dq_cantidad']);
	$venta_total = moneda_local($d['dq_precio_venta']*$d['dq_cantidad']);
	$d['dq_cantidad'] = number_format($d['dq_cantidad'],2,$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
	$d['dq_margen'] = moneda_local(($d['dq_precio_venta']*$d['dq_cantidad'])-($d['dq_precio_compra']*$d['dq_cantidad']),$data['dn_cantidad_decimales']);
	if($d['dq_precio_venta'] != 0)
		$d['dq_margen_p'] = moneda_local(($d['dq_precio_venta']-$d['dq_precio_compra'])/$d['dq_precio_venta'],2);
	else
		$d['dq_margen_p'] = '-';
	$d['dq_precio_venta'] = moneda_local($d['dq_precio_venta'],$data['dn_cantidad_decimales']);
	$d['dq_precio_compra'] = moneda_local($d['dq_precio_compra'],$data['dn_cantidad_decimales']);
	$d['dq_total'] = moneda_local($d['dq_total'],$data['dn_cantidad_decimales']);
	echo("<tr>
		<td>{$d['dq_cantidad']}</td>
		<td align='left'>{$d['dg_descripcion']}</td>
		<td align='left'>{$d['dg_razon']}</td>
		<td align='right'>{$d['dq_precio_compra']}</td>
		<td align='right'>{$costo_total}</td>
		<td align='right'>{$d['dq_precio_venta']}</td>
		<td align='right'>{$venta_total}</td>
		<td align='right'>{$d['dq_margen']}</td>
		<td align='right'>{$d['dq_margen_p']}</td>
	</tr>");
}

$data['dq_margen'] = moneda_local(($data['dq_neto']-$data['dq_total_costo'])*$data['dq_cambio']);
$data['dq_margen_cambio'] = moneda_local($data['dq_neto']-$data['dq_total_costo']);
if($data['dq_neto'] > 0)
	$data['dq_margen_p'] = moneda_local(($data['dq_neto']-$data['dq_total_costo'])*100/$data['dq_neto'],2);
else
	$data['dq_margen_p'] = '-';

$total = moneda_local($data['dq_neto'],$data['dn_cantidad_decimales']);
$data['dq_neto'] = moneda_local($data['dq_neto']*$data['dq_cambio']);
$data['dq_iva'] = moneda_local($data['dq_iva']);
$data['dq_total'] = moneda_local($data['dq_total']);

echo("
</tbody>
<tfoot>
<tr>
	<th colspan='6' align='right'>Total ({$data['dg_tipo_cambio']})</th>
	<th align='right'>{$total}</th>
	<th colspan='2'>&nbsp;</th>
</tr>
<tr>
	<th colspan='6' align='right'>Total Neto</th>
	<th align='right'>{$data['dq_neto']}</th>
	<th colspan='2'>&nbsp;</th>
</tr>
<tr>
	<th colspan='6' align='right'>IVA</th>
	<th align='right'>{$data['dq_iva']}</th>
	<th colspan='2'>&nbsp;</th>
</tr>
<tr>
	<th colspan='6' align='right'>Total a Pagar</th>
	<th align='right'>{$data['dq_total']}</th>
	<th colspan='2'>&nbsp;</th>
</tr>
<tr>
	<th colspan='6' align='right'>Margen Total</th>
	<th align='right'>{$data['dq_margen']} ({$data['dq_margen_p']} %)</th>
	<th colspan='2'>&nbsp;</th>
</tr>
<tr>
	<th colspan='6' align='right'>Margen Total ({$data['dg_tipo_cambio']})</th>
	<th align='right'>{$data['dq_margen_cambio']}</th>
	<th colspan='2'>&nbsp;</th>
</tr>
</tfoot>");

?>
<script type="text/javascript">
$('#print_version').unbind('click');
$('#print_version').click(function(){
	window.open("sites/ventas/ver_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>",'print_cotizacion','width=800;height=600');
});
$('#edit_cotizacion').unbind('click');
$('#edit_cotizacion').click(function(){
	$('#res_list').slideUp();
	$('.panes').css({marginLeft:'20px'});
	loadFile("sites/ventas/ed_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>",'#show_cotizacion','',globalFunction);
});
$('#gestion_cotizacion').unbind('click');
$('#gestion_cotizacion').click(function(){
	loadOverlay("sites/ventas/src_gestion_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>");
});
$('#historial_actividad').unbind('click');
$('#historial_actividad').click(function(){
	loadOverlay("sites/ventas/history_gestion_cotizacion.php?id=<?=$data['dc_cotizacion'] ?>");
});
$('#crear_nota_venta').unbind('click');
$('#crear_nota_venta').click(function(){
	loadpage("sites/ventas/nota_venta/proc/cr_nota_venta.php?cli_id=<?=$data['dc_cliente'] ?>&cot_numero=<?=$data['dq_cotizacion'] ?>");
});
</script>