<?php
/**
*	Ingresa una nueva cotización
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$conf = $db->select("tb_empresa_configuracion","dq_iva","dc_empresa={$empresa}");
$conf = $conf[0];

$fac_tcambio = $db->select("tb_tipo_cambio","dq_cambio","dc_tipo_cambio = {$_POST['prod_tipo_cambio']}");
$fac_emision = explode("/",$_POST['fac_fecha']);
$fac_emision = count($fac_emision)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$fac_emision[1],$fac_emision[0],$fac_emision[2]))."'":"0";

$factura = $db->insert("tb_factura_compra",
array(
	"dg_factura" => $_POST['fac_numero'],
	"dc_proveedor" => $_POST['prov_id'],
	"dc_empresa" => $empresa,
	"dq_iva" => $conf['dq_iva'],
	"dg_orden_compra" => $_POST['fac_ocompra'],
	"df_fecha_emision" => $fac_emision
));

foreach($_POST['prod'] as $i => $v){
	$db->query("
	INSERT INTO tb_factura_compra_detalle (dc_empresa,dc_factura,dq_cantidad,dc_producto,dq_precio)
	SELECT {$empresa},{$factura},{$_POST['cant'][$i]},p.dc_producto,{$_POST['precio'][$i]}
	FROM tb_producto p
	WHERE p.dg_codigo = '{$v}' AND p.dc_empresa={$empresa}");
}

$cant = $db->select("tb_factura_compra_detalle","count(dc_factura) as cant","dc_factura = {$factura}");

$error_man->showConfirm("Se ha generado la factura correctamente y se le han agregado {$cant[0]['cant']} productos al detalle");

?>