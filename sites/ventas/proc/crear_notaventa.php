<?php
/**
*	Ingresa una nueva cotización
**/
define("MAIN",1);
require_once("../../../inc/global.php");

	//Se cimprueba que el archivo temporal se haya subido correctamente y que sea del tipo de archivo correcto
	switch($_FILES['nv_orden_compra']['error']){
		case 1:
		case 2: $error_man->showWarning("El tamaño del archivo supera el máximo permitido.");
				exit();
				break;
		case 3: $error_man->showWarning("Se ha cancelado la transferencia del archivo.");
				exit();
				break;
	}
	
	$tipos = array(
		'image/x-windows-bmp','image/bmp','image/gif','image/pjpeg',
		'image/jpeg','image/jpg','image/x-png','image/png','image/gi_',
		'application/png','application/x-png','image/jp_',
		'application/x-jpg','image/pipeg','image/vnd.swiftview-jpeg',
		'image/x-xbitmap','image/x-bitmap','image/x-win-bitmap',
		'image/x-windows-bmp','image/ms-bmp','image/x-ms-bmp','application/bmp',
		'application/x-bmp','application/x-win-bitmap','application/pdf',
		'application/x-pdf','application/acrobat','applications/vnd.pdf',
		'text/pdf','text/x-pdf','application/excel','application/vnd.ms-excel',
		'application/x-excel','application/x-msexcel','application/msexcel',
		'application/x-ms-excel','application/x-dos_ms_excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/msword','application/doc','appl/text','application/vnd.msword',
		'application/vnd.ms-word','application/winword','application/word',
		'application/x-msw6','application/x-msword','vnd.oasis.opendocument.spreadsheet',
		'vnd.oasis.opendocument.spreadsheet-template','vnd.oasis.opendocument.text',
		'vnd.oasis.opendocument.text-master','vnd.oasis.opendocument.text-template',
		'vnd.oasis.opendocument.text-web'
	);
	
	if($_FILES['nv_orden_compra']['size']){
		if(!in_array($_FILES['nv_orden_compra']['type'],$tipos)){
			$error_man->showWarning("No se permite subir el tipo de archivo especificado para la orden de compra<br />
			Los formatos soportados son .pdf .xls .xlsx .jpg .bmp .png .gif y .doc");
			exit();
		}
		$oo_ext = substr($_FILES['nv_orden_compra']['name'],strrpos($_FILES['nv_orden_compra']['name'],"."));
	}
	//Fin de la comprobación del archivo

$conf = $db->select("tb_empresa_configuracion","dc_correlativo_nota_venta,dg_correlativo_nota_venta,dq_iva","dc_empresa={$empresa}");
$conf = $conf[0];

$nv_prefijo = "";
switch($conf['dc_correlativo_nota_venta']){
	case '1': $nv_prefijo = date("Y"); break;
	case '2': $nv_prefijo = date("Ym"); break;
}

$last = $db->select("tb_nota_venta","dg_nota_venta",
"dc_empresa = {$empresa} AND
df_fecha_emision in (SELECT MAX(df_fecha_emision) FROM tb_nota_venta WHERE dc_empresa = {$empresa})");

if(count($last)){
	if(substr($nv_prefijo,0,4) == substr($last[0]['dg_nota_venta'],0,4) || $conf['dc_correlativo_nota_venta'] == '3'){
		$nv_num = str_replace($nv_prefijo,"",$last[0]['dg_nota_venta']);
		$nv_num++;
	}else{
		$nv_num = $conf['dg_correlativo_nota_venta'];
	}
}else{
	$nv_num = $conf['dg_correlativo_nota_venta'];
}

$nv_tcambio = $db->select("tb_tipo_cambio","dq_cambio","dc_tipo_cambio = {$_POST['prod_tipo_cambio']}");
$nv_envio = explode("/",$_POST['nv_envio']);
$nv_envio = count($nv_envio)==3?"'".date("Y-m-d H:i:s",mktime(0,0,0,$nv_envio[1],$nv_envio[0],$nv_envio[2]))."'":"0";

$nota_venta = $db->insert("tb_nota_venta",
array(
	"dc_termino_comercial" => $_POST['nv_termino'],
	"dc_contacto" => $_POST['nv_contacto'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"dc_tipo_cambio" => $_POST['prod_tipo_cambio'],
	"dq_cambio" => $nv_tcambio[0]['dq_cambio'],
	"dc_modo_envio" => $_POST['nv_modo_envio'],
	"dc_cliente" => $_POST['cli_id'],
	"dg_nota_venta" => $nv_prefijo.$nv_num,
	"df_fecha_emision" => "NOW()",
	"df_fecha_envio" => $nv_envio,
	"dq_iva" => $conf['dq_iva'],
	"dg_observacion" => substr($_POST['nv_observacion'],0,254)
));

if(isset($_POST['contacto_extra'])){
	foreach($_POST['contacto_extra'] as $i => $v){
		if($v != '0'){
			$db->insert("tb_nota_venta_contactos_extra_nota_venta",
			array(
				"dc_nota_venta" => $nota_venta,
				"dc_contacto" => $v,
				"dc_contacto_extra" => $_POST['tipo_cextra'][$i]
			));
		}
	}
}

foreach($_POST['prod'] as $i => $v){
	$db->query("
	INSERT INTO tb_nota_venta_detalle (dc_empresa,dc_nota_venta,dq_cantidad,dc_producto,dq_precio)
	SELECT {$empresa},{$nota_venta},{$_POST['cant'][$i]},p.dc_producto,{$_POST['precio'][$i]}
	FROM tb_producto p
	WHERE p.dg_codigo = '{$v}' AND p.dc_empresa={$empresa}");
}

	if($_FILES['nv_orden_compra']['size']){
		if(!move_uploaded_file($_FILES['nv_orden_compra']['tmp_name'],"../orden_compra/oo_{$nota_venta}{$oo_ext}"))
		{
			$error_man->showWarning("No se pudo subir la orden de compra");
		}
	}

$cant = $db->select("tb_nota_venta_detalle","count(dc_nota_venta) as cant","dc_nota_venta = {$nota_venta}");

$error_man->showConfirm("
Se ha generado la nota de venta correctamente y se le han agregado {$cant[0]['cant']} productos al detalle<br />
<a href=\"#\" onclick=\"window.open('sites/ventas/ver_nota_venta.php?id={$nota_venta}','nota_venta','width=800,height=600')\"> Haga clic aquí para ver la nota de venta</a>");

?>