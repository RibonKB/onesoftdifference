<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->escape($_POST['id']);

$data = $db->select('tb_nota_venta','dq_nota_venta',"dc_nota_venta = {$_POST['id']}");

if(!count($data)){
	$error_man->showWarning('El número de nota de venta especificado es inválido');
	exit;
}

$dq_nota_venta = $data[0]['dq_nota_venta'];
unset($data);

echo('<div class="secc_bar">Anular nota de venta</div><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/nota_venta/proc/null_nota_venta.php','null_nv_form');
$form->Header('Indique las razones por las que se anulará la nota de venta');
$error_man->showAviso("Atención, está a punto de anular la nota de venta N° <b>{$dq_nota_venta}</b>");
$form->Section();
$form->Listado('Motivo de anulación','dc_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','dg_comentario',1);
$form->EndSection();
$form->Hidden('dc_nota_venta',$_POST['id']);
$form->End('Anular','delbtn');

?>