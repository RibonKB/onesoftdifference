<?php
require (__DIR__ . '/PlantillaPDF.php');
class PDF extends PlantillaPDF{

	private $datosEmpresa;

	public function  __construct(){
		parent::__construct();
		global $empresa,$db;
		$datosE = $db->select(
		"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
		"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
		"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
		$this->datosEmpresa = $datosE[0];
		unset($datosE);
	}

	function Header(){
		global $datosNV;
		
		$this->SetFont('Arial','',12);
		$this->SetDrawColor(0,100,0);
		$this->SetTextColor(0,100,0);
		$this->SetX(160);
		$this->MultiCell(50,9,"R.U.T. {$this->datosEmpresa['dg_rut']}\nNOTA DE VENTA\nNo {$datosNV['dq_nota_venta']}",1,'C');
		$this->SetY(10);
		$this->SetTextColor(0);
		
		$this->SetFont('','B',12);
		$this->Cell(145,5,$this->datosEmpresa['dg_razon']);
		$this->ln();
		$this->SetFont('Arial','',7);
		$this->MultiCell(120,2.4,"{$this->datosEmpresa['dg_giro']}\n{$this->datosEmpresa['dg_direccion']}, {$this->datosEmpresa['dg_comuna']} {$this->datosEmpresa['dg_region']}\n{$this->datosEmpresa['dg_fono']}");
		$this->Ln(3);
		
		$this->SetDrawColor(150,150,150);
		$this->Cell(0,5,'','B');
		$this->Ln();
		$y = $this->GetY();
		$this->SetFontSize(5);
		$this->MultiCell(14,7,"FECHA:\nSE�ORES:\nDIRECCI�N:\nGIRO: ",'L');
		$this->Cell(0,5,'','T');
		$this->SetY($y);
		$this->SetX(118);
		$this->MultiCell(14,7," RUT:\nCONTACTO:\nCOMUNA:");
		$this->SetY($y);
		$this->SetX(22);
		$this->SetFontSize(11);
		$this->MultiCell(120,7,"{$datosNV['df_fecha_emision']}\n{$datosNV['dg_razon']}\n{$datosNV['dg_direccion']}\n{$datosNV['dg_giro']}");
		$this->SetY($y);
		$this->SetX(130);
		$this->MultiCell(76,7," {$datosNV['dg_rut']}\n{$datosNV['dg_contacto']}\n{$datosNV['dg_comuna']}\n ","R");
		$this->Ln(3);
		
		$this->SetFillColor(210,210,210);
		$this->SetFontSize(9);
		$y = $this->GetY();
		
		$this->SetX(30);
		$this->Cell(40,7,"Tipo Cambio",'B',0,'L',1);
		$this->SetX(30);
		$this->Cell(40,7,$this->datosEmpresa['dg_moneda_local'],0,1,'R');
		$this->SetX(30);
		$this->Cell(40,5,$datosNV['dg_tipo_cambio'],0,0,'L');
		$this->SetX(30);
		$this->Cell(40,5,$datosNV['dq_cambio'],0,0,'R');
		$this->SetY($y);
		
		$this->SetX(75);
		$this->Cell(46,7,"Responsable",'B',2,'C',1);
		$this->Cell(46,7,$datosNV['dg_ejecutivo'],0,0,'R');
		$this->SetY($y);
		
		$this->SetX(130);
		$this->Cell(46,7,"T�rmino comercial",'B',2,'C',1);
		$this->Cell(46,7,$datosNV['dg_termino_comercial'],0,0,'R');
		$this->SetY($y+15);
		
		$w = array(25,91,40,40);
		$head = array("CANTIDAD","DESCRIPCION","PRECIO","TOTAL");
		$this->SetFontSize(7);
		for($i=0;$i<4;$i++){
			$this->Cell($w[$i],6,$head[$i],1,0,'L',1);
		}
		$this->Ln();
		
	}
}
?>