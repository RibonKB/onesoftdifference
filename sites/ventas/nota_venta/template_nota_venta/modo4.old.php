<?php
require("../../../inc/fpdf.php");

class PDF extends FPDF{

private $datosEmpresa;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','mm','Letter');
	
}

function Header(){
	global $datosNV;
	
	$this->SetFont('Arial','',12);
	$this->SetDrawColor(0,100,0);
	$this->SetTextColor(0,100,0);
	$this->SetX(160);
	$this->MultiCell(50,9,"R.U.T. {$this->datosEmpresa['dg_rut']}\nNOTA DE VENTA\nNo {$datosNV['dq_nota_venta']}",1,'C');
	$this->SetY(10);
	$this->SetTextColor(0);
	
	$this->SetFont('','B',12);
	$this->Cell(145,5,$this->datosEmpresa['dg_razon']);
	$this->ln();
	$this->SetFont('Arial','',7);
	$this->MultiCell(120,2.4,"{$this->datosEmpresa['dg_giro']}\n{$this->datosEmpresa['dg_direccion']}, {$this->datosEmpresa['dg_comuna']} {$this->datosEmpresa['dg_region']}\n{$this->datosEmpresa['dg_fono']}");
	$this->Ln(12);
	
	$this->SetDrawColor(150,150,150);
	$this->Cell(0,5,'','B');
	$this->Ln();
	$y = $this->GetY();
	$this->SetFontSize(5);
	$this->MultiCell(14,7,"FECHA:\nSE�ORES:\nDIRECCI�N:\nGIRO: ",'L');
	$this->Cell(0,5,'','T');
	$this->SetY($y);
	$this->SetX(118);
	$this->MultiCell(14,7," \nCONTACTO:\nCOMUNA:");
	$this->SetY($y);
	$this->SetX(22);
	$this->SetFontSize(11);
	$this->MultiCell(120,7,"{$datosNV['df_fecha_emision']}\n{$datosNV['dg_razon']}\n{$datosNV['dg_direccion']}\n{$datosNV['dg_giro']}");
	$this->SetY($y);
	$this->SetX(130);
	$this->MultiCell(76,7," \n{$datosNV['dg_contacto']}\n{$datosNV['dg_comuna']} \n ","R");
	$this->Ln(3);
	
	$this->SetFillColor(210,210,210);
	$this->SetFontSize(9);
	$y = $this->GetY();
	
	$this->SetX(50);
	$this->Cell(40,7,"Tipo Cambio",'B',0,'L',1);
	$this->SetX(50);
	$this->Cell(40,7,$this->datosEmpresa['dg_moneda_local'],0,1,'R');
	$this->SetX(50);
	$this->Cell(40,5,$datosNV['dg_tipo_cambio'],0,0,'L');
	$this->SetX(50);
	$this->Cell(40,5,$datosNV['dq_cambio'],0,0,'R');
	$this->SetY($y);
	
	$this->SetX(95);
	$this->Cell(46,7,"Responsable",'B',2,'C',1);
	$this->Cell(46,7,$datosNV['dg_ejecutivo'],0,0,'R');
	$this->SetY($y+15);
	
	$w = array(25,91,40,40);
	$head = array("CANTIDAD","DESCRIPCION","PRECIO","TOTAL");
	$this->SetFontSize(7);
	for($i=0;$i<4;$i++){
		$this->Cell($w[$i],6,$head[$i],1,0,'L',1);
	}
	$this->Ln();
	
}

function Footer()
{
	global $datosCliente;
	$pie = explode("\n",$this->datosEmpresa['dg_pie_cotizacion']);
    //Posici?n: a 1,5 cm del final
    $this->SetY((count($pie)*-3)-20);
    //Arial italic 8
    $this->SetFont('Arial','I',7);
    //N?mero de p?gina
	$this->AliasNbPages();
	$this->MultiCell(150,3,$this->datosEmpresa['dg_pie_cotizacion']);
	$this->SetY(-15);
    $this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'R');
}

function AddDetalle($detalle){
	global $datosNV,$empresa_conf;
	
	$this->AddPage();
	$this->SetFont('Times','',9);
	$this->SetDrawColor(150,150,150);
	$this->SetFillColor(240,240,240);
	$fill = false;
	foreach($detalle as $i => $det){
		$this->Cell(25,6,moneda_local($det['dq_cantidad']),'LR',0,'R',$fill);
		$this->Cell(91,6,$det['dg_descripcion'],'LR',0,'L',$fill);
		$this->Cell(40,6,moneda_local($det['dq_precio'],$datosNV['dn_cantidad_decimales']),'LR',0,'R',$fill);
		$this->Cell(40,6,moneda_local($det['dq_total'],$datosNV['dn_cantidad_decimales']),'LR',0,'R',$fill);
		$this->Ln();
		$fill = !$fill;
		if(($i+1)%10 == 0){
			$this->Cell(0,6,'','T');
			$this->AddPage();
		}
	}
	for($i=0;$i<(10 - count($detalle)%10);$i++){
		$this->Cell(25,6,"",'LR',0,'R',$fill);
		$this->Cell(91,6,"",'LR',0,'L',$fill);
		$this->Cell(40,6,"",'LR',0,'R',$fill);
		$this->Cell(40,6,"",'LR',0,'R',$fill);
		$this->Ln();
		$fill = !$fill;
	}
	$this->Cell(0,6,'','T');
	$this->Ln(0);
	
	$y = $this->GetY();
	if($datosNV['dg_observacion']){
		$this->SetFont('Arial','',7);
		$this->MultiCell(116,3,"Observaciones: \n".$datosNV['dg_observacion'],1);
	}
	
	$this->SetFillColor(200,200,200);
	$this->SetY($y);
	$this->SetX(126);
	$this->SetFont('','B');
	$this->Cell(40,5,"TOTAL ({$datosNV['dg_tipo_cambio']})",1,2,'R',1);
	$this->Cell(40,5,"TOTAL NETO",1,2,'R',1);
	$this->Cell(40,5,"I.V.A.",1,2,'R',1);
	$this->Cell(40,5,"TOTAL PAGAR",1,2,'R',1);
	
	$this->SetY($y);
	$this->SetX(166);
	$this->SetFont('Times','',9);
	$this->Cell(40,5,moneda_local($datosNV['dq_total'],$datosNV['dn_cantidad_decimales']),1,2,'R');
	$this->Cell(40,5,moneda_local($datosNV['dq_neto']),1,2,'R');
	$this->Cell(40,5,moneda_local($datosNV['dq_iva']),1,2,'R');
	$this->Cell(40,5,moneda_local($datosNV['dq_neto']+$datosNV['dq_iva']),1,2,'R');
	
}

}
?>