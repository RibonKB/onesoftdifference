<?php
require("../../../inc/fpdf.php");

class PlantillaPDF extends FPDF{
	private $datosEmpresa;

	public function  __construct(){
		parent::__construct('P','mm','Letter');
		global $empresa,$db;
		
		$datosE = $db->select(
		"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
		"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
		"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
		
		$this->datosEmpresa = $datosE[0];
		unset($datosE);
	}
	
	public function addDetalle($detalle){
		global $datosNV,$empresa_conf;
	
		$this->AddPage();
		$this->SetFont('Times','',9);
		$this->SetDrawColor(150,150,150);
		$this->SetFillColor(240,240,240);
		$fill = false;
		foreach($detalle as $i => $det){
			$this->Cell(25,6,moneda_local($det['dq_cantidad']),'LR',0,'R',$fill);
			$this->Cell(91,6,substr($det['dg_descripcion'],0,50),'LR',0,'L',$fill);
			$this->Cell(40,6,moneda_local($det['dq_precio'],$datosNV['dn_cantidad_decimales']),'LR',0,'R',$fill);
			$this->Cell(40,6,moneda_local($det['dq_total'],$datosNV['dn_cantidad_decimales']),'LR',0,'R',$fill);
			$this->Ln();
			$fill = !$fill;
			if(($i+1)%20 == 0){
				$this->Cell(0,6,'','T');
				$this->AddPage();
			}
		}
		for($i=0;$i<(20 - count($detalle)%20);$i++){
			$this->Cell(25,6,"",'LR',0,'R',$fill);
			$this->Cell(91,6,"",'LR',0,'L',$fill);
			$this->Cell(40,6,"",'LR',0,'R',$fill);
			$this->Cell(40,6,"",'LR',0,'R',$fill);
			$this->Ln();
			$fill = !$fill;
		}
		$this->Cell(0,6,'','T');
		$this->Ln(0);
		
		$y = $this->GetY();
		if($datosNV['dg_observacion']){
			$this->SetFont('Arial','',7);
			$this->MultiCell(116,3,"Observaciones: \n".$datosNV['dg_observacion'],1);
		}
		
		$this->SetFillColor(200,200,200);
		$this->SetY($y);
		$this->SetX(126);
		$this->SetFont('','B');
		$this->Cell(40,5,"TOTAL ({$datosNV['dg_tipo_cambio']})",1,2,'R',1);
		$this->Cell(40,5,"TOTAL NETO",1,2,'R',1);
		$this->Cell(40,5,"I.V.A.",1,2,'R',1);
		$this->Cell(40,5,"TOTAL PAGAR",1,2,'R',1);
		
		$this->SetY($y);
		$this->SetX(166);
		$this->SetFont('Times','',9);
		$this->Cell(40,5,moneda_local($datosNV['dq_total'],$datosNV['dn_cantidad_decimales']),1,2,'R');
		$this->Cell(40,5,moneda_local($datosNV['dq_neto']),1,2,'R');
		$this->Cell(40,5,moneda_local($datosNV['dq_iva']),1,2,'R');
		$this->Cell(40,5,moneda_local($datosNV['dq_neto']+$datosNV['dq_iva']),1,2,'R');
	}
	
	public function setMargenes($detalles){
		global $datosNV,$empresa_conf;
		$precioVenta = 0;
		$precioCosto = 0;
		foreach($detalles as $v){
			$precioVenta += floatval($v['dq_precio_venta']) * $v['dq_cantidad'];
			$precioCosto += floatval($v['dq_precio_compra'])  * $v['dq_cantidad'];
		}
		
		$margen = $precioVenta - $precioCosto;
		$costoTipoCambio = floatval($precioCosto / $datosNV['dq_cambio']);
		$margenTipoCambio = floatval($margen / $datosNV['dq_cambio']);
		$porcentajeMargen = round(($margen / $precioVenta) * 100, 2);
		
		$y = $this->getY();
		
		$this->SetFillColor(200,200,200);
		$this->SetY($y);
		$this->SetX(126);
		$this->SetFont('','B');
		
		$this->Cell(40,5,"COSTO TOTAL",1,2,'R',1);
		$this->Cell(40,5,"COSTO TOTAL ({$datosNV['dg_tipo_cambio']})",1,2,'R',1);
		$this->Cell(40,5,"MARGEN",1,2,'R',1);
		$this->Cell(40,5,"MARGEN ({$datosNV['dg_tipo_cambio']})",1,2,'R',1);
		$this->Cell(40,5,"PORCENTAJE MARGEN",1,2,'R',1);
		
		$this->SetY($y);
		$this->SetX(166);
		$this->SetFont('Times','',9);
		
		$this->Cell(40,5,moneda_local($precioCosto, $datosNV['dn_cantidad_decimales']),1,2,'R');
		$this->Cell(40,5,moneda_local($costoTipoCambio, $datosNV['dn_cantidad_decimales']),1,2,'R');
		$this->Cell(40,5,moneda_local($margen, $datosNV['dn_cantidad_decimales']),1,2,'R');
		$this->Cell(40,5,moneda_local($margenTipoCambio, $datosNV['dn_cantidad_decimales']),1,2,'R');
		$this->Cell(40,5,moneda_local($porcentajeMargen, $datosNV['dn_cantidad_decimales']) . '%',1,2,'R');
	}
} 