<?php
define("MAIN", 1);
require_once("../../../inc/init.php");
require_once("../../../inc/form-class.php");

$dc_tipo_venta = $_POST['dc_tipo_venta'];

$tipo_venta = $db->getRowById('tb_tipo_nota_venta', $dc_tipo_venta, 'dc_tipo');

if ($tipo_venta === false) {
  $error_man->showWarning('El tipo de venta es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
  exit;
}

if ($tipo_venta->dm_costeable == 0) {
  $error_man->showWarning('El tipo de venta no admite costeos, compruebe los datos de entrada y vuelva a intentarlo');
  exit;
}

$form = new Form($empresa);
?>
<div class="secc_bar">
  Crear Contrato
</div>
<div class="panes">
<?php $form->Start('#', 'cr_proyecto_form', 'proyectoForm') ?>
  <?php
  $form->Section();
    $form->Text('Nombre de Proyecto', 'dg_nombre_proyecto', 1);
    $form->DBSelect('Encargado', 'dc_encargado_proyecto', 'tb_funcionario', array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'), true);
  $form->EndSection();
  $form->Section();
    $form->Date('Fecha inicio', 'df_inicio_proyecto', true);
    $form->Date('Fecha Término', 'df_fin_proyecto', true);
  $form->EndSection();
  ?>
  <?php $form->End('Asignar Proyecto', 'addbtn') ?>
</div>
<script type="text/javascript">
  cr_proyecto.initCreation();
</script>