<?php
class InformeArriboRenovacionFactory extends Factory{
    protected $title = 'Informe de Arribo/Renovación';
    private $tiposProductos = array();
    private $marcas = array();
    private $formValues = array(
        'nv_desde' => '',
        'nv_hasta' => '',
        'df_nv_desde' => '',
        'df_nv_hasta' => '',
        'dc_tipo_producto' => array(),
        'dc_marca' => array(),
        'df_null' => false,
    );

    /**
     * Inicio de funciones para el método indexAction.
     * En las siguientes líneas se realizarán las funciones necesarias y
     * requeridas por el método indexAction, el cual es el encargado de
     * mostrar la vista principal de consulta para la base de datos.
     */

     /**
      * indexAction
      * Éste método prepara y lanza la vista de consulta para el informe
      * de arribo de llegada. Llama a los métodos necesarios para la carga
      * correcta de la vista.
      */
    public function indexAction(){
        $this->setTiposProductos();
        $this->setMarcas();
        $form =  $this->getFormView($this->getTemplateUrl('index'), array(
            'tiposProductos' => $this->tiposProductos,
            'marcas' => $this->marcas,
        ));
        echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
    }

    /**
     * setTiposProductos
     * Éste método se encarga de buscar en la base de datos los tipos
     * de productos registrados por la empresa y su respectivo identificador,
     * para luego asignarlos a la variable de clase $tiposProductos.
     */
    private function setTiposProductos(){
        $db = $this->getConnection();
        $table = 'tb_tipo_producto';
        $fields = 'dc_tipo_producto, dg_tipo_producto';
        $conditions = "dc_empresa = {$this->getEmpresa()} AND dm_activo = 1";
        $st = $db->doQuery($db->select($table, $fields, $conditions));
        $tipos = array();
        foreach ($st->fetchAll(PDO::FETCH_OBJ) as $v) {
            $tipos[$v->dc_tipo_producto] = $v->dg_tipo_producto;
        }
        $this->tiposProductos = $tipos;
    }

    /**
     * setMarcas
     * Éste método se encarga de buscar en la base de datos las marcas
     * registrados por la empresa y su respectivo identificador, para luego
     * asignarlos a la variable de clase $marcas.
     */
    private function setMarcas(){
        $db = $this->getConnection();
        $table = 'tb_marca';
        $fields = 'dc_marca, dg_marca';
        $conditions = "dc_empresa = {$this->getEmpresa()} AND dm_activo = 1";
        $st = $db->doQuery($db->select($table, $fields, $conditions));
        $marcas = array();
        foreach ($st->fetchAll(PDO::FETCH_OBJ) as $v) {
            $marcas[$v->dc_marca] = $v->dg_marca;
        }
        $this->marcas = $marcas;
    }

    /**
     * Inicio de la sección showAction
     * Ésta sección se encargará de procesar los elementos seleccionados por
     * el usuario y buscará las notas de ventas resultantes, indicando las
     * fechas de arribo o renovación de cada una.
     */

    /**
     *  showAction
     *  Éste método se encarga de llamar a llos métodos necesarios para
     *  realizar la consulta en la base de datos.
     */
    public function showAction(){
        $this->validarConsulta();
        $resultado = $this->realizarConsulta();
        echo $this->getView($this->getTemplateUrl('show'), array(
            'productos' => $resultado,
        ));
    }

    /**
    *   validarConsulta
     *  Ésta función se encargará de llamar a los métodos que realizan las
     *  comprobaciones para construir la consulta.
     */
    private function validarConsulta(){
        $this->validateEmptyFields();
        $this->validateFecha();
    }

    /**
     *  validateEmptyFields
     *  Obtiene los parámetros y valida cada uno verificando si está ingresado.
     *  remueve todos los parámetros que no están especificados.
     */
    private function validateEmptyFields(){
        $r = self::getRequest();
        $this->formValues['nv_desde'] = empty($r->nv_desde) ? false:$r->nv_desde;
        $this->formValues['nv_hasta'] = empty($r->nv_hasta) ? false:$r->nv_hasta;
        $this->formValues['df_nv_desde'] = empty($r->df_nv_desde) ? false:$r->df_nv_desde;
        $this->formValues['df_nv_hasta'] = empty($r->df_nv_hasta) ? false:$r->df_nv_hasta;
        $this->formValues['dc_tipo_producto'] = empty($r->dc_tipo_producto) ? false:$r->dc_tipo_producto;
        $this->formValues['dc_marca'] = empty($r->dc_marca) ? false:$r->dc_marca;
        $peticion = array_filter($this->formValues);
        $peticion['df_null'] = isset($r->df_null) ? true:false;
        $this->formValues = $peticion;
    }

    /**
     *  validateFecha
     *  Valida que se haya ingresado un rango de fechas válido, es decir, que
     *  no se haya dejado los campos vacíos voluntariamente,o que como fecha
     *  final se haya seleccionado un dia anterior a la fecha inicial
     *  La fecha debe ser validada puesto que es un parámetro
     */
    private function validateFecha(){
        //Si fecha desde no existe, lanza un error y finaliza la ejecución
        if(!isset($this->formValues['df_nv_desde'])){
            $error = 'Se ha producido un error al asignar la fecha desde '
                . ' o el campo está vacío. Contacte al administrador.';
            $this->getErrorMan()->showWarning($error);
            exit;
        }
        //Si fecha hasta no existe, asigna la fecha actual.
        if (!isset($this->formValues['df_nv_hasta'])) {
            $this->formValues['df_nv_hasta'] = date("d/m/Y");
        }
        //Transformar a formato MySQL
        $db = $this->getConnection();
        $this->formValues['df_nv_desde'] = $db->sqlDate2($this->formValues['df_nv_desde']);
        $this->formValues['df_nv_hasta'] = $db->sqlDate2($this->formValues['df_nv_hasta']);
        $desde = new DateTime($this->formValues['df_nv_desde']);
        $hasta = new DateTime($this->formValues['df_nv_hasta']);
        //Si fecha hasta es menor a fecha desde, lanzará error y finaliza
        if($desde > $hasta){
            $error = 'Se ha designado Hasta como una fecha anterior a Desde. '
                . 'Verifique su información e intente nuevamente';
            $this->getErrorMan()->showWarning($error);
            exit;
        }
    }


    private function realizarConsulta(){
        $db = $this->getConnection();
        $table = $this->getTable();
        $fields = $this->getFields();
        $conditions = $this->prepararCondiciones();
        $options = $this->prepararOpciones();
        $st = $db->prepare($db->select($table, $fields, $conditions, $options));
        $this->setBinds($st);
        $db->stExec($st);
        $resultado = array();
        foreach($st->fetchAll(PDO::FETCH_OBJ) as $v){
            $detalle = array(
                'dc_nota_venta' => $v->dc_nota_venta,
                'dq_nota_venta' => $v->dq_nota_venta,
                'dg_producto' => $v->dg_producto,
                'dg_tipo_producto' => $v->dg_tipo_producto,
                'dg_marca' => $v->dg_marca,
                'df_fecha_arribo' => $db->dateLocalFormat($v->df_fecha_arribo),
            );
            $resultado[] = $detalle;
        }
        return $resultado;
    }

    private function getTable(){
        $table = ' tb_nota_venta nvt ';
        $table .= ' JOIN tb_nota_venta_detalle nvd on nvt.dc_nota_venta = nvd.dc_nota_venta ';
        $table .= ' JOIN tb_producto pro ON nvd.dc_producto = pro.dc_producto ';
        $table .= ' JOIN tb_marca mar ON pro.dc_marca = mar.dc_marca ';
        $table .= ' JOIN tb_tipo_producto tpr ON  pro.dc_tipo_producto = tpr.dc_tipo_producto ';
        return $table;
    }

    private function getFields(){
        return 'nvt.dq_nota_venta, pro.dg_producto, tpr.dg_tipo_producto,'
            . ' mar.dg_marca, nvd.df_fecha_arribo, nvt.dc_nota_venta ';
    }

    /**
     *  prepararCondiciones
     *  Este método creará el string de condiciones para realizar la consulta
     *  Las condiciones serán ingresadas como:
     *      :df_nv_desde
     *      :df_nv_hasta
     */
    private function prepararCondiciones(){
        //Crea la variable condiciones y le asigna el rango de fecha de búsqueda
        $condiciones = ' nvt.df_fecha_emision BETWEEN ? AND ? ';
        //Evalúa si la variable nv_desde existe en el array de formulario
        if(isset($this->formValues['nv_desde'])){
            //agrega el campo dq_nota_venta como parámetro de consulta
            $condiciones .= ' AND  nvt.dq_nota_venta ';
            //evalúa si la variable nv_hasta existe en el array
            if(isset($this->formValues['nv_hasta'])){
                //Si existe, designa un rango de notas de venta para buscar
                $condiciones .= ' BETWEEN ? AND ? ';
            } else {
                //si no existe, busca sólo por la nv en el campo desde.
                $condiciones .= ' = ? ';
            }
            //Si no existe la variable nv_desde, la nota de venta no se incluye
            //como parámetro de búsqueda.
        }
        //Evalúa si dc_tipo_producto existe como variable en el array del formulario
        if(isset($this->formValues['dc_tipo_producto'])){
            //Cuenta la cantidad de elementos seleccionados y repite la secuencia
            // ?, por cada tipo de producto seleccionado
            $productos = str_repeat(' ?,', count($this->formValues['dc_tipo_producto']));
            //continúa agregando consulta a la variable condiciones.
            $condiciones .= ' AND pro.dc_tipo_producto IN ( ';
            //Agrega los ?, asignados anteriormente, removiendo la última coma
            $condiciones .= trim($productos, ',');
            //finaliza la adición de esta condición
            $condiciones .= ' ) ';
        }
        //Evalúa si dc_marca existe como variable en el array del formulario
        if(isset($this->formValues['dc_marca'])){
            //Cuenta la cantidad de elementos seleccionados y repite la secuencia
            // ?, por cada tipo de producto seleccionado
            $marcas = str_repeat(' ?,', count($this->formValues['dc_marca']));
            //continúa agregando consulta a la variable condiciones.
            $condiciones .= ' AND pro.dc_marca IN ( ';
            //Agrega los ?, asignados anteriormente, removiendo la última coma
            $condiciones .= trim($marcas, ',');
            //finaliza la adición de esta condición
            $condiciones .= ' ) ';
        }
        //Evalua si se deben incluir las fechas que no fueron ingresadas
        if(!$this->formValues['df_null']){
            $condiciones .= ' AND nvd.df_fecha_arribo IS NOT NULL ';
        }
        //Retorna el string de condiciones que se ensambló en éste método.
        $condiciones .= ' AND nvt.dc_empresa = ? ';
        return $condiciones;
    }

    private function setBinds(&$st){
        $contador = 1;
        $st->bindValue($contador++, $this->formValues['df_nv_desde'], PDO::PARAM_STR);
        $st->bindValue($contador++, $this->formValues['df_nv_hasta'], PDO::PARAM_STR);

        if(isset($this->formValues['nv_desde'])){
            $st->bindValue($contador++, $this->formValues['nv_desde'], PDO::PARAM_INT);
        }

        if(isset($this->formValues['nv_hasta'])){
            $st->bindValue($contador++, $this->formValues['nv_hasta'], PDO::PARAM_INT);
        }
        if(isset($this->formValues['dc_tipo_producto'])){
            foreach ($this->formValues['dc_tipo_producto'] as $tp) {
                $st->bindValue($contador++, $tp, PDO::PARAM_INT);
            }
        }

        if(isset($this->formValues['dc_marca'])){
            foreach ($this->formValues['dc_marca'] as $m) {
                $st->bindValue($contador++, $m, PDO::PARAM_INT);
            }
        }

        $st->bindValue($contador++, $this->getEmpresa(), PDO::PARAM_INT);
    }

    private function prepararOpciones(){
        $options = array();
        $options['order_by'] = 'nvd.df_fecha_arribo ASC';
        return $options;
    }

    /* Consulta de muestra
    SELECT nvt.dq_nota_venta, pro.dg_producto, tpr.dg_tipo_producto,
    mar.dg_marca, nvd.df_fecha_arribo
    FROM tb_nota_venta nvt
    JOIN tb_nota_venta_detalle nvd on nvt.dc_nota_venta = nvd.dc_nota_venta
    JOIN tb_producto pro ON nvd.dc_producto = pro.dc_producto
    JOIN tb_marca mar ON pro.dc_marca = mar.dc_marca
    JOIN tb_tipo_producto tpr ON  pro.dc_tipo_producto = tpr.dc_tipo_producto
    //WHERE nvt.df_fecha_emision BETWEEN df_nv_desde AND df_nv_hasta
    //AND nvt.dq_nota_venta BETWEEN nv_desde AND nv_hasta
    AND nvd.dc_producto IN (dc_producto)
    AND pro.dc_marca IN (dc_marca)
    AND nvt.dc_empresa = 1
    */


}
