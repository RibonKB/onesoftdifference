<?php
	define("MAIN",1);
	include_once("../../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	$includeDir = $empresa;
	if(isset($_GET['type'])){
		if($_GET['type'] == 1 ){
			$includeDir .= 'margen';
		}
	}
	
	include("template_nota_venta/modo{$includeDir}.php");
	
	/*$datosNV = $db->select(
	"(SELECT * FROM tb_nota_venta WHERE dc_nota_venta = {$_GET['id']} AND dc_empresa={$empresa}) nv
	JOIN (SELECT * FROM tb_tipo_cambio WHERE dc_empresa={$empresa}) tc ON nv.dc_tipo_cambio = tc.dc_tipo_cambio
	JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa = {$empresa}) f ON nv.dc_ejecutivo = f.dc_funcionario",
	"nv.dc_cliente,nv.dq_nota_venta,UNIX_TIMESTAMP(nv.df_fecha_emision) as df_fecha_emision,tc.dg_tipo_cambio,tc.dn_cantidad_decimales,nv.dq_cambio,
	CONCAT_WS(' ',f.dg_nombres,f.dg_ap_paterno) as dg_ejecutivo,nv.dg_observacion,nv.dq_neto,nv.dq_iva");*/
	
	$datosNV = $db->select("(SELECT * FROM tb_nota_venta WHERE dc_empresa={$empresa} AND dc_nota_venta = {$_GET['id']}) nv
	LEFT JOIN tb_termino_comercial term ON term.dc_termino_comercial = nv.dc_termino_comercial
	LEFT JOIN tb_contacto_cliente c ON c.dc_contacto = nv.dc_contacto
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
		LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
		LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
	LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = nv.dc_tipo_cambio
	LEFT JOIN tb_modo_envio me ON me.dc_modo_envio = nv.dc_modo_envio
	LEFT JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
	LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = nv.dc_ejecutivo
	LEFT JOIN tb_cotizacion_estado ce ON ce.dc_estado = nv.dc_estado",
	"nv.dc_usuario_creacion,cl.dc_cliente,nv.dc_nota_venta,nv.dq_nota_venta,term.dg_termino_comercial,s.dg_direccion,com.dg_comuna,reg.dg_region,tc.dg_tipo_cambio,
	tc.dn_cantidad_decimales,nv.dq_cambio,me.dg_modo_envio,cl.dg_razon,cl.dg_rut,nv.dq_iva,nv.dq_neto,c.dg_contacto,nv.dm_validada,nv.dm_confirmada,
	UNIX_TIMESTAMP(nv.df_fecha_emision) as df_fecha_emision,DATE_FORMAT(nv.df_fecha_envio,'%d/%m/%Y') as df_envio,s.dg_sucursal,cl.dg_giro,
	(nv.dq_iva+nv.dq_neto) as dq_total,nv.dg_observacion,CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno,ex.dg_ap_materno) as dg_ejecutivo,nv.dc_estado,ce.dg_estado");
	
	if(!count($datosNV)){
		$error_man->showWarning("La nota de venta especificada no existe");
		exit();
	}
	$datosNV = $datosNV[0];
	
	setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
	$datosNV['df_fecha_emision'] = strftime("%d de %B de %Y",$datosNV['df_fecha_emision']);
	
	$datosNV['dq_total'] = $datosNV['dq_neto']/$datosNV['dq_cambio'];
	
	$datosNV['detalle'] = $db->select("tb_nota_venta_detalle",
	"dq_cantidad,(dq_precio_venta/{$datosNV['dq_cambio']}) AS dq_precio,dg_descripcion,
	CAST(dq_cantidad*dq_precio_venta/{$datosNV['dq_cambio']} as DECIMAL(20,{$datosNV['dn_cantidad_decimales']})) as dq_total,
	dq_precio_compra, dq_precio_venta",
	"dc_nota_venta = {$_GET['id']} AND dm_tipo = 0");
	
	//Calcular el costo total
	$costoTotal = 0;
	$ventaTotal = 0;
	
	foreach($datosNV['detalle'] as $v){
		$costoTotal += $v['dq_precio_compra'] * $v['dq_cantidad'];
		$ventaTotal += $v['dq_precio_venta'] * $v['dq_cantidad'];
	}
	
	if($costoTotal > 0){
		$datosNV['costo_tipo_cambio'] = $costoTotal / $datosNV['dq_cambio'];
	}
	
	$datosNV['costo_total'] = $costoTotal;
	$datosNV['margen_moneda_local'] = $ventaTotal - $costoTotal;
	
	if($datosNV['margen_moneda_local'] > 0){
		$datosNV['margen_tipo_cambio'] = $datosNV['margen_moneda_local']/$datosNV['dq_cambio'];
	} else {
		$datosNV['margen_tipo_cambio'] = 0;
	}
	
	$datosNV['margen_porcentual'] = ((($costoTotal/$ventaTotal)-1)*-1)*100;
	
	$pdf = new PDF();
	$pdf->addDetalle($datosNV['detalle']);
	$pdf->Output();
	
	
?>