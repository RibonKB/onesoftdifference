<?php

/**
 * Description of ConsultaNotaVentaFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 23-07-2013
 */
class ConsultaNotaVentaFactory extends Factory {

  protected $title = "Nota de Venta";
  private $filter_params = array();

  private $nota_venta;

  public function indexAction() {
      $form = $this->getFormView($this->getTemplateURL('filtros.form'),array(
          'ejecutivos' => $this->getFuncionariosService()->getEjecutivos()
      ));
      echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
  }

  public function consultarAction(){
      $notas_venta = $this->getNotasVentasFiltradas();

      if(!count($notas_venta)){
        $this->getErrorMan()->showAviso('No se encontraron notas de venta con los criterios especificados');
        exit;
      }

      echo $this->getView($this->getTemplateURL('listado'), array(
          'notas_venta' => $notas_venta
      ));
  }

  public function showAction(){
    $this->nota_venta = $this->getNotaVentaById(self::getRequest()->id);

    if($this->nota_venta === false):
      $this->getErrorMan()->showWarning('No se ha encontrado la nota de venta que estás intentando visualizar, comprueba los datos de entrada y vuelve a intentarlo');
      exit;
    endif;

    $this->nota_venta->detalle = $this->getDetalleNotaVenta();

    $this->setExtraDataNotaVenta();

    if($this->nota_venta->dm_nula == 1):
      $this->nota_venta->datos_anulacion = $this->getDatosAnulacion();
    endif;

    if($this->nota_venta->dm_costeable == 1 && check_permiso(64)):
      $this->nota_venta->detalle_costo = $this->getDetalleCostoNotaVenta();
      $this->nota_venta->dq_costo_real = $this->getCostoReal();
      $this->nota_venta->dq_margen_real = $this->nota_venta->dq_neto - $this->nota_venta->dq_costo_real;
      if($this->nota_venta->dq_neto > 0):
        $this->nota_venta->dg_margen_real_porcentual = round(($this->nota_venta->dq_margen_real/$this->nota_venta->dq_neto)*100,2);

      else:
        $this->nota_venta->dg_margen_real_porcentual = '-';
      endif;
    endif;

    echo $this->getView($this->getTemplateURL('show'), array(
        'nota_venta' => $this->nota_venta
    ));

  }

  private function setExtraDataNotaVenta(){
    $this->nota_venta->dq_neto = $this->getNetoNotaVenta();
    $this->nota_venta->dq_neto_tc = $this->nota_venta->dq_neto/$this->nota_venta->dq_cambio;
    $this->nota_venta->dq_costo = $this->getTotalCostoNotaVenta();
    $this->nota_venta->dq_costo_tc = $this->nota_venta->dq_costo/$this->nota_venta->dq_cambio;
    $this->nota_venta->dq_total = $this->nota_venta->dq_neto + $this->nota_venta->dq_iva;
    $this->nota_venta->dq_margen = $this->nota_venta->dq_neto - $this->nota_venta->dq_costo;
    $this->nota_venta->dq_margen_tc = $this->nota_venta->dq_margen/$this->nota_venta->dq_cambio;

    if($this->nota_venta->dq_neto):
      $this->nota_venta->dg_margen_porcentual = round(($this->nota_venta->dq_margen/$this->nota_venta->dq_neto)*100,2);
    else:
      $this->nota_venta->dg_margen_porcentual = '-';
    endif;

    $this->nota_venta->dc_solicitada = $this->getCantidadSolicitadaNotaVenta();
    $this->nota_venta->dc_facturada = $this->getCantidadFacturadaNotaVenta();
  }

  private function getNotaVentaById($dc_nota_venta){
    $db = $this->getConnection();
    $nv = $db->prepare($db->select('tb_nota_venta nv
              JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
              LEFT JOIN tb_tipo_nota_venta tnv ON tnv.dc_tipo = nv.dc_tipo_nota_venta
              LEFT JOIN tb_contacto_cliente c ON c.dc_contacto = nv.dc_contacto
                  LEFT JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
                  LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
                  LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
              LEFT JOIN tb_termino_comercial term ON term.dc_termino_comercial = nv.dc_termino_comercial
              LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = nv.dc_tipo_cambio
              LEFT JOIN tb_modo_envio me ON me.dc_modo_envio = nv.dc_modo_envio
              LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = nv.dc_ejecutivo
              LEFT JOIN tb_cotizacion_estado ce ON ce.dc_estado = nv.dc_estado',
          'nv.dc_usuario_creacion,cl.dc_cliente,nv.dc_nota_venta,nv.dq_nota_venta,term.dg_termino_comercial,
           s.dg_direccion,com.dg_comuna,reg.dg_region,tc.dg_tipo_cambio,nv.dm_nula,tc.dn_cantidad_decimales,
           nv.dq_cambio,me.dg_modo_envio,cl.dg_razon,cl.dg_rut,nv.dq_iva,nv.dq_neto,c.dg_contacto,nv.dm_validada,
           nv.dm_confirmada,tnv.dg_tipo,tnv.dm_costeable,nv.df_fecha_emision,nv.df_fecha_envio, s.dg_sucursal,
           nv.dc_facturada,nv.dc_solicitada, tnv.dm_contratable,nv.dg_observacion,ex.dg_nombres,ex.dg_ap_paterno,
           ex.dg_ap_materno,nv.dc_estado,ce.dg_estado',
          'nv.dc_nota_venta = ? AND nv.dc_empresa = ?'));
    $nv->bindValue(1, $dc_nota_venta, PDO::PARAM_INT);
    $nv->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($nv);

    $nv = $nv->fetch(PDO::FETCH_OBJ);

    if($nv === false):
      return false;
    endif;

    $nv->dg_ejecutivo = implode(' ', array(
        $nv->dg_nombres,
        $nv->dg_ap_paterno,
        $nv->dg_ap_materno
    ));

    $nv->dq_total = floatval($nv->dq_neto) + floatval($nv->dq_iva);
    $nv->df_fecha_emision = $db->dateLocalFormat($nv->df_fecha_emision);
    $nv->df_fecha_envio = $db->dateLocalFormat($nv->df_fecha_envio);

    return $nv;

  }

  private function getDetalleNotaVenta(){
    $db = $this->getConnection();

    $data = $db->prepare(
                $db->select('tb_nota_venta_detalle d
                          JOIN tb_producto p ON p.dc_producto = d.dc_producto
                          LEFT JOIN tb_proveedor pr ON pr.dc_proveedor = d.dc_proveedor',
                        'd.dq_cantidad, d.dg_descripcion, d.dq_precio_venta, d.dq_precio_compra,
                         d.dc_comprada, d.dc_recepcionada, d.dc_despachada, d.dc_facturada, d.df_fecha_arribo,
                         p.dg_codigo, p.dg_producto, pr.dg_razon dg_proveedor',
                        'd.dc_nota_venta = ? AND d.dm_tipo = 0'));
    $data->bindValue(1, $this->nota_venta->dc_nota_venta, PDO::PARAM_INT);
    $db->stExec($data);

    $detalle = array();

    while($d = $data->fetch(PDO::FETCH_OBJ)):
      $this->setDetalleCompuesto($d);
	  $fechaArribo = $db->dateLocalFormat($d->df_fecha_arribo);
	  $d->df_fecha_arribo = empty($fechaArribo) ? 'N/A':$fechaArribo;
      $detalle[] = $d;
    endwhile;
    return $detalle;

  }

  private function getDetalleCostoNotaVenta(){
    $db = $this->getConnection();

    $data = $db->prepare(
                $db->select('tb_nota_venta_detalle d
                          JOIN tb_producto p ON p.dc_producto = d.dc_producto
                          LEFT JOIN tb_proveedor pr ON pr.dc_proveedor = d.dc_proveedor',
                        'd.dq_cantidad, d.dg_descripcion, d.dq_precio_venta, d.dq_precio_compra,
                         d.dc_comprada, d.dc_recepcionada, d.dc_despachada, d.dc_facturada,
                         p.dg_codigo, p.dg_producto, pr.dg_razon dg_proveedor',
                        'd.dc_nota_venta = ? AND d.dm_tipo = 1'));
    $data->bindValue(1, $this->nota_venta->dc_nota_venta, PDO::PARAM_INT);
    $db->stExec($data);

    $detalle = array();

    while($d = $data->fetch(PDO::FETCH_OBJ)):
      $this->setDetalleCompuesto($d);
      $detalle[] = $d;
    endwhile;

    return $detalle;
  }

    private function setDetalleCompuesto(&$d){
      $d->dq_total = $d->dq_precio_venta * $d->dq_cantidad;
      $d->dq_total_tc = $d->dq_total / $this->nota_venta->dq_cambio;

      $d->dq_total_costo = $d->dq_precio_compra * $d->dq_cantidad;
      $d->dq_total_costo_tc = $d->dq_total_costo / $this->nota_venta->dq_cambio;

      $d->dq_precio_compra_tc = $d->dq_precio_compra / $this->nota_venta->dq_cambio;
      $d->dq_precio_venta_tc = $d->dq_precio_venta / $this->nota_venta->dq_cambio;

      $d->dq_margen = $d->dq_total-$d->dq_total_costo;
      $d->dq_margen_tc = $d->dq_total_tc-$d->dq_total_costo_tc;

      if($d->dq_total > 0){
        $d->dg_margen_porcentual = round(($d->dq_margen/$d->dq_total)*100,2);
      }else{
        $d->dg_margen_porcentual = '-';
      }
    }

  private function getCantidadSolicitadaNotaVenta(){
    $cantidad = 0;
    foreach($this->nota_venta->detalle as $d):
      $cantidad += intval($d->dq_cantidad);
    endforeach;

    return $cantidad;
  }

  private function getCantidadFacturadaNotaVenta(){
    $cantidad = 0;
    foreach($this->nota_venta->detalle as $d):
      $cantidad += intval($d->dc_facturada);
    endforeach;

    return $cantidad;
  }

  private function getDatosAnulacion(){
    $db = $this->getConnection();

    $data = $db->prepare(
                $db->select(
                        'tb_nota_venta_anulada n
                         JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo',
                        'ma.dg_motivo,n.dg_comentario',
                        'n.dc_nota_venta = ?'));
    $data->bindValue(1, $this->nota_venta->dc_nota_venta, PDO::PARAM_INT);
    $db->stExec($data);

    return $data->fetch(PDO::FETCH_OBJ);
  }

  private function getCostoReal(){
    $costo = 0;
    foreach($this->nota_venta->detalle_costo as $d):
      $costo += $d->dq_total_costo;

    endforeach;

    return $costo;
  }

  private function getNetoNotaVenta(){
    $neto = 0;
    foreach($this->nota_venta->detalle as $d):
      $neto += $d->dq_total;
    endforeach;

    return $neto;
  }

  private function getTotalCostoNotaVenta(){
    $total_costo = 0;
    foreach($this->nota_venta->detalle as $d):
      $total_costo += $d->dq_total_costo;
    endforeach;

    return $total_costo;
  }

  private function getNotasVentasFiltradas(){
    $db = $this->getConnection();

    $filtros = $this->getCondicionesFiltro();

    $nv = $db->prepare(
            $db->select(
                    'tb_nota_venta nv
                     JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta
                     JOIN tb_producto prod ON prod.dc_producto = d.dc_producto',
                    'distinct nv.*', $filtros, array('order_by' => 'nv.df_fecha_emision DESC'))
          );
    foreach($this->filter_params as $i => $data):
      $nv->bindValue($i+1, $data[0], $data[1]);
    endforeach;

    $db->stExec($nv);

    return $nv->fetchAll(PDO::FETCH_OBJ);

  }

    private function getCondicionesFiltro(){
        $c = NULL;
        $this->setFiltroEmpresa($c);
        $this->setFiltroFechaEmision($c);
        $this->setFiltroNumeroDocumento($c);
        $this->setFiltroCliente($c);
        $this->setFiltroClienteFinal($c);
        $this->setFiltroEjecutivo($c);
        $this->setFiltroProductoDetalle($c);
        $this->setFiltroDescripcionDetalle($c);
        $this->setFiltroEstado($c);

        return $c;
    }

        private function setFiltroEmpresa(&$c){
            $c = 'nv.dc_empresa = ?';
            $this->filter_params[] = array($this->getEmpresa(), PDO::PARAM_INT);
        }

        private function setFiltroFechaEmision(&$c){
           $r = self::getRequest();
           $db = $this->getConnection();

           if(!$r->dq_nota_venta_desde){
              $c .= ' AND (nv.df_fecha_emision BETWEEN ? AND ?)';
              $this->filter_params[] = array($db->sqlDate2($r->df_emision_desde), PDO::PARAM_STR);
              $this->filter_params[] = array($db->sqlDate2($r->df_emision_hasta." 23:59"), PDO::PARAM_STR);
           }
        }

        private function setFiltroNumeroDocumento(&$c){
          $r = self::getRequest();

          if($r->dq_nota_venta_desde){
              if(isset($r->dq_nota_venta_hasta) && $r->dq_nota_venta_hasta){
                  $c .= " AND (nv.dq_nota_venta BETWEEN ? AND ?)";
                  $this->filter_params[] = array($r->dq_nota_venta_desde, PDO::PARAM_INT);
                  $this->filter_params[] = array($r->dq_nota_venta_hasta, PDO::PARAM_INT);

              }else{
                  $c .= " AND nv.dq_nota_venta = ?";
                  $this->filter_params[] = array($r->dq_nota_venta_desde, PDO::PARAM_INT);
              }
          }
        }

        private function setFiltroCliente(&$c){
            $r = self::getRequest();
            if(isset($r->dc_cliente)){
                $this->setFiltroMultiple($c, $r->dc_cliente, 'nv.dc_cliente');
            }
        }

        private function setFiltroClienteFinal(&$c){
            $r = self::getRequest();
            if(isset($r->dc_cliente_final)){
                $this->setFiltroMultiple($c, $r->dc_cliente_final, 'nv.dc_cliente_final');
            }
        }

        private function setFiltroEjecutivo(&$c){
            $r = self::getRequest();
            if(isset($r->dc_ejecutivo)){
                $this->setFiltroMultiple($c, $r->dc_ejecutivo, 'nv.dc_ejecutivo');
            }
        }

        private function setFiltroProductoDetalle(&$c){
            $r = self::getRequest();
            if($r->dg_codigo_producto){
                $c .= ' AND prod.dg_codigo LIKE ?';
                $this->filter_params[] = array($r->dg_codigo_producto, PDO::PARAM_STR);
            }
        }

        private function setFiltroDescripcionDetalle(&$c){
            $r = self::getRequest();
            if($r->dg_descripcion_detalle){
                $c .= ' AND d.dg_descripcion LIKE ?';
                $this->filter_params[] = array($r->dg_descripcion_detalle, PDO::PARAM_STR);
            }
        }

        private function setFiltroEstado(&$c){
            $r = self::getRequest();

            if(isset($r->dm_estado)){
              $data = implode(" = 1 OR ",$r->dm_estado);
              $c .= " AND ({$data} = 1)";
            }
        }

        private function setFiltroMultiple(&$c, $data, $fieldname, $type = PDO::PARAM_INT){
            $questions = substr(str_repeat(',?', count($data)),1);
            $c .= " AND {$fieldname} IN ({$questions})";
            foreach($data as $item){
                $this->filter_params[] = array($item, $type);
            }
        }

  /**
   *
   * @return ListadosFuncionariosService
   */
  private function getFuncionariosService(){
    return $this->getService('ListadosFuncionarios');
  }

}

?>
