<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_tipo_venta = intval($_POST['dc_tipo_venta']);

if(!$dc_tipo_venta){
	$error_man->showWarning('El tipo de venta es inválido, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

$tipo_venta = $db->prepare($db->select('tb_tipo_nota_venta','dm_contratable','dc_empresa = ? AND dc_tipo = ?'));
$tipo_venta->bindValue(1,$empresa,PDO::PARAM_INT);
$tipo_venta->bindValue(2,$dc_tipo_venta,PDO::PARAM_INT);
$db->stExec($tipo_venta);
$tipo_venta = $tipo_venta->fetch(PDO::FETCH_OBJ);

if($tipo_venta === false){
	$error_man->showWarning('No se encontró el tipo de venta, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

if($tipo_venta->dm_contratable == 0){
	$error_man->showWarning('El tipo de venta seleccionado no admite asignación de contratos, no puede continuar.');
	exit;
}

$conceptos_pago = $db->prepare($db->select('tb_nota_venta_detalle_contrato','dc_tipo_detalle, dg_tipo_detalle','dc_tipo = ?'));
$conceptos_pago->bindValue(1,$dc_tipo_venta,PDO::PARAM_INT);
$db->stExec($conceptos_pago);
$conceptos_pago = $conceptos_pago->fetchAll(PDO::FETCH_OBJ);

if(!count($conceptos_pago)){
	$error_man->showWarning('El tipo de venta no ha sido configurado para distribuir sus cuotas, no se puede continuar');
	exit;
}

$base_data = array();
if(isset($_POST['dq_cuota_concepto'])){
	foreach($_POST['dq_cuota_concepto'] as $i => $dq_cuota){
		$base_data[$_POST['dc_detalle_concepto'][$i]] = $dq_cuota;
	}
}

require_once("../../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Distribución de cuotas de contrato
</div>
<div class="panes">
	<?php $form->Start('#','cr_distribucion_contrato','contratoForm') ?>
	<table class="tab" width="100%">
	<thead>
		<tr>
			<th>Concepto</th>
			<th>Monto cuota</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($conceptos_pago as $c): ?>
		<tr>
			<td>valor cuota <b><?php echo $c->dg_tipo_detalle ?></b></td>
			<td>
				<?php
					$form->Text('','concepto'.$c->dc_tipo_detalle,1,Form::DEFAULT_TEXT_LENGTH,
								isset($base_data[$c->dc_tipo_detalle])?$base_data[$c->dc_tipo_detalle]:0,
								'inputtext dq_cuota_concepto',20)
				?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
	<?php $form->End('Asignar Valor cuota','addbtn') ?>
</div>
<script type="text/javascript">
	cr_contrato.initDistribucion();
</script>