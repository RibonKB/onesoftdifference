<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_nota_venta = intval($_POST['dc_nota_venta']);

$nota_venta = $db->prepare(
					$db->select('tb_nota_venta nv
								 JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = nv.dc_tipo_cambio',
								 'nv.dq_cambio, tc.dg_tipo_cambio, tc.dn_cantidad_decimales',
								 'nv.dc_nota_venta = ? AND nv.dc_empresa = ?'));
$nota_venta->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$nota_venta->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_venta);
$nota_venta = $nota_venta->fetch(PDO::FETCH_OBJ);

if($nota_venta === false){
	$error_man->showWarning('La nota de venta no es válida y no se puede continuar.');
	exit;
}

//Obtener datos de cabecera de contratos
$contrato = $db->prepare($db->select('tb_nota_venta_contrato',
						'dc_contrato, dq_contrato, dc_cuotas',
						'dc_nota_venta = ? AND dc_empresa = ?'));
$contrato->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$contrato->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($contrato);
$contrato = $contrato->fetch(PDO::FETCH_OBJ);

if($contrato === false){
	$error_man->showWarning('No se encontraron contratos para la nota de venta');
	exit;
}

$contrato_table = $db->doQuery(
						$db->select('tb_nota_venta_contrato_detalle d
									 JOIN tb_nota_venta_detalle_contrato t ON t.dc_tipo_detalle = d.dc_tipo
									 LEFT JOIN tb_factura_venta fv ON fv.dc_factura = d.dc_factura',
									 'd.dq_cuota, d.dc_numero_cuota, d.df_vencimiento, d.dc_tipo, t.dg_tipo_detalle, fv.dq_factura, fv.dq_folio',
									 "d.dc_contrato = {$contrato->dc_contrato}"));

$conceptos = array();
$table = array();

while($d = $contrato_table->fetch(PDO::FETCH_OBJ)):

	//Agregar el tipo de cuota (concepto) al listado
	$conceptos[$d->dc_tipo] = $d->dg_tipo_detalle;
	
	if(!isset($table[$d->dc_numero_cuota])){
		$table[$d->dc_numero_cuota] = new stdClass();
		$table[$d->dc_numero_cuota]->df_vencimiento = $d->df_vencimiento;
		$table[$d->dc_numero_cuota]->dq_factura = $d->dq_factura;
		$table[$d->dc_numero_cuota]->dq_folio = $d->dq_folio;
		$table[$d->dc_numero_cuota]->dq_cuota = array();
	}
	
	//Agrupar tabla de contrato por número de cuota y concepto
	$table[$d->dc_numero_cuota]->dq_cuota[$d->dc_tipo] = $d->dq_cuota;
	
endwhile;
unset($contrato_table);

?>
<div class="secc_bar">
	Tabla de cuotas contrato <b><?php echo $contrato->dq_contrato ?></b>
</div>
<div class="panes">
<div class="info">
	Los precios están representados en <b><?php echo $nota_venta->dg_tipo_cambio ?></b>
</div>
	<table class="tab" width="100%" id="tabla_contratos">
    	<thead>
        	<tr>
            	<th>Cuota</th>
                <th>Vencimiento</th>
                <th>Factura</th>
                <th>Folio</th>
                <?php foreach($conceptos as $c): ?>
                <th><?php echo $c ?></th>
                <?php endforeach ?>
                <th>Total</th>
                <th>Total <strong><?php echo $empresa_conf['dg_moneda_local'] ?></strong></th>
            </tr>
        </thead>
        <tbody>
        <?php
			$total = array();
        	foreach($table as $dc_cuota => $d):
				$total_linea = 0;
		?>
        	<tr>
            	<td><?php echo $dc_cuota ?>/<?php echo $contrato->dc_cuotas ?></td>
                <td><?php echo $db->dateLocalFormat($d->df_vencimiento) ?></td>
                <td><?php echo $d->dq_factura ?></td>
                <td><?php echo $d->dq_folio ?></td>
                <?php foreach($conceptos as $dc_concepto => $c): ?>
                <td align="right">
                	<?php if(isset($d->dq_cuota[$dc_concepto])): ?>
                    	<?php
                        	echo moneda_local($d->dq_cuota[$dc_concepto]/$nota_venta->dq_cambio,$nota_venta->dn_cantidad_decimales);
							$total_linea += $d->dq_cuota[$dc_concepto];
							if(!isset($total[$dc_concepto]))
								$total[$dc_concepto] = 0;
							$total[$dc_concepto] += $d->dq_cuota[$dc_concepto];
						?>
                    <?php else: ?>
                    	0
                    <?php endif ?>
                </td>
                <?php endforeach ?>
                <td align="right"><?php echo moneda_local($total_linea/$nota_venta->dq_cambio,$nota_venta->dn_cantidad_decimales) ?></td>
                <td align="right"><?php echo moneda_local($total_linea) ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
        <tfoot>
        	<tr>
            	<th align="right" colspan="4">Total</th>
                <?php foreach($conceptos as $dc_concepto => $c): ?>
                	<th align="right"><?php echo moneda_local($total[$dc_concepto]/$nota_venta->dq_cambio,$nota_venta->dn_cantidad_decimales) ?></th>
                <?php endforeach ?>
                <th align="right"><?php echo moneda_local(array_sum($total)/$nota_venta->dq_cambio,$nota_venta->dn_cantidad_decimales) ?></th>
                <th align="right"><?php echo moneda_local(array_sum($total)) ?></th>
            </tr>
        </tfoot>
    </table>
</div>
<script type="text/javascript">
	window.setTimeout(function(){
		$('#tabla_contratos').tableAdjust(10);
	},100);
</script>