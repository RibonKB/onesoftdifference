<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

if(!is_numeric($_POST['dc_nota_venta'])){
	$error_man->showWarning("La nota especificada es inválida, compruebe que no haya accedido desde un lugar indebido.");
	exit;
}


/**
*	Comprobar guias de recepción asociadas

$guias_recepcion = $db->prepare($db->select('tb_guia_recepcion','1',"dc_nota_venta=? AND dm_nula = 0"));
$guias_recepcion->bindValue(1,$_POST['dc_nota_venta'],PDO::PARAM_INT);
$db->stExec($guias_recepcion);

if($guias_recepcion->fetch() !== false){
	$error_man->showWarning("No puede anular la nota de venta, debido a que algunos de los productos indicados en el detalle han sido recepcionados.");
	exit;
}

unset($guias_recepcion);
**/
/**
*	Comprobar facturas de venta asociadas


$facturas_venta = $db->prepare($db->select('tb_factura_venta','1',"dc_nota_venta=? AND dm_nula = 0"));
$facturas_venta->bindValue(1,$_POST['dc_nota_venta'],PDO::PARAM_INT);
$db->stExec($facturas_venta);

if($facturas_venta->fetch() !== false){
	$error_man->showWarning("No puede anular la nota de venta, debido a que algunos de los productos indicados en el detalle han sido facturados");
	exit;
}

unset($facturas_venta);
**/
/**
*	Comprobar guias de despacho asociadas


$guias_despacho = $db->prepare($db->select('tb_guia_despacho','1',"dc_nota_venta=? AND dm_nula = 0"));
$guias_despacho->bindValue(1,$_POST['dc_nota_venta'],PDO::PARAM_INT);
$db->stExec($guias_despacho);

if($guias_despacho->fetch() !== false){
	$error_man->showWarning("No puede anular la nota de venta, debido a que algunos de los productos indicados en el detalle han sido despachados");
	exit;
}

unset($guias_despacho);
**/
/**
*	Anular Nota de venta
**/

$db->start_transaction();

$nota_venta = $db->prepare($db->update('tb_nota_venta',array('dm_nula'=>1),"dc_nota_venta=?"));
$nota_venta->bindValue(1,$_POST['dc_nota_venta'],PDO::PARAM_INT);
$db->stExec($nota_venta);

/**
*	Anular ordenes de compra asociadas
*

$orden_compra = $db->prepare($db->update('tb_orden_compra',array('dm_nula'=>1),"dc_nota_venta=?"));
$orden_compra->bindValue(1,$_POST['dc_nota_venta'],PDO::PARAM_INT);
$db->stExec($orden_compra);

unset($orden_compra);
*/
/**
*	Registrar log de nota de venta anulada
**/

$log_anulacion = $db->prepare($db->insert('tb_nota_venta_anulada',array(
	'dc_nota_venta' => '?',
	'dc_motivo' => '?',
	'dg_comentario' => '?',
	'dc_usuario' => $idUsuario,
	'df_anulacion' => $db->getNow()
)));

$log_anulacion->bindValue(1,$_POST['dc_nota_venta'],PDO::PARAM_INT);
$log_anulacion->bindValue(2,$_POST['dc_motivo'],PDO::PARAM_INT);
$log_anulacion->bindValue(3,$_POST['dg_comentario'],PDO::PARAM_STR);

$db->stExec($log_anulacion);

unset($log_anulacion);

/**
*	Mostrar confirmación (en alerta) indicando anulación de la nota de venta
**/

$db->commit();

$error_man->showAviso("Atención: se ha anulado la nota de venta.");

?>