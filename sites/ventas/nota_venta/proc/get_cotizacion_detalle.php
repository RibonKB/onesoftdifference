<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select('tb_cotizacion','dc_cotizacion,dc_tipo_cambio,dq_cambio',"dc_empresa = {$empresa} AND dq_cotizacion={$_GET['number']}");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

$detalle = $db->select("(SELECT * FROM tb_cotizacion_detalle WHERE dc_cotizacion = {$id['dc_cotizacion']}) d
LEFT JOIN (SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo = '1') p ON p.dg_codigo = d.dg_producto",
"d.dg_producto,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,p.dc_producto,p.dc_cebe,p.dg_codigo");

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>