<?php
  $dq_proyecto = doc_GetNextNumber('tb_nota_venta_proyecto','dq_proyecto');
  
  //Crear proyecto
  $dc_proyecto = $db->insert('tb_nota_venta_proyecto',array(
      'dq_proyecto' => $dq_proyecto,
      'dc_nota_venta' => $nota_venta,
      'df_emision' => 'NOW()',
      'dg_nombre' => $_POST['dg_nombre_proyecto'],
      'dc_encargado' => $_POST['dc_encargado_proyecto'],
      'df_inicio' => $db->sqlDate($_POST['df_inicio_proyecto']),
      'df_fin' => $db->sqlDate($_POST['df_fin_proyecto']),
      'dc_empresa' => $empresa,
      'dc_usuario_creacion' => $idUsuario
  ));
  
  
?>
