<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

/**
*	Validar que pueda asignarle costeo
*
*	Y en cuyo caso filtrar y no permitir productos que manejen stock en el detalle
**/
$_POST['nv_tipo_venta'] = explode('|',$_POST['nv_tipo_venta']);
$tipo_venta = $_POST['nv_tipo_venta'][0];
if($_POST['nv_tipo_venta'][1] == '1'){

	$prod_validar = implode(',',$_POST['prod']);

	$prod_validar = $db->select("(SELECT * FROM tb_producto WHERE dc_producto IN ({$prod_validar})) p
	JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto",'1',"tp.dm_controla_inventario = 0");

	if(count($prod_validar)){
		$error_man->showWarning("El tipo de venta seleccionado no permite productos que controlen stocks.");
		exit();
	}

	$solicitada = 0;

}else{
	//La cantidad solicitada indica el total de productos que se van a vender (incluyendo todos los detalles)
	$solicitada = array_sum($_POST['cant']);
}

$db->start_transaction();

$db->update('tb_nota_venta',array(
	'dc_cotizacion' => $_POST['nv_id_cotizacion'],
    'dc_cliente_final' => $_POST['dc_cliente_final'],
	'dc_tipo_nota_venta' => $tipo_venta,
	'dc_termino_comercial' => $_POST['nv_termino'],
	'dc_contacto' => $_POST['lst_contacto'],
	'dc_ejecutivo' => $_POST['nv_executive'],
	'dc_tipo_cambio' => $_POST['prod_tipo_cambio'],
	'dq_cambio' => $_POST['nv_cambio'],
	'dc_modo_envio' => $_POST['nv_modo_envio'],
	'dc_cliente' => $_POST['cli_id'],
	'df_fecha_envio' => $db->sqlDate($_POST['nv_envio']),
	'dq_neto' => $_POST['cot_neto'],
	'dq_iva' => $_POST['cot_iva'],
	'dg_observacion' => $_POST['nv_observacion'],
	'dc_solicitada' => array_sum($_POST['cant']),
        'dm_al_dia' => $_POST['nv_aldia'],
),"dc_nota_venta = {$_POST['nv_id']}");

$prod_list = implode(',',$_POST['prod']);
$aux = $db->select("(SELECT * FROM tb_producto WHERE dc_producto IN ({$prod_list})) p
LEFT JOIN tb_tipo_producto t ON p.dc_tipo_producto = t.dc_tipo_producto",'p.dc_producto,t.dm_controla_inventario');

$stock_change = array();
foreach($aux as $v){
	$stock_change[$v['dc_producto']] = $v['dm_controla_inventario'];
}
unset($aux);

$to_delete = implode(',',$_POST['id_detail']);

$db->query("DELETE FROM tb_nota_venta_detalle where dc_nota_venta = {$_POST['nv_id']} AND dc_nota_venta_detalle NOT IN ({$to_delete}) AND dm_tipo = 0");

$no_stock = 0;
foreach($_POST['prod'] as $i => $v){
	if($_POST['id_detail'][$i] != 0){

		$det = array(
			'dc_producto' => $v,
			'dg_descripcion' => $_POST['desc'][$i],
			'dq_cantidad' => $_POST['cant'][$i],
			'dq_precio_compra' => str_replace(',','',$_POST['costo'][$i])*$_POST['nv_cambio'],
			'dq_precio_venta' => str_replace(',','',$_POST['precio'][$i])*$_POST['nv_cambio'],
		);
		if(!empty($_POST['df_fecha_arribo'][$i])){
			$det["df_fecha_arribo"] = $db->sqlDate($_POST['df_fecha_arribo'][$i]);
		}

		if(isset($_POST['proveedor'][$i]))
			$det['dc_proveedor'] = $_POST['proveedor'][$i];

		if($stock_change[$v] == 1){
			$det['dc_comprada'] = $_POST['cant'][$i];
			$det['dc_recepcionada'] = $_POST['cant'][$i];
			$no_stock += $_POST['cant'][$i]-$_POST['comprada'][$i];
		}

		$db->update('tb_nota_venta_detalle',$det,"dc_nota_venta_detalle={$_POST['id_detail'][$i]}");

		continue;
	}

	$det = array(
		"dc_empresa" => $empresa,
		"dc_nota_venta" => $_POST['nv_id'],
		"dc_producto" => $_POST['prod'][$i],
		"dg_descripcion" => $_POST['desc'][$i],
		"dq_cantidad" => $_POST['cant'][$i],
		"dq_precio_venta" => str_replace(',','',$_POST['precio'][$i])*$_POST['nv_cambio'],
		"dq_precio_compra" => str_replace(',','',$_POST['costo'][$i])*$_POST['nv_cambio'],
		"dc_proveedor" => $_POST['proveedor'][$i]
	);
	if(!empty($_POST['df_fecha_arribo'][$i])){
		$det["df_fecha_arribo"] = $db->sqlDate($_POST['df_fecha_arribo'][$i]);
	}

	if($stock_change[$v] == 1){
		$det['dc_comprada'] = $_POST['cant'][$i];
		$det['dc_recepcionada'] = $_POST['cant'][$i];
		$no_stock += $_POST['cant'][$i];
	}

	$db->insert('tb_nota_venta_detalle',$det);
}

if($no_stock > 0){
	$db->update('tb_nota_venta',array('dc_comprada' => "dc_comprada+{$no_stock}",'dc_recepcionada' => "dc_comprada+{$no_stock}"),"dc_nota_venta={$_POST['nv_id']}");
}

/**
*	Actualizar la cantidad solicitada de la nota de venta
**/
$dc_solicitada = $db->select('tb_nota_venta_detalle','SUM(dq_cantidad) dc_solicitada',"dc_nota_venta = {$_POST['nv_id']}");
$dc_solicitada = $dc_solicitada[0]['dc_solicitada'];

$db->update('tb_nota_venta',array(
	'dc_solicitada' => $dc_solicitada
),"dc_nota_venta = {$_POST['nv_id']}");

//-----

$db->commit();

$error_man->showConfirm("Se ha modificado la nota de venta y sus detalles correctamente");
?>
