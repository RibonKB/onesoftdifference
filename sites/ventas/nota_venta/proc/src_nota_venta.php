<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if($_POST['nv_numero_desde']){
	if(isset($_POST['nv_numero_hasta']) && $_POST['nv_numero_hasta']){
		
		$_POST['nv_emision_desde'] = $db->sqlDate($_POST['nv_emision_desde']);
		$_POST['nv_emision_hasta'] = $db->sqlDate($_POST['nv_emision_hasta']." 23:59");
		
		$conditions = "nv.dc_empresa = {$empresa} AND (nv.df_fecha_emision BETWEEN {$_POST['nv_emision_desde']} AND {$_POST['nv_emision_hasta']})";
		
		$conditions .= " AND (nv.dq_nota_venta BETWEEN {$_POST['nv_numero_desde']} AND {$_POST['nv_numero_hasta']})";
	}else{
		$conditions = "nv.dq_nota_venta = {$_POST['nv_numero_desde']} AND dc_empresa = {$empresa}";
	}
}else{
	$_POST['nv_emision_desde'] = $db->sqlDate($_POST['nv_emision_desde']);
	$_POST['nv_emision_hasta'] = $db->sqlDate($_POST['nv_emision_hasta']." 23:59");
	
	$conditions = "nv.dc_empresa = {$empresa} AND (nv.df_fecha_emision BETWEEN {$_POST['nv_emision_desde']} AND {$_POST['nv_emision_hasta']})";
}

if(isset($_POST['nv_client'])){
	$_POST['nv_client'] = implode(',',$_POST['nv_client']);
	$conditions .= " AND nv.dc_cliente IN ({$_POST['nv_client']})";
}

if(isset($_POST['dc_cliente_final'])){
	$_POST['dc_cliente_final'] = implode(',',$_POST['dc_cliente_final']);
	$conditions .= " AND nv.dc_cliente_final IN ({$_POST['dc_cliente_final']})";
}

if(isset($_POST['nv_executive'])){
	$_POST['nv_executive'] = implode(',',$_POST['nv_executive']);
	$conditions .= " AND nv.dc_ejecutivo IN ({$_POST['nv_executive']})";
}

if(isset($_POST['nv_status'])){
	$_POST['nv_status'] = implode(" = '1' OR ",$_POST['nv_status']);
	$conditions .= " AND ({$_POST['nv_status']} = '1')";
}

if($_POST['nv_codigo_producto'] || $_POST['nv_detalle_descripcion']){
	
	$detail_conditions = ' JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta';
	
	if($_POST['nv_detalle_descripcion']){
		$db->escape($_POST['nv_detalle_descripcion']);
		$detail_conditions .= " AND d.dg_descripcion LIKE '%{$_POST['nv_detalle_descripcion']}%'";
	}
	
	if($_POST['nv_codigo_producto']){
		$db->escape($_POST['nv_codigo_producto']);
		$detail_conditions .= " JOIN tb_producto p ON p.dc_producto = d.dc_producto AND p.dg_codigo LIKE '%{$_POST['nv_codigo_producto']}%'";
	}
	
}else{
	$detail_conditions = '';
}

$data = $db->select('tb_nota_venta nv'.$detail_conditions,'nv.dc_nota_venta,nv.dq_nota_venta',$conditions,array('order_by' => 'nv.df_fecha_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron notas de venta con los criterios especificados");
	exit();
}

echo("<div id='show_nota_venta'></div>");

echo("<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Notas de venta<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Nota de venta</th>
	</tr>
</thead>
<tbody>");

foreach($data as $c){
echo("
<tr>
	<td align='left'>
		<a href='sites/ventas/nota_venta/proc/show_nota_venta.php?id={$c['dc_nota_venta']}' class='nv_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_nota_venta']}</a>
	</td>
</tr>");
}

echo("
</tbody>
</table>
</div>");

echo("<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>");
echo("<button type='button' class='button' id='print_version'>Version de impresión</button>");
if(check_permiso(60))
	echo("<button type='button' class='delbtn' id='null_nota_venta'>Anular</button>");
if(check_permiso(42) || check_permiso(40))
	echo("<button type='button' class='editbtn' id='edit_nota_venta'>Editar</button>");
echo("<button type='button' id='costeo_nota_venta' class='imgbtn' style='background-image:url(images/doc.png);'>Costos de venta</button>");
echo("<button type='button' id='comprar_nota_venta' class='imgbtn' style='background-image:url(images/doc.png);'>Comprar</button>");
if(check_permiso(24))
	echo("<button type='button' id='facturar_nota_venta' class='imgbtn' style='background-image:url(images/doc.png);'>Facturar</button>");
if(check_permiso(32))
	echo('<button type="button" id="valida_nota_venta" class="checkbtn">Validar</button>');
if(check_permiso(33))
	echo('<button type="button" id="confirma_nota_venta" class="checkbtn">Confirmar</button>');
echo('<button type="button" id="show_workflow" class="searchbtn">Workflow</button>');
if(check_permiso(90)): 
    echo ('<button type="button" id="print_special" class="button">Impresión con Márgenes</button>');
endif;

echo("</div>");

?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".nv_load").click(function(e){
		e.preventDefault();
		$('#show_nota_venta').html("<img src='images/ajax-loader.gif' alt='' /> cargando nota de venta ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_nota_venta');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>