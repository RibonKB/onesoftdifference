<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
require_once('../../proc/ventas_functions_old.php');

//Se obtiene el estado inicial configurado para la nota de venta
$estado = $db->select('tb_valor_defecto','dc_valor',"dg_tabla = 'tb_nota_venta_estado' AND dc_empresa={$empresa}",array('order_by'=> 'df_creacion DESC'));
$estado = count($estado)?$estado[0]['dc_valor']:0;

//El número de la nota de venta es identificado automáticamente a partir del anterior creado
$cot_num = doc_GetNextNumber('tb_nota_venta','dq_nota_venta',$empresa_conf['dc_correlativo_nota_venta'],'df_fecha_emision');

/**
*	Validar que pueda asignarle costeo
*
*	Y en cuyo caso filtrar y no permitir productos que manejen stock en el detalle
**/
$_POST['nv_tipo_venta'] = explode('|',$_POST['nv_tipo_venta']);
$tipo_venta = $_POST['nv_tipo_venta'][0];
if($_POST['nv_tipo_venta'][1] == '1'){

	$prod_validar = implode(',',$_POST['prod']);

	$prod_validar = $db->select("(SELECT * FROM tb_producto WHERE dc_producto IN ({$prod_validar})) p
		JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto",
	'1',"tp.dm_controla_inventario = 0");

	if(count($prod_validar)){
		$error_man->showWarning("El tipo de venta seleccionado no permite productos que controlen stocks.");
		exit;
	}

	$solicitada = 0;

}else{
	//La cantidad solicitada indica el total de productos que se van a vender (incluyendo todos los detalles)
	$solicitada = array_sum($_POST['cant']);

    /*if(count($_POST['prod']) != count(array_unique($_POST['prod']))){
		if(!in_array($idUsuario, array(15,42,62,69,71,73,127,133,134,135,137,153))){
			$error_man->showWarning("No se permite agregar el mismo código de producto más de una vez en la nota de venta");
			exit;
		}
	}*/
}

/**
*	Comprobar si debe ser automaticamente validada y confirmada segÃºn los margenes
**/
function soloNegativos($element){
	return $element < 0;
}

if(array_filter($_POST['margen'],"soloNegativos")){
	$autoValConf = true;
}else{
	$autoValConf = false;
}

$costoTotal = 0;
$precioTotal = 0;
foreach($_POST['precio'] as $i =>  $p){
	$precioTotal += str_replace(',','',$_POST['precio'][$i])*$_POST['cant'][$i];
	$costoTotal += str_replace(',','',$_POST['costo'][$i])*$_POST['cant'][$i];
}

if($autoValConf){
	if(($precioTotal-$costoTotal)/$precioTotal < 0.05){
		$autoValConf = false;
	}
}

$con_contrato = $_POST['nv_tipo_venta'][2] == '1';

if($con_contrato){
	include_once('validar_crear_contrato.php');
}

$con_proyecto = $_POST['nv_tipo_venta'][1] == '1';

$db->start_transaction();

$validacion = ($empresa_conf['dm_valida_nota_venta']==1 || ($autoValConf && $_POST['dm_validar_nota_venta']))?1:0;
$confirmacion = ($empresa_conf['dm_confirma_nota_venta']==1 || ($autoValConf && $_POST['dm_confirmar_nota_venta']))?1:0;


$nota_venta = $db->insert('tb_nota_venta',array(
	"dq_nota_venta" => $cot_num,
	"dc_tipo_nota_venta" => $tipo_venta,
	"dc_cotizacion" => $_POST['nv_id_cotizacion'],
	"dc_termino_comercial" => $_POST["nv_termino"],
	"dc_contacto" => $_POST["lst_contacto"],
	"dc_ejecutivo" => $_POST["nv_executive"],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"dc_tipo_cambio" => $_POST["prod_tipo_cambio"],
	"dq_cambio" => $_POST["nv_cambio"],
	"dc_modo_envio" => $_POST["nv_modo_envio"],
	"dc_cliente" => $_POST["cli_id"],
    "dc_cliente_final" => $_POST['dc_cliente_final'],
	"df_fecha_emision" => 'NOW()',
	"df_fecha_envio" => $db->sqlDate($_POST["nv_envio"]),
	"dq_iva" => $_POST["cot_iva"],
	"dq_neto" => $precioTotal*$_POST['nv_cambio'],
	"dg_observacion" => substr($_POST['nv_observacion'],0,255),
	"dm_al_dia" => $_POST['nv_aldia'],
	"dc_estado" => $estado,
	"dm_validada" => $validacion,
	"dm_confirmada" => $confirmacion,
	"dc_solicitada" => $solicitada
));

if(isset($_POST['contacto_extra'])){
foreach($_POST['contacto_extra'] as $i => $v){
	if($v != '0'){
		$db->insert("tb_nota_venta_contactos_extra_nota_venta",
		array(
			"dc_nota_venta" => $nota_venta,
			"dc_contacto" => $v,
			"dc_contacto_extra" => $_POST['tipo_cextra'][$i]
		));
	}
}}


	$prod_list = implode(',',$_POST['prod']);
	$aux = $db->select("(SELECT * FROM tb_producto WHERE dc_producto IN ({$prod_list})) p
	LEFT JOIN tb_tipo_producto t ON p.dc_tipo_producto = t.dc_tipo_producto",'p.dc_producto,t.dm_controla_inventario,t.dm_orden_compra,p.dq_precio_compra');

	$stock_change = array();
        $ctrl_stock=array();
        $precio_compra=array();
	foreach($aux as $v){
		$stock_change[$v['dc_producto']] = $v['dm_controla_inventario'] == 1 && $v['dm_orden_compra'] == 0;
                $ctrl_stock[$v['dc_producto']]=$v['dm_controla_inventario'] == 1;
                $precio_compra[$v['dc_producto']]=$v['dq_precio_compra'];
                // si dm_controla_inventario es 0 entonces controla stock.
	}
	//unset($aux);

$no_stock = 0;
foreach($_POST['prod'] as $i => $v){



	$det = array(
		"dc_empresa" => $empresa,
		"dc_nota_venta" => $nota_venta,
		"dc_producto" => $_POST['prod'][$i],
		"dg_descripcion" => $_POST['desc'][$i],
		"dq_cantidad" => $_POST['cant'][$i],
		"dq_precio_venta" => str_replace(',','',$_POST['precio'][$i])*$_POST['nv_cambio'],
		"dq_precio_compra" => str_replace(',','',$_POST['costo'][$i])*$_POST['nv_cambio'],
		"dc_proveedor" => $_POST['proveedor'][$i],
		"dc_cebe" => $_POST['cebe'][$i],
		"dc_ceco" => $_POST['ceco'][$i],
	);
	if(!empty($_POST['df_fecha_arribo'][$i])){
		$det["df_fecha_arribo"] = $db->sqlDate($_POST['df_fecha_arribo'][$i]);
	}
        /*
	if($v['dm_controla_inventario'] == 1)
        {
            $det['dq_pmp_compra']=str_replace(',','',$_POST['costo'][$i]);
        }else{
            $det['dq_pmp_compra']=$aux['dq_precio_compra'];
        }
        --------------------------------------------------------------------
        --------------------------------------------------------------------
        --------------------------------------------------------------------
         */
        if($ctrl_stock[$v])
        {

        $det['dq_pmp_compra']=str_replace(',','',$_POST['costo'][$i]);
        }else{
            $det['dq_pmp_compra']=$precio_compra[$v];
        }


	if($stock_change[$v]){
		$det['dc_comprada'] = $_POST['cant'][$i];
		$det['dc_recepcionada'] = $_POST['cant'][$i];
		$no_stock += $_POST['cant'][$i];
	}

	$db->insert('tb_nota_venta_detalle',$det);
}
if($no_stock > 0){
	$db->update('tb_nota_venta',array('dc_comprada' => $no_stock,'dc_recepcionada' => $no_stock),"dc_nota_venta={$nota_venta}");
}

if(isset($_POST['include_costos']) && $_POST['nv_tipo_venta'][1] == '1'){
	$dc_nota_venta_relacionada = $_POST['include_costos'];
	$db->query("INSERT INTO tb_nota_venta_detalle
	(dc_empresa,dc_nota_venta,dc_producto,dg_descripcion,dq_cantidad,dq_precio_compra,dq_precio_venta,dm_tipo ,dc_proveedor,dc_cebe,dc_ceco)
	SELECT
	{$empresa},{$nota_venta},dc_producto,dg_descripcion,dq_cantidad,dq_precio_compra,dq_precio_venta,1,dc_proveedor,dc_cebe,dc_ceco
	FROM tb_nota_venta_detalle
	WHERE dc_nota_venta = {$dc_nota_venta_relacionada} AND dm_tipo = 1");
}


$contacto = $db->select('tb_contacto_cliente','dg_contacto',"dc_contacto={$_POST['lst_contacto']}");
if(count($contacto)){
	$contacto = $contacto[0]['dg_contacto'];
}else{
	$contacto = 'N/A';
}

$db->insert('tb_nota_venta_avance',
array(
	"dc_nota_venta" => $nota_venta,
	"dg_contacto" => $contacto,
	"dg_gestion" => 'Creación',
	"df_creacion" => 'NOW()',
	"df_proxima_actividad" => "ADDDATE(NOW(),{$empresa_conf['dn_nota_venta_dias_actividad']})",
	"dc_estado_nota_venta" => $estado
));

if($con_contrato){
	include_once('crear_contrato.php');
}

if($con_proyecto){
  include_once 'crear_proyecto.php';
}

$db->commit();

$error_man->showConfirm("Se ha generado la nota de venta");
echo("<div class='title'>El número de nota de venta es <h1 style='margin:0;color:#000;'>
	<a href='sites/ventas/nota_venta/proc/src_nota_venta.php?nv_numero_desde={$cot_num}' id='load_gestor'>{$cot_num}</a>
</h1></div>");

$error_man->showConfirm("Fueron agregados ".count($_POST['prod'])." elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/ventas/nota_venta/ver_nota_venta.php?id={$nota_venta}','nota_venta','width=800,height=600')\"> Haga clic aquÃ­ para ver la nota de venta</a>");
?>
<script type="text/javascript">
$('#load_gestor').click(function(e){
	e.preventDefault();
	loadFile($(this).attr('href'),'#cr_nota_venta_res','',function(){
		$('#cr_nota_venta_res').attr('id','#src_nota_venta');
		$('#cr_nota_venta').prev('input').remove();
		$('#cr_nota_venta').remove();
	});
});
</script>
