<?php

	$dq_contrato = doc_GetNextNumber('tb_nota_venta_contrato','dq_contrato');
	
	$dc_meses_contrato = intval($_POST['dc_periodo'])*$dc_cuotas;
	
	$df_termino = "DATE_ADD(".$db->sqlDate($_POST['df_inicio']).", INTERVAL {$dc_meses_contrato} MONTH)";
	
	$dm_financiamiento = intval($_POST['dm_financiamiento']);
	
	//Almacenar Contrato
	$dc_contrato = $db->insert('tb_nota_venta_contrato',array(
		'dq_contrato' => $dq_contrato,
		'dc_nota_venta' => $nota_venta,
		'df_emision' => 'NOW()',
		'df_inicio' => $db->sqlDate($_POST['df_inicio']),
		'df_termino' => $df_termino,
		'dc_interes' => $dc_interes_contrato,
		'dc_cuotas' => $dc_cuotas,
		'dq_cuota' => $dq_cuota*$_POST['nv_cambio'],
		'dm_tasa_cambio' => $_POST['dm_tasa_cambio'],
		'dm_financiamiento' => $dm_financiamiento,
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario,
		'dg_patron_facturacion' => $_POST['dg_patron']
	));
	
	//Almacenar datos de financiamiento
	if($dm_financiamiento == 1){
		
		$dc_banco = intval($_POST['dc_banco']);
		$dq_cuota_banco = floatval($_POST['dq_cuota_banco']);
		$df_inicio_banco = $db->sqlDate($_POST['df_inicio_banco']);
		$df_termino_banco = $db->sqlDate($_POST['df_termino_banco']);
		$dc_interes_banco = floatval($_POST['dc_interes_banco']);
		
		$db->insert('tb_contrato_financiamiento_banco',array(
			'dc_contrato' => $dc_contrato,
			'dc_banco' => $dc_banco,
			'dq_cuota' => $dq_cuota_banco,
			'df_inicio' => $df_inicio_banco,
			'df_termino' => $df_termino_banco,
			'dc_interes' => $dc_interes_banco
		));
	}
	
	//Almacenar detalles del contrato
	for($i = 0; $i < $dc_cuotas; $i++){
		
		$dc_periodo_vencimiento = intval($_POST['dc_periodo'])*$i;
		$df_vencimiento = "DATE_ADD(".$db->sqlDate($_POST['df_inicio']).", INTERVAL {$dc_periodo_vencimiento} MONTH)";
		
		foreach($_POST['dc_detalle_concepto'] as $j => $dc_detalle){
			
			$dq_cuota_detalle = floatval($_POST['dq_cuota_concepto'][$j]*$_POST['nv_cambio']);
			
			$db->insert('tb_nota_venta_contrato_detalle',array(
				'dc_tipo' => $dc_detalle,
				'dc_contrato' => $dc_contrato,
				'dq_cuota' => $dq_cuota_detalle,
				'dc_numero_cuota' => $i+1,
				'df_vencimiento' => $df_vencimiento
			));
			
		}
		
	}

?>