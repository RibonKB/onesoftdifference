<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$poblacion = array(
	'termino' => '',
	'tipo_cambio' => '',
	'cambio' => '',
	'modo_envio' => '',
	'observacion' => '',
	'ejecutivo' => $userdata['dc_funcionario']
);

if(isset($_POST['nv_base_num']) && $_POST['nv_base_num']){

	//Se escapan los caracteres por seguridad
	$db->escape($_POST['nv_base_num']);

	$base = $db->select('tb_nota_venta',
	'dc_nota_venta,dc_termino_comercial,dc_ejecutivo,dc_tipo_cambio,dq_cambio,dc_modo_envio,df_fecha_emision,dg_observacion',
	"dq_nota_venta={$_POST['nv_base_num']} AND dc_empresa = {$empresa}");

	if(!count($base)){
		$error_man->showErrorRedirect("No se encontró la nota de venta especificada como plantilla, intentelo nuevamente.",
		"sites/ventas/nota_venta/cr_nota_venta.php");
	}
	$base = $base[0];

	$poblacion = array(
		'termino' => $base['dc_termino_comercial'],
		'tipo_cambio' => $base['dc_tipo_cambio'],
		'cambio' => number_format($base['dq_cambio'],$empresa_conf['dn_decimales_local']),
		'modo_envio' => $base['dc_modo_envio'],
		'observacion' => $base['dg_observacion'],
		'ejecutivo' => $base['dc_ejecutivo']
	);

	$detalles_base = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta={$base['dc_nota_venta']} AND dm_tipo=0) d
	LEFT JOIN (SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo='1') p ON p.dc_producto = d.dc_producto",
	'p.dc_producto,d.dq_cantidad,d.dg_descripcion,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,p.dg_codigo,d.dc_cebe,d.dc_ceco');

}

if(isset($_POST['cli_rut'])){
	//Se escapan los caracteres por seguridad
	if($idUsuario == 145){
		$ejecutivo_cond = 'AND dc_ejecutivo = 182';
	}else{
		$ejecutivo_cond = '';
	}
	$datos = $db->select("tb_cliente","dc_cliente,dg_razon,dc_contacto_default,dm_validar_nota_venta,dm_confirmar_nota_venta",
	"dg_rut = '{$_POST['cli_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1' {$ejecutivo_cond}");

	if(!count($datos)){
		$error_man->showErrorRedirect("No se encontró el cliente especificado, intentelo nuevamente.","sites/ventas/nota_venta/cr_nota_venta.php");
	}
	$datos = $datos[0];
	$poblacion['cotizacion'] = '';
}else if(isset($_POST['cli_id'])){
	$datos = $db->select("tb_cliente",'dc_cliente,dg_razon,dc_contacto_default,dm_validar_nota_venta,dm_confirmar_nota_venta',
	"dc_cliente={$_POST['cli_id']} AND dc_empresa={$empresa} AND dm_activo='1'");

	if(!count($datos)){
		$error_man->showErrorRedirect("Error: Cliente inválido, intentelo nuevamente.","sites/ventas/oportunidad/src_oportunidad.php");
	}
	$datos = $datos[0];

	$poblacion['cotizacion'] =& $_POST['cot_numero'];
}else{
	exit;
}

echo("<div id='secc_bar'>Creación de nota de venta</div>
<div id='main_cont'>
<div class='panes' style='width:1140px;'>
<div class='title center'>
<button class='right button' id='cli_switch'>Cambiar Cliente</button>
Generando nota de venta para <strong id='cli_razon' style='color:#000;'>{$datos['dg_razon']}</strong>
</div>");

	include_once("../../../../inc/form-class.php");
	$form = new Form($empresa);

	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio,dn_cantidad_decimales",
	"dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$c['dq_cambio'] = number_format($c['dq_cambio'],$c['dn_cantidad_decimales'],'.',',');
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |{$c['dq_cambio']}|";
		$form->Hidden("decimal{$c['dc_tipo_cambio']}",$c['dn_cantidad_decimales']);
	}

	$form->Start("sites/ventas/nota_venta/proc/crear_nota_venta.php","cr_nota_venta",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la nota de venta</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();
	$form->Listado('Contacto','lst_contacto',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');

    $form->Listado('Cliente final', 'dc_cliente_final', 'tb_cliente', array('dc_cliente','dg_razon'), true, $datos['dc_cliente']);
	$form->Listado('Tipo de venta','nv_tipo_venta','tb_tipo_nota_venta',array('CONCAT_WS("|",dc_tipo,dm_costeable,dm_contratable)','dg_tipo'),1);
	$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1,$poblacion['tipo_cambio']);
	$form->Text('Cambio','nv_cambio',1,255,$poblacion['cambio']);
	$form->EndSection();

	$form->Section();
	$form->Date("Fecha envio","nv_envio",0,1);
	$form->Listado("Modo de envio","nv_modo_envio","tb_modo_envio",array("dc_modo_envio","dg_modo_envio"),0,$poblacion['modo_envio']);
	$form->Listado('Ejecutivo','nv_executive','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),1,$poblacion['ejecutivo'],"tf.dc_empresa = {$empresa} AND dc_modulo = 1");
	$form->Text('Cotización Número','nv_cotizacion',$empresa_conf['dm_wf_op_requerida'],255,$poblacion['cotizacion']);
	$form->Hidden('nv_id_cotizacion',0);
    $form->Listado("Términos comerciales","nv_termino","tb_termino_comercial",array("dc_termino_comercial","dg_termino_comercial"),1,$poblacion['termino']);
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','nv_observacion',0,$poblacion['observacion']);
	$form->File("Orden de compra adjunta<br /><small>(Formatos soportados: pdf, xls, xlsx, jpg, bmp, doc, png, gif, odf)</small>","nv_orden_compra");
	$form->Radiobox('','nv_aldia',array('Tipo de cambio al día','Tipo de cambio al día de facturación'),0);
	echo('<br />');

	echo('<div id="contrato_container"></div><div id="contrato_data_container"></div>');
    echo('<div id="proyecto_container"></div><div id="proyecto_data_container"></div>');

	if(isset($_POST['nv_base_num']) && $_POST['nv_base_num']){
		echo("<br /><label>
		<input type='checkbox' name='include_costos' value='{$base['dc_nota_venta']}' />
		Incluir los detalles de costo de la nota de venta relacionada
		</label>");
	}

	$form->EndSection();

	$form->Group();

	$c_extra = $db->select(
	"tb_nota_venta_contacto_extra",
	"dg_etiqueta,dc_tipo_direccion,dc_contacto_extra",
	"dc_empresa = {$empresa} AND dm_activo = '1'"
	);
	foreach($c_extra as $ex){

		$form->Section();
		$form->Hidden('tipo_cextra[]',$ex['dc_contacto_extra']);
		$form->Listado($ex['dg_etiqueta'],'contacto_extra[]',"(SELECT * FROM tb_cliente_sucursal_tipo WHERE dc_tipo_direccion = {$ex['dc_tipo_direccion']}) st
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = st.dc_sucursal
		JOIN tb_contacto_cliente c ON c.dc_sucursal = s.dc_sucursal
		LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),0,0,'');
		$form->EndSection();
	}

	$form->Header("Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la nota de venta",'id="prod_info"');
	echo("<div id='prods' style='display:none;'>
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='80'>Opciones</th>
		<th width='95'>Código (Autocompleta)</th>
		<th>Descripción</th>
		<th width='115'>Proveedor</th>
		<th width='115'>CeBe</th>
		<th width='63'>Cantidad</th>
		<th width='85'>Precio</th>
		<th width='85'>Total</th>
		<th width='85'>Costo</th>
		<th width='85'>Costo total</th>
		<th width='85'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='6' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center'><span id='total_margen_p'></span>%</th>
	</tr>
	<tr>
		<th align='right' colspan='6'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th align='right' colspan='2'>Margen Tipo Cambio</th>
		<th align='right' id='total_margen_tc'>0</th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='6'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='6'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>");

    $form->Group();
	$form->Hidden('cli_id',$datos['dc_cliente']);
	$form->Hidden('dm_validar_nota_venta',$datos['dm_validar_nota_venta']);
	$form->Hidden('dm_confirmar_nota_venta',$datos['dm_confirmar_nota_venta']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->End('Crear','addbtn');

	echo("<table id='prods_form' style='display:none;'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/cal.png' alt='' title='Fecha de Arribo/Renovacion Estimada' class='set_fecha_arribo' />
			<input type='hidden' class='fecha_arribo' name='df_fecha_arribo[]' />
		</td>
		<td>
			<input type='text' class='prod_codigo searchbtn' size='7' />
		</td>
		<td>
			<input type='text' name='desc[]' class='prod_desc inputtext' size='17' required='required' />
		</td>
		<td><select name='proveedor[]' style='width:110px;' class='prod_proveedor inputtext' required='required'><option></option>");
		$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'");
		foreach($proveedores as $p){
			echo("<option value='{$p['dc_proveedor']}'>{$p['dg_razon']}</option>");
		}
		echo("</select></td>
		<td><select name='cebe[]' style='width:110px;' class='prod_cebe inputtext' required='required'><option value='0' id='0'></option>");
		$cebes = $db->select('tb_cebe','dc_cebe,dg_cebe,dc_ceco',"dc_empresa={$empresa} AND dm_activo ='1'");
		foreach($cebes as $p){
			echo("<option value='{$p['dc_cebe']}' id='{$p['dc_ceco']}'>{$p['dg_cebe']}</option>");
		}
		echo("</select>
		<input type='hidden' class='prod_ceco' name='ceco[]' value='0'>
		</td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input name='margen[]' type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
	</tr>
	<tr style='display:none;'>
		<td colspan='6' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_price' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_margen' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>
	</table>");
?>
</div></div>
<script type="text/javascript" src="jscripts/sites/ventas/product_manager.js?v=2_5b"></script>
<script type="text/javascript" src="jscripts/sites/ventas/nota_venta/cr_nota_venta.js?v=2_5b"></script>
<script type="text/javascript" src="jscripts/sites/ventas/product_manager_with_exchange.js?v=2_2b"></script>
<script type="text/javascript" src="jscripts/sites/ventas/nota_venta/cr_contrato.js?v=0_5b"></script>
<script type="text/javascript" src="jscripts/sites/ventas/nota_venta/cr_proyecto.js?v=0_1b"></script>
<script type="text/javascript">
<?php if(isset($detalles_base)): ?>
	js_data.detallesBase = <?php echo json_encode($detalles_base) ?>;
<?php endif; ?>
<?php include_once('../../../../inc/Factory.class.php');?>
	var setFechaArriboProducto = '<?php echo Factory::buildUrl('CrearNotaVenta', 'ventas', 'setFechaArriboProducto', 'nota_venta'); ?>';
	js_data.init();
	cr_contrato.init();
    cr_proyecto.init();
</script>
