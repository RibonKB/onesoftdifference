<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Obtener valor tipo de cambio
$db->escape($_POST['sel_tipo_cambio']);
$dq_cambio = $db->select('tb_tipo_cambio','dg_tipo_cambio,dq_cambio,dn_cantidad_decimales',"dc_tipo_cambio={$_POST['sel_tipo_cambio']}");
if(!count($dq_cambio)){
	$error_man->showAviso("El tipo de cambio especificado es inválido, en su lugar se utiliza moneda local");
	$dm_decimales = $empresa_conf['dn_decimales_local'];
	$dg_tipo_cambio = $empresa_conf['dg_moneda_local'];
	$dq_cambio = 1;
}else{
	$dm_decimales = $dq_cambio[0]['dn_cantidad_decimales'];
	$dg_tipo_cambio = $dq_cambio[0]['dg_tipo_cambio'];
	$dq_cambio = $dq_cambio[0]['dq_cambio'];
}

$db->escape($_POST['sel_inim']);
$db->escape($_POST['sel_finm']);
$db->escape($_POST['sel_anho']);
$date_condition = "MONTH(nvi.df_fecha_emision) >= {$_POST['sel_inim']} AND MONTH(nvi.df_fecha_emision) <= {$_POST['sel_finm']} AND YEAR(nvi.df_fecha_emision) = {$_POST['sel_anho']}";

$date_contrato_condition = "MONTH(cd.df_vencimiento) >= {$_POST['sel_inim']} AND MONTH(cd.df_vencimiento) <= {$_POST['sel_finm']} AND YEAR(cd.df_vencimiento) = {$_POST['sel_anho']}";

$sqlCotizacion = $db->select(
	"(SELECT t.dm_contratable, t.dm_costeable, nvi.*
		FROM tb_nota_venta nvi 
		JOIN tb_tipo_nota_venta t ON t.dc_tipo = nvi.dc_tipo_nota_venta
		WHERE nvi.dm_confirmada = 1 AND nvi.dc_empresa = {$empresa} AND nvi.dm_nula=0 AND (({$date_condition}) OR (t.dm_contratable = 1))
	) nv
	LEFT JOIN tb_cotizacion cot ON cot.dc_cotizacion = nv.dc_cotizacion
	JOIN tb_funcionario f ON f.dc_funcionario = nv.dc_ejecutivo
	JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
	LEFT JOIN (
		SELECT cnv.dc_nota_venta, MAX(cd.dc_numero_cuota)-MIN(cd.dc_numero_cuota)+1 dc_cuotas
		FROM tb_nota_venta_contrato cnv
		JOIN tb_nota_venta_contrato_detalle cd ON cd.dc_contrato = cnv.dc_contrato
		WHERE {$date_contrato_condition}
		GROUP BY cnv.dc_nota_venta
	) cn ON cn.dc_nota_venta = nv.dc_nota_venta
	JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta
	LEFT JOIN tb_proveedor prov ON prov.dc_proveedor = d.dc_proveedor
	LEFT JOIN tb_factura_venta_detalle fvd ON fvd.dc_detalle_nota_venta = d.dc_nota_venta_detalle
	LEFT JOIN tb_factura_venta fv ON fv.dc_factura = fvd.dc_factura
	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	LEFT JOIN tb_marca m ON m.dc_marca = p.dc_marca
	LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio",
"nv.dc_ejecutivo, nv.dq_nota_venta, nv.df_fecha_emision, nv.dq_neto, cot.dq_cotizacion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno,cl.dc_cliente, cl.dg_razon, m.dc_marca, m.dg_marca, d.dg_descripcion, p.dg_codigo as dg_producto,d.dq_precio_venta, d.dq_precio_compra, d.dq_cantidad, ln.dc_linea_negocio, ln.dg_linea_negocio,
(d.dq_precio_venta*d.dq_cantidad) dq_precio_total, (d.dq_precio_compra*d.dq_cantidad) dq_costo_total, (d.dq_precio_venta-d.dq_precio_compra)*d.dq_cantidad dq_margen,
nv.dm_contratable, cn.dc_cuotas, GROUP_CONCAT(fv.dq_folio SEPARATOR ',<br />') dg_factura_venta, nv.dm_costeable, d.dm_tipo dm_tipo_detalle, prov.dg_razon dg_proveedor",'',array(
	'order_by' => 'dq_margen',
	'group_by' => 'd.dc_nota_venta_detalle'
));

$ejecutivos = array();
$estados = array();
$marcas = array();
$lineas = array();

foreach($sqlCotizacion as $i => $cot){
	
    if($cot['dm_costeable'] == 1 && $cot['dm_contratable'] == 0 && $cot['dm_tipo_detalle'] == 1){
		unset($sqlCotizacion[$i]);
        continue;
    }else if($cot['dm_costeable'] == 1 && $cot['dm_contratable'] == 1 && $cot['dm_tipo_detalle'] == 1){
		unset($sqlCotizacion[$i]);
        continue;
    }
  
	if($cot['dm_contratable'] == 1){
		
		if($cot['dc_cuotas'] == NULL || $cot['dc_cuotas'] == 0){
			unset($sqlCotizacion[$i]);
			continue;
		}
		
		$sqlCotizacion[$i]['dq_precio_total'] = $cot['dq_precio_total'] = ($cot['dq_precio_total']/$cot['dq_cantidad'])*$cot['dc_cuotas'];
		$sqlCotizacion[$i]['dq_costo_total'] = $cot['dq_costo_total'] = ($cot['dq_costo_total']/$cot['dq_cantidad'])*$cot['dc_cuotas'];
		$sqlCotizacion[$i]['dq_margen'] = $cot['dq_margen'] = ($cot['dq_margen']/$cot['dq_cantidad'])*$cot['dc_cuotas'];
		$sqlCotizacion[$i]['dq_cantidad'] = $cot['dq_cantidad'] = $cot['dc_cuotas'];
		
	}
	
	if($cot['dc_marca'] == NULL){
		
		$sqlCotizacion[$i]['dc_marca'] = 0;
		$sqlCotizacion[$i]['dg_marca'] = 'Sin Marca';
		
		$marcas[0]['nombre'] = 'Sin Marca';
		$marcas[0]['monto'][] = $cot['dq_precio_total'];
	}else{
		$marcas[$cot['dc_marca']]['nombre'] = $cot['dg_marca'];
		$marcas[$cot['dc_marca']]['monto'][] = $cot['dq_precio_total'];
	}
	
	if($cot['dc_linea_negocio'] == NULL){
		
		$sqlCotizacion[$i]['dc_linea_negocio'] = 0;
		$sqlCotizacion[$i]['dg_linea_negocio'] = 'Sin Linea de negocio';
		
		$lineas[0]['nombre'] = 'Sin Linea de negocio';
		$lineas[0]['monto'][] = $cot['dq_neto'];
	}else{
		$lineas[$cot['dc_linea_negocio']]['nombre'] = $cot['dg_linea_negocio'];
		$lineas[$cot['dc_linea_negocio']]['monto'][] = $cot['dq_neto'];
	}
	
	if($cot['dq_cotizacion'] == NULL){
		$sqlCotizacion[$i]['dq_cotizacion'] = 'Sin definir';
	}
	
	$ejecutivos[$cot['dc_ejecutivo']]['nombre_parcial'] = $cot['dg_nombres'].' '.substr($cot['dg_ap_paterno'],0,1).'.';
	$ejecutivos[$cot['dc_ejecutivo']]['monto'][] = $cot['dq_precio_total'];
	
	$sqlCotizacion[$i]['dq_precio_venta'] = $sqlCotizacion[$i]['dq_precio_venta']/$dq_cambio;
	$sqlCotizacion[$i]['dq_precio_compra'] = $sqlCotizacion[$i]['dq_precio_compra']/$dq_cambio;
	$sqlCotizacion[$i]['dq_precio_total'] = $sqlCotizacion[$i]['dq_precio_total']/$dq_cambio;
	$sqlCotizacion[$i]['dq_costo_total'] = $sqlCotizacion[$i]['dq_costo_total']/$dq_cambio;
	$sqlCotizacion[$i]['dq_margen'] = $sqlCotizacion[$i]['dq_margen']/$dq_cambio;
}

$percentage = array();
foreach($ejecutivos as $ex){
	$percentage[] = array(
		'label' => $ex['nombre_parcial'],
		'data' => round(array_sum($ex['monto'])/$dq_cambio,$dm_decimales)
	);
}

$p_linea = array();
foreach($lineas as $l){
	$p_linea[] = array(
		'label' => $l['nombre'],
		'data' => round(array_sum($l['monto'])/$dq_cambio,$dm_decimales)
	);
}

$p_marca = array();
foreach($marcas as $m){
	$p_marca[] = array(
		'label' => $m['nombre'],
		'data' => round(array_sum($m['monto'])/$dq_cambio,$dm_decimales)
	);
}

$sqlPeriodos = $db->select('tb_nota_venta nv
JOIN tb_funcionario f ON f.dc_funcionario = nv.dc_ejecutivo',
'nv.dc_ejecutivo , f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno ,MONTH(nv.df_fecha_emision) df_mes_emision, SUM(nv.dq_neto) dq_neto_total',
"nv.dc_empresa = {$empresa} AND YEAR(nv.df_fecha_emision) = {$_POST['sel_anho']}",array('group_by' => 'nv.dc_ejecutivo, df_mes_emision'));

$periodos = array();
foreach($sqlPeriodos as $p){
	$periodos[$p['dc_ejecutivo']]['puntos'][] = array(intval($p['df_mes_emision']),floatval($p['dq_neto_total']));
	$periodos[$p['dc_ejecutivo']]['nombre_completo'] = $p['dg_nombres'].' '.substr($p['dg_ap_paterno'],0,1).'.';
}
unset($sqlPeriodos);

?>

<div id="ex_list"></div><br />
<div id="grafico_puntos" style="width:900px;height:450px;float:left;"></div>

<div id="grafico_torta" style="width:450px;height:300px;float:left;"></div>

<div id="grafico_torta_linea" style="width:450px;height:300px;float:left;"></div>

<!--div id="grafico_torta_estado" style="width:450px;height:300px;float:left;"></div-->

<div id="grafico_torta_marca" style="width:450px;height:300px;float:left;"></div>

<hr class="clear" />
<div id="data_table"></div>
<br class="clear" />
<div id="detail_data_table">

<table class="tab" width="2500" id="completeData">
<thead><tr>
	<th>Nota de venta</th>
	<th>Fecha emisión</th>
	<th>Cotización</th>
	<th>Ejecutivo</th>
	<th>Cliente</th>
	<th>Marca</th>
    <th>Proveedor</th>
	<th>Descripción</th>
	<th>Código</th>
	<th>Precio Unitario</th>
	<th>Costo unitario</th>
	<th>Cantidad</th>
	<th>Precio total</th>
	<th>Costo total</th>
	<th>Margen</th>
	<th>Factura(s)</th>
</tr></thead>
<tbody>
<?php foreach($sqlCotizacion as $nv): ?>
	<tr>
		<td><?php echo $nv['dq_nota_venta'] ?></td>
		<td><?php echo $db->dateTimeLocalFormat($nv['df_fecha_emision']) ?></td>
		<td><?php echo $nv['dq_cotizacion'] ?></td>
		<td class="dc_ejecutivo<?php echo $nv['dc_ejecutivo'] ?>"><?php echo $nv['dg_nombres'].' '.$nv['dg_ap_paterno'].' '.$nv['dg_ap_materno'] ?></td>
		<td class="dc_cliente<?php echo $nv['dc_cliente'] ?>"><?php echo $nv['dg_razon'] ?></td>
		<td class="dc_marca<?php echo $nv['dc_marca'] ?>"><?php echo $nv['dg_marca'] ?></td>
        <td><?php echo $nv['dg_proveedor'] ?></td>
		<td><?php echo $nv['dg_descripcion'] ?></td>
		<td><?php echo $nv['dg_producto'] ?></td>
		<td align="right"><?php echo moneda_local($nv['dq_precio_venta'],$dm_decimales) ?></td>
		<td align="right"><?php echo moneda_local($nv['dq_precio_compra'],$dm_decimales) ?></td>
		<td align="center"><?php echo $nv['dq_cantidad'] ?></td>
		<td align="right"><?php echo moneda_local($nv['dq_precio_total'],$dm_decimales) ?></td>
		<td align="right"><?php echo moneda_local($nv['dq_costo_total'],$dm_decimales) ?></td>
		<td align="right"><?php echo moneda_local($nv['dq_margen'],$dm_decimales) ?></td>
		<td><?php echo $nv['dg_factura_venta'] ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
<tfoot><tr>
	<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
	<th align="right"></th>
	<th align="right"></th>
	<th></th>
	<th align="right"></th>
	<th align="right"></th>
	<th align="right"></th>
	<th></th>
</tr></tfoot>
</table>

</div>

<script type="text/javascript">
	js_data.piePlotData = <?php echo json_encode($percentage) ?>;
	js_data.piePlotMarca = <?php echo json_encode($p_marca) ?>;
	js_data.piePlotLinea = <?php echo json_encode($p_linea) ?>;
	js_data.periodPlotData = <?php echo json_encode($periodos) ?>;
	
	js_data.cotizacionData = <?php echo json_encode($sqlCotizacion) ?>;
	js_data.tcDecimals = <?php echo $dm_decimales ?>;
	$('#completeData').tableExport();
</script>