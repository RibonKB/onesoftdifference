<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['mode'])){
	$db->update('tb_nota_venta',array('dm_validada' => 1,'dc_usuario_validacion' => $idUsuario,'df_validacion'=>'NOW()'),"dc_nota_venta={$_POST['id']}");
	$error_man->showConfirm('Nota de venta validada correctamente');
}else{
	$db->update('tb_nota_venta',array('dm_validada' => 0,'dc_usuario_validacion' => 0,'df_validacion'=>'0'),"dc_nota_venta={$_POST['id']}");
	$error_man->showAviso('Se ha desvalidado la nota de venta');
}

?>
<script type="text/javascript">
	$(".confirm .nv_load").click();
</script>