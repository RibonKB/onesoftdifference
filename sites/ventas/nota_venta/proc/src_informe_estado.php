<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['nv_emision_desde'] = $db->sqlDate($_POST['nv_emision_desde']);
$_POST['nv_emision_hasta'] = $db->sqlDate($_POST['nv_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['nv_emision_desde']} AND {$_POST['nv_emision_hasta']}) AND dm_nula=0";

if($_POST['nv_numero_desde']){
	if($_POST['nv_numero_hasta']){
		$conditions .= " AND (dq_nota_venta BETWEEN {$_POST['nv_numero_desde']} AND {$_POST['nv_numero_hasta']})";
	}else{
		$conditions .= " AND dq_nota_venta = {$_POST['nv_numero_desde']}";
	}
}

if(isset($_POST['nv_client'])){
	$_POST['nv_client'] = implode(',',$_POST['nv_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['nv_client']})";
}

if(isset($_POST['nv_executive'])){
	$_POST['nv_executive'] = implode(',',$_POST['nv_executive']);
	$conditions .= " AND dc_ejecutivo IN ({$_POST['nv_executive']})";
}

$data = $db->select("(SELECT * FROM tb_nota_venta WHERE {$conditions}) nv
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
JOIN tb_nota_venta_detalle nvd ON nv.dc_nota_venta = nvd.dc_nota_venta
LEFT JOIN tb_tipo_cambio tc ON nv.dc_tipo_cambio = tc.dc_tipo_cambio
LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = nv.dc_ejecutivo",
'nv.dc_nota_venta,
    nv.dq_nota_venta,
    DATE_FORMAT(nv.df_fecha_emision,"%d/%m/%Y %H:%i") AS df_emision,
    nv.dq_cambio,nv.dm_validada,nv.dm_confirmada,
SUM(nvd.dq_cantidad) dc_solicitada,
SUM(nvd.dc_facturada) dc_facturada,
SUM(nvd.dc_comprada) dc_comprada,
SUM(nvd.dc_recepcionada) dc_recepcionada,
SUM(nvd.dc_despachada) dc_despacho,
tc.dg_tipo_cambio,cl.dg_razon, 
SUM(nvd.dq_precio_venta*nvd.dq_cantidad) dq_neto,
CONCAT_WS(" ",ex.dg_nombres,ex.dg_ap_paterno,ex.dg_ap_materno) dg_ejecutivo,
SUM(nvd.dq_precio_compra*nvd.dq_cantidad) dq_costo_total,
SUM(nvd.dc_rutada) dc_rutada',''
,array('order_by' => 'df_fecha_emision DESC', 'group_by' => 'nv.dc_nota_venta'));

if(!count($data)){
	$error_man->showAviso("No se encontraron notas de venta con los criterios especificados");
	exit();
}

$filter_status = '<select id="filter_status" style="width:100px;">
	<option value="0">Todos</option>
	<option value="1">Pendiente</option>
	<option value="2">Aprobada</option>
	<option value="3">Completada</option>
</select>';

$filter_comprar = '<select id="filter_comprar" style="width:100px;">
	<option value="0">Todos</option>
	<option value="1">Sin comprar</option>
	<option value="2">Comprada Parcial</option>
	<option value="3">Comprada</option>
</select>';

$filter_facturar = '<select id="filter_facturar" style="width:100px;">
	<option value="0">Todos</option>
	<option value="1">Sin facturar</option>
	<option value="2">Facturada Parcial</option>
	<option value="3">Facturada</option>
</select>';

$filter_despachar = '<select id="filter_comprar" style="width:100px;">
	<option value="0">Todos</option>
	<option value="1">Sin despachar</option>
	<option value="2">Despachada Parcial</option>
	<option value="3">despachada</option>
</select>';

echo('<table width="100%" class="tab" id="informe_estado">
<caption>Resultados de la búsqueda</caption>
<thead>
<tr>
	<th>Nota de Venta</th>
	<th>Fecha emisión</th>
	<th>Estado Workflow</th>
	<th>Cliente</th>
	<th>Ejecutivo</th>
	<th width="90">Monto Neto</th>
	<th width="90">Costo</th>
	<th width="90">Margen</th>
	<th>Margen Porcentual</th>
	<th width="90">Comprado</th>
	<th width="90">Recepcionado</th>
        <th width="90">Despachado</th>
	<th width="90">Facturado</th>
	<th width="90">Ruta</th>
        
</tr></thead><tbody>');

$total = 0;
$total_costo = 0;
$margen_total = 0;
$cambio = $_POST['nv_tipo_cambio'];
foreach($data as $d){
	$total += $d['dq_neto'];
	$total_costo += $d['dq_costo_total'];
	$margen_total += $d['dq_neto']-$d['dq_costo_total'];
	
	if($d['dq_neto'] > 0)
		$margen_p = moneda_local(($d['dq_neto']-$d['dq_costo_total'])*100/$d['dq_neto'],2).'%';
	else
		$margen_p = '0%';
	$margen = moneda_local(($d['dq_neto']-$d['dq_costo_total'])/$cambio);
	$d['dq_neto'] = moneda_local($d['dq_neto']/$cambio);
	$d['dq_cambio'] = moneda_local($d['dq_cambio']);
	$d['dq_costo_total'] = moneda_local($d['dq_costo_total']/$cambio);
	
	if($d['dm_validada'] == 0 || $d['dm_confirmada'] == 0){
		$wokflow = 'Pendiente';
	}else if($d['dc_solicitada'] == $d['dc_comprada'] && $d['dc_solicitada'] == $d['dc_recepcionada'] && $d['dc_solicitada'] == $d['dc_facturada']){
		$wokflow = 'Completada';
	}else{
		$wokflow = 'Aprobada';
	}
	
	$comprar = $d['dc_solicitada']-$d['dc_comprada'];
	
	if($comprar == 0){
		$comprar = 'Comprada';
	}else if($comprar == $d['dc_solicitada']){
		$comprar = 'Sin compra';
	}else{
		$comprar = 'Compra Parcial';
	}
	
	$recepcionar = $d['dc_solicitada']-$d['dc_recepcionada'];
	
	if($recepcionar == 0){
		$recepcionar = 'Recepcionada';
	}else if($recepcionar == $d['dc_solicitada']){
		$recepcionar = 'Sin recepcionar';
	}else{
		$recepcionar = 'Recepción Parcial';
	}
	
	$facturar = $d['dc_solicitada']-$d['dc_facturada'];
	
	if($facturar == 0){
		$facturar = 'Facturada';
	}else if($facturar == $d['dc_solicitada']){
		$facturar = 'Sin Facturación';
	}else{
		$facturar = 'Facturada Parcial';
	}
	
	$rutar = $d['dc_solicitada']-$d['dc_rutada'];
	
	if($rutar == 0){
		$rutar = 'Entregada';
	}else if($rutar == $d['dc_solicitada']){
		$rutar = 'Sin Ruta';
	}else{
		$rutar = 'En Ruta';
	}
        
        $solicitado=$d['dc_solicitada'];
        $desp=$d['dc_despacho'];
        if($desp==0){
            $despacho='Sin Despachar';
        }elseif($desp < $solicitado){
            $despacho='Despacho Parcial';
        }elseif($desp == $solicitado){
            $despacho='Despacho Completo';
        }
	
	$fecha_corta = substr($d['df_emision'],0,10);
echo("<tr>
	<td><b title='Fecha emisión: {$d['df_emision']}'>
		<a href='sites/ventas/nota_venta/proc/show_nota_venta.php?id={$d['dc_nota_venta']}' class='loadOnOverlay'>{$d['dq_nota_venta']}</a>
	</b></td>
	<td>{$d['df_emision']}</td>
	<td align='left'><b>{$wokflow}</b></td>
	<td align='left'>{$d['dg_razon']}</td>
	<td align='left'>{$d['dg_ejecutivo']}</td>
	<td align='right'>{$d['dq_neto']}</td>
	<td align='right'>{$d['dq_costo_total']}</td>
	<td align='right'>{$margen}</td>
	<td>{$margen_p}</td>
	<td align='left'>{$comprar}</td>
	<td align='left'>{$recepcionar}</td>
        <td align='left'>{$despacho}</td>
	<td align='left'>{$facturar}</td>
	<td align='left'>{$rutar}</td>
        
</tr>");
}

$margen_total_p = moneda_local(($total-$total_costo)*100/$total_costo,2).'%';
$total = moneda_local($total/$cambio);
$total_costo = moneda_local($total_costo/$cambio);
$margen_total = moneda_local($margen_total/$cambio);

echo("</tbody>
<tfoot><tr>
	<th align='right' colspan='4'>Totales : </th>
	<th align='right'>{$total}</th>
	<th align='right'>{$total_costo}</th>
	<th align='right'>{$margen_total}</th>
	<th>{$margen_total_p}</th>
	<th colspan='6'></th>
</tr></tfoot>
</table>");

?>
<script type="text/javascript">
$('.loadOnOverlay').click(function(e){
	e.preventDefault();
	loadOverlay(this.href);
});
$('#informe_estado').tableExport().tableAdjust(10);
</script>