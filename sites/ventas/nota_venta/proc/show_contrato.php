<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_nota_venta = intval($_POST['dc_nota_venta']);

//Obtener datos de cabecera de contratos
$contratos = $db->prepare($db->select('tb_nota_venta_contrato',
						'dc_contrato, dq_contrato, df_emision, df_inicio, df_termino, dc_interes, dc_cuotas, dq_cuota, dm_financiamiento',
						'dc_nota_venta = ? AND dc_empresa = ?'));
$contratos->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$contratos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($contratos);
$contratos = $contratos->fetchAll(PDO::FETCH_OBJ);

$financiamiento = array();
$detalle = array();
foreach($contratos as $contrato){
	
	//Obtener información de financiamiento en caso de haber
	if($contrato->dm_financiamiento == 1){
		$financiamiento[$contrato->dc_contrato] = $db->doQuery(
						$db->select('tb_contrato_financiamiento_banco f
									 JOIN tb_banco b ON b.dc_banco = f.dc_banco',
									 'f.df_inicio, f.df_termino, f.dc_interes, f.dq_cuota, b.dg_banco',
									 'f.dc_contrato = '.$contrato->dc_contrato))->fetch(PDO::FETCH_OBJ);
	}
	
	/*$detalle[$contrato->dc_contrato] = $db->doQuery(
						$db->select('tb_nota_venta_contrato_detalle d
									 JOIN tb_nota_venta_detalle_contrato t ON t.dc_tipo_detalle = d.dc_tipo
									 LEFT JOIN tb_factura_venta fv ON fv.dc_factura = d.dc_factura',
									 'd.dq_cuota, d.dc_numero_cuota, d.df_vencimiento, t.dg_tipo_detalle'));*/
	
}
?>
<div class="secc_bar">
	Contratos de Nota de Venta
</div>
<div class="panes">
<?php foreach($contratos as $contrato): ?>
	<table class="tab" width="100%">
		<tbody>
			<tr>
				<td width="110">Contrato</td>
				<td><b><?php echo $contrato->dq_contrato ?></b></td>
				
				<?php if($contrato->dm_financiamiento == 1): ?>
				<td rowspan="7" valign="top">
					<table width="100%" class="tab">
						<caption>Datos de financiamiento</caption>
						<tbody>
							<tr>
								<td>Banco</td>
								<td><b><?php echo $financiamiento[$contrato->dc_contrato]->dg_banco ?></b></td>
							</tr>
							<tr>
								<td>Monto cuota</td>
								<td><b><?php echo moneda_local($financiamiento[$contrato->dc_contrato]->dq_cuota) ?></b></td>
							</tr>
							<tr>
								<td>Tasa de interés</td>
								<td><b><?php echo $financiamiento[$contrato->dc_contrato]->dc_interes ?></b></td>
							</tr>
							<tr>
								<td>Inicio Crédito</td>
								<td><b><?php echo $db->dateLocalFormat($financiamiento[$contrato->dc_contrato]->df_inicio) ?></b></td>
							</tr>
							<tr>
								<td>Término Crédito</td>
								<td><b><?php echo $db->dateLocalFormat($financiamiento[$contrato->dc_contrato]->df_termino) ?></b></td>
							</tr>
						</tbody>
					</table>
				</td>
				<?php endif; ?>
				
			</tr>
			<tr>
				<td>Fecha emisión</td>
				<td><b><?php echo $db->dateLocalFormat($contrato->df_emision) ?></b></td>
			</tr>
			<tr>
				<td>Inicio Contrato</td>
				<td><b><?php echo $db->dateLocalFormat($contrato->df_inicio) ?></b></td>
			</tr>
			<tr>
				<td>Término Contrato</td>
				<td><b><?php echo $db->dateLocalFormat($contrato->df_termino) ?></b></td>
			</tr>
			<tr>
				<td>Tasa de Interés</td>
				<td><b><?php echo $contrato->dc_interes ?></b></td>
			</tr>
			<tr>
				<td>Número de cuotas</td>
				<td><b><?php echo $contrato->dc_cuotas ?></b></td>
			</tr>
			<tr>
				<td>Monto de cuota</td>
				<td><b><?php echo moneda_local($contrato->dq_cuota) ?></b></td>
			</tr>
		</tbody>
	</table>
<?php endforeach; ?>
</div>