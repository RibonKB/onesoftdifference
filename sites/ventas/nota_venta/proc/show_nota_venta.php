<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_nota_venta = intval($_POST['id']);

$data = $db->select("(SELECT * FROM tb_nota_venta WHERE dc_empresa={$empresa} AND dc_nota_venta = {$dc_nota_venta}) nv
LEFT JOIN tb_termino_comercial term ON term.dc_termino_comercial = nv.dc_termino_comercial
LEFT JOIN tb_contacto_cliente c ON c.dc_contacto = nv.dc_contacto
	JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
	LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
	LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region
LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = nv.dc_tipo_cambio
LEFT JOIN tb_modo_envio me ON me.dc_modo_envio = nv.dc_modo_envio
LEFT JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = nv.dc_ejecutivo
LEFT JOIN tb_cotizacion_estado ce ON ce.dc_estado = nv.dc_estado
JOIN tb_tipo_nota_venta tnv ON tnv.dc_tipo = nv.dc_tipo_nota_venta",
"nv.dc_usuario_creacion,cl.dc_cliente,nv.dc_nota_venta,nv.dq_nota_venta,term.dg_termino_comercial,s.dg_direccion,com.dg_comuna,reg.dg_region,tc.dg_tipo_cambio,nv.dm_nula,
tc.dn_cantidad_decimales,nv.dq_cambio,me.dg_modo_envio,cl.dg_razon,cl.dg_rut,nv.dq_iva,nv.dq_neto,c.dg_contacto,nv.dm_validada,nv.dm_confirmada,tnv.dg_tipo,tnv.dm_costeable,
nv.df_fecha_emision,nv.df_fecha_envio, s.dg_sucursal,nv.dc_facturada,nv.dc_solicitada, tnv.dm_contratable,
(nv.dq_iva+nv.dq_neto) as dq_total,nv.dg_observacion,CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno,ex.dg_ap_materno) as dg_ejecutivo,nv.dc_estado,ce.dg_estado");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la nota de venta especificada");
	exit();
}
$data = $data[0];
$cambio = moneda_local($data['dq_cambio']);

if($data['dm_nula'] == '1'){
	$null_data = $db->select("(SELECT * FROM tb_nota_venta_anulada WHERE dc_nota_venta = {$dc_nota_venta}) n
	JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo",'ma.dg_motivo,n.dg_comentario');
	$null_data = array_shift($null_data);
}

if($data['dm_costeable'] && check_permiso(64)){
	$todos_detalles = $db->select("(SELECT * FROM tb_nota_venta WHERE dc_nota_venta = {$data['dc_nota_venta']}) nv
	JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta
	LEFT JOIN tb_proveedor p ON d.dc_proveedor = p.dc_proveedor",
	'dq_cantidad,d.dg_descripcion,(d.dq_precio_venta/nv.dq_cambio) dq_precio_venta,p.dg_razon,(d.dq_precio_venta/nv.dq_cambio*d.dq_cantidad) dq_total,
	(d.dq_precio_compra/nv.dq_cambio) as dq_precio_compra,d.dc_comprada,d.dc_recepcionada,d.dc_despachada,d.dc_facturada,d.dm_tipo');
	
	$detalle = array();
	$detalle_costo = array();
	$total_costo = 0;
	foreach($todos_detalles as $d){
		if($d['dm_tipo'] == 0){
			$detalle[] = $d;
		}else{
			$detalle_costo[] = $d;
			$total_costo += $d['dq_precio_compra']*$d['dq_cantidad'];
                        
		}
	}
	
	unset($todos_detalles);
	
}else{
	$detalle = $db->select("(SELECT * FROM tb_nota_venta WHERE dc_nota_venta = {$data['dc_nota_venta']}) nv
	JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta AND dm_tipo = 0
	LEFT JOIN tb_proveedor p ON d.dc_proveedor = p.dc_proveedor",
	'dq_cantidad,d.dg_descripcion,(d.dq_precio_venta/nv.dq_cambio) dq_precio_venta,p.dg_razon,(d.dq_precio_venta*d.dq_cantidad/nv.dq_cambio) dq_total,
	(d.dq_precio_compra/nv.dq_cambio) dq_precio_compra,d.dc_comprada,d.dc_recepcionada,d.dc_despachada,d.dc_facturada');
}

?>

<div class='title center'>
	Nota de venta Nº <?php echo $data['dq_nota_venta'] ?>
</div>
<table class='tab' width='100%' style='text-align:left;'>
	<caption>
		<a href='sites/clientes/ed_cliente.php?id=<?php echo $data['dc_cliente'] ?>' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='[ED]' />
		</a>
		Cliente:<br />
		<strong>(<?php echo $data['dg_rut'] ?>) <?php echo $data['dg_razon'] ?></strong>
	</caption>
	<tbody>
		<tr>
			<td width='160'>Fecha emision</td>
			<td><?php echo $db->dateTimeLocalFormat($data['df_fecha_emision']) ?></td>
			<td rowspan='9' width='200' valign="top">
				<h3 class='info' style='margin:0'><?php echo $data['dg_estado'] ?></h3>
				<?php if($data['dm_validada'] == 1): ?>
					<h3 class='confirm' style='margin:0'>Validada</h3>
				<?php endif; ?>
				<?php if($data['dm_confirmada'] == 1): ?>
					<h3 class='confirm' style='margin:0'>Confirmada</h3>
				<?php endif; ?>
				
				<?php if($data['dc_facturada'] > 0): ?>
				<h3 class='confirm' style='margin:0'>
					<?php if($data['dc_facturada'] == $data['dc_solicitada']): ?>
						FACTURADA
					<?php else: ?>
						FACTURADA PARCIAL
					<?php endif; ?>
				</h3>
				<?php endif; ?>
				
				<?php if($data['dm_nula'] == '1'): ?>
					<h3 class='alert'>NOTA VENTA ANULADA</h3>
					<?php if($null_data != NULL): ?>
						<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
							<div class='info'>Info anulación</div>
							Motivo: <b><?php echo $null_data['dg_motivo'] ?></b><br />
							Comentario: <b><?php echo $null_data['dg_comentario'] ?></b>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</td>
			
			<?php if($data['dm_contratable'] == 1): ?>
			<td rowspan="9" width="250" valign="top">
				<div class="info">
					<b>La Nota de venta posee un contrato</b>
				</div>
				<button type="button" id="show_contratos" class="imgbtn" style="background-image:url(images/doc.png);">Ver datos del contrato</button>
				<button type="button" id="show_contratos_table" class="imgbtn" style='background-image:url(images/facturada.jpg);'>Ver tabla de contrato</button>
                <button type="button" id="edit_contrato" class="editbtn">Editar Datos Contrato</button>
			</td>
			<?php endif; ?>
			
		</tr><tr>
			<td>Tipo de venta</td>
			<td><b><?php echo $data['dg_tipo'] ?></b></td>
		</tr><tr>
			<td>Domicilio cliente</td>
			<td><b><?php echo $data['dg_direccion'] ?></b> <?php echo $data['dg_comuna'] ?> <label><?php echo $data['dg_region'] ?></label></td>
		</tr><tr>
			<td>Tipo de cambio</td>
			<td><b><?php echo $data['dg_tipo_cambio'] ?></b> (<?php echo $cambio ?>)</td>
		</tr><tr>
			<td>Ejecutivo:</td>
			<td><b><?php echo $data['dg_ejecutivo'] ?></b></td>
		</tr><tr>
			<td>Término Comercial</td>
			<td><?php echo $data['dg_termino_comercial'] ?></td>
		</tr><tr>
			<td>Modo de envio</td>
			<td><?php echo $data['dg_modo_envio'] ?></td>
		</tr><tr>
			<td>Fecha de envio</td>
			<td><?php echo $db->dateLocalFormat($data['df_fecha_envio']) ?></td>
		</tr><tr>
			<td>Observación</td>
			<td><?php echo $data['dg_observacion'] ?></td>
		</tr>
	</tbody>
</table>

<table width='100%' class='tab'>
	<caption>Detalle</caption>
	<thead>
		<tr>
			<th colspan='4'>Avance</th>
			<th>Cantidad</th>
			<th>Descripción</th>
			<th width='5%'>Proveedor</th>
			<th width='8%'>Costo</th>
			<th width='8%'>Precio</th>
			<th width='8%'>Total</th>
			<th width='8%'>Margen</th>
			<th width='8%'>Margen (%)</th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$data['dq_total_costo'] = 0;
		$data['dq_neto'] = 0;
		foreach($detalle as $d):
			$data['dq_total_costo'] += $d['dq_precio_compra']*$d['dq_cantidad'];
			$data['dq_neto'] += $d['dq_precio_venta']*$d['dq_cantidad'];
	?>
		<tr>
			<td align='right' title='Se han Comprado <?php echo $d['dc_comprada'] ?> de <?php echo intval($d['dq_cantidad']) ?>'>
				<b><?php echo $d['dc_comprada'] ?></b>
				<img src='images/carrito.gif' style='vertical-align: middle;'>
			</td>
			<td align='right' title='Se han Recepcionado <?php echo $d['dc_recepcionada'] ?> de <?php echo intval($d['dq_cantidad']) ?>'>
				<b><?php echo $d['dc_recepcionada'] ?></b>
				<img src='images/inventario.png' style='vertical-align: middle; height:16px;'>
			</td>
			<td align='right' title='Se han Despachado <?php echo $d['dc_despachada'] ?> de <?php echo intval($d['dq_cantidad']) ?>'>
				<b><?php echo $d['dc_despachada'] ?></b>
				<img src='images/despachada.png' style='vertical-align: middle;'>
			</td>
			<td align='right' title='Se han Facturado <?php echo $d['dc_facturada'] ?> de <?php echo intval($d['dq_cantidad']) ?>'>
				<b><?php echo $d['dc_facturada'] ?></b>
				<img src='images/facturada.jpg' style='vertical-align: middle;'>
			</td>
			<td><?php echo intval($d['dq_cantidad']) ?></td>
			<td align='left'><?php echo $d['dg_descripcion'] ?></td>
			<td align='left'><?php echo $d['dg_razon'] ?></td>
			<td align='right'><?php echo moneda_local($d['dq_precio_compra'],$data['dn_cantidad_decimales']) ?></td>
			<td align='right'><?php echo moneda_local($d['dq_precio_venta'],$data['dn_cantidad_decimales']) ?></td>
			<td align='right'><?php echo moneda_local($d['dq_total'],$data['dn_cantidad_decimales']) ?></td>
			<td align='right'><?php echo moneda_local(($d['dq_precio_venta']-$d['dq_precio_compra'])*$d['dq_cantidad'],$data['dn_cantidad_decimales']) ?></td>
			<td align='right'>
			<?php if($d['dq_precio_venta'] != 0): ?>
				<?php echo moneda_local(($d['dq_precio_venta']-$d['dq_precio_compra'])*100/$d['dq_precio_venta'],2) ?>
			<?php else: ?>
				-
			<?php endif; ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<th colspan='8' align='right'>Total (<?php echo $data['dg_tipo_cambio'] ?>)</th>
			<th colspan='2' align='right'><?php echo moneda_local($data['dq_neto'],$data['dn_cantidad_decimales']) ?></th>
			<th colspan='2'>&nbsp;</th>
		</tr>
		<tr>
			<th colspan='8' align='right'>Total Neto</th>
			<th colspan='2' align='right'><?php echo moneda_local($data['dq_neto']*$data['dq_cambio']) ?></th>
			<th colspan='2'>&nbsp;</th>
		</tr>
		<tr>
			<th colspan='8' align='right'>IVA</th>
			<th colspan='2' align='right'><?php echo moneda_local($data['dq_iva']) ?></th>
			<th colspan='2'>&nbsp;</th>
		</tr>
		<tr>
			<th colspan='8' align='right'>Total a Pagar</th>
			<th colspan='2' align='right'><?php echo moneda_local($data['dq_total']) ?></th>
			<th colspan='2'>&nbsp;</th>
		</tr>
		<tr>
			<th colspan='8' align='right'>Margen (<?php echo $data['dg_tipo_cambio'] ?>)</th>
			<th colspan='2' align='right'><?php echo moneda_local($data['dq_neto']-$data['dq_total_costo']) ?></th>
			<th colspan='2'>&nbsp;</th>
		</tr>
		<tr>
			<th colspan='8' align='right'>Margen Total</th>
			<th colspan='2' align='right'>
				<?php echo moneda_local(($data['dq_neto']-$data['dq_total_costo'])*$data['dq_cambio']) ?>
				<?php if($data['dq_neto'] != 0): ?>
					(<?php echo moneda_local(($data['dq_neto']-$data['dq_total_costo'])*100/$data['dq_neto'],2) ?>%)
				<?php endif; ?>
			</th>
			<th colspan='2'>&nbsp;</th>
		</tr>
		<?php if($data['dm_costeable'] && check_permiso(64)): ?>
		<tr>
			<th colspan='8' align='right'>Margen Con costos reales</th>
			<th colspan='2' align='right'>
				<?php echo moneda_local(($data['dq_neto'] - $total_costo) * $data['dq_cambio']) ?>
                
				(<?php echo moneda_local(($data['dq_neto']-$total_costo)*100/$data['dq_neto'],2) ?>%)
                 
			</th>
			<th colspan='2'>&nbsp;</th>
		</tr>
		<?php endif; ?>
	</tfoot>
</table>
aihihaiaiahiahiahaihaiha
<script type="text/javascript">
$('#print_version').unbind('click').click(function(){
	window.open("sites/ventas/nota_venta/ver_nota_venta.php?id=<?php echo $data['dc_nota_venta'] ?>",'print_cotizacion','width=800;height=600');
});
$('#null_nota_venta').unbind('click').click(function(){
	loadOverlay("sites/ventas/nota_venta/null_nota_venta.php?id=<?php echo $data['dc_nota_venta'] ?>");
});
<?php if((check_permiso(42) && $idUsuario == $data['dc_usuario_creacion']) || check_permiso(40)): ?>
		enable_button('#edit_nota_venta');
		$('#edit_nota_venta').unbind('click').click(function(){
			loadpage('sites/ventas/nota_venta/ed_nota_venta.php?id=<?php echo $data['dc_nota_venta'] ?>');
		});
<?php else: ?>
		disable_button('#edit_nota_venta');
<?php endif; ?>

$('#show_workflow').unbind('click').click(function(){
	loadOverlay("sites/ventas/src_workflow.php?mode=nv&id=<?=$data['dc_nota_venta'] ?>");
});
//Opciones de notas de venta con costos asociados
<?php if($data['dm_costeable'] == 1 && (($data['dm_validada'] == '1' && $data['dm_confirmada'] == '0') || check_permiso(75))): ?>
	pymerp.enableButton('#costeo_nota_venta');
	$('#costeo_nota_venta').unbind('click').click(function(){
		loadOverlay("sites/ventas/nota_venta/cr_nota_venta_costeo.php?id=<?=$data['dc_nota_venta'] ?>&tc=<?=$data['dq_cambio'] ?>&tcdec=<?=$data['dn_cantidad_decimales'] ?>");
	});
<?php else: ?>
	$('#costeo_nota_venta').unbind('click');
	pymerp.disableButton('#costeo_nota_venta');
<?php endif; ?>

//Nota de venta validada
<?php if($data['dm_validada'] == '0'): ?>
	enable_button('#valida_nota_venta');
	$('#valida_nota_venta').unbind('click').click(function(){
		loadOverlay("sites/ventas/nota_venta/proc/val_nota_venta.php?id=<?=$data['dc_nota_venta'] ?>");
	}).text('Validar').addClass('checkbtn').removeClass('delbtn');
<?php else: ?>
	enable_button('#valida_nota_venta');
	$('#valida_nota_venta').unbind('click').click(function(){
		loadOverlay("sites/ventas/nota_venta/proc/val_nota_venta.php?id=<?=$data['dc_nota_venta'] ?>&mode=0");
	}).text('Desvalidar').removeClass('checkbtn').addClass('delbtn');
<?php endif; ?>

//Nota de venta confirmada
<?php if($data['dm_confirmada'] == '0'): ?>
	enable_button('#confirma_nota_venta');
	$('#confirma_nota_venta').unbind('click').click(function(){
		loadOverlay("sites/ventas/nota_venta/proc/conf_nota_venta.php?id=<?php echo $data['dc_nota_venta'] ?>");
	}).text('Confirmar').addClass('checkbtn').removeClass('delbtn');
<?php else: ?>
	enable_button('#confirma_nota_venta');
	$('#confirma_nota_venta').unbind('click').click(function(){
		loadOverlay("sites/ventas/nota_venta/proc/conf_nota_venta.php?id=<?php echo $data['dc_nota_venta'] ?>&mode=0");
	}).text('Desconfirmar').removeClass('checkbtn').addClass('delbtn');
<?php endif; ?>

//No se puede continuar workflow con notas de venta sin confirmar o validar
<?php if($data['dm_validada'] == '0' || $data['dm_confirmada'] == '0'): ?>
	disable_button('#comprar_nota_venta,#facturar_nota_venta');
<?php else: ?>
	enable_button('#comprar_nota_venta,#facturar_nota_venta');
	$('#comprar_nota_venta').unbind('click').click(function(){
		loadOverlay("sites/ventas/nota_venta/proc/cr_orden_compra_nota_venta.php?nv_numero=<?php echo $data['dq_nota_venta'] ?>");
	});
	$('#facturar_nota_venta').unbind('click').click(function(){
		loadpage("sites/ventas/factura_venta/proc/cr_factura_venta.php?nv_numero=<?php echo $data['dq_nota_venta'] ?>&cli_id=<?php echo $data['dc_cliente'] ?>");
	});
<?php endif; ?>

<?php if($data['dm_nula'] == 1): ?>
	disable_button('#null_nota_venta,#edit_nota_venta,#costeo_nota_venta,#comprar_nota_venta,#facturar_nota_venta,#valida_nota_venta,#confirma_nota_venta');
<?php endif; ?>

<?php if($data['dm_contratable'] == 1): ?>
	$('#show_contratos').click(function(){
		pymerp.loadOverlay('sites/ventas/nota_venta/proc/show_contrato.php','dc_nota_venta=<?php echo $dc_nota_venta ?>');
	});
	$('#show_contratos_table').click(function(){
		pymerp.loadOverlay('sites/ventas/nota_venta/proc/show_contrato_table.php','dc_nota_venta=<?php echo $dc_nota_venta ?>');
	});
	$('#edit_contrato').click(function(){
		pymerp.loadOverlay('sites/ventas/contrato/ed_contrato.php','dc_nota_venta=<?php echo $dc_nota_venta ?>');
	});
<?php endif; ?>

<?php if(check_permiso(90)):?>
    pymerp.enableButton('#print_special');
	$('#print_special').unbind('click').click(function() {
		window.open("sites/ventas/nota_venta/ver_nota_venta.php?id=<?php echo $data['dc_nota_venta'] ?>&type=1", 'print_nota_venta', 'width=800;height=600');
	});
<?php else:?>
    pymerp.disableButton('#print_special');
<?php endif;?>

pymerp.init('#show_nota_venta');

</script>
<script type="text/javascript" src="jscripts/clientes/src_cliente.js?v0_4"></script>