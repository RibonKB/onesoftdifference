<?php
	if(!isset($_POST['dc_cuotas'])){
		$error_man->showWarning('No se encontraron los datos necesarios para crear el contrato, compruebe que este haya sido ingresado.');
		exit;
	}
	
	$dc_cuotas = intval($_POST['dc_cuotas']);
	$dq_cuota = floatval($_POST['dq_cuota']);
	
	if(!$dc_cuotas || !$dq_cuota){
		$error_man->showWarning('La información de las cuotas del contrato es inválida, compruebe los datos y vuelva a intentarlo.');
		exit;
	}
	
	$dq_total_cuotas = $dc_cuotas * $dq_cuota;
	
	if(round($dq_total_cuotas,2) != round($precioTotal,2)){
		$error_man->showWarning('El valor total del contrato debe coincidir con el de los detalles de la nota de venta.');
		exit;
	}
	
	$dc_interes_contrato = floatval($_POST['dc_interes_interno']);
	
	if(!$dc_interes_contrato){
		$error_man->showWarning('El interés del contrato es inválido');
		exit;
	}

	if(!$_POST['dg_nombre_proyecto'] or !$_POST['dc_encargado_proyecto'] or !$_POST['df_inicio_proyecto']){
		$error_man->showWarning('Debe ingresar los datos de proyecto para crar un contrato');
		exit;
	}
	
?>
