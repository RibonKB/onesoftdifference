<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div class="secc_bar">Generar orden de compra para nota de venta</div>
<div class="panes center">');

include_once("../../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/orden_compra/proc/cr_orden_compra.php','cr_orden_compra','cValidar');
$form->Header("Indique el proveedor a quien se le comprarán los detalles de la nota de venta <b>{$_POST['nv_numero']}</b>");

$form->Listado('Proveedor','prov_id','tb_proveedor',array('dc_proveedor','dg_razon'),1);

$form->Hidden('nv_numero',$_POST['nv_numero']);
$form->End('crear','addbtn');

?>
</div>