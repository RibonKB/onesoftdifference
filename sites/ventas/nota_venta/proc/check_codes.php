<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$codes = implode("','",$_POST['c']);

$prods = $db->select("tb_producto",'dg_codigo',"dg_codigo IN ('{$codes}')");

echo json_encode($prods);

?>