<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();


foreach($_POST['prod'] as $i => $v){
	
	$_POST['cant'][$i] = str_replace(',','',$_POST['cant'][$i]);
	$_POST['costo'][$i] = str_replace(',','',$_POST['costo'][$i]);
	
	if(isset($_POST['id_nv_detail'][$i])){
		$db->update('tb_nota_venta_detalle',array(
			"dc_producto" => $v,
			"dg_descripcion" => $_POST['desc'][$i],
			"dq_cantidad" => $_POST['cant'][$i],
			"dq_precio_compra" => $_POST['costo'][$i]*$_POST['nv_cambio'],
			"dq_precio_venta" => 0,
			"dc_proveedor" => $_POST['proveedor'][$i],
			"dc_cebe" => $_POST['cebe'][$i],
			"dc_ceco" => $_POST['ceco'][$i]
		),"dc_nota_venta_detalle={$_POST['id_nv_detail'][$i]}");
	}else{
		$db->insert('tb_nota_venta_detalle',array(
			"dc_nota_venta" => $_POST['nv_id'],
			"dc_empresa" => $empresa,
			"dc_producto" => $v,
			"dg_descripcion" => $_POST['desc'][$i],
			"dq_cantidad" => $_POST['cant'][$i],
			"dq_precio_compra" => $_POST['costo'][$i]*$_POST['nv_cambio'],
			"dq_precio_venta" => 0,
			"dm_tipo" => 1,
			"dc_proveedor" => $_POST['proveedor'][$i],
			"dc_cebe" => $_POST['cebe'][$i],
			"dc_ceco" => $_POST['ceco'][$i]
		));
	}
}

if(isset($_POST['to_delete'])){
	$del = implode(',',$_POST['to_delete']);
	$db->query("DELETE FROM tb_nota_venta_detalle WHERE dc_nota_venta_detalle IN ({$del}) AND dm_tipo = 1");
}

/**
*	Actualizar la cantidad solicitada de la nota de venta
**/
$dc_solicitada = $db->select('tb_nota_venta_detalle','SUM(dq_cantidad) dc_solicitada',"dc_nota_venta = {$_POST['nv_id']}");
$dc_solicitada = $dc_solicitada[0]['dc_solicitada'];

$db->update('tb_nota_venta',array(
	'dc_solicitada' => $dc_solicitada
),"dc_nota_venta = {$_POST['nv_id']}");

//-----

$db->commit();

$error_man->showConfirm("Se han agregado ".count($_POST['prod'])." detalles al costo de la venta.");
?>