<?php
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/form-class.php");

$dc_tipo_venta = intval($_POST['dc_tipo_venta']);

$tipo_venta = $db->select('tb_tipo_nota_venta','dm_contratable','dc_tipo = '.$dc_tipo_venta.' AND dc_empresa = '.$empresa);
$tipo_venta = array_shift($tipo_venta);

if($tipo_venta == NULL){
	$error_man->showWarning('El tipo de venta es inválido, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

if($tipo_venta['dm_contratable'] == 0){
	$error_man->showWarning('El tipo de venta no admite contratos, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

$form = new Form($empresa);

?>
<div class="secc_bar">
	Crear Contrato
</div>
<div class="panes">
oli
	<?php $form->Start('#','cr_contrato_form','contratoForm') ?>
		<?php
			$form->Section();
				$form->Text('Número de cuotas','dc_cuotas',1,Form::DEFAULT_TEXT_LENGTH,isset($_POST['dc_cuotas'])?$_POST['dc_cuotas']:'');
				$form->Text('Valor cuota','dq_cuota',1,Form::DEFAULT_TEXT_LENGTH,isset($_POST['dq_cuota'])?$_POST['dq_cuota']:'');
				$form->Text('Tasa de interés','dc_interes_interno',1,Form::DEFAULT_TEXT_LENGTH,isset($_POST['dc_interes_interno'])?$_POST['dc_interes_interno']:'');
				$form->Select('Tasa de cambio','dm_tasa_cambio',array(
				1 =>'Último día hábil del mes',
					'Primer día hábil mes siguiente',
					'Último día del mes',
					'Primer día del mes siguiente',
					'Día de facturación',
					'Pactado en Nota de Venta'
				),1);
				$form->Select('Periodo','dc_periodo',array(
					1 => 'Mensual',
					3 => 'Trimestral',
					6 => 'Semestral',
					7 => 'Anual'
				),1,isset($_POST['dc_periodo'])?$_POST['dc_periodo']:1);
			$form->EndSection();
			$form->Section();
				$form->Date('Fecha de inicio','df_inicio',1,isset($_POST['df_inicio'])?$_POST['df_inicio']:0);
				//$form->Date('Fecha de término','df_termino',1);
				$form->Textarea('Patrón de facturación','dg_patron');
				?>
                <div style="border:1px solid #CCC; background-color:#FAFAFA; padding:3px;">
                	Máscaras para el patrón:
                    <table width="280">
                    	<tbody>
                            <tr>
                                <td width="140" align="right">
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{TC}}')">Tipo de Cambio</button>
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{QC}}')">Valor Tipo cambio</button>
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{CS}}')">Total Cuotas</button>
                                </td>
                                <td valign="top">
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{CT}}')">Cuota actual</button>
                                    <button type="button" onclick="pymerp.areaInsertAtPoint('dg_patron','{{CTO}}')">Número Contrato</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php
			$form->EndSection();
			$form->Group();
			$form->Section();
				echo('<div style="width:280px;height:1px"></div>');
				$form->Radiobox('Financiamiento','dm_financiamiento',array('Directo','Indirecto'),isset($_POST['dm_financiamiento'])?$_POST['dm_financiamiento']:0,' ');
			$form->EndSection();
			$form->Section();
			echo('<div id="financiamiento_data" class="hidden">');
				$form->Listado('Banco '.Form::MANDATORY_ICON,'dc_banco','tb_banco',
							array('dc_banco','dg_banco'),
							Form::DEFAULT_MANDATORY_STATUS,
							isset($_POST['dc_banco'])?$_POST['dc_banco']:'');
				$form->Text('Valor cuota '.Form::MANDATORY_ICON,'dq_cuota_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							Form::DEFAULT_TEXT_LENGTH,
							isset($_POST['dq_cuota_banco'])?$_POST['dq_cuota_banco']:'');
				$form->Date('Fecha inicio '.Form::MANDATORY_ICON,'df_inicio_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							isset($_POST['df_inicio_banco'])?$_POST['df_inicio_banco']:'');
				$form->Date('Fecha término '.Form::MANDATORY_ICON,'df_termino_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							isset($_POST['df_termino_banco'])?$_POST['df_termino_banco']:'');
				$form->Text('Tasa interés '.Form::MANDATORY_ICON,'dc_interes_banco',
							Form::DEFAULT_MANDATORY_STATUS,
							Form::DEFAULT_TEXT_LENGTH,
							isset($_POST['dc_interes_banco'])?$_POST['dc_interes_banco']:'');
			echo('</div>');
			$form->EndSection();
		?>
	<div id="distribucion_data">
		<?php
			if(isset($_POST['dq_cuota_concepto'])):
				foreach($_POST['dq_cuota_concepto'] as $i => $dq_cuota_concepto){
					$form->Hidden('dq_cuota_concepto[]',$dq_cuota_concepto);
					$form->Hidden('dc_detalle_concepto[]',$_POST['dc_detalle_concepto'][$i]);
				}
			endif;
		?>
	</div>
	<?php $form->End('Generar Contrato','addbtn') ?>
</div>
<script type="text/javascript">
	cr_contrato.initCreation();
	<?php if(isset($_POST['dm_financiamiento']) && $_POST['dm_financiamiento'] == 1): ?>
		$(':radio[name=dm_financiamiento]:checked').trigger('click');
	<?php endif; ?>
</script>
