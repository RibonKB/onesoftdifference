<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
//se le agrega el campo dc_nota_venta a solicitar
$data = $db->select('tb_nota_venta','dc_nota_venta, dq_nota_venta, dc_tipo_nota_venta',"dc_nota_venta={$_POST['id']}");

if(!count($data)){
	$error_man->showWarning("No se encontró la nota de venta especificada.");
	exit;
}
$data = $data[0];

/* Se agrega la verificación de facturable, y se verifica si existe al menos una factura asociada a la nota de venta */
$contratable = $db->select('tb_tipo_nota_venta', "dm_contratable", "dc_tipo = {$data['dc_tipo_nota_venta']}");
$contratable = $contratable[0]['dm_contratable'];
/*$db->getRowById('tb_tipo_nota_venta', $fv->dc_tipo_nota_venta, 'dc_tipo', 'dm_contratable')->dm_contratable;*/
$facturado = false;
$numeroPrimeraFactura = null;
if($contratable == 1){
	$factura = $db->select('tb_factura_venta', 'dq_factura', "dc_nota_venta = {$data['dc_nota_venta']} AND dm_nula = 0");
	$primeraFactura = array_shift($factura);
	if(!empty($primeraFactura)){
		$facturado = true;
		$numeroPrimeraFactura = $primeraFactura['dq_factura'];
	}
}

echo("<div class='secc_bar'>Costeo de nota de venta <b>{$data['dq_nota_venta']}</b></div><div class='panes'>");

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/nota_venta/proc/crear_nota_venta_costeo.php','cr_detalle_costo','os_form');
$form->Header("<b>Agregue detalles para el costo de la venta</b><br /><i>Nota: Los costos están en el mismo tipo de cambio asignado a la nota de venta</i>");
echo("<table class='tab' width='100%'>
<thead><tr>
	<th colspan='5' width='300'>Avance</th>
	<th width='100'>Producto</th>
	<th width='150'>Descripción</th>
	<th>Proveedor</th>
	<th width='120'>CeCo</th>
	<th>Cantidad</th>
	<th>Costo</th>
	<th width='120'>Costo Total</th>
</tr></thead>
<tfoot>
	<tr>
		<th colspan='11' align='right'>Totales</th>
		<th align='right' id='total_costo'>0</th>
	</tr>
	<tr>
		<th colspan='11' align='right'>Total Neto</th>
		<th align='right' id='total_neto'>0</th>
	</tr>
	<tr>
		<th colspan='11' align='right'>IVA</th>
		<th align='right' id='total_iva'>0</th>
	</tr>
	<tr>
		<th colspan='11' align='right'>Total a pagar</th>
		<th align='right' id='total_pagar'>0</th>
	</tr>
	</tfoot>
<tbody id='prod_list'></tbody>
</table>");
/* se valida si la nota de venta ya ha sido facturada alguna vez. De ser asi, no permite agregar nuevos items */
if($facturado){
	echo $error_man->showAviso('No es posible agregar nuevos costos a esta nota de venta de contrato '
		. 'por que ya ha sido facturada al menos una vez. Consulte la tabla de contrato para mas información.', 1);
} else {
	$form->Button('Agregar otro producto','id="prod_add"','addbtn');
}
//$form->Button('Agregar otro producto','id="prod_add"','addbtn');
$form->Hidden('cot_iva',0);
$form->Hidden('cot_neto',0);
$form->Hidden('nv_cambio',$_POST['tc']);
$form->Hidden('nv_id',$_POST['id']);
$form->End('Crear','addbtn');

echo("<table class='hidden' id='prods_form'>
<tr class='main'>
	<td align='center'>
		<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
	</td>
	<td align='right'>
		<b class='prod_comprada'>0</b>
		<img src='images/carrito.gif' style='vertical-align: middle;'>
	</td>
	<td align='right'>
		<b class='prod_recepcionada'>0</b>
		<img src='images/inventario.png' style='vertical-align: middle; height:16px;'>
	</td>
	<td align='right'>
		<b class='prod_despachada'>0</b>
		<img src='images/despachada.png' style='vertical-align: middle;'>
	</td>
	<td align='right'>
		<b class='prod_facturada'>0</b>
		<img src='images/facturada.jpg' style='vertical-align: middle;'>
	</td>
	<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='10' /></td>
	<td><input type='text' name='desc[]' class='prod_desc inputtext' size='30' /></td>
	<td><select name='proveedor[]' style='width:170px' class='prod_proveedor inputtext' required='required'><option></option>");
	$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'");
	foreach($proveedores as $p){
		echo("<option value='{$p['dc_proveedor']}'>{$p['dg_razon']}</option>");
	}
	echo("</select></td>
	<td><select name='cebe[]' style='width:120px' class='prod_cebe inputtext' required='required'><option></option>");
	$cebe = $db->select('tb_cebe','dc_cebe,dg_cebe,dc_ceco',"dc_empresa={$empresa} AND dm_activo = '1'",array('order_by' => 'dg_cebe'));
	foreach($cebe as $c){
		echo("<option id='{$c['dc_ceco']}' value='{$c['dc_cebe']}'>{$c['dg_cebe']}</option>");
	}
	echo("<input type='hidden' name='ceco[]' class='prod_ceco' value='0' >
	</td><td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
	<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
	<td class='costo_total' align='right'>0</td>
</tr>
</table>");

$detalle_costo = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$_POST['id']} AND dm_tipo = 1) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto",
'd.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dc_proveedor,d.dq_cantidad,d.dq_precio_compra,p.dc_producto,
d.dc_comprada,d.dc_recepcionada,d.dc_despachada,d.dc_facturada,d.dc_cebe,d.dc_ceco');

?>
</div>
<script type="text/javascript">
	var empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	var detalle_costo = <?=json_encode($detalle_costo) ?>;
	var tc = <?=$_POST['tc'] ?>;
	var tc_dec = <?=$_POST['tcdec'] ?>;
	var facturado = <?=json_encode($facturado)?>;
</script>
<script type="text/javascript" src="jscripts/nota_venta/detalle_costeo.js?v=0_5d"></script>
<script type="text/javascript">
    window.setTimeout(function(){
      $('#prod_list').parent('table').tableAdjust(8);
    },100)
</script>