<?php

/**
 * Description of ConsultaNotaVentaFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 23-07-2013
 */
class CrearNotaVentaFactory extends Factory {

	protected $title = "Crear Nota Venta";

	public function setFechaArriboProductoAction() {
		$r = $this->getRequest();
		$fecha = isset($r->fecha) ? $r->fecha:'';
		$form =  $this->getFormView($this->getTemplateUrl('dateSelect'), array('fecha' => $fecha));
		echo $this->getOverlayView($form, array(),Factory::STRING_TEMPLATE);
	}

}
