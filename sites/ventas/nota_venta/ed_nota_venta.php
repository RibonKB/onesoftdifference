﻿<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Se escapan los caracteres por seguridad
$db->escape($_POST['nv_base_num']);

$base = $db->select("(SELECT * FROM tb_nota_venta WHERE dc_nota_venta={$_POST['id']}) nv
LEFT JOIN tb_cotizacion c ON c.dc_cotizacion = nv.dc_cotizacion
LEFT JOIN tb_tipo_nota_venta tnv ON tnv.dc_tipo = nv.dc_tipo_nota_venta
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente",
'nv.dc_nota_venta,nv.dc_termino_comercial,nv.dc_ejecutivo,nv.dc_tipo_cambio,nv.dq_cambio,nv.dc_modo_envio,nv.df_fecha_emision,nv.df_fecha_envio,nv.dg_observacion,
cl.dc_cliente,cl.dg_razon,cl.dc_contacto_default,c.dq_cotizacion,nv.dc_cotizacion,nv.dc_contacto,nv.dc_comprada,nv.dc_tipo_nota_venta,tnv.dm_costeable,nv.dq_nota_venta,
nv.dm_confirmada,nv.dm_al_dia,nv.dc_cliente_final');

if(!count($base)){
	$error_man->showErrorRedirect("No se encontró la nota de venta especificada, intentelo nuevamente.",
	"sites/ventas/nota_venta/src_nota_venta.php");
}
$base = $base[0];

if($base['dm_confirmada'] == 1){
	if(!check_permiso(41)){
		$error_man->showErrorRedirect("No tiene permisos para editar notas de venta ya confirmadas, consulte con el encargado.",
		"sites/ventas/nota_venta/src_nota_venta.php");
	}
}

$detalles_base = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta={$base['dc_nota_venta']} AND dm_tipo=0) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto AND p.dm_activo = 1
JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto",
'p.dc_producto,d.dq_cantidad,d.dg_descripcion,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor, d.df_fecha_arribo,
p.dg_codigo AS dg_producto,dc_comprada,d.dc_nota_venta_detalle,d.dc_despachada,d.dc_facturada,t.dm_controla_inventario');
foreach($detalles_base as &$v){
	if(!empty($v['df_fecha_arribo'])){
		$v['df_fecha_arribo'] = $db->dateLocalFormat($v['df_fecha_arribo']);
	}
}
echo("<div id='secc_bar'>Edición de nota de venta</div>
<div id='main_cont'>
<div class='panes' style='width:1140px;'>
<div class='title center'>
<button class='right button' id='cli_switch'>Cambiar Cliente</button>
Generando nota de venta para <strong id='cli_razon' style='color:#000;'>{$base['dg_razon']}</strong>
</div>
<hr class='clear' />");

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio,dn_cantidad_decimales",
	"dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$c['dq_cambio'] = number_format($c['dq_cambio'],$c['dn_cantidad_decimales'],'.',',');
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |{$c['dq_cambio']}|";
		$form->Hidden("decimal{$c['dc_tipo_cambio']}",$c['dn_cantidad_decimales']);
	}

	$form->Header("<strong>Indique los datos para actualizar la nota de venta <b>{$base['dq_nota_venta']}</b></strong><br />Los datos marcados con [*] son obligatorios");
	$form->Start("sites/ventas/nota_venta/proc/editar_nota_venta.php","ed_nota_venta",'ventas_form');
	$form->Section();
	/*$form->Listado('Contacto','lst_contacto',
	"(SELECT * FROM tb_domicilio_cliente WHERE dc_cliente = {$base['dc_cliente']}) dc
	  LEFT JOIN tb_comuna co ON dc.dc_comuna = co.dc_comuna
	  JOIN (SELECT * FROM tb_domicilio_cliente_tipo_direccion WHERE dm_activo ='1') td ON dc.dc_domicilio = td.dc_domicilio
	  JOIN tb_contacto_cliente cc ON td.dc_contacto = cc.dc_contacto",
	array('DISTINCT cc.dc_contacto','dc.dg_direccion','co.dg_comuna','cc.dg_contacto'),1,$base['dc_contacto_default'],'');*/

	$form->Listado('Contacto','lst_contacto',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$base['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$base['dc_contacto'],'');

	$form->Listado('Cliente final', 'dc_cliente_final', 'tb_cliente', array('dc_cliente','dg_razon'), true, $base['dc_cliente_final']);
	$form->Listado('Tipo de venta','nv_tipo_venta','tb_tipo_nota_venta',array('CONCAT_WS("|",dc_tipo,dm_costeable)','dg_tipo'),1,
	$base['dc_tipo_nota_venta'].'|'.$base['dm_costeable']);
	$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1,$base['dc_tipo_cambio']);
	$form->Text('Cambio','nv_cambio',1,255,$base['dq_cambio']);
	$form->EndSection();

	$form->Section();
	$form->Date("Fecha envio","nv_envio",0,$base['df_fecha_envio']);
	$form->Listado("Modo de envio","nv_modo_envio","tb_modo_envio",array("dc_modo_envio","dg_modo_envio"),0,$base['dc_modo_envio']);
	$form->Listado('Ejecutivo','nv_executive','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'),1,$base['dc_ejecutivo']);
	$form->Text('Cotización Número','nv_cotizacion',$empresa_conf['dm_wf_op_requerida'],255,$base['dq_cotizacion']);
	$form->Hidden('nv_id_cotizacion',$base['dc_cotizacion']);
	$form->Listado("Términos comerciales","nv_termino","tb_termino_comercial",array("dc_termino_comercial","dg_termino_comercial"),0,$base['dc_termino_comercial']);
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','nv_observacion',0,$base['dg_observacion']);
	$form->Radiobox('','nv_aldia',array('Tipo de cambio al día','Tipo de cambio al día de facturación'),$base['dm_al_dia']);
	echo('<br />');
	$form->EndSection();

	echo("<hr class='clear' />");

	$c_extra = $db->select("tb_nota_venta_contacto_extra",
	"dg_etiqueta,dc_tipo_direccion,dc_contacto_extra",
	"dc_empresa = {$empresa} AND dm_activo = '1'");

	foreach($c_extra as $ex){

		$form->Section();
		$form->Hidden('tipo_cextra[]',$ex['dc_contacto_extra']);
		$form->Listado($ex['dg_etiqueta'],'contacto_extra[]',"(SELECT * FROM tb_cliente_sucursal_tipo WHERE dc_tipo_direccion = {$ex['dc_tipo_direccion']}) st
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = st.dc_sucursal
		JOIN tb_contacto_cliente c ON c.dc_sucursal = s.dc_sucursal
		LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),0,0,'');
		$form->EndSection();
	}

	echo("<br class='clear' /><br />");

	$form->Header("Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la nota de venta",'id="prod_info"');
	echo("<div id='prods' style='display:none;'>
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='70'>Opciones</th>
		<th width='95'>Código (Autocompleta)</th>
		<th>Descripción</th>
		<th width='158'>Proveedor</th>
		<th width='63'>Cantidad</th>
		<th width='85'>Precio</th>
		<th width='85'>Total</th>
		<th width='85'>Costo</th>
		<th width='85'>Costo total</th>
		<th width='85'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='5' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center'><span id='total_margen_p'></span>%</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th align='right' colspan='2'>Margen Tipo Cambio</th>
		<th align='right' id='total_margen_tc'>0</th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>");
	$form->Hidden('nv_id',$_POST['id']);
	$form->Hidden('cli_id',$base['dc_cliente']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->hidden('q_comprada',$base['dc_comprada']);
	$form->End('Editar','editbtn');

	echo("<table id='prods_form' style='display:none;'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/cal.png' alt='' title='Fecha de Arribo Estimada' class='set_fecha_arribo' />
			<input type='hidden' class='fecha_arribo' name='df_fecha_arribo[]' />
		</td>
		<td>
			<input type='text' class='prod_codigo searchbtn' size='7' />
		</td>
		<td>
			<input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' />
			<input type='hidden' name='id_detail[]' class='id_detail' value='0' />
		</td>
		<td><select name='proveedor[]' style='width:155px;' class='prod_proveedor inputtext' required='required'><option></option>");
		$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'");
		foreach($proveedores as $p){
			echo("<option value='{$p['dc_proveedor']}'>{$p['dg_razon']}</option>");
		}
		echo("</select></td>
		<td>
			<input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' />
			<input type='hidden' name='comprada[]' class='prod_min_cant' value='0' />
		</td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
	</tr>
	<tr style='display:none;'>
		<td colspan='5' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_price' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_margen' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>
	</table>");
?>
</div></div>
<script type="text/javascript">
	$('#cli_switch').click(function(){
		loadOverlay('sites/ventas/switch_cliente.php');
	});
	empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;

	var permiso_40 = <?=check_permiso(40)?'true':'false' ?>;
	var permiso_41 = <?=check_permiso(41)?'true':'false' ?>;
	var permiso_42 = <?=check_permiso(42)?'true':'false' ?>;

</script>
<script type="text/javascript" src="jscripts/product_manager/ed_nota_venta.js?v1_10A"></script>
<script type="text/javascript" src="jscripts/sites/ventas/nota_venta/ed_nota_venta.js?v=0_5b"></script>
<script type="text/javascript">
<?php
	if(isset($detalles_base))
		echo("var detalles_base = ".json_encode($detalles_base).";
		var cambio = $('#nv_cambio').val();
		$('#prod_tipo_cambio').change();
		$('#nv_cambio').val(cambio).change();
		set_detail(detalles_base);
		$('#prod_info').hide();
		$('#prods').show();");
?>
if($('#nv_id_cotizacion').val() != 0){
	v = $('#nv_cotizacion');
	create_cnumber_div(v.val()).width(v.width()).replaceAll(v);
}
<?php include_once('../../../inc/Factory.class.php');?>
var setFechaArriboProducto = '<?php echo Factory::buildUrl('EditarNotaVenta', 'ventas', 'setFechaArriboProducto', 'nota_venta'); ?>';
editarNotaVenta.init();
</script>
