<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Nota de Venta</div>
<div id="main_cont" class="center"><br /><br />
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/nota_venta/proc/src_nota_venta.php","src_nota_venta");
	$form->Header("<strong>Indicar los parámetros de búsqueda de la nota de venta</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50">Número de nota de venta</td><td width="280">');
	$form->Text("Desde","nv_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','nv_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','nv_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','nv_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','nv_client','tb_cliente',array('dc_cliente','dg_razon'));
	
	echo('</td><td>&nbsp;</td></tr><tr><td>Cliente Final</td><td>');
    $form->ListadoMultiple('', 'dc_cliente_final', 'tb_cliente', array('dc_cliente','dg_razon'));
	
	if(check_permiso(38)){
		echo('</td><td>&nbsp;</td></tr><tr><td>Ejecutivo</td><td>');
		$form->ListadoMultiple('','nv_executive','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
	}
	
	echo('</td><td>&nbsp;</td></tr><tr><td>Buscar en Detalle</td><td>');
	$form->Text('Código de producto','nv_codigo_producto');
	echo('</td><td>');
	$form->Text('Descripción','nv_detalle_descripcion');
	
	echo('</td></tr><tr><td>Incluir Notas de venta<br /><small>(Deja en blanco para ignorar estado)</small></td><td>');
	$form->Combobox('','nv_status',array('dm_validada'=>'Validadas','dm_confirmada'=>'Confirmadas'));
	echo('</td><td>&nbsp;</td></tr></table>');
	
	if(!check_permiso(38))
		$form->Hidden('nv_executive[]',$userdata['dc_funcionario']);
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#nv_client,#nv_executive,#dc_cliente_final').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>