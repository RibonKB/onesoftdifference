<?php
define('MAIN',1);
require_once("../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select("(SELECT * FROM tb_orden_compra WHERE dc_empresa={$empresa} AND dq_orden_compra={$_GET['number']} AND dm_nula = 0) o
LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = o.dc_tipo_cambio
JOIN tb_proveedor p ON p.dc_proveedor = o.dc_proveedor",
'o.dc_orden_compra,o.dq_cambio,tc.dg_tipo_cambio,p.dg_razon,DATE_FORMAT(o.df_fecha_emision,"%d/%m/%Y") as df_emision,o.dc_nota_venta,o.dc_orden_servicio');

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

$detalle = $db->select("(SELECT * FROM tb_orden_compra_detalle WHERE dc_orden_compra = {$id['dc_orden_compra']} AND (dq_cantidad-dc_recepcionada) > 0) d
LEFT JOIN (SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo = '1') p ON p.dc_producto = d.dc_producto",
"d.dc_detalle,d.dc_detalle_nota_venta,d.dc_detalle_orden_servicio,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio,p.dc_producto,d.dc_recepcionada,d.dc_ceco");

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>