<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_orden_compra","dc_nota_venta,dq_orden_compra,dc_responsable,dc_tipo_cambio,dq_cambio,dc_proveedor,dg_observacion","dc_orden_compra = {$_POST['id']}");

if(!count($data)){
	$error_man->showWarning('La orden de compra especificada no se ha encontrado');
	exit();
}
$data = $data[0];

$proveedor = $db->select('tb_proveedor','dg_razon,dg_rut',"dc_proveedor={$data['dc_proveedor']}");
$proveedor = $proveedor[0];

echo("<div id='secc_bar'>Edición de orden de compra</div>
<div id='main_cont'>
<div class='panes'>
<div class='title center'>
<button class='right button' id='prov_switch'>Cambiar Proveedor</button>
Editando orden de compra <b style='color:#000;'>{$data['dq_orden_compra']}</b> de <b id='prov_razon' style='color:#000;'>{$proveedor['dg_razon']}</b>
</div>
<hr class='clear' />");

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);

	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio,dn_cantidad_decimales",
	"dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$c['dq_cambio'] = number_format($c['dq_cambio'],$c['dn_cantidad_decimales'],'.',',');
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |{$c['dq_cambio']}|";
		$form->Hidden("decimal{$c['dc_tipo_cambio']}",$c['dn_cantidad_decimales']);
	}

	$form->Start("sites/ventas/orden_compra/proc/editar_orden_compra.php","cr_orden_compra",'ventas_form');
	$form->Header("<strong>Indique los actualizados de la orden de compra</strong><br />Los datos marcados con [*] son obligatorios");

	$form->Section();
	//$form->Text('Nota de venta','oc_nota_venta',$empresa_conf['dm_wf_nv_requerida'],255,$poblacion['nota_venta']);
	$form->Hidden('oc_id_nota_venta',$data['dc_nota_venta']);
	$form->Listado('Ejecutivo','oc_executive','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'),1,$data['dc_responsable']);
	$form->EndSection();

	$form->Section();
	$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1,$data['dc_tipo_cambio']);
	$form->Text('Cambio','oc_cambio',1,255,$data['dq_cambio']);
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','oc_observacion',0,$data['dg_observacion']);
	$form->EndSection();

	echo("<br class='clear' /><br />");

	$form->Header("Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la nota de venta",'id="prod_info"');
	echo("<div id='prods'>
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='70'>Opciones</th>
		<th width='120'>Código (Autocompleta)</th>
		<th>Descripción</th>
		<th width='178'>Proveedor</th>
		<th width='83'>Cantidad</th>
		<th width='95'>Costo</th>
		<th width='95'>Costo total</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='5' align='right'>Totales</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>");
	$form->Hidden('prov_id',$data['dc_proveedor']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('oc_id',$_POST['id']);
	$form->End('Editar','editbtn');

	echo("<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/cal.png' alt='' title='Fecha de Arribo Estimada' class='set_fecha_arribo' />
			<input type='hidden' class='fecha_arribo' name='df_fecha_arribo[]' />
		</td>
		<td>
			<input type='text' class='prod_codigo searchbtn' size='7' />
		</td>
		<td>
			<input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' style='width:94%;' />
		</td>
		<td><select name='proveedor[]' style='width:155px;' class='prod_proveedor inputtext' required='required'><option></option>");
		$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'",array('order_by' => 'dg_razon'));
		foreach($proveedores as $p){
			echo("<option value='{$p['dc_proveedor']}'>{$p['dg_razon']}</option>");
		}
		echo("</select></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
	</tr>
	<tr style='display:none;'>
		<td colspan='5' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>
	</table>");

	$oc_detail = $db->select("(SELECT * FROM tb_orden_compra_detalle WHERE dc_orden_compra = {$_POST['id']}) d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	LEFT JOIN tb_nota_venta_detalle dnv ON dnv.dc_nota_venta_detalle = d.dc_detalle_nota_venta",
	'p.dg_codigo,p.dg_producto,p.dc_producto,
	d.dc_detalle,d.dc_detalle_nota_venta,d.dg_descripcion,d.dq_cantidad,d.dq_precio as dq_precio_compra,
	d.dc_recepcionada,d.dc_proveedor,d.df_fecha_arribo,
	dnv.dc_comprada-dnv.dq_cantidad+d.dq_cantidad dc_pendiente');
	foreach($oc_detail as &$v){
		if(!empty($v['df_fecha_arribo'])){
			$v['df_fecha_arribo'] = $db->dateLocalFormat($v['df_fecha_arribo']);
		}
	}
?>
</div></div>
<?php
require_once(__DIR__ . '/../../../inc/Factory.class.php');
?>
<script type="text/javascript" src="jscripts/products_manager_orden_compra.js?v1_2"></script>
<script type="text/javascript" src="jscripts/sites/ventas/orden_compra/dateArribo.js"></script>
<script type="text/javascript">
	dateArribo.init();
	var setFechaArriboProducto = '<?php echo Factory::buildUrl('CrearOrdenCompra', 'ventas', 'setFechaArriboProducto', 'orden_compra'); ?>';
	tc = <?=$data['dq_cambio'] ?>;
	tc_dec = $(":hidden[name=decimal<?=$data['dc_tipo_cambio'] ?>]").val();

	empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	empresa_dec = conf.moneda_ndecimal;

	var detalle = <?= json_encode($oc_detail) ?>;

	var id_nv = <?=$data['dc_nota_venta'] ?>;
</script>
<script type="text/javascript" src="jscripts/orden_compra/ed_orden_compra.js?v0_1_3"></script>
