<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Gestión de compras</div><div id="main_cont"><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$meses = array(
	1 => 'Enero',
	2 => 'Febrero',
	3 => 'Marzo',
	4 => 'Abril',
	5 => 'Mayo',
	6 => 'Junio',
	7 => 'Julio',
	8 => 'Agosto',
	9 => 'Septiembre',
	10 => 'Octubre',
	11 => 'Noviembre',
	12 => 'Diciembre'
);

$form->Start('#','load_compras','load_compras');
$form->Header('Indique los filtros a aplicar en las compras pendientes');
$form->Section();
$form->Text('Documento','sel_doc');
$form->EndSection();
$form->Section();
$form->Select('Mes inicio','sel_inim',$meses,1,1);
$form->Select('Mes fin','sel_finm',$meses,1,12);
$form->EndSection();
$form->Section();
$form->Listado('Proveedor','sel_prov','tb_proveedor',array('dc_proveedor','dg_razon'));
$form->Listado('Cliente','sel_cli','tb_cliente',array('dc_cliente','dg_razon'));
$form->EndSection();
$form->End('Filtrar');

$form->Start('sites/ventas/orden_compra/proc/masive_crear_orden_compra.php','crear_orden_compra','crear_orden_compra');
$form->Header('
<div class="right" id="filter_menu">
<button class="button" type="button">Ver/Ocultar Filtros</button><br />
<div class="hidden">
	<label><input type="checkbox" id="semaforo" checked="checked" />Sémaforo</label><br />
	<label><input type="checkbox" id="docnum" checked="checked" />Número Documento</label><br />
	<label><input type="checkbox" id="docdate" />Fecha emisión</label><br />
	<label><input type="checkbox" id="docclient" />Cliente</label><br />
	<label><input type="checkbox" id="doctc" checked="checked" />Tipo de cambio</label><br />
	<label><input type="checkbox" id="docvtc" />Valor tipo de cambio</label><br />
	<label><input type="checkbox" id="pcode" checked="checked" />Código producto</label><br />
	<label><input type="checkbox" id="pname" checked="checked" />Nombre Producto</label><br />
	<label><input type="checkbox" id="pprov" checked="checked" />Proveedor</label><br />
	<label><input type="checkbox" id="pprice" />Monto unitario</label><br />
	<label><input type="checkbox" id="pcant" />Cantidad solicitada</label><br />
	<label><input type="checkbox" id="pendcant" checked="checked" />Cantidad a comprar</label><br />
	<label><input type="checkbox" id="pendprice" checked="checked" />Monto a comprar</label>
</div>
</div>
Compras pendientes','style="position:relative"');
echo('<table class="tab" width="100%" id="pend_table"><thead><tr>
	<th>OPT</th>
	<th width="20">&nbsp;</th>
	<th width="20"><input type="checkbox" id="select_all"></th>
	<th width="120">Documento</th>
	<th class="hidden2">Fecha Emision</th>
	<th class="hidden2">Cliente</th>
	<th width="80">Tipo cambio</th>
	<th class="hidden2">Valor tipo cambio</th>
	<th width="100">Código</th>
	<th>Producto</th>
	<th>Proveedor</th>
	<th width="100" class="hidden2">Monto Unitario</th>
	<th width="100" class="hidden2">Cant. Solicitada</th>
	<th width="100">Cant. a comprar</th>
	<th width="100">Monto a comprar</th>
</tr></thead><tbody id="prod_list"></tbody>
</table>');
$form->End('Crear Ordenes de compra','addbtn');

$pendientes = $db->select("(SELECT * FROM tb_nota_venta WHERE dm_validada = '1' AND dm_confirmada = '1' AND dc_comprada < dc_solicitada) nv
JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta
JOIN tb_producto p ON p.dc_producto = d.dc_producto
JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto AND tp.dm_controla_inventario = 0
LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = nv.dc_tipo_cambio",
'nv.dc_nota_venta,nv.dq_nota_venta,nv.dc_cliente,tc.dg_tipo_cambio,tc.dn_cantidad_decimales,nv.dq_cambio,p.dg_codigo,p.dg_producto,d.dc_proveedor,d.dq_cantidad,nv.dc_tipo_cambio,d.dc_nota_venta_detalle,p.dq_precio_venta,d.dc_comprada,DATE_FORMAT(nv.df_fecha_emision,"%d/%m/%Y") AS df_fecha_emision,MONTH(nv.df_fecha_emision) AS dc_month');

?>
</div></div>
<script type="text/javascript" src="jscripts/product_manager/gestor_orden_compra.js?v0_8"></script>
<script type="text/javascript">
	js_data.pendientes = <?=json_encode($pendientes) ?>;
	js_data.init();
</script>