<?php
define('MAIN',1);
include('../../../inc/global.php');

$data = $db->select('tb_orden_compra','dq_orden_compra, dm_nula',"dc_orden_compra = {$_POST['id']}");

if(!count($data)){
	$error_man->showWarning('La orden de compra especificada es inválida');
	exit;
}

$data = array_shift($data);

if($data['dm_nula'] == 1){
	$error_man->showAviso('La orden de compra seleccionada ya ha sido anulada');
	exit;
}

$dq_orden_compra = $data['dq_orden_compra'];
unset($data);

echo('<div class="secc_bar">Anular Orden de compra</div><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/orden_compra/proc/null_orden_compra.php','null_orden_form');
$form->Header('Indique las razones por las que se anulará la orden de compra');
$error_man->showAviso("Atención, está a punto de anular la orden de compra N° <b>{$dq_orden_compra}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('id_orden_compra',$_POST['id']);
$form->End('Anular','delbtn');

?>
</div>