<?php
/**
 * Clase EditarOrdenCompraFactory Busca agregar nuevas funcionalidades al modulo de Orden de compra.
 * Se utilizará como complemento al desarrollo anterior. 
 *  
 **/ 
class AsignarFacturaCompraFactory Extends Factory {
    
    protected $title = 'Asignación de Facturas de compra de servicios';
    private $ordenCompra;
    private $facturasAsignadas;
    private $facturasDisponibles;
    private $asignaDetalles = array();
    private $asignaComprobantes = array();
    private $errorMsg = array();
	
	public function buscarFacturaCompraAction(){
		$this->verificarDatosOrdenCompra();
	}
	
	public function showAsignarFacturaCompraAction(){
        $this->initFunctionsService();
        $datos = array();
        $datos['ordenCompra'] = $this->verificarDatosOrdenCompra();
        $datos['facturasAsignadas'] = $this->obtenerFacturasCompraAsignadas($datos['ordenCompra']);
        $datos['facturasDisponibles'] = $this->getFacturasCompraCoincidentes($datos['ordenCompra']);
        $datos['errorMsg'] = $this->errorMsg;
       // Factory::debug($datos['facturasDisponibles']);
        $form = $this->getFormView($this->getTemplateUrl('asignar_factura_compra'), $datos);
        echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
        
	}
	
	private function verificarDatosOrdenCompra() {
		$r = $this->getRequest();
		if(!isset($r->dc_orden_compra) || !is_numeric($r->dc_orden_compra)){
			$this->getErrorMan()->showWarning('No se ha designado una orden de compra. Contacte al administrador');
			exit;
		}
		$db = $this->getConnection();
		$ordenCompra = $db->getRowById('tb_orden_compra', $r->dc_orden_compra, 'dc_orden_compra');
        $this->validateOrdenCompra($ordenCompra);
		$detalles = $this->getDetallesOrdenCompra($ordenCompra);
        $ordenCompra->detalles = $this->getDetallesDeServicio($detalles);
        if(empty($ordenCompra->detalles)){
            $this->errorMsg[] = $this->getErrorMan()->showWarning('La orden de compra no tiene servicios pendientes de recepción. Verifique su información', 1);
        }
        return $ordenCompra;
		
	}
    
    private function validateOrdenCompra($ordenCompra) {
        if(!$ordenCompra || $ordenCompra->dm_nula) {
			$this->getErrorMan()->showWarning('No se ha encontrado la orden de compra. Contacte al administrador');
			exit;
		}
        if($ordenCompra->dc_empresa != $this->getEmpresa()) {
            $this->getErrorMan()->showWarning('La orden de compra no corresponde a la empresa actual. Contacte al administrador');
			exit;
        }
    }
	
	private function getDetallesOrdenCompra($ordenCompra) {
		$db = $this->getConnection();
		$table = 'tb_orden_compra_detalle ocd';
		$table .= ' JOIN tb_producto prod on ocd.dc_producto = prod.dc_producto ';
		$table .= ' JOIN tb_tipo_producto tp on prod.dc_tipo_producto = tp.dc_tipo_producto ';
		
        $fields = 'ocd.dc_detalle, ocd.dc_orden_compra, ocd.dc_producto, ocd.dc_proveedor, ocd.dq_cantidad, ocd.dc_recepcionada';
		$fields .= ', ocd.dc_detalle_nota_venta, ocd.dc_detalle_orden_servicio ';
		$fields .= ', prod.dc_tipo_producto, prod.dc_linea_negocio';
		$fields .= ', tp.dm_controla_inventario, tp.dm_orden_compra';
        
        $conditions = 'ocd.dc_orden_compra = :ordenCompra';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':ordenCompra', $ordenCompra->dc_orden_compra, PDO::PARAM_INT);
        $db->stExec($st);
        
        return $st->fetchAll(PDO::FETCH_OBJ);
	}
    
    private function getDetallesDeServicio($detalles) {
        if($detalles === false){
            $this->getErrorMan()->showWarning('La orden de compra seleccionada no posee detalles. Verifique su información.');
			exit;
        }
        
        $servicios = array();
        foreach ($detalles as $v) {
            if (!$v->dm_orden_compra) {
                continue;
            }
            
            if (!$v->dm_controla_inventario) {
                continue;    
            }
            
            if ($v->dq_cantidad == $v->dc_recepcionada) {
                continue;
            }
            $servicios[] = $v;
        }
        
        return $servicios;
    }
    
    private function obtenerFacturasCompraAsignadas($ordenCompra){
        $db = $this->getConnection();
        $table = 'tb_comprobante_recepcion_servicio_orden_compra';
        $fields = 'dc_comprobante, dq_comprobante, dc_orden_compra, dc_factura_compra, dg_comentario, df_creacion';
        $conditions = 'dc_orden_compra = :ordenCompra AND dc_empresa = :empresa';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':ordenCompra', $ordenCompra->dc_orden_compra,PDO::PARAM_INT);
        $st->bindValue(':empresa', $this->getEmpresa(),PDO::PARAM_INT);
        
        $db->stExec($st);
        
        $asignaciones = $this->completarDatosAsignaciones($st->fetchAll(PDO::FETCH_OBJ));
        
        return $asignaciones;
        
    }
    
    private function completarDatosAsignaciones($detalles) {
        if($detalles === false) {
            $this->getErrorMan()->showWarning('Existe un error al consultar las facturas asignadas. Contacte al administrador.');
			exit;
        }
        
        $asignacionesComplete = $this->completarComprobanteAsignaciones($detalles);
        $comprobantesComplete = $this->getDetallesComprobanteAsignaciones($asignacionesComplete);
        
        return $detalles;
    }
    
    private function completarComprobanteAsignaciones($comprobantes) {
        $db = $this->getConnection();
        
        foreach($comprobantes as $c => $v){
            $comprobantes[$c]->dc_orden_compra = clone $db->getRowById('tb_orden_compra', $v->dc_orden_compra, 'dc_orden_compra' );
            $comprobantes[$c]->dc_factura_compra = clone $db->getRowById('tb_factura_compra', $v->dc_factura_compra, 'dc_factura' );
        }
        return $comprobantes;
    }
    
    private function getDetallesComprobanteAsignaciones($comprobantes) {
        $db = $this->getConnection();
        $table = 'tb_comprobante_recepcion_servicio_orden_compra_detalle';
        $fields = 'dc_detalle, dc_comprobante_recepcion_servicio, dc_detalle_orden_compra, dc_detalle_factura_compra, dc_cantidad, df_creacion';
        $conditions = 'dc_comprobante_recepcion_servicio = :comprobante';
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $comprobante = null;
        $st->bindParam(':comprobante', $comprobante, PDO::PARAM_INT);
        
        foreach($comprobantes as $c => $v) {
            $comprobante = $v->dc_comprobante;
            $db->stExec($st);
            $comprobantes[$c]->detalles = $st->fetchAll(PDO::FETCH_OBJ);
        }
        
        $comprobantesDetallados = $this->completarDetallesComprobantesAsignaciones($comprobantes);
        return $comprobantesDetallados;
    }
    
    private function completarDetallesComprobantesAsignaciones($comprobantes){
        $db = $this->getConnection();
        foreach ($comprobantes as $c => $v) {
            foreach($v->detalles as $k => $w) {
                
                $comprobantes[$c]->detalles[$k]->dc_detalle_orden_compra = clone $db->getRowById(
                    'tb_orden_compra_detalle', $w->dc_detalle_orden_compra, 'dc_detalle');
                    
                $comprobantes[$c]->detalles[$k]->dc_detalle_factura_compra = clone $db->getRowById(
                    'tb_factura_compra_detalle', $w->dc_detalle_factura_compra, 'dc_detalle');
            }
        }
        
        return $comprobantes;
        
    }
    
    private function getFacturasCompraCoincidentes($ordenCompra){
        if(!empty($this->errorMsg)) {
            return array();
        }
        $productos = array();
        foreach ($ordenCompra->detalles as $v) {
            $productos[] = $v->dc_producto;
        }
        $detallesFacturas = $this->getDetalleFacturaCompraServicios($productos, $ordenCompra->dc_proveedor);
        $facturasCampos = $this->getCamposFacturasCompra($detallesFacturas);
        $facturasCompletas = $this->completarDatosFacturaCompra($facturasCampos);
        return $facturasCompletas;
    }
    
    private function getDetalleFacturaCompraServicios($productos, $proveedor) {
        $db = $this->getConnection();
        
        $b = str_repeat('?,', count($productos));
        $cantidadProductos = trim($b, ',');
        
        $table = 'tb_factura_compra fc';
        $table .= ' JOIN tb_factura_compra_detalle fcd on fc.dc_factura = fcd.dc_factura ';
        $fields = 'fcd.dc_detalle, fcd.dc_factura, fcd.dc_producto, fcd.dg_descripcion, fcd.dc_cantidad, fcd.dc_recepcionada';
        $conditions = "fc.dm_nula = 0 AND fc.dc_empresa = ?";
        $conditions .= " AND fc.dc_proveedor = ? AND fcd.dc_producto IN ({$cantidadProductos}) ";
        $conditions .= " AND fcd.dc_cantidad > fcd.dc_recepcionada ";
        
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $counter = 1;
        $st->bindValue($counter++, $this->getEmpresa(), PDO::PARAM_INT);
        $st->bindValue($counter++, $proveedor, PDO::PARAM_INT);
        foreach($productos as $v){
            $st->bindValue($counter++, $v, PDO::PARAM_INT);
        }
        
        $db->stExec($st);
        
        return $st->fetchAll(PDO::FETCH_OBJ);
        
    }
    
    private function getCamposFacturasCompra($detalles) {
        $db = $this->getConnection();
        $facturas = array();
        foreach($detalles as $v) {
            $facturas[$v->dc_factura] = null;
        }
        
        foreach (array_keys($facturas) as $v) {
            $facturas[$v] = clone $db->getRowById('tb_factura_compra', $v, 'dc_factura');
        }
        
        foreach($detalles as $v){
            $facturas[$v->dc_factura]->detalles[] = $v;
        }
        
        return $facturas;
    }
    
    private function completarDatosFacturaCompra($facturas) {
        $db = $this->getConnection();
        foreach ($facturas as $c => $v) {
            $facturas[$c]->dc_proveedor = clone $db->getRowById('tb_proveedor', $v->dc_proveedor, 'dc_proveedor', 'dc_proveedor, dc_contacto_default, dg_rut, dg_razon, dg_giro');
        }
        return $facturas;
    }
    
    //Asignación de resultados. ; tb_nota_venta_detalle
    
    public function asignarFacturasAction(){
        $this->ordenCompra = $this->verificarDatosOrdenCompra();
        $this->facturasAsignadas = $this->obtenerFacturasCompraAsignadas($this->ordenCompra);
        $this->facturasDisponibles = $this->getFacturasCompraCoincidentes($this->ordenCompra);
        $facturas = $this->getFacturasParaAsignacion();
        $facturasCompletas = $this->getFacturasConDetalles($facturas);
        $asignaciones = $this->getDetallesParaAsignar($facturasCompletas);
        $this->asignarComprobantesRecepcionServicio($asignaciones);
    }
    
    private function getFacturasParaAsignacion() {
        $r = $this->getRequest();
        if(!isset($r->dc_factura_compra)){
            $this->getErrorMan()->showInfo('No se ha seleccionado ninguna factura para asignacion. No se ha guardado ningún registro.');
            exit;
        }
        $facturas = $r->dc_factura_compra;
        foreach($facturas as $v) {
            if(!is_numeric($v)){
                $this->getErrorMan()->showWarning('Error al obtener las facturas. Contacte al administrador');
            }
        }
        return $facturas;
    }
    
    private function getFacturasConDetalles($facturas){
        $facturasCompletas = array();
        foreach($facturas as $v) {
            if(!isset($this->facturasDisponibles[$v])){
                $this->getErrorMan()->showWarning('Una de las facturas seleccionadas no corresponde para 
                    la orden de compra. Contacte al administrador.');
                    exit;
            }
            $facturasCompletas[$v] = $this->facturasDisponibles[$v];
        }
        return $facturasCompletas;
    }
    
    private function getDetallesParaAsignar($facturas){
        $arr = array();
        foreach($this->ordenCompra->detalles as &$ocd) {
            $arr[] = $this->recorreFacturas($ocd, $facturas);
        }
        return $arr;
    }
    
    private function recorreFacturas(&$ocd, &$facturas) {
        $arr = array();
        foreach($facturas as &$fac){
            $detalles = $this->recorreDetallesFacturas($ocd, $fac->detalles);
            if(empty($detalles)){
                continue;
            }
            $obj = new stdClass();
            $obj->dc_orden_compra = $ocd->dc_orden_compra;
            $obj->dc_factura_compra = $fac->dc_factura;
            $obj->dg_comentario = "Asignacion automatica orden compra {{$this->ordenCompra->dq_orden_compra}}, factura {$fac->dq_factura}";
            $obj->dc_empresa = $this->getEmpresa();
            $obj->dc_usuario_creacion = $this->getUserData()->dc_usuario;
            $obj->detalles = $detalles;
            $arr[] = $obj;
        }
        return $obj;
    }
    
    private function recorreDetallesFacturas(&$ocd, &$detalles) {
        $arr = array();
        foreach($detalles as &$fcd) {
            $arr[] = $this->enlazarDetalles($ocd, $fcd);
        }
        return array_filter($arr);
    }
    
    private function enlazarDetalles(&$ocd, &$fcd) {
        if($fcd->dc_cantidad == $fcd->dc_recepcionada || $ocd->dq_cantidad == $ocd->dc_recepcionada) {
            return false;
        }
        $cantidadOrdenCompra = $ocd->dq_cantidad - $ocd->dc_recepcionada;
        $cantidadFacturaCompra = $fcd->dc_cantidad - $fcd->dc_recepcionada;
        
        $obj = new stdClass();
        $obj->dc_detalle_orden_compra = $ocd->dc_detalle;
        $obj->dc_detalle_factura_compra = $fcd->dc_detalle;
        $obj->dc_detalle_nota_venta = $ocd->dc_detalle_nota_venta ? $ocd->dc_detalle_nota_venta:null;
        $obj->dc_detalle_orden_servicio = $ocd->dc_detalle_orden_servicio ? $ocd->dc_detalle_orden_servicio:null;
        if($cantidadOrdenCompra >= $cantidadFacturaCompra){
            $obj->dc_cantidad = $cantidadFacturaCompra;
            $ocd->dc_recepcionada += $cantidadFacturaCompra;
            $fcd->dc_recepcionada += $cantidadFacturaCompra;
        } else {
            $obj->dc_cantidad = $cantidadOrdenCompra;
            $ocd->dc_recepcionada += $cantidadOrdenCompra;
            $fcd->dc_recepcionada += $cantidadOrdenCompra;
        }
        return $obj;
        
    }
    
    private function asignarComprobantesRecepcionServicio($asignaciones) {
        $this->getConnection()->start_transaction();
        $comprobantes = array();
        foreach($asignaciones as &$v) {
            $comprobantes[] = $this->asignarComprobante($v);
        }
        $this->getConnection()->commit();
        $msg = "Se han guardado las asignaciones. Se han registrado los siquientes comprobantes: <ul>";
        foreach($comprobantes as $v) {
            $msg .= "<li>{$v}</li>";
        }
        $msg .= "</ul>";
        $this->getErrorMan()->showConfirm($msg);
    }
    
    private function asignarComprobante($comprobante) {
        $db = $this->getConnection();
        $table = 'tb_comprobante_recepcion_servicio_orden_compra';
        $fields = array(
            'dq_comprobante' => ':comprobante',
            'dc_orden_compra' => ':dc_orden_compra',
            'dc_factura_compra' => ':dc_factura_compra',
            'dg_comentario' => ':dg_comentario',
            'dc_empresa' => ':dc_empresa',
            'dc_usuario_creacion' => ':dc_usuario_creacion',
            'df_creacion' => $db->getNow(),
        );
        
        $document = $this->getService('DocumentCreation');
        $dq_comprobante = $document->getCorrelativo($table,'dq_comprobante', 2, 'df_creacion');
        $st = $db->prepare($db->insert($table, $fields));
        $st->bindValue(':comprobante', $dq_comprobante, PDO::PARAM_INT);
        $st->bindValue(':dc_orden_compra', $comprobante->dc_orden_compra, PDO::PARAM_INT);
        $st->bindValue(':dc_factura_compra', $comprobante->dc_factura_compra, PDO::PARAM_INT);
        $st->bindValue(':dg_comentario', $comprobante->dg_comentario, PDO::PARAM_STR);
        $st->bindValue(':dc_empresa', $comprobante->dc_empresa, PDO::PARAM_INT);
        $st->bindValue(':dc_usuario_creacion', $comprobante->dc_usuario_creacion, PDO::PARAM_INT);
        $db->stExec($st);
        $id = $db->lastInsertId();
        foreach($comprobante->detalles as &$v){
            $this->insertarDetalleComprobante($id, $v);
        }
        return $dq_comprobante;
    }
    
    private function insertarDetalleComprobante($id, $detalle) {
        $db = $this->getConnection();
        $table = 'tb_comprobante_recepcion_servicio_orden_compra_detalle';
        $fields = array(
            'dc_comprobante_recepcion_servicio' => ':dc_comprobante_recepcion_servicio',
            'dc_detalle_orden_compra' => ':dc_detalle_orden_compra',
            'dc_detalle_factura_compra' => ':dc_detalle_factura_compra',
            'dc_detalle_nota_venta' => ':dc_detalle_nota_venta',
            'dc_detalle_orden_servicio' => ':dc_detalle_orden_servicio',
            'dc_cantidad' => ':dc_cantidad',
            'df_creacion' => $db->getNow(),
        );
        $st = $db->prepare($db->insert($table, $fields));
        $st->bindValue(':dc_comprobante_recepcion_servicio', $id, PDO::PARAM_INT);
        $st->bindValue(':dc_detalle_orden_compra', $detalle->dc_detalle_orden_compra, PDO::PARAM_INT);
        $st->bindValue(':dc_detalle_factura_compra', $detalle->dc_detalle_factura_compra, PDO::PARAM_INT);
        $st->bindValue(':dc_detalle_nota_venta', $detalle->dc_detalle_nota_venta, PDO::PARAM_INT);
        $st->bindValue(':dc_detalle_orden_servicio', $detalle->dc_detalle_orden_servicio, PDO::PARAM_INT);
        $st->bindValue(':dc_cantidad', $detalle->dc_cantidad, PDO::PARAM_INT);
        $db->stExec($st);
        
        $this->actualizarDetalleNotaVenta($detalle);
        $this->actualizarDetalleOrdenServicio($detalle);
        $this->actualizarDetalleOrdenCompra($detalle);
        $this->actualizarDetalleFacturaCompra($detalle);
    }
    
    private function actualizarDetalleNotaVenta($detalle) {
        if(empty($detalle->dc_detalle_nota_venta)){
            return;
        }
        $db = $this->getConnection();
        $table = 'tb_nota_venta_detalle';
        $fields = array(
            'dc_recepcionada' => 'dc_recepcionada + :cantidad',
        );
        $conditions = 'dc_nota_venta_detalle = :detalle';
        $st = $db->prepare($db->update($table, $fields, $conditions));
        $st->bindValue(':cantidad', $detalle->dc_cantidad, PDO::PARAM_INT);
        $st->bindValue(':detalle', $detalle->dc_detalle_nota_venta, PDO::PARAM_INT);
        
        $db->stExec($st);
    }
    
    private function actualizarDetalleOrdenServicio($detalle) {
        if(empty($detalle->dc_detalle_orden_servicio)){
            return;
        }
        $db = $this->getConnection();
        $table = 'tb_orden_servicio_factura_detalle';
        $fields = array(
            'dc_recepcionada' => 'dc_recepcionada + :cantidad',
        );
        $conditions = 'dc_detalle = :detalle';
        
        $st = $db->prepare($db->update($table, $fields, $conditions));
        $st->bindValue(':cantidad', $detalle->dc_cantidad, PDO::PARAM_INT);
        $st->bindValue(':detalle', $detalle->dc_detalle_orden_servicio, PDO::PARAM_INT);
        $db->stExec($st);
    }
    
    
    private function actualizarDetalleOrdenCompra($detalle) {
        $db = $this->getConnection();
        $table = 'tb_orden_compra_detalle';
        $fields = array(
            'dc_recepcionada' => 'dc_recepcionada + :cantidad',
        );
        $conditions = 'dc_detalle = :detalle';
        $st = $db->prepare($db->update($table, $fields, $conditions));
        $st->bindValue(':cantidad', $detalle->dc_cantidad, PDO::PARAM_INT);
        $st->bindValue(':detalle', $detalle->dc_detalle_orden_compra, PDO::PARAM_INT);
        
        $db->stExec($st);
    }
    
    private function actualizarDetalleFacturaCompra($detalle) {
        $db = $this->getConnection();
        $table = 'tb_factura_compra_detalle';
        $fields = array(
            'dc_recepcionada' => 'dc_recepcionada + :cantidad',
        );
        $conditions = 'dc_detalle = :detalle';
        $st = $db->prepare($db->update($table, $fields, $conditions));
        $st->bindValue(':cantidad', $detalle->dc_cantidad, PDO::PARAM_INT);
        $st->bindValue(':detalle', $detalle->dc_detalle_factura_compra, PDO::PARAM_INT);
        
        $db->stExec($st);
    }
	
}
