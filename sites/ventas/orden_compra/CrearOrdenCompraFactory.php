<?php
//Esta clase podrá ser utilizada para la creación futura del módulo en factory.
class CrearOrdenCompraFactory extends Factory{
	protected $title = 'CrearOrdenCompraFactory';

	public function setFechaArriboProductoAction(){
		$r = $this->getRequest();
		$fecha = isset($r->fecha) ? $r->fecha:'';
		$form =  $this->getFormView($this->getTemplateUrl('dateSelect'), array('fecha' => $fecha));
		echo $this->getOverlayView($form, array(),Factory::STRING_TEMPLATE);
	}
}
