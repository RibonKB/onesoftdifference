<?php
define("MAIN",1);
require_once("../../../inc/global.php");

$dc_orden_compra = intval($_POST['id']);

if(!$dc_orden_compra){
	$error_man->showWarning('La factura ingresada es inválida, compruebe los valores de entrada');
	exit;
}

$data = $db->select('tb_orden_compra','dq_orden_compra, dq_neto+dq_iva dq_total, dq_monto_pagado','dc_orden_compra = '.$dc_orden_compra);
$data = array_shift($data);

if($data == NULL){
	$error_man->showWarning('No se encontró la factura de compra, compruebe los datos de entrada');
	exit;
}

if($data['dq_monto_pagado'] >= $data['dq_total']){
	$error_man->showAviso('La orden de compra ya fue pagada completamente');
	exit;
}

$guia_recepcion=$db->select('tb_guia_recepcion','dq_guia_recepcion',"dc_orden_compra={$dc_orden_compra} AND dm_nula=0");
$guia_recepcion = array_shift($guia_recepcion);
if($guia_recepcion != NULL){
        $msg='';
            foreach ($guia_recepcion as $dq):
              $msg=$msg."<span style='font-weight: bolder'>{$dq}</span><br/><br/>";    
            endforeach;
            $error_man->showWarning('No se puede realizar el pago <br> La orden de compra tiene la(s) siguiente(s) guia(s) de recepcion asociada(s): <br/><br/>
                '.$msg);
	exit;
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Pago anticipado Orden de compra <b><?php echo $data['dq_orden_compra'] ?></b>
</div>
<div class="panes center">
	<?php
		$form->Start('sites/ventas/orden_compra/proc/cr_pago_anticipado.php','cr_pago','overlayValidar');
			$form->Header('Indique el medio de pago con el que se realizará la gestión de pago sobre la orden de compra');
			$form->Listado('Medio de pago','dc_medio_pago','tb_medio_pago_proveedor',array('dc_medio_pago','dg_medio_pago'),1);
			$form->Hidden('dc_orden_compra',$dc_orden_compra);
		$form->End('Iniciar Pago','addbtn');
	?>
</div>