<?php

/**
 * Description of ConsultarPagosAnticipados
 *
 * @author Tomás Lara Valdovinos
 * @date 14-08-2013
 */
class GestionPagosAnticipadosFactory extends Factory{
  
    protected $title = "Gestión Pagos Anticipados";
    protected $orden_compra;
    
    public function indexAction(){
        $this->orden_compra = $this->getOrdenCompra();
        $pagos = $this->getPagosAnticipados();
        
        echo $this->getOverlayView($this->getTemplateURL('showPagos'), array(
            'pagos' => $pagos
        ));
    }
    
    public function anularAction(){
      $this->validaAnular();
    }
    
    private function validaAnular(){
        $orden_compra = $this->getOrdenCompra();
        $pago = $this->getPagoAnticipado();
        
        $monto_anulable = $orden_compra->dq_monto_pagado - $orden_compra->dq_pago_usado;
        
        
        
    }
    
    private function getPagoAnticipado(){
      $r = self::getRequest();
      $db = $this->getConnection();
      
      $pago = $db->prepare($db->select('tb_detalle_pago_anticipado', 'dc_detalle, dq_monto', 'dc_comprobante = ? AND dc_orden_compra = ?'));
      $pago->bindValue(1, $r->dc_comprobante, PDO::PARAM_INT);
      $pago->bindValue(2, $r->dc_orden_compra, PDO::PARAM_INT);
      $db->stExec($pago);
      
      return $pago->fetch(PDO::FETCH_OBJ);
    }
    
    private function getPagosAnticipados(){
       $db = $this->getConnection();
       
       $pagos = $db->prepare($db->select('tb_detalle_pago_anticipado d
                              JOIN tb_comprobante_pago_anticipado c ON c.dc_comprobante = d.dc_comprobante
                              JOIN tb_medio_pago_proveedor m ON m.dc_medio_pago = c.dc_medio_pago',
                             'c.*, d.dq_monto, m.dg_medio_pago',
                             'd.dc_orden_compra = ?'));
       $pagos->bindValue(1, $this->orden_compra->dc_orden_compra, PDO::PARAM_INT);
       $db->stExec($pagos);
       
       return $pagos->fetchAll(PDO::FETCH_OBJ);
       
    }
    
    private function getOrdenCompra(){
        if($this->orden_compra instanceof stdClass):
          return $this->orden_compra;
        endif;
        
        $r = self::getRequest();
        $this->orden_compra = $this->getConnection()->getRowById('tb_orden_compra', $r->dc_orden_compra, 'dc_orden_compra');
        return $this->orden_compra;
    }
    private function VerificarOrdenCompra(){
        $dc_orden_compra=$this->getOrdenCompra();
        $guia=$this->coneccion('select', 'tb_guia_recepcion','dm_nula', "dc_orden_compra={$dc_orden_compra}");
    }
}

?>
