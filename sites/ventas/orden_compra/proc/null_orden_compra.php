<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
//51

/*if(check_permiso(51))
	$month_cond = '';
else
	$month_cond = 'AND MONTH(df_emision) = MONTH(NOW())';*/
	
if(!is_numeric($_POST['id_orden_compra'])){
	$error_man->showWarning("Ocurrió un error inesperado al intentar acceder a la orden de compra seleccionada, por favor compruebe que no intento ingresar desde un lugar indebido");
	exit();
}

//Comprobar que existe la orden de compra y que esta no está ya anulada.
$data = $db->select('tb_orden_compra',
	'dc_nota_venta,dq_orden_compra, dq_monto_pagado, dq_pago_usado, dq_monto_liberado',
	"dc_orden_compra={$_POST['id_orden_compra']} AND dm_nula = 0 AND dc_empresa = {$empresa}");
if(!count($data)){
	$error_man->showWarning('No se ha encontrado la orden de compra especificada o esta ya a sido anulada anteriormente.');
	exit();
}
$data = $data[0];

if(($data['dq_monto_pagado'] - $data['dq_pago_usado'] - $data['dq_monto_liberado']) != 0){
	$error_man->showWarning('No puede anular la orden de compra mientras no libere los montos pagados.');
	exit;
}

//Se comprueba que no se hayan recepcionado ninguno de los productos en la orden de compra.
$cantidades = $db->select('tb_orden_compra_detalle','SUM(dc_recepcionada) suma_recepcion, SUM(dq_cantidad) dc_comprada',"dc_orden_compra={$_POST['id_orden_compra']}");
if($cantidades[0]['suma_recepcion'] != 0){
	$error_man->showWarning("La orden de compra <b>{$data['dq_orden_compra']}</b> a sido parcial o totalmente recepcionada, no puede continuar el proceso de anulación.");
	exit;
}
$cantidades = $cantidades[0];

$db->start_transaction();

//Se anula la orden de compra a nivel de cabecera 
$db->update('tb_orden_compra',array('dm_nula'=>1),"dc_orden_compra={$_POST['id_orden_compra']}");

//Se registra en el log la anulación incluyendo su motivo y su comentario
$db->insert('tb_orden_compra_anulada',array(
	'dc_orden_compra' => $_POST['id_orden_compra'],
	'dc_motivo' => $_POST['null_motivo'],
	'dg_comentario' => $_POST['null_comentario'],
	'dc_usuario' => $idUsuario,
	'df_anulacion' => 'NOW()'
));

//Se restan las cantidades compradas en caso de que la orden de compra tenga una nota de venta relacionada
if($data['dc_nota_venta'] != 0){
	
	//Se restan las cantidades compradas a nivel de cabecera en la nota de venta
	$db->update('tb_nota_venta',array(
		'dc_comprada' => "dc_comprada-{$cantidades['dc_comprada']}"
	),"dc_nota_venta = {$data['dc_nota_venta']}");

	//Se desligan los detalles de la orden de compra de los detalles de la nota de venta, y se realiza la rebaja de cantidades compradas en los últimos
	$db->update("tb_orden_compra_detalle ocd
	JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = ocd.dc_detalle_nota_venta",array(
		'ocd.dc_detalle_nota_venta' => 0,
		'nvd.dc_comprada' => 'nvd.dc_comprada-ocd.dq_cantidad'
	),"ocd.dc_orden_compra = {$_POST['id_orden_compra']}");
}

//Desligar orden de servicio y restar cantidades compradas
//CODE-HERE

$db->commit();
$error_man->showAviso("Atención a anulado la orden de compra <b>{$data['dq_orden_compra']}</b> y la relación con otros documentos");
?>
<script type="text/javascript">
$('#null_orden_form').prev('input').remove();
$('#null_orden_form').remove();
$('#res_list .confirm .oc_load').trigger('click');
</script>