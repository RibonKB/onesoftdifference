<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_orden_compra = intval($_POST['dc_orden_compra']);

$orden_compra = $db->prepare($db->select('tb_orden_compra','dc_proveedor, dq_monto_pagado, dq_pago_usado, dq_monto_liberado','dc_orden_compra = ? AND dc_empresa = ?'));
$orden_compra->bindValue(1,$dc_orden_compra,PDO::PARAM_INT);
$orden_compra->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($orden_compra);
$orden_compra = $orden_compra->fetch(PDO::FETCH_OBJ);

if($orden_compra === false){
	$error_man->showWarning('La orden de compra seleccionada es inválida, compruebe los datos de entrada.');
	exit;
}

$dq_disponible = floatval($orden_compra->dq_monto_pagado - $orden_compra->dq_pago_usado - $orden_compra->dq_monto_liberado);

if($dq_disponible <= 0):
	$error_man->showAviso('No hay saldos anticipados para liberar');
	exit;
endif;

require_once("../../proc/ventas_functions.php");
$dq_liberacion = doc_GetNextNumber('tb_comprobante_liberacion_anticipo','dq_liberacion');

$db->start_transaction();

//Ingresar comprobante de liberación
$insert_comprobante = $db->prepare($db->insert('tb_comprobante_liberacion_anticipo',array(
	'dq_liberacion' => $dq_liberacion,
	'dc_orden_compra' => '?',
	'dq_monto' => $dq_disponible,
	'dg_comentario' => '?',
	'df_emision' => $db->getNow(),
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
)));
$insert_comprobante->bindValue(1,$dc_orden_compra,PDO::PARAM_INT);
$insert_comprobante->bindValue(2,$_POST['dg_comentario'],PDO::PARAM_STR);

$db->stExec($insert_comprobante);

//Modificar saldo en la orden de compra
$update_orden_compra = $db->prepare($db->update('tb_orden_compra',array(
	'dq_monto_liberado' => 'dq_monto_liberado+?'
),'dc_orden_compra = ?'));
$update_orden_compra->bindValue(1,$dq_disponible,PDO::PARAM_STR);
$update_orden_compra->bindValue(2,$dc_orden_compra,PDO::PARAM_INT);
$db->stExec($update_orden_compra);

//Agregar el saldo a favor al proveedor
$update_proveedor = $db->prepare($db->update('tb_proveedor',array(
	'dq_saldo_favor' => 'dq_saldo_favor+?'
),'dc_proveedor = ?'));
$update_proveedor->bindValue(1,$dq_disponible,PDO::PARAM_STR);
$update_proveedor->bindValue(2,$orden_compra->dc_proveedor,PDO::PARAM_INT);

$db->stExec($update_proveedor);

$db->commit();

$error_man->showAviso('Atención: Se han liberado los saldos anticipados de la orden de compra, estos estarán disponible como saldos a favor para el proveedor');

?>
<script type="text/javascript">
	$('#res_list .confirm .oc_load').trigger('click');
</script>