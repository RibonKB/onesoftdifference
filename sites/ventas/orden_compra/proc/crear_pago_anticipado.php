<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_orden_compra = intval($_POST['dc_orden_compra']);
$dc_medio_pago = intval($_POST['dc_medio_pago']);

$medio_pago = $db->prepare($db->select('tb_medio_pago_proveedor',
'dc_tipo_movimiento, dm_requiere_banco, dm_asiento_intermedio, dm_requiere_documento, dc_cuenta_contable_intermedia, dg_medio_pago',
"dc_empresa = ? AND dc_medio_pago = ?"));
$medio_pago->bindValue(1,$empresa,PDO::PARAM_INT);
$medio_pago->bindValue(2,$dc_medio_pago,PDO::PARAM_INT);
$db->stExec($medio_pago);
$medio_pago = $medio_pago->fetch(PDO::FETCH_OBJ);

if($medio_pago === false){
	$error_man->showWarning('No se ha encontrado el medio de pago utilizado para hacer el pago.');
	exit;
}

//Validar que el medio de pago no haya cambiado en el formulario anterior
if(
	$medio_pago->dc_tipo_movimiento != $_POST['dc_tipo_movimiento'] ||
	$medio_pago->dm_requiere_banco != $_POST['dm_requiere_banco'] ||
	$medio_pago->dm_requiere_documento != $_POST['dm_requiere_documento']
){
	$error_man->showWarning('La configuración de Medios de pago ha cambiado, no se puede continuar debido a problemas de integridad.<br />Consulte con el administrador.');
	exit;
}

//Obtener monto adeudado factura de compra
$orden_compra = $db->getRowById('tb_orden_compra',$dc_orden_compra,'dc_orden_compra');

if($orden_compra === false){
	$error_man->showWarning('Error obteniendo la información de pago de la orden de compra');
	exit;
}
foreach($_POST['dq_monto'] as $i => $d){
$verifica_cheque=$db->doQuery($db->select('tb_pago_propuesta papro LEFT JOIN tb_propuesta_pago propa ON propa.dc_propuesta=papro.dc_propuesta','*',"papro.dc_banco=".$_POST['dc_banco'][$i]." AND papro.dg_documento='".$_POST['dg_documento'][$i]."' AND propa.dm_nula=0" ));
$verifica_cheque=$verifica_cheque->fetch();
if($verifica_cheque != FALSE){
    $error_man->showWarning('El documento ya se usó para una propuesta de pago.');
	exit;
}

$verifica_chequeII=$db->doQuery($db->select('tb_documento_pago_anticipado dpa LEFT JOIN tb_comprobante_pago_anticipado cpa ON cpa.dc_comprobante=dpa.dc_comprobante','*',"dpa.dc_banco=".$_POST['dc_banco'][$i]." AND dpa.dg_documento='".$_POST['dg_documento'][$i]."' AND cpa.dm_nula=0" ));
$verifica_chequeII=$verifica_chequeII->fetch();
if($verifica_chequeII != FALSE){
    $error_man->showWarning('El documento ya se usó para otro pago anticipado.');
	exit;
}
}

$orden_compra->dq_monto = floatval($orden_compra->dq_neto+$orden_compra->dq_iva-$orden_compra->dq_monto_pagado);
$dq_monto = $orden_compra->dq_monto;

/**
*	Validar valor adeudado menor o igual al total pagar
**/
if($medio_pago->dm_requiere_banco == 0 && $medio_pago->dm_requiere_documento == 0){
	$dq_monto_pagar = floatval($_POST['dq_monto_pagar']);
	if(!$dq_monto_pagar){
		$error_man->showWarning('El monto a pagar es inválido, compruebelo y vuelva a intentarlo');
		exit;
	}
	
	if($dq_monto < $dq_monto_pagar){
		$error_man->showWarning('El monto a pagar debe ser inferior o igual al monto adeudado.');
		exit;
	}
	
}else{
	
	$dq_monto_pagar = 0;
	
	foreach($_POST['dq_monto'] as $d){
		$dq = floatval($d);
		if(!$dq){
			$d = htmlentities($d);
			$error_man->showWarning("El monto {$d} es inválido y no puede usarse como monto.");
			exit;
		}
		
		$dq_monto_pagar += $dq;
	}
	
	if(!$dq_monto_pagar){
		$error_man->showWarning('No se encontraron detalles de pago, intentelo denuevo.');
		exit;
	}
	
	if($dq_monto < $dq_monto_pagar){
		$error_man->showWarning('El monto a pagar debe ser inferior o igual al monto adeudado.');
		exit;
	}
	
}

//Obtener datos requeridos del proveedor
$proveedor = $db->getRowById('tb_proveedor',$orden_compra->dc_proveedor,'dc_proveedor');

//Comprobar proveedor con pago anticipado
if($proveedor->dc_cuenta_anticipo == 0){
	$error_man->showWarning('El proveedor no es válido para realizar pagos anticipados, no tiene asociada una cuenta contable para esto.');
	exit;
}

//Iniciar transacción
$db->start_transaction();

require_once('../../proc/ventas_functions.php');
$insert_comprobante_pago = $db->prepare($db->insert('tb_comprobante_pago_anticipado',array(
	'dq_comprobante' => '?',
	'dc_proveedor' => '?',
	'dc_medio_pago' => '?',
	'dg_comentario' => '?',
	'df_emision' => $db->getNow(),
	'df_pago' => '?',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
)));
$dq_comprobante_pago = doc_GetNextNumber('tb_comprobante_pago_anticipado','dq_comprobante');

$insert_comprobante_pago->bindValue(1,$dq_comprobante_pago,PDO::PARAM_INT);
$insert_comprobante_pago->bindValue(2,$orden_compra->dc_proveedor,PDO::PARAM_INT);
$insert_comprobante_pago->bindValue(3,$dc_medio_pago,PDO::PARAM_INT);
$insert_comprobante_pago->bindValue(4,$_POST['dg_comentario'],PDO::PARAM_STR);
$insert_comprobante_pago->bindValue(5,$db->sqlDate2($_POST['df_propuesta']),PDO::PARAM_STR);

$db->stExec($insert_comprobante_pago);

$dc_comprobante_pago = $db->lastInsertId();

/**
*	Insertar detalle del comprobante indicando la orden de compra que será pagada
*/
$ins_det_propuesta = $db->prepare($db->insert('tb_detalle_pago_anticipado',array(
	'dc_comprobante' => '?',
	'dc_orden_compra' => '?',
	'dq_monto' => '?'
)));
$ins_det_propuesta->bindValue(1,$dc_comprobante_pago,PDO::PARAM_INT);
$ins_det_propuesta->bindValue(2,$dc_orden_compra,PDO::PARAM_INT);
$ins_det_propuesta->bindValue(3,$dq_monto_pagar,PDO::PARAM_INT);
$db->stExec($ins_det_propuesta);

if($medio_pago->dm_requiere_banco == 1 || $medio_pago->dm_requiere_documento == 1){
	$ins_det_pago = $db->prepare($db->insert('tb_documento_pago_anticipado',array(
		'dc_comprobante' => $dc_comprobante_pago,
		'dc_banco' => '?',
		'dg_documento' => '?',
		'df_vencimiento' => '?',
		'dq_monto' => '?',
		'df_emision' => '?'
	)));
	
	//Asignar sin banco en caso de no ser requerido por el medio de pago
	if($medio_pago->dm_requiere_banco == 1)
		$ins_det_pago->bindParam(1,$dc_banco,PDO::PARAM_INT);
	else
		$ins_det_pago->bindValue(1,0,PDO::PARAM_INT);
		
	//Insertar sin número de documento en caso de no requerirlo
	if($medio_pago->dm_requiere_documento){
		$ins_det_pago->bindParam(2,$dg_documento,PDO::PARAM_STR);
		$ins_det_pago->bindParam(3,$df_vencimiento,PDO::PARAM_STR);
	}else{
		$ins_det_pago->bindValue(2,NULL,PDO::PARAM_NULL);
		$ins_det_pago->bindValue(3,NULL,PDO::PARAM_NULL);
	}
	
	$ins_det_pago->bindParam(4,$dq_monto,PDO::PARAM_STR);
	$ins_det_pago->bindParam(5,$df_emision,PDO::PARAM_STR);
	
	foreach($_POST['dq_monto'] as $i => $d){
		if($medio_pago->dm_requiere_banco == 1)
			$dc_banco = $_POST['dc_banco'][$i];
		if($medio_pago->dm_requiere_documento == 1){
			$dg_documento = $_POST['dg_documento'][$i];
			$df_vencimiento = $db->sqlDate2($_POST['df_vencimiento'][$i]);
		}
		$dq_monto = floatval($_POST['dq_monto'][$i]);
		$df_emision = $db->sqlDate2($_POST['df_emision'][$i]);
		
		$db->stExec($ins_det_pago);
	}
	
}

/**
*	Insertar comprobante contable
*/
require_once('../../../contabilidad/stuff.class.php');

	//Obtener el número del siguiente comprobante contable
	$dq_comprobante_contable = ContabilidadStuff::getCorrelativoComprobante();

	//Insertar cabecera del comprobante contable
	$insert_comprobante_contable = $db->prepare($db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => '?',
		'dc_tipo_movimiento' => '?',
		'df_fecha_contable' => '?',
		'dc_tipo_cambio' => '?',
		'dq_cambio' => 1,
		'dg_glosa' => '?',
		'dq_saldo' => '?',
		'dc_mes_contable' => 'MONTH(?)',
		'dc_anho_contable' => 'YEAR(?)',
		'df_fecha_emision' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario
	)));
	$insert_comprobante_contable->bindValue(1,$dq_comprobante_contable,PDO::PARAM_INT);
	$insert_comprobante_contable->bindValue(2,$medio_pago->dc_tipo_movimiento,PDO::PARAM_INT);
	$insert_comprobante_contable->bindValue(3,$db->sqlDate2($_POST['df_propuesta']),PDO::PARAM_STR);
	$insert_comprobante_contable->bindValue(4,0,PDO::PARAM_INT);
	$insert_comprobante_contable->bindValue(5,"Comprobante Pago: {$dq_comprobante_pago}\nProveedor: {$proveedor->dg_razon}\nmedio de pago: {$medio_pago->dg_medio_pago}\n\n{$_POST['dg_comentario']}",PDO::PARAM_STR);
	$insert_comprobante_contable->bindValue(6,$dq_monto_pagar,PDO::PARAM_STR);
    $insert_comprobante_contable->bindValue(7,$db->sqlDate2($_POST['df_propuesta']),PDO::PARAM_STR);
    $insert_comprobante_contable->bindValue(8,$db->sqlDate2($_POST['df_propuesta']),PDO::PARAM_STR);
	
	$db->stExec($insert_comprobante_contable);
	
	$dc_comprobante_contable = $db->lastInsertId();
	
	/**
	*	Insertar detalles de comprobante contable
	*/
	$insert_detalle_comprobante = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => $dc_comprobante_contable,
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?'
	)));
	$insert_detalle_comprobante->bindParam(1,$dc_cuenta_contable,PDO::PARAM_INT);
	$insert_detalle_comprobante->bindParam(2,$dq_debe,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(3,$dq_haber,PDO::PARAM_STR);
	$insert_detalle_comprobante->bindParam(4,$dg_glosa,PDO::PARAM_STR);
    
    $insert_detalle_comprobante_analitico = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico',array(
        'dc_cuenta_contable' => '?',
        'dq_debe' => '?',
        'dq_haber' => '?',
        'dg_glosa' => '?',
        'dc_detalle_financiero' => '?',
        'dc_nota_venta' => '?',
        'dc_orden_servicio' => '?',
        'dc_proveedor' => '?',
        'dc_tipo_proveedor' => '?',
        'dc_orden_compra' => '?',
        'dc_banco' => '?',
        'dc_medio_pago_proveedor' => '?',
        'dc_mercado_proveedor' => '?',
        'dg_cheque' => '?',
        'df_cheque' => '?'
    )));
    $insert_detalle_comprobante_analitico->bindParam(1, $dc_cuenta_contable, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindParam(2, $dq_debe_analitico, PDO::PARAM_STR);
    $insert_detalle_comprobante_analitico->bindParam(3, $dq_haber_analitico, PDO::PARAM_STR);
    $insert_detalle_comprobante_analitico->bindParam(4, $dg_glosa_analitico, PDO::PARAM_STR);
    $insert_detalle_comprobante_analitico->bindParam(5, $dc_detalle_financiero, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(6, $orden_compra->dc_nota_venta, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(7, $orden_compra->dc_orden_servicio, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(8, $proveedor->dc_proveedor, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(9, $proveedor->dc_tipo_proveedor, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindParam(10, $orden_compra->dc_orden_compra, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindParam(11, $dc_banco, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(12, $dc_medio_pago, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindValue(13, $proveedor->dc_mercado, PDO::PARAM_INT);
    $insert_detalle_comprobante_analitico->bindParam(14, $dg_cheque, PDO::PARAM_STR);
    $insert_detalle_comprobante_analitico->bindParam(15, $df_fecha_cheque, PDO::PARAM_STR);
	
	//Detalle al DEBE, monto de anticipo
	$dc_cuenta_contable = $proveedor->dc_cuenta_anticipo;
	$dq_debe = round($dq_monto_pagar,$empresa_conf['dn_decimales_local']);
	$dq_haber = 0;
	$dg_glosa = "Anticipo Orden de compra: {$orden_compra->dq_orden_compra} Proveedor: {$proveedor->dg_razon}";
	
	$db->stExec($insert_detalle_comprobante);
    $dc_detalle_financiero = $db->lastInsertId();
    
    $dq_debe_analitico = $dq_debe;
    $dq_haber_analitico = $dq_haber;
    $dg_glosa_analitico = $dg_glosa;
	$dg_cheque = NULL;
	$df_fecha_cheque = NULL;
	$dc_banco = NULL;
    
    $db->stExec($insert_detalle_comprobante_analitico);
	
    $dq_haber = $dq_debe;
	$dq_debe = 0;
    
    $dg_glosa = "Pago Anticipado Medio de Pago: {$medio_pago->dg_medio_pago}";
    
    $dq_debe_analitico = $dq_debe;
	
	//Detalle al HABER, dependiento el medio de pago
	//Pago efectivo
	if($medio_pago->dm_requiere_banco == 0 && $medio_pago->dm_requiere_documento == 0){
		$dc_cuenta_contable = intval($_POST['dc_cuenta_contable']);
		$dq_haber_analitico = round($dq_monto_pagar,$empresa_conf['dn_decimales_local']);
		$dg_glosa_analitico = "{$medio_pago->dg_medio_pago} Proveedor:{$proveedor->dg_razon}";
        $dg_cheque = NULL;
        $df_fecha_cheque = NULL;
        $dc_banco = NULL;
        
        $db->stExec($insert_detalle_comprobante);
        $dc_detalle_financiero = $db->lastInsertId();
		
		$db->stExec($insert_detalle_comprobante_analitico);
		
	}
	//Pago transferencia
	else if($medio_pago->dm_requiere_banco == 1 && $medio_pago->dm_requiere_documento == 0){
		
		foreach($_POST['dq_monto'] as $i => $d){
			
			$dc_banco = intval($_POST['dc_banco'][$i]);
            $dg_cheque = NULL;
            $df_fecha_cheque = NULL;
			
			if(!$dc_banco){
				$error_man->showWarning('Ocurrió un error inesperado intentando obtener la información del banco, compruebe la información de entrada y vuelva a intentarlo.');
				$db->rollBack();
				exit;
			}
			
			$cc_banco = $db->getRowById('tb_banco',$dc_banco,'dc_banco');
			
			$dc_cuenta_contable = $cc_banco->dc_cuenta_contable;
			$dq_haber_analitico = round(floatval($d),$empresa_conf['dn_decimales_local']);
			$dg_glosa_analitico = "{$medio_pago->dg_medio_pago} banco: {$cc_banco->dg_banco} fecha: {$_POST['df_emision'][$i]} Proveedor:{$proveedor->dg_razon}";
            
            $db->stExec($insert_detalle_comprobante);
            $dc_detalle_financiero = $db->lastInsertId();
			
			$db->stExec($insert_detalle_comprobante_analitico);
		}
		
	}
	//Pago documentado
	else if($medio_pago->dm_requiere_banco == 1 && $medio_pago->dm_requiere_documento == 1 && $medio_pago->dm_asiento_intermedio == 1){
        $dc_cuenta_contable = $medio_pago->dc_cuenta_contable_intermedia;
        $db->stExec($insert_detalle_comprobante);
        $dc_detalle_financiero = $db->lastInsertId();
		foreach($_POST['dq_monto'] as $i => $d){
			
			$dc_banco = intval($_POST['dc_banco'][$i]);
			
			if(!$dc_banco){
				$error_man->showWarning('Ocurrió un error inesperado intentando obtener la información del banco, compruebe la información de entrada y vuelva a intentarlo.');
				$db->rollBack();
				exit;
			}
			
			$cc_banco = $db->getRowById('tb_banco',$dc_banco,'dc_banco');
			
			$dq_haber_analitico = round(floatval($d),$empresa_conf['dn_decimales_local']);
			$dg_glosa = "{$medio_pago->dg_medio_pago} banco: {$cc_banco->dg_banco} fecha: {$_POST['df_emision'][$i]} Proveedor:{$proveedor->dg_razon}";
			$dg_cheque = $_POST['dg_documento'][$i];
			$df_fecha_cheque = $db->sqlDate2($_POST['df_vencimiento'][$i]);
            
			
			$db->stExec($insert_detalle_comprobante_analitico);
		}
	}
	
/**
*	Actualizar monto pagado en la orden de compra
*/
$update_orden_compra = $db->prepare($db->update('tb_orden_compra',array(
	'dq_monto_pagado' => 'dq_monto_pagado+?'
),'dc_orden_compra = ?'));
$update_orden_compra->bindValue(1,$dq_monto_pagar,PDO::PARAM_STR);
$update_orden_compra->bindValue(2,$dc_orden_compra,PDO::PARAM_INT);
$db->stExec($update_orden_compra);

$db->commit();

$error_man->showConfirm('Se ha realizado el pago anticipado correctamente, los documentos generados son los siguientes:');
?>
<div class="info">
	<h2>
		<small>Comprobante de pago</small><br />
		<strong><?php echo $dq_comprobante_pago ?></strong><br /><br />
		<small>Comprobante contable</small><br />
		<strong><?php echo $dq_comprobante_contable?></strong>
		<small>
			<a href="sites/contabilidad/ver_comprobante.php?id=<?php echo $dc_comprobante_contable ?>">IMPRIMIR COMPROBANTE CONTABLE</a>
		</small>
	</h2>
</div>
<script type="text/javascript">
	$('#res_list .confirm .oc_load').trigger('click');
</script>
