<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select("tb_orden_servicio",'dc_orden_servicio',"dq_orden_servicio={$_GET['number']} AND dc_empresa={$empresa}");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

$detalle = $db->select("(SELECT * FROM tb_orden_servicio_factura_detalle WHERE dc_orden_servicio = {$id['dc_orden_servicio']} AND dq_cantidad > dc_comprada) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto
LEFT JOIN (SELECT dc_producto,SUM(dq_stock-dq_stock_reservado-dq_stock_reservado_os) dq_stock_libre FROM tb_stock GROUP BY dc_producto) s ON s.dc_producto = d.dc_producto",
"p.dg_codigo,p.dg_producto dg_descripcion,d.dq_cantidad-d.dc_comprada dq_cantidad,d.dq_precio_compra,p.dc_producto,d.dc_detalle,s.dq_stock_libre");

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>