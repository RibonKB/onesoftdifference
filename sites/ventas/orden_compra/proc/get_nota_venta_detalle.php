<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select("(SELECT * FROM tb_nota_venta WHERE dc_empresa = {$empresa} AND dq_nota_venta={$_GET['number']} AND dm_validada = '1' AND dm_confirmada = '1' AND dm_nula='0') nv
JOIN tb_tipo_nota_venta tnv ON tnv.dc_tipo = nv.dc_tipo_nota_venta",
'nv.dc_nota_venta,nv.dc_tipo_cambio,nv.dq_cambio,tnv.dm_costeable');

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

if($id['dm_costeable'] == 1){
	$detalle = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$id['dc_nota_venta']} AND dm_tipo=1 AND dq_cantidad > dc_comprada) d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	LEFT JOIN
	(SELECT dc_producto,SUM(dq_stock-dq_stock_reservado-dq_stock_reservado_os) dq_stock_libre
	FROM tb_stock s
	JOIN tb_bodega b ON b.dc_bodega = s.dc_bodega AND b.dm_tipo = 0
	GROUP BY dc_producto)
		st ON st.dc_producto = d.dc_producto",
	"p.dg_codigo,d.dg_descripcion,d.dq_cantidad-d.dc_comprada dq_cantidad,d.dq_precio_compra,d.dc_proveedor,d.df_fecha_arribo,p.dc_producto,d.dc_nota_venta_detalle,st.dq_stock_libre,d.dc_ceco");

}else{
	$detalle = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$id['dc_nota_venta']} AND dm_tipo=0 AND dq_cantidad > dc_comprada) d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	LEFT JOIN (SELECT dc_producto,SUM(dq_stock-dq_stock_reservado-dq_stock_reservado_os) dq_stock_libre
	FROM tb_stock s
	JOIN tb_bodega b ON b.dc_bodega = s.dc_bodega AND b.dm_tipo = 0
	GROUP BY dc_producto) s ON s.dc_producto = d.dc_producto",
	"p.dg_codigo,d.dg_descripcion,d.dq_cantidad-d.dc_comprada dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,d.df_fecha_arribo,p.dc_producto,d.dc_nota_venta_detalle,s.dq_stock_libre,d.dc_ceco");

}

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>
