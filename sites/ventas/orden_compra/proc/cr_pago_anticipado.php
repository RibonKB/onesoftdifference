<?php
define("MAIN",1);
require_once("../../../../inc/global.php");

$dc_orden_compra = intval($_POST['dc_orden_compra']);
$dc_medio_pago = intval($_POST['dc_medio_pago']);

if(!$dc_orden_compra || !$dc_medio_pago){
	$error_man->showWarning('Los datos de entrada son inválidos, compruebe la información y vuelva a intentarlo.');
	exit;
}

$medio_pago = $db->select('tb_medio_pago_proveedor',
'dc_tipo_movimiento, dm_requiere_banco, dm_asiento_intermedio, dm_requiere_documento, dc_banco_default',
"dc_empresa = {$empresa} AND dc_medio_pago = {$dc_medio_pago}");
$medio_pago = array_shift($medio_pago);

if($medio_pago === NULL){
	$error_man->showWarning('No se encuentra el medio de pago ingresado, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

$orden_compra = $db->select('tb_orden_compra oc
JOIN tb_proveedor pr ON pr.dc_proveedor = oc.dc_proveedor',
'oc.dq_orden_compra,(oc.dq_neto+oc.dq_iva-oc.dq_monto_pagado) dq_monto_pagar, pr.dg_razon',
"oc.dc_orden_compra = {$dc_orden_compra} AND oc.dc_empresa = {$empresa}");
$orden_compra = array_shift($orden_compra);

if($orden_compra === NULL){
	$error_man->showWarning('No se encuentra la orden de compra asociada, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}


require_once("../../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Anticipo de pago Orden de compra
</div>
<div class="panes">
	<?php
		$form->Start('sites/ventas/orden_compra/proc/crear_pago_anticipado.php','cr_pago');
			$form->Header('<b>Indique los datos para el anticipo de pago por orden de compra</b><br />
			Los datos marcados con [*] son obligatorios');
			$form->Section();
				
				//En caso de que el pago sea en efectivo se muestra la cuenta contable en que se cargará el pago
				if($medio_pago['dm_requiere_banco'] == 0 && $medio_pago['dm_requiere_documento'] == 0):

						$form->Text('Monto a pagar','dq_monto_pagar',1);
					
						$form->Listado('Cuenta Destino','dc_cuenta_contable','tb_cuenta_contable cc
						JOIN tb_cuentas_tipo_movimiento t ON t.dc_cuenta_contable = cc.dc_cuenta_contable AND t.dc_tipo_movimiento = '.$medio_pago['dc_tipo_movimiento']
						,array('cc.dc_cuenta_contable','cc.dg_cuenta_contable'),1);

				endif;
				
				$form->Textarea('Comentario','dg_comentario',1);
			$form->EndSection();
			$form->Section();
				$form->Date('Fecha de pago','df_propuesta',1,0);
				?>
				<fieldset>
					<div class="info">
						Datos Orden de compra
					</div>
					<div>
						Número: <b><?php echo $orden_compra['dq_orden_compra'] ?></b><br />
						Proveedor:<br /><b><?php echo $orden_compra['dg_razon'] ?></b><br />
						Monto en Deuda: <b><?php echo moneda_local($orden_compra['dq_monto_pagar']) ?></b>
					</div>
				</fieldset>
				<?php
			$form->EndSection();
			//En caso de que admita banco o documento el medio, el pago ódrá ser realizado en varios detalles.
			if($medio_pago['dm_requiere_banco'] == 1 || $medio_pago['dm_requiere_documento'] == 1):
			
				$bancos = $db->select('tb_banco b
				JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = b.dc_cuenta_contable
				JOIN tb_cuentas_tipo_movimiento t ON t.dc_cuenta_contable = c.dc_cuenta_contable',
				'b.dc_banco, b.dg_banco',
				"b.dc_empresa = {$empresa} AND t.dc_tipo_movimiento = {$medio_pago['dc_tipo_movimiento']}");
			
				$form->Group('id="payDetail"');
					$form->Button('Agregar detalle de pago','id="addPayDetail"','addbtn');
				$form->Group();
			endif;
		$form->Hidden('dm_requiere_banco',$medio_pago['dm_requiere_banco']);
		$form->Hidden('dm_requiere_documento',$medio_pago['dm_requiere_documento']);
		$form->Hidden('dm_asiento_intermedio',$medio_pago['dm_asiento_intermedio']);
		$form->Hidden('dc_tipo_movimiento',$medio_pago['dc_tipo_movimiento']);
		$form->Hidden('dc_orden_compra',$dc_orden_compra);
		$form->Hidden('dc_medio_pago',$dc_medio_pago);
		$form->End('Realizar pago','addbtn');
	?>
</div>
<?php /* Plantilla de detalles */if($medio_pago['dm_requiere_banco'] == 1 || $medio_pago['dm_requiere_documento'] == 1): ?>
	<div id="detailContainer" class="hidden">
	<fieldset class="payDetail">
		<div class="left">
			<label>
				Monto[*]<br />
				<input type="text" class="inputtext" name="dq_monto[]" required="required" size="41" maxlength="255" />
			</label><br />
			<label>
				Fecha Emisión[*]<br />
				<input type="text" class="date inputtext" name="df_emision[]" required="required" size="38" maxlength="255" />
			</label><br />
		</div>
		<div class="left">
			<?php /*Requiere documento*/ if($medio_pago['dm_requiere_documento'] == 1): ?>
			<label>
				Número de Documento[*]<br />
				<input type="text" class="inputtext" name="dg_documento[]" required="required" size="41" maxlength="255" />
			</label><br />
			<label>
				Fecha Vencimiento[*]<br />
				<input type="text" class="date inputtext" name="df_vencimiento[]" required="required" size="38" maxlength="255" />
			</label><br />
			<?php endif; ?>
			<?php /*Requiere Banco*/ if($medio_pago['dm_requiere_banco'] == 1): ?>
			<label>
				Banco[*]<br />
				<select class="inputtext dc_banco" name="dc_banco[]" required="required">
				<option value="0"></option>
				<?php foreach($bancos as $b): ?>
					<option value="<?php echo $b['dc_banco'] ?>"><?php echo $b['dg_banco'] ?></option>
				<?php endforeach; ?>
				</select>
			</label>
			<?php endif; ?>
		</div>
	</fieldset>
	</div>
	<script type="text/javascript" src="jscripts/sites/finanzas/pagos/cr_pago_individual.js"></script>
	<script type="text/javascript">
		js_data.init(<?php echo $medio_pago['dc_banco_default'] ?>);
	</script>
<?php endif; /* fin plantilla de detalles */ ?>
<script type="text/javascript">
	pymerp.init($('#cr_pago'));
</script>