<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->escape($_POST['id']);

$producto = $db->select('tb_producto','dg_producto,dg_codigo',"dc_producto={$_POST['id']}");

if(!count($producto)){
	$error_man->showWarning("El producto especificado no existe");
	exit;
}
$producto = $producto[0];

$stock = $db->select("(SELECT * FROM tb_stock WHERE dc_producto = {$_POST['id']}) s
JOIN tb_bodega b ON b.dc_bodega = s.dc_bodega AND dm_tipo = 0",
's.dq_stock-s.dq_stock_reservado-s.dq_stock_reservado_os dq_stock_libre,b.dc_bodega,b.dg_bodega',
"(s.dq_stock-s.dq_stock_reservado-s.dq_stock_reservado_os) > 0");

if(!count($stock)){
	$error_man->showWarning("No hay stock libre disponible para el producto <b>{$producto['dg_producto']}</b>");
	exit;
}

$bodegas = $db->select('tb_bodega','dc_bodega,dg_bodega',"dm_tipo=0 AND dm_activo = 1 AND dc_empresa={$empresa}");
?>

<div class="secc_bar">
	Asignación de stock libre a la compra de<br /> <b><?php echo($producto['dg_codigo']); ?></b> - <?php echo($producto['dg_producto']); ?>
</div>
<div class="panes">

<table class="tab" width="70%" style="margin:auto;">
	<caption>Stock libre distribuida en las bodegas</caption>
	<thead><tr>
		<th>Bodega</th>
		<th width="100">Stock Libre</th>
		<th width="100">Stock a asignar</th>
	</tr></thead>
	<tbody>
		<?php foreach($stock as $s): ?>
		<tr>
			<td><b><?php echo($s['dg_bodega']); ?></b></td>
			<td align="right"><?php echo($s['dq_stock_libre']); ?></td>
			<td align="right">
				<input type="text" class="fs_stock inputtext" size="15" value="0" style="text-align:right;" />
				<input type="hidden" class="fs_max_stock" value="<?php echo($s['dq_stock_libre']); ?>" />
				<input type="hidden" class="fs_id_bodega" value="<?php echo($s['dc_bodega']); ?>" />
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
<br />
<div class="center">
	<label>Bodega de entrada para reservar<br />
	<select class="inputtext" id="fs_bodega_entrada">
	<?php foreach($bodegas as $b): ?>
		<option value="<?php echo($b['dc_bodega']); ?>"><?php echo($b['dg_bodega']); ?></option>
	<?php endforeach; ?>
	</select>
	</label>
	<br /><br />
	<button class="button" id="fs_asignar_stock">Asignar stock</button>
	
</div>

</div>
<script type="text/javascript" src="jscripts/orden_compra/set_free_stock_to_document.js?v1_0_6"></script>
<script type="text/javascript">
	js_subdata.id_producto = <?php echo($_POST['id']) ?>;
	js_subdata.init();
</script>