<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
require_once("../../proc/ventas_functions_old.php");

$db->start_transaction();

$oc_number = doc_GetNextNumber('tb_orden_compra','dq_orden_compra',$empresa_conf['dc_correlativo_orden_compra'],'df_fecha_emision');

$orden_compra = $db->insert('tb_orden_compra',array(
	"dq_orden_compra" => $oc_number,
	"dc_nota_venta" => $_POST['oc_id_nota_venta'],
	"dc_orden_servicio" => $_POST['oc_id_orden_servicio'],
	"dc_responsable" => $_POST["oc_executive"],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario,
	"dc_tipo_cambio" => $_POST["prod_tipo_cambio"],
	"dq_cambio" => $_POST["oc_cambio"],
	"dc_proveedor" => $_POST["prov_id"],
	"df_fecha_emision" => 'NOW()',
	"dq_iva" => $_POST["cot_iva"],
	"dq_neto" => $_POST['cot_neto'],
	"dg_observacion" => $_POST['oc_observacion']
));

$any_reservacion = isset($_POST['fs_bodega_entrada']);

if($any_reservacion){
	require_once("../../proc/ventas_functions_old.php");
	$tipo_movimiento = $db->select('tb_configuracion_logistica','dc_tipo_movimiento_traspaso_compra',"dc_empresa={$empresa}");
	if(!count($tipo_movimiento)){
		$error_man->showWarning("No es posible realizar asignación de stock libre debido a que no se encuentra configurado, consulte con un administrador.");
		exit;
	}
	$tipo_movimiento = $tipo_movimiento[0]['dc_tipo_movimiento_traspaso_compra'];
	if($tipo_movimiento == 0){
		$error_man->showWarning("No es posible realizar asignación de stock libre debido a que no se encuentra configurado, consulte con un administrador.");
		exit;
	}
}

$cantidad_comprada = 0;
$cantidad_recepcionada = 0;

foreach($_POST['prod'] as $i => $v){

	$_POST['cant'][$i] = str_replace(',','',$_POST['cant'][$i]);
	$_POST['costo'][$i] = str_replace(',','',$_POST['costo'][$i]);

	$detail_data = array(
		"dc_orden_compra" => $orden_compra,
		"dc_producto" => $_POST['prod'][$i],
		"dg_descripcion" => $_POST['desc'][$i],
		"dq_cantidad" => $_POST['cant'][$i],
		"dq_precio" => $_POST['costo'][$i]*$_POST['oc_cambio'],
		"dc_proveedor" => $_POST['proveedor'][$i],
		"dc_ceco" => $_POST['ceco'][$i]
	);
	if(!empty($_POST['df_fecha_arribo'][$i])){
		$detail_data["df_fecha_arribo"] = $db->sqlDate($_POST['df_fecha_arribo'][$i]);
	}

	$producto = $db->select('tb_producto','dq_precio_compra',"dc_producto = {$_POST['prod'][$i]}");
	$producto = array_shift($producto);

	$set_free = $any_reservacion && isset($_POST['fs_bodega_entrada'][$v]);
	if($set_free){
		$res_stock = explode('|',$_POST['fs_reservado'][$v]);
		$total = 0;
		foreach($res_stock as $p){
			$p = explode(',',$p);
			$can = bodega_RebajarStockLibre($v,$p[0],$p[1]);
			if(!$can){
				$error_man->showWarning("Intentó asignar stock libre al documento relacionado pero este fue utilizado.");
				exit();
			}

			//Registrar movimiento de salida de bodega stock libre
			$dc_movimiento = $db->insert('tb_movimiento_bodega',array(
				'dc_proveedor' => $_POST['proveedor'][$i],
				'dc_tipo_movimiento' => $tipo_movimiento,
				'dc_bodega_salida' => $p[1],
				'dq_monto' => $producto['dq_precio_compra'],
				'dq_cambio' => $_POST['oc_cambio'],
				'dc_orden_compra' => $orden_compra,
				'dc_nota_venta' => $_POST['oc_id_nota_venta'],
				'dc_orden_servicio' => $_POST['oc_id_orden_servicio'],
				'dc_ceco' => $_POST['ceco'][$i],
				'dc_producto' => $_POST['prod'][$i],
				'dq_cantidad' => -1*$p[0],
				'dc_empresa' => $empresa,
				'df_creacion' => 'NOW()',
				'dc_usuario_creacion' => $idUsuario
			));

			//Registrar movimiento de entrada bodega reservada
			$dc_movimiento_relacionado = $db->insert('tb_movimiento_bodega',array(
				'dc_proveedor' => $_POST['proveedor'][$i],
				'dc_tipo_movimiento' => $tipo_movimiento,
				'dc_bodega_entrada' => $_POST['fs_bodega_entrada'][$v],
				'dq_monto' => $producto['dq_precio_compra'],
				'dq_cambio' => $_POST['oc_cambio'],
				'dc_orden_compra' => $orden_compra,
				'dc_nota_venta' => $_POST['oc_id_nota_venta'],
				'dc_orden_servicio' => $_POST['oc_id_orden_servicio'],
				'dc_ceco' => $_POST['ceco'][$i],
				'dc_producto' => $_POST['prod'][$i],
				'dq_cantidad' => $p[0],
				'dc_empresa' => $empresa,
				'df_creacion' => 'NOW()',
				'dc_usuario_creacion' => $idUsuario,
				'dc_movimiento_relacionado' => $dc_movimiento
			));

			$db->update('tb_movimiento_bodega',array(
				'dc_movimiento_relacionado' => $dc_movimiento_relacionado
			),"dc_movimiento = {$dc_movimiento}");

			$total += $p[0];
		}

	}

	if(isset($_POST['id_detail'][$i]) && $_POST['oc_id_nota_venta'] != 0){
		$detail_data['dc_detalle_nota_venta'] = $_POST['id_detail'][$i];

		$detail_nv_data = array(
			"dc_comprada" => "dc_comprada+{$_POST['cant'][$i]}"
		);

		if($set_free){
			$db->update('tb_stock',array(
				'dq_stock' => "dq_stock+{$total}",
				'dq_stock_reservado' => "dq_stock_reservado+{$total}"
			),"dc_bodega={$_POST['fs_bodega_entrada'][$v]} AND dc_producto={$v}");

			if($db->affected_rows() == 0){

				$db->insert('tb_stock',array(
					'dc_producto' => $v,
					'dc_bodega' => $_POST['fs_bodega_entrada'][$v],
					'dq_stock' => $total,
					'dq_stock_reservado' => $total
				));

			}

			/*$db->insert('tb_movimiento_bodega',array(
				'dc_proveedor' => $_POST['proveedor'][$i],
				'dc_tipo_movimiento' => $tipo_movimiento,
				'dc_bodega_entrada' => $_POST['fs_bodega_entrada'][$v],
				'dq_monto' => $producto['dq_precio_compra'],
				'dq_cambio' => $_POST['oc_cambio'],
				'dc_orden_compra' => $orden_compra,
				'dc_orden_servicio' => $_POST['oc_id_orden_servicio'],
				'dc_ceco' => $_POST['ceco'][$i],
				'dc_producto' => $_POST['prod'][$i],
				'dq_cantidad' => $total,
				'dc_empresa' => $empresa,
				'df_creacion' => 'NOW()',
				'dc_usuario_creacion' => $idUsuario
			));*/


			$detail_nv_data['dq_pmp_compra'] = $producto['dq_precio_compra'];
			$detail_data['dc_recepcionada'] = $total;
			$detail_nv_data['dc_recepcionada'] = "dc_recepcionada+{$total}";
			$cantidad_recepcionada += $total;
		}

		$db->update('tb_nota_venta_detalle',$detail_nv_data,"dc_nota_venta_detalle = {$_POST['id_detail'][$i]}");

		$cantidad_comprada += $_POST['cant'][$i];
	}else if(isset($_POST['id_detail'][$i]) && $_POST['oc_id_orden_servicio'] != 0){
		$detail_data['dc_detalle_orden_servicio'] = $_POST['id_detail'][$i];

		$detail_os_data = array(
			"dc_comprada" => "dc_comprada+{$_POST['cant'][$i]}"
		);

		if($set_free){
			$db->update('tb_stock',array(
				'dq_stock' => "dq_stock+{$total}",
				'dq_stock_reservado_os' => "dq_stock_reservado_os+{$total}"
			),"dc_bodega={$_POST['fs_bodega_entrada'][$v]} AND dc_producto={$v}");

			if($db->affected_rows() == 0){

				$db->insert('tb_stock',array(
					'dc_producto' => $v,
					'dc_bodega' => $_POST['fs_bodega_entrada'][$v],
					'dq_stock' => $total,
					'dq_stock_reservado_os' => $total
				));

			}

			/*$db->insert('tb_movimiento_bodega',array(
				'dc_proveedor' => $_POST['proveedor'][$i],
				'dc_tipo_movimiento' => $tipo_movimiento,
				'dc_bodega_entrada' => $_POST['fs_bodega_entrada'][$v],
				'dc_producto' => $_POST['prod'][$i],
				'dq_cambio' => $_POST['oc_cambio'],
				'dc_orden_compra' => $orden_compra,
				'dc_orden_servicio' => $_POST['oc_id_orden_servicio'],
				'dc_ceco' => $_POST['ceco'][$i],
				'dc_producto' => $_POST['prod'][$i],
				'dq_cantidad' => $total,
				'dc_empresa' => $empresa,
				'df_creacion' => 'NOW()',
				'dc_usuario_creacion' => $idUsuario
			));*/

			$detail_data['dc_recepcionada'] = $total;
			$detail_os_data['dc_recepcionada'] = "dc_recepcionada+{$total}";
		}

		$db->update('tb_orden_servicio_factura_detalle',$detail_os_data,"dc_detalle = {$_POST['id_detail'][$i]}");


	}

	if(isset($detail_data['dc_recepcionada'])){
		if($detail_data['dc_recepcionada'] >= $detail_data['dq_cantidad']){
			$incluir = false;
		}else{
			$incluir = true;
			$detail_data['dq_cantidad'] -= $detail_data['dc_recepcionada'];
			$detail_data['dc_recepcionada'] = 0;
		}
	}else{
		$incluir = true;
	}

	if($incluir){
		//var_dump($detail_data);
		$db->insert('tb_orden_compra_detalle',$detail_data);
	}


}


if($cantidad_comprada && $_POST['oc_id_nota_venta'] != 0){
	$db->update('tb_nota_venta',array(
		"dc_comprada" => "dc_comprada+{$cantidad_comprada}",
		"dc_recepcionada" => "dc_recepcionada+{$cantidad_recepcionada}"
	),"dc_nota_venta={$_POST['oc_id_nota_venta']}");
}

$db->commit();


$error_man->showConfirm("Se ha generado la orden de compra");
echo("<div class='title'>El número de orden de compra es <h1 style='margin:0;color:#000;'>{$oc_number}</h1></div>");

$error_man->showConfirm("Fueron agregados ".count($_POST['prod'])." elementos al detalle correctamente.<br />
<a href=\"#\" onclick=\"window.open('sites/ventas/orden_compra/ver_orden_compra.php?id={$orden_compra}','orden_compra','width=800,height=600')\"> Haga clic aquí para ver la orden de compra</a>");

?>
<script type="text/javascript">
$('#cr_orden_compra').prev('input').remove();
$('#cr_orden_compra').remove();
</script>
