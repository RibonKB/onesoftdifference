<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$nota_venta = array();
$det_id = array();
$det_cant = array();
foreach($_POST['oc_detail'] as $d){
	$aux = explode('|',$d);
	$nota_venta[$aux[0]][$aux[3]][] = array(
		'detalle' => $aux[1],
		'cantidad' => $aux[2]
	);
	$tipo_cambio[$aux[0]] = array($aux[4],$aux[5]);
	$det_id[] = $aux[1];
	$det_cant[] = $aux[2];
}
$det_id = implode(',',$det_id);

if(count($nota_venta) == 1){
	foreach($nota_venta as $i => $v){
		if(count($v) == 1){
			$nv_data = $db->select('tb_nota_venta','dq_nota_venta',"dc_nota_venta={$i}");
			$nv_data = $nv_data[0];
			$det_cant = implode(',',$det_cant);
			foreach($v as $ii => $vv){
				echo("<script type='text/javascript'>"
					."loadpage('sites/ventas/orden_compra/proc/cr_orden_compra.php','prov_id={$ii}&nv_numero={$nv_data['dq_nota_venta']}&dets={$det_id}&cants={$det_cant}');"
					."</script>");
				exit();
			}
		}
	}
}
unset($det_cant);

$aux = $db->select('tb_nota_venta_detalle',
'dc_nota_venta_detalle,dc_nota_venta,dc_producto,dg_descripcion,dq_cantidad,dq_precio_compra,dc_comprada',
"dc_nota_venta_detalle IN ({$det_id})");
unset($det_id);
$data_detalle = array();
foreach($aux as $v){
	$data_detalle[$v['dc_nota_venta_detalle']] = $v;
}
unset($aux);

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_orden_compra']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$db->start_transaction();

$last_date = $db->select('tb_orden_compra','MAX(df_fecha_emision) fecha',"dc_empresa={$empresa}");
if($last_date[0]['fecha'] != NULL){
	$last = $db->select('tb_orden_compra','dq_orden_compra',"dc_empresa={$empresa} AND df_fecha_emision = '{$last_date[0]['fecha']}'");
	if(substr($last[0]['dq_orden_compra'],0,$largo_prefix) == $cot_prefix || $empresa_conf['dc_correlativo_orden_compra'] == '3'){
		$cot_num = substr($last[0]['dq_orden_compra'],$largo_prefix)+1;
	}else{
		$cot_num = $empresa_conf['dg_correlativo_orden_compra'];
	}
}else{
	$cot_num = $empresa_conf['dg_correlativo_orden_compra'];
}

echo('<div id="secc_bar">Creación masiva de Ordenes de compra</div><div id="main_cont"><div class="panes">');
$insertadas = array();
foreach($nota_venta as $i => $v){
$total_comprado = 0;
	foreach($v as $ii => $vv){
		$orden_compra = $db->insert('tb_orden_compra',array(
			'dq_orden_compra' => $cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT),
			'dc_nota_venta' => $i,
			'dc_responsable' => $idUsuario,
			'dc_empresa' => $empresa,
			'dc_usuario_creacion' => $idUsuario,
			'dc_tipo_cambio' => $tipo_cambio[$i][0],
			'dq_cambio' => $tipo_cambio[$i][1],
			'dc_proveedor' => $ii,
			'df_fecha_emision' => 'NOW()',
			'dg_observacion' => 'Carga masiva orden de compra'
		));
		$insertadas[] = $orden_compra;
		$cot_num++;
		
		$neto = 0;
		foreach($vv as $det){
			$aux =& $data_detalle[$det['detalle']];
			if($det['cantidad'] > ($aux['dq_cantidad']-$aux['dc_comprada'])){
			  $error_man->showWarning("Los detalles de una de las notas de venta a sido alterado mientras se intentaba insertar los nuevos registros de las ordenes de compra");
			  $db->rollback();
			  exit();
			}
			$db->insert('tb_orden_compra_detalle',array(
				"dc_orden_compra" => $orden_compra,
				"dc_detalle_nota_venta" => $det['detalle'],
				"dc_producto" => $aux['dc_producto'],
				"dg_descripcion" => $aux['dg_descripcion'],
				"dq_cantidad" => $det['cantidad'],
				"dq_precio" => $aux['dq_precio_compra'],
				"dc_proveedor" => $ii
			));
			
			$neto += $det['cantidad']*$aux['dq_precio_compra'];
			$total_comprado += $det['cantidad'];
			
			$db->update('tb_nota_venta_detalle',array(
				"dc_comprada" => "dc_comprada+{$det['cantidad']}"
			),"dc_nota_venta_detalle={$det['detalle']}");
			
		}
		
		$iva = $neto*($empresa_conf['dq_iva']/100);
		$db->update('tb_orden_compra',array(
			"dq_neto" => $neto,
			"dq_iva" => $iva
		),"dc_orden_compra={$orden_compra}");
		
	}
	$db->update('tb_nota_venta',array(
		"dc_comprada" => "dc_comprada+{$total_comprado}"
	),"dc_nota_venta={$i}");
}
$db->commit();
$insertadas = implode(',',$insertadas);

$oc_data = $db->select("(SELECT * FROM tb_orden_compra WHERE dc_orden_compra IN ($insertadas)) oc
JOIN tb_proveedor p ON p.dc_proveedor = oc.dc_proveedor
JOIN tb_nota_venta nv ON nv.dc_nota_venta = oc.dc_nota_venta
JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = oc.dc_tipo_cambio",
'oc.dc_orden_compra,oc.dq_orden_compra,oc.dq_cambio,oc.dq_neto,p.dg_razon,nv.dq_nota_venta,tc.dg_tipo_cambio,tc.dn_cantidad_decimales');

$error_man->showConfirm("Se han ingresado correctamente las ordenes de compra");
echo('<table class="tab" width="100%">
<caption>Ordenes de compra generadas</caption>
<thead><tr>
	<th width="120">Orden de compra</th>
	<th width="120">Nota de venta</th>
	<th>Proveedor</th>
	<th width="120">Tipo de cambio</th>
	<th width="120">Neto</th>
	<th>Opciones</th>
</tr></thead><tbody>');
foreach($oc_data as $oc){
	$neto = moneda_local($oc['dq_neto']/$oc['dq_cambio'],$oc['dn_cantidad_decimales']);
	echo("<tr>
		<td><b>{$oc['dq_orden_compra']}</b></td>
		<td>{$oc['dq_nota_venta']}</td>
		<td>{$oc['dg_razon']}</td>
		<td>{$oc['dg_tipo_cambio']}</td>
		<td>{$neto}</td>
		<td>
			<button class='button' onclick='ver_impresion({oc['dc_orden_compra']})'>Versión de impresión</button>
		</td>
	</tr>");
}
echo('</tbody></table>');

?>
</div></div>