<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../../../inc/Factory.class.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_orden_compra = intval($_POST['id']);

//Obtener datos de cabecera de orden de compra
$data = $db->select("(SELECT * FROM tb_orden_compra WHERE dc_orden_compra = {$dc_orden_compra} AND dc_empresa={$empresa}) oc
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = oc.dc_nota_venta
JOIN tb_funcionario f ON f.dc_funcionario = oc.dc_responsable
JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = oc.dc_tipo_cambio
JOIN tb_proveedor p ON p.dc_proveedor = oc.dc_proveedor
	LEFT JOIN tb_contacto_proveedor c ON c.dc_contacto_proveedor = p.dc_contacto_default
	LEFT JOIN tb_comuna com ON com.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region r ON r.dc_region = com.dc_region
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
'oc.dq_orden_compra,oc.dq_cambio,oc.df_fecha_emision,oc.dq_neto,oc.dq_iva,oc.dg_observacion, oc.dq_monto_pagado, oc.dq_pago_usado, oc.dq_monto_liberado,
nv.dq_nota_venta,CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS dg_responsable, tc.dg_tipo_cambio, tc.dn_cantidad_decimales,
p.dg_razon,p.dg_rut,p.dg_giro,c.dg_contacto,c.dg_direccion,c.dg_telefono,com.dg_comuna,r.dg_region,cc.dg_cargo_contacto,oc.dm_nula');
$data = array_shift($data);

if($data == NULL){
	$error_man->showWarning("No se ha encontrado la orden de compra especificada");
	exit;
}

//Obtener detalles de la orden de compra
$detalle = $db->select('tb_orden_compra_detalle d
JOIN tb_producto p ON p.dc_producto = d.dc_producto',
'd.dg_descripcion,d.dq_cantidad,d.dq_precio,d.df_fecha_arribo,p.dg_codigo,p.dg_producto',
"d.dc_orden_compra = {$dc_orden_compra}");
foreach($detalle as &$d){
	$d['df_fecha_arribo'] = $db->dateLocalFormat($d['df_fecha_arribo']);
	$d['df_fecha_arribo'] = empty($d['df_fecha_arribo']) ? 'N/A':$d['df_fecha_arribo'];
}
unset($d);
//Obtener información de anulación de orden de compra
if($data['dm_nula'] == 1){
	$null_data = $db->select("(SELECT * FROM tb_orden_compra_anulada WHERE dc_orden_compra = {$dc_orden_compra}) n
							   JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo",'ma.dg_motivo,n.dg_comentario');
	$null_data = array_shift($null_data);
}

?>

<div class='title center'>Orden de Compra Nº <?php echo $data['dq_orden_compra'] ?></div>
<table class='tab' width='100%' style='text-align:left;'>
	<caption>Proveedor:<br /><strong>(<?php echo $data['dg_rut'] ?>) <?php echo $data['dg_razon'] ?></strong></caption>
	<tbody>
		<tr>
			<td width='160'>Fecha emision</td>
			<td><?php echo $db->dateLocalFormat($data['df_fecha_emision']) ?></td>
			<?php if($data['dm_nula'] == 1): ?>
				<td rowspan="7">
					<h3 class="alert">ORDEN DE COMPRA ANULADA</h3>
					<?php if($null_data != NULL): ?>
						<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
							<div class='info'>Info anulación</div>
							Motivo: <b><?php echo $null_data['dg_motivo'] ?></b><br />
							Comentario: <b><?php echo $null_data['dg_comentario'] ?></b>
						</div>
					<?php endif; ?>
				</td>
			<?php endif; ?>

			<?php if($data['dq_monto_pagado'] > 0): ?>
				<td rowspan="7">
					<h3 class="confirm">
					CON PAGO ANTICIPADO
						<?php if($data['dq_monto_pagado'] < $data['dq_neto']+$data['dq_iva']): ?>
							PARCIAL
						<?php else: ?>
							COMPLETO
						<?php endif; ?>
					</h3>
					<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
						<div class='info'>Info pago</div>
						Monto Pagado: <b><?php echo moneda_local($data['dq_monto_pagado']) ?></b><br />
						Monto usado en facturas: <b><?php echo moneda_local($data['dq_pago_usado']) ?></b><br />
						Monto liberado: <b><?php echo moneda_local($data['dq_monto_liberado']) ?></b>
					</div>
					<div class="center">
						<button id="show_pay_data" class="button" type="button">Ver información de pagos</button>
						<button id="liberar_saldo" class="button" type="button">Liberar Saldo Anticipado</button>
					</div>
				</td>
			<?php endif; ?>

		</tr><tr>
			<td>Contacto Proveedor</td>
			<td><?php echo $data['dg_contacto'] ?> <em><?php echo $data['dg_cargo_contacto'] ?></em> - fono: <b><?php echo $data['dg_telefono'] ?></b></td>
		</tr><tr>
			<td>Domicilio proveedor</td>
			<td><b><?php echo $data['dg_direccion'] ?></b> <?php echo $data['dg_comuna'] ?> <label><?php echo $data['dg_region'] ?></label></td>
		</tr><tr>
			<td>Tipo de cambio:</td>
			<td><b><?php echo $data['dg_tipo_cambio'] ?></b></td>
		</tr><tr>
			<td>Responsable:</td>
			<td><b><?php echo $data['dg_responsable'] ?></b></td>
		</tr><tr>
			<td>Nota de venta</td>
			<td><?php echo $data['dq_nota_venta'] ?></td>
		</tr><tr>
			<td>Observacion</td>
			<td><?php echo $data['dg_observacion'] ?></td>
		</tr>
	</tbody>
</table>

<table width='100%' class='tab'>
	<caption>Detalle</caption>
	<thead>
		<tr>
			<th width='60'>Código</th>
			<th width='90'>Fecha de Arribo / Renovación</th>
			<th>Producto</th>
			<th>Descripción</th>
			<th width='60'>Cantidad</th>
			<th width='100'>Precio</th>
			<th width='100'>Total</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($detalle as $d): ?>
		<tr>
			<td align='left'><?php echo $d['dg_codigo'] ?></td>
			<td align='left'><?php echo $d['df_fecha_arribo']; ?></td>
			<td align='left'><?php echo $d['dg_producto'] ?></td>
			<td align='left'><?php echo $d['dg_descripcion'] ?></td>
			<td><?php echo $d['dq_cantidad'] ?></td>
			<td align='right'><?php echo moneda_local($d['dq_precio']/$data['dq_cambio'],$data['dn_cantidad_decimales']) ?></td>
			<td align='right'><?php echo moneda_local($d['dq_precio']*$d['dq_cantidad']/$data['dq_cambio'],$data['dn_cantidad_decimales']) ?></td>
		</tr>
	<?php endforeach; ?>

	</tbody>
	<tfoot>
		<tr>
			<th colspan='6' align='right'>Total (<b><?php echo $data['dg_tipo_cambio'] ?></b>)</th>
			<th align='right'><?php echo moneda_local($data['dq_neto']/$data['dq_cambio'],$data['dn_cantidad_decimales']) ?></th>
		</tr>
		<tr>
			<th colspan='6' align='right'>Total Neto</th>
			<th align='right'><?php echo moneda_local($data['dq_neto'],$data['dn_cantidad_decimales']) ?></th>
		</tr>
		<tr>
			<th colspan='6' align='right'>IVA</th>
			<th align='right'><?php echo moneda_local($data['dq_iva'],$data['dn_cantidad_decimales']) ?></th>
		</tr>
		<tr>
			<th colspan='6' align='right'>Total a Pagar</th>
			<th align='right'><?php echo moneda_local($data['dq_neto']+$data['dq_iva'],$data['dn_cantidad_decimales']) ?></th>
		</tr>
	</tfoot>
</table>

<script type="text/javascript">
$('#print_version').unbind('click').click(function(){
	window.open("sites/ventas/orden_compra/ver_orden_compra.php?id=<?php echo $dc_orden_compra ?>&v=0",'print_orden_compra','width=800;height=600');
});
$('#edit_orden_compra').unbind('click').click(function(){
	pymerp.loadPage("sites/ventas/orden_compra/ed_orden_compra.php?id=<?php echo $dc_orden_compra ?>");
});
$('#null_orden_compra').unbind('click').click(function(){
	pymerp.loadOverlay("sites/ventas/orden_compra/null_orden_compra.php?id=<?php echo $dc_orden_compra ?>");
});
$('#pago_anticipado').unbind('click').click(function(){
	pymerp.loadOverlay("sites/ventas/orden_compra/cr_pago_anticipado.php?id=<?php echo $dc_orden_compra ?>");
});
$('#show_pay_data').click(function(){
	pymerp.loadOverlay('<?php echo Factory::buildUrl('GestionPagosAnticipados', 'ventas', 'index', 'orden_compra', array(
        'dc_orden_compra' => $dc_orden_compra
    )) ?>');
});
$('#liberar_saldo').unbind('click').click(function(){
	pymerp.loadOverlay("sites/ventas/orden_compra/liberar_pago_anticipado.php?id=<?php echo $dc_orden_compra ?>");
});
$('#asigna_facturas').unbind('click').click(function(){
    pymerp.loadOverlay("<?php echo Factory::buildUrl('AsignarFacturaCompra','ventas','showAsignarFacturaCompra','orden_compra',
    array('dc_orden_compra' => $dc_orden_compra)); ?>");
});
</script>
