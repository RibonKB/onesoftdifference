<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(isset($_POST['prov_rut'])){
	//Se escapan los caracteres por seguridad
	$db->escape($_POST['prov_rut']);
	$datos = $db->select("tb_proveedor","dc_proveedor,dg_razon,dg_rut",
	"dg_rut = '{$_POST['prov_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");

	if(!count($datos)){
		$error_man->showErrorRedirect("No se encontró el proveedor especificado, intentelo nuevamente.","sites/ventas/orden_compra/cr_orden_compra.php");
	}

	$datos = $datos[0];
	$poblacion['nota_venta'] = '';
}else if(isset($_POST['prov_id'])){
	$datos = $db->select("tb_proveedor",'dc_proveedor,dg_razon,dg_rut',
	"dc_proveedor={$_POST['prov_id']} AND dc_empresa={$empresa} AND dm_activo='1'");

	if(!count($datos)){
		$error_man->showErrorRedirect("Error: Proveedor inválido, intentelo nuevamente.","sites/ventas/nota_venta/src_nota_venta.php");
	}
	$datos = $datos[0];

	$poblacion['nota_venta'] =& $_POST['nv_numero'];
}

echo("<div id='secc_bar'>Creación de orden de compra</div>
<div id='main_cont'>
<div class='panes'>
<div class='title center'>
<button class='right button' id='prov_switch'>Cambiar Proveedor</button>
Generando orden de compra para <strong id='prov_razon' style='color:#000;'>{$datos['dg_razon']}</strong>
</div>
<hr class='clear' />");

	include_once("../../../../inc/form-class.php");
	$form = new Form($empresa);

	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio,dn_cantidad_decimales",
	"dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$c['dq_cambio'] = number_format($c['dq_cambio'],$c['dn_cantidad_decimales'],'.',',');
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |{$c['dq_cambio']}|";
		$form->Hidden("decimal{$c['dc_tipo_cambio']}",$c['dn_cantidad_decimales']);
	}

	$form->Start("sites/ventas/orden_compra/proc/crear_orden_compra.php","cr_orden_compra",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la orden de compra</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();

	$form->Text('Nota de venta','oc_nota_venta',0,255,$poblacion['nota_venta']);
	$form->Hidden('oc_id_nota_venta',0);
	$form->Text('Orden de servicio','oc_orden_servicio');
	$form->Hidden('oc_id_orden_servicio',0);
	$form->EndSection();

	$form->Section();
	$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1);
	$form->Text('Cambio','oc_cambio',1,255);
	$form->Listado('Ejecutivo','oc_executive','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),1,$userdata['dc_funcionario'],"tf.dc_empresa = {$empresa} AND dc_modulo = 1");
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','oc_observacion',0);
	$form->EndSection();

	echo("<br class='clear' /><br />");

	$form->Header("Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la nota de venta",'id="prod_info"');
	echo("<div id='prods' style='display:none;'>
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='55'>Opciones</th>
		<th width='120'>Código (Autocompleta)</th>
		<th>Descripción</th>
		<th width='170'>Proveedor</th>
		<th width='140'>CeCo</th>
		<th width='70'>Cantidad</th>
		<th width='110'>Stock</th>
		<th width='95'>Costo</th>
		<th width='95'>Costo total</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='7' align='right'>Totales</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='7'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='7'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='7'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>");
	$form->Hidden('prov_id',$datos['dc_proveedor']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->End('Crear','addbtn');

	echo("<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/cal.png' alt='' title='Fecha de Arribo/renovacion Estimada' class='set_fecha_arribo' />
			<input type='hidden' class='fecha_arribo' name='df_fecha_arribo[]' />
		</td>
		<td>
			<input type='text' class='prod_codigo searchbtn' size='7' />
		</td>
		<td>
			<input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' />
		</td>
		<td><select name='proveedor[]' style='width:165px;' class='prod_proveedor inputtext' required='required'><option></option>");
		$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'",array('order_by' => 'dg_razon'));
		foreach($proveedores as $p){
			echo("<option value='{$p['dc_proveedor']}'>{$p['dg_razon']}</option>");
		}
		echo("</select></td>

		<td><select name='ceco[]' style='width:160px;' class='prod_ceco inputtext' required='required'><option></option>");
		$ceco = $db->select('tb_ceco','dc_ceco,dg_ceco',"dc_empresa={$empresa} AND dm_activo = '1'",array('order_by' => 'dg_ceco'));
		foreach($ceco as $c){
			echo("<option value='{$c['dc_ceco']}'>{$c['dg_ceco']}</option>");
		}
		echo("</td><td>
			<input type='text' name='cant[]' class='prod_cant inputtext' size='5' style='text-align:right;' required='required' />
			<input type='hidden' class='min_value' value='0' />
		</td>

		<td class='prod_stock_libre'></td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='9' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
	</tr>
	<tr style='display:none;'>
		<td colspan='5' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>
	</table>");
?>
</div></div>
<style type="text/css">
	#prod_list .prod_desc{
		width:94%;
	}
</style>
<?php
require_once(__DIR__ . '/../../../../inc/Factory.class.php');
?>
<script type="text/javascript">
	$('#cli_switch').click(function(){
		loadOverlay('sites/ventas/switch_cliente.php');
	});
	empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;

</script>
<script type="text/javascript" src="jscripts/products_manager_orden_compra.js?v1_7_1"></script>
<script type="text/javascript" src="jscripts/sites/ventas/orden_compra/dateArribo.js"></script>
<script type="text/javascript">
dateArribo.init();
var setFechaArriboProducto = '<?php echo Factory::buildUrl('CrearOrdenCompra', 'ventas', 'setFechaArriboProducto', 'orden_compra'); ?>';
var valid_det = false;
if($('#oc_nota_venta').val() != ''){
	$('#oc_nota_venta').change();
<?php if(isset($_POST['dets'])): ?>
	valid_det = '<?=$_POST['dets'] ?>'.split(',');
	var valid_cant = '<?=$_POST['cants'] ?>'.split(',');
<?php endif; ?>
}
</script>
