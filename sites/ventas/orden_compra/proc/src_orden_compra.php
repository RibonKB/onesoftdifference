<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}


$_POST['oc_emision_desde'] = $db->sqlDate($_POST['oc_emision_desde']);
$_POST['oc_emision_hasta'] = $db->sqlDate($_POST['oc_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['oc_emision_desde']} AND {$_POST['oc_emision_hasta']})";

if($_POST['oc_nota_venta'] != ''){
	if(!is_numeric($_POST['oc_nota_venta'])){
		$error_man->showAviso('El formato de número de la nota de venta es incorrecto, corrijalo y vuelva a intentarlo');
		exit();
	}
	$nota_venta = $db->select('tb_nota_venta','dc_nota_venta',"dq_nota_venta={$_POST['oc_nota_venta']} AND dc_empresa = {$empresa}");
	if(!count($nota_venta)){
		$error_man->showWarning('La nota de venta especificada no existe');
		exit();
	}
	
	$conditions .= " AND dc_nota_venta = {$nota_venta[0]['dc_nota_venta']}";
	
}

if($_POST['oc_numero_desde']){
	if($_POST['oc_numero_hasta']){
		$conditions .= " AND (dq_orden_compra BETWEEN {$_POST['oc_numero_desde']} AND {$_POST['oc_numero_hasta']})";
	}else{
		$conditions = "dq_orden_compra = {$_POST['oc_numero_desde']} AND dc_empresa = {$empresa}";
	}
}

if(isset($_POST['oc_proveedor'])){
	$_POST['oc_proveedor'] = implode(',',$_POST['oc_proveedor']);
	$conditions .= " AND dc_proveedor IN ({$_POST['oc_proveedor']})";
}

$data = $db->select('tb_orden_compra','dc_orden_compra,dq_orden_compra',$conditions,array('order_by' => 'df_fecha_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron ordenes de compra con los criterios especificados");
	exit();
}

echo("<div id='show_orden_compra'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Ordenes de compra<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Orden</th>
	</tr>
</thead>
<tbody>
");

foreach($data as $c){
echo("
<tr>
	<td align='left'>
		<a href='sites/ventas/orden_compra/proc/show_orden_compra.php?id={$c['dc_orden_compra']}' class='oc_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_orden_compra']}</a>
	</td>
</tr>");
}

echo("</tbody>
</table>
</div>");
?>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='imgbtn' id='pago_anticipado' style='background-image:url(images/lukas.png);'>Pago Anticipado</button>
<button type='button' class='editbtn' id='edit_orden_compra'>Editar</button>
<button type='button' class='delbtn' id='null_orden_compra'>Anular</button>
<button type='button' class='editbtn' id='asigna_facturas'>Asignar Facturas</button>

</div>

<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".oc_load").click(function(e){
		e.preventDefault();
		$('#show_orden_compra').html("<img src='images/ajax-loader.gif' alt='' /> cargando orden de compra ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_orden_compra');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>