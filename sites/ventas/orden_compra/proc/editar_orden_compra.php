<?php
define('MAIN',1);
require_once('../../../../inc/init.php');

$db->start_transaction();

/**
*	Editar orden de compra
*
*	Primero se modifican los datos de cabecera de la orden de compra.
**/
$st = $db->prepare($db->update('tb_orden_compra',array(
	"dc_responsable" => '?',
	"dc_tipo_cambio" => '?',
	"dq_cambio" => '?',
	"dc_proveedor" => $_POST["prov_id"],
	"dq_iva" => $_POST["cot_iva"],
	"dq_neto" => $_POST['cot_neto'],
	"dg_observacion" => '?'
),"dc_orden_compra=?"));

$st->bindValue(1,$_POST["oc_executive"],PDO::PARAM_INT);
$st->bindValue(2,$_POST["prod_tipo_cambio"],PDO::PARAM_INT);
$st->bindValue(3,$_POST["oc_cambio"],PDO::PARAM_STR);
$st->bindValue(4,$_POST["oc_observacion"],PDO::PARAM_STR);
$st->bindValue(5,$_POST['oc_id'],PDO::PARAM_INT);

$db->stExec($st);

/**
*	Preparar SQL edición, creacion o eliminar detalle
*
*	Se preparan los SQL que actualizarán los detsalles de l orden de compra
*	en caso de ser detalle nuevo y que no haya nota de venta asignada se inserta un nuevo detalle
*	en caso en que uno de los detalles de la orden de compra se haya eliminado es necesario eliminarlo de la base de datos
*	y rebajar las cantidades compradas en caso de tener relacion con nota de venta
**/

//en caso de que se deba modificar el detalle
$det_update = $db->prepare($db->update('tb_orden_compra_detalle',array(
	'dc_producto' => '?',
	'dg_descripcion' => '?',
	'dq_cantidad' => '?',
	'dq_precio' => '?',
	'dc_proveedor' => $_POST["prov_id"],
	'df_fecha_arribo' => '?'
),"dc_detalle = ?"));

//En caso en que se deba crear un nuevo detalle (se prepara solo si no existe relación con nota de venta)
if($_POST['oc_id_nota_venta'] == 0){
	$det_insert = $db->prepare($db->insert('tb_orden_compra_detalle',array(
		'dc_orden_compra' => '?',
		'dc_producto' => '?',
		'dg_descripcion' => '?',
		'dq_cantidad' => '?',
		'dq_precio' => '?',
		'dc_proveedor' => '?',
		'df_fecha_arribo' => '?'
	)));

	$det_insert->bindValue(1,$_POST['oc_id'],PDO::PARAM_INT);
	$det_insert->bindValue(6,$_POST['prov_id'],PDO::PARAM_INT);

}
//En caso de que se asigne notas de ventas las cantidades compradas en los detalles de esta se verán modificados
else{
	$det_nv_update = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dc_comprada' => 'dc_comprada+?'
	),"dc_nota_venta_detalle = ?"));
}

//En caso de que se haya eliminado un detalle hay que borrarlo de la base de datos
if(isset($_POST['to_delete'])){
	$det_delete = $db->prepare("DELETE FROM tb_orden_compra_detalle WHERE dc_detalle = ?");
}

/**
*	Editar detalles orden de compra y acumular cantidades compradas antiguas y actuales
*
*	Se recorren cada uno de los detalles para revisar cambios sobre estos, se comprueba si es detalle nuevo y
*	se inserta o se modifica, todo con los SQL preparados anteriormente.
**/

$margen_total = 0;
foreach($_POST['prod'] as $i => $p){
	$_POST['cant'][$i] = str_replace(',','',$_POST['cant'][$i]);
	$_POST['costo'][$i] = str_replace(',','',$_POST['costo'][$i])*$_POST["oc_cambio"];

	if(isset($_POST['doc_wf_id'][$i])){
		$_POST['doc_wf_id'][$i] = explode('|',$_POST['doc_wf_id'][$i]);
		$id_detalle = $_POST['doc_wf_id'][$i][0];
		$id_detalle_nv = $_POST['doc_wf_id'][$i][1];
		$cantidad_antigua = $_POST['doc_wf_id'][$i][2];

		/*$det_update = $db->prepare($db->update('tb_orden_compra_detalle',array(
			'dc_producto' => '?',
			'dg_descripcion' => '?',
			'dq_cantidad' => '?',
			'dq_precio' => '?',
			'dc_proveedor' => $_POST["prov_id"]
		),"dc_detalle = ?"));*/

		$det_update->bindValue(1,$p,PDO::PARAM_INT);
		$det_update->bindValue(2,$_POST['desc'][$i],PDO::PARAM_STR);
		$det_update->bindValue(3,$_POST['cant'][$i],PDO::PARAM_INT);
		$det_update->bindValue(4,$_POST['costo'][$i],PDO::PARAM_STR);
		$det_update->bindValue(5,$db->sqlDate2($_POST['df_fecha_arribo'][$i]),PDO::PARAM_STR);
		$det_update->bindValue(6,$id_detalle,PDO::PARAM_INT);
		$db->stExec($det_update);
		if($id_detalle_nv != 0){
			$margen = $_POST['cant'][$i]-$cantidad_antigua;

			/*$det_nv_update = $db->prepare($db->update('tb_nota_venta_detalle',array(
				'dc_comprada' => 'dc_comprada+?'
			),"dc_nota_venta_detalle = ?"));*/
			$det_nv_update->bindValue(1,$margen,PDO::PARAM_INT);
			$det_nv_update->bindValue(2,$id_detalle_nv,PDO::PARAM_INT);

			$db->stExec($det_nv_update);

			$margen_total += $margen;
		}
	}else{
		//en caso de que el detalle sea nuevo, se inserta
		/*$det_insert = $db->prepare($db->insert('tb_orden_compra_detalle',array(
			'dc_orden_compra' => '?',
			'dc_producto' => '?',
			'dg_descripcion' => '?',
			'dq_cantidad' => '?',
			'dq_precio' => '?',
			'dc_proveedor' => '?'
		)));*/

		$det_insert->bindValue(2,$p,PDO::PARAM_INT);
		$det_insert->bindValue(3,$_POST['desc'][$i],PDO::PARAM_STR);
		$det_insert->bindValue(4,$_POST['cant'][$i],PDO::PARAM_INT);
		$det_insert->bindValue(5,$_POST['costo'][$i],PDO::PARAM_STR);
		$det_insert->bindValue(7,$db->sqlDate2($_POST['df_fecha_arribo'][$i]),PDO::PARAM_STR);

		$db->stExec($det_insert);
	}
}

/**
*	Eliminar detalles quitados
*
*	Se quitan de la base de datos los detalles de la orden de compra y se rebajan las cantidades correspondientes en los detalles de la nota de venta
*	Además se acumula la cantidad para restarsela al total comprado en la nota de venta
**/

if(isset($_POST['to_delete'])){

	foreach($_POST['to_delete'] as $d){
		$d = explode('|',$d);

		/* $det_delete = $db->prepare("DELETE FROM tb_orden_compra_detalle WHERE dc_detalle = ?"); */
		$det_delete->bindValue(1,$d[0],PDO::PARAM_INT);

		$db->stExec($det_delete);

		if($d[1] != 0){
			/*$det_nv_update = $db->prepare($db->update('tb_nota_venta_detalle',array(
				'dc_comprada' => 'dc_comprada+?'
			),"dc_nota_venta_detalle = ?"));*/

			$det_nv_update->bindValue(1,$d[2]*-1,PDO::PARAM_INT);
			$det_nv_update->bindValue(2,$d[1],PDO::PARAM_INT);

			$db->stExec($det_nv_update);

			$margen_total -= $d[2];
		}
	}
}

/**
*	Se modifican las cantidades compradas sobre la nota de venta
**/
if($margen_total != 0 && $_POST['oc_id_nota_venta'] != 0){
	$nv_ed = $db->doExec($db->update('tb_nota_venta',array(
		'dc_comprada' => "dc_comprada+{$margen_total}"
	),"dc_nota_venta = {$_POST['oc_id_nota_venta']}"));

	if($nv_ed == 0){
		$error_man->showAviso('La nota de venta asignada a la orden de compra no fue encontrada, pero se continúa de todas formas.');
	}
}

$db->commit();
$error_man->showConfirm("Se han modificado los datos de la orden de compra correctamente.");
?>
<script type="text/javascript">
$('#cr_orden_compra').prev('input').remove();
$('#cr_orden_compra').remove();
</script>
