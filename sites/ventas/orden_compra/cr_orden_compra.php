<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
require_once("../../../inc/lang/{$empresa}/nota_venta.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">Creación de Orden de compra</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/orden_compra/proc/cr_orden_compra.php","cr_orden_compra","cValidar");
	$form->Header("<strong>Indique el RUT del proveedor al que se le hará la orden de compra</strong><br />
	Los criterios de búsqueda son el RUT, Razon social o giro del proveedor");
	$form->Text("RUT proveedor","prov_rut",1,20);
	/*echo("<br /><fieldset style='display:inline;padding:0 20px;background:#E5E5E5;'><legend>Utilizar {$lang['nv_doc']} como plantilla (Opcional)</legend>");
	$form->Text("Indique el número de la {$lang['nv_doc']}",'nv_base_num');
	echo("<br /></fieldset>");*/
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
$("#prov_rut").autocomplete('sites/proc/autocompleter/proveedor.php',{
	formatItem: function(row){ return row[1]+" ("+row[0]+") "+row[2]; },
	minChars: 2,
	width:300
});
$('#nv_base_num').change(function(){
	val = parseInt($(this).val());
	if(val) $(this).val(val);
	else $(this).val('');
});
</script>