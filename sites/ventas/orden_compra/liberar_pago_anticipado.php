<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_orden_compra = intval($_POST['id']);

$orden_compra = $db->prepare($db->select('tb_orden_compra','dq_orden_compra, dq_monto_pagado, dq_pago_usado, dq_monto_liberado','dc_orden_compra = ? AND dc_empresa = ?'));
$orden_compra->bindValue(1,$dc_orden_compra,PDO::PARAM_INT);
$orden_compra->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($orden_compra);
$orden_compra = $orden_compra->fetch(PDO::FETCH_OBJ);

if($orden_compra === false){
	$error_man->showWarning('La orden de compra seleccionada es inválida, compruebe los datos de entrada.');
	exit;
}

$dq_disponible = $orden_compra->dq_monto_pagado - $orden_compra->dq_pago_usado - $orden_compra->dq_monto_liberado;

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="secc_bar">
	Liberación de saldos anticipados Orden de compra <b><?php echo $orden_compra->dq_orden_compra ?></b>
</div>
<div class="panes">
	<?php
		if($dq_disponible <= 0):
			$error_man->showAviso('No hay saldos anticipados para liberar');
			echo '</div>';
			exit;
		endif;
	?>
	
	<?php
		$form->Start('sites/ventas/orden_compra/proc/liberar_pago_anticipado.php','liberar_pagos');
			$form->Header('<strong>Indique los datos necesarios para poder liberar los saldos anticipados de la OC</strong><br />
			Los datos marcados con [*] son obligatorios.');
			$form->Section();
				$form->Textarea('Comentario','dg_comentario',1);
			$form->EndSection();
			$form->Section();
	?>
		<div style="background-color:#FFF;border:1px solid #BBB;padding:5px;line-height:20px;margin:22px 5px;width:250px;">
			<div class="info">
				Detalles
			</div>
			Orden de compra: <b><?php echo $orden_compra->dq_orden_compra ?></b>
			<br />
			Monto a liberar <b>$ <?php echo moneda_local($dq_disponible) ?></b>
		</div>
	<?php
			$form->EndSection();
		$form->Group();
		$form->Hidden('dc_orden_compra',$dc_orden_compra);
		$form->End('Liberar');
	?>
	
</div>