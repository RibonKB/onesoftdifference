<?php
	define("MAIN",1);
	include_once("../../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	include("template_orden_compra/modo{$empresa}.php");
	
	$datosOrden = $db->select("(SELECT * FROM tb_orden_compra WHERE dc_orden_compra = {$_GET['id']} AND dc_empresa={$empresa}) oc
	LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = oc.dc_nota_venta
	JOIN tb_funcionario f ON f.dc_funcionario = oc.dc_responsable
	JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio = oc.dc_tipo_cambio
	JOIN tb_proveedor p ON p.dc_proveedor = oc.dc_proveedor
		JOIN tb_contacto_proveedor c ON c.dc_contacto_proveedor = p.dc_contacto_default
		LEFT JOIN tb_comuna com ON com.dc_comuna = c.dc_comuna
		LEFT JOIN tb_region r ON r.dc_region = com.dc_region
		LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	'oc.dq_orden_compra,oc.dq_cambio,UNIX_TIMESTAMP(oc.df_fecha_emision) AS df_fecha_emision,oc.dq_neto,oc.dq_iva,oc.dg_observacion,
	nv.dq_nota_venta,CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS dg_responsable, tc.dg_tipo_cambio, tc.dn_cantidad_decimales,
	p.dg_razon,p.dg_rut,p.dg_giro,c.dg_contacto,c.dg_direccion,c.dg_telefono,com.dg_comuna,r.dg_region,cc.dg_cargo_contacto');
	
	if(!count($datosOrden)){
		$error_man->showWarning("La orden de compra especificada no existe");
		exit();
	}
	$datosOrden = $datosOrden[0];
	
	setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
	$datosOrden['df_fecha_emision'] = strftime("%d de %B de %Y",$datosOrden['df_fecha_emision']);
	
	$datosOrden['dq_total'] = $datosOrden['dq_neto']/$datosOrden['dq_cambio'];
	
	$datosOrden['detalle'] = $db->select("(SELECT * FROM tb_orden_compra_detalle WHERE dc_orden_compra={$_GET['id']}) d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto",
	'd.dg_descripcion,d.dq_cantidad,d.dq_precio,p.dg_codigo,p.dg_producto');
	
	$pdf = new PDF();
	$pdf->AddDetalle($datosOrden['detalle']);
	$pdf->Output();
	
	
?>