<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div class="secc_bar">Workflow</div><div class="panes">');

switch($_POST['mode']){
	case 'nv':
		$tb_nota_venta = "(SELECT * FROM tb_nota_venta WHERE dc_nota_venta={$_POST['id']} AND dc_empresa = {$empresa}) nv";
		
		$tb_usuario = "JOIN tb_usuario u ON u.dc_usuario =";
		$tb_funcionario = "JOIN tb_funcionario f ON f.dc_funcionario = u.dc_funcionario";
	
		$tb_oportunidad = "{$tb_nota_venta}
		JOIN tb_cotizacion cot ON cot.dc_cotizacion = nv.dc_cotizacion
		JOIN tb_oportunidad op ON op.dq_oportunidad = cot.dq_oportunidad AND op.dc_empresa = {$empresa}
		{$tb_usuario} op.dc_usuario_creacion
		{$tb_funcionario}";
		
		$tb_cotizacion = "{$tb_nota_venta}
		JOIN tb_cotizacion cot ON cot.dc_cotizacion = nv.dc_cotizacion
		{$tb_usuario} cot.dc_usuario_creacion
		{$tb_funcionario}";
		
		$tb_factura_venta = "(SELECT * FROM tb_factura_venta WHERE dc_nota_venta = {$_POST['id']} AND dc_empresa = {$empresa}) fv
		{$tb_usuario} fv.dc_usuario_creacion
		{$tb_funcionario}";
		
		$tb_factura_compra = "(SELECT * FROM tb_factura_compra WHERE dc_nota_venta = {$_POST['id']} AND dc_empresa = {$empresa}) fc
		{$tb_usuario} fc.dc_usuario_creacion
		{$tb_funcionario}";
		
		$tb_guia_despacho = "(SELECT * FROM tb_guia_despacho WHERE dc_nota_venta = {$_POST['id']} AND dc_empresa = {$empresa}) gd
		{$tb_usuario} gd.dc_usuario_creacion
		{$tb_funcionario}";
		
		$tb_orden_compra = "(SELECT * FROM tb_orden_compra WHERE dc_nota_venta = {$_POST['id']} AND dc_empresa = {$empresa}) oc
		{$tb_usuario} oc.dc_usuario_creacion
		{$tb_funcionario}";
		
		$tb_guia_recepcion = "(SELECT * FROM tb_guia_recepcion WHERE dc_nota_venta = {$_POST['id']} AND dc_empresa = {$empresa}) gr
		{$tb_usuario} gr.dc_usuario_creacion
		{$tb_funcionario}";
		
		$tb_nota_venta_val = $tb_nota_venta."
		{$tb_usuario} nv.dc_usuario_validacion
		{$tb_funcionario}";
		
		$tb_nota_venta_conf = $tb_nota_venta."
		{$tb_usuario} nv.dc_usuario_confirmacion
		{$tb_funcionario}";
		
		$tb_nota_venta = $tb_nota_venta."
		{$tb_usuario} nv.dc_usuario_creacion
		{$tb_funcionario}";
		
		break;
	default: exit;
}

$query = "SELECT 'Oportunidad' tipo, op.dq_oportunidad numero_documento, op.df_fecha_emision fecha_emision, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, IF(dm_estado ,'Abierta','Cerrada') dg_estado, op.dq_monto dq_neto
FROM {$tb_oportunidad}
UNION
SELECT 'Cotización', cot.dq_cotizacion, cot.df_fecha_emision, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, st.dg_estado, cot.dq_neto
FROM {$tb_cotizacion}
JOIN tb_cotizacion_estado st ON st.dc_estado = cot.dc_estado
UNION
SELECT 'Nota de venta', nv.dq_nota_venta, nv.df_fecha_emision, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, 'Creada', nv.dq_neto
FROM {$tb_nota_venta}
UNION
SELECT 'Nota de venta', nv.dq_nota_venta, nv.df_validacion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, 'Validada', nv.dq_neto
FROM {$tb_nota_venta_val}
UNION
SELECT 'Nota de venta', nv.dq_nota_venta, nv.df_confirmacion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, 'Confirmada', nv.dq_neto
FROM {$tb_nota_venta_conf}
UNION
SELECT 'Orden de compra', oc.dq_orden_compra, oc.df_fecha_emision, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, IF(oc.dm_nula = 0,'Vigente','Nula'), oc.dq_neto
FROM {$tb_orden_compra}
UNION 
SELECT 'Guía de recepción', gr.dq_guia_recepcion, gr.df_fecha_emision, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, '-', NULL
FROM {$tb_guia_recepcion}
UNION
SELECT 'Factura de venta', fv.dq_factura, fv.df_creacion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno,IF(fv.dm_nula = 0,'Vigente','Nula'), fv.dq_neto
FROM {$tb_factura_venta}
UNION
SELECT 'Factura de compra', fc.dq_factura, fc.df_creacion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno,IF(fc.dm_nula = 0,'Vigente','Nula'), fc.dq_neto
FROM {$tb_factura_compra}
UNION
SELECT 'Guía de despacho', gd.dq_guia_despacho, gd.df_emision, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno,IF(gd.dm_nula = 0,'Vigente','Nula'), gd.dq_neto
FROM {$tb_guia_despacho}
UNION
SELECT 'Factura de venta', fv2.dq_factura, fv2.df_creacion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno,IF(fv2.dm_nula = 0,'Vigente','Nula'), fv2.dq_neto
FROM {$tb_guia_despacho}
JOIN tb_factura_venta fv2 ON fv2.dc_factura = gd.dc_factura
WHERE fv2.dc_nota_venta = 0
ORDER BY fecha_emision";

$data = $db->selectToArray($db->query($query));

echo('<table width="100%" class="tab bicolor_tab">
<thead><tr>
	<th>Tipo de documento</th>
	<th>Número de documento</th>
	<th width="100">Neto ('.$empresa_conf['dg_moneda_local'].')</th>
	<th width="100">Fecha emisión</th>
	<th>Estado</th>
	<th width="200">Responsable</th>
</tr></thead><tbody>');

foreach($data as $doc){
	
	$doc['dq_neto'] = moneda_local($doc['dq_neto']);
	$doc['fecha_emision'] = $db->dateTimeLocalFormat($doc['fecha_emision']);
	
	echo("<tr>
		<td>{$doc['tipo']}</td>
		<td><b>{$doc['numero_documento']}</b></td>
		<td align='right'>{$doc['dq_neto']}</td>
		<td>{$doc['fecha_emision']}</td>
		<td>{$doc['dg_estado']}</td>
		<td>{$doc['dg_nombres']} {$doc['dg_ap_paterno']} {$doc['dg_ap_materno']}</td>
	</tr>");
}

echo('</tbody></table>');

?>
</div>