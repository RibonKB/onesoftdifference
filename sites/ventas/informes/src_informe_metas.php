<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">Informe de metas</div>
<div id="main_cont"><div class="panes">
	<?php $form->Start('sites/ventas/informes/proc/src_informe_metas.php','src_informe_metas'); ?>
		<?php $form->Header("Indique los filtros de consulta para el informe de metas<br /><b>Los campos marcados con [*] son obligatorios</b>"); ?>
		<table class="tab" width="100%">
			<tbody>
				<tr>
					<td width="220">Fecha Emisión (de las Ventas)</td>
					<td width="320"><?php $form->Date('Desde','df_emision_desde',1,"01/".date("m/Y")); ?></td>
					<td><?php $form->Date('Hasta','df_emision_hasta',1,0); ?></td>
				</tr>
				<tr>
					<td>Clientes</td>
					<td><?php $form->ListadoMultiple('','dc_cliente','tb_cliente',array('dc_cliente','dg_razon')); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Ejecutivos</td>
					<td><?php $form->ListadoMultiple('','dc_ejecutivo','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 1"); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Marcas</td>
					<td><?php $form->ListadoMultiple('','dc_marca','tb_marca',array('dc_marca','dg_marca')); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Lineas de negocio</td>
					<td><?php $form->ListadoMultiple('','dc_linea_negocio','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio')); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Estado Cotización</td>
					<td><?php $form->ListadoMultiple('','dc_estado_cotizacion','tb_cotizacion_estado',array('dc_estado','dg_estado')); ?></td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
	<?php $form->End('Ejecutar'); ?>
</div></div>
<script type="text/javascript">
$('#dc_cliente, #dc_ejecutivo, #dc_marca, #dc_linea_negocio, #dc_estado_cotizacion').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>