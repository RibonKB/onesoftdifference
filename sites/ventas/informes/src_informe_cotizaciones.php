<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">Informe de cotizaciones</div>
<div id="main_cont">
<div class="panes">

<?php
echo('
<fieldset>
<div class="left">
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=0">Informe por Proveedor</a></button><br />
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=1">Informe por Producto</a></button><br />
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=2">Informe por Tipo de Producto</a></button>
</div>		


<div class="left">
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=3">Informe por Tipo de Cambio</a></button><br />
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=4">Informe por Línea de Negocio</a></button><br />
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=5">Informe por Marca</a></button>
</div>

<div class="left">
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=6">Informe por Televenta</a></button><br />
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=7">Informe por Ejecutivo</a></button><br />
</div>

<div class="left">
		<button type="button" class="button"><a href="sites/ventas/informes/proc/src_informe_cotizaciones.php?tipo=8">Informe por Status</a></button>		
</div>

<br class="clear" />
</fieldset>
');
?>
</div>
</div>

<style type="text/css">
.button{
width:335px;
}
.button a{
display:block;
}
</style>

<script type="text/javascript">
	$('#content button').click(function(e){
		e.preventDefault();
		loadpage($(this).children('a').attr('href'));
	});
</script>