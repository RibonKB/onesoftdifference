<?php
	define("MAIN",1);
	require_once("../../../../inc/global.php");
	if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

// Generación de secc_bar según tipo de informe

	switch($_POST['tipo']){
	case '0':echo('<div id="secc_bar">Informe de cotizaciones por proveedor</div>');
	break;
	case '1':echo('<div id="secc_bar">Informe de cotizaciones por producto</div>');
	break;
	case '2':echo('<div id="secc_bar">Informe de cotizaciones por tipo de producto</div>');
	break;
	case '3':echo('<div id="secc_bar">Informe de cotizaciones por tipo de cambio</div>');
	break;
	case '4':echo('<div id="secc_bar">Informe de cotizaciones por línea de negocio</div>');
	break;
	case '5':echo('<div id="secc_bar">Informe de cotizaciones por marca</div>');
	break;
	case '6':echo('<div id="secc_bar">Informe de cotizaciones por televenta</div>');
	break;
	case '7':echo('<div id="secc_bar">Informe de cotizaciones por ejecutivo</div>');
	break;
	case '8':echo('<div id="secc_bar">Informe de cotizaciones por status</div>');
	break;
	}
?>

<div id="main_cont" class="center">
<div class="panes">

<?php
include_once('../../../../inc/form-class.php');
	$form = new Form($empresa);
	
	$form->Header('<b>Indique los filtros para el informe</b><br />Los campos marcados con [*] son obligatorios');
	$form->Start('sites/ventas/informes/proc/report_cotizaciones.php','report_cotizaciones');
	$form->Button('Aplicar variante guardada','id="opt_load"','btnfilter_apply');
	$form->Button('Guardar variante','id="opt_save"','btnfilter_save');
	
	// Campos del formulario según tipo de informe
	
	echo('<table class="tab" style="text-align:left;" id="form_container">');
	switch($_POST['tipo']){
	case '0':echo('<tr><td width="50">Proveedor</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione proveedor(es)','lista_proveedores','tb_proveedor',array('dc_proveedor','dg_razon'));
	echo('</td></tr>');
	break;
	case '1':echo('<td>Producto</td><td>');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->Textarea('Ingrese un código de producto por cada línea','lista_codigo');
	echo('</td><td>');
	$form->Text('Descripción del producto','producto_desc',0);
	echo('</td>');
	break;
	case '2':echo('<td>Tipo de Producto</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione tipo(s) de producto','lista_tipo_producto','tb_tipo_producto',array('dc_tipo_producto','dg_tipo_producto'));
	break;
	case '3':echo('<td>Tipo de Cambio</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione tipo(s) de cambio','lista_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'));
	break;
	case '4':echo('<td>Línea de Negocio</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione línea(s) de negocio','lista_linea_negocio','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio'));
	break;
	case '5':echo('<td>Marca</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione marca(s)','lista_marca','tb_marca',array('dc_marca','dg_marca'));
	break;
	case '6':echo('<td>Televenta</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione televenta(s)','lista_televenta','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
	break;
	case '7':echo('<td>Ejecutivo</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione ejecutivo(s)','lista_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
	break;
	case '8':echo('<td>Status</td><td colspan="2">');
	$form->Radiobox('','radio_tipo',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->ListadoMultiple('Seleccione status','lista_status','tb_cotizacion_estado',array('dc_estado','dg_estado'));
	break;
	}	
	
	// Campos del formulario por defecto
	
	echo('<tr><td width="50">RUT Cliente</td><td colspan="2">');
	$form->Radiobox('','radio_rut',array('Incluir','Excluir'),'','');
	echo('<br />');
	$form->Textarea('Ingrese un RUT por cada línea','lista_rut');
	echo('</td></tr><tr><td>Nº de Cotización</td><td>');
	$form->Text('Desde','inf_cot_desde',0);
	echo('</td><td>');
	$form->Text('Hasta','inf_cot_hasta',0);
	echo('</td></tr><tr><td>Fecha de emisión</td><td>');
	$form->Date('Desde','inf_fecha_desde',1,'01'.date('/m/Y'));
	echo('</td><td>');
	$form->Date('Hasta','inf_fecha_hasta',1,date('d/m/Y'));
	echo('</td></tr><tr><td>&nbsp;</td>');
	echo('</td><td>&nbsp;');
	echo('</td></tr></table>');
	$form->Hidden('hiddentipo',$_POST['tipo']);
	$form->End('Ejecutar consulta','searchbtn');
	
?>

</div>
</div>

<script type="text/javascript">
	$("#lista_proveedores,#lista_tipo_producto,#lista_tipo_cambio,#lista_linea_negocio,#lista_marca,#lista_televenta,#lista_ejecutivo,#lista_status").multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>