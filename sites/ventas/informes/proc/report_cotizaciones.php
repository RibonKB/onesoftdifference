<?php
	define("MAIN",1);
	require_once("../../../../inc/global.php");
	if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

// Validación del formato de RUT(s) y generación de consulta a tb_cliente

	if($_POST['lista_rut'] == '')
		$clientes = "(SELECT * FROM tb_cliente WHERE dc_empresa = {$empresa})";
	else{
		$aux = explode("\n",$_POST['lista_rut']);
		$rutemp = str_replace(array('-','.'),array('',''),$aux);
		foreach($rutemp as $i => $r){
			$fin = '-'.substr($r,-1,1);
			$initemp = substr($r,0,-1);
			$ini = '';
			while(strlen($initemp) > 3){
				$iniwhile = '.'.substr($initemp,-3,3);
				$initemp = substr($initemp,0,-3);
				$ini = $iniwhile.$ini;
				}
			$rutemp[$i] = $initemp.$ini.$fin;
			$db->escape($rutemp[$i]);
		}
		$rut = implode("','",$rutemp);
		if($_POST['radio_rut'] == 0)
		$clientes = "(SELECT * FROM tb_cliente WHERE dc_empresa = {$empresa} AND dg_rut IN ('{$rut}'))";
		else
		$clientes = "(SELECT * FROM tb_cliente WHERE dc_empresa = {$empresa} AND dg_rut NOT IN ('{$rut}'))";
	}

// Validación del formato de número de cotizacion y fecha de emisión, y generación de consulta a tb_cotizacion

	$cotizaciones = "(SELECT * FROM tb_cotizacion WHERE dc_empresa = {$empresa})";
	$tablas = "{$cotizaciones} cot 
			   JOIN {$clientes} cl ON cot.dc_cliente = cl.dc_cliente
			   JOIN (SELECT * FROM tb_cotizacion_detalle
			   WHERE dc_empresa = {$empresa}) det ON det.dc_cotizacion = cot.dc_cotizacion";

	$cot_desde = $_POST['inf_cot_desde'];
	$cot_hasta = $_POST['inf_cot_hasta'];

	if($cot_desde != '' & $cot_hasta != ''){
		if($cot_desde > $cot_hasta){
			$aux = $cot_hasta;
			$cot_hasta = $cot_desde;
			$cot_desde = $aux;
			}
		$cotizaciones .= " AND dq_cotizacion BETWEEN {$cot_desde} AND {$cot_hasta}";
	}else{
		if($cot_desde != '')
			$cotizaciones .= " AND dq_cotizacion = {$cot_desde}";
	}
	
	$fec_desde = $db->sqlDate($_POST['inf_fecha_desde']);
	$fec_hasta = $db->sqlDate($_POST['inf_fecha_hasta'].' 23:59');
	
	$cotizaciones .= " AND df_fecha_emision BETWEEN {$fec_desde} AND {$fec_hasta})";

// Generación de campos de consulta específica según tipo de informe

	$tipoconsulta = "(SELECT * FROM";
	
	switch($_POST['hiddentipo']){
	case '0':
	if(!isset($_POST['lista_proveedores']))
		$tipoconsulta .= " tb_proveedor WHERE dc_empresa = {$empresa})";
	else{
		$proveedores = implode(",",$_POST['lista_proveedores']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_proveedor WHERE dc_empresa = {$empresa} AND dc_proveedor IN ({$proveedores}))";
		else
		$tipoconsulta .= " tb_proveedor WHERE dc_empresa = {$empresa} AND dc_proveedor NOT IN ({$proveedores}))";
		}
	$tablas .= " JOIN {$tipoconsulta} tipo ON det.dc_proveedor = tipo.dc_proveedor";
	$campostipo = "tipo.dc_proveedor,tipo.dg_razon AS dg_proveedor";
	$headertipo = "Proveedor";
	$agrupador = "tipo.dc_proveedor";
	break;	
	case '1':
		if($_POST['lista_codigo'] != '' && $_POST['producto_desc'] == ''){
		$auxiliar = explode("\n",$_POST['lista_codigo']);
		$producto = implode("','",$auxiliar);
			if($_POST['radio_tipo'] == 0)
			$tablas .= " AND dg_producto IN ('{$producto}')";
			else
			$tablas .= " AND dg_producto NOT IN ('{$producto}')";
		}
		if($_POST['lista_codigo'] != '' && $_POST['producto_desc'] != ''){
		$auxiliar = explode("\n",$_POST['lista_codigo']);
		$producto = implode("','",$auxiliar);
			if($_POST['radio_tipo'] == 0)
			$tablas .= " AND dg_producto IN ('{$producto}') AND dg_producto LIKE '%{$_POST['producto_desc']}%'";
			else
			$tablas .= " AND dg_producto NOT IN ('{$producto}') AND dg_producto LIKE '%{$_POST['producto_desc']}%'";
		}
		if($_POST['lista_codigo'] == '' && $_POST['producto_desc'] != '')
			$tablas .= " AND dg_producto LIKE '%{$_POST['producto_desc']}%'";
		$campostipo = "det.dg_producto,concat_ws(' ',det.dg_producto,det.dg_descripcion) AS dg_producto";
		$headertipo = "Producto";
		$agrupador = "det.dg_producto";
	break;
	case '2':
	if(!isset($_POST['lista_tipo_producto']))
		$tipoconsulta .= " tb_tipo_producto WHERE dc_empresa = {$empresa})";
	else{
		$tipo_producto = implode(",",$_POST['lista_tipo_producto']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_tipo_producto WHERE dc_empresa = {$empresa} AND dc_tipo_producto IN ({$tipo_producto}))";
		else
		$tipoconsulta .= " tb_tipo_producto WHERE dc_empresa = {$empresa} AND dc_tipo_producto NOT IN ({$tipo_producto}))";
		}
	$tablas .= " JOIN (SELECT * FROM tb_producto WHERE dc_empresa = {$empresa}) prod ON det.dg_producto = prod.dg_codigo
						 JOIN {$tipoconsulta} tipo ON prod.dc_tipo_producto = tipo.dc_tipo_producto";
	$campostipo = "tipo.dc_tipo_producto,tipo.dg_tipo_producto";
	$headertipo = "Tipo Producto";
	$agrupador = "tipo.dc_tipo_producto";
	break;
	case '3':
	if(!isset($_POST['lista_tipo_cambio']))
		$tipoconsulta .= " tb_tipo_cambio WHERE dc_empresa = {$empresa})";
	else{
		$tipo_cambio = implode(",",$_POST['lista_tipo_cambio']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_tipo_cambio WHERE dc_empresa = {$empresa} AND dc_tipo_cambio IN ({$tipo_cambio}))";
		else
		$tipoconsulta .= " tb_tipo_cambio WHERE dc_empresa = {$empresa} AND dc_tipo_cambio NOT IN ({$tipo_cambio}))";
		}
	$tablas .= " JOIN {$tipoconsulta} tipo ON cot.dc_tipo_cambio = tipo.dc_tipo_cambio";
	$campostipo = "tipo.dc_tipo_cambio,tipo.dg_tipo_cambio";
	$headertipo = "Tipo Cambio";
	$agrupador = "tipo.dc_tipo_cambio";
	break;	
	case '4':
	if(!isset($_POST['lista_linea_negocio']))
		$tipoconsulta .= " tb_linea_negocio WHERE dc_empresa = {$empresa})";
	else{
		$linea_negocio = implode(",",$_POST['lista_linea_negocio']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_linea_negocio WHERE dc_empresa = {$empresa} AND dc_linea_negocio IN ({$linea_negocio}))";
		else
		$tipoconsulta .= " tb_linea_negocio WHERE dc_empresa = {$empresa} AND dc_linea_negocio NOT IN ({$linea_negocio}))";
		}
	$tablas .= " JOIN (SELECT * FROM tb_producto WHERE dc_empresa = {$empresa}) prod ON det.dg_producto = prod.dg_codigo
						 JOIN {$tipoconsulta} tipo ON prod.dc_linea_negocio = tipo.dc_linea_negocio";
	$campostipo = "tipo.dc_linea_negocio,tipo.dg_linea_negocio";
	$headertipo = "Línea de Negocio";
	$agrupador = "tipo.dc_linea_negocio";
	break;	
	case '5':
	if(!isset($_POST['lista_marca']))
		$tipoconsulta .= " tb_marca WHERE dc_empresa = {$empresa})";
	else{
		$marca = implode(",",$_POST['lista_marca']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_marca WHERE dc_empresa = {$empresa} AND dc_marca IN ({$marca}))";
		else
		$tipoconsulta .= " tb_marca WHERE dc_empresa = {$empresa} AND dc_marca NOT IN ({$marca}))";
		}
	$tablas .= " JOIN (SELECT * FROM tb_producto WHERE dc_empresa = {$empresa}) prod ON det.dg_producto = prod.dg_codigo
						 JOIN {$tipoconsulta} tipo ON prod.dc_marca = tipo.dc_marca";
	$campostipo = "tipo.dc_marca,tipo.dg_marca";
	$headertipo = "Marca";
	$agrupador = "tipo.dc_marca";
	break;	
	case '6':
	if(!isset($_POST['lista_televenta']))
		$tipoconsulta .= " tb_funcionario WHERE dc_empresa = {$empresa})";
	else{
		$televenta = implode(",",$_POST['lista_televenta']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_funcionario WHERE dc_empresa = {$empresa} AND dc_funcionario IN ({$televenta}))";
		else
		$tipoconsulta .= " tb_funcionario WHERE dc_empresa = {$empresa} AND dc_funcionario NOT IN ({$televenta}))";
		}
	$tablas .= " JOIN {$tipoconsulta} tipo ON cot.dc_ejecutivo_tv = tipo.dc_funcionario";
	$campostipo = "tipo.dc_funcionario,concat_ws(' ',tipo.dg_nombres,tipo.dg_ap_paterno,tipo.dg_ap_materno) AS dg_nombre";
	$headertipo = "Televenta";
	$agrupador = "tipo.dc_funcionario";
	break;
	case '7':
	if(!isset($_POST['lista_ejecutivo']))
		$tipoconsulta .= " tb_funcionario WHERE dc_empresa = {$empresa})";
	else{
		$ejecutivo = implode(",",$_POST['lista_ejecutivo']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_funcionario WHERE dc_empresa = {$empresa} AND dc_funcionario IN ({$ejecutivo}))";
		else
		$tipoconsulta .= " tb_funcionario WHERE dc_empresa = {$empresa} AND dc_funcionario NOT IN ({$ejecutivo}))";
		}
	$tablas .= " JOIN {$tipoconsulta} tipo ON cot.dc_ejecutivo = tipo.dc_funcionario";
	$campostipo = "tipo.dc_funcionario,concat_ws(' ',tipo.dg_nombres,tipo.dg_ap_paterno,tipo.dg_ap_materno) AS dg_nombre";
	$headertipo = "Ejecutivo";
	$agrupador = "tipo.dc_funcionario";
	break;	
	case '8':
	if(!isset($_POST['lista_status']))
		$tipoconsulta .= " tb_cotizacion_estado WHERE dc_empresa = {$empresa})";
	else{
		$status = implode(",",$_POST['lista_status']);
		if($_POST['radio_tipo'] == 0)
		$tipoconsulta .= " tb_cotizacion_estado WHERE dc_empresa = {$empresa} AND dc_estado IN ({$status}))";
		else
		$tipoconsulta .= " tb_cotizacion_estado WHERE dc_empresa = {$empresa} AND dc_estado NOT IN ({$status}))";
		}
	$tablas .= " JOIN (SELECT * FROM tb_cotizacion_estado WHERE dc_empresa = {$empresa}) tipo ON cot.dc_estado = tipo.dc_estado";
	$campostipo = "tipo.dc_estado,tipo.dg_estado";
	$headertipo = "Status";
	$agrupador = "tipo.dc_estado";
	break;	
	}
	
// Generación final de campos a consultar en la query
	
	$campos = "{$campostipo},cot.dq_cotizacion,cl.dg_razon,cl.dg_rut,date_format(cot.df_fecha_emision,'%d/%m/%Y') AS df_fecha_emision,cot.dq_neto,cot.dc_cotizacion, SUM(ABS(det.dq_precio_venta-det.dq_precio_compra)) AS dq_margen_total, (SUM(det.dq_precio_venta-det.dq_precio_compra)/SUM(det.dq_precio_venta))*100 AS dq_margen_porcentual";
		
// Generación final de la consulta a la base de datos

	$resultado = $db->select($tablas,$campos,'',array('order_by'=>$agrupador,'group_by'=>"dq_cotizacion,{$agrupador}"),MYSQL_BOTH);
	
	if(!count($resultado)){
		$error_man->showAviso('No se encontraron registros con los criterios especificados');
		exit;
	}
	$agrupados = array();
		
	foreach($resultado as $res){
			$agrupados[$res[0]][] = $res;
	}

		foreach($agrupados as $r){
		
			echo("<table class='tab' width='100%'><caption>{$r[0][1]}</caption><thead><tr>
			<th>Nº Cotización</th>
			<th>Cliente</th>
			<th>Fecha Emisión</th>
			<th>Monto Neto</th>
			<th>Margen Total</th>
			<th>Margen Porcentual</th>
			</tr></thead>
			<tbody>");
			
			$totalmonto = 0;
			$totalneto = 0;
			
			foreach($r as $res){
			$totalmonto += $res[6];
			$totalneto += $res[8];
			$res[6] = number_format($res[6],$empresa_conf['dn_decimales_local'],$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
			$res[8] = number_format($res[8],$empresa_conf['dn_decimales_local'],$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
			$res[9] = number_format($res[9],3,$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
			
			echo("<tr><td class='cotizacion'><a href='sites/ventas/proc/show_cotizacion.php?id={$res[7]}'>{$res[2]}</td>
				  <td class='cliente' title='{$res[4]}' align='left'><a href='sites/clientes/proc/src_cliente.php?cli_rut={$res[4]}'>{$res[3]}</a></td>
				  <td>{$res[5]}</td>
				  <td align='right'>\${$res[6]}</td>
				  <td align='right'>\${$res[8]}</td>
				  <td align='right'>{$res[9]}%</td>
				 </tr>");
				}
			$totalmonto = number_format($totalmonto,$empresa_conf['dn_decimales_local'],$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
			$totalneto = number_format($totalneto,$empresa_conf['dn_decimales_local'],$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
			echo("</tbody><tfoot>
			<tr>
			<th colspan='3' align='right'>Total:</th>
			<th align='right'>\${$totalmonto}</th>
			<th align='right'>\${$totalneto}</th>
			</tr>
			");
			echo("</tfoot></table>");
			echo("<br />");
		}
?>

<!-- Javascript que levanta el detalle de la cotización en la misma ventana, y envía a la ventana de detalles del cliente -->

<script type="text/javascript">
	$('.cotizacion a').click(
	function(e){
		e.preventDefault();
		loadOverlay($(this).attr('href'));
	});
	
	$('.cliente a').click(
	function(e){
		e.preventDefault();
		loadpage($(this).attr('href'));
	});	
</script>