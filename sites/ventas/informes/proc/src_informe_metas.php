<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

//Preparar datos de consulta en la base de datos.
$date_from = $db->sqlDate($_POST['df_emision_desde']);
$date_to = $db->sqlDate($_POST['df_emision_hasta']);
$header_condition = "dc_empresa = {$empresa}";

//¿Filtrar clientes?
if(isset($_POST['dc_cliente'])){
	$cliente_cond = implode(',',$_POST['dc_cliente']);
	$header_condition .= " AND dc_cliente IN ({$cliente_cond})";
}

//¿Filtrar Ejecutivos?
if(isset($_POST['dc_ejecutivo'])){
	$executive_cond = implode(',',$_POST['dc_ejecutivo']);
	$header_condition .= " AND dc_ejecutivo IN ({$executive_cond})"; 
}

//¿Filtrar lineas de negocio?
if(isset($_POST['dc_linea_negocio'])){
	$linea_negocio_cond = " AND p.dc_linea_negocio IN (".implode(',',$_POST['dc_linea_negocio']).")";
}else{
	$linea_negocio_cond = '';
}

//¿Filtrar marcas?
if(isset($_POST['dc_marca'])){
	$marca_cond = " AND p.dc_marca IN (".implode(',',$_POST['dc_marca']).")";
}else{
	$marca_cond = '';
}

//¿Filtrar estados de cotizacion?
if(isset($_POST['dc_estado_cotizacion'])){
	$estado_cot_cond = 'AND dc_estado IN ('.implode(',',$_POST['dc_estado_cotizacion']).')';
}else{
	$estado_cot_cond = '';
}

//Trae los datos de nota de venta y facturación.
$nv_data = $db->prepare($db->select("(SELECT * FROM tb_nota_venta WHERE {$header_condition} AND dm_nula = 0 AND (df_fecha_emision BETWEEN {$date_from} AND {$date_to})) nv
JOIN tb_funcionario ex ON ex.dc_funcionario = nv.dc_ejecutivo
JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta = nv.dc_nota_venta
LEFT JOIN (SELECT d.* FROM tb_factura_venta_detalle d
JOIN tb_factura_venta f ON f.dc_factura = d.dc_factura AND f.dm_nula = 0) fvd
	ON fvd.dc_detalle_nota_venta = nvd.dc_nota_venta_detalle
JOIN tb_producto p ON p.dc_producto = nvd.dc_producto AND p.dc_empresa = {$empresa} {$marca_cond} {$linea_negocio_cond}
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
JOIN tb_marca mc ON mc.dc_marca = p.dc_marca
JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio",
'nv.dc_ejecutivo, nv.dc_cliente, nvd.dc_producto, (nvd.dq_cantidad * nvd.dq_precio_venta) dq_cierre, (fvd.dc_cantidad * fvd.dq_precio) dq_facturado,
p.dc_marca, p.dc_linea_negocio, cl.dg_razon, mc.dg_marca, ln.dg_linea_negocio, ex.dg_nombres, ex.dg_ap_paterno, ex.dg_ap_materno, p.dg_producto'));

$db->stExec($nv_data);

$nv_data = $nv_data->fetchAll(PDO::FETCH_ASSOC);

foreach($nv_data as $i => $v){
	if($v['dq_facturado'] == NULL){
		$nv_data[$i]['dq_facturado'] = 0;
	}
}

//Trae los datos de proyección (de cotización)
$cot_data = $db->prepare($db->select("(SELECT * FROM tb_cotizacion WHERE {$header_condition} {$estado_cot_cond} AND (df_fecha_emision BETWEEN {$date_from} AND {$date_to})) cot
JOIN tb_funcionario ex ON ex.dc_funcionario = cot.dc_ejecutivo
JOIN tb_cotizacion_detalle cd ON cd.dc_cotizacion = cot.dc_cotizacion
JOIN tb_producto p ON p.dg_codigo = cd.dg_producto AND p.dc_empresa = {$empresa} {$marca_cond} {$linea_negocio_cond}
JOIN tb_cliente cl ON cl.dc_cliente = cot.dc_cliente
JOIN tb_marca mc ON mc.dc_marca = p.dc_marca
JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio",
'cot.dc_ejecutivo, cot.dc_cliente, p.dc_producto, (cd.dq_cantidad * cd.dq_precio_venta) dq_proyeccion, p.dc_marca, p.dc_linea_negocio, cl.dg_razon,
mc.dg_marca, ln.dg_linea_negocio, ex.dg_nombres, ex.dg_ap_paterno, ex.dg_ap_materno, p.dg_producto'));

$db->stExec($cot_data);

$cot_data = $cot_data->fetchAll(PDO::FETCH_ASSOC);

/*Obtiene las metas de las marcas y lineas de negocio
$metas_linea_marca = $db->doQuery($db->select('tb_meta m
JOIN tb_meta_mensual mm ON mm.dc_meta = m.dc_meta',
'dc_marca, dc_linea_negocio, SUM(mm.dq_monto_mensual) dq_meta',
"m.dc_empresa = {$empresa} AND mm.dc_mes > MONTH({$date_from}) AND mm.dc_mes < MONTH({$date_to})",
array('group_by' => 'm.dc_marca, m.dc_linea_negocio')));
$metas_linea_marca = $metas_linea_marca->fetchAll(PDO::FETCH_ASSOC);*/

//Metas ejecutivos
$metas = $db->doQuery($db->select('tb_meta_ejecutivo ex
JOIN tb_meta_mensual mm ON mm.dc_mes = ex.dc_mes AND mm.dc_meta = ex.dc_meta
JOIN tb_meta m ON m.dc_meta = mm.dc_meta',
'ex.dc_ejecutivo, SUM((ex.dc_porcentaje/100)*mm.dq_monto_mensual) dq_meta, m.dc_marca, m.dc_linea_negocio',
"m.dc_empresa = {$empresa} AND mm.dc_mes > MONTH({$date_from}) AND mm.dc_mes < MONTH({$date_to})",
array('group_by' => 'm.dc_marca, m.dc_linea_negocio, ex.dc_ejecutivo')));
$metas = $metas->fetchAll(PDO::FETCH_ASSOC);

//Tipos de cambio y su cambio para utilizarlo en los valores de la tabla
$tipos_cambio = $db->doQuery($db->select('tb_tipo_cambio','dq_cambio, dg_tipo_cambio',"dc_empresa = {$empresa} AND dm_activo = 1",array('order_by' => 'dq_cambio')));

//Obtener clientes
$clientes = array();
$marcas = array();
$lineas = array();
$ejecutivos = array();
$productos = array();
foreach($nv_data as $nv){
	$clientes[$nv['dc_cliente']] = $nv['dg_razon'];
	$marcas[$nv['dc_marca']] = $nv['dg_marca'];
	$lineas[$nv['dc_linea_negocio']] = $nv['dg_linea_negocio'];
	$ejecutivos[$nv['dc_ejecutivo']] = $nv['dg_nombres'] . ' '. $nv['dg_ap_paterno'] . ' ' . $nv['dg_ap_materno'];
	$productos[$nv['dc_producto']] = $nv['dg_producto'];
}
foreach($cot_data as $cot){
	$clientes[$cot['dc_cliente']] = $cot['dg_razon'];
	$marcas[$cot['dc_marca']] = $cot['dg_marca'];
	$lineas[$cot['dc_linea_negocio']] = $cot['dg_linea_negocio'];
	$ejecutivos[$cot['dc_ejecutivo']] = $cot['dg_nombres'] . ' ' . $cot['dg_ap_paterno'] . ' ' . $cot['dg_ap_materno'];
	$productos[$cot['dc_producto']] = $cot['dg_producto'];
}

?>
<div id="multiselect_order">
<fieldset class="decorated">
	<div class="left">
	<label>Campos disponibles<br />
	<select multiple="multiple" id="multi_ex" size="6" class="left inputtext"></select>
	</label>
	<br />
	<button id="add_field" class="button">Agregar</button>
	</div>
	<div class="left">
	<label>Campos en tabla<br />
	<select multiple="multiple" id="multi_in" size="6" class="left inputtext">
		<option value="dc_ejecutivo">Ejecutivo</option>
		<option value="dc_marca">Marca</option>
		<option value="dc_linea_negocio">Linea de negocio</option>
		<option value="dc_cliente">Cliente</option>
		<option value="dc_producto">Producto</option>
	</select>
	</label>
	<br />
	<button id="up_field" class="button">Subir</button>
	<button id="down_field" class="button">Bajar</button>
	<button id="delete_field" class="button">Quitar</button>
	</div>
	<div class="left">
	<label>Tipo de cambio usado para mostrar los valores<br />
	<select id="tipos_cambio" class="inputtext">
	<?php foreach($tipos_cambio as $tc): ?>
		<option value="<?php echo $tc['dq_cambio'] ?>"><?php echo $tc['dg_tipo_cambio'] ?></option>
	<?php endforeach; ?>
	</select></label>
	<br /><br />
	<button type="button" id="load_table" class="button">Cargar tabla dinámica</button>
	</div>
<br class="clear" />
</fieldset>
</div>
<div>
	<table class="tab" width="100%" id="dinamic_table">
		<thead><tr>
			<th id="campo_principal" width="300">Ejecutivo</th>
			<th>Meta General</th>
			<th>Cierre</th>
			<th>Diferencia</th>
			<th>Diferencia %</th>
			<th>Indicador</th>
			<th>Proyección cierre</th>
			<th>Diferencia</th>
			<th>Diferencia %</th>
			<th>Indicador</th>
			<th>Facturado</th>
			<th>Diferencia</th>
			<th>Diferencia %</th>
			<th>Indicador</th>
		</tr></thead>
		<tfoot><tr>
			<th align="right">Totales</th>
			<th align="right"  id="total_goal">-</th>
			<th align="right"  id="total_cierre">-</th>
			<th align="right"  id="total_cierre_diferencia">-</th>
			<th align="left"   id="total_cierre_porciento">-</th>
			<th align="center" id="total_cierre_indicador">-</th>
			<th align="right"  id="total_proyeccion">-</th>
			<th align="right"  id="total_proyeccion_diferencia">-</th>
			<th align="left"   id="total_proyeccion_porciento">-</th>
			<th align="center" id="total_proyeccion_indicador">-</th>
			<th align="right"  id="total_facturado">-</th>
			<th align="right"  id="total_facturado_diferencia">-</th>
			<th align="left"   id="total_facturado_porciento">-</th>
			<th align="center" id="total_facturado_indicador">-</th>
		</tr></tfoot>
		<tbody></tbody>
	</table>
</div>
<script type="text/javascript" src="jscripts/sites/ventas/meta/src_informe_metas.js?v=0_1a5"></script>
<script type="text/javascript">
	js_data.nv_data = <?php echo json_encode($nv_data) ?>;
	js_data.cot_data = <?php echo json_encode($cot_data) ?>;
	
	js_data.metas = <?php echo json_encode($metas) ?>;
	
	js_data.labels.dc_cliente = <?php echo json_encode($clientes) ?>;
	js_data.labels.dc_marca = <?php echo json_encode($marcas) ?>;
	js_data.labels.dc_linea_negocio = <?php echo json_encode($lineas) ?>;
	js_data.labels.dc_ejecutivo = <?php echo json_encode($ejecutivos) ?>;
	js_data.labels.dc_producto = <?php echo json_encode($productos) ?>;
	
	js_data.init();
</script>