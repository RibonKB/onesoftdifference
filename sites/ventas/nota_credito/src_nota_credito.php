<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Nota de Crédito</div>
<div id="main_cont"><br /><br />
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/nota_credito/proc/src_nota_credito.php","src_nota_credito");
	$form->Header("<strong>Indicar los parámetros de búsqueda de la nota de crédito</strong>");

	echo('<table class="tab" id="form_container" width="100%"><tr><td width="50">Número de nota de crédito</td><td width="280">');
	$form->Text("Desde","nc_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','nc_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','nc_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','nc_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','nc_client','tb_cliente',array('dc_cliente','dg_razon'));
	echo('</td><td>&nbsp;');
	// busqueda por monto
	echo('</td></tr><tr><td>Monto Total</td><td>');
	$form->Text('','nc_monto');
	echo('</td><td>&nbsp;</td></tr></table>');
	// fin busqueda por monto
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#nc_client').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>