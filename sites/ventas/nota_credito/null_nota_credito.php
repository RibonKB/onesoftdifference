<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['id'] = intval($_POST['id']);

$data = $db->select('tb_nota_credito','dq_nota_credito, dc_tipo_nota_credito',"dc_nota_credito = {$_POST['id']}");
$data = array_shift($data);

if($data == NULL){
	$error_man->showWarning('El número de nota de crédito especificado es inválido');
	exit;
}

$dq_nota_credito = $data['dq_nota_credito'];

/*
 * Caracteristicas que faltan por implementar
 */
if(count($db->select('tb_nota_credito_guia_despacho_liberada','true',"dc_nota_credito = {$_POST['id']}"))){
   $error_man->showWarning("La anulación de NC con guías de despacho liberadas no está implementada aún"); 
   exit;
}

if(count($db->select('tb_nota_credito_orden_servicio_liberada','true',"dc_nota_credito = {$_POST['id']}"))){
   $error_man->showWarning("La anulación de NC con ordenes de servicio liberadas no está implementada aún"); 
   exit;
}

/*if(count($db->select('tb_tipo_nota_credito','true',"dc_tipo_nota_credito = {$data['dc_tipo_nota_credito']} AND dm_retorna_anticipado = 1"))){
    $error_man->showWarning("La anulación de notas de crédito para devolución de facturas anticipadas no está implementada aún");
    exit;
}*/
/*
 * Fin faltas
 */

//END ESTADO CONTABLE

echo('<div class="secc_bar">Anular nota de crédito</div><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/nota_credito/proc/null_nota_credito.php','null_nc_form');
$form->Header('Indique las razones por las que se anulará la nota de crédito');
$error_man->showAviso("Atención, está a punto de anular la nota de crédito N° <b>{$dq_nota_credito}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->Radiobox('Emitida Fisicamente','null_fisica',array('NO','SI'),0);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('dc_nota_credito',$_POST['id']);
$form->End('Anular','delbtn');

?>