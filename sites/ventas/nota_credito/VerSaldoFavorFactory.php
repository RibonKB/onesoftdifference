<?php
require_once('../../../inc/Factory.class.php');
require_once('../../../inc/db-class.php');

class VerSaldoFavorFactory extends Factory{
	
	public $title = "Saldos a Favor";
	
	public function indexAction(){
		$dc_cliente = self::getRequest()->dc_cliente;
		$this->initFunctionsService();
		
		echo $this->getOverlayView('ventas/nota_credito/ver_saldo_favor',array(
			'notas_credito' => $this->getNotaCreditoFavor($dc_cliente),
			'cliente' => $this->getClienteData($dc_cliente)
		));
		
	}
	
	private function getNotaCreditoFavor($dc_cliente){
		
		$db = $this->getConnection();
		
		$data = $db->prepare(
					$db->select('tb_nota_credito nc
						LEFT JOIN tb_factura_venta fv ON fv.dc_factura = nc.dc_factura',
						'nc.dq_nota_credito, nc.dq_folio, fv.dq_factura, fv.dq_folio dq_folio_factura, nc.df_emision, nc.dq_total, nc.dq_favor_usado',
						'nc.dc_cliente = ? AND nc.dq_total > nc.dq_favor_usado'));
		$data->bindValue(1,$dc_cliente,PDO::PARAM_INT);
		$db->stExec($data);
		
		return $data;
		
	}
	
	private function getClienteData($dc_cliente){
		return $this->getConnection()->getRowById('tb_cliente',$dc_cliente,'dc_cliente');
	}
	
}