<?php
	define("MAIN",1);
	include_once("../../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	include("template_nota_credito/modo{$empresa}.php");
	
	$datosFactura = $db->select("(SELECT * FROM tb_nota_credito WHERE dc_empresa={$empresa} AND dc_nota_credito = {$_GET['id']}) nc
	LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = nc.dc_nota_venta
	LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = nc.dc_orden_servicio
	JOIN tb_medio_pago mp ON mp.dc_medio_pago = nc.dc_medio_pago
	JOIN tb_contacto_cliente c ON c.dc_contacto = nc.dc_contacto
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
		JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
		JOIN tb_region reg ON reg.dc_region = com.dc_region
	JOIN tb_contacto_cliente ce ON ce.dc_contacto = nc.dc_contacto_entrega
		JOIN tb_cliente_sucursal s2 ON s2.dc_sucursal = ce.dc_sucursal
		JOIN tb_comuna com2 ON com2.dc_comuna = s2.dc_comuna
		JOIN tb_region reg2 ON reg2.dc_region = com2.dc_region
	LEFT JOIN tb_tipo_cargo tc ON tc.dc_tipo_cargo = nc.dc_tipo_cargo
	LEFT JOIN tb_tipo_nota_credito top ON top.dc_tipo_nota_credito = nc.dc_tipo_nota_credito
	LEFT JOIN tb_cliente cl ON cl.dc_cliente = nc.dc_cliente
	LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = nc.dc_ejecutivo",
	"nc.dg_comentario, nc.dq_folio dq_nota_credito,UNIX_TIMESTAMP(nc.df_emision) AS df_emision,DATE_FORMAT(nc.df_vencimiento,'%d/%m/%Y') AS df_vencimiento,nc.dm_tipo_impresion,
	nc.dg_orden_compra,nv.dq_nota_venta,os.dq_orden_servicio,nc.dq_neto,nc.dq_iva,nc.dq_total,nc.dc_nota_credito,mp.dg_medio_pago,
	c.dg_contacto,s.dg_direccion,reg.dg_region,com.dg_comuna,c.dg_fono,s2.dg_direccion AS dg_direccion_entrega,com2.dg_comuna AS dg_comuna_entrega,
	reg2.dg_region AS dg_region_entrega,ce.dg_contacto AS dg_contacto_entrega,tc.dg_tipo_cargo,top.dg_tipo_nota_credito,cl.dg_razon,cl.dg_rut,cl.dg_giro,
	CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno) dg_ejecutivo");
	
	if(!count($datosFactura)){
		$error_man->showWarning("La nota de credito especificada no existe");
		exit();
	}
	$datosFactura = $datosFactura[0];
	
	setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
	$datosFactura['df_emision'] = strftime("%d de %B de %Y",$datosFactura['df_emision']);
	
	$datosFactura['detalle'] = $db->select("(SELECT * FROM tb_nota_credito_detalle WHERE dc_nota_credito={$datosFactura['dc_nota_credito']} AND dm_tipo='{$_GET['v']}') d
	LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
	LEFT JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto",
	'd.dg_producto,d.dc_cantidad,d.dg_descripcion,d.dq_precio,d.dq_descuento,d.dq_total,t.dm_controla_inventario');
	
	$neto = 0;
	foreach($datosFactura['detalle'] as $i => $v){
		if($v['dm_controla_inventario'] != 0)
			$datosFactura['detalle'][$i]['dg_producto'] = '';
			
		$neto += $v['dq_precio']*$v['dc_cantidad'];
		
	}
	//$datosFactura['dq_neto'] = $neto;
	//$datosFactura['dq_iva'] = $neto*0.19;
	//$datosFactura['dq_total'] = $datosFactura['dq_neto']+$datosFactura['dq_iva'];
	
	$pdf = new PDF();
	$pdf->AddDetalle($datosFactura['detalle']);
	$pdf->Output();
	
	
?>