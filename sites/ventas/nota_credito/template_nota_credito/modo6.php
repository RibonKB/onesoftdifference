<?php
require("../../../inc/fpdf.php");
require_once("Numbers/Words.php");

class PDF extends FPDF{

private $datosEmpresa;
private $margenY = 4.0;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','cm',array(21.5,28.2));
	
}

function Header(){
	global $datosFactura;
	
	//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
	//$this->SetXY(x,y);
	//MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
	
	$this->SetFont('Arial','',12);
	$this->SetTextColor(0,0,0);
	
	$this->SetXY(12.5,$this->margenY);//4.2);
	$this->Cell(8,0.5,$datosFactura['dq_nota_credito'],0,0,'C');
	
	$datosFactura['dg_giro'] = substr($datosFactura['dg_giro'],0,35);
	
	$this->SetFont('Arial','',9);
	$this->SetXY(3,$this->margenY+2.2);
	$this->MultiCell(10,0.44,
	"{$datosFactura['dg_razon']}\n{$datosFactura['dg_direccion']}\n{$datosFactura['dg_region']}\n{$datosFactura['dg_rut']}\n{$datosFactura['dg_giro']}\n".
	"{$datosFactura['dg_contacto_entrega']}\n{$datosFactura['dg_direccion_entrega']} {$datosFactura['dg_comuna_entrega']} {$datosFactura['dg_region_entrega']}");
	
	$this->setXY(9,3.5+$this->margenY);
	$this->MultiCell(4,0.44,"{$datosFactura['dg_comuna']}\n{$datosFactura['dg_fono']}");
	
	$this->SetXY(1.3,7.1+$this->margenY);
	$this->Cell(3.8,0.5,$datosFactura['dg_medio_pago'],0,0,'C');
	
	$this->SetXY(4.7,7.1+$this->margenY);
	$this->Cell(3,0.5,$datosFactura['dg_tipo_cargo'],0,0,'C');
	
	$this->SetXY(7.7,5.6+$this->margenY);
	$this->Cell(5.2,0.5,''/*$datosFactura['dg_tipo_nota_credito']*/,0,0,'C');
	
	$this->SetXY(16.5,4.5+$this->margenY);
	$datosFactura['dg_ejecutivo'] = substr($datosFactura['dg_ejecutivo'],0,20);
	$this->MultiCell(4,0.5,"{$datosFactura['dg_ejecutivo']}\n{$datosFactura['df_emision']}\n{$datosFactura['df_vencimiento']}");
	
	$this->SetXY(16,5.7+$this->margenY);
	$this->MultiCell(4.5,0.5,"{$datosFactura['dg_orden_compra']}\n{$datosFactura['dq_nota_venta']}{$datosFactura['dq_orden_servicio']}");
	
}

function Footer(){
	global $datosFactura;
	
	$nw = new Numbers_Words();
	$this->SetXY(1.7,18.7+$this->margenY);
	$this->Cell(14.5,0.6,ucfirst($nw->toWords($datosFactura['dq_total'],"es")));
	
	$this->SetFont('Arial','',12);
	$this->SetXY(18.3,18.7+$this->margenY);
	$this->Cell(2.5,0.83,moneda_local($datosFactura['dq_neto']),0,2,'R');
	$this->Cell(2.5,0.83,moneda_local($datosFactura['dq_iva']),0,2,'R');
	$this->Cell(2.5,0.83,moneda_local($datosFactura['dq_total']),0,2,'R');
}

function AddDetalle($detalle){
	global $datosFactura,$empresa_conf;
	
	$this->AddPage();
	$this->SetFont('Arial','',9);
	
	$this->SetY(9.4+$this->margenY);
	foreach($detalle as $i => $det){
		$det['dq_precio'] = moneda_local($det['dq_precio']);
		$det['dq_descuento'] = moneda_local($det['dq_descuento']);
		$det['dq_total'] = moneda_local($det['dq_total']);
		
		$this->SetX(1);
		$this->Cell(2.5,0.476,$det['dg_producto']);
		$this->Cell(1,0.476,$det['dc_cantidad']);
		$this->Cell(7.2,0.476,substr($det['dg_descripcion'],0,60));
		$this->Cell(3.3,0.476,'');
		
		$this->SetFont('Arial','',12);
		$this->Cell(2.4,0.476,$det['dq_precio'],0,0,'R');
		$this->Cell(1.1,0.476,$det['dq_descuento'],0,0,'R');
		$this->Cell(2.4,0.476,$det['dq_total'],0,0,'R');
		
		$this->SetFont('Arial','',9);
		$this->Ln();
		
	}
	
	$y = $this->GetY();
	if($datosFactura['dg_comentario']){
		$this->Ln();
		$this->Cell(116,3,'                  '.$datosFactura['dg_comentario']);
	}
	
}

}
?>