<?php
define('MAIN',1);
require_once('../../../inc/Factory.class.php');

$request = Factory::getRequest();

$fname= $request->factory.'Factory';
$action = $request->action.'Action';

unset($request->factory,$request->action);

require_once(__DIR__.'/'.$fname.'.php');
$factory = new $fname();

$factory->$action();

?>