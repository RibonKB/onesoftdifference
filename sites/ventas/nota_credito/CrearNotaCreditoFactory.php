<?php

/**
 * Description of CrearNotaCreditoFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 06-05-2013
 */
class CrearNotaCreditoFactory extends Factory{

    private $dq_folio;
    private $tipo_nc;
    private $tipoMovimientoLogisticoEntrada;
    private $dq_nota_credito;
    private $dc_nota_credito;
    private $dc_producto;
    private $dq_costo_stock;
    private $wStock;
    private $dq_cantidad_gd;
	private $tipoImpresion;

  public function procesarFormularioAction(){
		$this->validarNotaCredito();
        $this->getConnection()->start_transaction();
            $this->insertarNotaCredito();
            $this->insertarNotaCreditoDetalle();
            $this->initIdProductos();
            $this->rebajarDetallesNotaVenta();
            $this->cargarStockEnBodega();
            $this->liberarOrdenesServicio();
            $this->liberarGuiasDespacho();
        //$this->AnularLetra();
        $this->getConnection()->commit();

        echo $this->getView($this->getTemplateURL('datosCreacion'), array(
            'dc_nota_credito' => $this->dc_nota_credito,
            'dq_nota_credito' => $this->dq_nota_credito,
            'dq_folio' => $this->dq_folio
        ));
    }

    public function obtenerRelacionesFacturaAction(){
        $dc_factura = self::getRequest()->dc_factura;

        $this->validaFacturaLiberable($dc_factura);

        $guias_despacho = $this->obtenerGuiasDespachoLiberables($dc_factura);
        $ordenes_servicio = $this->obtenerOrdenesServicioLiberables($dc_factura);

        echo $this->getFormView($this->getTemplateURL('relacionesFactura.form'), array(
            'guias_despacho' => $guias_despacho,
            'ordenes_servicio' => $ordenes_servicio
        ));
    }

        private function validaFacturaLiberable($dc_factura){
            $factura = $this->getConnection()->getRowById('tb_factura_venta', $dc_factura, 'dc_factura');
            if($factura->dc_nota_venta != 0){
                exit;
            }
        }

        private function obtenerGuiasDespachoLiberables($dc_factura){
            $df_emision = trim(self::getRequest()->df_emision);

            $db = $this->getConnection();
            $guias = $db->prepare(
                        $db->select('tb_guia_despacho gd
                                      JOIN tb_tipo_guia_despacho t ON t.dc_tipo = gd.dc_tipo_guia',
                                    'gd.dc_guia_despacho, gd.dq_guia_despacho, gd.dq_folio',
                                    't.dm_facturable = 1 AND
                                     gd.dq_folio > 0 AND
                                     gd.dc_factura = ? AND
                                     MONTH(gd.df_emision) = MONTH(?) AND
                                     YEAR(gd.df_emision) = YEAR(?)'));
            $guias->bindValue(1, $dc_factura, PDO::PARAM_INT);

            if(!$df_emision)
                $df_emision = date('c');

            $guias->bindValue(2, $db->sqlDate2($df_emision), PDO::PARAM_STR);
            $guias->bindValue(3, $db->sqlDate2($df_emision), PDO::PARAM_STR);

            $db->stExec($guias);

            $return = array();
            while($g = $guias->fetch(PDO::FETCH_OBJ)){
                $return[$g->dc_guia_despacho] = $g->dq_guia_despacho.' folio: '.$g->dq_folio;
            }

            return $return;

        }

        private function obtenerOrdenesServicioLiberables($dc_factura){
            $db = $this->getConnection();
            $ordenes = $db->prepare(
                          $db->select('tb_orden_servicio',
                                      'dc_orden_servicio, dq_orden_servicio',
                                      'dm_facturable = 1 AND dc_factura = ?'));
            $ordenes->bindValue(1, $dc_factura, PDO::PARAM_INT);
            $db->stExec($ordenes);

            $return = array();
            while($o = $ordenes->fetch(PDO::FETCH_OBJ)){
                $return[$o->dc_orden_servicio] = $o->dq_orden_servicio;
            }

            return $return;

        }

  private function validarNotaCredito(){
        $this->validarFolio();
		$this->validarModoImpresion();
		$this->validarModoGlosa();
        $this->validarTipoNotaCredito();
        $this->validarFacturaExistencias();
        $this->validarBodegaEntrada();
        $this->validarTipoMovimientoLogistico();
        $this->validaNotaVentaConRetorno();
        $this->validaTotalOrdenesServicioLiberadas();
        $this->validaPeriodoGuiasDespachoLiberadas();
    }

    private function validarFolio(){
        $db = $this->getConnection();
        $request = self::getRequest();

        $this->dq_folio = intval($request->nc_number);

        if(!$this->dq_folio){
            $this->getErrorMan()->showWarning("El folio de la factura debe ser un número válido");
            exit;
        }

        $valida_numero = $db->prepare($db->select('tb_nota_credito','1',"dq_folio = ? AND dc_empresa = ? AND dm_nula = 0"));
        $valida_numero->bindValue(1, $this->dq_folio, PDO::PARAM_INT);
        $valida_numero->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($valida_numero);

        if($valida_numero->fetch() !== false){
            $this->getErrorMan()->showWarning("El número de nota de crédito <b>{$_POST['nc_number']}</b> es inválido pues ya existe una nota de crédito vigente con dicho número.");
            exit;
        }
    }

	private function validarModoImpresion(){
		$r = self::getRequest();
		$this->tipoImpresion = intval($r->fv_modo_impresion);
		if($this->tipoImpresion > 1){
			$this->getErrorMan()->showWarning("El tipo de impresión es erróneo. Por favor contacte al administrador.");
            exit;
		}
	}

	private function validarModoGlosa(){
		$r = self::getRequest();
		if($this->tipoImpresion == 0 && !isset($r->fake_costo)){
			$this->getErrorMan()->showWarning("El tipo de impresión debe concordar con los detalles. Ingrese el detalle de glosa o cambie el modo de impresión");
            exit;
		}
	}

    private function validarTipoNotaCredito(){
        $db = $this->getConnection();
        $request = self::getRequest();

        $r_tipo = explode('|',$request->nc_tipo);
        $tipo = $db->getRowById('tb_tipo_nota_credito', $r_tipo[0], 'dc_tipo_nota_credito');
        if(
                $tipo->dm_retorna_stock  != $r_tipo[1] ||
                $tipo->dc_bodega_entrada != $r_tipo[3]
          )
        {
            $this->getErrorMan()->showWarning('La configuración del tipo de Nota de crédito cambió mientras se intentaba crear la Nota de crédito, consulte con un administrador.');
            exit;
        }

        $this->tipo_nc = $tipo;

    }

    private function validarFacturaExistencias(){
        $request = self::getRequest();
        if($this->tipo_nc->dm_retorna_stock && $request->nc_id_factura_venta == 0){
            $this->getErrorMan()->showWarning("El tipo de Nota de crédito no debería mover existencias si no selecciona una Factura de Venta");
            exit;
        }
    }

    private function validarBodegaEntrada(){
        if($this->tipo_nc->dm_retorna_stock){
            if($this->tipo_nc->dc_bodega_entrada == 0){
                $this->getErrorMan()->showWarning("No se ha configurado una bodega de entrada para el tipo de nota de crédito, consulte con un administrador.");
                exit;
            }
        }
    }

    private function validarTipoMovimientoLogistico(){
        if($this->tipo_nc->dm_retorna_stock){
          $c = $this->getServicioLogistica()->getConfiguration();
          $this->tipoMovimientoLogisticoEntrada = $c->dc_tipo_movimiento_entrada_nota_credito;

          if($this->tipoMovimientoLogisticoEntrada == 0){
            $this->getErrorMan()->showWarning("No se ha configurado un tipo de movimiento para esta operación, no puede realizar movimientos de stock hasta que no se configure esta opción.");
            exit;
          }
        }
    }

    private function validaNotaVentaConRetorno(){
        $request = self::getRequest();
        $dc_nota_venta = $request->fv_id_nota_venta;

        if($dc_nota_venta == 0){
            return true;
        }

        if($this->tipo_nc->dm_retorna_anticipado){
            $to_validate = array('dc_facturada');
        }else if($this->tipo_nc->dm_retorna_stock){
            $to_validate = array('dc_facturada','dc_despachada');
        }else{
            return true;
        }

        $detalle_nv = $this->getNotaVentaDetalle();

        foreach($request->dc_detalle_nota_venta as $i => $dc_detalle_nv){
            $dc_detalle_nv = intval($dc_detalle_nv);
            $dq_cantidad = intval($request->cant[$i]);

			if(!$dc_detalle_nv)
				continue;

            foreach($to_validate as $campo){
                if($detalle_nv[$dc_detalle_nv]->$campo < $dq_cantidad){
                    $this->getErrorMan()->showWarning('No se puede procesar la Nota de Crédito pues las cantidades en la Nota de Venta no se pueden disminuir para la refacturación');
                    if(!$this->tipo_nc->dm_retorna_anticipado){
                        $this->getErrorMan()->showAviso('Verifique que la Factura de Venta seleccionada no sea anticipada, o corrija el tipo de Nota de Crédito');
                    }
                    exit;
                }
            }

        }

        return true;

    }

    private function validaTotalOrdenesServicioLiberadas(){
        $request = self::getRequest();

        if(!isset($request->dc_orden_servicio_liberar)){
            return;
        }

        $db = $this->getConnection();

        $questions = substr(str_repeat(',?', count($request->dc_orden_servicio_liberar)),1);
        $query = $db->prepare($db->select('tb_orden_servicio_factura_detalle',
                              'SUM(dq_precio_venta*dq_cantidad)',
                              "dc_orden_servicio IN ({$questions})"));
        foreach($request->dc_orden_servicio_liberar as $i => $dc_orden_servicio){
             $query->bindValue($i+1, $dc_orden_servicio, PDO::PARAM_INT);
        }
        $db->stExec($query);

        $decimales = $this->getParametrosEmpresa()->dn_decimales_local;

        if(round(floatval($request->cot_neto),$decimales) != round(floatval($query->fetchColumn()),$decimales)){
            $this->getErrorMan()->showWarning("El total de la nota de crédito debe coincidir con los totales en los detalles de facturación de las ordenes de servicio");
            exit;
        }

    }

    private function validaPeriodoGuiasDespachoLiberadas(){
        $request = self::getRequest();

        if(!isset($request->dc_guia_despacho_liberar)){
          return;
        }

        $db = $this->getConnection();
        $df_emision = $db->sqlDate2($request->fv_emision);

        $questions = substr(str_repeat(',?', count($request->dc_guia_despacho_liberar)),1);
        $query = $db->prepare(
                  $db->select(
                          'tb_guia_despacho',
                          'true',
                          "(MONTH(df_emision) != MONTH(?)
                            OR
                            YEAR(df_emision) != YEAR(?)) AND
                           dc_guia_despacho IN ({$questions})"));
        $query->bindValue(1, $df_emision, PDO::PARAM_STR);
        $query->bindValue(2, $df_emision, PDO::PARAM_STR);

        foreach($request->dc_guia_despacho_liberar as $i => $dc_guia_despacho){
            $query->bindValue($i+3, $dc_guia_despacho, PDO::PARAM_INT);
        }

        $db->stExec($query);

        if(count($query->fetchAll())){
            $this->getErrorMan()->showWarning('No puede liberar guías de despacho de periodos distintos al de la nota de crédito.');
            exit;
        }
    }

    private function getNotaVentaDetalle(){
        $db = $this->getConnection();
        $request = self::getRequest();

        $questions = substr(str_repeat(',?', count($request->dc_detalle_nota_venta)),1);

        $detalle = $db->prepare(
                    $db->select('tb_nota_venta_detalle',
                                'dc_nota_venta_detalle,dc_producto,dc_recepcionada,dc_despachada,dc_facturada,dc_comprada',
                                "dc_nota_venta_detalle IN ({$questions})"));
        foreach($request->dc_detalle_nota_venta as $i => $dc_detalle){
            $detalle->bindValue($i+1, $dc_detalle, PDO::PARAM_INT);
        }
        $db->stExec($detalle);

        $grouped = array();

        while($d = $detalle->fetch(PDO::FETCH_OBJ)){
            $grouped[$d->dc_nota_venta_detalle] = $d;
        }

        return $grouped;
    }

    private function insertarNotaCredito(){
        $db = $this->getConnection();
        $this->dq_nota_credito = $this->getServicioDocumento()->getCorrelativo('tb_nota_credito','dq_nota_credito',2,'df_creacion');
        $request = self::getRequest();
        $nota_credito = $db->prepare($db->insert('tb_nota_credito',array(
            'dc_nota_venta' => '?',
            'dc_orden_servicio' => '?',
            'dc_factura' => '?',
            'dc_ejecutivo' => '?',
            'dc_cliente' => '?',
            'dc_contacto' => '?',
            'dc_contacto_entrega' => '?',
            'dc_medio_pago' => '?',
            'dc_tipo_cargo' => '?',
            'dc_tipo_nota_credito' => $this->tipo_nc->dc_tipo_nota_credito,
            'dq_nota_credito' => $this->dq_nota_credito,
            'dq_folio' => '?',
            'dg_comentario' => '?',
            'df_emision' => '?',
            'df_creacion' => $db->getNow(),
            'df_vencimiento' => '?',
            'dg_orden_compra' => '?',
            'dq_neto' => '?',
            'dq_iva' => '?',
            'dq_total' => '?',
            'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
            'dc_empresa' => $this->getEmpresa(),
			      'dm_tipo_impresion' => '?'
        )));
        $nota_credito->bindValue(1, $request->fv_id_nota_venta, PDO::PARAM_INT);
        $nota_credito->bindValue(2, $request->fv_id_orden_servicio, PDO::PARAM_INT);
        $nota_credito->bindValue(3, $request->nc_id_factura_venta, PDO::PARAM_INT);
        $nota_credito->bindValue(4, $request->fv_ejecutivo, PDO::PARAM_INT);
        $nota_credito->bindValue(5, $request->cli_id, PDO::PARAM_INT);
        $nota_credito->bindValue(6, $request->fv_contacto, PDO::PARAM_INT);
        $nota_credito->bindValue(7, $request->fv_cont_entrega, PDO::PARAM_INT);
        $nota_credito->bindValue(8, $request->fv_medio_pago, PDO::PARAM_INT);
        $nota_credito->bindValue(9, $request->fv_tipo_cargo, PDO::PARAM_INT);
        $nota_credito->bindValue(10, $this->dq_folio, PDO::PARAM_STR);
        $nota_credito->bindValue(11, $request->fv_comentario, PDO::PARAM_STR);
        $nota_credito->bindValue(12, $db->sqlDate2($request->fv_emision), PDO::PARAM_STR);
        $nota_credito->bindValue(13, $db->sqlDate2($request->fv_vencimiento), PDO::PARAM_STR);
        $nota_credito->bindValue(14, $request->fv_orden_compra, PDO::PARAM_STR);
        $decimales = $this->getParametrosEmpresa()->dn_decimales_local;
        $nota_credito->bindValue(15, round(floatval($request->cot_neto),$decimales), PDO::PARAM_STR);
        $nota_credito->bindValue(16, round(floatval($request->cot_iva),$decimales), PDO::PARAM_STR);
        $nota_credito->bindValue(17, round(floatval($request->cot_neto)+floatval($request->cot_iva),$decimales), PDO::PARAM_STR);
		    $nota_credito->bindValue(18, $this->tipoImpresion, PDO::PARAM_STR);

        $db->stExec($nota_credito);

        $this->dc_nota_credito = $db->lastInsertId();
    }

    private function insertarNotaCreditoDetalle(){

        $this->initFunctionsService();
        $db = $this->getConnection();
        $request = self::getRequest();

        $det_st = $db->prepare($db->insert('tb_nota_credito_detalle',array(
            'dc_nota_credito' => $this->dc_nota_credito,
            'dg_producto' => '?',
            'dg_descripcion' => '?',
            'dm_tipo' => '?',
            'dc_cantidad' => '?',
            'dq_precio' => '?',
            'dq_descuento' => '?',
            'dq_total' => '?',
            'dq_costo' => '?',
            'dc_cuenta_contable_centralizacion' => '?'
        )));

        $det_st->bindParam(1,$producto,PDO::PARAM_STR);
        $det_st->bindParam(2,$descripcion,PDO::PARAM_STR);
        $det_st->bindParam(3,$tipo,PDO::PARAM_STR);
        $det_st->bindParam(4,$cantidad,PDO::PARAM_INT);
        $det_st->bindParam(5,$precio,PDO::PARAM_STR);
        $det_st->bindParam(6,$descuento,PDO::PARAM_STR);
        $det_st->bindParam(7,$total,PDO::PARAM_STR);
        $det_st->bindParam(8,$costo,PDO::PARAM_STR);
        if($this->tipo_nc->dm_retorna_stock == 0):
          $det_st->bindParam(9, $dc_cuenta_contable_centralizacion, PDO::PARAM_INT);
        else:
          $det_st->bindValue(9, null, PDO::PARAM_NULL);
        endif;

        $tipo = 0;
        foreach($request->prod as $i => $v){

            $producto = $v;
            $descripcion = $request->desc[$i];
            $cantidad = intval($request->cant[$i]);
            $precio = Functions::toNumber($request->precio[$i]);
            $costo = Functions::toNumber($request->costo[$i]);
            $descuento = Functions::toNumber($request->descuento[$i]);
            $total = $precio*$cantidad-$descuento;

            if($this->tipo_nc->dm_retorna_stock == 0):
              $dc_cuenta_contable_centralizacion = $request->dc_cuenta_contable[$i];
            endif;

            $db->stExec($det_st);
        }

        if(isset($request->fake_prod)){
            $tipo = 1;
            foreach($request->fake_prod as $i => $v){
                $producto = $v;
                $descripcion = $request->fake_desc[$i];
                $cantidad = intval($request->fake_cant[$i]);
                $precio = Functions::toNumber($request->fake_precio[$i]);
                $costo = Functions::toNumber($request->fake_costo[$i]);
                $descuento = Functions::toNumber($request->fake_descuento[$i]);
                $total = $precio*$cantidad-$descuento;

                $db->stExec($det_st);
            }
        }

    }

    private function initIdProductos(){
        $request = self::getRequest();
        $db = $this->getConnection();

        $questions = substr(str_repeat(',?', count($request->prod)),1);
        $productos = $db->prepare($db->select('tb_producto p
          JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto',
        'p.dc_producto, p.dg_codigo ,tp.dm_controla_inventario, p.dq_precio_compra',
        "p.dg_codigo IN ({$questions}) AND p.dc_empresa = ".$this->getEmpresa()));
        foreach($request->prod as $i => $dg_codigo){
            $productos->bindValue($i+1, $dg_codigo, PDO::PARAM_STR);
        }
        $db->stExec($productos);

        $id_producto = array();
        $dq_costo_stock = array();
        $wStock = array();

        while($p = $productos->fetch(PDO::FETCH_OBJ)){
            $id_producto[$p->dg_codigo] = $p->dc_producto;

            if($p->dm_controla_inventario == 0){
                $wStock[$p->dg_codigo] = $p->dc_producto;
                $dq_costo_stock[$p->dc_producto] = $p->dq_precio_compra;
            }

        }

        $this->dc_producto = $id_producto;
        $this->dq_costo_stock = $dq_costo_stock;
        $this->wStock = $wStock;
    }

    private function AnularLetra() {
        $request = self::getRequest();
        if ($request->nc_id_factura_venta != 0) {
            $verifica = $this->coneccion('select', 'tb_nota_venta_contrato_detalle', 'dc_detalle', "dc_factura={$request->nc_id_factura_venta}");
            $verificar = $verificar->fetch(PDO::FETCH_OBJ);
            if ($verificar != false) {
                $update = $this->coneccion('update', 'tb_nota_venta_contrato_detalle', array(
                    'dc_factura' => NULL
                        ), "dc_detalle={$verificar->dc_detalle}");
            }
        }
    }

    private function rebajarDetallesNotaVenta() {

        if (self::getRequest()->fv_id_nota_venta == 0) {
            return;
        }

        if($this->tipo_nc->dm_retorna_anticipado){
            $this->rebajarDetallesNotaVentaAnticipada();
        }else if($this->tipo_nc->dm_retorna_stock){
            $this->rebajarDetallesNotaVentaStockReservado();
        }else{
            return;
        }
    }

        private function rebajarDetallesNotaVentaAnticipada(){
            $db = $this->getConnection();
            $request = self::getRequest();

            $cantidad_gd = $this->getCantidadesGuiasPorFactura($request->nc_id_factura_venta);

            $update_detalle = $db->prepare($db->update('tb_nota_venta_detalle', array(
                'dc_facturada' => 'dc_facturada-?',
                'dc_despachada' => 'dc_despachada-?'
            ), 'dc_nota_venta_detalle = ?'));
            $update_detalle->bindParam(1, $dc_facturada, PDO::PARAM_INT);
            $update_detalle->bindParam(2, $dc_despachada, PDO::PARAM_INT);
            $update_detalle->bindParam(3, $dc_detalle, PDO::PARAM_INT);

            foreach($request->dc_detalle_nota_venta as $i => $dc_detalle){
                $dc_facturada = intval($request->cant[$i]);

        				if(!$dc_detalle)
        					continue;

                if(isset($cantidad_gd[$dc_detalle])){
                    $dc_despachada = $cantidad_gd[$dc_detalle];
                    if($dc_despachada > $dc_facturada){
                        $dc_despachada = $dc_facturada;
                    }
                }else{
                    $dc_despachada = 0;
                }

                $db->stExec($update_detalle);
            }

        }

            private function getCantidadesGuiasPorFactura($dc_factura){

                if(is_array($this->dq_cantidad_gd[$dc_factura])){
                    return $this->dq_cantidad_gd[$dc_factura];
                }

                $db = $this->getConnection();

                $guias = $db->prepare(
                            $db->select('tb_guia_despacho_detalle d
                                         JOIN tb_guia_despacho gd ON gd.dc_guia_despacho = d.dc_guia_despacho',
                                        'd.dc_detalle_nota_venta, SUM(d.dq_cantidad) dq_cantidad',
                                        'gd.dc_factura = ? AND gd.dm_nula = 0',
                                        array(
                                            'group_by' => 'd.dc_detalle_nota_venta'
                                        )));
                $guias->bindValue(1, $dc_factura, PDO::PARAM_INT);
                $db->stExec($guias);

                $cantidad = array();
                while($c = $guias->fetch(PDO::FETCH_OBJ)){
                    $cantidad[$c->dc_detalle_nota_venta] = intval($c->dq_cantidad);
                }

                $this->dq_cantidad_gd[$dc_factura] = $cantidad;

                return $cantidad;

            }

        private function rebajarDetallesNotaVentaStockReservado(){
            $db = $this->getConnection();
            $request = self::getRequest();

            $update_detalle = $db->prepare($db->update('tb_nota_venta_detalle', array(
                'dc_facturada' => 'dc_facturada-?',
                'dc_despachada' => 'dc_despachada-?'
            ), 'dc_nota_venta_detalle = ?'));
            $update_detalle->bindParam(1, $dq_cantidad, PDO::PARAM_INT);
            $update_detalle->bindParam(2, $dq_cantidad, PDO::PARAM_INT);
            $update_detalle->bindParam(3, $dc_detalle, PDO::PARAM_INT);
            foreach($request->dc_detalle_nota_venta as $i => $dc_detalle){
                $dq_cantidad = intval($request->cant[$i]);

      				if(!$dc_detalle)
      					continue;

                      $db->stExec($update_detalle);
                  }
              }

    private function cargarStockEnBodega(){
        if(self::getRequest()->fv_id_nota_venta == 0){
            return;
        }

        if($this->tipo_nc->dm_retorna_anticipado){
            $this->cargarStockAnticipado();
        }else if($this->tipo_nc->dm_retorna_stock){
            $this->cargarStockReservado();
        }else{
            return;
        }
    }

        private function cargarStockAnticipado(){
            $request = self::getRequest();

            $cantidad_gd = $this->getCantidadesGuiasPorFactura($request->nc_id_factura_venta);
            $servicio = $this->getServicioLogistica();

            foreach($request->dc_detalle_nota_venta as $i => $dc_detalle){

    				if(!$dc_detalle)
    					continue;

                if(!isset($this->wStock[$request->prod[$i]])){
                    continue;
                }

                $dc_facturada = intval($request->cant[$i]);

                if(isset($cantidad_gd[$dc_detalle])){
                    $dc_despachada = $cantidad_gd[$dc_detalle];
                    if($dc_despachada > $dc_facturada){
                        $dc_despachada = $dc_facturada;
                    }

                    $servicio->cargarStockNV($this->dc_producto[$request->prod[$i]], $dc_despachada, $this->tipo_nc->dc_bodega_entrada);
                    $this->insertarMovimientoBodega($this->dc_producto[$request->prod[$i]], $dc_despachada);

                }
            }
        }

        private function cargarStockReservado(){
            $request = self::getRequest();

            $servicio = $this->getServicioLogistica();

            foreach($request->dc_detalle_nota_venta as $i => $dc_detalle){

      				if(!$dc_detalle)
      					continue;

                if(!isset($this->wStock[$request->prod[$i]])){
                    continue;
                }

                $dq_cantidad = intval($request->cant[$i]);
                $dc_producto = $this->dc_producto[$request->prod[$i]];

                $servicio->cargarStockNV($dc_producto, $dq_cantidad, $this->tipo_nc->dc_bodega_entrada);
                $this->insertarMovimientoBodega($dc_producto, $dq_cantidad);

            }
        }

            private function insertarMovimientoBodega($dc_producto, $dq_cantidad){
                $db = $this->getConnection();
                $request = self::getRequest();

                $movimiento = $db->prepare($db->insert('tb_movimiento_bodega',array(
                    "dc_cliente" => '?',
                    "dc_tipo_movimiento" => '?',
                    "dc_bodega_entrada" => '?',
                    "dq_monto" => '?',
                    "dq_cambio" => 1,
                    "dc_factura" => '?',
                    "dc_nota_credito" => $this->dc_nota_credito,
                    "dc_nota_venta" => '?',
                    "dc_orden_servicio" => '?',
                    "dc_producto" => '?',
                    "dq_cantidad" => '?',
                    "dc_empresa" => $this->getEmpresa(),
                    "df_creacion" => $db->getNow(),
                    "dc_usuario_creacion" => $this->getUserData()->dc_usuario
                )));
                $movimiento->bindValue(1, $request->cli_id, PDO::PARAM_INT);
                $movimiento->bindValue(2, $this->tipoMovimientoLogisticoEntrada, PDO::PARAM_INT);
                $movimiento->bindValue(3, $this->tipo_nc->dc_bodega_entrada, PDO::PARAM_INT);
                $movimiento->bindValue(4, floatval($this->dq_costo_stock[$dc_producto]), PDO::PARAM_STR);
                $movimiento->bindValue(5, $request->nc_id_factura_venta, PDO::PARAM_INT);
                $movimiento->bindValue(6, $request->fv_id_nota_venta, PDO::PARAM_INT);
                $movimiento->bindValue(7, $request->fv_id_orden_servicio, PDO::PARAM_INT);
                $movimiento->bindValue(8, $dc_producto, PDO::PARAM_INT);
                $movimiento->bindValue(9, $dq_cantidad, PDO::PARAM_INT);

                $db->stExec($movimiento);

            }

    private function liberarOrdenesServicio(){

        if(!isset(self::getRequest()->dc_orden_servicio_liberar)){
            return;
        }

        $this->persistLiberarOrdenesServicio();
        $this->registraOrdenesServicioLiberadas();
    }

        private function persistLiberarOrdenesServicio(){
            $request = self::getRequest();
            $db = $this->getConnection();

            $questions = substr(str_repeat(',?', count($request->dc_orden_servicio_liberar)),1);
            $liberar = $db->prepare($db->update('tb_orden_servicio', array(
                'dc_factura' => 0
            ), "dc_orden_servicio IN ({$questions})"));
            foreach($request->dc_orden_servicio_liberar as $i => $dc_orden_servicio){
                $liberar->bindValue($i+1, $dc_orden_servicio, PDO::PARAM_INT);
            }

            $db->stExec($liberar);

        }

        private function registraOrdenesServicioLiberadas(){
            $request = self::getRequest();
            $db = $this->getConnection();

            $insert = $db->prepare($db->insert('tb_nota_credito_orden_servicio_liberada', array(
                'dc_nota_credito' => $this->dc_nota_credito,
                'dc_orden_servicio' => '?'
            )));
            $insert->bindParam(1, $dc_orden_servicio, PDO::PARAM_INT);

            foreach($request->dc_orden_servicio_liberar as $dc_orden_servicio){
                $db->stExec($insert);
            }

        }

    private function liberarGuiasDespacho(){

        if(!isset(self::getRequest()->dc_guia_despacho_liberar)){
          return;
        }

        $this->persisLiberarGuiasDespacho();
        $this->registraGuiasDespachoLiberadas();


    }

        private function persisLiberarGuiasDespacho(){
            $db = $this->getConnection();

            $update = $db->prepare($db->update('', array(
                'dc_factura' => 0
            ), 'dc_guia_despacho = ?'));
            $update->bindParam(1, $dc_guia_despacho, PDO::PARAM_INT);

            foreach(self::getRequest()->dc_guia_despacho_liberar as $dc_guia_despacho){
                $db->stExec($update);
            }

        }

        private function registraGuiasDespachoLiberadas(){
            $request = self::getRequest();
            $db = $this->getConnection();

            $insert = $db->prepare($db->insert('tb_nota_credito_guia_despacho_liberada', array(
                'dc_nota_credito' => $this->dc_nota_credito,
                'dc_guia_despacho' => '?'
            )));
            $insert->bindParam(1, $dc_guia_despacho, PDO::PARAM_INT);

            foreach($request->dc_guia_despacho_liberar as $dc_guia_despacho){
                $db->stExec($insert);
            }
        }

    /**
     * @return LogisticaStuffService Servicio de utilidades logísticas
     */
    private function getServicioLogistica(){
        return $this->getService('LogisticaStuff');
    }

    /**
     * @return DocumentCreationService Servicio de utilidades para crear documentos
     */
    private function getServicioDocumento(){
        return $this->getService('DocumentCreation');
    }

}
