<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once('../../proc/ventas_functions.php');

if(!is_numeric($_POST['nc_number'])){
	$error_man->showWarning("El folio de la factura debe ser un número válido");
	exit;
}

$valida_numero = $db->doQuery($db->select('tb_nota_credito','1',"dq_folio={$_POST['nc_number']} AND dc_empresa={$empresa} AND dm_nula = '0'"))->fetch();

if($valida_numero !== false){
	$error_man->showWarning("El número de nota de crédito <b>{$_POST['nc_number']}</b> es inválido pues ya existe una nota de crédito vigente con dicho número.");
	exit();
}

$dc_tipo_nota_credito = explode('|',$_POST['nc_tipo']);
$retorna_stock = $dc_tipo_nota_credito[1] == 1;
$retorna_libre = $dc_tipo_nota_credito[2] == 1;
$dc_bodega = $dc_tipo_nota_credito[3];
$dc_tipo_nota_credito = $dc_tipo_nota_credito[0];

if($retorna_stock && $_POST['nc_id_factura_venta'] == 0){
	$error_man->showWarning("El tipo de Nota de crédito no debería mover existencias si no selecciona una Factura de Venta");
	exit;
}

$db->start_transaction();

$correlativo = doc_GetNextNumber('tb_nota_credito','dq_nota_credito',2,'df_creacion');

$nota_credito = $db->prepare($db->insert('tb_nota_credito',array(
	'dc_nota_venta' => $_POST['fv_id_nota_venta'],
	'dc_orden_servicio' => $_POST['fv_id_orden_servicio'],
	'dc_factura' => $_POST['nc_id_factura_venta'],
	'dc_ejecutivo' => $_POST['fv_ejecutivo'],
	'dc_cliente' => $_POST['cli_id'],
	'dc_contacto' => $_POST['fv_contacto'],
	'dc_contacto_entrega' => $_POST['fv_cont_entrega'],
	'dc_medio_pago' => $_POST['fv_medio_pago'],
	'dc_tipo_cargo' => $_POST['fv_tipo_cargo'],
	'dc_tipo_nota_credito' => $dc_tipo_nota_credito,
	'dq_nota_credito' => $correlativo,
	'dq_folio' => '?',
	'dg_comentario' => '?',
	'df_emision' => $db->sqlDate($_POST['fv_emision']),
	'df_creacion' => $db->getNow(),
	'df_vencimiento' => $db->sqlDate($_POST['fv_vencimiento']),
	'dg_orden_compra' => '?',
	'dq_neto' => $_POST['cot_neto'],
	'dq_iva' => $_POST['cot_iva'],
	'dq_total' => $_POST['cot_neto']+$_POST['cot_iva'],
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
)));

$nota_credito->bindValue(1,$_POST['nc_number'],PDO::PARAM_INT);
$nota_credito->bindValue(2,$_POST['fv_comentario'],PDO::PARAM_STR);
$nota_credito->bindValue(3,$_POST['fv_orden_compra'],PDO::PARAM_STR);

$db->stExec($nota_credito);

$nota_credito = $db->lastInsertId();

$det_st = $db->prepare($db->insert('tb_nota_credito_detalle',array(
	'dc_nota_credito' => $nota_credito,
	'dg_producto' => '?',
	'dg_descripcion' => '?',
	'dm_tipo' => '?',
	'dc_cantidad' => '?',
	'dq_precio' => '?',
	'dq_descuento' => '?',
	'dq_total' => '?',
	'dq_costo' => '?'
)));

$det_st->bindParam(1,$producto,PDO::PARAM_STR);
$det_st->bindParam(2,$descripcion,PDO::PARAM_STR);
$det_st->bindParam(3,$tipo,PDO::PARAM_STR);
$det_st->bindParam(4,$cantidad,PDO::PARAM_INT);
$det_st->bindParam(5,$precio,PDO::PARAM_STR);
$det_st->bindParam(6,$descuento,PDO::PARAM_STR);
$det_st->bindParam(7,$total,PDO::PARAM_STR);
$det_st->bindParam(8,$costo,PDO::PARAM_STR);

$tipo = 0;
foreach($_POST['prod'] as $i => $v){
	
	$producto = $v;
	$descripcion = $_POST['desc'][$i];
	$cantidad = str_replace(',','',$_POST['cant'][$i]);
	$precio = str_replace(',','',$_POST['precio'][$i]);
	$costo = str_replace(',','',$_POST['costo'][$i]);
	$descuento = str_replace(',','',$_POST['descuento'][$i]);
	$total = $precio*$cantidad-$descuento;
	
	$db->stExec($det_st);
}

if(isset($_POST['fake_prod'])){
	$tipo = 1;
	foreach($_POST['fake_prod'] as $i => $v){
		$producto = $v;
		$descripcion = $_POST['fake_desc'][$i];
		$cantidad = str_replace(',','',$_POST['fake_cant'][$i]);
		$precio = str_replace(',','',$_POST['fake_precio'][$i]);
		$descuento = str_replace(',','',$_POST['fake_descuento'][$i]);
		$total = $precio*$cantidad-$descuento;
		
		$db->stExec($det_st);
	}
}//END Almacenar detalle glosa

if($retorna_stock){
	
	require_once('../../proc/ventas_functions.php');
	
	if($dc_bodega == 0){
		$error_man->showWarning("No se ha configurado una bodega de entrada para el tipo de nota de crédito, consulte con un administrador.");
		$db->rollBack();
		exit;
	}
	
	$tipo_movimiento = $db->doQuery($db->select('tb_configuracion_logistica','dc_tipo_movimiento_entrada_nota_credito',"dc_empresa={$empresa}"));
	$tipo_movimiento = $tipo_movimiento->fetch(PDO::FETCH_OBJ)->dc_tipo_movimiento_entrada_nota_credito;
	
	if($tipo_movimiento == 0){
	  $error_man->showWarning("No se ha configurado un tipo de movimiento para esta operación, no puede realizar movimientos de stock hasta que no se configure esta opción.");
	  $db->rollBack();
	  exit;
	}
	
	$dg_codigo = implode("','",$_POST['prod']);
	$productos = $db->doQuery($db->select('tb_producto p
	JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto',
	'p.dc_producto, p.dg_codigo ,tp.dm_controla_inventario, p.dq_precio_compra',
	"p.dg_codigo IN ('{$dg_codigo}') AND p.dc_empresa = {$empresa}"));
	
	$id_producto = array();
	$dq_costo_stock = array();
	$wStock = array();
	
	foreach($productos as $p){
		$id_producto[$p['dg_codigo']] = $p['dc_producto'];
		
		if($p['dm_controla_inventario'] == 0){
			$wStock[$p['dg_codigo']] = $p['dc_producto'];
			$dq_costo_stock[$p['dc_producto']] = $p['dq_precio_compra'];
		}
		
	}
	
	if($_POST['fv_id_nota_venta'] > 0){
		
		$fd_retorna_query = $db->prepare($db->update('tb_nota_venta_detalle',array(
			'dc_facturada' => "dc_facturada-:tofactura"
		),"dc_nota_venta = {$_POST['fv_id_nota_venta']} AND
		dc_producto = :whereproducto AND
		dc_facturada >= :wherefactura"));
		
		$fd_retorna_query->bindParam(':tofactura',$dq_cantidad,PDO::PARAM_INT);
		$fd_retorna_query->bindParam(':whereproducto',$dc_producto,PDO::PARAM_INT);
		$fd_retorna_query->bindParam(':wherefactura',$dq_cantidad,PDO::PARAM_INT);
		
		$to_detail = array(
			'dc_despachada' => "dc_despachada-:todespacho"
		);
		
		if($retorna_libre){
			$to_detail['dc_recepcionada'] = 'dc_recepcionada-:torecepcion';
			$recep_where = "AND dc_recepcionada >= :whererecepcion";
		}else{
			$recep_where = '';
		}
		
		$retorna_query = $db->prepare($db->update('tb_nota_venta_detalle',$to_detail,
		"dc_nota_venta = {$_POST['fv_id_nota_venta']} AND
		 dc_producto = :whereproducto AND
		 dc_despachada >= :wheredespacho {$recep_where}")." LIMIT 1");
		 
		 $retorna_query->bindParam(':todespacho',$dq_cantidad,PDO::PARAM_INT);
		 $retorna_query->bindParam(':whereproducto',$dc_producto,PDO::PARAM_INT);
		 $retorna_query->bindParam(':wheredespacho',$dq_cantidad,PDO::PARAM_INT);
		 
		 if($retorna_libre){
			 $retorna_query->bindParam(':torecepcion',$dq_cantidad,PDO::PARAM_INT);
			 $retorna_query->bindParam(':whererecepcion',$dq_cantidad,PDO::PARAM_INT);
		 }
		
		foreach($_POST['prod'] as $i => $v){
			
			
			
			$dq_cantidad = str_replace(',','',$_POST['cant'][$i]);
			$dc_producto = $id_producto[$v];
			
			$db->stExec($fd_retorna_query);

			if(isset($wStock[$v])){
				
				$db->stExec($retorna_query);

				
				if($retorna_query->rowCount()){
					if($retorna_libre){
						
						bodega_CargarStockLibre($dc_producto,$dq_cantidad,$dc_bodega);
						
					}else{
						
						bodega_CargarStockNV($dc_producto,$dq_cantidad,$dc_bodega);
						
					}
					
					$db->doExec($db->insert('tb_movimiento_bodega',array(
						"dc_cliente" => $_POST['cli_id'],
						"dc_tipo_movimiento" => $tipo_movimiento,
						"dc_bodega_entrada" => $dc_bodega,
						"dq_monto" => $dq_costo_stock[$dc_producto],
						"dq_cambio" => 1,
						"dc_factura" => $_POST['nc_id_factura_venta'],
						"dc_nota_credito" => $nota_credito,
						"dc_nota_venta" => $_POST['fv_id_nota_venta'],
						"dc_orden_servicio" => $_POST['fv_id_orden_servicio'],
						"dc_producto" => $dc_producto,
						"dq_cantidad" => $dq_cantidad,
						"dc_empresa" => $empresa,
						"df_creacion" => $db->getNow(),
						"dc_usuario_creacion" => $idUsuario
					)));
				}
				
			}
			
		}
		
	}else if($_POST['fv_id_orden_servicio'] > 0){
		
		//Rebajar stocks Ordenes de servicio
		$error_man->showWarning('Devolución de stock para ordenes de servicio no implementado');
		$db->rollBack();
		exit;
		
	}
	
}

$db->commit();

$_POST['nc_number'] = htmlentities($_POST['nc_number']);

$error_man->showConfirm("Se ha almacenado la nota de crédito correctamente.");
$error_man->showInfo("El número de folio para nota de crédito física es el <strong>{$_POST['nc_number']}</strong><br />
Además de eso el número de identificación interno es el <h1 style='margin:0;color:#000;'>{$correlativo}</h1>
<button type='button' class='button' id='print_nc'>Imprimir</button>");
?>
<script type="text/javascript">
$('#print_nc').click(function(){
	loadOverlay('sites/ventas/nota_credito/ver_nota_credito.php?id=<?php echo $nota_credito ?>');
});
</script>