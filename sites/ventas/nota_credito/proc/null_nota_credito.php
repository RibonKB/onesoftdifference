<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_nota_credito = intval($_POST['dc_nota_credito']);

$nota_credito = $db->prepare($db->select('tb_nota_credito nc
	JOIN tb_tipo_nota_credito t ON t.dc_tipo_nota_credito = nc.dc_tipo_nota_credito',
'nc.dq_nota_credito, t.dm_retorna_stock, t.dm_retorna_libre, nc.dc_nota_venta, t.dc_bodega_entrada, nc.dc_cliente, nc.dc_factura, t.dm_retorna_anticipado, nc.df_creacion',
'nc.dc_nota_credito = ? AND nc.dc_empresa = ?'));
$nota_credito->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$nota_credito->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_credito);
$nota_credito = $nota_credito->fetch(PDO::FETCH_OBJ);

if($nota_credito === false){
	$error_man->showWarning('La nota de crédito ingresada es inválida, compruebe los datos de entrada y vuelva a intentarlo');
	exit;
}

if($nota_credito->dm_retorna_stock == 1){
	
	$logistica = $db->doQuery(
						$db->select(
							'tb_configuracion_logistica',
							'dc_tipo_movimiento_anula_nota_credito dc_tipo_movimiento',
							'dc_empresa = '.$empresa)
						)->fetch(PDO::FETCH_OBJ);
	
	if($logistica === false || $logistica->dc_tipo_movimiento == 0){
		$error_man->showWarning('No se ha configurado el tipo de movimiento para mover stock al anular nota de credito, consulte con un adminitrador.');
		exit;
	}
	
	if($nota_credito->dc_nota_venta == 0){
		$error_man->showWarning('La nota de crédito con retorno de stock es inválida pues no tiene asignada una nota de venta, consulte con un administrador');
		exit;
	}
	
	$detalle = $db->prepare($db->select("tb_nota_credito_detalle ncd
		JOIN tb_producto p ON p.dg_codigo = ncd.dg_producto AND p.dc_empresa = {$empresa}
		JOIN tb_nota_venta_detalle nvd ON nvd.dc_producto = p.dc_producto",
	'ncd.dc_cantidad cantidad_nc, p.dg_codigo, p.dg_producto, nvd.dq_cantidad cantidad_nv, nvd.dc_facturada, p.dc_producto, p.dq_precio_compra,
	 nvd.dc_despachada, nvd.dc_recepcionada, nvd.dc_comprada, nvd.dc_nota_venta_detalle, nvd.dc_cebe, nvd.dc_ceco, ncd.dq_costo',
	'ncd.dm_tipo = 0 AND p.dc_empresa = ? AND nvd.dc_nota_venta = ? AND ncd.dc_nota_credito = ?'));
	$detalle->bindValue(1,$empresa,PDO::PARAM_INT);
	$detalle->bindValue(2,$nota_credito->dc_nota_venta,PDO::PARAM_INT);
	$detalle->bindValue(3,$dc_nota_credito,PDO::PARAM_INT);
	$db->stExec($detalle);
	$detalle = $detalle->fetchAll(PDO::FETCH_OBJ);
	
	$to_despacho = array();
	
	if($nota_credito->dm_retorna_anticipado == 1){
		$guias_factura = $db->prepare(
                            $db->select('tb_guia_despacho_detalle d
                                         	JOIN tb_guia_despacho gd ON gd.dc_guia_despacho = d.dc_guia_despacho',
                                        'd.dc_detalle_nota_venta, SUM(d.dq_cantidad) dq_cantidad, gd.dq_folio',
                                        'gd.dc_factura = ? AND gd.df_emision < ? AND gd.dm_nula = 0',
                                        array(
                                            'group_by' => 'd.dc_detalle_nota_venta'
                                        )));
         $guias_factura->bindValue(1, $nota_credito->dc_factura, PDO::PARAM_INT);
		 $guias_factura->bindValue(2, $nota_credito->df_creacion, PDO::PARAM_INT);
         $db->stExec($guias_factura);
		 
         while($c = $guias_factura->fetch(PDO::FETCH_OBJ)){
         	$to_despacho[$c->dc_detalle_nota_venta] = intval($c->dq_cantidad);
         }
	}
	
	//Verificar que las cantidades por facturar y/o despachar sean suficientes para la anulación
	foreach($detalle as $d):
	
		if($nota_credito->dm_retorna_anticipado != 1){
			$to_despacho[$d->dc_nota_venta_detalle] = $d->cantidad_nc;
		}else{
			if(!isset($to_despacho[$d->dc_nota_venta_detalle])){
				$to_despacho[$d->dc_nota_venta_detalle] = 0;
			}
		}
		
		if( ($d->cantidad_nv - $d->dc_facturada) < $d->cantidad_nc ){ //si cantidad por facturar menor a cantidad a refacturar
			$error_man->showWarning('No se puede anular la nota de crédito debido a que la nota de venta relacionada posee un detalle que no se puede refacturar.<br />
			El problema fue encontrado en el producto <b>'.$d->dg_codigo.'</b> - '.$d->dg_producto);
			exit;
		}
		
		if( ($d->cantidad_nv - $d->dc_despachada) < $to_despacho[$d->dc_nota_venta_detalle] ){
			$error_man->showWarning('No se puede anular la nota de crédito debido a que la nota de venta relacionada posee un detalle que no se puede restaurar sus despachos
			<br />El problema fue encontrado en el producto <b>'.$d->dg_codigo.'</b> - '.$d->dg_producto);
			exit;
		}
		
		//En caso de retorno de stock libre también se deben verificar las cantidades recepcionadas y compradas
		/*if($nota_credito->dm_retorna_libre == 1){
			if( ($d->cantidad_nv - $d->dc_recepcionada) < $d->cantidad_nc ){
				$error_man->showWarning('No se puede anular la nota de crédito debido a que la nota de venta relacionada se ha recepcionado sobre lo posible de retornar
				<br />El problema fue encontrado en el producto <b>'.$d->dg_codigo.'</b> - '.$d->dg_producto);
				exit;
			}
			
			if( ($d->cantidad_nv - $d->dc_comprada) < $d->cantidad_nc ){
				$error_man->showWarning('No se puede anular la nota de crédito debido a que la nota de venta relacionada se ha comprado sobre lo posible de retornar
				<br />El problema fue encontrado en el producto <b>'.$d->dg_codigo.'</b> - '.$d->dg_producto);
				exit;
			}
		}*/
		
	endforeach;
	
	//Verificar stock en bodega de entrada, el stock devuelto a las bodegas de entrada definidas en el tipo de nc deben estar en las bodegas para vovler a despacharlos.
	include_once('../../proc/ventas_functions.php');
	$bodega = $db->doQuery($db->select('tb_bodega','dg_bodega','dc_bodega = '.$nota_credito->dc_bodega_entrada))->fetch(PDO::FETCH_OBJ);
	foreach($detalle as $d):
		
		$stock = bodega_ComprobarTotalStock($d->dc_producto, $nota_credito->dc_bodega_entrada);
		
		if($nota_credito->dm_retorna_libre == 1){ //Comprobar en stock libre
			if($d->cantidad_nc > $stock[0]){
				$error_man->showWarning('No se puede anular la nota de crédito debido a que no hay stock libre suficiente en la bodega'.$bodega->dg_bodega.
				'<br />El problema fue encontrado en el producto <b>'.$d->dg_codigo.'</b> - '.$d->dg_producto);
				exit;
			}
		}else{ //Comprobar en stock reservado
			if($to_despacho[$d->dc_nota_venta_detalle] > $stock[1]){
				$error_man->showWarning('No se puede anular la nota de crédito debido a que no hay stock reservado suficiente en la bodega'.$bodega->dg_bodega.
				'<br />El problema fue encontrado en el producto <b>'.$d->dg_codigo.'</b> - '.$d->dg_producto);
				exit;
			}
		}
		
	endforeach;
	
}

$db->start_transaction();

$insert_motivo = $db->prepare($db->insert('tb_nota_credito_anulada',array(
	'dc_nota_credito' => '?',
	'dc_motivo' => '?',
	'dg_comentario' => '?',
	'dc_usuario' => $idUsuario,
	'df_anulacion' => $db->getNow(),
	'dm_fisica' => '?'
)));
$insert_motivo->bindValue(1,$dc_nota_credito,PDO::PARAM_INT);
$insert_motivo->bindValue(2,$_POST['null_motivo'],PDO::PARAM_INT);
$insert_motivo->bindValue(3,$_POST['null_comentario'],PDO::PARAM_STR);
$insert_motivo->bindValue(4,$_POST['null_fisica'],PDO::PARAM_STR);
$db->stExec($insert_motivo);

$update_nota_credito = $db->prepare($db->update('tb_nota_credito',array(
	'dm_nula' => '?'
),'dc_nota_credito = ?'));
$update_nota_credito->bindValue(1,'1',PDO::PARAM_STR);
$update_nota_credito->bindValue(2,$dc_nota_credito,PDO::PARAM_INT);
$db->stExec($update_nota_credito);

if($nota_credito->dm_retorna_stock == 1){
	
	$update_nota_venta_detalle = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dc_facturada' => 'dc_facturada + ?',
		'dc_despachada' => 'dc_despachada + ?',
		'dc_recepcionada' => 'dc_recepcionada + ?',
		'dc_comprada' => 'dc_comprada + ?'
	),'dc_nota_venta_detalle = ?'));
	$update_nota_venta_detalle->bindParam(1,$dc_facturada,PDO::PARAM_INT);
	$update_nota_venta_detalle->bindParam(2,$dc_despachada,PDO::PARAM_INT);
	$update_nota_venta_detalle->bindParam(3,$dc_recepcionada,PDO::PARAM_INT);
	$update_nota_venta_detalle->bindParam(4,$dc_comprada,PDO::PARAM_INT);
	$update_nota_venta_detalle->bindParam(5,$dc_detalle,PDO::PARAM_INT);
	
	$update_nota_venta = $db->prepare($db->update('tb_nota_venta',array(
		'dc_facturada' => 'dc_facturada+?',
		'dc_despachada' => 'dc_despachada + ?',
		'dc_recepcionada' => 'dc_recepcionada + ?',
		'dc_comprada' => 'dc_comprada + ?'
	),'dc_nota_venta = ?'));
	$update_nota_venta->bindParam(1,$dc_facturada,PDO::PARAM_INT);
	$update_nota_venta->bindParam(2,$dc_despachada,PDO::PARAM_INT);
	$update_nota_venta->bindParam(3,$dc_recepcionada,PDO::PARAM_INT);
	$update_nota_venta->bindParam(4,$dc_comprada,PDO::PARAM_INT);
	$update_nota_venta->bindValue(5,$nota_credito->dc_nota_venta,PDO::PARAM_INT);
	
	//Actualizar/ajustar cantidades nota de venta
	foreach($detalle as $d):
		$dc_facturada = $d->cantidad_nc;
		$dc_despachada = $to_despacho[$d->dc_nota_venta_detalle];
		
		if($nota_credito->dm_retorna_libre){
			$dc_recepcionada = $dc_comprada = $d->cantidad_nc;
		}else{
			$dc_recepcionada = $dc_comprada = 0;
		}
		
		$db->stExec($update_nota_venta_detalle);
		$db->stExec($update_nota_venta);
		
	endforeach;
	
	$movimiento_bodega = $db->prepare($db->insert('tb_movimiento_bodega',array(
		'dq_monto' => '?',
		'dc_cebe' => '?',
		'dc_ceco' => '?',
		'dc_producto' => '?',
		'dq_cantidad' => '?',
		'dc_cliente' => $nota_credito->dc_cliente,
		'dc_tipo_movimiento' => $logistica->dc_tipo_movimiento,
		'dc_factura' => $nota_credito->dc_factura,
		'dc_nota_credito' => $dc_nota_credito,
		'dc_nota_venta' => $nota_credito->dc_nota_venta,
		'dq_cambio' => 1,
		'dc_bodega_salida' => $nota_credito->dc_bodega_entrada,
		'dc_empresa' => $empresa,
		'df_creacion' => $db->getNow(),
		'dc_usuario_creacion' => $idUsuario
	)));
	$movimiento_bodega->bindParam(1,$dq_monto,PDO::PARAM_STR);
	$movimiento_bodega->bindParam(2,$dc_cebe,PDO::PARAM_INT);
	$movimiento_bodega->bindParam(3,$dc_ceco,PDO::PARAM_INT);
	$movimiento_bodega->bindParam(4,$dc_producto,PDO::PARAM_INT);
	$movimiento_bodega->bindParam(5,$dq_cantidad,PDO::PARAM_INT);
	
	foreach($detalle as $d):
		
		if($nota_credito->dm_retorna_libre == 1){ //Comprobar en stock libre
			bodega_CargarStockLibre($d->dc_producto, $d->cantidad_nc, $nota_credito->dc_bodega_entrada);
		}else{ //Comprobar en stock reservado
			if($to_despacho[$d->dc_nota_venta_detalle] > 0)
				bodega_CargarStockNV($d->dc_producto, $to_despacho[$d->dc_nota_venta_detalle], $nota_credito->dc_bodega_entrada);
		}
		
		$dq_monto = $d->dq_precio_compra;
		$dc_cebe = $d->dc_cebe;
		$dc_ceco = $d->dc_ceco;
		$dc_producto = $d->dc_producto;
		$dq_cantidad = -$to_despacho[$d->dc_nota_venta_detalle];
		
		$db->stExec($movimiento_bodega);
		
	endforeach;
	
}

$db->commit();

$error_man->showAviso('Atención, se ha anulado la nota de crédito');

?>