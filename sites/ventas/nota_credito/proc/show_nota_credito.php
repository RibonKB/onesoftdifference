<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_nota_credito WHERE dc_empresa={$empresa} AND dc_nota_credito = {$_POST['id']}) nc
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = nc.dc_nota_venta
LEFT JOIN tb_factura_venta fv ON fv.dc_factura = nc.dc_factura
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = nc.dc_orden_servicio
JOIN tb_medio_pago mp ON mp.dc_medio_pago = nc.dc_medio_pago
JOIN tb_contacto_cliente c ON c.dc_contacto = nc.dc_contacto
	JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
	JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
	JOIN tb_region reg ON reg.dc_region = com.dc_region
JOIN tb_contacto_cliente ce ON ce.dc_contacto = nc.dc_contacto_entrega
	JOIN tb_cliente_sucursal s2 ON s2.dc_sucursal = c.dc_sucursal
	JOIN tb_comuna com2 ON com2.dc_comuna = s2.dc_comuna
	JOIN tb_region reg2 ON reg2.dc_region = com2.dc_region
LEFT JOIN tb_tipo_cargo tc ON tc.dc_tipo_cargo = nc.dc_tipo_cargo
LEFT JOIN tb_tipo_nota_credito top ON top.dc_tipo_nota_credito = nc.dc_tipo_nota_credito
LEFT JOIN tb_cliente cl ON cl.dc_cliente = nc.dc_cliente
LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = nc.dc_ejecutivo",
"nc.dc_nota_credito,nc.dq_nota_credito,DATE_FORMAT(nc.df_emision,'%d/%m/%Y') AS df_emision,DATE_FORMAT(nc.df_vencimiento,'%d/%m/%Y') AS df_vencimiento,nc.dg_comentario,
nc.dg_orden_compra,nv.dq_nota_venta,os.dq_orden_servicio,nc.dq_neto,nc.dq_iva,nc.dq_total,nc.dc_factura,mp.dg_medio_pago,nc.dm_nula,nc.dm_tipo_impresion,
c.dg_contacto,s.dg_direccion,reg.dg_region,com.dg_comuna,c.dg_fono,s2.dg_direccion AS dg_direccion_entrega,com2.dg_comuna AS dg_comuna_entrega,
reg2.dg_region AS dg_region_entrega,ce.dg_contacto AS dg_contacto_entrega,tc.dg_tipo_cargo,top.dg_tipo_nota_credito,cl.dg_razon,cl.dg_rut,cl.dg_giro,
CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno) dg_ejecutivo,nc.dm_anticipada,fv.dq_factura,fv.dq_folio,nc.dq_folio dq_folio_nc");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la nota de crédito especificada");
	exit();
}
$data = $data[0];
$data['dg_comentario'] = str_replace("\n",'<br />',$data['dg_comentario']);

if($data['dm_tipo_impresion'] == 1){
	$tipo_impresion = 'Modo detalle';
}else{
	$tipo_impresion = 'Modo Glosa';
}

$nula = '';
if($data['dm_nula'] == '1'){
	$null_data = $db->select("(SELECT * FROM tb_nota_credito_anulada WHERE dc_nota_credito = {$_POST['id']}) n
	JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo",'ma.dg_motivo,n.dm_fisica,n.dg_comentario');
	$nula_info = '';
	if(count($null_data)){
		$null_data = $null_data[0];
		$null_data['dm_fisica'] = $null_data['dm_fisica']==1?'SI':'NO';
		$nula_info = "<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
		<div class='info'>Info anulación</div>
		Motivo: <b>{$null_data['dg_motivo']}</b><br />
		Facturada fisicamente: <b>{$null_data['dm_fisica']}</b><br />
		Comentario: <b>{$null_data['dg_comentario']}</b>";
	}
	$nula = "<td rowspan='14' valign='middle'><h3 class='alert'>NOTA DE CRÉDITO ANULADA</h3>{$nula_info}</td>";
}

echo("<div class='title center'>Nota de crédito Nº {$data['dq_nota_credito']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong>({$data['dg_rut']}) {$data['dg_razon']}</strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td>{$data['df_emision']}</td>
	{$nula}
</tr><tr>
	<td width='160'>Folio</td>
	<td>{$data['dq_folio_nc']}</td>
</tr><tr>
	<td width='160'>Factura de venta</td>
	<td><b>{$data['dq_factura']}</b> - folio: <b>{$data['dq_folio']}</b></td>
</tr><tr>
	<td>Domicilio cliente</td>
	<td><b>{$data['dg_direccion']}</b> {$data['dg_comuna']} <label>{$data['dg_region']}</label></td>
</tr><tr>
	<td>Domicilio entrega</td>
	<td><b>{$data['dg_direccion_entrega']}</b> {$data['dg_comuna_entrega']} <label>{$data['dg_region_entrega']}</label></td>
</tr><tr>
	<td>Ejecutivo</td>
	<td><b>{$data['dg_ejecutivo']}</b></td>
</tr><tr>
	<td>Medio de pago:</td>
	<td><b>{$data['dg_medio_pago']}</b></td>
</tr><tr>
	<td>Tipo de cargo:</td>
	<td><b>{$data['dg_tipo_cargo']}</b></td>
</tr><tr>
	<td>Tipo de nota de crédito</td>
	<td>{$data['dg_tipo_nota_credito']}</td>
</tr><tr>
	<td>Fecha de vencimiento</td>
	<td>{$data['df_vencimiento']}</td>
</tr><tr>
	<td>Orden de compra</td>
	<td>{$data['dg_orden_compra']}</td>
</tr><tr>
	<td>Nota de venta</td>
	<td>{$data['dq_nota_venta']}</td>
</tr><tr>
	<td>Orden de servicio</td>
	<td>{$data['dq_orden_servicio']}</td>
</tr><tr>
	<td>Comentario</td>
	<td>{$data['dg_comentario']}</td>
</tr><tr>
	<td>Modo de impresión</td>
	<td><b id='tipo_impresion_label'>{$tipo_impresion}</b>");
	if(check_permiso(59))
		echo('<img src="images/editbtn.png" id="edit_tipo_impresion" />');
	echo("</td>
</tr></table>");

$detalle = $db->select("tb_nota_credito_detalle",
'dg_producto,dc_cantidad,dg_descripcion,dq_precio,dq_costo,dq_descuento,dq_total,dm_tipo',
"dc_nota_credito={$data['dc_nota_credito']}");

$detalles_por_tipo = array();
foreach($detalle as $i){
	$detalles_por_tipo[$i['dm_tipo']][] = $i;
}
unset($detalle);

if(isset($detalles_por_tipo[1])){
	echo("<ul id='tabs'>
		<li><a href='#'>Detalle</a></li>
		<li><a href='#'>Detalle Glosa</a></li>
	</ul>
	
	<br class='clear' />
	<div class='tabpanes'><div>");
}

echo("<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
<tr>
	<th width='60'>Código</th>
	<th width='60'>Cantidad</th>
	<th>Descripción</th>
	<th width='100'>Precio</th>
	<th width='100'>Costo</th>
	<th width='100'>Descuento</th>
	<th width='100'>Total</th>
</tr>
</thead>
<tbody>");

foreach($detalles_por_tipo[0] as $d){
	$d['dc_cantidad'] = number_format($d['dc_cantidad'],2,$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
	$d['dq_precio'] = moneda_local($d['dq_precio']);
	$d['dq_costo'] = moneda_local($d['dq_costo']);
	$d['dq_descuento'] = moneda_local($d['dq_descuento']);
	$d['dq_total'] = moneda_local($d['dq_total']);
	echo("
	<tr>
		<td>{$d['dg_producto']}</td>
		<td>{$d['dc_cantidad']}</td>
		<td align='left'>{$d['dg_descripcion']}</td>
		<td align='right'>{$d['dq_precio']}</td>
		<td align='right'>{$d['dq_costo']}</td>
		<td align='right'>{$d['dq_descuento']}</td>
		<td align='right'>{$d['dq_total']}</td>
	</tr>");
}

$data['dq_neto'] = moneda_local($data['dq_neto']);
$data['dq_iva'] = moneda_local($data['dq_iva']);
$data['dq_total'] = moneda_local($data['dq_total']);

echo("</tbody>
<tfoot>
<tr>
	<th colspan='6' align='right'>Total Neto</th>
	<th align='right'>{$data['dq_neto']}</th>
</tr>
<tr>
	<th colspan='6' align='right'>IVA</th>
	<th align='right'>{$data['dq_iva']}</th>
</tr>
<tr>
	<th colspan='6' align='right'>Total a Pagar</th>
	<th align='right'>{$data['dq_total']}</th>
</tr>
</tfoot></table>");

if(isset($detalles_por_tipo[1])){
	echo("</div><div><table width='100%' class='tab'>
	<caption>Detalle</caption>
	<thead>
	<tr>
		<th width='60'>Código</th>
		<th width='60'>Cantidad</th>
		<th>Descripción</th>
		<th width='100'>Precio</th>
		<th width='100'>Descuento</th>
		<th width='100'>Total</th>
	</tr>
	</thead>
	<tbody>");
	
	foreach($detalles_por_tipo[1] as $d){
	$d['dc_cantidad'] = number_format($d['dc_cantidad'],2,$empresa_conf['dm_separador_decimal'],$empresa_conf['dm_separador_miles']);
	$d['dq_precio'] = moneda_local($d['dq_precio']);
	$d['dq_descuento'] = moneda_local($d['dq_descuento']);
	$d['dq_total'] = moneda_local($d['dq_total']);
	echo("
	<tr>
		<td>{$d['dg_producto']}</td>
		<td>{$d['dc_cantidad']}</td>
		<td align='left'>{$d['dg_descripcion']}</td>
		<td align='right'>{$d['dq_precio']}</td>
		<td align='right'>{$d['dq_descuento']}</td>
		<td align='right'>{$d['dq_total']}</td>
	</tr>");
	}
	
	echo("</tbody>
	<tfoot>
	<tr>
		<th colspan='5' align='right'>Total Neto</th>
		<th align='right'>{$data['dq_neto']}</th>
	</tr>
	<tr>
		<th colspan='5' align='right'>IVA</th>
		<th align='right'>{$data['dq_iva']}</th>
	</tr>
	<tr>
		<th colspan='5' align='right'>Total a Pagar</th>
		<th align='right'>{$data['dq_total']}</th>
	</tr>
	</tfoot></table></div></div>");
}

?>
<script type="text/javascript">
$("#tabs").tabs("div.tabpanes > div",{effect:'default'});
$('#edit_tipo_impresion').click(function(){
	loadFile('sites/ventas/nota_credito/proc/change_tipo_impresion.php?id=<?=$_POST['id'] ?>','#tipo_impresion_label');
});
<?php if($data['dm_nula']): ?>
disable_button('#print_version,#print_glosa_version,#null_factura');
<?php else: ?>
enable_button('#print_version,#print_glosa_version,#null_factura');
$('#print_version').unbind('click').click(function(){
	window.open('sites/ventas/nota_credito/ver_nota_credito.php?id=<?php echo $_POST['id'] ?>&v=<?php echo $data['dm_tipo_impresion'] * -1 + 1; ?>','print_nota_credito','width=800;height=600');
	//window.open("sites/ventas/factura_venta/ver_factura_venta.php?id=<?=$data['dc_factura'] ?>&v=0",'print_factura_venta','width=800;height=600');
});
$('#nota_credito_electronica').unbind('click').click(function(){
	pymerp.loadOverlay('god.php?modulo=ventas&submodulo=nota_credito&factory=NotaCreditoElectronica&dc_nota_credito=<?php echo $_POST['id'] ?>');
});
// $('#null_factura').unbind('click').click(function(){
	// loadOverlay('sites/ventas/factura_venta/null_factura_venta.php?id=<?php echo $_POST['id'] ?>');
// });
// $('#gestion_factura').unbind('click').click(function(){
	// loadOverlay('sites/ventas/factura_venta/cr_gestion_cobranza.php?id=<?php echo $_POST['id'] ?>');
// });
// $('#historial_gestion').unbind('click').click(function(){
	// loadOverlay('sites/ventas/factura_venta/history_gestion_cobranza.php?id=<?php echo $_POST['id'] ?>');
// });
$('#edit_folio').unbind('click').click(function(){
	alert('En construcción');
	//loadOverlay('sites/ventas/factura_venta/ch_folio.php?id=<?php echo $_POST['id'] ?>');
});
$('#null_nota_credito').unbind('click').click(function(){
	loadOverlay('sites/ventas/nota_credito/null_nota_credito.php?id=<?php echo $_POST['id'] ?>');
});
<?php endif; ?>
</script>