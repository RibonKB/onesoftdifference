<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select('tb_factura_venta',
'dc_factura,dc_nota_venta,dc_orden_servicio,dq_neto',
"dc_empresa = {$empresa} AND dq_factura={$_GET['number']} AND dm_nula='0'");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

$detalle = $db->select("tb_factura_venta_detalle",
"dc_detalle,dg_producto dg_codigo,dg_descripcion,dc_cantidad dq_cantidad,dq_precio dq_precio_venta,dq_costo dq_precio_compra,dm_tipo,
  dc_detalle_nota_venta, dc_detalle_orden_servicio",
"dc_factura = {$id['dc_factura']}",array('order_by'=>'dm_tipo'));


if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>