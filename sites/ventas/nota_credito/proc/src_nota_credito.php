<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['nc_emision_desde'] = $db->sqlDate($_POST['nc_emision_desde']);
$_POST['nc_emision_hasta'] = $db->sqlDate($_POST['nc_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['nc_emision_desde']} AND {$_POST['nc_emision_hasta']})";

if($_POST['nc_numero_desde']){
	if($_POST['nc_numero_hasta']){
		$conditions .= " AND
							((dq_nota_credito BETWEEN {$_POST['nc_numero_desde']} AND {$_POST['nc_numero_hasta']})
							OR
							(dq_folio BETWEEN {$_POST['nc_numero_desde']} AND {$_POST['nc_numero_hasta']}))";
	}else{
		$conditions = "(dq_nota_credito = {$_POST['nc_numero_desde']} OR dq_folio = {$_POST['nc_numero_desde']}) AND dc_empresa = {$empresa}";
	}
}

if(isset($_POST['nc_client'])){
	$_POST['nc_client'] = implode(',',$_POST['nc_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['nc_client']})";
}

if(isset($_POST['nc_monto'])){
	$monto = is_numeric($_POST['nc_monto']) ? floatval($_POST['nc_monto']): false;
	if($monto){
		// $monto .= '.00';
		$conditions .= " AND dq_total = {$monto} ";
	}
}

$data = $db->select('tb_nota_credito','dc_nota_credito,dq_nota_credito,dq_folio,dm_nula',$conditions,array('order_by' => 'dq_nota_credito DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron notas de crédito con los criterios especificados");
	exit();
}

echo("<div id='show_nota_credito'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Facturas<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Factura</th>
	</tr>
</thead>
<tbody>
");

foreach($data as $c){
$null_icon = '';
if($c['dm_nula'] == 1){
	$null_icon = '<img src="images/warning.png" alt="NULA" class="right" width="20" title="Nota de Crédito Anulada" />';
}
echo("<tr>
	<td align='left'>
		{$null_icon}
		<a href='sites/ventas/nota_credito/proc/show_nota_credito.php?id={$c['dc_nota_credito']}' class='fv_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_nota_credito']} - <b>{$c['dq_folio']}</b></a>
	</td>
</tr>");
}

echo("</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<!-- button type='button' class='imgbtn' id='gestion_factura' style='background-image:url(images/management.png);'>Gestión</button>
<button type='button' class='imgbtn' id='historial_gestion' style='background-image:url(images/archive.png);'>Historial</button -->
<button type='button' class='imgbtn' id='nota_credito_electronica' style='background-image:url(images/archive.png);'>Nota de Crédito Electrónica</button>
<button type='button' class='editbtn' id='edit_folio'>Editar Folio</button>
<button type='button' class='delbtn' id='null_nota_credito'>Anular</button>");
/*if(check_permiso(61))
echo("<button type='button' class='delbtn' id='null_factura'>Anular</button>");*/

echo("</div>");

?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".fv_load").click(function(e){
		e.preventDefault();
		$('#show_factura').html("<img src='images/ajax-loader.gif' alt='' /> cargando nota de crédito ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('#main_cont .panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_nota_credito');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>