<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$tipo = $db->select('tb_nota_credito','dm_tipo_impresion',"dc_nota_credito={$_POST['id']}");
$tipo = intval($tipo[0]['dm_tipo_impresion'])*-1+1;

$db->escape($_POST['id']);
$db->update('tb_nota_credito',array('dm_tipo_impresion' => $tipo),"dc_nota_credito={$_POST['id']}");

?>
<script type="text/javascript">
	$('#res_list .confirm a').trigger('click');
</script>