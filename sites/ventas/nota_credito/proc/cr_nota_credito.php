<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
require_once("../../../../inc/Factory.class.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(isset($_POST['cli_rut'])){
	//Se escapan los caracteres por seguridad
	$db->escape($_POST['cli_rut']);
	$datos = $db->select("tb_cliente","dc_cliente,dg_razon,dc_contacto_default",
	"dg_rut = '{$_POST['cli_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");

	if(!count($datos)){
		$error_man->showErrorRedirect("No se encontró el cliente especificado, intentelo nuevamente.","sites/ventas/nota_credito/cr_nota_credito.php");
	}
	$datos = $datos[0];
}else if(isset($_POST['cli_id'])){
	$datos = $db->select("tb_cliente",'dc_cliente,dg_razon,dc_contacto_default',
	"dc_cliente={$_POST['cli_id']} AND dc_empresa={$empresa} AND dm_activo='1'");

	if(!count($datos)){
		$error_man->showErrorRedirect("Error: Cliente inválido, intentelo nuevamente.","sites/ventas/factura_venta/src_factura_venta.php");
	}
	$datos = $datos[0];

}else
	exit;

$tipo = $db->select('tb_tipo_nota_credito','*',"dc_tipo_nota_credito = {$_POST['dc_tipo']}");
$tipo = array_shift($tipo);

if($tipo['dm_retorna_stock'] == 0):
	$cuentas_por_tipo = $db->select('tb_tipo_nota_credito_cuenta_contable t
																		JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = t.dc_cuenta_contable',
																		'cc.*',
																		"t.dc_tipo_nota_credito = {$tipo['dc_tipo_nota_credito']}");
	$cuentas_contables = array();
	foreach($cuentas_por_tipo as $c):
		$cuentas_contables[$c['dc_cuenta_contable']] = $c['dg_cuenta_contable'];
	endforeach;
endif;

echo("<div id='secc_bar'>Creación de nota de crédito</div>
<div id='main_cont'>
<input type='text' class='hidden' />
<div class='panes' style='width:1140px;'>
<div class='title center'>
Generando nota de crédito para <strong id='cli_razon' style='color:#000;'>{$datos['dg_razon']}</strong>
</div>");

	include_once("../../../../inc/form-class.php");
	$form = new Form($empresa);

	$dq_nota_credito = $db->select('tb_nota_credito','MAX(dq_folio)+1 AS number',"dc_empresa={$empresa}");
	$dq_nota_credito = $dq_nota_credito[0]['number'];

	$form->Start(Factory::buildUrl('CrearNotaCredito', 'ventas', 'procesarFormulario', 'nota_credito'),"cr_nota_credito",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la nota de crédito</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();

	$form->Date('Fecha emisión','fv_emision',1,0);

	//Contacto principal de la factura, aparecerá en la cabecera como principal contacto del cliente
	$form->Listado('Contacto','fv_contacto',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');

	//Dirección a la que se despachará el pedido
	$form->Listado('Contacto de entrega','fv_cont_entrega',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');

	$form->Listado('Medio de pago','fv_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'),1);
	/*$form->Listado('Tipo de nota de crédito','nc_tipo','tb_tipo_nota_credito',
	array('CONCAT_WS("|",dc_tipo_nota_credito,dm_retorna_stock,dm_retorna_libre,dc_bodega_entrada)','dg_tipo_nota_credito'),1);*/
	$form->Hidden('nc_tipo',"{$tipo['dc_tipo_nota_credito']}|{$tipo['dm_retorna_stock']}|{$tipo['dm_retorna_libre']}|{$tipo['dc_bodega_entrada']}");

	/*$form->Listado('Tipo operación','fv_tipo_operacion',
	"(SELECT * FROM tb_tipo_operacion WHERE dc_empresa={$empresa} AND dm_activo='1') tpo
	LEFT JOIN tb_tipo_guia_despacho tg ON tg.dc_tipo = tpo.dc_tipo_guia",
	array(
		'CONCAT_WS("|",tpo.dc_tipo_operacion,tpo.dc_tipo_movimiento,tpo.dc_tipo_guia,tg.dc_bodega,tg.dc_tipo_movimiento,tpo.dm_tipo_verificacion)',
		'tpo.dg_tipo_operacion'
	),1,0,'');*/

	$form->EndSection();
	$form->Section();

	$form->Text("Número de nota de crédito","nc_number",1,20,$dq_nota_credito);
	$form->Date('Fecha vencimiento','fv_vencimiento',1);
	$form->Text('Orden de compra cliente','fv_orden_compra');
	$form->Listado('Tipo cargo','fv_tipo_cargo','tb_tipo_cargo',array('dc_tipo_cargo','dg_tipo_cargo'));
	$form->Text('Factura de venta','nc_factura_venta',!check_permiso(71));
	//$form->Text('Nota de venta','fv_nota_venta',0,255,$poblacion['nota_venta']);
	$form->Hidden('fv_id_nota_venta',0);
	//$form->Text('Orden de servicio','fv_orden_servicio',0,255,$poblacion['orden_servicio']);
	$form->Hidden('fv_id_orden_servicio',0);
    $form->Hidden('nc_id_factura_venta', 0);

	$form->EndSection();
	$form->Section();

	$form->Textarea('Comentario','fv_comentario',0,isset($num_doc_list)?$num_doc_list:'');
	$form->Listado('Ejecutivo','fv_ejecutivo','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),check_permiso(71),0,"tf.dc_empresa = {$empresa} AND dc_modulo = 1");
    if(check_permiso(59)){
		$form->RadioBox('Modo de impresión','fv_modo_impresion',array( '1' => 'Modo Detalle', '0' => 'Modo Glosa'), 1 );
	} else {
		$form->Hidden('fv_tipo_impresion',1);
	}

    //echo '<div id="relation_docs_list">2013050002</div>';

	$form->EndSection();
?>

	<hr class='clear' />
	<ul id='tabs'>
		<li><a href='#'>Detalle</a></li>
		<li><a href='#'>Detalle Glosa</a></li>
	</ul>

	<br class='clear' />
	<div class='tabpanes'>
	<div id='prods'>
	<div class='info'>Detalle (Valores en <b><?php echo $empresa_conf['dg_moneda_local'] ?></b>)</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='70'>Opciones</th>
		<th width='95'>Código</th>
		<th>Descripción</th>
		<th width='63'>Cantidad</th>
		<th width='82'>Precio</th>
		<th width='82'>Total</th>
		<th width='82'>Costo</th>
		<th width='82'>Costo total</th>
		<th width='82'>Descuento</th>
		<th width='82'>Margen</th>
		<th width='80'>Margen (%)</th>
		<?php if($tipo['dm_retorna_stock'] == 0): ?>
		<th>Cuenta Contable [*]</th>
		<?php endif; ?>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='4' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th>&nbsp;</th>
		<th align='right' class='total_margen'>0</th>
		<th align='center' class='total_margen_p'>0</th>
		<?php if($tipo['dm_retorna_stock'] == 0): ?>
			<th>&nbsp;</th>
		<?php endif; ?>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th colspan='6'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='6'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='6'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>

	<div id='fake_prods' class='hidden'>
	<div class='info'><b>Detalle glosa</b> Este detalle aparecerá en sustitución del detalle real.<br />
	<small>Nota: Los precios deben cuadrar con el detalle real.</small>
	</div>

	</div>

	</div>
<?php
	$form->Hidden('cli_id',$datos['dc_cliente']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('nc_max_neto',0);
	if(isset($doc_list)){
		$form->Hidden('doc_list',$doc_list);
		$form->Hidden('doc_type',$_POST['tipo_doc']);
	}
	$form->End('Emitir Nota de Crédito','addbtn');
?>
<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/doc.png' alt='' title='Asignar Series' class='set_series' />
			<input type='hidden' class='prod_series' name='serie[]' />
            <input type='hidden' class='dc_detalle_nota_venta' name='dc_detalle_nota_venta[]' />
            <input type='hidden' class='dc_detalle_orden_servicio' name='dc_detalle_orden_servicio[]' />
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='9' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='36' required='required' /></td>
		<td>
			<input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' />
			<input type='hidden' name='despachada[]' value='0' class='despach_cant' />
			<input type='hidden' name='recepcionada[]' value='0' class='recep_cant' />
			<input type='hidden' name='facturada[]' value='0' class='fact_cant' />
		</td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td><input type='text' name='descuento[]' class='prod_descuento inputtext' size='7' style='text-align:right;' value='0' required='required' /></td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
		<?php if($tipo['dm_retorna_stock'] == 0): ?>
		<td>
			<?php $form->Select('','dc_cuenta_contable[]',$cuentas_contables,true); ?>
		</td>
		<?php endif; ?>
	</tr>
	</table>

</div></div>
<script type="text/javascript">
	$('#cli_switch').click(function(){
		loadOverlay('sites/ventas/switch_cliente.php');
	});
	var empresa_iva = <?php echo $empresa_conf['dq_iva'] ?>;
	var empresa_dec = <?php echo $empresa_conf['dn_decimales_local'] ?>;

    var url_relation_docs = '<?php echo Factory::buildUrl('CrearNotaCredito', 'ventas', 'obtenerRelacionesFactura', 'nota_credito'); ?>';

</script>
<script type="text/javascript" src="jscripts/product_manager/nota_credito.js?v1_2_8a"></script>
<script type="text/javascript">
create_fake_prods();

if($('#fv_nota_venta').val() != '')
	$('#fv_nota_venta').change();
if($('#fv_orden_servicio').val() != '')
	$('#fv_orden_servicio').change();

$("#fv_vencimiento,#fv_emision").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});
</script>
