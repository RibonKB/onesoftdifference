<?php

class AsignarSaldoFactory extends Factory{
	
	protected $title = 'Asignación saldo a favor Nota de crédito';
	
	/**
	*	Muestra el formulario de ingreso de nota de crédito y factura
	*	TIPO: Action
	*	TEMPLATE: ventas/nota_credito/asignar_saldo/index.form
	*/
	public function indexAction(){
		
		$form = $this->getFormView('ventas/nota_credito/asignar_saldo/index.form');
		echo $this->getFullView($form,array(),Factory::STRING_TEMPLATE);
		
	}
	
	/**
	*	Valida notas de credito y factura seleccionada, descuenta saldos pagados en factura y saldos disponibles en nota de crédito, ingresa comprobante contable
	*	TIPO: Action
	*	TEMPLATE: contabilidad/general/mostrar_comprobante_creado
	*/
	public function procesarAsignacionAction(){
		$db = $this->getConnection();
		
		//Obtener datos de la nota de crédito
		$nota_credito = $db->getRowById('tb_nota_credito',self::getRequest()->dc_nota_credito,'dc_nota_credito');
		
		//Validar nota de crédito
		$this->validaNotaCredito($nota_credito);
		
		//Obtener datos de la factura
		$factura_venta = $db->getRowById('tb_factura_venta',self::getRequest()->dc_factura_venta,'dc_factura');
		
		//Validar Factura de venta
		$this->validaFacturaVenta($factura_venta);
		
		//Validar integridad de ambos documentos
		$this->validaRelacion($nota_credito, $factura_venta);
		
		//Validar configuración contable
		$this->validaContabilidad($nota_credito);
		
		//Obtener monto a pagar en la factura
		$monto_pagar = floatval($this->obtenerMontoPagar($nota_credito, $factura_venta));
		
		$db->start_transaction();
		
			//Cargar monto pagado Factura de venta
			$this->pagarFactura($factura_venta, $monto_pagar);
			
			//Descontar saldo a favor nota de crédito
			$this->saldarNotaCredito($nota_credito, $monto_pagar);
			
			//Generar el comprobante contable para reflejar en la contabilidad la asignación de saldo a favor.
			$comprobante_contable = $this->creaComprobanteContable($monto_pagar);
			$this->insertarDetallesComprobanteContable($comprobante_contable->dc_comprobante, $nota_credito, $factura_venta, $monto_pagar);
			
		$db->commit();
		
		$this->getErrorMan()->showConfirm('Se ha realizado el pago por saldo a favor correctamente');
		echo $this->getView('contabilidad/general/mostrar_comprobante_creado',array(
			'comprobante' => $comprobante_contable
		));
		
	}
	
	/**
	*	Obtiene los detalles de la nota de crédito para mostrarlo como informativo para el usuario
	*	TEMPLATE: ventas/nota_credito/asignar_saldo/show_nota_credito_info
	*/
	public function obtenerDetallesNotaCreditoAction(){
		$db = $this->getConnection();
		$this->initFunctionsService();
		
		$nota_credito = $db->getRowById('tb_nota_credito',self::getRequest()->dc_nota_credito,'dc_nota_credito');
		$cliente = $db->getRowById('tb_cliente',$nota_credito->dc_cliente,'dc_cliente','dg_razon');
		
		echo $this->getView('ventas/nota_credito/asignar_saldo/show_nota_credito_info',array(
			'nota_credito' => $nota_credito,
			'cliente' => $cliente
		));
		
	}
	
	/**
	*	Obtiene los detalles de la factura de venta para mostrarlo como informatico para el usuario
	*	TEMPLATE: ventas/nota_credito/asignar_saldo/show_factura_venta_info
	*/
	public function obtenerDetallesFacturaVentaAction(){
		$db = $this->getConnection();
		$this->initFunctionsService();
		
		$factura = $db->getRowById('tb_factura_venta',self::getRequest()->dc_factura_venta,'dc_factura');
		$cliente = $db->getRowById('tb_cliente',$factura->dc_cliente,'dc_cliente','dg_razon');
		
		echo $this->getView('ventas/nota_credito/asignar_saldo/show_factura_venta_info',array(
			'factura' => $factura,
			'cliente' => $cliente
		));
	}
	
	/**
	*	Valida la nota de crédito ingresada por el usuario
	*	Validaciones:
	*		1.- NC nula
	*		2.- NC posee saldos a favor disponibles
	*/
	private function validaNotaCredito($nota_credito){
		//Validar que la nota de crédito no esté nula
		if($nota_credito->dm_nula == 1){
			$this->getErrorMan()->showWarning('La nota de crédito seleccionada fue anulada, intente con otra.');
		}
		
		//validar que la nota de crédito posea saldos a favor disponibles
		$decimales_empresa = $this->getParametrosEmpresa()->dn_decimales_local;
		if(round($nota_credito->dq_total,$decimales_empresa) == round($nota_credito->dq_favor_usado,$decimales_empresa)){
			$this->getErrorMan()->showAviso('La Nota de crédito seleccionada no posee saldo a favor disponible para utilizar en facturas');
			exit;
		}
	}
	
	/**
	*	Valida la factura de venta ingresada por el usuario
	*	Validaciones:
	*		1.- FV nula
	*		2.- FV tiene pagos pendientes
	*/
	private function validaFacturaVenta($factura_venta){
		//Validar que la factura no esté nula
		if($factura_venta->dm_nula == 1){
			$this->getErrorMan()->showWarning('La factura de Venta seleccionada está nula, seleccione otra.');
			exit;
		}
		
		//Validar que la factura no esté completamente pagada
		$decimales_empresa = $this->getParametrosEmpresa()->dn_decimales_local;
		if(round($factura_venta->dq_total,$decimales_empresa) == round($factura_venta->dq_monto_pagado,$decimales_empresa)){
			$this->getErrorMan()->showAviso('La factura de venta seleccionada no posee saldos a pagar pendientes, compruebe los datos de entrada y vuelva a intentarlo.');
			exit;
		}
	}
	
	/**
	*	Valida el conjunto NC/FV
	*	Validaciones:
	*		1.- Cliente igual para ambos documentos
	*/
	private function validaRelacion($nota_credito, $factura_venta){
		//Validar que el cliente para ambos documentos sea el mismo
		if($nota_credito->dc_cliente != $factura_venta->dc_cliente){
			$this->getErrorMan()->showWarning("No puede asignar saldos a favor entre documentos de distintos clientes");
			exit;
		}
	}
	
	/**
	*	Valida parámetros contables
	*	Validaciones:
	*		1.- Tipo movimiento
	*		2.- Periodo contable de venta abierto
	*		3.- Cuenta contable de cliente válida
	*/
	private function validaContabilidad($data){
		require_once('../../contabilidad/stuff.class.php');
		
		$db = $this->getConnection();
		$conf = ContabilidadStuff::getConfiguration($db, $this->getEmpresa());
		
		if($conf->dc_tipo_movimiento_pago_saldo_favor_cliente == 0){
			$this->getErrorMan()->showWarning('No se ha configurado un tipo de movimiento para el uso de saldo a favor de clientes');
			exit;
		}
		
		//Verificar estado de periodo contable para ventas
		$dc_mes_contable = substr(self::getRequest()->df_fecha_contable,3,2);
		$dc_anho_contable = substr(self::getRequest()->df_fecha_contable,-4);
		$estado_periodo = ContabilidadStuff::getEstadoPeriodo($dc_mes_contable, $dc_anho_contable, ContabilidadStuff::FIELD_ESTADO_FV, $db, $this->getEmpresa(), $this->getUserData()->dc_usuario);
		if($estado_periodo !== ContabilidadStuff::PERIODO_ABIERTO){
			$this->getErrorMan()->showWarning('El periodo contable en el que desea cargar los saldos no está disponible para su usuario');
			exit;
		}
		
		//Verificar cuenta contable del cliente
		$cliente = $db->getRowById('tb_cliente',$data->dc_cliente,'dc_cliente');
		if($cliente->dc_cuenta_contable == 0){
			$this->getErrorMan()->showWarning('El cliente no tiene asociada una cuenta contable, compruebe el maestro y vuelva a intentarlo.');
			exit;
		}
		
	}
	
	/**
	*	Obtiene el monto a pagar a partir de los saldos pendientes de la factura y los saldos disponibles en la nota de credito
	*/
	private function obtenerMontoPagar($nota_credito, $factura_venta){
		
		$decimales_empresa = $this->getParametrosEmpresa()->dn_decimales_local;
		$pendiente_factura = round($factura_venta->dq_total-$factura_venta->dq_monto_pagado, $decimales_empresa);
		$disponible_nota_credito = round($nota_credito->dq_total-$nota_credito->dq_favor_usado, $decimales_empresa);
		
		if(self::getRequest()->dq_monto_saldar){
			$monto_saldar = floatval(trim(self::getRequest()->dq_monto_saldar));
			
			if($monto_saldar <= 0){
				$this->getErrorMan()->showWarning('Ingrese un monto válido y mayor a 0 (cero) para saldar en la factura');
				exit;
			}
			
			if($monto_saldar > $disponible_nota_credito){
				$this->getErrorMan()->showWarning('Asegúrese de que el monto a saldar no supere el saldo disponible en la nota de crédito.');
				exit;
			}
			
			if($monto_saldar > $pendiente_factura){
				$this->getErrorMan()->showWarning('Asegúrese de que el monto a saldar no supere el saldo pendiente de la factura');
				exit;
			}
			
			return $monto_saldar;
			
		}
		
		if($pendiente_factura > $disponible_nota_credito){
			return $disponible_nota_credito;
		}else{
			return $pendiente_factura;
		}
	}
	
	/**
	*	Ingresa la información de pago en la tabla de factura
	*/
	private function pagarFactura($factura_venta, $monto_pagar){
		$db = $this->getConnection();
		$update_factura = $db->prepare($db->update('tb_factura_venta',array(
			'dq_monto_pagado' => 'dq_monto_pagado+?'
		),'dc_factura = ?'));
		$update_factura->bindValue(1,$monto_pagar,PDO::PARAM_STR);
		$update_factura->bindValue(2,$factura_venta->dc_factura,PDO::PARAM_INT);
		$db->stExec($update_factura);
	}
	
	/**
	*	Descuenta el saldo disponible en la nota de crédito
	*/
	private function saldarNotaCredito($nota_credito, $monto_saldar){
		$db = $this->getConnection();
		$update_nota_credito = $db->prepare($db->update('tb_nota_credito',array(
			'dq_favor_usado' => 'dq_favor_usado+?'
		),'dc_nota_credito = ?'));
		$update_nota_credito->bindValue(1,$monto_saldar,PDO::PARAM_STR);
		$update_nota_credito->bindValue(2,$nota_credito->dc_nota_credito,PDO::PARAM_INT);
		$db->stExec($update_nota_credito);
	}
	
	/**
	*	Genera un comprobante para reflejar contablemente el movimiento de pago y descuento
	*/
	private function creaComprobanteContable($saldo){
		$db = $this->getConnection();
		
		require_once('../../contabilidad/stuff.class.php');
		
		$data = new stdClass();
		$data->dg_comprobante = ContabilidadStuff::getCorrelativoComprobante($db, $this->getEmpresa());
		
		$conf = ContabilidadStuff::getConfiguration();
		
		$comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
			'dg_comprobante' => '?',
			'dc_tipo_movimiento' => '?',
			'df_fecha_contable' => '?',
			'dc_mes_contable' => 'MONTH(?)',
			'dc_anho_contable' => 'YEAR(?)',
			'dg_glosa' => '?',
			'dq_saldo' => '?',
			'df_fecha_emision' => $db->getNow(),
			'dc_empresa' => $this->getEmpresa(),
			'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
			'dm_tipo_comprobante' => '?'
		)));
		$comprobante->bindValue(1,$data->dg_comprobante,PDO::PARAM_INT);
		$comprobante->bindValue(2,$conf->dc_tipo_movimiento_pago_saldo_favor_cliente,PDO::PARAM_INT);
		$comprobante->bindValue(3,$db->sqlDate2(self::getRequest()->df_fecha_contable),PDO::PARAM_STR);
		$comprobante->bindValue(4,$db->sqlDate2(self::getRequest()->df_fecha_contable),PDO::PARAM_STR);
		$comprobante->bindValue(5,$db->sqlDate2(self::getRequest()->df_fecha_contable),PDO::PARAM_STR);
		$comprobante->bindValue(6,'Pago con saldo a favor '.date('d/m/Y'),PDO::PARAM_STR);
		$comprobante->bindValue(7,$saldo,PDO::PARAM_STR);
		$comprobante->bindValue(8,'2',PDO::PARAM_STR);
		$db->stExec($comprobante);
		
		$data->dc_comprobante = $db->lastInsertId();
		
		return $data;
	}
	
	/**
	*	Inserta los detalles del comprobante contable
	*/
	private function insertarDetallesComprobanteContable($dc_comprobante, $nota_credito, $factura_venta, $monto){
		$db = $this->getConnection();
		
		$cliente = $db->getRowById('tb_cliente',$nota_credito->dc_cliente,'dc_cliente');
		
		$detalle_financiero = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
			'dc_comprobante' => '?',
			'dc_cuenta_contable' => '?',
			'dq_debe' => '?',
			'dq_haber' => '?',
			'dg_glosa' => '?',
			'dg_rut' => '?',
			'dc_cliente' => '?',
			'dg_doc_venta' => '?',
			'dc_factura_venta' => '?',
			'dc_nota_credito' => '?'
		)));
		$detalle_financiero->bindValue(1,$dc_comprobante,PDO::PARAM_INT);
		$detalle_financiero->bindValue(2,$cliente->dc_cuenta_contable,PDO::PARAM_INT);
		$detalle_financiero->bindParam(3,$dq_debe,PDO::PARAM_STR);
		$detalle_financiero->bindParam(4,$dq_haber,PDO::PARAM_STR);
		$detalle_financiero->bindValue(5,"Pago Cliente con saldo a favor Nota de crédito: {$nota_credito->dq_nota_credito} folio {$nota_credito->dq_folio} a Factura de venta: {$factura_venta->dq_factura} folio {$factura_venta->dq_folio}",PDO::PARAM_STR);
		$detalle_financiero->bindValue(6,$cliente->dg_rut,PDO::PARAM_STR);
		$detalle_financiero->bindValue(7,$cliente->dc_cliente,PDO::PARAM_INT);
		$detalle_financiero->bindValue(8,$factura_venta->dq_folio,PDO::PARAM_STR);
		$detalle_financiero->bindValue(9,$factura_venta->dc_factura,PDO::PARAM_INT);
		$detalle_financiero->bindValue(10,$nota_credito->dc_nota_credito,PDO::PARAM_INT);
		
		$dq_debe = $monto;
		$dq_haber = 0;
		
		$db->stExec($detalle_financiero);
		
		$dq_debe = 0;
		$dq_haber = $monto;
		
		$db->stExec($detalle_financiero);
		
	}
	
	/**
	*	Obtiene el listado de facturas de venta que cumplen los criterios puestos por Autocompleter
	*/
	public function facturaVentaAutoCompletarAction(){
		require_once('../../../inc/Autocompleter.class.php');
		
		$request = Factory::getRequest();
		$db = $this->getConnection();
		
		$q = $request->q;
		$limit = $request->limit;
		
		$data = $db->prepare($db->select('tb_factura_venta fv
			JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente',
		'fv.dc_factura, fv.dq_factura, fv.dq_folio, fv.dq_neto, cl.dg_razon',
		'fv.dc_empresa = ? AND fv.dm_nula = 0 AND (fv.dq_factura LIKE ? OR fv.dq_folio LIKE ? OR cl.dg_razon LIKE ?)',
		array('limit' => $limit)));
		$data->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$data->bindValue(2,'%'.$q.'%',PDO::PARAM_STR);
		$data->bindValue(3,'%'.$q.'%',PDO::PARAM_STR);
		$data->bindValue(4,'%'.$q.'%',PDO::PARAM_STR);
		$db->stExec($data);
		
		while($fv = $data->fetch(PDO::FETCH_OBJ)){
			echo Autocompleter::getItemRow(array(
				$fv->dq_factura,
				$fv->dq_folio,
				$fv->dg_razon,
				$fv->dc_factura
			));
		}
		
	}
	
	/**
	*	Obtiene el listado de notas de crédito que cumplen los criterios puestos por Autocompleter
	*/
	public function notaCreditoAutoCompletarAction(){
		require_once('../../../inc/Autocompleter.class.php');
		
		$request = Factory::getRequest();
		$db = $this->getConnection();
		
		$q = $request->q;
		$limit = $request->limit;
		
		$data = $db->prepare($db->select('tb_nota_credito nc
			JOIN tb_cliente cl ON cl.dc_cliente = nc.dc_cliente',
		'nc.dc_nota_credito, nc.dq_nota_credito, nc.dq_folio, nc.dq_neto, cl.dg_razon',
		'nc.dc_empresa = ? AND nc.dm_nula = 0 AND (nc.dq_nota_credito LIKE ? OR nc.dq_folio LIKE ? OR cl.dg_razon LIKE ?)'));
		$data->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
		$data->bindValue(2,'%'.$q.'%',PDO::PARAM_STR);
		$data->bindValue(3,'%'.$q.'%',PDO::PARAM_STR);
		$data->bindValue(4,'%'.$q.'%',PDO::PARAM_STR);
		$db->stExec($data);
		
		while($nc = $data->fetch(PDO::FETCH_OBJ)){
			echo Autocompleter::getItemRow(array(
				$nc->dq_nota_credito,
				$nc->dq_folio,
				$nc->dg_razon,
				$nc->dc_nota_credito
			));
		}
		
	}
	
}
?>