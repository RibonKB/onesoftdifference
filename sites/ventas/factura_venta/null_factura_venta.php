<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_factura_venta','dq_factura, MONTH(df_libro_venta) dc_mes_contable, YEAR(df_libro_venta) dc_anho_contable',"dc_factura = {$_POST['id']}");
$data = array_shift($data);

if($data == NULL){
	$error_man->showWarning('El número de factura especificado es inválido');
	exit;
}

$dq_factura = $data['dq_factura'];

require_once("../../contabilidad/stuff.class.php");

/**
*	Comprobar Estado del periodo contable en el que se está la factura de venta
**/

$dm_estado_periodo_contable = ContabilidadStuff::getEstadoPeriodo_OLD($data['dc_mes_contable'],$data['dc_anho_contable'],ContabilidadStuff::FIELD_ESTADO_FV);

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_CERRADO){
	$error_man->showWarning('No puede anular facturas de venta que se encuentran en periodos contables cerrados.');
	exit;
}

//END ESTADO CONTABLE

echo('<div class="secc_bar">Anular factura de venta</div><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/factura_venta/proc/null_factura_venta.php','null_factura_form');
$form->Header('Indique las razones por las que se anulará la factura de venta');
$error_man->showAviso("Atención, está a punto de anular la factura N° <b>{$dq_factura}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->Radiobox('Facturada fisicamente','null_fisica',array('NO','SI'),0);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('id_factura',$_POST['id']);
$form->End('Anular','delbtn');

?>