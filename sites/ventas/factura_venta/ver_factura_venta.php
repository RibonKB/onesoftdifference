<?php
	define("MAIN",1);
	include_once("../../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	
	$datosFactura = $db->select("(SELECT * FROM tb_factura_venta WHERE dc_empresa={$empresa} AND dc_factura = {$_GET['id']}) fv
	LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fv.dc_nota_venta
	LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fv.dc_orden_servicio
	JOIN tb_medio_pago mp ON mp.dc_medio_pago = fv.dc_medio_pago
	JOIN tb_contacto_cliente c ON c.dc_contacto = fv.dc_contacto
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
		JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
		JOIN tb_region reg ON reg.dc_region = com.dc_region
	JOIN tb_contacto_cliente ce ON ce.dc_contacto = fv.dc_contacto_entrega
		JOIN tb_cliente_sucursal s2 ON s2.dc_sucursal = ce.dc_sucursal
		JOIN tb_comuna com2 ON com2.dc_comuna = s2.dc_comuna
		JOIN tb_region reg2 ON reg2.dc_region = com2.dc_region
	LEFT JOIN tb_tipo_cargo tc ON tc.dc_tipo_cargo = fv.dc_tipo_cargo
	LEFT JOIN tb_tipo_operacion top ON top.dc_tipo_operacion = fv.dc_tipo_operacion
	LEFT JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente
	LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = fv.dc_ejecutivo",
	"fv.dq_folio dq_factura,UNIX_TIMESTAMP(fv.df_emision) AS df_emision,DATE_FORMAT(fv.df_vencimiento,'%d/%m/%Y') AS df_vencimiento,fv.dm_tipo_impresion,
	fv.dg_orden_compra,fv.df_emision as emision_factura,nv.dc_nota_venta,nv.dq_nota_venta,nv.dq_cambio,nv.dm_al_dia,nv.df_fecha_emision as emision_nv,
	os.dq_orden_servicio,fv.dq_neto,fv.dq_iva,fv.dq_total,fv.dc_factura,mp.dg_medio_pago,
	c.dg_contacto,s.dg_direccion,reg.dg_region,com.dg_comuna,c.dg_fono,s2.dg_direccion AS dg_direccion_entrega,com2.dg_comuna AS dg_comuna_entrega,
	reg2.dg_region AS dg_region_entrega,ce.dg_contacto AS dg_contacto_entrega,tc.dg_tipo_cargo,top.dc_tipo_operacion,top.dg_tipo_operacion,cl.dg_razon,cl.dg_rut,cl.dg_giro,
	CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno) dg_ejecutivo,fv.dg_comentario");
	
	if(!count($datosFactura)){
		$error_man->showWarning("La factura de venta especificada no existe");
		exit();
	}
	
	$datosFactura = $datosFactura[0];
	setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
	$datosFactura['df_emision'] = strftime("%d de %B de %Y",$datosFactura['df_emision']);
	
	$_GET['v'] = $datosFactura['dm_tipo_impresion'];
	
	$datosFactura['detalle'] = $db->select("(SELECT * FROM tb_factura_venta_detalle WHERE dc_factura={$datosFactura['dc_factura']} AND dm_tipo<>'{$_GET['v']}') d
	LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
	LEFT JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto",
	'd.dg_producto,d.dc_cantidad,d.dg_descripcion,d.dg_serie,d.dq_precio,d.dq_descuento,d.dq_total,t.dm_controla_inventario');
	
	//obtener las guias de despacho asociadas a la compra
	
	$datosFactura['guias_despacho'] = $db->select('tb_guia_despacho','dq_folio, df_emision',
		"dc_factura = {$datosFactura['dc_factura']} AND dq_folio <> 0",array('order_by' => 'df_emision asc'));
		
	foreach ($datosFactura['guias_despacho'] as $c => $v){
		$datosFactura['guias_despacho'][$c]['df_emision'] = $db->dateLocalFormat($v['df_emision']);
	}
	
	//Obtener tipo de operacion
	
	$tipoOperacion = $db->select('tb_tipo_operacion', '*', "dc_tipo_operacion = {$datosFactura['dc_tipo_operacion']} "
	. " AND dc_empresa = {$empresa} ");
	
	$alDia = $datosFactura['dm_al_dia'];
	
	//Realizar cambios de acuerdo al tipo de operacion
	
	$tipoOperacion = $tipoOperacion[0];
	if(!is_null($tipoOperacion['dc_tipo_cambio']) && $tipoOperacion['dc_tipo_cambio'] > 0){
		$tipoCambio = 0;
		
		if($alDia == 0){
			$emision = explode(' ', $datosFactura['emision_nv']);
			$cambio = $db->select('tb_tipo_cambio_mensual','dq_cambio', "dc_tipo_cambio = {$tipoOperacion['dc_tipo_cambio']} 
				AND df_cambio = '{$emision[0]}'" );
			$cambio = empty($cambio[0]) ? 0:$cambio[0];
			$tipoCambio = $cambio['dq_cambio'];
		} else {
			$emision = explode(' ', $datosFactura['emision_factura']);
			$cambio = $db->select('tb_tipo_cambio_mensual','dq_cambio', "dc_tipo_cambio = {$tipoOperacion['dc_tipo_cambio']} 
				AND df_cambio = '{$emision[0]}'" );
			$cambio = empty($cambio[0]) ? 0:$cambio[0];
			$tipoCambio = $cambio['dq_cambio'];
			//verificar que el tipo de cambio no sea 0 dividir por el tipo de cambio en los detalles y totales despues de los if. 
		}
		//echo $tipoCambio;
		if($tipoCambio == 0){
			$error_man->showWarning("El tipo de cambio para el día {$db->dateLocalFormat($emision[0])} no fué especificado. Verifique la información e intente nuevamente.");
			exit();
		}
		
		//dividir los montos por el tipo de cambio para los detalles de la factura.
		foreach($datosFactura['detalle'] as $i => $v){
			if($v['dq_precio'] != 0) {
				$datosFactura['detalle'][$i]['dq_precio'] = $v['dq_precio']/$tipoCambio;
			}
			if($v['dq_descuento'] != 0) {
				$datosFactura['detalle'][$i]['dq_descuento'] = $v['dq_descuento']/$tipoCambio;
			}
			if($v['dq_total'] != 0) {
				$datosFactura['detalle'][$i]['dq_total'] = $v['dq_total']/$tipoCambio;
			}
		}
		//dividir los totales de la cabecera de la factura fv.dq_neto,fv.dq_iva,fv.dq_total
		if($datosFactura['dq_neto'] != 0) {
			$datosFactura['dq_neto'] = $datosFactura['dq_neto']/$tipoCambio;
		}
		if($datosFactura['dq_iva'] != 0) {
			$datosFactura['dq_iva'] = $datosFactura['dq_iva']/$tipoCambio;
		}
		if($datosFactura['dq_total'] != 0) {
			$datosFactura['dq_total'] = $datosFactura['dq_total']/$tipoCambio;
		}
		
	}
	
	
	foreach($datosFactura['detalle'] as $i => $v){
		if($v['dm_controla_inventario'] != 0 && $tipoOperacion['dm_exenta'] != 1 && $tipoOperacion['dm_agrega_iva_producto'] != 1 )
			$datosFactura['detalle'][$i]['dg_producto'] = '';
	}
	if($tipoOperacion['dm_zona_franca'] == 1) {
		include("template_factura_venta/modo{$empresa}zofri.php");
	} else if($tipoOperacion['dm_exenta'] == 1 && $tipoOperacion['dm_agrega_iva_producto'] == 1 ){
		include("template_factura_venta/modo{$empresa}exportacion.php");
	} else if ($tipoOperacion['dm_exenta'] == 1 && $tipoOperacion['dm_agrega_iva_producto'] == 0 ) {
		include("template_factura_venta/modo{$empresa}exenta.php");
	} else if (($tipoOperacion['dm_exenta'] == 0 && $tipoOperacion['dm_agrega_iva_producto'] == 1 )){
		$error_man->showWarning("Existe un error en la definicion del tipo de operación de esta factura. Contacte con el administrador");
		exit();
	} else {
		include("template_factura_venta/modo{$empresa}.php");
	}
	
	$datosFactura['emision_factura'] = $db->dateLocalFormat($datosFactura['emision_factura']);

	$pdf = new PDF();
	$pdf->AddDetalle($datosFactura['detalle']);
	$pdf->Output();
	
	
?>