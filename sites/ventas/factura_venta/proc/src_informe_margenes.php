<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

//Obtener información del tipo de cambio
$tipo_cambio = $db->prepare($db->select('tb_tipo_cambio','dg_tipo_cambio, dq_cambio, dn_cantidad_decimales','dc_tipo_cambio = ?'));
$tipo_cambio->bindValue(1,$_POST['dc_tipo_cambio'],PDO::PARAM_INT);
$db->stExec($tipo_cambio);
$tipo_cambio = $tipo_cambio->fetch(PDO::FETCH_OBJ);

if($tipo_cambio === false){
	$error_man->showWarning('No se pudo obtener la información del tipo de cambio, compruebe con otro o consulte con un administrador.');
	exit;
}

$dq_cambio = $tipo_cambio->dq_cambio;
$dn_decimales = $tipo_cambio->dn_cantidad_decimales;

//Validar mes del periodo
$dc_mes = $_POST['dc_mes'];
if(!is_numeric($dc_mes) || $dc_mes > 12 || $dc_mes < 1){
	$error_man->showWarning("El mes indicado es inválido, no se puede continuar con la consulta.");
	exit;
}
$dc_mes = intval($dc_mes);

//Validar año del periodo
$dc_anho = $_POST['dc_anho'];
if(!is_numeric($dc_anho)){
	$error_man->showWarning("El año indicado es inválido, no se puede continuar con la consulta.");
	exit;
}
$dc_anho = intval($dc_anho);

//Comprobar filtro de ejecutivos
$ejecutivo_cond = '';
$ejecutivo_join = 'LEFT JOIN';
if(isset($_POST['dc_ejecutivo'])){
	$dc_ejecutivo = implode(',',$_POST['dc_ejecutivo']);
	$ejecutivo_cond = "AND ex.dc_funcionario IN ({$dc_ejecutivo})";
	$ejecutivo_join = 'JOIN';
}

/*
*	Obtener Facturas
*	
*	IN: dc_mes, dc_anho
*	OUT: data
*/

$data = $db->prepare(
'('.
	$db->select("tb_factura_venta fv
		LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fv.dc_nota_venta
		LEFT JOIN tb_tipo_nota_venta tnv ON tnv.dc_tipo = nv.dc_tipo_nota_venta
		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fv.dc_orden_servicio
		{$ejecutivo_join} tb_funcionario ex ON ex.dc_funcionario = fv.dc_ejecutivo {$ejecutivo_cond}
		JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente
		JOIN tb_factura_venta_detalle fvd ON fv.dc_factura = fvd.dc_factura AND fvd.dm_tipo = 0
		LEFT JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = fvd.dc_detalle_nota_venta
		LEFT JOIN tb_orden_servicio_factura_detalle osd ON osd.dc_detalle = fvd.dc_detalle_orden_servicio
		LEFT JOIN tb_orden_compra_detalle ocd ON ocd.dc_detalle_nota_venta = nvd.dc_nota_venta_detalle OR ocd.dc_detalle_orden_servicio = osd.dc_detalle
		LEFT JOIN tb_orden_compra oc ON oc.dc_orden_compra = ocd.dc_orden_compra
		LEFT JOIN tb_factura_compra_detalle fcd ON fcd.dc_detalle_nota_venta = nvd.dc_nota_venta_detalle OR fcd.dc_detalle_orden_servicio = osd.dc_detalle
		LEFT JOIN tb_factura_compra fc ON fc.dc_factura = fcd.dc_factura",
	'"FV" td, fv.dq_factura dq_factura_venta, fv.dq_folio, fv.df_emision, fv.dm_anticipada,
	nv.dq_nota_venta, os.dq_orden_servicio, cl.dg_rut, cl.dg_razon,
	fvd.dg_producto, fvd.dg_descripcion, fvd.dq_precio dq_precio_factura, fvd.dq_costo dq_costo_factura, fvd.dc_cantidad,
	MAX(nvd.dq_precio_compra) dq_costo_nota_venta, MAX(nvd.dq_precio_venta) dq_precio_nota_venta,
	MAX(osd.dq_precio_compra) dq_costo_orden_servicio, MAX(osd.dq_precio_venta) dq_precio_orden_servicio,
	MAX(ocd.dq_precio) dq_costo_orden_compra, oc.dq_orden_compra,
	MAX(fcd.dq_precio) dq_costo_factura_compra, fc.dq_factura dq_factura_compra,
	CONCAT_WS(" ",ex.dg_nombres, ex.dg_ap_paterno, ex.dg_ap_materno) dg_ejecutivo, tnv.dm_costeable',
	'MONTH(fv.df_emision) = ? AND YEAR(fv.df_emision) = ? AND fv.dc_empresa = ? AND fv.dm_nula = 0',
	array(
		'group_by' => 'fvd.dc_detalle'
	))
.') UNION ALL ('.
	$db->select("tb_nota_credito nc
		LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = nc.dc_nota_venta
		LEFT JOIN tb_tipo_nota_venta tnv ON tnv.dc_tipo = nv.dc_tipo_nota_venta
		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = nc.dc_orden_servicio
		LEFT JOIN tb_factura_venta fv ON fv.dc_factura = nc.dc_factura
		{$ejecutivo_join} tb_funcionario ex ON (nc.dc_ejecutivo = ex.dc_funcionario OR fv.dc_ejecutivo = ex.dc_funcionario) {$ejecutivo_cond}
		JOIN tb_cliente cl ON cl.dc_cliente = nc.dc_cliente
		JOIN tb_nota_credito_detalle ncd ON ncd.dc_nota_credito = nc.dc_nota_credito",
	'"NC", nc.dq_nota_credito, nc.dq_folio, nc.df_emision, "X", nv.dq_nota_venta, os.dq_orden_servicio, cl.dg_rut, cl.dg_razon, ncd.dg_producto, ncd.dg_descripcion,
	 -ncd.dq_precio, -ncd.dq_costo, ncd.dc_cantidad, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	 CONCAT_WS(" ",ex.dg_nombres, ex.dg_ap_paterno, ex.dg_ap_materno), tnv.dm_costeable',
	'MONTH(nc.df_emision) = ? AND YEAR(nc.df_emision) = ? AND nc.dc_empresa = ? AND nc.dm_nula = 0 AND ncd.dm_tipo = 0')
.') ORDER BY df_emision');

$data->bindValue(1,$dc_mes,PDO::PARAM_INT);
$data->bindValue(2,$dc_anho,PDO::PARAM_INT);
$data->bindValue(3,$empresa,PDO::PARAM_INT);
$data->bindValue(4,$dc_mes,PDO::PARAM_INT);
$data->bindValue(5,$dc_anho,PDO::PARAM_INT);
$data->bindValue(6,$empresa,PDO::PARAM_INT);

$db->stExec($data);

$facturas = array();
while($d = $data->fetch(PDO::FETCH_OBJ)){
	
	$i = $d->dq_factura_venta.$d->td;
	
	if(!isset($facturas[$i])){
		$fdata = new stdClass;
		$fdata->dg_td = $d->td;
		$fdata->dq_folio = $d->dq_folio;
		$fdata->dg_cliente = $d->dg_razon;
		$fdata->dq_nota_venta = $d->dq_nota_venta;
		$fdata->dq_orden_servicio = $d->dq_orden_servicio;
		$fdata->dg_ejecutivo = $d->dg_ejecutivo;
		$fdata->dq_neto_venta = 0;
		$fdata->dq_neto_compra = 0;
		$fdata->dq_margen = 0;
		$fdata->dq_margen_p = 0;
		$fdata->detalle = array();
		
		switch($d->dm_anticipada){
			case 'N' : $fdata->dg_tipo = 'Normal'; break;
			case 'A' : $fdata->dg_tipo = 'Anticipada'; break;
			case 'E' : $fdata->dg_tipo = 'Anticipada Completa'; break;
			 default : $fdata->dg_tipo = '-';
		}
		
		$facturas[$i] = $fdata;
		unset($fdata);
	}
	
	$item = new stdClass;
	$item->dg_codigo = $d->dg_producto;
	$item->dg_descripcion = $d->dg_descripcion;
	$item->dq_factura_compra = $d->dq_factura_compra;
	$item->dq_orden_compra = $d->dq_orden_compra;
	$item->dq_cantidad = $d->dc_cantidad;
	
	if($d->dq_costo_factura_compra != NULL){
		$item->dq_costo = $d->dq_costo_factura_compra;
		$item->dg_origen_costo = 'FC';
	}else if($d->dq_costo_orden_compra != NULL){
		$item->dq_costo = $d->dq_costo_orden_compra;
		$item->dg_origen_costo = 'OC';
	}else if($d->dq_costo_factura != 0 || ($d->dq_nota_venta != NULL && $d->dm_costeable == 1)){
		$item->dq_costo = $d->dq_costo_factura;
		if($d->td === 'FV')
			$item->dg_origen_costo = 'FV';
		else
			$item->dg_origen_costo = 'NC';
	}else if($d->dq_costo_nota_venta != NULL){
		$item->dq_costo = $d->dq_costo_nota_venta;
		$item->dg_origen_costo = 'NV';
	}else if($d->dq_costo_orden_servicio != NULL){
		$item->dq_costo = $d->dq_costo_orden_servicio;
		$item->dg_origen_costo = 'OS';
	}else{
		$item->dq_costo = $d->dq_costo_factura;
		if($d->td === 'FV')
			$item->dg_origen_costo = 'FV';
		else
			$item->dg_origen_costo = 'NC';
	}
	
	$item->dq_precio = $d->dq_precio_factura;
	$item->dq_margen = $item->dq_precio - $item->dq_costo;
	
	if($item->dq_precio != 0){
		$item->dq_margen_p = ($item->dq_margen / $item->dq_precio)*100;
	}else
		$item->dq_margen_p = 0;
	
	$facturas[$i]->detalle[] = $item;
	
	$facturas[$i]->dq_neto_venta += $item->dq_precio*$item->dq_cantidad;
	$facturas[$i]->dq_neto_compra += $item->dq_costo*$item->dq_cantidad;
	$facturas[$i]->dg_origen_costo = $item->dg_origen_costo;
	
}

$total_precio = 0;
$total_costo = 0;
foreach($facturas as $i => $f){
	$facturas[$i]->dq_margen = $f->dq_neto_venta - $f->dq_neto_compra;
	
	if($f->dq_neto_venta != 0)
		$facturas[$i]->dq_margen_p = ($facturas[$i]->dq_margen / $f->dq_neto_venta)*100;
	else
		$facturas[$i]->dq_margen_p = 0;
		
	$total_precio += $f->dq_neto_venta;
	$total_costo += $f->dq_neto_compra;

}

$total_margen = $total_precio - $total_costo;
$total_margen_p = ($total_margen / $total_precio)*100;

?>
<div class="info">
	Los valores están presentados con el tipo de cambio <b><?php echo $tipo_cambio->dg_tipo_cambio ?></b>
</div>
<table class="tab" width="100%" id="datatab">
	<thead>
		<th></th>
		<th>Documento</th>
        <th>Folio</th>
		<th>Tipo</th>
		<th>Cliente</th>
		<th>Nota de venta</th>
		<th>Orden de Servicio</th>
		<th>Ejecutivo</th>
		<th>Neto Venta</th>
		<th>Neto Costo</th>
        <th width="5">Src</th>
		<th>Margen</th>
		<th>Margen %</th>
	</thead>
	<tfoot>
		<tr>
			<th align="right" colspan="8">Total</th>
			<th align="right"><?php echo moneda_local($total_precio/$dq_cambio,$dn_decimales) ?></th>
			<th align="right"><?php echo moneda_local($total_costo/$dq_cambio,$dn_decimales) ?></th>
            <th>&nbsp;</th>
			<th align="right"><?php echo moneda_local($total_margen/$dq_cambio,$dn_decimales) ?></th>
			<th><?php echo moneda_local($total_margen_p,2) ?> %</th>
		</tr>
	</tfoot>
	<tbody>
	<?php foreach($facturas as $dq_factura_venta => $f): ?>
		<tr class="fhead <?php echo $f->dg_td ?>">
			<td><img src="images/addbtn.png" alt="(+)" class="expand_detail" /></td>
			<td><b><?php echo substr($dq_factura_venta,0,-2) ?></b></td>
            <td><b><?php echo $f->dq_folio ?></b></td>
			<td><?php echo $f->dg_tipo ?></td>
			<td><?php echo $f->dg_cliente ?></td>
			<td><?php echo $f->dq_nota_venta ?></td>
			<td><?php echo $f->dq_orden_servicio ?></td>
			<td><?php echo $f->dg_ejecutivo ?></td>
			<td align="right"><?php echo moneda_local($f->dq_neto_venta/$dq_cambio,$dn_decimales) ?></td>
			<td align="right"><?php echo moneda_local($f->dq_neto_compra/$dq_cambio,$dn_decimales) ?></td>
            <td align="center"><?php echo $f->dg_origen_costo ?></td>
			<td align="right"><?php echo moneda_local($f->dq_margen/$dq_cambio,$dn_decimales) ?></td>
			<td align="center"><?php echo moneda_local($f->dq_margen_p,2) ?> %</td>
		</tr>
		<tr class="hidden">
			<td> -> </td>
			<td colspan="11">
				<table class="tab" width="100%">
					<thead>
						<th>Producto</th>
						<th>Factura de compra</th>
						<th>Orden de compra</th>
						<th>Cantidad</th>
						<th>Precio Neto</th>
						<th>Costo Neto</th>
						<th>Margen</th>
						<th>Margen %</th>
					</thead>
					<tfoot>
						<tr>
							<th align="right" colspan="4">Totales</th>
							<th align="right"><?php echo moneda_local($f->dq_neto_venta/$dq_cambio,$dn_decimales) ?></th>
							<th align="right"><?php echo moneda_local($f->dq_neto_compra/$dq_cambio,$dn_decimales) ?></th>
							<th align="right"><?php echo moneda_local($f->dq_margen/$dq_cambio,$dn_decimales) ?></th>
							<th><?php echo moneda_local($f->dq_margen_p,2) ?> %</th>
						</tr>
					</tfoot>
					<tbody>
					<?php foreach($f->detalle as $d): ?>
						<tr>
							<td><b><?php echo $d->dg_codigo ?></b> <?php echo $d->dg_descripcion ?></td>
							<td><?php echo $d->dq_factura_compra ?></td>
							<td><?php echo $d->dq_orden_compra ?></td>
							<td><?php echo $d->dq_cantidad ?></td>
							<td align="right" title="PU $ <?php echo moneda_local($d->dq_precio/$dq_cambio,$dn_decimales) ?>">
								<?php echo moneda_local($d->dq_precio*$d->dq_cantidad/$dq_cambio,$dn_decimales) ?>
                            </td>
							<td align="right" title="CU $ <?php echo moneda_local($d->dq_costo/$dq_cambio,$dn_decimales) ?>">
								<?php echo moneda_local($d->dq_costo*$d->dq_cantidad/$dq_cambio,$dn_decimales) ?>
                            </td>
							<td align="right">
								<?php echo moneda_local($d->dq_margen*$d->dq_cantidad/$dq_cambio,$dn_decimales) ?>
                            </td>
							<td align="center">
								<?php echo moneda_local($d->dq_margen_p,2) ?> %
                            </td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">
	js_data.init();
	$('#datatab').tableExport();
</script>