<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$_POST['fv_emision_desde'] = $db->sqlDate($_POST['fv_emision_desde']);
$_POST['fv_emision_hasta'] = $db->sqlDate($_POST['fv_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['fv_emision_desde']} AND {$_POST['fv_emision_hasta']})";

$params = array();

/*if($_POST['fv_numero_desde']){
	if($_POST['fv_numero_hasta']){
		$conditions .= " AND (dq_factura BETWEEN ? AND ?)";
		$params[] = $_POST['fv_numero_desde'];
		$params[] = $_POST['fv_numero_hasta'];
	}else{
		$conditions = "dq_factura = ?";
		$params[] = $_POST['fv_numero_desde'];
	}
}*/

if(isset($_POST['fv_cliente'])){
	$clientes = substr(str_repeat(',?',count($_POST['fv_cliente'])),1);
	$conditions .= " AND dc_cliente IN ({$clientes})";
	$params = array_merge($params,$_POST['fv_cliente']);
}

if(isset($_POST['fv_ejecutivo'])){
	$ejecutivos = substr(str_repeat(',?',count($_POST['fv_ejecutivo'])),1);
	$conditions .= " AND dc_ejecutivo IN ({$ejecutivos})";
	$params = array_merge($params,$_POST['fv_ejecutivo']);
}

$tables_fv = "(SELECT * FROM tb_factura_venta WHERE {$conditions}) fv
LEFT JOIN tb_factura_venta_anulada nul ON nul.dc_factura = fv.dc_factura
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fv.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fv.dc_orden_servicio
LEFT JOIN tb_funcionario ej ON ej.dc_funcionario = fv.dc_ejecutivo
LEFT JOIN tb_tipo_operacion t_op ON t_op.dc_tipo_operacion = fv.dc_tipo_operacion
LEFT JOIN tb_usuario us ON us.dc_usuario = fv.dc_usuario_creacion
JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente";

$fields_fv = 'nv.dq_nota_venta, CONCAT_WS(" ",ej.dg_nombres,ej.dg_ap_paterno,ej.dg_ap_materno) dg_vendedor,
DATE_FORMAT(fv.df_emision,"%d-%m-%Y") df_emision, cl.dg_rut, cl.dg_razon, fv.dq_exento, fv.dq_neto, fv.dq_iva, fv.dq_total,
fv.dq_factura dq_documento,fv.dq_folio,os.dq_orden_servicio,fv.dm_nula, t_op.dg_tipo_operacion dg_tipo, nul.dm_fisica, us.dg_usuario,
DATE_FORMAT(fv.df_libro_venta,"%d-%m-%Y") df_libro_venta';

$tables_nc = "(SELECT * FROM tb_nota_credito WHERE {$conditions}) nc
LEFT JOIN tb_nota_credito_anulada nul ON nul.dc_nota_credito = nc.dc_nota_credito
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = nc.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = nc.dc_orden_servicio
LEFT JOIN tb_funcionario ej ON ej.dc_funcionario = nc.dc_ejecutivo
LEFT JOIN tb_tipo_nota_credito t_nc ON t_nc.dc_tipo_nota_credito = nc.dc_tipo_nota_credito
LEFT JOIN tb_usuario us ON us.dc_usuario = nc.dc_usuario_creacion
JOIN tb_cliente cl ON cl.dc_cliente = nc.dc_cliente";

$fields_nc = 'nv.dq_nota_venta, CONCAT_WS(" ",ej.dg_nombres,ej.dg_ap_paterno,ej.dg_ap_materno) dg_vendedor,
DATE_FORMAT(nc.df_emision,"%d-%m-%Y") df_emision, cl.dg_rut, cl.dg_razon, -nc.dq_exento, -nc.dq_neto, -nc.dq_iva, -nc.dq_total,
nc.dq_nota_credito,nc.dq_folio,os.dq_orden_servicio,nc.dm_nula, t_nc.dg_tipo_nota_credito dg_tipo, nul.dm_fisica, us.dg_usuario,
DATE_FORMAT(nc.df_libro_venta,"%d-%m-%Y")';

$st = $db->prepare(
"(".$db->select($tables_fv,$fields_fv).") UNION ALL (".$db->select($tables_nc,$fields_nc).") ORDER BY df_emision"
);

$params = array_merge($params,$params);

foreach($params as $i => $v){
	$st->bindValue($i+1,$v,PDO::PARAM_INT);
}

$db->stExec($st);

echo('<div class="secc_bar">Libro de venta</div><div class="panes">
<table id="result_libro_venta" class="tab bicolor_tab" width="100%">
<thead><tr>
	<th>FV/NC</th>
	<th>Tipo</th>
	<th>Folio</th>
	<th width="90">Fecha emisión</th>
	<th>DP</th>
	<th>Documento Relacionado</th>
	<th>RUT Cliente</th>
	<th>Cliente</th>
	<th>Responsable</th>
	<th>Exento</th>
	<th>Neto</th>
	<th>IVA</th>
	<th>Total</th>
</tr></thead><tbody>');

$total_exento = 0;
$total_neto = 0;
$total_iva = 0;
$total = 0;

foreach($st as $d){
	
	if($d['dm_fisica'] === '0')
		continue;
	
	$negativo = $d['dq_neto'] < 0;
	
	if(!$negativo){
		$exento = moneda_local($d['dq_exento']);
		$neto = moneda_local($d['dq_neto']);
		$iva = moneda_local($d['dq_iva']);
		$subtotal = moneda_local($d['dq_total']);
	}else{
		$exento =	'<span style="color:#F00">'.moneda_local($d['dq_exento']).'</span>';
		$neto = 	'<span style="color:#F00">'.moneda_local($d['dq_neto']).'</span>';
		$iva = 		'<span style="color:#F00">'.moneda_local($d['dq_iva']).'</span>';
		$subtotal = '<span style="color:#F00">'.moneda_local($d['dq_total']).'</span>';
	}
	
	if($d['dm_nula'] == 0){
		$total_exento += $d['dq_exento'];
		$total_neto += $d['dq_neto'];
		$total_iva += $d['dq_iva'];
		$total += $d['dq_total'];
	}else{
		$exento =	'<span style="color:#666">NULA</span>';
		$neto = 	'<span style="color:#666">NULA</span>';
		$iva = 		'<span style="color:#666">NULA</span>';
		$subtotal = '<span style="color:#666">NULA</span>';
	}
	
	$DP = '';
	if($d['dq_nota_venta'])
		$DP = '<b>NV</b>';
	if($d['dq_orden_servicio'])
		$DP = '<b>OS</b>';
	
	echo("<tr>
		<td><b>{$d['dq_documento']}</b></td>
		<td>{$d['dg_tipo']}</td>
		<td><b>{$d['dq_folio']}</b></td>
		<td>{$d['df_emision']}</td>
		<td>{$DP}</td>
		<td>{$d['dq_nota_venta']}{$d['dq_orden_servicio']}</td>
		<td width='100'>{$d['dg_rut']}</td>
		<td>{$d['dg_razon']}</td>
		<td>{$d['dg_usuario']}</td>
		<td align='right'><b>{$exento}</b></td>
		<td align='right'><b>{$neto}</b></td>
		<td align='right'><b>{$iva}</b></td>
		<td align='right'><b>{$subtotal}</b></td>
	</tr>");
}

$total_exento = moneda_local($total_exento);
$total_neto = moneda_local($total_neto);
$total_iva = moneda_local($total_iva);
$total = moneda_local($total);

echo("</tbody><tfoot>
	<tr>
		<th colspan='9' align='right'>Totales</th>
		<th align='right'>{$total_exento}</th>
		<th align='right'>{$total_neto}</th>
		<th align='right'>{$total_iva}</th>
		<th align='right'>{$total}</th>
	</tr>
</tfoot></table></div>");
?>
<script type="text/javascript">
	window.setTimeout(function(){
		$('#result_libro_venta').tableExport().tableAdjust(20);
	},100);
</script>