<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select('tb_orden_servicio','dc_orden_servicio,dg_glosa_facturacion, dg_solucion',
"dc_empresa = {$empresa} AND dq_orden_servicio={$_GET['number']} AND dm_facturable= '1' AND dm_estado = '1'");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}
$id = $id[0];

$detalle = $db->select("(SELECT * FROM tb_orden_servicio_factura_detalle WHERE dc_orden_servicio = {$id['dc_orden_servicio']}) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto",
'p.dg_codigo,IF(d.dg_descripcion="",p.dg_producto,d.dg_descripcion) dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,p.dc_producto,d.dg_serie,p.dm_requiere_serie');

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>