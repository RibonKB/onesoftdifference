<?php
define("MAIN",1);
require_once("../../../../inc/init.php");


$anular = $db->prepare($db->select('tb_factura_venta_anulada' , 'dm_fisica' , 'dc_factura=?'));
$anular->bindValue(1,$_POST['id'],PDO::PARAM_INT);
$db->stExec($anular);

$anular = $anular->fetch(FETCH::OBJ);

if($anular === false){
	$error_man->showWarning("No se ha encontrado la factura especificada o no esta anulada");
	exit;
}

if($anular->dm_fisica == 1){
	$db->update('tb_factura_venta_anulada' , array('dm_fisica'=>0) , 'dc_factura=?' );
}
?>