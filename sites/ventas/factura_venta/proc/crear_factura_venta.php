<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once('../../proc/ventas_functions.php');
require_once("../../../contabilidad/stuff.class.php");

if(!is_numeric($_POST['fv_number'])){
	$error_man->showWarning("El folio de la factura debe ser un número válido");
	exit;
}

/*$valida_numero = $db->doQuery($db->select('tb_factura_venta','1',"dq_folio={$_POST['fv_number']} AND dc_empresa={$empresa} AND dm_nula = '0'"))->fetch();

if($valida_numero !== false){
	$error_man->showWarning("El número de factura <b>{$_POST['fv_number']}</b> es inválido pues ya existe una factura con dicho número.");
	exit();
}*/

$_POST['fv_tipo_operacion'] = explode('|',$_POST['fv_tipo_operacion']);
$tipo_operacion = $_POST['fv_tipo_operacion'][0];
$tipo_movimiento = $_POST['fv_tipo_operacion'][1];
$tipo_guia = $_POST['fv_tipo_operacion'][2];
$dm_anticipada = 'N';
//Se agrega la fecha de la orden de compra inicializada como null de SQL
$ordenCompra = $_POST['fv_orden_compra'];
$fechaOrdenCompra = $_POST['fv_fecha_orden_compra'];


if(!empty($ordenCompra)){
	if(empty($fechaOrdenCompra)){
		$error_man->showWarning('No se ha registrado la fecha de la orden de compra. Verifique su información e intente nuevamente.');
		exit;
	}
} else {
	$fechaOrdenCompra = 'NULL';

}

/**
*	Comprobar Estado del periodo contable en el que se agregará la factura de compra
**/

$mes_contable = substr($_POST['fv_fecha_contable'],3,2);
$anho_contable = substr($_POST['fv_fecha_contable'],6);

$dm_estado_periodo_contable = ContabilidadStuff::getEstadoPeriodo($mes_contable,$anho_contable,ContabilidadStuff::FIELD_ESTADO_FV);

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_INEXISTENTE){
	$error_man->showWarning('El periodo contable en el que intenta agregar la factura de venta no existe');
	exit;
}

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_CERRADO){
	$error_man->showWarning('El periodo contable en el que intenta agregar la factura de venta se encuentra cerrado.');
	exit;
}

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_GRUPO_DENEGADO){
	$error_man->showWarning('Su grupo de usuario no tiene permiso para generar facturas de venta en el periodo contable seleccionado.');
	exit;
}

if($dm_estado_periodo_contable == ContabilidadStuff::ERROR_PERIODO_USUARIO_DENEGADO){
	$error_man->showWarning('No tiene permiso para generar facturas de venta en el periodo contable seleccionado.');
	exit;
}

//END ESTADO CONTABLE

/**
*	Obtener los productos que realizan movimientos de stock
*	
*	En el caso en que al menos un producto expuesto en el detalle realice movimiento de stock es candidato a ser un detalle de guía virtual
**/
//los codigos ingresados en el detalle de factura
$codes = implode("','",$_POST['prod']);
//array en el que se almacenarán los ids de productos que mueven stock
$ids = array();
//array en que se almacenarán los costos de bodega de los productos que mueven stock
$dq_costo_stock = array();

//array en el que se almacenarán los ids de productos que no mueven stock
$ns_ids = array();

//se consulta por productos que muevan stock
$db_prod = $db->doQuery($db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_codigo IN ('{$codes}')) p
JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto",
'p.dc_producto,p.dg_codigo,t.dm_controla_inventario,p.dq_precio_compra'));

//se le da formato a la estructura de datos para que sea facilmente manipulable y accesible
foreach($db_prod as $p){
	if($p['dm_controla_inventario'] == 0){
		$ids[$p['dg_codigo']] = $p['dc_producto'];
		$dq_costo_stock[$p['dc_producto']] = $p['dq_precio_compra'];
	}else
		$ns_ids[$p['dg_codigo']] = $p['dc_producto'];
	
}
unset($db_prod);

//En el caso de que al menos uno de los productos en el detalle mueva stock es candidato a hacer guía virtual con ese detalle
$virtual = !isset($_POST["doc_type"]);// || (isset($_POST['doc_type']) && $_POST['doc_type'] == 1)/**/);


/**
*	Comprobar stock
*	
*	Primero se filtran los productos que realizan movimientos de stock, estos serán los que se utilizarán en los procesos donde el stock y cantidades deben ser validadas
*	Se verifica que la cantidad a facturar satisfaga la cantidad reservada para cada detalle de la nota de venta.
*	Para esto se verifica solo las
*	cantidades pendientes de despacho (cantidad a facturar menos cantidad ya despachada)
*	cantidad disponibles/reservadas restantes (Cantidad recepcionada menos cantidad ya despachada)
**/

if($virtual){
	$bodega = $_POST['fv_tipo_operacion'][3];
	$guia_tipo_movimiento = $_POST['fv_tipo_operacion'][4];
	
	$tipo_verificacion = $_POST['fv_tipo_operacion'][5];
	
	$to_guia = array();
	$anticipados = array();
	$_POST['guia_cant'] = array();
	
	if($_POST['fv_id_nota_venta'] != 0){
		foreach($_POST['prod'] as $i => $p){
				$pendiente = $_POST['cant'][$i]-$_POST['despachada'][$i];
				
				if($pendiente > 0){
					$disponible = $_POST['recepcionada'][$i]-$_POST['despachada'][$i];
					
					if($pendiente > $disponible){
						/**
						*	Rescatar detalle erroneo
						*	
						*	En caso de que no haya stock reservado suficiente se acumulan los detalles con error.
						**/
						
						$anticipados[] = $i;
					}//END if pendiente > disponible
					
					$to_guia[] = $i;
					//La cantidad a despachar siempre es igual a la cantidad disponible para la nota de venta, a menos que se haya pedido facturar parcialmente
					//en cuyo caso la cantidad a facturar será igual a la cantidad especificada en el formulario.
					if($disponible > $_POST['cant'][$i])
						$_POST['guia_cant'][$i] = $_POST['cant'][$i];
					else
						$_POST['guia_cant'][$i] = $disponible;
					
				}//END if pendiente

		}//END foreach products
	}//END if $_POST['fv_id_nota_venta']
	else{
		//Si no se ha asignado nota de venta pero si hay productos que mueven stock entonces hay que revisar si hay stock libre para asignarle.
		
		foreach($_POST['prod'] as $i => $p){

				$to_guia[] = $i;
				$_POST['guia_cant'][$i] = $_POST['cant'][$i];

		}
		
	}//END if not nota_venta
	
	if(!isset($_POST['anticipada']) && $tipo_verificacion == 0 && count($to_guia) && $_POST['fv_id_nota_venta'] != 0){
		
		if(count($anticipados)){
			$table  = '<table class="tab" width="75%" align="center"0><caption>Productos sin reserva de stock</caption><thead>';
			$table .= '<th width="150">Código</th>';
			$table .= '<th>Descripción</th>';
			$table .= '<th width="100">Cantidad</th>';
			$table .= '<th width="100">Cantidad disponible</th>';
			$table .= '<th width="100">Cantidad anticipada</th></thead><tbody>';
			
			foreach($anticipados as $i){
				
				$disponible = $_POST['recepcionada'][$i]-$_POST['despachada'][$i];
				$anticipada = $_POST['cant'][$i]-($_POST['recepcionada'][$i]-$_POST['despachada'][$i]);
				
				$table .= "<tr>";
				$table .= "<td><b>{$_POST['prod'][$i]}</b></td>";
				$table .= "<td>{$_POST['desc'][$i]}</td>";
				$table .= "<td>{$_POST['cant'][$i]}</td>";
				$table .= "<td>{$disponible}</td>";
				$table .= "<td>{$anticipada}</td>";
				$table .= "</tr>";
			}
			
			$table .= '</tbody></table><br />';
		}else{
			$table = '';
		}
		
		if(!check_permiso(28) && count($anticipados)){
			$error_man->showWarning('<strong>No hay stock reservado suficiente para la nota de venta</strong><br />
			Debe hacer las compras, recepciones y/o traspasos necesarios para satisfacer la necesidad de stock<br />
			No posee los permisos necesarios para facturar anticipadamente<br />
			A continuación se muestra el detalle de los productos que ocasionaron el error<br /><br />'.$table);
		}else {
			
			if(!count($anticipados))
				$btnMsg = "Facturar y despachar stock disponible";
			else
				$btnMsg = "Facturar anticipado despachando el stock disponible";
			
			$error_man->showInfo($table.'<div class="center">
				<strong>No hay stock reservado suficiente para la nota de venta</strong>
				<br />Puede facturar anticipado en los siguientes modos:
				<br /><br />
				<button id="facturar_anticipado_parcial" class="button">'.$btnMsg.'</button>
				<br /><br />
				<button id="facturar_anticipado_completa" class="button">Factura anticipada completa</button>
			</div>');
			echo('<script type="text/javascript">
				set_fact_anticipada();
			</script>');
		}
		
		exit;
	}
	
	if(isset($_POST['anticipada'])){
		if($_POST['anticipada'] == 1 || !count($to_guia)){
			$virtual = false;
		}else
			$virtual = true;
			
	}else if($tipo_verificacion == 1){
		$virtual = false;
	}
	
	if($tipo_verificacion == 0){
		$dm_anticipada = isset($_POST['anticipada'])?(($virtual && !count(array_diff_assoc($_POST['guia_cant'],$_POST['cant'])))?'N':'A'):'N';
	}else if($tipo_verificacion == 1){
		$dm_anticipada = count($to_guia)?'A':'N';
	}
	
}//END if $virtual

$db->start_transaction();
/**
*	Rebajar stock
*	
*	Luego de verificado el stock reservado se hace la rebaja de stock correspondiente de la bodega
*	Esto también puede generar error si no hay stock suficiente, para ello se debe realizar una nueva recepción general (sin reservar)	
**/

if($virtual){
	if($_POST['fv_id_nota_venta'] != 0){
		foreach($to_guia as $i){
			$cantidad = $_POST['guia_cant'][$i];
			
			if($cantidad == 0 || isset($ns_ids[$_POST['prod'][$i]])){
				continue;
			}
			
			$existencia = bodega_RebajarStockReservado($ids[$_POST['prod'][$i]],$cantidad,$bodega);
			
			if($existencia != 1){
				
				$nombre_bodega = $db->doQuery($db->select('tb_bodega','dg_bodega',"dc_bodega={$bodega}"))->fetch(PDO::FETCH_OBJ)->dg_bodega;
				
				$error_man->showWarning("Hay stock reservado suficiente para la nota de venta pero no se encontró stock disponible en bodega {$nombre_bodega}<br />
				Error detectado en el detalle <b>{$_POST['prod'][$i]}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />
				Cantidad necesaria : <b>{$cantidad}</b><br />
				Bodega en que se busca: <b>{$nombre_bodega}</b>");
				
				$db->rollBack();
				exit;
			}
		}//END foreach to_guia
	}//END if nota venta
	else{
		//Movimiento de stock si no hubiese asignación de nota de venta

		foreach($to_guia as $i){
			
			$cantidad = $_POST['guia_cant'][$i];
			
			if($cantidad == 0 || isset($ns_ids[$_POST['prod'][$i]])){
				continue;
			}
			
			$existencia = bodega_RebajarStockLibre($ids[$_POST['prod'][$i]],$cantidad,$bodega);
			
			if($existencia != 1){
				$nombre_bodega = $db->doQuery($db->select('tb_bodega','dg_bodega',"dc_bodega={$bodega}"))->fetch(PDO::FETCH_OBJ)->dg_bodega;
				
				$error_man->showWarning("No hay stock libre suficiente en la bodega <b>{$nombre_bodega}</b><br />
				Error detectado en el detalle <b>{$_POST['prod'][$i]}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />
				Cantidad necesaria : <b>{$cantidad}</b><br />
				Bodega en que se busca: <b>{$nombre_bodega}</b>");
				
				$db->rollBack();
				exit;
			}
		}
		
	}
}//END if virtual

/**
*	Almacenar Factura
*	
*	Ya rebajados los stocks se procede a almacenar la factura, para obtener el correlativo generado dependencia para los siguientes procesos
**/

$correlativo = doc_GetNextNumber('tb_factura_venta','dq_factura',2,'df_creacion');

$factura = $db->prepare($db->insert('tb_factura_venta',array(
	'dc_nota_venta' => $_POST['fv_id_nota_venta'],
	'dc_orden_servicio' => $_POST['fv_id_orden_servicio'],
	'dc_ejecutivo' => $_POST['fv_ejecutivo'],
	'dc_cliente' => $_POST['cli_id'],
	'dc_contacto' => $_POST['fv_contacto'],
	'dc_contacto_entrega' => $_POST['fv_cont_entrega'],
	'dc_medio_pago' => $_POST['fv_medio_pago'],
	'dc_tipo_cargo' => $_POST['fv_tipo_cargo'],
	'dc_tipo_operacion' => $tipo_operacion,
	'dq_factura' => $correlativo,
	'dq_folio' => '?',
	'dg_comentario' => '?',
	'df_emision' => $db->sqlDate($_POST['fv_emision']),
	'df_creacion' => $db->getNow(),
	'df_vencimiento' => $db->sqlDate($_POST['fv_vencimiento']),
	'df_libro_venta' => $db->sqlDate($_POST['fv_fecha_contable']),
	'dg_orden_compra' => '?',
	'df_fecha_orden_compra' => '?',
	'dq_neto' => $_POST['cot_neto'],
	'dq_iva' => $_POST['cot_iva'],
	'dq_total' => $_POST['cot_neto']+$_POST['cot_iva'],
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa,
	'dm_tipo_impresion' => $_POST['fv_tipo_impresion'],
	'dm_anticipada' => '?'
)));

$factura->bindValue(1,$_POST['fv_number'],PDO::PARAM_INT);
$factura->bindValue(2,$_POST['fv_comentario'],PDO::PARAM_STR);
$factura->bindValue(3,$ordenCompra,PDO::PARAM_STR);
$factura->bindValue(5,$dm_anticipada,PDO::PARAM_STR);
//Si la fecha no se ha asignado (si la orden de compra está vacía) o si la fecha es NULL.
if($fechaOrdenCompra === 'NULL'){
	$factura->bindValue(4,null,PDO::PARAM_NULL);
} else {
	$factura->bindValue(4,$db->sqlDate2($fechaOrdenCompra),PDO::PARAM_STR);
}


$db->stExec($factura);

$factura = $db->lastInsertId();

/**
*	Almacenar detalle factura
*	
*	Como ya se han realizado las validaciones de stock necesarias se puede directamente almacenar los detalles de la factura
**/

$det_st = $db->prepare($db->insert('tb_factura_venta_detalle',array(
	'dc_factura' => $factura,
	'dg_producto' => '?',
	'dc_detalle_nota_venta' => '?',
	'dg_descripcion' => '?',
	'dm_tipo' => '?',
	'dc_cantidad' => '?',
	'dq_precio' => '?',
	'dq_descuento' => '?',
	'dq_total' => '?',
	'dq_costo' => '?'
)));

$det_st->bindParam(1,$producto,PDO::PARAM_STR);
$det_st->bindParam(2,$detalle_nv,PDO::PARAM_INT);
$det_st->bindParam(3,$descripcion,PDO::PARAM_STR);
$det_st->bindParam(4,$tipo,PDO::PARAM_STR);
$det_st->bindParam(5,$cantidad,PDO::PARAM_INT);
$det_st->bindParam(6,$precio,PDO::PARAM_STR);
$det_st->bindParam(7,$descuento,PDO::PARAM_INT);
$det_st->bindParam(8,$total,PDO::PARAM_INT);
$det_st->bindParam(9,$costo,PDO::PARAM_STR);

$tipo = 0;
$from_nv = isset($_POST['id_detail']);
foreach($_POST['prod'] as $i => $v){
	
	$producto = $v;
	$detalle_nv = $from_nv?$_POST['id_detail'][$i]:0;
	$descripcion = $_POST['desc'][$i];
	$cantidad = str_replace(',','',$_POST['cant'][$i]);
	$precio = str_replace(',','',$_POST['precio'][$i]);
	$costo = str_replace(',','',$_POST['costo'][$i]);
	$descuento = str_replace(',','',$_POST['descuento'][$i]);
	$total = $precio*$cantidad-$descuento;
	
	$db->stExec($det_st);
}

if(isset($_POST['fake_prod'])){
	$tipo = 1;
	foreach($_POST['fake_prod'] as $i => $v){
		$producto = $v;
		$detalle_nv = 0;
		$descripcion = $_POST['fake_desc'][$i];
		$cantidad = str_replace(',','',$_POST['fake_cant'][$i]);
		$precio = str_replace(',','',$_POST['fake_precio'][$i]);
		$descuento = str_replace(',','',$_POST['fake_descuento'][$i]);
		$total = $precio*$cantidad-$descuento;
		
		$db->stExec($det_st);
	}
}//END Almacenar detalle glosa


/**
 * Verificar si la nota de venta posee alguna asignación de costo prévia a la creación de la factura
 * 
 */

if($_POST['fv_id_nota_venta'] != 0){
    $caTable = 'tb_factura_compra_asignacion_costos';
    $caFields = 'dc_asignacion, dc_factura_compra, dc_nota_venta, dc_cantidad_cuotas, dq_monto';
    $caConditions = 'dc_cantidad_cuotas > dc_cuotas_saldadas AND dc_nota_venta = :notaVenta';
    $caSt = $db->prepare($db->select($caTable, $caFields, $caConditions));
    $caSt->bindValue(':notaVenta', $_POST['fv_id_nota_venta'], PDO::PARAM_INT);
    $db->stExec($caSt);
    $asignada = $caSt->fetch(PDO::FETCH_OBJ);
    unset($caSt);
    if(!empty($asignada)){
        //actualizar comprobante.
        $uacFields = array(
            'dc_cuotas_saldadas' => 'dc_cuotas_saldadas + 1'
        );
        $uacConditions = 'dc_asignacion = :asignacion';
        $uac = $db->prepare($db->update($caTable, $uacFields, $uacConditions));
        $uac->bindValue(':asignacion', $asignada->dc_asignacion, PDO::PARAM_INT);
        $db->stExec($uac);
        //insertar detalle comprobante
        $cacTable = 'tb_comprobante_asignacion_costos';
        $cacFields = array(
            'dc_asignacion_costo' => ':asignacion',
            'dc_factura_compra' => ':dc_factura_compra',
            'dc_factura_venta' => ':dc_factura_venta',
            'dq_monto' => ':dq_monto',
        );
        $cacMonto = round(floatval($asignada->dq_monto / $asignada->dc_cantidad_cuotas), 2);
        $cac = $db->prepare($db->insert($cacTable, $cacFields));
        $cac->bindValue(':asignacion', $asignada->dc_asignacion, PDO::PARAM_INT);
        $cac->bindValue(':dc_factura_compra', $asignada->dc_factura_compra, PDO::PARAM_INT);
        $cac->bindValue(':dc_factura_venta', $factura, PDO::PARAM_INT);
        $cac->bindValue(':dq_monto', $cacMonto, PDO::PARAM_STR);
        $db->stExec($cac);
    }

}


/**
*	Rebajar stock de bodegas de entrada para guías de traslado no facturables
*	
*	Al facturar por ejemplo un proyecto se realizaron diferentes guías de traslado que dejaron stock en bodegas temporales (p.e. bodega de cliente o de proyectos)
*	Este stock debe ser sacado de esas bodegas y se considera que son parte del cliente e incluidos con los costos de la factura.
**/

if($_POST['fv_id_nota_venta'] != 0){
	
	$guias_traslado = $db->doQuery($db->select(
	'('.$db->select("(SELECT * FROM tb_guia_despacho WHERE dc_nota_venta = {$_POST['fv_id_nota_venta']} AND dc_factura = 0 AND dm_nula = 0) gd
	JOIN tb_tipo_guia_despacho t ON t.dc_tipo = gd.dc_tipo_guia AND t.dm_facturable = 0
	JOIN tb_guia_despacho_detalle d ON d.dc_guia_despacho = gd.dc_guia_despacho AND d.dc_bodega_entrada > 0
	LEFT JOIN (SELECT dc_detalle_guia, SUM(dq_cantidad) dq_cantidad_devuelta
			FROM tb_devolucion_guia_despacho_detalle
			GROUP BY dc_detalle_guia) dev ON dev.dc_detalle_guia = d.dc_detalle
	JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
	JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto AND tp.dm_controla_inventario = 0",
	'p.dc_producto, p.dg_producto, p.dg_codigo, d.dq_cantidad, d.dc_bodega_entrada, dev.dq_cantidad_devuelta')
	.' UNION '.
	$db->select("(SELECT * FROM tb_orden_servicio WHERE dc_nota_venta = {$_POST['fv_id_nota_venta']}) os
	JOIN tb_guia_despacho gd ON os.dc_orden_servicio = gd.dc_orden_servicio AND gd.dc_factura = 0 AND gd.dm_nula = 0
	JOIN tb_tipo_guia_despacho t ON t.dc_tipo = gd.dc_tipo_guia AND t.dm_facturable = 0
	JOIN tb_guia_despacho_detalle d ON d.dc_guia_despacho = gd.dc_guia_despacho AND d.dc_bodega_entrada > 0
	LEFT JOIN (SELECT dc_detalle_guia, SUM(dq_cantidad) dq_cantidad_devuelta
			FROM tb_devolucion_guia_despacho_detalle
			GROUP BY dc_detalle_guia) dev ON dev.dc_detalle_guia = d.dc_detalle
	JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
	JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto AND tp.dm_controla_inventario = 0",
	'p.dc_producto, p.dg_producto, p.dg_codigo, d.dq_cantidad, d.dc_bodega_entrada, dev.dq_cantidad_devuelta').') t',
	't.dc_producto, t.dg_producto, t.dg_codigo, SUM(t.dq_cantidad) dq_cantidad, t.dc_bodega_entrada, SUM(dq_cantidad_devuelta) dq_cantidad_devuelta','',
	array('group_by' => 't.dc_producto, t.dc_bodega_entrada')))->fetchAll(PDO::FETCH_ASSOC);
	
	foreach($guias_traslado as &$gt){
		$gt['dq_cantidad_devuelta'] = intval($gt['dq_cantidad_devuelta']);
		$gt['dq_cantidad'] -= $gt['dq_cantidad_devuelta'];
		if($gt['dq_cantidad'] < 1){
			unset($gt);
		}
	}
	
	if(count($guias_traslado) && $guias_traslado[0]['dq_cantidad'] != NULL){
		
		$guia_liberacion = $db->prepare($db->insert('tb_guia_despacho',array(
			'dc_tipo_guia' => $tipo_guia,
			'dc_nota_venta' => $_POST['fv_id_nota_venta'],
			'dc_orden_servicio' => $_POST['fv_id_orden_servicio'],
			'dc_factura' => $factura,
			'dc_cliente' => $_POST['cli_id'],
			'dc_contacto' => $_POST['fv_contacto'],
			'dc_contacto_entrega' => $_POST['fv_cont_entrega'],
			'dq_guia_despacho' => '?',
			'dg_comentario' => '?',
			'dc_usuario_creacion' => $idUsuario,
			'df_emision' => $db->getNow(),
			'dc_empresa' => $empresa
		)));
		
		$guia_liberacion->bindValue(1,doc_GetNextNumber('tb_guia_despacho','dq_guia_despacho'),PDO::PARAM_INT);
		$guia_liberacion->bindValue(2,'Guía de liberación de stocks en traslado',PDO::PARAM_STR);
		
		$db->stExec($guia_liberacion);
		
		$guia_liberacion = $db->lastInsertId();
		
		/**
		*	Detalles guia de liberacion de stocks
		**/
		
		$detalle_template = $db->prepare($db->insert('tb_guia_despacho_detalle',array(
			'dc_guia_despacho' => '?',
			'dg_producto' => '?',
			'dg_descripcion' => '?',
			'dc_bodega_salida' => '?',
			'dq_cantidad' => '?'
		)));
		
		$detalle_template->bindValue(1,$guia_liberacion,PDO::PARAM_STR);
		$detalle_template->bindParam(2,$producto,PDO::PARAM_STR);
		$detalle_template->bindParam(3,$descripcion,PDO::PARAM_STR);
		$detalle_template->bindParam(4,$bodega_liberar,PDO::PARAM_INT);
		$detalle_template->bindParam(5,$cantidad,PDO::PARAM_INT);

		foreach($guias_traslado as $g){
			if($g['dq_cantidad'] == 0){
				continue;
			}
			
			$existencia = bodega_RebajarStockLibre($g['dc_producto'],$g['dq_cantidad'],$g['dc_bodega_entrada']);
			
			if($existencia != 1){
				
				$nombre_bodega = $db->doQuery($db->select('tb_bodega','dg_bodega',"dc_bodega={$g['dc_bodega_entrada']}"))->fetch(PDO::FETCH_OBJ)->dg_bodega;
				
				$error_man->showWarning("No se ha podido liberar el stock de la bodega <b>{$nombre_bodega}</b> recepcionada por una guía de traslado<br />
					Error detectado en el detalle <b>{$g['dg_codigo']}</b> - <i>{$g['dg_producto']}</i><br /><br />
					Cantidad necesaria : <b>{$g['dq_cantidad']}</b><br />
					Bodega en que se busca: <b>{$nombre_bodega}</b>");
				
				$db->rollBack();
				exit;
			}
			
			$producto = $g['dg_codigo'];
			$descripcion = '[LIB] '.$g['dg_producto'];
			$bodega_liberar = $g['dc_bodega_entrada'];
			$cantidad = $g['dq_cantidad'];
			
			$db->stExec($detalle_template);
			
		}
		
		unset($detalle_template);
	}
}

/**
*	Asignar factura a guías físicas de la nota de venta
*	
*	En caso de que la nota de venta haya sido despachada anteriormente con guías físicas estas deben quedar relacionadas con la factura de venta.
*	
*	IN: id_nota_venta, factura
*	OUT: Guías de despacho físicas de la nota de venta con factura asignada.
**/

if($_POST['fv_id_nota_venta'] != 0){
	$guias_facturadas = $db->doExec($db->update('tb_guia_despacho',
	array('dc_factura' => $factura),
	"dc_nota_venta = {$_POST['fv_id_nota_venta']} AND dc_factura = 0"));
}


/**
*	Generar Guía Virtual
*	
*	Las guías virtuales son generadas para las cantidades pendientes de despacho (Cantidad a facturar menos cantidades ya despachadas)
*	
**/

if($virtual && count($to_guia)){
	
	$num_actual = doc_GetNextNumber('tb_guia_despacho','dq_guia_despacho');
	
	$guia_template = $db->prepare($db->insert('tb_guia_despacho',array(
		'dc_tipo_guia' => $tipo_guia,
		'dc_nota_venta' => $_POST['fv_id_nota_venta'],
		'dc_orden_servicio' => $_POST['fv_id_orden_servicio'],
		'dc_factura' => $factura,
		'dc_cliente' => $_POST['cli_id'],
		'dc_contacto' => $_POST['fv_contacto'],
		'dc_contacto_entrega' => $_POST['fv_cont_entrega'],
		'dq_guia_despacho' => '?',
		'dg_comentario' => '?',
		'df_emision' => $db->getNow(),
		'dg_orden_compra' => '?',
		'df_fecha_orden_compra' => '?',
		'dq_neto' => $_POST['cot_neto'],
		'dq_iva' => $_POST['cot_iva'],
		'dq_total' => $_POST['cot_neto']+$_POST['cot_iva'],
		'dc_usuario_creacion' => $idUsuario,
		'dc_empresa' => $empresa
	)));
	
	$guia_template->bindParam(1,$num_actual,PDO::PARAM_INT);
	$guia_template->bindValue(2,"Guía virtual generada automáticamente para la factura {$_POST['fv_number']}",PDO::PARAM_STR);
	$guia_template->bindValue(3,$ordenCompra,PDO::PARAM_STR);
	
	if($fechaOrdenCompra === 'NULL'){
		$guia_template->bindValue(4,$fechaOrdenCompra,PDO::PARAM_NULL);
	} else {
		$guia_template->bindValue(4,$db->sqlDate($fechaOrdenCompra),PDO::PARAM_STR);
	}
	
	$detalle_template = $db->prepare($db->insert('tb_guia_despacho_detalle',array(
		'dc_guia_despacho' => '?',
		'dg_producto' => '?',
		'dc_detalle_nota_venta' => '?',
		'dg_descripcion' => '?',
		'dg_serie' => '?',
		'dq_precio' => '?',
		'dq_costo' => '?',
		'dc_bodega_salida' => $bodega
	)));
	
	$detalle_template->bindParam(1,$id_actual,PDO::PARAM_STR);
	$detalle_template->bindParam(2,$producto,PDO::PARAM_STR);
	$detalle_template->bindParam(3,$detalle_nv,PDO::PARAM_INT);
	$detalle_template->bindParam(4,$descripcion,PDO::PARAM_STR);
	$detalle_template->bindParam(5,$serie,PDO::PARAM_STR);
	$detalle_template->bindParam(6,$precio,PDO::PARAM_STR);
	$detalle_template->bindParam(7,$costo,PDO::PARAM_STR);
	
	$db->stExec($guia_template);
	$num_guia =& $num_actual;
	$id_actual = $db->lastInsertId();
	
	$num_detalles = 0;
	$movs = array();
	//$num_guias = array();
	foreach($to_guia as $i){
		//Si los detalles son de una nota de venta solo hay que realizar guías de las cantidades no despachadas
		
		$cantidad = $_POST['guia_cant'][$i];
		
		$series = explode(',',$_POST['serie'][$i]);
		
		$producto = $_POST['prod'][$i];
		$detalle_nv = $from_nv?$_POST['id_detail'][$i]:0;
		$descripcion = $_POST['desc'][$i];
		$precio = str_replace(',','',$_POST['precio'][$i]);
		$costo = str_replace(',','',$_POST['costo'][$i]);
		
		if(isset($ids[$producto])){
			$movs[$producto] = array();
			$movs[$producto]['guias'] = array();
			$movs[$producto]['precio'] = $precio;
		}
		
		
		
		
		for($j=0;$j<$cantidad;$j++){
			/*if($num_detalles%20 == 0){
				$db->stExec($guia_template);
				$num_guias[] = $num_actual;
				$num_actual = doc_GetRealNext($num_actual);
				$id_actual = $db->lastInsertId();
			}
			$num_detalles++;*/
			
			if(isset($series[$j])){
				$serie = $series[$j];
			}else{
				$serie = '';
			}
			
			if(isset($ids[$producto]))
				$movs[$producto]['guias'][$id_actual][] = $serie;
			
			$db->stExec($detalle_template);
			
			
		}
	}
}//END if se hacen guías virtuales

/**
*	Registrar movimientos de bodega
*	
*	Se registran los despachos extras que se realizaron para satisfacer las cantidades pendientes
**/

if($virtual && count($to_guia) && count($movs)){
	
	$template_mov = $db->prepare($db->insert('tb_movimiento_bodega',array(
		'dc_cliente' => $_POST['cli_id'],
		'dc_tipo_movimiento' => $guia_tipo_movimiento,
		'dc_bodega_salida' => $bodega,
		'dq_monto' => '?',
		'dc_guia_despacho' => '?',
		'dg_series' => '?',
		'dc_nota_venta' => $_POST['fv_id_nota_venta'],
		'dc_producto' => '?',
		'dq_cantidad' => '?',
		'dc_empresa' => $empresa,
		'df_creacion' => $db->getNow(),
		'dc_usuario_creacion' => $idUsuario,
		'dc_factura' => $factura
	)));

	$template_mov->bindParam(1,$monto,PDO::PARAM_INT);
	$template_mov->bindParam(2,$guia,PDO::PARAM_INT);
	$template_mov->bindParam(3,$series_str,PDO::PARAM_STR);
	$template_mov->bindParam(4,$producto,PDO::PARAM_INT);
	$template_mov->bindParam(5,$cantidad,PDO::PARAM_INT);
	
	foreach($movs as $codigo => $v){
		
	$producto = $ids[$codigo];
	$monto = $dq_costo_stock[$producto];
	
	foreach($v['guias'] as $guia_actual => $arr){
			$series_str = implode(',',array_diff($arr,array('')));
			$cantidad = count($arr)*-1;
			$guia = $guia_actual;
			
			$db->stExec($template_mov);
		}
	}
}


/**
*	Asignación de cantidades Facturadas
**/

if($_POST['fv_id_nota_venta'] != 0 && $from_nv){
	/**
	*	Asignar cantidades facturadas a detalles de la nota de venta
	*	
	*	Para saber cuantas cantidades faltan aún por facturar en caso de que la nota de venta sea facturada multiples veces
	*	También sirve para validar si una nota de venta está pendiente de facturación.
	**/

	$factured_template = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dc_facturada' => 'dc_facturada+?'
	),"dc_nota_venta_detalle=?"));
	
	$factured_template->bindParam(1,$facturado,PDO::PARAM_INT);
	$factured_template->bindParam(2,$id_detalle,PDO::PARAM_INT);
	
	$total_facturado = 0;
	
	foreach($_POST['prod'] as $i => $v){
		$facturado = $_POST['cant'][$i];
		$id_detalle = $_POST['id_detail'][$i];
		
		$db->stExec($factured_template);
		
		$total_facturado += $facturado;
	}

	/**
	*	Asignar Cantidad facturada a nota de venta
	*	
	*	Al igual que en el proceso anterior pero a nivel de cabecera para tener una idea global de cantidades pendientes de facturación
	**/

	if($total_facturado > 0){
		$nv_factured = $db->doExec($db->update('tb_nota_venta',array(
			'dc_facturada' => "dc_facturada+{$total_facturado}"
		),"dc_nota_venta = {$_POST['fv_id_nota_venta']}"));
		
		if($nv_factured == 0){
			$error_man->showWarning('Ocurrió un error intentando relacionar la factura con la nota de venta, compruebe la existencia de la nota de venta.');
			$db->rollBack();
			exit;
		}
	}
	
}//END if asignar cantidades facturadas a nota de venta


/**
*	Asignación de cantidades despachadas guia de despacho y asignación de facturas a guías ya asignadas
**/
if($_POST['fv_id_nota_venta'] != 0 && $from_nv && $virtual && count($to_guia)){

	/**
	*	Asignar cantidad despachada a detalles nota de venta
	*	
	*	En caso de que se hayan realizado guías de despacho estas deben ser consideradas en las cantidades despachas de los detalles de la nota de venta
	*	Para saber las cantidades pendientes de despacho en futuras facturaciones/despachos
	**/
	
	$dispatch_template = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dc_despachada' => 'dc_despachada+?'
	),"dc_nota_venta_detalle=?"));
	
	$dispatch_template->bindParam(1,$despachado,PDO::PARAM_INT);
	$dispatch_template->bindParam(2,$id_detalle,PDO::PARAM_INT);
	
	$total_despachado = 0;
	
	foreach($to_guia as $i){
		$despachado = $_POST['guia_cant'][$i];
		$id_detalle = $_POST['id_detail'][$i];
		
		$db->stExec($dispatch_template);
		
		$total_despachado += $despachado;
	}
	
	
	/**
	*	Asignar cantidad despachada a nota de venta
	*	
	*	El mismo proceso anterior pero a nivel de cabecera, para tener una idea global de las cantidades pendientes de despacho.
	**/
	
	if($total_despachado > 0){
		$nv_dispatch = $db->doExec($db->update('tb_nota_venta',array(
			'dc_despachada' => "dc_despachada+{$total_despachado}"
		),"dc_nota_venta = {$_POST['fv_id_nota_venta']}"));
		
		if($nv_dispatch == 0){
			$error_man->showWarning('Ocurrió un error intentando relacionar las guías virtuales con la nota de venta, compruebe la existencia de la nota de venta.');
			$db->rollBack();
			exit;
		}
	}
	
}//END if asignar cantidades despachadas a nota de venta

	
/**
*	Asignar cantidad facturadas a notas de venta de documentos facturados con el gestor de facturas
*
*	al facturar guías de despacho es necesario asignar las cantidades facturadas a las notas de venta relacionadas a las guías
*	en caso de tener
*	además de asignar a estos documentos la factura recién creada
*	Esto último también a la facturación de múltiples ordenes de servicio
**/

if(isset($_POST["doc_type"])){
	if($_POST['doc_type'] == 0){
		$num_docs = $db->doQuery($db->select("(SELECT * FROM tb_guia_despacho_detalle WHERE dc_guia_despacho IN ({$_POST['doc_list']})) d
		JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta",
		'd.dc_detalle_nota_venta,count(d.dc_detalle_nota_venta) AS dq_cantidad,nvd.dc_nota_venta','',array('group_by' => 'd.dc_detalle_nota_venta')));
		
		$factured_template = $db->prepare($db->update('tb_nota_venta_detalle',array(
			'dc_facturada' => 'dc_facturada+?'
		),"dc_nota_venta_detalle=?"));
		
		$factured_template->bindParam(1,$facturado,PDO::PARAM_INT);
		$factured_template->bindParam(2,$id_detalle,PDO::PARAM_INT);
		
		$notas = array();
		foreach($num_docs as $n){
			$facturado = $n['dq_cantidad'];
			$id_detalle = $n['dc_detalle_nota_venta'];
			
			$db->stExec($factured_template);
			
			$notas[$n['dc_nota_venta']][] = $n['dq_cantidad'];
		}
		
		$factured_template = $db->prepare($db->update('tb_nota_venta',array(
			'dc_facturada' => 'dc_facturada+?'
		),"dc_nota_venta=?"));
		
		$factured_template->bindParam(1,$sum,PDO::PARAM_INT);
		$factured_template->bindParam(2,$indice,PDO::PARAM_INT);
		
		foreach($notas as $i => $n){
			$sum = array_sum($n);
			$indice = $i;
			
			$db->stExec($factured_template);
		}
	}
	
	switch($_POST['doc_type']){
		case 0:
			$db->doExec($db->update('tb_guia_despacho',array('dc_factura' => $factura),"dc_guia_despacho IN ({$_POST['doc_list']})"));
			break;
		case 1:
			$db->doExec($db->update('tb_orden_servicio',array('dc_factura' => $factura),"dc_orden_servicio IN ({$_POST['doc_list']})"));
			break;
		default: $db->rollback(); exit();
	}
}

$db->commit();
// $db->rollBack();
$_POST['fv_number'] = htmlentities($_POST['fv_number']);

$error_man->showConfirm("Se ha almacenado la factura de venta correctamente.");
$error_man->showInfo("El número de folio para factura física es el <strong>{$_POST['fv_number']}</strong><br />
Además de eso el número de identificación interno es el <h1 style='margin:0;color:#000;'>{$correlativo}</h1>
<button type='button' class='button' id='print_factura'>Imprimir</button>");

if(isset($guias_facturadas)){
	$error_man->showInfo("La nota de venta poseía <b>{$guias_facturadas}</b> guias de despacho antiguas a las cuales se le asignó la factura");
}

if(isset($num_guia)){
	$error_man->showConfirm("Se ha generado la siguiente guía de despacho virtual <strong>sin folio</strong>");
	$error_man->showInfo("El número de guía de despacho interno es <h1 style='margin:0;color:#000;'>{$num_guia}</h1>");
}

?>
<script type="text/javascript">
$('#print_factura').click(function(){
	loadOverlay('sites/ventas/factura_venta/print_factura_venta.php?id=<?=$factura ?>');
});
</script>
