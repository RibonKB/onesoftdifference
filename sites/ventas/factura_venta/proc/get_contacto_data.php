<?php
define("MAIN",1);
require_once("../../../../inc/global.php");

$data = $db->select("tb_contacto_cliente c
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
		JOIN tb_cliente cl ON cl.dc_cliente = c.dc_cliente",
	'c.dg_contacto, c.dg_fono, c.dg_movil, c.dg_email, cl.dg_razon, s.dg_sucursal, s.dg_horario_atencion',
	"c.dc_contacto={$_GET['id']} AND c.dm_activo = 1");

if(!count($data)){
	echo json_encode('<not-found>');
}
$data = array_shift($data);

$anexo = $db->select('tb_funcionario','dg_anexo',"dc_funcionario={$userdata['dc_funcionario']}");
$data['anexo'] = $anexo[0]['dg_anexo'];

echo json_encode($data);
?>