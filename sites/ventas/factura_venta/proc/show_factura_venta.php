<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_factura = intval($_POST['id']);

$data = $db->prepare($db->select("tb_factura_venta fv
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fv.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fv.dc_orden_servicio
JOIN tb_medio_pago mp ON mp.dc_medio_pago = fv.dc_medio_pago
JOIN tb_contacto_cliente c ON c.dc_contacto = fv.dc_contacto
	JOIN tb_cliente_sucursal s ON s.dc_sucursal = c.dc_sucursal
	JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
	JOIN tb_region reg ON reg.dc_region = com.dc_region
JOIN tb_contacto_cliente ce ON ce.dc_contacto = fv.dc_contacto_entrega
	JOIN tb_cliente_sucursal s2 ON s2.dc_sucursal = c.dc_sucursal
	JOIN tb_comuna com2 ON com2.dc_comuna = s2.dc_comuna
	JOIN tb_region reg2 ON reg2.dc_region = com2.dc_region
LEFT JOIN tb_tipo_cargo tc ON tc.dc_tipo_cargo = fv.dc_tipo_cargo
LEFT JOIN tb_tipo_operacion top ON top.dc_tipo_operacion = fv.dc_tipo_operacion
LEFT JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente
LEFT JOIN tb_funcionario ex ON ex.dc_funcionario = fv.dc_ejecutivo",
"fv.dq_factura,fv.df_emision as emision_factura,fv.df_vencimiento,fv.dg_comentario,
fv.dg_orden_compra,fv.df_fecha_orden_compra,nv.dq_nota_venta,nv.dm_al_dia,nv.df_fecha_emision as emision_nv,os.dq_orden_servicio,fv.dq_neto,fv.dq_iva,fv.dq_total,fv.dc_factura,mp.dg_medio_pago,fv.dm_nula,fv.dm_tipo_impresion,
c.dg_contacto,s.dg_direccion,reg.dg_region,com.dg_comuna,c.dg_fono,s2.dg_direccion dg_direccion_entrega,com2.dg_comuna dg_comuna_entrega,
reg2.dg_region dg_region_entrega,ce.dg_contacto dg_contacto_entrega,tc.dg_tipo_cargo,top.dc_tipo_operacion,top.dg_tipo_operacion,cl.dg_razon,cl.dg_rut,cl.dg_giro,
CONCAT_WS(' ',ex.dg_nombres,ex.dg_ap_paterno) dg_ejecutivo,fv.dm_anticipada,fv.dm_centralizada",
'fv.dc_empresa = ? AND fv.dc_factura = ?'));
$data->bindValue(1,$empresa,PDO::PARAM_INT);
$data->bindValue(2,$dc_factura,PDO::PARAM_INT);
$db->stExec($data);
$data = $data->fetch(PDO::FETCH_OBJ);

if($data === false){
	$error_man->showWarning("No se ha encontrado la factura especificada");
	exit;
}

//Consultar los datos del tipo de operación
$tipoOperacion = $db->getRowById('tb_tipo_operacion top
	LEFT JOIN tb_tipo_cambio  tca on top.dc_tipo_cambio = tca.dc_tipo_cambio', $data->dc_tipo_operacion, 'dc_tipo_operacion',
	'top.dm_exenta, top.dm_agrega_iva_producto, top.dc_tipo_cambio, tca.dg_tipo_cambio');

//Si el tipo de cambio no es null y es mayor a 0, buscar el valor para el tipo de cambio especificado.
$tipoCambio = 0; 

if(!is_null($tipoOperacion->dc_tipo_cambio) && $tipoOperacion->dc_tipo_cambio > 0){
	if(isset($data->dm_al_dia) && $data->dm_al_dia == 0){
		$fechaEmision = explode(' ', $data->emision_nv);
		$consultaTipoCambio = $db->doQuery($db->select('tb_tipo_cambio_mensual','dq_cambio',
			"dc_tipo_cambio = {$tipoOperacion->dc_tipo_cambio} AND df_cambio = '{$fechaEmision[0]}'"));
		$consultaTipoCambio = $consultaTipoCambio->fetch(PDO::FETCH_OBJ);
		$tipoCambio = isset($consultaTipoCambio->dq_cambio) ? $consultaTipoCambio->dq_cambio:0;
	} else {
		$fechaEmision = explode(' ', $data->emision_factura);
		$consultaTipoCambio = $db->doQuery($db->select('tb_tipo_cambio_mensual','dq_cambio',
			"dc_tipo_cambio = {$tipoOperacion->dc_tipo_cambio} AND df_cambio = '{$fechaEmision[0]}'"));
		$consultaTipoCambio = $consultaTipoCambio->fetch(PDO::FETCH_OBJ);
		$tipoCambio = isset($consultaTipoCambio->dq_cambio) ? $consultaTipoCambio->dq_cambio:0;	
	}
	
	if($tipoCambio == 0){
		$error_man->showAviso("El valor del tipo de cambio {$tipoOperacion->dg_tipo_cambio} no ha sido definido 
			para el día {$db->dateLocalFormat($fechaEmision[0] . ' 00:00:00')}. La información se mostrará en moneda local.");
	}
	
}
//fin de modificaciones

$detalle = $db->prepare($db->select("tb_factura_venta_detalle",
	'dg_producto,dc_cantidad,dg_descripcion,dg_serie,dq_precio,dq_costo,dq_descuento,dq_total,dm_tipo',
	"dc_factura = ?"));
$detalle->bindValue(1,$dc_factura,PDO::PARAM_INT);
$db->stExec($detalle);

$detalles_por_tipo = array();
while($d = $detalle->fetch(PDO::FETCH_OBJ)){
	if($tipoCambio > 0){
		//cambiar los valores a los detalles por los del tipo de cambio
		if($d->dq_precio > 0){
			$d->dq_precio = $d->dq_precio/$tipoCambio;
		}
		if($d->dq_costo > 0){
			$d->dq_costo = $d->dq_costo/$tipoCambio;
		}
		if($d->dq_descuento > 0){
			$d->dq_descuento = $d->dq_descuento/$tipoCambio;
		}
		if($d->dq_total > 0){
			$d->dq_total = $d->dq_total/$tipoCambio;
		}
		//cambiar los valores de la factura fv.dq_neto,fv.dq_iva,fv.dq_total
		if($data->dq_neto > 0){
			$data->dq_neto = $data->dq_neto/$tipoCambio;
		}
		if($data->dq_iva > 0){
			$data->dq_iva = $data->dq_iva/$tipoCambio;
		}
		if($data->dq_total > 0){
			$data->dq_total = $data->dq_total/$tipoCambio;
		}
	}
	
	$detalles_por_tipo[$d->dm_tipo][] = $d;
}
unset($detalle);

if($data->dm_nula == 1):
	$null_data = $db->prepare($db->select("tb_factura_venta_anulada n
											JOIN tb_motivo_anulacion ma ON ma.dc_motivo = n.dc_motivo",
										 'ma.dg_motivo,n.dm_fisica,n.dg_comentario',
										 'n.dc_factura = ?'));
	$null_data->bindValue(1,$dc_factura,PDO::PARAM_INT);
	$db->stExec($null_data);
	$null_data = $null_data->fetch(PDO::FETCH_OBJ);
endif;

if($data->dm_centralizada == 1){
	$centralizacion = $db->prepare($db->select('tb_comprobante_contable_detalle_analitico da
			JOIN tb_comprobante_contable_detalle d ON d.dc_detalle = da.dc_detalle_financiero
			JOIN tb_comprobante_contable c ON c.dc_comprobante = d.dc_comprobante',
		'c.dg_comprobante, c.df_fecha_contable',
		'da.dc_factura_venta = ? AND c.dm_anulado = 0 AND c.dm_tipo_comprobante = 3'));
	$centralizacion->bindValue(1,$dc_factura,PDO::PARAM_INT);
	$db->stExec($centralizacion);
	$centralizacion = $centralizacion->fetch(PDO::FETCH_OBJ);
}

?>

<div class='title center'>Factura nº <?php echo $data->dq_factura ?></div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong>(<?php echo $data->dg_rut ?>) <?php echo $data->dg_razon ?></strong></caption>
<tbody>
	<?php if($data->dm_anticipada == 'A'): ?>
        <tr>
            <th colspan="3" class="info"><b>FACTURA ANTICIPADA</b></th>
        </tr>
    <?php elseif($data->dm_anticipada == 'E'): ?>
        <tr>
            <th colspan="3" class="info"><b>FACTURA ANTICIPADA COMPLETA</b></th>
        </tr>
    <?php endif ?>
    <tr>
        <td width='160'>Fecha emision</td>
        <td><?php echo $db->dateLocalFormat($data->emision_factura) ?></td>
        <?php if($data->dm_nula == 1): ?>
        	<td rowspan='13' valign='top'>
            	<h3 class='alert'>FACTURA ANULADA</h3>
                <?php if($null_data !== false): ?>
                	<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
                        <div class='info'>Info anulación</div>
                        Motivo: <b><?php echo $null_data->dg_motivo ?></b><br />
                        Facturada fisicamente: <b><?php echo $null_data->dm_fisica==1?'SI':'NO' ?></b>
                        <a href='sites/ventas/factura_venta/proc/change_facturada_fisicamente.php?id=<?php echo $dc_factura ?>' class='loadOnOverlay' >
                            <img class='left' src='images/editbtn.png' title='Cambiar estado de impresión'>
                        </a>
                        <br />
                        Comentario: <b><?php echo $null_data->dg_comentario ?></b>
                    </div>
                <?php endif ?>
            </td>
        <?php endif ?>
        
        <?php if($data->dm_centralizada == 1): ?>
        	<td rowspan="13" valign="top">
            	<h3 class="confirm">FACTURA CENTRALIZADA</h3>
                <?php if($centralizacion !== false): ?>
                	<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
                    	<div class='info'>Info centralización</div>
                    	Comprobante centralización: <b><?php echo $centralizacion->dg_comprobante ?></b><br />
                        Periodo Contable: <b><?php $db->dateLocalFormat($centralizacion->df_fecha_contable) ?></b>
                    </div>
                <?php endif ?>
            </td>
        <?php endif; ?>
        
    </tr><tr>
        <td>Domicilio cliente</td>
        <td><b><?php echo $data->dg_direccion ?></b> <?php echo $data->dg_comuna ?> <label><?php echo $data->dg_region ?></label></td>
    </tr><tr>
        <td>Domicilio entrega</td>
        <td><b><?php echo $data->dg_direccion_entrega ?></b> <?php echo $data->dg_comuna_entrega ?> <label><?php echo $data->dg_region_entrega ?></label></td>
    </tr><tr>
        <td>Ejecutivo</td>
        <td><b><?php echo $data->dg_ejecutivo ?></b></td>
    </tr><tr>
        <td>Medio de pago:</td>
        <td><b><?php echo $data->dg_medio_pago ?></b></td>
    </tr><tr>
        <td>Tipo de cargo:</td>
        <td><b><?php echo $data->dg_tipo_cargo ?></b></td>
    </tr><tr>
        <td>Tipo de operacion</td>
        <td><?php echo $data->dg_tipo_operacion ?></td>
    </tr><tr>
        <td>Fecha de vencimiento</td>
        <td><?php echo $db->dateLocalFormat($data->df_vencimiento) ?></td>
    </tr><tr>
        <td>Orden de compra</td>
        <td>
			<?php echo $data->dg_orden_compra ?>
			<?php if(check_permiso(59)): ?>
				<img src="images/editbtn.png" class="edit_orden_compra" />
			<?php endif ?>
		</td>
    </tr><tr>
        <td>Fecha Orden de compra</td>
        <td>
			<?php echo $db->dateLocalFormat($data->df_fecha_orden_compra) ?>
			<?php if(check_permiso(59)): ?>
				<img src="images/editbtn.png" class="edit_orden_compra" />
			<?php endif ?>
		</td>
		
    </tr><tr>
        <td>Nota de venta</td>
        <td><?php echo $data->dq_nota_venta ?></td>
    </tr><tr>
        <td>Orden de servicio</td>
        <td><?php echo $data->dq_orden_servicio ?></td>
    </tr><tr>
        <td>Comentario</td>
        <td><?php echo nl2br($data->dg_comentario) ?></td>
    </tr><tr>
        <td>Modo de impresión</td>
        <td><b id='tipo_impresion_label'>
        	<?php if($data->dm_tipo_impresion == 1): ?>
            	Modo Detalle
            <?php else: ?>
            	Modo Glosa
            <?php endif ?>
        </b>
        <?php if(check_permiso(59)): ?>
            <img src="images/editbtn.png" id="edit_tipo_impresion" />
        <?php endif ?>
        </td>
    </tr>
</tbody>
</table>

<?php if(isset($detalles_por_tipo[1])): ?>
	<ul id='tabs'>
		<li><a href='#'>Detalle</a></li>
		<li><a href='#'>Detalle Glosa</a></li>
	</ul>
	
	<br class='clear' />
	<div class='tabpanes'><div>
<?php endif ?>

<table width='100%' class='tab'>
<caption>Detalle <?php echo $tipoCambio > 0 ? "(Tipo de cambio: <b>{$tipoOperacion->dg_tipo_cambio})</b>":'' ?></caption>
<thead>
    <tr>
        <th width='60'>Código</th>
        <th width='60'>Cantidad</th>
        <th>Descripción</th>
        <th width='60'>Número de serie</th>
        <th width='100'>Precio</th>
        <th width='100'>Costo</th>
        <th width='100'>Descuento</th>
        <th width='100'>Total</th>
    </tr>
</thead>
<tbody>

<?php foreach($detalles_por_tipo[0] as $d): ?>
	<tr>
		<td><?php echo $d->dg_producto ?></td>
		<td><?php echo intval($d->dc_cantidad) ?></td>
		<td align='left'><?php echo $d->dg_descripcion ?></td>
		<td><?php echo $d->dg_serie ?></td>
		<td align='right'><?php echo $tipoCambio > 0 ? moneda_local($d->dq_precio,2):moneda_local($d->dq_precio) ?></td>
		<td align='right'><?php echo $tipoCambio > 0 ? moneda_local($d->dq_costo,2):moneda_local($d->dq_costo) ?></td>
		<td align='right'><?php echo $tipoCambio > 0 ? moneda_local($d->dq_descuento,2):moneda_local($d->dq_descuento) ?></td>
		<td align='right'><?php echo $tipoCambio > 0 ? moneda_local($d->dq_total,2):moneda_local($d->dq_total) ?></td>
	</tr>
<?php endforeach ?>

</tbody>
<tfoot>
<tr>
	<th colspan='7' align='right'>Total Neto</th>
	<th align='right'><?php echo $tipoCambio > 0 ? moneda_local($data->dq_neto,2):moneda_local($data->dq_neto) ?></th>
</tr>
<tr>
	<th colspan='7' align='right'>IVA</th>
	<th align='right'><?php echo $tipoCambio > 0 ? moneda_local($data->dq_iva,2):moneda_local($data->dq_iva) ?></th>
</tr>
<tr>
	<th colspan='7' align='right'>Total a Pagar</th>
	<th align='right'><?php echo $tipoCambio > 0 ? moneda_local($data->dq_total,2):moneda_local($data->dq_total) ?></th>
</tr>
</tfoot></table>

<?php if(isset($detalles_por_tipo[1])): ?>
	</div><div><table width='100%' class='tab'>
	<caption>Detalle</caption>
	<thead>
        <tr>
            <th width='60'>Código</th>
            <th width='60'>Cantidad</th>
            <th>Descripción</th>
            <th width='60'>Número de serie</th>
            <th width='100'>Precio</th>
            <th width='100'>Descuento</th>
            <th width='100'>Total</th>
        </tr>
	</thead>
	<tbody>
	
		<?php foreach($detalles_por_tipo[1] as $d): ?>
        <tr>
            <td><?php echo $d->dg_producto ?></td>
            <td><?php echo intval($d->dc_cantidad) ?></td>
            <td align='left'><?php echo $d->dg_descripcion ?></td>
            <td><?php echo $d->dg_serie ?></td>
            <td align='right'><?php echo moneda_local($d->dq_precio) ?></td>
            <td align='right'><?php echo moneda_local($d->dq_descuento) ?></td>
            <td align='right'><?php echo moneda_local($d->dq_total) ?></td>
        </tr>
        <?php endforeach ?>
	
	</tbody>
	<tfoot>
	<tr>
		<th colspan='6' align='right'>Total Neto</th>
		<th align='right'><?php echo moneda_local($data->dq_neto) ?></th>
	</tr>
	<tr>
		<th colspan='6' align='right'>IVA</th>
		<th align='right'><?php echo moneda_local($data->dq_iva) ?></th>
	</tr>
	<tr>
		<th colspan='6' align='right'>Total a Pagar</th>
		<th align='right'><?php echo moneda_local($data->dq_total) ?></th>
	</tr>
	</tfoot></table></div></div>
<?php endif ?>

<script type="text/javascript">
pymerp.init('#show_factura');
$('#edit_tipo_impresion').click(function(){
	loadFile('sites/ventas/factura_venta/proc/change_tipo_impresion.php?id=<?php echo $dc_factura ?>','#tipo_impresion_label');
});
<?php if($data->dm_nula): ?>
disable_button('#print_version,#print_glosa_version,#null_factura');
<?php else: ?>
enable_button('#print_version,#print_glosa_version,#null_factura');
$('#print_version').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/factura_venta/print_factura_venta.php?id=<?php echo $dc_factura ?>');
	//window.open("sites/ventas/factura_venta/ver_factura_venta.php?id=<?php echo $dc_factura ?>&v=0",'print_factura_venta','width=800;height=600');
});
$('#print_glosa_version').unbind('click').click(function(){
	window.open("sites/ventas/factura_venta/ver_factura_venta.php?id=<?php echo $dc_factura ?>&v=1",'print_factura_venta','width=800;height=600');
});
$('#null_factura').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/factura_venta/null_factura_venta.php?id=<?php echo $dc_factura ?>');
});
$('#gestion_factura').unbind('click').click(function(){
	pymerp.loadOverlay('sites/finanzas/cobros/cr_gestion_cobranza_masiva.php?mode=G&dc_factura[]=<?php echo $dc_factura ?>');
});
$('#pago_factura').unbind('click').click(function(){
	pymerp.loadOverlay('sites/finanzas/cobros/cr_gestion_cobranza_masiva.php?mode=P&dc_factura[]=<?php echo $dc_factura ?>');
});
$('#historial_gestion').unbind('click').click(function(){
	pymerp.loadOverlay('sites/finanzas/cobros/history_gestion_cobranza.php?id=<?php echo $dc_factura ?>');
});
$('#edit_folio').unbind('click').click(function(){
	pymerp.loadOverlay('sites/ventas/factura_venta/ch_folio.php?id=<?php echo $dc_factura ?>');
});
$('#factura_electronica').unbind('click').click(function(){
	pymerp.loadOverlay('god.php?modulo=ventas&submodulo=factura_venta&factory=FacturaElectronica&dc_factura=<?php echo $dc_factura ?>');
});
//Editar Orden Compra de cliente y fecha
$('.edit_orden_compra').unbind('click').click(function(){
	pymerp.loadOverlay('god.php?factory=FacturaVenta&modulo=ventas&action=getViewEditOrdenCompraData&submodulo=factura_venta&dc_factura=<?php echo $dc_factura ?>');
});
<?php endif; ?>
</script>