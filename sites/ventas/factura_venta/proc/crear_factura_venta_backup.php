<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
require_once('../../../proc/bodega_functions.php');

if(count($db->select('tb_factura_venta','1',"dq_factura={$_POST['fv_number']} AND dc_empresa={$empresa}"))){
	$error_man->showWarning("El número de factura <b>{$_POST['fv_number']}</b> es inválido pues ya existe una factura con dicho número.");
	exit();
}

$_POST['fv_tipo_operacion'] = explode('|',$_POST['fv_tipo_operacion']);
$tipo_operacion = $_POST['fv_tipo_operacion'][0];
$tipo_movimiento = $_POST['fv_tipo_operacion'][1];
$tipo_guia = $_POST['fv_tipo_operacion'][2];
$virtual = ($tipo_guia != 0) && (!isset($_POST['doc_type']));
if($virtual){
	$bodega = $_POST['fv_tipo_operacion'][3];
	$guia_tipo_movimiento = $_POST['fv_tipo_operacion'][4];
	
	//Obtener los primary key de los productos que realizan movimientos en bodega a través de los códigos.
	$codes = implode("','",$_POST['prod']);
	$ids = array();
	$db_prod = $db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_codigo IN ('{$codes}')) p
	JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto AND dm_controla_inventario = '0'",
	'dc_producto,dg_codigo');
	foreach($db_prod as $p){
		$ids[$p['dg_codigo']] = $p['dc_producto'];
	}
	unset($db_prod);

}


$db->start_transaction();
//$db->query("SET AUTOCOMMIT=0");
//$db->query("LOCK TABLES ");// <-- incluir tablas

if($virtual){
	foreach($_POST['prod'] as $i => $p){
		if(isset($ids[$p])){
			$cant = str_replace(',','',$_POST['cant'][$i]);
			
			$movs = bodega_rebajar_stock($ids[$p],$cant,$bodega);
			
			/*$db->update('tb_stock',array(
				'dq_stock' => "dq_stock-{$cant}"
			),"dc_producto={$ids[$p]} AND dc_bodega={$bodega} AND dq_stock >= {$cant}");*/
			
			if($movs != 1){
				$error_man->showWarning("No hay stock disponible para: <b>{$_POST['desc'][$i]}</b> código: <b>{$p}</b> En la bodega especificada");
				$db->rollback();
				//$db->query("UNLOCK TABLES");
				exit();
			}
		}
	}
}

$factura = $db->insert('tb_factura_venta',array(
	"dc_nota_venta" => $_POST['fv_id_nota_venta'],
	"dc_orden_servicio" => $_POST['fv_id_orden_servicio'],
	"dc_cliente" => $_POST['cli_id'],
	"dc_contacto" => $_POST['fv_contacto'],
	"dc_contacto_entrega" => $_POST['fv_cont_entrega'],
	"dc_medio_pago" => $_POST['fv_medio_pago'],
	"dc_tipo_cargo" => $_POST['fv_tipo_cargo'],
	"dc_tipo_operacion" => $tipo_operacion,
	"dq_factura" => $_POST['fv_number'],
	"dg_comentario" => $_POST['fv_comentario'],
	"df_emision" => 'NOW()',
	"df_vencimiento" => $db->sqlDate($_POST['fv_vencimiento']),
	"dg_orden_compra" => $_POST['fv_orden_compra'],
	"dq_neto" => $_POST['cot_neto'],
	"dq_iva" => $_POST['cot_iva'],
	"dq_total" => $_POST['cot_neto']+$_POST['cot_iva'],
	"dq_monto_pagado" => 0,
	"dc_empresa" => $empresa
));

if($virtual){
	$num_guia = $db->select('tb_guia_despacho','MAX(dq_guia_despacho)+1 AS number',"dc_empresa={$empresa}");
	$actual = $num_guia[0]['number'];
	$cantidad_guias = ceil(array_sum($_POST['cant'])/20);
	
	$guias = array();
	$guias_number = array();
	for($i = 0; $i <$cantidad_guias; $i++){
		while(count($db->select('tb_guia_despacho','1',"dq_guia_despacho={$actual} AND dc_empresa={$empresa}"))){
			$actual++;
		}
		$guias[] = $db->insert('tb_guia_despacho',array(
			'dc_factura' => $factura,
			'dc_nota_venta' => $_POST['fv_id_nota_venta'],
			'dc_orden_servicio' => $_POST['fv_id_orden_servicio'],
			'dc_cliente' => $_POST['cli_id'],
			'dc_contacto' => $_POST['fv_contacto'],
			'dc_contacto_entrega' => $_POST['fv_cont_entrega'],
			'dq_guia_despacho' => $actual,
			'dg_comentario' => "Guía virtual generada automáticamente para la factura {$_POST['fv_number']}",
			'df_emision' => 'NOW()',
			'dg_orden_compra' => $_POST['fv_orden_compra'],
			'dq_neto' => $_POST['cot_neto'],
			'dq_iva' => $_POST['cot_iva'],
			'dq_total' => $_POST['cot_neto']+$_POST['cot_iva'],
			'dc_empresa' => $empresa
		));
		
		$guias_number[] = $actual;
		
		$actual++;
	}
	
	$guia_actual = -1;
	$det_actual = 0;
	$cantidad_despachada = 0;
	$movements = array();
	foreach($_POST['cant'] as $i => $v){
	
		$series = explode(',',$_POST['serie'][$i]);
		$_POST['precio'][$i] = str_replace(',','',$_POST['precio'][$i]);
				
		$detail_data = array(
			"dg_producto" => $_POST['prod'][$i],
			"dg_descripcion" => $_POST['desc'][$i],
			"dq_precio" => $_POST['precio'][$i]
		);
		
		if(isset($ids[$_POST['prod'][$i]])){
			$movements[$_POST['prod'][$i]] = array();
			$movements[$_POST['prod'][$i]]['price'] = $_POST['precio'][$i];
			$movements[$_POST['prod'][$i]]['bodega'] = $bodega;
		}
		
		if(isset($_POST['id_detail'][$i])){
			$detail_data['dc_detalle_nota_venta'] = $_POST['id_detail'][$i];
			$db->update('tb_nota_venta_detalle',array(
				"dc_despachada" => "dc_despachada+{$v}"
			),"dc_nota_venta_detalle = {$_POST['id_detail'][$i]}");
			
			$cantidad_despachada += $v;
		}
		
		for($j = 0; $j<$v; $j++){
			if($det_actual%20 == 0)
				$guia_actual++;
			
			if(isset($ids[$_POST['prod'][$i]]))
				$movements[$_POST['prod'][$i]]['guias'][$guias[$guia_actual]][] = isset($series[$j])?$series[$j]:'';
				
			$detail_data['dc_guia_despacho'] = $guias[$guia_actual];
	
			if(isset($series[$j])){
				$detail_data['dg_serie'] = $series[$j];
			}
			
			$db->insert('tb_guia_despacho_detalle',$detail_data);
			
			$det_actual++;
		}
	}
	
	foreach($movements as $codigo => $v){
	
	foreach($v['guias'] as $guia => $arr){
			$series = implode(',',array_diff($arr,array('')));
			$cantidad = count($arr);
			
			$db->insert('tb_movimiento_bodega',array(
				'dc_cliente' => $_POST['cli_id'],
				'dc_tipo_movimiento' => $guia_tipo_movimiento,
				'dc_bodega_salida' => $v['bodega'],
				'dq_monto' => $v['price'],
				'dc_guia_despacho' => $guia,
				'dg_series' => $series,
				'dc_nota_venta' => $_POST['fv_id_nota_venta'],
				'dc_producto' => $ids[$codigo],
				'dq_cantidad' => -1*$cantidad,
				'dc_empresa' => $empresa,
				'df_creacion' => 'NOW()',
				'dc_usuario_creacion' => $idUsuario
			));
		}
	}
}

if($_POST['fv_id_orden_servicio'] != 0)
	$db->update('tb_orden_servicio',array('dc_factura' => $factura),"dc_orden_servicio={$_POST['fv_id_orden_servicio']}");

$cantidad_facturada = 0;
foreach($_POST['prod'] as $i => $v){
	
	$_POST['cant'][$i] = str_replace(',','',$_POST['cant'][$i]);
	$_POST['precio'][$i] = str_replace(',','',$_POST['precio'][$i]);
	$_POST['descuento'][$i] = str_replace(',','',$_POST['descuento'][$i]);
	
	$detail_data = array(
		"dc_factura" => $factura,
		"dg_producto" => $v,
		"dg_descripcion" => $_POST['desc'][$i],
		"dm_tipo" => 0,
		"dc_cantidad" => $_POST['cant'][$i],
		"dg_serie" => $_POST['serie'][$i],
		"dq_precio" => $_POST['precio'][$i],
		"dq_descuento" => $_POST['descuento'][$i],
		"dq_total" => $_POST['cant'][$i]*$_POST['precio'][$i]-$_POST['descuento'][$i]
	);

	if(isset($_POST['id_detail'][$i])){
		$detail_data['dc_detalle_nota_venta'] = $_POST['id_detail'][$i];
		$db->update('tb_nota_venta_detalle',array(
			"dc_facturada" => "dc_facturada+{$_POST['cant'][$i]}"
		),"dc_nota_venta_detalle = {$_POST['id_detail'][$i]}");
		
		$cantidad_facturada += $_POST['cant'][$i];
	}

	$db->insert('tb_factura_venta_detalle',$detail_data);

}

if($cantidad_facturada){
	$db->update('tb_nota_venta',array("dc_facturada" => "dc_facturada+{$cantidad_facturada}"),"dc_nota_venta={$_POST['fv_id_nota_venta']}");
}

if(isset($_POST['fake_prod'])){
	foreach($_POST['fake_prod'] as $i => $v){
		
	$_POST['fake_cant'][$i] = str_replace(',','',$_POST['fake_cant'][$i]);
	$_POST['fake_precio'][$i] = str_replace(',','',$_POST['fake_precio'][$i]);
	$_POST['fake_descuento'][$i] = str_replace(',','',$_POST['fake_descuento'][$i]);
	
	$detail_data = array(
		"dc_factura" => $factura,
		"dg_producto" => $v,
		"dg_descripcion" => $_POST['fake_desc'][$i],
		"dm_tipo" => 1,
		"dc_cantidad" => $_POST['fake_cant'][$i],
		"dg_serie" => $_POST['fake_serie'][$i],
		"dq_precio" => $_POST['fake_precio'][$i],
		"dq_descuento" => $_POST['fake_descuento'][$i],
		"dq_total" => $_POST['fake_cant'][$i]*$_POST['fake_precio'][$i]-$_POST['fake_descuento'][$i]
	);
		
	$db->insert('tb_factura_venta_detalle',$detail_data);
	}
}

if(isset($_POST["doc_type"])){
	if($_POST['doc_type'] == 0){
		$num_docs = $db->select("(SELECT * FROM tb_guia_despacho_detalle WHERE dc_guia_despacho IN ({$_POST['doc_list']})) d
		JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta",
		'd.dc_detalle_nota_venta,count(d.dc_detalle_nota_venta) AS dq_cantidad,nvd.dc_nota_venta','',array('group_by' => 'd.dc_detalle_nota_venta'));
		
		$notas = array();
		foreach($num_docs as $n){
			$db->update('tb_nota_venta_detalle',array('dc_facturada' => "dc_facturada+{$n['dq_cantidad']}"),"dc_nota_venta_detalle={$n['dc_detalle_nota_venta']}");
			
			$notas[$n['dc_nota_venta']][] = $n['dq_cantidad'];
		}
		
		foreach($notas as $i => $n){
			$sum = array_sum($n);
			$db->update('tb_nota_venta',array('dc_facturada' => "dc_facturada+{$sum}"),"dc_nota_venta={$i}");
		}
	}
	
	switch($_POST['doc_type']){
		case 0:
			$db->update('tb_guia_despacho',array('dc_factura' => $factura),"dc_guia_despacho IN ({$_POST['doc_list']})");
			break;
		case 1:
			$db->update('tb_orden_servicio',array('dc_factura' => $factura),"dc_orden_servicio IN ({$_POST['doc_list']})");
			break;
		default: $db->rollback(); exit();
	}
}

//$db->query("UNLOCK TABLES");
$db->commit();

$error_man->showConfirm("Se ha generado la factura de venta correctamente
<div class='title'>El número de factura es <h1 style='margin:0;color:#000;'>{$_POST['fv_number']}</h1></div>");
$error_man->showInfo("Se han agregado ".count($_POST['prod'])." productos al detalle<br />
<a href=\"#\" class='button' onclick=\"window.open('sites/ventas/factura_venta/ver_factura_venta.php?id={$factura}&v=0','factura_detalle','width=800,height=600')\">
	Ver versión Detalle
</a><br />
<a href=\"#\" class='button' onclick=\"window.open('sites/ventas/factura_venta/ver_factura_venta.php?id={$factura}&v=1','factura_glosa','width=800,height=600')\">
	Ver versión Glosa
</a>");

?>