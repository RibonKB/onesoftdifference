<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(isset($_POST['cli_rut'])){
	//Se escapan los caracteres por seguridad
	$db->escape($_POST['cli_rut']);
	$datos = $db->select("tb_cliente","dc_cliente,dg_razon,dc_contacto_default",
	"dg_rut = '{$_POST['cli_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");
	
	if(!count($datos)){
		$error_man->showErrorRedirect("No se encontró el cliente especificado, intentelo nuevamente.","sites/ventas/factura_venta/cr_factura_venta.php");
	}
	$datos = $datos[0];
	$poblacion['nota_venta'] = '';
	$poblacion['orden_servicio'] = '';
}else if(isset($_POST['cli_id'])){
	$datos = $db->select("tb_cliente",'dc_cliente,dg_razon,dc_contacto_default',
	"dc_cliente={$_POST['cli_id']} AND dc_empresa={$empresa} AND dm_activo='1'");
	
	if(!count($datos)){
		$error_man->showErrorRedirect("Error: Cliente inválido, intentelo nuevamente.","sites/ventas/oportunidad/src_oportunidad.php");
	}
	$datos = $datos[0];

	$poblacion['nota_venta'] = isset($_POST['nv_numero'])?$_POST['nv_numero']:'';
	$poblacion['orden_servicio'] = isset($_POST['os_numero'])?$_POST['os_numero']:'';
}else if(isset($_POST['cli_id_mass'])){
	$datos = $db->select("tb_cliente",'dc_cliente,dg_razon,dc_contacto_default',
	"dc_cliente={$_POST['cli_id_mass']} AND dc_empresa={$empresa} AND dm_activo='1'");
	
	if(!count($datos)){
		$error_man->showErrorRedirect("Error: Cliente inválido, intentelo nuevamente.","sites/ventas/factura_venta/gestor_factura_venta.php");
	}
	$datos = $datos[0];
	
	$doc_list = implode(',',$_POST['oc_detail']);
	
	if($_POST['tipo_doc'] == 0){
		$aux_num_doc = $db->select('tb_guia_despacho','dq_guia_despacho',"dc_guia_despacho IN ({$doc_list})");
		$num_doc_list = "Guías de despacho facturadas\n";
		foreach($aux_num_doc as $doc){
			$num_doc_list .= "\n{$doc['dq_guia_despacho']}";
		}
		
		switch($_POST['mode_id']){
			case 1: $detalles_base = $db->select('tb_guia_despacho_detalle',
				'dg_producto as dg_codigo,avg(dq_precio) AS dq_precio_venta,avg(dq_precio) AS dq_precio_compra,SUM(dq_cantidad) AS dq_cantidad,dg_descripcion, 0 AS dc_proveedor',
				"dc_guia_despacho IN ({$doc_list})",array('group_by' => 'dg_producto')); break;
			case 2: $detalles_base = $db->select('tb_guia_despacho_detalle',
				'dg_producto as dg_codigo,dq_precio AS dq_precio_venta,dq_precio AS dq_precio_compra,SUM(dq_cantidad) AS dq_cantidad,dg_descripcion, 0 AS dc_proveedor',
				"dc_guia_despacho IN ({$doc_list})",array('group_by' => 'dg_producto,dq_precio')); break;
			case 3: $detalles_base = $db->select('tb_guia_despacho_detalle',
				'dg_producto as dg_codigo,dq_precio AS dq_precio_venta,dq_precio AS dq_precio_compra,SUM(dq_cantidad) AS dq_cantidad,dg_descripcion, 0 AS dc_proveedor',
				"dc_guia_despacho IN ({$doc_list})",array('group_by' => 'dg_producto,dq_precio'));
				$precio_glosa = 0;
				foreach($detalles_base as $v){
					$precio_glosa += $v['dq_precio_venta']*$v['dq_cantidad'];
				}
				break;
			default: exit();
		}
	}else if($_POST['tipo_doc'] == 1){
		$os_num_doc = $db->select('tb_orden_servicio','dq_orden_servicio,dg_glosa_facturacion,dq_neto',"dc_orden_servicio IN ({$doc_list})");
		$num_doc_list = "Ordenes de servicio facturadas\n";
		foreach($os_num_doc as $doc){
			$num_doc_list .= "\n{$doc['dq_orden_servicio']}";
		}
		
		$detalles_base = $db->select("(SELECT * FROM tb_orden_servicio_factura_detalle WHERE dc_orden_servicio IN ({$doc_list})) d
		JOIN tb_producto p ON p.dc_producto = d.dc_producto",
		'p.dg_codigo,d.dq_precio_venta,d.dq_precio_compra,SUM(d.dq_cantidad) AS dq_cantidad,p.dg_producto AS dg_descripcion,0 AS dc_proveedor',
		'',array('group_by'=>'d.dc_producto,d.dq_precio_venta'));
	}
	
	$poblacion['nota_venta'] = '';
	$poblacion['orden_servicio'] = '';
}else
	exit();

echo("<div id='secc_bar'>Creación de factura de venta</div>
<div id='main_cont'>
<input type='text' class='hidden' />
<div class='panes' style='width:1140px;'>
<div class='title center'>
<button class='right button' id='cli_switch'>Cambiar Cliente</button>
Generando factura de venta para <strong id='cli_razon' style='color:#000;'>{$datos['dg_razon']}</strong>
</div>");

	include_once("../../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dq_cambio",
	"dc_empresa={$empresa} AND dm_activo='1'");
	$cambio_list = array();
	foreach($list_seg as $c){
		$form->Hidden("cambio{$c['dc_tipo_cambio']}",$c['dq_cambio']);
	}
	
	$num_factura = $db->select('tb_factura_venta','MAX(dq_factura)+1 AS number',"dc_empresa={$empresa}");
	$num_factura = $num_factura[0]['number'];
	
	$form->Start("sites/ventas/factura_venta/proc/crear_factura_venta.php","cr_factura_venta",'ventas_form');
	$form->Header("<strong>Indique los datos para la generación de la factura de venta</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Section();
	
	$form->Date('Fecha emisión','fv_emision',1,0);
	
	//Contacto principal de la factura, aparecerá en la cabecera como principal contacto del cliente
	$form->Listado('Contacto','fv_contacto',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');
	
	//Dirección a la que se despachará el pedido
	$form->Listado('Contacto de entrega','fv_cont_entrega',
	"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto_default'],'');
	
	$form->Listado('Medio de pago','fv_medio_pago','tb_medio_pago',array('dc_medio_pago','dg_medio_pago'),1);
	$form->Listado('Tipo cargo','fv_tipo_cargo','tb_tipo_cargo',array('dc_tipo_cargo','dg_tipo_cargo'));
	
	$form->Listado('Tipo operación','fv_tipo_operacion',
	"(SELECT * FROM tb_tipo_operacion WHERE dc_empresa={$empresa} AND dm_activo='1') tpo
	LEFT JOIN tb_tipo_guia_despacho tg ON tg.dc_tipo = tpo.dc_tipo_guia",
	array(
		'CONCAT_WS("|",tpo.dc_tipo_operacion,tpo.dc_tipo_movimiento,tpo.dc_tipo_guia,tg.dc_bodega,tg.dc_tipo_movimiento,tpo.dm_tipo_verificacion)',
		'tpo.dg_tipo_operacion'
	),1,0,'');
	
	$form->EndSection();
	$form->Section();
	
	$form->Text("Número de factura","fv_number",1,20,$num_factura);
	$form->Date('Fecha vencimiento','fv_vencimiento',1);
	$form->Text('Orden de compra cliente','fv_orden_compra');
	$form->Date('Fecha Orden Compra','fv_fecha_orden_compra',0,0,'fv_orden_compra inputtext');
	$form->Text('Nota de venta','fv_nota_venta',0,255,$poblacion['nota_venta']);
	$form->Hidden('fv_id_nota_venta',0);
	$form->Text('Orden de servicio','fv_orden_servicio',0,255,$poblacion['orden_servicio']);
	$form->Hidden('fv_id_orden_servicio',0);
	
	$form->EndSection();
	$form->Section();
	
	$form->Textarea('Comentario','fv_comentario',0,isset($num_doc_list)?$num_doc_list:'');
	$form->Listado('Ejecutivo','fv_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
	
	if(check_permiso(59))
		$form->Radiobox('Modo de impresión','fv_tipo_impresion',array(1=>'Modo detalle',0=>'Modo glosa'),1);
	else
		$form->Hidden('fv_tipo_impresion',1);
	
	
	$form->Date('Fecha contable','fv_fecha_contable',1,0);
	
	$form->EndSection();
	
	echo("<hr class='clear' />");
	
	echo("
	<ul id='tabs'>
		<li><a href='#'>Detalle</a></li>
		<li><a href='#'>Detalle Glosa</a></li>
	</ul>
	
	<br class='clear' />
	<div class='tabpanes'>
	<div id='prods'>
	<div class='info'>Detalle (Valores en <b>{$empresa_conf['dg_moneda_local']}</b>)</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='70'>Opciones</th>
		<th width='95'>Código</th>
		<th>Descripción</th>
		<th width='63'>Cantidad</th>
		<th width='82'>Precio</th>
		<th width='82' class='add_iva hidden' >Precio con IVA</th>
		<th width='82'>Total</th>
		<th width='82'>Costo</th>
		<th width='82' class='add_iva hidden' >Costo con IVA</th>
		<th width='82'>Costo total</th>
		<th width='82'>Descuento</th>
		<th width='82'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='4' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th>&nbsp;</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center' id='total_margen_p'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th colspan='5'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='5'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='4'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='5'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>
	
	<div id='fake_prods' class='hidden'>
	<div class='info'><b>Detalle glosa</b> Este detalle aparecerá en sustitución del detalle real.<br />
	<small>Nota: Los precios deben cuadrar con el detalle real.</small>
	</div>
	
	</div>
	
	</div>");
	$form->Hidden('cli_id',$datos['dc_cliente']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	if(isset($doc_list)){
		$form->Hidden('doc_list',$doc_list);
		$form->Hidden('doc_type',$_POST['tipo_doc']);
	}
	$form->End('Facturar','addbtn');
	
	echo("<table id='prods_form' class='hidden'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/doc.png' alt='' title='Asignar Series' class='set_series' />
			<input type='hidden' class='prod_series' name='serie[]'>
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='9' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='36' required='required' /></td>
		<td>
			<input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' />
			<input type='hidden' name='despachada[]' value='0' class='despach_cant' />
			<input type='hidden' name='recepcionada[]' value='0' class='recep_cant' />
			<input type='hidden' name='facturada[]' value='0' class='fact_cant' />
		</td>
		
		<td class='add_iva precio_sin_iva hidden' align='right'></td>
		
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>
		
		<td class='add_iva costo_sin_iva hidden' align='right'></td>
		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td><input type='text' name='descuento[]' class='prod_descuento inputtext' size='7' style='text-align:right;' value='0' required='required' /></td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
	</tr>
	</table>");

?>
</div></div>
<script type="text/javascript">
	$('#cli_switch').click(function(){
		loadOverlay('sites/ventas/switch_cliente.php');
	});
	var empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	var empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;
	var anticipa = <?=check_permiso(28)?1:0 ?>;

</script>
<script type="text/javascript" src="jscripts/product_manager/factura_venta.js?v1_4b13"></script>
<script type="text/javascript">
create_fake_prods();
if($('#fv_nota_venta').val() != '')
	$('#fv_nota_venta').change();
if($('#fv_orden_servicio').val() != '')
	$('#fv_orden_servicio').change();
<?php if(isset($detalles_base)): ?>
	$('#fv_nota_venta,#fv_orden_servicio').attr('disabled',true);
	var detalles_base = <?=json_encode($detalles_base) ?>;
	for(i in detalles_base){
		var det = get_valid_detail(detalles_base[i]);
		det.find('.prod_cant').attr('readonly',true);
		det.find('.del_detail').remove();
		$('#prod_list').append(det);
	}
	actualizar_detalles();
	actualizar_totales();
<?php endif; if(isset($precio_glosa)): ?>
var det = get_fake_detail();
var precio_glosa = <?=json_encode($precio_glosa) ?>;
det.find('.prod_desc').val('Glosa Factura Acumulada');
det.find('.prod_cant').val(1);
det.find('.prod_price,.prod_costo').val(parseFloat(precio_glosa).toFixed(tc_dec)); 
det.appendTo('#prod_list_fake');
det.find('.prod_price').trigger('change');
<?php endif; if(isset($os_num_doc)): ?>
var tot_det = <?=json_encode($os_num_doc) ?>;
for(var i in tot_det){
	var data = tot_det[i];
	var det = get_fake_detail();
	det.find('.prod_desc').val(data.dq_orden_servicio+' - '+data.dg_glosa_facturacion);
	det.find('.prod_cant').val(1);
	det.find('.prod_price,.prod_costo').val(parseFloat(data.dq_neto).toFixed(tc_dec));
	det.appendTo('#prod_list_fake');
	det.find('.prod_price').trigger('change');
}
<?php endif; ?>
$(".date").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});

//Quitar la fecha de la orden de compra por defecto
$(".fv_orden_compra").val('');

/*
 * Evento para quitar el iva al seleccionar 
 * la opción de factura sin iva.
 */
 var ivaProductoModificado = false;
 $('#fv_tipo_operacion').change(function(){
	 var valor = $(this).val();
	 $.post('god.php?factory=FacturaVentaHelper&modulo=ventas&action=index&submodulo=factura_venta',{'fv_tipo_operacion':valor},
		function(data){
			
			var iva = <?=$empresa_conf['dq_iva'] ?>;
			if(data.dm_exenta == 1){
				empresa_iva = '';
				actualizar_detalles();
				actualizar_totales();
			}else{
				empresa_iva = iva;
				actualizar_detalles();
				actualizar_totales();
			}
			
			if(data.dm_agrega_iva_producto == 1){
				
				ivaProductoModificado = true;
				
				$('.add_iva').removeClass('hidden');
				$('#total').attr('colspan','3');
				$('#total_costo').attr('colspan','3');
				$('#total_neto').attr('colspan','3');
				$('#total_iva').attr('colspan','3');
				$('#total_pagar').attr('colspan','3');
				
				$('.prod_price').each(function(){
					var value = toNumber($(this).val()).toFixed(2);
					var num = iva/100 + 1;
					if(value > 0){
						var newValue = value*num;
						var conIva = mil_format((newValue/num).toFixed(2));
						$(this).val(mil_format((newValue).toFixed(2)));
						$(this).parent().prev().html(conIva);
					}
					
				});
				
				$('.prod_costo').each(function(){
					var value = toNumber($(this).val()).toFixed(2);
					var num = iva/100 + 1;
					if(value > 0){
						var newValue = value*num;
						$(this).val(mil_format((newValue).toFixed(2)));
						var conIva = mil_format((newValue/num).toFixed(2));
						$(this).parent().prev().html(conIva);
					}
					
				});
				
				
				
			} else {
				if(ivaProductoModificado){
					ivaProductoModificado = false;
					$('.add_iva').addClass('hidden');
					$('#total').attr('colspan','2');
					$('#total_costo').attr('colspan','2');
					$('#total_neto').attr('colspan','2');
					$('#total_iva').attr('colspan','2');
					$('#total_pagar').attr('colspan','2');
					
					$('.prod_price').each(function(){
						var value = toNumber($(this).val()).toFixed(2);
						var num = iva/100 + 1;
						if(value > 0){
							var sinIva = mil_format((value/num).toFixed(2));
							$(this).val(sinIva);
							$(this).parent().prev().html(0);
						}
						
					});
					$('.prod_costo').each(function(){
						var value = toNumber($(this).val()).toFixed(2);
						var num = iva/100 + 1;
						if(value > 0){
							var sinIva = mil_format((value/num).toFixed(2));
							$(this).val(sinIva);
							$(this).parent().prev().html(0);
						}
						
					});
				}
				
				
			}
			
			actualizar_detalles();
			actualizar_totales();
			
			
			
		},'json');
 });
 // FIN DE EXPORTACION - IVA

</script>
