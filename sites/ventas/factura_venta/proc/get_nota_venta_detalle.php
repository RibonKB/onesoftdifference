<?php
define('MAIN',1);
require_once("../../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$id = $db->select('tb_nota_venta nv
JOIN tb_tipo_nota_venta t ON t.dc_tipo = nv.dc_tipo_nota_venta',
'nv.dc_nota_venta,nv.dq_cambio,nv.dc_tipo_cambio,nv.dm_al_dia,t.dm_facturable,t.dm_costeable,nv.dc_ejecutivo,t.dm_tipo_impresion_factura',
"nv.dc_empresa = {$empresa} AND nv.dq_nota_venta={$_GET['number']} AND nv.dm_validada = '1' AND nv.dm_confirmada = '1' AND dm_nula='0'");

if(!count($id)){
	echo json_encode('<not-found>');
	exit;
}

$id = $id[0];

if($id['dm_facturable'] == 0){
	echo json_encode('<not-facturable>');
	exit;
}

$guias_fisicas = $db->select('tb_guia_despacho gd
JOIN tb_tipo_guia_despacho t ON t.dc_tipo = gd.dc_tipo_guia AND t.dm_facturable = 1',
'group_concat(dq_guia_despacho) dq_guias, count(*) cantidad',"dc_nota_venta = {$id['dc_nota_venta']} AND dc_factura = 0 AND gd.dm_nula = 0");

$id['dc_guias_fisicas'] = $guias_fisicas[0]['cantidad'];
$id['dq_guias'] = $guias_fisicas[0]['dq_guias'];

if($id['dm_costeable'] == 1){
	/*$detalle = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$id['dc_nota_venta']}) d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	LEFT JOIN (SELECT d2.* FROM tb_guia_despacho gd
		JOIN tb_guia_despacho_detalle d2 ON d2.dc_guia_despacho = gd.dc_guia_despacho AND gd.dm_nula = 0
	) dgd ON dgd.dc_detalle_nota_venta = d.dc_nota_venta_detalle",
	"d.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra*d.dm_tipo dq_precio_compra,
	d.dc_proveedor,p.dc_producto,d.dc_facturada,d.dc_despachada,d.dc_comprada,d.dc_recepcionada,p.dm_requiere_serie,d.dm_tipo,GROUP_CONCAT(dgd.dg_serie) dg_series",
	'',array('order_by'=>'d.dm_tipo', 'group_by' => 'd.dc_nota_venta_detalle'));*/
	$detalle = $db->select("tb_nota_venta_detalle d
        	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	        LEFT JOIN tb_guia_despacho_detalle dgd ON dgd.dc_detalle_nota_venta = d.dc_nota_venta_detalle",
        "d.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra*d.dm_tipo dq_precio_compra,
        d.dc_proveedor,p.dc_producto,d.dc_facturada,d.dc_despachada,d.dc_comprada,d.dc_recepcionada,p.dm_requiere_serie,d.dm_tipo,GROUP_CONCAT(dgd.dg_serie) dg_series",
        "d.dc_nota_venta = {$id['dc_nota_venta']}",
	array('order_by'=>'d.dm_tipo', 'group_by' => 'd.dc_nota_venta_detalle'));
}else{
	/*etalle = $db->select("(SELECT * FROM tb_nota_venta_detalle WHERE dc_nota_venta = {$id['dc_nota_venta']} AND dm_tipo=0) d
	JOIN tb_producto p ON p.dc_producto = d.dc_producto
	LEFT JOIN (SELECT d2.* FROM tb_guia_despacho gd
		JOIN tb_guia_despacho_detalle d2 ON d2.dc_guia_despacho = gd.dc_guia_despacho AND gd.dm_nula = 0
	) dgd ON dgd.dc_detalle_nota_venta = d.dc_nota_venta_detalle",
	"d.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,d.dc_facturada,
	p.dc_producto,d.dc_despachada,d.dc_comprada,d.dc_recepcionada,p.dm_requiere_serie,GROUP_CONCAT(dgd.dg_serie) dg_series",
	'',array('group_by'=>'d.dc_nota_venta_detalle'));*/

$detalle = $db->select("tb_nota_venta_detalle d
        JOIN tb_producto p ON p.dc_producto = d.dc_producto
        LEFT JOIN tb_guia_despacho_detalle dgd ON dgd.dc_detalle_nota_venta = d.dc_nota_venta_detalle",
        "d.dc_nota_venta_detalle,p.dg_codigo,d.dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dc_proveedor,d.dc_facturada,
        p.dc_producto,d.dc_despachada,d.dc_comprada,d.dc_recepcionada,p.dm_requiere_serie,GROUP_CONCAT(dgd.dg_serie) dg_series",
        "d.dc_nota_venta = {$id['dc_nota_venta']} AND d.dm_tipo=0",
	array('group_by'=>'d.dc_nota_venta_detalle'));
}

if(!count($detalle)){
	echo json_encode(array('<empty>',$id));
	exit;
}

echo json_encode(array($detalle,$id));
?>
