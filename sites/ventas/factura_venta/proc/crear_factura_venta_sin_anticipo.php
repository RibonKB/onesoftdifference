<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once('../../proc/ventas_functions.php');

if(!is_numeric($_POST['fv_number'])){
	$error_man->showWarning("El folio de la factura debe ser un número válido");
	exit;
}

$valida_numero = $db->doQuery($db->select('tb_factura_venta','1',"dq_folio={$_POST['fv_number']} AND dc_empresa={$empresa} AND dm_nula = '0'"))->fetch();

if($valida_numero !== false){
	$error_man->showWarning("El número de factura <b>{$_POST['fv_number']}</b> es inválido pues ya existe una factura con dicho número.");
	exit();
}

$_POST['fv_tipo_operacion'] = explode('|',$_POST['fv_tipo_operacion']);
$tipo_operacion = $_POST['fv_tipo_operacion'][0];
$tipo_movimiento = $_POST['fv_tipo_operacion'][1];
$tipo_guia = $_POST['fv_tipo_operacion'][2];

/**
*	Obtener los productos que realizan movimientos de stock
*	
*	En el caso en que al menos un producto expuesto en el detalle realice movimiento de stock es candidato a ser un detalle de guía virtual
**/
//los codigos ingresados en el detalle de factura
$codes = implode("','",$_POST['prod']);
//array en el que se almacenarán los ids de productos que mueven stock
$ids = array();

//se consulta por productos que muevan stock
$db_prod = $db->doQuery($db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_codigo IN ('{$codes}')) p
JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto AND dm_controla_inventario = '0'",
'dc_producto,dg_codigo'));

//se le da formato a la estructura de datos para que sea facilmente manipulable y accesible
foreach($db_prod as $p){
	$ids[$p['dg_codigo']] = $p['dc_producto'];
}
unset($db_prod);

//En el caso de que al menos uno de los productos en el detalle mueva stock es candidato a hacer guía virtual con ese detalle
$virtual = count($ids) && (!isset($_POST["doc_type"]) || (isset($_POST['doc_type']) && $_POST['doc_type'] == 1));

/**
*	Comprobar stock
*	
*	Primero se filtran los productos que realizan movimientos de stock, estos serán los que se utilizarán en los procesos donde el stock y cantidades deben ser validadas
*	Se verifica que la cantidad a facturar satisfaga la cantidad reservada para cada detalle de la nota de venta.
*	Para esto se verifica solo las
*	cantidades pendientes de despacho (cantidad a facturar menos cantidad ya despachada)
*	cantidad disponibles/reservadas restantes (Cantidad recepcionada menos cantidad ya despachada)
**/

if($virtual){
	$bodega = $_POST['fv_tipo_operacion'][3];
	$guia_tipo_movimiento = $_POST['fv_tipo_operacion'][4];
	
	$to_guia = array();
	
	if($_POST['fv_id_nota_venta'] != 0){
		foreach($_POST['prod'] as $i => $p){
			if(isset($ids[$p])){
				$pendiente = $_POST['cant'][$i]-$_POST['despachada'][$i];
				
				if($pendiente > 0){
					$disponible = $_POST['recepcionada'][$i]-$_POST['despachada'][$i];
					
					if($pendiente > $disponible){
						$error_man->showWarning("No hay stock reservado suficiente para la nota de venta<br />
						Error detectado en el detalle <b>{$p}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />
						Cantidad necesaria : <b>{$pendiente}</b><br />
						Cantidad Disponible: <b>{$disponible}</b><br /><br />
						Debe hacer las compras, recepciones y/o traspasos necesarios para satisfacer la necesidad de stock");
						exit;
					}//END if pendiente > disponible
					
					$to_guia[] = $i;
				}//END if pendiente
			}//END if manipula stock
		}//END foreach products
	}//END if $_POST['fv_id_nota_venta']
	else{
		//Si no se ha asignado nota de venta pero si hay productos que mueven stock entonces hay que revisar si hay stock libre para asignarle.
		
		foreach($_POST['prod'] as $i => $p){
			if(isset($ids[$p]))
				$to_guia[] = $i;
		}
		
	}//END if not nota_venta
	
}//END if $virtual

$db->start_transaction();
/**
*	Rebajar stock
*	
*	Luego de verificado el stock reservado se hace la rebaja de stock correspondiente de la bodega
*	Esto también puede generar error si no hay stock suficiente, para ello se debe realizar una nueva recepción general (sin reservar)	
**/

if($virtual){
	if($_POST['fv_id_nota_venta'] != 0){
		foreach($to_guia as $i){
			$cantidad = $_POST['cant'][$i]-$_POST['despachada'][$i];
			
			$existencia = bodega_RebajarStockReservado($ids[$_POST['prod'][$i]],$cantidad,$bodega);
			
			if($existencia != 1){
				
				$nombre_bodega = $db->doQuery($db->select('tb_bodega','dg_bodega',"dc_bodega={$bodega}"))->fetch(PDO::FETCH_OBJ)->dg_bodega;
				
				$error_man->showWarning("Hay stock reservado suficiente para la nota de venta pero no se encontró stock disponible en bodega {$nombre_bodega}<br />
				Error detectado en el detalle <b>{$_POST['prod'][$i]}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />
				Cantidad necesaria : <b>{$cantidad}</b><br />
				Bodega en que se busca: <b>{$nombre_bodega}</b>");
				
				$db->rollBack();
				exit;
			}
		}//END foreach to_guia
	}//END if nota venta
	else{
		//Movimiento de stock si no hubiese asignación de nota de venta

		foreach($to_guia as $i){
			$existencia = bodega_RebajarStockLibre($ids[$_POST['prod'][$i]],$_POST['cant'][$i],$bodega);
			
			if($existencia != 1){
				$nombre_bodega = $db->doQuery($db->select('tb_bodega','dg_bodega',"dc_bodega={$bodega}"))->fetch(PDO::FETCH_OBJ)->dg_bodega;
				
				$error_man->showWarning("No hay stock libre suficiente en la bodega <b>{$nombre_bodega}</b><br />
				Error detectado en el detalle <b>{$_POST['prod'][$i]}</b> - <i>{$_POST['desc'][$i]}</i><br /><br />
				Cantidad necesaria : <b>{$_POST['cant'][$i]}</b><br />
				Bodega en que se busca: <b>{$nombre_bodega}</b>");
				
				$db->rollBack();
				exit;
			}
		}
		
	}
}//END if virtual

/**
*	Almacenar Factura
*	
*	Ya rebajados los stocks se procede a almacenar la factura, para obtener el correlativo generado dependencia para los siguientes procesos
**/

$correlativo = doc_GetNextNumber('tb_factura_venta','dq_factura');

$factura = $db->prepare($db->insert('tb_factura_venta',array(
	'dc_nota_venta' => $_POST['fv_id_nota_venta'],
	'dc_orden_servicio' => $_POST['fv_id_orden_servicio'],
	'dc_ejecutivo' => $_POST['fv_ejecutivo'],
	'dc_cliente' => $_POST['cli_id'],
	'dc_contacto' => $_POST['fv_contacto'],
	'dc_contacto_entrega' => $_POST['fv_cont_entrega'],
	'dc_medio_pago' => $_POST['fv_medio_pago'],
	'dc_tipo_cargo' => $_POST['fv_tipo_cargo'],
	'dc_tipo_operacion' => $tipo_operacion,
	'dq_factura' => $correlativo,
	'dq_folio' => '?',
	'dg_comentario' => '?',
	'df_emision' => $db->sqlDate($_POST['fv_emision']),
	'df_creacion' => $db->getNow(),
	'df_vencimiento' => $db->sqlDate($_POST['fv_vencimiento']),
	'dg_orden_compra' => '?',
	'dq_neto' => $_POST['cot_neto'],
	'dq_iva' => $_POST['cot_iva'],
	'dq_total' => $_POST['cot_neto']+$_POST['cot_iva'],
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa,
	'dm_tipo_impresion' => $_POST['fv_tipo_impresion']
)));

$factura->bindValue(1,$_POST['fv_number'],PDO::PARAM_INT);
$factura->bindValue(2,$_POST['fv_comentario'],PDO::PARAM_STR);
$factura->bindValue(3,$_POST['fv_orden_compra'],PDO::PARAM_STR);

$db->stExec($factura);

$factura = $db->lastInsertId();

/**
*	Almacenar detalle factura
*	
*	Como ya se han realizado las validaciones de stock necesarias se puede directamente almacenar los detalles de la factura
**/

$det_st = $db->prepare($db->insert('tb_factura_venta_detalle',array(
	'dc_factura' => $factura,
	'dg_producto' => '?',
	'dc_detalle_nota_venta' => '?',
	'dg_descripcion' => '?',
	'dm_tipo' => '?',
	'dc_cantidad' => '?',
	'dq_precio' => '?',
	'dq_descuento' => '?',
	'dq_total' => '?'
)));

$det_st->bindParam(1,$producto,PDO::PARAM_STR);
$det_st->bindParam(2,$detalle_nv,PDO::PARAM_INT);
$det_st->bindParam(3,$descripcion,PDO::PARAM_STR);
$det_st->bindParam(4,$tipo,PDO::PARAM_STR);
$det_st->bindParam(5,$cantidad,PDO::PARAM_INT);
$det_st->bindParam(6,$precio,PDO::PARAM_INT);
$det_st->bindParam(7,$descuento,PDO::PARAM_INT);
$det_st->bindParam(8,$total,PDO::PARAM_INT);

$tipo = 0;
$from_nv = isset($_POST['id_detail']);
foreach($_POST['prod'] as $i => $v){
	
	$producto = $v;
	$detalle_nv = $from_nv?$_POST['id_detail'][$i]:0;
	$descripcion = $_POST['desc'][$i];
	$cantidad = str_replace(',','',$_POST['cant'][$i]);
	$precio = str_replace(',','',$_POST['precio'][$i]);
	$descuento = str_replace(',','',$_POST['descuento'][$i]);
	$total = $precio*$cantidad-$descuento;
	
	$db->stExec($det_st);
}

if(isset($_POST['fake_prod'])){
	$tipo = 1;
	foreach($_POST['fake_prod'] as $i => $v){
		$producto = $v;
		$detalle_nv = 0;
		$descripcion = $_POST['fake_desc'][$i];
		$cantidad = str_replace(',','',$_POST['fake_cant'][$i]);
		$precio = str_replace(',','',$_POST['fake_precio'][$i]);
		$descuento = str_replace(',','',$_POST['fake_descuento'][$i]);
		$total = $precio*$cantidad-$descuento;
		
		$db->stExec($det_st);
	}
}//END Almacenar detalle glosa

/**
*	Asignar factura a guías físicas de la nota de venta
*	
*	En caso de que la nota de venta haya sido despachada anteriormente con guías físicas estas deben quedar relacionadas con la factura de venta.
*	
*	IN: id_nota_venta, factura
*	OUT: Guías de despacho físicas de la nota de venta con factura asignada.
**/

if($_POST['fv_id_nota_venta'] != 0){
	$guias_facturadas = $db->doExec($db->update('tb_guia_despacho',array('dc_factura' => $factura),"dc_nota_venta = {$_POST['fv_id_nota_venta']} AND dc_factura = 0"));
}

/**
*	Generar Guía Virtual
*	
*	Las guías virtuales son generadas para las cantidades pendientes de despacho (Cantidad a facturar menos cantidades ya despachadas)
*	
**/

if($virtual && count($to_guia)){
	
	$num_actual = doc_GetNextNumber('tb_guia_despacho','dq_guia_despacho');
	
	$guia_template = $db->prepare($db->insert('tb_guia_despacho',array(
		'dc_tipo_guia' => $tipo_guia,
		'dc_nota_venta' => $_POST['fv_id_nota_venta'],
		'dc_orden_servicio' => $_POST['fv_id_orden_servicio'],
		'dc_factura' => $factura,
		'dc_cliente' => $_POST['cli_id'],
		'dc_contacto' => $_POST['fv_contacto'],
		'dc_contacto_entrega' => $_POST['fv_cont_entrega'],
		'dq_guia_despacho' => '?',
		'dg_comentario' => '?',
		'df_emision' => $db->getNow(),
		'dg_orden_compra' => '?',
		'dq_neto' => $_POST['cot_neto'],
		'dq_iva' => $_POST['cot_iva'],
		'dq_total' => $_POST['cot_neto']+$_POST['cot_iva'],
		'dc_empresa' => $empresa
	)));
	
	$guia_template->bindParam(1,$num_actual,PDO::PARAM_INT);
	$guia_template->bindValue(2,"Guía virtual generada automáticamente para la factura {$_POST['fv_number']}",PDO::PARAM_STR);
	$guia_template->bindValue(3,$_POST['fv_orden_compra'],PDO::PARAM_STR);
	
	$detalle_template = $db->prepare($db->insert('tb_guia_despacho_detalle',array(
		'dc_guia_despacho' => '?',
		'dg_producto' => '?',
		'dc_detalle_nota_venta' => '?',
		'dg_descripcion' => '?',
		'dg_serie' => '?',
		'dq_precio' => '?',
		'dc_bodega_salida' => $bodega
	)));
	
	$detalle_template->bindParam(1,$id_actual,PDO::PARAM_STR);
	$detalle_template->bindParam(2,$producto,PDO::PARAM_STR);
	$detalle_template->bindParam(3,$detalle_nv,PDO::PARAM_INT);
	$detalle_template->bindParam(4,$descripcion,PDO::PARAM_STR);
	$detalle_template->bindParam(5,$serie,PDO::PARAM_STR);
	$detalle_template->bindParam(6,$precio,PDO::PARAM_INT);
	
	$db->stExec($guia_template);
	$num_guia =& $num_actual;
	$id_actual = $db->lastInsertId();
	
	$num_detalles = 0;
	$movs = array();
	//$num_guias = array();
	foreach($to_guia as $i){
		//Si los detalles son de una nota de venta solo hay que realizar guías de las cantidades no despachadas
		if($_POST['fv_id_nota_venta'] != 0){
			$cantidad = $_POST['cant'][$i]-$_POST['despachada'][$i];
		}else{
			$cantidad = $_POST['cant'][$i];
		}
		
		$series = explode(',',$_POST['serie'][$i]);
		
		$producto = $_POST['prod'][$i];
		$detalle_nv = $from_nv?$_POST['id_detail'][$i]:0;
		$descripcion = $_POST['desc'][$i];
		$precio = str_replace(',','',$_POST['precio'][$i]);
		
		$movs[$producto] = array();
		$movs[$producto]['guias'] = array();
		$movs[$producto]['precio'] = $precio;
		
		
		
		
		for($j=0;$j<$cantidad;$j++){
			/*if($num_detalles%20 == 0){
				$db->stExec($guia_template);
				$num_guias[] = $num_actual;
				$num_actual = doc_GetRealNext($num_actual);
				$id_actual = $db->lastInsertId();
			}
			$num_detalles++;*/
			
			if(isset($series[$j])){
				$serie = $series[$j];
			}else{
				$serie = '';
			}
			
			$movs[$producto]['guias'][$id_actual][] = $serie;
			
			$db->stExec($detalle_template);
			
			
		}
	}
}//END if se hacen guías virtuales

/**
*	Registrar movimientos de bodega
*	
*	Se registran los despachos extras que se realizaron para satisfacer las cantidades pendientes
**/

if($virtual && count($to_guia)){
	
	$template_mov = $db->prepare($db->insert('tb_movimiento_bodega',array(
				'dc_cliente' => $_POST['cli_id'],
				'dc_tipo_movimiento' => $guia_tipo_movimiento,
				'dc_bodega_salida' => $bodega,
				'dq_monto' => '?',
				'dc_guia_despacho' => '?',
				'dg_series' => '?',
				'dc_nota_venta' => $_POST['fv_id_nota_venta'],
				'dc_producto' => '?',
				'dq_cantidad' => '?',
				'dc_empresa' => $empresa,
				'df_creacion' => $db->getNow(),
				'dc_usuario_creacion' => $idUsuario,
				'dc_factura' => $factura
			)));

	$template_mov->bindParam(1,$monto,PDO::PARAM_INT);
	$template_mov->bindParam(2,$guia,PDO::PARAM_INT);
	$template_mov->bindParam(3,$series_str,PDO::PARAM_STR);
	$template_mov->bindParam(4,$producto,PDO::PARAM_INT);
	$template_mov->bindParam(5,$cantidad,PDO::PARAM_INT);
	
	foreach($movs as $codigo => $v){
		
	$producto = $ids[$codigo];
	$monto = $v['precio'];
	
	foreach($v['guias'] as $guia_actual => $arr){
			$series_str = implode(',',array_diff($arr,array('')));
			$cantidad = count($arr)*-1;
			$guia = $guia_actual;
			
			$db->stExec($template_mov);
		}
	}
}


/**
*	Asignación de cantidades Facturadas
**/

if($_POST['fv_id_nota_venta'] != 0 && $from_nv){
	/**
	*	Asignar cantidades facturadas a detalles de la nota de venta
	*	
	*	Para saber cuantas cantidades faltan aún por facturar en caso de que la nota de venta sea facturada multiples veces
	*	También sirve para validar si una nota de venta está pendiente de facturación.
	**/

	$factured_template = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dc_facturada' => 'dc_facturada+?'
	),"dc_nota_venta_detalle=?"));
	
	$factured_template->bindParam(1,$facturado,PDO::PARAM_INT);
	$factured_template->bindParam(2,$id_detalle,PDO::PARAM_INT);
	
	$total_facturado = 0;
	
	foreach($_POST['prod'] as $i => $v){
		$facturado = $_POST['cant'][$i];
		$id_detalle = $_POST['id_detail'][$i];
		
		$db->stExec($factured_template);
		
		$total_facturado += $facturado;
	}

	/**
	*	Asignar Cantidad facturada a nota de venta
	*	
	*	Al igual que en el proceso anterior pero a nivel de cabecera para tener una idea global de cantidades pendientes de facturación
	**/

	if($total_facturado > 0){
		$nv_factured = $db->doExec($db->update('tb_nota_venta',array(
			'dc_facturada' => "dc_facturada+{$total_facturado}"
		),"dc_nota_venta = {$_POST['fv_id_nota_venta']}"));
		
		if($nv_factured == 0){
			$error_man->showWarning('Ocurrió un error intentando relacionar la factura con la nota de venta, compruebe la existencia de la nota de venta.');
			$db->rollBack();
			exit;
		}
	}
	
}//END if asignar cantidades facturadas a nota de venta


/**
*	Asignación de cantidades despachadas guia de despacho y asignación de facturas a guías ya asignadas
**/
if($_POST['fv_id_nota_venta'] != 0 && $from_nv && $virtual && count($to_guia)){

	/**
	*	Asignar cantidad despachada a detalles nota de venta
	*	
	*	En caso de que se hayan realizado guías de despacho estas deben ser consideradas en las cantidades despachas de los detalles de la nota de venta
	*	Para saber las cantidades pendientes de despacho en futuras facturaciones/despachos
	**/
	
	$dispatch_template = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dc_despachada' => 'dc_despachada+?'
	),"dc_nota_venta_detalle=?"));
	
	$dispatch_template->bindParam(1,$despachado,PDO::PARAM_INT);
	$dispatch_template->bindParam(2,$id_detalle,PDO::PARAM_INT);
	
	$total_despachado = 0;
	
	foreach($to_guia as $i){
		$despachado = $_POST['cant'][$i]-$_POST['despachada'][$i];
		$id_detalle = $_POST['id_detail'][$i];
		
		$db->stExec($dispatch_template);
		
		$total_despachado += $despachado;
	}
	
	
	/**
	*	Asignar cantidad despachada a nota de venta
	*	
	*	El mismo proceso anterior pero a nivel de cabecera, para tener una idea global de las cantidades pendientes de despacho.
	**/
	
	if($total_despachado > 0){
		$nv_dispatch = $db->doExec($db->update('tb_nota_venta',array(
			'dc_despachada' => "dc_despachada+{$total_despachado}"
		),"dc_nota_venta = {$_POST['fv_id_nota_venta']}"));
		
		if($nv_dispatch == 0){
			$error_man->showWarning('Ocurrió un error intentando relacionar las guías virtuales con la nota de venta, compruebe la existencia de la nota de venta.');
			$db->rollBack();
			exit;
		}
	}
	
}//END if asignar cantidades despachadas a nota de venta

	
/**
*	Asignar cantidad facturadas a notas de venta de documentos facturados con el gestor de facturas
*
*	al facturar guías de despacho es necesario asignar las cantidades facturadas a las notas de venta relacionadas a las guías
*	en caso de tener
*	además de asignar a estos documentos la factura recién creada
*	Esto último también a la facturación de múltiples ordenes de servicio
**/

if(isset($_POST["doc_type"])){
	if($_POST['doc_type'] == 0){
		$num_docs = $db->doQuery($db->select("(SELECT * FROM tb_guia_despacho_detalle WHERE dc_guia_despacho IN ({$_POST['doc_list']})) d
		JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta",
		'd.dc_detalle_nota_venta,count(d.dc_detalle_nota_venta) AS dq_cantidad,nvd.dc_nota_venta','',array('group_by' => 'd.dc_detalle_nota_venta')));
		
		$factured_template = $db->prepare($db->update('tb_nota_venta_detalle',array(
			'dc_facturada' => 'dc_facturada+?'
		),"dc_nota_venta_detalle=?"));
		
		$factured_template->bindParam(1,$facturado,PDO::PARAM_INT);
		$factured_template->bindParam(2,$id_detalle,PDO::PARAM_INT);
		
		$notas = array();
		foreach($num_docs as $n){
			$facturado = $n['dq_cantidad'];
			$id_detalle = $n['dc_detalle_nota_venta'];
			
			$db->stExec($factured_template);
			
			$notas[$n['dc_nota_venta']][] = $n['dq_cantidad'];
		}
		
		$factured_template = $db->prepare($db->update('tb_nota_venta',array(
			'dc_facturada' => 'dc_facturada+?'
		),"dc_nota_venta=?"));
		
		$factured_template->bindParam(1,$sum,PDO::PARAM_INT);
		$factured_template->bindParam(2,$indice,PDO::PARAM_INT);
		
		foreach($notas as $i => $n){
			$sum = array_sum($n);
			$indice = $i;
			
			$db->stExec($factured_template);
		}
	}
	
	switch($_POST['doc_type']){
		case 0:
			$db->doExec($db->update('tb_guia_despacho',array('dc_factura' => $factura),"dc_guia_despacho IN ({$_POST['doc_list']})"));
			break;
		case 1:
			$db->doExec($db->update('tb_orden_servicio',array('dc_factura' => $factura),"dc_orden_servicio IN ({$_POST['doc_list']})"));
			break;
		default: $db->rollback(); exit();
	}
}

$db->commit();

$_POST['fv_number'] = htmlentities($_POST['fv_number']);

$error_man->showConfirm("Se ha almacenado la factura de venta correctamente.");
$error_man->showInfo("El número de folio para factura física es el <strong>{$_POST['fv_number']}</strong><br />
Además de eso el número de identificación interno es el <h1 style='margin:0;color:#000;'>{$correlativo}</h1>
<button type='button' class='button' id='print_factura'>Imprimir</button>");

if($guias_antiguas){
	$error_man->showInfo("La nota de venta poseía <b>{$guias_antiguas}</b> guias de despacho antiguas a las cuales se le asignó la factura");
}

if(isset($num_guia)){
	$error_man->showConfirm("Se ha generado la siguiente guía de despacho virtual <strong>sin folio</strong>");
	$error_man->showInfo("El número de guía de despacho interno es <h1 style='margin:0;color:#000;'>{$num_guia}</h1>");
}

?>
<script type="text/javascript">
$('#print_factura').click(function(){
	loadOverlay('sites/ventas/factura_venta/print_factura_venta.php?id=<?=$factura ?>');
});
</script>