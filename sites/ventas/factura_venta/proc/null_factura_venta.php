<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}


//51

if(check_permiso(51))
	$month_cond = '';
else
	$month_cond = 'AND MONTH(df_emision) = MONTH(NOW())';

$data = $db->select('tb_factura_venta','dc_nota_venta,dc_orden_servicio,dm_nula,dm_centralizada',"dc_factura={$_POST['id_factura']} {$month_cond}");
if(!count($data)){
	$error_man->showWarning('No se ha encontrado la factura especificada o esta no puede anularse por no encontrarse en el periodo actual.');
	exit();
}
$data = $data[0];

//Comprobar que no esté ya nula
if($data['dm_nula'] == 1){
	$error_man->showWarning("La factura de venta ya ha sido anulada anteriormente");
	exit;
}

//Comprobar centralización
if($data['dm_centralizada'] == 1){
	$error_man->showWarning("La factura de venta ha sido incluida en una centralización, se debe anular los comprobantes de centralización antes");
	exit;
}

$db->start_transaction();

//Comprobar que no se esté utilizando la ordende servicio o nota de venta asignada a la factura
$db->update('tb_factura_venta',array('dm_nula'=>1),"dc_factura={$_POST['id_factura']}");

//Detalles solo para productos que no mueven stock
$detalles = $db->select("tb_factura_venta_detalle",'dc_detalle_nota_venta,dc_cantidad',
"dc_factura={$_POST['id_factura']} AND dc_detalle_nota_venta > 0");
//Comprobar que no se esté usando el numero de detalle de la nota de venta para otro proceso.

//$db->update('tb_factura_venta_detalle',array('dc_detalle_nota_venta'=>0),"dc_factura={$_POST['id_factura']}");

$db->insert('tb_factura_venta_anulada',array(
	'dc_factura' => $_POST['id_factura'],
	'dc_motivo' => $_POST['null_motivo'],
	'dm_fisica' => $_POST['null_fisica'],
	'dg_comentario' => $_POST['null_comentario'],
	'dc_usuario' => $idUsuario,
	'df_anulacion' => 'NOW()'
));

$facturada = 0;
foreach($detalles as $d){
	$db->update('tb_nota_venta_detalle',array('dc_facturada'=>"dc_facturada-{$d['dc_cantidad']}"),"dc_nota_venta_detalle={$d['dc_detalle_nota_venta']}");
	$facturada += $d['dc_cantidad'];
}

if($facturada && $data['dc_nota_venta']){
	$db->update('tb_nota_venta',array('dc_facturada'=>"dc_facturada-{$facturada}"),"dc_nota_venta={$data['dc_nota_venta']}");
}

$db->update('tb_orden_servicio',array('dc_factura'=>0),"dc_factura={$_POST['id_factura']}");

$num_docs = $db->select("tb_guia_despacho gd
	JOIN tb_guia_despacho_detalle d ON d.dc_guia_despacho = gd.dc_guia_despacho
	JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta",
'd.dc_detalle_nota_venta,SUM(d.dq_cantidad) dq_cantidad,nvd.dc_nota_venta,gd.dq_folio,nvd.dc_producto,gd.dc_guia_despacho,d.dc_bodega_salida',
"gd.dc_factura = {$_POST['id_factura']}",
array('group_by' => 'd.dc_detalle_nota_venta'));

$tipo_movimiento = $db->select('tb_configuracion_logistica','dc_tipo_movimiento_entrada_anulacion',"dc_empresa={$empresa}");
$tipo_movimiento = $tipo_movimiento[0]['dc_tipo_movimiento_entrada_anulacion'];

//Rebajar cantidades facturadas de detalles de nota de venta para documentos multiples seleccionados para la factura
/*if($data['dc_nota_venta'] == 0){ //No hay una nota de venta asignada a la factura, pueden ser muchas y van relacionadas a los demás documentos
	
	$db->update('tb_guia_despacho gd
	JOIN tb_guia_despacho_detalle d ON d.dc_guia_despacho = gd.dc_guia_despacho
	JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
	JOIN tb_nota_venta nv ON nv.dc_nota_venta = nvd.dc_nota_venta',array(
		'nvd.dc_facturada' => 'nvd.dc_facturada-d.dq_cantidad',
		'nv.dc_facturada' => 'nv.dc_facturada-d.dq_cantidad'
	),"gd.dc_factura = {$_POST['id_factura']}");
	
}/**/

//Solo para detalles conproductos que mueven stock
if(count($num_docs)){
$notas = array();
$notas_desp = array();
$guias = array();
$guias_virtuales = array();
	foreach($num_docs as $n){
		if($n['dq_folio'] != 0){
			$guias[] = $n['dc_guia_despacho'];
		}else{
			
			$guias_virtuales[] = $n['dc_guia_despacho'];
			
			$db->update('tb_nota_venta_detalle',array(
				'dc_despachada' => "dc_despachada-{$n['dq_cantidad']}"
			),"dc_nota_venta_detalle={$n['dc_detalle_nota_venta']}");
			$notas_desp[$n['dc_nota_venta']][] = $n['dq_cantidad'];
			
			$db->update('tb_stock',array(
				'dq_stock' => "dq_stock+{$n['dq_cantidad']}",
				'dq_stock_reservado' => "dq_stock_reservado+{$n['dq_cantidad']}"
			),"dc_producto = {$n['dc_producto']} AND dc_bodega = {$n['dc_bodega_salida']}");
			
			$db->query("INSERT INTO tb_movimiento_bodega (dc_cliente,dc_tipo_movimiento,dc_bodega_entrada,dq_monto,dq_cambio,dc_guia_despacho,dg_series,dc_factura,dc_nota_venta,dc_cebe,dc_ceco,dc_producto,dq_cantidad,dc_empresa,df_creacion,dc_usuario_creacion)
			SELECT
dc_cliente,{$tipo_movimiento},dc_bodega_salida,dq_monto,dq_cambio,dc_guia_despacho,dg_series,dc_factura,dc_nota_venta,dc_cebe,dc_ceco,dc_producto,-dq_cantidad,{$empresa},NOW(),{$idUsuario}
			FROM tb_movimiento_bodega WHERE dc_guia_despacho = {$n['dc_guia_despacho']} AND dc_producto = {$n['dc_producto']}");
			
		}
	}
	
	foreach($notas_desp as $i => $n){
		$sum = array_sum($n);
		$db->update('tb_nota_venta',array(
			'dc_despachada' => "dc_despachada-{$sum}"
		),"dc_nota_venta={$i}");
	}
	
	if(count($guias)){
		$guias = implode(',',array_unique($guias));
		
		$db->update('tb_guia_despacho',array(
			'dc_factura' => 0
		),"dc_guia_despacho IN ({$guias})");
	}
	
	if(count($guias_virtuales)){
		$guias_virtuales = implode(',',array_unique($guias_virtuales));
		
		$db->update('tb_guia_despacho',array(
			'dm_nula' => 1
		),"dc_guia_despacho IN ({$guias_virtuales})");
	}
}

$db->update('tb_nota_venta_contrato_detalle',array(
	"dc_factura" => 'NULL'
),"dc_factura = {$_POST['id_factura']}");

$db->commit();
$error_man->showAviso("Atención a anulado la factura de venta y las relaciones con otros documentos");
?>
<script type="text/javascript">
$('#res_list .confirm .fv_load').trigger('click');
</script>