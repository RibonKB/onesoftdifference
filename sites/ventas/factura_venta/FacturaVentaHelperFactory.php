<?php

class FacturaVentaHelperFactory extends Factory {
	
	public function indexAction(){
		// echo 'Hola';
		$r = $this->getRequest();
		$tipoOperacionBruto = explode('|',$r->fv_tipo_operacion);
		$tipoOperacion = $tipoOperacionBruto[0];
		
		$db = $this->getConnection();
		$deglose = $db->getRowById('tb_tipo_operacion', $tipoOperacion, 'dc_tipo_operacion');
		
		echo json_encode($deglose);
		
	}
	
}