<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Libro de ventas</div>
<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/ventas/factura_venta/proc/src_libro_venta.php","src_libro_venta",'overlayValidar');
	$form->Header("<strong>Indicar los parámetros de consulta del libro de ventas</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','fv_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','fv_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','fv_cliente','tb_cliente',array('dc_cliente','dg_razon'));
	echo('</td><td>&nbsp;</td></tr><tr><td>Ejecutivo (Vendedor)</td><td>');
	$form->ListadoMultiple('','fv_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
	echo('</td><td>&nbsp;</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#fv_cliente,#fv_ejecutivo').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>