<?php
define("MAIN",1);
require_once("../../../inc/init.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$meses = array(
	1 => 'Enero',
	2 => 'Febrero',
	3 => 'Marzo',
	4 => 'Abril',
	5 => 'Mayo',
	6 => 'Junio',
	7 => 'Julio',
	8 => 'Agosto',
	9 => 'Septiembre',
	10 => 'Octubre',
	11 => 'Noviembre',
	12 => 'Diciembre'
);
?>

<div id="secc_bar">Informe de Margenes</div>
	<div id="main_cont">
    <div class="panes">

        <?php $form->Start('sites/ventas/factura_venta/proc/src_informe_margenes.php','informe_margenes') ?>
        <?php $form->Header("<b>Indique el periodo del que quiere obtener el informe de margenes</b>") ?>
        
       <table class="tab" width="100%">
        	<tbody>
            	<tr>
                	<td>Periodo</td>
                    <td><?php $form->Select('Mes','dc_mes',$meses,1,date('n')); ?></td>
                    <td><?php $form->Text('Año','dc_anho',1,4,date('Y')); ?></td>
                </tr>
                <?php if(check_permiso(72)): ?>
                <tr>
                	<td>Ejecutivo</td>
                    <td><?php $form->DBMultiSelect('','dc_ejecutivo','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno')) ?></td>
                    <td>&nbsp;</td>
                </tr>
                <?php endif; ?>
                <tr>
                	<td>Tipo de cambio</td>
                    <td><?php $form->DBSelect('','dc_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'),1) ?></td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <?php if(!check_permiso(72)): ?>
        	<?php $form->Hidden('dc_ejecutivo[]',$userdata['dc_funcionario']) ?>
        <?php endif; ?>
        <?php $form->End('Consultar','searchbtn') ?>
</div></div>
<script type="text/javascript" src="jscripts/sites/ventas/factura_venta/src_informe_margenes.js?v=1_1"></script>
<script type="text/javascript">
	$('#dc_ejecutivo').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>