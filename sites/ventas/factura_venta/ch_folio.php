<?php
define("MAIN",1);
require_once("../../../inc/global.php");

$db->escape($_POST['id']);
$factura = $db->select('tb_factura_venta','dq_factura',"dc_factura = {$_POST['id']}");

if(!count($factura)){
	$error_man->showWarning('No se ha encontrado la factura especificada, intentelo denuevo.');
	exit;
}
$factura = $factura[0]['dq_factura'];

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

echo('<div class="secc_bar">Cambiar folio de Factura</div><div class="panes center">');
$form->Start('sites/ventas/factura_venta/proc/change_folio_factura_venta.php','ch_folio_factura');
$form->Header("Indique el nuevo folio para la factura <b>{$factura}</b>");
$form->Text('Nuevo Folio','dq_folio',1);
$form->Hidden('dc_factura',$_POST['id']);
$form->End('Editar','editbtn');
echo('</div></div>');
?>