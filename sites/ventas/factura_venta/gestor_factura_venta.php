<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Gestión de compras</div><div id="main_cont"><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$meses = array(
	1 => 'Enero',
	2 => 'Febrero',
	3 => 'Marzo',
	4 => 'Abril',
	5 => 'Mayo',
	6 => 'Junio',
	7 => 'Julio',
	8 => 'Agosto',
	9 => 'Septiembre',
	10 => 'Octubre',
	11 => 'Noviembre',
	12 => 'Diciembre'
);

$form->Start('#','load_documentos','load_documentos');
$form->Header('Indique los filtros a aplicar en las compras pendientes');
$form->Section();
$form->Text('Documento','sel_doc');
$form->Select('Tipo documento','sel_tipo',array('Guia de despacho','Orden de servicio'));
$form->EndSection();
$form->Section();
$form->Select('Mes inicio','sel_inim',$meses,1,1);
$form->Select('Mes fin','sel_finm',$meses,1,12);
$form->EndSection();
$form->Section();
$form->Listado('Cliente','sel_cli','tb_cliente',array('dc_cliente','dg_razon'));
$form->EndSection();
$form->End('Filtrar');

$form->Start('#','crear_factura_venta','crear_factura_venta');
$form->Header('
<div class="right" id="filter_menu">
<button class="button" type="button">Ver/Ocultar Filtros</button><br />
<div class="hidden">
	<label><input type="checkbox" id="semaforo" checked="checked" />Sémaforo</label><br />
	<label><input type="checkbox" id="docnum" checked="checked" />Número Documento</label><br />
	<label><input type="checkbox" id="docdate" checked="checked" />Fecha emisión</label><br />
	<label><input type="checkbox" id="docclient" checked="checked" />Cliente</label><br />
	<label><input type="checkbox" id="docneto" checked="checked" />Neto</label><br />
	<label><input type="checkbox" id="dociva" checked="checked" />IVA</label><br />
	<label><input type="checkbox" id="doctotal" checked="checked" />Total</label>
</div>
</div>
Compras pendientes','style="position:relative"');
echo('<table class="tab" width="100%" id="pend_table"><thead><tr>
	<th width="20">&nbsp;</th>
	<th width="20"><input type="checkbox" id="select_all"></th>
	<th width="150">Documento</th>
	<th width="150">Fecha Emision</th>
	<th>Cliente</th>
	<th width="100">Neto</th>
	<th width="100">IVA</th>
	<th width="100">Total</th>
</tr></thead><tbody id="prod_list"></tbody>
</table>');
$form->End('Crear Factura','addbtn');

$gd_pendientes = $db->select("tb_guia_despacho gd
JOIN tb_tipo_guia_despacho t ON t.dc_tipo = gd.dc_tipo_guia",
'dc_cliente,dc_guia_despacho AS dc_documento,dq_guia_despacho AS dq_documento,DATE_FORMAT(df_emision,"%d/%m/%Y") AS df_emision,dq_neto,dq_iva,MONTH(df_emision) AS dc_month',
"gd.dc_factura = 0 AND gd.dc_empresa={$empresa} AND gd.dm_nula = 0 AND gd.dq_folio != 0 AND t.dm_facturable = 1");

$os_pendientes = $db->select("tb_orden_servicio",
'dc_cliente,dc_orden_servicio AS dc_documento,dq_orden_servicio AS dq_documento,DATE_FORMAT(df_fecha_emision,"%d/%m/%Y") AS df_emision,dq_neto,dq_iva,MONTH(df_fecha_emision) AS dc_month',
"dc_empresa={$empresa} AND dm_facturable=1 AND dc_factura = 0 AND dm_estado='1'");

?>
</div></div>
<script type="text/javascript" src="jscripts/product_manager/gestor_factura_venta.js?v0_1_4"></script>
<script type="text/javascript">
	js_data.pendientes[0] = <?=json_encode($gd_pendientes) ?>;
	js_data.pendientes[1] = <?=json_encode($os_pendientes) ?>;
	js_data.init();
</script>