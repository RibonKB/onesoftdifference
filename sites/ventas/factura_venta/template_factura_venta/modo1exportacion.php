<?php
require("../../../inc/fpdf.php");
require_once("Numbers/Words.php");

class PDF extends FPDF{

private $datosEmpresa;
private $margenY = 0.9;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','cm',array(21.5,28));
	
}

function Header(){
	global $datosFactura;
	
	//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
	//$this->SetXY(x,y);
	//MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
	
	$this->SetFont('Arial','',12);
	$this->SetTextColor(0,0,0);
	
	$this->SetXY(15.5,$this->margenY+1.1);//4.2);
	$this->Cell(3,0.5,$datosFactura['dq_factura'],0,0,'C');
	//Datos de cabecera (información)
	
	$this->SetFont('Arial','',9);
	
	$this->setXY(4,$this->margenY+4.5);
	$this->cell(5.5,0,$datosFactura['dg_razon']);
	
	$this->setXY(4,$this->margenY+5.7);
	$this->cell(5.5,0,$datosFactura['dg_direccion']);
	
	$this->setXY(4,$this->margenY+6.9);
	$this->cell(5.5,0,'');//Casilla Postal
	
	$this->setXY(4,$this->margenY+8);
	$this->cell(5.5,0,'');//Ciudad
	
	//Columna del medio
	
	$this->setXY(13,$this->margenY+4.5);
	$this->cell(2.2,0,$datosFactura['dq_factura']);//Factura Comercial (N°)
	
	$this->setXY(13.3,$this->margenY+5.7);
	$this->cell(2.2,0,$datosFactura['dg_comuna']);//Your Order Of - ahi le pusieron el pais jajaja
	
	$this->setXY(13,$this->margenY+6.9);
	$this->cell(2.2,0,$datosFactura['dg_medio_pago']);//Condiciones de venta
	
	$this->setXY(13,$this->margenY+8);
	$this->cell(2.2,0,$datosFactura['dg_comuna']);//Pais (Otros Paises -> Comuna)
	
	//Columna de la derecha
	
	$this->setXY(17.8,$this->margenY+4.5);
	$this->cell(2.5,0,$datosFactura['emision_factura']);//Fecha
	
	$this->setXY(17.8,$this->margenY+5.7);
	$this->cell(2.5,0,'');//Numero de Registro - RUT EXTRANJERO
	
	$this->setXY(17.8,$this->margenY+6.9);
	$this->cell(2.5,0,$datosFactura['dg_contacto_entrega']);//Representante
	
	$this->setXY(17.8,$this->margenY+8);
	$this->cell(2.5,0,$datosFactura['dg_medio_pago']);//Pago
	
}

function Footer(){
	global $datosFactura;
	
	$this->SetFont('Arial','',12);
	$this->setXY(17.3,$this->margenY+22.5);
	$this->cell(2.5,0.476,moneda_local($datosFactura['dq_total'],2),0,0,'R');//TOTAL
	
}

function AddDetalle($detalle){
	global $datosFactura,$empresa_conf;
	
	$this->AddPage();
	$this->SetFont('Arial','',7);
	$alto = 0.476;
	
	$this->SetY($this->margenY+11);
	foreach($detalle as $i => $det){
		$det['dq_precio'] = moneda_local($det['dq_precio'],2);
		$det['dq_total'] = moneda_local($det['dq_total'], 2);
		
		$this->SetX(1.5);
		$this->SetFont('Arial','',8);
		$this->Cell(2.1,$alto,substr($det['dg_producto'],0,18),0,0,'C');
		
		$this->SetX(3.6);
		$this->SetFont('Arial','',9);
		$this->Cell(2.1,$alto,$det['dc_cantidad'],0,0,'C');
		
		$this->SetX(5.8);
		$this->Cell(8.3,$alto,substr($det['dg_descripcion'],0,55));
		
		$this->SetX(14.5);
		$this->SetFont('Arial','',12);
		$this->Cell(2.6,$alto,$det['dq_precio'],0,0,'R');
		
		$this->SetX(17.3);
		$this->Cell(2.6,$alto,$det['dq_total'],0,0,'R');
		
		$this->SetFont('Arial','',9);
		$this->Ln();
		
	}
	
	$this->Ln(2);
	$this->SetX(4);
	$this->MultiCell(15,0.5,$datosFactura['dg_comentario']);
	
	/*$y = $this->GetY();
	if($datosOrden['dg_observacion']){
		$this->SetFont('Arial','',7);
		$this->MultiCell(116,3,"Observaciones: \n".$datosOrden['dg_observacion'],1);
	}*/
	
}

}
?>