<?php
require("../../../inc/fpdf.php");
require_once("Numbers/Words.php");

class PDF extends FPDF{

private $datosEmpresa;
private $margenY = 1.0;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','cm',array(21.5,28.2));
	
}

function Header(){
	global $datosFactura;
	
	//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
	//$this->SetXY(x,y);
	//MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
	
	$this->SetFont('Arial','',12);
	$this->SetTextColor(0,0,0);
	
	$this->SetXY(12.5,$this->margenY);//4.2);
	$this->Cell(8,0.5,$datosFactura['dq_factura'],0,0,'C');
	
	$this->SetFont('Arial','',11);
	$this->SetXY(3,$this->margenY+1.7);
	$datosFactura['dg_giro'] = substr($datosFactura['dg_giro'],0,30);
	$this->MultiCell(10,0.44,
	"{$datosFactura['dg_razon']}\n{$datosFactura['dg_direccion']}\n{$datosFactura['dg_region']}\n{$datosFactura['dg_rut']}\n{$datosFactura['dg_giro']}\n".
	"\n{$datosFactura['dg_direccion_entrega']} {$datosFactura['dg_comuna_entrega']} {$datosFactura['dg_region_entrega']}\n{$datosFactura['dg_contacto_entrega']}");
	
	$this->setXY(9,3+$this->margenY);
	$this->MultiCell(4,0.44,"{$datosFactura['dg_comuna']}\n{$datosFactura['dg_fono']}");
	
	$this->SetXY(1.3,6.6+$this->margenY);
	$this->Cell(3.8,0.5,$datosFactura['dg_medio_pago'],0,0,'C');
	
	$this->SetXY(4.7,6.6+$this->margenY);
	$this->Cell(3,0.5,$datosFactura['dg_tipo_cargo'],0,0,'C');
	
	$this->SetXY(7.7,6.6+$this->margenY);
	$this->Cell(5.2,0.5,$datosFactura['dg_tipo_operacion'],0,0,'C');
	
	$this->SetXY(16.5,3.8+$this->margenY);
	$this->SetFont('Arial','',10);
	$datosFactura['dg_ejecutivo'] = substr($datosFactura['dg_ejecutivo'],0,20);
	$this->MultiCell(4,0.5,"{$datosFactura['dg_ejecutivo']}\n{$datosFactura['df_emision']}\n{$datosFactura['df_vencimiento']}");
	
	$this->SetXY(16,6.1+$this->margenY);
	$this->MultiCell(4.5,0.5,"{$datosFactura['dg_orden_compra']}\n{$datosFactura['dq_nota_venta']}{$datosFactura['dq_orden_servicio']}");
	
	$this->SetFont('Arial','',9);
	
}

function Footer(){
	global $datosFactura;
	
	$nw = new Numbers_Words();
	$this->SetXY(1.7,18.7+$this->margenY);
	$this->Cell(14.5,0.6,ucfirst($nw->toWords($datosFactura['dq_total'],"es")));
	
	$this->SetFont('Arial','',12);
	$this->SetXY(18.3,18.7+$this->margenY);
	$this->Cell(2.5,0.83,moneda_local($datosFactura['dq_neto']),0,2,'R');
	$this->Cell(2.5,0.83,moneda_local($datosFactura['dq_iva']),0,2,'R');
	$this->Cell(2.5,0.83,moneda_local($datosFactura['dq_total']),0,2,'R');
}

function AddDetalle($detalle){
	global $datosFactura,$empresa_conf;
	
	$this->AddPage();
	$this->SetFillColor(255,255,255);
	
	$this->SetY(8.4+$this->margenY);
	foreach($detalle as $i => $det){
		$det['dq_precio'] = moneda_local($det['dq_precio']);
		$det['dq_descuento'] = moneda_local($det['dq_descuento']);
		$det['dq_total'] = moneda_local($det['dq_total']);
		
		$this->SetFont('Arial','',8);
		
		$y = $this->GetY();
		$x = 0.5;
		$this->SetX($x);
		$this->MultiCell(2.5,0.476,$det['dg_producto']);
		$x += 2.7;
		$this->SetXY($x,$y);
		$this->MultiCell(1,0.476,$det['dc_cantidad']);
		$x += 1.2;
		$this->SetXY($x,$y);
		$this->MultiCell(7.2,0.476,$det['dg_descripcion']);
		$y1 = $this->GetY();
		$x += 7.4;
		$this->SetXY($x,$y);
		$this->MultiCell(2.5,0.476,$det['dg_serie']);
		$x += 2.7;
		$this->SetXY($x,$y);
		$this->SetFont('Arial','',12);
		$this->MultiCell(2.4,0.476,$det['dq_precio']);
		$x += 2.6;
		$this->SetXY($x,$y);
		$this->MultiCell(1.1,0.476,$det['dq_descuento']);
		$x += 1.3;
		$this->SetXY($x,$y);
		$this->MultiCell(3,0.476,$det['dq_total']);
		$this->SetY($y1);
		
		/*
		$this->SetX(1);
		$this->Cell(2.5,0.476,$det['dg_producto']);
		$this->Cell(1,0.476,$det['dc_cantidad']);
		$this->Cell(7.2,0.476,substr($det['dg_descripcion'],0,70));
		$this->Cell(3.3,0.476,$det['dg_serie']);
		
		$this->SetFont('Arial','',12);
		$this->Cell(2.4,0.476,$det['dq_precio'],0,0,'R');
		$this->Cell(1.1,0.476,$det['dq_descuento'],0,0,'R');
		$this->Cell(2.4,0.476,$det['dq_total'],0,0,'R');
		
		$this->SetFont('Arial','',9);
		$this->Ln();*/
		
	}
	
	$this->Ln(1);
	$this->SetX(4);
	$this->SetFont('','',8);
	$this->MultiCell(15,0.3,$datosFactura['dg_comentario']);
	
	/*$y = $this->GetY();
	if($datosOrden['dg_observacion']){
		$this->SetFont('Arial','',7);
		$this->MultiCell(116,3,"Observaciones: \n".$datosOrden['dg_observacion'],1);
	}*/
	
}

}
?>