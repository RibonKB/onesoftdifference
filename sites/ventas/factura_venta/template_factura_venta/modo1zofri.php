<?php
require("../../../inc/fpdf.php");
require_once("Numbers/Words.php");

class PDF extends FPDF{

private $datosEmpresa;
private $margenY = 0.2;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','cm',array(21.8,27.3));
	
}

function Header(){
	global $datosFactura;
	
	//Cell(float w [, float h [, string txt [, mixed border [, int ln [, string align [, boolean fill [, mixed link]]]]]]])
	//$this->SetXY(x,y);
	//MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
	
	$this->SetFont('Arial','',12);
	$this->SetTextColor(0,0,0);
	
	$this->SetXY(16.0,$this->margenY+1);//4.2);
	$this->Cell(8.0,0.5,$datosFactura['dq_factura']);
	
	//Datos de cabecera (informaci�n)
	
	$this->SetFont('Arial','',9);
	//Se�ores: razon, direccion, comuna, telefono
	$this->setXY(3.0,$this->margenY+2.8);
	$this->MultiCell(9.0,0.5,
		"{$datosFactura['dg_razon']}\n{$datosFactura['dg_direccion']}\n{$datosFactura['dg_region']}");
	
	//$this->cell(9,0,$datosFactura['dg_razon']);
	
	$this->setXY(3.0,$this->margenY+5.1);
	$this->cell(3.2,0.5,$datosFactura['dg_rut']);
	
	$this->setXY(8.0,$this->margenY+5.1);
	$this->cell(4.0,0.5,$datosFactura['dg_orden_compra']);
	
	//preparar el string de guias de despacho
	$folios = '';
	$fechaPrimerDespacho = isset($datosFactura['guias_despacho'][0]['df_emision']) ? $datosFactura['guias_despacho'][0]['df_emision']:'';
	foreach($datosFactura['guias_despacho'] as $v){
		$folios .= ',' . $v['dq_folio'];
	}
	$folios = trim($folios,',');
	
	//Columna de datos
	$this->SetXY(1.3,$this->margenY+6.7);
	$this->cell(2.7,0.5,$datosFactura['emision_factura']);//Fecha Emision
	
	$this->setX(4.2);
	$this->cell(2.4,0.5, $folios);//Guia Despacho
	
	$this->setX(6.8);
	$this->cell(3.0,0.5,$fechaPrimerDespacho);//Fecha Despacho
	
	$this->setX(10.0);
	$this->cell(2.2,0.5,$datosFactura['dg_ejecutivo']);//Vendedor
	
	$this->setX(12.5);
	$this->cell(8.0,0.5,$datosFactura['dg_medio_pago'],0,0,'C');//Forma de Pago
	
	
}

function Footer(){
	global $datosFactura;
	
	$this->SetFont('Arial','',12);
	$this->setXY(17.6,$this->margenY+22.6);
	$this->cell(3.2,0.5,moneda_local($datosFactura['dq_total']),0,0,'R');//TOTAL
	
}

function AddDetalle($detalle){
	global $datosFactura,$empresa_conf;
	
	$this->AddPage();
	$this->SetFont('Arial','',7);
	$alto = 0.5;
	
	$this->SetY($this->margenY+9.0);
	foreach($detalle as $i => $det){
		$det['dq_precio'] = moneda_local($det['dq_precio']);
		$det['dq_total'] = moneda_local($det['dq_total']);
		//Codigo
		$this->SetX(1.4);
		$this->SetFont('Arial','',8);
		$this->Cell(3.0,$alto,substr($det['dg_producto'],0,18),0,0,'C');
		//Descripcion
		$this->SetX(4.4);
		$this->Cell(9.2,$alto,substr($det['dg_descripcion'],0,55));
		//Cantidad
		$this->SetX(13.6);
		$this->SetFont('Arial','',9);
		$this->Cell(2.0,$alto,$det['dc_cantidad'],0,0,'C');
		
		
		$this->SetX(16.0);
		$this->SetFont('Arial','',12);
		$this->Cell(1.7,$alto,$det['dq_precio'],0,0,'R');
		
		$this->SetX(17.7);
		$this->Cell(3.5,$alto,$det['dq_total'],0,0,'R');
		
		$this->SetFont('Arial','',9);
		$this->Ln();
		
	}
	
	$this->Ln(2);
	$this->SetX(1.4);
	$this->MultiCell(15.0,0.5,$datosFactura['dg_comentario']);
	
	/*$y = $this->GetY();
	if($datosOrden['dg_observacion']){
		$this->SetFont('Arial','',7);
		$this->MultiCell(116,3,"Observaciones: \n".$datosOrden['dg_observacion'],1);
	}*/
	
}

}
?>