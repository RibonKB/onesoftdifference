<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_factura_venta_detalle','1',"dm_tipo='1' AND dc_factura={$_POST['id']}");

if(count($data)):
?>
<div class="secc_bar">Impresión de factura</div><div class="panes">
<form action="#">
<fieldset>
<div class="info">Esta factura está disponible en 2 modos de impresión, indique cual desea visualizar</div>
<br />
<div class="center">
<button type="button" id="detail_version" class="button">Versión detalle de facturación</button> 
<button type="button" id="glosa_version" class="button">Versión glosa de facturación</button> 
<br />
</div>
</fieldset>
</form>
</div>
<script type="text/javascript">
$('#detail_version').click(function(){
	window.open("sites/ventas/factura_venta/ver_factura_venta.php?id=<?=$_POST['id'] ?>&v=0",'print_factura_venta','width=800;height=600');
});
$('#glosa_version').click(function(){
	window.open("sites/ventas/factura_venta/ver_factura_venta.php?id=<?=$_POST['id'] ?>&v=1",'print_factura_venta','width=800;height=600');
});
</script>
<?php
else:
?>
<script type="text/javascript">
$('#genOverlay').remove();
window.open("sites/ventas/factura_venta/ver_factura_venta.php?id=<?=$_POST['id'] ?>&v=0",'print_factura_venta','width=800;height=600');
</script>
<?php
endif;
?>