<?php

class FacturaVentaFactory extends Factory {
	
	protected $title = 'Factura de venta';
	
	public function getViewEditOrdenCompraDataAction(){
		
		$factura = $this->getFactura();
		$factura->df_fecha_orden_compra = $this->getConnection()->dateLocalFormat($factura->df_fecha_orden_compra);
		
		$form =  $this->getFormView($this->getTemplateUrl('edit_orden_compra'), array(
				'idFactura' => $factura->dc_factura,
				'ordenCompra' => $factura->dg_orden_compra,
				'fechaOrdenCompra' => $factura->df_fecha_orden_compra,
				'cliente' => $factura->dc_cliente,
			));
		echo $this->getOverlayView($form, array(),Factory::STRING_TEMPLATE);
	}
	
	public function setEditOrdenCompraDataAction() {
		
		$factura = $this->getFactura();
		$this->validateFacturaCliente($factura);
		$dataOrdenCompra = $this->validateDataOrdenCompra();
		$dataOrdenCompra->dc_factura = $factura->dc_factura;
		
		$this->updateOrdenCompraData($dataOrdenCompra);
		
		$this->getErrorMan()->showConfirm('Se han modificado los datos de la Orden de Compra Exitosamente.');
		
	}
	
	private function getFactura(){
		
		$r = $this->getRequest();
		$idFactura = $r->dc_factura;
		
		$db = $this->getConnection();
		$factura = $db->getRowById('tb_factura_venta', $idFactura, 'dc_factura');
		
		if(empty($factura)){
			$this->getErrorMan()->showWarning('La factura seleccionada no es válida. Contacte al administrador.');
			exit;
		}
		if($factura->dm_nula){
			$this->getErrorMan()->showWarning('La factura seleccionada esta anulada. Verifique su información e intente nuevamente.');
			exit;
		}
		
		return $factura;
		
	}
	
	private function validateFacturaCliente($factura){
		$r = $this->getRequest();
		if($factura->dc_cliente != $r->dc_cliente){
			$this->getErrorMan()->showWarning('Error de validación de factura. Contacte al administrador.');
			exit;
		}
	}
	
	// Valida los datos de la orden de compra. Devuelve un objeto con los datos ingresados.
	private function validateDataOrdenCompra(){
		$r = $this->getRequest();
		$data = new stdClass();
		if(empty($r->dg_orden_compra)){
		
			$data->dg_orden_compra = '';
			$data->df_fecha_orden_compra = null;
		
		} else {
			
			if(empty($r->df_fecha_orden_compra)){
				$this->getErrorMan()->showWarning('Es obligatorio ingresar la fecha de la orden de compra.');
				exit;
			}
			
			$data->dg_orden_compra = $r->dg_orden_compra;
			$data->df_fecha_orden_compra = $r->df_fecha_orden_compra;
		}
		
		return $data;
		
	}
	
	//Actualizar la orden de compra y la fecha de esta en la cabecera de la factura.
	private function updateOrdenCompraData($data){
		$db = $this->getConnection();
		$db->start_transaction();
		if(isset($data->df_fecha_orden_compra)){
			$data->df_fecha_orden_compra = $db->sqlDate2($data->df_fecha_orden_compra);
			$data->typeFecha = PDO::PARAM_STR;
		} else {
			$data->typeFecha = PDO::PARAM_NULL;
		}
		
		$st = $db->prepare($db->update('tb_factura_venta', array(
				'dg_orden_compra' => ':ordenCompra',
				'df_fecha_orden_compra' => ':fechaOrdenCompra',
			), 'dc_factura = :factura'));
		$st->bindValue(':ordenCompra',$data->dg_orden_compra,PDO::PARAM_STR);
		$st->bindValue(':fechaOrdenCompra', $data->df_fecha_orden_compra, $data->typeFecha);
		$st->bindValue(':factura',$data->dc_factura,PDO::PARAM_STR);
		
		$db->stExec($st);
		
		$db->commit();
	}
	
}