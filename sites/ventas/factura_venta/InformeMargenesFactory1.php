<?php

/**
 * Description of InformeMargenesFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 29-08-2013
 */
class InformeMargenesFactory extends Factory {

    protected $title = "Informe de Margenes";
    private $tipo_cambio = null;
    private $dc_mes;
    private $dc_anho;
    private $ejecutivos_join = 'LEFT JOIN';
    private $queryParams = array();
    private $comprasUsadas = array();
    private $existenciasUsadas = array();
	private $productosLibres = array();
	private $tipoDetalle = array();
    private $facturasResultado = array();
    private $campos_agrupacion = array(
        'dg_tipo_costo' => 'dg_glosa_tipo_costo',
        'dc_ejecutivo' => 'dg_ejecutivo',
        'dc_producto' => 'dg_detalle',
        'dc_proveedor' => 'dg_proveedor',
        'dc_marca' => 'dg_marca',
        'dc_linea_negocio' => 'dg_linea_negocio',
        'dc_cliente' => 'dg_cliente'
    );
    private $nombres_campo = array(
        'dg_tipo_costo' => 'Tipo de Costo',
        'dc_ejecutivo' => 'Ejecutivo',
        'dc_producto' => 'Producto',
        'dc_proveedor' => 'Proveedor',
        'dc_marca' => 'Marca',
        'dc_linea_negocio' => 'Linea de Negocio',
        'dc_cliente' => 'Cliente'
    );
  
    private $productos = array();
	
	private $stDBMarginFV = null;
	private $DBMarginFVdq_margen = null;
	private $DBMarginFVdc_factura = null;

    public function indexAction() {
        $form = $this->getFormView($this->getTemplateURL('index.form'), array(
            'meses' => $this->getListadoMeses(),
            'empresa' => $this->getEmpresa()
        ));

        echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
    }

    public function obtenerTotalMargenesAction() {
		//self::debug(self::getRequest());
        $this->initPeriodo();
        $data = $this->getTotalesData();
        $this->initFunctionsService();
        $ajustados = $this->ajustarTotales($data);

        $extradata = $this->ajustarTotales($this->getNotasCreditoAdicionales());
		
		if($this->checkFromService($ajustados)){
			echo "DONE!";
			$f = fopen("/var/www/html/logs/actualizaAporteFV.txt",'w');
			fwrite($f,"DONE");
			fclose($f);
			exit;
		}

        echo $this->getView($this->getTemplateURL('resultados'), array(
            'detalles' => $ajustados,
            'detallesPie' => $this->getDetallesPie($ajustados),
            'dataTotalUrl' => $this->getUrlDetallesTotal(),
            'tipo_cambio' => $this->getTipoCambio(),
            'extraData' => $extradata,
            'extraDataPie' => $this->getDetallesPie($extradata)
        ));
    }

    public function obtenerDetalleDocumentoAction() {
        $data = $this->getDetalleData();
        $this->initFunctionsService();
        $ajustados = $this->ajustarDetalles($data);

        echo $this->getView($this->getTemplateURL('detalles'), array(
            'detalles' => $ajustados,
            'detallesPie' => $this->getDetallesPieDetalle($ajustados),
            'tipo_cambio' => $this->getTipoCambio()
        ));
    }

    public function getDetalleAgrupadoAction() {
        $data = $this->getDetalleData();
        $this->initFunctionsService();
        $ajustados = $this->ajustarDetalles($data);

        $agrupados = $this->agruparDetalles($ajustados);

        echo $this->getView($this->getTemplateURL('agrupados'), array(
            'detalles' => $agrupados,
            'detallesPie' => $this->getDetallesPieDetalle($ajustados),
            'tipo_cambio' => $this->getTipoCambio(),
            'nombre_campo' => $this->nombres_campo[self::getRequest()->campo]
        ));
    }

    private function getUrlDetallesTotal() {
        $r = self::getRequest();
        $validaData = array(
            'dc_mes' => $r->dc_mes,
            'dc_anho' => $r->dc_anho,
            'dc_tipo_cambio' => $r->dc_tipo_cambio
        );

        if (isset($r->dc_ejecutivo)):
            $validaData['dc_ejecutivo'] = $r->dc_ejecutivo;
        endif;

        return http_build_query($validaData);
    }

    private function getDetallesPie($data) {
        $totales = (object) array(
                    'dq_venta_total' => 0,
                    'dq_costo_total' => 0,
                    'dq_margen_total' => 0,
                    'dq_margen_porcentual' => 0
        );

        foreach ($data as $d):
            $totales->dq_venta_total += $d->dq_venta_total;
            $totales->dq_costo_total += $d->dq_costo_total;
        endforeach;

        $totales->dq_margen_total = $totales->dq_venta_total - $totales->dq_costo_total;

        if ($totales->dq_venta_total != 0):
      $totales->dq_margen_porcentual = round($totales->dq_margen_total * 100 / $totales->dq_venta_total, 2);
        else:
            $totales->dq_margen_porcentual = -100;
        endif;

        return $totales;
    }

    private function getDetallesPieDetalle($data) {
        $totales = (object) array(
                    'dq_venta_total' => 0,
                    'dq_costo_total' => 0,
                    'dq_margen_total' => 0,
                    'dq_margen_porcentual' => 0
        );

        foreach ($data as $detalle):
            foreach ($detalle[1] as $d):
                $totales->dq_venta_total += $d->dq_venta_total;
                $totales->dq_costo_total += $d->dq_costo_total;
            endforeach;

        endforeach;

        $totales->dq_margen_total = $totales->dq_venta_total - $totales->dq_costo_total;

        if ($totales->dq_venta_total != 0):
            $totales->dq_margen_porcentual = round($totales->dq_margen_total * 100 / $totales->dq_venta_total, 2);
        else:
            $totales->dq_margen_porcentual = -100;
        endif;

        return $totales;
    }

    private function getDetalleData() {
        $r = self::getRequest();
        $db = $this->getConnection();

        if ($r->tipo == 'FV'):
            $cond = 'doc.dc_factura = ?';
            $query = $this->getFacturaQuery($cond);
        elseif ($r->tipo == 'NC'):
            $cond = 'doc.dc_nota_credito = ?';
            $query = $this->getNotaCreditoQuery($cond);
        else:
            $this->initPeriodo();
            return $this->getTotalesData();
        endif;

        $data = $db->prepare($query);
        $data->bindValue(1, $r->dc_documento, PDO::PARAM_INT);
        $db->stExec($data);

        return $this->getFilteredTotalDetail($data->fetchAll(PDO::FETCH_OBJ));
    }

    private function ajustarTotales($data) {
        $sum = array();
        $db = $this->getConnection();

        foreach ($data as $d):
            $lukas_totales = $this->getTotalizados($d[1]);
            $d[0]->dq_venta_total = $lukas_totales[0];
            $d[0]->dq_costo_total = $lukas_totales[1];
            $d[0]->dq_margen_total = $lukas_totales[2];
            $d[0]->dq_margen_porcentual_total = $lukas_totales[3];

            if ($d[0]->dc_nota_venta != 0) {
                $d[0]->dq_nota_venta = $db->getRowById('tb_nota_venta', $d[0]->dc_nota_venta, 'dc_nota_venta')->dq_nota_venta;
            }

            if ($d[0]->dc_orden_servicio != 0) {
                $d[0]->dq_orden_servicio = $db->getRowById('tb_orden_servicio', $d[0]->dc_orden_servicio, 'dc_orden_servicio')->dq_orden_servicio;
            }

            $sum[] = $d[0];
        endforeach;

        return $sum;
    }

    private function ajustarDetalles(&$data) {
        $db = $this->getConnection();

        foreach ($data as &$d):

            foreach ($d[1] as &$detalle):
                if ($detalle->dc_orden_compra):
                    $orden_compra = $db->getRowById('tb_orden_compra', $detalle->dc_orden_compra, 'dc_orden_compra');
                    $detalle->dq_orden_compra = $orden_compra->dq_orden_compra;
                    $proveedor = $this->getConnection()->getRowById('tb_proveedor', $orden_compra->dc_proveedor, 'dc_proveedor');
                    $tipo_proveedor = $this->getConnection()->getRowById('tb_tipo_proveedor', $proveedor->dc_tipo_proveedor, 'dc_tipo_proveedor');

                    $detalle->dc_proveedor = $orden_compra->dc_proveedor;
                    $detalle->dg_proveedor = $proveedor->dg_razon;

                    if ($tipo_proveedor->dm_incluido_libro_compra == 0):
                        $detalle->dg_tipo_costo = 'PI';
                        $detalle->dg_glosa_tipo_costo = 'Proveedor Internacional';
                    endif;
                endif;

                if ($detalle->dc_factura_compra):
                    $detalle->dq_factura_compra = $db->getRowById('tb_factura_compra', $detalle->dc_factura_compra, 'dc_factura')->dq_folio;
                endif;

                $detalle->dc_ejecutivo = $d[0]->dc_ejecutivo;
                $detalle->dg_ejecutivo = $d[0]->dg_nombre_ejecutivo . ' ' . $d[0]->dg_ap_ejecutivo . ' ' . $d[0]->dg_am_ejecutivo;

                $detalle->dc_cliente = $d[0]->dc_cliente;
                $detalle->dg_cliente = $d[0]->dg_cliente;

                $producto = $this->getProducto($detalle->dg_codigo);

                if ($producto !== false):
                    $detalle->dc_producto = $producto->dc_producto;
                    $detalle->dc_marca = $producto->dc_marca;
                    $detalle->dg_marca = $producto->dg_marca;
                    $detalle->dc_linea_negocio = $producto->dc_linea_negocio;
                    $detalle->dg_linea_negocio = $producto->dg_linea_negocio;
                endif;


                $detalle->dq_venta_total = $detalle->dq_venta_unitario * $detalle->dq_cantidad;
                $detalle->dq_costo_total = $detalle->dq_costo_unitario * $detalle->dq_cantidad;

                $detalle->dq_margen = $detalle->dq_venta_total - $detalle->dq_costo_total;

                if ($detalle->dq_venta_total != 0):
                    $detalle->dq_margen_p = round($detalle->dq_margen * 100 / $detalle->dq_venta_total, 2);
                else:
                    $detalle->dq_margen_p = -100;
                endif;

            endforeach;
        endforeach;

        return $data;
    }

    private function agruparDetalles($data) {
        $dc_campo = self::getRequest()->campo;
        $dg_contenido = $this->campos_agrupacion[$dc_campo];

        $ret = array();
        foreach ($data as $d):
            foreach ($d[1] as $detalle):

                if (!isset($ret[$detalle->$dc_campo])):
                    $ret[$detalle->$dc_campo] = (object) array(
                                'dg_glosa' => $detalle->$dg_contenido,
                                'dq_venta_total' => 0,
                                'dq_costo_total' => 0,
                                'dq_margen' => 0,
                                'dq_margen_porcentual' => 0
                    );
                endif;

                $ret[$detalle->$dc_campo]->dq_venta_total += $detalle->dq_venta_unitario;
                $ret[$detalle->$dc_campo]->dq_costo_total += $detalle->dq_costo_unitario;

            endforeach;
        endforeach;

        return $this->calculaMargenAgrupado($ret);
    }

  private function calculaMargenAgrupado($data){
    foreach($data as &$d):
      $d->dq_margen = $d->dq_venta_total-$d->dq_costo_total;
      if($d->dq_venta_total != 0):
        $d->dq_margen_porcentual = round($d->dq_margen*100/$d->dq_venta_total,2);
      elseif($d->dq_costo_total == 0):
                $d->dq_margen_porcentual = 0;
            else:
                $d->dq_margen_porcentual = -100;
            endif;
        endforeach;

        return $data;
    }

  private function getProducto($dg_codigo){
    if(isset($this->productos[$dg_codigo])):
            return $this->productos[$dg_codigo];
        endif;

        $db = $this->getConnection();
        $producto = $db->prepare($db->select('tb_producto p
                      JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio
                      JOIN tb_marca m ON m.dc_marca = p.dc_marca',
            'p.dc_producto, ln.dc_linea_negocio, ln.dg_linea_negocio, m.dc_marca, m.dg_marca',
            'p.dg_codigo = ? AND p.dc_empresa = ?'));
        $producto->bindValue(1, $dg_codigo, PDO::PARAM_STR);
        $producto->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($producto);

        $this->productos[$dg_codigo] = $producto->fetch(PDO::FETCH_OBJ);

        return $this->productos[$dg_codigo];
    }

    private function getTotalizados($detalle) {
        //Obtiene venta y costo total
        $venta = 0;
        $costo = 0;
        foreach ($detalle as $d) {
            $venta += floatval($d->dq_venta_unitario * $d->dq_cantidad);
            $costo += floatval($d->dq_costo_unitario * $d->dq_cantidad);
        }

        $margen = $venta - $costo;

        if ($venta != 0)
      $margen_p = round($margen * 100 / $venta, 2);
        else
            $margen_p = -100;

        return array($venta, $costo, $margen, $margen_p);
    }

    private function getListadoMeses() {
        return array(
            1 => 'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre'
        );
    }

    private function getTipoCambio() {
        if (!($this->tipo_cambio instanceof stdClass)):
            $this->tipo_cambio = $this->getConnection()->getRowById('tb_tipo_cambio', self::getRequest()->dc_tipo_cambio, 'dc_tipo_cambio');

            if ($this->tipo_cambio === false):
                $this->getErrorMan()->showWarning('No se pudo obtener la información del tipo de cambio, compruebe con otro o consulte con un administrador');
                exit;
            endif;
        endif;

        return $this->tipo_cambio;
    }

    private function initPeriodo() {
        $r = self::getRequest();

        $this->dc_mes = intval($r->dc_mes);
        if ($this->dc_mes > 12 or $this->dc_mes < 1) {
            $this->getErrorMan()->showWarning('El mes indicado es inválido, no se puede continuar con la consulta');
            exit;
        }

        $this->dc_anho = intval($r->dc_anho);
        if ($this->dc_anho == 0 or $this->dc_anho < 2010) {
            $this->getErrorMan()->showWarning('El año indicado es inválido, no se puede continuar con la consulta');
            exit;
        }
    }

    private function getTotalesData() {
        $cond = $this->getCondiciones();
        $db = $this->getConnection();

        $factura = $this->getFacturaQuery($cond);
        $nota_credito = $this->getNotaCreditoQuery($cond);

        $data = $db->prepare('(' . $factura . ') UNION ALL (' . $nota_credito . ') ORDER BY df_emision');

        for ($i = 0; $i <= 1; $i++):
            foreach ($this->queryParams as $j => $p):
                $data->bindValue(($j + 1) + ($i * count($this->queryParams)), $p[0], $p[1]);
            endforeach;
        endfor;

        $db->stExec($data);

        return $this->getFilteredTotalDetail($data->fetchAll(PDO::FETCH_OBJ));
    }

    private function getFacturaQuery(&$cond) {
        return $this->getConnection()->select("tb_factura_venta doc
          {$this->ejecutivos_join} tb_funcionario ex ON ex.dc_funcionario = doc.dc_ejecutivo
          LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = doc.dc_nota_venta
          LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = doc.dc_orden_servicio
          LEFT JOIN tb_cliente cl ON cl.dc_cliente = doc.dc_cliente", '"FV" dg_tipo_doc, doc.dc_factura dc_documento, doc.dq_factura dq_documento, doc.dq_folio, doc.df_emision,
         ex.dg_nombres dg_nombre_ejecutivo, ex.dg_ap_paterno dg_ap_ejecutivo, ex.dg_ap_materno dg_am_ejecutivo, doc.dc_ejecutivo,
         nv.dc_nota_venta, nv.dq_nota_venta, nv.dc_tipo_nota_venta,
         os.dc_orden_servicio, os.dq_orden_servicio,
         cl.dc_cliente, cl.dg_razon dg_cliente', $cond, array('order_by' => 'doc.df_emision ASC'));
    }

    private function getNotaCreditoQuery($cond) {
        return $this->getConnection()->select("tb_nota_credito doc
          {$this->ejecutivos_join} tb_funcionario ex ON ex.dc_funcionario = doc.dc_ejecutivo
          LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = doc.dc_nota_venta
          LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = doc.dc_orden_servicio
          LEFT JOIN tb_cliente cl ON cl.dc_cliente = doc.dc_cliente", '"NC" dg_tipo_doc, doc.dc_nota_credito dc_documento, doc.dq_nota_credito dq_documento, doc.dq_folio, doc.df_emision,
         ex.dg_nombres dg_nombre_ejecutivo, ex.dg_ap_paterno dg_ap_ejecutivo, ex.dg_ap_materno dg_am_ejecutivo, doc.dc_ejecutivo,
         nv.dc_nota_venta, nv.dq_nota_venta, nv.dc_tipo_nota_venta,
         os.dc_orden_servicio, os.dq_orden_servicio,
         cl.dc_cliente, cl.dg_razon dg_cliente', $cond);
    }

    private function getCondiciones() {

        $this->queryParams[] = array($this->getEmpresa(), PDO::PARAM_INT);

        $condicion = "doc.dc_empresa = ? AND doc.dm_nula = 0";
        $condicion .= $this->getCondicionEjecutivos();
        $condicion .= $this->getCondicionPeriodo();

        return $condicion;
    }

    private function getCondicionEjecutivos() {
        $r = self::getRequest();
        if (!isset($r->dc_ejecutivo)):
            return '';
        endif;

        $this->ejecutivos_join = 'JOIN';
        $questions = substr(str_repeat(',?', count($r->dc_ejecutivo)), 1);

        foreach ($r->dc_ejecutivo as $e) {
            $this->queryParams[] = array($e, PDO::PARAM_INT);
        }

        return " AND ex.dc_funcionario IN ({$questions})";
    }

    private function getCondicionPeriodo() {
        $fecha = new DateTime();
        $fecha->setDate($this->dc_anho, $this->dc_mes, 1);

        $this->queryParams[] = array($this->getConnection()->sqlDate2($fecha->format('d/m/Y')), PDO::PARAM_STR);
        $this->queryParams[] = array($this->getConnection()->sqlDate2($fecha->format('t/m/Y 23:59')), PDO::PARAM_STR);

        return " AND (doc.df_emision BETWEEN ? AND ?)";
    }

    private function getFilteredTotalDetail($data) {
        $detalles = array();
        foreach ($data as $doc):
            if ($doc->dg_tipo_doc == 'FV') {
                $this->facturasResultado[] = $doc->dc_documento;
                $detalles[] = $this->descomponerFacturaVenta($doc);
            } else {
                $detalles[] = $this->descomponerNotaCredito($doc);
            }
        endforeach;

        return $detalles;
    }

    private function descomponerFacturaVenta(stdClass $fv) {
        if ($fv->dc_nota_venta != 0) {
            return array(
                $fv,
                $this->descomponerFacturaNotaVenta($fv)
            );
        } else if ($fv->dc_orden_servicio != 0) {
            return array(
                $fv,
                $this->descomponerFacturaOrdenServicio($fv)
            );
        } else {
            if ($this->esFacturaMultiple($fv)) {
                return array(
                    $fv,
                    $this->descomponeFacturaMultiple($fv)
                );
            } else {
                return array(
                    $fv,
                    $this->descomponeFacturaOtros($fv)
                );
            }
        }
    }

    private function esFacturaMultiple($fv) {
        return false;
    }

    private function descomponerFacturaNotaVenta($fv) {
        $db = $this->getConnection();
        $tipo_nota_venta = $db->getRowById('tb_tipo_nota_venta', $fv->dc_tipo_nota_venta, 'dc_tipo');

        if ($tipo_nota_venta === false or $tipo_nota_venta->dm_contratable == 1) {
            return $this->descomponeFacturaOtros($fv);
        } else if ($tipo_nota_venta->dm_contratable == 0) {
            return $this->descomponeFacturaVentaNormal($fv);
        }
    }

    private function descomponeFacturaVentaNormal($fv) {
        $detalle = $this->getDetallesFacturaVenta($fv->dc_documento);
        $arr = array();

        foreach ($detalle as $d):
	    $arr = array_merge($arr, $this->descomponeDetalleVentaNormal($d));
        endforeach;

        return $arr;
    }

    private function descomponeDetalleVentaNormal($dfv) {
        $comprados = $this->getDetallesCompraVentaNormal($dfv->dc_detalle_nota_venta);
        $arr = array();

    $tipo_producto = $this->getTipoDetalle($dfv->dc_detalle_nota_venta);

        foreach ($comprados as $c):
            $f = $this->getEmptyEstandarDetail();
            $f->dg_detalle = $dfv->dg_descripcion;
            $f->dg_codigo = $dfv->dg_producto;
            $cantidad = $this->descontarCantidadCompra($c, $dfv);
            $f->dq_cantidad = $cantidad;
            $f->dq_venta_unitario = floatval($dfv->dq_precio);
      $f->dq_costo_unitario = floatval($c->dq_precio);
      $f->dc_factura_compra = $c->dc_factura;
            $f->dq_guia_recepcion = $c->dq_guia_recepcion;
            $f->dc_orden_compra = $c->dc_orden_compra;
      if($tipo_producto == 2):
                $f->dg_tipo_costo = 'FC';
                $f->dg_glosa_tipo_costo = 'Costo Factura';
      else:
        $f->dg_tipo_costo = 'FCS';
        $f->dg_glosa_tipo_costo = 'Costo Factura Servicios';
      endif;

            $arr[] = $f;

            $dfv->dc_cantidad -= $cantidad;
            if ($dfv->dc_cantidad == 0):
                return $arr;
            endif;

        endforeach;

    if($tipo_producto == 2):
        //$nvd = $this->getConnection()->getRowById('tb_nota_venta_detalle', $dfv->dc_detalle_nota_venta, 'dc_nota_venta_detalle');
        $existencia = $this->getDetallesExistencia($dfv->dc_detalle_nota_venta, $comprados);
    
        foreach ($existencia as $e):
            $f = $this->getEmptyEstandarDetail();
            $f->dg_detalle = $dfv->dg_descripcion;
            $f->dg_codigo = $dfv->dg_producto;
            $cantidad = $this->descontarCantidadExistencia($e, $dfv);
            $f->dq_cantidad = $cantidad;
            $f->dq_venta_unitario = floatval($dfv->dq_precio);
            $f->dq_costo_unitario = floatval($e->dq_pmp);
      $f->dg_tipo_costo = $e->tipo;
      $f->dg_glosa_tipo_costo = $e->glosa;

      if($e->tipo == 'FCL'):
          $this->rebajarCompraLibreMes($e->dc_producto, $e->dc_guia_recepcion, $cantidad);
          $f->dq_guia_recepcion = $e->dq_guia_recepcion;
          $f->dc_factura_compra = $e->dc_factura;
          $f->dc_orden_compra = $e->dc_orden_compra;
      endif;

            $arr[] = $f;

            $dfv->dc_cantidad -= $cantidad;
      if($dfv->dc_cantidad == 0):
                return $arr;
            endif;

        endforeach;

    endif;

        $f = $this->getEmptyEstandarDetail();
        $f->dg_detalle = $dfv->dg_descripcion;
        $f->dg_codigo = $dfv->dg_producto;
        $f->dq_cantidad = $dfv->dc_cantidad;
        $f->dq_venta_unitario = floatval($dfv->dq_precio);
        $f->dq_costo_unitario = floatval($dfv->dq_costo);
    
    if($tipo_producto == 0):
        $f->dg_tipo_costo = '-';
        $f->dg_glosa_tipo_costo = 'Otros costos';
    else:
        $f->dg_tipo_costo = 'PR';
        $f->dg_glosa_tipo_costo = 'Provisión';
    endif;

        $arr[] = $f;

        return $arr;
    }

  private function getTipoDetalle($dc_detalle_nota_venta){
      
      $db = $this->getConnection();
      $nvd = $db->getRowById('tb_nota_venta_detalle', $dc_detalle_nota_venta, 'dc_nota_venta_detalle');
      if(isset($this->tipoDetalle[$nvd->dc_producto])):
          return $this->tipoDetalle[$nvd->dc_producto];
      endif;
      
      $query = $db->prepare($db->select('tb_producto p '
              . 'LEFT JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
              't.dm_orden_compra, t.dm_controla_inventario',
              'p.dc_producto = ?'));
      $query->bindValue(1, $nvd->dc_producto, PDO::PARAM_INT);
      $db->stExec($query);
      $query = $query->fetch(PDO::FETCH_OBJ);
      
      $this->tipoDetalle[$nvd->dc_producto] = intval($query->dm_orden_compra)+((intval($query->dm_controla_inventario)-1)*-1);
      
      return $this->tipoDetalle[$nvd->dc_producto];
      
  }

    private function descontarCantidadExistencia($e, $dfv) {
        $cantidad = $e->dc_cantidad;

        if (!isset($this->existenciasUsadas[$dfv->dc_detalle_nota_venta])):
            $this->existenciasUsadas[$dfv->dc_detalle_nota_venta] = 0;
        endif;

        if ($cantidad >= $dfv->dc_cantidad):
            $this->existenciasUsadas[$dfv->dc_detalle_nota_venta] += $dfv->dc_cantidad;
            return $dfv->dc_cantidad;
        else:
            $this->existenciasUsadas[$dfv->dc_detalle_nota_venta] += $cantidad;
            return $cantidad;
        endif;
    }

    private function getDetallesExistencia($dc_detalle_nota_venta, $comprados) {
        $nvd = $this->getConnection()->getRowById('tb_nota_venta_detalle', $dc_detalle_nota_venta, 'dc_nota_venta_detalle');

        $dq_comprado = 0;
        foreach ($comprados as $c):
            $dq_comprado += intval($c->dq_cantidad_original);
        endforeach;

        $data = (object) array(
                    'dq_pmp' => $nvd->dq_pmp_compra,
                'dc_cantidad' => $nvd->dc_recepcionada - $dq_comprado,
                'dc_factura' => NULL,
                'dc_guia_recepcion' => NULL,
                'tipo' => 'EX',
                'glosa' => 'Existencias'
        );

        if (isset($this->existenciasUsadas[$dc_detalle_nota_venta])):
            $data->dc_cantidad -= $this->existenciasUsadas[$dc_detalle_nota_venta];
        endif;

        if ($data->dc_cantidad <= 0):
            return array();
        endif;

    $libre = array();
    $comprasLibres = array();//$this->getComprasLibresMes($nvd->dc_producto);
    
    foreach($comprasLibres as $dc_guia_recepcion => $c):
        if($c->dc_cantidad < $data->dc_cantidad):
            $libre[] = (object)array(
                'dq_pmp' => $c->dq_costo,
                'dc_cantidad' => $c->dc_cantidad,
                'dc_factura' => $c->dc_factura,
                'dc_guia_recepcion' => $dc_guia_recepcion,
                'dq_guia_recepcion' => $c->dq_guia_recepcion,
                'dc_orden_compra' => $c->dc_orden_compra,
                'tipo' => 'FCL',
                'glosa' => 'Compra Libre',
                'dc_producto' => $nvd->dc_producto
            );
            $data->dc_cantidad -= $c->dc_cantidad;
            
        else:
            $libre[] = (object)array(
                'dq_pmp' => $c->dq_costo,
                'dc_cantidad' => $data->dc_cantidad,
                'dc_factura' => $c->dc_factura,
                'dc_guia_recepcion' => $dc_guia_recepcion,
                'dq_guia_recepcion' => $c->dq_guia_recepcion,
                'dc_orden_compra' => $c->dc_orden_compra,
                'tipo' => 'FCL',
                'glosa' => 'Compra Libre',
                'dc_producto' => $nvd->dc_producto
            );
            return $libre;
        endif;
    endforeach;

    $libre[] = $data;
    
    return $libre;
    }

  private function getComprasLibresMes($dc_producto){
      if(isset($this->productosLibres[$dc_producto])):
          return $this->productosLibres[$dc_producto];
      endif;
      
      $db = $this->getConnection();
      $query = $db->prepare($db->select('tb_guia_recepcion_detalle d'
              . ' JOIN tb_guia_recepcion gr ON gr.dc_guia_recepcion = d.dc_guia_recepcion',
              'd.dq_cantidad, d.dq_precio, gr.dc_factura, gr.dc_guia_recepcion, gr.dq_guia_recepcion, gr.dc_orden_compra',
              'gr.dc_nota_venta = 0 AND gr.dc_orden_servicio = 0 AND MONTH(gr.df_fecha_emision) = ? AND YEAR(gr.df_fecha_emision) = ? AND gr.dc_empresa = ?'));
      $query->bindValue(1, self::getRequest()->dc_mes, PDO::PARAM_INT);
      $query->bindValue(2, self::getRequest()->dc_anho, PDO::PARAM_INT);
      $query->bindValue(3, $this->getEmpresa(), PDO::PARAM_INT);
      $db->stExec($query);
      
      $this->productosLibres[$dc_producto] = array();
      
      while($d = $query->fetch(PDO::FETCH_OBJ)):
          $this->productosLibres[$dc_producto][$d->dc_guia_recepcion] = (object) array(
              'dc_cantidad' => $d->dq_cantidad,
              'dq_costo' => floatval($d->dq_precio),
              'dc_factura' => $d->dc_factura,
              'dq_guia_recepcion' => $d->dq_guia_recepcion,
              'dc_orden_compra' => $d->dc_orden_compra
          );
      endwhile;
      
      return $this->productosLibres[$dc_producto];
      
  }
  
  private function rebajarCompraLibreMes($dc_producto, $dc_guia_recepcion, $dc_cantidad){
      if($this->productosLibres[$dc_producto][$dc_guia_recepcion]->dc_cantidad == $dc_cantidad):
          unset($this->productosLibres[$dc_producto][$dc_guia_recepcion]);
      else:
          $this->productosLibres[$dc_producto][$dc_guia_recepcion]->dc_cantidad -= $dc_cantidad;
      endif;
  }

    private function getDetallesCompraVentaNormal($dc_detalle_nota_venta) {
        $db = $this->getConnection();

        $data = $db->prepare(
                $db->select(
                        'tb_guia_recepcion_detalle d
                         JOIN tb_guia_recepcion gr ON gr.dc_guia_recepcion = d.dc_guia_recepcion',
                    'd.*, gr.dc_orden_compra, gr.dc_factura, gr.dq_guia_recepcion, gr.dc_nota_venta, d.dq_cantidad dq_cantidad_original', 'd.dc_detalle_nota_venta = ? AND gr.dm_nula = 0'));
        $data->bindValue(1, $dc_detalle_nota_venta, PDO::PARAM_INT);
        $db->stExec($data);

        $detalles = array();
        while ($d = $data->fetch(PDO::FETCH_OBJ)):

            if (isset($this->comprasUsadas[$d->dc_detalle])):
                $d->dq_cantidad = intval($d->dq_cantidad) - $this->comprasUsadas[$d->dc_detalle];
            endif;

            if ($d->dq_cantidad > 0):
                $detalles[] = $d;
            endif;

        endwhile;

        return $detalles;
    }

    private function descontarCantidadCompra($c, $dfv) {
        $cantidad = $c->dq_cantidad;

        if (!isset($this->comprasUsadas[$c->dc_detalle])):
            $this->comprasUsadas[$c->dc_detalle] = 0;
        endif;

        if ($cantidad >= $dfv->dc_cantidad):
            $this->comprasUsadas[$c->dc_detalle] += $dfv->dc_cantidad;
            return $dfv->dc_cantidad;
        else:
            $this->comprasUsadas[$c->dc_detalle] += $cantidad;
            return $cantidad;
        endif;
    }

    private function descomponerFacturaOrdenServicio($fv) {
        return $this->descomponeFacturaOtros($fv);
    }

    private function descomponeFacturaMultiple($fv) {
        return array();
    }

    private function descomponeFacturaOtros($fv) {
        $detalle = $this->getDetallesFacturaVenta($fv->dc_documento);

        $arr = array();
        foreach ($detalle as $d):
            $f = $this->getEmptyEstandarDetail();
            $f->dg_detalle = $d->dg_descripcion;
            $f->dg_codigo = $d->dg_producto;
            $f->dq_cantidad = $d->dc_cantidad;
            $f->dq_venta_unitario = floatval($d->dq_precio);
            $f->dq_costo_unitario = floatval($d->dq_costo);

            $arr[] = $f;
        endforeach;

        return $arr;
    }

    private function getDetallesFacturaVenta($dc_factura) {
        $db = $this->getConnection();

        $detalle = $db->prepare($db->select('tb_factura_venta_detalle', '*', 'dc_factura = ? AND dm_tipo = 0'));
        $detalle->bindValue(1, $dc_factura, PDO::PARAM_INT);
        $db->stExec($detalle);

        return $detalle->fetchAll(PDO::FETCH_OBJ);
    }

    private function descomponerNotaCredito(stdClass $nc) {
        return array(
            $nc,
            $this->getDetalleNotaCredito($nc->dc_documento)
        );
    }

    private function getDetalleNotaCredito($dc_nota_credito) {
        $db = $this->getConnection();

        $detalle = $db->prepare($db->select('tb_nota_credito_detalle', '*', 'dc_nota_credito = ? AND dm_tipo = 0'));
        $detalle->bindValue(1, $dc_nota_credito, PDO::PARAM_INT);
        $db->stExec($detalle);

        $acc = array();

        while ($d = $detalle->fetch(PDO::FETCH_OBJ)):
            $f = $this->getEmptyEstandarDetail();
            $f->dg_detalle = $d->dg_descripcion;
            $f->dg_codigo = $d->dg_producto;
            $f->dq_cantidad = intval($d->dc_cantidad);
            $f->dq_venta_unitario = floatval($d->dq_precio) * -1;
            $f->dq_costo_unitario = floatval($d->dq_costo) * -1;

            $acc[] = $f;
        endwhile;

        return $acc;
    }

    private function getEmptyEstandarDetail() {
        return (object) array(
                    'dg_detalle' => NULL,
                    'dg_codigo' => NULL,
                    'dq_cantidad' => 0,
                    'dq_venta_unitario' => 0,
                    'dq_costo_unitario' => 0,
                    'dc_factura_compra' => 0,
                    'dq_guia_recepcion' => NULL,
                    'dc_orden_compra' => 0,
                    'dq_factura_compra' => NULL,
                    'dq_orden_compra' => NULL,
                    'dg_tipo_costo' => '-',
                    'dg_glosa_tipo_costo' => 'Otros Costos',
                    'dc_ejecutivo' => 0,
                    'dg_ejecutivo' => NULL,
                    'dc_producto' => 0,
                    'dc_proveedor' => 0,
                    'dg_proveedor' => NULL,
                    'dc_marca' => 0,
                    'dg_marca' => NULL,
                    'dc_linea_negocio' => 0,
                    'dg_linea_negocio' => NULL,
                    'dc_cliente' => 0,
                    'dg_cliente' => NULL
        );
    }

    //

    private function getNotasCreditoAdicionales() {

        if (!count($this->facturasResultado)) {
            return array();
        }

        $quests = substr(str_repeat(',?', count($this->facturasResultado)), 1);
        $cond = "doc.dm_nula = 0 AND doc.dc_factura IN ({$quests})";
        $query = $this->getNotaCreditoQuery($cond);

        $data = $this->getConnection()->prepare($query);

        foreach ($this->facturasResultado as $i => $dc_factura):
            $data->bindValue($i + 1, $dc_factura, PDO::PARAM_INT);
        endforeach;

        $this->getConnection()->stExec($data);

        $result = array();
        while ($nc = $data->fetch(PDO::FETCH_OBJ)):
            $result[] = $this->descomponerNotaCredito($nc);
        endwhile;

        return $result;
    }
	
	private function checkFromService($data){
		$r = self::getRequest();
		if(!isset($r->fromService)){
			return false;
		}
		
		$db = $this->getConnection();
		$this->stDBMarginFV = $db->prepare($db->update('tb_factura_venta',array(
			'dq_margen' => '?'
		),'dc_factura = ?'));
		$this->stDBMarginFV->bindParam(1,$this->DBMarginFVdq_margen,PDO::PARAM_STR);
		$this->stDBMarginFV->bindParam(2,$this->DBMarginFVdc_factura,PDO::PARAM_INT);
		
		foreach($data as $f):
			$this->setDBMargin($f);
		endforeach;
		
		return true;
		
	}
  
	private function setDBMargin($data){
		$db = $this->getConnection();
		
		if($data->dg_tipo_doc == 'FV'){
			$this->DBMarginFVdq_margen = $data->dq_margen_total;
			$this->DBMarginFVdc_factura = $data->dc_documento;
			$db->stExec($this->stDBMarginFV);
		}
		
	}

}
