<?php
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/cotizacion.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("tb_cotizacion",
"dq_cotizacion,dq_oportunidad,dc_termino_comercial,dc_contacto,dc_tipo_cambio,dq_cambio,dc_modo_envio,dc_cliente,df_fecha_envio,dq_iva,dq_neto,dg_observacion,dc_ejecutivo,dc_ejecutivo_tv",
"dc_cotizacion = '{$_POST['id']}' AND dc_empresa={$empresa}");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró la {$lang['cot_doc']} especificada, intentelo nuevamente.","sites/ventas/src_cotizacion.php");
}
$datos = $datos[0];

$datosCliente = $db->select('tb_cliente','dg_razon,dg_rut',"dc_cliente={$datos['dc_cliente']} AND dm_activo='1'");

if(!count($datosCliente)){
	$error_man->showWarning("No puede modificar la {$lang['cot_doc']} debido a que el cliente para quien fue emitida no existe");
	exit();
}
$datosCliente = $datosCliente[0];

/*$contactos = $db->select("(SELECT * FROM tb_domicilio_cliente WHERE dc_cliente = {$datos['dc_cliente']}) dc
LEFT JOIN tb_comuna co ON dc.dc_comuna = co.dc_comuna
JOIN (SELECT * FROM tb_domicilio_cliente_tipo_direccion WHERE dm_activo ='1') td ON dc.dc_domicilio = td.dc_domicilio
JOIN tb_contacto_cliente cc ON td.dc_contacto = cc.dc_contacto",
"DISTINCT cc.dc_contacto, dc.dg_direccion, co.dg_comuna, cc.dg_contacto",
'',array("order_by"=>"cc.dm_default","order_dir"=>"DESC"));*/

?>
<div class="title center">Editando <?=$lang['cot_doc'] ?> <strong><?=$datos['dq_cotizacion'] ?></strong></div>
<hr class="clear" />
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dg_tipo_cambio,dq_cambio,dn_cantidad_decimales","dc_empresa={$empresa} AND dm_activo='1'",array("order_by"=>"dg_tipo_cambio"));
	$cambio_list = array();
	foreach($list_seg as $c){
		$cambio_list[$c['dc_tipo_cambio']] = "{$c['dg_tipo_cambio']} |".moneda_local($c['dq_cambio'])."|";
		$form->Hidden("decimal{$c['dc_tipo_cambio']}",$c['dn_cantidad_decimales']);
	}
	
	$form->Header("
	<img src='images/ocultar.png' title='Mostrar/Ocultar datos de cabecera' alt='' id='toggle_header'  class='right'/>
	cliente: <strong>{$datosCliente['dg_razon']}</strong>({$datosCliente['dg_rut']})","title='Indique los datos para la generación de la {$lang['cot_doc']} Los datos marcados con [*] son obligatorios'
	");
	$form->Start("sites/ventas/proc/editar_cotizacion.php","ed_cotizacion",'ventas_form');
	echo("<div id='cot_header_data' class='hidden'>");
	$form->Section();
	$form->Listado('Contacto','cot_contacto',"(SELECT * FROM tb_cliente_sucursal WHERE dc_cliente = {$datos['dc_cliente']} AND dm_activo='1') s
	JOIN tb_contacto_cliente c ON s.dc_sucursal = c.dc_sucursal AND c.dm_activo = '1'
	LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",
	array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),1,$datos['dc_contacto'],'');
	
	$form->Select("Tipo de cambio","prod_tipo_cambio",$cambio_list,1,$datos['dc_tipo_cambio']);
	$form->Text('Cambio','cot_cambio',1,255,$datos['dq_cambio']);
	$form->Listado("Términos comerciales","cot_termino","tb_termino_comercial",array("dc_termino_comercial","dg_termino_comercial"),0,$datos['dc_termino_comercial']);
	$form->EndSection();
	
	$form->Section();
	$form->Date("Fecha envio","cot_envio",0,$datos['df_fecha_envio']);
	$form->Listado("Modo de envio","cot_modo_envio","tb_modo_envio",array("dc_modo_envio","dg_modo_envio"),0,$datos['dc_modo_envio']);
	$form->Listado('Ejecutivo','cot_executive','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'),1,$datos['dc_ejecutivo']);
	$form->Listado('Ejecutivo televenta','cot_televenta','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'),0,$datos['dc_ejecutivo_tv']);
	$form->EndSection();
	$form->Section();
	$form->Textarea('Observaciones','cot_observacion',0,$datos['dg_observacion']);
	$form->Text('Nº Oportunidad','cot_oportunidad',0,255,$datos['dq_oportunidad']);
	$form->EndSection();
	
	echo("<hr class='clear' />");
	
	$c_extra = $db->select(
	"tb_cotizacion_contacto_extra",
	"dg_etiqueta,dc_tipo_direccion,dc_contacto_extra",
	"dc_empresa = {$empresa} AND dm_activo = '1'"
	);
	foreach($c_extra as $ex){
		$form->Section();
		$form->Hidden('tipo_cextra[]',$ex['dc_contacto_extra']);
		$form->Listado($ex['dg_etiqueta'],'contacto_extra[]',"(SELECT * FROM tb_cliente_sucursal_tipo WHERE dc_tipo_direccion = {$ex['dc_tipo_direccion']}) st
		JOIN tb_cliente_sucursal s ON s.dc_sucursal = st.dc_sucursal
		JOIN tb_contacto_cliente c ON c.dc_sucursal = s.dc_sucursal
		LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto",array('c.dc_contacto','c.dg_contacto','s.dg_sucursal','cc.dg_cargo_contacto'),0,0,'');
		$form->EndSection();
	}
	echo("<br class='clear' /><br /></div>");
	$form->Header("Debe seleccionar un <strong>tipo de cambio</strong> antes de poder agregar productos a la {$lang['cot_doc']}",'id="prod_info"  style="display:none;"');
	echo("<div id='prods'>
	<div class='info'>Detalle</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='60'>Opciones</th>
		<th width='95'>Código (Autocompleta)</th>
		<th>Descripción</th>
		<th width='158'>Proveedor</th>
		<th width='63'>Cantidad</th>
		<th width='85'>Precio</th>
		<th width='85'>Total</th>
		<th width='85'>Costo</th>
		<th width='85'>Costo total</th>
		<th width='85'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'>");
	
	$detalles = $db->select('tb_cotizacion_detalle',
	"dc_cotizacion_detalle,dg_producto,dq_cantidad,dg_descripcion,
	dq_precio_venta/{$datos['dq_cambio']} as dq_precio_venta,dq_precio_compra/{$datos['dq_cambio']} as dq_precio_compra,dc_proveedor",
	"dc_cotizacion={$_POST['id']}");
	
	$proveedores = $db->select('tb_proveedor','dc_proveedor,dg_razon',"dc_empresa={$empresa} AND dm_activo ='1'");
	
	foreach($detalles as $d){
		echo("
		<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
			<input type='hidden' name='prod_id_detalle[]' value='{$d['dc_cotizacion_detalle']}' />
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo inputtext' size='7' value='{$d['dg_producto']}' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' value='{$d['dg_descripcion']}' /></td>
		<td><select name='proveedor[]' style='width:155px;' class='inputtext' required='required'><option></option>");
		foreach($proveedores as $p){
			$selected = ($d['dc_proveedor'] == $p['dc_proveedor'])?"selected='selected'":'';
			echo("<option value='{$p['dc_proveedor']}' {$selected}>{$p['dg_razon']}</option>");
		}
		echo("</select></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' value='{$d['dq_cantidad']}' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' value='{$d['dq_precio_venta']}' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' value='{$d['dq_precio_compra']}' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td class='margen_p'><input type='text' class='margen_p inputtext' size='7' /></td>
	</tr>
	<tr style='display:none;'>
		<td colspan='5' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_price' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_margen' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>");
	}
	
	echo("</tbody>
	<tfoot>
	<tr>
		<th colspan='5' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center' id='total_margen_p'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div>
	</div>");
	$form->Hidden('cot_id',$_POST['id']);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->End('Editar','editbtn');
	
	echo("<table id='prods_form' style='display:none;'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
			<img src='images/addbtn.png' alt='' title='Más detalles' class='more_details' />
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo inputtext' size='7' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' /></td>
		<td><select name='proveedor[]' style='width:155px;' class='inputtext' required='required'><option></option>");
		foreach($proveedores as $p){
			echo("<option value='{$p['dc_proveedor']}'>{$p['dg_razon']}</option>");
		}
		echo("</select></td>
		<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td class='margen_p'><input type='text' class='margen_p inputtext' size='7' /></td>
	</tr>
	<tr style='display:none;'>
		<td colspan='5' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>
			Valores en {$empresa_conf['dg_moneda_local']}
		</td>
		<td class='l_price' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_costo_total' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td class='l_margen' align='right' style='background:#EEE;border-bottom:1px solid #AAA;'>0</td>
		<td style='background:#EEE;border-bottom:1px solid #AAA;'>&nbsp;</td>
	</tr>
	</table>");
?>
<script type="text/javascript">
	$('#toggle_header').click(function(){
		$('#cot_header_data').toggle();
	});
	empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	empresa_dec = <?=$empresa_conf['dn_decimales_local'] ?>;
</script>
<script type="text/javascript" src="jscripts/product_manager/cotizacion.js?v=1_1"></script>
<script type="text/javascript">
	init_autocomplete();
	tc = <?=$datos['dq_cambio'] ?>;
	tc_dec = $(":hidden[name=decimal<?=$datos['dc_tipo_cambio'] ?>]").val();
	actualizar_detalles();
	actualizar_totales();
</script>