<?php

/**
 * Description of ForecastFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 27-08-2013
 */
class ForecastFactory extends Factory{
  
    protected $title = "Forecast";
    private $tipo_cambio;
    
    public function indexAction(){
      
      $form = $this->getFormView($this->getTemplateURL('index.form'), array(
          'meses' => $this->getListadoMeses()
      ));
      
      echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
    }
    
    public function obtenerForecastAction(){
        $this->tipo_cambio = $this->getTipoCambio();
        $data = $this->getListadoCotizaciones();
		$this->initFunctionsService();
        
        echo $this->getView($this->getTemplateURL('graficos'), array(
            'ejecutivo' => $this->filtrarPorEjecutivo($data),
            'estado' => $this->filtrarPorEstado($data),
            'marca' => $this->filtrarPorMarca($data),
            'lineaNegocio' => $this->filtrarPorLineaNegocio($data),
            'data' => $data,
            'tipo_cambio' => $this->tipo_cambio
        ));
    }
    
    private function getListadoMeses(){
      return array(
          1 => 'Enero',
               'Febrero',
               'Marzo',
               'Abril',
               'Mayo',
               'Junio',
               'Julio',
               'Agosto',
               'Septiembre', 
               'Octubre',
               'Noviembre',
               'Diciembre'
      );
    }
    
    private function getTipoCambio(){
      $tc = $this->getConnection()->getRowById('tb_tipo_cambio', self::getRequest()->dc_tipo_cambio, 'dc_tipo_cambio');
      if($tc === false){
        $p = $this->getParametrosEmpresa();
        return (object) array(
            'dm_decimales' => $p->dn_decimales_local,
            'dg_tipo_cambio' => $p->dg_moneda_local,
            'dq_cambio' => 1.0
        );
      }else{
        return (object) array(
            'dm_decimales' => $tc->dn_cantidad_decimales,
            'dg_tipo_cambio' => $tc->dg_tipo_cambio,
            'dq_cambio' => $tc->dq_cambio
        );
      }
    }
    
    private function getListadoCotizaciones(){
      $db = $this->getConnection();
      $r = self::getRequest();
      
      $data = $db->prepare(
                $db->select(
                    'tb_cotizacion cot
                        JOIN tb_cotizacion_estado e ON e.dc_estado = cot.dc_estado
                        JOIN tb_funcionario f ON f.dc_funcionario = cot.dc_ejecutivo
                        JOIN tb_cliente cl ON cl.dc_cliente = cot.dc_cliente
                        LEFT JOIN
                             (SELECT dc_cotizacion, MAX(df_creacion) as df_ultima_gestion, MAX(dc_avance) dc_avance
                              FROM tb_cotizacion_avance
                              GROUP BY dc_cotizacion) av ON av.dc_cotizacion = cot.dc_cotizacion
                        LEFT JOIN tb_cotizacion_avance av2 ON av2.dc_avance = av.dc_avance
                        JOIN tb_cotizacion_detalle d ON d.dc_cotizacion = cot.dc_cotizacion
                        LEFT JOIN tb_proveedor prov ON prov.dc_proveedor = d.dc_proveedor
                        LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = ?
                        LEFT JOIN tb_marca m ON m.dc_marca = p.dc_marca
                        LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio',
                    'cot.dc_ejecutivo, cot.dq_cotizacion, cot.df_fecha_emision, cot.dq_neto, cot.dq_oportunidad, e.dc_estado,
                     e.dg_estado, av.df_ultima_gestion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, cl.dc_cliente,
                     cl.dg_razon, m.dc_marca, m.dg_marca, d.dg_descripcion, d.dg_producto,d.dq_precio_venta, d.dq_precio_compra,
                     d.dq_cantidad, av2.dg_gestion, (d.dq_precio_venta*d.dq_cantidad) dq_precio_total,
                     (d.dq_precio_compra*d.dq_cantidad) dq_costo_total, (d.dq_precio_venta-d.dq_precio_compra)*d.dq_cantidad dq_margen,
                     ln.dc_linea_negocio, ln.dg_linea_negocio, prov.dg_razon dg_proveedor',
                    'cot.dc_empresa = ?
                     AND MONTH(df_fecha_emision) >= ?
                     AND MONTH(df_fecha_emision) <= ?
                     AND YEAR(df_fecha_emision) = ?',
                    array('order_by' => 'dq_margen')));
      $data->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
      $data->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
      $data->bindValue(3, $r->dc_mes_inicio, PDO::PARAM_INT);
      $data->bindValue(4, $r->dc_mes_final, PDO::PARAM_INT);
      $data->bindValue(5, $r->dc_anho, PDO::PARAM_INT);
      $db->stExec($data);
      //var_dump($data);
      return $this->limpiaCotizaciones($data);
      
    }
    
    private function limpiaCotizaciones(PDOStatement $data){
        $cot = array();
        
        while($d = $data->fetch(PDO::FETCH_OBJ)):
          
          if($d->dc_marca == NULL):
            $d->dc_marca = 0;
            $d->dg_marca = 'Sin Marca';
          endif;
          
          if($d->dc_linea_negocio == NULL):
            $d->dc_linea_negocio = 0;
            $d->dg_linea_negocio = 'Sin Linea Negocio';
          endif;
          
          if($d->dq_oportunidad == NULL):
            $d->dq_oportunidad = 'Sin definir';
          endif;
          
          $d->dq_precio_venta = floatval($d->dq_precio_venta/$this->tipo_cambio->dq_cambio);
          $d->dq_precio_compra = floatval($d->dq_precio_compra/$this->tipo_cambio->dq_cambio);
          $d->dq_precio_total = floatval($d->dq_precio_total/$this->tipo_cambio->dq_cambio);
          $d->dq_costo_total = floatval($d->dq_costo_total/$this->tipo_cambio->dq_cambio);
          $d->dq_margen = floatval($d->dq_margen/$this->tipo_cambio->dq_cambio);
          
          $d->dg_ejecutivo = $d->dg_nombres.' '.substr($d->dg_ap_paterno, 0, 1).'.';
          
          $cot[] = $d;
          
        endwhile;
        
        return $cot;
    }
    
    private function agruparPorCampo(&$data, $dc_campo, $dg_campo){
      $group = array();
      
      foreach($data as $d):
          if(!isset($group[$d->$dc_campo])):
            $group[$d->$dc_campo] = array($d->$dg_campo, 0);
          endif;
          
          $group[$d->$dc_campo][1] += $d->dq_neto;
      endforeach;
      
      return $group;
    }
    
    private function preparePie($group){
      $data = array();
      foreach($group as $d){
          $data[] = array(
              'label' => $d[0],
              'data' => round($d[1]/$this->tipo_cambio->dq_cambio,$this->tipo_cambio->dm_decimales)
          );
      }
      
      return $data;
    }
    
    private function filtrarPorEjecutivo(&$data){
      $group = $this->agruparPorCampo($data, 'dc_ejecutivo', 'dg_ejecutivo');
      return $this->preparePie($group);
    }
    
    private function filtrarPorEstado(&$data){
      $group = $this->agruparPorCampo($data, 'dc_estado', 'dg_estado');
      return $this->preparePie($group);
    }
    
    private function filtrarPorMarca(&$data){
      $group = $this->agruparPorCampo($data, 'dc_marca', 'dg_marca');
      return $this->preparePie($group);
    }
    
    private function filtrarPorLineaNegocio(&$data){
      $group = $this->agruparPorCampo($data, 'dc_linea_negocio', 'dg_linea_negocio');
      return $this->preparePie($group);
    }
    
}

?>
