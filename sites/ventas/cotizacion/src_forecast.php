<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Forecast</div><div id="main_cont"><div class="panes">');

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$meses = array(
	1 => 'Enero',
	2 => 'Febrero',
	3 => 'Marzo',
	4 => 'Abril',
	5 => 'Mayo',
	6 => 'Junio',
	7 => 'Julio',
	8 => 'Agosto',
	9 => 'Septiembre',
	10 => 'Octubre',
	11 => 'Noviembre',
	12 => 'Diciembre'
);

$form->Start('sites/ventas/cotizacion/proc/src_forecast.php','load_forecast','load_forecast','method="post"');
$form->Header('Indique los filtros a aplicar en el forecast');
$form->Section();

	$form->Select('Mes inicio','sel_inim',$meses,1,1);
	$form->Select('Mes fin','sel_finm',$meses,1,date('m'));

$form->EndSection();
$form->Section();

	$form->Text('Año','sel_anho',1,4,date('Y'));
	$form->Listado('Tipo de cambio','sel_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'));

$form->EndSection();
$form->End('Ejecutar');

?>
<div id="result_forecast"></div>
</div></div>
<script type="text/javascript" src="jscripts/lib/jquery.flot.js"></script>
<script type="text/javascript" src="jscripts/lib/jquery.flot.pie.js"></script>
<!--<script type="text/javascript" src="jscripts/sites/ventas/cotizacion/src_forecast.js?v0_3_2_2"></script>-->