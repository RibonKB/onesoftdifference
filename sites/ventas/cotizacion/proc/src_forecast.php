<?php
define("MAIN",1);
require_once("../../../../inc/global.php");

//Obtener valor tipo de cambio
$db->escape($_POST['sel_tipo_cambio']);
$dq_cambio = $db->select('tb_tipo_cambio','dg_tipo_cambio,dq_cambio,dn_cantidad_decimales',"dc_tipo_cambio={$_POST['sel_tipo_cambio']}");
if(!count($dq_cambio)){
	$error_man->showAviso("El tipo de cambio especificado es inválido, en su lugar se utiliza moneda local");
	$dm_decimales = $empresa_conf['dn_decimales_local'];
	$dg_tipo_cambio = $empresa_conf['dg_moneda_local'];
	$dq_cambio = 1;
}else{
	$dm_decimales = $dq_cambio[0]['dn_cantidad_decimales'];
	$dg_tipo_cambio = $dq_cambio[0]['dg_tipo_cambio'];
	$dq_cambio = $dq_cambio[0]['dq_cambio'];
}

$db->escape($_POST['sel_inim']);
$db->escape($_POST['sel_finm']);
$db->escape($_POST['sel_anho']);
$date_condition = "MONTH(df_fecha_emision) >= {$_POST['sel_inim']} AND MONTH(df_fecha_emision) <= {$_POST['sel_finm']} AND YEAR(df_fecha_emision) = {$_POST['sel_anho']}";

$sqlCotizacion = $db->select("(SELECT * FROM tb_cotizacion WHERE dc_empresa = {$empresa} AND {$date_condition}) cot
JOIN tb_cotizacion_estado e ON e.dc_estado = cot.dc_estado
LEFT JOIN (SELECT dc_cotizacion, MAX(df_creacion) as df_ultima_gestion, MAX(dc_avance) dc_avance FROM tb_cotizacion_avance GROUP BY dc_cotizacion) av ON av.dc_cotizacion = cot.dc_cotizacion
LEFT JOIN tb_cotizacion_avance av2 ON av2.dc_avance = av.dc_avance
JOIN tb_funcionario f ON f.dc_funcionario = cot.dc_ejecutivo
JOIN tb_cliente cl ON cl.dc_cliente = cot.dc_cliente
JOIN tb_cotizacion_detalle d ON d.dc_cotizacion = cot.dc_cotizacion
LEFT JOIN tb_proveedor prov ON prov.dc_proveedor = d.dc_proveedor
LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
LEFT JOIN tb_marca m ON m.dc_marca = p.dc_marca
LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio",
"cot.dc_ejecutivo, cot.dq_cotizacion, cot.df_fecha_emision, cot.dq_neto, cot.dq_oportunidad, e.dc_estado, e.dg_estado, av.df_ultima_gestion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, cl.dc_cliente, cl.dg_razon, m.dc_marca, m.dg_marca, d.dg_descripcion, d.dg_producto,d.dq_precio_venta, d.dq_precio_compra, d.dq_cantidad, av2.dg_gestion,
(d.dq_precio_venta*d.dq_cantidad) dq_precio_total, (d.dq_precio_compra*d.dq_cantidad) dq_costo_total, (d.dq_precio_venta-d.dq_precio_compra)*d.dq_cantidad dq_margen,
ln.dc_linea_negocio, ln.dg_linea_negocio, prov.dg_razon dg_proveedor",'',
array('order_by' => 'dq_margen'));

$ejecutivos = array();
$estados = array();
$marcas = array();
$lineas = array();

foreach($sqlCotizacion as $i => $cot){
	
	if($cot['dc_marca'] == NULL){
		
		$sqlCotizacion[$i]['dc_marca'] = 0;
		$sqlCotizacion[$i]['dg_marca'] = 'Sin Marca';
		
		$marcas[0]['nombre'] = 'Sin Marca';
		$marcas[0]['monto'][] = $cot['dq_neto'];
	}else{
		$marcas[$cot['dc_marca']]['nombre'] = $cot['dg_marca'];
		$marcas[$cot['dc_marca']]['monto'][] = $cot['dq_neto'];
	}
	
	if($cot['dc_linea_negocio'] == NULL){
		
		$sqlCotizacion[$i]['dc_linea_negocio'] = 0;
		$sqlCotizacion[$i]['dg_linea_negocio'] = 'Sin Linea de negocio';
		
		$lineas[0]['nombre'] = 'Sin Linea de negocio';
		$lineas[0]['monto'][] = $cot['dq_neto'];
	}else{
		$lineas[$cot['dc_linea_negocio']]['nombre'] = $cot['dg_linea_negocio'];
		$lineas[$cot['dc_linea_negocio']]['monto'][] = $cot['dq_neto'];
	}
	
	if($cot['dq_oportunidad'] == NULL){
		$sqlCotizacion[$i]['dq_oportunidad'] = 'Sin definir';
	}
	
	$ejecutivos[$cot['dc_ejecutivo']]['nombre_parcial'] = $cot['dg_nombres'].' '.substr($cot['dg_ap_paterno'],0,1).'.';
	$ejecutivos[$cot['dc_ejecutivo']]['monto'][] = $cot['dq_neto'];
	
	$estados[$cot['dc_estado']]['nombre'] = $cot['dg_estado'];
	$estados[$cot['dc_estado']]['monto'][] = $cot['dq_neto'];
	
	$sqlCotizacion[$i]['dq_precio_venta'] = $sqlCotizacion[$i]['dq_precio_venta']/$dq_cambio;
	$sqlCotizacion[$i]['dq_precio_compra'] = $sqlCotizacion[$i]['dq_precio_compra']/$dq_cambio;
	$sqlCotizacion[$i]['dq_precio_total'] = $sqlCotizacion[$i]['dq_precio_total']/$dq_cambio;
	$sqlCotizacion[$i]['dq_costo_total'] = $sqlCotizacion[$i]['dq_costo_total']/$dq_cambio;
	$sqlCotizacion[$i]['dq_margen'] = $sqlCotizacion[$i]['dq_margen']/$dq_cambio;
}

$percentage = array();
foreach($ejecutivos as $ex){
	$percentage[] = array(
		'label' => $ex['nombre_parcial'],
		'data' => round(array_sum($ex['monto'])/$dq_cambio,$dm_decimales)
	);
}

$p_estado = array();
foreach($estados as $es){
	$p_estado[] = array(
		'label' => $es['nombre'],
		'data' => round(array_sum($es['monto'])/$dq_cambio,$dm_decimales)
	);
}

$p_marca = array();
foreach($marcas as $m){
	$p_marca[] = array(
		'label' => $m['nombre'],
		'data' => round(array_sum($m['monto'])/$dq_cambio,$dm_decimales)
	);
}

$p_linea = array();
foreach($lineas as $l){
	$p_linea[] = array(
		'label' => $l['nombre'],
		'data' => round(array_sum($l['monto'])/$dq_cambio,$dm_decimales)
	);
}

$sqlPeriodos = $db->select('tb_cotizacion c
JOIN tb_funcionario f ON f.dc_funcionario = c.dc_ejecutivo',
'c.dc_ejecutivo , f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno ,MONTH(c.df_fecha_emision) df_mes_emision, SUM(c.dq_neto) dq_neto_total',
"c.dc_empresa = {$empresa} AND YEAR(c.df_fecha_emision) = {$_POST['sel_anho']}",array('group_by' => 'c.dc_ejecutivo, df_mes_emision'));

$periodos = array();
foreach($sqlPeriodos as $p){
	$periodos[$p['dc_ejecutivo']]['puntos'][] = array(intval($p['df_mes_emision']),floatval($p['dq_neto_total']));
	$periodos[$p['dc_ejecutivo']]['nombre_completo'] = $p['dg_nombres'].' '.substr($p['dg_ap_paterno'],0,1).'.';
}
unset($sqlPeriodos);

?>

<!-- div id="ex_list"></div><br / -->
<!--div id="grafico_puntos" style="width:900px;height:400px;float:left;"></div-->

<div id="grafico_torta" style="width:450px;height:300px;float:left;"></div>

<div id="grafico_torta_estado" style="width:450px;height:300px;float:left;"></div>

<div id="grafico_torta_marca" style="width:450px;height:300px;float:left;"></div>

<div id="grafico_torta_linea" style="width:450px;height:300px;float:left;"></div>

<hr class="clear" />
<div id="data_table"></div>
<br class="clear" />

<div id="detail_data_table">

<table class="tab" width="2500">
<thead><tr>
	<th>Cotización</th>
	<th>Fecha emisión</th>
	<th>Oportunidad</th>
	<th>Estado</th>
	<th>Fecha Última Gestión</th>
	<th>Gestión</th>
	<th>Ejecutivo</th>
	<th>Cliente</th>
	<th>Marca</th>
	<th>Linea negocio</th>
	<th>Descripción</th>
	<th>Código</th>
    <th>Proveedor</th>
	<th>Precio Unitario</th>
	<th>Costo unitario</th>
	<th>Cantidad</th>
	<th>Precio total</th>
	<th>Costo total</th>
	<th>Margen</th>
</tr></thead>
<tbody>
<?php foreach($sqlCotizacion as $cot): ?>
	<tr>
		<td><?php echo $cot['dq_cotizacion'] ?></td>
		<td><?php echo $cot['df_fecha_emision'] ?></td>
		<td><?php echo $cot['dq_oportunidad'] ?></td>
		<td class="dc_estado<?php echo $cot['dc_estado'] ?>"><?php echo $cot['dg_estado'] ?></td>
		<td><?php echo $cot['df_ultima_gestion'] ?></td>
		<td><?php echo $cot['dg_gestion'] ?></td>
		<td class="dc_ejecutivo<?php echo $cot['dc_ejecutivo'] ?>">
			<?php echo $cot['dg_nombres'].' '.$cot['dg_ap_paterno'].' '.$cot['dg_ap_materno'] ?>
		</td>
		<td class="dc_cliente<?php echo $cot['dc_cliente'] ?>">
			<?php echo $cot['dg_razon'] ?>
		</td>
		<td class="dc_marca<?php echo $cot['dc_marca'] ?>">
			<?php echo $cot['dg_marca'] ?>
		</td>
		<td class="dc_linea_negocio<?php echo $cot['dc_linea_negocio'] ?>">
			<?php echo $cot['dg_linea_negocio'] ?>
		</td>
		<td><?php echo $cot['dg_descripcion'] ?></td>
		<td><?php echo $cot['dg_producto'] ?></td>
        <td><?php echo $cot['dg_proveedor'] ?></td>
		<td align="right"><?php echo moneda_local($cot['dq_precio_venta'],$dm_decimales) ?></td>
		<td align="right"><?php echo moneda_local($cot['dq_precio_compra'],$dm_decimales) ?></td>
		<td align="center"><?php echo $cot['dq_cantidad'] ?></td>
		<td align="right"><?php echo moneda_local($cot['dq_precio_total'],$dm_decimales) ?></td>
		<td align="right"><?php echo moneda_local($cot['dq_costo_total'],$dm_decimales) ?></td>
		<td align="right"><?php echo moneda_local($cot['dq_margen'],$dm_decimales) ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
<tfoot>
	<th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th><th></th>
	<th align="right"></th>
	<th align="right"></th>
	<th></th>
	<th align="right"></th>
	<th align="right"></th>
	<th align="right"></th>
</tfoot>
</table>

</div>

<script type="text/javascript">
	js_data.piePlotData = <?php echo json_encode($percentage) ?>;
	js_data.piePlotStatus = <?php echo json_encode($p_estado) ?>;
	js_data.piePlotMarca = <?php echo json_encode($p_marca) ?>;
	js_data.piePlotLinea = <?php echo json_encode($p_linea) ?>;
	js_data.periodPlotData = <?php echo json_encode($periodos) ?>;
	
	js_data.cotizacionData = <?php echo json_encode($sqlCotizacion) ?>;
	js_data.tcDecimals = <?php echo $dm_decimales ?>;
	
	$('#detail_data_table table').tableExport();
</script>