<?php
define('MAIN',1);
require('../../../../inc/init.php');

$dq_oportunidad = $_GET['dq_oportunidad'];
if(!is_numeric($dq_oportunidad)){
	echo json_encode('<not-found>');
	exit;
}

$dc_oportunidad = $db->prepare($db->select('tb_oportunidad','dc_oportunidad',"dq_oportunidad = ?"));
$dc_oportunidad->bindValue(1,$dq_oportunidad,PDO::PARAM_INT);
$dc_oportunidad = $dc_oportunidad->fetch(PDO::FETCH_OBJ);

if($dc_oportunidad === false){
	echo json_encode('<not-found>');
}else{
	echo json_encode($dc_oportunidad->dc_oportunidad);
}
?>