<?php
error_reporting(E_ALL);
ini_set('memory_limit','0');
date_default_timezone_set('Europe/London');

define("MAIN",1);
require_once("../../../../inc/global.php");
require_once('../../../../inc/PHPExcel.php');

if(!isset($_POST))exit();

//Obtener valor tipo de cambio
$db->escape($_POST['sel_tipo_cambio']);
$dq_cambio = $db->select('tb_tipo_cambio','dg_tipo_cambio,dq_cambio,dn_cantidad_decimales',"dc_tipo_cambio={$_POST['sel_tipo_cambio']}");
if(!count($dq_cambio)){
	$dm_decimales = $empresa_conf['dn_decimales_local'];
	$dg_tipo_cambio = $empresa_conf['dg_moneda_local'];
	$dq_cambio = 1;
}else{
	$dm_decimales = $dq_cambio[0]['dn_cantidad_decimales'];
	$dg_tipo_cambio = $dq_cambio[0]['dg_tipo_cambio'];
	$dq_cambio = $dq_cambio[0]['dq_cambio'];
}

$db->escape($_POST['sel_inim']);
$db->escape($_POST['sel_finm']);
$db->escape($_POST['sel_anho']);
$date_condition = "MONTH(df_fecha_emision) >= {$_POST['sel_inim']} AND MONTH(df_fecha_emision) <= {$_POST['sel_finm']} AND YEAR(df_fecha_emision) = {$_POST['sel_anho']}";

$sqlCotizacion = $db->select("(SELECT * FROM tb_cotizacion WHERE dc_empresa = {$empresa} AND {$date_condition}) cot
JOIN tb_cotizacion_estado e ON e.dc_estado = cot.dc_estado
LEFT JOIN (SELECT dc_cotizacion, MAX(df_creacion) as df_ultima_gestion, MAX(dc_avance) dc_avance FROM tb_cotizacion_avance GROUP BY dc_cotizacion) av ON av.dc_cotizacion = cot.dc_cotizacion
LEFT JOIN tb_cotizacion_avance av2 ON av2.dc_avance = av.dc_avance
JOIN tb_funcionario f ON f.dc_funcionario = cot.dc_ejecutivo
JOIN tb_cliente cl ON cl.dc_cliente = cot.dc_cliente
JOIN tb_cotizacion_detalle d ON d.dc_cotizacion = cot.dc_cotizacion
LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
LEFT JOIN tb_marca m ON m.dc_marca = p.dc_marca
LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio",
"cot.dc_ejecutivo, cot.dq_cotizacion, cot.df_fecha_emision, cot.dq_neto, cot.dq_oportunidad, e.dc_estado, e.dg_estado, av.df_ultima_gestion, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, cl.dc_cliente, cl.dg_razon, m.dc_marca, m.dg_marca, d.dg_descripcion, d.dg_producto,d.dq_precio_venta, d.dq_precio_compra, d.dq_cantidad, av2.dg_gestion,
(d.dq_precio_venta*d.dq_cantidad) dq_precio_total, (d.dq_precio_compra*d.dq_cantidad) dq_costo_total, (d.dq_precio_venta-d.dq_precio_compra)*d.dq_cantidad dq_margen,
ln.dc_linea_negocio, ln.dg_linea_negocio",'',
array('order_by' => 'dq_margen'));

$excel = new PHPExcel();

$excel->getProperties()->setCreator("{$userdata['dg_nombres']} {$userdata['dg_ap_paterno']} {$userdata['dg_ap_materno']}")
						->setLastModifiedBy("{$userdata['dg_nombres']} {$userdata['dg_ap_paterno']} {$userdata['dg_ap_materno']}")
						->setTitle("Forecast {$_POST['sel_inim']}/{$_POST['sel_anho']} - {$_POST['sel_finm']}/{$_POST['sel_anho']}")
						->setSubject("Forecast {$_POST['sel_inim']}/{$_POST['sel_anho']} - {$_POST['sel_finm']}/{$_POST['sel_anho']}")
						->setCategory("Forecast");
						
$excel->setActiveSheetIndex(0)
	->setCellValue('A1', 'Cotización')
	->setCellValue('B1', 'Fecha emisión')
	->setCellValue('C1', 'Oportunidad')
	->setCellValue('D1', 'Estado')
	->setCellValue('E1', 'Fecha Última Gestión')
	->setCellValue('F1', 'Gestión')
	->setCellValue('G1', 'Ejecutivo')
	->setCellValue('H1', 'Cliente')
	->setCellValue('I1', 'Marca')
	->setCellValue('J1', 'Linea negocio')
	->setCellValue('K1', 'Descripción')
	->setCellValue('L1', 'Código')
	->setCellValue('M1', 'Precio Unitario')
	->setCellValue('N1', 'Costo unitario')
	->setCellValue('O1', 'Cantidad')
	->setCellValue('P1', 'Precio total')
	->setCellValue('Q1', 'Costo total')
	->setCellValue('R1', 'Margen');
	
foreach($sqlCotizacion as $i => $cot):

$indice = $i+2;

$excel->setActiveSheetIndex(0)
	->setCellValue('A'.$indice, $cot['dq_cotizacion'])
	->setCellValue('B'.$indice, $cot['df_fecha_emision'])
	->setCellValue('C'.$indice, $cot['dq_oportunidad'])
	->setCellValue('D'.$indice, $cot['dg_estado'])
	->setCellValue('E'.$indice, $cot['df_ultima_gestion'])
	->setCellValue('F'.$indice, $cot['dg_gestion'])
	->setCellValue('G'.$indice, $cot['dg_nombres'].' '.$cot['dg_ap_paterno'].' '.$cot['dg_ap_materno'])
	->setCellValue('H'.$indice, $cot['dg_razon'])
	->setCellValue('I'.$indice, $cot['dg_marca'])
	->setCellValue('J'.$indice, $cot['dg_linea_negocio'])
	->setCellValue('K'.$indice, $cot['dg_descripcion'])
	->setCellValue('L'.$indice, $cot['dg_producto'])
	->setCellValue('M'.$indice, $cot['dq_precio_venta'])
	->setCellValue('N'.$indice, $cot['dq_precio_compra'])
	->setCellValue('O'.$indice, $cot['dq_cantidad'])
	->setCellValue('P'.$indice, $cot['dq_precio_total'])
	->setCellValue('Q'.$indice, $cot['dq_costo_total'])
	->setCellValue('R'.$indice, $cot['dq_margen']);
	
endforeach;

$letras = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R');
foreach($letras as $letra){
	$excel->getActiveSheet()->getColumnDimension($letra)->setAutoSize(true);
}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Forecast.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');

$objWriter->save('php://output');

?>