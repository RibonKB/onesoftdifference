<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$ejecutivos = $db->select('tb_funcionario','dc_funcionario, dg_nombres, dg_ap_paterno, dg_ap_materno','dc_empresa='.$empresa);
$ordered_ex = array();
foreach($ejecutivos as $e){
	$ordered_ex[$e['dc_funcionario']] = $e['dg_nombres'].' '.$e['dg_ap_paterno'].' '.$e['dg_ap_materno'];
}

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">Metas de Ventas</div>
<div id="main_cont">
<div class="panes">

<?php
$form->Start('sites/ventas/meta/proc/crear_meta.php','cr_meta');
$form->Header('<strong>Indique los datos para crear una nueva meta</strong><br />Los campos marcados con [*] son obligatorios');

$form->Section();
	$form->Listado('Marca','dc_marca','tb_marca',array('dc_marca','dg_marca'),1);
	$form->Listado('Linea de Negocio','dc_linea_negocio','tb_linea_negocio',array('dc_linea_negocio','dg_linea_negocio'),1);
	$form->Listado('Tipo de cambio','dc_tipo_cambio','tb_tipo_cambio',array('dc_tipo_cambio','dg_tipo_cambio'),1);
$form->EndSection();

$form->Section();
	$form->Text('Monto Meta Anual','dq_monto_anual',1,22);
	$form->Button('Distribuir monto entre meses','id="month_amount_distribution"');
$form->EndSection();

$form->Section();
	$form->Text('Cantidad Meta Anual','dq_cantidad_anual',1,22);
	$form->Button('Distribuir cantidad entre meses','id="month_cant_distribution"');
$form->EndSection();

$form->Group();

$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

?>

<table class="tab" width="100%">
<caption>Distribución mensual de la meta</caption>
<thead>
	<tr>
		<th width="100">Mes</th>
		<th width="120">Monto</th>
		<th width="120">Cantidad</th>
		<th colspan="2">Distribución por ejecutivo</th>
	</tr>
</thead>
<tbody>
	<?php foreach($meses as $i => $mes): ?>
	<tr>
		<td><b><?php echo $mes ?></b></td>
		<td><input type="text" class="dq_monto_mensual inputtext" name="dq_monto_mensual[]" size="15" /></td>
		<td><input type="text" class="dq_cantidad_mensual inputtext" name="dq_cantidad_mensual[]" size="15" /></td>
		<td width="140">
			<button type="button" class="setExecutive addbtn">Asignar Ejecutivos</button>
			<input type="hidden" value="<?php echo $i ?>" />
		</td>
		<td id="ex_container<?php echo $i ?>"></td>
	</tr>
	<?php endforeach; ?>
</tbody>
</table>

<?php
$form->End('Crear','addbtn');
?>

</div>
</div>
<script type="text/javascript" src="jscripts/sites/ventas/meta/cr_meta.js?v=0_1a5"></script>
<script type="text/javascript">
	js_data.ejecutivosList = <?php echo json_encode($ordered_ex) ?>;
	js_data.mesesList = <?php echo json_encode($meses) ?>;
	js_data.init();
</script>