<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$insert_meta = $db->prepare($db->insert('tb_meta',array(
	'dc_marca' => '?',
	'dc_linea_negocio' => '?',
	'dq_monto_anual' => '?',
	'dq_meta_cantidad' => '?',
	'dc_empresa' => $empresa,
	'dc_usuario_creacion' => $idUsuario,
	'df_creacion' => $db->getNow()
)));

$insert_meta->bindValue(1,$_POST['dc_marca'],PDO::PARAM_INT);
$insert_meta->bindValue(2,$_POST['dc_linea_negocio'],PDO::PARAM_INT);
$insert_meta->bindValue(3,$_POST['dq_monto_anual'],PDO::PARAM_INT);
$insert_meta->bindValue(4,$_POST['dq_cantidad_anual'],PDO::PARAM_INT);

$insert_meta_mensual = $db->prepare($db->insert('tb_meta_mensual',array(
	'dc_mes' => '?',
	'dc_meta' => '?',
	'dq_monto_mensual' => '?',
	'dq_meta_cantidad' => '?'
)));

$insert_meta_mensual->bindParam(1,$dc_mes,PDO::PARAM_INT);
$insert_meta_mensual->bindParam(2,$dc_meta,PDO::PARAM_INT);
$insert_meta_mensual->bindParam(3,$dq_monto_mensual,PDO::PARAM_STR);
$insert_meta_mensual->bindParam(4,$dq_meta_cantidad,PDO::PARAM_INT);

$insert_meta_ejecutivo = $db->prepare($db->insert('tb_meta_ejecutivo',array(
	'dc_ejecutivo' => '?',
	'dc_mes' => '?',
	'dc_meta' => '?',
	'dc_porcentaje' => '?'
)));

$insert_meta_ejecutivo->bindParam(1,$dc_ejecutivo,PDO::PARAM_INT);
$insert_meta_ejecutivo->bindParam(2,$dc_mes,PDO::PARAM_INT);
$insert_meta_ejecutivo->bindParam(3,$dc_meta,PDO::PARAM_INT);
$insert_meta_ejecutivo->bindParam(4,$dc_porcentaje,PDO::PARAM_INT);

$db->start_transaction();
$db->stExec($insert_meta);
$dc_meta = $db->lastInsertId();

foreach($_POST['dq_monto_mensual'] as $i => $v):
	if(trim($v) == '')
		continue;
	
	$dc_mes = $i;
	$dq_monto_mensual = intval($v);
	$dq_meta_cantidad = intval($_POST['dq_cantidad_mensual'][$i]);
	
	$db->stExec($insert_meta_mensual);
	
	if(isset($_POST['dc_ejecutivo'][$dc_mes])):
		foreach($_POST['dc_ejecutivo'][$dc_mes] as $j => $vv):
			$dc_ejecutivo = $vv;
			$dc_porcentaje = $_POST['dq_cantidad_ejecutivo'][$dc_mes][$j];
			
			$db->stExec($insert_meta_ejecutivo);
		endforeach;
	endif;
	
endforeach;

$db->commit();

$error_man->showConfirm("Se han creado las metas para los ejecutivos en los periodos seleccionados.");

?>