<?php
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/cotizacion.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_cotizacion_contacto_extra",
"dg_etiqueta",
"dc_contacto_extra = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado el contacto extra");
	exit();
}
?>
<div class="secc_bar">Eliminar Contactos extra de <?=$lang['cot_doc_may'] ?></div>
<div class="panes">
	<form action="sites/ventas/proc/delete_cotizacion_cextra.php" class="confirmValidar" id="del_cextra">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar el contacto extra de <?=$lang['cot_doc'] ?>?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_cextra_id" value="<?php echo($_POST['id']); ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
				<input type="button" class="button" value="Cancelar" onclick="$('#genOverlay').remove();" />
			</div>
		</fieldset>
	</form>
	<div id="del_cextra_res"></div>
</div>