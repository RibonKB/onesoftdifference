<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/lang/{$empresa}/cotizacion.lang.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$estado_cot =& $empresa_conf['dn_cotizacion_dias_actividad'];

$data = $db->select("(SELECT * FROM tb_cotizacion_avance WHERE dc_cotizacion = {$_POST['id']} ORDER BY df_creacion DESC LIMIT 2) a
LEFT JOIN tb_cotizacion_estado e ON a.dc_estado_cotizacion = e.dc_estado",
'a.dg_gestion,a.dg_contacto,a.dc_estado_cotizacion,e.dg_estado,a.dc_avance,
DATE_FORMAT(a.df_creacion,"%d/%m/%Y %H:%i") as df_creacion,
DATE_FORMAT(a.df_proxima_actividad,"%d/%m/%Y") as df_estimada,
DATE_FORMAT(a.df_cierre_actividad,"%d/%m/%Y") as df_cierre');

$dc_cliente = $db->select('tb_cotizacion','dc_cliente',"dc_cotizacion={$_POST['id']}");
$dc_cliente = $dc_cliente[0]['dc_cliente'];

echo("<div class='panes' style='width:850px;'>");
if(count($data)){

$estado_cot = $data[0]['dc_estado_cotizacion'];

echo("<table class='tab' width='100%'>
<caption>Últimas actividades realizadas sobre la {$lang['cot_doc']}</caption>
<thead><tr>
	<th>Gestion</th>
	<th width='150'>Contacto</th>
	<th width='120'>Estado</th>
	<th width='90'>Fecha creación</th>
	<th width='90'>Cierre estimado</th>
	<th width='90'>Fecha cierre</th>
</thead><tbody>");
	foreach($data as $d){
		echo("<tr>
			<td>{$d['dg_gestion']}</td>
			<td>{$d['dg_contacto']}</td>
			<td>{$d['dg_estado']}</td>
			<td>{$d['df_creacion']}</td>
			<td>{$d['df_estimada']}</td>
			<td>{$d['df_cierre']}</td>
		</tr>");
	}
echo("</tbody></table><hr />");
}

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/ventas/proc/crear_gestion_cotizacion.php','cr_gestion');
$form->Header('Nueva actividad');
$form->Section();
$form->Listado('Contacto','dc_contacto','tb_contacto_cliente',array('dc_contacto','dg_contacto'),1,0,"dc_cliente={$dc_cliente}");
$form->Listado("Estado {$lang['cot_doc']}",'gestion_estado','tb_cotizacion_estado',array('dc_estado','dg_estado'),1,$estado_cot);
$form->Date('Fecha próxima gestión','gestion_fecha');

//Hora de la gestión
echo("<label>Hora</label>
<select name='gestion_hora' class='inputtext' style='width:50px;'>");
foreach(range(1,12) as $h){
	$h = str_pad($h,2,'0',STR_PAD_LEFT);
	echo("<option value='{$h}'>{$h}</option>");
}
echo("</select> : <select name='gestion_minuto' class='inputtext' style='width:50px;'>");
foreach(range(0,45,15) as $m){
	$m = str_pad($m,2,'0',STR_PAD_LEFT);
	echo("<option value='{$m}'>{$m}</option>");
}
echo("</select> - 
<select name='gestion_meridiano' class='inputtext' style='width:70px;'>
<option value='0'>AM</option>
<option value='12'>PM</option>
</select>");

$form->EndSection();
$form->Section();
$form->Textarea('Gestión','gestion_gestion',1);
$form->Listado('Motivo Estado','dc_motivo_estado','tb_motivo_estado_cotizacion',array('dc_motivo','dg_motivo'));
$form->Listado('Marca referencia','dc_marca_referencia','tb_marca',array('dc_marca','dg_marca'));
$form->EndSection();
echo('<div class="left" id="contact_data"></div>');
$form->Hidden('cot_id',$_POST['id']);
$form->Hidden('gestion_cambio','');
if(count($data))
	$form->Hidden('gestion_avance',$data[0]['dc_avance']);
$form->End('Crear','addbtn');
?>
</div>
<script type="text/javascript" src="jscripts/sites/ventas/cotizacion/src_gestion_cotizacion.js?v=0_2b"></script>