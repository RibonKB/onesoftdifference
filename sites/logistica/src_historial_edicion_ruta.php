<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_ruta = intval($_POST['id']);

$data = $db->select('tb_ruta','dq_ruta',"dc_ruta={$dc_ruta}");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la Ruta especificada, por favor compruebe los parámetros de entrada.");
	exit;
}

$data = $data[0];

$detail_data = $db->select('tb_ruta_detalle d
JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
JOIN tb_producto p ON p.dc_producto = nvd.dc_producto
JOIN tb_nota_venta nv ON nv.dc_nota_venta = nvd.dc_nota_venta',
'd.dq_cantidad, d.dt_hora, nvd.dg_descripcion, p.dg_codigo, nv.dq_nota_venta',
"d.dc_ruta = {$dc_ruta} AND d.dm_activo = 0");

?>
<div class="secc_bar">Historial de cambios de ruta <?php echo $data['dq_ruta'] ?></div>
<div class="panes">
	<?php if(count($detail_data)): ?>
	<table class="tab" width="100%">
	<thead>
		<tr>
			<th>Código</th>
			<th>Producto</th>
			<th>Hora</th>
			<th>Cantidad</th>
			<th>NV</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($detail_data as $d): ?>
		<tr>
			<td><?php echo $d['dg_codigo'] ?></td>
			<td><?php echo $d['dg_descripcion'] ?></td>
			<td><?php echo substr($d['dt_hora'],0,5) ?></td>
			<td><?php echo $d['dq_cantidad'] ?></td>
			<td><?php echo $d['dq_nota_venta'] ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
	<?php 
		else: 
			$error_man->showConfirm("No se han realizado modificaciones sobre la ruta") ;
		endif;
	?>
</div>