<?php
/**
*	Rama aferente de traspaso de stock
*	
*	Procesos: Elegir producto -> Mostrar stock por bodega -> Ingresar datos de traspaso
*	
*	OUT: dc_producto, dc_bodega_salida, dc_bodega_entrada, stock_a_traspasar
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Traspaso de stock simple</div><div id="main_cont"><div class="panes">');

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/logistica/proc/set_traspaso_simple.php','set_traspaso','logistica_form');
$form->Header('<b>Indique los datos necesarios para realizar un traspaso simple (de stock libre) entre bodegas</b><br />
Los campos marcados con [*] son obligatorios');

/**
*	Elegir Producto
*	
*	El traspaso se realiza sobre un producto
*	
*	IN:		dg_codigo
*	OUT:	dc_producto
**/

$form->Text('Producto (autocompleta)','tr_producto',1);

/**
*	Mostrar Stock por bodega
*	
*	Se muestra el detalle de stock para el producto seleccionado
*	Debido a que el producto es cambiado dinámicamente solo se inclye un contenedor para los resultados de stock
*	
*	IN:		dc_producto
*	OUT:	[Grid] listado stock por bodega
**/

echo('<div id="src_stock_container"></div>
<br /><hr class="clear" />');

/**
*	Ingresar datos de traspaso
*	
*	Se capturan los datos extra para el traspaso, las bodegas y la cantidad de stock que será trasladado
*	
*	IN:		Listado bodegas
*	OUT:	dc_bodega_salida, dc_bodega_entrada, stock_a_traspasar
**/

//CODE-HERE
$form->Section();
$form->Listado('Bodega de salida','tr_bodega_salida','tb_bodega',array('dc_bodega','dg_bodega'),1);
$form->Listado('Bodega de entrada','tr_bodega_entrada','tb_bodega',array('dc_bodega','dg_bodega'),1);
$form->EndSection();

$form->Section();
$form->Text('Cantidad','tr_cantidad',1,11);
$form->Radiobox('Incluir','tr_modo',array('Stock Libre','Stock Reservado','Todo el Stock (Stock libre prioritario)'),0);
$form->EndSection();

$form->End('Traspasar');

?>
</div></div>
<script type="text/javascript" src="jscripts/logistica/src_traspaso_simple.js?v=1_0"></script>