<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_ruta = intval($_POST['id']);

$data = $db->select('tb_ruta','dq_ruta',"dc_ruta = {$dc_ruta}");

if(!count($data)){
	$error_man->showWarning('El número de ruta especificado es inválido');
	exit;
}

$dq_ruta = $data[0]['dq_ruta'];
unset($data);

echo('<div class="secc_bar">Anular Ruta</div><div class="panes">');

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/logistica/proc/null_ruta.php','null_ruta_form');
$form->Header('Indique las razones por las que se anulará la ruta');
$error_man->showAviso("Atención, está a punto de anular la ruta N° <b>{$dq_ruta}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion',array('dc_motivo','dg_motivo'),1);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('id_ruta',$dc_ruta);
$form->End('Anular','delbtn');
?>