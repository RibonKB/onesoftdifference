<?php
define("MAIN",1);
require_once("../../inc/init.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo_movimiento = intval($_POST['id']);

$datos = $db->prepare($db->select('tb_tipo_movimiento_logistico',
	'dc_tipo_movimiento ,dg_tipo_movimiento, dg_codigo,
	 dm_genera_asiento,dc_tipo_movimiento_contable','dc_tipo_movimiento = ? AND dc_empresa = ?'));
$datos->bindValue(1,$dc_tipo_movimiento,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('El tipo de movimiento no fue encontrado');
	exit;
}
?>

<div class="secc_bar">  
	Editar tipo de movimiento
</div>
<div class="panes">
<?php $form->Start('sites/logistica/proc/editar_tipo_movimiento.php', 'ed_tipo_movimiento') ?>
<?php $form->Header('<b>Ingrese el nombre para actualizar el tipo de movimiento</b><br />Los campos marcados con [*] son obligatorios') ?>

		<?php $form->Section(); 
         	$form->Text('Nombre','dg_tipo_movimiento_ed',true,Form::DEFAULT_TEXT_LENGTH,$datos->dg_tipo_movimiento);    
         	$form->Text('Código','dg_codigo_ed',true,Form::DEFAULT_TEXT_LENGTH,$datos->dg_codigo);    
		$form->EndSection();
		
		$form->Section();
			$form->Radiobox('Genera asientos contables','dm_genera_asiento_ed',array('SI','NO'),$datos->dm_genera_asiento==1?0:1);
		$form->EndSection();
		
		?>
        
             
        <?php $form->Hidden('dc_tipo_movimiento_ed',$dc_tipo_movimiento) ?>
    <?php $form->End('Editar','editbtn') ?>
    