<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Bodegas</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear una nueva bodega<br />Puede consultar las que ya están agregadas.");
	$form->Start("sites/logistica/proc/crear_bodega.php","cr_bodega");
	$form->Section();
	$form->Text("Nombre","bodega_name",1);
	$form->Radiobox('Tipo','bodega_tipo',array('Física','Virtual'),0);
	$form->EndSection();
	$form->Section();
	$form->Radiobox('Bodega en transito','bodega_transito',array('NO','SI'),0);
	$form->Radiobox('Maneja stock crítico','bodega_critico',array('NO','SI'),0);
	$form->EndSection();
	$form->End("Crear","addbtn");
	
	$list = $db->select("tb_bodega",'dc_bodega,dg_bodega,DATE_FORMAT(df_creacion,"%d/%m/%Y %H:%i") AS df_fecha',"dc_empresa={$empresa} AND dm_activo='1'");
	
	echo('<table class="tab" width="100%">
	<caption>Bodegas ya agregadas</caption>
	<thead>
		<tr>
			<th width="40%">Bodega</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("<tr id='item{$l['dc_bodega']}'>
			<td>{$l['dg_bodega']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/logistica/ed_bodega.php?id={$l['dc_bodega']}' class='loadOnOverlay left'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/logistica/del_bodega.php?id={$l['dc_bodega']}' class='loadOnOverlay left'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>