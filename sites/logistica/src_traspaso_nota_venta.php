<?php
/*
*	Traspaso de stock entre notas de venta rama aferente
*	
*	Proceso: -> elegir producto -> Mostrar stock por bodega -> Cargar detalles de nota de venta -> elegir nota venta destino -> mostrar detalles nota venta para producto
*	-> Selección de cantidades -> 
*/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Traspaso stock reservado nota de venta</div><div id="main_cont"><div class="panes">');

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/logistica/proc/set_traspaso_simple.php','set_traspaso','logistica_form');
$form->Header('<b>Indique los datos necesarios para realizar un traspaso simple (de stock libre) entre bodegas</b><br />
Los campos marcados con [*] son obligatorios');

?>
</div></div>