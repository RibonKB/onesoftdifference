<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_bodega",
"dg_bodega,dm_tipo,dm_transito,dm_maneja_critico",
"dc_bodega = {$_POST['id']} AND dc_empresa = {$empresa}"
);
if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la bodega especificada");
	exit();
}
$datos = $datos[0];
?>
<div class="secc_bar">Edición Bodegas</div>
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Indique los datos actualizados de la bodega");
	$form->Start("sites/logistica/proc/editar_bodega.php","ed_bodega");
	$form->Section();
	$form->Text("Nombre","ed_bodega_name",1,255,$datos['dg_bodega']);
	$form->Radiobox('Tipo','ed_bodega_tipo',array('Física','Virtual'),$datos['dm_tipo']);
	$form->EndSection();
	$form->Section();
	$form->Radiobox('Bodega en transito','ed_bodega_transito',array('NO','SI'),$datos['dm_transito']);
	$form->Radiobox('Maneja stock crítico','ed_bodega_critico',array('NO','SI'),$datos['dm_maneja_critico']);
	$form->EndSection();
	$form->Hidden('ed_bodega_id',$_POST['id']);
	$form->End("Editar","editbtn");
?>
</div>