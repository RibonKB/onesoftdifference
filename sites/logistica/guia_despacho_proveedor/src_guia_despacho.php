<?php
define("MAIN",1);
require_once("../../../inc/init.php");

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">Guía de Despacho proveedor</div>
<div id="main_cont"><br /><br />
	<div class="panes">
    	<?php
			$form->Start("sites/logistica/guia_despacho_proveedor/proc/src_guia_despacho.php","src_guia_despacho");
			$form->Header("<strong>Indicar los parámetros de búsqueda de la guía de despacho</strong>");
		?>
        <table class="tab" width="100%">
        	<tbody>
            	<tr>
                	<td width="50">Número de guía de despacho</td>
                    <td width="280"><?php $form->Text("Desde","dq_guia_despacho_desde") ?></td>
                    <td width="280"><?php $form->Text('Hasta','dq_guia_despacho_hasta') ?></td>
                </tr>
                <tr>
                	<td>Fecha emisión</td>
                    <td><?php $form->Date('Desde','df_emision_desde',1,"01/".date("m/Y")) ?></td>
                    <td><?php $form->Date('Hasta','df_emision_hasta',1,0) ?></td>
                </tr>
                <tr>
                	<td>Proveedor</td>
                    <td><?php $form->DBMultiSelect('','dc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon')) ?></td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <?php $form->End('Ejecutar consulta','searchbtn') ?>
    </div>
</div>
<script type="text/javascript">
$('#dc_proveedor').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>