<?php
define("MAIN",1);
include_once("../../../inc/init.php");

$db->doQuery("SET NAMES 'latin1'");

include("template_guia_despacho/modo{$empresa}.php");

$datosGuia = $db->prepare($db->select('tb_guia_despacho_proveedor gd
	JOIN tb_proveedor pr ON pr.dc_proveedor = gd.dc_proveedor
	JOIN tb_contacto_proveedor c ON c.dc_contacto_proveedor = gd.dc_contacto
		JOIN tb_comuna com1 ON com1.dc_comuna = c.dc_comuna
		JOIN tb_region r1 ON r1.dc_region = com1.dc_region
	JOIN tb_contacto_proveedor ce ON ce.dc_contacto_proveedor = gd.dc_contacto_entrega
		JOIN tb_comuna com2 ON com2.dc_comuna = ce.dc_comuna
		JOIN tb_region r2 ON r2.dc_region = com2.dc_region',
'gd.dq_folio,gd.dg_comentario, UNIX_TIMESTAMP(gd.df_emision) AS df_emision ,gd.dq_neto, gd.dq_iva, gd.dq_total,
 c.dg_contacto, com1.dg_comuna, r1.dg_region, ce.dg_contacto dg_contacto_entrega, com2.dg_comuna dg_comuna_entrega, r2.dg_region dg_region_entrega,
 c.dg_direccion, ce.dg_direccion dg_direccion_entrega, pr.dg_razon, pr.dg_rut, c.dg_codigo_postal, c.dg_telefono',
'gd.dc_guia_despacho = ? AND gd.dc_empresa = ?'));
$datosGuia->bindValue(1,$_GET['id'],PDO::PARAM_INT);
$datosGuia->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datosGuia);

$datosGuia = $datosGuia->fetch(PDO::FETCH_OBJ);

if($datosGuia === false){
	$error_man->showWarning('La guía de despacho indicada no es válida');
	exit;
}

$datosGuia->df_emision = strftime("%d de %B de %Y",$datosGuia->df_emision);

$detalle = $db->prepare($db->select('tb_guia_despacho_proveedor_detalle','dq_cantidad,dg_producto,dg_descripcion,dg_serie,dq_precio','dc_guia_despacho = ?'));
$detalle->bindValue(1,$_GET['id'],PDO::PARAM_INT);
$db->stExec($detalle);

$pdf = new PDF();
$pdf->AddDetalle($detalle);
$pdf->Output();

?>