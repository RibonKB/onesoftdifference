<?php
require("../../../inc/fpdf.php");

class PDF extends FPDF{
	
	private $margenY = 1.0;
	
	public function __construct(){
		parent::__construct('P','cm',array(21.5,28.2));
	}
	
	public function Header(){
		global $datosGuia;
		
		$this->SetFont('Arial','',12);
		$this->SetTextColor(0,0,0);
		
		$this->SetXY(12.5,$this->margenY);//4.2);
		$this->Cell(8,0.5,$datosGuia->dq_folio,0,0,'C');
		
		$this->SetFont('Arial','',9);
		$this->SetXY(3,$this->margenY+2.2);
		$this->MultiCell(10,0.44,
		"{$datosGuia->dg_razon}\n{$datosGuia->dg_direccion}\n{$datosGuia->dg_region}\n{$datosGuia->dg_codigo_postal}\n{$datosGuia->dg_rut}\n".
		"{$datosGuia->dg_direccion_entrega} {$datosGuia->dg_comuna_entrega} {$datosGuia->dg_region_entrega}\n{$datosGuia->dg_contacto_entrega}");
		
		$this->setXY(9,3.5+$this->margenY);
		$this->MultiCell(4,0.44,"{$datosGuia->dg_comuna}\n{$datosGuia->dg_telefono}");
		
		$this->SetXY(16.5,4.5+$this->margenY);
		$this->MultiCell(4,0.5,"{$datosGuia->df_emision}");
	}
	
	public function AddDetalle($detalle){
		global $empresa_conf, $datosGuia;
	
		$this->AddPage();
		$this->SetFont('Arial','',9);
		$this->SetFillColor(255,255,255);
		
		$this->SetY(7.9+$this->margenY);
		foreach($detalle as $det){
			$det['dq_total'] = moneda_local($det['dq_precio']*$det['dq_cantidad']);
			$det['dq_precio'] = moneda_local($det['dq_precio']);
			$this->SetX(1.7);
			$this->Cell(1,0.476,$det['dq_cantidad']);
			$code_large = $this->GetStringWidth($det['dg_producto']);
			if($code_large > 2.3){
				$desc_margin = $code_large-2.3;
			}else{
				$desc_margin = 0;
			}
			$this->SetFont('','B');
			$this->Cell(2.5+$desc_margin,0.476,$det['dg_producto']);
			$this->SetFont('','');
			$this->Cell(10-$desc_margin,0.476,substr($det['dg_descripcion'],0,49).'.');
			$y = $this->GetY();
			$x = $this->GetX()+3.3;
			
			$this->Cell(2.5,0.476,$det['dg_serie'],0,2);
			
			$y2 = $this->GetY();
			
			$this->SetY($y);
			$this->SetX($x);
			
			$this->Cell(1.8,0.476,$det['dq_precio'],0,0,'R');
			$this->SetY($y2);
			
		}
		
		$this->Ln(2);
		$this->SetX(4);
		$this->MultiCell(15,0.5,$datosGuia->dg_comentario);
		
		
	}
	
}
?>