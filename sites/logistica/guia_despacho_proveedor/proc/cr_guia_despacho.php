<?php
define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->escape($_POST['dg_rut']);
$datos = $db->select("tb_proveedor","dc_proveedor,dg_razon,dc_contacto_default",
"dg_rut = '{$_POST['dg_rut']}' AND dc_empresa={$empresa} AND dm_activo = '1'");

$datos = array_shift($datos);
	
if($datos == NULL){
	$error_man->showErrorRedirect("No se encontró el proveedor especificado, intentelo nuevamente.","sites/logistica/guia_despacho_proveedor/cr_guia_despacho.php");
}

$bodegas = $db->select('tb_bodega','dc_bodega,dg_bodega',"dc_empresa={$empresa} AND dm_activo='1'");

$bodega_default = array();
$tipo_doc_mov = array();
foreach($db->select('tb_tipo_guia_despacho','dc_tipo,dc_bodega,dc_bodega_entrada,dc_tipo_movimiento,dm_facturable',"dc_empresa={$empresa} AND dm_activo='1'") as $t){
	$bodega_salida[$t['dc_tipo']] = $t['dc_bodega'];
	$bodega_entrada[$t['dc_tipo']] = $t['dc_bodega_entrada'];
	$tipo_doc_mov[$t['dc_tipo']] = $t['dc_tipo_movimiento'];
	$tipo_doc_fac[$t['dc_tipo']] = $t['dm_facturable'];
}

$list_seg = $db->select("tb_tipo_cambio","dc_tipo_cambio,dq_cambio","dc_empresa={$empresa} AND dm_activo='1'");

$cambio_list = array();

$num_guia = $db->select('tb_guia_despacho_proveedor','MAX(dq_guia_despacho)+1 AS number',"dc_empresa={$empresa}");
$num_guia = $num_guia[0]['number'];

include_once("../../../../inc/form-class.php");
$form = new Form($empresa);


?>

<div id='secc_bar'>
	Guía de Despacho Proveedor
</div>
<div id='main_cont'>
<div class='panes' style='width:1140px;'>
<div class='title center'>
Emitiendo Guía de despacho para <strong id='cli_razon' style='color:#000;'><?php echo $datos['dg_razon'] ?></strong>
</div>
	<?php
		foreach($list_seg as $c):
			$form->Hidden("cambio{$c['dc_tipo_cambio']}",$c['dq_cambio']);
		endforeach;
	?>
	
	<?php $form->Start("sites/logistica/guia_despacho_proveedor/proc/crear_guia_despacho.php","cr_guia_despacho",'ventas_form') ?>
	<?php $form->Header("<strong>Indique los datos para la generación de la guía de despacho</strong><br />Los datos marcados con [*] son obligatorios") ?>
	
	<div style="width:580px;float:left;">
	<?php 
		$form->Section();
	
		$form->Text("Número de guia","gd_number",1,20,$num_guia);
		
		$form->Listado('Tipo de guía de despacho','dc_tipo_guia','tb_tipo_guia_despacho_proveedor',array('dc_tipo','dg_tipo'),1);
		
		//Contacto principal de la factura, aparecerá en la cabecera como principal contacto del cliente
		$form->Listado('Contacto','gd_contacto',
		"(SELECT * FROM tb_contacto_proveedor WHERE dc_proveedor = {$datos['dc_proveedor']}) c",
		array('c.dc_contacto_proveedor','c.dg_contacto'),1,$datos['dc_contacto_default'],'');
		
		//Dirección a la que se despachará el pedido
		$form->Listado('Contacto de entrega','gd_cont_entrega',
		"(SELECT * FROM tb_contacto_proveedor WHERE dc_proveedor = {$datos['dc_proveedor']}) c",
		array('c.dc_contacto_proveedor','c.dg_contacto'),1,$datos['dc_contacto_default'],'');
	
	
	$form->EndSection();
	$form->Section();
	
		$form->Text('Guía referencia','gd_guia_referencia');
		$form->Text('Orden de compra cliente','gd_orden_compra');
		$form->Text('Factura de compra','dq_factura');
		$form->Hidden('dc_factura',0);
	
	$form->EndSection();
	
	?>
	
	<br class="clear" id="after_header">
	</div>
	
<?php
	$form->Section();
		$form->Date('Fecha Emisión','gd_emision',1,0);
		$form->Textarea('Comentario','gd_comentario');
		echo('<div id="factura_anticipada_container"></div>');
	$form->EndSection();
?>

	<hr class='clear' />
	
	<div id='prods'>
	<div class='info'>Detalle (Valores en <b><?php echo $empresa_conf['dg_moneda_local'] ?></b>)</div>
	<table width='100%' class='tab'>
	<thead>
	<tr>
		<th width='80'>Opciones</th>
		<th width='95'>Código</th>
		<th>Descripción</th>
		<th>Bodega Salida</th>
		<th width='63'>Cantidad</th>
		<th width='82'>Precio</th>
		<th width='82'>Total</th>
		<th width='82'>Costo</th>
		<th width='82'>Costo total</th>
		<th width='82'>Margen</th>
		<th width='80'>Margen (%)</th>
	</tr>
	</thead>
	<tbody id='prod_list'></tbody>
	<tfoot>
	<tr>
		<th colspan='5' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
		<th align='right' id='total_margen'>0</th>
		<th align='center' id='total_margen_p'>0</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total Neto</th>
		<th align='right' colspan='2' id='total_neto'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>IVA</th>
		<th align='right' colspan='2' id='total_iva'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	<tr>
		<th align='right' colspan='5'>Total a pagar</th>
		<th align='right' colspan='2' id='total_pagar'>0</th>
		<th colspan='4'>&nbsp;</th>
	</tr>
	</tfoot>
	</table>
	<div class='center'>
		<input type='button' class='addbtn' id='prod_add' value='Agregar otro producto' />
	</div></div>
	
	<?php
	$form->Hidden('dc_proveedor',$datos['dc_proveedor']);
	$form->Hidden('gd_tipo_mov',0);
	$form->Hidden('cot_iva',0);
	$form->Hidden('cot_neto',0);
	$form->Hidden('facturable',0);
	$form->End('Emitir','addbtn');
	?>
	
	<table id='prods_form' style='display:none;'>
	<tr class='main'>
		<td align='center'>
			<img src='images/delbtn.png' alt='' title='Eliminar detalle' class='del_detail' />
			<img src='images/descbtn.png' alt='' title='Restaurar Descripción' class='get_description' />
			<img src='images/doc.png' alt='' title='Asignar Series' class='set_series' />
			<input type='hidden' class='prod_series' name='serie[]' value=''>
		</td>
		<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='9' /></td>
		<td><input type='text' name='desc[]' class='prod_desc inputtext' size='25' required='required' /></td>
		<td>
			<select name='bodega_salida[]' style='width:110px;' class='prod_bodega_salida inputtext' required='required'>
			<?php foreach($bodegas as $b): ?>
				<option value="<?php echo $b['dc_bodega'] ?>"><?php echo $b['dg_bodega'] ?></option>
			<?php endforeach; ?>
			</select>
		</td>
		<td>
			<input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' />
			<input type='hidden' name='despachada[]' value='0' class='despach_cant' />
			<input type='hidden' name='recepcionada[]' value='0' class='recep_cant' />
		</td>
		<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='total' align='right'>0</td>

		<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
		<td class='costo_total' align='right'>0</td>
		<td class='margen' align='right'>0</td>
		<td align='center'>
			<input type='text' class='margen_p inputtext' size='3' required='required' />%
		</td>
	</tr>
	</table>

</div></div>
<script type="text/javascript">
	var empresa_iva = <?php echo $empresa_conf['dq_iva'] ?>;
	var empresa_dec = <?php echo $empresa_conf['dn_decimales_local'] ?>;
	
	var out_bod = <?php echo json_encode($bodega_salida) ?>;
	var in_bod = <?php echo json_encode($bodega_entrada) ?>;
	var tipo_mov = <?php echo json_encode($tipo_doc_mov) ?>;
	var facturable = <?php echo json_encode($tipo_doc_fac) ?>;
	
	var anticipa = <?php echo check_permiso(28)?1:0 ?>;

</script>
<script type="text/javascript" src="jscripts/product_manager/guia_despacho.js?v2_10b"></script>
<script type="text/javascript" src="jscripts/sites/logistica/cr_guia_despacho_proveedor.js?v=0_1a"></script>
<script type="text/javascript">
	js_data.dc_proveedor = <?php echo $datos['dc_proveedor'] ?>;
	js_data.init();
</script>