<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$dc_factura = intval($_GET['dc_factura']);

//Obtener detalles
$detalle = $db->prepare(
				$db->select(
					'tb_factura_compra_detalle d
					 JOIN tb_producto p ON p.dc_producto = d.dc_producto',
					'p.dg_codigo, p.dg_producto, d.dc_detalle, d.dq_precio, p.dm_requiere_serie',
					'd.dc_factura = ?'
				));
$detalle->bindValue(1,$dc_factura,PDO::PARAM_INT);
$db->stExec($detalle);
$detalle = $detalle->fetchAll();

echo json_encode($detalle);
?>