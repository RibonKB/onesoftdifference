<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$data = $db->prepare($db->select('tb_guia_despacho_proveedor gd
	JOIN tb_proveedor pr ON pr.dc_proveedor = gd.dc_proveedor
	JOIN tb_contacto_proveedor c ON c.dc_contacto_proveedor = gd.dc_contacto
		JOIN tb_comuna com1 ON com1.dc_comuna = c.dc_comuna
		JOIN tb_region r1 ON r1.dc_region = com1.dc_region
	JOIN tb_contacto_proveedor ce ON ce.dc_contacto_proveedor = gd.dc_contacto_entrega
		JOIN tb_comuna com2 ON com2.dc_comuna = ce.dc_comuna
		JOIN tb_region r2 ON r2.dc_region = com2.dc_region',
'gd.dq_folio,gd.dq_guia_despacho,gd.dg_comentario, UNIX_TIMESTAMP(gd.df_emision) AS df_emision ,gd.dq_neto, gd.dq_iva, gd.dq_total, gd.dm_nula,
 c.dg_contacto, com1.dg_comuna, r1.dg_region, ce.dg_contacto dg_contacto_entrega, com2.dg_comuna dg_comuna_entrega, r2.dg_region dg_region_entrega,
 c.dg_direccion, ce.dg_direccion dg_direccion_entrega, pr.dg_razon, pr.dg_rut, c.dg_codigo_postal, c.dg_telefono, gd.dc_guia_despacho',
'gd.dc_guia_despacho = ? AND gd.dc_empresa = ?'));
$data->bindValue(1,$_POST['id'],PDO::PARAM_INT);
$data->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($data);

$data = $data->fetch(PDO::FETCH_OBJ);

if($data === false){
	$error_man->showWarning("No se ha encontrado la guia de despacho especificada");
	exit;
}

$detalle = $db->prepare($db->select("tb_guia_despacho_proveedor_detalle",
	'dg_producto,dg_descripcion,dg_serie,dq_precio,dq_costo,dq_cantidad',
"dc_guia_despacho=?"));
$detalle->bindValue(1,$_POST['id'],PDO::PARAM_INT);
$db->stExec($detalle);

?>
<div class='title center'>Guía de Despacho Nº <?php echo $data->dq_guia_despacho ?></div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong>(<?php echo $data->dg_rut ?>) <?php echo $data->dg_razon ?></strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td><?php echo $data->df_emision ?></td>
</tr><tr>
	<td>Domicilio proveedor</td>
	<td><b><?php echo $data->dg_direccion ?></b> <?php echo $data->dg_comuna ?> <label><?php echo $data->dg_region ?></label></td>
</tr><tr>
	<td>Domicilio entrega</td>
	<td><b><?php echo $data->dg_direccion_entrega ?></b> <?php echo $data->dg_comuna_entrega ?> <label><?php echo $data->dg_region_entrega ?></label></td>
</tr><tr>
	<td>Comentario</td>
	<td><?php echo $data->dg_comentario ?></td>
</tr></table>

<?php
if($data->dm_nula == 1):
	$error_man->showAviso('GUÍA DE DESPACHO NULA');
endif;
?>

<table width='100%' class='tab'>
<caption>Detalle</caption>
<thead>
<tr>
	<th width='60'>Código</th>
	<th width='60'>Cantidad</th>
	<th>Descripción</th>
	<th width='100'>Precio</th>
	<th width='100'>Costo</th>
	<th width='100'>Total</th>
</tr>
</thead>
<tbody>

<?php foreach($detalle as $i => $d): ?>
	<tr>
		<td><?php echo $d['dg_producto'] ?></td>
		<td><?php echo $d['dq_cantidad'] ?></td>
		<td align='left'><?php echo $d['dg_descripcion'] ?></td>
		<td align='right'><?php echo moneda_local($d['dq_precio']) ?></td>
		<td align='right'><?php echo moneda_local($d['dq_costo']) ?></td>
		<td align='right'><?php echo moneda_local($d['dq_precio']*$d['dq_cantidad']) ?></td>
	</tr>
<?php endforeach ?>

</tbody>
<tfoot>
<tr>
	<th colspan='5' align='right'>Total Neto</th>
	<th align='right'><?php echo moneda_local($data->dq_neto) ?></th>
</tr>
<tr>
	<th colspan='5' align='right'>IVA</th>
	<th align='right'><?php echo moneda_local($data->dq_iva) ?></th>
</tr>
<tr>
	<th colspan='5' align='right'>Total a Pagar</th>
	<th align='right'><?php echo moneda_local($data->dq_total) ?></th>
</tr>
</tfoot></table>
<script type="text/javascript">
	$('#print_version').click(function(){
		window.open('sites/logistica/guia_despacho_proveedor/ver_guia_despacho.php?id=<?php echo $data->dc_guia_despacho ?>','ver_guia_despacho');
	});
</script>