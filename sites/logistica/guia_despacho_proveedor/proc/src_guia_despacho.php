<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$df_emision_desde = $db->sqlDate($_POST['df_emision_desde']);
$df_emision_hasta = $db->sqlDate($_POST['df_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$df_emision_desde} AND {$df_emision_hasta})";

if($_POST['dq_guia_despacho_desde']){
	if($_POST['dq_guia_despacho_hasta']){
		$conditions .= " AND (dq_guia_despacho BETWEEN {$_POST['dq_guia_despacho_desde']} AND {$_POST['dq_guia_despacho_hasta']})";
	}else{
		$conditions = "dq_guia_despacho = {$_POST['dq_guia_despacho_desde']} AND dc_empresa = {$empresa}";
	}
}

if(isset($_POST['dc_proveedor'])){
	$_POST['dc_proveedor'] = implode(',',$_POST['dc_proveedor']);
	$conditions .= " AND dc_proveedor IN ({$_POST['dc_proveedor']})";
}

$data = $db->doQuery(
			$db->select('tb_guia_despacho_proveedor','dc_guia_despacho,dq_guia_despacho,dq_folio',$conditions,array('order_by' => 'df_emision DESC'))
		)->fetchAll(PDO::FETCH_OBJ);

if(!count($data)){
	$error_man->showAviso('No se encontraron guías de despacho con los criterios especificados');
	exit;
}

?>
<div id='show_guia_despacho'></div>

<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Guías de Despacho<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Guía</th>
	</tr>
</thead>
<tbody>

<?php foreach($data as $c): ?>
	<?php 
		if($c->dq_folio == 0){
			$c->dq_folio = '[V]';
		}
	?>
	
	<tr>
		<td align='left'>
			<a href='sites/logistica/guia_despacho_proveedor/proc/show_guia_despacho.php?id=<?php echo $c->dc_guia_despacho ?>' class='gd_load'>
                <img src='images/doc.png' alt='' style='vertical-align:middle;' />
				<?php echo $c->dq_guia_despacho ?> -
                <b><?php echo $c->dq_folio ?></b>
            </a>
		</td>
	</tr>
<?php endforeach ?>

</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='imgbtn' id='gestion_guia' style='background-image:url(images/management.png);'>Gestión</button>
<button type='button' class='editbtn' id='edit_folio'>Editar Folio</button>
<button type='button' class='delbtn' id='null_guia'>Anular</button>
</div>

<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".gd_load").click(function(e){
		e.preventDefault();
		$('#show_factura').html("<img src='images/ajax-loader.gif' alt='' /> cargando guía de despacho ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_guia_despacho');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>