<?php
define("MAIN",1);
require_once("../../../../inc/global.php");

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$db->escape($_GET['number']);

switch($_GET['mode']){
	case 'gd': get_guia_despacho_data($_GET['number']); break;
	default: echo json_encode('<not-found>');
}


function get_guia_despacho_data($number){
	global $db,$empresa;
	
	$data = $db->select("tb_guia_despacho_proveedor",'dc_guia_despacho',"dc_empresa={$empresa} AND dq_guia_despacho={$number}");
	if(!count($data)){
		echo json_encode('<not-found>');
		exit;
	}
	$data = $data[0];
	
	
	
	echo json_encode($data);
}
?> 