<?php
/**
*	Rama aferente proceso de devolución de productos despachados.
*
*	Procesos:
*		- Consultar guía
*		- Obtener detalle guía de despacho
*		- Ingresar detalles de cantidad y bodega de entrada
**/

//Dependencias
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Devolución de guías de despacho</div><div id="main_cont"><div class="panes">');

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/logistica/proc/crear_devolucion.php','cr_devolucion');
$form->Header('<b>Indique los datos para crear la devolución de productos</b><br />Los campos marcados con [*] son obligatorios');

/**
*	Consultar guía
*	
*	Campo que estará en espera del número de guía de despacho que se hará devolución
*	
*	IN: dq_guia_despacho
*	OUT: dc_guia_despacho, dc_orden_servicio, dc_nota_venta
**/

//CODE-HERE
$form->Section(); //BEG-Section 1
$form->Text('Guía de despacho','dq_guia_despacho',1);


/**
*	Obtener información de cabecera para la devolución
*	
*	Al seleccionar la guía de despacho traerá con ella información como el número de orden de servicio o nota de venta asignada.
*	Otra información es necesaria para la creación del comprobante de devolución como un comentario (razón de devolución)
*	
*	IN: dc_nota_venta, dc_orden_servicio
*	OUT: dc_nota_venta, dc_orden_servicio, dg_comentario
**/

//CODE-HERE
$form->Text('Nota de venta','dq_nota_venta');
$form->Text('Orden de servicio','dq_orden_servicio');
$form->EndSection();//END-Section 1
$form->Section();
$form->Textarea('Comentario','dg_comentario',1);
$form->Date('Fecha de devolución','df_fecha_emision',true,0);
$form->EndSection();
$form->Group();

/**
*	Obtener detalle guía de despacho, Ingresar detalles de cantidad y bodega de entrada
*	
*	Contenedor de los detalles que se devolverán de la guía de despacho
*	las cantidades máximas permitidas son las cantidades totales despachadas originalmente menos las ya devueltas
*	Esto debido a que los detalles de la guía de despacho no reciben ninguna modificación a la hora de ser devueltas
*	
*	IN: dc_guia_despacho
*	OUT: dc_detalle_guia_despacho[], dc_detalle_nota_venta[], dc_detalle_orden_servicio[], dc_cantidad[], dg_descripcion[], bodega_entrada[], bodega_salida[]
**/

//CODE-HERE
$form->Header('Seleccione una guía de despacho a la que se hará la devolución y los detalles serán cargados acá','id="detail_info"');
echo('<table class="hidden tab" width="100%" id="prod_table">
<caption>Detalles a devolver</caption>
<thead><tr>
	<th width="10"><input type="checkbox" id="select_all_detail" /></th>
	<th width="300">Producto</th>
	<th width="120">Serie</th>
	<th>Descripción</th>
	<th width="120">Cantidad</th>
	<th width="230">Bodega de entrada</th>
</tr></thead><tbody id="prod_list"></tbody></table>');

$form->Group();

$form->Hidden('dev_traslado',0);
$form->End('Crear','addbtn');
echo('</div></div>');

/**
*	Dependencias funcionales y helpers
*	
*	Listado:
*		- Template de detalle
**/
$bodegas = $db->select('tb_bodega','dc_bodega,dg_bodega',"dc_empresa = {$empresa} AND dm_activo = 1");

?>
<table class="hidden"><tbody id="det_template"><tr class="main">
	<td>
		<input type="checkbox" class="selected_item" />
	</td>
	<td class="det_name">
		<input type="hidden" disabled="disabled" class="det_gd_detail"	name="gd_detail_id[]" />
		<input type="hidden" disabled="disabled" class="det_prod_id"	name="prods[]" />
		<input type="hidden" disabled="disabled" class="det_doc_detail"	name="doc_detail_id[]" />
		<input type="hidden" disabled="disabled" class="prod_serie" 	name="prod_serie[]" />
	</td>
	<td>
		<b class="det_serie"></b>
	</td>
	<td>
		<input type="text" name="dg_descripcion[]" class="det_descripcion inputtext" style="width:90%" disabled="disabled" />
	</td>
	<td>
		<input type="text" name="dq_cantidad[]" class="det_cantidad inputtext" size="10" disabled="disabled" />
		<input type="hidden" class="det_max_value" value="0" disabled="disabled" />
	</td>
	<td>
		<select name="dc_bodega_entrada[]" class="det_bodega_entrada inputtext" disabled="disabled">
			<?php foreach($bodegas as $bodega): ?>
				<option value="<?php echo($bodega['dc_bodega']) ?>"><?php echo($bodega['dg_bodega']) ?></option>
			<?php endforeach; ?>
		</select>
		<input type="hidden" class="det_bodega_entrada_bkp" disabled="disabled" />
		<input type="hidden" name="dc_bodega_salida[]" class="det_bodega_salida" disabled="disabled" />
	</td>
</tr></tbody></table>
<style type="text/css">
.dev_active_prod{
	background:#9AEE4F !important;
}
</style>
<script type="text/javascript" src="jscripts/logistica/cr_devolucion.js?v=0_6"></script>