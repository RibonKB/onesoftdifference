<?php
define("MAIN",1);
require_once("../../inc/global.php");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">
	Liberación stock Nota de Venta
</div>
<div id="main_cont">
	<div class="panes">
	<?php
		$form->Start('sites/logistica/proc/crear_liberar_nota_venta.php','liberar_nota_venta');
		$form->Header('<b>Seleccione una nota de venta para hacer la liberación de stock</b><br />Los datos marcados con [*] son obligatorios');
			$form->Section();
				$form->Text('Nota de Venta','dq_nota_venta',1);
				$form->Hidden('dc_nota_venta',0);
			$form->EndSection();
			$form->Section();
				$form->Listado('Bodega de liberación','dc_bodega','tb_bodega',array('dc_bodega','dg_bodega'),1);
			$form->EndSection();
			$form->Group('id="nv_data"');
				$form->Header('Para continuar seleccione una nota de venta para cargar el stock disponible para liberación');
			$form->Group();
		$form->End('Liberar');
	?>
	</div>
</div>
<script type="text/javascript" src="jscripts/sites/logistica/src_liberar_nota_venta.js"></script>