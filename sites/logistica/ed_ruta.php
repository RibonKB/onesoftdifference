<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_ruta = intval($_POST['id']);

$data = $db->select('tb_ruta','dq_ruta, df_ruta, dm_nula, dc_tipo_ruta',"dc_ruta = {$dc_ruta}");

if(!count($data)){
	$error_man->showWarning("No se encontró la Ruta especificada intentelo denuevo.");
	exit;
}
$data = $data[0];

if($data['dm_nula'] == 1){
	$error_man->showWarning("La Ruta especificada ha sido anulada, no puede editarse.");
	exit;
}

$detalle = $db->select('tb_ruta_detalle d
JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
JOIN tb_producto p ON p.dc_producto = nvd.dc_producto
JOIN tb_nota_venta nv ON nv.dc_nota_venta = nvd.dc_nota_venta
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente',
'd.dc_detalle, d.dc_detalle_nota_venta, d.dq_cantidad, d.dt_hora, nv.dq_nota_venta, cl.dg_razon,
p.dg_codigo, nvd.dg_descripcion, (nvd.dq_cantidad - nvd.dc_rutada + d.dq_cantidad) dq_cantidad_maxima',
"d.dc_ruta = {$dc_ruta} AND d.dm_activo = 1");

?>
<div class="secc_bar">Edición ruta</div>
<div class="panes">
<?php
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/logistica/proc/editar_ruta.php','ed_ruta');
$form->Header("<strong>Indique los cambios que hará en la ruta</strong><br />Los campos marcados con [*] son obligatorios");

$form->Section();
	$form->Date('Fecha de ruta','ruta_fecha',1,$data['df_ruta']);
$form->EndSection();

$form->Section();
	$form->Listado('Tipo de ruta','ruta_tipo','tb_tipo_ruta',array('dc_tipo','dg_tipo'),1,$data['dc_tipo_ruta']);
$form->EndSection();

$form->Group();

?>
<table class="tab" width="100%">
<thead>
	<tr>
		<th>¿Eliminar?</th>
		<th>Hora</th>
		<th>Cantidad</th>
		<th>Código</th>
		<th>Producto</th>
		<th>NV</th>
	</tr>
</thead>
<tbody>
<?php foreach($detalle as $d): ?>
	<tr>
		<td align="center">
			<input type="checkbox" name="dc_detalle_to_delete[]" value="<?php echo $d['dc_detalle'] ?>" class="del_item" />
			<input type="hidden" class="dc_detalle" name="dc_detalle[]" value="<?php echo $d['dc_detalle'] ?>" />
		</td>
		<td>
		<select name="dt_hora[]" class="inputtext dt_hora" style="width:70px;">
			<?php
			foreach(range(0,23) as $hora): 
				foreach(array(':00',':30') as $minutos):
					$dt_hora = str_pad($hora, 2, 0, STR_PAD_LEFT).$minutos;
			?>
			<option <?php if($dt_hora == substr($d['dt_hora'],0,5)): ?>selected="selected"<?php endif; ?> value="<?php echo $dt_hora ?>:00"><?php echo $dt_hora ?></option>
			<?php
				endforeach;
			endforeach;
			?>
		</select>
		</td>
		<td>
			<input type="text" value="<?php echo $d['dq_cantidad'] ?>" name="dq_cantidad[]" style="text-align:right;" class="inputtext dq_cantidad" size="10" />
			<input type="hidden" class="max_value" value="<?php echo $d['dq_cantidad_maxima'] ?>" />
			<input type="hidden" class="dq_cantidad_old" name="dq_cantidad_old[]" value="<?php echo $d['dq_cantidad'] ?>" />
			<input type="hidden" class="dc_detalle_nota_venta" name="dc_detalle_nota_venta[]" value="<?php echo $d['dc_detalle_nota_venta'] ?>" />
		</td>
		<td><b><?php echo $d['dg_codigo'] ?></b></td>
		<td><?php echo $d['dg_descripcion'] ?></td>
		<td><b><?php echo $d['dq_nota_venta'] ?></b></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<?php
$form->Group();
$form->Hidden('dc_ruta',$dc_ruta);
$form->End('Editar','editbtn');
?>
</div>
<script type="text/javascript" src="jscripts/sites/logistica/ed_ruta.js"></script>
<script type="text/javascript">
js_data.editInit();
</script>