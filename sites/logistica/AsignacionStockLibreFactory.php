<?php

require_once('inc/Autocompleter.class.php');

/**
 * Description of AsignacionStockLibreFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 21-08-2013
 */
class AsignacionStockLibreFactory extends Factory {

  protected $title = 'Asignación Stock Libre';
  private $orden_compra;
  private $dc_comprobante_asignacion;
  private $dq_comprobante_asignacion;

  public function indexAction() {
    $form = $this->getFormView($this->getTemplateURL('crear.form'));

    echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
  }

  public function acOrdenCompraAction() {
    $q = '%' . self::getRequest()->q . '%';
    $limit = self::getRequest()->limit;

    $db = $this->getConnection();

    $ordenes = $db->prepare(
            $db->select('tb_orden_compra oc
                      JOIN tb_proveedor p ON p.dc_proveedor = oc.dc_proveedor
                      LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = oc.dc_nota_venta', 'oc.dc_orden_compra, oc.dq_orden_compra, p.dg_razon, nv.dq_nota_venta', 'oc.dc_empresa = ? AND (oc.dq_orden_compra LIKE ? OR p.dg_razon LIKE ? OR p.dg_rut LIKE ?)', array('limit' => $limit)));
    $ordenes->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
    $ordenes->bindValue(2, $q, PDO::PARAM_STR);
    $ordenes->bindValue(3, $q, PDO::PARAM_STR);
    $ordenes->bindValue(4, $q, PDO::PARAM_STR);
    $db->stExec($ordenes);

    while ($oc = $ordenes->fetch(PDO::FETCH_OBJ)) {
      echo Autocompleter::getItemRow(array(
          $oc->dq_orden_compra,
          $oc->dg_razon,
          $oc->dq_nota_venta != null ? 'NV: ' . $oc->dq_nota_venta : '',
          $oc->dc_orden_compra
      ));
    }
  }

  public function getOCDetailDataResultAction() {
    $this->orden_compra = $this->getOrdenCompra(self::getRequest()->dc_orden_compra);
    $pendientes = $this->getDetallesPendientesRecepcion();

    if (!count($pendientes)):
      $this->getErrorMan()->showAviso('La orden de compra no posee detalles pendientes de recepción por lo que no es necesario asignar stock libre');
      exit;
    endif;

    $this->asignarDetalleStockLibre($pendientes);

    echo $this->getView($this->getTemplateURL('detallesOC'), array(
        'detalle' => $pendientes
    ));
  }

  public function procesarCrearAction() {
    $this->validarCrear();
    $this->validarStockBodega();

    $this->getConnection()->start_transaction();
      $this->ajustarDetallesOrdenCompra();
      $this->ajustarDetallesDocumentoRelacionado();
      $this->insertarComprobanteAsignacionStock();
      $this->reservarStockBodega();
      $this->ingresarMovimientosLogistica();
    $this->getConnection()->commit();

    $this->getErrorMan()->showConfirm('Se ha generado la reserva de stock correctamente');

  }

  public function asignarStockAction() {
    echo $this->getOverlayView($this->getTemplateURL('distribucionStock'), array(
        'dc_detalle' => self::getRequest()->dc_detalle,
        'detalle' => $this->getDistribucionStock()
    ));
  }

  private function getOrdenCompra($dc_orden_compra) {
    $oc = $this->getConnection()->getRowById('tb_orden_compra', $dc_orden_compra, 'dc_orden_compra');
    if ($oc === false || $oc->dc_empresa != $this->getEmpresa()):
      $this->getErrorMan()->showWarning('Orden de compra incorrecta');
    endif;
    return $oc;
  }

  private function getDetallesPendientesRecepcion() {
    $db = $this->getConnection();

    $data = $db->prepare($db->select('tb_orden_compra_detalle', '*', 'dc_orden_compra = ? AND dc_recepcionada < dq_cantidad'));
    $data->bindValue(1, $this->orden_compra->dc_orden_compra, PDO::PARAM_INT);
    $db->stExec($data);

    return $data->fetchAll(PDO::FETCH_OBJ);
  }

  private function asignarDetalleStockLibre(&$detalle) {
    $db = $this->getConnection();
    $stock = $db->prepare(
            $db->select('tb_stock s
                      JOIN tb_bodega b ON b.dc_bodega = s.dc_bodega', 'SUM(s.dq_stock-s.dq_stock_reservado-s.dq_stock_reservado_os) dq_stock_libre', 'b.dm_transito = 0 AND s.dc_producto = ?')
    );
    $stock->bindParam(1, $dc_producto, PDO::PARAM_INT);

    foreach ($detalle as &$d):
      $dc_producto = $d->dc_producto;
      $db->stExec($stock);
      $d->dc_stock_libre = intval($stock->fetchColumn());
    endforeach;
  }

  private function getDistribucionStock() {
    $r = self::getRequest();
    $db = $this->getConnection();

    $detalle = $db->getRowById('tb_orden_compra_detalle', $r->dc_detalle, 'dc_detalle');

    $stock = $db->prepare(
            $db->select(
                    'tb_stock s
                       JOIN tb_bodega b ON b.dc_bodega = s.dc_bodega', '(s.dq_stock-s.dq_stock_reservado-s.dq_stock_reservado_os) dq_stock_libre, b.dc_bodega, b.dg_bodega', 's.dc_producto = ? AND b.dm_transito = 0'));
    $stock->bindValue(1, $detalle->dc_producto, PDO::PARAM_INT);
    $db->stExec($stock);

    return $stock->fetchAll(PDO::FETCH_OBJ);
  }

  /**
   *
   * @return LogisticaStuffService
   */
  private function getLogisticaService() {
    return $this->getService('LogisticaStuff');
  }

  private function validarCrear() {
    $r = self::getRequest();

    if (!isset($r->dc_orden_compra) or !$r->dc_orden_compra):
      $this->getErrorMan()->showWarning('La orden de compra seleccionada no es válida, compruebe los datos de entrada y vuelva a intentarlo');
      exit;
    endif;

    if (!isset($r->dc_detalle) or !isset($r->dc_bodega) or !isset($r->dc_cantidad)):
      $this->getErrorMan()->showWarning('Debe seleccionar al menos un producto al que asignarle stock libre');
      exit;
    endif;

    $this->orden_compra = $this->getOrdenCompra($r->dc_orden_compra);

    if (!$this->orden_compra->dc_nota_venta and !$this->orden_compra->dc_orden_servicio):
      $this->getErrorMan()->showWarning('La orden de compra es inválida pues no ha sido relacionada a un documento de reserva de stock');
      exit;
    endif;
  }

  private function validarStockBodega() {
    $r = self::getRequest();
    $db = $this->getConnection();
    $service = $this->getLogisticaService();

    foreach ($r->dc_detalle as $i => $dc_detalle):
      $detalle = $db->getRowById('tb_orden_compra_detalle', $dc_detalle, 'dc_detalle');
      $dc_cantidad = $r->dc_cantidad[$i];
      $dc_bodega = $r->dc_bodega[$i];
      $stock = $service->comprobarTotalStock($detalle->dc_producto, $dc_bodega);
      if ($stock[0] < $dc_cantidad):
        $producto = $db->getRowById('tb_producto', $detalle->dc_producto, 'dc_producto');
        $bodega = $db->getRowById('tb_bodega', $dc_bodega, 'dc_bodega');
        $this->getErrorMan()->showWarning('No existe stock libre suficiente del producto <b>' . $producto->dg_producto . '</b> en la bodega <b>' . $bodega->dg_bodega . '</b>');
        exit;
      endif;
    endforeach;
  }

  private function ajustarDetallesOrdenCompra() {
    $r = self::getRequest();
    $db = $this->getConnection();

    $update = $db->prepare($db->update('tb_orden_compra_detalle', array(
                'dc_recepcionada' => 'dc_recepcionada+?'
                    ), 'dc_detalle = ?'));
    $update->bindParam(1, $dc_cantidad, PDO::PARAM_INT);
    $update->bindParam(2, $dc_detalle, PDO::PARAM_INT);

    foreach ($r->dc_detalle as $i => $dc_detalle):
      $dc_cantidad = $r->dc_cantidad[$i];
      $db->stExec($update);
    endforeach;
  }

  private function ajustarDetallesDocumentoRelacionado() {
    if ($this->orden_compra->dc_nota_venta):
      $this->ajustarDetallesNotaVenta();
    elseif ($this->orden_compra->dc_orden_servicio):
      $this->ajustarDetallesOrdenServicio();
    endif;
  }

  private function ajustarDetallesNotaVenta() {
    $r = self::getRequest();
    $db = $this->getConnection();

    $update = $db->prepare($db->update('tb_nota_venta_detalle', array(
                'dc_recepcionada' => 'dc_recepcionada+?'
                    ), 'dc_nota_venta_detalle = ?'));
    $update->bindParam(1, $dc_cantidad, PDO::PARAM_INT);
    $update->bindParam(2, $dc_detalle_nota_venta, PDO::PARAM_INT);

    foreach ($r->dc_detalle as $i => $dc_detalle):
      $detalle = $db->getRowById('tb_orden_compra_detalle', $dc_detalle, 'dc_detalle');
      $dc_detalle_nota_venta = $detalle->dc_detalle_nota_venta;
      $dc_cantidad = $r->dc_cantidad[$i];

      $db->stExec($update);
    endforeach;
  }

  private function ajustarDetallesOrdenServicio() {
    $r = self::getRequest();
    $db = $this->getConnection();

    $update = $db->prepare($db->update('tb_orden_servicio_factura_detalle', array(
                'dc_recepcionada' => 'dc_recepcionada+?'
                    ), 'dc_nota_venta_detalle = ?'));
    $update->bindParam(1, $dc_cantidad, PDO::PARAM_INT);
    $update->bindParam(2, $dc_detalle_orden_servicio, PDO::PARAM_INT);

    foreach ($r->dc_detalle as $i => $dc_detalle):
      $detalle = $db->getRowById('tb_orden_compra_detalle', $dc_detalle, 'dc_detalle');
      $dc_detalle_orden_servicio = $detalle->dc_detalle_orden_servicio;
      $dc_cantidad = $r->dc_cantidad[$i];

      $db->stExec($update);
    endforeach;
  }

  private function insertarComprobanteAsignacionStock() {
    $db = $this->getConnection();
    $insertar = $db->prepare($db->insert('tb_comprobante_asignacion_stock_libre', array(
                'dc_orden_compra' => '?',
                'dc_nota_venta' => '?',
                'dc_orden_servicio' => '?',
                'df_emision' => $db->getNow(),
                'dc_usuario_creacion' => $this->getUserData()->dc_usuario
    )));
    $insertar->bindValue(1, $this->orden_compra->dc_orden_compra, PDO::PARAM_INT);

    if ($this->orden_compra->dc_nota_venta):
      $insertar->bindValue(2, $this->orden_compra->dc_nota_venta, PDO::PARAM_INT);
      $insertar->bindValue(3, null, PDO::PARAM_NULL);
    elseif ($this->orden_compra->dc_orden_servicio):
      $insertar->bindValue(2, null, PDO::PARAM_NULL);
      $insertar->bindValue(3, $this->orden_compra->dc_orden_servicio, PDO::PARAM_INT);
    endif;

    $db->stExec($insertar);
    $this->dc_comprobante_asignacion = $db->lastInsertId();

    $this->insertarDetallesComprobanteAsignacionStock();
  }

  private function insertarDetallesComprobanteAsignacionStock() {
    $r = self::getRequest();
    $db = $this->getConnection();

    $insertar = $db->prepare($db->insert('tb_detalle_asignacion_stock_libre', array(
        'dc_comprobante' => $this->dc_comprobante_asignacion,
        'dc_detalle_orden_compra' => '?',
        'dc_detalle_nota_venta' => '?',
        'dc_detalle_orden_servicio' => '?',
        'dc_cantidad' => '?',
        'dq_pmp' => '?',
        'dc_bodega' => '?'
    )));
    $insertar->bindParam(1, $dc_detalle_orden_compra, PDO::PARAM_INT);
    $insertar->bindParam(2, $dc_detalle_nota_venta, PDO::PARAM_INT);
    $insertar->bindParam(3, $dc_detalle_orden_servicio, PDO::PARAM_INT);
    $insertar->bindParam(4, $dc_cantidad, PDO::PARAM_INT);
    $insertar->bindParam(5, $dq_pmp, PDO::PARAM_STR);
    $insertar->bindParam(6, $dc_bodega, PDO::PARAM_INT);

    foreach ($r->dc_detalle as $i => $dc_detalle_orden_compra):
      $detalle = $db->getRowById('tb_orden_compra_detalle', $dc_detalle_orden_compra, 'dc_detalle');
      $dc_cantidad = $r->dc_cantidad[$i];
      $dc_bodega = $r->dc_bodega[$i];

      if($detalle->dc_detalle_nota_venta):
        $dc_detalle_nota_venta = $detalle->dc_detalle_nota_venta;
        $dc_detalle_orden_servicio = NULL;
      else:
        $dc_detalle_nota_venta = NULL;
        $dc_detalle_orden_servicio = $detalle->dc_detalle_orden_servicio;
      endif;

      $producto = $db->getRowById('tb_producto', $detalle->dc_producto, 'dc_producto');

      $dq_pmp = floatval($producto->dq_precio_compra);

      $db->stExec($insertar);

    endforeach;

  }

  private function reservarStockBodega(){
    $r = self::getRequest();
    $db = $this->getConnection();
    $service = $this->getLogisticaService();

    foreach ($r->dc_detalle as $i => $dc_detalle):
      $detalle = $db->getRowById('tb_orden_compra_detalle', $dc_detalle, 'dc_detalle');
      $dc_cantidad = $r->dc_cantidad[$i];
      $dc_bodega = $r->dc_bodega[$i];

      $service->rebajarStockLibre($detalle->dc_producto, $dc_cantidad, $dc_bodega);

      if($detalle->dc_detalle_nota_venta):
        $service->cargarStockNV($detalle->dc_producto, $dc_cantidad, $dc_bodega);
      else:
        $service->cargarStockOS($detalle->dc_producto, $dc_cantidad, $dc_bodega);
      endif;

    endforeach;
  }

  private function ingresarMovimientosLogistica(){
    $r = self::getRequest();
    $db = $this->getConnection();
    $service = $this->getLogisticaService();

    $insertar = $db->prepare($db->insert('tb_movimiento_bodega', array(
        'dc_cliente' => '?',
        'dc_proveedor' => '?',
        'dc_tipo_movimiento' => '?',
        'dc_bodega_entrada' => '?',
        'dc_bodega_salida' => '?',
        'dq_monto' => '?',
        'dq_costo_stock' => '?',
        'dq_cambio' => '?',
        'dc_orden_compra' => '?',
        'dc_nota_venta' => '?',
        'dc_orden_servicio' => '?',
        'dc_ceco' => '?',
        'dc_producto' => '?',
        'dq_cantidad' => '?',
        'dc_movimiento_relacionado' => '?',
        'dc_empresa' => $this->getEmpresa(),
        'df_creacion' => $db->getNow(),
        'dc_usuario_creacion' => $this->getUserData()->dc_usuario
    )));

    if($this->orden_compra->dc_nota_venta):
      $insertar->bindValue(1, $db->getRowById('tb_nota_venta', $this->orden_compra->dc_nota_venta, 'dc_nota_venta')->dc_cliente, PDO::PARAM_INT);
    else:
      $insertar->bindValue(1, $db->getRowById('tb_orden_servicio', $this->orden_compra->dc_orden_servicio, 'dc_orden_servicio')->dc_cliente, PDO::PARAM_INT);
    endif;

    $insertar->bindValue(2, $this->orden_compra->dc_proveedor, PDO::PARAM_INT);
    $insertar->bindValue(3, $service->getConfiguration()->dc_tipo_movimiento_traspaso_compra, PDO::PARAM_INT);
    $insertar->bindParam(4, $dc_bodega_entrada, PDO::PARAM_INT);
    $insertar->bindParam(5, $dc_bodega_salida, PDO::PARAM_INT);
    $insertar->bindParam(6, $dq_pmp, PDO::PARAM_STR);
    $insertar->bindParam(7, $dq_pmp, PDO::PARAM_STR);
    $insertar->bindValue(8, $this->orden_compra->dq_cambio, PDO::PARAM_STR);
    $insertar->bindValue(9, $this->orden_compra->dc_orden_compra, PDO::PARAM_INT);
    $insertar->bindValue(10, $this->orden_compra->dc_nota_venta, PDO::PARAM_INT);
    $insertar->bindValue(11, $this->orden_compra->dc_orden_servicio, PDO::PARAM_INT);
    $insertar->bindParam(12, $dc_ceco, PDO::PARAM_INT);
    $insertar->bindParam(13, $dc_producto, PDO::PARAM_INT);
    $insertar->bindParam(14, $dc_cantidad, PDO::PARAM_INT);

    $actualizar = $db->prepare($db->update('tb_movimiento_bodega',array(
      'dc_movimiento_relacionado' => '?'
    ),'dc_movimiento = ?'));
    $actualizar->bindParam(1, $up_movimiento_relacionado, PDO::PARAM_INT);
    $actualizar->bindParam(2, $up_movimiento, PDO::PARAM_INT);

    foreach ($r->dc_detalle as $i => $dc_detalle):
      $detalle = $db->getRowById('tb_orden_compra_detalle', $dc_detalle, 'dc_detalle');
      $producto = $db->getRowById('tb_producto', $detalle->dc_producto, 'dc_producto');
      $cantidad = $r->dc_cantidad[$i];
      $dc_bodega = $r->dc_bodega[$i];

      $dq_pmp = $producto->dq_precio_compra;
      $dc_ceco = $detalle->dc_ceco;
      $dc_producto = $producto->dc_producto;

      $dc_cantidad = $cantidad*-1;
      $dc_bodega_salida = $dc_bodega;
      $dc_bodega_entrada = 0;

      $insertar->bindValue(15, null, PDO::PARAM_NULL);
      $db->stExec($insertar);

      $up_movimiento = $db->lastInsertId();
      $insertar->bindValue(15, $up_movimiento, PDO::PARAM_INT);
      $dc_cantidad = $cantidad;
      $dc_bodega_salida = 0;
      $dc_bodega_entrada = $dc_bodega;

      $db->stExec($insertar);

      $up_movimiento_relacionado = $db->lastInsertId();
      $db->stExec($actualizar);

    endforeach;


  }

}

?>
