<?php
define("MAIN",1);
require_once("../../inc/init.php");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">
	Informe de movimientos de bodega
</div>
<div id="main_cont">
	<div class="panes" id="desbordar">
    	<?php $form->Start('sites/logistica/proc/src_informe_movimientos.php','src_informe_movimientos') ?>
        <?php $form->Header('<b>Ingrese los filtros para el informe de movimientos</b><br />Los campos marcados con [*] son obligatorios') ?>
        <table class="tab" width="100%">
        	<tbody>
            	<tr>
                	<td>Fecha Movimiento</td>
                    <td><?php $form->Date('Desde','df_movimiento_desde',1,'01/'.date('m/Y')) ?></td>
                    <td><?php $form->Date('Hasta','df_movimiento_hasta',1,0) ?></td>
                </tr>
                <tr>
                	<td>Productos</td>
                    <td><?php $form->DBMultiSelect('','dc_producto','tb_producto',array('dc_producto','dg_codigo','SUBSTRING(dg_producto,1,80)')) ?></td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
        <div class="alert">
        	En caso de querer nuevos filtros enviar un correo al área de desarrollo para indicarlos
        </div>
        <?php $form->End('Ejecutar Consulta','searchbtn') ?>
    </div>
</div>
<script type="text/javascript">
$('#dc_producto').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>