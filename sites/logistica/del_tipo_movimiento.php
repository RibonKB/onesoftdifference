<?php
define("MAIN",1);
require_once("../../inc/init.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);

$dc_tipo_movimiento = intval($_POST['id']);

$datos = $db->prepare($db->select('tb_tipo_movimiento_logistico',
	'dc_tipo_movimiento, dg_tipo_movimiento, dg_codigo, dm_genera_asiento, dc_tipo_movimiento_contable','dc_tipo_movimiento = ? AND dc_empresa = ?'));
$datos->bindValue(1,$dc_tipo_movimiento,PDO::PARAM_INT);
$datos->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($datos);

$datos = $datos->fetch(PDO::FETCH_OBJ);

if($datos === false){
	$error_man->showWarning('El tipo de movimiento no fue encontrado');
	exit;
}


?>


<div class="secc_bar" >
	Eliminar tipo de movimiento
</div>

<div class="panes" >
	<?php $form->Start('sites/logistica/proc/delete_tipo_movimiento.php', 'del_tipo_movimiento') ?>
    <?php $error_man->showAviso("¿Está seguro que desea eliminar el tipo de movimiento? <strong>{$datos->dg_tipo_movimiento}</strong>") ?>

    <?php $form->Hidden('dc_tipo_movimiento_ed', $dc_tipo_movimiento)?>
    <?php $form->End('Eliminar','delbtn')?>
    
</div>

