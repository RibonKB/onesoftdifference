<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}


$_POST['ac_emision_desde'] = $db->sqlDate($_POST['ac_emision_desde']);
$_POST['ac_emision_hasta'] = $db->sqlDate($_POST['ac_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['ac_emision_desde']} AND {$_POST['ac_emision_hasta']})";

if($_POST['ac_nota_venta'] != ''){
	if(!is_numeric($_POST['ac_nota_venta'])){
		$error_man->showAviso('El formato de número de la nota de venta es incorrecto, corrijalo y vuelva a intentarlo');
		exit();
	}
	$nota_venta = $db->select('tb_nota_venta','dc_nota_venta',"dq_nota_venta={$_POST['ac_nota_venta']} AND dc_empresa = {$empresa}");
	if(!count($nota_venta)){
		$error_man->showWarning('La nota de venta especificada no existe');
		exit();
	}
	
	$conditions .= " AND dc_nota_venta = {$nota_venta[0]['dc_nota_venta']}";
	
}

if($_POST['ac_orden_servicio'] != ''){
	if(!is_numeric($_POST['ac_orden_servicio'])){
		$error_man->showAviso('El formato de número de la orden de servicio es incorrecto, corríjalo y vuelva a intentarlo');
		exit();
	}
	$orden_servicio = $db->select('tb_orden_servicio','dc_orden_servicio',"dq_orden_servicio={$_POST['ac_orden_servicio']} AND dc_empresa = {$empresa}");
	if(!count($orden_servicio)){
		$error_man->showWarning('La orden de servicio especificada no existe');
		exit();
	}
	
	$conditions .= " AND dc_orden_servicio = {$orden_servicio[0]['dc_orden_servicio']}";
	
}

if($_POST['ac_numero_desde']){
	if($_POST['ac_numero_hasta']){
		$conditions .= " AND (dq_comprobante BETWEEN {$_POST['ac_numero_desde']} AND {$_POST['ac_numero_hasta']})";
	}else{
		$conditions .= " AND dq_comprobante = {$_POST['ac_numero_desde']}";
	}
}

if(isset($_POST['ac_producto'])){
	$_POST['ac_producto'] = implode(',',$_POST['ac_producto']);
	$conditions .= " AND dc_producto IN ({$_POST['ac_producto']})";
}

$data = $db->select('tb_comprobante_ajuste_logistico','dc_comprobante,dq_comprobante',$conditions,array('order_by' => 'df_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron comprobantes de ajustes con los criterios especificados");
	exit();
}

echo("<div id='show_comprobante'></div>");

echo("<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Comprobantes de ajuste<br />Encontrados</caption>
<thead>
	<tr>
		<th>Nº Comprobante</th>
	</tr>
</thead>
<tbody>");

foreach($data as $c){
echo("<tr>
	<td align='left'>
		<a href='sites/logistica/proc/show_ajuste_cantidad.php?id={$c['dc_comprobante']}' class='oc_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dq_comprobante']}</a>
	</td>
</tr>");
}

echo("</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='delbtn' id='null_orden_compra'>Anular</button>
</div>");

?>
<script type="text/javascript">
$("#res_list").slideDown();
$("table.sortable").tablesorter();
$(".oc_load").click(function(e){
	e.preventDefault();
	$('#show_comprobante').html("<img src='images/ajax-loader.gif' alt='' /> cargando guía de recepción ...");
	$("#res_list td").removeClass('confirm');
	$(this).parent().addClass('confirm');
	$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
	loadFile($(this).attr('href'),'#show_comprobante');
}).first().trigger('click');

$('#show_hide_list').click(function(){
	$('#res_list').toggle();
});
</script>