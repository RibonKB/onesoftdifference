<?php
define("MAIN",1);
require_once("../../../inc/init.php");

//Comprobar que las cantidades a liberar no superen las cantidades máximas.
$dc_cantidad = array();
foreach($_POST['dc_cantidad'] as $i => $q){
	$cant = intval($q);
	$max = intval($_POST['dc_maximo'][$i]);
	
	if($cant > $max){
		$error_man->showWarning('Las cantidades no pueden superar el disponible para ser liberado');
		exit;
	}
	
	$dc_cantidad[$i] = $cant;
	
}

//Verificar nota de venta
$dc_nota_venta = intval($_POST['dc_nota_venta']);

$nota_venta = $db->prepare($db->select('tb_nota_venta','dc_cliente','dc_nota_venta = ? AND dc_empresa = ?'));
$nota_venta->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$nota_venta->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($nota_venta);
$nota_venta = $nota_venta->fetch(PDO::FETCH_OBJ);

if($nota_venta === false){
	$error_man->showWarning('La nota de venta es inválida, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Obtener tipo de movimiento logístico para liberacion de stock
$dc_tipo_movimiento = $db->doQuery(
							$db->select(
								'tb_configuracion_logistica',
								'dc_tipo_movimiento_liberar_stock',
								'dc_empresa = '.$empresa)
							)->fetch(PDO::FETCH_OBJ)->dc_tipo_movimiento_liberar_stock;

if($dc_tipo_movimiento == 0){
	$error_man->showWarning('No se ha configurado el tipo de movimiento para la liberación de stock, consulte con un administrador.');
	exit;
}

//Preparar actualización stock
$update_stock = $db->prepare($db->update('tb_stock',array(
	'dq_stock_reservado' => 'dq_stock_reservado-?'
),'dc_producto = ? AND dc_bodega = ? AND dq_stock_reservado >= ?'));
$update_stock->bindParam(1,$dq_stock,PDO::PARAM_INT);
$update_stock->bindParam(2,$dc_producto,PDO::PARAM_INT);
$update_stock->bindValue(3,$_POST['dc_bodega'],PDO::PARAM_INT);
$update_stock->bindParam(4,$dq_stock,PDO::PARAM_INT);

//Preparar actualización cantidades en detalle
$update_detalle = $db->prepare($db->update('tb_nota_venta_detalle',array(
	'dc_recepcionada' => 'dc_recepcionada-?',
	'dc_comprada' => 'dc_comprada-?'
),'dc_nota_venta_detalle = ?'));
$update_detalle->bindParam(1,$dq_stock,PDO::PARAM_INT);
$update_detalle->bindParam(2,$dq_stock,PDO::PARAM_INT);
$update_detalle->bindParam(3,$dc_detalle,PDO::PARAM_INT);

//Preparar Insertar movimiento de bodega
$insert_movimiento = $db->prepare($db->insert('tb_movimiento_bodega',array(
	'dc_cliente' => '?',
	'dc_tipo_movimiento' => '?',
	'dc_bodega_entrada' => '?',
	'dc_bodega_salida' => '?',
	'dc_nota_venta' => '?',
	'dc_producto' => '?',
	'dq_cantidad' => '?',
	'dq_monto' => '?',
	'dc_empresa' => $empresa,
	'df_creacion' => $db->getNow(),
	'dc_usuario_creacion' => $idUsuario
)));
$insert_movimiento->bindValue(1,$nota_venta->dc_cliente,PDO::PARAM_INT);
$insert_movimiento->bindValue(2,$dc_tipo_movimiento,PDO::PARAM_INT);
$insert_movimiento->bindParam(3,$dc_bodega_entrada,PDO::PARAM_INT);
$insert_movimiento->bindParam(4,$dc_bodega_salida,PDO::PARAM_INT);
$insert_movimiento->bindParam(5,$dc_nota_venta_movimiento,PDO::PARAM_INT);
$insert_movimiento->bindParam(6,$dc_producto,PDO::PARAM_INT);
$insert_movimiento->bindParam(7,$dq_stock,PDO::PARAM_INT);
$insert_movimiento->bindParam(8,$dq_monto,PDO::PARAM_STR);

$db->start_transaction();

	foreach($_POST['dc_detalle'] as $i => $v){
		
		$dc_producto = $_POST['dc_producto'][$i];
		$dc_detalle = $v;
		$dq_stock = $dc_cantidad[$i];
		
		if($dq_stock < 1)
			continue;
		
		$db->stExec($update_stock);
		
		$producto = $db->doQuery($db->select('tb_producto','dg_codigo, dg_producto, dq_precio_compra','dc_producto = '.$dc_producto))->fetch(PDO::FETCH_OBJ);
		
		if($update_stock->rowCount() < 1){
			
			$error_man->showWarning("Ocurrió un error al intentar liberar el producto de la bodega, no hay stock reservado suficiente en la bodega seleccionada
			<br />
			Error encontrado en el producto
			<br /><r />
			Código: <b>{$producto->dg_codigo}</b><br />
			Producto: <b>{$producto->dg_producto}</b>");
			$db->rollBack();
			exit;
		}
		
		$db->stExec($update_detalle);
		
		$dc_bodega_entrada = 0;
		$dc_bodega_salida = $_POST['dc_bodega'];
		$dc_nota_venta_movimiento = $dc_nota_venta;
		$dq_stock = -$dq_stock;
		$dq_monto = $producto->dq_precio_compra;
		
		$db->stExec($insert_movimiento);
		
		$dc_bodega_entrada = $_POST['dc_bodega'];
		$dc_bodega_salida = 0;
		$dc_nota_venta_movimiento = 0;
		$dq_stock = -$dq_stock;
		
		$db->stExec($insert_movimiento);
		
	}
	
	$dc_cantidad_total = array_sum($dc_cantidad);
	
	$update_nota_venta = $db->prepare($db->update('tb_nota_venta',array(
		'dc_recepcionada' => 'dc_recepcionada-?',
		'dc_comprada' => 'dc_comprada-?'
	),'dc_nota_venta = ?'));
	$update_nota_venta->bindValue(1,$dc_cantidad_total,PDO::PARAM_INT);
	$update_nota_venta->bindValue(2,$dc_cantidad_total,PDO::PARAM_INT);
	$update_nota_venta->bindValue(3,$dc_nota_venta,PDO::PARAM_INT);
	$db->stExec($update_nota_venta);

$db->commit();

$error_man->showConfirm('Se han liberado los productos de la nota de venta');
?>
<script type="text/javascript">
	js_data.NVChange(null, [0,0,0,<?php echo $dc_nota_venta ?>]);
</script>