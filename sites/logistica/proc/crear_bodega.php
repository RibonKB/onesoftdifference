<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['bodega_name'] = ucwords($_POST['bodega_name']);

$new = $db->insert('tb_bodega',array(
	'dg_bodega' => $_POST['bodega_name'],
	'dm_tipo' => $_POST['bodega_tipo'],
	'dm_transito' => $_POST['bodega_transito'],
	'dm_maneja_critico' => $_POST['bodega_critico'],
	'dc_empresa' => $empresa,
	'df_creacion' => 'NOW()'
));

$error_man->showConfirm("Se ha creado la bodega <strong>{$_POST['bodega_name']}</strong> correctamente");

$date_now = date("d/m/Y H:i");
echo("<table class='hidden'>
<tr id='item{$new}'>
	<td>{$_POST['bodega_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/logistica/ed_bodega.php?id={$new}' class='loadOnOverlay left'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/logistica/del_bodega.php?id={$new}' class='loadOnOverlay left'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>");

?>
<script type="text/javascript">
	$("#item<?=$new ?>").appendTo("#list");
	
	$("a.loadOnOverlay").unbind('click').click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});
</script>