<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once('../../ventas/proc/ventas_functions.php');

$dq_ruta  = doc_GetNextNumber('tb_ruta','dq_ruta');

$insert_ruta = $db->prepare($db->insert('tb_ruta',array(
	'dq_ruta' => $dq_ruta,
	'df_emision' => $db->getNow(),
	'df_ruta' => $db->sqlDate($_POST['ruta_fecha']),
	'dc_usuario_creacion' => '?',
	'dc_empresa' => '?',
	'dc_tipo_ruta' => '?'
)));

$insert_ruta->bindValue(1,$idUsuario,PDO::PARAM_INT);
$insert_ruta->bindValue(2,$empresa,PDO::PARAM_INT);
$insert_ruta->bindValue(3,$_POST['ruta_tipo'],PDO::PARAM_INT);

$db->stExec($insert_ruta);

$dc_ruta = $db->lastInsertId();

//PDOStatement encargado de insertar detalles de rutas
$insert_detalle = $db->prepare($db->insert('tb_ruta_detalle',array(
	'dc_detalle_nota_venta' => '?',
	'dc_ruta' => $dc_ruta,
	'dt_hora' => '?',
	'dq_cantidad' => '?'
)));

$insert_detalle->bindParam(1,$dc_detalle,PDO::PARAM_INT);
$insert_detalle->bindParam(2,$dt_hora,PDO::PARAM_STR);
$insert_detalle->bindParam(3,$dc_cantidad,PDO::PARAM_INT);

//PDOStatement encargado de modificar los detalles de notas de venta afectados.
$update_detalle = $db->prepare($db->update('tb_nota_venta_detalle',array(
	'dc_rutada' => 'dc_rutada+?'
),"dc_nota_venta_detalle = ?"));

$update_detalle->bindParam(1,$dc_cantidad,PDO::PARAM_INT);
$update_detalle->bindParam(2,$dc_detalle,PDO::PARAM_INT);

foreach($_POST['dc_detalle'] as $i => $v){
	$dt_hora = $_POST['df_hora'][$i];
	$dc_detalle = $v;
	$dc_cantidad = $_POST['dq_cantidad'][$i];
	
	$db->stExec($insert_detalle);
	$db->stExec($update_detalle);
}

$error_man->showConfirm("Se ha creado la gestión de ruta correctamente.");
echo("<div class='title'>El número de comprobante es <br><strong style='color:#000;'>$dq_ruta</strong></div>");
?>