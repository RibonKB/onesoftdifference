<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if($_POST['nv_numero_desde']){
	if(isset($_POST['nv_numero_hasta']) && $_POST['nv_numero_hasta']){
		
		$_POST['nv_emision_desde'] = $db->sqlDate($_POST['nv_emision_desde']);
		$_POST['nv_emision_hasta'] = $db->sqlDate($_POST['nv_emision_hasta']." 23:59");
		
		$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['nv_emision_desde']} AND {$_POST['nv_emision_hasta']})";
		
		$conditions .= " AND (dq_nota_venta BETWEEN {$_POST['nv_numero_desde']} AND {$_POST['nv_numero_hasta']})";
	}else{
		$conditions = "dq_nota_venta = {$_POST['nv_numero_desde']} AND dc_empresa = {$empresa}";
	}
}else{
	$_POST['nv_emision_desde'] = $db->sqlDate($_POST['nv_emision_desde']);
	$_POST['nv_emision_hasta'] = $db->sqlDate($_POST['nv_emision_hasta']." 23:59");
	
	$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['nv_emision_desde']} AND {$_POST['nv_emision_hasta']})";
}

if(isset($_POST['nv_client'])){
	$_POST['nv_client'] = implode(',',$_POST['nv_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['nv_client']})";
}

if(isset($_POST['nv_executive'])){
	$_POST['nv_executive'] = implode(',',$_POST['nv_executive']);
	$conditions .= " AND dc_ejecutivo IN ({$_POST['nv_executive']})";
}

if(isset($_POST['nv_status'])){
	$_POST['nv_status'] = implode(" = '1' OR ",$_POST['nv_status']);
	$conditions .= " AND ({$_POST['nv_status']} = '1')";
}

$data = $db->select("(SELECT * FROM tb_nota_venta WHERE {$conditions}) nv
	JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
	JOIN tb_nota_venta_detalle d ON d.dc_nota_venta = nv.dc_nota_venta AND d.dq_cantidad > d.dc_rutada
	JOIN tb_producto p ON p.dc_producto = d.dc_producto",
'nv.dc_nota_venta,nv.dq_nota_venta,cl.dg_razon,p.dg_codigo,p.dg_producto,(d.dq_cantidad-d.dc_rutada) dq_cantidad,d.dc_nota_venta_detalle');

if(!count($data)){
	$error_man->showAviso("No se encontraron notas de venta con los criterios especificados");
	exit();
}

$NV_DATA = array();
foreach($data as $d){
	if(!isset($NV_DATA[$d['dc_nota_venta']])){
		$NV_DATA[$d['dc_nota_venta']]['dq_nota_venta'] = $d['dq_nota_venta'];
		$NV_DATA[$d['dc_nota_venta']]['dg_razon'] = $d['dg_razon'];
	}
	
	$NV_DATA[$d['dc_nota_venta']]['detalle'][] = array(
		'dg_codigo' => $d['dg_codigo'],
		'dg_producto' => $d['dg_producto'],
		'dc_detalle' => $d['dc_nota_venta_detalle'],
		'dq_cantidad' => $d['dq_cantidad']
	);
}
unset($data);

require_once("../../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/logistica/proc/crear_ruta.php','cr_ruta');
$form->Header("Indique los datos para la creación de la nueva ruta<br />Los datos marcados con [*] son obligatorios");

$form->Section();
	$form->Date('Fecha de ruta','ruta_fecha',1,0);
$form->EndSection();

$form->Section();
	$form->Listado('Tipo de ruta','ruta_tipo','tb_tipo_ruta',array('dc_tipo','dg_tipo'),1);
$form->EndSection();

$form->Group();

?>

<link rel="stylesheet" type="text/css" href="styles/ruta_calendar.css?v=0_1a2" />

<!-- Listado de notas de venta -->
<div id="ruta_nota_venta">
<?php foreach($NV_DATA as $dc_nota_venta => $NV): ?>
	<div class="nv_data">
		<div class="nv_header">
			<img src="images/menu-arrow.png" alt="v" title="Mostrar Detalles" class="togglebtn right" />
			<b><?php echo $NV['dq_nota_venta'] ?></b><br />
			<small><?php echo $NV['dg_razon'] ?></small>
			<input type="hidden" class="nv_id" value="<?php echo $dc_nota_venta ?>" />
		</div>
		
		<ul class="nv_detail">
		<?php foreach($NV['detalle'] as $d): ?>
			<li class="selectable">
				<span id="detail<?php echo $d['dc_detalle'] ?>"><b><?php echo $d['dg_codigo'] ?></b> - <?php echo $d['dg_producto'] ?></span>
				<input type="hidden" class="detail_id" value="<?php echo $d['dc_detalle'] ?>" />
				<input type="hidden" class="detail_cant" value="<?php echo $d['dq_cantidad'] ?>" />
			</li>
		<?php endforeach; ?>
		</ul>
		
	</div>
<?php endforeach; ?>
</div>

<!-- Agenda de entregas -->
<div id="ruta_calendar">
<?php foreach(range(0,23) as $h):
	foreach(array('00','30') as $m):
	$hora = str_pad($h, 2, 0, STR_PAD_LEFT).':'.$m;
	?>
	<div class="calendar_hour_item">
		<span><?php echo $hora ?></span>
		<input type="hidden" class="calendar_time_value" value="<?php echo $hora ?>" />
	</div>
<?php endforeach;
endforeach; ?>
</div>

<!-- Detalles de ruta -->
<div id="detailDataContainer"></div>
<br class="clear" />

<?php
	$form->Group();
	$form->End("Crear",'addbtn');
?>
<script type="text/javascript" src="jscripts/sites/logistica/cr_ruta.js?v=0_1a7"></script>