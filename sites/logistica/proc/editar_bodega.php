<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->update('tb_bodega',array(
	'dg_bodega' => $_POST['ed_bodega_name'],
	'dm_tipo' => $_POST['ed_bodega_tipo'],
	'dm_transito' => $_POST['ed_bodega_transito'],
	'dm_maneja_critico' => $_POST['ed_bodega_critico']
),"dc_bodega={$_POST['ed_bodega_id']}");

?>
<script type="text/javascript">
	alert('Se ha modificado la bodega correctamente');
	loadpage("sites/logistica/src_bodega.php");
</script>