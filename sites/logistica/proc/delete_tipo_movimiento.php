<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_tipo_movimiento = intval($_POST['dc_tipo_movimiento_ed']);

$datos = $db->prepare($db->select('tb_tipo_movimiento_logistico','dm_activo','dc_empresa = ? AND dc_tipo_movimiento = ?'));
$datos->bindValue(1,$empresa,PDO::PARAM_INT);
$datos->bindValue(2,$dc_tipo_movimiento,PDO::PARAM_INT); 
$db->stExec($datos);  

$db->start_transaction();

$eliminar = $db->prepare($db->update('tb_tipo_movimiento_logistico',array(
				'dm_activo' => '0'),
				'dc_empresa = ? AND dc_tipo_movimiento =?'));
$eliminar->bindValue(1,$empresa,PDO::PARAM_INT);
$eliminar->bindValue(2,$dc_tipo_movimiento,PDO::PARAM_INT);
$db->stExec($eliminar);

$db->commit();

$error_man->showConfirm('Se ha eliminado correctamente'); 


?>