<?php
define("MAIN",1);
require_once("../../../inc/global.php");

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$db->escape($_GET['number']);

switch($_GET['mode']){
	case 'gd': get_guia_despacho_data($_GET['number']); break;
	case 'os': get_orden_servicio_data($_GET['number']); break;
	case 'nv': get_nota_venta_data($_GET['number']); break;
	default: echo json_encode('<not-found>');
}


function get_guia_despacho_data($number){
	global $db,$empresa;
	
	$data = $db->select("tb_guia_despacho",'dc_guia_despacho,dc_nota_venta,dc_orden_servicio',"dc_empresa={$empresa} AND dc_factura = 0 AND dq_guia_despacho={$number}");
	
	if(!count($data)){
		echo json_encode('<not-found>');
		exit;
	}
	$data = $data[0];
	
	if($data['dc_nota_venta'] != 0){
		$doc = $db->select('tb_nota_venta','dq_nota_venta',"dc_nota_venta={$data['dc_nota_venta']}");
		$data['dq_nota_venta'] = $doc[0]['dq_nota_venta'];
	}else if($data['dc_orden_servicio'] != 0){
		$doc = $db->select('tb_orden_servicio','dq_orden_servicio',"dc_orden_servicio={$data['dc_orden_servicio']}");
		$data['dq_orden_servicio'] = $doc[0]['dq_orden_servicio'];
	}
	
	echo json_encode($data);
}

function get_nota_venta_data($number){
	global $db;
}

function get_orden_servicio_data($number){
	global $db;
}
?>