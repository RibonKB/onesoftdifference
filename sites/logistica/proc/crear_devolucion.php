<?php
/**
*	Recepcion por devolución (rama aferente de validación, rama de proceso y rama deferente)
*	
*	Se valida la existencia de stock en las bodegas de salida virtuales y se realiza la recepción a las bodegas de entrada.
*	Además de registrar los movimientos de bodega
*	terminando con la emisión de un documento comprobante de devolución.
*	
*	IN: DEV_DATA, cantidad[], producto[], bodega_entrada[], |bodega_salida[]|, descripcion[]
*	OUT: Comprobante de devolución y stock rebajado/cargado a las bodegas de salida/entrada
**/

//Se carga el núcleo del sistema
define("MAIN",1);
require_once("../../../inc/init.php");
require_once('../../ventas/proc/ventas_functions.php');

/**
*	Obtención de dependencias
*	
*	OUT: tipo_movimiento, dev_number
**/

//tipo de movimiento de las transacciones sobre la bodegas
$tipo_movimiento = $db->doQuery($db->select('tb_configuracion_logistica','dc_tipo_movimiento_devolucion_gd',"dc_empresa={$empresa}"))
						->fetch(PDO::FETCH_OBJ)
						->dc_tipo_movimiento_devolucion_gd;
						
//El número que utilizará el comprobante de devolución
$dev_number = doc_GetNextNumber('tb_devolucion_guia_despacho','dq_devolucion');

$dc_guia_despacho = $_POST["dc_guia_despacho"];

/**
*	Comprobar formato y Verificar guía de despacho sin facturar
*	
*	IN: dc_guia_despacho
*	OUT: Guía de despacho verificada
**/

if(!is_numeric($dc_guia_despacho)){
	$error_man->showWarning("No puede proseguir con la devolución debido a que la guía de despacho ingresada es inválida.");
	exit;
}

$validez = $db->doQuery($db->select('tb_guia_despacho','1',"dc_guia_despacho = {$dc_guia_despacho} AND dc_factura = 0"))
			->fetch();

if($validez === false){
	$error_man->showWarning("La guía de despacho a sido facturada, por este motivo no puede hacer la devolución de la guía de despacho a menos que anule antes la factura asociada.");
	exit;
}

$db->start_transaction();

/**
*	Ingresa cabecera de comprobante de devolución
*	
*	IN: dev_number, dc_guia_despacho, dg_comentario, |dc_orden_servicio|, |dc_nota_venta|
*	OUT: dc_devolucion
**/

$insert_dev = $db->prepare($db->insert('tb_devolucion_guia_despacho',array(
	"dq_devolucion" => '?',
	"dc_guia_despacho" => '?',
	"dc_orden_servicio" => '?',
	"dg_comentario" => '?',
	"df_emision" => '?',
	"df_fecha_creacion" => $db->getNow(),
	"dc_usuario_creacion" => $idUsuario,
	"dc_empresa" => $empresa,
	"dc_nota_venta" => '?'
)));

$insert_dev->bindValue(1,$dev_number,PDO::PARAM_INT);
$insert_dev->bindValue(2,$dc_guia_despacho,PDO::PARAM_INT);
$insert_dev->bindValue(4,$_POST["dg_comentario"],PDO::PARAM_INT);
$insert_dev->bindValue(5,$db->sqlDate2($_POST["df_fecha_emision"]),PDO::PARAM_INT);

if(isset($_POST["dc_nota_venta"])){
	$insert_dev->bindValue(6,$_POST["dc_nota_venta"],PDO::PARAM_INT);
	$insert_dev->bindValue(3,0,PDO::PARAM_INT);
}else if(isset($_POST["dc_orden_servicio"])){
	$insert_dev->bindValue(3,$_POST["dc_orden_servicio"],PDO::PARAM_INT);
	$insert_dev->bindValue(6,0,PDO::PARAM_INT);
}else{
	$insert_dev->bindValue(3,0,PDO::PARAM_INT);
	$insert_dev->bindValue(6,0,PDO::PARAM_INT);
}

$db->stExec($insert_dev);

$dc_devolucion = $db->lastInsertId();

$detail_st = $db->prepare($db->insert('tb_devolucion_guia_despacho_detalle',array(
	'dc_devolucion' => $dc_devolucion,
	'dq_cantidad' => '?',
	'dg_descripcion' => '?',
	'dc_detalle_guia' => '?'
)));

$detail_st->bindParam(1,$dq_cantidad,PDO::PARAM_INT);
$detail_st->bindParam(2,$dg_descripcion,PDO::PARAM_STR);
$detail_st->bindParam(3,$dc_detalle_guia,PDO::PARAM_INT);


$registrar_movimiento_entrada = $db->prepare($db->insert('tb_movimiento_bodega',array(
	"dc_tipo_movimiento" => $tipo_movimiento,
	"dc_bodega_entrada" => '?',
	"dc_guia_despacho" => $dc_guia_despacho,
	"dg_series" => '?',
	"dc_nota_venta" => '?',
	"dc_orden_servicio" => '?',
	"dc_devolucion_guia_despacho" => $dc_devolucion,
	"dc_producto" => '?',
	"dq_cantidad" => '?',
	"dq_monto" => '?',
	"dc_empresa" => $empresa,
	"df_creacion" => $db->getNow(),
	"dc_usuario_creacion" => $idUsuario
)));

$registrar_movimiento_entrada->bindParam(1,$dc_bodega_entrada,PDO::PARAM_INT);
$registrar_movimiento_entrada->bindParam(2,$dg_serie,PDO::PARAM_STR);

if(isset($_POST["dc_nota_venta"])){
	$registrar_movimiento_entrada->bindValue(3,$_POST["dc_nota_venta"],PDO::PARAM_INT);
	$registrar_movimiento_entrada->bindValue(4,0,PDO::PARAM_INT);
}else if(isset($_POST["dc_orden_servicio"])){
	$registrar_movimiento_entrada->bindValue(3,0,PDO::PARAM_INT);
	$registrar_movimiento_entrada->bindValue(4,$_POST["dc_orden_servicio"],PDO::PARAM_INT);
}else{
	$registrar_movimiento_entrada->bindValue(3,0,PDO::PARAM_INT);
	$registrar_movimiento_entrada->bindValue(4,0,PDO::PARAM_INT);
}

$registrar_movimiento_entrada->bindParam(5,$dc_producto,PDO::PARAM_INT);
$registrar_movimiento_entrada->bindParam(6,$dq_cantidad,PDO::PARAM_INT);
$registrar_movimiento_entrada->bindParam(7,$dq_monto,PDO::PARAM_STR);

//
if(isset($_POST["dc_nota_venta"])){
	$actualizar_detalle_nota_venta = $db->prepare($db->update('tb_nota_venta_detalle',array(
		'dc_despachada' => 'dc_despachada-?'
	),"dc_nota_venta_detalle=?"));
	
	$cantidad_total_nv = 0;
	
} else if(isset($_POST["dc_orden_servicio"])){
	$actualizar_detalle_orden_servicio = $db->prepare($db->update('tb_orden_servicio_factura_detalle',array(
		'dq_despachado' => 'dq_despachado-?'
	),"dc_detalle=?"));

}

foreach($_POST['gd_detail_id'] as $i => $detail_id){


/**
*	Ingresa detalle comprobante de devolucion
*	
*	IN: dq_cantidad, dg_descripcion, dc_detalle_guia
*	
**/

	$dq_cantidad = $_POST['dq_cantidad'][$i];
	$dg_descripcion = $_POST['dg_descripcion'][$i];
	$dc_detalle_guia = $_POST['gd_detail_id'][$i];
		
	$db->stExec($detail_st);
	
/**
*	Quitar stock de bodega de salida
*	
*	En caso de ser guia de traslado la afectada el stock debió ser almacenado temporalmente en una bodega de salida virtual,
*	el stock debe ser removido desde esa bodega e ingresado en la bodega de entrada.
*	
*	IN: dc_producto, dq_cantidad, dc_bodega_salida
*	OUT: productos rebajados desde la bodega de salida
*/

	$dc_producto	=	$_POST['prods'][$i];
	$dc_bodega_salida	=	$_POST['dc_bodega_salida'][$i];
	
	if($dc_bodega_salida != 0){
		$validez = bodega_RebajarStockLibre($dc_producto,$dq_cantidad,$dc_bodega_salida);
		if($validez != 1){
			$error_man->showWarning("Ocurrió un error inesperado al intentar rebajar stock desde la bodega de traslado<br />");
			exit;
		}
	}

/**
*	Ingresar stock a bodega de entrada
*	
*	El stock es cargado en la bodega de entrada y queda almacenado de manera libre.
*	
*	IN: dc_prodcuto, dq_cantidad, dc_bodega_entrada
*	OUT: productos cargados en la bodega de entrada
**/

	$dc_bodega_entrada	 =	$_POST['dc_bodega_entrada'][$i];
	
	if(isset($_POST["dc_nota_venta"])){
		
		bodega_CargarStockNV($dc_producto,$dq_cantidad,$dc_bodega_entrada);
		
	}else if(isset($_POST["dc_orden_servicio"])){
		
		bodega_CargarStockOS($dc_producto,$dq_cantidad,$dc_bodega_entrada);
		
	}else{
		
		bodega_CargarStockLibre($dc_producto,$dq_cantidad,$dc_bodega_entrada);
		
	}

/**
*	Ingresar movimiento de bodega de entrada
*	
*	IN: dc_tipo_movimiento, dc_bodega_entrada, dc_guia_despacho, dg_serie, dc_nota_venta, dc_orden_servicio, dc_devolucion_guia_despacho, dc_producto, dq_cantidad, 
**/

	$dg_serie = $_POST['prod_serie'][$i];
	
	$producto = $db->prepare($db->select('tb_producto','dq_precio_compra','dc_producto = ?'));
	$producto->bindValue(1,$dc_producto,PDO::PARAM_INT);
	$db->stExec($producto);
	$producto = $producto->fetch(PDO::FETCH_OBJ);
	
	$dq_monto = floatval($producto->dq_precio_compra);
	
	$db->stExec($registrar_movimiento_entrada);
	
/**
*	Rebajar cantidad despachada de detalle de factura en orden de servicio
*	
*	IN: dc_detalle_orden_servicio
**/

	if(isset($_POST["dc_orden_servicio"])){
		$dc_detalle_orden_servicio = $_POST['doc_detail_id'][$i];
		
		$actualizar_detalle_orden_servicio->bindValue(1,$dq_cantidad,PDO::PARAM_INT);
		$actualizar_detalle_orden_servicio->bindValue(2,$dc_detalle_orden_servicio,PDO::PARAM_INT);
		
		$db->stExec($actualizar_detalle_orden_servicio);
	}	
/**
*	Rebajar cantidad despachada en detalle de la nota de venta
*	
*	IN: dc_detalle_orden_servicio
**/

	else if(isset($_POST["dc_nota_venta"])){
		$dc_detalle_nota_venta = $_POST['doc_detail_id'][$i];
		
		$actualizar_detalle_nota_venta->bindValue(1,$dq_cantidad,PDO::PARAM_INT);
		$actualizar_detalle_nota_venta->bindValue(2,$dc_detalle_nota_venta,PDO::PARAM_INT);
		
		$db->stExec($actualizar_detalle_nota_venta);
		
		$cantidad_total_nv += $dq_cantidad;
	}
 
}

if(isset($_POST["dc_nota_venta"]) && $cantidad_total_nv > 0){
	$cabecera_nv = $db->prepare($db->update('tb_nota_venta',array(
		'dc_despachada' => 'dc_despachada-?'
	),"dc_nota_venta=?"));
	
	$cabecera_nv->bindValue(1,$cantidad_total_nv,PDO::PARAM_INT);
	$cabecera_nv->bindValue(2,$_POST["dc_nota_venta"],PDO::PARAM_INT);
	
	$db->stExec($cabecera_nv);
}

$db->commit();

$error_man->showConfirm("Se ha realizado la devolución de los productos seleccionados.");
echo("<div class='title'>El numero de la devolución es <h1 style='margin:0;color:#000;'>{$dev_number}</h1></div>");
?>