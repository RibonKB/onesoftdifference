<?php
/**
*	Rama de aferente de validación, rama de proceso y rama deferente de traspaso simple de stock
*
*	Procesos:
*		- Comprobar y rebajar stock bodega de salida
*		- Cargar stock bodega de entrada
*		- Registrar movimiento salida de bodega
*		- Registrar movimiento entrada a bodega
*		- Mostrar confirmación
*
*	OUT: Traspaso de bodega realizado satisfactoriamente
**/

//Dependencias
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../sites/ventas/proc/ventas_functions.php");

$dc_producto =& $_POST['prod_id'];
$dc_bodega_salida =& $_POST['tr_bodega_salida'];
$dc_bodega_entrada =& $_POST['tr_bodega_entrada'];
$stock_a_traspasar =& intval($_POST['tr_cantidad']);

if(!$stock_a_traspasar){
	$error_man->showWarning('Debe indicar una cantidad de stock a traspasar válida y esta no puede ser 0 (cero)');
	exit;
}

$producto = $db->prepare($db->select('tb_producto','dq_precio_compra','dc_producto = ? AND dc_empresa = ?'));
$producto->bindValue(1,$dc_producto,PDO::PARAM_INT);
$producto->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($producto);
$producto = $producto->fetch(PDO::FETCH_OBJ);

if($producto === false){
	$error_man->showWarning('El producto seleccionado no fue encontrado, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

$tipo_movimiento = $db->doQuery($db->select('tb_configuracion_logistica','dc_tipo_movimiento_traspaso_simple',"dc_empresa={$empresa}"))
						->fetch(PDO::FETCH_OBJ)
						->dc_tipo_movimiento_traspaso_simple;

$db->start_transaction();

/**
*	Comprobar y rebajar stock bodega de salida
*
*	Se rebajan los productos de la bodega de salida de manera segura en una transacción
*
*	IN: dc_bodega_salida, dc_producto, stock_a_traspasar
*
*	OUT: stock rebajado en bodega de salida
**/
if($_POST['tr_modo'] == 0){
	$valida = bodega_RebajarStockLibre($dc_producto,$stock_a_traspasar,$dc_bodega_salida);

	if($valida != 1){
		$error_man->showWArning("No se pudo sacar el stock especificado de la bodega de salida, compruebe que haya stock disponible e intentelo denuevo");
		$db->rollBack();
		exit;
	}
}else if($_POST['tr_modo'] == 1){
	$valida = bodega_RebajarStockReservado($dc_producto,$stock_a_traspasar,$dc_bodega_salida);

	if($valida != 1){
		$error_man->showWArning("No se pudo sacar el stock especificado de la bodega de salida, no hay stock reservado suficiente.<br />
		Compruebe que haya stock disponible e intentelo denuevo");
		$db->rollBack();
		exit;
	}
}else if($_POST['tr_modo'] == 2){
	$dc_stock_disponible = bodega_ComprobarTotalStock($dc_producto,$dc_bodega_salida);

	if($dc_stock_disponible[0]+$dc_stock_disponible[1] < $stock_a_traspasar){
		$error_man->showWArning("No se pudo sacar el stock especificado de la bodega de salida, compruebe que haya stock disponible e intentelo denuevo");
		$db->rollBack();
		exit;
	}

	if($dc_stock_disponible[0] < $stock_a_traspasar){
		$dc_stock_libre = $dc_stock_disponible[0];
		$dc_stock_reservado = $stock_a_traspasar-$dc_stock_disponible[0];
	}else{
		$dc_stock_libre = $stock_a_traspasar;
		$dc_stock_reservado = 0;
	}

	bodega_RebajarStockLibre($dc_producto,$dc_stock_libre,$dc_bodega_salida);
	if($dc_stock_reservado > 0)
		bodega_RebajarStockReservado($dc_producto,$dc_stock_reservado,$dc_bodega_salida);
}else{
	$error_man->showWarning("Ha ocurrido un error inesperado e irrecuperable al intentar realizar el traspaso, compruebe los parámetros de entrada y vuelva a intentarlo.");
	$db->rollback();
	exit;
}
/**
*	Cargar stock bodega entrada
*
*	Luego de restado el stock en la bodega de salida se ingresa este en la bodega de entrada
*
*	IN: dc_bodega_entrada, dc_producto, stock_a_traspasar
*
*	OUT: Stock cargado en bodega de entrada
**/

if($_POST['tr_modo'] == 0){
	bodega_CargarStockLibre($dc_producto,$stock_a_traspasar,$dc_bodega_entrada);
}else if($_POST['tr_modo'] == 1){
	bodega_CargarStockNV($dc_producto,$stock_a_traspasar,$dc_bodega_entrada);
}else if($_POST['tr_modo'] == 2){
	if(!isset($dc_stock_libre) || !isset($dc_stock_reservado)){
		$error_man->showWarning("Ha ocurrido un error inesperado e irrecuperable al intentar realizar el traspaso, compruebe los parámetros de entrada y vuelva a intentarlo.");
		$db->rollback();
		exit;
	}

	bodega_CargarStockLibre($dc_producto,$dc_stock_libre,$dc_bodega_entrada);
	if($dc_stock_reservado > 0)
		bodega_CargarStockNV($dc_producto,$dc_stock_reservado,$dc_bodega_entrada);

}else{
	$error_man->showWarning("Ha ocurrido un error inesperado e irrecuperable al intentar realizar el traspaso, compruebe los parámetros de entrada y vuelva a intentarlo.");
	$db->rollback();
	exit;
}

/**
*	Registrar movimiento salida de bodega
*
*	A manera de log se registra el movimiento de salida en la tabla de movimientos de bodega con respecto al movimiento realizado en la bodega de salida
*
*	IN: dc_tipo_movimiento, tb_bodega_salida, stock_a_traspasar, dc_producto
*
*	OUT: Movimiento de salida registrado correctamente
**/

$db->doExec($db->insert('tb_movimiento_bodega',array(
	"dc_tipo_movimiento" => $tipo_movimiento,
	"dc_bodega_salida" => $dc_bodega_salida,
	"dc_producto" => $dc_producto,
	"dq_cantidad" => -1*$stock_a_traspasar,
	"dq_monto" => $producto->dq_precio_compra,
	"dc_empresa" => $empresa,
	"df_creacion" => $db->getNow(),
	"dc_usuario_creacion" => $idUsuario
)));

$dc_movimiento = $db->lastInsertId();

/**
*	Registrar movimiento entrada de bodega
*
*	Al igual que en el proceso anterior, pero esta vez para el ingreso en la bodega de entrada
*
*	IN: dc_tipo_movimiento, tb_bodega_entrada, stock_a_traspasar
*
*	OUT: Movimiento de entrada registrado correctamente
**/

$db->doExec($db->insert('tb_movimiento_bodega',array(
	"dc_tipo_movimiento" => $tipo_movimiento,
	"dc_bodega_entrada" => $dc_bodega_entrada,
	"dc_producto" => $dc_producto,
	"dq_cantidad" => $stock_a_traspasar,
	"dq_monto" => $producto->dq_precio_compra,
	"dc_empresa" => $empresa,
	"df_creacion" => $db->getNow(),
	"dc_usuario_creacion" => $idUsuario,
	"dc_movimiento_relacionado" => $dc_movimiento
)));

$dc_movimiento_relacionado = $db->lastInsertId();

/**
*	Relacionar último registro de movimiento con el hecho anteriormente.
**/

$update_movimiento = $db->prepare($db->update('tb_movimiento_bodega',array('dc_movimiento_relacionado' => '?'),'dc_movimiento = ?'));
$update_movimiento->bindValue(1,$dc_movimiento_relacionado,PDO::PARAM_INT);
$update_movimiento->bindValue(2,$dc_movimiento,PDO::PARAM_INT);
$db->stExec($update_movimiento);

//Terminar transaction
$db->commit();

/**
*	Mostrar confirmación
*
*	Se muestra al usuario un mensaje confirmando el traspaso entre bodegas y se actualiza el stock en  la vista de stocks disponible
**/
//CODE-HERE
?>
<script type="text/javascript">
show_confirm("Se ha realizado el traspaso entre bodegas.");
$('#tr_cantidad').val('').trigger('blur');
js_data.load_stock_list(<?php echo($dc_producto); ?>);

</script>
