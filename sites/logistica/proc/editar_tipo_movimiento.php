<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dg_tipo_movimiento = $_POST['dg_tipo_movimiento_ed'];
$dg_codigo = $_POST['dg_codigo_ed'];
$dm_genera_asiento = $_POST['dm_genera_asiento_ed'];
$dc_tipo_movimiento = $_POST['dc_tipo_movimiento_ed'];

$datos = $db->prepare($db->select('tb_tipo_movimiento_logistico','true','dc_empresa = ? AND dg_tipo_movimiento = ? AND dc_tipo_movimiento = ?'));
$datos->bindValue(1,$empresa,PDO::PARAM_INT);
$datos->bindValue(2,$dg_tipo_movimiento,PDO::PARAM_STR);
$datos->bindValue(3,$dc_tipo_movimiento,PDO::PARAM_INT);
$db->stExec($datos);

/*if($datos->fetch() !== false){
	$error_man->showWarning("Ya existe ese nombre");
	exit; 
}*/

$db->start_transaction();

$editar = $db->prepare($db->update('tb_tipo_movimiento_logistico',array(
				'dg_tipo_movimiento' => '?',
				'dg_codigo' => '?',
				'dm_genera_asiento' => '?'),
				'dc_tipo_movimiento = ? AND dc_empresa = ?'));
$editar->bindValue(1,$dg_tipo_movimiento,PDO::PARAM_STR);
$editar->bindValue(2,$dg_codigo,PDO::PARAM_STR);
$editar->bindvalue(3,$dm_genera_asiento,PDO::PARAM_BOOL);
$editar->bindValue(4,$dc_tipo_movimiento,PDO::PARAM_INT);
$editar->bindValue(5,$empresa,PDO::PARAM_INT);
$db->stExec($editar);

$db->commit();


$error_man->showConfirm('Se ha modificado correctamente'); 

?>