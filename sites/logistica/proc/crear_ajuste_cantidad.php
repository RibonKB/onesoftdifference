<?php
define("MAIN",1);
require_once("../../../inc/init.php");
require_once("../../../sites/ventas/proc/ventas_functions.php");

/**
*	Validar que el producto haya sido ingresado correctamente
*
*	IN: post-dc_producto
*	OUT: dc_producto
**/
$dc_producto = intval($_POST['dc_producto']);
if($dc_producto == 0){
	$error_man->showWarning("El producto no ha sido seleccionado correctamente.");
	exit;
}

$producto = $db->prepare($db->select('tb_producto','dq_precio_compra','dc_producto = ? AND dc_empresa = ?'));
$producto->bindValue(1,$dc_producto,PDO::PARAM_INT);
$producto->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($producto);
$producto = $producto->fetch(PDO::FETCH_OBJ);

if($producto === false){
	$error_man->showWarning('El producto seleccionado no fue encontrado, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

/**
*	Validar cantidad numérica y positiva.
*
*	IN: post-dc_cantidad
*	OUT: dc_cantidad
**/

$dc_cantidad = $_POST['dc_cantidad'];

if(!is_numeric($dc_cantidad)){
	$error_man->showWarning("La cantidad debe ser un número entero y siempre positivo.");
	exit;
}

$dc_cantidad = intval($dc_cantidad);

if($dc_cantidad < 1){
	$error_man->shoWarning("La cantidad debe ser siempre positiva, en caso de querer hacer ajustes negativos indiquelo en el tipo de ajuste.");
	exit;
}

/**
*	Obtener tipo de movimiento configurado para el tipo de ajuste y validar que esté configurado
*
*	IN: post-dm_tipo_ajuste
*	OUT: dm_tipo_ajuste, dc_tipo_movimiento
**/

$dm_tipo_ajuste = intval($_POST['dm_tipo_ajuste']);

if($dm_tipo_ajuste == 1){
	$dc_tipo_movimiento = $db->doQuery($db->select('tb_configuracion_logistica','dc_tipo_movimiento_ajuste_entrada',"dc_empresa={$empresa}"))
							->fetch(PDO::FETCH_OBJ)
							->dc_tipo_movimiento_ajuste_entrada;
}else if($dm_tipo_ajuste == -1){
	$dc_tipo_movimiento = $db->doQuery($db->select('tb_configuracion_logistica','dc_tipo_movimiento_ajuste_salida',"dc_empresa={$empresa}"))
							->fetch(PDO::FETCH_OBJ)
							->dc_tipo_movimiento_ajuste_salida;
}else{
	exit;
}

if($dc_tipo_movimiento == 0 || $dc_tipo_movimiento === false){
	$error_man->showAviso("Los tipos de operación no han sido configurados para los ajustes, no puede continuar hasta que estos sean configurados.<br />
	Contacte con un administrador.");
	exit;
}


$db->start_transaction();
/**
*	Ingresar/sacar stock desde bodega
*
*	IN: dc_bodega, dc_producto, dc_cantidad
**/

$dc_bodega = intval($_POST['dc_bodega']);

if($dc_bodega == 0){
	$error_man->showWarning("La bodega indicada es inválida.");
	exit;
}

//Intentar ingresar/sacar stock bodega
if($dm_tipo_ajuste == 1){
	bodega_CargarStockLibre($dc_producto,$dc_cantidad,$dc_bodega);
}else if($dm_tipo_ajuste == -1){
	$stock_disponible = bodega_RebajarStockLibre($dc_producto,$dc_cantidad,$dc_bodega);

	if($stock_disponible != 1){
		$error_man->showWarning("No hay stock disponible para quitar de la bodega");
		$db->rollBack();
		exit;
	}
}


$dc_nota_venta = $_POST['dc_nota_venta'];
$dc_orden_servicio = $_POST['dc_orden_servicio'];

/**
*	Obtener número para comprobante de ajuste
**/

$dq_comprobante_ajuste = doc_GetNextNumber('tb_comprobante_ajuste_logistico','dq_comprobante');

/**
*	Ingresar comprobante de ajuste
*
*	IN:	dc_producto, dc_cantidad, dc_tipo_movimiento, dc_nota_venta, dc_orden_servicio, dq_comprobante_ajuste
*	OUT:dc_comprobante_ajuste
**/

$dc_comprobante_ajuste = $db->prepare($db->insert('tb_comprobante_ajuste_logistico',array(
	'dq_comprobante' => $dq_comprobante_ajuste,
	'dc_tipo_movimiento' => $dc_tipo_movimiento,
	'dc_producto' => $dc_producto,
	'dc_cantidad' => $dm_tipo_ajuste*$dc_cantidad,
	'dc_nota_venta' => '?',
	'dc_orden_servicio' => '?',
	'dg_comentario' => '?',
	'dq_monto' => '?',
	'dc_bodega' => '?',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa,
	'df_creacion' => $db->getNow(),
	'df_emision' => '?',
)));

$dc_comprobante_ajuste->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$dc_comprobante_ajuste->bindValue(2,$dc_orden_servicio,PDO::PARAM_INT);
$dc_comprobante_ajuste->bindValue(3,$_POST['dg_comentario'],PDO::PARAM_STR);
$dc_comprobante_ajuste->bindValue(4,$_POST['dq_monto']*$_POST['dq_cambio'],PDO::PARAM_STR);
$dc_comprobante_ajuste->bindValue(5,$dc_bodega,PDO::PARAM_INT);
$dc_comprobante_ajuste->bindValue(6,$db->sqlDate2($_POST['df_emision']),PDO::PARAM_STR);

$db->stExec($dc_comprobante_ajuste);

$dc_comprobante_ajuste = $db->lastInsertId();

/**
*	Registrar movimientos de bodega
*
*	IN: dc_producto, dc_cantidad, dc_tipo_movimiento, dc_nota_venta, dc_orden_servicio
**/

$ingresar_movimiento = $db->prepare($db->insert('tb_movimiento_bodega',array(
	'dc_tipo_movimiento' => $dc_tipo_movimiento,
	'dc_bodega_entrada' => $dm_tipo_ajuste == 1?$dc_bodega:0,
	'dc_bodega_salida' => $dm_tipo_ajuste == -1?$dc_bodega:0,
	'dc_nota_venta' => $dc_nota_venta,
	'dc_orden_servicio' => $dc_orden_servicio,
	'dc_producto' => $dc_producto,
	'dq_cantidad' => $dc_cantidad*$dm_tipo_ajuste,
	'dc_empresa' => $empresa,
	'df_creacion' => $db->getNow(),
	'dc_usuario_creacion' => $idUsuario,
	'dq_monto' => '?',
	'dq_cambio' => '?',
	'dc_comprobante_ajuste_logistico' => '?'
)));

$ingresar_movimiento->bindValue(1,$producto->dq_precio_compra,PDO::PARAM_STR);
$ingresar_movimiento->bindValue(2,$_POST['dq_cambio'],PDO::PARAM_STR);
$ingresar_movimiento->bindValue(3,$dc_comprobante_ajuste,PDO::PARAM_INT);

$db->stExec($ingresar_movimiento);

/**
*	Confirmación creación comprobante ajuste
*
*	IN: dq_comprobante_ajuste
**/

$error_man->showConfirm("Se ha realizado el ajuste de cantidad correctamente y se ha emitido un comprobante de ajuste");
echo("<div class='title'>El número de comprobante es <h1 style='margin:0;color:#000;'>{$dq_comprobante_ajuste}</h1></div>");


$db->commit();

?>
