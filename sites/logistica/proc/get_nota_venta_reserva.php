<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_nota_venta = intval($_POST['id']);


$nota_venta = $db->prepare($db->select(
					'tb_nota_venta nv
					 JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente',
					'nv.dq_nota_venta, cl.dg_rut, cl.dg_razon',
					'dc_nota_venta = ?')
				);
$nota_venta->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$db->stExec($nota_venta);
$nota_venta = $nota_venta->fetch(PDO::FETCH_OBJ);

if($nota_venta === false){
	$error_man->showWarning('La nota de venta que ha ingresado es inválida, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

$detalle_nota_venta = $db->prepare($db->select(
							'tb_nota_venta_detalle d
							 JOIN tb_producto p ON p.dc_producto = d.dc_producto
							 JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
							'd.dc_nota_venta_detalle, d.dq_cantidad, d.dc_recepcionada, d.dc_despachada, p.dc_producto, p.dg_codigo, p.dg_producto',
							'd.dc_nota_venta = ? AND t.dm_controla_inventario = 0 AND d.dc_despachada < d.dc_recepcionada'));
$detalle_nota_venta->bindValue(1,$dc_nota_venta,PDO::PARAM_INT);
$db->stExec($detalle_nota_venta);
$detalle_nota_venta = $detalle_nota_venta->fetchAll(PDO::FETCH_OBJ);

if(count($detalle_nota_venta) == 0){
	$error_man->showAviso('No hay productos en la nota de venta disponibles para liberación');
	exit;
}

?>
<table class="tab" width="90%" align="center">
	<caption>Productos Disponibles para liberar nota de venta <b><?php echo $nota_venta->dq_nota_venta ?></b></caption>
	<thead>
		<th>Código</th>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>Disponible para liberar</th>
		<th>Cantidad a Liberar</th>
	</thead>
	<tbody>
	<?php foreach($detalle_nota_venta as $detalle): ?>
		<tr>
			<td><?php echo $detalle->dg_codigo ?></td>
			<td><?php echo $detalle->dg_producto ?></td>
			<td><?php echo $detalle->dq_cantidad ?></td>
			<td><?php echo $detalle->dc_recepcionada - $detalle->dc_despachada ?></td>
			<td>
				<input type="text" class="inputtext" name="dc_cantidad[]" value="<?php echo $detalle->dc_recepcionada - $detalle->dc_despachada ?>" />
				<input type="hidden" name="dc_maximo[]" value="<?php echo $detalle->dc_recepcionada - $detalle->dc_despachada ?>">
				<input type="hidden" name="dc_producto[]" value="<?php echo $detalle->dc_producto ?>" />
				<input type="hidden" name="dc_detalle[]" value="<?php echo $detalle->dc_nota_venta_detalle ?>" />
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>