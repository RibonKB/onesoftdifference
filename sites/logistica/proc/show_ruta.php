<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_ruta = intval($_POST['id']);

$data = $db->select('tb_ruta r
JOIN tb_tipo_ruta tr ON tr.dc_tipo = r.dc_tipo_ruta',
'r.dq_ruta, r.df_ruta, r.df_emision, r.dm_nula, tr.dg_tipo',
"r.dc_ruta = {$dc_ruta} AND r.dc_empresa = {$empresa}");

if(!count($data)){
	$error_man->showWarning("No se encontró la ruta especificada, compruebe los datos de entrada");
	exit;
}
$data = $data[0];

$detalle = $db->select('tb_ruta_detalle d
JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
JOIN tb_producto p ON p.dc_producto = nvd.dc_producto
JOIN tb_nota_venta nv ON nv.dc_nota_venta = nvd.dc_nota_venta
JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente',
'd.dq_cantidad, d.dt_hora, nvd.dg_descripcion, p.dg_codigo, nv.dq_nota_venta, cl.dg_razon',
"d.dc_ruta = {$dc_ruta} AND d.dm_activo = 1");

?>
<div class='title center'>Ruta Nº <?php echo $data['dq_ruta'] ?></div>
<table class="tab" width="100%" style="text-align:left;">
<tbody>
	<tr>
		<td width="150">Fecha de ruta</td>
		<td><b><?php echo $db->dateLocalFormat($data['df_ruta']) ?></b></td>
	</tr>
	<tr>
		<td>Fecha de emisión</td>
		<td><b><?php echo $db->dateTimeLocalFormat($data['df_emision']) ?></b></td>
	</tr>
	<tr>
		<td>Tipo de ruta</td>
		<td><b><?php echo $data['dg_tipo'] ?></b></td>
	</tr>
</tbody>
</table>
<br />
<table class="tab" width="100%" style="text-align:left;">
<caption>Detalle de Ruta</caption>
<thead>
	<tr>
		<th>Hora</th>
		<th>Código</th>
		<th>Producto</th>
		<th>Cantidad</th>
		<th>NV</th>
		<th>Cliente</th>
	</tr>
</thead>
<tbody>
<?php foreach($detalle as $d): ?>
	<tr>
		<td><?php echo substr($d['dt_hora'],0,5) ?></td>
		<td><b><?php echo $d['dg_codigo'] ?></b></td>
		<td><?php echo $d['dg_descripcion'] ?></td>
		<td align="right"><?php echo $d['dq_cantidad'] ?></td>
		<td><b><?php echo $d['dq_nota_venta'] ?></b></td>
		<td><?php echo $d['dg_razon'] ?></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
<script type="text/javascript">
$('#null_ruta').unbind('click').click(function(){
	loadOverlay('sites/logistica/null_ruta.php?id=<?php echo $dc_ruta ?>');
});
$('#edit_ruta').unbind('click').click(function(){
	loadOverlay('sites/logistica/ed_ruta.php?id=<?php echo $dc_ruta ?>');
});
$('#historial_ruta').unbind('click').click(function(){
	loadOverlay('sites/logistica/src_historial_edicion_ruta.php?id=<?php echo $dc_ruta ?>');
});
</script>