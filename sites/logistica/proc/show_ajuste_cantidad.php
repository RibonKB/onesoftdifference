<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_comprobante_ajuste_logistico WHERE dc_comprobante = {$_POST['id']} AND dc_empresa={$empresa}) ac
JOIN tb_tipo_movimiento_logistico tm ON tm.dc_tipo_movimiento = ac.dc_tipo_movimiento
JOIN tb_producto p ON p.dc_producto = ac.dc_producto
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = ac.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = ac.dc_orden_servicio
JOIN tb_bodega b ON b.dc_bodega = ac.dc_bodega
JOIN tb_usuario u ON u.dc_usuario = ac.dc_usuario_creacion
JOIN tb_funcionario f ON f.dc_funcionario = u.dc_funcionario",
'ac.dq_comprobante, tm.dg_tipo_movimiento, p.dg_codigo, p.dg_producto, ac.dc_cantidad, b.dg_bodega, nv.dq_nota_venta, os.dq_orden_servicio, ac.dg_comentario,
f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, DATE_FORMAT(ac.df_emision,"%d/%m/%Y %H:%i") df_emision');

if(!count($data)){
	$error_man->showWarning("No se ha encontrado el comprobante de ajuste de cantidad especificado");
	exit();
}
$data = $data[0];

echo("<div class='title center'>Comprobante Nº {$data['dq_comprobante']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<tr>
	<td width='160'>Fecha emision</td>
	<td><b>{$data['df_emision']}</b></td>
</tr><tr>
	<td>Tipo de movimiento</td>
	<td><b>{$data['dg_tipo_movimiento']}</b></td>
</tr><tr>
	<td>Producto</td>
	<td><b>({$data['dg_codigo']}) <i>{$data['dg_producto']}</i></b></td>
</tr><tr>
	<td>Cantidad</td>
	<td><b>{$data['dc_cantidad']}</b></td>
</tr><tr>
	<td>Bodega</td>
	<td><b>{$data['dg_bodega']}</b></td>
</tr><tr>
	<td>Responsable</td>
	<td><b>{$data['dg_nombres']} {$data['dg_ap_paterno']} {$data['dg_ap_materno']}</b></td>
</tr><tr>
	<td>Nota de Venta</td>
	<td><b>{$data['dq_nota_venta']}</b></td>
</tr><tr>
	<td>Orden de servicio</td>
	<td><b>{$data['dq_orden_servicio']}</b></td>
</tr><tr>
	<td>Observacion</td>
	<td>{$data['dg_comentario']}</td>
</tr></table>");

?>
<script type="text/javascript">
/*$('#print_version').unbind('click').click(function(){
	window.open("sites/ventas/orden_compra/ver_orden_compra.php?id=<?=$_POST['id'] ?>&v=0",'print_orden_compra','width=800;height=600');
});
$('#edit_orden_compra').unbind('click').click(function(){
	loadpage("sites/ventas/orden_compra/ed_orden_compra.php?id=<?=$_POST['id'] ?>");
});
$('#null_orden_compra').unbind('click').click(function(){
	loadOverlay("sites/ventas/orden_compra/null_orden_compra.php?id=<?=$_POST['id'] ?>");
});*/
</script>