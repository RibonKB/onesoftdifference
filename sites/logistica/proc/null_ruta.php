<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$dc_ruta = intval($_POST['id_ruta']);

$db->start_transaction();

$db->update('tb_ruta',array('dm_nula'=>1),"dc_ruta={$dc_ruta}");

$db->insert('tb_ruta_anulada',array(
	'dc_ruta' => $dc_ruta,
	'dc_motivo' => $_POST['null_motivo'],
	'dg_comentario' => $_POST['null_comentario'],
	'dc_usuario' => $idUsuario,
	'df_anulacion' => 'NOW()'
));

$db->update('tb_ruta_detalle d
JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta',
array(
	'nvd.dc_rutada' => 'nvd.dc_rutada - d.dq_cantidad'
),"d.dc_ruta = {$dc_ruta} AND dm_activo = 1");

$db->commit();

$error_man->showAviso("Atención a anulado la ruta y las relaciones con otros documentos");
?>
<script type="text/javascript">
$('#res_list .confirm .rt_load').trigger('click');
</script>