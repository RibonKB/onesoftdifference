<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_movimiento_bodega WHERE dc_producto = {$_POST['id']}) mov
LEFT JOIN tb_cliente cl ON cl.dc_cliente = mov.dc_cliente
LEFT JOIN tb_proveedor prov ON prov.dc_proveedor = mov.dc_proveedor
LEFT JOIN tb_tipo_movimiento_logistico tm ON tm.dc_tipo_movimiento = mov.dc_tipo_movimiento
LEFT JOIN tb_bodega bin ON bin.dc_bodega = mov.dc_bodega_entrada
LEFT JOIN tb_bodega bout ON bout.dc_bodega = mov.dc_bodega_salida
LEFT JOIN tb_guia_recepcion gr ON gr.dc_guia_recepcion = mov.dc_guia_recepcion
LEFT JOIN tb_guia_despacho gd ON gd.dc_guia_despacho = mov.dc_guia_despacho
LEFT JOIN tb_orden_compra oc ON oc.dc_orden_compra = mov.dc_orden_compra
LEFT JOIN tb_factura_venta fv ON fv.dc_factura = mov.dc_factura
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = mov.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = mov.dc_orden_servicio
LEFT JOIN tb_orden_servicio os2 ON os2.dc_orden_servicio = gd.dc_orden_servicio
LEFT JOIN tb_cebe cbe ON cbe.dc_cebe = mov.dc_cebe
LEFT JOIN tb_ceco cco ON cco.dc_ceco = mov.dc_ceco",
'cl.dg_razon dg_cliente, prov.dg_razon dg_proveedor,tm.dg_tipo_movimiento, bin.dg_bodega dg_bodega_entrada, bout.dg_bodega dg_bodega_salida,
mov.dq_monto/(mov.dq_cantidad/ABS(mov.dq_cantidad)) dq_monto, mov.dq_cambio, gr.dq_guia_recepcion, gd.dq_guia_despacho, mov.dg_series, oc.dq_orden_compra, fv.dq_factura, nv.dq_nota_venta, cbe.dg_cebe,
cco.dg_ceco, mov.dq_cantidad, DATE_FORMAT(mov.df_creacion,"%d/%m/%Y") df_creacion, os.dq_orden_servicio, os2.dq_orden_servicio dq_orden_servicio_02');

echo('<table class="tab" width="100"><thead><tr>
	<th>Fecha</th>
	<th>Cliente</th>
	<th>Proveedor</th>
	<th>Tipo movimiento</th>
	<th>Bodega entrada</th>
	<th>Bodega Salida</th>
	<th>Monto</th>
	<th>Cambio</th>
	<th>Guía de recepción</th>
	<th>Guía de despacho</th>
	<th>Series</th>
	<th>Orden de compra</th>
	<th>Factura de venta</th>
	<th>Nota de venta</th>
	<th>Orden de Servicio</th>
	<th>CeBe</th>
	<th>CeCo</th>
	<th>Cantidad</th>
</tr></thead><tbody>');

foreach($data as $d){
	$d['dq_monto'] = moneda_local($d['dq_monto']);
	$d['dq_cambio'] = moneda_local($d['dq_cambio']);
	$d['dq_cantidad'] = moneda_local($d['dq_cantidad']);
	
	echo("<tr>
		<td>{$d['df_creacion']}</td>
		<td>{$d['dg_cliente']}</td>
		<td>{$d['dg_proveedor']}</td>
		<td>{$d['dg_tipo_movimiento']}</td>
		<td>{$d['dg_bodega_entrada']}</td>
		<td>{$d['dg_bodega_salida']}</td>
		<td>{$d['dq_monto']}</td>
		<td>{$d['dq_cambio']}</td>
		<td>{$d['dq_guia_recepcion']}</td>
		<td>{$d['dq_guia_despacho']}</td>
		<td>{$d['dg_series']}</td>
		<td>{$d['dq_orden_compra']}</td>
		<td>{$d['dq_factura']}</td>
		<td>{$d['dq_nota_venta']}</td>
		<td>{$d['dq_orden_servicio']} {$d['dq_orden_servicio_02']}</td>
		<td>{$d['dg_cebe']}</td>
		<td>{$d['dg_ceco']}</td>
		<td>{$d['dq_cantidad']}</td>
	</tr>");
}

echo('</tbody></table>');
?>