<?php
define("MAIN",1);
require_once("../../../inc/init.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['dc_detalle'])){
	$error_man->showWarning("No elimine todos los detalles de la ruta de esta manera, para ello utilice la opción de anular Ruta.");
	exit;
}

$db->start_transaction();

$dc_ruta = $_POST['dc_ruta'];
$update_ruta = $db->prepare($db->update('tb_ruta',array(
	'df_ruta' => $db->sqlDate($_POST['ruta_fecha']),
	'dc_tipo_ruta' => '?'
),"dc_ruta = ?"));
$update_ruta->bindValue(1,$_POST['ruta_tipo'],PDO::PARAM_INT);
$update_ruta->bindValue(2,$dc_ruta,PDO::PARAM_INT);

$db->stExec($update_ruta);

if(isset($_POST['dc_detalle_to_delete'])){
	$in_dc_detalle = implode(',',$_POST['dc_detalle_to_delete']);
	
	$db->doExec($db->update('tb_ruta_detalle',array(
		'dm_activo' => 0
	),"dc_detalle IN ({$in_dc_detalle})"));
	
	$db->doExec("UPDATE tb_nota_venta_detalle d
	JOIN tb_ruta_detalle r ON r.dc_detalle_nota_venta = d.dc_nota_venta_detalle
	SET	d.dc_rutada = d.dc_rutada - r.dq_cantidad
	WHERE r.dc_detalle IN ({$in_dc_detalle})");
	
}

$update_detail = $db->prepare($db->update('tb_ruta_detalle',array('dm_activo' => 0),"dc_detalle = ? AND (dt_hora <> ? OR dq_cantidad <> ?)"));
$update_detail->bindParam(1,$dc_detalle,PDO::PARAM_INT);
$update_detail->bindParam(2,$dt_hora,PDO::PARAM_STR);
$update_detail->bindParam(3,$dq_cantidad,PDO::PARAM_INT);

$insert_updated = $db->prepare("INSERT INTO tb_ruta_detalle(dc_detalle_nota_venta,dc_ruta,dq_cantidad,dt_hora)
SELECT dc_detalle_nota_venta, dc_ruta, ?, ? FROM tb_ruta_detalle WHERE dc_detalle = ?");
$insert_updated->bindParam(1,$dq_cantidad,PDO::PARAM_INT);
$insert_updated->bindParam(2,$dt_hora,PDO::PARAM_STR);
$insert_updated->bindParam(3,$dc_detalle,PDO::PARAM_INT);

$update_nv_detail = $db->prepare($db->update('tb_nota_venta_detalle',array(
	'dc_rutada' => 'dc_rutada + ?'
),"dc_nota_venta_detalle = ?"));
$update_nv_detail->bindParam(1,$dc_result,PDO::PARAM_INT);
$update_nv_detail->bindParam(2,$dc_detalle_nota_venta,PDO::PARAM_INT);

foreach($_POST['dc_detalle'] as $i => $v){
	$dc_detalle = intval($v);
	$dt_hora = $_POST['dt_hora'][$i];
	$dq_cantidad = intval($_POST['dq_cantidad'][$i]);
	
	$db->stExec($update_detail);
	
	if($update_detail->rowCount() == 1){
		
		$dc_result = $dq_cantidad - intval($_POST['dq_cantidad_old'][$i]);
		
		if($dc_result != 0){
			$dc_detalle_nota_venta = intval($_POST['dc_detalle_nota_venta'][$i]);
			$db->stExec($update_nv_detail);
		}

		$db->stExec($insert_updated);
	}
}

$db->commit();

$error_man->showConfirm("Se ha modificado la Ruta correctamente.");

?>
<script type="text/javascript">
	$('#res_list .confirm .rt_load').trigger('click');
</script>