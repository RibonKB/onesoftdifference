<?php
define("MAIN",1);
require_once("../../../inc/global.php");

$db->escape($_GET['id']);

$detalle = $db->select("(SELECT * FROM tb_guia_despacho_detalle WHERE dc_guia_despacho={$_GET['id']}) d
JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}",
'd.dq_cantidad,d.dg_producto,d.dg_descripcion,d.dg_serie,d.dc_detalle,d.dc_detalle_nota_venta,d.dc_bodega_entrada,d.dc_bodega_salida,p.dc_producto');

//$detalle = $db->select("tb_guia_despacho_detalle",'dc_detalle,dg_producto,dg_descripcion,dc_detalle_nota_venta',"dc_guia_despacho={$_GET['id']}");

if(!count($detalle)){
	echo json_encode('<empty>');
	exit();
}

echo json_encode($detalle);
?>