<?php
define("MAIN",1);
require_once("../../../../inc/init.php");
require_once('../../../ventas/proc/ventas_functions.php');


//Validar Folio de guía de despacho
$validar_folio = $db->prepare($db->select('tb_guia_despacho_proveedor','true','dq_folio = ? AND dc_empresa = ?'));
$validar_folio->bindValue(1,$_POST['gd_number'],PDO::PARAM_INT);
$validar_folio->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($validar_folio);
$validar_folio = $validar_folio->fetch(PDO::FETCH_OBJ);

if($validar_folio != false){
	$error_man->showWarning("Ya hay otra guía de despacho utilizando el folio <b>{$_POST['gd_number']}</b>");
	exit;
}

//Validar tipo de guía de despacho
$tipo_guia = $db->prepare($db->select('tb_tipo_guia_despacho_proveedor','dc_tipo_movimiento','dc_tipo = ? AND dc_empresa = ?'));
$tipo_guia->bindValue(1,$_POST['dc_tipo_guia'],PDO::PARAM_INT);
$tipo_guia->bindValue(2,$empresa,PDO::PARAM_INT);
$db->stExec($tipo_guia);
$tipo_guia = $tipo_guia->fetch(PDO::FETCH_OBJ);

if($tipo_guia === false){
	$error_man->showWarning('El tipo de guía seleccionado s inválido, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

//Obtener datos de productos que controlan inventario.
$vars = substr(str_repeat(',?',count($_POST['prod'])),1);
$stock_prods = $db->prepare(
					$db->select('tb_producto p
								 JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
								'p.dc_producto, p.dg_codigo, p.dq_precio_compra',
								"p.dc_empresa = ? AND t.dm_controla_inventario = 0 AND p.dg_codigo IN ({$vars})"
								)
							);
$stock_prods->bindValue(1,$empresa,PDO::PARAM_INT);
foreach($_POST['prod'] as $i => $dg_codigo){
	$stock_prods->bindValue($i+2,$dg_codigo,PDO::PARAM_STR);
}
$db->stExec($stock_prods);

//Agrupar productos que controlan inventario
$dc_producto_stock = array();
$dq_costo_stock = array();
while($producto = $stock_prods->fetch(PDO::FETCH_OBJ)){
	$dc_producto_stock[$producto->dg_codigo] = $producto->dc_producto;
	$dq_costo_stock[$producto->dc_producto] = $producto->dq_precio_compra;
}

//Comprobar que hay stock libre suficiente para los productos que mueven stock
foreach($_POST['prod'] as $i => $dg_codigo){
	if(isset($dc_producto_stock[$dg_codigo])){
		
		$dq_cantidad = intval($_POST['cant'][$i]); //La cantidad solicitada
		$dc_bodega = intval($_POST['bodega_salida'][$i]); //La bodega en la que debería estar
		
		$stock = bodega_ComprobarTotalStock($dc_producto_stock[$dg_codigo],$dc_bodega); //array(stock_libre, stock_nv, stock_os)
		
		if($stock[0] < $dq_cantidad){ //¿Hay suficiente stock libre?
			$error_man->showWarning("No hay stock libre suficiente para despachar el producto <b>{$dg_codigo}</b>");
			exit;
		}
		
	}
}

$db->start_transaction(); //Comenzar transacción en la base de datos.
	
	/**
		Almacenar cabecera de guía de despacho
	*/
	
	//Obtener correlativo interno para guía
	$dq_guia_despacho = doc_GetNextNumber('tb_guia_despacho_proveedor','dq_guia_despacho',2);
	
	//Preparar inserción
	$insert_guia = $db->prepare($db->insert('tb_guia_despacho_proveedor',array(
		'dc_tipo_guia' => '?',
		'dc_proveedor' => '?',
		'dc_contacto' => '?',
		'dc_contacto_entrega' => '?',
		'dq_guia_despacho' => '?',
		'dq_folio' => '?',
		'dg_guia_referencia' => '?',
		'dg_comentario' => '?',
		'df_emision' => '?',
		'dq_neto' => '?',
		'dq_iva' => '?',
		'dq_total' => '?',
		'dc_factura' => '?',
		'dg_orden_compra' => '?',
		'df_creacion' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario
	)));
	$insert_guia->bindValue(1,$_POST['dc_tipo_guia'],PDO::PARAM_INT);
	$insert_guia->bindValue(2,$_POST['dc_proveedor'],PDO::PARAM_INT);
	$insert_guia->bindValue(3,$_POST['gd_contacto'],PDO::PARAM_INT);
	$insert_guia->bindValue(4,$_POST['gd_cont_entrega'],PDO::PARAM_INT);
	$insert_guia->bindValue(5,$dq_guia_despacho,PDO::PARAM_INT);
	$insert_guia->bindValue(6,$_POST['gd_number'],PDO::PARAM_INT);
	$insert_guia->bindValue(7,$_POST['gd_guia_referencia'],PDO::PARAM_INT);
	$insert_guia->bindValue(8,$_POST['gd_comentario'],PDO::PARAM_STR);
	$insert_guia->bindValue(9,$db->sqlDate2($_POST['gd_emision']),PDO::PARAM_STR);
	$insert_guia->bindValue(10,floatval($_POST['cot_neto']),PDO::PARAM_STR);
	$insert_guia->bindValue(11,floatval($_POST['cot_iva']),PDO::PARAM_STR);
	$insert_guia->bindValue(12,floatval($_POST['cot_neto'])+floatval($_POST['cot_iva']),PDO::PARAM_STR);
	$insert_guia->bindValue(13,$_POST['dc_factura'],PDO::PARAM_INT);
	$insert_guia->bindValue(14,$_POST['gd_orden_compra'],PDO::PARAM_STR);
	
	//Insertar guía de despacho
	$db->stExec($insert_guia);
	
	//Obtener id de la guía de despacho generada
	$dc_guia_despacho = $db->lastInsertId();
	
	/**
	*	Insertar detalles de guía de despacho
	*/
	
	//Preparar inserción de detalle
	$insert_detalle = $db->prepare($db->insert('tb_guia_despacho_proveedor_detalle',array(
		'dc_guia_despacho' => $dc_guia_despacho,
		'dg_producto' => '?',
		'dg_descripcion' => '?',
		'dq_precio' => '?',
		'dq_costo' => '?',
		'dq_cantidad' => '?',
		'dc_bodega_salida' => '?'
	)));
	$insert_detalle->bindParam(1,$dg_producto,PDO::PARAM_STR);
	$insert_detalle->bindParam(2,$dg_descripcion,PDO::PARAM_STR);
	$insert_detalle->bindParam(3,$dq_precio,PDO::PARAM_STR);
	$insert_detalle->bindParam(4,$dq_costo,PDO::PARAM_STR);
	$insert_detalle->bindParam(5,$dq_cantidad,PDO::PARAM_INT);
	$insert_detalle->bindParam(6,$dc_bodega,PDO::PARAM_INT);
	
	//Preparar inserción de series
	$insert_serie = $db->prepare($db->insert('tb_guia_despacho_proveedor_serie',array(
		'dc_detalle' => '?',
		'dg_serie' => '?'
	)));
	$insert_serie->bindParam(1,$dc_detalle,PDO::PARAM_INT);
	$insert_serie->bindParam(2,$dg_serie,PDO::PARAM_STR);
	
	foreach($_POST['prod'] as $i => $dg_codigo){
 		
		//Agrupar series
		$series = explode(',',$_POST['serie'][$i]);
			if(count($series) == 1 && $series[0] == ''){
			$series = array();
		}
		
		$dg_producto = $dg_codigo;
		$dg_descripcion = $_POST['desc'][$i];
		$dq_precio = floatval(str_replace(',','',$_POST['precio'][$i])); //formato de precio
		$dq_costo = floatval(str_replace(',','',$_POST['costo'][$i])); //Formato de costo
		$dq_cantidad = intval($_POST['cant'][$i]);
		$dc_bodega = intval($_POST['bodega_salida'][$i]);
		
		$db->stExec($insert_detalle); //Almacenar detalle
		
		$dc_detalle = $db->lastInsertId(); //Obtener id del detalle del id recién insertado
		
		//Almacenar series para el detalle actual
		foreach($series as $dg_serie){
			$db->stExec($insert_serie);
		}
		
	}
	
	/**
	*	Generar descuentos de stock
	*/
	
	//Preparar inserción de movimientos de bodega
	$insert_movimiento = $db->prepare($db->insert('tb_movimiento_bodega',array(
			'dc_proveedor' => '?',
			'dc_tipo_movimiento' => '?',
			'dc_bodega_salida' => '?',
			'dq_monto' => '?',
			'dc_guia_despacho' => $dc_guia_despacho,
			'dg_series' => '?',
			'dc_producto' => '?',
			'dq_cantidad' => '?',
			'dc_empresa' => $empresa,
			'df_creacion' => $db->getNow(),
			'dc_usuario_creacion' => $idUsuario
		)));
	$insert_movimiento->bindValue(1,$_POST['dc_proveedor'],PDO::PARAM_INT);
	$insert_movimiento->bindValue(2,$tipo_guia->dc_tipo_movimiento,PDO::PARAM_INT);
	$insert_movimiento->bindParam(3,$dc_bodega,PDO::PARAM_INT);
	$insert_movimiento->bindParam(4,$dq_monto,PDO::PARAM_STR);
	$insert_movimiento->bindParam(5,$dg_series,PDO::PARAM_STR);
	$insert_movimiento->bindParam(6,$dc_producto,PDO::PARAM_INT);
	$insert_movimiento->bindParam(7,$dq_cantidad,PDO::PARAM_INT);

	//Rebajar Stock libre para los productos que controlan inventario
	foreach($_POST['prod'] as $i => $dg_codigo){
		
		if(isset($dc_producto_stock[$dg_codigo])){
			$dc_bodega = intval($_POST['bodega_salida'][$i]); //Bodega de la que se despacha
			$dq_cantidad = intval($_POST['cant'][$i]); //Cantidad a Despachar
			$dc_producto = $dc_producto_stock[$dg_codigo];
			$dq_monto = floatval($dq_costo_stock[$dc_producto]);
			$dg_series = $_POST['serie'][$i];
			
			$in_stock = bodega_RebajarStockLibre($dc_producto,$dq_cantidad,$dc_bodega);
			
			if($in_stock != 1){ //Segunda comprobación de salida de stock, si no hay produce error fatal
				$error_man->showWarning("Hubo un error al intentar rebajar el stock del producto <b>{$dg_codigo}</b>,
										 hubo movimientos mientras se intentaba generar la guía de despacho, no se puede continuar.");
				$db->rollBack();
				exit;
			}
			
			$db->stExec($insert_movimiento);
			
		}
		
	}

$db->commit(); //Confirmar transacción en la base de datos.

$error_man->showConfirm('Se ha generado la guía de despacho correctamente.');

?>
<div class="info">
	El número de guía generada es <h1 style='margin:0;color:#000;'><?php echo $dq_guia_despacho ?></h1>
</div>