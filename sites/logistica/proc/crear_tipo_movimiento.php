<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

$data = array(
	'dg_tipo_movimiento' => $_POST['tipo_name'],
	'dg_codigo' => $_POST['tipo_codigo'],
	'dm_genera_asiento' => $_POST['tipo_asiento'],
	'df_creacion' => 'NOW()',
	'dc_usuario_creacion' => $idUsuario,
	'dc_empresa' => $empresa
);

if($_POST['tipo_asiento'] == '1')
	$data['dc_tipo_movimiento_contable'] = $_POST['tipo_tipo_mov'];

$tipo_movimiento = $db->insert('tb_tipo_movimiento_logistico',$data);

if($_POST['tipo_asiento'] == '1'){
	if(!isset($_POST['tipo_debe']) || !isset($_POST['tipo_haber'])){
		$error_man->showWarning('Debe seleccionar al menos una cuenta contable para el DEBE y otra para el HABER');
		$db->rollback();
		exit();
	}
	
	foreach($_POST['tipo_debe'] as $cta){
		$db->insert('tb_cuentas_tipo_movimiento_logistico',array(
			'dc_tipo_movimiento' => $tipo_movimiento,
			'dc_cuenta_contable' => $cta,
			'dm_tipo' => '0'
		));
	}
	
	foreach($_POST['tipo_haber'] as $cta){
		$db->insert('tb_cuentas_tipo_movimiento_logistico',array(
			'dc_tipo_movimiento' => $tipo_movimiento,
			'dc_cuenta_contable' => $cta,
			'dm_tipo' => '1'
		));
	}
}

$error_man->showConfirm("Se ha creado el tipo de movimiento <b>{$_POST['tipo_name']}</b> correctamente");

$db->commit();

$date_now = date("d/m/Y H:i");
echo("<table style='display:none;'>
<tr id='item{$tipo_movimiento}'>
	<td>{$_POST['tipo_name']}</td>
	<td>{$date_now}</td>
	<td>
		<a href='sites/logistica/ed_tipo_movimiento.php?id={$tipo_movimiento}' class='loadOnOverlay left'>
			<img src='images/editbtn.png' alt='' title='Editar' />
		</a>
		<a href='sites/logistica/del_tipo_movimiento.php?id={$tipo_movimiento}' class='loadOnOverlay left'>
			<img src='images/delbtn.png' alt='' title='Eliminar' />
		</a>
	</td>
</tr>
</table>");

?>
<script type="text/javascript">
	$("#item<?=$tipo_movimiento ?>").appendTo("#list");

	$("a.loadOnOverlay").click(
	function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});
</script>