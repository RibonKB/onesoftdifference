<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$cond_productos = '';
if(isset($_POST['st_productos'])){
	$cond_productos = implode(',',$_POST['st_productos']);
	$cond_productos = "AND dc_producto IN ({$cond_productos})";
}

$cond_bodegas = '';
if(isset($_POST['st_bodegas'])){
	$cond_bodegas = implode(',',$_POST['st_bodegas']);
	$cond_bodegas = "AND dc_bodega IN ({$cond_bodegas})";
}

$bodegas = $db->select('tb_bodega','dc_bodega,dg_bodega',"dc_empresa={$empresa} AND dm_activo='1' {$cond_bodegas}");

$hseparator = '<th style="background:#444;width:1px;padding:0;" class="nochange"></th>';
$separator = '<td style="background:#444;width:1px;padding:0;" class="nochange"></td>';

if(!isset($_POST['no_wo_stock'])){
	$cant_cond = "s.dq_stock > 0";
}else{
	$cant_cond = '';
}

$data = $db->select("(SELECT * FROM tb_producto WHERE dc_empresa={$empresa} AND dm_activo = '1' {$cond_productos}) p
JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto AND dm_controla_inventario = 0
LEFT JOIN tb_stock s ON p.dc_producto = s.dc_producto {$cond_bodegas}",
'p.dc_producto,p.dg_codigo,p.dg_producto,p.dq_precio_compra,s.dq_stock,s.dq_stock_reservado,s.dq_stock_reservado_os,s.dc_bodega',$cant_cond);

$agruped = array();
foreach($data as $p){
	$agruped[$p['dc_producto']][$p['dc_bodega']] = array($p['dq_stock'],$p['dq_stock_reservado'],$p['dq_stock_reservado_os']);
	$agruped[$p['dc_producto']]['dg_codigo'] = $p['dg_codigo'];
	$agruped[$p['dc_producto']]['dg_producto'] = $p['dg_producto'];
	$agruped[$p['dc_producto']]['dq_precio_venta'] = $p['dq_precio_compra'];
}

echo('<table class="tab" width="100%">
<caption>Stock de productos</caption>
<thead><tr>
<th rowspan="2">Código</th>
<th rowspan="2" width="400">Producto/Bodega</th>');

$vertical_stock_total = array();
$vertical_cost_total = array();
foreach($bodegas as $b){
	echo("<th width='100' colspan='4'>{$b['dg_bodega']}</th>".$hseparator);
	$vertical_stock_total[$b['dc_bodega']] = 0;
	$vertical_cost_total[$b['dc_bodega']] = 0;
}

echo('<th rowspan="2">Costo Producto</th>
<th rowspan="2">Precio Total</th>
</tr><tr>');

foreach($bodegas as $b){
	echo('<th width="25">LB</th>
	<th width="25">NV</th>
	<th width="25">OS</th>
	<th width="25">T</th>'.$hseparator);
}

echo('</tr></thead><tbody>');

$precio_total = 0;

foreach($agruped as $dc_producto => $p){
	echo("<tr><td><b>
	<a href='sites/logistica/proc/src_producto_cartola.php?id={$dc_producto}' class='loadOnOverlay'>{$p['dg_codigo']}</a></b></td>
	<td><em>{$p['dg_producto']}</em></td>");
	
	$stock_total = 0;
	
	foreach($bodegas as $b){
		$stock = isset($p[$b['dc_bodega']])?$p[$b['dc_bodega']]:array(0,0,0);
		
		$libre = $stock[0]-$stock[1]-$stock[2];
		
		echo "<td align='right'>".$libre."</td>
			<td align='right'>".$stock[1]."</td>
			<td align='right'>".$stock[2]."</td>
			<td align='right'>".$stock[0]."</td>".$separator;
		
		$stock = $stock[0];
		
		$vertical_stock_total[$b['dc_bodega']] += $stock;
		$vertical_cost_total[$b['dc_bodega']] += $stock*$p['dq_precio_venta'];
		
		$stock_total += $stock;
	}
	
	$precio_subtotal = moneda_local($p['dq_precio_venta']*$stock_total);
	$precio_total += $p['dq_precio_venta']*$stock_total;
	$p['dq_precio_venta'] = moneda_local($p['dq_precio_venta']);
	echo("<td align='right'>{$p['dq_precio_venta']}</td>
	<td align='right'>{$precio_subtotal}</td>
	</tr>");
}

$precio_total = moneda_local($precio_total);

$colspan = 3+count($bodegas)*5;

echo("</tbody>
<tfoot>
	<tr>
		<th colspan='{$colspan}' align='right'>Totales</th>
		<th align='right'>{$precio_total}</th>
	</tr><tr><th colspan='2' align='right'>Stock Total</th>");
	
	foreach($bodegas as $b){
		echo("<th align='right' colspan='4'>{$vertical_stock_total[$b['dc_bodega']]}</th>".$hseparator);
	}
	
	echo('<th colspan="2"></th></tr><tr><th colspan="2">Costos por bodega</th>');
	
	foreach($bodegas as $b){
		$total = moneda_local($vertical_cost_total[$b['dc_bodega']]);
		echo("<th align='right' colspan='4'>{$total}</th>".$hseparator);
	}
	
echo('<th colspan="2"></th></tr></tfoot></table>');

?>
<script type="text/javascript">
$("a.loadOnOverlay").click(function(e){
	e.preventDefault();
	loadOverlay(this.href);
});
$(".tab tr:even td[class!=nochange]").css({"backgroundColor": "#E5E5E5"});
</script>