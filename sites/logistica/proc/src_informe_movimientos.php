<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$df_movimiento_desde = $db->sqlDate($_POST['df_movimiento_desde']);
$df_movimiento_hasta = $db->sqlDate($_POST['df_movimiento_hasta']);
$conditions = "((fv.df_creacion BETWEEN {$df_movimiento_desde} AND {$df_movimiento_hasta}) || (fc.df_creacion BETWEEN {$df_movimiento_desde} AND {$df_movimiento_hasta})) AND m.dc_empresa = {$empresa}";

if(isset($_POST['dc_producto'])){
	$dc_producto = implode(',',$_POST['dc_producto']);
	$conditions .= " AND m.dc_producto IN ({$dc_producto})";
}

$data = $db->prepare($db->select('tb_movimiento_bodega m
				JOIN tb_producto prod ON prod.dc_producto = m.dc_producto
				LEFT JOIN tb_cliente cl ON cl.dc_cliente = m.dc_cliente
				LEFT JOIN tb_proveedor pr ON pr.dc_proveedor = m.dc_proveedor
				LEFT JOIN tb_tipo_movimiento_logistico tm ON tm.dc_tipo_movimiento = m.dc_tipo_movimiento
				LEFT JOIN tb_bodega be ON be.dc_bodega = m.dc_bodega_entrada
				LEFT JOIN tb_bodega bs ON bs.dc_bodega = m.dc_bodega_salida
				LEFT JOIN tb_guia_recepcion gr ON gr.dc_guia_recepcion = m.dc_guia_recepcion
				LEFT JOIN tb_factura_compra fc ON fc.dc_factura = gr.dc_factura
				LEFT JOIN tb_guia_despacho gd ON m.dc_guia_despacho = gd.dc_guia_despacho
				LEFT JOIN tb_orden_compra oc ON oc.dc_orden_compra = m.dc_orden_compra
				LEFT JOIN tb_factura_venta fv ON gd.dc_factura = fv.dc_factura
				LEFT JOIN tb_nota_credito nc ON nc.dc_nota_credito = m.dc_nota_credito
				LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = m.dc_nota_venta
				LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio =m.dc_orden_servicio
                                LEFT JOIN tb_orden_servicio os2 ON os2.dc_orden_servicio = gd.dc_orden_servicio
				LEFT JOIN tb_marca mr ON mr.dc_marca = prod.dc_marca
				LEFT JOIN tb_usuario us ON us.dc_usuario = m.dc_usuario_creacion',
			'm.df_creacion,
			prod.dg_codigo,
			prod.dg_producto,
			mr.dg_marca,
			prod.dg_unidad_medida,
			m.dq_monto/(m.dq_cantidad/ABS(m.dq_cantidad)) dq_monto,
			m.dq_cantidad,
			cl.dg_razon dg_cliente,
			pr.dg_razon dg_proveedor,
			tm.dg_tipo_movimiento,
			be.dg_bodega dg_bodega_entrada,
			bs.dg_bodega dg_bodega_salida,
			gr.dq_guia_recepcion,
			fc.dq_factura dq_factura_compra,
			fc.dq_folio dq_folio_factura_compra,
			gd.dq_guia_despacho dq_guia_despacho_cliente,
			oc.dq_orden_compra,
			fv.dq_factura dq_factura_venta,
			fv.dq_folio dq_folio_factura_venta,
			nc.dq_nota_credito dq_nota_credito_cliente,
			nc.dq_folio dq_folio_nota_credito_cliente,
			nv.dq_nota_venta,
			os.dq_orden_servicio,
                        os2.dq_orden_servicio dq_orden_servicio_guia,
			us.dg_usuario dg_responsable',
			$conditions));
$db->stExec($data);
//var_dump($data);
$data = $data->fetchAll(PDO::FETCH_OBJ);

if(!count($data)){
	$error_man->showAviso('No se encontraron movimientos con los filtros especificados, compruebe la información con otros datos.');
	exit;
}

?>
<table class="tab" width="100%" id="informe_movimiento_data">
	<thead>
    	<tr>
        	<th>Fecha</th>
            <th>Código</th>
            <th width="200">Producto</th>
            <th>Marca</th>
            <th>Unidad de médida</th>
            <th>Monto</th>
            <th>Cantidad</th>
            <th>Cliente</th>
            <th>Proveedor</th>
            <th>Tipo de movimiento</th>
            <th>Bodega Entrada</th>
            <th>Bodega Salida</th>
            <th>Guía de Recepción</th>
            <th>Factura Compra</th>
            <th>Folio FC</th>
            <th>Guía de Despacho</th>
            <th>Factura Venta</th>
            <th>Folio FV</th>
            <th>Nota de Crédito</th>
            <th>Folio NC</th>
            <th>Nota Venta</th>
            <th>Orden Servicio</th>
            <th>Responsable</th>
        </tr>
    </thead>
    <tbody>
    	<?php while($d = array_shift($data)): ?>
        <tr>
        	<td><?php echo $db->dateLocalFormat($d->df_creacion) ?></td>
            <td><b><?php echo $d->dg_codigo ?></b></td>
            <td><b><?php echo $d->dg_producto ?></b></td>
            <td><?php echo $d->dg_marca ?></td>
            <td><?php echo $d->dg_unidad_medida ?></td>
            <td align="right"><?php echo moneda_local($d->dq_monto) ?></td>
            <td><?php echo $d->dq_cantidad ?></td>
            <td><?php echo $d->dg_cliente ?></td>
            <td><?php echo $d->dg_proveedor ?></td>
            <td><?php echo $d->dg_tipo_movimiento ?></td>
            <td><?php echo $d->dg_bodega_entrada ?></td>
            <td><?php echo $d->dg_bodega_salida ?></td>
            <td><?php echo $d->dq_guia_recepcion ?></td>
            <td><?php echo $d->dq_factura_compra ?></td>
            <td><?php echo $d->dq_folio_factura_compra ?></td>
            <td><?php echo $d->dq_guia_despacho_cliente?></td>
            <td><?php echo $d->dq_factura_venta ?></td>
            <td><?php echo $d->dq_folio_factura_venta ?></td>
            <td><?php echo $d->dq_nota_credito_cliente ?></td>
            <td><?php echo $d->dq_folio_nota_credito_cliente ?></td>
            <td><?php echo $d->dq_nota_venta ?></td>
            <td>
                <?php if($d->dq_orden_servicio): ?>
                    <?php echo $d->dq_orden_servicio ?>
                <?php else: ?>
                    <?php echo $d->dq_orden_servicio_guia ?>
                <?php endif; ?>
            </td>
            <td><?php echo $d->dg_responsable ?></td>
        </tr>
        <?php endwhile ?>
    </tbody>
</table>
<script type="text/javascript">
	$('#desbordar').width(2500);
	$('#informe_movimiento_data').tableExport().tableAdjust(6);
</script>