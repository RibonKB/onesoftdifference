<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['rt_emision_desde'] = $db->sqlDate($_POST['rt_emision_desde']);
$_POST['rt_emision_hasta'] = $db->sqlDate($_POST['rt_emision_hasta']." 23:59");

$conditions = "dc_empresa = {$empresa} AND (df_emision BETWEEN {$_POST['rt_emision_desde']} AND {$_POST['rt_emision_hasta']})";

if($_POST['rt_numero_desde']){
	if($_POST['rt_numero_hasta']){
		$conditions .= " AND (dq_factura BETWEEN {$_POST['rt_numero_desde']} AND {$_POST['rt_numero_hasta']})";
	}else{
		$conditions .= " AND dq_factura = {$_POST['rt_numero_desde']}";
	}
}

if($_POST['rt_fecha_desde']){
	$_POST['rt_fecha_desde'] = $db->sqlDate($_POST['rt_fecha_desde']);
	if($_POST['rt_fecha_hasta']){
		$_POST['rt_fecha_hasta'] = $db->sqlDate($_POST['rt_fecha_hasta']);
		$conditions .= " AND (df_ruta BETWEEN {$_POST['rt_fecha_desde']} AND {$_POST['rt_fecha_hasta']})";
	}else{
		$conditions .= " AND df_ruta = {$_POST['rt_fecha_desde']}";
	}
}

if(isset($_POST['rt_tipo'])){
	$_POST['rt_tipo'] = implode(',',$_POST['rt_tipo']);
	$conditions .= " AND dc_tipo_ruta IN ({$_POST['rt_tipo']})";
}

$data = $db->select('tb_ruta','dc_ruta,dq_ruta,dm_nula',$conditions,array('order_by' => 'dq_ruta DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron Rutas con los criterios especificados.");
	exit;
}

?>
<div id='show_ruta'></div>

<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Rutas<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Factura</th>
	</tr>
</thead>
<tbody>
<?php foreach($data as $c): ?>
	<tr>
		<td align='left'>
			<?php if($c['dm_nula']): ?>
				<img src="images/warning.png" alt="NULA" class="right" width="20" title="Factura Anulada" />
			<?php endif; ?>
			<a href='sites/logistica/proc/show_ruta.php?id=<?php echo $c['dc_ruta'] ?>' class='rt_load'>
			<img src='images/doc.png' alt='' style='vertical-align:middle;' /><?php echo $c['dq_ruta'] ?></a>
		</td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>

<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type="button" class="editbtn" id="edit_ruta">Editar</button>
<button type="button" class="imgbtn" style="background-image:url(images/archive.png);" id="historial_ruta">Historial de cambios</button>
<button type='button' class='delbtn' id='null_ruta'>Anular</button>

</div>

<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".rt_load").click(function(e){
		e.preventDefault();
		$('#show_ruta').html("<img src='images/ajax-loader.gif' alt='' /> cargando ruta ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('#main_cont .panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_ruta');
	}).first().trigger('click');

	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>