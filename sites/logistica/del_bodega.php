<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select("tb_bodega","dg_bodega",
"dc_bodega= {$_POST['id']} AND dc_empresa = {$empresa}");

if(!count($datos)){
	$error_man->showWarning("Error, no se ha encontrado la bodega especificada");
	exit();
}
?>
<div class="secc_bar">Eliminar Bodegas</div>
<div class="panes">
	<div id="del_bodega_res"></div>
	<form action="sites/logistica/proc/delete_bodega.php" class="confirmValidar" id="del_bodega">
		<fieldset>
		<div class="center alert">
			<strong>¿Está seguro de querer eliminar la bodega '<?=$datos[0]['dg_bodega'] ?>'?</strong>
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="del_bodega_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
			</div>
		</fieldset>
	</form>
</div>