<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Informe de stock</div>
<div id="main_cont"><div class="panes">');

include_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start("sites/logistica/proc/src_stock_report.php","src_stock_report");
$form->Header("<strong>Indicar los parámetros de búsqueda de stock</strong>");

$form->Section();
$form->ListadoMultiple('Productos','st_productos','tb_producto',array('dc_producto','dg_producto'));
echo('<br /><label>
<input type="checkbox" name="no_wo_stock" value="1" /> Incluir Productos sin stock disponible
</label>');
$form->EndSection();

$form->Section();
$form->ListadoMultiple('Bodegas','st_bodegas','tb_bodega',array('dc_bodega','dg_bodega'));
$form->EndSection();

$form->End('Ejecutar consulta','searchbtn');

?>
</div></div>
<script type="text/javascript">
$('#st_productos,#st_bodegas').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>