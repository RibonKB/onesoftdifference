<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Rutas</div>
<div id="main_cont" class="center"><br /><br />
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/logistica/proc/src_ruta.php","src_ruta");
	$form->Header("<strong>Indicar los parámetros de búsqueda de la ruta</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50">Número de ruta</td><td width="280">');
	$form->Text("Desde","rt_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','rt_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','rt_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','rt_emision_hasta',1,0);
	echo('</td></tr><tr><td>Fecha ruta</td><td>');
	$form->Date('Desde','rt_fecha_desde');
	echo('</td><td>');
	$form->Date('Hasta','rt_fecha_hasta');
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','rt_client','tb_cliente',array('dc_cliente','dg_razon'));
	echo('</td><td>&nbsp;</td></tr><tr><td>Tipo de Ruta</td><td>');
	$form->ListadoMultiple('','rt_tipo','tb_tipo_ruta',array('dc_tipo','dg_tipo'));
	echo('</td><td>&nbsp;</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#rt_client,#rt_tipo').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>