<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Ajustes de cantidad</div>
<div id="main_cont" class="center"><br /><br />
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/logistica/proc/src_ajuste_cantidad.php","src_ajuste_cantidad");
	$form->Header("<strong>Indicar los parámetros de búsqueda de los comprobantes de ajuste de cantidad</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50">Número de Comprobante</td><td width="280">');
	$form->Text("Desde","ac_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','ac_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','ac_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','ac_emision_hasta',1,0);
	echo('</td></tr><tr><td>Nota de venta</td><td><br />');
	$form->Text('','ac_nota_venta');
	echo('</td><td>&nbsp;</td></tr><tr><td>Orden de servicio</td><td><br />');
	$form->Text('','ac_orden_servicio');
	echo('</td><td>&nbsp;</td></tr><tr><td>Producto</td><td>');
	$form->ListadoMultiple('','ac_producto','tb_producto',array('dc_producto','dg_producto'));
	echo('</td><td>&nbsp;</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#ac_producto').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>