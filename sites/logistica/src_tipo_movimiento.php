<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Tipos de movimientos logísticos</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);

	$form->Header("Complete los datos requeridos para crear un nuevo tipo de movimiento<br />Puede consultar las que ya están agregadas.");
	$form->Start("sites/logistica/proc/crear_tipo_movimiento.php","cr_tipo_movimiento","confirmValidar");
	$form->Section();
	$form->Text("Nombre","tipo_name",1);
	$form->Text("Código","tipo_codigo",1);
	$form->EndSection();
	$form->Section();
	$form->Radiobox('Genera asientos contables','tipo_asiento',array('NO','SI'),0);
	$form->EndSection();
	echo('<br class="clear" /><br /><fieldset id="tipo_mov_cont" class="hidden">');
	$form->Header('Indique un tipo de movimiento contable para asignar cuentas');
	$form->Listado('Tipo de movimiento Contable [*]','tipo_tipo_mov','tb_tipo_movimiento',array('dc_tipo_movimiento','dg_tipo_movimiento'));
	echo('<table><tbody><tr><td rowspan="2">');
	$form->Multiselect('Cuentas contables [*]','tipo_cuentas',array());
	echo('</td><td valign="center">');
	$form->Button('»','id="addDebe"');
	echo('<br />');
	$form->Button('«','id="rmvDebe"');
	echo('</td><td valign="top">');
	$form->Multiselect('DEBE','tipo_debe',array());
	echo('</td></tr><tr><td valign="center">');
	$form->Button('»','id="addHaber"');
	echo('<br />');
	$form->Button('«','id="rmvHaber"');
	echo('</td><td valign="top">');
	$form->Multiselect('HABER','tipo_haber',array());
	echo('</td></tr></tbody></table>');
	echo('</fieldset>');
	$form->End("Crear","addbtn");
	
	$list = $db->select("tb_tipo_movimiento_logistico",'dc_tipo_movimiento,dg_tipo_movimiento,dg_codigo,DATE_FORMAT(df_creacion,"%d/%m/%Y %H:%i") AS df_fecha',
	"dc_empresa={$empresa} AND dm_activo='1'");
	
	echo('<table class="tab" width="100%">
	<caption>Bodegas ya agregadas</caption>
	<thead>
		<tr>
			<th width="40%">Tipo de movimiento</th>
			<th width="40%">Código</th>
			<th width="20%">Fecha de creación</th>
			<th width="15%">Opciones</th>
		</tr>
	</thead>
	<tbody id="list">');
	
	foreach($list as $l){
		echo("<tr id='item{$l['dc_tipo_movimiento']}'>
			<td>{$l['dg_tipo_movimiento']}</td>
			<td>{$l['dg_codigo']}</td>
			<td>{$l['df_fecha']}</td>
			<td>
				<a href='sites/logistica/ed_tipo_movimiento.php?id={$l['dc_tipo_movimiento']}' class='loadOnOverlay left'>
				<img src='images/editbtn.png' alt='' title='Editar' />
				</a>
				<a href='sites/logistica/del_tipo_movimiento.php?id={$l['dc_tipo_movimiento']}' class='loadOnOverlay left'>
				<img src='images/delbtn.png' alt='' title='Eliminar' />
				</a>
			</td>
		</tr>");
	}
	
	echo("</tbody></table>");
	
?>
</div>
</div>
<script type="text/javascript" src="jscripts/logistica/cr_tipo_movimiento.js"></script>