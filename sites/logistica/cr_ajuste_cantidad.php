<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo('<div id="secc_bar">Ajustes de cantidad</div><div id="main_cont"><div class="panes">');

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/logistica/proc/crear_ajuste_cantidad.php','cr_ajuste_cantidad');
$form->Header("<b>Indique los datos para realizar el ajuste de cantidad y el comprobante de ajuste</b><br />
Los datos marcados con [*] son obligatorios.");

$form->Section();
	$form->Text('Producto','dg_producto',1);
	$form->Hidden('dc_producto',0);
	
	$form->Radiobox('Tipo de ajuste','dm_tipo_ajuste',array(1 => 'Positivo','-1' => 'Negativo'),1);
	$form->Textarea('Comentario','dg_comentario',1);
$form->EndSection();

$form->Section();
	$form->Text('Cantidad','dc_cantidad',1);
	$form->Listado('<span id="tipo_bodega"></span>','dc_bodega','tb_bodega',array('dc_bodega','dg_bodega'),1);
	
	$form->Text('Nota de venta','dq_nota_venta');
	$form->Hidden('dc_nota_venta',0);
	
	$form->Text('Orden de servicio','dq_orden_servicio');
	$form->Hidden('dc_orden_servicio',0);

$form->EndSection();

$form->Section();
	
    $form->Text("Monto",'dq_monto',1,20,0);
	$form->Listado('Tipo de cambio','dq_cambio','tb_tipo_cambio',array('dq_cambio','dg_tipo_cambio'),1);
    $form->Date('Fecha de Ajuste', 'df_emision', true,0);
    
$form->EndSection();

$form->End('Crear','addbtn');

?>
</div></div>
<script type="text/javascript" src="jscripts/logistica/cr_ajuste_cantidad.js?v0_1b"></script>