<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Rutar Notas de Venta</div>
<div id="main_cont"><br /><br />
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/logistica/proc/cr_ruta.php","src_nota_venta");
	$form->Header("<strong>Indicar los parámetros de búsqueda de las nota de venta que irán en la ruta</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="50">Número de nota de venta</td><td width="280">');
	$form->Text("Desde","nv_numero_desde");
	echo('</td><td width="280">');
	$form->Text('Hasta','nv_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','nv_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','nv_emision_hasta',1,0);
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','nv_client','tb_cliente',array('dc_cliente','dg_razon'));
	
	if(check_permiso(38)){
		echo('</td><td>&nbsp;</td></tr><tr><td>Ejecutivo</td><td>');
		$form->ListadoMultiple('','nv_executive','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 1");

	}
	
	echo('</td><td>&nbsp;</td></tr></table>');
	
	if(!check_permiso(38))
		$form->Hidden('nv_executive[]',$userdata['dc_funcionario']);
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#nv_client,#nv_executive').multiSelect({
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	});
</script>