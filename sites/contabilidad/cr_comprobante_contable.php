<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">Comprobante contable</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Indique el código del tipo de movimiento que afectará el comprobante contable</strong><br />Los criterios de busqueda son el código, nombre o descripción del tipo de movimiento");
	$form->Start("sites/contabilidad/proc/cr_comprobante_contable.php","cr_comprobante_contable","cValidar");
	$form->Text("Código tipo de movimiento","tipo_codigo",1);
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[0]+" "+row[1]+" - "+row[2];
}
$("#tipo_codigo").autocomplete('sites/proc/autocompleter/tipo_movimiento.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>