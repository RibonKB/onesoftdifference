<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
$datos = $db->select(
"tb_comprobante_contable",
"dg_comprobante,dc_mes_contable,dc_anho_contable",
"dc_comprobante= {$_POST['id']} AND dm_anulado = '0' AND dc_empresa={$empresa}");
if(!count($datos)){
	$error_man->showWarning("Error, el comprobante no fue encontrado, asegurese que posee los permisos para anular y que el comprobante no este previamente anulado");
	exit();
}
$datos = $datos[0];

$periodo = $db->select("(SELECT * FROM tb_mes_contable WHERE dc_mes = {$datos['dc_mes_contable']}) m
JOIN (SELECT * FROM tb_periodo_contable WHERE dc_anho={$datos['dc_anho_contable']} AND dm_estado='1') p ON m.dc_mes_contable = p.dc_mes_contable",'1');

if(!count($periodo)){
	$error_man->showWarning('El comprobante pertenece a un periodo contable cerrado, no se puede anular.');
	exit();
}

$dg_comprobante = $datos['dg_comprobante'];

echo('<div class="secc_bar">Anular nota de crédito</div><div class="panes">');

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/contabilidad/proc/null_comprobante.php','null_comprobante_form');
$form->Header('Indique las razones por las que se anulará el comprobante');
$error_man->showAviso("Atención, está a punto de anular el comprobante° <b>{$dg_comprobante}</b>");
$form->Section();
$form->Listado('Motivo de anulación','null_motivo','tb_motivo_anulacion ma JOIN tb_motivo_anulacion_modulo mm ON ma.dc_motivo = mm.dc_motivo',array('mm.dc_motivo','ma.dg_motivo'),1,0,"dc_empresa = {$empresa} AND dc_modulo = 2 AND dm_activo = 1 ");
$form->Radiobox('Emitida Fisicamente','null_fisica',array('NO','SI'),0);
$form->EndSection();
$form->Section();
$form->Textarea('Comentario','null_comentario',1);
$form->EndSection();
$form->Hidden('null_comp_id',$_POST['id']);
$form->End('Anular','delbtn');

?>

