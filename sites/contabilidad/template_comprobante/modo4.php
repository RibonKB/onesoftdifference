<?php
require("../../inc/fpdf.php");

class PDF extends FPDF{

private $datosEmpresa;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"(SELECT * FROM tb_empresa WHERE dc_empresa={$empresa}) e
	JOIN (SELECT * FROM tb_empresa_configuracion WHERE dc_empresa={$empresa}) ec ON e.dc_empresa = ec.dc_empresa
	LEFT JOIN tb_comuna c ON e.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region r ON c.dc_region = r.dc_region",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','mm','Letter');
	
}

function Header(){
	global $datosComprobante;
	
	$this->SetFont('Arial','',12);
	$this->SetDrawColor(0,100,0);
	$this->SetTextColor(0,100,0);
	$this->SetX(160);
	$this->MultiCell(50,7,"COMPROBANTE\nCONTABLE\nN� {$datosComprobante['dg_comprobante']}\n{$datosComprobante['dg_numero_interno']}",1,'C');
	$this->SetY(10);
	$this->SetTextColor(0);
	
	$this->SetFont('','B',12);
	$this->Cell(145,5,$this->datosEmpresa['dg_razon']);
	$this->ln();
	$this->SetFont('Arial','',7);
	$this->SetTextColor(110);
	$this->MultiCell(120,2.4,"{$this->datosEmpresa['dg_giro']}\n{$this->datosEmpresa['dg_direccion']}, {$this->datosEmpresa['dg_comuna']} {$this->datosEmpresa['dg_region']}\n{$this->datosEmpresa['dg_fono']}");
	$this->SetTextColor(0);
	
	$this->SetY(40);
	$this->SetDrawColor(150,150,150);
	$this->Cell(0,5,'','B');
	$this->Ln();
	$y = $this->GetY();
	$this->SetFontSize(5);
	$this->MultiCell(22,5,"TIPO MOVIMIENTO:\nPERIODO CONTABLE:\nGLOSA GENERAL:",'L');
	$this->SetY($y);
	$this->SetX(100);
	$this->MultiCell(25,5,"FECHA EMISI�N:\nRESPONSABLE:");
	$this->SetY($y);
	$this->SetX(30);
	$this->SetFontSize(9);
	$this->MultiCell(70,5,"({$datosComprobante['dg_codigo']}) {$datosComprobante['dg_tipo_movimiento']}\n{$datosComprobante['df_fecha_contable']}");
	$this->SetY($y);
	$this->SetX(116);
	$this->MultiCell(90,5,"{$datosComprobante['df_fecha_emision']}\n{$datosComprobante['dg_responsable']}","R");
	$this->MultiCell(0,5,"                   {$datosComprobante['dg_glosa']}",1);
	$this->Ln(3);
	
	$this->SetFont('','B');
	$this->SetDrawColor(190);
	$this->SetFillColor(190);
	$this->SetTextColor(40);
	$this->Cell(0,7,'Detalle del comprobante',1,1,'C',1);
	$this->SetFillColor(230);
	
	$w = array(23,58,25,25,65);
	$head = array("C�DIGO CUENTA","DESCRIPCI�N","DEBE","HABER","GLOSA");
	$this->SetFontSize(7);
	for($i=0;$i<5;$i++){
		$this->Cell($w[$i],6,$head[$i],1,0,'L',1);
	}
	$this->Ln();
	
}

function setDetalle($detalle,$saldo){
	$this->SetFont('Arial','');
	$this->AddPage();
	$this->SetDrawColor(190);
	$this->SetFillColor(250);
	$fill = false;
	
	$w = array(23,58,25,25,65);
	$lm = 10;
	$xs = array($lm+$w[0],$lm+$w[0]+$w[1],$lm+$w[0]+$w[1]+$w[2],$lm+$w[0]+$w[1]+$w[2]+$w[3]);
	
	foreach($detalle as $i => $det){
	$this->SetFontSize(7);
		if(($i+1)%45 == 0){
			$this->Cell(0,6,'','T');
			$this->AddPage();
		}
		//echo($this->GetStringWidth($det['dg_glosa']).'<br>');
		
		$hdesc = ceil($this->GetStringWidth($det['dg_cuenta_contable'])/58);
		$hglos = ceil($this->GetStringWidth($det['dg_glosa'])/65);
		$lines = $hdesc>$hglos?$hdesc:$hglos;
		$gl = ($lines-$hglos)<0?0:$lines-$hglos;
		$dl = ($lines-$hdesc)<0?0:$lines-$hdesc;
		
		//MultiCell(float w, float h, string txt [, mixed border [, string align [, boolean fill]]])
		$y = $this->GetY();
		$this->MultiCell($w[0],3.2,$det['dg_codigo'].str_repeat("\n",$lines),'LR','L',$fill);
		$this->SetY($y);
		$this->SetX($xs[0]);
		$this->MultiCell($w[1],3.2,ucwords(strtolower($det['dg_cuenta_contable'])).str_repeat("\n",$dl),'LR','L',$fill);
		$this->SetY($y);
		$this->SetX($xs[1]);
		$this->MultiCell($w[2],3.2,moneda_local($det['dq_debe']).str_repeat("\n",$lines),'LR','R',$fill);
		$this->SetY($y);
		$this->SetX($xs[2]);
		$this->MultiCell($w[3],3.2,moneda_local($det['dq_haber']).str_repeat("\n",$lines),'LR','R',$fill);
		$this->SetY($y);
		$this->SetX($xs[3]);
		$this->MultiCell($w[4],3.2,$det['dg_glosa'].str_repeat("\n",$gl),'LR','L',$fill);
		$fill = !$fill;
	}
	$this->Cell(0,6,'','T');
	$this->Ln(0);
	$this->Cell(81,6,'Saldo',1,0,'R');
	$this->SetFont('','B');
	$this->Cell(25,6,moneda_local($saldo),1,0,'R');
	$this->Cell(25,6,moneda_local($saldo),1,0,'R');
	$this->Cell(65,6,'',1);
}

function setFoot($tipo_mov){
	if($tipo_mov == 6){
		$this->SetY(-45);
		$this->MultiCell(80,6,"___________________________\nAceptaci�n",0,'C');
		$this->SetY(-50);
		$this->SetX(120);
		$this->MultiCell(80,6,"Retiro cheque\nNombre\nC.I.\nFirma",0,'L');
	}
}

}

?>