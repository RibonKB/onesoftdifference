<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VerDiferenciasComprobante
 *
 * @author Eduardo
 */
class VerDiferenciasComprobante extends Factory {
    //put your code here
    
    private function buscarDiff($mes=''){
        
        $comprobantes=$this->coneccion('select', 
                'tb_comprobante_contable A,
                    tb_comprobante_contable_detalle B, 
                    tb_comprobante_contable_detalle_analitico C', 
                'A.dg_comprobante, `df_fecha_contable`,`dc_mes_contable`,`dc_anho_contable`,`df_fecha_emision`,`dc_empresa`,
sum(B.dq_debe), sum(B.dq_haber)  ,sum(B.dq_debe) - sum(B.dq_haber) , 
sum(C.dq_debe), sum(C.dq_haber)  ,sum(C.dq_debe) - sum(C.dq_haber) ,
sum(B.dq_debe) -  sum(C.dq_debe),  sum(B.dq_haber) - sum(C.dq_haber)
', "A.dc_comprobante = B.dc_comprobante and B.dc_detalle = C.dc_detalle_financiero and A.dc_empresa = 1 
and A.dm_anulado =0
group by A.dg_comprobante, `df_fecha_contable`,`dc_mes_contable`,`dc_anho_contable`,`df_fecha_emision`,`dc_empresa`
");
        $comprobantes=$comprobantes->fetchAll(PDO::FETCH_OBJ);
        return $comprobantes;
    }
    
    public function buscaAction(){
        $diff=$this->buscarDiff();
        $mostrar = $this->getFormView($this->getTemplateURL('diferencia'), array('diff'=>$diff));
        echo $this->getFullView($mostrar, array(), Factory::STRING_TEMPLATE);
    }
}

?>
