<?php
define('MAIN',1);
require_once("../../../inc/global.php");
require_once("../../../inc/form-class.php");

$form = new Form($empresa);

?>
<div id="secc_bar">
	Propuestas de pago
</div>
<div id="main_cont">
	<div class="panes">
		<?php $form->Start('sites/contabilidad/pagos/proc/src_propuesta_pago.php','src_propuesta_pago'); ?>
		<?php $form->Header('<b>Indique los filtros de búsqueda para las propuestas de pago</b><br />Los campso marcados con [*] son obligatorios'); ?>
		<table class="tab" width="100%">
			<tbody>
				<tr>
					<td>Número de propuesta</td>
					<td><?php $form->Text('Desde','dq_propuesta_desde') ?></td>
					<td><?php $form->Text('hasta','dq_propuesta_hasta') ?></td>
				</tr>
				<tr>
					<td>Nombre de propuesta</td>
					<td><br /><?php $form->Text('','dg_propuesta'); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Fecha de Propuesta</td>
					<td><?php $form->Date('Desde','df_propuesta_desde') ?></td>
					<td><?php $form->Date('Hasta','df_propuesta_hasta') ?></td>
				</tr>
				<tr>
					<td>Fecha emisión propuesta</td>
					<td><?php $form->Date('Desde','df_emision_desde',1,'01/'.date('m/Y')) ?></td>
					<td><?php $form->Date('Hasta','df_emision_hasta',1,0) ?></td>
				</tr>
				<tr>
					<td>Proveedor</td>
					<td><?php $form->ListadoMultiple('','dc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon')); ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Estado</td>
					<td><?php $form->Combobox('','dm_estado',array('L' => 'Levantado', 'V' => 'Validado', 'P' => 'Pagado'),array('L','V')) ?></td>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>
		<?php $form->End('Ejecutar consulta','searchbtn') ?>
	</div>
</div>