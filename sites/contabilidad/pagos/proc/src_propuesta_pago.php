<?php
define("MAIN",1);
require_once("../../../../inc/init.php");

$conditions = "dc_empresa = {$empresa}";

if($_POST['dq_propuesta_desde']){
	$dq_propuesta_desde = intval($_POST['dq_propuesta_desde']);
	if(!$dq_propuesta_desde){
		$error_man->showWarning("El formato para el número de propuesta no es el esperado, por favor indique un número.");
		exit;
	}
	
	if($_POST['dq_propuesta_hasta']){
		$dq_propuesta_hasta = intval($_POST['dq_propuesta_hasta']);
		
		if(!$dq_propuesta_hasta){
			$error_man->showWarning("El formato para el número de propuesta no es el esperado, por favor indique un número.");
			exit;
		}
		
		$conditions .= " AND (dq_propuesta BETWEEN {$dq_propuesta_desde} AND {$dq_propuesta_hasta})";
	}else{
		$conditions .= " AND dq_propuesta = {$dq_propuesta_desde}";
	}
}else{
	if($_POST['df_propuesta_desde']){
		$df_propuesta_desde = $db->sqlDate($_POST['df_propuesta_desde']);
		if($_POST['df_propuesta_hasta']){
			$df_propuesta_hasta = $db->sqlDate($_POST['df_propuesta_hasta'].' 23:59');
			$conditions .= " AND (df_propuesta BETWEEN {$df_propuesta_desde} AND {$df_propuesta_hasta})";
		}else{
			$conditions .= " AND df_propuesta = {$df_propuesta_desde}";
		}
	}else{
		$df_emision_desde = $db->sqlDate($_POST['df_emision_desde']);
		$df_emision_hasta = $db->sqlDate($_POST['df_emision_hasta'].' 23:59');
		
		$conditions .= " AND (df_emision BETWEEN {$df_emision_desde} AND {$df_emision_hasta})";
	}
}

if(isset($_POST['dc_proveedor'])){
	$dc_proveedor = implode(',',$_POST['dc_proveedor']);
	
	$conditions .= " AND dc_proveedor IN ({$dc_proveedor})";
}

if($_POST['dg_propuesta']){
	$dg_propuesta = mysql_real_escape_string($_POST['dg_propuesta']);
	$conditions .= " AND dg_propuesta LIKE '%{$dg_propuesta}%'";
}

if(isset($_POST['dm_estado'])){
	$dm_estado = implode("','",$_POST['dm_estado']);
	$conditions .= " AND dm_estado IN ('{$dm_estado}')";
}

$data = $db->doQuery(
	$db->select("(SELECT * FROM tb_propuesta_pago WHERE {$conditions}) pp
	LEFT JOIN tb_proveedor p ON p.dc_proveedor = pp.dc_proveedor
	JOIN tb_usuario u ON u.dc_usuario = pp.dc_usuario_creacion
	JOIN tb_funcionario f ON f.dc_funcionario = u.dc_funcionario",
	'pp.dc_propuesta, pp.dq_propuesta, pp.dg_propuesta, p.dg_razon, pp.dg_comentario, pp.df_propuesta, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, pp.dm_estado')
);

?>
<table class="tab" width="100%">
<caption>Propuestas de pago</caption>
<thead>
	<tr>
		<th><input type="checkbox" id="select_all_cb" /></th>
		<th>N° Propuesta</th>
		<th>Propuesta</th>
		<th>Proveedor</th>
		<th>Comentario</th>
		<th>Fecha propuesta</th>
		<th>Responsable</th>
		<th>ST</th>
		<th>&nbsp;</th>
	</tr>
</thead>
<tbody>
<?php while($propuesta = $data->fetch(PDO::FETCH_OBJ)): ?>
	<tr>
		<td><input type="checkbox" class="item_cb" name="dc_propuesta[]" value="<?php echo $propuesta->dc_propuesta ?>" /></td>
		<td><b><?php echo $propuesta->dq_propuesta ?></b></td>
		<td><?php echo $propuesta->dg_propuesta ?></td>
		<td><?php echo $propuesta->dg_razon ?></td>
		<td><?php echo $propuesta->dg_comentario ?></td>
		<td><?php echo $propuesta->df_propuesta ?></td>
		<td><?php echo $propuesta->dg_nombres.' '.$propuesta->dg_ap_paterno.' '.$propuesta->dg_ap_materno ?></td>
		<td><?php echo $propuesta->dm_estado ?></td>
		<td></td>
	</tr>
<?php endwhile; ?>
</tbody>
</table>