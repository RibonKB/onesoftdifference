<?php
class ContabilidadStuff{

	const FIELD_ESTADO_FC = 'dm_estado_factura_compra';
	const FIELD_ESTADO_FV = 'dm_estado_factura_venta';
	const FIELD_ESTADO_CT = 'dm_estado';

	const PERIODO_ABIERTO = 0;
	const ERROR_PERIODO_INEXISTENTE = 1;
	const ERROR_PERIODO_CERRADO = 2;
	const ERROR_PERIODO_USUARIO_DENEGADO = 3;
	const ERROR_PERIODO_GRUPO_DENEGADO = 4;

	static private $configuration = null;

	/**
	*	Obtiene el siguiente número correlatico para ser asignado a un comprobante contable
	*/
	public static function getCorrelativoComprobante($base = null, $empr = null){

		if($base == null){
			global $db;
		}else{
			$db = $base;
		}

		if($empr == null){
			global $empresa;
		}else{
			$empresa = $empr;
		}

		$dg_comprobante = $db->prepare($db->select('tb_comprobante_contable',
					'dg_comprobante',
					'dc_empresa=?',
					array('order_by' => 'df_fecha_emision DESC', 'limit' => 1)));
		$dg_comprobante->bindValue(1,$empresa,PDO::PARAM_INT);
		$db->stExec($dg_comprobante);
		$dg_comprobante = $dg_comprobante->fetch(PDO::FETCH_OBJ);

		if($dg_comprobante !== false && substr($dg_comprobante->dg_comprobante,0,6) == date("Ym")){
			$dg_comprobante = date("Ym").((int)substr($dg_comprobante->dg_comprobante,6)+1);
		}else{
			$dg_comprobante = date("Ym").'0';
		}

		return $dg_comprobante;

	}

	/**
	*	Obtiene el siguiente número correlatico para ser asignado a un comprobante contable
	*/
	public static function getCorrelativoComprobante_OLD(){

		global $db, $empresa;

		$dg_comprobante = $db->select('tb_comprobante_contable',
					'dg_comprobante',
					'dc_empresa='.$empresa,
					array('order_by' => 'df_fecha_emision DESC', 'limit' => 1));

		$dg_comprobante = array_shift($dg_comprobante);

		if($dg_comprobante !== NULL && substr($dg_comprobante['dg_comprobante'],0,6) == date("Ym")){
			$dg_comprobante = date("Ym").((int)substr($dg_comprobante['dg_comprobante'],6)+1);
		}else{
			$dg_comprobante = date("Ym").'0';
		}

		return $dg_comprobante;

	}

	/**
	*	Comprobar el estado de un periodo contable
	*/
	public static function getEstadoPeriodo($mes_contable, $anho_contable, $type = self::FIELD_ESTADO_CT, $base = null, $empr = null, $uid = null){

		if($base == null){
			global $db;
		}else{
			$db = $base;
		}

		if($empr == null){
			global $empresa;
		}else{
			$empresa = $empr;
		}

		if($uid == null){
			global $idUsuario;
		}else{
			$idUsuario = $uid;
		}

		$data = $db->doQuery($db->select("tb_mes_contable m
          JOIN tb_periodo_contable p ON m.dc_mes_contable = p.dc_mes_contable",
		"p.dm_filtro,p.dg_permisos,{$type} dm_estado",
		"m.dc_mes = {$mes_contable} AND	dc_anho = {$anho_contable} AND	m.dc_empresa = {$empresa} AND p.dm_activo = '1'")
		)->fetch(PDO::FETCH_OBJ);

		//El periodo no existe
		if($data === false){
			return self::ERROR_PERIODO_INEXISTENTE;
		}

		//El periodo está cerrado
		if($data->dm_estado == '0'){
			return self::ERROR_PERIODO_CERRADO;
		}

		//Comprobar permiso del grupo del usuario actual
		if($data->dm_filtro == '1'){
			$gr = substr($data->dg_permisos,0,-1);
			$grupo_usuario = $db->doQuery($db->select("tb_usuario_grupo","1","dc_usuario = {$idUsuario} AND dc_grupo IN ({$gr})"))->fetch();
			if($grupo_usuario === false){
				return self::ERROR_PERIODO_GRUPO_DENEGADO;
			}
		}

		//Comprobar permiso del usuario actual
		else if($data->dm_filtro == '2'){
			if(strpos($data->dg_permisos,"{$idUsuario},") === false){
				return self::ERROR_PERIODO_USUARIO_DENEGADO;
			}
		}

		//El periodo está abierto
		return self::PERIODO_ABIERTO;

	}

    public static function getEstadoPeriodoFromDate($periodo, $type = self::FIELD_ESTADO_CT, $base = null, $empr = null, $uid = null){

		if($base == null){
			global $db;
		}else{
			$db = $base;
		}

		if($empr == null){
			global $empresa;
		}else{
			$empresa = $empr;
		}

		if($uid == null){
			global $idUsuario;
		}else{
			$idUsuario = $uid;
		}

        $data = $db->prepare($db->select('tb_mes_contable m
          JOIN tb_periodo_contable p ON m.dc_mes_contable = p.dc_mes_contable',
        "p.dm_filtro,p.dg_permisos,{$type} dm_estado",
        'm.dc_mes = MONTH(?) AND dc_anho = YEAR(?) AND	m.dc_empresa = ? AND p.dm_activo = 1'));
        $data->bindValue(1,$periodo,PDO::PARAM_STR);
        $data->bindValue(2,$periodo,PDO::PARAM_STR);
        $data->bindValue(3,$empresa,PDO::PARAM_INT);
        $db->stExec($data);

        $data = $data->fetch(PDO::FETCH_OBJ);

		//El periodo no existe
		if($data === false){
			return self::ERROR_PERIODO_INEXISTENTE;
		}

		//El periodo está cerrado
		if($data->dm_estado == '0'){
			return self::ERROR_PERIODO_CERRADO;
		}

		//Comprobar permiso del grupo del usuario actual
		if($data->dm_filtro == '1'){
			$gr = substr($data->dg_permisos,0,-1);
			$grupo_usuario = $db->doQuery($db->select("tb_usuario_grupo","1","dc_usuario = {$idUsuario} AND dc_grupo IN ({$gr})"))->fetch();
			if($grupo_usuario === false){
				return self::ERROR_PERIODO_GRUPO_DENEGADO;
			}
		}

		//Comprobar permiso del usuario actual
		else if($data->dm_filtro == '2'){
			if(strpos($data->dg_permisos,"{$idUsuario},") === false){
				return self::ERROR_PERIODO_USUARIO_DENEGADO;
			}
		}

		//El periodo está abierto
		return self::PERIODO_ABIERTO;

	}

	public static function getEstadoPeriodo_OLD($mes_contable, $anho_contable, $type = self::FIELD_ESTADO_CT){
		global $db,$empresa,$idUsuario;

		$data = $db->select("tb_mes_contable m
		JOIN tb_periodo_contable p ON m.dc_mes_contable = p.dc_mes_contable",
		"p.dm_filtro,p.dg_permisos,{$type}",
		"m.dc_mes = {$mes_contable} AND	dc_anho = {$anho_contable} AND	m.dc_empresa = {$empresa} AND p.dm_activo = '1'");
		$data = array_shift($data);

		//El periodo no existe
		if($data == NULL){
			return self::ERROR_PERIODO_INEXISTENTE;
		}

		//El periodo está cerrado
		if($data[$type] == '0'){
			return self::ERROR_PERIODO_CERRADO;
		}

		//Comprobar permiso del grupo del usuario actual
		if($data['dm_filtro'] == '1'){
			$gr = substr($data['dg_permisos'],0,-1);
			$grupo_usuario = $db->select("tb_usuario_grupo","1","dc_usuario = {$idUsuario} AND dc_grupo IN ({$gr})");
			$grupo_usuario = array_shift($grupo_usuario);
			if($grupo_usuario == NULL){
				return self::ERROR_PERIODO_GRUPO_DENEGADO;
			}
		}

		//Comprobar permiso del usuario actual
		else if($data['dm_filtro'] == '2'){
			if(strpos($data['dg_permisos'],"{$idUsuario},") === false){
				return self::ERROR_PERIODO_USUARIO_DENEGADO;
			}
		}

		//El periodo está abierto
		return self::PERIODO_ABIERTO;
	}

	public static function getConfiguration($base = null, $empr = null){

		if($base == null){
			global $db;
		}else{
			$db = $base;
		}

		if($empr == null){
			global $empresa;
		}else{
			$empresa = $empr;
		}


		if(self::$configuration != null){
			return self::$configuration;
		}

		self::$configuration = $db->doQuery($db->select('tb_configuracion_contabilidad','*','dc_empresa='.$empresa))->fetch(PDO::FETCH_OBJ);

		return self::$configuration;

	}

	public static function getAnaliticoDesdeIds(array $ids, DBConnector $dbo = null){
		if($dbo == null):
			global $db;
		else:
			$db = $dbo;
		endif;
		$data = array();

		//
		if(isset($ids['dc_cuenta_contable'])){
			$data['dg_codigo_cuenta'] = @$db->getRowById('tb_cuenta_contable',$ids['dc_cuenta_contable'],'dc_cuenta_contable')->dg_codigo;
			$data['dg_cuenta_contable'] = @$db->getRowById('tb_cuenta_contable',$ids['dc_cuenta_contable'],'dc_cuenta_contable')->dg_cuenta_contable;
		}else{
			$data['dg_codigo_cuenta'] = NULL;
			$data['dg_cuenta_contable'] = NULL;
		}

		//
		$data['dq_debe'] = $ids['dq_debe'];
		//
		$data['dq_haber'] = $ids['dq_haber'];
		//
		$data['dg_glosa'] = $ids['dg_glosa'];

		//
		if(isset($ids['dc_factura_compra'])){
			$data['dq_factura_compra'] = @$db->getRowById('tb_factura_compra',$ids['dc_factura_compra'],'dc_factura')->dq_factura;
			$data['dq_factura_compra_folio'] = @$db->getRowById('tb_factura_compra',$ids['dc_factura_compra'],'dc_factura')->dq_folio;
		}else{
			$data['dq_factura_compra'] = NULL;
			$data['dq_factura_compra_folio'] = NULL;
		}

		//
		if(isset($ids['dc_factura_venta'])){
			$data['dq_factura_venta'] = @$db->getRowById('tb_factura_venta',$ids['dc_factura_venta'],'dc_factura')->dq_factura;
			$data['dq_factura_venta_folio'] = @$db->getRowById('tb_factura_venta',$ids['dc_factura_venta'],'dc_factura')->dq_folio;
		}else{
			$data['dq_factura_venta'] = NULL;
			$data['dq_factura_venta_folio'] = NULL;
		}

		//
		if(isset($ids['dc_nota_venta']))
			$data['dq_nota_venta'] = @$db->getRowById('tb_nota_venta',$ids['dc_nota_venta'],'dc_nota_venta')->dq_nota_venta;
		else
			$data['dq_nota_venta'] = NULL;

		//
		if(isset($ids['dc_orden_servicio']))
			$data['dq_orden_servicio'] = @$db->getRowById('tb_orden_servicio',$ids['dc_orden_servicio'],'dc_orden_servicio')->dq_orden_servicio;
		else
			$data['dq_orden_servicio'] = NULL;

		//
		if(isset($ids['dc_proveedor']))
			$data['dg_proveedor'] = @$db->getRowById('tb_proveedor',$ids['dc_proveedor'],'dc_proveedor')->dg_razon;
		else
			$data['dg_proveedor'] = NULL;

		//
		if(isset($ids['dc_tipo_proveedor']))
			$data['dg_tipo_proveedor'] = @$db->getRowById('tb_tipo_proveedor',$ids['dc_tipo_proveedor'],'dc_tipo_proveedor')->dg_tipo_proveedor;
		else
			$data['dg_tipo_proveedor'] = NULL;

		//
		if(isset($ids['dc_cliente']))
			$data['dg_cliente'] = @$db->getRowById('tb_cliente',$ids['dc_cliente'],'dc_cliente')->dg_razon;
		else
			$data['dg_cliente'] = NULL;

		//
		if(isset($ids['dc_tipo_cliente']))
			$data['dg_tipo_cliente'] = @$db->getRowById('tb_tipo_cliente',$ids['dc_tipo_cliente'],'dc_tipo_cliente')->dg_tipo_cliente;
		else
			$data['dg_tipo_cliente'] = NULL;

		//
		if(isset($ids['dc_orden_compra']))
			$data['dq_orden_compra'] = @$db->getRowById('tb_orden_compra',$ids['dc_orden_compra'],'dc_orden_compra')->dq_orden_compra;
		else
			$data['dq_orden_compra'] = NULL;

		//
		if(isset($ids['dc_producto']))
			$data['dg_producto'] = @$db->getRowById('tb_producto',$ids['dc_producto'],'dc_producto')->dg_producto;
		else
			$data['dg_producto'] = NULL;

		//
		if(isset($ids['dc_cebe']))
			$data['dg_cebe'] = @$db->getRowById('tb_cebe',$ids['dc_cebe'],'dc_cebe')->dg_cebe;
		else
			$data['dg_cebe'] = NULL;

		//
		if(isset($ids['dc_ceco']))
			$data['dg_ceco'] = @$db->getRowById('tb_ceco',$ids['dc_ceco'],'dc_ceco')->dg_ceco;
		else
			$data['dg_ceco'] = NULL;

		//
		if(isset($ids['dc_marca']))
			$data['dg_marca'] = @$db->getRowById('tb_marca',$ids['dc_marca'],'dc_marca')->dg_marca;
		else
			$data['dg_marca'] = NULL;

		//
		if(isset($ids['dc_linea_negocio']))
			$data['dg_linea_negocio'] = @$db->getRowById('tb_linea_negocio',$ids['dc_linea_negocio'],'dc_linea_negocio')->dg_linea_negocio;
		else
			$data['dg_linea_negocio'] = NULL;

		//
		if(isset($ids['dc_tipo_producto']))
			$data['dg_tipo_producto'] = @$db->getRowById('tb_tipo_producto',$ids['dc_tipo_producto'],'dc_tipo_producto')->dg_tipo_producto;
		else
			$data['dg_tipo_producto'] = NULL;

		//
		if(isset($ids['dc_banco']))
			$data['dg_banco'] = @$db->getRowById('tb_banco',$ids['dc_banco'],'dc_banco')->dg_banco;
		else
			$data['dg_banco'] = NULL;

		//
		if(isset($ids['dc_banco_cobro']))
			$data['dg_banco_cobro'] = @$db->getRowById('tb_banco_cobro',$ids['dc_banco_cobro'],'dc_banco')->dg_banco;
		else
			$data['dg_banco_cobro'] = NULL;

		//
		if(isset($ids['dc_guia_recepcion']))
			$data['dq_guia_recepcion'] = @$db->getRowById('tb_guia_recepcion',$ids['dc_guia_recepcion'],'dc_guia_recepcion')->dq_guia_recepcion;
		else
			$data['dq_guia_recepcion'] = NULL;

		//
		if(isset($ids['dc_medio_pago_proveedor']))
			$data['dg_medio_pago_proveedor'] = @$db->getRowById('tb_medio_pago_proveedor',$ids['dc_medio_pago_proveedor'],'dc_medio_pago')->dg_medio_pago;
		else
			$data['dg_medio_pago_proveedor'] = NULL;

		//
		if(isset($ids['dc_medio_cobro_cliente']))
			$data['dg_medio_cobro_cliente'] = @$db->getRowById('tb_medio_cobro_cliente',$ids['dc_medio_cobro_cliente'],'dc_medio_cobro')->dg_medio_cobro;
		else
			$data['dg_medio_cobro_cliente'] = NULL;

		//
		if(isset($ids['dc_mercado_cliente']))
			$data['dg_mercado_cliente'] = @$db->getRowById('tb_mercado',$ids['dc_mercado_cliente'],'dc_mercado')->dg_mercado;
		else
			$data['dg_mercado_cliente'] = NULL;

		//
		if(isset($ids['dc_mercado_proveedor']))
			$data['dg_mercado_proveedor'] = @$db->getRowById('tb_mercado',$ids['dc_mercado_proveedor'],'dc_mercado')->dg_mercado;
		else
			$data['dg_mercado_proveedor'] = NULL;

		//
		if(isset($ids['dc_nota_credito'])){
			$data['dq_nota_credito'] = @$db->getRowById('tb_nota_credito',$ids['dc_nota_credito'],'dc_nota_credito')->dq_nota_credito;
			$data['dq_nota_credito_folio'] = @$db->getRowById('tb_nota_credito',$ids['dc_nota_credito'],'dc_nota_credito')->dq_folio;
		}else{
			$data['dq_nota_credito'] = NULL;
			$data['dq_nota_credito_folio'] = NULL;
		}

		//
		if(isset($ids['dc_nota_credito_proveedor'])){
			$data['dq_nota_credito_proveedor'] = @$db->getRowById('tb_nota_credito_proveedor',$ids['dc_nota_credito_proveedor'],'dc_nota_credito')->dq_nota_credito;
			$data['dq_nota_credito_proveedor_folio'] = @$db->getRowById('tb_nota_credito_proveedor',$ids['dc_nota_credito_proveedor'],'dc_nota_credito')->dq_folio;
		}else{
			$data['dq_nota_credito_proveedor'] = NULL;
			$data['dq_nota_credito_proveedor_folio'] = NULL;
		}

		//
		if(isset($ids['dc_segmento']))
			$data['dg_segmento'] = @$db->getRowById('tb_segmento',$ids['dc_segmento'],'dc_segmento')->dg_segmento;
		else
			$data['dg_segmento'] = NULL;

		//
		if(isset($ids['dg_cheque']))
			$data['dg_cheque'] = $ids['dg_cheque'];
		else
			$data['dg_cheque'] = NULL;

		//
		if(isset($ids['df_cheque']))
			$data['df_cheque'] = $db->dateLocalFormat($ids['df_cheque']);
		else
			$data['df_cheque'] = NULL;

		//
		if(isset($ids['dc_bodega']))
			$data['dg_bodega'] = @$db->getRowById('tb_bodega',$ids['dc_bodega'],'dc_bodega')->dg_bodega;
		else
			$data['dg_bodega'] = NULL;

		//
		if(isset($ids['dc_ejecutivo'])){
			$data['dg_ejecutivo'] = @$db->getRowById('tb_funcionario',$ids['dc_ejecutivo'],'dc_funcionario')->dg_nombres
									.' '.
									@$db->getRowById('tb_funcionario',$ids['dc_ejecutivo'],'dc_funcionario')->dg_ap_paterno
									.' '.
									@$db->getRowById('tb_funcionario',$ids['dc_ejecutivo'],'dc_funcionario')->dg_ap_materno;
		}else{
			$data['dg_ejecutivo'] = NULL;
		}

		return $data;

	}

	public static function delEmptyCols(&$data){

		if(!count($data))
			return $data;

		$row = current($data);

		$names = array();
		foreach($row as $campo => $v){
			$names[$campo] = true;
		}

		foreach($data as $row){
			foreach($row as $col => $v){
				if(trim($v) != ''){
					unset($names[$col]);
				}
			}
		}

		foreach($names as $col => $dalomismo){
			foreach($data as &$reference){
				unset($reference[$col]);
			}
		}

		return $data;
	}

	public static function getHeaderAnalitico(&$data){

		if(!count($data))
			return false;

		$headers = array(
			'dg_codigo_cuenta' => array(100,'Código'),
			'dg_cuenta_contable' => array(120,'Cuenta Contable'),
			'dq_debe' => array(90,'DEBE'),
			'dq_haber' => array(90,'HABER'),
			'dg_glosa' => array(140,'Glosa'),
			'dq_factura_compra' => array(80,'FC'),
			'dq_factura_compra_folio' => array(80,'Folio FC'),
			'dq_factura_venta' => array(80,'FV'),
			'dq_factura_venta_folio' => array(80,'Folio FV'),
			'dq_nota_venta' => array(80,'NV'),
			'dq_orden_servicio' => array(80,'OS'),
			'dg_proveedor' => array(120,'Proveedor'),
			'dg_tipo_proveedor' => array(120,'Tipo Prov.'),
			'dg_cliente' => array(120,'Cliente'),
			'dg_tipo_cliente' => array(120,'Tipo Cli.'),
			'dq_orden_compra' => array(80,'OC'),
			'dg_producto' => array(120,'Producto'),
			'dg_cebe' => array(100,'CeBe'),
			'dg_ceco' => array(100,'CeCo'),
			'dg_marca' => array(100,'Marca'),
			'dg_linea_negocio' => array(100,'L. Negocio'),
			'dg_tipo_producto' => array(100,'Tipo Prod.'),
			'dg_banco' => array(100,'Banco'),
			'dg_banco_cobro' => array(100,'Banco cobro'),
			'dq_guia_recepcion' => array(80,'GR'),
			'dg_medio_pago_proveedor' => array(100,'M. Pago Prov.'),
			'dg_medio_cobro_cliente' => array(100,'M. Pago Cli.'),
			'dg_mercado_cliente' => array(100,'Mercado Cli.'),
			'dg_mercado_proveedor' => array(100,'Mercado Prov.'),
			'dq_nota_credito' => array(80,'NC'),
			'dq_nota_credito_folio' => array(80,'Folio NC'),
			'dq_nota_credito_proveedor' => array(80,'NC Prov.'),
			'dq_nota_credito_proveedor_folio' => array(80,'Folio NC Prov.'),
			'dg_segmento' => array(100,'Segmento'),
			'dg_cheque' => array(80,'Cheque'),
			'df_cheque' => array(80,'F. Cheque'),
			'dg_bodega' => array(100,'Bodega'),
			'dg_ejecutivo' => array(120,'Ejecutivo')
		);

		$row = current($data);

		$head = array();
		foreach($row as $campo => $v){
			$head[$campo] = $headers[$campo];
		}

		return $head;

	}



}
