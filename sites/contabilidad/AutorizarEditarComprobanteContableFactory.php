<?php

class AutorizarEditarComprobanteContableFactory extends Factory{
	
	protected $title = 'Autorizar Edicion de comprobante contable';
	protected $comprobante;
	
	public function indexAction(){
		$this->getAutorizacion();
		echo $this->getFormView($this->getTemplateUrl('autorizarComprobante'), array(
			'dc_comprobante' => $this->comprobante->dc_comprobante,
			'dg_comprobante' => $this->comprobante->dg_comprobante
		));
		
	}
	
	private function getAutorizacion(){
		$r  = $this->getRequest();
		$db = $this->getConnection();
		$this->comprobante = $db->getRowById('tb_comprobante_contable', $r->dc_comprobante, 'dc_comprobante');
		$this->comprobarPeriodoContable($this->comprobante);
		$this->comprobarComprobanteNulo();
		if(!is_null($this->comprobante->dc_autorizacion)){
			$st = $db->prepare($db->select('tb_comprobante_contable_registro_edicion', 'dc_usuario_autorizacion, df_fecha_autorizacion',
			'dc_comprobante_contable = :comprobante AND dm_activo = 1'));
			$st->bindValue(':comprobante', $this->comprobante->dc_comprobante, PDO::PARAM_STR);
			$db->stExec($st);
			$autorizacion = $st->fetch(PDO::FETCH_OBJ);
			$st->closeCursor();
			if($autorizacion !== false){
				$msg = $this->getErrorMan()->showInfo("Este comprobante contable fue autorizado para su edición el día {$db->dateLocalFormat($autorizacion->df_fecha_autorizacion)}.", 1);
				echo $this->getOverlayView($msg, array(), Factory::STRING_TEMPLATE);
				exit;
			} else {
				//error fatal va aquí
			}
		}
		return null;
	}
	
	private function comprobarPeriodoContable($comprobante){
		require_once(__DIR__ . '/stuff.class.php');
		$mesContable = $comprobante->dc_mes_contable;
		$anhoContable = $comprobante->dc_anho_contable;
		$estado = ContabilidadStuff::getEstadoPeriodo($mesContable, $anhoContable, ContabilidadStuff::FIELD_ESTADO_CT,
			$this->getConnection(), $this->getEmpresa(), $this->getUserData()->dc_usuario);
		if($estado !== 0){
			$this->getErrorMan()->showWarning('No es posible autorizar el comprobante contable: ' 
				. ContabilidadStuff::getErrorMessage($estado));
			exit;
		}
	}
	
	private function comprobarComprobanteNulo(){
		if($this->comprobante->dm_anulado == 1){
			$msg = $this->getErrorMan()->showWarning("No se puede autorizar la edición de este comprobante debido a que se encuentra anulado.", 1);
			echo $this->getOverlayView($msg, array(), Factory::STRING_TEMPLATE);
            exit;
		}
	}
	
	public function autorizarAction(){
		$db = $this->getConnection();
		$this->comprobante = $db->getRowById('tb_comprobante_contable', $this->getRequest()->dc_comprobante, 'dc_comprobante');
		$this->verificaAutorizacion();
		$values = array(
			'dc_comprobante_contable' => $this->comprobante->dc_comprobante,
			'dc_usuario_autorizacion' => $this->getUserData()->dc_usuario,
			'df_fecha_autorizacion' => $db->getNow(),
			'dm_activo' => 1
		);
		$st = $db->doExec($db->insert('tb_comprobante_contable_registro_edicion', $values));
		$autorizacion = $db->lastInsertId();
		$updateComprobante = array(
			'dc_autorizacion' => $autorizacion
		);
		$db->doExec($db->update('tb_comprobante_contable', $updateComprobante, "dc_comprobante = {$this->comprobante->dc_comprobante}"));
		if($st !== false){
			$this->getErrorMan()->showConfirm("Se ha autorizado la edición del comprobante contable <strong>{$this->comprobante->dg_comprobante}</strong>");
		} else {
			$this->getErrorMan()->showWarning("Ha ocurrido un error al autorizar el comprobante. Contacte con el administrador");
		}
		
	}
	
	public function verificaAutorizacion(){
		if($this->comprobante->dc_autorizacion !== null){
			$db = $this->getConnection();
			$autorizacion = $db->getRowById('tb_comprobante_contable_registro_edicion', $this->comprobante->dc_autorizacion, 'dc_autorizacion');
			$this->getErrorMan()->showInfo("Este comprobante contable fue autorizado para su edición el día {$db->dateLocalFormat($autorizacion->df_fecha_autorizacion)}.");
			exit;
		}
	}
	
}
