<?php
	define("MAIN",1);
	include_once("../../inc/global.php");
	
	$db->query("SET NAMES 'latin1'");
	
	include("template_comprobante/modo1.php");
	
	if(isset($_GET['id'])){
		$cond = "dc_comprobante={$_GET['id']}";
	}else if(isset($_GET['num'])){
		$cond = "dg_comprobante={$_GET['num']}";
	}else{
		die();
	}
	
	$datosComprobante = $db->select("(SELECT * FROM tb_comprobante_contable WHERE {$cond} AND dc_empresa={$empresa}) c
	LEFT JOIN tb_tipo_movimiento mov ON c.dc_tipo_movimiento = mov.dc_tipo_movimiento
	LEFT JOIN tb_usuario u ON c.dc_usuario_creacion = u.dc_usuario
	LEFT JOIN tb_funcionario f ON u.dc_funcionario = f.dc_funcionario",
	"dc_comprobante,dg_comprobante,dg_numero_interno,dg_codigo,c.dc_tipo_movimiento,dg_tipo_movimiento,
	DATE_FORMAT(df_fecha_contable,'%d/%m/%Y') AS df_fecha_contable, DATE_FORMAT(c.df_fecha_emision,'%d/%m/%Y') AS df_fecha_emision,
	dg_glosa, CONCAT_WS(' ',dg_nombres,dg_ap_paterno,dg_ap_materno) AS dg_responsable, dq_saldo");
	
	if(!count($datosComprobante)){
		$error_man->show_fatal_error('Comprobante no encontrado. detalles:',
		array(
			'Acceso Denegado'=> 'No tiene permiso de acceder al comprobante',
			'No encontrado' => 'El comprobante contable no existe'
		));
	}
	
	$datosComprobante = $datosComprobante[0];
	$_GET['id'] = $datosComprobante['dc_comprobante'];
	
	$detalle = $bd->select("(SELECT * FROM tb_comprobante_contable_detalle WHERE dc_comprobante={$_GET['id']} AND dm_activo = 0) d
	LEFT JOIN tb_cuenta_contable c ON d.dc_cuenta_contable = c.dc_cuenta_contable",
	'dq_debe,dq_haber,dg_glosa,dg_codigo,dg_cuenta_contable');
	
	$pdf = new PDF();
	$pdf->SetAutoPageBreak(false);
	$pdf->setDetalle($detalle,$datosComprobante['dq_saldo']);
	$pdf->setFoot($datosComprobante['dc_tipo_movimiento']);
	$pdf->Output();
?>