<?php
require_once( __DIR__ . '/CRUDComprobanteContableFactory.php');

class EditarComprobanteContableFactory extends CRUDComprobanteContableFactory {
	
	public $title = 'Editar Comprobante Contable';
	private $comprobante;
	//Utilizado para procesar la edicion
	private $dc_comprobante;
	private $detalleFinanciero;
	private $saldo;
	private $camposDetallesAnaliticos = array(
		'dc_cuenta_contable' => ':dc_cuenta_contable',
		'dq_debe' => ':dq_debe',
		'dq_haber' => ':dq_haber',
		'dg_glosa' => ':dg_glosa',
		'dc_detalle_financiero' => ':dc_detalle_financiero',
		'dc_factura_compra' => ':dc_factura_compra',
		'dc_factura_venta' => ':dc_factura_venta',
		'dc_nota_venta' => ':dc_nota_venta',
		'dc_orden_servicio' => ':dc_orden_servicio',
		'dc_proveedor' => ':dc_proveedor',
		'dc_tipo_proveedor' => ':dc_tipo_proveedor',
		'dc_cliente' => ':dc_cliente',
		'dc_tipo_cliente' => ':dc_tipo_cliente',
		'dc_orden_compra' => ':dc_orden_compra',
		'dc_producto' => ':dc_producto',
		'dc_cebe' => ':dc_cebe',
		'dc_ceco' => ':dc_ceco',
		'dc_marca' => ':dc_marca',
		'dc_linea_negocio' => ':dc_linea_negocio',
		'dc_tipo_producto' => ':dc_tipo_producto',
		'dc_banco' => ':dc_banco',
		'dc_banco_cobro' => ':dc_banco_cobro',
		'dc_guia_recepcion' => ':dc_guia_recepcion',
		'dc_medio_pago_proveedor' => ':dc_medio_pago_proveedor',
		'dc_medio_cobro_cliente' => ':dc_medio_cobro_cliente',
		'dc_mercado_cliente' => ':dc_mercado_cliente',
		'dc_mercado_proveedor' => ':dc_mercado_proveedor',
		'dc_nota_credito' => ':dc_nota_credito',
		'dc_nota_credito_proveedor' => ':dc_nota_credito_proveedor',
		'dc_segmento' => ':dc_segmento',
		'dg_cheque' => ':dg_cheque',
		'df_cheque' => ':df_cheque',
		'dc_bodega' => ':dc_bodega',
		'dc_ejecutivo' => ':dc_ejecutivo'
	);
	private $camposStringDetalleAnalitico = array(
		'dq_debe',
		'dq_haber',
		'dg_glosa',
		'dg_cheque',
		'df_cheque',
	);
	private $autorizacionEdicion;
	private $motivoEdicion;
    //Variable que contendrán los IDS originales de los detalles financieros
    private $oldFinancieros;
    //Variable que contendrá los ids originales de los detalles analíticos
    private $oldAnaliticos;
	
	private $conservarDetalles = array();
	//El loader está al final
	
	public function indexAction(){
		$this->comprobante = $this->getComprobanteContable();
		$this->mesContable = $this->comprobante->dc_mes_contable;
        $this->anhoContable = $this->comprobante->dc_anho_contable;
		$tipoMovimiento = $this->getTipoMovimiento($this->comprobante->dc_tipo_movimiento);
		$detalleFinanciero = $this->getDetalleComprobante();
		$detalleAnalitico = $this->getDetalleAnalitico($detalleFinanciero);
		$cuentas = $this->obtenerCuentasTipoMovimiento($this->comprobante->dc_tipo_movimiento);
		$fechaContable = explode(' ', $this->comprobante->df_fecha_contable);
		
		$form = $this->getFormView($this->getTemplateUrl('editarComprobanteContable'), array(
			'comprobante' => $this->comprobante->dc_comprobante,
			'tipo_movimiento' => $tipoMovimiento,
			'cuentas' => $cuentas,
			'numero_interno' => $this->comprobante->dg_numero_interno, 
			'glosa' => $this->comprobante->dg_glosa,
			'detalleFinanciero' => $detalleFinanciero,
			'detalleAnalitico' => $detalleAnalitico,
			'fecha_contable' => $fechaContable[0]
		));
		echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
		
	}
	
	/**
	 * Comprobar que el comprobante contable exista y se pueda modificar.
	 * 
	 */
	private function getComprobanteContable(){
		$r = self::getRequest();
		$comprobante = $this->getConnection()->getRowById('tb_comprobante_contable', $r->dc_comprobante, 'dc_comprobante');

        if ($comprobante === false):
            $msg = $this->getErrorMan()->showWarning('El comprobante contable seleccionado no existe, compruebe los datos de entrada y vuelva a intentarlo', 1);
			echo $this->getOverlayView($msg, array(), Factory::STRING_TEMPLATE);
            exit;
        endif;

        if ($comprobante->dm_anulado == 1):
            $msg = $this->getErrorMan()->showWarning('El comprobante contable que intenta ingresar está nulo, no puede ser modificado.', 1);
			echo $this->getOverlayView($msg, array(), Factory::STRING_TEMPLATE);
            exit;
        endif;

        if (is_null($comprobante->dc_autorizacion)):
            $msg = $this->getErrorMan()->showWarning('No tiene permisos para editar el comprobante contable, consulte con el encargado.',1);
			echo $this->getOverlayView($msg, array(), Factory::STRING_TEMPLATE);
            exit;
        endif;
        
        if($comprobante->dm_tipo_comprobante != 0 ):
            $msg = $this->getErrorMan()->showWarning('Los comprobantes contables generados automáticamente por el ERP no pueden ser modificados para evitar descuadres o inconsistencia en la información. consulte con un administrador.', 1);
			echo $this->getOverlayView($msg, array(), Factory::STRING_TEMPLATE);
            exit;
        endif;

        return $comprobante;
	}
	
	private function getTipoMovimiento($tipoMovimiento){
		$db = $this->getConnection();
		$st = $db->doQuery($db->select('tb_tipo_movimiento', 'dc_tipo_movimiento, dg_tipo_movimiento', "dc_tipo_movimiento = {$tipoMovimiento} AND dc_empresa  = {$this->getEmpresa()} AND dm_activo = 1"));
		$tipoMovimiento = $st->fetch(PDO::FETCH_OBJ);
		if($tipoMovimiento === false){
			$this->getErrorMan()->showErrorRedirect("No se encontró el tipo de movimiento especificado, intentelo nuevamente.", self::buildActionUrl('index'));
		}
		return $tipoMovimiento;
	}
	
	private function getDetalleComprobante() {
        $db = $this->getConnection();
        $detalle = $db->doQuery($db->select('tb_comprobante_contable_detalle d
                               JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = d.dc_cuenta_contable', 
							   'd.dc_detalle,cc.dg_codigo,d.dq_debe,d.dq_haber,d.dg_glosa,cc.dg_cuenta_contable,cc.dc_cuenta_contable', "d.dc_comprobante = {$this->comprobante->dc_comprobante} AND d.dm_activo = 0"));
        $db->stExec($detalle);
        return $detalle->fetchAll(PDO::FETCH_OBJ);
		
    }
	
	private function getDetalleAnalitico($detalleFinanciero){
		$idDetalle = array();
		foreach($detalleFinanciero as $v){
			$idDetalle[] = $v->dc_detalle;
		}
		$ids = implode(',', $idDetalle);
		$db = $this->getConnection();
		$st = $db->doQuery($db->select('tb_comprobante_contable_detalle_analitico', '*', "dc_detalle_financiero IN ({$ids})"));
		return $st->fetchAll(PDO::FETCH_OBJ);
	}
	/**
	 * editarComprobanteContableAction
	 * inicia la edición de un comprobante, capturando todos los valores que han sido enviados desde la base de datos
	 *
	 */
	public function editarComprobanteContableAction(){
		$r = self::getRequest();
		$this->dc_comprobante = $r->dc_comprobante;
		$this->motivoEdicion = trim($r->dg_motivo);
		if(!isset($r->dg_glosa) || count($r->dg_glosa) < 2 ){
			$this->getErrorMan()->showWarning("Un comprobante contable debe tener al menos dos detalles. Verifique la información ingresada");
			exit;
		}
		//Variable que contendrá todos los datos de un detalle de comprobante contable, incluyendo los detalles analíticos si existen.
		$detalleFinanciero = array();
		//Se agregan los ids que ya existen previamente a $detalle financiero
		if(isset($r->id_detalle_comprobante)){
			foreach($r->id_detalle_comprobante as $c => $v){
				$detalleFinanciero[]['dc_detalle'] = $v;
			}
		}
		//Se agregan las cuentas contables. Si se han creado nuevos detalles, se agregan dc_detalle vacios para identificarlos
		foreach($r->dc_cuenta_contable as $c => $v){
			if(!isset($detalleFinanciero[$c]['dc_detalle'])){
				$detalleFinanciero[$c]['dc_detalle'] = '';
			}
			$detalleFinanciero[$c]['dc_cuenta_contable'] = $v;
		}
		//Agregar los debe
		foreach($r->dq_debe as $c => $v){
			$detalleFinanciero[$c]['dq_debe'] = $v;
		}
		//agregar los haberes
		foreach($r->dq_haber as $c => $v){
			$detalleFinanciero[$c]['dq_haber'] = $v;
		}
		//agregar las glosas
		foreach($r->dg_glosa as $c => $v){
			$detalleFinanciero[$c]['dg_glosa'] = $v;
		}
        //Agregar los item_id
        foreach($r->item_id as $c => $v){
            $detalleFinanciero[$c]['item_id'] = $v;
        }
        //Agregar los detalles analítico si existen
        foreach ($detalleFinanciero as $c => $v){
            if(isset($r->detalle_analitico[$v['item_id']])){
                $detalleAnalitico  = array();
                foreach($r->detalle_analitico[$v['item_id']] as $valor => $indice){
                    foreach($indice as $i => $v){
						if($valor == 'an_dc_nota_credito_cliente'){
							$valor = 'an_dc_nota_credito';
						}
						$detalleAnalitico[$i][$valor] = $v;
					}
                }
                $detalleFinanciero[$c]['detalle_analitico'] = $detalleAnalitico;
            }
        }
		//agregar los detalles analíticos si existen
		// if(isset($r->detalle_analitico)){
			// foreach($r->detalle_analitico as $c => $valores){
				// //se crea un array que contendrá cada detalle analítico
				// $detalleAnalitico  = array();
				// //se recorren los campos 
				// foreach($valores as $valor => $indice){
					// foreach($indice as $i => $v){
						// if($valor == 'an_dc_nota_credito_cliente'){
							// $valor = 'an_dc_nota_credito';
						// }
						// $detalleAnalitico[$i][$valor] = $v;
					// }
				// }
				// $detalleFinanciero[$c]['detalle_analitico'] = $detalleAnalitico;
			// }

		//
		$this->detalleFinanciero = $detalleFinanciero;
		$this->prepararEdicion();
	}
	
	private function prepararEdicion(){
		if(empty($this->detalleFinanciero)){
			$this->getErrorMan()->showWarning("Error al generar los comprobantes. Contacte al administrador");
			exit;
		}
		$r = $this->getRequest();
		//Obtener la fecha contable y asignarlo a la variable en CRUDComprobanteContableFactory
		$this->mesContable = substr($r->df_contable_comprobante, 3, 2);
		$this->anhoContable = substr($r->df_contable_comprobante, -4);
		//Validar que esté disponible para el usuario actual, en CRUDComprobanteContableFactory
		$this->validarPeriodoContable();
		//Validar que las cuentas contables sean válidas
		$this->validarCuentasContables();
		//Validar que los vaores de DEBE y HABER cuadren
		$this->saldo = $this->validarSaldos();
		//Validar Detalles Analíticos
		$this->validarDetalleAnalitico();
		// Validar si el comprobante es editable
		$this->validarEditable();
		// validar que el motivo sea un string mayor a 5 caracteres
		$this->validarMotivo();
		//Editar comprobante contable
		$this->procesarEditar();
	}
	
	private function validarCuentasContables(){
		foreach($this->detalleFinanciero as $v){
			if(!isset($v['dc_cuenta_contable'])){
				$this->getErrorMan()->showWarning('Existe un error en una de las cuentas contables. Por favor valide la información e intente nuevamente.');
				exit;
			}
		}
	}
	
	//Función obtenida desde crearComprobanteFactory para validar los saldos
	private function validarSaldos() {
		$dq_saldo_debe = 0;
		$dq_saldo_haber = 0;

		$request = self::getRequest();
		$this->initFunctionsService();

		foreach ($request->dq_debe as $dq_debe) {
		  $dq_saldo_debe += Functions::fixNumberToLocal(Functions::toNumber($dq_debe));
		}

		foreach ($request->dq_haber as $dq_haber) {
		  $dq_saldo_haber += Functions::fixNumberToLocal(Functions::toNumber($dq_haber));
		}

		if ($dq_saldo_debe != $dq_saldo_haber) {
		  $this->getErrorMan()->showWarning('Los saldos DEBE y HABER no coinciden, verifique estos valores y vuelva a intentarlo');
		  exit;
		}

		return $dq_saldo_debe;
	}
	
	//Validación de los detalles analíticos
	private function validarDetalleAnalitico(){
		//Recorrer los detalles financieros
		foreach($this->detalleFinanciero as $elementos){
			//Variables que permitirán la validación de la cuadratura debe y haber por cada detalle analítico.
			$dqDebe = 0;
			$dqHaber = 0;
			if(isset($elementos['detalle_analitico'])){
				foreach($elementos['detalle_analitico'] as $detalles){
					foreach($detalles as $c => $v){
						switch($c){
							case 'an_dq_debe':
								$dqDebe += $v;
								break;
							case 'an_dq_haber':
								$dqHaber += $v;
								break;
						}	
					}
				}
				if(($dqDebe != 0 && $dqHaber != 0) || ($dqDebe == 0 && $dqHaber == 0)){
					$this->getErrorMan()->showWarning("Los saldos DEBE y HABER del detalle analítico no coinciden. Verifique los valores e intente nuevamente.");
					exit;
				}
			} else {
				continue;
			}
		}
	}
	
	private function validarEditable(){
		$comprobante = $this->getConnection()->getRowById('tb_comprobante_contable', $this->dc_comprobante, 'dc_comprobante');
		if(is_null($comprobante->dc_autorizacion)){
			$this->getErrorMan()->showWarning("Éste comprobante ya ha sido editado y no tiene permisos para modificarse nuevamente. Contacte al encargado.");
			exit;
		}
		$this->autorizacionEdicion = $comprobante->dc_autorizacion;
	}
	
	private function validarMotivo(){
		if(strlen($this->motivoEdicion) < 5){
			$this->getErrorMan()->showWarning("Es necesario escribir un motivo de edición. El mínimo de caracteres para este es 5");
			exit;
		}
	}
	
	// Actualizar el comprobante contable
	private function procesarEditar(){
		//iniciar db en modo transaccion
		$this->getConnection()->start_transaction();
        //Setear los ids de los comprobantes contables antiguos
        $this->oldFinancieros = $this->getDetallesFinancierosAntiguos();
        $this->oldAnaliticos = $this->getDetallesAnaliticosAntiguos();
		// Editar Comprobante Contable
		$this->editarComprobantecontable();
		//insertar los detalles Financieros
		$this->operarDetallesFinancieros();
        //Insertar los detalles analíticos
        $this->operarDetallesAnaliticos();
		//Eliminar los detalles que se desecharon del comprobante contable durante la edición
		$this->eliminarDetalles();
		//Registrar fecha y usuario de edicion
		$this->revocarAutorizacionEdicion();
		//Finalizar la transaccion en la base de datos.
		$this->getConnection()->commit();
		$comprobante = $this->getConnection()->getRowById('tb_comprobante_contable', $this->dc_comprobante, 'dc_comprobante');
		$this->getErrorMan()->showConfirm("Se ha modificado exitosamente el comprobante contable <b>{$comprobante->dg_comprobante}</b>");
	}
	
	private function editarComprobanteContable(){
		$r = self::getRequest();
		$db = $this->getConnection();
		$valores = array(
			'df_fecha_contable' => ':fechaContable',
			'dc_mes_contable' => ':mes',
			'dc_anho_contable' => ':anho',
			'dg_glosa' => ':glosa',
			'dq_saldo' => ':saldo',
			'dc_autorizacion' => ':autorizacion'
		);
		$conditions = 'dc_comprobante = :comprobante';
		$st = $db->prepare($db->update('tb_comprobante_contable', $valores, $conditions));
		$st->bindValue(':comprobante', $this->dc_comprobante ,PDO::PARAM_INT);
		$st->bindValue(':fechaContable', $db->sqlDate2($r->df_contable_comprobante), PDO::PARAM_STR);
		$st->bindValue(':mes', $this->mesContable, PDO::PARAM_INT);
		$st->bindValue(':anho', $this->anhoContable, PDO::PARAM_INT);
		$st->bindValue(':glosa', $r->dg_glosa_general, PDO::PARAM_STR);
		$st->bindValue(':saldo', $this->saldo, PDO::PARAM_STR);
		$st->bindValue(':autorizacion', null, PDO::PARAM_STR);
		$db->stExec($st);
	}
    
    private function operarDetallesFinancieros(){
        //recorrer los detalles
        foreach($this->detalleFinanciero as $dfIndice => $dfValor){
            //se evalúa si el detalle debe ser insertado o modificado, y se envía para que sea procesado
			if(empty($dfValor['dc_detalle'])){
				$idFinanciero = $this->insertarDetalleFinanciero($this->detalleFinanciero[$dfIndice]);
				$this->detalleFinanciero[$dfIndice]['dc_detalle'] = $idFinanciero;
			} else {
				$this->editarDetalleFinanciero($this->detalleFinanciero[$dfIndice]);
			}
        }
    }
    
    private function operarDetallesAnaliticos(){
        //recorrer los detalles
        foreach($this->detalleFinanciero as $dfIndice => $dfValor){
            if(isset($dfValor['detalle_analitico'])){
                foreach($dfValor['detalle_analitico'] as $daIndice => $daValor){
                    //Asignar dc_detalle_financiero y dc_cuenta_contable al detalle analítico
                    $this->detalleFinanciero[$dfIndice]['detalle_analitico'][$daIndice]['an_dc_detalle_financiero'] = $dfValor['dc_detalle'];
                    $this->detalleFinanciero[$dfIndice]['detalle_analitico'][$daIndice]['an_dc_cuenta_contable'] = $dfValor['dc_cuenta_contable'];
                    if(empty($daValor['an_dc_detalle'])){
                        $idAnalitico = $this->insertarDetalleAnalitico(
                            $this->detalleFinanciero[$dfIndice]['detalle_analitico'][$daIndice],
                            $this->detalleFinanciero[$dfIndice]
                        );
                        $this->detalleFinanciero[$dfIndice]['detalle_analitico'][$daIndice]['an_dc_detalle'] = $idAnalitico;
                    } else {
                        $idAnalitico = $this->editarDetalleAnalitico(
                            $this->detalleFinanciero[$dfIndice]['detalle_analitico'][$daIndice],
                            $this->detalleFinanciero[$dfIndice]
                        );
                    }
                }
            } 
            else{
                $detalleAnalitico = $this->generarDetalleAnalitico($this->detalleFinanciero[$dfIndice]);
                $this->detalleFinanciero[$dfIndice]['detalle_analitico'][] = $detalleAnalitico;
            }
        }
    }
	
	private function insertarDetalleFinanciero($detalleFinanciero){
		$db = $this->getConnection();
		$values  = array(
			'dc_comprobante' => ':comprobante',
			'dc_cuenta_contable' => ':cuentaContable',
			'dq_debe' => ':debe',
			'dq_haber' => ':haber',
			'dg_glosa' => ':glosa'
		);
		$st = $db->prepare($db->insert('tb_comprobante_contable_detalle', $values));
		$st->bindValue(':comprobante', $this->dc_comprobante,PDO::PARAM_INT);
		$st->bindValue(':cuentaContable', $detalleFinanciero['dc_cuenta_contable'],PDO::PARAM_INT);
		$st->bindValue(':debe', Functions::fixNumberToLocal(Functions::toNumber($detalleFinanciero['dq_debe'])),PDO::PARAM_STR);
		$st->bindValue(':haber', Functions::fixNumberToLocal(Functions::toNumber($detalleFinanciero['dq_haber'])),PDO::PARAM_STR);
		$st->bindValue(':glosa', $detalleFinanciero['dg_glosa'],PDO::PARAM_STR);
		$db->stExec($st);
		$id = $db->lastInsertId();
		return $id;
		
	}
	
	private function editarDetalleFinanciero($detalleFinanciero){
		$db = $this->getConnection();
		$values = array(
			'dc_cuenta_contable' => ':cuentaContable',
			'dq_debe' => ':debe',
			'dq_haber' => ':haber',
			'dg_glosa' => ':glosa'
		);
		$conditions = 'dc_detalle = :idDetalle';
		$st = $db->prepare($db->update('tb_comprobante_contable_detalle', $values, $conditions));
		$st->bindValue(':cuentaContable', $detalleFinanciero['dc_cuenta_contable'],PDO::PARAM_INT);
		$st->bindValue(':debe', Functions::fixNumberToLocal(Functions::toNumber($detalleFinanciero['dq_debe'])),PDO::PARAM_STR);
		$st->bindValue(':haber', Functions::fixNumberToLocal(Functions::toNumber($detalleFinanciero['dq_haber'])),PDO::PARAM_STR);
		$st->bindValue(':glosa', $detalleFinanciero['dg_glosa'],PDO::PARAM_STR);
		$st->bindValue(':idDetalle', $detalleFinanciero['dc_detalle'],PDO::PARAM_STR);
		$db->stExec($st);
	}
	
	private function insertarDetalleAnalitico($detalleAnalitico, $detalleFinanciero){
		// Poner los elementos del array que esten vacíos en null
		$this->setNullCamposVacios($detalleAnalitico);
		$db = $this->getConnection();
		$values = $this->camposDetallesAnaliticos;
		//En este array deben agregarse los campos que sean de tipo STR para validarlos con PDO a la hora de llenar los bindValues
		$valueString = $this->camposStringDetalleAnalitico;
		$st = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico', $values));
		foreach($values as $c => $v){
			$dataType = PDO::PARAM_INT;
			//Evaluar si $c es un campo que requiere validarse como string
			if(in_array($c, $valueString)){
				$dataType = PDO::PARAM_STR;
			}
			$st->bindValue($v, $detalleAnalitico['an_' . $c], $dataType);
		}
		$db->stExec($st);
        $id = $db->lastInsertId();
        return $id;
	}
	
	private function editarDetalleAnalitico($detalleAnalitico, $detalleFinanciero){
		// Poner los elementos del array que esten vacíos en null
		$this->setNullCamposVacios($detalleAnalitico);
		$db = $this->getConnection();
		$values = $this->camposDetallesAnaliticos;
		//En este array deben agregarse los campos que sean de tipo STR para validarlos con PDO a la hora de llenar los bindValues
		$valueString = $this->camposStringDetalleAnalitico;
		$st = $db->prepare($db->update('tb_comprobante_contable_detalle_analitico', $values, 'dc_detalle = :idDetalle'));
		foreach($values as $c => $v){
			$dataType = PDO::PARAM_INT;
			//Evaluar si $c es un campo que requiere validarse como string
			if(in_array($c, $valueString)){
				$dataType = PDO::PARAM_STR;
			}
			$st->bindValue($v, $detalleAnalitico['an_' . $c], $dataType);
		}
		$st->bindValue(':idDetalle', $detalleAnalitico['an_dc_detalle'], PDO::PARAM_INT);
		$db->stExec($st);
	}
	
	private function generarDetalleAnalitico($detalleFinanciero){
		$detalleAnalitico = array();
		foreach($this->camposDetallesAnaliticos as $c => $v){
			$detalleAnalitico['an_' . $c] = null;
		}
		$detalleAnalitico['an_dq_debe'] = Functions::fixNumberToLocal(Functions::toNumber($detalleFinanciero['dq_debe']));
		$detalleAnalitico['an_dq_haber'] = Functions::fixNumberToLocal(Functions::toNumber($detalleFinanciero['dq_haber']));
		$detalleAnalitico['an_dg_glosa'] = $detalleFinanciero['dg_glosa'];
		$detalleAnalitico['an_dc_cuenta_contable'] = $detalleFinanciero['dc_cuenta_contable'];
		$detalleAnalitico['an_dc_detalle_financiero'] = $detalleFinanciero['dc_detalle'];
		$id = $this->insertarDetalleAnalitico($detalleAnalitico, $detalleFinanciero);
        $detalleAnalitico = $this->getConnection()->getRowById('tb_comprobante_contable_detalle_analitico', $id, 'dc_detalle');
        return $this->formatearDetalleAnalitico($detalleAnalitico);
	}
	
	private function setNullCamposVacios(&$coleccion){
		foreach($coleccion as $c => &$v){
			if($c == 'an_dq_debe' || $c == 'an_dq_haber'){
				continue;
			}
			if(empty($v)){
				$v = null;
			}
		}
	}
    
    private function formatearDetalleAnalitico($detalleAnalitico){
        $detalle = array();
        foreach($detalleAnalitico as $c => $v){
            $detalle['an_'.$c] = $v;
        }
        return $detalle;
        
    }
    
    private function eliminarDetalles(){
        $newFinancieros = $this->getDetallesFinancierosNuevos();
        $newAnaliticos = $this->getDetallesAnaliticosNuevos();
        $deleteFinancieros = array_diff($this->oldFinancieros, $newFinancieros);
        $deleteAnaliticos = array_diff($this->oldAnaliticos, $newAnaliticos);
        $this->eliminarDetallesAnaliticos($deleteAnaliticos);
        $this->eliminarDetallesFinancieros($deleteFinancieros);
    }
    
    private function getDetallesFinancierosAntiguos(){
        $db = $this->getConnection();
        $st = $db->prepare($db->select('tb_comprobante_contable_detalle', 'dc_detalle', 'dc_comprobante = ?'));
        $st->bindValue(1, $this->dc_comprobante, PDO::PARAM_INT);
        $db->stExec($st);
        $ids = array();
        foreach($st->fetchAll(PDO::FETCH_OBJ) as $v){
            $ids[] = $v->dc_detalle;
        }
        return $ids;
    }
    
    private function getDetallesAnaliticosAntiguos(){
        $idsFinancieros = $this->oldFinancieros;
        $bindIds =  str_repeat("?,", count($idsFinancieros));
        $bindIds = trim($bindIds, ',');
        $db = $this->getConnection();
        $st = $db->prepare($db->select('tb_comprobante_contable_detalle_analitico', 'dc_detalle', "dc_detalle_financiero IN ({$bindIds})"));
        $contador = 1;
        foreach($idsFinancieros as $v){
            $st->bindValue($contador, $v, PDO::PARAM_INT);
            $contador++;
        }
        $db->stExec($st);
        
        $ids = array();
        foreach($st->fetchAll(PDO::FETCH_OBJ) as $v){
            $ids[] = $v->dc_detalle;
        }
        return $ids;
    }
    
    private function getDetallesFinancierosNuevos(){
        $ids = array();
        foreach($this->detalleFinanciero as $v){
            $ids[] = $v['dc_detalle'];
        }
        return $ids;
    }
    
    private function getDetallesAnaliticosNuevos(){
        $ids = array();
        foreach($this->detalleFinanciero as $c){
            foreach ($c['detalle_analitico'] as $v){
                $ids[] = $v['an_dc_detalle'];
            }
        }
        return $ids;
    }
    
    private function eliminarDetallesAnaliticos($deleteAnaliticos){
        if(empty($deleteAnaliticos)){
            return;
        }
        $db = $this->getConnection();
        $bindIds =  str_repeat("?,", count($deleteAnaliticos));
        $bindIds = trim($bindIds, ',');
        $st = $db->prepare($db->delete('tb_comprobante_contable_detalle_analitico', "dc_detalle IN ({$bindIds})" ));
        $contador = 1;
        foreach($deleteAnaliticos as $v){
            $st->bindValue($contador, $v, PDO::PARAM_INT);
            $contador++;
        }
        $db->stExec($st);
    }
    
	private function eliminarDetallesFinancieros($deleteFinancieros){
        if(empty($deleteFinancieros)){
            return;
        }
        $db = $this->getConnection();
        $bindIds =  str_repeat("?,", count($deleteFinancieros));
        $bindIds = trim($bindIds, ',');
        $st = $db->prepare($db->delete('tb_comprobante_contable_detalle', "dc_detalle IN ({$bindIds})" ));
        $contador = 1;
        foreach($deleteFinancieros as $v){
            $st->bindValue($contador, $v, PDO::PARAM_INT);
            $contador++;
        }
        $db->stExec($st);
    }
	
	
	
	public function revocarAutorizacionEdicion(){
		$db = $this->getConnection();
		$values = array(
			'dc_usuario_edicion' => ':usuario',
			'df_fecha_edicion' => $db->getNow(),
			'dg_motivo' => ':motivo',
			'dm_activo' => 0
		);
		$conditions = 'dc_autorizacion = :autorizacion';
		$st = $db->prepare($db->update('tb_comprobante_contable_registro_edicion', $values, $conditions));
		$st->bindValue(':usuario', $this->getUserData()->dc_usuario, PDO::PARAM_INT);
		$st->bindValue(':motivo', $this->motivoEdicion, PDO::PARAM_STR);
		$st->bindValue(':autorizacion', $this->autorizacionEdicion, PDO::PARAM_STR);
		$db->stExec($st);
	}
	
	// Ayuda a determinar a la vista general si debe abrir la página en una capa sobrepuesta o la debe abrir como una vista completa.
	public function verificarEditableAction(){
		$r = $this->getRequest();
		$comprobante = $this->getConnection()->getRowById('tb_comprobante_contable', $r->dc_comprobante, 'dc_comprobante');
		$editable = is_null($comprobante->dc_autorizacion) ? 0:1;
		echo json_encode($editable);
	}
	
	//permite la correcta inicialización de la vista a mostrar.
	public function loaderAction(){
		$this->validarInicio();
		echo $this->getView($this->getTemplateUrl('initEdit'), array(
			'dc_comprobante' => $this->getRequest()->dc_comprobante
		));
	}
	
	private function validarInicio(){
		$this->comprobante = $this->getComprobanteContable();
		$this->mesContable = $this->comprobante->dc_mes_contable;
        $this->anhoContable = $this->comprobante->dc_anho_contable;
		$tipoMovimiento = $this->getTipoMovimiento($this->comprobante->dc_tipo_movimiento);
		$detalleFinanciero = $this->getDetalleComprobante();
		$detalleAnalitico = $this->getDetalleAnalitico($detalleFinanciero);
		$cuentas = $this->obtenerCuentasTipoMovimiento($this->comprobante->dc_tipo_movimiento);
		$fechaContable = explode(' ', $this->comprobante->df_fecha_contable);
	}
	
}
