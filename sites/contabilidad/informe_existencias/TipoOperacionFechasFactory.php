<?php

class TipoOperacionFechasFactory extends Factory{
    /*
        tipos de operacion:

           *1           La fecha se obtiene desde la misma fecha de creación del movimiento
                            14, 21, 38, 45, 48
           *2           La fecha se obtiene de la devolución de guías de despacho.
                            13, 20
           *3           La fecha se obtiene de la emisión de una guía de despacho a proveedor
                            44
           *4           La fecha se obtiene de la emisión del ajuste de cantidades
                            25, 26, 27, 28, 39, 40, 51, 52
            5           La fecha se obtiene de la devolución de guías de despacho de proveedor.
                            53, 54, 55, 56
           *6           La fecha se obtendrá de la emisión de la guía de despacho a cliente
                            1, 3, 4, 5, 8, 15, 16, 17, 18, 19, 23, 32, 33, 49
                            Crear Tipo Movimiento Anulación guía despacho adis e iscomp
           *7           Se utilizará la fecha de emisión de la guia de recepcion
                            2, 6, 16, 22, 34, 43
                            Crear tipo de movimiento Devolucion por anulación de guia rececpcion todas las empresas
                            Cambiar tipo movimiento tb_guia_recepcion al 23 (entrada)
           *8           Se utlizará la fecha de emisión de la nota de credito
                            30, 31, 46, 47
            default     Se solicitará proporcionar la fecha manualmente
                            19 o cualquier otro
                            En el caso del tipo de movimiento 3: Si está asociada a una guía de despacho, se utilizará esa fecha.
            
            debo llamar a js_data.disable() si no está vacío el array de fechas (si existen fechas por asignar.;
        
    */
    protected $title = 'Asignación de Fecha manual';
    
    private $movimientosVerificados = array();
    private $movimientosSinFecha = array();
    private $movimientosConError = array();
    
    public function indexAction(){
        //echo $this->getFullView('Hola Mundo', array(), self::STRING_TEMPLATE);
        //sleep(2);
        $movimientos = $this->getMovimientosBodegaSinFecha();
        $db = $this->getConnection();
        $db->start_transaction();
        foreach($movimientos as $v){
            if(in_array($v->dc_movimiento, $this->movimientosVerificados)){
                continue;
            }
            $this->prepararAsignarFechaMovimientoBodega($v);
        }
        $db->commit();
        $movimientosSinFecha = $this->procesarMovimientosSinFecha();
        $params = array(
                'movimientosSinFecha' => $movimientosSinFecha,
            );
        
        echo $this->getView($this->getTemplateUrl('listado'), $params);
        
        //echo 'Días soleados se vienen después de la creación de este módulo';
    } 
    
    private function getMovimientosBodegaSinFecha(){
        $db = $this->getConnection();
        $table = 'tb_movimiento_bodega';
        $fields = '*';
        $conditions = 'df_movimiento IS NULL AND dc_empresa = :empresa';
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($st);
        
        return $st->fetchAll(PDO::FETCH_OBJ);
    }
    
    private function prepararAsignarFechaMovimientoBodega($movimiento){
        $operacion = $this->getTipoOperacion($movimiento->dc_tipo_movimiento, $movimiento);
        $this->asignarFechaMovimientoBodega($movimiento, $operacion);
        
    }
    
    private function getTipoOperacion($tipoMovimiento){
        $movimientoLogistico = $this->getTipoMovimiento($tipoMovimiento);
        return $movimientoLogistico->dc_tipo_operacion;
    }
    
    private function asignarFechaMovimientoBodega($movimiento, $operacion){
        switch($operacion){
            case 1:
                $this->asignaMismaFechaMovimiento($movimiento);
                break;
            case 2:
                $this->obtenerFechaDevolucionGuiaDespacho($movimiento);
                break;
            case 3:
                $this->obtenerFechaGuiaDespachoProveedor($movimiento);
                break;
            case 4:
                $this->obtenerFechaAjusteCantidad($movimiento);
                break;
            case 5:
                $this->obtenerFechaDevolucionGuiaDespachoProveedor($movimiento);
                break;
            case 6:
                $this->obtenerFechaGuiaDespachoCliente($movimiento);
                break;
            case 7:
                $this->obtenerFechaGuiaRecepcion($movimiento);
                break;
            case 8:
                $this->obtenerFechaNotaCredito($movimiento);
                break;
            default:
                $this->asignarMovimientoPedirFecha($movimiento);
                break;
        }
    }
    
    /*
     * actualizarFechamovimiento
     * Método que será utilizado por todas los demás métodos para asignar la fecha correspondiente al movimiento.
     * 
     */
    private function actualizarFechamovimiento($dc_movimiento, $fecha){
        $db = $this->getConnection(); 
        $table = 'tb_movimiento_bodega';
        $fields = array('df_movimiento' => ':fechaMovimiento');
        $conditions = 'dc_movimiento = :movimiento AND dc_empresa = :empresa';
        
        $st = $db->prepare($db->update($table, $fields, $conditions));
        $st->bindValue(':fechaMovimiento', $fecha, PDO::PARAM_INT);
        $st->bindValue(':empresa', $this->getEmpresa(), PDO::PARAM_INT);
        $st->bindParam(':movimiento', $dc_movimiento, PDO::PARAM_INT);
        $db->stExec($st);
    }
    
    /*
     * asignarMovimientoPedirFecha
     * Este método se utilizará cada vez que sea imposible obtener la fecha de alguno de los documentos relacionados.
     * 
     */
    private function asignarMovimientoPedirFecha($movimiento){
        $this->movimientosSinFecha[] = $movimiento;
        $this->movimientosVerificados[] = $movimiento->dc_movimiento;
    }
    
    //Caso 1
    private function asignaMismaFechaMovimiento($movimiento){
        $this->actualizarFechamovimiento($movimiento->dc_movimiento, $movimiento->df_creacion);
        $this->movimientosVerificados[] = $movimiento->dc_movimiento;
        if($movimiento->dc_movimiento_relacionado){
            $this->actualizarFechamovimiento($movimiento->dc_movimiento_relacionado, $movimiento->df_creacion);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento_relacionado;
        }
    }
    
    //caso 2
    private function obtenerFechaDevolucionGuiaDespacho($movimiento){
        if($movimiento->dc_devolucion_guia_despacho > 0){
            $devolucion = $this->getDevolucionGuiaDespacho($movimiento->dc_devolucion_guia_despacho);
            if($devolucion === false || empty($devolucion->df_emision)){
                $this->asignarMovimientoPedirFecha($movimiento);
                return;
            }
            $this->actualizarFechamovimiento($movimiento->dc_movimiento, $devolucion->df_emision);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento;
            
        } else {
            $this->asignarMovimientoPedirFecha($movimiento);
        }
    }
    
    //Caso 3
    private function obtenerFechaGuiaDespachoProveedor($movimiento){
        if($movimiento->dc_guia_despacho_proveedor){
            $gdp = $this->getGuiaDespachoProveedor($movimiento->dc_guia_despacho_proveedor);
            if($gdp === false || empty($gdp->df_emision)){
                $this->asignarMovimientoPedirFecha($movimiento);
                return;
            }
            $this->actualizarFechamovimiento($movimiento->dc_movimiento, $gdp->df_emision);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento;
        
        } else {
            $this->asignarMovimientoPedirFecha($movimiento);
        }
    }
    
    //caso 4
    private function obtenerFechaAjusteCantidad($movimiento){
        if($movimiento->dc_comprobante_ajuste_logistico){
            $ajuste = $this->getComprobanteAjusteLogistico($movimiento->dc_comprobante_ajuste_logistico);
            if($ajuste === false || empty($ajuste->df_emision)){
                $this->asignarMovimientoPedirFecha($movimiento);
                return;
            }
            $this->actualizarFechamovimiento($movimiento->dc_movimiento, $ajuste->df_emision);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento;
        } else {
            $this->asignarMovimientoPedirFecha($movimiento);
        }
    }
    
    //caso 5
    private function obtenerFechaDevolucionGuiaDespachoProveedor($movimiento){
        if($movimiento->dc_devolucion_guia_despacho_proveedor){
            $dgdp = $this->getDevolucionGuiaDespachoProveedor($movimiento->dc_devolucion_guia_despacho_proveedor);
            if($dgdp === false || empty($dgdp->df_emision)){
                $this->asignarMovimientoPedirFecha($movimiento);
                return;
            }
            $this->actualizarFechamovimiento($movimiento->dc_movimiento, $gd->df_emision);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento;
        } else {
            $this->asignarMovimientoPedirFecha($movimiento);
        }
    }
    
    //caso 6
    private function obtenerFechaGuiaDespachoCliente($movimiento){
        if($movimiento->dc_guia_despacho){
            $gd = $this->getGuiaDespacho($movimiento->dc_guia_despacho);
            if($gd === false || empty($gd->df_emision)){
                $this->asignarMovimientoPedirFecha($movimiento);
                return;
            }
            $this->actualizarFechamovimiento($movimiento->dc_movimiento, $gd->df_emision);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento;
        } else {
            $this->asignarMovimientoPedirFecha($movimiento);
        }
    }
    
    //caso 7
    private function obtenerFechaGuiaRecepcion($movimiento){
        if($movimiento->dc_guia_recepcion){
            $gr = $this->getGuiaRecepcion($movimiento->dc_guia_recepcion);
            if($gr === false || empty($gr->df_recepcion)){
                $this->asignarMovimientoPedirFecha($movimiento);
                return;
            }
            $this->actualizarFechamovimiento($movimiento->dc_movimiento, $gr->df_recepcion);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento;
        } else {
            $this->asignarMovimientoPedirFecha($movimiento);
        }
    }
    
    //caso 8
    private function obtenerFechaNotaCredito($movimiento){
        if($movimiento->dc_nota_credito){
            $nc = $this->getNotaCredito($movimiento->dc_nota_credito);
            if($nc === false || empty($nc->df_emision)){
                $this->asignarMovimientoPedirFecha($movimiento);
                return;
            }
            $this->actualizarFechamovimiento($movimiento->dc_movimiento, $nc->df_emision);
            $this->movimientosVerificados[] = $movimiento->dc_movimiento;
        } else {
            $this->asignarMovimientoPedirFecha($movimiento);
        }
    }
    
    /*
     * procesarMovimientosSinFecha
     * Obtiene todos los movimientos a los cuales no se le pudo asignar una fecha y se procesan
     * para que luego puedan ser enviados a una vista que muestre la información del elemento y permita
     * deducir la fecha para el área contable.
     * 
     */
    private function procesarMovimientosSinFecha(){
        $db = $this->getConnection();
        $rezagados = array();
        foreach($this->movimientosSinFecha as $v){
            $msf = new stdClass();
            $msf->id = $v->dc_movimiento;
            $msf->producto = $this->getProducto($v->dc_producto);
            $msf->cliente = $this->getCliente($v->dc_cliente);
            $msf->proveedor = $this->getProveedor($v->dc_proveedor);
            $msf->movimiento = $this->getTipoMovimiento($v->dc_tipo_movimiento);
            $msf->bodegaEntrada = $this->getBodega($v->dc_bodega_entrada);
            $msf->bodegaSalida = $this->getBodega($v->dc_bodega_salida);
            $msf->monto = $v->dq_monto;
            $msf->cantidad = $v->dq_cantidad;
            $msf->documentos = new stdClass();
            $msf->documentos->guia_recepcion = $this->getGuiaRecepcion($v->dc_guia_recepcion);
            $msf->documentos->guia_recepcion->df_recepcion = $db->dateLocalFormat($msf->documentos->guia_recepcion->df_recepcion);
            
            $msf->documentos->guia_despacho = $this->getGuiaDespacho($v->dc_guia_despacho);
            $msf->documentos->guia_despacho->df_emision = $db->dateLocalFormat($msf->documentos->guia_despacho->df_emision);
            
            $msf->documentos->guia_despacho_proveedor = $this->getGuiaDespachoProveedor($v->dc_guia_despacho_proveedor);
            $msf->documentos->guia_despacho_proveedor->df_emision = $db->dateLocalFormat($msf->documentos->guia_despacho_proveedor->df_emision);
            
            $msf->documentos->series = $v->dg_series . $v->dc_movimiento;
            
            $msf->documentos->orden_compra = $this->getOrdenCompra($v->dc_orden_compra);
            $msf->documentos->orden_compra->df_fecha_emision = $db->dateLocalFormat($msf->documentos->orden_compra->df_fecha_emision);
            
            $msf->documentos->factura = $this->getFactura($v->dc_factura);
            $msf->documentos->factura->df_emision = $db->dateLocalFormat($msf->documentos->factura->df_emision);
            
            $msf->documentos->nota_credito = $this->getNotaCredito($v->dc_nota_credito);
            $msf->documentos->nota_credito->df_emision = $db->dateLocalFormat($msf->documentos->nota_credito->df_emision);
            
            $msf->documentos->nota_venta = $this->getNotaVenta($v->dc_nota_venta);
            $msf->documentos->nota_venta->df_fecha_emision = $db->dateLocalFormat($msf->documentos->nota_venta->df_fecha_emision);
            
            $msf->documentos->orden_servicio = $this->getOrdenServicio($v->dc_orden_servicio);
            $msf->documentos->orden_servicio->df_fecha_emision = $db->dateLocalFormat($msf->documentos->orden_servicio->df_fecha_emision);
            
            $msf->documentos->devolucion_guia_despacho = $this->getDevolucionGuiaDespacho($v->dc_devolucion_guia_despacho);
            $msf->documentos->devolucion_guia_despacho->df_emision = $db->dateLocalFormat($msf->documentos->devolucion_guia_despacho->df_emision);
            
            $msf->documentos->devolucion_guia_despacho_proveedor = $this->getDevolucionGuiaDespachoProveedor($v->dc_devolucion_guia_despacho_proveedor);
            $msf->documentos->devolucion_guia_despacho_proveedor->df_emision = $db->dateLocalFormat($msf->documentos->devolucion_guia_despacho_proveedor->df_emision);
            
            $msf->documentos->comprobante_ajuste_logistico = $this->getComprobanteAjusteLogistico($v->dc_comprobante_ajuste_logistico);
            $msf->documentos->comprobante_ajuste_logistico->df_emision = $db->dateLocalFormat($msf->documentos->comprobante_ajuste_logistico->df_emision);
            // $msf->cebe = $this->getCebe($v->dc_cebe);
            // $msf->ceco = $this->getCeco($v->dc_ceco);
            
            $rezagados[] = $msf;
        }
        
        
        return $rezagados;
    }
    
    private function getProducto($dc_producto){
        $empty = new stdClass();
        $empty->dc_producto = '';
        $empty->dg_producto = '';
        $empty->dg_codigo = '';
        if(empty($dc_producto)){
            return $empty;
        }
        $var = $this->getConnection()->getRowById('tb_producto', $dc_producto, 'dc_producto','dc_producto, dg_producto, dg_codigo');
        return $var === false ? $empty:$var; 
    }
    
    private function getCliente($dc_cliente){
        $empty = new stdClass();
        $empty->dc_cliente = '';
        $empty->dg_rut = '';
        $empty->dg_razon = '';
        if(empty($dc_cliente)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_cliente', $dc_cliente, 'dc_cliente', 'dc_cliente, dg_rut, dg_razon');
        return $var === false ? $empty:$var; 
    }
    
    private function getProveedor($dc_proveedor){
        $empty = new stdClass();
        $empty->dc_proveedor = '';
        $empty->dg_rut = '';
        $empty->dg_razon = '';
        if(empty($dc_proveedor)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_proveedor', $dc_proveedor, 'dc_proveedor', 'dc_proveedor, dg_rut, dg_razon');
        return $var === false ? $empty:$var; 
    }
    
    private function getTipoMovimiento($dc_tipo_movimiento){
        $empty = new stdClass();
        $empty->dc_tipo_movimiento = '';
        $empty->dc_tipo_operacion = '';
        $empty->dg_tipo_movimiento = '';
        if(empty($dc_tipo_movimiento)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_tipo_movimiento_logistico', $dc_tipo_movimiento, 
                    'dc_tipo_movimiento', 'dc_tipo_movimiento, dc_tipo_operacion, dg_tipo_movimiento');
        return $var === false ? $empty:$var; 
    }
    
    private function getBodega($dc_bodega){
        $empty = new stdClass();
        $empty->dc_bodega = '';
        $empty->dg_bodega = '';
        if(empty($dc_bodega)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_bodega', $dc_bodega, 'dc_bodega', 'dc_bodega, dg_bodega');
        return $var === false ? $empty:$var; 
    }
    
    private function getGuiaRecepcion($dc_guia_recepcion){
        $empty = new stdClass();
        $empty->dc_guia_recepcion = '';
        $empty->dq_guia_recepcion = '';
        $empty->df_recepcion = '';
        if(empty($dc_guia_recepcion)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_guia_recepcion', $dc_guia_recepcion, 
                    'dc_guia_recepcion', 'dc_guia_recepcion, dq_guia_recepcion, df_recepcion');
        return $var === false ? $empty:$var; 
    }
    
    private function getGuiaDespacho($dc_guia_despacho){
        $empty = new stdClass();
        $empty->dc_guia_despacho = '';
        $empty->dq_guia_despacho = '';
        $empty->dq_folio = '';
        $empty->df_emision = '';
        if(empty($dc_guia_despacho)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_guia_despacho', $dc_guia_despacho, 
                    'dc_guia_despacho', 'dc_guia_despacho, dq_guia_despacho, dq_folio, df_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getGuiaDespachoProveedor($dc_guia_despacho_proveedor){
        $empty = new stdClass();
        $empty->dc_guia_despacho = '';
        $empty->dq_guia_despacho = '';
        $empty->dq_folio = '';
        $empty->df_emision = '';
        if(empty($dc_guia_despacho_proveedor)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_guia_despacho', $dc_guia_despacho_proveedor, 
                    'dc_guia_despacho', 'dc_guia_despacho, dq_guia_despacho, dq_folio, df_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getOrdenCompra($dc_orden_compra){
        $empty = new stdClass();
        $empty->dc_orden_compra = '';
        $empty->dq_orden_compra = '';
        $empty->df_fecha_emision = '';
        if(empty($dc_orden_compra)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_orden_compra', $dc_orden_compra, 
                    'dc_orden_compra', 'dc_orden_compra, dq_orden_compra, df_fecha_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getFactura($dc_factura){
        $empty = new stdClass();
        $empty->dc_factura = '';
        $empty->dq_factura = '';
        $empty->dq_folio = '';
        $empty->df_emision = '';
        if(empty($dc_factura)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_factura_compra', $dc_factura, 
                    'dc_factura', 'dc_factura, dq_factura, dq_folio, df_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getNotaCredito($dc_nota_credito){
        $empty = new stdClass();
        $empty->dc_nota_credito = '';
        $empty->dq_nota_credito = '';
        $empty->dq_folio = '';
        $empty->df_emision = '';
        if(empty($dc_nota_credito)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_nota_credito', $dc_nota_credito, 
                    'dc_nota_credito', 'dc_nota_credito, dq_nota_credito, dq_folio, df_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getNotaVenta($dc_nota_venta){
        $empty = new stdClass();
        $empty->dc_nota_venta = '';
        $empty->dq_nota_venta = '';
        $empty->df_fecha_emision = '';
        if(empty($dc_nota_venta)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_nota_venta', $dc_nota_venta, 
                    'dc_nota_venta', 'dc_nota_venta, dq_nota_venta, df_fecha_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getOrdenServicio($dc_orden_servicio){
        $empty = new stdClass();
        $empty->dc_orden_servicio = '';
        $empty->dq_orden_servicio = '';
        $empty->df_fecha_emision = '';
        if(empty($dc_orden_servicio)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_orden_servicio', $dc_orden_servicio, 
                    'dc_orden_servicio', 'dc_orden_servicio, dq_orden_servicio, df_fecha_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getDevolucionGuiaDespacho($dc_devolucion_guia_despacho){
        $empty = new stdClass();
        $empty->dc_devolucion = '';
        $empty->dq_devolucion = '';
        $empty->df_emision = '';
        if(empty($dc_devolucion_guia_despacho)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_devolucion', $dc_devolucion_guia_despacho, 
                    'dc_devolucion', 'dc_devolucion, dq_devolucion, df_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getDevolucionGuiaDespachoProveedor($dc_devolucion_guia_despacho_proveedor){
        $empty = new stdClass();
        $empty->dc_devolucion = '';
        $empty->dq_devolucion = '';
        $empty->df_emision = '';
        if(empty($dc_devolucion_guia_despacho_proveedor)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_devolucion_guia_despacho_proveedor', $dc_devolucion_guia_despacho_proveedor, 
                    'dc_devolucion', 'dc_devolucion, dq_devolucion, df_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getComprobanteAjusteLogistico($dc_comprobante_ajuste_logistico){
        $empty = new stdClass();
        $empty->dc_comprobante = '';
        $empty->dq_comprobante = '';
        $empty->dq_cantidad = '';
        $empty->df_emision = '';
        if(empty($dc_comprobante_ajuste_logistico)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_comprobante_ajuste_logistico', $dc_comprobante_ajuste_logistico, 
                    'dc_comprobante', 'dc_comprobante, dq_comprobante, dq_cantidad, df_emision');
        return $var === false ? $empty:$var; 
    }
    
    private function getCebe($dc_cebe){
        $empty = new stdClass();
        $empty->dc_cebe = '';
        $empty->dg_cebe = '';
        if(empty($dc_cebe)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_cebe', $dc_cebe, 'dc_cebe', 'dc_cebe, dg_cebe');
        return $var === false ? $empty:$var; 
    }
    
    private function getCeco($dc_ceco){
        $empty = new stdClass();
        $empty->dc_ceco = '';
        $empty->dg_ceco = '';
        if(empty($dc_ceco)){
            return $empty;
        }
        $var =  $this->getConnection()->getRowById('tb_ceco', $dc_ceco, 
                    'dc_ceco', 'dc_ceco, dg_ceco');
        return $var === false ? $empty:$var; 
    }
    
    /*
     * Asignación manual de fechas para los movimientos
     * 
     */
    
    public function showAsignarFechaAction(){
        $r = $this->getRequest();
        $params  = array(
                'dc_movimiento' => $r->dc_movimiento
            );
        $form = $this->getFormView($this->getTemplateUrl('asigna_fecha'), $params);
        echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
    }
     
    public function asignaFechaManualAction(){
        $fecha = $this->validateFecha();
        $movimiento = $this->getMovimiento();
        if(!$fecha || !$movimiento){
            $this->getErrorMan()->showWarning('No se ha podido registrar la fecha por que el formato de esta no es correcto.');
        } else {
            $result = $this->asignaFechaMovimiento($fecha, $movimiento);
            if($result){
                $this->getErrorMan()->showConfirm('Se ha registrado la fecha');
                echo '<script type="text/javascript">js_data.recargar_vista();</script>"';
            } else {
                $this->getErrorMan()->showWarning('No se ha podido registrar la fecha debido a que existe un error con el tipo de movimiento.');
            }
        }
    }
    
    private function validateFecha(){
        $r = Factory::getRequest();
        $this->initFunctionsService();
        $fecha = Functions::validateDate($r->df_movimiento, 'd/m/Y');
        if($fecha){
            return $r->df_movimiento;
        }
        return $fecha;
    }
    
    private function getMovimiento(){
        $r = Factory::getRequest();
        if(!isset($r->dc_movimiento)){
            return false;
        }
        return $this->getConnection()->getRowById('tb_movimiento_bodega', $r->dc_movimiento, 'dc_movimiento');
    }
    
    private function asignaFechaMovimiento($fecha, $movimiento){
        if(empty($movimiento->dc_tipo_movimiento)){
            return false;
        }
        $db = $this->getConnection();
        $db->start_transaction();
        $fields = array(
                'df_movimiento' => ':fecha'
            );
        $st = $db->prepare($db->update('tb_movimiento_bodega', $fields, 'dc_movimiento = :movimiento'));
        $st->bindValue(':fecha', $db->sqlDate2($fecha), PDO::PARAM_STR);
        $st->bindValue(':movimiento', $movimiento->dc_movimiento, PDO::PARAM_INT);
        $db->stExec($st);
        $db->commit();
        return true;
    }
    
}