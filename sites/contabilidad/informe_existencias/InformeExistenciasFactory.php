<?php
class InformeExistenciasFactory extends Factory{

  protected $title = "Informe de Existencias";
  private $mes, $anho, $bodega;

  public function indexAction(){

    $form = $this->getFormView($this->getTemplateURL("index.form"),array(
      "meses" => $this->getMesesSelect()
    ));
    echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
  }

  public function showInformeAction(){
    $this->validarRequest();
    $this->initParameters();

    $cierre_periodo_anterior = $this->getCierreAnterior();
    $movimientos = $this->getMovimientos();

    echo $this->getView($this->getTemplateURL('tablaInforme'),array(
      'productos' => $this->getMovimientosAgrupadosPorProducto($movimientos, $cierre_periodo_anterior),
      'permiteCierre' => $this->getPermiteCierre()
    ));

  }

  private function getMovimientosAgrupadosPorProducto($movimientos, $cierre_periodo_anterior){
    $cierre_pa = $cierre_periodo_anterior[0];
    $data = $cierre_periodo_anterior[1];

    foreach($movimientos as $mov):

      //Si no hay registros del producto en el mes anterior
      if(!isset($data[$mov->dc_producto])){
        $data[$mov->dc_producto] = (object)array(
          "dg_producto" => $mov->dg_producto,
          "dg_codigo" => $mov->dg_codigo,
          "dg_marca" => $mov->dg_marca,
          "dc_cantidad" => 0,
          "dq_valor" => 0.0,
          "dq_pmp" => 0.0
        );
      }

      if($data[$mov->dc_producto]->dq_pmp == 0 or ($data[$mov->dc_producto]->dc_cantidad + $mov->dq_cantidad) == 0):
        $data[$mov->dc_producto]->dq_pmp = $mov->dq_monto;
      else:
        $data[$mov->dc_producto]->dq_pmp =
            (
              $data[$mov->dc_producto]->dc_cantidad * $data[$mov->dc_producto]->dq_pmp
              +
              $mov->dq_cantidad * $mov->dq_monto
            )/($data[$mov->dc_producto]->dc_cantidad + $mov->dq_cantidad);
      endif;

      //Recalcular cantidad
      $data[$mov->dc_producto]->dc_cantidad += intval($mov->dq_cantidad);

      //Recalcular valor en bodega
      $data[$mov->dc_producto]->dq_valor += floatval($mov->dq_monto) * (intval($mov->dq_cantidad)/(abs(intval($mov->dq_cantidad))));

    endforeach;

    return $data;

  }

  private function getMovimientos(){
    $db = $this->getConnection();

    $data = $db->prepare(
              $db->select('tb_movimiento_bodega m
                JOIN tb_producto p ON p.dc_producto = m.dc_producto
                LEFT JOIN tb_marca k ON k.dc_marca = p.dc_marca',
              'm.*, p.dg_producto, p.dg_codigo, k.dg_marca',
              'm.dc_empresa = ? AND MONTH(m.df_movimiento) = ? AND YEAR(m.df_movimiento) = ?'
            ));
    $data->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $data->bindValue(2,$this->mes,PDO::PARAM_INT);
    $data->bindValue(3,$this->anho,PDO::PARAM_INT);

    $db->stExec($data);

    return $data->fetchAll(PDO::FETCH_OBJ);

  }

  private function getCierreAnterior(){
    $periodo = $this->getPeriodoAnterior();
    $db = $this->getConnection();

    $query = $db->prepare(
      $db->select('tb_comprobante_cierre_existencias',
      'dc_comprobante, dq_comprobante',
      'dc_mes = ? AND dc_anho = ? AND dc_empresa = ? AND dm_nula = 0'));
    $query->bindValue(1, $periodo->mes, PDO::PARAM_INT);
    $query->bindValue(2, $periodo->anho, PDO::PARAM_INT);
    $query->bindValue(3, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($query);

    $cierre = $query->fetch(PDO::FETCH_OBJ);

    if($cierre === false):
      $this->getErrorMan()->showWarning(
          "<strong>Error: Proceso de cierre de existencias no ha sido completado</strong><br />".
          "Durante el periodo anterior no fue realizado el cierre de existencias, debido a esto no es posible sacar un balance del último estado de las bodegas.<br />".
          "Realice el cierre de existencias de los periodos anteriores antes de consultar este.");

      $ultimo_cierre = $db->doQuery($db->select('tb_comprobante_cierre_existencias','dc_mes, dc_anho','',array('limit'=>1,'order_by'=>'df_emision DESC')))->fetch(PDO::FETCH_OBJ);

      if($ultimo_cierre != false):
        $this->getErrorMan()->showInfo("El último cierre realizado fue el correspondiente al periodo: <b>{$ultimo_cierre->dc_mes}</b>/<b>{$ultimo_cierre->dc_anho}</b>");
      endif;

      exit;

    endif;

    return array($cierre, $this->getProductosCierreAnterior($cierre));

  }

  private function getProductosCierreAnterior($cierre){
    $db = $this->getConnection();

    $query = $db->prepare(
             $db->select('tb_detalle_cierre_existencias d
             JOIN tb_producto p ON p.dc_producto = d.dc_producto
             LEFT JOIN tb_marca m ON m.dc_marca = p.dc_marca',
             'p.dc_producto, p.dg_codigo, p.dg_producto, m.dg_marca, d.dq_valor, d.dq_pmp, d.dc_cantidad',
             'dc_comprobante = ?'));
    $query->bindValue(1, $cierre->dc_comprobante, PDO::PARAM_INT);
    $db->stExec($query);

    $agrupados = array();

    foreach ($query->fetchAll(PDO::FETCH_OBJ) as $d) {
      $agrupados[$d->dc_producto] = (object) array(
        "dg_producto" => $d->dg_producto,
        "dg_codigo" => $d->dg_codigo,
        "dg_marca" => $d->dg_marca,
        "dc_cantidad" => intval($d->dc_cantidad),
        "dq_valor" => floatval($d->dq_valor),
        "dq_pmp" => floatval($d->dq_pmp)
      );
    }

    return $agrupados;

  }

  private function getPeriodoAnterior(){
    $mes = $this->mes - 1;
    if($mes == 0):
      $mes = 12;
      $anho = $this->anho - 1;
    else:
      $anho = $this->anho;
    endif;

    $periodo = new stdClass;
    $periodo->mes = $mes;
    $periodo->anho = $anho;

    return $periodo;

  }

  private function initParameters(){
    $r = self::getRequest();

    list($this->mes, $this->anho, $this->bodega) = array(intval($r->dc_mes), intval($r->dc_anho), intval($r->dc_bodega));
  }

  private function validarRequest(){
    $r = self::getRequest();

    //Validar Año
    if(preg_match("/^(10|20)\d\d$/",$r->dc_anho) == 0){
      $this->getErrorMan()->showWarning("El año ingresado es incorrecto y no cumple con el formato esperado, compruebe el dato e inténtelo de nuevo.");
      exit;
    }

    //Validar Mes
    if(preg_match("/^[0|1]?\d$/",$r->dc_mes) == 0 || !intval($r->dc_mes)){
      $this->getErrorMan()->showWarning("El mes ingresado es incorrecto y no cumple con el formato esperado, compruebe el dato e inténtelo de nuevo.");
      exit;
    }

    //Validar Bodega
    if(preg_match("/^\d+$/",$r->dc_bodega) == 0 || !intval($r->dc_bodega)){
      $this->getErrorMan()->showWarning("La bodega ingresada es incorrecta y no cumple con el formato esperado, comrpuebe el dato e intentelo de nuevo.");
      exit;
    }

    //Validar Periodo Posterior
    if(!$this->validaPeriodoPosterior(intval($r->dc_mes), intval($r->dc_anho))){
      $this->getErrorMan()->showWarning("No puede consultar el informe de existencias de un periodo posterior al actual, favor verifique la información e intentelo nuevamente.");
      exit;
    }

  }

  private function getMesesSelect(){
    return array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
  }

  private function getPermiteCierre(){
    $mes = intval(date('n'));
    $anho = intval(date('Y'));

    if($anho == $this->anho):
      if($mes > $this->mes):
        return true;
      endif;
    else:
      return true;
    endif;

    return false;

  }

  private function validaPeriodoPosterior($mes, $anho){
    $mactual = intval(date('n'));
    $yactual = intval(date('Y'));

    if($anho > $yactual):
      return false;
    endif;

    if($anho == $yactual and $mes > $mactual){
      return false;
    }

    return true;

  }

}
