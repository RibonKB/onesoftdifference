<?php
require_once('../../../inc/db-class.php');
require_once('../stuff.class.php');

require_once('src_centralizacion_compra_factura.php');
require_once('src_centralizacion_compra_nota_credito.php');

function buscarResultados($param = array(),DBConnector $db){

	global $empresa;

	$df_corte = $db->sqlDate($param['df_emision_hasta'].' 23:59');
	$dc_anho = intval($_POST['dc_anho_corte']);
	$dc_mes = intval($_POST['dc_mes_corte']);
	$cond_fc = "fc.dc_empresa = {$empresa} AND YEAR(fc.df_libro_compra) = {$dc_anho} AND MONTH(fc.df_libro_compra) = {$dc_mes} AND fc.dm_nula = 0 AND fc.dm_centralizada = 0";
	$cond_nc = "nc.dc_empresa = {$empresa} AND YEAR(nc.df_libro_compra) = {$dc_anho} AND MONTH(nc.df_libro_compra) = {$dc_mes} AND nc.dm_nula = 0 AND nc.dm_centralizada = 0";

	if(isset($param['dc_proveedor'])){
		$proveedores = implode(',',$param['dc_proveedor']);
		$cond_fc .= " AND fc.dc_proveedor IN ({$proveedores})";
		$cond_nc .= " AND nc.dc_proveedor IN ({$proveedores})";
	}

	$tables_fc = "tb_factura_compra fc
		LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fc.dc_nota_venta
		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fc.dc_orden_servicio
		LEFT JOIN tb_tipo_factura_compra t_fc ON t_fc.dc_tipo = fc.dc_tipo
		LEFT JOIN tb_usuario us ON us.dc_usuario = fc.dc_usuario_creacion
		JOIN tb_proveedor pr ON pr.dc_proveedor = fc.dc_proveedor
		JOIN tb_tipo_proveedor t ON t.dc_tipo_proveedor = pr.dc_tipo_proveedor AND t.dm_incluido_libro_compra = 1";

	$fields_fc = '"FC" tp, GROUP_CONCAT(nv.dq_nota_venta SEPARATOR "<br />") dq_nota_venta,GROUP_CONCAT(os.dq_orden_servicio SEPARATOR "<br />") dq_orden_servicio,
	fc.df_emision, pr.dg_rut, pr.dg_razon,fc.dq_exento,SUM(fc.dq_neto) dq_neto,SUM(fc.dq_iva) dq_iva,SUM(fc.dq_total) dq_total,
	GROUP_CONCAT(fc.dq_factura SEPARATOR "<br />") dq_factura,fc.dq_folio,fc.dm_nula,t_fc.dg_tipo,us.dg_usuario,GROUP_CONCAT(fc.dc_factura) dc_documento';

	$tables_nc = "tb_nota_credito_proveedor nc
		JOIN tb_tipo_nota_credito_proveedor t_nc ON t_nc.dc_tipo = nc.dc_tipo
		JOIN tb_proveedor pr ON pr.dc_proveedor = nc.dc_proveedor
		JOIN tb_tipo_proveedor t ON t.dc_tipo_proveedor = pr.dc_tipo_proveedor AND t.dm_incluido_libro_compra = 1
		LEFT JOIN tb_usuario us ON us.dc_usuario = nc.dc_usuario_creacion";

	$fields_nc = '"NC", "", "", nc.df_emision, pr.dg_rut, pr.dg_razon, 0, -SUM(nc.dq_neto), -SUM(nc.dq_iva), -SUM(nc.dq_total),
	GROUP_CONCAT(nc.dq_nota_credito SEPARATOR "<br />"), nc.dq_folio, nc.dm_nula, t_nc.dg_tipo, us.dg_usuario,GROUP_CONCAT(nc.dc_nota_credito)';

	return $db->doQuery(
		"(".$db->select($tables_fc,$fields_fc,$cond_fc,array('group_by' => 'fc.dc_proveedor, fc.dq_folio')).")".
		"UNION ALL".
		"(".$db->select($tables_nc,$fields_nc,$cond_nc,array('group_by' => 'nc.dc_proveedor, nc.dq_folio')).")".
		" ORDER BY df_emision"
	)->fetchAll(PDO::FETCH_OBJ);

}

//Funciones de inserción
function insertarComprobanteContable($data,$df_contable, DBConnector $db){
	global $empresa, $idUsuario;

	$dq_comprobante = ContabilidadStuff::getCorrelativoComprobante();
	$conf = ContabilidadStuff::getConfiguration();
	$date_comp = explode('/',$df_contable);

	$dq_saldo = 0;
	foreach($data[0] as $dc_factura => $data_fac):
		$factura = $data_fac[0];
		$dq_saldo += floatval($factura->dq_total);
	endforeach;

	foreach($data[1] as $dc_nota_credito => $data_nc):
		$nota_credito = $data_nc[0];
		$dq_saldo += floatval($nota_credito->dq_total);
	endforeach;

	$comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => '?',
		'dc_tipo_movimiento' => '?',
		'df_fecha_contable' => '?',
		'dc_mes_contable' => '?',
		'dc_anho_contable' => '?',
		'dq_cambio' => '1',
		'dg_glosa' => '?',
		'dq_saldo' => '?',
		'df_fecha_emision' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario,
		'dm_tipo_comprobante' => 3
	)));
	$comprobante->bindValue(1,$dq_comprobante,PDO::PARAM_INT);
	$comprobante->bindValue(2,$conf->dc_tipo_movimiento_centralizacion_compra,PDO::PARAM_INT);
	$comprobante->bindValue(3,$db->sqlDate2($df_contable),PDO::PARAM_STR);
	$comprobante->bindValue(4,$date_comp[1],PDO::PARAM_INT);
	$comprobante->bindValue(5,$date_comp[2],PDO::PARAM_INT);
	$comprobante->bindValue(6,"Centralización Libro de Compra: ".date('d-m-Y'),PDO::PARAM_STR);
	$comprobante->bindValue(7,$dq_saldo,PDO::PARAM_STR);

	$db->stExec($comprobante);

	return $db->lastInsertId();

}

?>
