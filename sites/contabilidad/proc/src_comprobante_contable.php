<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$cond_month = '';
if($_POST['comp_mes'] != '' && is_numeric($_POST['comp_mes'])){
	$cond_month = "AND dc_mes_contable = {$_POST['comp_mes']}";
}

$nulls = '';
if(!isset($_POST['comp_nulls'])){
	$nulls = "AND dm_anulado='0'";
}

$by_saldo = '';
if($_POST['comp_saldo'] != '' && is_numeric(str_replace(array(',','.'),'',$_POST['comp_saldo']))){
	$_POST['comp_saldo'] = str_replace(array(',','.'),'',$_POST['comp_saldo']);
	$by_saldo = "AND dq_saldo = {$_POST['comp_saldo']}";
}

$by_propuesta = '';
if($_POST['dq_propuesta'] != ''){
	$dq_propuesta = intval($_POST['dq_propuesta']);
	$by_propuesta = "JOIN tb_propuesta_pago prop ON prop.dc_propuesta = c.dc_propuesta_pago AND prop.dq_propuesta = {$dq_propuesta}";
}

$resp = '';
if($_POST['comp_resp']){
	$aux = $db->select("(SELECT * FROM tb_funcionario WHERE dc_empresa = {$empresa} AND dg_rut = '{$_POST['comp_resp']}') f
	JOIN (SELECT * FROM tb_usuario WHERE dc_empresa={$empresa}) u ON f.dc_funcionario = u.dc_funcionario",'dc_usuario');
	if(count($aux)){
		$resp = "AND dc_usuario_creacion = {$aux[0]['dc_usuario']}";
	}else{
		$error_man->showAviso("El responsable indicado no fue encontrado, se ignora en los resultados");
	}
	unset($aux);
}

if(isset($_POST['comp_cuentas']) && !$_POST['comp_numero']){

	$cuentas = implode(',',$_POST['comp_cuentas']);
	$res = $db->select(
	"(SELECT * FROM tb_comprobante_contable WHERE dc_empresa = {$empresa} AND dc_anho_contable = {$_POST['comp_anho']} {$cond_month} {$nulls} {$resp} {$by_saldo}) AS c
	JOIN (SELECT * FROM tb_comprobante_contable_detalle WHERE dc_cuenta_contable IN({$cuentas})  AND dm_activo=0) AS d ON c.dc_comprobante = d.dc_comprobante
	LEFT JOIN tb_usuario AS u ON c.dc_usuario_creacion = u.dc_usuario
	LEFT JOIN tb_funcionario AS f ON u.dc_funcionario = f.dc_funcionario
	{$by_propuesta}",
	"c.dc_comprobante, c.dg_comprobante, c.dg_numero_interno, c.dc_mes_contable, c.dc_anho_contable, dm_anulado,
	DATE_FORMAT(c.df_fecha_emision,'%d/%m/%Y') AS df_emision,
	CONCAT(f.dg_nombres,' ',f.dg_ap_paterno) as dg_responsable, u.dg_usuario",'',array('order_by'=>'df_fecha_contable DESC'));
	
}else if(isset($_POST['comp_cuentas']) && $_POST['comp_numero']){
	
	$db->escape($_POST['comp_numero']);

	$cuentas = implode(',',$_POST['comp_cuentas']);
	$res = $db->select(
	"(SELECT * FROM tb_comprobante_contable WHERE  dc_empresa = {$empresa} AND dc_anho_contable = {$_POST['comp_anho']} {$cond_month} {$nulls} {$resp} {$by_saldo} AND (dg_comprobante LIKE '%{$_POST['comp_numero']}%' OR dg_numero_interno LIKE '%{$_POST['comp_numero']}%')) AS c
	JOIN (SELECT * FROM tb_comprobante_contable_detalle WHERE  dc_cuenta_contable IN({$cuentas}) AND dm_activo=0) AS d ON c.dc_comprobante = d.dc_comprobante
	LEFT JOIN tb_usuario AS u ON c.dc_usuario_creacion = u.dc_usuario
	LEFT JOIN tb_funcionario AS f ON u.dc_funcionario = f.dc_funcionario
	{$by_propuesta}",
	"distinct c.dc_comprobante, c.dg_comprobante, dg_numero_interno, c.dc_mes_contable, c.dc_anho_contable, dm_anulado,
	DATE_FORMAT(c.df_fecha_emision,'%d/%m/%Y') AS df_emision,
	CONCAT(f.dg_nombres,' ',f.dg_ap_paterno) as dg_responsable, u.dg_usuario",'',array('order_by'=>'df_fecha_contable DESC'));
	
}else{
	
	$db->escape($_POST['comp_numero']);
	
	$res = $db->select(
	"(SELECT * FROM tb_comprobante_contable WHERE dc_empresa = {$empresa} AND dc_anho_contable = {$_POST['comp_anho']} {$cond_month} {$nulls} {$resp} {$by_saldo} AND (dg_comprobante LIKE '%{$_POST['comp_numero']}%' OR dg_numero_interno LIKE '%{$_POST['comp_numero']}%')) AS c
	LEFT JOIN tb_usuario AS u ON c.dc_usuario_creacion = u.dc_usuario
	LEFT JOIN tb_funcionario AS f ON u.dc_funcionario = f.dc_funcionario
	{$by_propuesta}",
	"c.dc_comprobante, c.dg_comprobante, dg_numero_interno, c.dc_mes_contable, c.dc_anho_contable, dm_anulado,
	DATE_FORMAT(c.df_fecha_emision,'%d/%m/%Y') AS df_emision,
	CONCAT(f.dg_nombres,' ',f.dg_ap_paterno) as dg_responsable, u.dg_usuario",'',array('order_by'=>'df_fecha_contable DESC'));
}

if(!count($res)){
	$error_man->showAviso("No se encontraron comprobantes contables con los criterios especificados");
	exit();
}

echo("<div id='show_comprobante'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Comprobantes<br />Encontrados</caption>
<thead>
	<tr>
		<th>Nº Comprobante</th>
	</tr>
</thead>
<tbody>
");

foreach($res as $c){

if($c['dg_numero_interno'])
	$interno = " - <b>{$c['dg_numero_interno']}</b>";
else
	$interno = '';
	
if($c['dm_anulado'] == 1)
	$nula = '<img src="images/delbtn.png" title="Comprobante anulado" class="right" />';
else
	$nula = '';

echo("<tr>
	<td align='left'>
		{$nula}
		<a href='sites/contabilidad/proc/show_comprobante_contable.php?id={$c['dc_comprobante']}' class='comp_load'>
		<img src='images/doc.png' alt='' style='vertical-align:middle;' />{$c['dg_comprobante']}{$interno}</a>
	</td>
</tr>");

}

echo("
</tbody>
</table>
</div>");

echo("
<button type='button' class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>");
if(check_permiso(4))
echo("<button type='button' class='delbtn' id='null_comprobante'>Anular</button>");
if(check_permiso(2))
echo("<button type='button' class='editbtn' id='edit_comprobante'>Modificar</button>");
if(check_permiso(3))
echo("<button type='button' class='checkbtn' id='auth_comprobante' title='Autorizar modificación'>Autorizar</button>");

echo("</div>");

?>
<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".comp_load").click(function(e){
		e.preventDefault();
		$('#show_comprobante').html("<img src='images/ajax-loader.gif' alt='' /> cargando comprobante ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px','marginTop':25});
		loadFile($(this).attr('href'),'#show_comprobante');
	}).first().trigger('click');
	
	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>