<?php
require_once("../../../inc/form-class.php");
require_once('../stuff.class.php');

$form = new Form($empresa);

if(count($factura))
	$asientosFactura = obtenerAsientos($factura, $db);
else
	$asientosFactura = array(array(),array());
	
if(count($nota_credito))
	$asientosNotaCredito = obtenerAsientosNC($nota_credito, $db);
else
	$asientosNotaCredito = array(array(),array());
	
$financiero = array_merge($asientosFactura[0],$asientosNotaCredito[0]);
$analitico = array_merge($asientosFactura[1],$asientosNotaCredito[1]);
$analitico = ContabilidadStuff::delEmptyCols($analitico);
$header_analitico = ContabilidadStuff::getHeaderAnalitico($analitico);

$array_widths = array();
foreach($header_analitico as $w){
	$array_widths[] = $w[0];
}

$totales = array();
$total_debe = 0;
$total_haber = 0;

?>
<div class="secc_bar">
	Centralización libros de compra/venta
</div>
<div class="panes" style="width:910px; margin:10px;">

<?php
	$form->Start('sites/contabilidad/proc/crear_centralizacion.php','cr_centralizacion_form','centralizar_form');
		$form->Header('<b>Indique los datos requeridos para la centralización y revise los comprobantes preliminares a continuación</b><br />
					   Los campos marcados con [*] son obligatorios');
		$form->Date('Fecha contable centralización','df_fecha_contable',true,0);
	$form->End('Confirmar Centralización','addbtn')
?>

<ul id="tabs">
	<li><a href="#">Detalle Financiero</a></li>
    <li><a href="#">Detalle Analítico</a></li>
</ul>
<br class="clear" />
<div class="tabpanes">
<div>
    <div class="info">
        Detalles comprobante financiero
    </div>
    <table width="100%" class="tab bicolor_tab" id="tabla_financiero">
        <thead>
            <tr>
                <th>Código</th>
                <th>Cuenta Contable</th>
                <th>DEBE</th>
                <th>HABER</th>
                <th>Glosa</th>
            </tr>
        </thead>
        <tbody>
            <?php
            	foreach($financiero as $d):
					$total_debe += $d['dq_debe'];
					$total_haber += $d['dq_haber'];
			?>
            <tr>
                <td><?php echo $d['dg_codigo_cuenta'] ?></td>
                <td><?php echo $d['dg_cuenta_contable'] ?></td>
                <td align="right"><?php echo moneda_local($d['dq_debe']) ?></td>
                <td align="right"><?php echo moneda_local($d['dq_haber']) ?></td>
                <td><?php echo $d['dg_glosa'] ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
        <tfoot>
        	<tr>
            	<th>&nbsp;</th>
                <th>&nbsp;</th>
                <th align="right"><?php echo moneda_local($total_debe) ?></th>
                <th align="right"><?php echo moneda_local($total_haber) ?></th>
                <th>&nbsp;</th>
            </tr>
        </tfoot>
    </table>
</div>
<div>
    <div class="info">
        Detalles comprobante analítico
    </div>
	<table class="tab bicolor_tab" id="tabla_analitico">
    	<thead>
        	<tr>
            	<?php foreach($header_analitico as $h): ?>
                <th><?php echo $h[1] ?></th>
                <?php endforeach ?>
            </tr>
        </thead>
        <tbody>
        	<?php foreach($analitico as $d): ?>
            <tr>
            	<?php foreach($d as $col => $dato): ?>
                	<?php
                    	if(in_array($col,array('dq_debe','dq_haber'))):
							if(!isset($totales[$col]))
								$totales[$col] = 0;
							$totales[$col] += $dato;
					?>
                		<td align="right"><?php echo moneda_local($dato) ?></td>
                    <?php else: ?>
                    	<td><?php echo $dato ?></td>
                    <?php endif ?>
                <?php endforeach ?>
            </tr>
            <?php endforeach ?>
        </tbody>
        <tfoot>
        	<?php foreach($header_analitico as $col => $dato): ?>
            	<?php if(isset($totales[$col])): ?>
                	<th align="right"><?php echo moneda_local($totales[$col]) ?></th>
                <?php else: ?>
                	<th>&nbsp;</th>
                <?php endif ?>
            <?php endforeach ?>
        </tfoot>
    </table>
</div>
</div>
<script type="text/javascript">
window.setTimeout(function(){
	$('#tabla_analitico')
		.tableExport()
		.tableOverflow(<?php echo json_encode($array_widths) ?>)
		.tableAdjust(10,undefined,900);
		
	$('#tabla_financiero')
		.tableExport()
		.tableAdjust(10);
},100);
$('#cr_centralizacion_form').submit(function(e){
	e.preventDefault();
	var action = $(this).attr('action');
	var data = $(':input',this).serialize();
	pymerp.loadOverlay(action+'?tipo_centralizacion='+js_data.tipo_centralizacion+'&'+data,js_data.activeData.serialize());
});
</script>