<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//se desactiva el registro en la base de datos
$db->update("tb_comprobante_contable",array("dm_editable" => "1"),"dc_comprobante = {$_POST['null_comp_id']}");

$error_man->showConfirm('Se le han asignado permisos de edición al comprobante contable');

?>