<?php
require_once('../../../inc/db-class.php');

//Funciones de obtención  de datos
function obtenerDataFactura($param, DBConnector $db){
	global $empresa, $error_man;
	
	$fc = array();
	if(isset($param['dc_documento']['FV'])){
		$dc_factura = explode(',',implode(',',$param['dc_documento']['FV']));
		$questions = substr(str_repeat(',?',count($dc_factura)),1);
		
		//Obtener los detalles de cabecera para las facturas
		$facturas = $db->prepare(
						$db->select('tb_factura_venta fv
									 JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente
									 JOIN tb_tipo_operacion op ON op.dc_tipo_operacion = fv.dc_tipo_operacion',
									'fv.dc_factura, fv.dq_folio, fv.dq_factura, fv.dc_cliente, fv.dq_total, fv.dq_iva, cl.dc_cuenta_contable dc_cuenta_cliente, fv.dc_ejecutivo,
									 cl.dc_tipo_cliente, cl.dc_mercado dc_mercado_cliente, fv.dc_nota_venta, fv.dc_orden_servicio, op.dc_cuenta_contable_centralizacion',
									"fv.dc_empresa = {$empresa} AND fv.dm_centralizada = 0 AND fv.dc_factura IN ({$questions})"));
		foreach($dc_factura as $i => $f){
			$facturas->bindValue($i+1,$f,PDO::PARAM_INT);
		}
		$db->stExec($facturas);
		
		//Agrupar en el array de facturas de compra las facturas encontradas
		$dc_factura_get = array();
		while($f = $facturas->fetch(PDO::FETCH_OBJ)):
			$fc[$f->dc_factura] = array($f, array());
			$dc_factura_get[] = $f->dc_factura;
		endwhile;
		unset($facturas);
		
		//Comprobar que la cantidad de facturas enviadas desde el formulario sea la misma obtenida desde la base de datos
		$diferencias = valida_cantidad_erronea_facturas($dc_factura,$dc_factura_get);
		if(count($diferencias)){
			return array('ERROR_ITEM_NOT_FOUND',$diferencias);
		}
		
		//obtener los detalles de las facturas para asignarlos y agruparlos.
		$detalles = $db->prepare($db->select("tb_factura_venta_detalle d
									JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND dc_empresa = {$empresa}
									JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio
									LEFT JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
									LEFT JOIN tb_proveedor prov ON prov.dc_proveedor = nvd.dc_proveedor
									LEFT JOIN tb_orden_servicio_factura_detalle osd ON osd.dc_detalle = d.dc_detalle_orden_servicio
									LEFT JOIN tb_ceco ceco ON ceco.dc_ceco = nvd.dc_ceco",
								'p.dg_codigo, d.dc_detalle, d.dc_factura, p.dc_producto, d.dc_cantidad, d.dq_precio, nvd.dc_ceco, nvd.dc_cebe, ceco.dc_segmento,
								 p.dc_marca, p.dc_tipo_producto, p.dc_linea_negocio, nvd.dc_nota_venta, osd.dc_orden_servicio,
								 prov.dc_proveedor, prov.dc_tipo_proveedor, prov.dc_mercado dc_mercado_proveedor, ln.dc_cuenta_contable_venta',
								"d.dc_factura IN ({$questions})"));
		foreach($dc_factura as $i => $f){
			$detalles->bindValue($i+1,$f,PDO::PARAM_INT);
		}
		$db->stExec($detalles);
		
		//Agrupar los detalles de la factura en el array de agrupaciones
		while($d = $detalles->fetch(PDO::FETCH_OBJ)):
			$d->dc_cliente = $fc[$d->dc_factura][0]->dc_cliente;
			$d->dc_tipo_cliente = $fc[$d->dc_factura][0]->dc_tipo_cliente;
			$d->dc_mercado_cliente = $fc[$d->dc_factura][0]->dc_mercado_cliente;
			$d->dc_cuenta_contable = $d->dc_cuenta_contable_venta;
			$d->dc_ejecutivo = $fc[$d->dc_factura][0]->dc_ejecutivo;
			
			$fc[$d->dc_factura][1][$d->dc_detalle] = $d;
		endwhile;
		
		return array('OK',$fc);
		
	}else{
		return array('OK',array());
	}
}

//Funciones de validación de datos
function validarFacturaCreacion($fv, DBConnector $db){
	global $empresa;
	
	//Validar que los valores de los detalles cuadren con los totales de la cabecera
	foreach($fv as $dc_factura => $data){
		$dq_total = floatval($data[0]->dq_total);
		$dq_iva = floatval($data[0]->dq_iva);
		$dq_total_productos = sumaDetalle($data[1]);
		
		if($dq_total != ($dq_iva+$dq_total_productos)){
			return array('ERROR_NO_CUADRA',array($data[0],$dq_total_productos, $dq_total_servicios));
		}
		
	}
	
	//Validar cuenta contable disponible para todas los tipos de movimiento de las facturas
	foreach($fv as $dc_factura => $data){
		if($data[0]->dc_cuenta_contable_centralizacion == 0){
			return array('ERROR_TIPO_OPERACION_CUENTA_CONTABLE');
		}
	}
	
	$conf = ContabilidadStuff::getConfiguration();
	
	if($conf->dc_cuenta_contable_iva_debito == 0){
		return array('ERROR_IVA_CUENTA_CONTABLE',array());
	}
	
	return true;
	
}

function sumaDetalle($detalle){
	$suma = 0;
	foreach($detalle as $dc_detalle => $data){
		$suma += floatval($data->dc_cantidad * $data->dq_precio);
	}
	return floatval($suma);
}

function valida_cantidad_erronea_facturas($dc_factura, $facturas){
	return array_diff($dc_factura,$facturas);
}

function agruparDetalles($data){
	$top = array();
	$cliente = array();
	$iva = array('sum' => 0, 'detalle' => array());
	
	foreach($data as $dc_factura => $factura){
      
		//agrupar los proveedores
		if(!isset($cliente[$factura[0]->dc_cuenta_cliente])){
			$cliente[$factura[0]->dc_cuenta_cliente] = array('sum' => 0, 'detalle' => array());
		}
		$cliente[$factura[0]->dc_cuenta_cliente]['sum'] += $factura[0]->dq_total;
		$cliente[$factura[0]->dc_cuenta_cliente]['detalle'][] = $factura[0];
		
		$iva['sum'] += $factura[0]->dq_iva;
		$iva['detalle'][] = $factura[0];
		
		//Agrupar los detalles de productos que mueven stock
		if(count($factura[1])){
			foreach($factura[1] as $dc_detalle => $detalle):
				if(!isset($top[$detalle->dc_cuenta_contable])){
					$top[$detalle->dc_cuenta_contable] = array('sum' => 0, 'detalle' => array());
				}
				$top[$detalle->dc_cuenta_contable]['sum'] += $detalle->dq_precio*$detalle->dc_cantidad;
				$top[$detalle->dc_cuenta_contable]['detalle'][] = $detalle;
			endforeach;
		}
	}
	
	return array($top,$iva,$cliente);
}

function insertarDetalles($data, $dc_comprobante, DBConnector $db){
	global $empresa;
	
	$grouped = agruparDetalles($data);
	//debug($grouped);return;
	
	//Insertar el asiento de gastos cobros o perdida
	$insertar_gastos = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => '?',
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?'
	)));
	$insertar_gastos->bindValue(1,$dc_comprobante,PDO::PARAM_INT);
	$insertar_gastos->bindParam(2,$dc_cuenta_contable,PDO::PARAM_INT);
	$insertar_gastos->bindParam(3,$dq_debe,PDO::PARAM_STR);
	$insertar_gastos->bindParam(4,$dq_haber,PDO::PARAM_STR);
	$insertar_gastos->bindParam(5,$dg_glosa,PDO::PARAM_STR);
	
	//Insertar el detalle analítico de gastos, cobros o perdida
	$insertar_analitico = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico',array(
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?',
		'dc_detalle_financiero' => '?',
		'dc_factura_compra' => '?',
		'dc_factura_venta' => '?',
		'dc_nota_venta' => '?',
		'dc_orden_servicio' => '?',
		'dc_proveedor' => '?',
		'dc_tipo_proveedor' => '?',
		'dc_cliente' => '?',
		'dc_tipo_cliente' => '?',
		'dc_orden_compra' => '?',
		'dc_producto' => '?',
		'dc_cebe' => '?',
		'dc_ceco' => '?',
		'dc_marca' => '?',
		'dc_linea_negocio' => '?',
		'dc_tipo_producto' => '?',
		'dc_banco' => '?',
		'dc_banco_cobro' => '?',
		'dc_guia_recepcion' => '?',
		'dc_medio_pago_proveedor' => '?',
		'dc_medio_cobro_cliente' => '?',
		'dc_mercado_cliente' => '?',
		'dc_mercado_proveedor' => '?',
		'dc_nota_credito' => '?',
		'dc_nota_credito_proveedor' => '?',
		'dc_segmento' => '?',
		'dg_cheque' => '?',
		'df_cheque' => '?',
		'dc_bodega' => '?',
		'dc_ejecutivo' => '?'
	)));
	$insertar_analitico->bindParam( 1,$dc_cuenta_contable,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 2,$dq_debe_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 3,$dq_haber_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 4,$dg_glosa,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 5,$dc_detalle_financiero,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 6,$dc_factura_compra,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 7,$dc_factura_venta,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 8,$dc_nota_venta,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 9,$dc_orden_servicio,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 10,$dc_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 11,$dc_tipo_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 12,$dc_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 13,$dc_tipo_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 14,$dc_orden_compra,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 15,$dc_producto,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 16,$dc_cebe,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 17,$dc_ceco,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 18,$dc_marca,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 19,$dc_linea_negocio,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 20,$dc_tipo_producto,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 21,$dc_banco,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 22,$dc_banco_cobro,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 23,$dc_guia_recepcion,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 24,$dc_medio_pago_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 25,$dc_medio_cobro_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 26,$dc_mercado_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 27,$dc_mercado_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 28,$dc_nota_credito,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 29,$dc_nota_credito_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 30,$dc_segmento,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 31,$dg_cheque,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 32,$df_cheque,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 33,$dc_bodega,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 34,$dc_ejecutivo,PDO::PARAM_INT);
	
	$top = $grouped[0];
	$dq_debe = 0;
	foreach($top as $dc_cuenta_contable => $item){
		$dq_haber = $item['sum'];
		$dg_glosa = "Centralización Libro de Venta: ".date('d-m-Y');
		$db->stExec($insertar_gastos);
		
		$dc_detalle_financiero = $db->lastInsertId();
		$dq_debe_analitico = 0;
		$dc_factura_compra = NULL;
		$dc_orden_compra = NULL;
		$dc_banco = NULL;
		$dc_banco_cobro = NULL;
		$dc_guia_recepcion = NULL;
		$dc_medio_pago_proveedor = NULL;
		$dc_medio_cobro_cliente = NULL;
		$dc_nota_credito = NULL;
		$dg_cheque = NULL;
		$df_cheque = NULL;
		$dc_bodega = NULL;
		$dc_nota_credito_proveedor = NULL;
		
		foreach($item['detalle'] as $d){

			$dq_haber_analitico = $d->dq_precio*$d->dc_cantidad;
			$dc_factura_venta = $d->dc_factura;
			$dc_nota_venta = $d->dc_nota_venta;
			$dc_orden_servicio = $d->dc_orden_servicio;
			$dc_proveedor = $d->dc_proveedor;//preguntar validez
			$dc_tipo_proveedor = $d->dc_tipo_proveedor;//preguntar validez
			$dc_cliente = $d->dc_cliente;
			$dc_tipo_cliente = $d->dc_tipo_cliente;
			$dc_producto = $d->dc_producto;
			$dc_cebe = $d->dc_cebe;
			$dc_ceco = $d->dc_ceco;
			$dc_marca = $d->dc_marca;
			$dc_linea_negocio = $d->dc_linea_negocio;
			$dc_tipo_producto = $d->dc_tipo_producto;
			$dc_mercado_cliente = $d->dc_mercado_cliente;
			$dc_mercado_proveedor = $d->dc_mercado_proveedor;
			$dc_segmento = $d->dc_segmento;
			$dc_ejecutivo = $d->dc_ejecutivo;
			
			$db->stExec($insertar_analitico);
		}
		
	}
	
	//Insertar detalle de IVA para el comprobante de centralización
	$iva = $grouped[1];
	$conf = ContabilidadStuff::getConfiguration();
	
	$dc_cuenta_contable = $conf->dc_cuenta_contable_iva_debito;
	
	$dq_debe = 0;
	$dq_haber = floatval($iva['sum']);
	$dg_glosa = "IVA DÉBITO Centralización Libro de Venta: ".date('d-m-Y');
	
	$db->stExec($insertar_gastos);
	
	$dc_detalle_financiero = $db->lastInsertId();
	$dq_debe_analitico = 0;
	$dg_glosa = '';
	$dc_factura_compra = NULL;
	$dc_proveedor = NULL;
	$dc_tipo_proveedor = NULL;
	$dc_orden_compra = NULL;
	$dc_producto = NULL;
	$dc_cebe = NULL;
	$dc_ceco = NULL;
	$dc_marca = NULL;
	$dc_linea_negocio = NULL;
	$dc_tipo_producto = NULL;
	$dc_banco = NULL;
	$dc_banco_cobro = NULL;
	$dc_guia_recepcion = NULL;
	$dc_medio_pago_proveedor = NULL;
	$dc_medio_cobro_cliente = NULL;
	$dc_mercado_proveedor = NULL;
	$dc_nota_credito = NULL;
	$dc_nota_credito_proveedor = NULL;
	$dc_segmento = NULL;
	$dg_cheque = NULL;
	$df_cheque = NULL;
	$dc_bodega = NULL;
	
	foreach($iva['detalle'] as $d){
		$dq_haber_analitico = $d->dq_iva;
		$dc_factura_venta = $d->dc_factura;
		$dc_nota_venta = $d->dc_nota_venta;
		$dc_orden_servicio = $d->dc_orden_servicio;
		$dc_cliente = $d->dc_cliente;
		$dc_tipo_cliente = $d->dc_tipo_cliente;
		$dc_mercado_cliente = $d->dc_mercado_cliente;
		$dc_ejecutivo = $d->dc_ejecutivo;
		
		$db->stExec($insertar_analitico);
	}
	
	//Cargo a cuentas de proveedores
	$cliente = $grouped[2];
	//echo "<pre>";var_dump($proveedor);echo "</pre>";
	
	$dq_haber = 0;
	foreach($cliente as $dc_cuenta_contable => $item){
		$dq_debe = $item['sum'];
		$dg_glosa = "Carga Cliente Centralización Libro de Venta: ".date('d-m-Y');
		
		$db->stExec($insertar_gastos);
		
		$dc_detalle_financiero = $db->lastInsertId();
		$dq_haber_analitico = 0;
		$dg_glosa = '';
		$dc_factura_compra = NULL;
		$dc_proveedor = NULL;
		$dc_tipo_proveedor = NULL;
		$dc_orden_compra = NULL;
		$dc_producto = NULL;
		$dc_cebe = NULL;
		$dc_ceco = NULL;
		$dc_marca = NULL;
		$dc_linea_negocio = NULL;
		$dc_tipo_producto = NULL;
		$dc_banco = NULL;
		$dc_banco_cobro = NULL;
		$dc_guia_recepcion = NULL;
		$dc_medio_pago_proveedor = NULL;
		$dc_medio_cobro_cliente = NULL;
		$dc_mercado_proveedor = NULL;
		$dc_nota_credito = NULL;
		$dc_nota_credito_proveedor = NULL;
		$dc_segmento = NULL;
		$dg_cheque = NULL;
		$df_cheque = NULL;
		$dc_bodega = NULL;
		
		foreach($item['detalle'] as $d){
			$dq_debe_analitico = floatval($d->dq_total);
			$dc_factura_venta = $d->dc_factura;
			$dc_nota_venta = $d->dc_nota_venta;
			$dc_orden_servicio = $d->dc_orden_servicio;
			$dc_cliente = $d->dc_cliente;
			$dc_tipo_cliente = $d->dc_tipo_cliente;
			$dc_mercado_cliente = $d->dc_mercado_cliente;
			$dc_ejecutivo = $d->dc_ejecutivo;
			
			$db->stExec($insertar_analitico);
		}
	}
	
}

function desmarcarFacturas($doc, DBConnector $db){
	
	if(!isset($doc['FV'])){
		return;
	}
	
	$dc_factura = explode(',',implode(',',$doc['FV']));
	$questions = substr(str_repeat(',?',count($dc_factura)),1);
	
	$update_factura = $db->prepare($db->update('tb_factura_venta',array(
		'dm_centralizada' => 1
	),"dc_factura IN ({$questions})"));
	
	foreach($dc_factura as $i => $fc){
		$update_factura->bindValue($i+1,$fc,PDO::PARAM_INT);
	}
	
	$db->stExec($update_factura);
}

function obtenerAsientos($data, DBConnector $db){
	global $empresa;
	
	$grouped = agruparDetalles($data);
	//debug($grouped);return;
	
	$detalles_financieros = array();
	$detalles_analiticos = array();
	
	$top = $grouped[0];
	$dq_debe = 0;
	foreach($top as $dc_cuenta_contable => $item){
		
		$detalles_financieros[] = array(
			'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
			'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
			'dg_glosa' => "Centralización Libro de Venta: ".date('d-m-Y'),
			'dq_debe' => 0,
			'dq_haber' => $item['sum']
		);
		
		foreach($item['detalle'] as $d){
			
			$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
				'dc_cuenta_contable' => $dc_cuenta_contable,
				'dq_debe' => 0,
				'dq_haber' => $d->dq_precio*$d->dc_cantidad,
				'dg_glosa' => "Centralización Libro de Compra: ".date('d-m-Y'),
				'dc_factura_venta' => $d->dc_factura,
				'dc_nota_venta' => $d->dc_nota_venta,
				'dc_orden_servicio' => $d->dc_orden_servicio,
				'dc_proveedor' => $d->dc_proveedor,
				'dc_tipo_proveedor' => $d->dc_tipo_proveedor,
				'dc_cliente' => $d->dc_cliente,
				'dc_tipo_cliente' => $d->dc_tipo_cliente,
				'dc_producto' => $d->dc_producto,
				'dc_debe' => $d->dc_cebe,
				'dc_ceco' => $d->dc_ceco,
				'dc_marca' => $d->dc_marca,
				'dc_linea_negocio' => $d->dc_linea_negocio,
				'dc_tipo_producto' => $d->dc_tipo_producto,
				'dc_mercado_cliente' => $d->dc_mercado_cliente,
				'dc_mercado_proveedor' => $d->dc_mercado_proveedor,
				'dc_segmento' => $d->dc_segmento,
				'dc_ejecutivo' => $d->dc_ejecutivo
			));
		}
		
	}
	
	//Insertar detalle de IVA para el comprobante de centralización
	$iva = $grouped[1];
	$conf = ContabilidadStuff::getConfiguration();
	
	$dc_cuenta_contable = $conf->dc_cuenta_contable_iva_debito;
	
	$detalles_financieros[] = array(
		'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
		'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
		'dg_glosa' => "IVA DÉBITO Centralización Libro de Venta: ".date('d-m-Y'),
		'dq_debe' => 0,
		'dq_haber' => floatval($iva['sum'])
	);
	
	foreach($iva['detalle'] as $d){
		
		$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
			'dc_cuenta_contable' => $dc_cuenta_contable,
			'dq_debe' => 0,
			'dq_haber' => floatval($d->dq_iva),
			'dg_glosa' => "IVA DÉBITO Centralización Libro de Venta: ".date('d-m-Y'),
			'dc_factura_venta' => $d->dc_factura,
			'dc_nota_venta' => $d->dc_nota_venta,
			'dc_orden_servicio' => $d->dc_orden_servicio,
			'dc_cliente' => $d->dc_cliente,
			'dc_tipo_cliente' => $d->dc_tipo_cliente,
			'dc_mercado_cliente' => $d->dc_mercado_cliente,
			'dc_ejecutivo' => $d->dc_ejecutivo
		));
	}
	
	//VAMOS A DEJARLO HASTA AQUÍ POR HOY XD!!!
	
	//Cargo a cuentas de proveedores
	$cliente = $grouped[2];
	//echo "<pre>";var_dump($proveedor);echo "</pre>";
	
	$dq_debe = 0;
	foreach($cliente as $dc_cuenta_contable => $item){
		
		$detalles_financieros[] = array(
			'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
			'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
			'dg_glosa' => "Carga Cliente Centralización Libro de Venta: ".date('d-m-Y'),
			'dq_debe' => floatval($item['sum']),
			'dq_haber' => 0
		);
		
		foreach($item['detalle'] as $d){
			
			$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
				'dc_cuenta_contable' => $dc_cuenta_contable,
				'dq_debe' => floatval($d->dq_total),
				'dq_haber' => 0,
				'dg_glosa' => "Carga Cliente Centralización Libro de Venta: ".date('d-m-Y'),
				'dc_factura_venta' => $d->dc_factura,
				'dc_nota_venta' => $d->dc_nota_venta,
				'dc_orden_servicio' => $d->dc_orden_servicio,
				'dc_cliente' => $d->dc_cliente,
				'dc_tipo_cliente' => $d->dc_tipo_cliente,
				'dc_mercado_cliente' => $d->dc_mercado_cliente,
				'dc_ejecutivo' => $d->dc_ejecutivo
			));
		}
	}
	
	return array($detalles_financieros, $detalles_analiticos);
	
}