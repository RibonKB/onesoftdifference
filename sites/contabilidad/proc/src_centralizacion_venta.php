<?php
require_once('../../../inc/db-class.php');
require_once('../stuff.class.php');

require_once('src_centralizacion_venta_factura.php');
require_once('src_centralizacion_venta_nota_credito.php');

function buscarResultados($param = array(),DBConnector $db){
	
	global $empresa;
	
	$df_corte = $db->sqlDate($param['df_emision_hasta'].' 23:59');
	$cond_fc = "fv.dc_empresa = {$empresa} AND fv.df_emision < {$df_corte} AND fv.dm_nula = 0 AND fv.dm_centralizada = 0";
	$cond_nc = "nc.dc_empresa = {$empresa} AND nc.df_emision < {$df_corte} AND nc.dm_nula = 0 AND nc.dm_centralizada = 0";
	
	if(isset($param['dc_cliente'])){
		$clientes = implode(',',$param['dc_cliente']);
		$cond_fc .= " AND fv.dc_cliente IN ({$clientes})";
		$cond_nc .= " AND nc.dc_cliente IN ({$clientes})";
	}
	
	$tables_fc = "tb_factura_venta fv
		LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fv.dc_nota_venta
		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fv.dc_orden_servicio
		LEFT JOIN tb_tipo_operacion t_op ON t_op.dc_tipo_operacion = fv.dc_tipo_operacion
		LEFT JOIN tb_usuario us ON us.dc_usuario = fv.dc_usuario_creacion
		JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente";
		
	$fields_fc = '"FV" tp, nv.dq_nota_venta,fv.df_emision, cl.dg_rut, cl.dg_razon, fv.dq_exento, fv.dq_neto, fv.dq_iva, fv.dq_total,
	fv.dq_factura dq_factura,fv.dq_folio,os.dq_orden_servicio,fv.dm_nula, t_op.dg_tipo_operacion dg_tipo, us.dg_usuario, fv.dc_factura dc_documento';
	
	$tables_nc = "tb_nota_credito nc
		LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = nc.dc_nota_venta
		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = nc.dc_orden_servicio
		LEFT JOIN tb_tipo_nota_credito t_nc ON t_nc.dc_tipo_nota_credito = nc.dc_tipo_nota_credito
		LEFT JOIN tb_usuario us ON us.dc_usuario = nc.dc_usuario_creacion
		JOIN tb_cliente cl ON cl.dc_cliente = nc.dc_cliente";
		
	$fields_nc = '"NC", nv.dq_nota_venta, nc.df_emision, cl.dg_rut, cl.dg_razon, -nc.dq_exento, -nc.dq_neto, -nc.dq_iva, -nc.dq_total,
	nc.dq_nota_credito,nc.dq_folio,os.dq_orden_servicio,nc.dm_nula, t_nc.dg_tipo_nota_credito, us.dg_usuario, nc.dc_nota_credito';
	
	return $db->doQuery(
		"(".$db->select($tables_fc,$fields_fc,$cond_fc).")".
		"UNION ALL".
		"(".$db->select($tables_nc,$fields_nc,$cond_nc).")".
		" ORDER BY df_emision"
	)->fetchAll(PDO::FETCH_OBJ);
	
}

//Funciones de inserción
function insertarComprobanteContable($data,$df_contable, DBConnector $db){
	global $empresa, $idUsuario;
	
	$dq_comprobante = ContabilidadStuff::getCorrelativoComprobante();
	$conf = ContabilidadStuff::getConfiguration();
	$date_comp = explode('/',$df_contable);
	
	$dq_saldo = 0;
	foreach($data[0] as $dc_factura => $data_fac):
		$factura = $data_fac[0];
		$dq_saldo += floatval($factura->dq_total);
	endforeach;
	
	foreach($data[1] as $dc_nota_credito => $data_nc):
		$nota_credito = $data_nc[0];
		$dq_saldo += floatval($nota_credito->dq_total);
	endforeach;
	
	$comprobante = $db->prepare($db->insert('tb_comprobante_contable',array(
		'dg_comprobante' => '?',
		'dc_tipo_movimiento' => '?',
		'df_fecha_contable' => '?',
		'dc_mes_contable' => '?',
		'dc_anho_contable' => '?',
		'dq_cambio' => '1',
		'dg_glosa' => '?',
		'dq_saldo' => '?',
		'df_fecha_emision' => $db->getNow(),
		'dc_empresa' => $empresa,
		'dc_usuario_creacion' => $idUsuario,
		'dm_tipo_comprobante' => 3
	)));
	$comprobante->bindValue(1,$dq_comprobante,PDO::PARAM_INT);
	$comprobante->bindValue(2,$conf->dc_tipo_movimiento_centralizacion_venta,PDO::PARAM_INT);
	$comprobante->bindValue(3,$db->sqlDate2($df_contable),PDO::PARAM_STR);
	$comprobante->bindValue(4,$date_comp[1],PDO::PARAM_INT);
	$comprobante->bindValue(5,$date_comp[2],PDO::PARAM_INT);
	$comprobante->bindValue(6,"Centralización Libro de Venta: ".date('d-m-Y'),PDO::PARAM_STR);
	$comprobante->bindValue(7,$dq_saldo,PDO::PARAM_STR);
	
	$db->stExec($comprobante);
	
	return $db->lastInsertId();
	
}

?>