<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_comprobante_contable','dm_anulado, dm_tipo_comprobante',"dc_comprobante = {$_POST['null_comp_id']} AND dc_empresa = {$empresa}");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado el comprobante contable especificado");
}
$data = array_shift($data);

if($data['dm_anulado'] == 1){
	$error_man->showWarning('Este comprobante se encuentra anulado');
	exit();
}

if($data['dm_tipo_comprobante'] != 0){
	$error_man->showWarning("No puede anular de esta manera comprobantes contables generados automáticamente por los otros módulos del sistema, debe anularlo en el módulo correspondiente.");
	exit;
}

//se desactiva el registro en la base de datos
$db->update("tb_comprobante_contable",array("dm_anulado" => "1"),"dc_comprobante = {$_POST['null_comp_id']}");



$db->insert('tb_comprobante_contable_anulado', array(
	'dc_comprobante' => $_POST['null_comp_id'],
	'dc_motivo' => $_POST['null_motivo'],
	'dg_comentario' => $_POST['null_comentario'],
	'dc_usuario' => $idUsuario,
	'df_anulacion' => 'NOW()'
));

?>
<script type="text/javascript">
	$('#genOverlay').remove();
</script>