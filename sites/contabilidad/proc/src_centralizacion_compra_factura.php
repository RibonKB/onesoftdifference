<?php

require_once('../../../inc/db-class.php');
require_once('../stuff.class.php');

//Funciones de obtención  de datos
function obtenerDataFactura($param, DBConnector $db){
	global $empresa, $error_man;

	$fc = array();
	if(isset($param['dc_documento']['FC'])){
		$dc_factura = explode(',',implode(',',$param['dc_documento']['FC']));
		$questions = substr(str_repeat(',?',count($dc_factura)),1);

		//Obtener los detalles de cabecera para las facturas
		$facturas = $db->prepare(
						$db->select('tb_factura_compra fc
									 JOIN tb_proveedor p ON p.dc_proveedor = fc.dc_proveedor
									 LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fc.dc_nota_venta
									 LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fc.dc_orden_servicio
									 LEFT JOIN tb_cliente clnv ON clnv.dc_cliente = nv.dc_cliente
									 LEFT JOIN tb_cliente clos ON clos.dc_cliente = os.dc_cliente',
									'fc.dc_factura, fc.dq_folio, fc.dq_factura, fc.dc_proveedor, fc.dq_total, fc.dq_iva, p.dc_cuenta_contable dc_cuenta_proveedor,
									 nv.dc_cliente dc_cliente_nv, os.dc_cliente dc_cliente_os, clnv.dc_tipo_cliente dc_tipo_cliente_nv, clos.dc_tipo_cliente dc_tipo_cliente_os,
									 p.dc_tipo_proveedor, clnv.dc_mercado dc_mercado_nv, clos.dc_mercado dc_mercado_os, fc.dc_nota_venta, fc.dc_orden_servicio,
									 p.dc_mercado dc_mercado_proveedor',
									"fc.dc_empresa = {$empresa} AND fc.dm_centralizada = 0 AND fc.dc_factura IN ({$questions})"));
		foreach($dc_factura as $i => $f){
			$facturas->bindValue($i+1,$f,PDO::PARAM_INT);
		}
		$db->stExec($facturas);

		//Agrupar en el array de facturas de compra las facturas encontradas
		$dc_factura_get = array();
		while($f = $facturas->fetch(PDO::FETCH_OBJ)):
			if($f->dc_nota_venta){
				$f->dc_cliente = $f->dc_cliente_nv;
				$f->dc_tipo_cliente = $f->dc_tipo_cliente_nv;
				$f->dc_mercado_cliente = $f->dc_mercado_nv;
			}else if($f->dc_orden_servicio){
				$f->dc_cliente = $f->dc_cliente_os;
				$f->dc_tipo_cliente = $f->dc_tipo_cliente_os;
				$f->dc_mercado_cliente = $f->dc_mercado_os;
			}else{
				$f->dc_cliente = NULL;
				$f->dc_tipo_cliente = NULL;
				$f->dc_mercado_cliente = NULL;
			}
			$fc[$f->dc_factura] = array($f, array(), array());
			$dc_factura_get[] = $f->dc_factura;
		endwhile;
		unset($facturas);

		//Comprobar que la cantidad de facturas enviadas desde el formulario sea la misma obtenida desde la base de datos
		$diferencias = valida_cantidad_erronea_facturas($dc_factura,$dc_factura_get);
		if(count($diferencias)){
			return array('ERROR_ITEM_NOT_FOUND',$diferencias);
		}

		//obtener los detalles de las facturas para asignarlos y agruparlos.
		$detalles = $db->prepare($db->select('tb_factura_compra_detalle d
									LEFT JOIN tb_ceco cco ON cco.dc_ceco = d.dc_ceco
									LEFT JOIN tb_producto p ON p.dc_producto = d.dc_producto
									LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto
									LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio
									LEFT JOIN tb_nota_venta_detalle nvd ON nvd.dc_nota_venta_detalle = d.dc_detalle_nota_venta
									LEFT JOIN tb_orden_servicio_factura_detalle osd ON osd.dc_detalle = d.dc_detalle_orden_servicio
									LEFT JOIN tb_guia_recepcion gr ON gr.dc_factura = d.dc_factura
									LEFT JOIN tb_guia_recepcion_detalle grd ON grd.dc_guia_recepcion = gr.dc_guia_recepcion AND grd.dc_producto = d.dc_producto
									LEFT JOIN tb_linea_negocio_cuenta_contable lnc ON lnc.dc_linea_negocio = p.dc_linea_negocio AND lnc.dc_bodega = grd.dc_bodega',
								'p.dg_codigo, d.dc_detalle, d.dc_factura, d.dc_producto, d.dc_cantidad dc_cantidad_factura, d.dq_precio, d.dc_ceco, tp.dm_controla_inventario,
								 p.dc_marca, p.dc_tipo_producto, p.dc_linea_negocio, nvd.dc_nota_venta, nvd.dc_cebe, lnc.dc_cuenta_contable dc_cuenta_contable_producto,
								 osd.dc_orden_servicio, gr.dc_orden_compra, SUM(grd.dq_cantidad) dc_cantidad_recepcion, grd.dc_bodega, ln.dc_cuenta_contable_compra,
								 gr.dc_guia_recepcion, cco.dc_segmento, d.dc_cuenta_servicio dc_cuenta_contable_servicio_detalle',
								"d.dc_factura IN ({$questions}) AND (gr.dc_guia_recepcion IS NULL OR (gr.dc_guia_recepcion IS NOT NULL AND grd.dc_detalle IS NOT NULL))",
								array('group_by' => 'd.dc_detalle, d.dc_producto, grd.dc_detalle, grd.dc_bodega')));
		foreach($dc_factura as $i => $f){
			$detalles->bindValue($i+1,$f,PDO::PARAM_INT);
		}
		$db->stExec($detalles);

		//Agrupar los detalles de la factura en el array de agrupaciones
		while($d = $detalles->fetch(PDO::FETCH_OBJ)):
			$d->dc_cliente = $fc[$d->dc_factura][0]->dc_cliente;
			$d->dc_tipo_cliente = $fc[$d->dc_factura][0]->dc_tipo_cliente;
			$d->dc_mercado_cliente = $fc[$d->dc_factura][0]->dc_mercado_cliente;
			$d->dc_proveedor = $fc[$d->dc_factura][0]->dc_proveedor;
			$d->dc_tipo_proveedor = $fc[$d->dc_factura][0]->dc_tipo_proveedor;
			$d->dc_mercado_proveedor = $fc[$d->dc_factura][0]->dc_mercado_proveedor;

			if($d->dc_cuenta_contable_servicio_detalle){
				$d->dc_cuenta_contable_servicio = $d->dc_cuenta_contable_servicio_detalle;
			}else{
				$d->dc_cuenta_contable_servicio = $d->dc_cuenta_contable_compra;
			}

			if(!$d->dc_cuenta_contable_producto){
				$d->dc_cuenta_contable_producto = $d->dc_cuenta_contable_compra;
			}

			$fc[$d->dc_factura][$d->dm_controla_inventario+1][$d->dc_detalle][] = $d;
		endwhile;

		return array('OK',$fc);

	}else{
		return array('OK',array());
	}
}

//Funciones de validación de datos
function validarFacturaCreacion($fc, DBConnector $db){
	global $empresa, $empresa_conf;

	//Validar para todas las facturas que posean productos que mueven stock que estos estén relacionados a una guía de recepción
	foreach($fc as $dc_factura => $data){
		if(count($data[1])){
			$error = validaStockRecepcionado($data[1]);
			if($error !== true){
				return array('ERROR_NO_RECEPCIONADO',array($error, $data[0]));
			}
		}
	}

	//Validar que los valores de los detalles cuadren con los totales de la cabecera
	foreach($fc as $dc_factura => $data){
		$dq_total = round(floatval($data[0]->dq_total),$empresa_conf['dn_decimales_local']);
		$dq_iva = round(floatval($data[0]->dq_iva),$empresa_conf['dn_decimales_local']);
		$dq_total_productos = round(sumaDetalle($data[1]),$empresa_conf['dn_decimales_local']);
		$dq_total_servicios = round(sumaDetalle($data[2]),$empresa_conf['dn_decimales_local']);

		if($dq_total != ($dq_iva+$dq_total_productos+$dq_total_servicios)){
			return array('ERROR_NO_CUADRA',array($data[0],$dq_total_productos, $dq_total_servicios,$dq_iva));
		}

	}

	//Validar cuenta contable disponible para todas las lineas de negocio en los detalles
	foreach($fc as $dc_factura => $data){
		if(count($data[1])){
			foreach($data[1] as $dc_detalle => $data_producto){
				foreach($data_producto as $detalle){
					if($detalle->dc_cuenta_contable_producto == 0){
						return array('ERROR_PRODUCTO_CUENTA_CONTABLE',array($data[0], $detalle));
					}
				}
			}
		}

		if(count($data[2])){
			foreach($data[2] as $detalle){
				if($detalle[0]->dc_cuenta_contable_servicio == 0){
					return array('ERROR_SERVICIO_CUENTA_CONTABLE',array($data[0], $detalle[0]));
				}
			}
		}
	}

	$conf = ContabilidadStuff::getConfiguration();

	if($conf->dc_cuenta_contable_iva_credito == 0){
		return array('ERROR_IVA_CUENTA_CONTABLE',array());
	}

	return true;

}

function validaStockRecepcionado($detalle){
	foreach($detalle as $dc_detalle => $data){
		$suma = 0;
		$total = 0;
		foreach($data as $instance){
			$total = $instance->dc_cantidad_factura;
			$suma += $instance->dc_cantidad_recepcion;
			$last = $instance;
		}
		if($suma != $total){
			//Aquí tengo que verlo xD!
			return $last;
		}
	}

	return true;
}

function sumaDetalle($detalle){
	$suma = 0;
	foreach($detalle as $dc_detalle => $data){
		$suma += floatval($data[0]->dc_cantidad_factura * $data[0]->dq_precio);
	}
	return floatval($suma);
}

function valida_cantidad_erronea_facturas($dc_factura, $facturas){
	return array_diff($dc_factura,$facturas);
}

function agruparDetalles($data){
	$ln = array();
	$proveedor = array();
	$iva = array('sum' => 0, 'detalle' => array());

	foreach($data as $dc_factura => $factura){
		//agrupar los proveedores
		if(!isset($proveedor[$factura[0]->dc_cuenta_proveedor])){
			$proveedor[$factura[0]->dc_cuenta_proveedor] = array('sum' => 0, 'detalle' => array());
		}
		$proveedor[$factura[0]->dc_cuenta_proveedor]['sum'] += $factura[0]->dq_total;
		$proveedor[$factura[0]->dc_cuenta_proveedor]['detalle'][] = $factura[0];

		$iva['sum'] += $factura[0]->dq_iva;
		$iva['detalle'][] = $factura[0];

		//Agrupar los detalles de productos que mueven stock
		if(count($factura[1])){
			foreach($factura[1] as $dc_detalle => $detalle):
				foreach($detalle as $producto):
					if(!isset($ln[$producto->dc_cuenta_contable_producto])){
						$ln[$producto->dc_cuenta_contable_producto] = array('sum' => 0, 'detalle' => array());
					}
					$ln[$producto->dc_cuenta_contable_producto]['sum'] += $producto->dq_precio*$producto->dc_cantidad_recepcion;
					$ln[$producto->dc_cuenta_contable_producto]['detalle'][] = $producto;
				endforeach;
			endforeach;
		}

		//Agrupar los detalles de servicios
		if(count($factura[2])){
			foreach($factura[2] as $dc_detalle => $servicio):
				if(!isset($ln[$servicio->dc_cuenta_contable_servicio])){
					$ln[$servicio->dc_cuenta_contable_servicio] = array('sum' => 0, 'detalle' => array());
				}
				$ln[$servicio->dc_cuenta_contable_servicio]['sum'] += $servicio->dq_precio*$servicio->dc_cantidad_factura;
				$ln[$servicio->dc_cuenta_contable_servicio]['detalle'][] = $servicio;
			endforeach;
		}
	}

	return array($ln,$iva,$proveedor);
}

function insertarDetalles($data, $dc_comprobante, DBConnector $db){
	global $empresa;

	$grouped = agruparDetalles($data);
	//debug($grouped);return;

	//Insertar el asiento de gastos cobros o perdida
	$insertar_gastos = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => '?',
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?'
	)));
	$insertar_gastos->bindValue(1,$dc_comprobante,PDO::PARAM_INT);
	$insertar_gastos->bindParam(2,$dc_cuenta_contable,PDO::PARAM_INT);
	$insertar_gastos->bindParam(3,$dq_debe,PDO::PARAM_STR);
	$insertar_gastos->bindParam(4,$dq_haber,PDO::PARAM_STR);
	$insertar_gastos->bindParam(5,$dg_glosa,PDO::PARAM_STR);

	//Insertar el detalle analítico de gastos, cobros o perdida
	$insertar_analitico = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico',array(
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?',
		'dc_detalle_financiero' => '?',
		'dc_factura_compra' => '?',
		'dc_nota_venta' => '?',
		'dc_orden_servicio' => '?',
		'dc_proveedor' => '?',
		'dc_tipo_proveedor' => '?',
		'dc_cliente' => '?',
		'dc_tipo_cliente' => '?',
		'dc_orden_compra' => '?',
		'dc_producto' => '?',
		'dc_cebe' => '?',
		'dc_ceco' => '?',
		'dc_marca' => '?',
		'dc_linea_negocio' => '?',
		'dc_tipo_producto' => '?',
		'dc_guia_recepcion' => '?',
		'dc_mercado_cliente' => '?',
		'dc_mercado_proveedor' => '?',
		'dc_nota_credito_proveedor' => '?',
		'dc_segmento' => '?',
		'dc_bodega' => '?'
	)));
	$insertar_analitico->bindParam( 1,$dc_cuenta_contable,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 2,$dq_debe_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 3,$dq_haber_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 4,$dg_glosa_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 5,$dc_detalle_financiero,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 6,$dc_factura,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 7,$dc_nota_venta,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 8,$dc_orden_servicio,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 9,$dc_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam(10,$dc_tipo_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam(11,$dc_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam(12,$dc_tipo_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam(13,$dc_orden_compra,PDO::PARAM_INT);
	$insertar_analitico->bindParam(14,$dc_producto,PDO::PARAM_INT);
	$insertar_analitico->bindParam(15,$dc_cebe,PDO::PARAM_INT);
	$insertar_analitico->bindParam(16,$dc_ceco,PDO::PARAM_INT);
	$insertar_analitico->bindParam(17,$dc_marca,PDO::PARAM_INT);
	$insertar_analitico->bindParam(18,$dc_linea_negocio,PDO::PARAM_INT);
	$insertar_analitico->bindParam(19,$dc_tipo_producto,PDO::PARAM_INT);
	$insertar_analitico->bindParam(20,$dc_guia_recepcion,PDO::PARAM_INT);
	$insertar_analitico->bindParam(21,$dc_mercado_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam(22,$dc_mercado_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam(23,$dc_nota_credito,PDO::PARAM_INT);
	$insertar_analitico->bindParam(24,$dc_segmento,PDO::PARAM_INT);
	$insertar_analitico->bindParam(25,$dc_bodega,PDO::PARAM_INT);

	$ln = $grouped[0];
	$dq_haber = 0;
	foreach($ln as $dc_cuenta_contable => $item){
		$dq_debe = $item['sum'];
		$dg_glosa = "Centralización Libro de Compra: ".date('d-m-Y');
		$db->stExec($insertar_gastos);

		$dc_detalle_financiero = $db->lastInsertId();
		$dq_haber_analitico = 0;

		foreach($item['detalle'] as $d){
			if($d->dm_controla_inventario == 0)
				$dq_debe_analitico = $d->dq_precio*$d->dc_cantidad_recepcion;
			else
				$dq_debe_analitico = $d->dq_precio*$d->dc_cantidad_factura;

			$dg_glosa_analitico = "Centralización Libro de Compra: ".date('d-m-Y');
			$dc_factura = $d->dc_factura;
			$dc_nota_venta = $d->dc_nota_venta;
			$dc_orden_servicio = $d->dc_orden_servicio;
			$dc_proveedor = $d->dc_proveedor;
			$dc_tipo_proveedor = $d->dc_tipo_proveedor;
			$dc_cliente = $d->dc_cliente;
			$dc_tipo_cliente = $d->dc_tipo_cliente;
			$dc_orden_compra = $d->dc_orden_compra;
			$dc_producto = $d->dc_producto;
			$dc_cebe = $d->dc_cebe;
			$dc_ceco = $d->dc_ceco;
			$dc_marca = $d->dc_marca;
			$dc_linea_negocio = $d->dc_linea_negocio;
			$dc_tipo_producto = $d->dc_tipo_producto;
			$dc_guia_recepcion = $d->dc_guia_recepcion;
			$dc_mercado_cliente = $d->dc_mercado_cliente;
			$dc_mercado_proveedor = $d->dc_mercado_proveedor;
			$dc_nota_credito = NULL;
			$dc_segmento = $d->dc_segmento;
			$dc_bodega = $d->dc_bodega;

			$db->stExec($insertar_analitico);
		}

	}

	//Insertar detalle de IVA para el comprobante de centralización
	$iva = $grouped[1];
	$conf = ContabilidadStuff::getConfiguration();

	$dc_cuenta_contable = $conf->dc_cuenta_contable_iva_credito;

	$dq_debe = floatval($iva['sum']);
	$dq_haber = 0;
	$dg_glosa = "IVA CRÉDITO Centralización Libro de Compra: ".date('d-m-Y');

	$db->stExec($insertar_gastos);

	$dc_detalle_financiero = $db->lastInsertId();
	$dq_haber_analitico = 0;
	$dg_glosa_analitico = "IVA CRÉDITO Centralización Libro de Compra: ".date('d-m-Y');
	$dc_orden_compra = NULL;
	$dc_producto = NULL;
	$dc_cebe = NULL;
	$dc_ceco = NULL;
	$dc_marca = NULL;
	$dc_linea_negocio = NULL;
	$dc_tipo_producto = NULL;
	$dc_guia_recepcion = NULL;
	$dc_nota_credito = NULL;
	$dc_segmento = NULL;
	$dc_bodega = NULL;

	foreach($iva['detalle'] as $d){
		$dq_debe_analitico = floatval($d->dq_iva);
		$dc_factura = $d->dc_factura;
		$dc_nota_venta = $d->dc_nota_venta;
		$dc_orden_servicio = $d->dc_orden_servicio;
		$dc_proveedor = $d->dc_proveedor;
		$dc_tipo_proveedor = $d->dc_tipo_proveedor;
		$dc_cliente = $d->dc_cliente;
		$dc_tipo_cliente = $d->dc_tipo_cliente;
		$dc_mercado_cliente = $d->dc_mercado_cliente;
		$dc_mercado_proveedor = $d->dc_mercado_proveedor;

		$db->stExec($insertar_analitico);
	}

	//Cargo a cuentas de proveedores
	$proveedor = $grouped[2];
	//echo "<pre>";var_dump($proveedor);echo "</pre>";

	$dq_debe = 0;
	foreach($proveedor as $dc_cuenta_contable => $item){
		$dq_haber = $item['sum'];
		$dg_glosa = "Carga Proveedor Centralización Libro de Compra: ".date('d-m-Y');

		$db->stExec($insertar_gastos);

		$dc_detalle_financiero = $db->lastInsertId();
		$dq_debe_analitico = 0;
		$dg_glosa_analitico = '';
		$dc_orden_compra = NULL;
		$dc_producto = NULL;
		$dc_cebe = NULL;
		$dc_ceco = NULL;
		$dc_marca = NULL;
		$dc_linea_negocio = NULL;
		$dc_tipo_producto = NULL;
		$dc_guia_recepcion = NULL;
		$dc_nota_credito = NULL;
		$dc_segmento = NULL;
		$dc_bodega = NULL;

		foreach($item['detalle'] as $d){
			$dq_haber_analitico = floatval($d->dq_total);
			$dc_factura = $d->dc_factura;
			$dc_nota_venta = $d->dc_nota_venta;
			$dc_orden_servicio = $d->dc_orden_servicio;
			$dc_proveedor = $d->dc_proveedor;
			$dc_tipo_proveedor = $d->dc_tipo_proveedor;
			$dc_cliente = $d->dc_cliente;
			$dc_tipo_cliente = $d->dc_tipo_cliente;
			$dc_mercado_cliente = $d->dc_mercado_cliente;
			$dc_mercado_proveedor = $d->dc_mercado_proveedor;

			$db->stExec($insertar_analitico);
		}
	}

}

function desmarcarFacturas($doc, DBConnector $db){

	if(!isset($doc['FC'])){
		return;
	}

	$dc_factura = explode(',',implode(',',$doc['FC']));
	$questions = substr(str_repeat(',?',count($dc_factura)),1);

	$update_factura = $db->prepare($db->update('tb_factura_compra',array(
		'dm_centralizada' => 1
	),"dc_factura IN ({$questions})"));

	foreach($dc_factura as $i => $fc){
		$update_factura->bindValue($i+1,$fc,PDO::PARAM_INT);
	}

	$db->stExec($update_factura);
}

function obtenerAsientos($data, DBConnector $db){

	$grouped = agruparDetalles($data);
	//debug($grouped);return;

	$detalles_financieros = array();
	$detalles_analiticos = array();

	$ln = $grouped[0];
	foreach($ln as $dc_cuenta_contable => $item){

		$detalles_financieros[] = array(
			'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
			'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
			'dg_glosa' => "Centralización Libro de Compra: ".date('d-m-Y'),
			'dq_debe' => $item['sum'],
			'dq_haber' => 0
		);

		foreach($item['detalle'] as $d){
			if($d->dm_controla_inventario == 0)
				$dq_debe_analitico = $d->dq_precio*$d->dc_cantidad_recepcion;
			else
				$dq_debe_analitico = $d->dq_precio*$d->dc_cantidad_factura;

			$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
				'dc_cuenta_contable' => $dc_cuenta_contable,
				'dq_debe' => $dq_debe_analitico,
				'dq_haber' => 0,
				'dg_glosa' => "Centralización Libro de Compra: ".date('d-m-Y'),
				'dc_factura_compra' => $d->dc_factura,
				'dc_nota_venta' => $d->dc_nota_venta,
				'dc_orden_servicio' => $d->dc_orden_servicio,
				'dc_proveedor' => $d->dc_proveedor,
				'dc_tipo_proveedor' => $d->dc_tipo_proveedor,
				'dc_cliente' => $d->dc_cliente,
				'dc_tipo_cliente' => $d->dc_tipo_cliente,
				'dc_orden_compra' => $d->dc_orden_compra,
				'dc_producto' => $d->dc_producto,
				'dc_cebe' => $d->dc_cebe,
				'dc_ceco' => $d->dc_ceco,
				'dc_marca' => $d->dc_marca,
				'dc_linea_negocio' => $d->dc_linea_negocio,
				'dc_tipo_producto' => $d->dc_tipo_producto,
				'dc_guia_recepcion' => $d->dc_guia_recepcion,
				'dc_mercado_cliente' => $d->dc_mercado_cliente,
				'dc_mercado_proveedor' => $d->dc_mercado_proveedor,
				'dc_segmento' => $d->dc_segmento,
				'dc_bodega' => $d->dc_bodega
			));
		}

	}

	//Insertar detalle de IVA para el comprobante de centralización
	$iva = $grouped[1];
	$conf = ContabilidadStuff::getConfiguration();

	$dc_cuenta_contable = $conf->dc_cuenta_contable_iva_credito;

	$detalles_financieros[] = array(
		'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
		'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
		'dg_glosa' => "IVA CRÉDITO Centralización Libro de Compra: ".date('d-m-Y'),
		'dq_debe' => floatval($iva['sum']),
		'dq_haber' => 0
	);

	foreach($iva['detalle'] as $d){

		$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
			'dc_cuenta_contable' => $dc_cuenta_contable,
			'dq_debe' => floatval($d->dq_iva),
			'dq_haber' => 0,
			'dg_glosa' => "IVA CRÉDITO Centralización Libro de Compra: ".date('d-m-Y'),
			'dc_factura_compra' => $d->dc_factura,
			'dc_nota_venta' => $d->dc_nota_venta,
			'dc_orden_servicio' => $d->dc_orden_servicio,
			'dc_proveedor' => $d->dc_proveedor,
			'dc_tipo_proveedor' => $d->dc_tipo_proveedor,
			'dc_cliente' => $d->dc_cliente,
			'dc_tipo_cliente' => $d->dc_tipo_cliente,
			'dc_mercado_cliente' => $d->dc_mercado_cliente,
			'dc_mercado_proveedor' => $d->dc_mercado_proveedor
		));

	}

	//Cargo a cuentas de proveedores
	$proveedor = $grouped[2];
	//echo "<pre>";var_dump($proveedor);echo "</pre>";

	$dq_debe = 0;
	foreach($proveedor as $dc_cuenta_contable => $item){

		$detalles_financieros[] = array(
			'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
			'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
			'dg_glosa' => "Carga Proveedor Centralización Libro de Compra: ".date('d-m-Y'),
			'dq_debe' => 0,
			'dq_haber' => floatval($item['sum'])
		);

		foreach($item['detalle'] as $d){

			$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
				'dc_cuenta_contable' => $dc_cuenta_contable,
				'dq_debe' => 0,
				'dq_haber' => floatval($d->dq_total),
				'dg_glosa' => "Carga Proveedor Centralización Libro de Compra: ".date('d-m-Y'),
				'dc_factura_compra' => $d->dc_factura,
				'dc_nota_venta' => $d->dc_nota_venta,
				'dc_orden_servicio' => $d->dc_orden_servicio,
				'dc_proveedor' => $d->dc_proveedor,
				'dc_tipo_proveedor' => $d->dc_tipo_proveedor,
				'dc_cliente' => $d->dc_cliente,
				'dc_tipo_cliente' => $d->dc_tipo_cliente,
				'dc_mercado_ciente' => $d->dc_mercado_cliente,
				'dc_mercado_proveedor' => $d->dc_mercado_proveedor
			));
		}
	}

	return array($detalles_financieros, $detalles_analiticos);

}
