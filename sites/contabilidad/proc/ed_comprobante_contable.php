<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("tb_comprobante_contable",
"dg_comprobante,dc_tipo_movimiento,df_fecha_contable,dc_tipo_cambio,dq_cambio,dg_glosa,DATE_FORMAT(df_fecha_emision,'%d/%m/%Y') as df_fecha_emision,dg_numero_interno",
"dc_comprobante = '{$_POST['id']}' AND dc_empresa={$empresa} AND dm_anulado = '0'");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el tipo de movimiento especificado, intentelo nuevamente.","sites/contabilidad/cr_comprobante_contable.php");
}
$datos = $datos[0];

//Cuentas contables
$cuentas = $db->select("tb_cuenta_contable c
JOIN tb_cuentas_tipo_movimiento t ON t.dc_cuenta_contable = c.dc_cuenta_contable",
"c.dg_codigo,c.dg_cuenta_contable,c.dg_descripcion_larga,c.dc_cuenta_contable",
"c.dc_empresa = {$empresa} AND t.dc_tipo_movimiento = {$datos['dc_tipo_movimiento']}");

?>
<div id="secc_bar">Creación de comprobante contable</div>
<div id="main_cont">
<div class="panes" style="width:1100px;">
<div class="title center">Editando el comprobante <strong style="color:#000;"><?=$datos['dg_comprobante'] ?></strong></div>
<hr class="clear" />
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Indique los datos para la generación del comprobante contable</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Start("sites/contabilidad/proc/editar_comprobante_contable.php","ed_comprobante_contable","comprobante_form");
	$form->Section();
	$form->Text('Número de comprobante interno','comp_num_interno',0,255,$datos['dg_numero_interno']);
	$form->Date('Fecha contable','comp_fecha',1,$datos['df_fecha_contable']);
	$form->EndSection();
	$form->Section();
	echo("<fieldset><legend>Dejar en blanco para usar moneda local</legend>");
	$form->Listado("Tipo de cambio","comp_tipo_cambio","tb_tipo_cambio",array("dc_tipo_cambio","dg_tipo_cambio","ROUND(dq_cambio,{$empresa_conf['dn_decimales_local']})"),0,$datos['dc_tipo_cambio']);
	$form->Text("Cambio","comp_cambio",0,255,number_format($datos['dq_cambio'],$empresa_conf['dn_decimales_local']));
	echo("</fieldset>");
	$form->EndSection();
	$form->Section();
	echo("<fieldset><legend>Fecha de emisión del comprobante</legend><b>{$datos['df_fecha_emision']}</b></fieldset>");
	$form->Textarea("Glosa general","comp_glosa_general",0,$datos['dg_glosa'],32,3);
	$form->EndSection();
?>
	<br class="clear" />
	<input type="hidden" name="comp_id" value="<?=$_POST['id'] ?>" />
	<input type="hidden" name="comp_saldo" value="" />
	<fieldset>
	<legend>Detalle del comprobante</legend>
	<div id="detalle">
	<table width="100%" class="tab">
		<thead id="detalle_head">
			<tr>
				<th width="8%">Opciones</th>
				<th width="1%">Código cuenta</th>
				<th>Descripción</th>
				<th width="1%">DEBE</th>
				<th width="1%">HABER</th>
				<th width="1%">Glosa</th>
				<th width="23%">Documento</th>
			</tr>
		</thead>
		<tbody id="detalle_list">
<?php
	$detalles = $db->select("(SELECT * FROM tb_comprobante_contable_detalle WHERE dc_comprobante = {$_POST['id']}) d
	JOIN tb_cuenta_contable c ON d.dc_cuenta_contable=c.dc_cuenta_contable",
	'c.dg_codigo,c.dg_cuenta_contable,c.dc_cuenta_contable,d.dq_debe,d.dq_haber,d.dg_glosa,d.dc_tipo_documento,d.dg_numero_documento,
	d.dg_cheque,d.dg_rut,d.dg_activo_fijo,d.dc_banco,d.dc_cebe,d.dc_ceco,d.dg_doc_venta,d.dg_doc_compra','',
	array('order_by' => "dc_detalle ASC"));
	
	$tipos_doc = $db->select("tb_tipo_documento","dc_tipo_documento,dg_tipo_documento","dc_empresa = {$empresa} AND dm_activo='1'");
	$bancos = $db->select("tb_banco","dc_banco,dg_banco","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by" => "dg_banco"));
	$cebes = $db->select("tb_cebe","dc_cebe,dg_cebe","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by" => "dg_cebe"));
	$cecos = $db->select("tb_ceco","dc_ceco,dg_ceco","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by" => "dg_ceco"));
	foreach($detalles as $d){
	$debe = number_format($d['dq_debe']/$datos['dq_cambio'],2);
	$haber = number_format($d['dq_haber']/$datos['dq_cambio'],2);
?>
			<tr>
				<td>
				<img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" />
				<img src="images/addbtn.png" alt="+" title="Más detalles" class="show_more" />
				<img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
				</td>
				<td>
				<input type="text" class="det_cta inputtext" size="10" required="required" value="<?=$d['dg_codigo'] ?>" />
				<input type="hidden" name="comp_cuentas[]" value="<?=$d['dc_cuenta_contable'] ?>" />
				</td>
				<td><?=$d['dg_cuenta_contable'] ?></td>
				<td>
					<input type="text" name="comp_debe[]" class="det_debe inputtext" size="10" value="<?=$debe ?>" required="required" style="text-align:right;" />
				</td>
				<td>
					<input type="text" name="comp_haber[]" class="det_haber inputtext" size="10" value="<?=$haber ?>" required="required" style="text-align:right;" />
				</td>
				<td><input type="text" name="comp_glosa[]" class="det_glosa inputtext" size="25" value="<?=$d['dg_glosa'] ?>" required="required" /></td>
				<td>
				<select name="comp_tipo_doc[]" class="inputtext" style="width:100px;">
					<option value="0">Tipo</option>
				<?php
					foreach($tipos_doc as $t){
						$selected = $d['dc_tipo_documento']==$t['dc_tipo_documento']?"selected=selected":'';
						echo("<option value='{$t['dc_tipo_documento']}' {$selected}>{$t['dg_tipo_documento']}</option>");
					}
				?>
				</select>
				Nº<input type="text" name="comp_num_doc[]" size="8" class="inputtext" value="<?=$d['dg_numero_documento'] ?>" />
				</td>
			</tr>
			<tr style="display:none;">
				<td colspan="7" align="center" style="background-color:#E3E3E3;">
				<div class="left">Nº cheque<br /><input type="text" name="comp_cheque[]" class="inputtext" size="13" value="<?=$d['dg_cheque'] ?>" /></div>
				<div class="left">RUT<br /><input type="text" name="comp_rut[]" class="inputtext" size="13" value="<?=$d['dg_rut'] ?>" /></div>
				<div class="left">Activo fijo<br /><input type="text" name="comp_activo[]" class="inputtext" size="13" value="<?=$d['dg_activo_fijo'] ?>" /></div>
				<div class="left">Banco<br />
				<select name="comp_banco[]" class="inputtext" style="width:140px;">
				<option value="0"></option>
				<?php
					foreach($bancos as $b){
						$selected = $d['dc_banco']==$b['dc_banco']?"selected='selected'":'';
						echo("<option value='{$b['dc_banco']}' {$selected}>{$b['dg_banco']}</option>");
					}
				?>
				</select>
				</div>
				<div class="left">Cebe<br />
				<select name="comp_cebe[]" class="inputtext" style="width:140px;">
				<option value="0"></option>
				<?php
					foreach($cebes as $c){
						$selected = $d['dc_cebe']==$c['dc_cebe']?"selected='selected'":'';
						echo("<option value='{$c['dc_cebe']}' {$selected}>{$c['dg_cebe']}</option>");
					}
				?>
				</select>
				</div>
				<div class="left">Ceco<br />
				<select name="comp_ceco[]" class="inputtext" style="width:140px;">
				<option value="0"></option>
				<?php
					foreach($cecos as $c){
						$selected = $d['dc_ceco']==$c['dc_ceco']?"selected='selected'":'';
						echo("<option value='{$c['dc_ceco']}' {$selected}>{$c['dg_ceco']}</option>");
					}
				?>
				</select>
				</div>
				<div class="left">Documento de venta<br /><input type="text" name="comp_venta[]" value="<?=$d['dg_doc_venta'] ?>" class="inputtext" size="13" /></div>
				<div class="left">Documento de compra<br /><input type="text" name="comp_compra[]" value="<?=$d['dg_doc_compra'] ?>" class="inputtext" size="13" /></div>
				</td>
			</tr>
<?php
	}
?>
		</tbody>
		<tfoot id="detalle_foot">
			<tr>
				<th style="background:#DDD;" colspan="3">&nbsp;</td>
				<th style="background:#DDD;">Total DEBE</td>
				<th style="background:#DDD;">Total HABER</td>
				<th style="background:#DDD;">Diferencia Total DEBE-HABER</td>
				<th style="background:#DDD;"></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td align="right" class="total_debe">0</td>
				<td align="right" class="total_haber">0</td>
				<td align="right" class="diferencia_dh">0</td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
	</table>
		
	<div class="center">
		<input type="button" class="addbtn" id="detalle_add" value="Agregar otro detalle" />
	</div>
		
		</div>
		
		</fieldset>
		
		<div class="center">
			<input type="submit" class="editbtn" value="Editar" />
		</div>
		
		</fieldset>
		</form>
	</div>
</div>
<script type="text/javascript">
	var cuentas = <?=json_encode($cuentas) ?>
</script>
<script type="text/javascript" src="jscripts/comprobante_contable.js"></script>
<script type="text/javascript">
	refresh_totals();
	$(".det_debe[value!=0.00]").trigger('change');
	$(".det_haber[value!=0.00]").trigger('change');
</script>