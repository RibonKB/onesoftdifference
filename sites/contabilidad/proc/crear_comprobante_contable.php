<?php
/**
*	Creación de comprobantes contables
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$mes_contable = substr($_POST['comp_fecha'],3,2);
$anho_contable = substr($_POST['comp_fecha'],6);


$val = $db->select(
"tb_mes_contable m
JOIN tb_periodo_contable p ON m.dc_mes_contable = p.dc_mes_contable",
"p.dm_filtro,p.dg_permisos,p.dm_estado",
"m.dc_mes = {$mes_contable} AND
dc_anho = {$anho_contable} AND
m.dc_empresa = {$empresa} AND p.dm_activo = '1'");

if(!count($val)){
	$error_man->showWarning("No existe el periodo en el que intenta crear el comprobante");
	exit();
}
$val = $val[0];
if($val['dm_filtro'] == '1'){
	$gr = substr($val['dg_permisos'],0,-1);
	if(!count($db->select("tb_usuario_grupo","1","dc_usuario = {$idUsuario} AND dc_grupo IN ({$gr})"))){
		$error_man->showWarning("Su grupo de usuario no tiene permiso de generar comprobantes en este periodo, consulte con el administrador del sistema");
		exit();
	}
}else if($val['dm_filtro'] == '2'){
	if(strpos($val['dg_permisos'],"{$idUsuario},") === false){
		$error_man->showWarning("No tiene permisos para generar comprobantes en este periodo, consulte con el administrador del sistema");
		exit();
	}
}
if($val['dm_estado'] == '0'){
	$error_man->showWarning("El periodo contable seleccionado está cerrado.");
	exit();
}

/*if($_POST['comp_num_interno']){
	if(count($db->select('tb_comprobante_contable','1',"dm_anulado = 0 AND dc_empresa={$empresa} AND dg_numero_interno='{$_POST['comp_num_interno']}' AND MONTH(df_fecha_contable) = MONTH(".$db->sqlDate($_POST['comp_fecha']).")"))){
		$error_man->showWarning('Ya se ha ingresado un comprobante contable con el número interno especificado en el periodo contable especificado');
		exit();
	}
}*/

$db->start_transaction();

$comp_num = $db->select('tb_comprobante_contable','dg_comprobante',
"dc_empresa={$empresa}",array('order_by' => 'df_fecha_emision DESC', 'limit' => 1));


if(count($comp_num) && substr($comp_num[0]['dg_comprobante'],0,6) == date("Ym")){
	$correlativo = date("Ym").((int)substr($comp_num[0]['dg_comprobante'],6)+1);
}else{
	$correlativo = date("Ym").'0';
}
$cambio = $_POST['comp_tipo_cambio']?$_POST['comp_cambio']:1;

$comprobante = $db->insert("tb_comprobante_contable",
array(
	"dg_comprobante" => $correlativo,
	"dc_tipo_movimiento" => $_POST['tipo_mov_id'],
	"df_fecha_contable" => $db->sqlDate($_POST['comp_fecha']),
	"dc_mes_contable" => $mes_contable,
	"dc_anho_contable" => $anho_contable,
	"dc_tipo_cambio" => $_POST['comp_tipo_cambio'],
	"dq_cambio" => $cambio,
	"dg_glosa" => $_POST['comp_glosa_general'],
	"dq_saldo" => str_replace(',','',$_POST['comp_saldo'])*$cambio,
	"df_fecha_emision" => "NOW()",
	"dg_numero_interno" => $_POST['comp_num_interno'],
	"dc_empresa" => $empresa,
	"dc_usuario_creacion" => $idUsuario
));

if($empresa_conf['dm_contabilidad_analitica'] == '1'){
	$ca_comprobante = $db->insert("tb_comprobante_contable_analitica",
	array(
		"dg_comprobante" => $correlativo,
		"dc_tipo_movimiento" => $_POST['tipo_mov_id'],
		"df_fecha_contable" => $db->sqlDate($_POST['comp_fecha']),
		"dc_mes_contable" => $mes_contable,
		"dc_anho_contable" => $anho_contable,
		"dc_tipo_cambio" => $_POST['comp_tipo_cambio'],
		"dq_cambio" => $cambio,
		"dg_glosa" => $_POST['comp_glosa_general'],
		"dq_saldo" => str_replace(',','',$_POST['comp_saldo'])*$cambio,
		"dg_numero_interno" => $_POST['comp_num_interno'],
		"df_fecha_emision" => "NOW()",
		"dc_empresa" => $empresa,
		"dc_usuario_creacion" => $idUsuario
	));
}

$error_man->showConfirm("Se ha generado el comprobante contable");
echo("<div class='title'>El número de comprobante es <h1 style='margin:0;color:#000;'>{$correlativo}</h1>
<a href='sites/contabilidad/ver_comprobante.php?id={$comprobante}' target='_blank' class='button'>HAGA CLICK ACÁ PARA IMPRIMIR EL COMPROBANTE</a></div>");

foreach($_POST['comp_cuentas'] as $i => $v){
	$db->insert("tb_comprobante_contable_detalle",
	array(
		"dc_comprobante" => $comprobante,
		"dc_cuenta_contable" => $v,
		"dq_debe" => str_replace(',','',$_POST['comp_debe'][$i])*$cambio,
		"dq_haber" => str_replace(',','',$_POST['comp_haber'][$i])*$cambio,
		"dg_glosa" => $_POST['comp_glosa'][$i],
		"dc_tipo_documento" => $_POST['comp_tipo_doc'][$i],
		"dg_numero_documento" => $_POST['comp_num_doc'][$i],
		"dg_cheque" => $_POST['comp_cheque'][$i],
		"df_fecha_cheque" => $db->sqlDate($_POST['comp_fecha_cheque'][$i]),
		"dg_rut" => $_POST['comp_rut'][$i],
		"dg_activo_fijo" => $_POST['comp_activo'][$i],
		"dc_banco" => $_POST['comp_banco'][$i],
		"dc_cebe" => $_POST['comp_cebe'][$i],
		"dc_ceco" => $_POST['comp_ceco'][$i],
		"dg_doc_venta" => $_POST['comp_venta'][$i],
		"dg_doc_compra" => $_POST['comp_compra'][$i]
	));
	
	if($empresa_conf['dm_contabilidad_analitica'] == '1'){
		$db->insert("tb_comprobante_contable_detalle_analitica",
		array(
			"dc_comprobante" => $comprobante,
			"dc_cuenta_contable" => $v,
			"dq_debe" => str_replace(',','',$_POST['comp_debe'][$i])*$cambio,
			"dq_haber" => str_replace(',','',$_POST['comp_haber'][$i])*$cambio,
			"dg_glosa" => $_POST['comp_glosa'][$i],
			"dc_tipo_documento" => $_POST['comp_tipo_doc'][$i],
			"dg_numero_documento" => $_POST['comp_num_doc'][$i],
			"dg_cheque" => $_POST['comp_cheque'][$i],
			"dg_rut" => $_POST['comp_rut'][$i],
			"dg_activo_fijo" => $_POST['comp_activo'][$i],
			"dc_banco" => $_POST['comp_banco'][$i],
			"dc_cebe" => $_POST['comp_cebe'][$i],
			"dc_ceco" => $_POST['comp_ceco'][$i],
			"dg_doc_venta" => $_POST['comp_venta'][$i],
			"dg_doc_compra" => $_POST['comp_compra'][$i]
		));
	}
}
$db->commit();

$error_man->showConfirm("Fueron agregados ".count($_POST['comp_cuentas'])." elementos al detalle correctamente.");

?>