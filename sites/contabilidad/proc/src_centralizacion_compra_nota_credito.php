<?php

require_once('../../../inc/db-class.php');

function obtenerDataNotaCredito($param, DBConnector $db){
	global $empresa, $error_man;

	$nc = array();
	if(isset($param['dc_documento']['NC'])){

		$dc_nota_credito = explode(',',implode(',',$param['dc_documento']['NC']));
		$questions = substr(str_repeat(',?',count($dc_nota_credito)),1);

		$notas = $db->prepare(
					$db->select('tb_nota_credito_proveedor nc
								 JOIN tb_proveedor p ON p.dc_proveedor = nc.dc_proveedor
								 JOIN tb_tipo_nota_credito_proveedor t ON t.dc_tipo = nc.dc_tipo',
								'nc.dc_nota_credito, nc.dq_folio, nc.dq_nota_credito, nc.dq_total, nc.dq_neto, nc.dq_iva, nc.dc_proveedor,
								 p.dc_cuenta_contable dc_cuenta_proveedor, p.dc_tipo_proveedor, p.dc_mercado dc_mercado_proveedor,
								 t.dm_retorna_stock, t.dm_multiple_factura, t.dc_cuenta_contable_centralizacion',
								"nc.dc_empresa = {$empresa} AND nc.dm_centralizada = 0 AND nc.dc_nota_credito IN ({$questions})"));
		foreach($dc_nota_credito as $i => $n){
			$notas->bindValue($i+1,$n,PDO::PARAM_INT);
		}
		$db->stExec($notas);

		//Agrupar en el array de notas de crédito las nc encontradas
		$dc_nc_get = array();
		while($n = $notas->fetch(PDO::FETCH_OBJ)):
			$nc[$n->dc_nota_credito] = array($n, array(), array());
			$dc_nc_get[] = $n->dc_nota_credito;
		endwhile;
		unset($notas);

		//Comprobar que la cantidad de notas de crédito enviadas desde el formulario sea la misma obtenida desde la base de datos
		$diferencias = valida_cantidad_erronea_nota_credito($dc_nota_credito,$dc_nc_get);
		if(count($diferencias)){
			return array('ERROR_ITEM_NOT_FOUND',$diferencias);
		}

		/*$detalles = $db->prepare($db->select("tb_nota_credito_proveedor_detalle d
									LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
									LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto
									LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio",
									'd.dq_cantidad, d.dq_precio, d.dc_nota_credito, p.dc_producto, tp.dc_tipo_producto, ln.dc_linea_negocio'));*/

		$detalles = $db->prepare($db->select("tb_nota_credito_proveedor_detalle d
									LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto AND p.dc_empresa = {$empresa}
									LEFT JOIN tb_tipo_producto tp ON tp.dc_tipo_producto = p.dc_tipo_producto
									LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio = p.dc_linea_negocio
									LEFT JOIN tb_guia_despacho_proveedor gd ON gd.dc_nota_credito = d.dc_nota_credito
									LEFT JOIN tb_guia_despacho_proveedor_detalle gdd ON gd.dc_guia_despacho = gdd.dc_guia_despacho AND d.dg_producto = gdd.dg_producto
									LEFT JOIN tb_linea_negocio_cuenta_contable lnc ON lnc.dc_linea_negocio = p.dc_linea_negocio AND lnc.dc_bodega = gdd.dc_bodega_salida",
								'd.dq_cantidad dc_cantidad_detalle, d.dq_precio, d.dc_nota_credito, p.dc_producto, tp.dc_tipo_producto, ln.dc_linea_negocio,
								 gdd.dq_cantidad dc_cantidad_despacho, ln.dc_cuenta_contable_servicio, lnc.dc_cuenta_contable dc_cuenta_contable_producto,
								 gdd.dc_bodega_salida dc_bodega, tp.dm_controla_inventario, p.dc_marca, d.dc_detalle',
								 "d.dc_nota_credito IN ({$questions}) AND (gd.dc_guia_despacho IS NULL OR (gd.dc_guia_despacho IS NOT NULL AND gdd.dc_detalle IS NOT NULL))"));
		foreach($dc_nota_credito as $i => $n){
			$detalles->bindValue($i+1,$n,PDO::PARAM_INT);
		}
		$db->stExec($detalles);

		//Agrupar los detalles de la factura en el array de agrupaciones
		while($d = $detalles->fetch(PDO::FETCH_OBJ)):
			//$d->dc_cliente = $fc[$d->dc_factura][0]->dc_cliente;
			//$d->dc_tipo_cliente = $fc[$d->dc_factura][0]->dc_tipo_cliente;
			//$d->dc_mercado_cliente = $fc[$d->dc_factura][0]->dc_mercado_cliente;
			$d->dc_proveedor = $nc[$d->dc_nota_credito][0]->dc_proveedor;
			$d->dc_tipo_proveedor = $nc[$d->dc_nota_credito][0]->dc_tipo_proveedor;
			$d->dc_mercado_proveedor = $nc[$d->dc_nota_credito][0]->dc_mercado_proveedor;

			$nc[$d->dc_nota_credito][$d->dm_controla_inventario+1][$d->dc_detalle][] = $d;
		endwhile;

		return array('OK',$nc);

	}else{
		return array('OK',array());
	}
}

//Funciones de validación de datos
function validarNotaCreditoCreacion($nc, DBConnector $db){
	global $empresa, $empresa_conf;

	//Validar para todas las notas de crédito con despachos que esas hayan sido completamente devueltos
	foreach($nc as $dc_nota_credito => $data){
		if(count($data[1]) && $data[0]->dm_retorna_stock == 1){
			$error = validaStockDespachado($data[1]);
			if($error !== true){
				return array('ERROR_NO_DESPACHADO',array($error, $data[0]));
			}
		}
	}

	//Validar que los valores de los detalles cuadren con los totales de la cabecera
	foreach($nc as $dc_nota_credito => $data){
		$dq_total = round(floatval($data[0]->dq_total),$empresa_conf['dn_decimales_local']);;
		$dq_iva = round(floatval($data[0]->dq_iva),$empresa_conf['dn_decimales_local']);;
		$dq_total_productos = round(sumaDetalleNC($data[1]),$empresa_conf['dn_decimales_local']);;
		$dq_total_servicios = round(sumaDetalleNC($data[2]),$empresa_conf['dn_decimales_local']);;

		if($dq_total != ($dq_iva+$dq_total_productos+$dq_total_servicios)){
			return array('ERROR_NO_CUADRA',array($data[0],$dq_total_productos, $dq_total_servicios));
		}

	}

	//Validar cuenta contable disponible para todas las lineas de negocio en los detalles
	foreach($nc as $dc_nota_credito => $data){

		if($data[0]->dm_retorna_stock == 0){
			if($data[0]->dc_cuenta_contable_centralizacion == 0){
				return array('ERROR_ADMINISTRATIVA_CUENTA_CONTABLE',array($data[0]));
			}
			continue;
		}

		if(count($data[1])){
			foreach($data[1] as $dc_detalle => $data_producto){
				foreach($data_producto as $detalle){
					if($detalle->dc_cuenta_contable_producto == 0){
						return array('ERROR_PRODUCTO_CUENTA_CONTABLE',array($data[0], $detalle));
					}
				}
			}
		}

		if(count($data[2])){
			foreach($data[2] as $detalle){
				if($detalle[0]->dc_cuenta_contable_servicio == 0){
					return array('ERROR_SERVICIO_CUENTA_CONTABLE',array($data[0], $detalle[0]));
				}
			}
		}
	}

	$conf = ContabilidadStuff::getConfiguration();

	if($conf->dc_cuenta_contable_iva_credito == 0){
		return array('ERROR_IVA_CUENTA_CONTABLE',array());
	}

	return true;

}

function validaStockDespachado($detalle){
	foreach($detalle as $dc_detalle => $data){
		$suma = 0;
		$total = 0;
		foreach($data as $instance){
			$total = $instance->dc_cantidad_detalle;
			$suma += $instance->dc_cantidad_despacho;
			$last = $instance;
		}
		if($suma != $total){
			//Aquí tengo que verlo xD!
			return $last;
		}
	}

	return true;
}

function sumaDetalleNC($detalle){
	$suma = 0;
	foreach($detalle as $dc_detalle => $data){
		$suma += floatval($data[0]->dc_cantidad_detalle * $data[0]->dq_precio);
	}
	return floatval($suma);
}

function valida_cantidad_erronea_nota_credito($dc_nota_credito, $notas){
	return array_diff($dc_nota_credito,$notas);
}

function agruparDetallesNC($data){
	$ln = array();
	$proveedor = array();
	$iva = array('sum' => 0, 'detalle' => array());

	foreach($data as $dc_nota_credito => $nota_credito){
		//agrupar los proveedores
		if(!isset($proveedor[$nota_credito[0]->dc_cuenta_proveedor])){
			$proveedor[$nota_credito[0]->dc_cuenta_proveedor] = array('sum' => 0, 'detalle' => array());
		}
		$proveedor[$nota_credito[0]->dc_cuenta_proveedor]['sum'] += $nota_credito[0]->dq_total;
		$proveedor[$nota_credito[0]->dc_cuenta_proveedor]['detalle'][] = $nota_credito[0];

		$iva['sum'] += $nota_credito[0]->dq_iva;
		$iva['detalle'][] = $nota_credito[0];

		//Detectar notas de crédito administrativas
		if($nota_credito[0]->dm_retorna_stock == 0){

			continue;
		}

		//Agrupar los detalles de productos que mueven stock
		if(count($nota_credito[1])){
			foreach($nota_credito[1] as $dc_detalle => $detalle):
				foreach($detalle as $producto):
					//Detectar notas de crédito administrativas
					//A este if no debería entrar nunca ya que después se configurará para que las notas de crédito administrativas no soporten productos que mueven stock
					if($nota_credito[0]->dm_retorna_stock == 0){
						$producto->dc_cuenta_contable_producto = $nota_credito[0]->dc_cuenta_contable_centralizacion;
						$producto->dc_cantidad_recepcion = $producto->dc_cantidad_detalle;
					}
					if(!isset($ln[$producto->dc_cuenta_contable_producto])){
						$ln[$producto->dc_cuenta_contable_producto] = array('sum' => 0, 'detalle' => array());
					}
					$ln[$producto->dc_cuenta_contable_producto]['sum'] += $producto->dq_precio*$producto->dc_cantidad_recepcion;
					$ln[$producto->dc_cuenta_contable_producto]['detalle'][] = $producto;
				endforeach;
			endforeach;
		}

		//Agrupar los detalles de servicios
		if(count($nota_credito[2])){
			foreach($nota_credito[1] as $dc_detalle => $servicio):
				//Detectar notas de crédito administrativas
				if($nota_credito[0]->dm_retorna_stock == 0)
					$producto->dc_cuenta_contable_servicio = $nota_credito[0]->dc_cuenta_contable_centralizacion;
				if(!isset($ln[$servicio->dc_cuenta_contable_servicio])){
					$ln[$servicio->dc_cuenta_contable_servicio] = array('sum' => 0, 'detalle' => array());
				}
				$ln[$servicio->dc_cuenta_contable_servicio]['sum'] += $servicio->dq_precio*$servicio->dc_cantidad_nota_credito;
				$ln[$servicio->dc_cuenta_contable_servicio]['detalle'][] = $servicio;
			endforeach;
		}
	}

	return array($ln,$iva,$proveedor);
}

function insertarDetallesNC($data, $dc_comprobante, DBConnector $db){
	global $empresa;

	$grouped = agruparDetallesNC($data);
	//debug($grouped);return;

	//Insertar el asiento de gastos cobros o perdida
	$insertar_gastos = $db->prepare($db->insert('tb_comprobante_contable_detalle',array(
		'dc_comprobante' => '?',
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?'
	)));
	$insertar_gastos->bindValue(1,$dc_comprobante,PDO::PARAM_INT);
	$insertar_gastos->bindParam(2,$dc_cuenta_contable,PDO::PARAM_INT);
	$insertar_gastos->bindParam(3,$dq_debe,PDO::PARAM_STR);
	$insertar_gastos->bindParam(4,$dq_haber,PDO::PARAM_STR);
	$insertar_gastos->bindParam(5,$dg_glosa,PDO::PARAM_STR);

	//Insertar el detalle analítico de gastos, cobros o perdida
	$insertar_analitico = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico',array(
		'dc_cuenta_contable' => '?',
		'dq_debe' => '?',
		'dq_haber' => '?',
		'dg_glosa' => '?',
		'dc_detalle_financiero' => '?',
		'dc_factura_compra' => '?',
		'dc_nota_venta' => '?',
		'dc_orden_servicio' => '?',
		'dc_proveedor' => '?',
		'dc_tipo_proveedor' => '?',
		'dc_cliente' => '?',
		'dc_tipo_cliente' => '?',
		'dc_orden_compra' => '?',
		'dc_producto' => '?',
		'dc_cebe' => '?',
		'dc_ceco' => '?',
		'dc_marca' => '?',
		'dc_linea_negocio' => '?',
		'dc_tipo_producto' => '?',
		'dc_guia_recepcion' => '?',
		'dc_mercado_cliente' => '?',
		'dc_mercado_proveedor' => '?',
		'dc_nota_credito_proveedor' => '?',
		'dc_segmento' => '?',
		'dc_bodega' => '?'
	)));
	$insertar_analitico->bindValue( 1,$dc_cuenta_contable,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 2,$dq_debe_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 3,$dq_haber_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 4,$dg_glosa_analitico,PDO::PARAM_STR);
	$insertar_analitico->bindParam( 5,$dc_detalle_financiero,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 6,$dc_factura,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 7,$dc_nota_venta,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 8,$dc_orden_servicio,PDO::PARAM_INT);
	$insertar_analitico->bindParam( 9,$dc_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam(10,$dc_tipo_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam(11,$dc_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam(12,$dc_tipo_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam(13,$dc_orden_compra,PDO::PARAM_INT);
	$insertar_analitico->bindParam(14,$dc_producto,PDO::PARAM_INT);
	$insertar_analitico->bindParam(15,$dc_cebe,PDO::PARAM_INT);
	$insertar_analitico->bindParam(16,$dc_ceco,PDO::PARAM_INT);
	$insertar_analitico->bindParam(17,$dc_marca,PDO::PARAM_INT);
	$insertar_analitico->bindParam(18,$dc_linea_negocio,PDO::PARAM_INT);
	$insertar_analitico->bindParam(19,$dc_tipo_producto,PDO::PARAM_INT);
	$insertar_analitico->bindParam(20,$dc_guia_recepcion,PDO::PARAM_INT);
	$insertar_analitico->bindParam(21,$dc_mercado_cliente,PDO::PARAM_INT);
	$insertar_analitico->bindParam(22,$dc_mercado_proveedor,PDO::PARAM_INT);
	$insertar_analitico->bindParam(23,$dc_nota_credito,PDO::PARAM_INT);
	$insertar_analitico->bindParam(24,$dc_segmento,PDO::PARAM_INT);
	$insertar_analitico->bindParam(25,$dc_bodega,PDO::PARAM_INT);

	$ln = $grouped[0];
	$dq_debe = 0;
	foreach($ln as $dc_cuenta_contable => $item){
		$dq_haber = $item['sum'];
		$dg_glosa = "Centralización Libro de Compra: ".date('d-m-Y');
		$db->stExec($insertar_gastos);

		$dc_detalle_financiero = $db->lastInsertId();
		$dq_debe_analitico = 0;

		foreach($item['detalle'] as $d){
			if($d->dm_controla_inventario == 0)
				$dq_haber_analitico = $d->dq_precio*$d->dc_cantidad_despacho;
			else
				$dq_haber_analitico = $d->dq_precio*$d->dc_cantidad_detalle;

			$dg_glosa_analitico = '';
			$dc_factura = NULL;//$d->dc_factura;
			$dc_nota_venta = NULL;//$d->dc_nota_venta;
			$dc_orden_servicio = NULL;//$d->dc_orden_servicio;
			$dc_proveedor = $d->dc_proveedor;
			$dc_tipo_proveedor = $d->dc_tipo_proveedor;
			$dc_cliente = NULL;//$d->dc_cliente;
			$dc_tipo_cliente = NULL;//$d->dc_tipo_cliente;
			$dc_orden_compra = NULL;//$d->dc_orden_compra;
			$dc_producto = $d->dc_producto;
			$dc_cebe = NULL;//$d->dc_cebe;
			$dc_ceco = NULL;//$d->dc_ceco;
			$dc_marca = $d->dc_marca;
			$dc_linea_negocio = $d->dc_linea_negocio;
			$dc_tipo_producto = $d->dc_tipo_producto;
			$dc_guia_recepcion = NULL;//$d->dc_guia_recepcion;
			$dc_mercado_cliente = NULL;//$d->dc_mercado_cliente;
			$dc_mercado_proveedor = $d->dc_mercado_proveedor;
			$dc_nota_credito = $d->dc_nota_credito;
			$dc_segmento = NULL;//$d->dc_segmento;
			$dc_bodega = $d->dc_bodega;

			$db->stExec($insertar_analitico);
		}

	}

	//Insertar detalle de IVA para el comprobante de centralización
	$iva = $grouped[1];
	$conf = ContabilidadStuff::getConfiguration();

	$dc_cuenta_contable = $conf->dc_cuenta_contable_iva_credito;

	$dq_debe = 0;
	$dq_haber = floatval($iva['sum']);
	$dg_glosa = "IVA CRÉDITO Centralización Libro de Compra: ".date('d-m-Y');

	$db->stExec($insertar_gastos);

	$dc_detalle_financiero = $db->lastInsertId();
	$dq_debe_analitico = 0;
	$dg_glosa_analitico = '';
	$dc_orden_compra = NULL;
	$dc_producto = NULL;
	$dc_cebe = NULL;
	$dc_ceco = NULL;
	$dc_marca = NULL;
	$dc_linea_negocio = NULL;
	$dc_tipo_producto = NULL;
	$dc_guia_recepcion = NULL;
	$dc_factura = NULL;
	$dc_segmento = NULL;
	$dc_bodega = NULL;

	foreach($iva['detalle'] as $d){
		$dq_haber_analitico = floatval($d->dq_iva);
		$dc_nota_credito = $d->dc_nota_credito;
		$dc_nota_venta = NULL;//$d->dc_nota_venta;
		$dc_orden_servicio = NULL;//$d->dc_orden_servicio;
		$dc_proveedor = $d->dc_proveedor;
		$dc_tipo_proveedor = $d->dc_tipo_proveedor;
		$dc_cliente = NULL;//$d->dc_cliente;
		$dc_tipo_cliente = NULL;//$d->dc_tipo_cliente;
		$dc_mercado_cliente = NULL;//$d->dc_mercado_cliente;
		$dc_mercado_proveedor = $d->dc_mercado_proveedor;

		$db->stExec($insertar_analitico);
	}

	//Cargo a cuentas de proveedores
	$proveedor = $grouped[2];
	//echo "<pre>";var_dump($proveedor);echo "</pre>";

	$dq_haber = 0;
	foreach($proveedor as $dc_cuenta_contable => $item){
		$dq_debe = $item['sum'];
		$dg_glosa = "Carga Proveedor Centralización Libro de Compra: ".date('d-m-Y');

		$db->stExec($insertar_gastos);

		$dc_detalle_financiero = $db->lastInsertId();
		$dq_haber_analitico = 0;
		$dg_glosa_analitico = '';
		$dc_orden_compra = NULL;
		$dc_producto = NULL;
		$dc_cebe = NULL;
		$dc_ceco = NULL;
		$dc_marca = NULL;
		$dc_linea_negocio = NULL;
		$dc_tipo_producto = NULL;
		$dc_guia_recepcion = NULL;
		$dc_factura = NULL;
		$dc_segmento = NULL;
		$dc_bodega = NULL;

		foreach($item['detalle'] as $d){
			$dq_debe_analitico = floatval($d->dq_total);
			$dc_nota_credito = $d->dc_nota_credito;
			$dc_nota_venta = NULL;//$d->dc_nota_venta;
			$dc_orden_servicio = NULL;//$d->dc_orden_servicio;
			$dc_proveedor = $d->dc_proveedor;
			$dc_tipo_proveedor = $d->dc_tipo_proveedor;
			$dc_cliente = NULL;//$d->dc_cliente;
			$dc_tipo_cliente = NULL;//$d->dc_tipo_cliente;
			$dc_mercado_cliente = NULL;//$d->dc_mercado_cliente;
			$dc_mercado_proveedor = $d->dc_mercado_proveedor;

			$db->stExec($insertar_analitico);
		}
	}

}

function desmarcarNotasCredito($doc, DBConnector $db){

	if(!isset($doc['NC'])){
		return;
	}

	$dc_nota_credito = explode(',',implode(',',$doc['NC']));
	$questions = substr(str_repeat(',?',count($dc_nota_credito)),1);

	$update_nc = $db->prepare($db->update('tb_nota_credito_proveedor',array(
		'dm_centralizada' => 1
	),"dc_nota_credito IN ({$questions})"));

	foreach($dc_nota_credito as $i => $nc ){
		$update_nc->bindValue($i+1,$nc,PDO::PARAM_INT);
	}

	$db->stExec($update_nc);
}

function obtenerAsientosNC($data, DBConnector $db){
	global $empresa;

	$grouped = agruparDetalles($data);
	//debug($grouped);return;

	$detalles_financieros = array();
	$detalles_analiticos = array();

	$ln = $grouped[0];
	$dq_debe = 0;
	foreach($ln as $dc_cuenta_contable => $item){

		$detalles_financieros[] = array(
			'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
			'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
			'dg_glosa' => "Centralización Libro de Compra: ".date('d-m-Y'),
			'dq_debe' => 0,
			'dq_haber' => $item['sum']
		);

		foreach($item['detalle'] as $d){
			if($d->dm_controla_inventario == 0)
				$dq_haber_analitico = $d->dq_precio*$d->dc_cantidad_despacho;
			else
				$dq_haber_analitico = $d->dq_precio*$d->dc_cantidad_detalle;

			$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
				'dc_cuenta_contable' => $dc_cuenta_contable,
				'dq_debe' => 0,
				'dq_haber' => $dq_haber_analitico,
				'dg_glosa' => "Centralización Libro de Compra: ".date('d-m-Y'),
				'dc_proveedor' => $d->dc_tipo_proveedor,
				'dc_tipo_proveedor' => $d->dc_tipo_proveedor,
				'dc_producto' => $d->dc_producto,
				'dc_marca' => $d->dc_marca,
				'dc_linea_negocio' => $d->dc_linea_negocio,
				'dc_tipo_producto' => $d->dc_tipo_producto,
				'dc_mercado_proveedor' => $d->dc_mercado_proveedor,
				'dc_nota_credito_proveedor' => $d->dc_nota_credito,
				'dc_bodega' => $d->dc_bodega
			));
		}

	}

	//Insertar detalle de IVA para el comprobante de centralización
	$iva = $grouped[1];
	$conf = ContabilidadStuff::getConfiguration();

	$dc_cuenta_contable = $conf->dc_cuenta_contable_iva_credito;

	$detalles_financieros[] = array(
		'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
		'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
		'dg_glosa' => "IVA CRÉDITO Centralización Libro de Compra: ".date('d-m-Y'),
		'dq_debe' => 0,
		'dq_haber' => floatval($iva['sum'])
	);

	$dc_detalle_financiero = $db->lastInsertId();
	$dq_debe_analitico = 0;
	$dg_glosa_analitico = '';
	$dc_orden_compra = NULL;
	$dc_producto = NULL;
	$dc_cebe = NULL;
	$dc_ceco = NULL;
	$dc_marca = NULL;
	$dc_linea_negocio = NULL;
	$dc_tipo_producto = NULL;
	$dc_guia_recepcion = NULL;
	$dc_factura = NULL;
	$dc_segmento = NULL;
	$dc_bodega = NULL;

	foreach($iva['detalle'] as $d){

		$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
			'dc_cuenta_contable' => $dc_cuenta_contable,
			'dq_debe' => 0,
			'dq_haber' => floatval($d->dq_iva),
			'dg_glosa' => "IVA CRÉDITO Centralización Libro de Compra: ".date('d-m-Y'),
			'dc_nota_credito_proveedor' => $d->dc_nota_credito,
			'dc_proveedor' => $d->dc_proveedor,
			'dc_tipo_proveedor' => $d->dc_tipo_proveedor,
			'dc_mercado_proveedor' => $d->dc_mercado_proveedor
		));
	}

	//Cargo a cuentas de proveedores
	$proveedor = $grouped[2];
	//echo "<pre>";var_dump($proveedor);echo "</pre>";

	$dq_haber = 0;
	foreach($proveedor as $dc_cuenta_contable => $item){

		$detalles_financieros[] = array(
			'dg_codigo_cuenta' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_codigo,
			'dg_cuenta_contable' => $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable')->dg_cuenta_contable,
			'dg_glosa' => "Carga Proveedor Centralización Libro de Compra: ".date('d-m-Y'),
			'dq_debe' => floatval($item['sum']),
			'dq_haber' => 0
		);

		foreach($item['detalle'] as $d){

			$detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds(array(
				'dc_cuenta_contable' => $dc_cuenta_contable,
				'dq_debe' => floatval($d->dq_total),
				'dq_haber' => 0,
				'dg_glosa' => "Carga Proveedor Centralización Libro de Compra: ".date('d-m-Y'),
				'dc_nota_credito_proveedor' => $d->dc_nota_credito,
				'dc_proveedor' => $d->dc_proveedor,
				'dc_tipo_proveedor' => $d->dc_tipo_proveedor,
				'dc_mercado_proveedor' => $d->dc_mercado_proveedor
			));
		}
	}

	return array($detalles_financieros, $detalles_analiticos);

}
