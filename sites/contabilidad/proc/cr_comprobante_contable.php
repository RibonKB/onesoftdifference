<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select("tb_tipo_movimiento","dc_tipo_movimiento,dg_tipo_movimiento","dg_codigo = '{$_POST['tipo_codigo']}' AND dc_empresa={$empresa} AND dm_activo = '1'");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el tipo de movimiento especificado, intentelo nuevamente.","sites/contabilidad/cr_comprobante_contable.php");
}
$datos = $datos[0];

//Cuentas contables
$cuentas = $db->select("tb_cuenta_contable c
JOIN tb_cuentas_tipo_movimiento t ON t.dc_cuenta_contable = c.dc_cuenta_contable",
"c.dg_codigo,c.dg_cuenta_contable,c.dg_descripcion_larga,c.dc_cuenta_contable",
"c.dc_empresa = {$empresa} AND t.dc_tipo_movimiento = {$datos['dc_tipo_movimiento']}");

?>
<div id="secc_bar">Creación de comprobante contable</div>
<div id="main_cont">
<div class="panes" style="width:1100px;">
<div class="title center">Comprobante contable para el tipo de movimiento <strong><?=$datos['dg_tipo_movimiento'] ?></strong></div>
<hr class="clear" />
<?php

	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Indique los datos para la generación del comprobante contable</strong><br />Los datos marcados con [*] son obligatorios");
	$form->Start("sites/contabilidad/proc/crear_comprobante_contable.php","cr_comprobante_contable","comprobante_form");
	$form->Section();
	$form->Text('Número de comprobante interno','comp_num_interno');
	$form->Date('Fecha contable','comp_fecha',1,date('d/m/Y'));
	$form->EndSection();
	$form->Section();
	echo("<fieldset><legend>Dejar en blanco para usar moneda local</legend>");
	$form->Listado("Tipo de cambio","comp_tipo_cambio","tb_tipo_cambio",array("dc_tipo_cambio","dg_tipo_cambio","dq_cambio"));
	$form->Text("Cambio","comp_cambio");
	echo("</fieldset>");
	$form->EndSection();
	$form->Section();
	echo("<fieldset><legend>Fecha de emisión del comprobante</legend><b>".date("d/m/Y")."</b></fieldset>");
	$form->Textarea("Glosa general","comp_glosa_general",0,'',32,3);
	$form->EndSection();
?>
	<br class="clear" />
	<fieldset>
	<legend>Detalle del comprobante</legend>
	<div id="detalle">
	<table width="100%" class="tab">
		<thead id="detalle_head">
			<tr>
				<th width="8%">Opciones</th>
				<th width="1%">Código cuenta</th>
				<th>Descripción</th>
				<th width="1%">DEBE</th>
				<th width="1%">HABER</th>
				<th width="1%">Glosa</th>
				<th width="23%">Documento</th>
			</tr>
		</thead>
		<tbody id="detalle_list">
			<tr>
				<td>
				<img src="images/copy.png" alt="C" title="Duplicar" class="duplicate" />
				<img src="images/addbtn.png" alt="+" title="Más detalles" class="show_more" />
				<img src="images/delbtn.png" alt="X" title="Eliminar detalle" class="del_detail" />
				</td>
				<td>
				<input type="text" class="det_cta inputtext" size="10" required="required" />
				<input type="hidden" name="comp_cuentas[]" value="" class="det_cta_id" />
				</td>
				<td>-</td>
				<td>
					<input type="text" name="comp_debe[]" class="det_debe inputtext" size="10" required="required" style="text-align:right;" />
				</td>
				<td>
					<input type="text" name="comp_haber[]" class="det_haber inputtext" size="10" required="required" style="text-align:right;" />
				</td>
				<td><input type="text" name="comp_glosa[]" class="det_glosa inputtext" size="25" required="required" /></td>
				<td>
				<select name="comp_tipo_doc[]" class="det_tipo_doc inputtext" style="width:100px;">
					<option value="0">Tipo</option>
				<?php
					$tipos_doc = $db->select("tb_tipo_documento","dc_tipo_documento,dg_tipo_documento","dc_empresa = {$empresa} AND dm_activo='1'");
					foreach($tipos_doc as $t){
						echo("<option value='{$t['dc_tipo_documento']}'>{$t['dg_tipo_documento']}</option>");
					}
				?>
				</select>
				Nº<input type="text" name="comp_num_doc[]" size="8" class="det_num_doc inputtext" />
				</td>
			</tr>
			<tr style="display:none;">
				<td colspan="7" align="center" style="background-color:#E3E3E3;">
				<div class="left">Nº cheque<br /><input type="text" name="comp_cheque[]" class="det_cheque inputtext" size="13" /></div>
				<div class="left">Fecha cheque<br /><input type="text" name="comp_fecha_cheque[]" class="det_cheque_date date inputtext" size="13" /></div>
				<div class="left">RUT<br /><input type="text" name="comp_rut[]" class="det_rut inputtext" size="13" /></div>
				<div class="left">Activo fijo<br /><input type="text" name="comp_activo[]" class="det_activo inputtext" size="13" /></div>
				<div class="left">Banco<br />
				<select name="comp_banco[]" class="det_banco inputtext" style="width:140px;">
				<option value="0"></option>
				<?php
					$bancos = $db->select("tb_banco","dc_banco,dg_banco","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by" => "dg_banco"));
					foreach($bancos as $b){
						echo("<option value='{$b['dc_banco']}'>{$b['dg_banco']}</option>");
					}
				?>
				</select>
				</div>
				<div class="left hidden">Cebe<br />
				<select name="comp_cebe[]" class="det_cebe inputtext" style="width:140px;">
				<option value="0"></option>
				<?php
					$cebes = $db->select("tb_cebe","dc_cebe,dg_cebe","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by" => "dg_cebe"));
					foreach($cebes as $c){
						echo("<option value='{$c['dc_cebe']}'>{$c['dg_cebe']}</option>");
					}
				?>
				</select>
				</div>
				<div class="left hidden">Ceco<br />
				<select name="comp_ceco[]" class="det_ceco inputtext" style="width:140px;">
				<option value="0"></option>
				<?php
					$cecos = $db->select("tb_ceco","dc_ceco,dg_ceco","dc_empresa={$empresa} AND dm_activo = '1'",array("order_by" => "dg_ceco"));
					foreach($cecos as $c){
						echo("<option value='{$c['dc_ceco']}'>{$c['dg_ceco']}</option>");
					}
				?>
				</select>
				</div>
				<div class="left">Documento de venta<br /><input type="text" name="comp_venta[]" class="det_doc_venta inputtext" size="13" /></div>
				<div class="left">Documento de compra<br /><input type="text" name="comp_compra[]" class="det_doc_compra inputtext" size="13" /></div>
				</td>
			</tr>
		</tbody>
		<tfoot id="detalle_foot">
			<tr>
				<th style="background:#DDD;" colspan="3">&nbsp;</td>
				<th style="background:#DDD;">Total DEBE</td>
				<th style="background:#DDD;">Total HABER</td>
				<th style="background:#DDD;">Diferencia Total DEBE-HABER</td>
				<th style="background:#DDD;"></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td align="right" class="total_debe">0</td>
				<td align="right" class="total_haber">0</td>
				<td align="right" class="diferencia_dh">0</td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
	</table>
		
	<div class="center">
		<input type="button" class="addbtn" id="detalle_add" value="Agregar otro detalle" />
	</div>
		
		</div>
		
		</fieldset>
		
		<div class="center">
			<input type="hidden" name="tipo_mov_id" value="<?=$datos['dc_tipo_movimiento'] ?>" />
			<input type="hidden" name="comp_saldo" value="" />
			<input type="submit" class="addbtn" value="Crear" />
		</div>
		
		</fieldset>
		</form>
	</div>
</div>
<script type="text/javascript">
	var cuentas = <?=json_encode($cuentas) ?>;
</script>
<script type="text/javascript" src="jscripts/comprobante_contable.js?v=1_2Y"></script>