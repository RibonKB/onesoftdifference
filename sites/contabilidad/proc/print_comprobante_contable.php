<?php
require("../../../inc/fpdf.php");

class PDF extends FPDF{

private $datosEmpresa;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"tb_empresa e,tb_empresa_configuracion ec,tb_comuna c,tb_region r",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region",
	"e.dc_empresa={$empresa} AND e.dc_comuna = c.dc_comuna AND c.dc_region = r.dc_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','mm','Letter');
	
}

function Header(){
	global $datosCom;
	
	$this->SetFont('Arial','',12);
	$this->SetDrawColor(0,100,0);
	$this->SetTextColor(0,100,0);
	$this->SetX(160);
	$this->MultiCell(50,9,"R.U.T. {$this->datosEmpresa['dg_rut']}\nCOMPROBANTE\nCONTABLE\nNº {$datosCom['dg_comprobante']}",1,'C');
	$this->SetY(10);
	$this->SetTextColor(0);
	
	$this->SetFont('','B',14);
	$this->Cell(145,5,$this->datosEmpresa['dg_razon']);
	$this->ln();
	$this->SetFont('Arial','',10);
	$this->MultiCell(145,5,"{$this->datosEmpresa['dg_giro']}\n{$this->datosEmpresa['dg_direccion']}, {$this->datosEmpresa['dg_comuna']} {$this->datosEmpresa['dg_region']}\n{$this->datosEmpresa['dg_fono']}");
	$this->Ln(7);
	
	$this->SetDrawColor(150,150,150);
	$this->Cell(0,5,'','B');
	$this->Ln();
	$y = $this->GetY();
	$this->SetFontSize(5);
	$this->MultiCell(14,7,"EMISIÓN:\nTIPO MOVIMIENTO:\n",'L');
	$this->Cell(0,5,'','T');
	$this->SetY($y);
	$this->SetX(118);
	$this->MultiCell(14,7," \nPERIODO CONTABLE:\nRESPONSABLE:");
	$this->SetY($y);
	$this->SetX(22);
	$this->SetFontSize(11);
	$this->MultiCell(120,7,"{$datosCom['df_fecha_emision']}\n{$datosCom['dg_tipo_movimiento']}");
	$this->SetY($y);
	$this->SetX(130);
	$this->MultiCell(76,7," \n{$datosCom['dg_periodo']}\n{$datosCom['dg_responsable']}\n \n ","R");
	$this->Ln(3);
	
	$this->SetFillColor(210,210,210);
	$this->SetFontSize(9);
	$this->Cell(0,7,"Glosa general",'B',2,'C',1);
	$this->MultiCell(0,5,$datosCom['dg_glosa'],0,0,'R');
	$this->Ln(3);
	
	$w = array(25,70,30,30,41);
	$head = array("CODIGO","DESCRIPCIÓN","DEBE","HABER","GLOSA");
	$this->SetFontSize(7);
	for($i=0;$i<4;$i++){
		$this->Cell($w[$i],6,$head[$i],1,0,'L',1);
	}
	$this->Ln();
	
}

function Footer()
{
	$pie = explode("\n",$this->datosEmpresa['dg_pie_cotizacion']);
    //Posición: a 1,5 cm del final
    $this->SetY((count($pie)*-3)-20);
    //Arial italic 8
    $this->SetFont('Arial','I',7);
    //Número de púgina
	$this->AliasNbPages();
	$this->MultiCell(150,3,$this->datosEmpresa['dg_pie_cotizacion']);
	$this->SetY(-15);
    $this->Cell(0,10,'Pagina '.$this->PageNo().' de {nb}',0,0,'R');
}

function AddDetalle($detalle){
	global $datosCom;
	
	$this->AddPage();
	$this->SetFont('Times','',9);
	$this->SetDrawColor(150,150,150);
	$this->SetFillColor(240,240,240);
	$fill = false;
	foreach($detalle as $i => $det){
		$this->Cell(25,6,$det['dg_codigo'],'LR',0,'R',$fill);
		$this->Cell(70,6,$det['dg_cuenta_contable'],'LR',0,'L',$fill);
		$this->Cell(30,6,number_format($det['dq_debe'],2),'LR',0,'R',$fill);
		$this->Cell(30,6,number_format($det['dq_haber'],2),'LR',0,'R',$fill);
		$this->Cell(41,6,$det['dg_glosa'],'LR',0,'R',$fill);
		$this->Ln();
		$fill = !$fill;
		if(($i+1)%10 == 0){
			$this->Cell(0,6,'','T');
			$this->AddPage();
		}
	}
	for($i=0;$i<(10 - count($detalle)%10);$i++){
		$this->Cell(25,6,"",'LR',0,'R',$fill);
		$this->Cell(70,6,"",'LR',0,'L',$fill);
		$this->Cell(30,6,"",'LR',0,'R',$fill);
		$this->Cell(30,6,"",'LR',0,'R',$fill);
		$this->Cell(41,6,"",'LR',0,'R',$fill);
		$this->Ln();
		$fill = !$fill;
	}
	$this->Cell(0,6,'','T');
	$this->Ln(0);
	
	$y = $this->GetY();
	$this->SetFont('Arial','',7);
	
	$this->SetFillColor(200,200,200);
	$this->SetY($y);
	$this->SetX(126);
	$this->SetFont('','B');
	$this->Cell(40,5,"TOTAL",1,2,'R',1);
	$this->Cell(40,5,"I.V.A.",1,2,'R',1);
	$this->Cell(40,5,"TOTAL PAGAR",1,2,'R',1);
	
	$this->SetY($y);
	$this->SetX(166);
	$this->SetFont('Times','',9);
	$this->Cell(40,5,number_format($datosCom['dq_saldo'],2),1,2,'R');
	$this->Cell(40,5,number_format($datosCom['dq_saldo'],2),1,2,'R');
	$this->Cell(40,5,number_format($datosCom['dq_saldo']+$datosCom['dq_saldo'],2),1,2,'R');
	
}

}

$datosCom = $db->select(
"(SELECT * FROM tb_comprobante_contable WHERE dc_comprobante = {$_POST['id']}) AS c
LEFT JOIN tb_usuario AS u ON c.dc_usuario_creacion = u.dc_usuario
LEFT JOIN tb_funcionario AS f ON u.dc_funcionario = f.dc_funcionario",
"c.dg_comprobante,c.dq_mes_contable,c.dg_anho_contable,c.dg_glosa,c.dq_saldo,UNIX_TIMESTAMP(c.df_fecha_emision) as df_fecha_emision, CONCAT_WS(' ',f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno)");
?>