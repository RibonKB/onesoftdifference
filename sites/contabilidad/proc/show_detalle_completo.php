<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_detalle = intval($_POST['id']);

$data = $db->prepare($db->select('tb_comprobante_contable_detalle d
JOIN tb_cuenta_contable cc ON cc.dc_cuenta_contable = d.dc_cuenta_contable
LEFT JOIN tb_tipo_documento td ON td.dc_tipo_documento = d.dc_tipo_documento
LEFT JOIN tb_proveedor prov ON prov.dc_proveedor = d.dc_proveedor
LEFT JOIN tb_cliente cl ON cl.dc_cliente = d.dc_cliente
LEFT JOIN tb_banco b ON b.dc_banco = d.dc_banco
LEFT JOIN tb_cebe cb ON cb.dc_cebe = d.dc_cebe
LEFT JOIN tb_ceco cco ON cco.dc_ceco = d.dc_ceco
LEFT JOIN tb_factura_compra fc ON fc.dc_factura = d.dc_factura_compra
LEFT JOIN tb_factura_venta fv ON fv.dc_factura = d.dc_factura_venta
LEFT JOIN tb_orden_compra oc ON oc.dc_orden_compra = d.dc_orden_compra
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta IN(fc.dc_nota_venta, fv.dc_nota_venta, oc.dc_nota_venta)',
'd.dq_debe,d.dq_haber,d.dg_glosa,d.dg_numero_documento,d.dg_cheque,d.df_fecha_cheque,d.dg_rut,d.dg_activo_fijo,d.dg_doc_venta,d.dg_doc_compra,
cc.dg_cuenta_contable,td.dg_tipo_documento, prov.dg_razon dg_proveedor, cl.dg_razon dg_cliente, b.dg_banco, cb.dg_cebe, cco.dg_ceco,
fc.dq_factura dq_factura_compra, fc.dq_folio dq_folio_compra, fv.dq_factura dq_factura_venta, fv.dq_folio dq_folio_venta,
oc.dq_orden_compra, nv.dq_nota_venta',
'd.dc_detalle = ?'));
$data->bindValue(1,$dc_detalle,PDO::PARAM_INT);
$db->stExec($data);
$data = $data->fetch(PDO::FETCH_OBJ);

if($data === false){
	$error_man->showWarning('No se encontró el detalle seleccionado, compruebe los datos de entrada y vuelva a intentarlo.');
	exit;
}

?>
<div class="secc_bar">
	Detalle completo comprobante contable
</div>
<table class="tab" width="100%">
<tbody>
	<tr>
		<td>Cuenta Contable:</td>
		<td><?php echo $data->dg_cuenta_contable ?></td>
	</tr>
	<tr>
		<td>Glosa</td>
		<td><?php echo $data->dg_glosa ?></td>
	</tr>
	<tr>
		<td>DEBE</td>
		<td><?php echo moneda_local($data->dq_debe) ?></td>
	</tr>
	<tr>
		<td>HABER</td>
		<td><?php echo moneda_local($data->dq_haber) ?></td>
	</tr>
	<tr>
		<td>Tipo de documento</td>
		<td><?php echo $data->dg_tipo_documento ?></td>
	</tr>
	<tr>
		<td>Número de documento</td>
		<td><?php echo $data->dg_numero_documento ?></td>
	</tr>
	<tr>
		<td>Banco</td>
		<td><?php echo $data->dg_banco ?></td>
	</tr>
	<tr>
		<td>Cheque</td>
		<td><?php echo $data->dg_cheque ?></td>
	</tr>
	<tr>
		<td>Fecha cheque</td>
		<td><?php echo $db->dateLocalFormat($data->df_fecha_cheque) ?></td>
	</tr>
	<tr>
		<td>Rut</td>
		<td><?php echo $data->dg_rut ?></td>
	</tr>
	<tr>
		<td>Cliente</td>
		<td><?php echo $data->dg_cliente ?></td>
	</tr>
	<tr>
		<td>Proveedor</td>
		<td><?php echo $data->dg_proveedor ?></td>
	</tr>
	<tr>
		<td>Centro de beneficio</td>
		<td><?php echo $data->dg_cebe ?></td>
	</tr>
	<tr>
		<td>Centro de costo</td>
		<td><?php echo $data->dg_ceco ?></td>
	</tr>
	<tr>
		<td>Factura de Compra</td>
		<td><b><?php echo $data->dq_factura_compra ?></b> folio: <b><?php echo $data->dq_folio_compra ?></b></td>
	</tr>
	<tr>
		<td>Factura de Venta</td>
		<td><b><?php echo $data->dq_factura_venta ?></b> folio: <b><?php echo $data->dq_folio_venta ?></b></td>
	</tr>
	<tr>
		<td>Orden de compra</td>
		<td><?php echo $data->dq_orden_compra ?></td>
	</tr>
	<tr>
		<td>Nota de venta</td>
		<td><?php echo $data->dq_nota_venta ?></td>
	</tr>
</tbody>
</table>