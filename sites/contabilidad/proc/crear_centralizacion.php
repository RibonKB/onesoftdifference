<?php
define("MAIN",1);
require_once("../../../inc/init.php");

//Validar que se haya seleccioando al menos un documento
if(!isset($_POST['dc_documento'])){
	$error_man->showWarning('Debe seleccionar al menos un documento del listado para realizar la centralización');
	exit;
}

$tipo_centralizacion = intval($_POST['tipo_centralizacion']);

if($tipo_centralizacion == 0){
	require_once("src_centralizacion_compra.php");
}else{
	require_once("src_centralizacion_venta.php");
}

//Obtener información de facturas de venta/compra
$fData = obtenerDataFactura($_POST,$db);

//Validar la obtención de Facturas de compra/venta desde la base de datos
if($status = array_shift($fData) != 'OK'){
	switch($status){
		case 'ERROR_ITEM_NOT_FOUND':
			$error_man->showWarning('Hay facturas que no fueron encontradas o ya fueron incluidas en otra centralización');
			break;
		default:
			$error_man->showWarning('Ocurrió un error desconocido, consulte con un administrador para recibir detalles.');
	}
	exit;
}

//Obtener información de Notas de Crédito/notas de crédito proveedor
$ncData = obtenerDataNotaCredito($_POST,$db);

//Validar la obtención de Notas de Crédito desde la base de datos
if($status = array_shift($ncData) != 'OK'){
	switch($status){
		case 'ERROR_ITEM_NOT_FOUND':
			$error_man->showWarning('Hay notas de crédito que no fueron encontradas o ya fueron incluidas en otra centralización');
			break;
		default:
			$error_man->showWarning('Ocurrió un error desconocido, consulte con un administrador para recibir detalles.');
	}
	exit;
}

$factura = array_shift($fData);
$status = validarFacturaCreacion($factura,$db);
if($status !== true){
	switch($status[0]){
		case "ERROR_NO_RECEPCIONADO":
			$producto = array_shift($status[1]);
			$fc = array_shift($status[1]);
			$error_man->showWarning("Se ha encontrado un producto que mueve stock pero no ha sido recepcionado, por lo tanto no puede ser centralizado en existencias<br />".
															"El error fue encontrado en la factura con el folio: <strong>{$fc->dq_folio}</strong>");
			$error_man->showInfo("El producto que gatilló este error corresponde al siguiente:<br />".
												 	 "Código: <strong>{$producto->dg_codigo}</strong>");
			break;
		default:
			$error_man->showWarning('Error de validación');
			echo'<pre>';var_dump($status);echo'</pre>';
	}
	//echo'<pre>';var_dump($status);echo'</pre>';
	exit;
}

$nota_credito = array_shift($ncData);
$status = validarNotaCreditoCreacion($nota_credito,$db);
if($status !== true){
	switch($status[0]){
		default:
			$error_man->showWarning('Error de validación');
	}
	echo'<pre>';var_dump($status);echo'</pre>';
	exit;
}

//Antes de crear la centralización se pide el periodo contable en que se creará y se muestra una vista preliminar del comprobane contable final
if(!isset($_POST['df_fecha_contable'])){
	require_once('src_pre_centralizacion.php');
	exit;
}

$db->start_transaction();
	$dc_comprobante = insertarComprobanteContable(array($factura,$nota_credito), $_POST['df_fecha_contable'], $db);
	if(count($factura)){
		insertarDetalles($factura, $dc_comprobante, $db);
		desmarcarFacturas($_POST['dc_documento'], $db);
	}
	if(count($nota_credito)){
		insertarDetallesNC($nota_credito, $dc_comprobante, $db);
		desmarcarNotasCredito($_POST['dc_documento'], $db);
	}
$db->commit();

$error_man->showConfirm("Se han centralizado los documentos seleccionados, para ver el comprobante contable");

?>
