<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$mes_contable = substr($_POST['comp_fecha'],3,2);
$anho_contable = substr($_POST['comp_fecha'],6);


$val = $db->select(
"tb_mes_contable m
JOIN tb_periodo_contable p ON m.dc_mes_contable = p.dc_mes_contable",
"p.dm_filtro,p.dg_permisos,p.dm_estado",
"m.dc_mes = {$mes_contable} AND
dc_anho = {$anho_contable} AND
m.dc_empresa = {$empresa} AND p.dm_activo = '1'");

if(!count($val)){
	$error_man->showWarning("No existe el periodo en el que intenta crear el comprobante");
	exit();
}
$val = $val[0];

if($val['dm_filtro'] == '1'){
	$gr = substr($val['dg_permisos'],0,-1);
	if(!count($db->select("tb_usuario_grupo","1","dc_usuario = {$idUsuario} AND dc_grupo IN ({$gr})"))){
		$error_man->showWarning("Su grupo de usuario no tiene permiso de generar comprobantes en este periodo, consulte con el encargado.");
		exit();
	}
}else if($val['dm_filtro'] == '2'){
	if(strpos($val['dg_permisos'],"{$idUsuario},") === false){
		$error_man->showWarning("No tiene permisos para generar comprobantes en este periodo, consulte con el encargado.");
		exit();
	}
}
if($val['dm_estado'] == '0'){
	$error_man->showWarning("El periodo contable seleccionado está cerrado.");
	exit();
}

/*if($_POST['comp_num_interno']){
	if(count($db->select('tb_comprobante_contable','1',"dc_empresa = {$empresa} AND dg_numero_interno='{$_POST['comp_num_interno']}' AND MONTH(df_fecha_contable) = MONTH(".$db->sqlDate($_POST['comp_fecha']).") AND dc_comprobante<>{$_POST['comp_id']}"))){
		$error_man->showWarning('Ya se ha ingresado un comprobante contable con el número interno especificado en el periodo contable especificado');
		exit();
	}
}*/

$cambio = $_POST['comp_tipo_cambio']?$_POST['comp_cambio']:1;

$db->update('tb_comprobante_contable',
array(
	"df_fecha_contable" => $db->sqlDate($_POST['comp_fecha']),
	"dc_mes_contable" => $mes_contable,
	"dc_anho_contable" => $anho_contable,
	"dc_tipo_cambio" => $_POST['comp_tipo_cambio'],
	"dq_cambio" => $cambio,
	"dg_glosa" => $_POST['comp_glosa_general'],
	"dq_saldo" => str_replace(',','',$_POST['comp_saldo'])*$cambio,
	"dg_numero_interno" => $_POST['comp_num_interno'],
	"dm_editable" => 0
),"dc_comprobante={$_POST['comp_id']} AND dc_empresa={$empresa}");

$db->query("DELETE FROM tb_comprobante_contable_detalle WHERE dc_comprobante={$_POST['comp_id']}");

foreach($_POST['comp_cuentas'] as $i => $v){
	$db->insert("tb_comprobante_contable_detalle",
	array(
		"dc_comprobante" => $_POST['comp_id'],
		"dc_cuenta_contable" => $v,
		"dq_debe" => str_replace(',','',$_POST['comp_debe'][$i])*$cambio,
		"dq_haber" => str_replace(',','',$_POST['comp_haber'][$i])*$cambio,
		"dg_glosa" => $_POST['comp_glosa'][$i],
		"dc_tipo_documento" => $_POST['comp_tipo_doc'][$i],
		"dg_numero_documento" => $_POST['comp_num_doc'][$i],
		"dg_cheque" => $_POST['comp_cheque'][$i],
		"dg_rut" => $_POST['comp_rut'][$i],
		"dg_activo_fijo" => $_POST['comp_activo'][$i],
		"dc_banco" => $_POST['comp_banco'][$i],
		"dc_cebe" => $_POST['comp_cebe'][$i],
		"dc_ceco" => $_POST['comp_ceco'][$i],
		"dg_doc_venta" => $_POST['comp_venta'][$i],
		"dg_doc_compra" => $_POST['comp_compra'][$i]
	));
}

$error_man->showConfirm("El comprobante fue modificado correctamente");

?>