<?php
define("MAIN",1);
require_once("../../../inc/init.php");

//Validación datos de ingreso
//Validar Año
if(preg_match("/^(10|20)\d\d$/",$_POST['dc_anho_corte']) == 0){
	$error_man->showWarning("El año ingresado es incorrecto y no cumple con el formato esperado, compruebe el dato e inténtelo de nuevo.");
	exit;
}

//Validar Mes
if(preg_match("/^[0|1]?\d$/",$_POST['dc_mes_corte']) == 0 || !intval($_POST['dc_mes_corte'])){
	$error_man->showWarning("El mes ingresado es incorrecto y no cumple con el formato esperado, compruebe el dato e inténtelo de nuevo.");
	exit;
}

//Validar tipo de centralización
if(preg_match("/^[0|1]$/",$_POST['tipo_centralizacion']) == 0){
	$error_man->showWarning("El tipo de centralización ingresado es incorrecto y no cumple con el formato esperado, compruebe el dato e inténtelo de nuevo.");
	exit;
}

$tipo_centralizacion = intval($_POST['tipo_centralizacion']);

if($tipo_centralizacion == 0){
	require_once("src_centralizacion_compra.php");
	$tipo_razon = "Proveedor";
}else{
	require_once("src_centralizacion_venta.php");
	$tipo_razon = "Cliente";
}

$_POST['df_emision_hasta'] =  DateTime::createFromFormat('Y/n', "{$_POST['dc_anho_corte']}/{$_POST['dc_mes_corte']}")->format('t/m/Y');

$data = buscarResultados($_POST, $db);

if(count($data) == 0){
	$error_man->showAviso("No se han encontrado documentos sin centralizar en el periodo seleccionado");
	exit;
}

//Calcular totales
$total_exento = 0;
$total_neto = 0;
$total_iva = 0;
$total = 0;
foreach($data as $doc){
	$total_exento += $doc->dq_exento;
	$total_neto += $doc->dq_neto;
	$total_iva += $doc->dq_iva;
	$total += $doc->dq_total;
}

?>
<div class="right">
	<button id="btn_centralizar" class="imgbtn" style="background-image:url(images/management.png)">Centralizar Selección</button>
</div>
<table class="tab" width="100%" id="grid_centralizacion">
	<thead>
    	<tr>
        	<th><input type="checkbox" id="select_all_chbx" /></th>
        	<th>Documento</th>
            <th>Folio</th>
            <th>Tipo</th>
            <th>Fecha Emisión</th>
            <th><?php echo $tipo_razon ?></th>
            <th>Exento</th>
            <th>Neto</th>
            <th>IVA</th>
            <th>Total</th>
        </tr>
    </thead>
    <tfoot>
    	<tr>
        	<th align="right" colspan="6">Totales</th>
            <th align="right"><?php echo moneda_local($total_exento) ?></th>
            <th align="right"><?php echo moneda_local($total_neto) ?></th>
            <th align="right"><?php echo moneda_local($total_iva) ?></th>
            <th align="right"><?php echo moneda_local($total) ?></th>
        </tr>
    </tfoot>
    <tbody id="tbody_list">
    <?php foreach($data as $doc): ?>
    <?php if($doc->tp === 'NC'): ?>
    	<tr style="color:#F00;">
    <?php else: ?>
    	<tr>
    <?php endif; ?>
        	<td><input type="checkbox" name="dc_documento[<?php echo $doc->tp ?>][]" value="<?php echo $doc->dc_documento ?>" /></td>
        	<td><b><?php echo $doc->dq_factura ?></b></td>
            <td><b><?php echo $doc->dq_folio ?></b></td>
            <td><?php echo $doc->dg_tipo ?></td>
            <td><?php echo $db->dateLocalFormat($doc->df_emision) ?></td>
            <td><?php echo $doc->dg_razon ?></td>
            <td align="right"><b><?php echo moneda_local($doc->dq_exento) ?></b></td>
            <td align="right"><b><?php echo moneda_local($doc->dq_neto) ?></b></td>
            <td align="right"><b><?php echo moneda_local($doc->dq_iva) ?></b></td>
            <td align="right"><b><?php echo moneda_local($doc->dq_total) ?></b></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript" src="jscripts/sites/contabilidad/src_centralizacion.js"></script>
<script type="text/javascript">
	js_data.tipo_centralizacion = <?php echo $tipo_centralizacion ?>;
	js_data.init();
</script>
