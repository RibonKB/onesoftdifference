<?php
define("MAIN", 1);
require_once("../../../inc/init.php");
require_once("../stuff.class.php");
require_once("../../../inc/Factory.class.php");

$dc_comprobante = intval($_POST['id']);

$comprobante = $db->prepare($db->select("tb_comprobante_contable c
	LEFT JOIN tb_tipo_movimiento m ON c.dc_tipo_movimiento = m.dc_tipo_movimiento
	LEFT JOIN tb_usuario u ON c.dc_usuario_creacion = u.dc_usuario
	LEFT JOIN tb_funcionario f ON u.dc_funcionario = f.dc_funcionario", "c.dg_comprobante, c.df_fecha_contable, c.dg_glosa, c.dq_saldo, c.df_fecha_emision, c.dc_tipo_cambio, c.dq_cambio,c.dm_anulado,
m.dg_tipo_movimiento,m.dg_codigo,m.dg_descripcion, f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno, u.dg_usuario", 'c.dc_comprobante = ? AND c.dc_empresa = ?'));
$comprobante->bindValue(1, $dc_comprobante, PDO::PARAM_INT);
$comprobante->bindValue(2, $empresa, PDO::PARAM_INT);
$db->stExec($comprobante);

$comprobante = $comprobante->fetch(PDO::FETCH_OBJ);

if ($comprobante === false) {
  $error_man->showWarning('El comprobante contable seleccionado no existe, compruebe los datos de entrada y vuelva a intentarlo.');
  exit;
}

if ($comprobante->dc_tipo_cambio == 0) {
  $comprobante->dg_tipo_cambio = $empresa_conf['dg_moneda_local'];
}


$nula = '';
if ($comprobante->dm_anulado == '1') {
  $null_data = $db->doQuery($db->select("(SELECT * FROM tb_comprobante_contable_anulado WHERE dc_comprobante = {$_POST['id']}) ca
	JOIN tb_motivo_anulacion ma ON ma.dc_motivo = ca.dc_motivo", 'ma.dg_motivo,ca.dc_usuario,ca.dg_comentario'));
  $nula_info = '';
  $null_data = $null_data->fetch(PDO::FETCH_OBJ);
  if ($null_data != false) {
    $nula_info = "<div style='border:1px solid #CCC; background:#FFF;padding:5px;line-height:20px;'>
		<div class='info'>Info anulación</div>
		Motivo: <b>{$null_data->dg_motivo}</b><br />
		Comentario: <b>{$null_data->dg_comentario}</b>";
  }
  $nula = "<td rowspan='11' valign='middle'><h3 class='alert'>COMPROBANTE CONTABLE ANULADO</h3>{$nula_info}</td>";
}

$detalle = $db->prepare($db->select("tb_comprobante_contable_detalle d
	JOIN tb_cuenta_contable c ON c.dc_cuenta_contable = d.dc_cuenta_contable", "d.dc_detalle, c.dg_codigo,c.dg_cuenta_contable,c.dg_descripcion_larga,d.dq_debe,d.dq_haber,d.dg_glosa", 'd.dc_comprobante = ? AND d.dm_activo=0'));
$detalle->bindValue(1, $dc_comprobante, PDO::PARAM_INT);
$db->stExec($detalle);

$aux_analitico = $db->prepare($db->select('tb_comprobante_contable_detalle d
	JOIN tb_comprobante_contable_detalle_analitico a ON a.dc_detalle_financiero = d.dc_detalle', 
        '*', 'd.dc_comprobante = ? AND d.dm_activo=0'));
$aux_analitico->bindValue(1, $dc_comprobante, PDO::PARAM_INT);
$db->stExec($aux_analitico);

$detalles_analiticos = array();
while ($d = $aux_analitico->fetch(PDO::FETCH_ASSOC)) {
  $detalles_analiticos[] = ContabilidadStuff::getAnaliticoDesdeIds($d);
}
$detalles_analiticos = ContabilidadStuff::delEmptyCols($detalles_analiticos);
$header_analitico = ContabilidadStuff::getHeaderAnalitico($detalles_analiticos);

$array_widths = array();
if($header_analitico){
foreach ($header_analitico as $w) {
  $array_widths[] = $w[0];
}
}
?>
<div class='title'>Comprobante contable nº <?php echo $comprobante->dg_comprobante ?></div>
<table class='tab' width='100%' style='text-align:left;'>
  <caption>
    Tipo de movimiento:<br />
    <strong>
      (<?php echo $comprobante->dg_codigo ?>) <?php echo $comprobante->dg_tipo_movimiento ?>
    </strong>
  </caption>
  <tbody>
    <tr>
      <td width='160'>Periodo contable :</td>
      <td><b><?php echo $db->dateLocalFormat($comprobante->df_fecha_contable) ?><?php echo $nula ?></b></td>
    </tr>
    <tr>
      <td>Fecha emisión :</td>
      <td><b><?php echo $db->dateLocalFormat($comprobante->df_fecha_emision) ?></b></td>
    </tr>
    <tr>
      <td>Responsable :</td>
      <td title='<?php echo $comprobante->dg_usuario ?>'>
        <b><?php echo $comprobante->dg_nombres . ' ' . $comprobante->dg_ap_paterno . ' ' . $comprobante->dg_ap_materno ?></b>
      </td>
    </tr>
    <tr>
      <td colspan='2'>
        <br /><b>Glosa general:</b> <?php echo $comprobante->dg_glosa ?><br /><br />
      </td>
    </tr>
  </tbody>
</table>

<ul id="tabs">
  <li><a href="#">Detalle Financiero</a></li>
  <li><a href="#">Detalle Analítico</a></li>
</ul>
<br class="clear" />
<div class="tabpanes">
  <div>
    <div class="info">
      Detalles comprobante financiero
    </div>
    <table width='100%' class='tab sortable' id='detalle'>
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th width='90'>Código cuenta</th>
          <th width='150'>Descripción</th>
          <th width='90'>DEBE</th>
          <th width='90'>HABER</th>
          <th>Glosa</th>
        </tr>
      </thead>
      <tbody>
<?php while ($d = $detalle->fetch(PDO::FETCH_OBJ)): ?>
          <tr>
            <td>
              <a href="sites/contabilidad/proc/show_detalle_completo.php?id=<?php echo $d->dc_detalle ?>" class="loadOnOverlay">
                <img src="images/doc.png" alt="[D]" title="Visualizar detalle completo" />
              </a>
            </td>
            <td><?php echo $d->dg_codigo ?></td>
            <td title='<?php echo $d->dg_descripcion_larga ?>' align='left'><?php echo $d->dg_cuenta_contable ?></td>
            <td align='right' class='price'><?php echo moneda_local($d->dq_debe) ?></td>
            <td align='right' class='price'><?php echo moneda_local($d->dq_haber) ?></td>
            <td align='left'><?php echo $d->dg_glosa ?></td>
          </tr>
<?php endwhile; ?>
      </tbody>
      <tfoot>
        <tr>
          <th colspan='3' align='right'>saldo</th>
          <th align='right' class='price'><?php echo moneda_local($comprobante->dq_saldo) ?></th>
          <th align='right' class='price'><?php echo moneda_local($comprobante->dq_saldo) ?></th>
          <th>&nbsp;</th>
        </tr>
      </tfoot>
    </table>
  </div>
  <div>
    <div class="info">
      Detalles comprobante analítico
    </div>
    <table class="tab bicolor_tab" id="tabla_analitico">
      <thead>
        <tr>
           
<?php 
if($header_analitico):
foreach ($header_analitico as $h): ?>
            <th><?php echo $h[1] ?></th>
<?php endforeach ;
endif
?>
        </tr>
      </thead>
      <tbody>
          <?php foreach ($detalles_analiticos as $d): ?>
          <tr>
            <?php foreach ($d as $col => $dato): ?>
    <?php
    if (in_array($col, array('dq_debe', 'dq_haber'))):
      if (!isset($totales[$col]))
        $totales[$col] = 0;
      $totales[$col] += $dato;
      ?>
                <td align="right"><?php echo moneda_local($dato) ?></td>
              <?php else: ?>
                <td><?php echo $dato ?></td>
              <?php endif ?>
            <?php endforeach ?>
          </tr>
          <?php endforeach ?>
      </tbody>
      <tfoot>
          <?php 
          if($header_analitico):
          foreach ($header_analitico as $col => $dato): ?>
            <?php if (isset($totales[$col])): ?>
          <th align="right"><?php echo moneda_local($totales[$col]) ?></th>
          <?php else: ?>
          <th>&nbsp;</th>
          <?php endif ?>
        <?php endforeach;
        endif
        ?>
      </tfoot>
    </table>
  </div>
</div>
<script src="jscripts/jquery.printElement.min.js" type="text/javascript"></script>
<script type="text/javascript">

	$('#edit_comprobante').unbind('click');
	$('#edit_comprobante').click(function() {
		pymerp.loadOverlay('<?php echo Factory::buildUrl('EditarComprobanteContable', 'contabilidad', 'loader', null, array(
		'dc_comprobante' => $dc_comprobante)); ?>');
  });

  $('#tabla_analitico')
          .tableExport()
          .tableOverflow(<?php echo json_encode($array_widths) ?>)
          .tableAdjust(10, undefined, 920);

  $('#detalle')
          .tableExport()
          .tableAdjust(10);

  $("table.sortable").tablesorter();

  pymerp.init($('#show_comprobante'));

  $("#print_version").click(function() {
    window.open("sites/contabilidad/ver_comprobante.php?id=<?php echo $dc_comprobante ?>", 'ver_comprobante', "width=800;height=600");
  });

  $('#null_comprobante').unbind('click');
  $('#null_comprobante').click(function() {
    loadOverlay("sites/contabilidad/null_comprobante.php?id=<?php echo $dc_comprobante ?>");
  });

  $('#auth_comprobante').unbind('click');
  $('#auth_comprobante').click(function() {
    loadOverlay("<?php echo Factory::buildUrl('AutorizarEditarComprobanteContable', 'contabilidad', null, null, array(
				'dc_comprobante' => $dc_comprobante)); ?>");
  });

  $(".actionbtn").click(function(e) {
    e.preventDefault();
    loadOverlay($(this).attr('href'));
  });

</script>