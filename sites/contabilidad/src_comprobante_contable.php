<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">Comprobante contable</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$datacuentas = $db->select("tb_cuenta_contable",
	"dc_cuenta_contable,dg_cuenta_contable,dg_codigo",
	"dc_empresa= {$empresa} AND dm_activo ='1'",
	array("order_by" => "dg_cuenta_contable"));
	$cuentas = array();
	foreach($datacuentas as $c){
		$cuentas[$c['dc_cuenta_contable']] = $c['dg_codigo']." ".$c['dg_cuenta_contable'];
	}
	
	$form->Start("sites/contabilidad/proc/src_comprobante_contable.php","src_comprobante");
	$form->Header("<strong>Indicar los parámetros de búsqueda del comprobante contable</strong>");
	$form->Text("Número de comprobante","comp_numero");
	echo("<br /><table align='center'><tr><td valign='top'>");
	$form->Text("Mes contable","comp_mes");
	echo("</td><td></td><td valign='top'>");
	$form->Text("Periodo contable (año)","comp_anho",1,4,date("Y"));
	echo("</td></tr><tr><td>");
	$form->Text("Responsable","comp_resp");
	$form->Text("Propuesta de pago a proveedor",'dq_propuesta');
	echo('<br /></td><td></td><td>');
	$form->Text('Saldo','comp_saldo');
	echo("<br /></td></tr><tr><td valign='bottom'>
	<label>Filtro: <input type='text' id='cuenta_filtro' size='32' /></label>");
	$form->Multiselect('
	<div class="right" style="position:relative;">
	<img src="images/manual.png" alt="" title="Agregar manual" id="manual_filter" />
	<div style="display:none;position:absolute;top:20px;right:-130px;background:#DDD;border:1px solid #BBB;">
		Ingrese los códigos de cuentas (una por linea)
		<textarea cols="30" rows="5"></textarea><br />
		<button type="button" class="addbtn" id="manual_add">Agregar</button>
		<button type="button" class="button">Cancelar</button>
	</div></div>
	','lista_cuentas',$cuentas);
	echo("</td><td valign='middle'><br />
	<button type='button' id='add_cuentas'>&raquo;</button><br />
	<button type='button' id='del_cuentas'>&laquo;</button>
	</td><td valign='bottom'>");
	$form->Multiselect('Que incluya estas cuentas contables','comp_cuentas');
	echo("</td></tr></table>
	<div id='omitted'></div><br />");
	echo("<label><input type='checkbox' name='comp_nulls' value='1' /> Incluir comprobantes anulados en los resultados</label><br />");
	$form->End("Buscar","searchbtn");
	
	echo("<select id='filtradas' style='display:none;'></select>");
?>
</div>
</div>
<script type="text/javascript">
$("#cuenta_filtro").keyup(function(){
	cri = $(this).val();
	$("#lista_cuentas option").each(function(){
		if($(this).text().toLowerCase().indexOf(cri) == -1){
			$(this).remove().appendTo("#filtradas");
		}
	});
	$("#filtradas option").each(function(){
		if($(this).text().toLowerCase().indexOf(cri) != -1){
			$(this).remove().appendTo("#lista_cuentas");
		}
	});
});

$("#add_cuentas").click(function(){
	$("#lista_cuentas option:selected").remove().appendTo("#comp_cuentas");
});
$("#del_cuentas").click(function(){
	$("#comp_cuentas option:selected").remove().appendTo("#lista_cuentas");
	$("#cuenta_filtro").trigger('keyup');
});

$(".searchbtn").click(function(e){
	$("#comp_cuentas option").attr("selected",true);
});

$("#manual_filter").click(function(){
	$(this).next().toggle();
});

$("#manual_filter").next().mouseenter(function(){
	t = this;
	$(document).click(function(){
    	$(t).hide();
		$(document).unbind('click');
	});
});

$("#manual_add").click(function(){
	manual_txt = $(this).prev().prev().val().split('\n');
	$("#lista_cuentas option").each(function(){
		code = $(this).text();
		code = code.substring(0,code.indexOf(' '));
		for(i in manual_txt){
			if(manual_txt[i] == code){
				$(this).remove().appendTo("#comp_cuentas");
				manual_txt.splice(i,1);
				return;
			}
		}
	});
	if(manual_txt.length){
		$("#omitted").addClass("warning").text("cuentas omitidas: "+manual_txt.toString());
	}
	$(this).parent().hide();
});

$("#manual_add").next().click(function(){
	$(this).parent().toggle();
});

$('#comp_resp').autocomplete('sites/proc/autocompleter/funcionario.php',
{
formatItem: function(row){
	return row[0]+" ( "+row[1]+" )";
},
minChars: 2,
width:300
}
);
</script>