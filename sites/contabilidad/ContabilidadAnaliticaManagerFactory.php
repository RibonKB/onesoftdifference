<?php

require_once('inc/Autocompleter.class.php');

class ContabilidadAnaliticaManagerFactory extends Factory{
  
  public $title = "Contabilidad Analítica";
  
  public function indexAction(){
    
    $request = self::getRequest();
    
    if(isset($request->detalle_analitico)){
        $selected = $this->organizaSelected($request->detalle_analitico);
        $nameMapping = array(
            'an_dc_factura_compra' => array('dc_factura_compra','.an_dq_factura_compra'),
            'an_dc_nota_venta' => array('dc_nota_venta','.an_dq_nota_venta'),
            'an_dc_orden_compra' => array('dc_orden_compra','.an_dq_orden_compra'),
            'an_dc_nota_credito_proveedor' => array('dc_nota_credito','.an_nota_credito_proveedor'),
            'an_dc_producto' => array('dc_producto','.an_dg_producto'),
            'an_dc_factura_venta' => array('dc_factura_venta','.an_dq_factura_venta'),
            'an_dc_orden_servicio' => array('dc_orden_servicio','.an_dq_orden_servicio'),
            'an_dc_guia_recepcion' => array('dc_guia_recepcion','.an_dq_guia_recepcion'),
            'an_dc_nota_credito_cliente' => array('dc_nota_credito_cliente','.an_nota_credito_cliente')
        );
    }else{
        $selected = null;
        $nameMapping = null;
    }
    
    $form = $this->getFormView($this->getTemplateURL('index.form'),array(
        'selected' => $selected,
        'mapping' => $nameMapping
    ));
    
    echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
  }
  
  public function testFormSendAction(){
    self::debug(self::getRequest());
  }
  
  public function acFacturaCompraAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $facturas = $db->prepare(
                  $db->select('tb_factura_compra fc
                      JOIN tb_proveedor p ON p.dc_proveedor = fc.dc_proveedor',
                  'fc.dc_factura, fc.dq_factura, fc.dq_folio, p.dg_razon',
                  'fc.dc_empresa = ? AND (fc.dq_folio LIKE ? OR fc.dq_factura LIKE ? OR p.dg_razon LIKE ? OR p.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $facturas->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $facturas->bindValue(2,$q,PDO::PARAM_STR);
    $facturas->bindValue(3,$q,PDO::PARAM_STR);
    $facturas->bindValue(4,$q,PDO::PARAM_STR);
    $facturas->bindValue(5,$q,PDO::PARAM_STR);
    $db->stExec($facturas);
    
    while($fc = $facturas->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$fc->dq_factura,
            $fc->dq_folio,
            $fc->dg_razon,
            $fc->dc_factura,
            'dc_factura_compra'
		));
	}
    
  }
  
  public function acNotaVentaAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $notas_venta = $db->prepare(
                  $db->select('tb_nota_venta nv
                      JOIN tb_cliente cl ON cl.dc_cliente = nv.dc_cliente
                      LEFT JOIN tb_tipo_nota_venta t ON t.dc_tipo = nv.dc_tipo_nota_venta',
                  'nv.dc_nota_venta, nv.dq_nota_venta, t.dg_tipo, cl.dg_razon',
                  'nv.dc_empresa = ? AND (nv.dq_nota_venta LIKE ? OR cl.dg_razon LIKE ? OR cl.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $notas_venta->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $notas_venta->bindValue(2,$q,PDO::PARAM_STR);
    $notas_venta->bindValue(3,$q,PDO::PARAM_STR);
    $notas_venta->bindValue(4,$q,PDO::PARAM_STR);
    $db->stExec($notas_venta);
    
    while($nv = $notas_venta->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$nv->dq_nota_venta,
            $nv->dg_tipo,
            $nv->dg_razon,
            $nv->dc_nota_venta,
            'dc_nota_venta'
		));
	}
  }
  
  public function acOrdenCompraAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $ordenes = $db->prepare(
                  $db->select('tb_orden_compra oc
                      JOIN tb_proveedor p ON p.dc_proveedor = oc.dc_proveedor',
                  'oc.dc_orden_compra, oc.dq_orden_compra, p.dg_razon',
                  'oc.dc_empresa = ? AND (oc.dq_orden_compra LIKE ? OR p.dg_razon LIKE ? OR p.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $ordenes->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $ordenes->bindValue(2,$q,PDO::PARAM_STR);
    $ordenes->bindValue(3,$q,PDO::PARAM_STR);
    $ordenes->bindValue(4,$q,PDO::PARAM_STR);
    $db->stExec($ordenes);
    
    while($oc = $ordenes->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$oc->dq_orden_compra,
            $oc->dg_razon,
            '',
            $oc->dc_orden_compra,
            'dc_orden_compra'
		));
	}
  }
  
  public function acNotaCreditoProveedorAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $notas_credito = $db->prepare(
                  $db->select('tb_nota_credito_proveedor nc
                      JOIN tb_proveedor p ON p.dc_proveedor = nc.dc_proveedor',
                  'nc.dc_nota_credito, nc.dq_nota_credito, nc.dq_folio, p.dg_razon',
                  'nc.dc_empresa = ? AND (nc.dq_folio LIKE ? OR nc.dq_nota_credito LIKE ? OR p.dg_razon LIKE ? OR p.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $notas_credito->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $notas_credito->bindValue(2,$q,PDO::PARAM_STR);
    $notas_credito->bindValue(3,$q,PDO::PARAM_STR);
    $notas_credito->bindValue(4,$q,PDO::PARAM_STR);
    $notas_credito->bindValue(5,$q,PDO::PARAM_STR);
    $db->stExec($notas_credito);
    
    while($nc = $notas_credito->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$nc->dq_nota_credito,
            $nc->dq_folio,
            $nc->dg_razon,
            $nc->dc_nota_credito,
            'dc_nota_credito'
		));
	}
  }
  
  public function acProductoAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $productos = $db->prepare(
                  $db->select('tb_producto p
                      JOIN tb_tipo_producto t ON t.dc_tipo_producto = p.dc_tipo_producto',
                  'p.dc_producto, p.dg_codigo, p.dg_producto, t.dg_tipo_producto',
                  'p.dc_empresa = ? AND (p.dg_codigo LIKE ? OR p.dg_producto LIKE ?)',
                  array('limit' => $limit)));
    $productos->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $productos->bindValue(2,$q,PDO::PARAM_STR);
    $productos->bindValue(3,$q,PDO::PARAM_STR);
    $db->stExec($productos);
    
    while($p = $productos->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$p->dg_codigo,
            $p->dg_tipo_producto,
            $p->dg_producto,
            $p->dc_producto,
            'dc_producto'
		));
	}
  }
  
  public function acFacturaVentaAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $facturas = $db->prepare(
                  $db->select('tb_factura_venta fv
                      JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente',
                  'fv.dc_factura, fv.dq_factura, fv.dq_folio, cl.dg_razon',
                  'fv.dc_empresa = ? AND (fv.dq_factura LIKE ? OR fv.dq_folio LIKE ? OR cl.dg_razon LIKE ? OR cl.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $facturas->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $facturas->bindValue(2,$q,PDO::PARAM_STR);
    $facturas->bindValue(3,$q,PDO::PARAM_STR);
    $facturas->bindValue(4,$q,PDO::PARAM_STR);
    $facturas->bindValue(5,$q,PDO::PARAM_STR);
    $db->stExec($facturas);
    
    while($fv = $facturas->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$fv->dq_factura,
            $fv->dq_folio,
            $fv->dg_razon,
            $fv->dc_factura,
            'dc_factura_venta'
		));
	}
  }
  
  public function acOrdenServicioAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $ordenes = $db->prepare(
                  $db->select('tb_orden_servicio os
                      JOIN tb_cliente cl ON cl.dc_cliente = os.dc_cliente',
                  'os.dc_orden_servicio, os.dq_orden_servicio, cl.dg_razon',
                  'os.dc_empresa = ? AND (os.dq_orden_servicio LIKE ? OR cl.dg_razon LIKE ? OR cl.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $ordenes->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $ordenes->bindValue(2,$q,PDO::PARAM_STR);
    $ordenes->bindValue(3,$q,PDO::PARAM_STR);
    $ordenes->bindValue(4,$q,PDO::PARAM_STR);
    $db->stExec($ordenes);
    
    while($os = $ordenes->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$os->dq_orden_servicio,
            $os->dg_razon,
            '',
            $os->dc_orden_servicio,
            'dc_orden_servicio'
		));
	}
  }
  
  public function acGuiaRecepcionAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $guias = $db->prepare(
                  $db->select('tb_guia_recepcion gr
                      JOIN tb_orden_compra oc ON oc.dc_orden_compra = gr.dc_orden_compra
                      JOIN tb_proveedor p ON p.dc_proveedor = oc.dc_proveedor',
                  'gr.dc_guia_recepcion, gr.dq_guia_recepcion, oc.dq_orden_compra, p.dg_razon',
                  'gr.dc_empresa = ? AND (gr.dq_guia_recepcion LIKE ? OR p.dg_razon LIKE ? OR p.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $guias->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $guias->bindValue(2,$q,PDO::PARAM_STR);
    $guias->bindValue(3,$q,PDO::PARAM_STR);
    $guias->bindValue(4,$q,PDO::PARAM_STR);
    $db->stExec($guias);
    
    while($gr = $guias->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$gr->dq_guia_recepcion,
            'oc: '.$gr->dq_orden_compra,
            $gr->dg_razon,
            $gr->dc_guia_recepcion,
            'dc_guia_recepcion'
		));
	}
  }
  
  public function acNotaCreditoClienteAction(){
    $q = '%'.self::getRequest()->q.'%';
    $limit = self::getRequest()->limit;
    
    $db = $this->getConnection();
    
    $notas_credito = $db->prepare(
                  $db->select('tb_nota_credito nc
                      JOIN tb_cliente cl ON cl.dc_cliente = nc.dc_cliente',
                  'nc.dc_nota_credito, nc.dq_nota_credito, nc.dq_folio, cl.dg_razon',
                  'nc.dc_empresa = ? AND (nc.dq_nota_credito LIKE ? OR nc.dq_folio LIKE ? OR cl.dg_razon LIKE ? OR cl.dg_rut LIKE ?)',
                  array('limit' => $limit)));
    $notas_credito->bindValue(1,$this->getEmpresa(),PDO::PARAM_INT);
    $notas_credito->bindValue(2,$q,PDO::PARAM_STR);
    $notas_credito->bindValue(3,$q,PDO::PARAM_STR);
    $notas_credito->bindValue(4,$q,PDO::PARAM_STR);
    $notas_credito->bindValue(5,$q,PDO::PARAM_STR);
    $db->stExec($notas_credito);
    
    while($nc = $notas_credito->fetch(PDO::FETCH_OBJ)){
		echo Autocompleter::getItemRow(array(
			$nc->dq_nota_credito,
            $nc->dq_folio,
            $nc->dg_razon,
            $nc->dc_nota_credito,
            'dc_nota_credito_cliente'
		));
	}
  }
  
  private function organizaSelected($s){
      $result = array();
      
      foreach($s as $campo => $data){
          $prefix = substr($campo, 0, 5);
          foreach($data as $i => $value){
            if(($prefix == 'an_dc' && $value != 0) || ($prefix != 'an_dc' && $value != '')){
              $result[$i][$campo] = $value;
            }
          }
      }
      
      return $result;
  }
  
}