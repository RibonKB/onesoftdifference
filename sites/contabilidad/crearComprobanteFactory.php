<?php

require_once('sites/contabilidad/CRUDComprobanteContableFactory.php');

class crearComprobanteFactory extends CRUDComprobanteContableFactory {

  public $title = "Crear Comprobante Contable";
  private $dc_comprobante;
  private $dq_comprobante;

  public function indexAction() {

    $form = $this->getFormView($this->getTemplateURL('index.form'));
    echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
  }

  public function crearAction() {

    $tipo_movimiento = $this->obtenerTipoMovimiento(self::getRequest()->dg_codigo_tipo_movimiento);

    $cuentas = $this->obtenerCuentasTipoMovimiento($tipo_movimiento->dc_tipo_movimiento);

    $form = $this->getFormView($this->getTemplateURL('crear.form'), array(
        'tipo_movimiento' => $tipo_movimiento,
        'cuentas' => $cuentas
    ));

    echo $this->getFullView($form, array(), Factory::STRING_TEMPLATE);
  }

  public function procesarCrearAction() {

    $request = self::getRequest();

    $this->mesContable = substr($request->df_contable_comprobante, 3, 2);
    $this->anhoContable = substr($request->df_contable_comprobante, -4);

    //Validar que esté disponible para el usuario actual 
    $this->validarPeriodoContable();

    //Validar que los vaores de DEBE y HABER cuadren
    $saldo = $this->validarSaldos();
	
	
	//Validar Cuentas Contables Existentes
	$this->validarCuentasContablesExistentes();

    $this->getConnection()->start_transaction();

    //Crear el comprobante contable
    $this->dc_comprobante = $this->insertarComprobanteContable($saldo);

    //Insertar los detalles financieros del comprobante, retornando la matriz de ids.
    $matriz_financiera = $this->insertarDetallesFinancieros();

    //Insertar detalles analíticos a aprtir de la matriz de ids financieros
    $this->insertarDetallesAnaliticos($matriz_financiera);
    $this->validaValores($this->dc_comprobante);
    $this->getConnection()->commit();

    $data = new stdClass();
    $data->dg_comprobante = $this->dq_comprobante;
    $data->dc_comprobante = $this->dc_comprobante;
    echo $this->getView('contabilidad/general/mostrar_comprobante_creado', array(
        'comprobante' => $data
    ));
  }
  
  private function validaValores($id_comp){
      $r=self::getRequest();
      $valores='';
      $get=$this->coneccion('select', 
              'tb_comprobante_contable_detalle_analitico A
               RIGHT JOIN tb_comprobante_contable_detalle B ON B.dc_detalle= A.dc_detalle_financiero', 
                  'A.dq_debe debe_an,
                   A.dq_haber haber_an,
                   B.dq_debe debe_fin,
                   B.dq_haber haber_fin', 
              "B.dc_comprobante={$id_comp}");
      $get=$get->fetchAll(PDO::FETCH_OBJ);
                $def=0;
                $haf=0;
                $dea=0;
                $haa=0;
                //$sf=0;
                //$sa=0;
                
               
                foreach($get as $d):
                    $def+=$d->debe_fin;
                    $haf+=$d->haber_fin;
                    $dea+=$d->debe_an;
                    $haa+=$d->haber_an;
                    endforeach;
       $db=$this->getConnection();
       if($def!=$haf || $def!=$dea || $def!=$haa){
           $db->rollBack();
           $this->getErrorMan()->showWarning("Existe un error en la cuadratura de los detalles, o de aviso a un ADMINISTRADOR Anexo 6806");
           
       }
  }

  private function validarSaldos() {
    $dq_saldo_debe = 0;
    $dq_saldo_haber = 0;

    $request = self::getRequest();
    $this->initFunctionsService();

    foreach ($request->dq_debe as $dq_debe) {
      $dq_saldo_debe += Functions::fixNumberToLocal(Functions::toNumber($dq_debe));
    }

    foreach ($request->dq_haber as $dq_haber) {
      $dq_saldo_haber += Functions::fixNumberToLocal(Functions::toNumber($dq_haber));
    }

    if ($dq_saldo_debe != $dq_saldo_haber) {
      $this->getErrorMan()->showWarning('Los saldos DEBE y HABER no coinciden, verifique estos valores y vuelva a intentarlo');
      exit;
    }

    return $dq_saldo_debe;
  }
  
  private function validarCuentasContablesExistentes(){
		$request = self::getRequest();
		foreach ($request->item_id as $i => $did) {
			if(!isset($request->dc_cuenta_contable[$i])){
				$this->getErrorMan()->showWarning('Una o más cuentas contables no han sido ingresadas correctamente. Verifique su información e intente nuevamente.');
				exit;
			}
		}
  }

  private function insertarComprobanteContable($saldo) {
    $this->dq_comprobante = ContabilidadStuff::getCorrelativoComprobante($this->getConnection(), $this->getEmpresa());
    $db = $this->getConnection();
    $request = self::getRequest();

    $comprobante = $db->prepare($db->insert('tb_comprobante_contable', array(
                'dg_comprobante' => '?',
                'dc_tipo_movimiento' => '?',
                'df_fecha_contable' => '?',
                'dc_mes_contable' => '?',
                'dc_anho_contable' => '?',
                'dg_glosa' => '?',
                'dq_saldo' => '?',
                'df_fecha_emision' => $db->getNow(),
                'dc_empresa' => $this->getEmpresa(),
                'dc_usuario_creacion' => $this->getUserData()->dc_usuario,
                'dg_numero_interno' => '?',
                'dm_tipo_comprobante' => 0
    )));
    $comprobante->bindValue(1, $this->dq_comprobante, PDO::PARAM_INT);
    $comprobante->bindValue(2, $request->dc_tipo_movimiento, PDO::PARAM_INT);
    $comprobante->bindValue(3, $db->sqlDate2($request->df_contable_comprobante), PDO::PARAM_STR);
    $comprobante->bindValue(4, $this->mesContable, PDO::PARAM_INT);
    $comprobante->bindValue(5, $this->anhoContable, PDO::PARAM_INT);
    $comprobante->bindValue(6, $request->dg_glosa_general, PDO::PARAM_STR);
    $comprobante->bindValue(7, $saldo, PDO::PARAM_STR);
    $comprobante->bindValue(8, $request->dg_numero_interno, PDO::PARAM_STR);
    $db->stExec($comprobante);

    return $db->lastInsertId();
  }

  private function insertarDetallesFinancieros() {
    $db = $this->getConnection();
    $request = self::getRequest();
    $matriz = array();

    $detalle = $db->prepare($db->insert('tb_comprobante_contable_detalle', array(
                'dc_comprobante' => '?',
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?'
    )));
    $detalle->bindValue(1, $this->dc_comprobante, PDO::PARAM_INT);
    $detalle->bindParam(2, $dc_cuenta_contable, PDO::PARAM_INT);
    $detalle->bindParam(3, $dq_debe, PDO::PARAM_STR);
    $detalle->bindParam(4, $dq_haber, PDO::PARAM_STR);
    $detalle->bindParam(5, $dg_glosa, PDO::PARAM_STR);

    foreach ($request->item_id as $i => $did) {
      $dc_cuenta_contable = $request->dc_cuenta_contable[$i];
      $dq_debe = Functions::fixNumberToLocal(Functions::toNumber($request->dq_debe[$i]));
      $dq_haber = Functions::fixNumberToLocal(Functions::toNumber($request->dq_haber[$i]));
      $dg_glosa = $request->dg_glosa[$i];

      $db->stExec($detalle);
      $matriz[] = array($did, $db->lastInsertId());
    }
    return $matriz;
  }

  private function insertarDetallesAnaliticos($matriz) {
    $db = $this->getConnection();
    $request = self::getRequest();
    $ddata = isset($request->detalle_analitico) ? $request->detalle_analitico: NULL ;

    $detalle = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico', array(
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?',
                'dc_detalle_financiero' => '?',
                'dc_factura_compra' => '?',
                'dc_factura_venta' => '?',
                'dc_nota_venta' => '?',
                'dc_orden_servicio' => '?',
                'dc_proveedor' => '?',
                'dc_tipo_proveedor' => '?',
                'dc_cliente' => '?',
                'dc_tipo_cliente' => '?',
                'dc_orden_compra' => '?',
                'dc_producto' => '?',
                'dc_cebe' => '?',
                'dc_ceco' => '?',
                'dc_marca' => '?',
                'dc_linea_negocio' => '?',
                'dc_tipo_producto' => '?',
                'dc_banco' => '?',
                'dc_banco_cobro' => '?',
                'dc_guia_recepcion' => '?',
                'dc_medio_pago_proveedor' => '?',
                'dc_medio_cobro_cliente' => '?',
                'dc_mercado_cliente' => '?',
                'dc_mercado_proveedor' => '?',
                'dc_nota_credito' => '?',
                'dc_nota_credito_proveedor' => '?',
                'dc_segmento' => '?',
                'dg_cheque' => '?',
                'df_cheque' => '?',
                'dc_bodega' => '?',
                'dc_ejecutivo' => '?'
    )));
    $detalle->bindParam(1, $dc_cuenta_contable, PDO::PARAM_INT);
    $detalle->bindParam(2, $dq_debe, PDO::PARAM_STR);
    $detalle->bindParam(3, $dq_haber, PDO::PARAM_STR);
    $detalle->bindParam(4, $dg_glosa, PDO::PARAM_STR);
    $detalle->bindParam(5, $dc_detalle_financiero, PDO::PARAM_INT);
    $detalle->bindParam(6, $dc_factura_compra, PDO::PARAM_INT);
    $detalle->bindParam(7, $dc_factura_venta, PDO::PARAM_INT);
    $detalle->bindParam(8, $dc_nota_venta, PDO::PARAM_INT);
    $detalle->bindParam(9, $dc_orden_servicio, PDO::PARAM_INT);
    $detalle->bindParam(10, $dc_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(11, $dc_tipo_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(12, $dc_cliente, PDO::PARAM_INT);
    $detalle->bindParam(13, $dc_tipo_cliente, PDO::PARAM_INT);
    $detalle->bindParam(14, $dc_orden_compra, PDO::PARAM_INT);
    $detalle->bindParam(15, $dc_producto, PDO::PARAM_INT);
    $detalle->bindParam(16, $dc_cebe, PDO::PARAM_INT);
    $detalle->bindParam(17, $dc_ceco, PDO::PARAM_INT);
    $detalle->bindParam(18, $dc_marca, PDO::PARAM_INT);
    $detalle->bindParam(19, $dc_linea_negocio, PDO::PARAM_INT);
    $detalle->bindParam(20, $dc_tipo_producto, PDO::PARAM_INT);
    $detalle->bindParam(21, $dc_banco, PDO::PARAM_INT);
    $detalle->bindParam(22, $dc_banco_cobro, PDO::PARAM_INT);
    $detalle->bindParam(23, $dc_guia_recepcion, PDO::PARAM_INT);
    $detalle->bindParam(24, $dc_medio_pago_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(25, $dc_medio_cobro_cliente, PDO::PARAM_INT);
    $detalle->bindParam(26, $dc_mercado_cliente, PDO::PARAM_INT);
    $detalle->bindParam(27, $dc_mercado_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(28, $dc_nota_credito, PDO::PARAM_INT);
    $detalle->bindParam(29, $dc_nota_credito_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(30, $dc_segmento, PDO::PARAM_INT);
    $detalle->bindParam(31, $dg_cheque, PDO::PARAM_STR);
    $detalle->bindParam(32, $df_cheque, PDO::PARAM_STR);
    $detalle->bindParam(33, $dc_bodega, PDO::PARAM_INT);
    $detalle->bindParam(34, $dc_ejecutivo, PDO::PARAM_INT);

    foreach ($matriz as $did => $fdata) {
      $dc_cuenta_contable = $request->dc_cuenta_contable[$did];
      if (!isset($ddata[$fdata[0]])) {
        $dq_debe = Functions::fixNumberToLocal(Functions::toNumber($request->dq_debe[$did]));
        $dq_haber = Functions::fixNumberToLocal(Functions::toNumber($request->dq_haber[$did]));
        ;
        $dg_glosa = $request->dg_glosa[$did];
        $dc_detalle_financiero = $fdata[1];
        $dc_factura_compra = NULL;
        $dc_factura_venta = NULL;
        $dc_nota_venta = NULL;
        $dc_orden_servicio = NULL;
        $dc_proveedor = NULL;
        $dc_tipo_proveedor = NULL;
        $dc_cliente = NULL;
        $dc_tipo_cliente = NULL;
        $dc_orden_compra = NULL;
        $dc_producto = NULL;
        $dc_cebe = NULL;
        $dc_ceco = NULL;
        $dc_marca = NULL;
        $dc_linea_negocio = NULL;
        $dc_tipo_producto = NULL;
        $dc_banco = NULL;
        $dc_banco_cobro = NULL;
        $dc_guia_recepcion = NULL;
        $dc_medio_pago_proveedor = NULL;
        $dc_medio_cobro_cliente = NULL;
        $dc_mercado_cliente = NULL;
        $dc_mercado_proveedor = NULL;
        $dc_nota_credito = NULL;
        $dc_nota_credito_proveedor = NULL;
        $dc_segmento = NULL;
        $dg_cheque = NULL;
        $df_cheque = NULL;
        $dc_bodega = NULL;
        $dc_ejecutivo = NULL;

        $db->stExec($detalle);
      } else {
        foreach ($ddata[$fdata[0]]['an_dq_debe'] as $i => $dq_debe) {

          $dq_debe = Functions::fixNumberToLocal(Functions::toNumber($ddata[$fdata[0]]['an_dq_debe'][$i]));
          $dq_haber = Functions::fixNumberToLocal(Functions::toNumber($ddata[$fdata[0]]['an_dq_haber'][$i]));

          $this->nullify($ddata[$fdata[0]]);

          $dg_glosa = $ddata[$fdata[0]]['an_dg_glosa'][$i];
          $dc_detalle_financiero = $fdata[1];
          $dc_factura_compra = $ddata[$fdata[0]]['an_dc_factura_compra'][$i];
          $dc_factura_venta = $ddata[$fdata[0]]['an_dc_factura_venta'][$i];
          $dc_nota_venta = $ddata[$fdata[0]]['an_dc_nota_venta'][$i];
          $dc_orden_servicio = $ddata[$fdata[0]]['an_dc_orden_servicio'][$i];
          $dc_proveedor = $ddata[$fdata[0]]['an_dc_proveedor'][$i];
          $dc_tipo_proveedor = $ddata[$fdata[0]]['an_dc_tipo_proveedor'][$i];
          $dc_cliente = $ddata[$fdata[0]]['an_dc_cliente'][$i];
          $dc_tipo_cliente = $ddata[$fdata[0]]['an_dc_tipo_cliente'][$i];
          $dc_orden_compra = $ddata[$fdata[0]]['an_dc_orden_compra'][$i];
          $dc_producto = $ddata[$fdata[0]]['an_dc_producto'][$i];
          $dc_cebe = $ddata[$fdata[0]]['an_dc_cebe'][$i];
          $dc_ceco = $ddata[$fdata[0]]['an_dc_ceco'][$i];
          $dc_marca = $ddata[$fdata[0]]['an_dc_marca'][$i];
          $dc_linea_negocio = $ddata[$fdata[0]]['an_dc_linea_negocio'][$i];
          $dc_tipo_producto = $ddata[$fdata[0]]['an_dc_tipo_producto'][$i];
          $dc_banco = $ddata[$fdata[0]]['an_dc_banco'][$i];
          $dc_banco_cobro = $ddata[$fdata[0]]['an_dc_banco_cobro'][$i];
          $dc_guia_recepcion = $ddata[$fdata[0]]['an_dc_guia_recepcion'][$i];
          $dc_medio_pago_proveedor = $ddata[$fdata[0]]['an_dc_medio_pago_proveedor'][$i];
          $dc_medio_cobro_cliente = $ddata[$fdata[0]]['an_dc_medio_cobro_cliente'][$i];
          $dc_mercado_cliente = $ddata[$fdata[0]]['an_dc_mercado_cliente'][$i];
          $dc_mercado_proveedor = $ddata[$fdata[0]]['an_dc_mercado_proveedor'][$i];
          $dc_nota_credito = $ddata[$fdata[0]]['an_dc_nota_credito_cliente'][$i];
          $dc_nota_credito_proveedor = $ddata[$fdata[0]]['an_dc_nota_credito_proveedor'][$i];
          $dc_segmento = $ddata[$fdata[0]]['an_dc_segmento'][$i];
          $dg_cheque = $ddata[$fdata[0]]['an_dg_cheque'][$i];
          $df_cheque = $ddata[$fdata[0]]['an_df_cheque'][$i];
          $dc_bodega = $ddata[$fdata[0]]['an_dc_bodega'][$i];
          $dc_ejecutivo = $ddata[$fdata[0]]['an_dc_ejecutivo'][$i];

          $db->stExec($detalle);
        }
      }
    }
  }

  private function nullify(&$collection) {
    foreach ($collection as &$c) {
      foreach ($c as &$item) {
        if ($item == false) {
          $item = NULL;
        }
      }
    }
  }

  private function obtenerTipoMovimiento($dg_codigo) {
    $db = $this->getConnection();

    $tipo = $db->prepare($db->select('tb_tipo_movimiento', 'dc_tipo_movimiento, dg_tipo_movimiento', 'dg_codigo = ? AND dc_empresa = ? AND dm_activo = 1'));
    $tipo->bindValue(1, $dg_codigo, PDO::PARAM_STR);
    $tipo->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($tipo);
    $tipo = $tipo->fetch(PDO::FETCH_OBJ);

    if ($tipo === false) {
      $this->getErrorMan()->showErrorRedirect("No se encontró el tipo de movimiento especificado, intentelo nuevamente.", self::buildActionUrl('index'));
    }

    return $tipo;
  }

  public function tipoMovimientoAutoCompletarAction() {
    require_once('inc/Autocompleter.class.php');

    $request = Factory::getRequest();
    $db = $this->getConnection();

    $q = $request->q;
    $limit = $request->limit;

    $data = $db->prepare($db->select('tb_tipo_movimiento', 'dg_tipo_movimiento, dg_codigo, dg_descripcion', 'dc_empresa = ? AND dm_activo = 1 AND (dg_tipo_movimiento LIKE ? OR dg_codigo LIKE ? OR dg_descripcion LIKE ?)', array('limit' => $limit)));
    $data->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
    $data->bindValue(2, '%' . $q . '%', PDO::PARAM_STR);
    $data->bindValue(3, '%' . $q . '%', PDO::PARAM_STR);
    $data->bindValue(4, '%' . $q . '%', PDO::PARAM_STR);
    $db->stExec($data);

    while ($fv = $data->fetch(PDO::FETCH_OBJ)) {
      echo Autocompleter::getItemRow(array(
          $fv->dg_codigo,
          $fv->dg_tipo_movimiento,
          $fv->dg_descripcion
      ));
    }
  }

}
