<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

?>
<div id="secc_bar">Balance General</div>
<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once('../../../inc/form-class.php');
	$form = new Form($empresa);
	
	$form->Header('<b>Indique los filtros para el informe</b><br />Los campos marcados con [*] son obligatorios');
	$form->Button('Aplicar variante guardada','id="opt_load"','btnfilter_apply');
	$form->Button('Guardar variante','id="opt_save"','btnfilter_save');
	$form->Button('Más filtros','id="opt_more"','btnfilter_add');
	
	echo('<div style="display:none;" id="filter_more">
	<table class="tab" width="100%" style="text-align:left;">
	<caption>Indicar los filtros extra que quiere aplicar a la búsqueda</caption>
	<tr>
		<td><label><input type="checkbox" class="flt_tipo_mov" />Tipo de movimiento</label></td>
		<td><label><input type="checkbox" class="flt_glosa_head" />Glosa de cabecera</label></td>
		<td><label><input type="checkbox" class="flt_emision" />Fecha de emisión</label></td>
	</tr><tr>
		<td><label><input type="checkbox" class="flt_responsable" />Responsable</label></td>
		<td><label><input type="checkbox" class="flt_glosa_det" />Glosa de detalle</label></td>
		<td><label><input type="checkbox" class="flt_tipo_doc" />Tipo de documento</label></td>
	</tr><tr>
		<td><label><input type="checkbox" class="flt_cheque" />Cheque</label></td>
		<td><label><input type="checkbox" class="flt_rut" />RUT</label></td>
		<td><label><input type="checkbox" class="flt_act_fijo" />Activo fijo</label></td>
	</tr><tr>
		<td><label><input type="checkbox" class="flt_banco" />Banco</label></td>
		<td><label><input type="checkbox" class="flt_cebe" />Centro de beneficio</label></td>
		<td><label><input type="checkbox" class="flt_ceco" />Centro de costo</label></td>
	</tr>
	<tr>
		<td><label><input type="checkbox" class="flt_doc_compra" />Documento de compra</label></td>
		<td><label><input type="checkbox" class="flt_doc_venta" />Documento de venta</label></td>
		<td>&nbsp;</td>
	</tr></table>
	</div>');
	
	$form->Start('sites/contabilidad/informes/proc/src_balance_general.php','src_balance_general','cValidar');
	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td>Cuentas contables</td><td>');
	$form->Text('Desde','inf_cta_desde');
	echo('</td><td>');
	$form->Text('Hasta','inf_cta_hasta');
	echo('</td></tr><tr><td>Mes contable</td><td>');
	$form->Text('Desde','inf_mes_desde',0,2);
	echo('</td><td>');
	$form->Text('Hasta','inf_mes_hasta',0,2);
	echo('</td></tr><tr><td>Periodo contable (año)</td><td>');
	$form->Text('Desde','inf_anho_desde',1,4,date('Y'));
	echo('</td><td>');
	$form->Text('Hasta','inf_anho_hasta',0,4);
	echo('</td></tr><tr><td>Comparar con años</td><td>');
	$form->Text('Desde','inf_compare_desde',0,4);
	echo('</td><td>');
	$form->Text('Hasta','inf_compare_hasta',0,4);
	echo('</td></tr><tr><td>&nbsp;</td><td colspan="2">');
	echo('<br /><label><input type="checkbox" name="inf_todas_cuentas" value="1" />Mostrar cuentas sin movimientos</label>');
	echo('</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
	
	echo('<table id="filtered" style="display:none;">
	<tr class="flt_tipo_mov"><td>&nbsp;</td><td>');
	$form->Listado('Tipo de movimiento','inf_tipo_mov','tb_tipo_movimiento',array('dc_tipo_movimiento','dg_tipo_movimiento'),1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_glosa_head"><td>&nbsp;</td><td>');
	$form->Text('Glosa de cabecera','inf_glosa_head',1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_emision"><td>Fecha emisión</td><td>');
	$form->Date('Desde','inf_emision_desde',1,date('d/m/Y'));
	echo('</td><td>');
	$form->Date('Hasta','inf_emision_hasta');
	echo('</td></tr><tr class="flt_responsable"><td>&nbsp;</td><td>');
	$form->Text('Responsable','inf_responsable',1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_glosa_det"><td>&nbsp;</td><td>');
	$form->Text('Glosa de detalle','inf_glosa_det',1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_tipo_doc"><td>&nbsp;</td><td>');
	$form->Listado('Tipo de documento','inf_tipo_doc','tb_tipo_documento',array('dc_tipo_documento','dg_tipo_documento'),1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_cheque"><td>Cheque</td><td>');
	$form->Text('Desde','inf_cheque_desde',1);
	echo('</td><td>');
	$form->Text('Hasta','inf_cheque_hasta');
	echo('</td></tr><tr class="flt_rut"><td>&nbsp;</td><td>');
	$form->Text('Rut','inf_rut',1,255,'','inputrut');
	echo('</td><td>&nbsp;</td></tr><tr class="flt_act_fijo"><td>&nbsp;</td><td>');
	$form->Text('Activo fijo','inf_act_fijo',1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_banco"><td>&nbsp;</td><td>');
	$form->Listado('Banco','inf_banco','tb_banco',array('dc_banco','dg_banco'),1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_cebe"><td>&nbsp;</td><td>');
	$form->Listado('Centro beneficio','inf_cebe','tb_cebe',array('dc_cebe','dg_cebe'),1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_ceco"><td>&nbsp;</td><td>');
	$form->Listado('Centro costo','inf_ceco','tb_ceco',array('dc_ceco','dg_ceco'),1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_doc_compra"><td>&nbsp;</td><td>');
	$form->Text('Documento de compra','inf_doc_compra',1);
	echo('</td><td>&nbsp;</td></tr><tr class="flt_doc_venta"><td>&nbsp;</td><td>');
	$form->Text('Documento de venta','inf_doc_venta',1);
	echo('</td><td>&nbsp;</td></tr></table>');
?>
</div>
</div>
<script type="text/javascript">
	$("#opt_more").toggle(function(){
		$("#filter_more").show();
	},function(){
		$("#filter_more").hide();
	});
	
	$("#filter_more input[type=checkbox]").click(function(){
		cls = $(this).attr('class');
		if($(this).attr('checked')){
			$("#filtered ."+cls).appendTo("#form_container");
		}else{
			$("#form_container ."+cls).appendTo("#filtered");
		}
	});
	
	$('#opt_save').click(function(){
		$(".flt_tipo_mov").trigger('change');
	});
	
	$("#inf_cta_desde, #inf_cta_hasta").autocomplete('sites/proc/autocompleter/cuenta_contable.php',
	{
		formatItem: function(row){
			return row[0]+" "+row[1]+" - "+row[2];
		},
		minChars: 2,
		width:300
	});
	
	$("#inf_responsable").autocomplete('sites/proc/autocompleter/usuario.php',
	{
		formatItem: function(row){
			return row[0]+" "+row[1]+" - "+row[2];
		},
		minChars: 2,
		width:300
	});
</script>