<?php
class CentralizacionComprasFactory extends Factory{

  private $anho, $mes, $documentos, $total_neto, $total_exento, $total_iva, $total,
          $cuentaIva, $templateAnalitico = null, $detalleFinanciero, $df_fecha_contable = null,
          $dc_comprobante, $dc_detalle_financiero, $dc_centralizacion, $dq_centralizacion;
  protected $title = "Centralización Libro de Compras";

  public function indexAction(){
    $form = $this->getFormView($this->getTemplateURL('index.form'),array(
      'periodo' => $this->getFormMesPeriodo(),
      'periodoMesDefault' => date('m'),
      'periodoAnhoDefault' => date('Y')
    ));

    echo $this->getFullview($form,array(),Factory::STRING_TEMPLATE);

  }

  public function procesarBusquedaAction(){
    $this->initFormData();
    $this->initFunctionsService();

    echo $this->getView($this->getTemplateURL('resultados'),array(
      'data' => $this->documentos,
      'total_neto' => $this->total_neto,
      'total_exento' => $this->total_exento,
      'total_iva' => $this->total_iva,
      'total' => $this->total,
      'mes' => $this->mes,
      'anho' => $this->anho
    ));

  }

  public function centralizarAction(){
    $this->initCentralizacion();
    $this->initFunctionsService();
    $asientos_fc_analiticos = $this->getFCService()->getAsientosAnaliticos($this->getTemplateDetalleAnalitico(), $this->cuentaIva);
    $asientos_nc_analiticos = $this->getNCService()->getAsientosAnaliticos($this->getTemplateDetalleAnalitico(), $this->cuentaIva);
    $this->detalleFinanciero = array(array(),array(),array());
    $this->setAsientosFinancieros($asientos_fc_analiticos);
    $this->setAsientosFinancieros($asientos_nc_analiticos);

    if($this->df_fecha_contable == null):
      $this->mostrarComprobantePreliminar($asientos_fc_analiticos, $asientos_nc_analiticos);
    else:
      $this->insertarComprobanteCentralizacion($asientos_fc_analiticos, $asientos_nc_analiticos);
    endif;

  }

  private function getFormMesPeriodo(){
    return array(
      1 =>  "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
            "Septiembre", "Octubre", "Noviembre", "Diciembre"
    );
  }

  private function initFormData(){
    $r = self::getRequest();

    $this->initPeriodo($r);
    $this->initProveedores($r);
    $this->initEjecutivos($r);
    $this->initDocumentos($r);
    $this->initTotales();
  }

  private function initPeriodo($r){
    //Validar Año
    if(!isset($r->dc_anho) or preg_match("/^(19|20)\d\d$/",$r->dc_anho) == 0){
    	$this->getErrorMan()->showWarning("El año ingresado es incorrecto y no cumple con el formato esperado, compruebe el dato e inténtelo de nuevo.");
    	exit;
    }
    $this->anho = $r->dc_anho = intval($r->dc_anho);

    //Validar Mes
    if(!isset($r->dc_mes) or preg_match("/^[0|1]?\d$/",$r->dc_mes) == 0 or !intval($r->dc_mes) or intval($r->dc_mes) > 12){
    	$this->getErrorMan()->showWarning("El mes ingresado es incorrecto y no cumple con el formato esperado, compruebe el dato e inténtelo de nuevo.");
    	exit;
    }
    $this->mes = $r->dc_mes = intval($r->dc_mes);

  }

  private function initDocumentos($r){
    $db = $this->getConnection();
    $fc = $this->getFCService();
    $nc = $this->getNCService();

    $this->documentos = $db->doQuery(
  		"(".$db->select(
            $fc->getTables($r),
            $fc->getCampos($r),
            $fc->getCondiciones($r),
            array('group_by' => 'fc.dc_proveedor, fc.dq_folio')).")".
  		"UNION ALL".
  		"(".$db->select(
            $nc->getTables($r),
            $nc->getCampos($r),
            $nc->getcondiciones($r),
            array('group_by' => 'nc.dc_proveedor, nc.dq_folio')).")".
  		" ORDER BY df_emision"
  	)->fetchAll(PDO::FETCH_OBJ);

    if(count($this->documentos) == 0):
      $this->getErrorMan()->showAviso("No se han encontrado documentos sin centralizar en el periodo seleccionado.");
      exit;
    endif;

  }

  private function initProveedores($r){

    if(isset($r->dc_proveedor)):

      foreach ($r->dc_proveedor as &$p):
        if(empty($p) or !preg_match('/^\d+$/',$p)):
          $this->getErrorMan()->showWarning("Indicó proveedores de manera incorrecta,".
                                            " verifique los datos de entrada y vuelva a intentarlo.");
          exit;
        endif;

        $p = intval($p);

      endforeach;

    endif;

  }

  private function initEjecutivos($r){
    if(isset($r->dc_ejecutivo)):

      foreach ($r->dc_ejecutivo as &$p):
        if(empty($p) or !preg_match('/^\d+$/',$p)):
          $this->getErrorMan()->showWarning("Indicó ejecutivos de manera incorrecta,".
                                            " verifique los datos de entrada y vuelva a intentarlo.");
          exit;
        endif;

        $p = intval($p);

      endforeach;

    endif;
  }

  private function initTotales(){
    $this->total_neto = 0;
    $this->total_exento = 0;
    $this->total_iva = 0;
    $this->total = 0;

    foreach($this->documentos as &$doc):
      $this->total_exento += $doc->dq_exento;
    	$this->total_neto += $doc->dq_neto;
    	$this->total_iva += $doc->dq_iva;
    	$this->total += $doc->dq_total;
    endforeach;

  }

  private function getFCService(){
    return $this->getService('CentralizacionFacturaCompra');
  }

  private function getNCService(){
    return $this->getService('CentralizacionNotaCreditoProveedor');
  }

  private function initCentralizacion(){
    $r = self::getRequest();

    if(
      (!isset($r->dc_documento['FC']) or empty($r->dc_documento['FC']))
      and
      (!isset($r->dc_documento['NC']) or empty($r->dc_documento['NC']))):
        $this->getErrorMan()->showWarning("Debe indicar al menos una Factura de Compra o".
                                          " Nota de Crédito para iniciar el proceso de Centralización");
        exit;
    endif;

    //$this->initPeriodo($r);
    $this->getFCService()->initCentralizacion($r);
    $this->getNCService()->initCentralizacion($r);
    $this->initCuentaIvaCredito();
    $this->initTipoMovimiento();

    if(isset($r->df_fecha_contable)):
      $this->df_fecha_contable = $this->getConnection()->sqlDate2($r->df_fecha_contable);
    endif;
  }

  private function initCuentaIvaCredito(){
    $conf = $this->getComprobanteContableService()->getConfiguracionContabilidad();
    $iva = $conf->dc_cuenta_contable_iva_credito;

    if(empty($iva) or !preg_match("/^\d+$/",$iva)):
      $this->getErrorMan()->showWarning("La cuenta contable para asignar IVA Crédito no ha sido configurada,".
                                        " verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    $this->cuentaIva = $this->getConnection()->getRowById('tb_cuenta_contable',$iva, 'dc_cuenta_contable');

    if($this->cuentaIva === false):
      $this->getErrorMan()->showWarning("La cuenta contable configurada para IVA Crédito no pudo ser obtenida,".
                                        " verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    $this->cuentaIva->dq_porcentaje_iva = floatval($this->getParametrosEmpresa()->dq_iva);

  }

  private function initTipoMovimiento(){
    $conf = $this->getComprobanteContableService()->getConfiguracionContabilidad();
    $tipo = $conf->dc_tipo_movimiento_centralizacion_compra;

    if(empty($tipo) or !preg_match("/^\d+$/",$tipo)):
      $this->getErrorMan()->showWarning("El tipo de movimiento contable para la centralización".
                                        " de compras no ha sido configurado,".
                                        " verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    $this->tipoMovimiento = $this->getConnection()->getRowById('tb_tipo_movimiento',$tipo, 'dc_tipo_movimiento');

    if($this->tipoMovimiento === false):
      $this->getErrorMan()->showWarning("El tipo de movimiento contable no pudo ser obtenido,".
                                        " verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;
  }

  private function getComprobanteContableService(){
    return $this->getService('ComprobanteContable');
  }

  private function getTemplateDetalleAnalitico(){
    if($this->templateAnalitico != null):
      return $this->templateAnalitico;
    endif;

    $this->templateAnalitico = (object) $this->getComprobanteContableService()->getTemplateDetalleAnalitico();

    return $this->templateAnalitico;

  }

  private function setAsientosFinancieros($analitico){
    foreach($analitico as $i => $cuentas):
      foreach($cuentas as $dc_cuenta_contable => $cuenta):
        foreach($cuenta as $a):
          if(!isset($this->detalleFinanciero[$i][$dc_cuenta_contable])):
            $this->detalleFinanciero[$i][$dc_cuenta_contable] = $this->getTemplateDetalleFinanciero();
            $this->detalleFinanciero[$i][$dc_cuenta_contable]->dc_cuenta_contable = $a->dc_cuenta_contable;
            switch($i):
              case 0:
                $this->detalleFinanciero[$i][$dc_cuenta_contable]->dg_glosa = "Centralización Compras";
                break;
              case 1:
                $this->detalleFinanciero[$i][$dc_cuenta_contable]->dg_glosa = "IVA Centralización Compras";
                break;
              case 2:
                $this->detalleFinanciero[$i][$dc_cuenta_contable]->dg_glosa = "Cargo Proveedor Centralización Compras";
                break;
            endswitch;
          endif;

          $this->detalleFinanciero[$i][$dc_cuenta_contable]->dq_debe += $a->dq_debe;
          $this->detalleFinanciero[$i][$dc_cuenta_contable]->dq_haber += $a->dq_haber;
        endforeach;
      endforeach;
    endforeach;
  }

  private function getTemplateDetalleFinanciero(){
    return (object) $this->getComprobanteContableService()->getTemplateDetalleFinanciero();
  }

  private function mostrarComprobantePreliminar($fc_analiticos, $nc_analiticos){
    $this->setDetalleFinancieroCompleto();
    $this->setDetalleAnaliticoCompleto($fc_analiticos);
    $this->setDetalleAnaliticoCompleto($nc_analiticos);

    $form = $this->getFormView($this->getTemplateURL('obtenerDatosComprobante.form'));

    echo $this->getOverlayView($this->getTemplateURL('showComprobantePreliminar'),array(
      'form' => $form,
      'financieros' => $this->detalleFinanciero,
      'fc_analiticos' => $fc_analiticos,
      'nc_analiticos' => $nc_analiticos
    ));
  }

  private function setDetalleFinancieroCompleto(){
    $db = $this->getConnection();
    foreach($this->detalleFinanciero as $df):
      foreach($df as $dc_cuenta_contable => &$d):
        $d->cuenta = $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable');
      endforeach;
    endforeach;
  }

  private function setDetalleAnaliticoCompleto(&$detalles){
    $db = $this->getConnection();
    foreach($detalles as $d):
      foreach($d as $dc_cuenta_contable => $asientos):
        foreach($asientos as &$a):
          $a->cuenta = $db->getRowById('tb_cuenta_contable',$dc_cuenta_contable,'dc_cuenta_contable');
          $a->proveedor = $db->getRowById('tb_proveedor',$a->dc_proveedor,'dc_proveedor');
          $a->producto = $db->getRowById('tb_producto',$a->dc_producto,'dc_producto');
          if(isset($a->dc_cliente) and $a->dc_cliente):
            $a->cliente = $db->getRowById('tb_cliente',$a->dc_cliente,'dc_cliente');
          endif;
          if(isset($a->dc_ejecutivo) and $a->dc_ejecutivo):
            $a->ejecutivo = $db->getRowById('tb_funcionario',$a->dc_ejecutivo,'dc_funcionario');
          endif;
          if(isset($a->dc_factura_compra) and $a->dc_factura_compra):
            $a->factura = $db->getRowById('tb_factura_compra',$a->dc_factura_compra,'dc_factura');
          endif;
          if(isset($a->dc_nota_credito_proveedor) and $a->dc_nota_credito_proveedor):
            $a->nota_credito = $db->getRowById('tb_nota_credito_proveedor',$a->dc_nota_credito_proveedor,'dc_nota_credito');
          endif;
        endforeach;
      endforeach;
    endforeach;
  }

  private function insertarComprobanteCentralizacion($fc_analiticos, $nc_analiticos){

    $this->getConnection()->start_transaction();
      $this->insertarCabeceraComprobante();
      $this->insertarDetallesFinancieros();
      $this->insertarDetallesAnaliticos($fc_analiticos, $nc_analiticos);

      $this->insertarCentralizacion();
      $this->insertarDetallesCentralizacion();

      $this->cambiarEstadoDocumentos();
    $this->getConnection()->commit();

    echo $this->getView($this->getTemplateURL('datosCreacion'),array(
      'dq_centralizacion' => $this->dq_centralizacion
    ));

    echo $this->getComprobanteContableService()->getStandarCreationOutput();

  }

  private function insertarCabeceraComprobante(){
    $cc = $this->getComprobanteContableService();

    $cc->prepareComprobante($this->tipoMovimiento->dc_tipo_movimiento, $this->df_fecha_contable, 5);
    $cc->setComprobanteParam('dg_glosa',"Centralización de Libro de Compras {$this->df_fecha_contable}");
    $cc->setComprobanteParam('dq_saldo',$this->getSaldoComprobante());

    $cc->ComprobantePersist();

    $data = $cc->getComprobanteInfo();
    $this->dc_comprobante = $data->dc_comprobante;

  }

  private function getSaldoComprobante(){
    $saldo = 0;
    foreach($this->detalleFinanciero as $d):
      foreach($d as $a):
        $saldo += $a->dq_debe;
      endforeach;
    endforeach;

    return $saldo;
  }

  private function insertarDetallesFinancieros(){
    $cc = $this->getComprobanteContableService();
    $cc->prepareDetalleFinanciero();
    $this->dc_detalle_financiero = array(array(),array(),array());

    foreach($this->detalleFinanciero as $i => $d):
      foreach($d as $dc_cuenta_contable => $a):
        $cc->setParametroFinanciero('dc_cuenta_contable',$dc_cuenta_contable);
        $cc->setParametroFinanciero('dg_glosa',$a->dg_glosa);

        if($a->dq_debe > 0):
          $cc->setParametroFinanciero('dq_debe',$a->dq_debe);
          $cc->setParametroFinanciero('dq_haber',0);
          $this->dc_detalle_financiero[$i]["debe_".$dc_cuenta_contable] = $cc->DetalleFinancieroPersist();
        endif;

        if($a->dq_haber > 0):
          $cc->setParametroFinanciero('dq_debe',0);
          $cc->setParametroFinanciero('dq_haber',$a->dq_haber);
          $this->dc_detalle_financiero[$i]["haber_".$dc_cuenta_contable] = $cc->DetalleFinancieroPersist();
        endif;

      endforeach;
    endforeach;
  }

  private function insertarDetallesAnaliticos($fc_analiticos, $nc_analiticos){
    $cc = $this->getComprobanteContableService();
    $cc->prepareDetalleAnalitico();

    foreach($fc_analiticos as $i => $d):

      foreach($fc_analiticos[$i] as $dc_cuenta_contable => $asientos):
        foreach($asientos as $a):
          if($a->dq_debe > 0):
            $a->dc_detalle_financiero = $this->dc_detalle_financiero[$i]["debe_".$dc_cuenta_contable];
          else:
            $a->dc_detalle_financiero = $this->dc_detalle_financiero[$i]["haber_".$dc_cuenta_contable];
          endif;
          foreach($a as $paramName => $value):
            $cc->setParametroAnalitico($paramName, $value);
          endforeach;

          $cc->DetalleAnaliticoPersist();
        endforeach;
      endforeach;

      foreach($nc_analiticos[$i] as $dc_cuenta_contable => $asientos):
        foreach($asientos as $a):
          if($a->dq_debe > 0):
            $a->dc_detalle_financiero = $this->dc_detalle_financiero[$i]["debe_".$dc_cuenta_contable];
          else:
            $a->dc_detalle_financiero = $this->dc_detalle_financiero[$i]["haber_".$dc_cuenta_contable];
          endif;
          foreach($a as $paramName => $value):
            $cc->setParametroAnalitico($paramName, $value);
          endforeach;

          $cc->DetalleAnaliticoPersist();
        endforeach;
      endforeach;

    endforeach;

  }

  private function insertarCentralizacion(){
    $db = $this->getConnection();
    $this->dq_centralizacion = $this->getService('DocumentCreation')
                         ->getCorrelativo('tb_centralizacion_compra', 'dq_centralizacion');

    $insert = $db->prepare($db->insert('tb_centralizacion_compra',array(
      "dq_centralizacion" => '?',
      "dc_comprobante_contable" => '?',
      "df_emision" => '?',
      "df_creacion" => $db->getNow(),
      "dc_usuario_creacion" => $this->getUserData()->dc_usuario,
      "dc_empresa" => $this->getempresa(),
      "dm_nula" => 0
    )));
    $insert->bindValue(1, $this->dq_centralizacion, PDO::PARAM_INT);
    $insert->bindValue(2, $this->dc_comprobante, PDO::PARAM_INT);
    $insert->bindValue(3, $this->df_fecha_contable, PDO::PARAM_STR);
    $db->stExec($insert);

    $this->dc_centralizacion = $db->lastInsertId();
  }

  private function insertarDetallesCentralizacion(){
    $this->getFCService()->insertarDetallesCentralizacion($this->dc_centralizacion);
    $this->getNCService()->insertarDetallesCentralizacion($this->dc_centralizacion);
  }

  private function cambiarEstadoDocumentos(){
    $this->getFCService()->cambiarEstadoDocumentos();
    $this->getNCService()->cambiarEstadoDocumentos();
  }

}
