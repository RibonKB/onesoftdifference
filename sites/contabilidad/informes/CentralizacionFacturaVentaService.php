<?php
require_once 'inc/AbstractFactoryService.class.php';

class CentralizacionFacturaVentaService extends AbstractFactoryService{

  private $fv;

  public function getTables($r){
    return "tb_factura_venta fv
  		LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fv.dc_nota_venta
  		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fv.dc_orden_servicio
  		LEFT JOIN tb_tipo_operacion t_fv ON t_fv.dc_tipo_operacion = fv.dc_tipo_operacion
  		LEFT JOIN tb_usuario us ON us.dc_usuario = fv.dc_usuario_creacion
  		JOIN tb_cliente pr ON pr.dc_cliente = fv.dc_cliente
  		JOIN tb_tipo_cliente t ON t.dc_tipo_cliente = pr.dc_tipo_cliente";
  }

  public function getCampos($r){
    return '"FV" tp, GROUP_CONCAT(nv.dq_nota_venta SEPARATOR "<br />") dq_nota_venta,GROUP_CONCAT(os.dq_orden_servicio SEPARATOR "<br />") dq_orden_servicio,
  	fv.df_emision, pr.dg_rut, pr.dg_razon,fv.dq_exento,SUM(fv.dq_neto) dq_neto,SUM(fv.dq_iva) dq_iva,SUM(fv.dq_total) dq_total,
  	GROUP_CONCAT(fv.dq_factura SEPARATOR "<br />") dq_factura,fv.dq_folio,fv.dm_nula,t_fv.dg_tipo_operacion dg_tipo,us.dg_usuario,GROUP_CONCAT(fv.dc_factura) dc_documento';
  }

  public function getCondiciones($r){
    $empresa = $this->factory->getEmpresa();
    $cond =  "fv.dc_empresa = {$empresa} AND ".
                "YEAR(fv.df_emision) = {$r->dc_anho} ".
                "AND MONTH(fv.df_emision) = {$r->dc_mes} ".
                "AND fv.dm_nula = 0 AND ".
                "fv.dm_centralizada = 0 AND ".
                "fv.dq_neto > 0";

    if(isset($r->dc_cliente)):
      $clientes = $this->getCondicionesCliente($r);
      $cond .= " AND fv.dc_cliente IN ({$clientes})";
    endif;

    return $cond;
  }

  public function initCentralizacion($r){
    if(!isset($r->dc_documento['FV']) or empty($r->dc_documento['FV'])):
      $this->fc = array();
      return;
    endif;

    $this->initDocumentos($r);
    //$this->initNotaVenta();
    //$this->initOrdenServicio();
    //$this->initClientes();

    //Para facturas de compra que mueven stock
    //$this->initRecepciones();
    $this->initCuentaContableLineasNegocio();
    //Para facturas de compra de servicios y gastos
    //$this->initCuentasServicios();
  }

  private function getCondicionesCliente($r){
    return implode(',',$r->dc_cliente);
  }

  private function initDocumentos($r){

    $this->validaIds($r->dc_documento['FV']);
    $this->fv = $this->getFacturas($r->dc_documento['FV']);
    $this->initDetallesFactura($r->dc_documento['FV']);
    $this->limpiaDetallesVacios();
    //$this->initTipoFactura();

  }

  private function limpiaDetallesVacios(){
    foreach($this->fv as $dc_factura => &$fv):
      foreach($fv->detalle as $i => $d):
        if($d->dq_precio == 0):
          unset($fv->detalle[$i]);
        endif;
      endforeach;
      if(count($fv->detalle) == 0):
        unset($this->fv[$dc_factura]);
      endif;
    endforeach;
  }

  private function validaIds(&$fcs){
    foreach($fcs as &$fc):
      if(empty($fc) or !Functions::isPK($fc)):
        $this->getErrorMan()->showWarning("Existen facturas de Venta con un formato incorrecto,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;
    endforeach;

    foreach($fcs as &$fc):
      $fc = intval($fc);
    endforeach;

  }

  private function getFacturas($fvs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(",?",count($fvs)),1);

    $select = $db->prepare(
                $db->select('tb_factura_venta fv
                             JOIN tb_cliente c ON c.dc_cliente = fv.dc_cliente',
                             'fv.dc_factura, fv.dq_folio, fv.dq_factura, fv.dc_cliente, fv.dq_total, fv.dq_iva, c.dc_cuenta_contable dc_cuenta_cliente,
                              c.dc_tipo_cliente, fv.dc_nota_venta, fv.dc_orden_servicio, c.dc_mercado dc_mercado_cliente,
                              fv.dc_tipo_operacion, fv.dc_ejecutivo',
                             'fv.dc_empresa = ? AND fv.dm_centralizada = 0 AND fv.dc_factura IN ('.$questions.')'));
    $select->bindValue(1, $this->factory->getEmpresa(), PDO::PARAM_INT);

    for($i = 0; $i < count($fvs); $i++):
      $select->bindValue($i + 2, $fvs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    $data = array();

    while($fv = $select->fetch(PDO::FETCH_OBJ)):
      $data[$fv->dc_factura] = $fv;
    endwhile;

    if(count($data) != count($fvs)):
      $this->getErrorMan()->showWarning("Hay facturas de venta seleccionadas que no fueron encontradas,".
                                        "Probablemente estas fueron centralizadas mientras se realizaba la consulta, ".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    return $data;

  }

  private function initDetallesFactura($fvs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(',?',count($fvs)),1);

    $select = $db->prepare(
                $db->select('tb_factura_venta_detalle d
                LEFT JOIN tb_producto p ON d.dg_producto = p.dg_codigo',
                '*',
                'p.dc_empresa = ? AND d.dc_factura IN ('.$questions.')'));
    $select->bindValue(1, $this->factory->getEmpresa());
    for($i = 0; $i < count($fvs); $i++):
      $select->bindValue($i+2, $fvs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    foreach($this->fv as &$fv):
      $fv->detalle = array();
    endforeach;

    while($d = $select->fetch(PDO::FETCH_OBJ)):
      //Factory::debug($d);
      $this->fv[$d->dc_factura]->detalle[] = $d;
    endwhile;

  }

  private function initCuentaContableLineasNegocio(){
    foreach($this->fv as &$fv):
      foreach($fv->detalle as &$d):
        $d->dc_cuenta_contable = $this->factory->getCuentaBodegaLineaNegocio($d->dc_linea_negocio);
      endforeach;
    endforeach;
  }

  public function getAsientosAnaliticos($template, $iva){
    $data = array(array(),array(),array());

    if(count($this->fv) == 0):
      return $data;
    endif;

    $data[1][$iva->dc_cuenta_contable] = array();

    foreach($this->fv as &$fv):

      if(!isset($data[2][$fv->dc_cuenta_cliente])):
        $data[2][$fv->dc_cuenta_cliente] = array();
      endif;

      $this->setCabeceraRegularAnalitico($template, $fv);

      $this->descomponeAnalitico($data, $template, $fv, $iva);

    endforeach;

    return $data;
  }

  private function descomponeAnalitico(&$data, $template, $fv, $iva){
    //dc_producto, dc_ceco, dc_marca, dc_linea_negocio, dc_tipo_producto, dc_guia_recepcion, dc_bodega
    foreach($fv->detalle as $d):
      $this->setDetalleRegularAnalitico($template, $d, $fv);

      //Cuenta ventas
      $t = clone($template);
      $t->dc_cuenta_contable = $d->dc_cuenta_contable;
      $t->dq_debe = Functions::fixNumberToLocal($d->dc_cantidad * $d->dq_precio);
      $t->dq_haber = 0;

      if(!isset($data[0][$t->dc_cuenta_contable])):
        $data[0][$t->dc_cuenta_contable] = array();
      endif;
      $data[0][$t->dc_cuenta_contable][] = $t;

      //IVA
      $tiva = clone($template);
      $tiva->dc_cuenta_contable = $iva->dc_cuenta_contable;
      $tiva->dq_debe = Functions::fixNumberToLocal(($t->dq_debe) * ($iva->dq_porcentaje_iva / 100.0));
      $tiva->dq_haber = 0;
      $data[1][$tiva->dc_cuenta_contable][] = $tiva;

      //Cliente
      $tcliente = clone($template);
      $tcliente->dc_cuenta_contable = $fv->dc_cuenta_cliente;
      $tcliente->dq_debe = 0;
      $tcliente->dq_haber = $t->dq_debe + $tiva->dq_debe;
      $data[2][$tcliente->dc_cuenta_contable][] = $tcliente;

    endforeach;
  }

  private function setCabeceraRegularAnalitico($template, $fv){
    $template->dc_factura_venta = $fv->dc_factura;
    $template->dc_nota_venta = $fv->dc_nota_venta;
    $template->dc_orden_servicio = $fv->dc_orden_servicio;
    $template->dc_cliente = $fv->dc_cliente;
    $template->dc_tipo_cliente = $fv->dc_tipo_cliente;
    $template->dc_mercado_cliente = $fv->dc_mercado_cliente;
    $template->dc_ejecutivo = $fv->dc_ejecutivo;
  }

  private function setDetalleRegularAnalitico($template, $d, $fv){
    $template->dc_producto = $d->dc_producto;
    $template->dc_cebe = $d->dc_cebe;
    if(empty($d->dc_cebe) and Functions::isPK($d->dc_cebe)):
      $cebe = $this->getConnection()->getRowById('tb_cebe',$d->dc_cebe,'dc_cebe');
      $template->dc_ceco = $cebe->dc_ceco;
    endif;
    $template->dc_marca = $d->dc_marca;
    $template->dc_linea_negocio = $d->dc_linea_negocio;
    $template->dc_tipo_producto = $d->dc_tipo_producto;
    $template->dg_glosa = trim("Folio Factura: {$fv->dq_folio}, Producto: ({$d->dg_codigo}) {$d->dg_descripcion}");
  }

  public function insertarDetallesCentralizacion($dc_centralizacion){
    $db = $this->getConnection();

    $insert = $db->prepare($db->insert('tb_centralizacion_venta_detalle_factura',array(
      'dc_centralizacion' => '?',
      'dc_factura' => '?'
    )));
    $insert->bindValue(1, $dc_centralizacion, PDO::PARAM_INT);
    $insert->bindParam(2, $dc_factura_venta, PDO::PARAM_INT);

    foreach($this->fv as $dc_factura_venta => $d):
      $db->stExec($insert);
    endforeach;
  }

  public function cambiarEstadoDocumentos(){

    if(!count($this->fv)):
      return;
    endif;

    $db = $this->getConnection();

    $update = $db->prepare($db->update('tb_factura_venta',array(
      'dm_centralizada' => 1
    ),'dc_factura = ?'));
    $update->bindParam(1, $dc_factura, PDO::PARAM_INT);

    foreach($this->fv as $dc_factura => $fc):
      $db->stExec($update);
    endforeach;
  }

}
