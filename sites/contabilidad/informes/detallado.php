<?php
error_reporting(E_ALL);
ini_set('memory_limit','128M');

define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$textData = html_entity_decode(file_get_contents("cache/informe_{$_POST['fid']}.xml"));
$textData = str_replace('&','&amp;',$textData);
$data = new SimpleXMLElement($textData);

$filtro = array();
foreach($data->detalle as $d){
	if(!((isset($_POST['mes']) && $d->dc_mes_contable != $_POST['mes']) ||
		(isset($_POST['debe']) && (float)$d->dq_debe == 0.0) ||
		(isset($_POST['haber']) && (float)$d->dq_haber == 0.0) ||
		(isset($_POST['cuenta']) && $d->dg_codigo != $_POST['cuenta']))){
		
			$filtro["{$d->dg_codigo}"][] = $d;
	}
}
unset($data);

$fltr = '';
foreach($_POST as $i => $v){
	$fltr .= "<input type='hidden' name='$i' value='$v' />";
}

echo("<div class='secc_bar'>Informe detallado</div>
<div id='other_options'>
<ul>
	<li style='background-image:url(images/filter_save.png)'>
		<a href='#'>Exportar</a>
		<div class='menu' style='width:290px;'>
			<iframe id='inf_export_res' class='hidden'></iframe>
			<form action='sites/contabilidad/informes/proc/export.php' method='post' target='inf_export_res'>
				<button type='button' id='sc_export'>Campos a exportar</button><br />
				Elija el formato en que exportará el informe<br />
				<select name='ex_type'>
					<option value='2'>Excel 2003</option>
					<option value='3'>PDF</option>
				</select><br />
				{$fltr}
				<input type='submit' value='Descargar' />
			</form>
		</div>
	</li>
	<li style='background-image:url(images/filter_add.png)'>
		<a href='#' >Columnas</a>
		<div class='menu' id='sc_show_filter' style='width:290px;'>
		</div>
	</li>
	<li style='background-image:url(images/print.png)'>
	<a href='#' id='inf_print'>Imprimir informe</a>
	</li>
</ul>
</div>
");

$suma_debe = 0;
$suma_haber = 0;
echo('<div id="informe_cont">');
foreach($filtro as $cuenta){
	$total_debe = 0;
	$total_haber = 0;
	echo("
	<table class='tab sortable detallado' style='margin: 20px 50px' width='90%'>
	<caption>({$cuenta[0]->dg_codigo}) {$cuenta[0]->dg_cuenta_contable}</caption>
	<thead>
		<tr>
			<th width='120' class='d_compr'>Nº Comprobante</th>
			<th width='120' class='d_interno hidden'>N° Interno</th>
			<th width='100' class='d_periodo'>Periodo contable</th>
			<th width='230' class='d_glosa'>Glosa</th>
			<th width='100' class='d_debe'>DEBE</th>
			<th width='100' class='d_haber'>HABER</th>
			<th width='250' class='d_mov hidden'>Tipo de movimiento</th>
			<th width='250' class='d_emision hidden'>Fecha emisión</th>
			<th width='250' class='d_banco hidden'>Banco</th>
			<th width='250' class='d_doc hidden'>Tipo documento</th>
			<th width='250' class='d_cheque hidden'>Cheque</th>
			<th width='250' class='d_rut hidden'>RUT</th>
			<th width='250' class='d_activo hidden'>Activo fijo</th>
			<th width='250' class='d_resp hidden'>Responsable</th>
			<th width='250' class='d_cebe hidden'>Centro beneficio</th>
			<th width='250' class='d_ceco hidden'>Centro costo</th>
			<th width='250' class='d_compra hidden'>Documento de compra</th>
			<th width='250' class='d_venta hidden'>Documento de venta</th>
                        <th width='250' class='d_factura_compra hidden'>Factura de Compra</th>
                        <th width='250' class='d_factura_venta hidden'>Factura de Venta</th>
                        <th width='250' class='d_nota_venta hidden'>Nota de Venta</th>
                        <th width='250' class='d_orden_servicio hidden'>Orden de Servicio</th>
                        <th width='250' class='d_cliente hidden'>Cliente</th>
                        <th width='250' class='d_tipo_cliente hidden'>Tipo Cliente</th>
                        
                        <th width='250' class='d_orden_compra hidden'>Orden de Compra</th>
                        <th width='250' class='d_producto hidden'>Producto</th>
                        <th width='250' class='d_tipo_producto hidden'>Tipo Producto</th>
                        <th width='250' class='d_proveedor hidden'>Proveedor</th>
                        <th width='250' class='d_tipo_proveedor hidden'>Tipo Proveedor</th>
                        <th width='250' class='d_marca hidden'>Marca</th>
                        <th width='250' class='d_linea hidden'>Linea de Negocio</th>
                        <th width='250' class='d_banco_pago hidden'>Banco</th>
                        <th width='250' class='d_banco_cobro hidden'>Banco Cobro</th>
                        <th width='250' class='d_guia_re hidden'>Guia Recepcion</th>
                        <th width='250' class='d_medio_pago hidden'>Medio de Pago</th>
                        
                        <th width='250' class='d_medio_cobro hidden'>Medio de Cobro</th>
                        <th width='250' class='d_merca_cliente hidden'>Mercado Cliente</th>
                        <th width='250' class='d_merca_prov hidden'>Mercado Proveedor</th>
                        <th width='250' class='d_nota_credito hidden'>Nota de Credito</th>
                        <th width='250' class='d_nota_cred_prov hidden'>Nota credito Proveedor</th>
                        <th width='250' class='d_segmento hidden'>Segmento</th>
                        <th width='250' class='d_cheque hidden'>Cheque</th>
                        <th width='250' class='d_cheque_fecha hidden'>Fecha Cheque</th>
                        <th width='250' class='d_bodega hidden'>Bodega</th>
                        
                        
		</tr>
	</thead>
	<tbody>");
	foreach($cuenta as $detalle){
		$total_debe += $detalle->dq_debe;
		$total_haber += $detalle->dq_haber;
		$detalle->dq_debe = number_format((float)$detalle->dq_debe,2,',','.');
		$detalle->dq_haber = number_format((float)$detalle->dq_haber,2,',','.');
		echo("
		<tr>
			<td class='d_compr'><a href='sites/contabilidad/ver_comprobante.php?num={$detalle->dg_comprobante}' target='_blank'>{$detalle->dg_comprobante}</a></td>
			<td class='d_interno hidden'><a href='sites/contabilidad/ver_comprobante.php?num={$detalle->dg_comprobante}' target='_blank'>{$detalle->dg_numero_interno}</a></td>
			<td class='d_periodo'>{$detalle->df_fecha_contable}</td>
			<td class='d_glosa'>{$detalle->dg_glosa_detalle}</td>
			<td class='d_debe' align='right'>{$detalle->dq_debe}</td>
			<td class='d_haber' align='right'>{$detalle->dq_haber}</td>
			<td class='d_mov hidden'>{$detalle->dg_tipo_movimiento}</td>
			<td class='d_emision hidden'>{$detalle->df_fecha_emision}</td>
			<td class='d_banco hidden'>{$detalle->dg_banco}</td>
			<td class='d_doc hidden'>{$detalle->dg_tipo_documento}</td>
			<td class='d_cheque hidden'>{$detalle->dg_cheque}</td>
			<td class='d_rut hidden'>{$detalle->dg_rut}</td>
			<td class='d_activo hidden'>{$detalle->dg_activo_fijo}</td>
			<td class='d_resp hidden'>{$detalle->dg_usuario}</td>
			<td class='d_cebe hidden'>{$detalle->dg_cebe}</td>
			<td class='d_ceco hidden'>{$detalle->dg_ceco}</td>
			<td class='d_compra hidden'>{$detalle->dg_doc_compra}</td>
			<td class='d_venta hidden'>{$detalle->dg_doc_venta}</td>
                        <td class='d_factura_compra hidden'>{$detalle->factura_compra}</td>
                        <td class='d_factura_venta hidden'>{$detalle->factura_venta}</td>
                        <td class='d_nota_venta hidden'>{$detalle->dq_nota_venta}</td>
                        <td class='d_orden_servicio hidden'>{$detalle->dq_orden_servicio}</td>
                        <td class='d_cliente hidden'>{$detalle->dg_cliente}</td>
                        <td class='d_tipo_cliente hidden'>{$detalle->dg_tipo_cliente}</td>
                        <td class='d_orden_compra hidden'>{$detalle->dq_orden_compra}</td>
                        <td class='d_producto hidden'>{$detalle->dg_producto}</td>
                        <td class='d_tipo_producto hidden'>{$detalle->dg_tipo_producto}</td>
                        <td class='d_proveedor hidden'>{$detalle->dg_proveedor}</td>
                        <td class='d_tipo_proveedor hidden'>{$detalle->dg_tipo_proveedor}</td>
                            <td class='d_marca hidden'>{$detalle->dg_marca}</td>
                        <td class='d_linea hidden'>{$detalle->dg_linea_negocio}</td>
                        <td class='d_banco_pago hidden'>{$detalle->banco_pago}</td>
                        <td class='d_banco_cobro hidden'>{$detalle->banco_cobro}</td>
                        <td class='d_guia_re hidden'>{$detalle->dq_guia_recepcion}</td>
                        <td class='d_medio_pago hidden'>{$detalle->dg_medio_pago}</td>
                            
                        <td  class='d_medio_cobro hidden'>{$detalle->dg_medio_cobro}</td>
                        <td  class='d_merca_cliente hidden'>{$detalle->mercado_cliente}</td>
                        <td  class='d_merca_prov hidden'>{$detalle->mercado_proveedor}</td>
                        <td  class='d_nota_credito hidden'>{$detalle->nota_credito}</td>
                        <td  class='d_nota_cred_prov hidden'>{$detalle->nota_prov}</td>
                        <td  class='d_segmento hidden'>{$detalle->dg_segmento}</td>
                        <td  class='d_cheque hidden'>{$detalle->dg_cheque}</td>
                        <td  class='d_cheque_fecha hidden'>{$detalle->cheque_fecha}</td>
                        <td  class='d_bodega hidden'>{$detalle->dg_bodega}</td>
		</tr>");
	}
	$saldo_dh = $total_debe-$total_haber;
	$suma_debe += $total_debe;
	$suma_haber += $total_haber;
	$stl = '';
	$saldo = 'Saldado';
	if($saldo_dh < 0.0){
		$stl = 'style="background-color:#F00;color:#FFF;"';
		$saldo = 'Saldo acreedor';
	}else if($saldo_dh > 0.0){
		$stl = 'style="background-color:#0D0;"';
		$saldo = 'Saldo deudor';
	}
	$saldo_dh = number_format($saldo_dh,2,',','.');
	$total_debe = number_format($total_debe,2,',','.');
	$total_haber = number_format($total_haber,2,',','.');
	echo("</tbody>
	<tfoot>
		<tr>
			<th align='right' colspan='3'>Totales</th>
			<th class='d_debe' align='right'>{$total_debe}</th>
			<th class='d_haber' align='right'>{$total_haber}</th>
		</tr>
		<tr>
			<th align='right' colspan='4'>{$saldo}</th>
			<th align='right' $stl>{$saldo_dh}</th>
		</tr>
	</tfoot>
	</table><br />");
}

$saldo_total = $suma_debe-$suma_haber;
$stl = '';
if($saldo_total < 0.0){
	$stl = 'style="background-color:#F00;color:#FFF;"';
}else if($saldo_total > 0.0){
	$stl = 'style="background-color:#0D0;"';
}
$saldo_total = number_format($saldo_total,2,',','.');
$suma_debe = number_format($suma_debe,2,',','.');
$suma_haber = number_format($suma_haber,2,',','.');

echo("
<table class='tab' style='margin:0 50px;'>
<caption>Totales</caption>
<thead>
	<tr>
		<th width='90'>DEBE</th>
		<th width='90'>HABER</th>
		<th width='90'>Saldo total</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td align='right'>{$suma_debe}</td>
		<td align='right'>{$suma_haber}</td>
		<td align='right' {$stl}>{$saldo_total}</td>
	</tr>
</tbody>
<tfoot>
	<tr>
	</tr>
</tfoot>
</table>
</div>
");
?>
<script type="text/javascript" src="jscripts/informe_contable.js?v=1_6"></script>
<script type="text/javascript" src="jscripts/jquery.printElement.min.js"></script>