<?php

require_once 'inc/AbstractFactoryService.class.php';

class CentralizacionFacturaCompraService extends AbstractFactoryService{

  private $fc, $cuentas_ln;

  public function getTables($r){
    return "tb_factura_compra fc
  		LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = fc.dc_nota_venta
  		LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio = fc.dc_orden_servicio
  		LEFT JOIN tb_tipo_factura_compra t_fc ON t_fc.dc_tipo = fc.dc_tipo
  		LEFT JOIN tb_usuario us ON us.dc_usuario = fc.dc_usuario_creacion
  		JOIN tb_proveedor pr ON pr.dc_proveedor = fc.dc_proveedor
  		JOIN tb_tipo_proveedor t ON t.dc_tipo_proveedor = pr.dc_tipo_proveedor AND t.dm_incluido_libro_compra = 1";
  }

  public function getCondiciones($r){
    $empresa = $this->factory->getEmpresa();
    $cond =  "fc.dc_empresa = {$empresa} AND ".
                "YEAR(fc.df_libro_compra) = {$r->dc_anho} ".
                "AND MONTH(fc.df_libro_compra) = {$r->dc_mes} ".
                "AND fc.dm_nula = 0 AND ".
                "fc.dm_centralizada = 0";

    if(isset($r->dc_proveedor)):
      $proveedores = $this->getCondicionesProveedor($r);
      $cond .= " AND fc.dc_proveedor IN ({$proveedores})";
    endif;

    return $cond;
  }

  public function getCampos($r){
    return '"FC" tp, GROUP_CONCAT(nv.dq_nota_venta SEPARATOR "<br />") dq_nota_venta,GROUP_CONCAT(os.dq_orden_servicio SEPARATOR "<br />") dq_orden_servicio,
  	fc.df_emision, pr.dg_rut, pr.dg_razon,fc.dq_exento,SUM(fc.dq_neto) dq_neto,SUM(fc.dq_iva) dq_iva,SUM(fc.dq_total) dq_total,
  	GROUP_CONCAT(fc.dq_factura SEPARATOR "<br />") dq_factura,fc.dq_folio,fc.dm_nula,t_fc.dg_tipo,us.dg_usuario,GROUP_CONCAT(fc.dc_factura) dc_documento';
  }

  public function initCentralizacion($r){

    if(!isset($r->dc_documento['FC']) or empty($r->dc_documento['FC'])):
      $this->fc = array();
      return;
    endif;

    $this->initDocumentos($r);
    $this->initNotaVenta();
    $this->initOrdenServicio();
    $this->initClientes();

    //Para facturas de compra que mueven stock
    $this->initRecepciones();
    $this->initCuentaContableLineasNegocio();

    //Para facturas de compra de servicios y gastos
    $this->initCuentasServicios();

  }

  private function initDocumentos($r){

    $this->validaIds($r->dc_documento['FC']);
    $this->fc = $this->getFacturas($r->dc_documento['FC']);
    $this->initDetallesFactura($r->dc_documento['FC']);

    $this->initTipoFactura();

  }

  private function validaIds($fcs){
    foreach($fcs as &$fc):
      if(empty($fc) or !preg_match('/^\d+$/',$fc)):
        $this->getErrorMan()->showWarning("Existen facturas de Compra con un formato incorrecto,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;
    endforeach;

    foreach($fcs as &$fc):
      $fc = intval($fc);
    endforeach;

  }

  private function getFacturas($fcs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(",?",count($fcs)),1);

    $select = $db->prepare(
                $db->select('tb_factura_compra fc
                             JOIN tb_proveedor p ON p.dc_proveedor = fc.dc_proveedor',
                             'fc.dc_factura, fc.dq_folio, fc.dq_factura, fc.dc_proveedor, fc.dq_total, fc.dq_iva, p.dc_cuenta_contable dc_cuenta_proveedor,
                              p.dc_tipo_proveedor, fc.dc_nota_venta, fc.dc_orden_servicio, p.dc_mercado dc_mercado_proveedor,
                              fc.dc_tipo, fc.dc_orden_compra',
                             'fc.dc_empresa = ? AND fc.dm_centralizada = 0 AND fc.dc_factura IN ('.$questions.')'));
    $select->bindValue(1, $this->factory->getEmpresa(), PDO::PARAM_INT);

    for($i = 0; $i < count($fcs); $i++):
      $select->bindValue($i + 2, $fcs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    $data = array();

    while($fc = $select->fetch(PDO::FETCH_OBJ)):
      $data[$fc->dc_factura] = $fc;
    endwhile;

    if(count($data) != count($fcs)):
      $this->getErrorMan()->showWarning("Hay facturas de compra seleccionadas que no fueron encontradas,".
                                        "Probablemente estas fueron centralizadas mientras se realizaba la consulta, ".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    return $data;

  }

  private function initDetallesFactura($fcs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(',?',count($fcs)),1);

    $select = $db->prepare(
                $db->select('tb_factura_compra_detalle d
                LEFT JOIN tb_producto p ON p.dc_producto = d.dc_producto',
                '*',
                'd.dc_factura IN ('.$questions.')'));
    for($i = 0; $i < count($fcs); $i++):
      $select->bindValue($i+1, $fcs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    foreach($this->fc as &$fc):
      $fc->detalle = array();
    endforeach;

    while($d = $select->fetch(PDO::FETCH_OBJ)):
      //Factory::debug($d);
      $this->fc[$d->dc_factura]->detalle[] = $d;
    endwhile;

  }

  private function initNotaVenta(){
    $db = $this->getConnection();

    foreach($this->fc as &$fc):
      if($fc->dc_nota_venta):
        $fc->nota_venta = $db->getRowById('tb_nota_venta',$fc->dc_nota_venta,'dc_nota_venta');
      endif;
    endforeach;
  }

  private function initOrdenServicio(){
    $db = $this->getConnection();

    foreach($this->fc as &$fc):
      if($fc->dc_orden_servicio):
        $fc->nota_venta = $db->getRowById('tb_orden_servicio',$fc->dc_orden_servicio,'dc_orden_servicio');
      endif;
    endforeach;

  }

  private function initClientes(){
    $db = $this->getConnection();

    foreach($this->fc as &$fc):
      if($fc->dc_nota_venta):
        $fc->cliente = $db->getRowById('tb_cliente',$fc->nota_venta->dc_cliente,'dc_cliente');
      elseif($fc->dc_orden_servicio):
        $fc->cliente = $db->getRowById('tb_cliente',$fc->orden_servicio->dc_cliente,'dc_cliente');
      endif;
    endforeach;

  }

  private function initTipoFactura(){
    $db = $this->getConnection();

    foreach($this->fc as &$fc):
      $fc->tipo = $db->getRowById('tb_tipo_factura_compra',$fc->dc_tipo,'dc_tipo');

      if($fc->tipo === false):
        $this->getErrorMan()->showWarning("La factura de compra <strong>{$fc->dq_factura} (folio: {$fc->dq_folio})</strong>".
                                          " posee un tipo de factura incorrecto y no fue encontrado,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;

    endforeach;

  }

  private function getCondicionesProveedor($r){
    return implode(',',$r->dc_proveedor);
  }

  private function initRecepciones(){

    foreach($this->fc as &$fc):
      if($fc->tipo->dm_factura_manual == 0):
        $this->relacionaDetalleGR($fc);
        $this->validarRecepciones($fc);
      endif;
    endforeach;

  }

  private function validarRecepciones($fc){
    foreach($fc->detalle as &$d):
      if($d->dc_cantidad > $d->recepcionados):
        $this->getErrorMan()->showWarning("Existen detalles en la Factura de Compra ".
                                          "<strong>{$fc->dq_factura} (folio: {$fc->dq_folio})</strong> que no han sido recepcionados.".
                                          "<br />Error encontrado en el detalle: <strong>{$d->dg_producto}</strong>".
                                          "<br />Tipo de Factura: <strong>{$fc->tipo->dg_tipo}</strong>");
        exit;
      endif;
    endforeach;
  }

  private function relacionaDetalleGR($fc){
    $db = $this->getConnection();

    $select = $db->prepare(
                $db->select('tb_guia_recepcion gr
                JOIN tb_guia_recepcion_detalle d ON d.dc_guia_recepcion = gr.dc_guia_recepcion',
                '*',
                'gr.dc_factura = ? AND gr.dm_nula = 0'));
    $select->bindValue(1, $fc->dc_factura, PDO::PARAM_INT);
    $db->stExec($select);

    foreach($fc->detalle as &$d):
      $d->recepcionados = 0;
      $d->recepcion = array();
    endforeach;

    while($grd = $select->fetch(PDO::FETCH_OBJ)):
      $afectados = $this->getDetallesAfectadosGR($grd, $fc->detalle);

      if(count($afectados) == 0):
        $this->getErrorMan()->showWarning("La guía de recepción <strong>{$grd->dq_guia_recepcion}</strong>".
                                          " posee detalles que no corresponden a la factura de compra".
                                          " <strong>{$fc->dq_factura} (folio: {$fc->dq_folio})</strong>. ".
                                          "<br />Validar la información en el módulo correspondiente y volver a intentarlo.".
                                          "<br />Producto: {$grd->dg_descripcion}");
        exit;
      endif;

      $this->relacionaMejorOpcionDetalleGR($grd, $afectados);

    endwhile;

  }

  private function getDetallesAfectadosGR($grd, $detalle){
    $res = array();
    foreach($detalle as &$d):
      if($d->dc_producto == $grd->dc_producto and $d->dc_cantidad >= $grd->dq_cantidad):
        $res[] = $d;
      endif;
    endforeach;

    return $res;
  }

  private function relacionaMejorOpcionDetalleGR($grd, $detalles){
    $saldar = $grd->dq_cantidad;

    while($saldar > 0):
      $puntaje = 0;
      $actual = null;

      foreach($detalles as &$d):
        $p = 0;

        if($d->recepcionados >= $d->dc_cantidad ):
          continue;
        endif;

        if($d->dc_cantidad == $grd->dq_cantidad):
          $p++;
        endif;

        if($d->dq_precio == $grd->dq_precio):
          $p++;
        endif;

        if($d->dc_detalle_nota_venta != 0 and $d->dc_detalle_nota_venta == $grd->dc_detalle_nota_venta):
          $p++;
        endif;

        if($d->dc_detalle_orden_servicio != 0 and $d->dc_detalle_orden_servicio == $grd->dc_detalle_orden_servicio):
          $p++;
        endif;

        if($p >= $puntaje):
          $puntaje = $p;
          $actual = $d;
        endif;

      endforeach;

      if($actual == null):
        $this->getErrorMan()->showWarning("La guía de recepción <strong>{$grd->dq_guia_recepcion}</strong>".
                                          " posee detalles que no corresponden a la factura de compra".
                                          "<br />Validar la información en el módulo correspondiente y volver a intentarlo.");
        exit;
      endif;

      if(($actual->dc_cantidad - $actual->recepcionados) >= $saldar):
        $actual->recepcionados += $saldar;
        $actual->recepcion[] = (object)array(
          'dc_cantidad' => $saldar,
          'dc_guia_recepcion' => $grd->dc_guia_recepcion,
          'dc_bodega' => $grd->dc_bodega,
          'dc_ceco' => $grd->dc_ceco
        );
        $saldar = 0;
      else:
        $actual->recepcionados += ($actual->dc_cantidad - $actual->recepcionados);
        $actual->recepcion[] = (object)array(
          'dc_cantidad' => $actual->dc_cantidad - $actual->recepcionados,
          'dc_guia_recepcion' => $grd->dc_guia_recepcion,
          'dc_bodega' => $grd->dc_bodega,
          'dc_ceco' => $grd->dc_ceco
        );
        $saldar -= ($actual->dc_cantidad - $actual->recepcionados);
      endif;

    endwhile;

  }

  private function initCuentaContableLineasNegocio(){
    $this->cuentas_ln = array();
    foreach($this->fc as &$fc):
      if($fc->tipo->dm_factura_manual == 0):
        foreach($fc->detalle as &$d):
          foreach($d->recepcion as &$r):
            $r->dc_cuenta_contable = $this->getCuentaBodegaLineaNegocio($d->dc_linea_negocio, $r->dc_bodega);
          endforeach;
        endforeach;
      endif;
    endforeach;
  }

  private function getCuentaBodegaLineaNegocio($dc_linea_negocio, $dc_bodega){
    $hash = "{$dc_linea_negocio}_{$dc_bodega}";
    if(isset($this->cuentas_ln[$hash])):
      return $this->cuentas_ln[$hash];
    endif;

    $db = $this->getConnection();

    $select = $db->prepare(
                $db->select('tb_linea_negocio_cuenta_contable',
                'dc_cuenta_contable',
                'dc_linea_negocio = ? AND dc_bodega = ?'));
    $select->bindValue(1, $dc_linea_negocio, PDO::PARAM_INT);
    $select->bindValue(2, $dc_bodega, PDO::PARAM_INT);
    $db->stExec($select);

    $dc_cuenta_contable = $select->fetch(PDO::FETCH_OBJ);

    if($dc_cuenta_contable === false):
      $ln = $db->getRowById('tb_linea_negocio',$dc_linea_negocio,'dc_linea_negocio');
      $bodega = $db->getRowById('tb_bodega',$dc_bodega,'dc_bodega');
      $this->getErrorMan()->showWarning("La Línea de negocio <strong>{$ln->dg_linea_negocio}</strong>".
                                        " no posee configurada una cuenta contable para la bodega".
                                        " <strong>{$bodega->dg_bodega}</strong>, verifique los datos".
                                        " de entrada y vuelva a intentarlo.");
      exit;
    endif;

    $this->cuentas_ln[$hash] = $dc_cuenta_contable->dc_cuenta_contable;
    return $this->cuentas_ln[$hash];

  }

  private function initCuentasServicios(){
    foreach($this->fc as &$fc):
      if($fc->tipo->dm_factura_manual == 1):
        foreach($fc->detalle as &$d):
          if(empty($d->dc_cuenta_servicio) or !preg_match("/^\d+$/",$d->dc_cuenta_servicio)):
            $this->getErrorMan()->showWarning("La cuenta contable para el detalle".
                                              " <strong>{$d->dg_producto}</strong> en la factura de compra".
                                              " <strong>{$fc->dq_factura} (folio: {$fc->dq_folio})</strong>".
                                              " No ha sido asignado, realice esta asignación en el módulo de consulta.");
            exit;
          endif;
        endforeach;
      endif;
    endforeach;
  }

  public function getAsientosAnaliticos($template, $iva){
    $data = array(array(),array(),array());

    if(count($this->fc) == 0):
      return $data;
    endif;

    $data[1][$iva->dc_cuenta_contable] = array();

    foreach($this->fc as $fc):

      if(!isset($data[2][$fc->dc_cuenta_proveedor])):
        $data[2][$fc->dc_cuenta_proveedor] = array();
      endif;

      $this->setCabeceraRegularAnalitico($template, $fc);

      if($fc->tipo->dm_factura_manual == 0):
        $this->descomponeAnaliticoStock($data, $template, $fc, $iva);
      else:
        $this->descomponeAnaliticoServicio($data, $template, $fc, $iva);
      endif;
    endforeach;

    return $data;

  }

  private function descomponeAnaliticoStock(&$data, $template, $fc, $iva){

    //dc_producto, dc_ceco, dc_marca, dc_linea_negocio, dc_tipo_producto, dc_guia_recepcion, dc_bodega
    foreach($fc->detalle as $d):
      $this->setDetalleRegularAnalitico($template, $d, $fc);
      foreach($d->recepcion as $r):
        $t = clone($template);
        $t->dc_guia_recepcion = $r->dc_guia_recepcion;
        $t->dc_bodega = $r->dc_bodega;
        $t->dc_cuenta_contable = $r->dc_cuenta_contable;
        $t->dq_debe = Functions::fixNumberToLocal($r->dc_cantidad * $d->dq_precio);
        $t->dq_haber = 0;

        if(!isset($data[0][$r->dc_cuenta_contable])):
          $data[0][$r->dc_cuenta_contable] = array();
        endif;
        $data[0][$r->dc_cuenta_contable][] = $t;

        //iva
        $tiva = clone($t);
        $tiva->dq_debe = Functions::fixNumberToLocal(($r->dc_cantidad * $d->dq_precio) * ($iva->dq_porcentaje_iva / 100.0));
        $tiva->dc_cuenta_contable = $iva->dc_cuenta_contable;
        $data[1][$iva->dc_cuenta_contable][] = $tiva;

        //Proveedor
        $tprov = clone($t);
        $tprov->dq_debe = 0;
        $tprov->dq_haber = $t->dq_debe + $tiva->dq_debe;
        $tprov->dc_cuenta_contable = $fc->dc_cuenta_proveedor;
        $data[2][$fc->dc_cuenta_proveedor][] = $tprov;

      endforeach;
    endforeach;

  }

  private function descomponeAnaliticoServicio(&$data, $template, $fc, $iva){
    foreach($fc->detalle as $d):
      $this->setDetalleRegularAnalitico($template, $d, $fc);

      $t = clone($template);
      $t->dc_cuenta_contable = $d->dc_cuenta_servicio;
      $t->dq_debe = Functions::fixNumberToLocal($d->dc_cantidad * $d->dq_precio);
      $t->dq_haber = 0;

      if(!isset($data[0][$d->dc_cuenta_servicio])):
        $data[0][$d->dc_cuenta_servicio] = array();
      endif;
      $data[0][$d->dc_cuenta_servicio][] = $t;

      //iva
      $tiva = clone($t);
      $tiva->dq_debe = Functions::fixNumberToLocal(($d->dc_cantidad * $d->dq_precio) * ($iva->dq_porcentaje_iva / 100.0));
      $tiva->dc_cuenta_contable = $iva->dc_cuenta_contable;
      $data[1][$iva->dc_cuenta_contable][] = $tiva;

      //Proveedor
      $tprov = clone($t);
      $tprov->dq_debe = 0;
      $tprov->dq_haber = $t->dq_debe + $tiva->dq_debe;
      $tprov->dc_cuenta_contable = $fc->dc_cuenta_proveedor;
      $data[2][$fc->dc_cuenta_proveedor][] = $tprov;

    endforeach;
  }

  private function setDetalleRegularAnalitico($template, $d, $fc){
    $template->dc_producto = $d->dc_producto;
    $template->dc_ceco = $d->dc_ceco;
    $template->dc_cebe = $d->dc_cebe;
    $template->dc_marca = $d->dc_marca;
    $template->dc_linea_negocio = $d->dc_linea_negocio;
    $template->dc_tipo_producto = $d->dc_tipo_producto;
    $template->dg_glosa = trim("Folio Factura: {$fc->dq_folio}, Producto: ({$d->dg_codigo}) {$d->dg_descripcion}");
  }

  private function setCabeceraRegularAnalitico($template, $fc){
    $template->dc_factura_compra = $fc->dc_factura;
    $template->dc_nota_venta = $fc->dc_nota_venta;
    $template->dc_orden_servicio = $fc->dc_orden_servicio;
    $template->dc_proveedor = $fc->dc_proveedor;
    $template->dc_tipo_proveedor = $fc->dc_tipo_proveedor;
    $template->dc_mercado_proveedor = $fc->dc_mercado_proveedor;

    if(isset($fc->cliente)):
      $template->dc_cliente = $fc->cliente->dc_cliente;
      $template->dc_tipo_cliente = $fc->cliente->dc_tipo_cliente;
      $template->dc_mercado_cliente = $fc->cliente->dc_mercado;
    else:
      $template->dc_cliente = null;
      $template->dc_tipo_cliente = null;
      $template->dc_mercado_cliente = null;
    endif;

    $template->dc_orden_compra = $fc->dc_orden_compra;

    if(isset($fc->nota_venta)):
      $template->dc_ejecutivo = $fc->nota_venta->dc_ejecutivo;
    else:
      $template->dc_ejecutivo = null;
    endif;
  }

  public function insertarDetallesCentralizacion($dc_centralizacion){
    $db = $this->getConnection();

    $insert = $db->prepare($db->insert('tb_centralizacion_compra_detalle_factura',array(
      'dc_centralizacion' => '?',
      'dc_factura' => '?'
    )));
    $insert->bindValue(1, $dc_centralizacion, PDO::PARAM_INT);
    $insert->bindParam(2, $dc_factura_compra, PDO::PARAM_INT);

    foreach($this->fc as $dc_factura_compra => $d):
      $db->stExec($insert);
    endforeach;
  }

  public function cambiarEstadoDocumentos(){

    if(!count($this->fc)):
      return;
    endif;

    $db = $this->getConnection();

    $update = $db->prepare($db->update('tb_factura_compra',array(
      'dm_centralizada' => 1
    ),'dc_factura = ?'));
    $update->bindParam(1, $dc_factura, PDO::PARAM_INT);

    foreach($this->fc as $dc_factura => $fc):
      $db->stExec($update);
    endforeach;
  }

}
