<div id="secc_bar">Balance General</div>
<div id="main_cont">
<?php
error_reporting(E_ALL);
ini_set('memory_limit','128M');

define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if($_POST['inf_cta_hasta'] == ''){
	if($_POST['inf_cta_desde'] != ''){
		$cuentas = "(SELECT * FROM tb_cuenta_contable WHERE dg_codigo = {$_POST['inf_cta_desde']} AND dc_empresa = {$empresa})";
	}else{
		$cuentas = "(SELECT * FROM tb_cuenta_contable WHERE dc_empresa = {$empresa})";
	}
}else{
	if($_POST['inf_cta_desde'] == ''){
		$cuentas = "(SELECT * FROM tb_cuenta_contable WHERE dg_codigo = {$_POST['inf_cta_hasta']} AND dc_empresa = {$empresa})";
	}else{
		$cuentas = "(SELECT * FROM tb_cuenta_contable WHERE dg_codigo BETWEEN {$_POST['inf_cta_desde']} AND {$_POST['inf_cta_hasta']} AND dc_empresa = {$empresa})";
	}
}

$comprobante = '';

if($_POST['inf_mes_hasta'] == ''){
	if($_POST['inf_mes_desde'] != ''){
		$comprobante .= " AND dc_mes_contable = {$_POST['inf_mes_desde']}";
	}
}else{
	if($_POST['inf_mes_desde'] == ''){
		$comprobante .= " AND dc_mes_contable = {$_POST['inf_mes_hasta']}";
	}else{
		$comprobante .= " AND (dc_mes_contable BETWEEN {$_POST['inf_mes_desde']} AND {$_POST['inf_mes_hasta']})";
	}
}


if(isset($_POST['inf_tipo_mov'])){
	$comprobante .= " AND dc_tipo_movimiento = {$_POST['inf_tipo_mov']}";
}

if(isset($_POST['inf_glosa_head'])){
	$comprobante .= " AND dg_glosa LIKE '%{$_POST['inf_glosa_head']}%'";
}

if(isset($_POST['inf_emision_desde'])){
	$_POST['inf_emision_desde'] = $db->sqlDate($_POST['inf_emision_desde']);
	if($_POST['inf_emision_hasta'] == ''){
		$comprobante .= " AND (df_fecha_emision BETWEEN '{$_POST['inf_emision_desde']}' AND '{$_POST['inf_emision_desde']} 23:59:59')";
	}else{
		$_POST['inf_emision_hasta'] = $db->sqlDate($_POST['inf_emision_hasta']);
		$comprobante .= " AND (df_fecha_emision BETWEEN '{$_POST['inf_emision_desde']}' AND '{$_POST['inf_emision_hasta']} 23:59:59')";
	}
}

if(isset($_POST['inf_responsable'])){
	$responsable = $db->select('tb_usuario','dc_usuario',"dg_usuario = '{$_POST['inf_responsable']}' AND dc_empresa = {$empresa}");
	if(!count($responsable)){
		$error_man->showWarning('El usuario responsable es inválido, no existe. pruebe con las sugerencias del autocompletado');
		exit();
	}else{
		$comprobante .= " AND com.dc_usuario_creacion = {$responsable[0]['dc_usuario']}";
	}
}

$comprobante .= " AND dm_anulado='0'";

$detalle = '';

if(isset($_POST['inf_glosa_det'])){
	$detalle .= " AND dg_glosa LIKE '%{$_POST['inf_glosa_det']}%'";
}

if(isset($_POST['inf_tipo_doc'])){
	$detalle .= " AND dc_tipo_documento = {$_POST['inf_tipo_doc']}";
}

if(isset($_POST['inf_cheque_desde'])){
	if($_POST['inf_cheque_hasta'] == ''){
		$detalle .= " AND dg_cheque = '{$_POST['inf_cheque_desde']}'";
	}else{
		$detalle .= " AND (dg_cheque BETWEEN '{$_POST['inf_cheque_desde']}' AND '{$_POST['inf_cheque_hasta']}')";
	}
}

if(isset($_POST['inf_rut'])){
	$detalle .= " AND dg_rut = '{$_POST['inf_rut']}'";
}

if(isset($_POST['inf_act_fijo'])){
	$detalle .= " AND dg_activo_fijo = '{$_POST['inf_act_fijo']}'";
}

if(isset($_POST['inf_banco'])){
	$detalle .= " AND dc_banco = {$_POST['inf_banco']}";
}

if(isset($_POST['inf_cebe'])){
	$detalle .= " AND dc_cebe = {$_POST['inf_cebe']}";
}

if(isset($_POST['inf_ceco'])){
	$detalle .= " AND dc_ceco = {$_POST['inf_ceco']}";
}

if(isset($_POST['inf_doc_compra'])){
	$detalle .= " AND dg_doc_compra = '{$_POST['inf_doc_compra']}'";
}

if(isset($_POST['inf_doc_venta'])){
	$detalle .= " AND dg_doc_venta = '{$_POST['inf_doc_venta']}'";
}

/*if($detalle == ''){
	$detalle = 'tb_comprobante_contable_detalle det ON cc.dc_cuenta_contable = det.dc_cuenta_contable';
}else{
	$detalle = "tb_comprobante_contable_detalle det ON cc.dc_cuenta_contable = det.dc_cuenta_contable {$detalle}";
}*/

if(isset($_POST['inf_todas_cuentas'])){
	$tipo_join = "LEFT JOIN";
}else{
	$tipo_join = "JOIN";
}

if($_POST['inf_compare_desde'] == ''){

	if($_POST['inf_anho_hasta'] == ''){
		$comprobante ="tb_comprobante_contable com ON det.dc_comprobante = com.dc_comprobante AND dc_anho_contable = {$_POST['inf_anho_desde']} AND com.dc_empresa = {$empresa}".$comprobante;
	}else{
		$comprobante ="(SELECT * FROM tb_comprobante_contable WHERE (dc_anho_contable BETWEEN {$_POST['inf_anho_desde']} AND {$_POST['inf_anho_hasta']}) AND dc_empresa = {$empresa}".$comprobante;
	}

	$tables = "{$cuentas} AS cc
	{$tipo_join} tb_comprobante_contable_detalle det ON cc.dc_cuenta_contable = det.dc_cuenta_contable {$detalle}
	JOIN {$comprobante}";
	
	$fields = "cc.dg_codigo, cc.dg_cuenta_contable, cc.dm_tipo_cuenta, SUM(det.dq_debe) as dq_debe, SUM(det.dq_haber) AS dq_haber";
}else{
	if($_POST['inf_anho_hasta'] == ''){
	$comprobante_actual = "tb_comprobante_contable com ON det.dc_comprobante = com.dc_comprobante AND dc_anho_contable = {$_POST['inf_anho_desde']} AND com.dc_empresa = {$empresa}".$comprobante;
	$actual_range = "{$_POST['inf_anho_desde']}";
	}else{
	$comprobante_actual = "tb_comprobante_contable com ON det.dc_comprobante = com.dc_comprobante AND (dc_anho_contable BETWEEN {$_POST['inf_anho_desde']} AND {$_POST['inf_anho_hasta']}) AND com.dc_empresa = {$empresa}".$comprobante;
	$actual_range = "{$_POST['inf_anho_desde']} - {$_POST['inf_anho_hasta']}";
	}
	
	if($_POST['inf_compare_hasta'] == ''){
	$comprobante_compare = "tb_comprobante_contable comp ON det2.dc_comprobante = comp.dc_comprobante AND dc_anho_contable = {$_POST['inf_compare_desde']} AND comp.dc_empresa = {$empresa}".$comprobante;
	$compare_range = "{$_POST['inf_compare_desde']}";
	}else{
	$comprobante_compare = "tb_comprobante_contable comp ON det2.dc_comprobante = comp.dc_comprobante AND (dc_anho_contable BETWEEN {$_POST['inf_compare_desde']} AND {$_POST['inf_compare_hasta']}) AND comp.dc_empresa = {$empresa}".$comprobante;
	$compare_range = "{$_POST['inf_anho_desde']} - {$_POST['inf_compare_hasta']}";
	}
	
	$tables = "{$cuentas} AS cc
	{$tipo_join} (SELECT det.* FROM tb_comprobante_contable_detalle det
				JOIN {$comprobante_actual}{$detalle}) AS da ON cc.dc_cuenta_contable = da.dc_cuenta_contable
	LEFT JOIN (SELECT det2.* FROM tb_comprobante_contable_detalle det2
				JOIN {$comprobante_compare}{$detalle}) AS dc ON cc.dc_cuenta_contable = dc.dc_cuenta_contable";
				
	$fields = "cc.dg_codigo, cc.dg_cuenta_contable, cc.dm_tipo_cuenta,
	SUM(da.dq_debe) AS dq_debe_actual, SUM(dc.dq_debe) AS dq_debe_compare,
	SUM(da.dq_haber) AS dq_haber_actual, SUM(dc.dq_haber) AS dq_haber_compare";
}

$informe = $db->select($tables,$fields,'',array("group_by" => 'cc.dc_cuenta_contable',"order_by" => 'cc.dm_tipo_cuenta,cc.dg_codigo'));
unset($tables,$fields);

$total_debe = $total_haber = $total_deudor = $total_acreedor = $total_activo = $total_pasivo = $total_perdida = $total_ganancia = 0.0;

if($_POST['inf_compare_desde'] == ''){

	echo("<table class='tab' id='balance' width='100%'>
	<caption>Balance General</caption>
	<thead>
	<tr>
		<th>Cuenta contable</th>
		<th>Descripción</th>
		<th>DEBE</th>
		<th>HABER</th>
		<th>Deudor</th>
		<th>Acreedor</th>
		<th>Activo</th>
		<th>Pasivo</th>
		<th>Perdida</th>
		<th>Ganancia</th>
	</tr>
	</thead>
	<tbody>");
	
	foreach($informe as $detalle){
		$saldo = $detalle['dq_debe']-$detalle['dq_haber'];
		if($saldo < 0){
			$saldo = abs($saldo);
			if($detalle['dm_tipo_cuenta'] == '1' || $detalle['dm_tipo_cuenta'] == '2'){
				$deudor = $activo = $perdida = $ganancia = 0.0;
				$acreedor = $pasivo = $saldo;
				
				$total_acreedor += $saldo;
				$total_pasivo += $saldo;
			}else{
				$deudor = $activo = $pasivo = $perdida = 0.0;
				$acreedor = $ganancia = $saldo;
				
				$total_acreedor += $saldo;
				$total_ganancia += $saldo;
			}
		}else{
			if($detalle['dm_tipo_cuenta'] == '1' || $detalle['dm_tipo_cuenta'] == '2'){
				$acreedor = $pasivo = $perdida = $ganancia = 0.0;
				$deudor = $activo = $saldo;
				
				$total_deudor += $saldo;
				$total_activo += $saldo;
			}else{
				$acreedor = $activo = $pasivo = $ganancia = 0.0;
				$deudor = $perdida = $saldo;
				
				$total_deudor += $saldo;
				$total_perdida += $saldo;
			}
		}
			$total_debe += $detalle['dq_debe'];
			$total_haber += $detalle['dq_haber'];
			$detalle['dq_debe'] = moneda_local($detalle['dq_debe']);
			$detalle['dq_haber'] = moneda_local($detalle['dq_haber']);
			$acreedor = moneda_local($acreedor);
			$deudor = moneda_local($deudor);
			$activo = moneda_local($activo);
			$pasivo = moneda_local($pasivo);
			$perdida = moneda_local($perdida);
			$ganancia = moneda_local($ganancia);
			
			echo("
			<tr>
				<td>{$detalle['dg_codigo']}</td>
				<td>{$detalle['dg_cuenta_contable']}</td>
				<td align='right'>{$detalle['dq_debe']}</td>
				<td align='right'>{$detalle['dq_haber']}</td>
				<td align='right'>{$deudor}</td>
				<td align='right'>{$acreedor}</td>
				<td align='right'>{$activo}</td>
				<td align='right'>{$pasivo}</td>
				<td align='right'>{$perdida}</td>
				<td align='right'>{$ganancia}</td>
			</tr>");
	}
	
	$total_debe = $total_debe;
	$total_haber = $total_haber;
	$total_deudor = $total_deudor;
	$total_acreedor = $total_acreedor;
	$total_activo = $total_activo;
	$total_pasivo = $total_pasivo;
	$total_perdida = $total_perdida;
	$total_ganancia = $total_ganancia;
	
	$activo_menos_pasivo = $total_activo-$total_pasivo;
	$ganancia_menos_perdida = $total_ganancia-$total_perdida;
	
	if($activo_menos_pasivo < 0){
		$total_activo_2 = abs($activo_menos_pasivo);
		$total_pasivo_2 = 0;
	}else{
		$total_pasivo_2 = abs($activo_menos_pasivo);
		$total_activo_2 = 0;
	}
	
	if($ganancia_menos_perdida < 0){
		$total_ganancia_2 = abs($ganancia_menos_perdida);
		$total_perdida_2 = 0;
	}else{
		$total_perdida_2 = abs($ganancia_menos_perdida);
		$total_ganancia_2 = 0;
	}
	
	$total_activo_3 = $total_activo_2 + $total_activo;
	$total_pasivo_3 = $total_pasivo_2 + $total_pasivo;
	$total_perdida_3 = $total_perdida_2 + $total_perdida;
	$total_ganancia_3 = $total_ganancia_2 + $total_ganancia;
	
	echo("
	<tfoot>
	<tr>
		<th colspan='2' align='right'>Totales</th>
		<th align='right'>".moneda_local($total_debe)."</th>
		<th align='right'>".moneda_local($total_haber)."</th>
		<th align='right'>".moneda_local($total_deudor)."</th>
		<th align='right'>".moneda_local($total_acreedor)."</th>
		<th align='right'>".moneda_local($total_activo)."</th>
		<th align='right'>".moneda_local($total_pasivo)."</th>
		<th align='right'>".moneda_local($total_perdida)."</th>
		<th align='right'>".moneda_local($total_ganancia)."</th>
	</tr>
	
	<tr>
		<th colspan='2' align='right'>Pérdida y Ganancia</th>
		<th align='right'>0</th>
		<th align='right'>0</th>
		<th align='right'>0</th>
		<th align='right'>0</th>
		<th align='right'>".moneda_local($total_activo_2)."</th>
		<th align='right'>".moneda_local($total_pasivo_2)."</th>
		<th align='right'>".moneda_local($total_perdida_2)."</th>
		<th align='right'>".moneda_local($total_ganancia_2)."</th>
	</tr>
	
	<tr>
		<th colspan='2' align='right'>Totales</th>
		<th align='right'>".moneda_local($total_debe)."</th>
		<th align='right'>".moneda_local($total_haber)."</th>
		<th align='right'>".moneda_local($total_deudor)."</th>
		<th align='right'>".moneda_local($total_acreedor)."</th>
		<th align='right'>".moneda_local($total_activo_3)."</th>
		<th align='right'>".moneda_local($total_pasivo_3)."</th>
		<th align='right'>".moneda_local($total_perdida_3)."</th>
		<th align='right'>".moneda_local($total_ganancia_3)."</th>
	</tr>
	</tfoot>
	</tbody></table>");
	
}else{

$total_debe_compare = $total_haber_compare = $total_deudor_compare = $total_acreedor_compare = $total_activo_compare = $total_pasivo_compare = $total_perdida_compare = $total_ganancia_compare = 0.0;
	
	echo("<table class='tab' style='width:1150px' id='balance'>
	<caption>Balance General</caption>
	<thead>
	<tr>
		<th width='100'>Cuenta contable</th>
		<th width='250'>Descripción</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.debe' class='right' style='width:13px' />
			DEBE<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='debe'>
			DEBE<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='debe'>
			Variación DEBE<br />
		</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.haber' class='right' style='width:13px' />
			HABER<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='haber'>
			HABER<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='haber'>
			Variación HABER<br />
		</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.deudor' class='right' style='width:13px' />
			Deudor<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='deudor'>
			Deudor<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='deudor'>
			Variación Deudor<br />
		</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.acreedor' class='right' style='width:13px' />
			Acreedor<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='acreedor'>
			Acreedor<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='acreedor'>
			Variación Acreedor<br />
		</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.activo' class='right' style='width:13px' />
			Activo<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='activo'>
			Activo<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='activo'>
			Variación Activo<br />
		</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.pasivo' class='right' style='width:13px' />
			Pasivo<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='pasivo'>
			Pasivo<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='pasivo'>
			Variación Pasivo<br />
		</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.perdida' class='right' style='width:13px' />
			Perdida<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='perdida'>
			Perdida<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='perdida'>
			Variación Perdida<br />
		</th>
		<th width='100'>
			<img src='images/addbtn.png' alt='.ganancia' class='right' style='width:13px' />
			Ganancia<br />({$actual_range})
		</th>
		<th width='100' style='display:none;' class='ganancia'>
			Ganancia<br />({$compare_range})
		</th>
		<th width='100' style='display:none;' class='ganancia'>
			Variación Ganancia<br />
		</th>
	</tr>
	</thead>
	<tbody>");
	
	foreach($informe as $detalle){
		$saldo_actual = $detalle['dq_debe_actual']-$detalle['dq_haber_actual'];
		$saldo_compare = $detalle['dq_debe_compare']-$detalle['dq_haber_compare'];
		
		if($saldo_actual < 0){
			$saldo_actual = abs($saldo_actual);
			if($detalle['dm_tipo_cuenta'] == '1' || $detalle['dm_tipo_cuenta'] == '2'){
				$deudor_actual = $activo_actual = $perdida_actual = $ganancia_actual = 0.0;
				$acreedor_actual = $pasivo_actual = $saldo_actual;
				
				$total_acreedor += $saldo_actual;
				$total_pasivo += $saldo_actual;
			}else{
				$deudor_actual = $activo_actual = $pasivo_actual = $perdida_actual = 0.0;
				$acreedor_actual = $ganancia_actual = $saldo_actual;
				
				$total_acreedor += $saldo_actual;
				$total_ganancia += $saldo_actual;
			}
		}else{
			if($detalle['dm_tipo_cuenta'] == '1' || $detalle['dm_tipo_cuenta'] == '2'){
				$acreedor_actual = $pasivo_actual = $perdida_actual = $ganancia_actual = 0.0;
				$deudor_actual = $activo_actual = $saldo_actual;
				
				$total_deudor += $saldo_actual;
				$total_activo += $saldo_actual;
			}else{
				$acreedor_actual = $activo_actual = $pasivo_actual = $ganancia_actual = 0.0;
				$deudor_actual = $perdida_actual = $saldo_actual;
				
				$total_deudor += $saldo_actual;
				$total_perdida += $saldo_actual;
			}
		}
		
		//comparar
		if($saldo_compare < 0){
			$saldo_compare = abs($saldo_compare);
			if($detalle['dm_tipo_cuenta'] == '1' || $detalle['dm_tipo_cuenta'] == '2'){
				$deudor_compare = $activo_compare = $perdida_compare = $ganancia_compare = 0.0;
				$acreedor_compare = $pasivo_compare = $saldo_compare;
				
				$total_acreedor_compare += $saldo_compare;
				$total_pasivo_compare += $saldo_compare;
			}else{
				$deudor_compare = $activo_compare = $pasivo_compare = $perdida_compare = 0.0;
				$acreedor_compare = $ganancia_compare = $saldo_compare;
				
				$total_acreedor_compare += $saldo_compare;
				$total_ganancia_compare += $saldo_compare;
			}
		}else{
			if($detalle['dm_tipo_cuenta'] == '1' || $detalle['dm_tipo_cuenta'] == '2'){
				$acreedor_compare = $pasivo_compare = $perdida_compare = $ganancia_compare = 0.0;
				$deudor_compare = $activo_compare = $saldo_compare;
				
				$total_deudor_compare += $saldo_compare;
				$total_activo_compare += $saldo_compare;
			}else{
				$acreedor_compare = $activo_compare = $pasivo_compare = $ganancia_compare = 0.0;
				$deudor_compare = $perdida_compare = $saldo_compare;
				
				$total_deudor_compare += $saldo_compare;
				$total_perdida_compare += $saldo_compare;
			}
		}
		
			$total_debe += $detalle['dq_debe_actual'];
			$total_debe_compare += $detalle['dq_debe_compare'];
			
			$total_haber += $detalle['dq_haber_actual'];
			$total_haber_compare += $detalle['dq_haber_compare'];
			
			$debe_dif = moneda_local(abs($detalle['dq_debe_compare']-$detalle['dq_debe_actual']));
			$detalle['dq_debe_actual'] = moneda_local($detalle['dq_debe_actual']);
			$detalle['dq_debe_compare'] = moneda_local($detalle['dq_debe_compare']);
			
			$haber_dif = moneda_local(abs($detalle['dq_haber_compare']-$detalle['dq_haber_actual']));
			$detalle['dq_haber_actual'] = moneda_local($detalle['dq_haber_actual']);
			$detalle['dq_haber_compare'] = moneda_local($detalle['dq_haber_compare']);
			
			$acreedor_dif = moneda_local(abs($acreedor_actual-$acreedor_compare));
			$acreedor_actual = moneda_local($acreedor_actual);
			$acreedor_compare = moneda_local($acreedor_compare);
			
			$deudor_dif = moneda_local(abs($deudor_actual-$deudor_compare));
			$deudor_actual = moneda_local($deudor_actual);
			$deudor_compare = moneda_local($deudor_compare);
			
			$activo_dif = moneda_local(abs($activo_actual-$activo_compare));
			$activo_actual = moneda_local($activo_actual);
			$activo_compare = moneda_local($activo_compare);
			
			$pasivo_dif = moneda_local(abs($pasivo_actual-$pasivo_compare));
			$pasivo_actual = moneda_local($pasivo_actual);
			$pasivo_compare = moneda_local($pasivo_compare);
			
			$perdida_dif = moneda_local(abs($perdida_actual-$perdida_compare));
			$perdida_actual = moneda_local($perdida_actual);
			$perdida_compare = moneda_local($perdida_compare);
			
			$ganancia_dif = moneda_local(abs($ganancia_actual-$ganancia_compare));
			$ganancia_actual = moneda_local($ganancia_actual);
			$ganancia_compare = moneda_local($ganancia_compare);
			
			echo("
			<tr>
				<td>{$detalle['dg_codigo']}</td>
				<td>{$detalle['dg_cuenta_contable']}</td>
				<td align='right'>{$detalle['dq_debe_actual']}</td>
				<td align='right' style='display:none;' class='debe'>{$detalle['dq_debe_compare']}</td>
				<td align='right' style='display:none;' class='debe'>{$debe_dif}</td>
				<td align='right'>{$detalle['dq_haber_actual']}</td>
				<td align='right' style='display:none;' class='haber'>{$detalle['dq_haber_compare']}</td>
				<td align='right' style='display:none;' class='haber'>{$haber_dif}</td>
				<td align='right'>{$deudor_actual}</td>
				<td align='right' style='display:none;' class='deudor'>{$deudor_compare}</td>
				<td align='right' style='display:none;' class='deudor'>{$deudor_dif}</td>
				<td align='right'>{$acreedor_actual}</td>
				<td align='right' style='display:none;' class='acreedor'>{$acreedor_compare}</td>
				<td align='right' style='display:none;' class='acreedor'>{$acreedor_dif}</td>
				<td align='right'>{$activo_actual}</td>
				<td align='right' style='display:none;' class='activo'>{$activo_compare}</td>
				<td align='right' style='display:none;' class='activo'>{$activo_dif}</td>
				<td align='right'>{$pasivo_actual}</td>
				<td align='right' style='display:none;' class='pasivo'>{$pasivo_compare}</td>
				<td align='right' style='display:none;' class='pasivo'>{$pasivo_dif}</td>
				<td align='right'>{$perdida_actual}</td>
				<td align='right' style='display:none;' class='perdida'>{$perdida_compare}</td>
				<td align='right' style='display:none;' class='perdida'>{$perdida_dif}</td>
				<td align='right'>{$ganancia_actual}</td>
				<td align='right' style='display:none;' class='ganancia'>{$ganancia_compare}</td>
				<td align='right' style='display:none;' class='ganancia'>{$ganancia_dif}</td>
			</tr>");
	}
	
	$total_debe_dif = moneda_local($total_debe-$total_debe_compare);
	$total_haber_dif = moneda_local($total_haber-$total_haber_compare);
	$total_deudor_dif = moneda_local($total_deudor-$total_deudor_compare);
	$total_acreedor_dif = moneda_local($total_acreedor-$total_acreedor_compare);
	$total_activo_dif = moneda_local($total_activo-$total_activo_compare);
	$total_pasivo_dif = moneda_local($total_pasivo-$total_pasivo_compare);
	$total_perdida_dif = moneda_local($total_perdida-$total_perdida_compare);
	$total_ganancia_dif = moneda_local($total_ganancia-$total_ganancia_compare);
	
	$total_debe = moneda_local($total_debe);
	$total_haber = moneda_local($total_haber);
	$total_deudor = moneda_local($total_deudor);
	$total_acreedor = moneda_local($total_acreedor);
	$total_activo = moneda_local($total_activo);
	$total_pasivo = moneda_local($total_pasivo);
	$total_perdida = moneda_local($total_perdida);
	$total_ganancia = moneda_local($total_ganancia);
	
	$total_debe_compare = moneda_local($total_debe_compare);
	$total_haber_compare = moneda_local($total_haber_compare);
	$total_deudor_compare = moneda_local($total_deudor_compare);
	$total_acreedor_compare = moneda_local($total_acreedor_compare);
	$total_activo_compare = moneda_local($total_activo_compare);
	$total_pasivo_compare = moneda_local($total_pasivo_compare);
	$total_perdida_compare = moneda_local($total_perdida_compare);
	$total_ganancia_compare = moneda_local($total_ganancia_compare);
	
	$activo_menos_pasivo = $total_activo-$total_pasivo;
	$activo_menos_pasivo_compare = $total_activo_compare-$total_pasivo_compare;
	$activo_menos_pasivo_dif = $activo_menos_pasivo-$activo_menos_pasivo_compare;

	$ganancia_menos_perdida = $total_ganancia-$total_perdida;
	$ganancia_menos_perdida_compare = $total_ganancia_compare-$total_perdida_compare;
	$ganancia_menos_perdida_dif = $ganancia_menos_perdida-$ganancia_menos_perdida_compare;
	
	
	echo("
	<tfoot>
	<tr>
		<th colspan='2' align='right'>Totales</th>
		<th align='right'>{$total_debe}</th>
		<th align='right' style='display:none;' class='debe'>{$total_debe_compare}</th>
		<th align='right' style='display:none;' class='debe'>{$total_debe_dif}</th>
		<th align='right'>{$total_haber}</th>
		<th align='right' style='display:none;' class='haber'>{$total_haber_compare}</th>
		<th align='right' style='display:none;' class='haber'>{$total_haber_dif}</th>
		<th align='right'>{$total_deudor}</th>
		<th align='right' style='display:none;' class='deudor'>{$total_deudor_compare}</th>
		<th align='right' style='display:none;' class='deudor'>{$total_deudor_dif}</th>
		<th align='right'>{$total_acreedor}</th>
		<th align='right' style='display:none;' class='acreedor'>{$total_acreedor_compare}</th>
		<th align='right' style='display:none;' class='acreedor'>{$total_acreedor_dif}</th>
		<th align='right'>{$total_activo}</th>
		<th align='right' style='display:none;' class='activo'>{$total_activo_compare}</th>
		<th align='right' style='display:none;' class='activo'>{$total_activo_dif}</th>
		<th align='right'>{$total_pasivo}</th>
		<th align='right' style='display:none;' class='pasivo'>{$total_pasivo_compare}</th>
		<th align='right' style='display:none;' class='pasivo'>{$total_pasivo_dif}</th>
		<th align='right'>{$total_perdida}</th>
		<th align='right' style='display:none;' class='perdida'>{$total_perdida_compare}</th>
		<th align='right' style='display:none;' class='perdida'>{$total_perdida_dif}</th>
		<th align='right'>{$total_ganancia}</th>
		<th align='right' style='display:none;' class='ganancia'>{$total_ganancia_compare}</th>
		<th align='right' style='display:none;' class='ganancia'>{$total_ganancia_dif}</th>
	</tr>
	
	<tr>
		<th colspan='2' align='right'>Pérdida y ganancia</th>
		<th align='right'>0</th>
		<th align='right' style='display:none;' class='debe'>0</th>
		<th align='right' style='display:none;' class='debe'>0</th>
		<th align='right'>0</th>
		<th align='right' style='display:none;' class='haber'>0</th>
		<th align='right' style='display:none;' class='haber'>0</th>
		<th align='right'>0</th>
		<th align='right' style='display:none;' class='deudor'>0</th>
		<th align='right' style='display:none;' class='deudor'>0</th>
		<th align='right'>0</th>
		<th align='right' style='display:none;' class='acreedor'>0</th>
		<th align='right' style='display:none;' class='acreedor'>0</th>
		<th align='right'>0</th>
		<th align='right' style='display:none;' class='activo'>0</th>
		<th align='right' style='display:none;' class='activo'>0</th>
		<th align='right'>{$activo_menos_pasivo}</th> 
		<th align='right' style='display:none;' class='pasivo'>{$activo_menos_pasivo_compare}</th>
		<th align='right' style='display:none;' class='pasivo'>{$activo_menos_pasivo_dif}</th>
		<th align='right'>{$ganancia_menos_perdida}</th>
		<th align='right' style='display:none;' class='perdida'>{$ganancia_menos_perdida_compare}</th>
		<th align='right' style='display:none;' class='perdida'>{$ganancia_menos_perdida_dif}</th>
		<th align='right'>0</th>
		<th align='right' style='display:none;' class='ganancia'>0</th>
		<th align='right' style='display:none;' class='ganancia'>0</th>
	</tr>
	
	<tr>
		<th colspan='2' align='right'>Totales</th>
		<th align='right'>{$total_debe}</th>
		<th align='right' style='display:none;' class='debe'>{$total_debe_compare}</th>
		<th align='right' style='display:none;' class='debe'>{$total_debe_dif}</th>
		<th align='right'>{$total_haber}</th>
		<th align='right' style='display:none;' class='haber'>{$total_haber_compare}</th>
		<th align='right' style='display:none;' class='haber'>{$total_haber_dif}</th>
		<th align='right'>{$total_deudor}</th>
		<th align='right' style='display:none;' class='deudor'>{$total_deudor_compare}</th>
		<th align='right' style='display:none;' class='deudor'>{$total_deudor_dif}</th>
		<th align='right'>{$total_acreedor}</th>
		<th align='right' style='display:none;' class='acreedor'>{$total_acreedor_compare}</th>
		<th align='right' style='display:none;' class='acreedor'>{$total_acreedor_dif}</th>
		<th align='right'>{$total_activo}</th>
		<th align='right' style='display:none;' class='activo'>{$total_activo_compare}</th>
		<th align='right' style='display:none;' class='activo'>{$total_activo_dif}</th>
		<th align='right'>{$total_pasivo}</th>
		<th align='right' style='display:none;' class='pasivo'>{$total_pasivo_compare}</th>
		<th align='right' style='display:none;' class='pasivo'>{$total_pasivo_dif}</th>
		<th align='right'>{$total_perdida}</th>
		<th align='right' style='display:none;' class='perdida'>{$total_perdida_compare}</th>
		<th align='right' style='display:none;' class='perdida'>{$total_perdida_dif}</th>
		<th align='right'>{$total_ganancia}</th>
		<th align='right' style='display:none;' class='ganancia'>{$total_ganancia_compare}</th>
		<th align='right' style='display:none;' class='ganancia'>{$total_ganancia_dif}</th>
	</tr>
	</tfoot>
	</tbody></table>");
}
?>
</div>
<script type="text/javascript">
	$("#balance thead th img").toggle(function(){
		$($(this).attr('alt')).show();
		$("#balance").width($("#balance").width()+200);
		$(this).attr('src','images/minusbtn.png');
	},function(){
		$($(this).attr('alt')).hide();
		$("#balance").width($("#balance").width()-192);
		$(this).attr('src','images/addbtn.png');
	});
	$('#balance').tableExport().tableAdjust(20);
</script>