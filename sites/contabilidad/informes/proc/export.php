<?php
error_reporting(E_ALL);
ini_set('memory_limit',-1);

date_default_timezone_set('Europe/London');
	define('MAIN',1);
	require_once('../../../../inc/global.php');
	require_once('../../../../inc/PHPExcel.php');
	if(!isset($_POST))exit();
	
	$textData = html_entity_decode(file_get_contents("../cache/informe_{$_POST['fid']}.xml"));
	$textData = str_replace('&','&amp;',$textData);
	$data = new SimpleXMLElement($textData);

	$filtro = array();
	foreach($data->detalle as $d){
		if(!((isset($_POST['mes']) && $d->dc_mes_contable != $_POST['mes']) ||
			(isset($_POST['debe']) && (float)$d->dq_debe == 0.0) ||
			(isset($_POST['haber']) && (float)$d->dq_haber == 0.0) ||
			(isset($_POST['cuenta']) && $d->dg_codigo != $_POST['cuenta']))){
			
				$filtro[] = $d;
		}
	}
	unset($data);
	
	$excel = new PHPExcel();
	
	$excel->getProperties()->setCreator("{$userdata['dg_nombres']} {$userdata['dg_ap_paterno']} {$userdata['dg_ap_materno']}")
							 ->setLastModifiedBy("{$userdata['dg_nombres']} {$userdata['dg_ap_paterno']} {$userdata['dg_ap_materno']}")
							 ->setTitle("Informe {$_POST['fid']}")
							 ->setSubject("Informe {$_POST['fid']}")
							 ->setCategory("Informes");
							 
	$letras = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',
    'P','Q','R','S','T','U','V','W','X',
'Y','Z','AA','AB','AC','AD','AE',
'AF','AG','AH','AI','AJ','AK','AL',
'AM','AN','AO','AP','AQ','AR','AS','AT','AU');
	
	$indices = array('dg_numero_interno','df_fecha_contable','dg_tipo_movimiento','df_fecha_emision','dg_banco','dg_glosa_detalle','dg_tipo_documento','dg_cheque','dg_rut','dg_activo_fijo','dg_usuario','dg_cebe','dg_ceco','dg_doc_compra','dg_doc_venta'
            ,'factura_compra','factura_venta','dq_nota_venta','dq_orden_servicio','dg_cliente','dg_tipo_cliente','dq_orden_compra',
            'dg_producto','dg_marca','dg_proveedor','dg_tipo_proveedor','dg_tipo_producto','dg_linea_negocio','banco_pago',
            'banco_cobro','dq_guia_recepcion','dg_medio_pago','dg_medio_cobro','mercado_cliente','mercado_proveedor',
            'nota_credito','nota_prov','dg_segmento','dg_cheque','cheque_fecha','dg_bodega');
	$titulos = array('Número interno','Periodo contable','Tipo de movimiento','Fecha emisión','Banco','Glosa detalle','Tipo de documento','Cheque','RUT','Activo Fijo','Responsable','Centro beneficio','Centro costo','Documento de compra','Documento de venta','Factura de Compra',
    'Factura de Venta','Nota de Venta','Orden de Servicio','Cliente','Tipo Cliente','Orden de Compra','Producto','Marca','Proveedor',
'tipo Proveedor','Tipo Producto','Linea de Negocio','Banco','Banco Cobro','Guia Recepcion','Medio de Pago',
'Medio de Cobro','Mercado Cliente','Mercado Proveedor','Nota de Credito','Nota credito Proveedor','Segmento','Cheque',
'Fecha Cheque','Bodega');
        
	if(isset($_POST['ex_fields'])){
		foreach($indices as $i => $v){
			if(!in_array($i,$_POST['ex_fields'])){
				unset($indices[$i]);
				unset($titulos[$i]);
			}
		}
	}else{
		$indices = array();
		$titulos = array();
	}
	
	$excel->setActiveSheetIndex(0)
		  ->setCellValue('A1', 'Comprobante')
          ->setCellValue('B1', 'Código cuenta')
          ->setCellValue('C1', 'Descripción')
		  ->setCellValue('D1', 'DEBE')
		  ->setCellValue('E1', 'HABER');
	$col = 5;
	foreach($titulos as $v){
		$excel->setActiveSheetIndex(0)
		->setCellValue($letras[$col++].'1', $v);
	}
	
	$row = 2;
	foreach($filtro as $detalle){
		$excel->setActiveSheetIndex(0)
          ->setCellValue("A{$row}", $detalle->dg_comprobante)
          ->setCellValue("B{$row}", $detalle->dg_codigo)
		  ->setCellValue("C{$row}", $detalle->dg_cuenta_contable)
		  ->setCellValue("D{$row}", $detalle->dq_debe)
		  ->setCellValue("E{$row}", $detalle->dq_haber);
		  
		  $col = 5;
		  foreach($indices as $i){
		  	$excel->setActiveSheetIndex(0)
          		  ->setCellValue("{$letras[$col]}{$row}", $detalle->$i);
			$col++;
		  }
		  $row++;
	}
	
	foreach($letras as $letra){
		$excel->getActiveSheet()->getColumnDimension($letra)->setAutoSize(true);
	}
	$excel->setActiveSheetIndex(0);
if($_POST['ex_type'] == '1'){
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$_POST['fid'].'.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
}else if($_POST['ex_type'] == '2'){
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$_POST['fid'].'.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
}else if($_POST['ex_type'] == '3'){
	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment;filename="'.$_POST['fid'].'.pdf"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($excel, 'PDF');
}

$objWriter->save('php://output');
exit;
?>