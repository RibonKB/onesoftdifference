<?php

define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$tables = "
(SELECT * FROM tb_comprobante_contable WHERE dc_empresa = {$empresa} AND dm_anulado='0') AS com
JOIN tb_comprobante_contable_detalle AS det ON det.dc_comprobante = com.dc_comprobante
JOIN tb_cuenta_contable AS cc ON det.dc_cuenta_contable = cc.dc_cuenta_contable
LEFT JOIN tb_tipo_movimiento AS mov ON mov.dc_tipo_movimiento = com.dc_tipo_movimiento
LEFT JOIN tb_banco AS bco ON bco.dc_banco = det.dc_banco
LEFT JOIN tb_usuario AS us ON us.dc_usuario = com.dc_usuario_creacion
LEFT JOIN tb_tipo_documento AS doc ON doc.dc_tipo_documento = det.dc_tipo_documento
LEFT JOIN tb_cebe AS cebe ON cebe.dc_cebe = det.dc_cebe
LEFT JOIN tb_ceco AS ceco ON ceco.dc_ceco = det.dc_ceco";

$fields = '
com.dg_comprobante, com.dc_mes_contable, com.dc_anho_contable, com.dg_numero_interno,
DATE_FORMAT(com.df_fecha_contable,"%d/%m/%Y") AS df_fecha_contable,
DATE_FORMAT(com.df_fecha_emision,"%d/%m/%Y") AS df_fecha_emision,
det.dq_debe, det.dq_haber, det.dg_glosa AS dg_glosa_detalle, det.dg_numero_documento,
det.dg_cheque, det.dg_rut, det.dg_activo_fijo, det.dg_doc_compra, det.dg_doc_venta,
mov.dg_tipo_movimiento, us.dg_usuario, cc.dg_cuenta_contable, cc.dg_codigo,
doc.dg_tipo_documento, bco.dg_banco, cebe.dg_cebe, ceco.dg_ceco';

$conditions = '';

if($_POST['inf_cta_hasta'] == ''){
	if($_POST['inf_cta_desde'] != ''){
		$conditions .= "
		cc.dg_codigo = '{$_POST['inf_cta_desde']}' AND";
	}
}else{
	if($_POST['inf_cta_desde'] == ''){
		$conditions .= "
		cc.dg_codigo = '{$_POST['inf_cta_hasta']}' AND";
	}else{
		$conditions .= "
		(cc.dg_codigo BETWEEN '{$_POST['inf_cta_desde']}' AND '{$_POST['inf_cta_hasta']}') AND";
	}
}

if($_POST['inf_mes_hasta'] == ''){
	if($_POST['inf_mes_desde'] != ''){
		$conditions .="
		com.dc_mes_contable = {$_POST['inf_mes_desde']} AND";
	}
}else{
	if($_POST['inf_mes_desde'] == ''){
		$conditions .="
		com.dc_mes_contable = {$_POST['inf_mes_hasta']} AND";
	}else{
		$conditions .= "
		(com.dc_mes_contable BETWEEN {$_POST['inf_mes_desde']} AND {$_POST['inf_mes_hasta']}) AND";
	}
}

if($_POST['inf_anho_hasta'] == ''){
	$conditions .="
	com.dc_anho_contable = '{$_POST['inf_anho_desde']}' AND";
}else{
	$conditions .= "
	(com.dc_anho_contable BETWEEN '{$_POST['inf_anho_desde']}' AND '{$_POST['inf_anho_hasta']}') AND";
}

if(isset($_POST['inf_tipo_mov'])){
	$conditions .= "
	com.dc_tipo_movimiento = {$_POST['inf_tipo_mov']} AND";
}

if(isset($_POST['inf_glosa_head'])){
	$conditions .= "
	com.dg_glosa LIKE '%{$_POST['inf_glosa_head']}%' AND";
}

if(isset($_POST['inf_emision_desde'])){
	$emision = explode('/',$_POST['inf_emision_desde'],3);
	$_POST['inf_emision_desde'] = "{$emision[2]}-{$emision[1]}-{$emision[0]}";
	if($_POST['inf_emision_hasta'] == ''){
		$conditions .= "
		(com.df_fecha_emision BETWEEN '{$_POST['inf_emision_desde']}' AND '{$_POST['inf_emision_desde']} 23:59:59') AND";
	}else{
		$emision = explode('/',$_POST['inf_emision_hasta'],3);
		$_POST['inf_emision_hasta'] = "{$emision[2]}-{$emision[1]}-{$emision[0]}";
		$conditions .= "
		(com.df_fecha_emision BETWEEN '{$_POST['inf_emision_desde']}' AND '{$_POST['inf_emision_hasta']}') AND";
	}
}

if(isset($_POST['inf_responsable'])){
	$responsable = $db->select('tb_usuario','dc_usuario',"dg_usuario = '{$_POST['inf_responsable']}'");
	if(!count($responsable)){
		$error_man->showWarning('El usuario responsable es inválido, no existe. pruebe con las sugerencias del autocompletado');
		exit();
	}else{
		$conditions .= "
		com.dc_usuario_creacion = {$responsable[0]['dc_usuario']} AND";
	}
}

if(isset($_POST['inf_glosa_det'])){
	$conditions .= "
	det.dg_glosa LIKE '%{$_POST['inf_glosa_det']}%' AND";
}

if(isset($_POST['inf_tipo_doc'])){
	$conditions .= "
	det.dc_tipo_documento = '{$_POST['inf_tipo_doc']}' AND";
}

if(isset($_POST['inf_cheque_desde'])){
	if($_POST['inf_cheque_hasta'] == ''){
		$conditions .= "
		det.dg_cheque = '{$_POST['inf_cheque_desde']}' AND";
	}else{
		$conditions .= "
		(det.dg_cheque BETWEEN '{$_POST['inf_cheque_desde']}' AND '{$_POST['inf_cheque_hasta']}') AND";
	}
}

if(isset($_POST['inf_rut'])){
	$conditions .= "
	det.dg_rut = '{$_POST['inf_rut']}' AND";
}

if(isset($_POST['inf_act_fijo'])){
	$conditions .= "
	det.dg_activo_fijo = '{$_POST['inf_act_fijo']}' AND";
}

if(isset($_POST['inf_banco'])){
	$conditions .= "
	det.dc_banco = {$_POST['inf_banco']} AND";
}

if(isset($_POST['inf_cebe'])){
	$conditions .= "
	det.dc_cebe = {$_POST['inf_cebe']} AND";
}

if(isset($_POST['inf_ceco'])){
	$conditions .= "
	det.dc_ceco = {$_POST['inf_ceco']} AND";
}

if(isset($_POST['inf_doc_compra'])){
	$conditions .= "
	det.dg_doc_compra = '{$_POST['inf_doc_compra']}' AND";
}

if(isset($_POST['inf_doc_venta'])){
	$conditions .= "
	det.dg_doc_venta = '{$_POST['inf_doc_venta']}' AND";
}

$conditions = substr($conditions,0,-4).' ORDER BY cc.dg_codigo';

$informe = $db->select($tables,$fields,$conditions);
unset($tables,$fields,$conditions);

$dom = new DOMDocument('1.0','UTF-8');
$dom->standalone = true;

$dom_informe = $dom->createElement('informe');
$dom->appendChild($dom_informe);
	
	$meses_contables = $db->select('tb_mes_contable','dc_mes','',array("order_by"=>"dc_mes"));
	$listado = array();
	foreach($meses_contables as $m){
		$listado[$m['dc_mes']] = array(0.0,0.0);
	}
	$total_debe = 0.0;
	$total_haber = 0.0;

	foreach($informe as $detalles){
		$detalle = $dom->createElement('detalle');
		
		foreach($detalles as $i => $v){
			$detalle->appendChild($dom->createElement($i,htmlentities($v)));
		}
		
		$dom_informe->appendChild($detalle);
		
		//aprovechamos el ciclo para hacer las sumas de saldos
		$listado[$detalles['dc_mes_contable']][0] += $detalles['dq_debe'];
		$listado[$detalles['dc_mes_contable']][1] += $detalles['dq_haber'];
		$total_debe += $detalles['dq_debe'];
		$total_haber += $detalles['dq_haber'];
	}
	
	$fileid = uniqid('',1);
	$salida = fopen("../cache/informe_{$fileid}.xml",'w');
	fwrite($salida,$dom->saveXML());
	
	echo("
	<table class='tab' width='100%'>
	<thead>
	<tr>
		<th width='80'>Mes contable</th>
		<th>DEBE</th>
		<th>HABER</th>
		<th>SALDO</th>
	</tr>
	</thead>
	<tbody>");
	
	foreach($listado as $i => $v){
		$saldo = number_format($v[0]-$v[1],2,',','.');
		$debe = number_format($v[0],2,',','.');
		$haber = number_format($v[1],2,',','.');
		echo("
		<tr>
			<td align='center'>{$i}</td>
			<td align='right' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&mes={$i}&debe=1\")'><a href='#'>{$debe}</a></td>
			<td align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&mes={$i}&haber=1\")'>{$haber}</a></td>
			<td align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&mes={$i}\")'>{$saldo}</a></td>
		</tr>");
	}
	
	$saldo_total = number_format($total_debe-$total_haber,2,',','.');
	$total_debe = number_format($total_debe,2,',','.');
	$total_haber = number_format($total_haber,2,',','.');
	echo("</tbody>
	<tfoot>
	<tr>
		<th align='right'>Totales</th>
		<th align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&debe=1\")'>{$total_debe}</a></th>
		<th align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&haber=1\")'>{$total_haber}</a></th>
		<th align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}\")'>{$saldo_total}</a></th>
	</tr>
	</tfoot></table>");
	
?>