<?php
error_reporting(E_ALL);
ini_set('memory_limit','1024M');

define("MAIN",1);
require_once("../../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$tables = "
(SELECT * FROM tb_comprobante_contable WHERE dc_empresa = {$empresa} AND dm_anulado='0') AS com
JOIN tb_comprobante_contable_detalle AS det ON det.dc_comprobante = com.dc_comprobante
JOIN tb_comprobante_contable_detalle_analitico AS detAn ON detAn.dc_detalle_financiero=det.dc_detalle 
JOIN tb_cuenta_contable AS cc ON det.dc_cuenta_contable = cc.dc_cuenta_contable
LEFT JOIN tb_tipo_movimiento AS mov ON mov.dc_tipo_movimiento = com.dc_tipo_movimiento
LEFT JOIN tb_banco bco ON bco.dc_banco = detAn.dc_banco
LEFT JOIN tb_usuario AS us ON us.dc_usuario = com.dc_usuario_creacion
LEFT JOIN tb_tipo_documento AS doc ON doc.dc_tipo_documento = det.dc_tipo_documento
LEFT JOIN tb_cebe AS cebe ON cebe.dc_cebe = detAn.dc_cebe
LEFT JOIN tb_ceco AS ceco ON ceco.dc_ceco = detAn.dc_ceco
LEFT JOIN tb_factura_compra fc ON fc.dc_factura=detAn.dc_factura_compra
LEFT JOIN tb_factura_venta fv ON fv.dc_factura=detAn.dc_factura_venta
LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta=detAn.dc_nota_venta
LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio=detAn.dc_orden_servicio
LEFT JOIN tb_proveedor prov ON prov.dc_proveedor=detAn.dc_proveedor
LEFT JOIN tb_tipo_proveedor tprov ON tprov.dc_tipo_proveedor=detAn.dc_tipo_proveedor
LEFT JOIN tb_cliente cl ON cl.dc_cliente=detAn.dc_cliente
LEFT JOIN tb_tipo_cliente tcl ON tcl.dc_tipo_cliente=detAn.dc_tipo_cliente
LEFT JOIN tb_orden_compra oc ON oc.dc_orden_compra=detAn.dc_orden_compra
LEFT JOIN tb_producto prod ON prod.dc_producto=detAn.dc_producto
LEFT JOIN tb_marca ma ON ma.dc_marca=detAn.dc_marca
LEFT JOIN tb_linea_negocio ln ON ln.dc_linea_negocio=detAn.dc_linea_negocio
LEFT JOIN tb_tipo_producto tprod ON tprod.dc_tipo_producto=detAn.dc_tipo_producto
LEFT JOIN tb_banco_cobro bac ON bac.dc_banco=detAn.dc_banco_cobro
LEFT JOIN tb_guia_recepcion grec ON grec.dc_guia_recepcion=detAn.dc_guia_recepcion
LEFT JOIN tb_medio_pago_proveedor mpprov ON mpprov.dc_medio_pago=detAn.dc_medio_pago_proveedor
LEFT JOIN tb_medio_cobro_cliente mccl ON mccl.dc_medio_cobro=detAn.dc_medio_cobro_cliente
LEFT JOIN tb_mercado merc ON merc.dc_mercado=detAn.dc_mercado_cliente
LEFT JOIN tb_mercado merp ON merp.dc_mercado=detAn.dc_mercado_proveedor
LEFT JOIN tb_nota_credito ncred ON ncred.dc_nota_credito=detAn.dc_nota_credito
LEFT JOIN tb_nota_credito_proveedor ntcp ON ntcp.dc_nota_credito=detAn.dc_nota_credito_proveedor
LEFT JOIN tb_segmento seg ON seg.dc_segmento=detAn.dc_segmento
LEFT JOIN tb_bodega bod ON bod.dc_bodega=detAn.dc_bodega";

$fields = '
com.dg_comprobante, com.dc_mes_contable, com.dc_anho_contable, com.dg_numero_interno,
DATE_FORMAT(com.df_fecha_contable,"%d/%m/%Y") AS df_fecha_contable,
DATE_FORMAT(com.df_fecha_emision,"%d/%m/%Y") AS df_fecha_emision,
detAn.dq_debe, detAn.dq_haber, detAn.dg_glosa AS dg_glosa_detalle, det.dg_numero_documento,
detAn.dg_cheque, cl.dg_rut,fc.dq_folio AS factura_compra, det.dg_activo_fijo, det.dg_doc_compra, 
det.dg_doc_venta,mov.dg_tipo_movimiento, us.dg_usuario, cc.dg_cuenta_contable, cc.dg_codigo,
doc.dg_tipo_documento, bco.dg_banco, cebe.dg_cebe, ceco.dg_ceco,fv.dq_folio AS factura_venta,
nv.dq_nota_venta,os.dq_orden_servicio,cl.dg_razon AS dg_cliente,tcl.dg_tipo_cliente,oc.dq_orden_compra,
prod.dg_producto,ma.dg_marca,prov.dg_razon AS dg_proveedor,tprov.dg_tipo_proveedor,tprod.dg_tipo_producto,

ln.dg_linea_negocio,bco.dg_banco AS Banco_pago,bac.dg_banco AS banco_cobro,grec.dq_guia_recepcion,mpprov.dg_medio_pago AS pago_prov,

mccl.dg_medio_cobro,
merc.dg_mercado AS mercado_cliente,
merp.dg_mercado AS mercado_prov,
ncred.dq_nota_credito AS nota_credito,
ntcp.dq_nota_credito AS nota_prov,
seg.dg_segmento,detAn.dg_cheque,
DATE_FORMAT (detAn.df_cheque, "%d/%m/%Y") AS cheque_fecha,
bod.dg_bodega';

$conditions = '';

if($_POST['inf_cta_hasta'] == ''){
	if($_POST['inf_cta_desde'] != ''){
		$conditions .= "
		cc.dg_codigo = '{$_POST['inf_cta_desde']}' AND";
	}
}else{
	if($_POST['inf_cta_desde'] == ''){
		$conditions .= "
		cc.dg_codigo = '{$_POST['inf_cta_hasta']}' AND";
	}else{
		$conditions .= "
		(cc.dg_codigo BETWEEN '{$_POST['inf_cta_desde']}' AND '{$_POST['inf_cta_hasta']}') AND";
	}
}

if($_POST['inf_mes_hasta'] == ''){
	if($_POST['inf_mes_desde'] != ''){
		$conditions .="
		com.dc_mes_contable = {$_POST['inf_mes_desde']} AND";
	}
}else{
	if($_POST['inf_mes_desde'] == ''){
		$conditions .="
		com.dc_mes_contable = {$_POST['inf_mes_hasta']} AND";
	}else{
		$conditions .= "
		(com.dc_mes_contable BETWEEN {$_POST['inf_mes_desde']} AND {$_POST['inf_mes_hasta']}) AND";
	}
}

if($_POST['inf_anho_hasta'] == ''){
	$conditions .="
	com.dc_anho_contable = '{$_POST['inf_anho_desde']}' AND";
}else{
	$conditions .= "
	(com.dc_anho_contable BETWEEN '{$_POST['inf_anho_desde']}' AND '{$_POST['inf_anho_hasta']}') AND";
}

if(isset($_POST['inf_tipo_mov'])){
	$conditions .= "
	com.dc_tipo_movimiento = {$_POST['inf_tipo_mov']} AND";
}

if(isset($_POST['inf_glosa_head'])){
	$conditions .= "
	com.dg_glosa LIKE '%{$_POST['inf_glosa_head']}%' AND";
}

if(isset($_POST['inf_emision_desde'])){
	$emision = explode('/',$_POST['inf_emision_desde'],3);
	$_POST['inf_emision_desde'] = "{$emision[2]}-{$emision[1]}-{$emision[0]}";
	if($_POST['inf_emision_hasta'] == ''){
		$conditions .= "
		(com.df_fecha_emision BETWEEN '{$_POST['inf_emision_desde']}' AND '{$_POST['inf_emision_desde']} 23:59:59') AND";
	}else{
		$emision = explode('/',$_POST['inf_emision_hasta'],3);
		$_POST['inf_emision_hasta'] = "{$emision[2]}-{$emision[1]}-{$emision[0]}";
		$conditions .= "
		(com.df_fecha_emision BETWEEN '{$_POST['inf_emision_desde']}' AND '{$_POST['inf_emision_hasta']}') AND";
	}
}

if(isset($_POST['inf_responsable'])){
	$responsable = $db->select('tb_usuario','dc_usuario',"dg_usuario = '{$_POST['inf_responsable']}'");
	if(!count($responsable)){
		$error_man->showWarning('El usuario responsable es inválido, no existe. pruebe con las sugerencias del autocompletado');
		exit();
	}else{
		$conditions .= "
		com.dc_usuario_creacion = {$responsable[0]['dc_usuario']} AND";
	}
}

if(isset($_POST['inf_glosa_det'])){
	$conditions .= "
	det.dg_glosa LIKE '%{$_POST['inf_glosa_det']}%' AND";
}

if(isset($_POST['inf_tipo_doc'])){
	$conditions .= "
	det.dc_tipo_documento = '{$_POST['inf_tipo_doc']}' AND";
}

if(isset($_POST['inf_cheque_desde'])){
	if($_POST['inf_cheque_hasta'] == ''){
		$conditions .= "
		det.dg_cheque = '{$_POST['inf_cheque_desde']}' AND";
	}else{
		$conditions .= "
		(det.dg_cheque BETWEEN '{$_POST['inf_cheque_desde']}' AND '{$_POST['inf_cheque_hasta']}') AND";
	}
}

if(isset($_POST['inf_rut'])){
	$conditions .= "
	det.dg_rut = '{$_POST['inf_rut']}' AND";
}

if(isset($_POST['inf_act_fijo'])){
	$conditions .= "
	det.dg_activo_fijo = '{$_POST['inf_act_fijo']}' AND";
}

if(isset($_POST['inf_banco'])){
	$conditions .= "
	det.dc_banco = {$_POST['inf_banco']} AND";
}

if(isset($_POST['inf_cebe'])){
	$conditions .= "
	det.dc_cebe = {$_POST['inf_cebe']} AND";
}

if(isset($_POST['inf_ceco'])){
	$conditions .= "
	det.dc_ceco = {$_POST['inf_ceco']} AND";
}

if(isset($_POST['inf_doc_compra'])){
	$conditions .= "
	det.dg_doc_compra = '{$_POST['inf_doc_compra']}' AND";
}

if(isset($_POST['inf_doc_venta'])){
	$conditions .= "
	det.dg_doc_venta = '{$_POST['inf_doc_venta']}' AND";
}

$conditions = substr($conditions,0,-4).' ORDER BY cc.dg_codigo';
$informe = $db->select($tables,$fields,$conditions);
unset($tables,$fields,$conditions);

$dom = new DOMDocument('1.0','UTF-8');
$dom->standalone = true;

$dom_informe = $dom->createElement('informe');
$dom->appendChild($dom_informe);

	$cuentas = array();
	
	$total_debe = 0.0;
	$total_haber = 0.0;

	foreach($informe as $detalles){
		$detalle = $dom->createElement('detalle');
		
		foreach($detalles as $i => $v){
			$v = str_replace('&','_',$v);
			$detalle->appendChild($dom->createElement(htmlentities($i),htmlentities($v)));
		}
		
		$dom_informe->appendChild($detalle);
		
		$cuentas[$detalles['dg_codigo']][] = $detalles;
		$total_debe += $detalles['dq_debe'];
		$total_haber += $detalles['dq_haber'];
	}
	
	$fileid = uniqid('',1);
	$salida = fopen("../cache/informe_{$fileid}.xml",'w');
	fwrite($salida,$dom->saveXML());
	
	$listado = array();
	foreach($cuentas as $i => $v){
		$listado[$i] = array($v[0]['dg_cuenta_contable'],0.0,0.0);
		foreach($v as $d){
			$listado[$i][1] += $d['dq_debe'];
			$listado[$i][2] += $d['dq_haber'];
		}
	}
	
	echo("
	<table class='tab' width='100%'>
	<thead>
	<tr>
		<th>Cuenta contable</th>
		<th>Descripción</th>
		<th>DEBE</th>
		<th>HABER</th>
		<th>SALDO</th>
	</tr>
	</thead>
	<tbody>");
	$i=0;
	foreach($listado as $i => $v){
		$saldo = number_format($v[1]-$v[2],2,',','.');
		$debe = number_format($v[1],2,',','.');
		$haber = number_format($v[2],2,',','.');
		echo("
		<tr>
			<td>{$i}</td>
			<td align='left'>{$v[0]}</td>
			<td align='right' id='debe_$i' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&cuenta={$i}&debe=1\")'><a href='#'>{$debe}</a></td>
			<td align='right' id='haber_$i'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&cuenta={$i}&haber=1\")'>{$haber}</a></td>
			<td align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&cuenta={$i}\")'>{$saldo}</a></td>
		</tr>");
	}
	
	$saldo_total = number_format($total_debe-$total_haber,2,',','.');
	$total_debe = number_format($total_debe,2,',','.');
	$total_haber = number_format($total_haber,2,',','.');
	echo("</tbody>
	<tfoot>
	<tr>
		<th align='right' colspan='2'>Totales</th>
		<th align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&debe=1\")'>{$total_debe}</a></th>
		<th align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}&haber=1\")'>{$total_haber}</a></th>
		<th align='right'><a href='#' onclick='loadOverlay(\"sites/contabilidad/informes/detallado.php?fid={$fileid}\")'>{$saldo_total}</a></th>
	</tr>
	</tfoot></table><label id='ttlD'></label><label id='ttlH'></label><div class='center'>");

if($_POST['inf_mes_desde'] == '' && $_POST['inf_mes_hasta'] == '' && $_POST['inf_anho_hasta'] == ''){
	echo("<button type='button' class='button' id='folio_print'><img src='images/print.png' alt='' /> Imprimir con folio</button>");
}

echo("<button type='button' class='button' id='quick_print'><img src='images/print.png' alt='' /> Impresión rápida</button></div>");
?>
<script type="text/javascript">

    
	$('#folio_print').click(function(){
		window.open('sites/contabilidad/informes/print_libro_mayor.php?m=1&i=<?=$fileid ?>','pdf_view','width=800,height=600');
	});
	$('#quick_print').click(function(){
		window.open('sites/contabilidad/informes/print_libro_mayor.php?m=0&i=<?=$fileid ?>','pdf_view','width=800,height=600');
	});
</script>
