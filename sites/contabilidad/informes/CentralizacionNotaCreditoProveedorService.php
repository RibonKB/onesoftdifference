<?php

require_once 'inc/AbstractFactoryService.class.php';

class CentralizacionNotaCreditoProveedorService extends AbstractFactoryService{

  public function getTables($r){
    return "tb_nota_credito_proveedor nc
  		JOIN tb_tipo_nota_credito_proveedor t_nc ON t_nc.dc_tipo = nc.dc_tipo
  		JOIN tb_proveedor pr ON pr.dc_proveedor = nc.dc_proveedor
  		JOIN tb_tipo_proveedor t ON t.dc_tipo_proveedor = pr.dc_tipo_proveedor AND t.dm_incluido_libro_compra = 1
  		LEFT JOIN tb_usuario us ON us.dc_usuario = nc.dc_usuario_creacion";
  }

  public function getCampos($r){
    return '"NC", "", "", nc.df_emision, pr.dg_rut, pr.dg_razon, 0, -SUM(nc.dq_neto), -SUM(nc.dq_iva), -SUM(nc.dq_total),
  	GROUP_CONCAT(nc.dq_nota_credito SEPARATOR "<br />"), nc.dq_folio, nc.dm_nula, t_nc.dg_tipo, us.dg_usuario,GROUP_CONCAT(nc.dc_nota_credito)';
  }

  public function getcondiciones($r){
    $empresa = $this->factory->getEmpresa();
    $cond = "nc.dc_empresa = {$empresa} AND ".
            "YEAR(nc.df_libro_compra) = {$r->dc_anho} AND ".
            "MONTH(nc.df_libro_compra) = {$r->dc_mes} AND ".
            "nc.dm_nula = 0 AND ".
            "nc.dm_centralizada = 0";

    if(isset($r->dc_proveedor)):
      $proveedores = $this->getCondicionesProveedor($r);
      $cond .= " AND nc.dc_proveedor IN ({$proveedores})";
    endif;

    return $cond;

  }

  public function initCentralizacion($r){
    if(!isset($r->dc_documento['NC']) or empty($r->dc_documento['NC'])):
      $this->nc = array();
      return;
    endif;

    $this->initDocumentos($r);
    $this->initFacturaCompra();
    $this->initNotaVenta();
    $this->initOrdenServicio();
    $this->initClientes();

    //Para notas de crédito que mueven stock
    $this->initCuentaContableLineasNegocio();

    //Para notas de crédito de servicios y gastos
    $this->initCuentasServicios();

  }

  private function initDocumentos($r){
    $this->validaIds($r->dc_documento['NC']);
    $this->nc = $this->getNotasCredito($r->dc_documento['NC']);
    $this->initDetallesNotaCredito($r->dc_documento['NC']);

    $this->initTipoNotaCredito();
  }

  private function validaIds($ncs){
    foreach($ncs as &$nc):
      if(empty($nc) or !preg_match('/^\d+$/',$nc)):
        $this->getErrorMan()->showWarning("Existen Notas de Crédito con un formato incorrecto,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;
    endforeach;

    foreach($ncs as &$nc):
      $nc = intval($nc);
    endforeach;
  }

  private function getNotasCredito($ncs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(",?",count($ncs)),1);

    $select = $db->prepare(
                $db->select('tb_nota_credito_proveedor nc
                             JOIN tb_proveedor p ON p.dc_proveedor = nc.dc_proveedor',
                             'nc.dc_nota_credito, nc.dq_folio, nc.dq_nota_credito, nc.dc_proveedor, nc.dq_total, nc.dq_iva, p.dc_cuenta_contable dc_cuenta_proveedor,
                              p.dc_tipo_proveedor, nc.dc_factura, p.dc_mercado dc_mercado_proveedor, nc.dc_tipo',
                             'nc.dc_empresa = ? AND nc.dm_centralizada = 0 AND nc.dc_nota_credito IN ('.$questions.')'));
    $select->bindValue(1, $this->factory->getEmpresa(), PDO::PARAM_INT);

    for($i = 0; $i < count($ncs); $i++):
      $select->bindValue($i + 2, $ncs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    $data = array();

    while($nc = $select->fetch(PDO::FETCH_OBJ)):
      $data[$nc->dc_nota_credito] = $nc;
    endwhile;

    if(count($data) != count($ncs)):
      $this->getErrorMan()->showWarning("Hay Notas de Crédito seleccionadas que no fueron encontradas,".
                                        "Probablemente estas fueron centralizadas mientras se realizaba la consulta, ".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    return $data;
  }

  private function initDetallesNotaCredito($ncs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(',?',count($ncs)),1);

    $select = $db->prepare(
                $db->select('tb_nota_credito_proveedor_detalle d
                LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto',
                '*',
                'p.dc_empresa = ? AND d.dc_nota_credito IN ('.$questions.')'));
    $select->bindValue(1, $this->factory->getEmpresa(), PDO::PARAM_INT);
    for($i = 0; $i < count($ncs); $i++):
      $select->bindValue($i+2, $ncs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    foreach($this->nc as &$nc):
      $nc->detalle = array();
    endforeach;

    while($d = $select->fetch(PDO::FETCH_OBJ)):
      //Factory::debug($d);
      $this->nc[$d->dc_nota_credito]->detalle[] = $d;
    endwhile;
  }

  private function initTipoNotaCredito(){
    $db = $this->getConnection();

    foreach($this->nc as &$nc):
      $nc->tipo = $db->getRowById('tb_tipo_nota_credito_proveedor',$nc->dc_tipo,'dc_tipo');

      if($nc->tipo === false):
        $this->getErrorMan()->showWarning("La Nota de Crédito <strong>{$nc->dq_nota_credito} (folio: {$nc->dq_folio})</strong>".
                                          " posee un tipo de nota de crédito incorrecto y no fue encontrado,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;

    endforeach;
  }

  private function getCondicionesProveedor($r){
    return implode(',',$r->dc_proveedor);
  }

  private function initFacturaCompra(){
    $db = $this->getConnection();

    foreach($this->nc as &$nc):
      if($nc->dc_factura):
        $nc->factura = $db->getRowById('tb_factura_compra',$nc->dc_factura,'dc_factura');
      endif;
    endforeach;
  }

  private function initNotaVenta(){
    $db = $this->getConnection();

    foreach($this->nc as &$nc):
      if(isset($nc->factura) and $nc->factura->dc_nota_venta):
        $nc->nota_venta = $db->getRowById('tb_nota_venta',$nc->factura->dc_nota_venta,'dc_nota_venta');
      endif;
    endforeach;
  }

  private function initOrdenServicio(){
    $db = $this->getConnection();

    foreach($this->nc as &$nc):
      if(isset($nc->factura) and $nc->factura->dc_orden_servicio):
        $nc->nota_venta = $db->getRowById('tb_orden_servicio',$nc->factura->dc_orden_servicio,'dc_orden_servicio');
      endif;
    endforeach;

  }

  private function initClientes(){
    $db = $this->getConnection();

    foreach($this->nc as &$nc):
      if(isset($nc->nota_venta)):
        $nc->cliente = $db->getRowById('tb_cliente',$nc->nota_venta->dc_cliente,'dc_cliente');
      elseif(isset($nc->orden_servicio)):
        $nc->cliente = $db->getRowById('tb_cliente',$nc->orden_servicio->dc_cliente,'dc_cliente');
      endif;
    endforeach;

  }

  private function initCuentaContableLineasNegocio(){
    $this->cuentas_ln = array();
    foreach($this->nc as &$nc):
      if($nc->tipo->dm_retorna_stock == 1):
        foreach($nc->detalle as &$d):
          $d->dc_cuenta_contable = $this->getCuentaBodegaLineaNegocio($d->dc_linea_negocio, $d->dc_bodega);
        endforeach;
      endif;
    endforeach;
  }

  private function getCuentaBodegaLineaNegocio($dc_linea_negocio, $dc_bodega){
    $hash = "{$dc_linea_negocio}_{$dc_bodega}";
    if(isset($this->cuentas_ln[$hash])):
      return $this->cuentas_ln[$hash];
    endif;

    $db = $this->getConnection();

    $select = $db->prepare(
                $db->select('tb_linea_negocio_cuenta_contable',
                'dc_cuenta_contable',
                'dc_linea_negocio = ? AND dc_bodega = ?'));
    $select->bindValue(1, $dc_linea_negocio, PDO::PARAM_INT);
    $select->bindValue(2, $dc_bodega, PDO::PARAM_INT);
    $db->stExec($select);

    $dc_cuenta_contable = $select->fetch(PDO::FETCH_OBJ);

    if($dc_cuenta_contable === false):
      $ln = $db->getRowById('tb_linea_negocio',$dc_linea_negocio,'dc_linea_negocio');
      $bodega = $db->getRowById('tb_bodega',$dc_bodega,'dc_bodega');
      $this->getErrorMan()->showWarning("La Línea de negocio <strong>{$ln->dg_linea_negocio}</strong>".
                                        " no posee configurada una cuenta contable para la bodega".
                                        " <strong>{$bodega->dg_bodega}</strong>, verifique los datos".
                                        " de entrada y vuelva a intentarlo.");
      exit;
    endif;

    $this->cuentas_ln[$hash] = $dc_cuenta_contable->dc_cuenta_contable;
    return $this->cuentas_ln[$hash];

  }

  private function initCuentasServicios(){
    foreach($this->nc as &$nc):
      if($nc->tipo->dm_retorna_stock == 0):
        foreach($nc->detalle as &$d):
          if(empty($d->dc_cuenta_contable_centralizacion) or !preg_match("/^\d+$/",$d->dc_cuenta_contable_centralizacion)):
            $this->getErrorMan()->showWarning("La cuenta contable para el detalle".
                                              " <strong>{$d->dg_producto}</strong> en la nota de crédito".
                                              " <strong>{$nc->dq_nota_credito} (folio: {$nc->dq_folio})</strong>".
                                              " No ha sido asignado, realice esta asignación en el módulo de consulta.");
            exit;
          endif;
        endforeach;
      endif;
    endforeach;
  }

  public function getAsientosAnaliticos($template, $iva){
    $data = array(array(),array(),array());

    if(count($this->nc) == 0):
      return $data;
    endif;

    $data[1][$iva->dc_cuenta_contable] = array();

    foreach($this->nc as $nc):

      if(!isset($data[2][$nc->dc_cuenta_proveedor])):
        $data[2][$nc->dc_cuenta_proveedor] = array();
      endif;

      $this->setCabeceraRegularAnalitico($template, $nc);

      if($nc->tipo->dm_retorna_stock == 1):
        $this->descomponeAnaliticoStock($data, $template, $nc, $iva);
      else:
        $this->descomponeAnaliticoServicio($data, $template, $nc, $iva);
      endif;
    endforeach;

    return $data;
  }

  public function insertarDetallesCentralizacion($dc_centralizacion){
    $db = $this->getConnection();

    $insert = $db->prepare($db->insert('tb_centralizacion_compra_detalle_nota_credito',array(
      'dc_centralizacion' => '?',
      'dc_nota_credito' => '?'
    )));
    $insert->bindValue(1, $dc_centralizacion, PDO::PARAM_INT);
    $insert->bindParam(2, $dc_nota_credito, PDO::PARAM_INT);

    foreach($this->nc as $dc_nota_credito => $d):
      $db->stExec($insert);
    endforeach;
  }

  private function descomponeAnaliticoStock(&$data, $template, $nc, $iva){

    //dc_producto, dc_ceco, dc_marca, dc_linea_negocio, dc_tipo_producto, dc_guia_recepcion, dc_bodega
    foreach($nc->detalle as $d):
      $this->setDetalleRegularAnalitico($template, $d, $nc);
      $t = clone($template);
      $t->dc_bodega = $d->dc_bodega;
      $t->dc_cuenta_contable = $d->dc_cuenta_contable;
      $t->dq_haber = Functions::fixNumberToLocal($d->dq_cantidad * $d->dq_precio);
      $t->dq_debe = 0;

      if(!isset($data[0][$d->dc_cuenta_contable])):
        $data[0][$d->dc_cuenta_contable] = array();
      endif;
      $data[0][$d->dc_cuenta_contable][] = $t;

      //iva
      $tiva = clone($t);
      $tiva->dq_haber = Functions::fixNumberToLocal(($d->dq_cantidad * $d->dq_precio) * ($iva->dq_porcentaje_iva / 100.0));
      $tiva->dc_cuenta_contable = $iva->dc_cuenta_contable;
      $data[1][$iva->dc_cuenta_contable][] = $tiva;

      //Proveedor
      $tprov = clone($t);
      $tprov->dq_haber = 0;
      $tprov->dq_debe = $t->dq_haber + $tiva->dq_haber;
      $tprov->dc_cuenta_contable = $nc->dc_cuenta_proveedor;
      $data[2][$nc->dc_cuenta_proveedor][] = $tprov;

    endforeach;

  }

  private function setCabeceraRegularAnalitico($template, $nc){

    if(isset($nc->factura)):
      $template->dc_factura_compra = $nc->factura->dc_factura;
      $template->dc_nota_venta = $nc->factura->dc_nota_venta;
      $template->dc_orden_servicio = $nc->factura->dc_orden_servicio;
      $template->dc_orden_compra = $nc->factura->dc_orden_compra;
    else:
      $template->dc_factura_compra = null;
      $template->dc_nota_venta = null;
      $template->dc_orden_servicio = null;
      $template->dc_orden_compra = null;
    endif;

    $template->dc_nota_credito_proveedor = $nc->dc_nota_credito;
    $template->dc_proveedor = $nc->dc_proveedor;
    $template->dc_tipo_proveedor = $nc->dc_tipo_proveedor;
    $template->dc_mercado_proveedor = $nc->dc_mercado_proveedor;

    if(isset($nc->cliente)):
      $template->dc_cliente = $nc->cliente->dc_cliente;
      $template->dc_tipo_cliente = $nc->cliente->dc_tipo_cliente;
      $template->dc_mercado_cliente = $nc->cliente->dc_mercado;
    else:
      $template->dc_cliente = null;
      $template->dc_tipo_cliente = null;
      $template->dc_mercado_cliente = null;
    endif;

    if(isset($nc->nota_venta)):
      $template->dc_ejecutivo = $nc->nota_venta->dc_ejecutivo;
    else:
      $template->dc_ejecutivo = null;
    endif;
  }

  private function setDetalleRegularAnalitico($template, $d, $nc){
    $template->dc_producto = $d->dc_producto;
    $template->dc_cebe = $d->dc_cebe;
    $template->dc_marca = $d->dc_marca;
    $template->dc_linea_negocio = $d->dc_linea_negocio;
    $template->dc_tipo_producto = $d->dc_tipo_producto;
    $template->dg_glosa = trim("Folio Nota de Crédito: {$nc->dq_folio}, Producto: ({$d->dg_codigo}) {$d->dg_descripcion}");
  }

  private function descomponeAnaliticoServicio(&$data, $template, $nc, $iva){
    foreach($nc->detalle as $d):
      $this->setDetalleRegularAnalitico($template, $d, $nc);

      $t = clone($template);
      $t->dc_cuenta_contable = $d->dc_cuenta_contable_centralizacion;
      $t->dq_haber = Functions::fixNumberToLocal($d->dq_cantidad * $d->dq_precio);
      $t->dq_debe = 0;

      if(!isset($data[0][$d->dc_cuenta_contable_centralizacion])):
        $data[0][$d->dc_cuenta_contable_centralizacion] = array();
      endif;
      $data[0][$d->dc_cuenta_contable_centralizacion][] = $t;

      //iva
      $tiva = clone($t);
      $tiva->dq_haber = Functions::fixNumberToLocal(($d->dq_cantidad * $d->dq_precio) * ($iva->dq_porcentaje_iva / 100.0));
      $tiva->dc_cuenta_contable = $iva->dc_cuenta_contable;
      $data[1][$iva->dc_cuenta_contable][] = $tiva;

      //Proveedor
      $tprov = clone($t);
      $tprov->dq_haber = 0;
      $tprov->dq_debe = $t->dq_haber + $tiva->dq_haber;
      $tprov->dc_cuenta_contable = $nc->dc_cuenta_proveedor;
      $data[2][$nc->dc_cuenta_proveedor][] = $tprov;

    endforeach;
  }

  public function cambiarEstadoDocumentos(){

    if(!count($this->nc)):
      return;
    endif;

    $db = $this->getConnection();

    $update = $db->prepare($db->update('tb_nota_credito_proveedor',array(
      'dm_centralizada' => 1
    ),'dc_nota_credito = ?'));
    $update->bindParam(1, $dc_nota_credito, PDO::PARAM_INT);

    foreach($this->nc as $dc_nota_credito => $nc):
      $db->stExec($update);
    endforeach;
  }

}
