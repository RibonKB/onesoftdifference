<?php
require_once 'inc/AbstractFactoryService.class.php';

class CentralizacionNotaCreditoService extends AbstractFactoryService{

  private $nc;

  public function getTables($r){
    return "tb_nota_credito nc
  		JOIN tb_tipo_nota_credito t_nc ON t_nc.dc_tipo_nota_credito = nc.dc_tipo_nota_credito
  		JOIN tb_cliente pr ON pr.dc_cliente = nc.dc_cliente
  		JOIN tb_tipo_cliente t ON t.dc_tipo_cliente = pr.dc_tipo_cliente
  		LEFT JOIN tb_usuario us ON us.dc_usuario = nc.dc_usuario_creacion";
  }

  public function getCampos($r){
    return '"NC", "", "", nc.df_emision, pr.dg_rut, pr.dg_razon, 0, -SUM(nc.dq_neto), -SUM(nc.dq_iva), -SUM(nc.dq_total),
  	GROUP_CONCAT(nc.dq_nota_credito SEPARATOR "<br />"), nc.dq_folio, nc.dm_nula, t_nc.dg_tipo_nota_credito, us.dg_usuario,GROUP_CONCAT(nc.dc_nota_credito)';
  }

  public function getcondiciones($r){
    $empresa = $this->factory->getEmpresa();
    $cond = "nc.dc_empresa = {$empresa} AND ".
            "YEAR(nc.df_emision) = {$r->dc_anho} AND ".
            "MONTH(nc.df_emision) = {$r->dc_mes} AND ".
            "nc.dm_nula = 0 AND ".
            "nc.dm_centralizada = 0 AND ".
            "nc.dq_neto > 0";

    if(isset($r->dc_cliente)):
      $clientes = $this->getCondicionesCliente($r);
      $cond .= " AND nc.dc_cliente IN ({$clientes})";
    endif;

    return $cond;

  }

  public function initCentralizacion($r){
    if(!isset($r->dc_documento['NC']) or empty($r->dc_documento['NC'])):
      $this->nc = array();
      return;
    endif;

    $this->initDocumentos($r);
    $this->initFacturaVenta();
    //$this->initNotaVenta();
    //$this->initOrdenServicio();
    //$this->initClientes();

    //Para notas de crédito que mueven stock
    $this->initCuentaContableLineasNegocio();

    //Para notas de crédito de servicios y gastos
    //$this->initCuentasServicios();
  }

  private function initFacturaVenta(){
    $db = $this->getConnection();

    foreach($this->nc as &$nc):
      if($nc->dc_factura and Functions::isPK($nc->dc_factura)):
        $nc->factura = $db->getRowById('tb_factura_venta',$nc->dc_factura,'dc_factura');
      endif;
    endforeach;
  }

  private function initDocumentos($r){
    $this->validaIds($r->dc_documento['NC']);
    $this->nc = $this->getNotasCredito($r->dc_documento['NC']);
    $this->initDetallesNotaCredito($r->dc_documento['NC']);
    $this->limpiaDetallesVacios();

    //$this->initTipoNotaCredito();
  }

  private function limpiaDetallesVacios(){
    foreach($this->nc as $dc_nota_credito => &$nc):
      foreach($nc->detalle as $i => $d):
        if($d->dq_precio == 0):
          unset($nc->detalle[$i]);
        endif;
      endforeach;
      if(count($nc->detalle) == 0):
        unset($this->nc[$dc_nota_credito]);
      endif;
    endforeach;
  }

  private function getNotasCredito($ncs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(",?",count($ncs)),1);

    $select = $db->prepare(
                $db->select('tb_nota_credito nc
                             JOIN tb_cliente c ON c.dc_cliente = nc.dc_cliente',
                             'nc.dc_nota_credito, nc.dq_folio, nc.dq_nota_credito, nc.dc_cliente, nc.dq_total, nc.dq_iva, c.dc_cuenta_contable dc_cuenta_cliente,
                              c.dc_tipo_cliente, nc.dc_factura, c.dc_mercado dc_mercado_cliente, nc.dc_tipo_nota_credito, nc.dc_ejecutivo',
                             'nc.dc_empresa = ? AND nc.dm_centralizada = 0 AND nc.dc_nota_credito IN ('.$questions.')'));
    $select->bindValue(1, $this->factory->getEmpresa(), PDO::PARAM_INT);

    for($i = 0; $i < count($ncs); $i++):
      $select->bindValue($i + 2, $ncs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    $data = array();

    while($nc = $select->fetch(PDO::FETCH_OBJ)):
      $data[$nc->dc_nota_credito] = $nc;
    endwhile;

    if(count($data) != count($ncs)):
      $this->getErrorMan()->showWarning("Hay Notas de Crédito seleccionadas que no fueron encontradas,".
                                        "Probablemente estas fueron centralizadas mientras se realizaba la consulta, ".
                                        "verifique los datos de entrada y vuelva a intentarlo.");
      exit;
    endif;

    return $data;
  }

  private function validaIds(&$ncs){
    foreach($ncs as &$nc):
      if(empty($nc) or !preg_match('/^\d+$/',$nc)):
        $this->getErrorMan()->showWarning("Existen Notas de Crédito con un formato incorrecto,".
                                          " verifique los datos de entrada y vuelva a intentarlo.");
        exit;
      endif;
    endforeach;

    foreach($ncs as &$nc):
      $nc = intval($nc);
    endforeach;
  }

  private function getCondicionesCliente($r){
    return implode(',',$r->dc_cliente);
  }

  private function initCuentaContableLineasNegocio(){
    foreach($this->nc as &$nc):
      foreach($nc->detalle as &$d):
        $d->dc_cuenta_contable = $this->factory->getCuentaBodegaLineaNegocio($d->dc_linea_negocio);
      endforeach;
    endforeach;
  }

  private function initDetallesNotaCredito($ncs){
    $db = $this->getConnection();
    $questions = substr(str_repeat(',?',count($ncs)),1);

    $select = $db->prepare(
                $db->select('tb_nota_credito_detalle d
                LEFT JOIN tb_producto p ON p.dg_codigo = d.dg_producto',
                '*',
                'p.dc_empresa = ? AND d.dc_nota_credito IN ('.$questions.')'));
    $select->bindValue(1, $this->factory->getEmpresa(), PDO::PARAM_INT);
    for($i = 0; $i < count($ncs); $i++):
      $select->bindValue($i+2, $ncs[$i], PDO::PARAM_INT);
    endfor;

    $db->stExec($select);

    foreach($this->nc as &$nc):
      $nc->detalle = array();
    endforeach;

    while($d = $select->fetch(PDO::FETCH_OBJ)):
      //Factory::debug($d);
      $this->nc[$d->dc_nota_credito]->detalle[] = $d;
    endwhile;
  }

  public function getAsientosAnaliticos($template, $iva){
    $data = array(array(),array(),array());

    if(count($this->nc) == 0):
      return $data;
    endif;

    $data[1][$iva->dc_cuenta_contable] = array();

    foreach($this->nc as &$nc):

      if(!isset($data[2][$nc->dc_cuenta_cliente])):
        $data[2][$nc->dc_cuenta_cliente] = array();
      endif;

      $this->setCabeceraRegularAnalitico($template, $nc);
      $this->descomponeAnalitico($data, $template, $nc, $iva);

    endforeach;

    return $data;
  }

  private function descomponeAnalitico(&$data, $template, $nc, $iva){

    //dc_producto, dc_ceco, dc_marca, dc_linea_negocio, dc_tipo_producto, dc_guia_recepcion, dc_bodega
    foreach($nc->detalle as $d):
      $this->setDetalleRegularAnalitico($template, $d, $nc);
      $t = clone($template);
      $t->dc_cuenta_contable = $d->dc_cuenta_contable;
      $t->dq_haber = Functions::fixNumberToLocal($d->dc_cantidad * $d->dq_precio);
      $t->dq_debe = 0;

      if(!isset($data[0][$d->dc_cuenta_contable])):
        $data[0][$d->dc_cuenta_contable] = array();
      endif;
      $data[0][$d->dc_cuenta_contable][] = $t;

      //iva
      $tiva = clone($t);
      $tiva->dq_haber = Functions::fixNumberToLocal($t->dq_haber * ($iva->dq_porcentaje_iva / 100.0));
      $tiva->dc_cuenta_contable = $iva->dc_cuenta_contable;
      $data[1][$iva->dc_cuenta_contable][] = $tiva;

      //Cliente
      $tcliente = clone($t);
      $tcliente->dq_haber = 0;
      $tcliente->dq_debe = $t->dq_haber + $tiva->dq_haber;
      $tcliente->dc_cuenta_contable = $nc->dc_cuenta_cliente;
      $data[2][$nc->dc_cuenta_cliente][] = $tcliente;

    endforeach;

  }

  private function setCabeceraRegularAnalitico($template, $nc){
    if(isset($nc->factura)):
      $template->dc_factura_venta = $nc->factura->dc_factura;
      $template->dc_nota_venta = $nc->factura->dc_nota_venta;
      $template->dc_orden_servicio = $nc->factura->dc_orden_servicio;
    else:
      $template->dc_factura_compra = null;
      $template->dc_nota_venta = null;
      $template->dc_orden_servicio = null;
    endif;

    $template->dc_nota_credito = $nc->dc_nota_credito;
    $template->dc_cliente = $nc->dc_cliente;
    $template->dc_tipo_cliente = $nc->dc_tipo_cliente;
    $template->dc_mercado_cliente = $nc->dc_mercado_cliente;
    $template->dc_ejecutivo = $nc->dc_ejecutivo;

  }

  private function setDetalleRegularAnalitico($template, $d, $nc){
    $template->dc_producto = $d->dc_producto;
    $template->dc_cebe = $d->dc_cebe;
    if(empty($d->dc_cebe) and Functions::isPK($d->dc_cebe)):
      $cebe = $this->getConnection()->getRowById('tb_cebe',$d->dc_cebe,'dc_cebe');
      $template->dc_ceco = $cebe->dc_ceco;
    endif;
    $template->dc_marca = $d->dc_marca;
    $template->dc_linea_negocio = $d->dc_linea_negocio;
    $template->dc_tipo_producto = $d->dc_tipo_producto;
    $template->dg_glosa = trim("Folio Nota de Crédito: {$nc->dq_folio}, Producto: ({$d->dg_codigo}) {$d->dg_descripcion}");
  }

  public function insertarDetallesCentralizacion($dc_centralizacion){
    $db = $this->getConnection();

    $insert = $db->prepare($db->insert('tb_centralizacion_venta_detalle_nota_credito',array(
      'dc_centralizacion' => '?',
      'dc_nota_credito' => '?'
    )));
    $insert->bindValue(1, $dc_centralizacion, PDO::PARAM_INT);
    $insert->bindParam(2, $dc_nota_credito, PDO::PARAM_INT);

    foreach($this->nc as $dc_nota_credito => $d):
      $db->stExec($insert);
    endforeach;
  }

  public function cambiarEstadoDocumentos(){

    if(!count($this->nc)):
      return;
    endif;

    $db = $this->getConnection();

    $update = $db->prepare($db->update('tb_nota_credito',array(
      'dm_centralizada' => 1
    ),'dc_nota_credito = ?'));
    $update->bindParam(1, $dc_nota_credito, PDO::PARAM_INT);

    foreach($this->nc as $dc_nota_credito => $nc):
      $db->stExec($update);
    endforeach;
  }

}
