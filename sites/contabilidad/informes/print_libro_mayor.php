<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_GET['i'])){
	$options['Detalle'] = "Acceso al recurso no permitido";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = new SimpleXMLElement("cache/informe_{$_GET['i']}.xml",0,1);

if($_GET['m'] == '1'){
	$periodo = $data->detalle[0]->dc_anho_contable;
	
	if(count($db->select('tb_log_impresion_libro_mayor','1',"dg_anho = '{$periodo}'"))){
		die('No se puede volver a imprimir con folio este informe');
	}
}

require_once("../../../inc/fpdf.php");
$sort = array();
foreach($data->detalle as $d){
	$sort["{$d->dg_codigo}"][] = $d;
}

class PDF extends FPDF{
	function Header(){
		global $cabecera;
		if($_GET['m'] = '1'){
			$this->SetTextColor(255);
			$this->SetFont('Arial','',7);
			$this->Cell(0,4,'',0,1,'R');
			$this->MultiCell(0,3,$cabecera);
			$this->Ln();
		}
	}
}

$cabecera = utf8_decode($empresa_conf['dg_encabezado_libro_mayor']);

$pdf = new PDF('P','mm','Letter');
$pdf->SetFont('Arial','B',9);
$pdf->AddPage();
$anchos = array(31,30,65,35,35);
$titles = array('N� Comprobante','Fecha contable','Glosa','DEBE','HABER');
foreach($sort as $cuenta){
    $pdf->SetTextColor(0);
	$pdf->SetDrawColor(0);
	$pdf->Cell(0,10,"({$cuenta[0]->dg_codigo}) {$cuenta[0]->dg_cuenta_contable}",1,1,'C');
	/*$pdf->SetFillColor(170);
    $pdf->SetTextColor(0);
	$pdf->SetDrawColor(255);
	*/
	foreach($titles as $i => $v){
		$pdf->Cell($anchos[$i],6,$v,1,0,'C');
	}
	$pdf->Ln();
	/*
	$pdf->SetFillColor(251);
	$pdf->SetDrawColor(238);
	*/
	$total_debe = 0.0;
	$total_haber = 0.0;
	foreach($cuenta as $detalle){
		$total_debe += $detalle->dq_debe;
		$total_haber += $detalle->dq_haber;
		$detalle->dq_debe = number_format($detalle->dq_debe,0,',','.');
		$detalle->dq_haber = number_format($detalle->dq_haber,0,',','.');
		$pdf->Cell($anchos[0],6,$detalle->dg_comprobante,1,0,'L');
		$pdf->Cell($anchos[1],6,$detalle->df_fecha_contable,1,0,'L');
		$pdf->Cell($anchos[2],6,$detalle->dg_glosa_detalle,1,0,'L');
		$pdf->Cell($anchos[3],6,$detalle->dq_debe,1,0,'R');
		$pdf->Cell($anchos[4],6,$detalle->dq_haber,1,0,'R');
		$pdf->Ln();
	}
	$saldo_dh = $total_debe-$total_haber;
	$saldo = 'Saldado';
	if($saldo_dh < 0.0){
		$saldo = 'Saldo acreedor';
	}else if($saldo_dh > 0.0){
		$saldo = 'Saldo deudor';
	}
	$saldo_dh = number_format($saldo_dh,0,',','.');
	$total_debe = number_format($total_debe,0,',','.');
	$total_haber = number_format($total_haber,0,',','.');
	/*$pdf->SetFillColor(221);*/
	
	$pdf->Cell($anchos[0]+$anchos[1]+$anchos[2],6,'Totales',1,0,'R');
	$pdf->Cell($anchos[3],6,$total_debe,1,0,'R');
	$pdf->Cell($anchos[4],6,$total_haber,1,0,'R');
	$pdf->Ln();
	$pdf->Cell($anchos[0]+$anchos[1]+$anchos[2]+$anchos[3],6,$saldo,1,0,'R');
	$pdf->Cell($anchos[4],6,$saldo_dh,1,0,'R');
	
	$pdf->Ln(20);
}

$pdf->Output();

?>