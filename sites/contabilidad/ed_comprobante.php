<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select(
"tb_comprobante_contable",
"dg_comprobante,dc_mes_contable,dc_anho_contable,dm_tipo_comprobante",
"dc_comprobante= {$_POST['id']} AND dm_anulado = '0' AND dm_editable = '1' AND dc_empresa={$empresa}");
if(!count($datos)){
	$error_man->showWarning("Error, imposible editar el comprobante, las posibles causas son:
	<ul>
		<li>El comprobante no existe o no tiene permisos para acceder a él</li>
		<li>No posee permisos para editar este comprobante por no autorización previa</li>
		<li>El comprobante fue anulado anteriormente</li>
	</ul>");
	exit();
}
$datos = $datos[0];

if($datos['dm_tipo_comprobante'] != 0){
	$error_man->showWarning("No puede editar comprobantes contables generados por los otros módulos del sistema, consulte a un administrador.");
	exit;
}

$periodo = $db->select("(SELECT * FROM tb_mes_contable WHERE dc_mes = {$datos['dc_mes_contable']}) m
JOIN (SELECT * FROM tb_periodo_contable WHERE dc_anho={$datos['dc_anho_contable']} AND dm_estado='1') p ON m.dc_mes_contable = p.dc_mes_contable",'1');

if(!count($periodo)){
	$error_man->showWarning('El comprobante pertenece a un periodo contable cerrado, no se puede editar.');
	exit();
}
?>
<script type="text/javascript">
	loadpage('sites/contabilidad/proc/ed_comprobante_contable.php?id=<?=$_POST['id'] ?>');
</script>