<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$datos = $db->select(
"tb_comprobante_contable",
"dg_comprobante,dc_mes_contable,dc_anho_contable",
"dc_comprobante= {$_POST['id']} AND dm_anulado = '0' AND dc_empresa={$empresa}");
if(!count($datos)){
	$error_man->showWarning("Error, el comprobante no fue encontrado, asegurese que posee los permisos para autorizar y que el comprobante no este previamente anulado");
	exit();
}
$datos = $datos[0];

$periodo = $db->select("(SELECT * FROM tb_mes_contable WHERE dc_mes = {$datos['dc_mes_contable']}) m
JOIN (SELECT * FROM tb_periodo_contable WHERE dc_anho={$datos['dc_anho_contable']} AND dm_estado='1') p ON m.dc_mes_contable = p.dc_mes_contable",'1');

if(!count($periodo)){
	$error_man->showWarning('El comprobante pertenece a un periodo contable cerrado, no se puede editar.');
	exit();
}
?>
<div class="secc_bar">Autorizar comprobante contable para su edición</div>
<div class="panes">
	<form action="sites/contabilidad/proc/auth_comprobante.php" class="confirmValidar" id="auth_comprobante_form">
		<fieldset>
		<div class="center alert">
			¿Está seguro de querer autorizar la modificación del comprobante <strong><?=$datos['dg_comprobante'] ?></strong>?<br />
			El comprobante será nuevamente bloqueado para edición luego de que este sea editado
		</div>
			<hr />
			<div class="center">
				<input type="hidden" name="null_comp_id" value="<?=$_POST['id'] ?>" />
				<input type="submit" class="checkbtn" value="Autorizar edición" />
			</div>
		</fieldset>
	</form>
	<div id="auth_comprobante_form_res"></div>
</div>