<?php

require_once('sites/contabilidad/stuff.class.php');

/**
 * Description of CRUDComprobanteContable
 *
 * @author Tomás Lara Valdovinos
 * @date 09-10-2013
 */
abstract class CRUDComprobanteContableFactory extends Factory {

  protected $mesContable;
  protected $anhoContable;

  protected function validarPeriodoContable() {
    $estado = ContabilidadStuff::getEstadoPeriodo(
                    $this->mesContable, $this->anhoContable, ContabilidadStuff::FIELD_ESTADO_CT, $this->getConnection(), $this->getEmpresa(), $this->getUserData()->dc_usuario
    );

    switch ($estado) {
      case ContabilidadStuff::ERROR_PERIODO_INEXISTENTE:
        $this->getErrorMan()->showWarning('No existe el periodo en el que intenta crear el comprobante');
        exit;
      case ContabilidadStuff::ERROR_PERIODO_CERRADO:
        $this->getErrorMan()->showWarning('El periodo contable seleccionado está cerrado');
        exit;
      case ContabilidadStuff::ERROR_PERIODO_USUARIO_DENEGADO:
        $this->getErrorMan()->showWarning('No tiene permisos para generar comprobantes en este periodo, consulte con el encargado');
        exit;
      case ContabilidadStuff::ERROR_PERIODO_GRUPO_DENEGADO:
        $this->getErrorMan()->showWarning('Su grupo de usuario no tiene permiso de generar comprobantes en este periodo, consulte con el encargado');
        exit;
    }

    return true;
  }
  
  protected function obtenerCuentasTipoMovimiento($dc_tipo_movimiento) {

    $db = $this->getConnection();

    $cuentas = $db->prepare($db->select('tb_cuenta_contable c
									JOIN tb_cuentas_tipo_movimiento t ON t.dc_cuenta_contable = c.dc_cuenta_contable', 'c.dg_codigo,c.dg_cuenta_contable,c.dg_descripcion_larga,c.dc_cuenta_contable', 't.dc_tipo_movimiento = ? AND c.dm_activo = 1 AND c.dc_empresa = ?'));
    $cuentas->bindValue(1, $dc_tipo_movimiento, PDO::PARAM_INT);
    $cuentas->bindValue(2, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($cuentas);

    return $cuentas->fetchAll(PDO::FETCH_OBJ);
  }

}

?>
