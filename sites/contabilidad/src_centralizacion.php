<?php
define("MAIN",1);
require_once("../../inc/init.php");

include_once("../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">
	Centralización de libros
</div>
<div id="main_cont">
	<div class="panes">
    	<?php $form->Start('sites/contabilidad/proc/src_centralizacion.php','src_centralización') ?>
        <?php $form->Header('<b>Indique los filtros apra obtener las facturas incluidas en la centralización</b><br />
							 Los campos marcados con [*] son obligatorios') ?>
        <table width="100%" class="tab">
        	<tbody>
            	<tr>
                	<td>Tipo de centralización</td>
                    <td><?php $form->Radiobox('','tipo_centralizacion',array('Libro de Compra','Libro de Venta')) ?><br /></td>
                </tr>
            	<tr>
                	<td>Periodo</td>
                    <td>
                      <div class="left">
                        <?php $form->Text('Mes', 'dc_mes_corte', true, 2, date('n')) ?>
                      </div>
                      <div class="left">
                        <?php $form->Text('Año', 'dc_anho_corte', true, 4, date('Y')) ?>
                      </div>
                    </td>
                </tr>
                <tr>
                	<td>Cliente</td>
                    <td><?php $form->DBMultiSelect('(Solo libro de venta)','dc_cliente','tb_cliente',array('dc_cliente','dg_razon')) ?></td>
                </tr>
                <tr>
                	<td>Proveedor</td>
                    <td><?php $form->DBMultiSelect('(Solo libro de compra)','dc_proveedor','tb_proveedor',array('dc_proveedor','dg_razon')) ?></td>
                </tr>
                <tr>
                	<td>Ejecutivo</td>
                    <td><?php $form->DBMultiSelect('','dc_ejecutivo','tb_funcionario tf JOIN tb_cargo_funcionario tc ON tf.dc_ceco = tc.dc_ceco',array('tf.dc_funcionario','tf.dg_nombres','tf.dg_ap_paterno','tf.dg_ap_materno'),array(),"tf.dc_empresa = {$empresa} AND dc_modulo = 1"); ?></td>
                </tr>
            </tbody>
        </table>
        <?php $form->End('Ejecutar','searchbtn') ?>
    </div>
</div>
<script type="text/javascript">
$('#dc_cliente, #dc_proveedor, #dc_ejecutivo').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>