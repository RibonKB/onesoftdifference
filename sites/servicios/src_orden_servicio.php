<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Ordenes de servicio</div>
<div id='main_cont'><br /><br /><div class='panes'>");

include_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/servicios/proc/src_orden_servicio.php','src_orden_venta');
$form->Header("<strong>Indicar los parámetros de búsqueda de orden de servicio</strong>");

	echo('<table class="tab" style="text-align:left;" width="100%" id="form_container"><tr><td>Número de orden</td><td>');
	$form->Text("Desde","os_numero_desde");
	echo('</td><td>');
	$form->Text('Hasta','os_numero_hasta');
	echo('</td></tr><tr><td>Fecha emisión</td><td>');
	$form->Date('Desde','os_emision_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','os_emision_hasta',1,0);
	echo('</td></tr><tr><td>Fecha compromiso</td><td>');
	$form->Date('Desde','os_compromiso_desde');
	echo('</td><td>');
	$form->Date('Hasta','os_compromiso_hasta');
	echo('</td></tr><tr><td>Cliente</td><td>');
	$form->ListadoMultiple('','os_client','tb_cliente',array('dc_cliente','dg_razon'));
	echo('</td><td>&nbsp;</td></tr><tr><td>Tipo de falla</td><td>');
	$form->ListadoMultiple('','os_falla','tb_tipo_falla_os',array('dc_tipo_falla','dg_tipo_falla'));
	echo('</td><td>&nbsp;</td></tr><tr><td>Técnico</td><td>');
	$form->ListadoMultiple('','os_tecnico','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
	echo('</td><td>&nbsp;</td></tr><tr><td>Estado</td><td>');
	$form->ListadoMultiple('','os_estado','tb_estado_orden_servicio',array('dc_estado','dg_estado'));
	echo('</td><td>&nbsp;</td></tr><tr><td colspan="3">');
	$form->Combobox('Incluir','os_status',array('Ordenes abiertas','Ordenes Cerradas'),array(0,1));
	$form->Combobox('','os_factured',array('Pendiente de facturación','Facturada'),array(0,1));
	echo('</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');


?>
</div></div>
<script type="text/javascript">
$('#os_client,#os_falla,#os_tecnico,#os_estado').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});

$(':checkbox[name="os_status[]"]').click(function(e){
	if($(':checked[name="os_status[]"]').size() < 1){
		e.preventDefault();
	}
});
</script>