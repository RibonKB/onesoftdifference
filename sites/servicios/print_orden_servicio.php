<?php
define("MAIN",1);
include_once("../../inc/global.php");

$db->query("SET NAMES 'latin1'");

include("template_orden_servicio/modo{$empresa}.php");

$datosOrden = $db->select("(SELECT * FROM tb_orden_servicio WHERE dc_orden_servicio = {$_GET['id']} AND dc_empresa = {$empresa}) o
LEFT JOIN tb_cliente_sucursal s ON s.dc_sucursal = o.dc_sucursal
LEFT JOIN tb_comuna com ON com.dc_comuna = s.dc_comuna
LEFT JOIN tb_cliente cl ON cl.dc_cliente = o.dc_cliente
LEFT JOIN tb_tipo_falla_os tf ON tf.dc_tipo_falla = o.dc_tipo_falla
LEFT JOIN tb_funcionario tc ON tc.dc_funcionario = o.dc_tecnico
LEFT JOIN tb_estado_orden_servicio es ON es.dc_estado = o.dc_estado",
'o.dq_orden_servicio,cl.dg_razon,o.dg_solicitante,o.dg_requerimiento,tf.dg_tipo_falla,o.dg_tipo_falla_manual,
CONCAT_WS(" ",tc.dg_nombres,tc.dg_ap_paterno,tc.dg_ap_materno) AS dg_tecnico,es.dg_estado,o.dm_estado,s.dg_sucursal, s.dg_direccion, com.dg_comuna,
DATE_FORMAT(o.df_compromiso,"%d/%m/%Y %I:%i %p") AS df_compromiso, o.dg_solucion,
DATE_FORMAT(o.df_fecha_emision,"%d/%m/%Y %I:%i %p") AS df_emision, tf.dg_checklist_herramientas, tf.dg_checklist_estado');

if(!count($datosOrden)){
	$error_man->show_fatal_error('Oportunidad no encontrada. detalles:',
	array(
		'Acceso Denegado'=> 'No tiene permiso de acceso al recurso',
		'No encontrado' => 'La oportunidad no existe'
	));
}

$datosOrden = $datosOrden[0];

$pdf = new PDF();
$pdf->SetBody();
$pdf->setFoot();
$pdf->Output();

?>