<?php

/**
 * Description of BotoneraGestionOrdenServicioFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 30-07-2013
 */
class BotoneraGestionOrdenServicioFactory extends Factory{
  
    private $orden;
    
    public function cierreTelefonicoAction(){
        $this->title = "Cierre Telefónico de Orden de Servicio";
        
        $form = $this->getFormView($this->getTemplateURL('cierreTelefonico.form'), array(
            'orden_servicio' => $this->getOrdenServicio()
        ));
        
        echo $this->getOverlayView($form, array(), Factory::STRING_TEMPLATE);
        
    }
    
    public function procesarCierreTelefonicoAction(){
        if($this->esGestionTelefonicaReapertura()){
          $this->procesarGestionTelefonicaReapertura();
        }else{
          $this->procesarGestionTelefonica();
          $this->notificarCierreTelefonico();
        }
        
        $this->getErrorMan()->showConfirm('Se ha realizado la gestión telefónica correctamente');
    }
    
    private function notificarCierreTelefonico(){
        $r = self::getRequest();
        $db = $this->getConnection();
        $orden_servicio = $this->getOrdenServicio();
        $cliente = $db->getRowById('tb_cliente', $orden_servicio->dc_cliente, 'dc_cliente');
        if($r->dc_evaluacion_servicio > 7){
          $nivel = "Positivo";
          $mensaje = "Gracias por preferir nuestros servicios.";
        }else if($r->dc_evaluacion_servicio < 5){
          $nivel = "Negativo";
          $mensaje = "Gracias por sus comentarios, pronto se contactara con usted nuestra area de servicio al cliente para evaluar las respectivas acciones.";
        }else{
          $nivel = "Regular";
          $mensaje = "Gracias por sus comentarios, realizaremos las gestiones necesarias para mejorar en lo recalcado por usted.";
        }
        
        $tecnicos = $db->prepare(
                      $db->select(
                              'tb_tecnico_apoyo t JOIN tb_funcionario f ON f.dc_funcionario = t.dc_tecnico',
                              'dg_nombres, dg_ap_paterno, dg_ap_materno',
                              'dc_orden_servicio = ?')
                );
        $tecnicos->bindValue(1, $orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
        $db->stExec($tecnicos);
        $tecnicos = $tecnicos->fetchAll(PDO::FETCH_OBJ);
        
        $tech = array();
        foreach($tecnicos as $t):
          $tech[] = $t->dg_nombres.' '.$t->dg_ap_paterno.' '.$t->dg_ap_materno;
        endforeach;
        
        $subject = "Notificación de Cierre Telefónico OS {$orden_servicio->dq_orden_servicio} ({$nivel})";
        
        $body = "Orden de Servicio: {$orden_servicio->dq_orden_servicio}".
                "<br />Fecha Creación: ".$db->dateLocalFormat($orden_servicio->df_fecha_emision).
                "<br />Fecha Cierre: ".$r->df_fin_telefonico.
                "<br />Cliente: ".$cliente->dg_razon.
                "<br />Contacto: {$orden_servicio->dg_solicitante}".
                "<br /><br />Estimado cliente su orden de servicio número {$orden_servicio->dq_orden_servicio} se ha procedido a cerrar de manera telefónica con los siguientes comentarios:".
                "<br /><br />{$r->dg_gestion}".
                "<br /><br />Nota Otorgada al servicio: {$r->dc_evaluacion_servicio}".
                "<br />Técnicos Involucrados: ".implode(' , ',$tech).
                "<br />Resultado: {$nivel}".
                "<br /><br />{$mensaje}";
                
         $smtp = $this->getMailService();
         $smtp->setMode(SMTPService::MODE_SERVICIO);
         $smtp->connect();
         $smtp->setAddress('varredondo@adischile.cl', 'Victor Arredondo');
         $smtp->sendMail($subject, $body);
    }
    
    private function esGestionTelefonicaReapertura(){
      return self::getRequest()->dm_estado_solucion == 1;
    }
    
    private function procesarGestionTelefonicaReapertura(){
      $this->validarDatosReaperturaTelefonica();
      $db = $this->getConnection();
      $r = self::getRequest();
      
      $orden_servicio = $this->getOrdenServicio();
      
      $db->start_transaction();
      
      $cerrar = $db->prepare($db->update('tb_orden_servicio', array(
          'dm_estado' => 0,
          'dc_estado' => '?'
      ), 'dc_orden_servicio = ?'));
      $cerrar->bindValue(1, $r->dc_estado_orden_reabrir, PDO::PARAM_INT);
      $cerrar->bindValue(2, $orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
      $db->stExec($cerrar);
      
      $gestion = $db->prepare($db->insert('tb_orden_servicio_avance', array(
          'dc_orden_servicio' => '?',
          'dg_gestion' => '?',
          'df_creacion' => $db->getNow(),
          'dc_estado_orden' => '?',
          'dm_estado_orden' => '?'
      )));
      $gestion->bindValue(1, $orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
      $gestion->bindValue(2, $r->dg_gestion, PDO::PARAM_STR);
      $gestion->bindValue(3, $r->dc_estado_orden_reabrir, PDO::PARAM_INT);
      $gestion->bindValue(4, 0, PDO::PARAM_STR);
      $db->stExec($gestion);
      
      $db->commit();
      
    }
    
        private function validarDatosReaperturaTelefonica(){
          $r = self::getRequest();
          
          if($r->dc_estado_orden_reabrir == 0){
            $this->getErrorMan()->showWarning('Debe indicar un nuevo estado para la orden de servicio para la reapertura');
            exit;
          }
        }
    
    private function procesarGestionTelefonica(){
      $this->validarDatosGestionTelefonica();
      $this->initFunctionsService();
      
      $db = $this->getConnection();
      $r = self::getRequest();
      
      $orden_servicio = $this->getOrdenServicio();
      
      $db->start_transaction();
        
        $update = $db->prepare($db->update('tb_orden_servicio', array(
            'dm_gestion_telefonica' => 1,
            'df_inicio_telefonico' => '?',
            'df_fin_telefonico' => '?',
            'dc_evaluacion_servicio' => '?'
        ), 'dc_orden_servicio = ?'));
        $update->bindValue(1, $db->sqlDate2(Functions::getRequestDateTime('df_inicio_telefonico'), PDO::PARAM_STR));
        $update->bindValue(2, $db->sqlDate2(Functions::getRequestDateTime('df_fin_telefonico'), PDO::PARAM_STR));
        $update->bindValue(3, $r->dc_evaluacion_servicio, PDO::PARAM_INT);
        $update->bindValue(4, $orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
        $db->stExec($update);
        
        $gestion = $db->prepare($db->insert('tb_orden_servicio_avance', array(
          'dc_orden_servicio' => '?',
          'dg_gestion' => '?',
          'df_creacion' => $db->getNow(),
          'dc_estado_orden' => '?',
          'dm_estado_orden' => '?'
      )));
      $gestion->bindValue(1, $orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
      $gestion->bindValue(2, $r->dg_gestion, PDO::PARAM_STR);
      $gestion->bindValue(3, $orden_servicio->dc_estado, PDO::PARAM_INT);
      $gestion->bindValue(4, $orden_servicio->dm_estado, PDO::PARAM_STR);
      $db->stExec($gestion);
        
      $db->commit();
      
    }
    
        private function validarDatosGestionTelefonica(){
          $r = self::getRequest();
          
          if(!$r->df_inicio_telefonico || !$r->df_fin_telefonico){
            $this->getErrorMan()->showWarning('Debe indicar las fechas de apertura y cierre del servicio');
            exit;
          }
          
          if($r->dc_evaluacion_servicio == 0){
            $this->getErrorMan()->showWarning('Debe indicar una nota de evaluación del servicio');
            exit;
          }
        }
    
    private function getOrdenServicio(){
      if(!($this->orden instanceof stdClass)){
        $this->orden = $this->getConnection()->getRowById('tb_orden_servicio', self::getRequest()->dc_orden_servicio, 'dc_orden_servicio');
      }
      
      return $this->orden;
    }
    
    /**
     * @return SMTPService
     */
    private function getMailService(){
      return $this->getService('SMTP');
    }
    
}

?>
