<?php

/**
 * Description of CRUDOrdenServicioFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 19-07-2013
 */
class CRUDOrdenServicioFactory extends Factory {

  protected function getDatetime($field) {
    $r = self::getRequest();

    $hora = $field . '_hora';
    $minu = $field . '_minuto';
    $meri = $field . '_meridiano';

    $hora = str_pad($r->$hora + $r->$meri, 2, '0', STR_PAD_LEFT) . ":" . $r->$minu;
    return $this->getConnection()->sqlDate2($r->{$field} . ' ' . $hora);
  }

}

?>
