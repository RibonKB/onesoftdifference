<?php
require("../../inc/fpdf.php");

class PDF extends FPDF{

private $datosEmpresa;

public function  PDF(){
	global $empresa,$db;

	$datosE = $db->select(
	"(SELECT * FROM tb_empresa WHERE dc_empresa={$empresa}) e
	JOIN (SELECT * FROM tb_empresa_configuracion WHERE dc_empresa={$empresa}) ec ON e.dc_empresa = ec.dc_empresa
	LEFT JOIN tb_comuna c ON e.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region r ON c.dc_region = r.dc_region",
	"e.*, ec.dg_moneda_local, ec.dg_pie_cotizacion, c.dg_comuna, r.dg_region");
	$this->datosEmpresa = $datosE[0];
	
	unset($datosE);
	
	parent::__construct('P','mm','Letter');
	
}

function Header(){
	global $datosOrden;
	
	$this->SetFont('Arial','',12);
	$this->SetDrawColor(0,100,0);
	$this->SetTextColor(0,100,0);
	$this->SetX(160);
	$this->MultiCell(50,7,"ORDEN\nDE SERVICIO\nN� {$datosOrden['dq_orden_servicio']}\n",1,'C');
	$this->SetY(10);
	$this->SetTextColor(0);
	
	$this->SetFont('','B',12);
	$this->Cell(145,5,$this->datosEmpresa['dg_razon']);
	$this->ln();
	$this->SetFont('Arial','',7);
	$this->SetTextColor(110);
	$this->MultiCell(120,2.4,"{$this->datosEmpresa['dg_giro']}\n{$this->datosEmpresa['dg_direccion']}, {$this->datosEmpresa['dg_comuna']} {$this->datosEmpresa['dg_region']}\n{$this->datosEmpresa['dg_fono']}");
	$this->SetTextColor(0);
	$this->Ln(4);
	
}

function setBody(){
	global $datosOrden;
	$this->AddPage();
	$this->SetFont('Arial','',7);
	$this->SetDrawColor(200);
	$this->SetTextColor(50);
	$this->Cell(0,5,'','B');
	$this->Ln();
	$y = $this->GetY();
	$this->MultiCell(26,6,"CLIENTE\nFECHA EMISI�N\nSOLICITANTE\nSUCURSAL\nTIPO FALLA\nT�CNICO\nFECHA COMPROMISO\nESTADO",'L');
	$this->Cell(0,5,'','T');
	$this->SetY($y);
	$this->SetX(40);
	$this->SetFont('','B',10);
	$estado = '';
	if($datosOrden['dm_estado'] == '0')
		$estado = '(Solucionado)';
	$this->MultiCell(166,6,
	"{$datosOrden['dg_razon']}\n{$datosOrden['df_emision']}\n{$datosOrden['dg_solicitante']}\n{$datosOrden['dg_sucursal']}\n{$datosOrden['dg_tipo_falla']}{$datosOrden['dg_tipo_falla_manual']}\n{$datosOrden['dg_tecnico']}\n{$datosOrden['df_compromiso']}\n{$datosOrden['dg_estado']} {$estado}",
	'R');
	$this->SetFillColor(180);
	$this->Cell(0,6,'Requerimiento:',1,1,'L',1);
	$this->MultiCell(0,6,$datosOrden['dg_requerimiento'],1);
	if($datosOrden['dg_solucion']){
		$this->Cell(0,6,'Soluci�n:',1,1,'L',1);
		$this->MultiCell(0,6,$datosOrden['dg_solucion'],1);
	}else{
		$this->Cell(0,6,'Soluci�n y Observaciones:',1,1,'L',1);
		$this->MultiCell(0,6,"\n\n\n\n\n\n\n\n\n\n",1);
	}
	
	$this->Ln(20);
	$this->SetX(110);
	$this->Cell(90,6,' ','B',2);
	$this->Cell(90,6,'Nombre RUT receptor',0,1,'C');
	
	$this->Ln(15);
	$this->Cell(45,6,'Fecha inicio',1,0,'C',1);
	$this->Cell(45,6,'Hora',1,0,'C',1);
	$this->SetX($this->GetX()+10);
	$this->Cell(45,6,'Fecha T�rmino',1,0,'C',1);
	$this->Cell(45,6,'Hora',1,1,'C',1);
	
	$this->Cell(45,15,' ',1,0,'C');
	$this->Cell(45,15,' ',1,0);
	$this->SetX($this->GetX()+10);
	$this->Cell(45,15,' ',1,0,'C');
	$this->Cell(45,15,' ',1,1);
	
}

function setFoot(){
}

}

?>