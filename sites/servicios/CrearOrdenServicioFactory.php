<?php

require_once(__DIR__."/CRUDOrdenServicioFactory.php");

/**
 * Description of CrearOrdenServicioFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 11-07-2013
 */
class CrearOrdenServicioFactory extends CRUDOrdenServicioFactory{
  
    private $fecha_inicio;
    private $fecha_termino;
    private $area;
    private $tipo_falla;
    
    public function verDisponibilidadAction(){
        $r = self::getRequest();
        $this->validarRequisitosDisponibilidad();
        
        $this->title = "Disponibilidad técnica";
        
        $this->fecha_inicio  = $this->getDatetime('os_fecha');
        $this->fecha_termino = $this->getDatetime('os_fecha_final');
        
        echo $this->getOverlayView($this->getTemplateURL('disponibilidad_tecnica'), array(
            'tecnicos' => $this->getDisponibilidad(),
            'area' => $this->getAreaServicio(),
            'tipo_falla' => $this->getTipoFalla(),
            'fecha_inicio' => new DateTime($this->fecha_inicio),
            'fecha_termino' => new DateTime($this->fecha_termino)
        ));
        
    }
    
    private function validarRequisitosDisponibilidad(){
        $r = self::getRequest();
        
        if($this->getAreaServicio() === false){
          $this->getErrorMan()->showWarning('Debe seleccionar un área de servicio en la orden de servicio para comprobar las disponibilidades');
          exit;
        }
        
        if(!$r->os_fecha){
          $this->getErrorMan()->showWarning('Debe indicar una fecha de inicio de Orden de Servicio para utilizar como filtro a la disponibilidad');
          exit;
        }
        
        if(!$r->os_fecha_final){
          $this->getErrorMan()->showWarning('Debe indicar una fecha de término de Orden de Servicio para utilizar como filtro a la disponibilidad');
          exit;
        }
        
        if($this->getTipoFalla() === false){
          $this->getErrorMan()->showWarning('Debe seleccionar un tipo de falla en la orden de servicio para comprobar las disponibilidades');
          exit;
        }
        
    }
    
    private function getAreaServicio(){
      if($this->area instanceof stdClass){
        return $this->area;
      }
      
      $this->area = $this->getConnection()->getRowById('tb_area_servicio', self::getRequest()->os_area, 'dc_area_servicio');
      
      return $this->area;
    }
    
    private function getTipoFalla(){
      if($this->tipo_falla instanceof stdClass){
        return $this->tipo_falla;
      }
      
      $this->tipo_falla = $this->getConnection()->getRowById('tb_tipo_falla_os', self::getRequest()->os_tipo_falla, 'dc_tipo_falla');
      
      return $this->tipo_falla;
    }
    
    private function getDisponibilidad(){
        $db = $this->getConnection();
        $area = $this->getAreaServicio();
        $tipo_falla = $this->getTipoFalla();
        
        $data = $db->prepare(
                  $db->select('tb_perfil_tecnico pt
                               JOIN tb_funcionario f ON f.dc_funcionario = pt.dc_tecnico
                               LEFT JOIN
                                  (SELECT dc_tecnico
                                   FROM tb_tecnico_apoyo
                                   WHERE
                                      (df_fin    > ? AND df_fin    < ?) OR
                                      (df_inicio > ? AND df_fin    < ?) OR
                                      (df_inicio > ? AND df_inicio < ?) OR
                                      (df_inicio < ? AND df_fin    > ?)
                                   ) d ON d.dc_tecnico = pt.dc_tecnico',
                          'f.*',
                          'd.dc_tecnico IS NULL AND pt.dc_area_servicio = ? AND pt.dc_tipo_falla = ?')
                );
        $data->bindValue(1, $this->fecha_inicio, PDO::PARAM_STR);
        $data->bindValue(2, $this->fecha_termino, PDO::PARAM_STR);
        $data->bindValue(3, $this->fecha_inicio, PDO::PARAM_STR);
        $data->bindValue(4, $this->fecha_termino, PDO::PARAM_STR);
        $data->bindValue(5, $this->fecha_inicio, PDO::PARAM_STR);
        $data->bindValue(6, $this->fecha_termino, PDO::PARAM_STR);
        $data->bindValue(7, $this->fecha_inicio, PDO::PARAM_STR);
        $data->bindValue(8, $this->fecha_termino, PDO::PARAM_STR);
        $data->bindValue(9, $area->dc_area_servicio, PDO::PARAM_INT);
        $data->bindValue(10, $tipo_falla->dc_tipo_falla, PDO::PARAM_INT);
        $db->stExec($data);
        
        return $data->fetchAll(PDO::FETCH_OBJ);
        
    }
  
}

?>
