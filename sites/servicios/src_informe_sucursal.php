<?php
define("MAIN",1);
require_once("../../inc/init.php");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

?>
<div id="secc_bar">
	Informe de servicios por sucursal
</div>
<div id="main_cont">
	<div class="panes">
    	<?php $form->Start('sites/servicios/proc/src_informe_sucursal.php','src_informe_sucursal') ?>
        <?php $form->Header('<b>Indique los datos de filtro para consultar por las ordenes de servicio por sucursal</b><br />
							 Los campos marcados con [*] son obligatorios') ?>
        <table class="tab" width="100%">
        	<tbody>
            	<tr>
                	<td width="150">Cliente [*]</td>
                    <td><?php $form->DBSelect('','dc_cliente','tb_cliente',array('dc_cliente','dg_razon'),1) ?></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td>Fecha Emisión</td>
                    <td><?php $form->Date('Desde','df_emision_desde',1,'01/'.date('m/Y')) ?></td>
                    <td><?php $form->Date('Hasta','df_emision_hasta',1,0) ?></td>
                </tr>
            </tbody>
        </table>
        <?php $form->End('Ejecutar consulta','searchbtn') ?>
    </div>
</div>