<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$status = $db->select('tb_orden_servicio','dc_estado,dm_estado,dm_facturable,dg_glosa_facturacion,df_fecha_emision',"dc_orden_servicio={$_POST['id']}");

if(!count($status)){
	$error_man->showWarning('No se encontró la orden de servicio indicada');
	exit();
}
$solucionado = $status[0]['dm_estado'];
$facturable = $status[0]['dm_facturable'];
$glosa = $status[0]['dg_glosa_facturacion'];
$creacion = $status[0]['df_fecha_emision'];
$status = $status[0]['dc_estado'];

$data = $db->select("(SELECT * FROM tb_orden_servicio_avance WHERE dc_orden_servicio={$_POST['id']}) av
LEFT JOIN tb_estado_orden_servicio es ON es.dc_estado = av.dc_estado_orden",
'dg_gestion,es.dg_estado,dm_estado_orden,dc_avance,
DATE_FORMAT(av.df_creacion,"%d/%m/%Y %H:%i") as df_creacion,
DATE_FORMAT(df_proxima_actividad,"%d/%m/%Y") as df_estimada,
DATE_FORMAT(df_cierre_actividad,"%d/%m/%Y") as df_cierre',"",
array('order_by'=>'av.df_creacion DESC'));

echo("<div class='panes' style='width:910px;'>");
if(count($data)){

	echo("<table class='tab' width='100%'>
	<caption>Últimas actividades realizadas sobre la orden de servicio</caption>
	<thead><tr>
		<th>Gestión</th>
		<th width='120'>Estado Orden de servicio</th>
		<th width='90'>Fecha creación</th>
		<th width='90'>Cierre estimado</th>
		<th width='90'>Fecha cierre</th>
	</thead><tbody>");
		foreach($data as $d){
		$estado = 'Abierta';
		if($d['dm_estado_orden'] == '1')
			$estado = 'Cerrada';
			echo("<tr>
				<td>{$d['dg_gestion']}</td>
				<td>{$estado}</td>
				<td>{$d['df_creacion']}</td>
				<td>{$d['df_estimada']}</td>
				<td>{$d['df_cierre']}</td>
			</tr>");
		}
	echo("</tbody></table><hr />");

}
?>