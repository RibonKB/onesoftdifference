<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_orden_servicio','dq_orden_servicio',"dc_orden_servicio={$_POST['id']}");

if(!count($data)){
	$error_man->showWarning("No se encontró la Orden de servicio especificada.");
	exit;
}
$data = $data[0];

echo("<div class='secc_bar'>Detalle de Factura para orden de servicio <b>{$data['dq_orden_servicio']}</b></div>
<div class='panes'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/servicios/proc/crear_detalle_facturacion.php','cr_detalle_factura','os_form');
$form->Header("Agregue detalles que irán en la factura");
echo("<table class='tab' width='100%'>
<thead><tr>
	<th>&nbsp;</th>
	<th width='150'>Producto</th>
	<th>Serie</th>
	<th>Cantidad</th>
	<th>Precio</th>
	<th width='120'>Total</th>
	<th>Costo</th>
	<th width='120'>Costo Total</th>
</tr></thead>
<tfoot>
	<tr>
		<th colspan='4' align='right'>Totales</th>
		<th colspan='2' align='right' id='total'>0</th>
		<th colspan='2' align='right' id='total_costo'>0</th>
	</tr>
	<tr>
		<th colspan='4' align='right'>Total Neto</th>
		<th colspan='2' align='right' id='total_neto'>0</th>
		<th colspan='2'></th>
	</tr>
	<tr>
		<th colspan='4' align='right'>IVA</th>
		<th colspan='2' align='right' id='total_iva'>0</th>
		<th colspan='2'></th>
	</tr>
	<tr>
		<th colspan='4' align='right'>Total a pagar</th>
		<th colspan='2' align='right' id='total_pagar'>0</th>
		<th colspan='2'></th>
	</tr>
	</tfoot>
<tbody id='prod_list'></tbody>
</table>");
$form->Button('Agregar otro producto','id="prod_add"','addbtn');
$form->Hidden('cot_iva',0);
$form->Hidden('cot_neto',0);
$form->Hidden('os_id',$_POST['id']);
$form->End('Crear','addbtn');

echo("<table class='hidden' id='prods_form'>
<tr class='main'>
	<td align='center'>
		<img src='images/delbtn.png' alt='' title='' title='Eliminar detalle' class='del_detail' />
	</td>
	<td><input type='text' name='prod[]' class='prod_codigo searchbtn' size='17' /></td>
	<td><input type='text' name='serie[]' class='prod_serie inputtext' size='10' /></td>
	<td><input type='text' name='cant[]' class='prod_cant inputtext' size='3' style='text-align:right;' required='required' /></td>
	<td><input type='text' name='precio[]' class='prod_price inputtext' size='7' style='text-align:right;' required='required' /></td>
	<td class='total' align='right'>0</td>
	<td align='right'><input type='text' name='costo[]' class='prod_costo inputtext' size='7' style='text-align:right;' required='required' /></td>
	<td class='costo_total' align='right'>0</td>
</tr>
</table>");

$detalle_factura = $db->select("(SELECT * FROM tb_orden_servicio_factura_detalle WHERE dc_orden_servicio = {$_POST['id']}) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto",
'd.dc_detalle,p.dg_codigo,p.dg_producto AS dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,p.dc_producto,d.dg_serie,d.dq_despachado');

?>
</div>
<script type="text/javascript">
	var empresa_iva = <?=$empresa_conf['dq_iva'] ?>;
	var detalle_factura = <?=json_encode($detalle_factura) ?>;
</script>
<script type="text/javascript" src="jscripts/product_manager/det_orden_servicio.js?v0_0_8"></script>