<?php
define('MAIN',1);
require_once("../../../inc/global.php");

$db->escape($_GET['number']);

if(!is_numeric($_GET['number'])){
	echo json_encode('<not-found>');
	exit;
}

$tb_nota_venta = $db->select('tb_nota_venta',
'dc_nota_venta, dq_nota_venta',
"dc_empresa = {$empresa} AND dq_nota_venta={$_GET['number']} AND dm_validada = '1' AND dm_confirmada = '1' AND dm_nula='0'");

if(!count($tb_nota_venta)){
	echo json_encode('<not-found>');
	exit;
}
$tb_nota_venta = $tb_nota_venta[0];

echo json_encode($tb_nota_venta);
?>