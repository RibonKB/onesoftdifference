<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if($_POST['os_solucionado'] != '0'){
    if(!isset($_POST['os_tecnico_apoyo'])){
      $error_man->showWarning("Debe seleccionar al menos un técnico ejecutor para realizar las labores de la orden de servicio");
      exit;
    }
}

$data = array(
	'dc_cliente' => $_POST['os_cliente'],
	'dg_solicitante' => $_POST['os_solicitante'],
	'dc_sucursal' => $_POST['os_sucursal'],
	'dc_area_servicio' => $_POST['os_area'],
	'dg_requerimiento' => $_POST['os_request'],
	'dc_estado' => $_POST['os_estado'],
	'dc_tecnico' => $_POST['os_tecnico'],
	'df_fecha_emision' => 'NOW()',
	'dc_empresa' => $empresa,
	'dm_facturable' => ($_POST['os_facturable']*-1)+1,
	'dg_glosa_facturacion' => $_POST['os_glosa_factura'],
	'dc_nota_venta' => $_POST['dc_nota_venta']
);

if(isset($_POST['os_other_falla']))
	$data['dg_tipo_falla_manual'] =& $_POST['os_tipo_falla_txt'];
else
	$data['dc_tipo_falla'] =& $_POST['os_tipo_falla'];
	
if($_POST['os_solucionado'] == '0'){
	$data['dg_solucion'] =& $_POST['os_solucion'];
	$data['dm_estado'] = '1';
}else{
	if($_POST['os_fecha_hora'] == 12) $_POST['os_fecha_hora'] = 0;
	$hora = str_pad($_POST['os_fecha_hora']+$_POST['os_fecha_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['os_fecha_minuto'];
	$data['df_compromiso'] = $db->sqlDate($_POST['os_fecha'].' '.$hora);
    
    $hora = str_pad($_POST['os_fecha_final_hora']+$_POST['os_fecha_final_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['os_fecha_final_minuto'];
    $data['df_fecha_final'] = $db->sqlDate($_POST['os_fecha_final'].' '.$hora);
    
    unset($hora);
    
	$data['dm_estado'] = '0';
}

$cot_prefix = "";
$largo_prefix = 0;
switch($empresa_conf['dc_correlativo_orden_servicio']){
	case '1':$cot_prefix = date("Y"); $largo_prefix = 4; break;
	case '2':$cot_prefix = date("Ym"); $largo_prefix = 6; break;
}

$db->start_transaction();
$Mactual = date("m");
$Yactual = date("Y");
if($empresa_conf['dc_correlativo_orden_servicio'] != 3){
	$last = $db->select('tb_orden_servicio','MAX(dq_orden_servicio) AS dq',
	"dc_empresa={$empresa} AND MONTH(df_fecha_emision) = '{$Mactual}' AND YEAR(df_fecha_emision) = '{$Yactual}'");
}else{
	$last = $db->select('tb_orden_servicio','MAX(dq_orden_servicio) AS dq',"dc_empresa={$empresa}");
}

if($last[0]['dq'] != NULL){
	$last = $last[0]['dq'];
	$cot_num = substr($last,$largo_prefix)+1;
}else{
	$cot_num = $empresa_conf['dg_correlativo_orden_servicio'];
}

$data['dq_orden_servicio'] = $cot_prefix.str_pad($cot_num,4,'0',STR_PAD_LEFT);

$orden = $db->insert('tb_orden_servicio',$data);

$error_man->showConfirm("Se ha generado la orden de servicio");
echo("<div class='title'>El número de orden es <h1 style='margin:0;color:#000;'>{$data['dq_orden_servicio']}</h1></div>");

if($_POST['os_area']){
	$error_man->showInfo("<button class='button' id='area_mail'>Enviar correo electrónico a los encargados del area de servicio indicada</button>");
?>
<script type="text/javascript">
$("#area_mail").click(function(){
	$(this).html("<img src='images/ajax-loader.gif' alt='...' /> Cargando ...");
	disable_button(this);
	loadOverlay("sites/servicios/proc/send_area_mail.php?id=<?php echo $orden ?>");
});
</script>
<?php
}

$avance = array(
	"dc_orden_servicio" => $orden,
	"dg_gestion" => $data['dg_requerimiento'],
	"df_creacion" => 'NOW()',
	"dc_estado_orden" => $data['dc_estado'],
	"dm_estado_orden" => $data['dm_estado']
);

if($_POST['os_solucionado'] == '0'){
	$avance['df_cierre_actividad'] = 'NOW()';
	$avance['dm_estado_actividad'] = '0';
}else
	$avance['df_proxima_actividad'] = $data['df_compromiso'];

$db->insert('tb_orden_servicio_avance',$avance);

if($_POST['os_solucionado'] != '0'){
    if(isset($_POST['os_tecnico_apoyo'])){
      foreach($_POST['os_tecnico_apoyo'] as $dc_tecnico){
          $db->insert('tb_tecnico_apoyo',array(
              'dc_orden_servicio' => $orden,
              'dc_tecnico' => $dc_tecnico,
              'df_inicio' => $data['df_compromiso'],
              'df_fin' => $data['df_fecha_final']
          ));
      }
    }
}

if(intval($_POST['dc_ticket_servicio'])){
  $db->insert('tb_ticket_orden_servicio',array(
      'dc_orden_servicio' => $orden,
      'dc_ticket' => intval($_POST['dc_ticket_servicio'])
  ));
}

$db->commit();
?>