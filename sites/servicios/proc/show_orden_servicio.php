<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
require_once("../../../inc/Factory.class.php");

$data = $db->select("(SELECT * FROM tb_orden_servicio WHERE dc_orden_servicio = {$_POST['id']} AND dc_empresa = {$empresa}) o
  LEFT JOIN tb_nota_venta nv ON nv.dc_nota_venta = o.dc_nota_venta
  LEFT JOIN tb_cliente cl ON o.dc_cliente = cl.dc_cliente
  LEFT JOIN tb_tipo_falla_os tf ON o.dc_tipo_falla = tf.dc_tipo_falla
  LEFT JOIN tb_funcionario tc ON o.dc_tecnico = tc.dc_funcionario
  LEFT JOIN tb_estado_orden_servicio es ON es.dc_estado = o.dc_estado
  LEFT JOIN tb_cliente_sucursal s ON s.dc_sucursal = o.dc_sucursal",
'o.dq_orden_servicio,cl.dg_razon,o.dg_solicitante,o.dg_requerimiento,tf.dg_tipo_falla,o.dg_tipo_falla_manual,o.dm_facturable,o.dc_factura,
CONCAT_WS(" ",tc.dg_nombres,tc.dg_ap_paterno,tc.dg_ap_materno) AS dg_tecnico,es.dg_estado,o.dm_estado,s.dg_sucursal,
DATE_FORMAT(o.df_compromiso,"%d/%m/%Y %I:%i %p") AS df_compromiso, o.dg_solucion, nv.dq_nota_venta,
DATE_FORMAT(o.df_fecha_emision,"%d/%m/%Y %I:%i %p") AS df_emision,o.dc_cliente,
TIMESTAMPDIFF(MINUTE,o.df_inicio,o.df_fin) AS horas_consumidas,DATE_FORMAT(o.df_inicio,"%d/%m/%Y %h:%i %p") AS df_inicio,DATE_FORMAT(o.df_fin,"%d/%m/%Y %h:%i %p") AS df_fin');

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la oportunidad especificada");
	exit();
}
$data = $data[0];

$detalle_factura = $db->select("(SELECT * FROM tb_orden_servicio_factura_detalle WHERE dc_orden_servicio = {$_POST['id']}) d
JOIN tb_producto p ON p.dc_producto = d.dc_producto",
'p.dg_producto AS dg_descripcion,d.dq_cantidad,d.dq_precio_venta,d.dq_precio_compra,d.dq_cantidad*d.dq_precio_venta as dq_total,d.dq_cantidad*d.dq_precio_compra as dq_costo_total,d.dg_serie');

$estado = '';
if($data['dm_estado'] == '1')
	$estado = '(Solucionado)';
	
$facturada = '';
if($data['dm_facturable'] == '1'){
	if($data['dc_factura'] != '0')
		$factura = $db->select('tb_factura_venta','dq_factura, dq_folio',"dc_factura={$data['dc_factura']}");
	else
		$factura = $db->select('tb_factura_venta','dq_factura, dq_folio',"dc_orden_servicio = {$_POST['id']} AND dm_nula = 0");
	
	if(count($factura)){
		$facturada = "<div class='info'>
							Factura: <strong>{$factura[0]['dq_factura']}</strong>
							<br />
							Folio: <strong>{$factura[0]['dq_folio']}</strong>
					  </div>";
	}
}

$tech_q = $db->select(
			'tb_tecnico_apoyo t JOIN tb_funcionario f ON f.dc_funcionario = t.dc_tecnico',
			'f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno',
			"t.dc_orden_servicio = {$_POST['id']}");
			
$tecnicos = '';

if(count($tech_q)){
	$tecnicos  = '<ul>';
	
	while($t = array_shift($tech_q)):
		$tecnicos .= "<li>{$t['dg_nombres']} {$t['dg_ap_paterno']} {$t['dg_ap_materno']}</li>";
	endwhile;
	
	$tecnicos .= '</ul>';
}

echo("<div class='title center'>Orden de servicio Nº {$data['dq_orden_servicio']}</div>
<table class='tab' width='100%' style='text-align:left;'>
<caption>Cliente:<br /><strong> {$data['dg_razon']}</strong></caption>
<tr>
	<td width='160'>Fecha emision</td>
	<td><b>{$data['df_emision']}</td>
	<td rowspan='7' width='250'>
		<label>Estado de orden</label><br />
		<h3 class='info' style='margin:0'>{$data['dg_estado']} {$estado}</h3>
		{$facturada}
	</td>
</tr><tr>
	<td>Nota de Venta</td>
	<td><b>{$data['dq_nota_venta']}</b></td>
</tr><tr>
	<td>Solicitante</td>
	<td><b>{$data['dg_solicitante']}</b></td>
</tr><tr>
	<td>Sucursal</td>
	<td><b>{$data['dg_sucursal']}</b></td>
</tr><tr>
	<td>Tipo falla</td>
	<td><b>{$data['dg_tipo_falla']}{$data['dg_tipo_falla_manual']}</b></td>
</tr><tr>
	<td>Supervisor</td>
	<td><b>{$data['dg_tecnico']}</b></td>
</tr><tr>
	<td>Técnicos</td>
	<td><b>{$tecnicos}</b></td>
</tr><tr>
	<td>Fecha compromiso</td>
	<td><b>{$data['df_compromiso']}</b></td>
</tr><tr>
	<td>Requerimiento</td>
	<td colspan='2'><b>{$data['dg_requerimiento']}</b></td>
</tr><tr>
	<td>Solución</td>
	<td colspan='2'><b>{$data['dg_solucion']}</b></td>
</tr></table>");

if(count($detalle_factura)){
echo('<br /><table class="tab" width="100%">
<caption>Detalle de facturación</caption>
<thead><tr>
	<th>Producto</th>
	<th width="55">Cantidad</th>
	<th width="90">Precio</th>
	<th width="90">Total</th>
	<th width="90">Costo</th>
	<th width="90">Costo Total</th>
</tr></thead>
<tbody>');
foreach($detalle_factura as $d){
	$d['dq_precio_venta'] = moneda_local($d['dq_precio_venta']);
	$d['dq_precio_compra'] = moneda_local($d['dq_precio_compra']);
	$d['dq_total'] = moneda_local($d['dq_total']);
	$d['dq_costo_total'] = moneda_local($d['dq_costo_total']);
	echo("<tr>
		<td>{$d['dg_descripcion']}</td>
		<td align='center'>{$d['dq_cantidad']}</td>
		<td align='right'>{$d['dq_precio_venta']}</td>
		<td align='right'>{$d['dq_total']}</td>
		<td align='right'>{$d['dq_precio_compra']}</td>
		<td align='right'>{$d['dq_costo_total']}</td>
	</tr>");
}
echo("</tbody>
</table>");
}

if($data['horas_consumidas'] != NULL){
	
	$data['horas_consumidas'] = ceil(($data['horas_consumidas']-5)/60);
	
	echo("<br /><table class='tab' width='100%'>
	<caption>Tiempo consumido en el servicio</caption>
	<thead><tr>
		<th>Fecha Inicio</th>
		<th>Fecha de Término</th>
	</tr></thead>
	<tfoot><tr>
		<th align='right'>Horas consumidas : </th>
		<th align='left'>{$data['horas_consumidas']}</th>
	</tr></tfoot>
	<tbody><tr>
		<td>{$data['df_inicio']}</td>
		<td>{$data['df_fin']}</td>
	</tr></tbody></table>");
}

?>
<script type="text/javascript">
$('#print_version').unbind('click').click(function(){
	window.open("sites/servicios/print_orden_servicio.php?id=<?=$_POST['id'] ?>",'print_orden_servicio','width=800;height=600');
});
$('#edit_orden').unbind('click').click(function(){
	pymerp.loadPage("<?php echo Factory::buildUrl('EditarOrdenServicio', 'servicios',null,null,array('id' => $_POST['id'])) ?>");
});
<?php if($data['dm_estado'] == '1'): ?>
disable_button('#gestion_orden');
<?php else: ?>
enable_button('#gestion_orden');
$('#gestion_orden').unbind('click').click(function(){
	loadOverlay("sites/servicios/src_gestion_orden_servicio.php?id=<?=$_POST['id'] ?>");
});
<?php endif; ?>
<?php if($data['dm_estado'] == '1'): ?>
  pymerp.enableButton('#ov_gestion_telefonica');
  $('#ov_gestion_telefonica').unbind('click').click(function(){
    pymerp.loadOverlay("<?php echo Factory::buildUrl('BotoneraGestionOrdenServicio', 'servicios', 'cierreTelefonico', null, array('dc_orden_servicio' => $_POST['id'])) ?>");
  });
<?php else: ?>
  pymerp.disableButton('#ov_gestion_telefonica');
<?php endif ?>
$('#historial_actividad').unbind('click').click(function(){
	loadOverlay("sites/servicios/history_gestion_orden_servicio.php?id=<?=$_POST['id'] ?>");
});
$('#detalle_factura').unbind('click').click(function(){
	loadOverlay("sites/servicios/src_gestion_factura_det.php?id=<?=$_POST['id'] ?>");
});
$('#facturar_os').unbind('click').click(function(){
	loadpage("sites/ventas/factura_venta/proc/cr_factura_venta.php?os_numero=<?=$data['dq_orden_servicio'] ?>&cli_id=<?=$data['dc_cliente'] ?>");
});
$('#add_guia').unbind('click').click(function(){
	loadpage("sites/ventas/guia_despacho/proc/cr_guia_despacho.php?os_numero=<?=$data['dq_orden_servicio'] ?>&cli_id=<?=$data['dc_cliente'] ?>");
});

</script>