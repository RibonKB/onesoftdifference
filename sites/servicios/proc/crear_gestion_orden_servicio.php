<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!$_POST['gestion_fecha']){
	if($_POST['gestion_solucionado'] == 1){
		$error_man->showWarning('Si no indica una fecha para la próxima actividad debe seleccionar que fue solucionado y poner una razón.');
		exit;
	}
}

$cambios_op = array();
if($_POST['gestion_cambio'])
	$cambios_op['dc_estado'] = $_POST['gestion_cambio'];

$cambios_op['dm_estado'] = ($_POST['gestion_solucionado']*-1)+1;

if($cambios_op['dm_estado'] == 1){
	$cambios_op['dg_solucion'] = $_POST['gestion_gestion'];
	
	if($_POST['gestion_horaini'] == 12) $_POST['gestion_horaini'] = 0;
	if($_POST['gestion_horafin'] == 12) $_POST['gestion_horafin'] = 0;
	
	$hora = str_pad($_POST['gestion_horaini']+$_POST['gestion_meridianoini'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_minutoini'];
	$cambios_op['df_inicio'] = $db->sqlDate($_POST['gestion_solini'].' '.$hora);
	
	$hora = str_pad($_POST['gestion_horafin']+$_POST['gestion_meridianofin'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_minutofin'];
	$cambios_op['df_fin'] = $db->sqlDate($_POST['gestion_solfin'].' '.$hora);
	
	$cambios_op['dg_aceptante'] = $_POST['gestion_aceptante'];
}

$db->update('tb_orden_servicio',$cambios_op,"dc_orden_servicio = {$_POST['os_id']}");

if(isset($_POST['gestion_avance'])){
	$db->update('tb_orden_servicio_avance',
	array(
		"dm_estado_actividad" => 0,
		"df_cierre_actividad" => 'NOW()'
	),"dc_avance = {$_POST['gestion_avance']}");
}

if($_POST['gestion_fecha']){
	if($_POST['gestion_hora'] == 12) $_POST['gestion_hora'] = 0;
	$hora = str_pad($_POST['gestion_hora']+$_POST['gestion_meridiano'],2,'0',STR_PAD_LEFT).":".$_POST['gestion_minuto'];
	$_POST['gestion_fecha'] = $_POST['gestion_fecha']." ".$hora;
	unset($hora);
}

$db->insert('tb_orden_servicio_avance',
array(
	"dc_orden_servicio" => $_POST['os_id'],
	"dg_gestion" => $_POST['gestion_gestion'],
	"df_creacion" => 'NOW()',
	"df_proxima_actividad" => $db->sqlDate($_POST['gestion_fecha']),
	"dc_estado_orden" => $_POST['gestion_estado'],
	"dm_estado_actividad" => 1,
	"dm_estado_orden" => $cambios_op['dm_estado']
));

?>
<script type="text/javascript">
	$('#genOverlay').remove();
	alert("Se ha generado una nueva actividad sobre la orden de servicio");
</script>