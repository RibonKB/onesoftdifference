<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['os_emision_desde'] = $db->sqlDate($_POST['os_emision_desde']);
$_POST['os_emision_hasta'] = $db->sqlDate($_POST['os_emision_hasta']." 23:59");

$_POST['os_status'] = implode("','",$_POST['os_status']);

$conditions = "dc_empresa = {$empresa} AND (df_fecha_emision BETWEEN {$_POST['os_emision_desde']} AND {$_POST['os_emision_hasta']}) AND dm_estado IN ('{$_POST['os_status']}')";

if($_POST['os_numero_desde']){
	if($_POST['os_numero_hasta'])
		$conditions .= " AND (dq_orden_servicio BETWEEN {$_POST['os_numero_desde']} AND {$_POST['os_numero_hasta']})";
	else
		$conditions .= " AND dq_orden_servicio = {$_POST['os_numero_desde']}";
}

if($_POST['os_compromiso_desde']){
	$_POST['os_compromiso_desde'] = $db->sqlDate($_POST['os_compromiso_desde']);
	if($_POST['os_compromiso_hasta']){
		$_POST['os_compromiso_hasta'] = $db->sqlDate($_POST['os_compromiso_hasta']);
		$conditions .= " AND (df_compromiso BETWEEN {$_POST['os_compromiso_desde']} AND {$_POST['os_compromiso_hasta']})";
	}else
		$conditions .= " AND df_compromiso = {$_POST['os_compromiso_desde']}";
}

if(isset($_POST['os_client'])){
	$_POST['os_client'] = implode(',',$_POST['os_client']);
	$conditions .= " AND dc_cliente IN ({$_POST['os_client']})";
}

if(isset($_POST['os_falla'])){
	$_POST['os_falla'] = implode(',',$_POST['os_falla']);
	$conditions .= " AND dc_cliente IN ({$_POST['os_falla']})";
}

if(isset($_POST['os_tecnico'])){
	$_POST['os_tecnico'] = implode(',',$_POST['os_tecnico']);
	$conditions .= " AND dc_tecnico IN ({$_POST['os_tecnico']})";
}

if(isset($_POST['op_estado'])){
	$_POST['os_estado'] = implode(',',$_POST['os_estado']);
	$conditions .= " AND dc_estado IN ({$_POST['os_estado']})";
}

if(isset($_POST['os_factured'])){
	if(count($_POST['os_factured']) == 1){
		if($_POST['os_factured'][0] == 0)
			$simbol = '=';
		else
			$simbol = '>';
		$conditions .= " AND dc_factura {$simbol} 0 AND dm_facturable=1";
	}
}

$data = $db->select('tb_orden_servicio','dc_orden_servicio,dq_orden_servicio,dm_facturable,dc_factura',$conditions,array('order_by' => 'df_fecha_emision DESC'));

if(!count($data)){
	$error_man->showAviso("No se encontraron ordenes de servicio con los criterios especificados");
	exit();
}

echo("<div id='show_orden'></div>");

echo("
<div id='options_menu'>
<div id='res_list'>
<table class='tab sortable' width='100%'>
<caption>Ordenes de Servicio<br />Encontradas</caption>
<thead>
	<tr>
		<th>Nº Orden</th>
	</tr>
</thead>
<tbody>");

foreach($data as $o){
	
	if($o['dm_facturable'] == 1 && $o['dc_factura'] == 0){
		$facturas = $db->select('tb_factura_venta','true','dc_orden_servicio = '.$o['dc_orden_servicio']);
		if(count($facturas))
			$lukas = '<img src="images/lukas.png" alt="" class="right" width="18" />';
		else
			$lukas = '<img src="images/lukas_red.png" alt="" class="right" width="18" />';
	}else if($o['dm_facturable'] == 1 && $o['dc_factura'] != 0)
		$lukas = '<img src="images/lukas.png" alt="" class="right" width="18" />';
	else
		$lukas = '';

echo("<tr>
	<td align='left'>{$lukas}
		<a href='sites/servicios/proc/show_orden_servicio.php?id={$o['dc_orden_servicio']}' class='os_load'>
		 <img src='images/doc.png' alt='' style='vertical-align:middle;' />{$o['dq_orden_servicio']}</a>
	</td>
</tr>");
}
?>

</tbody>
</table>
</div>

<button class='button' id='show_hide_list'>Mostrar/Ocultar Listado</button>
<button type='button' class='button' id='print_version'>Version de impresión</button>
<button type='button' class='editbtn' id='edit_orden'>Editar</button>
<button type='button' id='gestion_orden' class='imgbtn' style='background-image:url(images/management.png)'>Gestión</button>
<button type='button' id='historial_actividad' class='imgbtn' style='background-image:url(images/archive.png)'>Historial</button>
<button type='button' id='detalle_factura' class='imgbtn' style='background-image:url(images/doc.png)'>Detalle de Facturación</button>
<button type='button' id='facturar_os' class='imgbtn' style='background-image:url(images/doc.png)'>Facturar</button>
<button type='button' id='add_guia' class='imgbtn' style='background-image:url(images/doc.png)'>Generar guía por traslado</button>
<?php if(check_permiso(76)): ?>
<button type='button' id='ov_gestion_telefonica' class='imgbtn' style='background-image:url(images/phone.png)'>Cierre Telefónico</button>
<?php endif ?>
</div>


<script type="text/javascript">
	$("#res_list").slideDown();
	$("table.sortable").tablesorter();
	$(".os_load").click(function(e){
		e.preventDefault();
		$('#show_orden').html("<img src='images/ajax-loader.gif' alt='' /> cargando orden de servicio ...");
		$("#res_list td").removeClass('confirm');
		$(this).parent().addClass('confirm');
		$('.panes').width('auto').css({marginLeft:'210px',marginRight:'20px'});
		loadFile($(this).attr('href'),'#show_orden');
	}).first().trigger('click');
	
	$('#show_hide_list').click(function(){
		$('#res_list').toggle();
	});
</script>