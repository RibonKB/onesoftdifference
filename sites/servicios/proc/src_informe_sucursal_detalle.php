<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$conditions = "os.dc_empresa = ? AND (os.df_fecha_emision BETWEEN ? AND ?) AND os.dc_cliente = ?";

$params = array(
	array($empresa, 			PDO::PARAM_INT),
	array($_POST['df_emision_desde'],	PDO::PARAM_STR),
	array($_POST['df_emision_hasta'],	PDO::PARAM_STR),
	array($_POST['dc_cliente'],	PDO::PARAM_INT)
);

if(isset($_POST['dc_sucursal'])){
	$conditions .= ' AND os.dc_sucursal = ?';
	$params[] = array($_POST['dc_sucursal'],PDO::PARAM_INT);
}

if(isset($_POST['dc_estado'])){
	$conditions .= ' AND os.dc_estado = ?';
	$params[] = array($_POST['dc_estado'],PDO::PARAM_INT);
}

$data = $db->prepare($db->select('tb_orden_servicio os
			LEFT JOIN tb_cliente_sucursal s ON s.dc_sucursal = os.dc_sucursal
			LEFT JOIN tb_estado_orden_servicio es ON es.dc_estado = os.dc_estado
			LEFT JOIN 
				(SELECT *
				 FROM tb_orden_servicio_avance
				 WHERE 
				 	dm_estado_actividad = 1 AND
					dm_estado_orden = 0
				) gs ON gs.dc_orden_servicio = os.dc_orden_servicio
			LEFT JOIN tb_funcionario f ON f.dc_funcionario = os.dc_tecnico',
			'os.dq_orden_servicio, es.dg_estado, os.dg_solicitante, os.dg_requerimiento, os.dg_solucion, os.dm_estado, s.dg_sucursal,
			 gs.df_proxima_actividad, CONCAT_WS(" ",f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS dg_tecnico',$conditions));

foreach($params as $i => $p){
	$data->bindValue($i+1,$p[0],$p[1]);
}

$db->stExec($data);

?>
<div class="secc_bar">
	Ordenes de Servicio
</div>
<div class="panes">
	<table class="tab bicolor_tab" width="100%" id="informe_sucursal_detalle">
    	<thead>
        	<tr>
            	<th>Orden de servicio</th>
                <th>Estado</th>
                <th>Solicitante</th>
                <th width="250">Requerimiento</th>
                <th width="250">Solución</th>
                <th>Sucursal</th>
                <th>Próxima gestión</th>
                <th>Técnico</th>
            </tr>
        </thead>
        <tbody>
        <?php while($d = $data->fetch(PDO::FETCH_OBJ)): ?>
        	<tr>
            	<td><b><?php echo $d->dq_orden_servicio ?></b></td>
                <td>
					<?php echo $d->dg_estado ?>
					<?php if($d->dm_estado == 1): ?>
                    	(Solucionado)
                    <?php endif; ?>
                </td>
                <td><?php echo $d->dg_solicitante ?></td>
                <td><?php echo $d->dg_requerimiento ?></td>
                <td><?php echo $d->dg_solucion ?></td>
                <td><?php echo $d->dg_sucursal ?></td>
                <td><?php echo $db->dateLocalFormat($d->df_proxima_actividad) ?></td>
                <td><?php echo $d->dg_tecnico ?></td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
	window.setTimeout(function(){
		$('#informe_sucursal_detalle').tableAdjust(5);
	},100);
</script>