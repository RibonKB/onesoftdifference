<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_tipo_falla_os','dc_tipo_falla,dg_tipo_falla',"dc_area_servicio={$_POST['area']}");

echo("<option value='0'></option>");
foreach($data as $tipo){
	echo("<option value='{$tipo['dc_tipo_falla']}'>{$tipo['dg_tipo_falla']}</option>");
}
?>