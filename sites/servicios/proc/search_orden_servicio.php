<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select('tb_orden_servicio','*',"dq_orden_servicio = {$_POST['number']}");

if(!count($data)){
	$error_man->showAviso('<div style="padding:15px;">No se ha encontrado la orden de servicio especificada</div>');
	exit;
}
$data = $data[0];
?>
<script type="text/javascript">
$('#os_cliente').val(<?=$data['dc_cliente'] ?>);
$('#os_solicitante').val("<?=$data['dg_solicitante'] ?>");
$('#os_request').val('<?=$data['dg_requerimiento'] ?>');
$('#os_tipo_falla').val(<?=$data['dc_tipo_falla'] ?>);
$('input[name=os_tipo_falla_txt]').attr('disabled',false).val('<?=$data['dg_tipo_falla'] ?>');
$('#os_tecnico').val(<?=$data['dc_tecnico'] ?>);
$('#os_estado').val(<?=$data['dc_estado'] ?>);
</script>