<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$df_emision_desde = $db->sqlDate2($_POST['df_emision_desde']);
$df_emision_hasta = $db->sqlDate2($_POST['df_emision_hasta'].' 23:59');

$condiciones = 'os.dc_empresa = ? AND (os.df_fecha_emision BETWEEN ? AND ?) AND os.dc_cliente = ?';

$params = array(
	array($empresa, 			PDO::PARAM_INT),
	array($df_emision_desde,	PDO::PARAM_STR),
	array($df_emision_hasta,	PDO::PARAM_STR),
	array($_POST['dc_cliente'],	PDO::PARAM_INT)
);

$data = $db->prepare($db->select('tb_orden_servicio os
								  LEFT JOIN tb_cliente_sucursal s ON s.dc_sucursal = os.dc_sucursal
								  
								  LEFT JOIN (
								  	SELECT SUM(dq_total) dq_total,dc_orden_servicio   
								  	FROM tb_orden_servicio_factura_detalle 
									GROUP BY dc_orden_servicio) fd ON fd.dc_orden_servicio = os.dc_orden_servicio
								  
								  LEFT JOIN(
								  	SELECT SUM(d.dq_total) dq_total_facturable,os2.dc_orden_servicio
									FROM tb_orden_servicio_factura_detalle d
									JOIN tb_orden_servicio os2 ON os2.dc_orden_servicio = d.dc_orden_servicio
									WHERE os2.dm_facturable = 1
									GROUP BY os2.dc_orden_servicio) fsd ON fsd.dc_orden_servicio = os.dc_orden_servicio',
								  
								  'os.dc_sucursal, s.dg_sucursal, os.dc_estado, COUNT(os.dc_estado) dc_cantidad,
								   SUM(fd.dq_total) dq_total, SUM(fsd.dq_total_facturable) dq_total_facturable,dm_facturable',
								  $condiciones,
								  array('group_by' => 'os.dc_sucursal, os.dc_estado')));

foreach($params as $i => $p){
	$data->bindValue($i+1,$p[0],$p[1]);
}

$db->stExec($data);

$grouped = array();

while($d = $data->fetch(PDO::FETCH_OBJ)){
	if(!isset($grouped[$d->dc_sucursal])){
		$grouped[$d->dc_sucursal] = array($d->dg_sucursal, array());
	}
	
	$grouped[$d->dc_sucursal][1][$d->dc_estado] = array($d->dc_cantidad,$d->dq_total,$d->dq_total_facturable);
	
}

$estados = $db->doQuery($db->select('tb_estado_orden_servicio','dc_estado, dg_estado',"dc_empresa = {$empresa}"))->fetchAll(PDO::FETCH_OBJ);

$todo = array();
foreach($estados as $s){
	$todo[$s->dc_estado] = array(0,0,0);
}

?>
<table width="100%" class="tab" id="informe_sucursal_general">
	<thead>
    	<tr>
        	<th width="250">Sucursal</th>
            <?php foreach($estados as $e): ?>
            	<th width="80" style="position:relative;">
                	<img src="images/cal_prev.gif" class="collapse_fv" id="e<?php echo $e->dc_estado ?>" style="position:absolute;top:0;right:0;" />
					<?php echo $e->dg_estado ?>
                </th>
                <th width="80" class="e<?php echo $e->dc_estado ?>"><?php echo $e->dg_estado ?> (Facturable)</th>
                <th width="80" class="e<?php echo $e->dc_estado ?>"><?php echo $e->dg_estado ?> (Facturado)</th>
            <?php endforeach; ?>
            <th width="80" style="position:relative;">
            	<img src="images/cal_prev.gif" class="collapse_fv" id="e_total" style="position:absolute;top:0;right:0;" />
            	Total
            </th>
            <th width="80" class="e_total">Total (Facturable)</th>
            <th width="80" class="e_total">Total (Facturado)</th>
        </tr>
    </thead>
    <tbody>
    	<?php foreach($grouped as $dc_sucursal => $s): ?>
        	<tr>
            	<td><b><?php echo $s[0] ?></b></td>
                <?php
                	$total = array(0,0,0);
					foreach($estados as $e):
						if(!isset($s[1][$e->dc_estado])){
							$s[1][$e->dc_estado] = array(0,0,0);
						}
						$total[0] += $s[1][$e->dc_estado][0];
						$total[1] += $s[1][$e->dc_estado][1];
						$total[2] += $s[1][$e->dc_estado][2];
				?>
                	<td align="right">
						<a href="sites/servicios/proc/src_informe_sucursal_detalle.php?dc_estado=<?php echo $e->dc_estado ?>&dc_sucursal=<?php echo $dc_sucursal ?>" class="loadOnOverlay">
							<?php echo $s[1][$e->dc_estado][0]; $todo[$e->dc_estado][0] += $s[1][$e->dc_estado][0]; ?>
                        </a>
                    </td>
                    <td align="right" class="e<?php echo $e->dc_estado ?>"><?php echo moneda_local($s[1][$e->dc_estado][1]); $todo[$e->dc_estado][1] += $s[1][$e->dc_estado][1]; ?></td>
                    <td align="right" class="e<?php echo $e->dc_estado ?>"><?php echo moneda_local($s[1][$e->dc_estado][2]); $todo[$e->dc_estado][2] += $s[1][$e->dc_estado][2]; ?></td>
                <?php endforeach; ?>
                <td align="right">
                	<a href="sites/servicios/proc/src_informe_sucursal_detalle.php?dc_sucursal=<?php echo $dc_sucursal ?>" class="loadOnOverlay">
						<?php echo $total[0] ?>
                    </a>
                </td>
                <td align="right" class="e_total"><?php echo moneda_local($total[1]) ?></td>
                <td align="right" class="e_total"><?php echo moneda_local($total[2]) ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
    
    
	<tfoot>

    	<tr> 
        	<th align="right">Totales</th>
    			<?php 
					$totales = array(0,0,0);
					foreach($todo as $dc_estado => $estado):
						$totales[0] += $estado[0];
						$totales[1] += $estado[1];
						$totales[2] += $estado[2];
				?>
            	<th align="right"><?php echo $estado[0] ?></th>
                <th align="right" class="e<?php echo $dc_estado ?>"><?php echo moneda_local($estado[1]) ?></th>
                <th align="right" class="e<?php echo $dc_estado ?>"><?php echo moneda_local($estado[2]) ?></th>
            <?php endforeach; ?>
            	<th align="right"><?php echo $totales[0] ?></th>
                <th align="right" class="e_total"><?php echo moneda_local($totales[1]) ?></th>
                <th align="right" class="e_total"><?php echo moneda_local($totales[2]) ?></th>
        </tr>
        
    </tfoot>    
    
</table>


<script type="text/javascript">
	//$('#informe_sucursal_general').tableAdjust(15);
	$('a.loadOnOverlay','#informe_sucursal_general').click(function(e){
		e.preventDefault();
		var dc_cliente = <?php echo $_POST['dc_cliente'] ?>;
		var df_emision_desde = '<?php echo $df_emision_desde ?>';
		var df_emision_hasta = '<?php echo $df_emision_hasta ?>';
		var href = this.href;
		
		pymerp.loadOverlay(href, 'dc_cliente='+dc_cliente+'&df_emision_desde='+df_emision_desde+'&df_emision_hasta='+df_emision_hasta);
	});
	
	$('.collapse_fv').toggle(function(){
		var id = $(this).attr('id');
		$('.'+id).hide();
		$(this).attr('src','images/cal_next.gif');
	},function(){
		var id = $(this).attr('id');
		$('.'+id).show();
		$(this).attr('src','images/cal_prev.gif');
	});
</script>