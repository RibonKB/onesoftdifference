<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_orden_servicio WHERE dc_orden_servicio = {$_POST['id']} AND dc_empresa={$empresa}) os
JOIN tb_area_servicio ase ON ase.dc_area_servicio = os.dc_area_servicio
JOIN tb_cliente cl ON cl.dc_cliente = os.dc_cliente
LEFT JOIN tb_cliente_sucursal cs ON cs.dc_sucursal = os.dc_sucursal
LEFT JOIN tb_tipo_falla_os tf ON tf.dc_tipo_falla = os.dc_tipo_falla
LEFT JOIN tb_funcionario f ON f.dc_funcionario = os.dc_tecnico
LEFT JOIN tb_estado_orden_servicio es ON es.dc_estado = os.dc_estado",
"os.dq_orden_servicio,cl.dg_razon,os.dg_solicitante,cs.dg_sucursal,ase.dg_correos,os.dg_requerimiento,
tf.dg_tipo_falla,os.dg_tipo_falla_manual,CONCAT_WS(' ',f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno) AS dg_tecnico,
es.dg_estado,DATE_FORMAT(os.df_compromiso, '%d/%m/%Y %H:%i') AS df_compromiso");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado la orden de servicio, correo no enviado");
	exit();
}
$data = $data[0];
str_replace("\n","<br>",$data['dg_requerimiento']);

$mensaje = "Este es un correo de notificación sobre una orden de servicio emitida en <b>Onesoft PYMEERP</b> en la que se definió este correo para alertas<br><br>
Los detalles de la orden son los siguientes:<br><br>
Número de orden: <b>{$data['dq_orden_servicio']}</b><br>
Fecha compromiso: <b>{$data['df_compromiso']}</b><br>
Cliente: <b>{$data['dg_razon']}</b><br>
Solicitante: <b>{$data['dg_solicitante']}</b><br>
Sucursal: <b>{$data['dg_sucursal']}</b><br>
Tipo de falla: <b>{$data['dg_tipo_falla']}{$data['dg_tipo_falla_manual']}</b><br>
Técnico: <b>{$data['dg_tecnico']}</b><br>
Estado actual de la orden: <b>{$data['dg_estado']}</b><br><br><br>
<b>Requerimiento:</b><br><br>
{$data['dg_requerimiento']}";

require_once("../../../inc/mail/class.phpmailer.php");
$mail = new PHPMailer();

$mail->IsSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->SMTPSecure = 'ssl';
$mail->SMTPKeepAlive = true;
$mail->Port = 465;
$mail->SMTPAuth = true;
$mail->Username = 'ribon22x@gmail.com';
$mail->Password = 'hyrule4370';

$mail->CharSet = 'UTF-8';
$mail->SetFrom('ribon22x@gmail.com','Tomás Kelne Benath');

$correos = explode(';',str_replace(' ','',$data['dg_correos']));		
foreach($correos as $c){
	$mail->AddAddress($c, '');
}

$mail->Subject = "Emitida Orden de servicio N° {$data['dq_orden_servicio']}";
$mail->MsgHTML($mensaje);

if(!$mail->Send()) {
	$error_man->showWarning("No se pudo enviar el correo a los destinatarios.<br />
compruebe los datos de acceso al servidor SMTP");
}

?>
<script type="text/javascript">
$("#area_mail").addClass('checkbtn').html("Correo enviado correctamente");
$("#genOverlay").remove();
</script>