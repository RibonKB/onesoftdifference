<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = array(
	'dc_cliente' => $_POST['os_cliente'],
	'dg_solicitante' => $_POST['os_solicitante'],
	'dc_sucursal' => $_POST['os_sucursal'],
	'dg_requerimiento' => $_POST['os_request'],
	'dc_tecnico' => $_POST['os_tecnico'],
	'df_fecha_emision' => 'NOW()',
	'dc_empresa' => $empresa,
	'dm_facturable' => ($_POST['os_facturable']*-1)+1,
	'dg_glosa_facturacion' => $_POST['os_glosa_factura']
);

if(isset($_POST['os_other_falla']))
	$data['dg_tipo_falla_manual'] =& $_POST['os_tipo_falla_txt'];
else
	$data['dc_tipo_falla'] =& $_POST['os_tipo_falla'];
	
if($_POST['os_solucionado'] == '0'){
	$data['dg_solucion'] =& $_POST['os_solucion'];
	$data['dm_estado'] = '1';
}else{
	$data['dm_estado'] = '0';
}

$db->update('tb_orden_servicio',$data,"dc_orden_servicio={$_POST['os_id']}");
?>
<script type="text/javascript">
	alert('Se han modificado los datos de la orden de servicio correctamente');
	$('#genOverlay').remove();
</script>