<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->start_transaction();

if(isset($_POST['prod'])){
	foreach($_POST['prod'] as $i => $v){
		
		$_POST['cant'][$i] = str_replace(',','',$_POST['cant'][$i]);
		$_POST['precio'][$i] = str_replace(',','',$_POST['precio'][$i]);
		$_POST['costo'][$i] = str_replace(',','',$_POST['costo'][$i]);
		
		if(isset($_POST['id_os_detail'][$i])){
			$db->update('tb_orden_servicio_factura_detalle',array(
				'dc_producto' => $v,
				'dq_cantidad' => $_POST['cant'][$i],
				'dg_serie' => $_POST['serie'][$i],
				'dq_precio_venta' => $_POST['precio'][$i],
				'dq_precio_compra' => $_POST['costo'][$i],
				'dq_total' => $_POST['cant'][$i]*$_POST['precio'][$i]
			),"dc_detalle={$_POST['id_os_detail'][$i]}");
		}else{
			$db->insert('tb_orden_servicio_factura_detalle',array(
				'dc_orden_servicio' => $_POST['os_id'],
				'dc_producto' => $v,
				'dq_cantidad' => $_POST['cant'][$i],
				'dg_serie' => $_POST['serie'][$i],
				'dq_precio_venta' => $_POST['precio'][$i],
				'dq_precio_compra' => $_POST['costo'][$i],
				'dq_total' => $_POST['cant'][$i]*$_POST['precio'][$i]
			));
		}
	}
}

if(isset($_POST['to_delete'])){
	$del = implode(',',$_POST['to_delete']);
	$db->query("DELETE FROM tb_orden_servicio_factura_detalle WHERE dc_detalle IN ({$del})");
}

$db->update('tb_orden_servicio',array('dq_neto' => $_POST['cot_neto'],'dq_iva' => $_POST['cot_iva']),"dc_orden_servicio={$_POST['os_id']}");

$db->commit();

$error_man->showConfirm("Se han agregado ".count($_POST['prod'])." detalles.");
?>