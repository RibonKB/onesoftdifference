<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Creación Ordenes de Servicio</div>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header('<b>Indique los datos de ingreso de la nueva orden de servicio</b><br />(Los campos marcados con [*] son obligatorios)');
	echo('<fieldset><label>Número de orden : 
	<input type="text" id="os_number_search" class="inputtext" size="30" /></label>
	<button type="button" id="os_number_submit" class="searchbtn">Buscar</button>');
	echo('</fieldset>');
	$form->Start('sites/servicios/proc/crear_orden_servicio.php','cr_orden_servicio');
	$form->Section();
	$form->Listado('Cliente','os_cliente','tb_cliente',array('dc_cliente','dg_razon'),1);
	$form->Text('Solicitante','os_solicitante',1);
	$form->Textarea('Requerimiento','os_request',1);
	$form->EndSection();
	$form->Section();
	echo("<fieldset>");
	$form->Listado('Tipo de falla','os_tipo_falla','tb_tipo_falla_os',array('dc_tipo_falla','dg_tipo_falla'));
	echo("<label><input type='checkbox' name='os_other_falla'/>Otra</label>
		<input type='text' name='os_tipo_falla_txt' disabled='disabled' class='inputtext' size='30' /></fieldset><br />");
	$form->Listado('Técnico asignado','os_tecnico','tb_funcionario',
	array('dc_funcionario','CONCAT_WS(" ",dg_nombres,dg_ap_paterno,dg_ap_materno)'));
	$form->Listado('Estado','os_estado','tb_estado_orden_servicio',array('dc_estado','dg_estado'),1);
	$form->EndSection();
	$form->Section();
	echo('<fieldset>');
	$form->Radiobox('Solucionado','os_solucionado',array('SI','NO'),1," ");
	echo('<br /><div id="solucion_after_date">');
	$form->Date('Fecha comprometida','os_fecha',1,0);
	
	//Hora de compromiso
	echo("<label>Hora</label>
	<select name='os_hora' class='inputtext' style='width:50px;'>");
	foreach(range(1,12) as $h){
		$h = str_pad($h,2,'0',STR_PAD_LEFT);
		echo("<option value='{$h}'>{$h}</option>");
	}
	echo("</select> : <select name='os_minuto' class='inputtext' style='width:50px;'>");
	foreach(range(0,45,15) as $m){
		$m = str_pad($m,2,'0',STR_PAD_LEFT);
		echo("<option value='{$m}'>{$m}</option>");
	}
	echo("</select> - 
	<select name='os_meridiano' class='inputtext' style='width:70px;'>
	<option value='0'>AM</option>
	<option value='12'>PM</option>
	</select>");
	
	echo('</div><div id="solucion_after_text" class="hidden">');
	$form->Textarea('Solución [*]','os_solucion');
	echo('</div></fieldset>');
	$form->EndSection();
	$form->End('Crear','addbtn');
	
?>
</div></div>
<script type="text/javascript">
$('input[name=os_solucionado]').click(function(){
	if($(this).val() == 0){
		$('#solucion_after_date').hide().find('#os_fecha').attr('required',false);
		$('#solucion_after_text').show().find('#os_solucion').attr('required',true);
	}else{
		$('#solucion_after_date').show().find('#os_fecha').attr('required',true);
		$('#solucion_after_text').hide().find('#os_solucion').attr('required',false);
	}
		
});
$('input[name=os_other_falla]').click(function(){
	if($(this).attr('checked')){
		$(this).parent().next().attr('disabled',false).attr('required',true);
		$('#os_tipo_falla').attr('required',false);
	}else{
		$(this).parent().next().attr('disabled',true).attr('required',false);
		$('#os_tipo_falla').attr('required',true);
	}
});
$('#os_number_submit').click(function(){
	v = parseInt($('#os_number_search').val());
	if(v){
		loadOverlay('sites/servicios/proc/search_orden_servicio.php?number='+v);
	}else
		$('#os_number_search').val('')
});
</script>