<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$os_editable = '';
if($empresa_conf['dm_editar_os_cerradas'] == '0'){
	$os_editable = "AND dm_estado = '0'";
}

$data = $db->select('tb_orden_servicio',
'dq_orden_servicio,dc_cliente,dg_solicitante,dg_requerimiento,dc_tipo_falla,dg_tipo_falla_manual,dc_tecnico,dc_estado,dm_estado,DATE_FORMAT(df_compromiso,"%d/%m/%Y %I:%i %p") df_compromiso,dg_solucion,dm_facturable,dg_glosa_facturacion,dc_sucursal',
"dc_orden_servicio = {$_POST['id']} {$os_editable}");

if(!count($data)){
	$error_man->showWarning('No se encontró la orden de servicio indicada o está se encuentra en un estado cerrado');
	exit;
}
$data = $data[0];

echo("<div class='secc_bar'>Editar Orden de servicio {$data['dq_orden_servicio']}</div>
<div class='panes'>");

	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start('sites/servicios/proc/editar_orden_servicio.php','ed_orden_servicio');
    $form->Header('<b>Indique los datos actualizados de la orden de servicio</b><br />(Los campos marcados con [*] son obligatorios)');
	$form->Section();
	$form->Listado('Cliente','os_cliente','tb_cliente',array('dc_cliente','dg_razon'),1,$data['dc_cliente']);
	$form->Text('Solicitante','os_solicitante',1,255,$data['dg_solicitante']);
	$form->Listado('Sucursal','os_sucursal','tb_cliente_sucursal',array('dc_sucursal','dg_sucursal'),1,$data['dc_sucursal'],"dc_cliente={$data['dc_cliente']}");
    
	$form->Textarea('Requerimiento','os_request',1,$data['dg_requerimiento']);
	
	echo('<fieldset>');
	$form->Radiobox('Facturable','os_facturable',array('SI','NO'),($data['dm_facturable']*-1)+1,'');
	$form->Text('Glosa Facturación','os_glosa_factura',0,255,$data['dg_glosa_facturacion']);
	echo('</fieldset>');
	
	$form->EndSection();
	$form->Section();
	echo("<fieldset>");
	if($data['dg_tipo_falla_manual'])
		$select = array('checked="checked"',"value='{$data['dg_tipo_falla_manual']}'");
	else
		$select = array('','disabled="disabled"');
	$form->Listado('Tipo de falla','os_tipo_falla','tb_tipo_falla_os',array('dc_tipo_falla','dg_tipo_falla'),0,$data['dc_tipo_falla']);
	echo("<label><input type='checkbox' name='os_other_falla' {$select[0]}/>Otra</label>
		<input type='text' name='os_tipo_falla_txt' {$select[1]} class='inputtext' size='30' /></fieldset><br />");
	$form->Listado('Técnico asignado','os_tecnico','tb_funcionario',
	array('dc_funcionario','CONCAT_WS(" ",dg_nombres,dg_ap_paterno,dg_ap_materno)'),0,$data['dc_tecnico']);
	$form->EndSection();
	$form->Section();
	echo('<fieldset>');
	if($data['dm_estado'] == '1'){
		$class_1 = 'class="hidden"';
		$class_2 = '';
	}else{
		$class_1 = '';
		$class_2 = 'class="hidden"';
	}
	$form->Radiobox('Solucionado','os_solucionado',array('SI','NO'),($data['dm_estado']*-1)+1," ");
	echo("<br /><div id='solucion_after_date' {$class_1}>
	Fecha comprometida:<br /><b>{$data['df_compromiso']}</b>");
	
	echo("</div><div id='solucion_after_text' {$class_2}>");
	$form->Textarea('Solución [*]','os_solucion',0,$data['dg_solucion'],20,3);
	echo('</div></fieldset>');
	$form->EndSection();
	$form->Hidden('os_id',$_POST['id']);
	$form->End('Editar','editbtn');

?>
<script type="text/javascript">
$('input[name=os_solucionado]').click(function(){
	if($(this).val() == 0){
		$('#solucion_after_date').hide().find('#os_fecha').attr('required',false);
		$('#solucion_after_text').show().find('#os_solucion').attr('required',true);
	}else{
		$('#solucion_after_date').show().find('#os_fecha').attr('required',true);
		$('#solucion_after_text').hide().find('#os_solucion').attr('required',false);
	}
		
});
$('input[name=os_other_falla]').click(function(){
	if($(this).attr('checked')){
		$(this).parent().next().attr('disabled',false).attr('required',true);
		$('#os_tipo_falla').attr('required',false);
	}else{
		$(this).parent().next().attr('disabled',true).attr('required',false);
		$('#os_tipo_falla').attr('required',true);
	}
});
$('#os_cliente').change(function(){
	var cli = $(this).val();
	$('#os_sucursal').html('').attr('disabled',true);
	if(cli != 0){
		loadFile('sites/servicios/proc/get_sucursales_cliente.php','#os_sucursal','id='+cli,function(){
			$('#os_sucursal').attr('disabled',false);
		});
	}
});
</script>