<?php

require_once(__DIR__."/CRUDOrdenServicioFactory.php");

/**
 * 
 * Description of EditarOrdenServicioFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 09-07-2013
 */
class EditarOrdenServicioFactory extends CRUDOrdenServicioFactory {

  protected $title = "Editar Orden de Servicio";
  private $orden_servicio;

  public function indexAction() {
    $this->orden_servicio = $this->getOSData(self::getRequest()->id);
    $db = $this->getConnection();

    if ($this->orden_servicio === false || $this->orden_servicio->dc_empresa != $this->getEmpresa()) {
      $this->getErrorMan()->showWarning("No se ha encontrado la orden de servicio especificada, compruebe los datos de entrada y vuelva a intentarlo");
    }
    
    $nota_venta = $this->getNVData();
    $ticket = $this->getTicketData();
    $ejecutores = $this->getEjecutores();

    $form = $this->getFormView($this->getTemplateURL('form'), array(
        'orden_servicio' => $this->orden_servicio,
        'nota_venta' => $nota_venta,
        'ticket' => $ticket,
        'ejecutores' => $ejecutores
    ), true);

    echo $this->getFullView($form, null, self::STRING_TEMPLATE);
  }
  
  public function procesarFormularioAction(){
    $request = self::getRequest();
    $this->orden_servicio = $this->getOSData($request->os_id);
    
    if($this->orden_servicio === false || $this->orden_servicio->dc_empresa != $this->getEmpresa()){
      $this->getErrorMan()->showWarning("La orden de servicio que intenta actualizar no ha sido encontrada, compruebe los datos de entrada y vuelva a intentarlo");
      exit;
    }
    
    $this->validaTecnicos();
    $this->validaDisponibilidad();
    
    $this->getConnection()->start_transaction();
      $this->actualizaOrden();
      $this->actualizaEjecutores();
      $this->actualizaTicket();
    $this->getConnection()->commit();
    
    $this->getErrorMan()->showConfirm("Se han actualizado los datos de la orden de servicio correctamente");
    
  }

  private function getOSData($dc_orden_servicio) {
    return $this->getConnection()->getRowById('tb_orden_servicio', $dc_orden_servicio, 'dc_orden_servicio');
  }
  
  private function getNVData(){
    return 
      $this->orden_servicio->dc_nota_venta
            ?$this->getConnection()->getRowById('tb_nota_venta', $this->orden_servicio->dc_nota_venta, 'dc_nota_venta')
            :false;
  }
  
  private function getTicketData(){
    $db = $this->getConnection();
    $has = $db->getRowById('tb_ticket_orden_servicio', $this->orden_servicio->dc_orden_servicio, 'dc_orden_servicio');
    if($has){
      return $db->getRowById('tb_ticket_servicio', $has->dc_ticket, 'dc_ticket');
    }else{
      return false;
    }
  }
  
  private function getEjecutores(){
    $db = $this->getConnection();
    
    $query = $db->prepare($db->select('tb_tecnico_apoyo', 'dc_tecnico', 'dc_orden_servicio = ?'));
    $query->bindValue(1, $this->orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
    $db->stExec($query);
    
    $data = array();
    while($d = $query->fetch(PDO::FETCH_OBJ)):
      $data[] = $d->dc_tecnico;
    endwhile;
    
    return $data;
  }
  
  private function validaTecnicos(){
    if(!isset(self::getRequest()->os_tecnico_apoyo)){
      $this->getErrorMan()->showWarning("Debe seleccionar al menos un técnico ejecutor para realizar las labores de la orden de servicio");
      exit;
    }
  }
  
  private function validaDisponibilidad(){
    $disponibilidad = $this->getDisponibilidadTecnica();
    if(count($disponibilidad)){
      $this->getErrorMan()->showWarning("Los siguientes técnicos no están disponibles para realizar servicios en las fechas de la orden.");
      echo $this->getView($this->getTemplateURL('no_disponibilidad'), array(
          'tecnicos' => $this->getTecnicosNames($disponibilidad)
      ));
      exit;
    }
  }
  
  private function getDisponibilidadTecnica(){
    $db = $this->getConnection();
    $r = self::getRequest();
    
    $questions = substr(str_repeat(',?', count($r->os_tecnico_apoyo)),1);
    
    $Ai = $this->getDatetime('os_fecha');
    $Af = $this->getDatetime('os_fecha_final');
    
   $query = $db->prepare($db->select('tb_tecnico_apoyo ta '
            . 'LEFT JOIN tb_orden_servicio os ON os.dc_orden_servicio=ta.dc_orden_servicio', 
            'distinct ta.dc_tecnico', "
        ta.dc_orden_servicio <> ? AND 
        (
          (TIMESTAMP(ta.df_inicio) < TIMESTAMP(?) AND TIMESTAMP(ta.df_inicio) > TIMESTAMP(?)) OR
          (TIMESTAMP(ta.df_inicio) > TIMESTAMP(?) AND TIMESTAMP(ta.df_fin) < TIMESTAMP(?)) OR
          (TIMESTAMP(?) < TIMESTAMP(ta.df_inicio) AND TIMESTAMP(?) > TIMESTAMP(ta.df_fin))
        )
        AND os.dm_estado=0 AND 
        ta.dc_tecnico IN ({$questions})
    "));
    $query->bindValue(1, $this->orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
    $query->bindValue(2, $Ai, PDO::PARAM_STR);
    $query->bindValue(3, $Ai, PDO::PARAM_STR);
    $query->bindValue(4, $Af, PDO::PARAM_STR);
    $query->bindValue(5, $Af, PDO::PARAM_STR);
    $query->bindValue(6, $Ai, PDO::PARAM_STR);
    $query->bindValue(7, $Af, PDO::PARAM_STR);
    
    foreach($r->os_tecnico_apoyo as $i => $dc_tecnico){
      $query->bindValue(8+$i, $dc_tecnico, PDO::PARAM_INT);
    }
    
    $db->stExec($query);
    
    return $query->fetchAll(PDO::FETCH_OBJ);
    
  }
  
  private function getTecnicosNames($disponibilidad){
    $nombres = array();
    foreach($disponibilidad as $tecnico):
      $nombres[] = $this->getConnection()->getRowById('tb_funcionario', $tecnico->dc_tecnico, 'dc_funcionario');
    endforeach;
    return $nombres;
  }
  
  private function actualizaOrden(){
    $db = $this->getConnection();
    $request = self::getRequest();
    
    $update = $db->prepare($db->update('tb_orden_servicio', array(
        'dc_cliente' => '?',
        'dg_solicitante' => '?',
        'dc_sucursal' => '?',
        'dc_area_servicio' => '?',
        'dg_requerimiento' => '?',
        'dc_tecnico' => '?',
        'dm_facturable' => '?',
        'dg_glosa_facturacion' => '?',
        'dc_nota_venta' => '?',
        'dg_tipo_falla_manual' => '?',
        'dc_tipo_falla' => '?',
        'df_compromiso' => '?',
        'df_fecha_final' => '?'
    ), 'dc_orden_servicio = ?'));
    $update->bindValue(1, $request->os_cliente, PDO::PARAM_INT);
    $update->bindValue(2, $request->os_solicitante, PDO::PARAM_STR);
    $update->bindValue(3, $request->os_sucursal, PDO::PARAM_INT);
    $update->bindValue(4, $request->os_area, PDO::PARAM_INT);
    $update->bindValue(5, $request->os_request, PDO::PARAM_INT);
    $update->bindValue(6, $request->os_tecnico, PDO::PARAM_INT);
    $update->bindValue(7, ($request->os_facturable*-1)+1, PDO::PARAM_STR);
    $update->bindValue(8, $request->os_glosa_factura, PDO::PARAM_STR);
    $update->bindValue(9, $request->dc_nota_venta, PDO::PARAM_INT);
    $update->bindValue(10, isset($request->os_other_falla)?$request->os_tipo_falla_txt:NULL, PDO::PARAM_STR);
    $update->bindValue(11, !isset($request->os_other_falla)?$request->os_tipo_falla:NULL, PDO::PARAM_INT);
    $update->bindValue(12, $this->getDatetime('os_fecha'), PDO::PARAM_STR);
    $update->bindValue(13, $this->getDatetime('os_fecha_final'), PDO::PARAM_STR);
    $update->bindValue(14, $this->orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
    
    $db->stExec($update);
    
  }
  
  private function actualizaEjecutores(){
    $r = self::getRequest();
    $db = $this->getConnection();
    
    $db->doExec("DELETE FROM tb_tecnico_apoyo WHERE dc_orden_servicio = {$this->orden_servicio->dc_orden_servicio}");
    
    $inserta = $db->prepare($db->insert('tb_tecnico_apoyo', array(
        'dc_orden_servicio' => '?',
        'dc_tecnico' => '?',
        'df_inicio' => '?',
        'df_fin' => '?'
    )));
    $inserta->bindValue(1, $this->orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
    $inserta->bindParam(2, $dc_tecnico, PDO::PARAM_INT);
    $inserta->bindValue(3, $this->getDatetime('os_fecha'), PDO::PARAM_STR);
    $inserta->bindValue(4, $this->getDatetime('os_fecha_final'), PDO::PARAM_STR);
    
    foreach($r->os_tecnico_apoyo as $dc_tecnico){
      $db->stExec($inserta);
    }
    
  }
  
  private function actualizaTicket(){
    $r = self::getRequest();
    $db = $this->getConnection();
    
    $db->doExec("DELETE FROM tb_ticket_orden_servicio WHERE dc_orden_servicio = {$this->orden_servicio->dc_orden_servicio}");
    
    if(!$r->dc_ticket_servicio){
      return;
    }
    
    $inserta = $db->prepare($db->insert('tb_ticket_orden_servicio', array(
        'dc_orden_servicio' => '?',
        'dc_ticket' => '?'
    )));
    $inserta->bindValue(1, $this->orden_servicio->dc_orden_servicio, PDO::PARAM_INT);
    $inserta->bindValue(2, $r->dc_ticket_servicio, PDO::PARAM_INT);
    
    $db->stExec($inserta);
    
  }

}

?>
