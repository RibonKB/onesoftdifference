<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$status = $db->select('tb_orden_servicio','dc_estado,dm_estado,dm_facturable,dg_glosa_facturacion,df_fecha_emision',"dc_orden_servicio={$_POST['id']}");

if(!count($status)){
	$error_man->showWarning('No se encontró la orden de servicio indicada');
	exit();
}
$solucionado = $status[0]['dm_estado'];
$facturable = $status[0]['dm_facturable'];
$glosa = $status[0]['dg_glosa_facturacion'];
$creacion = $status[0]['df_fecha_emision'];
$status = $status[0]['dc_estado'];

$data = $db->select("(SELECT * FROM tb_orden_servicio_avance WHERE dc_orden_servicio={$_POST['id']}) av
LEFT JOIN tb_estado_orden_servicio es ON es.dc_estado = av.dc_estado_orden",
'dg_gestion,es.dg_estado,dm_estado_orden,dc_avance,
DATE_FORMAT(av.df_creacion,"%d/%m/%Y %H:%i") as df_creacion,
DATE_FORMAT(df_proxima_actividad,"%d/%m/%Y") as df_estimada,
DATE_FORMAT(df_cierre_actividad,"%d/%m/%Y") as df_cierre',"",
array('order_by'=>'av.df_creacion DESC','limit'=>2));

echo("<div class='panes' style='width:910px;'>");
if(count($data)){

	echo("<table class='tab' width='100%'>
	<caption>Últimas actividades realizadas sobre la orden de servicio</caption>
	<thead><tr>
		<th>Gestión</th>
		<th width='120'>Estado Orden de servicio</th>
		<th width='90'>Fecha creación</th>
		<th width='90'>Cierre estimado</th>
		<th width='90'>Fecha cierre</th>
	</thead><tbody>");
		foreach($data as $d){
		$estado = 'Abierta';
		if($d['dm_estado_orden'] == '1')
			$estado = 'Cerrada';
			echo("<tr>
				<td>{$d['dg_gestion']}</td>
				<td>{$estado}</td>
				<td>{$d['df_creacion']}</td>
				<td>{$d['df_estimada']}</td>
				<td>{$d['df_cierre']}</td>
			</tr>");
		}
	echo("</tbody></table><hr />");

}

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start('sites/servicios/proc/crear_gestion_orden_servicio.php','cr_gestion');
$form->Header('Nueva actividad');
$form->Section();
$form->Listado('Estado Orden de servicio','gestion_estado','tb_estado_orden_servicio',array('dc_estado','dg_estado'),1,$status);
$form->Date('Fecha próxima gestión','gestion_fecha');

//Hora de la gestión
echo("<label>Hora</label>
<select name='gestion_hora' class='inputtext' style='width:50px;'>");
foreach(range(1,12) as $h){
	$h = str_pad($h,2,'0',STR_PAD_LEFT);
	echo("<option value='{$h}'>{$h}</option>");
}
echo("</select> : <select name='gestion_minuto' class='inputtext' style='width:50px;'>");
foreach(range(0,45,15) as $m){
	$m = str_pad($m,2,'0',STR_PAD_LEFT);
	echo("<option value='{$m}'>{$m}</option>");
}
echo("</select> - 
<select name='gestion_meridiano' class='inputtext' style='width:70px;'>
<option value='0'>AM</option>
<option value='12'>PM</option>
</select>");

$form->EndSection();
$form->Section();

$form->Textarea('Gestión','gestion_gestion',1);

$form->EndSection();
$form->Section();

$form->Radiobox("Solucionado",'gestion_solucionado',array('SI','NO'),1," ");

//Inicio rangos fecha solución
echo('<br /><fieldset id="data_solucion" class="hidden">');

$form->Text('Aceptante','gestion_aceptante');

$form->Date('Fecha Inicio [*]','gestion_solini',0,$creacion);
echo("<label>Hora</label>
<select name='gestion_horaini' class='inputtext' style='width:50px;'>");
foreach(range(1,12) as $h){
	$h = str_pad($h,2,'0',STR_PAD_LEFT);
	echo("<option value='{$h}'>{$h}</option>");
}
echo("</select> : <select name='gestion_minutoini' class='inputtext' style='width:50px;'>");
foreach(range(0,45,15) as $m){
	$m = str_pad($m,2,'0',STR_PAD_LEFT);
	echo("<option value='{$m}'>{$m}</option>");
}
echo("</select> - 
<select name='gestion_meridianoini' class='inputtext' style='width:70px;'>
<option value='0'>AM</option>
<option value='12'>PM</option>
</select><br /><br />");

$form->Date('Fecha Término [*]','gestion_solfin',0,0);
echo("<label>Hora</label>
<select name='gestion_horafin' class='inputtext' style='width:50px;'>");
foreach(range(1,12) as $h){
	$h = str_pad($h,2,'0',STR_PAD_LEFT);
	echo("<option value='{$h}'>{$h}</option>");
}
echo("</select> : <select name='gestion_minutofin' class='inputtext' style='width:50px;'>");
foreach(range(0,45,15) as $m){
	$m = str_pad($m,2,'0',STR_PAD_LEFT);
	echo("<option value='{$m}'>{$m}</option>");
}
echo("</select> - 
<select name='gestion_meridianofin' class='inputtext' style='width:70px;'>
<option value='0'>AM</option>
<option value='12'>PM</option>
</select>");

echo('</fieldset>');
//Fin rangos fecha solución

$form->EndSection();
$form->Hidden('os_id',$_POST['id']);
if(count($data))
	$form->Hidden('gestion_avance',$data[0]['dc_avance']);
$form->Hidden('gestion_cambio','');
$form->End('Crear','addbtn');
?>
</div>
<script type="text/javascript">
$('#gestion_estado').change(function(){
	$('input[name=gestion_cambio]').val($(this).val());
});
$("#gestion_fecha,#gestion_solini,#gestion_solfin").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});
$('input[name=gestion_solucionado]').click(function(){
	if($(this).val() == 0){
		$('#data_solucion').show();
		$('#gestion_solfin,#gestion_solini').attr('required',true);
	}else{
		$('#data_solucion').hide();
		$('#gestion_solfin,#gestion_solini').attr('required',false);
	}
		
});
</script>