<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/Factory.class.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Creación Ordenes de Servicio</div>
<div id="main_cont">
  <div id='options_menu'>
    <button class="imgbtn" id="disponibilidad_tecnica_btn" style="background-image: url(images/setupbtn.png)">Ver Disponibilidad Técnica</button>
  </div>
  <br /><br />
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start('sites/servicios/proc/crear_orden_servicio.php','cr_orden_servicio');
	$form->Header('<b>Indique los datos de ingreso de la nueva orden de servicio</b><br />(Los campos marcados con [*] son obligatorios)');
	$form->Section();
	$form->Listado('Cliente','os_cliente','tb_cliente',array('dc_cliente','dg_razon'),1);
	$form->Text('Solicitante','os_solicitante',1);
	$form->Select('Sucursal','os_sucursal',array(),1);
	$form->Text('Nota de venta','os_nota_venta');
	$form->Hidden('dc_nota_venta',0);
    $form->Text('Ticket Servicio', 'os_ticket_servicio');
    $form->Hidden('dc_ticket_servicio', 0);
	
	echo('<br /><fieldset>');
	$form->Radiobox('Facturable','os_facturable',array('SI','NO'),1,'');
	$form->Text('Glosa Facturación','os_glosa_factura');
	echo('</fieldset>');
	
	$form->EndSection();
	$form->Section();
	echo("<fieldset>");
	$form->Listado('Área de servicio','os_area','tb_area_servicio',array('dc_area_servicio','dg_area_servicio'),1);
	$form->Listado('Tipo de falla','os_tipo_falla','tb_tipo_falla_os',array('dc_tipo_falla','dg_tipo_falla'));
	echo("<label><input type='checkbox' name='os_other_falla'/>Otra</label>
		<input type='text' name='os_tipo_falla_txt' disabled='disabled' class='inputtext' size='30' /></fieldset><br />");
	
	$form->Textarea('Requerimiento','os_request',1);
	$form->Listado('Estado inicial','os_estado','tb_estado_orden_servicio',array('dc_estado','dg_estado'),1);
	
	$form->EndSection();
	$form->Section();
	echo('<fieldset>');
	$form->Radiobox('Solucionado','os_solucionado',array('SI','NO'),1," ");
	echo('<br /><div id="solucion_after_date">');
	$form->DateTime('Fecha inicio','os_fecha',1,0);
    echo '<br />';
    $form->DateTime('Fecha estimada de término','os_fecha_final',1,0);
	
	echo('</div><div id="solucion_after_text" class="hidden">');
	$form->Textarea('Solución [*]','os_solucion');
	echo('</div></fieldset>');
	
	$form->Listado('Supervisor','os_tecnico','tb_funcionario',array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
    $form->ListadoMultiple('Ejecutores', 'os_tecnico_apoyo', 'tb_funcionario', array('dc_funcionario','dg_nombres','dg_ap_paterno','dg_ap_materno'));
	
	$form->EndSection();
	$form->End('Crear','addbtn');
	
?>
</div></div>
<script type="text/javascript" src="jscripts/sites/servicios/cr_orden_servicio.js?v=0_3"></script>
<script type="text/javascript">
  $('#disponibilidad_tecnica_btn').click(function(){
    var data = $('#cr_orden_servicio :input').serialize();
    pymerp.loadOverlay('<?php echo Factory::buildUrl('CrearOrdenServicio', 'servicios', 'verDisponibilidad') ?>',data);
  });
</script>