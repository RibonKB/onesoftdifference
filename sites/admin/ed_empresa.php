<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_empresa e
LEFT JOIN tb_comuna c ON c.dc_comuna = e.dc_comuna","e.*,c.dc_region","dc_empresa={$_POST['id']}");

if(!count($data)){
	$error_man->showWarning("Empresa no encontrada, verifique el origen de los datos");
	exit();
}

$data = $data[0];

echo("<div class='secc_bar'>Editar empresa</div><div class='panes'>");

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$form->Start("sites/admin/proc/editar_empresa.php","ed_empresa");
$form->Header("Editando empresa <b>{$data['dg_razon']}</b>");
$form->Section();
$form->Text("RUT",'em_rut',1,12,$data['dg_rut']);
$form->Text("Razón social",'em_razon',1,255,$data['dg_razon']);
$form->Textarea("Giro",'em_giro',1,$data['dg_razon']);
$form->EndSection();
$form->Section();
$form->Text("Dirección",'em_direccion',1,255,$data['dg_direccion']);

$regiones = $db->select("tb_region");
echo("<label>Region<br />
	<select onchange='showComunas(\"ed_comuna\",this)' id='em_region' class='inputtext'>
	<option></option>");

	foreach($regiones as $r){
		echo("<option value='{$r['dc_region']}'>{$r['dg_region']}</option>");
	}

echo("</select></label><br />
<label>Comuna[*]<br />
<select name='em_comuna' id='em_comuna' class='inputtext' required='required'>
	<option></option>
</select>
</label>
<span id='comuna_{$_POST['indice']}_comloader'></span>
<br />");

$form->Textarea("Teléfono",'em_fono',1,$data['dg_fono']);
$form->EndSection();
$form->Hidden('em_id',$_POST['id']);
$form->End("Editar","editbtn");

echo("</div>");

?>
<script type="text/javascript">
$("#em_region").val(<?=$data['dc_region'] ?>);
showComunas("em_comuna",document.getElementById("em_region"),<?=$data['dc_comuna'] ?>);
</script>