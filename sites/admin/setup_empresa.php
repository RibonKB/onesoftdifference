<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("tb_empresa_configuracion","*","dc_empresa={$_POST['id']}");

if(!count($data)){
	$error_man->showWarning("No se ha encontrado los datos necesarios para cambiar la configuración");
	exit();
}

$data = $data[0];

require_once("../../inc/form-class.php");
$form = new Form($empresa);

$cant = intval(count($data)/3);
unset($data['dc_empresa']);

$form->Start("sites/admin/proc/save_setup_empresa.php","setup_empresa");
$form->Header("Cambiar configuración de la empresa");
$form->Section();
$cont = 0;

foreach($data as $opt => $val){
	$name = ucwords(str_replace("_"," ",substr($opt,3)));
	$form->Textarea($name,$opt,1,$val,32,3);
	$cont++;
	if($cont%$cant==0){
		$form->EndSection();
		$form->Section();
	}
}

$form->EndSection();
$form->Hidden("em_id",$_POST['id']);
$form->End("Guardar cambios","editbtn");
?>