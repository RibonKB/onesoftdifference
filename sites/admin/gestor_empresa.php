<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

echo("<div id='secc_bar'>Mantenedor de empresas</div>
<div id='main_cont'>
<div class='panes'>
<button type='button' class='addbtn'>Crear nueva empresa</button><br /><br />");

$empresas = $db->select('tb_empresa','dc_empresa,dg_rut,dg_razon');

echo("<table class='tab' width='100%'>
<caption>Empresas actuales</caption>
<thead>
<tr>
	<th width='100'>RUT</th>
	<th>Razón</th>
	<th>Opciones</th>
</tr>
</thead>
<tbody id='items'>");

foreach($empresas as $e){
	echo("<tr>
		<td>{$e['dg_rut']}</td>
		<td>{$e['dg_razon']}</td>
		<td>
			<a href='sites/admin/ed_empresa.php?id={$e['dc_empresa']}' class='loadOnOverlay left'>
				<img src='images/editbtn.png' alt='Editar' title='Editar' />
			</a>
			<a href='sites/admin/del_empresa.php?id={$e['dc_empresa']}' class='loadOnOverlay left'>
				<img src='images/delbtn.png' alt='Editar' title='Editar' />
			</a>
			<a href='sites/admin/setup_empresa.php?id={$e['dc_empresa']}' class='loadOnOverlay left'>
				<img src='images/setupbtn.png' alt='Opciones' title='Opciones' />
			</a>
		</td>
	</tr>");
}

echo("</tbody></table>");

echo("</div></div>");

?>