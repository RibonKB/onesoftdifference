<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//var_dump($_SERVER['HTTP_X_REQUESTED_WITH']);

/*
* astk main db	-> asterisk
* astk calls db	-> asteriskcdrdb
*
*$internal = $db->select("(SELECT * FROM asteriskcdrdb.cdr WHERE dcontext='from-internal' AND MONTH(calldate) = MONTH(NOW())) cdr
JOIN asterisk.users u ON u.extension = cdr.src
LEFT JOIN tb_contacto_cliente c ON c.dg_fono = cdr.dst
LEFT JOIN tb_cliente cl ON cl.dc_cliente = c.dc_cliente","cl.dg_razon",'',array('limit'=>100));
*/
?>
<div id="secc_bar">Informe llamadas Asterisk</div>
<input type="text" class="hidden" />
<div id="main_cont" class="center"><br /><br />
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Start("sites/admin/proc/src_asterisk_phone.php","src_asterisk");
	$form->Header("<strong>Indique los filtros que se usarán para las llamadas</strong>");

	echo('<table class="tab" style="text-align:left;" id="form_container" width="100%"><tr><td width="155">Fecha llamada</td><td>');
	$form->Date('Desde','ask_fecha_desde',1,"01/".date("m/Y"));
	echo('</td><td>');
	$form->Date('Hasta','ask_fecha_hasta',1,0);
	echo('</td></tr><tr><td>Origen</td><td>');
	$form->ListadoMultiple('','ask_source','tb_funcionario',array('dg_anexo','dg_nombres','dg_ap_paterno','dg_ap_materno'),'',"dc_empresa={$empresa} AND dg_anexo <> 0");
	echo('</td><td>');
	$form->Radiobox('Destino de llamada','ask_dest',array('Externa','Interna'),0);
	echo('</td></tr><tr><td>Disposición de la llamada</td><td>');
	$form->Multiselect('','ask_status',array('ANSWERED' => 'Contestada','NO ANSWER' => 'Sin Respuesta','FAILED' => 'Fallada','BUSY' => 'Ocupada'));
	echo('</td><td>');
	$form->Select('Llamadas por página','ask_cant',array(10=>10,20=>20,30=>30,40=>40,50=>50,100=>100),1,20);
	echo('</td></tr></table>');
	$form->End('Ejecutar consulta','searchbtn');
?>
</div>
</div>
<script type="text/javascript">
$('#ask_source,#ask_status').multiSelect({
	selectAll: true,
	selectAllText: "Seleccionar todos",
	noneSelected: "---",
	oneOrMoreSelected: "% seleccionado(s)"
});
</script>