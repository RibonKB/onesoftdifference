<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$_POST['ask_fecha_desde'] = $db->sqlDate($_POST['ask_fecha_desde']);
$_POST['ask_fecha_hasta'] = $db->sqlDate($_POST['ask_fecha_hasta']." 23:59");

$conditions = "(calldate BETWEEN {$_POST['ask_fecha_desde']} AND {$_POST['ask_fecha_hasta']})";

if(isset($_POST['ask_source'])){
	$source = implode(',',$_POST['ask_source']);
	$conditions .= " AND src IN ({$source})";
}else{
	$conditions .= " AND dcontext='from-internal'";
}

if(isset($_POST['ask_status'])){
	$status = implode("','",$_POST['ask_status']);
	$conditions .= " AND disposition IN ('{$status}')";
}

if($_POST['ask_dest'] == 0){
	$ln = '>';
	$join = "LEFT JOIN tb_contacto_cliente c ON (cdr.dst = c.dg_fono OR cdr.dst = CONCAT('9',c.dg_fono) OR cdr.dst = CONCAT('92',c.dg_fono) OR cdr.dst = CONCAT('922',c.dg_fono) OR cdr.dst = CONCAT('99',c.dg_movil) OR cdr.dst = CONCAT('909',c.dg_movil))
	LEFT JOIN tb_cliente cl ON cl.dc_cliente = c.dc_cliente";
	$field = 'cl.dg_razon';
}else{
	$ln = '<=';
	$join = "JOIN tb_funcionario dst ON dst.dg_anexo = cdr.dst";
	$field = 'CONCAT_WS(" ",dst.dg_nombres, dst.dg_ap_paterno)';
}

$data = $db->select("(SELECT * FROM cdr WHERE {$conditions} AND LENGTH(dst) {$ln} 5) cdr
JOIN tb_funcionario u ON u.dg_anexo = cdr.src AND u.dc_empresa={$empresa}
{$join}",
"{$field} AS dst_name,cdr.src,cdr.dst,COUNT(cdr.dst) AS cantidad_llamados,SUM(cdr.duration) AS duration,
AVG(cdr.duration) AS time_avg,CONCAT_WS(' ',u.dg_nombres,u.dg_ap_paterno) as name,cdr.disposition",
'',array('group_by'=>'cdr.src,cdr.dst'));

echo('<table width="100%" class="tab" id="res_list">
<thead><tr>
	<th id="name">Origen</th>
	<th id="src">Teléfono Origen</th>
	<th id="dst_name">Destino</th>
	<th id="dst">Teléfono Destino</th>
	<th id="cantidad_llamados">Veces llamado</th>
	<th id="duration">Duración</th>
	<th id="time_avg">Tiempo promedio</th>
	<th id="disposition">Disposición</th>
</tr></thead><tbody id="body"></tbody>
</table>');

?>
<script type="text/javascript" src="jscripts/admin/asterisk_phone.js?v0_1_5"></script>
<script type="text/javascript">
	js_data.data = <?=json_encode($data) ?>;
	js_data.rpt = js_data.data.length;
	js_data.adjust = <?php echo json_encode($_POST['ask_cant']) ?>;
	js_data.fromDate = <?=$_POST['ask_fecha_desde'] ?>;
	js_data.toDate = <?=$_POST['ask_fecha_hasta'] ?>;
	js_data.type = <?=$_POST['ask_dest'] ?>;
	<?php if(isset($_POST['ask_status'])): ?>
	js_data.disposition = "<?=$status ?>";
	<?php else: ?>
	js_data.disposition = 'ALL';
	<?php endif; ?>
	js_data.init();
</script>
