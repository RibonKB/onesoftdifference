<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$id = $_POST['em_id'];
unset($_POST['asinc'],$_POST['em_id']);

$db->update("tb_empresa_configuracion",$_POST,"dc_empresa={$id}");

$error_man->showConfirm("Se han modificado las configuraciones de la empresa correctamente");
?>