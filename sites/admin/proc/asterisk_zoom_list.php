<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(isset($_POST['src'])){
	$conditions = "src='{$_POST['src']}'";
}else if(isset($_POST['dst'])){
	$conditions = "dst='{$_POST['dst']}'";
}else{
	die();
}

if($_POST['disp'] != 'ALL'){
	$conditions .= " AND disposition IN ('{$_POST['disp']}')";
}

if($_POST['type'] == 0){
	$ln = '>';
	$join = "LEFT JOIN tb_contacto_cliente c ON c.dg_fono = cdr.dst
	LEFT JOIN tb_cliente cl ON cl.dc_cliente = c.dc_cliente";
	$field = 'cl.dg_razon';
}else{
	$ln = '<=';
	$join = "JOIN users dst ON dst.extension = cdr.dst";
	$field = 'dst.name';
}

$data = $db->select("(SELECT * FROM cdr WHERE (calldate BETWEEN '{$_POST['from']}' AND '{$_POST['to']}') AND {$conditions} AND LENGTH(dst) {$ln} 5) cdr
JOIN users u ON u.extension = cdr.src
{$join}",
"{$field} AS dst_name,cdr.src,cdr.dst,1 AS cantidad_llamados,duration,duration AS time_avg,u.name,cdr.disposition");

echo('<div id="zoom_data"></div>');

?>
<script type="text/javascript" src="jscripts/admin/asterisk_zoom_list.js"></script>
<script type="text/javascript">
	js_data.zoomData = <?=json_encode($data) ?>;
	js_data.initZoom();
</script>