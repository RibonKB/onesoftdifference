<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Consulta de proveedores</div><div id="main_cont"><br /><br /><div class="panes">
<?php
	//Obtiene los tipos de dirección

	include("../../inc/form-class.php");
	$form = new Form($empresa);
	$form->Start("sites/proveedores/proc/src_proveedor.php","src_proveedor");
	echo("<div class='center'>");
	$form->Header("<strong>Ingrese el RUT del proveedor para mostrar su información</strong>");
	$form->Text("RUT","prov_rut",1,20,'',"inputrut");
	echo("</div>");
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
$("#prov_rut").autocomplete('sites/proc/autocompleter/proveedor.php',
{
formatItem: function(row){
	return row[1]+" ("+row[0]+") "+row[2];
},
minChars: 2,
width:300
}
);
</script>