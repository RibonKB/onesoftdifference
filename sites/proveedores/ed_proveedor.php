<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div class="secc_bar">
	Edición proveedor
</div>
<?php

$dc_proveedor = intval($_POST['id']);

$data = $db->select('tb_proveedor',
	'dg_rut, dg_giro, dc_mercado, dg_razon, dg_sitioweb, dc_tipo_proveedor, dc_cuenta_contable, dc_cuenta_anticipo',
	"dc_proveedor = {$dc_proveedor} AND dc_empresa = {$empresa}");
$data = array_shift($data);

if($data === NULL){
	$error_man->showWarning('No se encontró el proveedor, inténtelo denuevo.');
	exit;
}

require_once("../../inc/form-class.php");
$form = new Form($empresa);

?>
<div class="panes">
	<?php
		$form->Start('sites/proveedores/proc/editar_proveedor.php','ed_proveedor');
		$form->Header('<b>Ingrese los datos para actualizar el proveedor</b><br />Los campos marcados con [*] son obligatorios');
		
		$form->Section();
		$form->Text("RUT","prov_rut",1,12,$data['dg_rut'],"inputrut");
		$form->Text("Giro","prov_giro",1,Form::DEFAULT_TEXT_LENGTH,$data['dg_giro']);
		$form->Listado("Mercado","prov_mercado","tb_mercado",array("dc_mercado","dg_mercado"),1,$data['dc_mercado']);
		
		$form->Listado('Cuenta contable asociada','prov_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),1,$data['dc_cuenta_contable']);
		$form->Listado('Cuenta contable anticipos','prov_cuenta_anticipo','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),1,$data['dc_cuenta_anticipo']);
		
		$form->EndSection();
		
		$form->Section();
		$form->Text("Razon social","prov_razon",1,Form::DEFAULT_TEXT_LENGTH,$data['dg_razon']);
		$form->Text("Sitio web","prov_web",0,Form::DEFAULT_TEXT_LENGTH,$data['dg_sitioweb']);
		$form->Listado("Tipo de proveedor","prov_tipo","tb_tipo_proveedor",array("dc_tipo_proveedor","dg_tipo_proveedor"),1,$data['dc_tipo_proveedor']);
		$form->EndSection();
		
		$form->Hidden('dc_proveedor',$dc_proveedor);
		$form->End('Editar','editbtn');
	?>
</div>