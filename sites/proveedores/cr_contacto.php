<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div class="secc_bar">Creación de contactos</div><div class="panes">');

$form->Start('sites/proveedores/proc/crear_contacto.php','cr_contacto');
$form->Header('<b>Ingrese los datos para la creación del contacto</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();	 
$form->Text('Nombre','prov_cont_name',1);
$form->Text('Dirección','prov_cont_direccion',1);
$form->Listado('Región','prov_region','tb_region',array('dc_region','dg_region'),0,0,'');
$form->Select('Comuna','prov_cont_comuna',array(),1);
$form->Text('Código Postal','prov_cont_postal');
$form->Text('Horario atención','prov_cont_horario');
$form->EndSection();

$form->Section();
$form->Text('Teléfono','prov_cont_fono');
$form->Text('Fax','prov_cont_fax');
$form->Listado("Tipo de contacto","prov_cont_tipo","tb_tipo_direccion",array("dc_tipo_direccion","dg_tipo_direccion"),1);
$form->Text('Teléfono movil','prov_cont_movil');
$form->Text('E-Mail','prov_cont_mail');
$form->Listado('Cargo contacto','prov_cont_cargo','tb_cargo_contacto',array('dc_cargo_contacto','dg_cargo_contacto'));
$form->EndSection();

$form->Hidden('prov_id',$_POST['id']);
$form->End('Crear','addbtn');

$aux = $db->select('tb_comuna','dc_comuna,dg_comuna,dc_region');
$comunas = array();
foreach($aux as $comuna){
	$comunas[$comuna['dc_region']][] = array($comuna['dc_comuna'],$comuna['dg_comuna']);
}
?>
</div>
<script type="text/javascript">
$('#prov_region').change(js_data.changeRegion);
js_data.comunas = <?=json_encode($comunas) ?>;
</script>