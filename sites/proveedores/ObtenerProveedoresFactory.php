<?php

require_once (__DIR__ . '/../../inc/PHPExcel.php');

class ObtenerProveedoresFactory extends Factory {
	
	public function indexAction(){
		$db = $this->getconnection();
		$table = 'tb_proveedor prov '
					. 'LEFT JOIN tb_contacto_proveedor con on prov.dc_contacto_default = con.dc_contacto_proveedor';
					
		$fields = 'prov.dg_razon, df_creacion, con.dg_telefono';
		
		$conditions = 'prov.dc_empresa = ' . $this->getEmpresa();
		$options = array(
					'order_by'=> 'prov.dg_razon'
					);
		$st = $db->doQuery($db->select($table, $fields, $conditions, $options));
		$proveedores = $st->fetchAll(PDO::FETCH_OBJ);
		
		$excel = new PHPExcel();
		$excel->getProperties()
				->setCreator("Onesoft")
				->setLastModifiedBy("Onesoft")
				->setTitle("Lista de proveedores")
				->setSubject("Lista de proveedores")
				->setDescription("Lista de proveedores")
				->setKeywords("Excel Office 2007 openxml php")
				->setCategory("Proveedores");
		
		$columna = 'A';
		$fila = 1;
		$contador = 0;
		
		$excel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Proveedor");
		$excel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
		$columna++;
		$contador++;
		$excel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Fecha de creación");
		$excel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
		$columna++;
		$contador++;
		$excel->setActiveSheetIndex(0)->setCellValue("$columna$fila","Numero de contacto");
		$excel->getActiveSheet()->getStyle("$columna$fila")->getFont()->setBold(true);
		$columna++;
		$contador++;
		
		foreach($proveedores as $v){
			$fila++;
			$columna = 'A';
			$excel->setActiveSheetIndex(0)->setCellValue("$columna$fila", $v->dg_razon);
			$columna++;
			$excel->setActiveSheetIndex(0)->setCellValue("$columna$fila", $v->df_creacion);
			$columna++;
			$excel->setActiveSheetIndex(0)->setCellValue("$columna$fila", $v->dg_telefono);
		}
		
		$columna = 'A';
		for($i = 1; $i <=$contador; $i++){
			$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
			$columna++;
		}
		
		//Renombrar hoja
		$excel->getActiveSheet()->setTitle('Listado Proveedores');
		//Establecer hoja activa para que se muestre al abrir el excel
		$excel->setActiveSheetIndex(0);
		//Indicar al navegador que muestre el dialogo de descarga
		header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
		//Indicar que se esta devolviendo un archivo
		header("Content-Disposition: attachment; filename=Listado_Proveedores.xlsx");
		//Evitar que quede en cache
		header('Cache-control: max-age=0');
		//Dar salida al documento
		$excel = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$excel->save('php://output');
		exit;
	}
}