<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$valida_rut = $db->select('tb_proveedor','true',"dg_rut = '{$_POST['prov_rut']}' AND dm_activo = 1 AND dc_empresa = {$empresa}");
$valida_rut = array_shift($valida_rut);

if($valida_rut != NULL){
	$error_man->showWarning("Ya existe un proveedor con el rut ingresado.");
	exit;
}

$db->start_transaction();

$proveedor = $db->insert('tb_proveedor',array(
	'dg_rut' => $_POST['prov_rut'],
	'dg_razon' => $_POST['prov_razon'],
	'dg_giro' => $_POST['prov_giro'],
	'dg_sitioweb' => $_POST['prov_web'],
	'dc_cuenta_contable' => $_POST['prov_cuenta_contable'],
	'dc_cuenta_anticipo' => $_POST['prov_cuenta_anticipo'],
	'dc_empresa' => $empresa,
	'dg_password' => md5($_POST['prov_rut']),
	'dc_tipo_proveedor' => $_POST['prov_tipo'],
	'dc_mercado' => $_POST['prov_mercado'],
	'df_creacion' => 'NOW()',
	'dc_usuario_creacion' => $idUsuario
));

$contacto = $db->insert('tb_contacto_proveedor',array(
	'dg_contacto' => $_POST['prov_cont_name'],
	'dc_proveedor' => $proveedor,
	'dg_direccion' => $_POST['prov_cont_direccion'],
	'dc_comuna' => $_POST['prov_cont_comuna'],
	'dg_telefono' => $_POST['prov_cont_fono'],
	'dg_movil' => $_POST['prov_cont_movil'],
	'dg_fax' => $_POST['prov_cont_fax'],
	'dg_codigo_postal' => $_POST['prov_cont_postal'],
	'dg_horario_atencion' => $_POST['prov_cont_horario'],
	'dc_tipo_contacto' => $_POST['prov_cont_tipo'],
	'dc_cargo_contacto' => $_POST['prov_cont_cargo']
));


$db->update('tb_proveedor',array(
	'dc_contacto_default' => $contacto
),"dc_proveedor={$proveedor}");

$db->commit();

$error_man->showConfirm("Se ha creado el proveedor correctamente, pero no se encuentra validado para utilizarlo en los procesos de la empresa.");

?>