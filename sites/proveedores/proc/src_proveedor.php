<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$data = $db->select("(SELECT * FROM tb_proveedor WHERE dg_rut='{$_POST['prov_rut']}' AND dc_empresa={$empresa}) p
LEFT JOIN tb_tipo_proveedor t ON t.dc_tipo_proveedor = p.dc_tipo_proveedor
JOIN tb_mercado m ON m.dc_mercado = p.dc_mercado
JOIN tb_contacto_proveedor c ON c.dc_contacto_proveedor = p.dc_contacto_default
	LEFT JOIN tb_comuna com ON com.dc_comuna = c.dc_comuna
	LEFT JOIN tb_region reg ON reg.dc_region = com.dc_region",
'p.dc_proveedor,p.dg_rut,p.dg_razon,p.dg_giro,p.dg_sitioweb,p.dc_proveedor_estado,DATE_FORMAT(p.df_creacion,"%d/%m/%Y") AS df_creacion,
m.dg_mercado,t.dg_tipo_proveedor,c.dg_contacto,c.dg_telefono,c.dg_movil,c.dg_fax,com.dg_comuna,reg.dg_region');

if(!count($data)){
	$error_man->showAviso("No se encontró ningún proveedor con los criterios especificados");
	exit();
}
$data = $data[0];

//http://192.168.0.202/callto/manager.php?call=&ext=6804&call=90978581368&Submit+now.x=0&Submit+now.y=0

$anexo = $db->select('tb_funcionario','dg_anexo',"dc_funcionario={$userdata['dc_funcionario']}");
$anexo = $anexo[0]['dg_anexo'];
echo("<div id='options_menu'>");
if(check_permiso(48))
echo("<a href='sites/proveedores/ed_proveedor.php?id={$data['dc_proveedor']}' class='loadOnOverlay editbtn'>Editar</a>");
if(check_permiso(50) && $data['dc_proveedor_estado'] < 2)
echo("<a href='sites/proveedores/proc/validar_proveedor.php?id={$data['dc_proveedor']}' id='valida_proveedor' class='checkbtn'>Validar</a>");
//echo("<a href='sites/clientes/base_conocimiento.php?id={$data['dc_cliente']}' class='loadOnOverlay button'>Base de conocimiento</a>");
echo("<a href='sites/proveedores/cr_contacto.php?id={$data['dc_proveedor']}' class='loadOnOverlay addbtn'>Crear contacto</a>");
echo("<a href='sites/proveedores/src_analisis_proveedor.php?id={$data['dc_proveedor']}' class='loadOnOverlay button'>Análisis Proveedor</a>");
echo("</div>");


if($data['dg_movil']) $data['dg_movil'] = '<i>móvil:</i> '.$data['dg_movil'];
if($data['dg_fax']) $data['dg_fax'] = '<i>fax:</i> '.$data['dg_fax'];

echo("<div id='show_proveedor'>
<table width='100%' class='tab'>
<caption>Razón social: <b>{$data['dg_razon']}</b></caption>
<tbody>
	<tr>
		<td>RUT</td>
		<td>{$data['dg_rut']}</td>
	</tr>
	<tr>
		<td>Giro</td>
		<td>{$data['dg_giro']}</td>
	</tr>
	<tr>
		<td>Contacto</td>
		<td><b>{$data['dg_contacto']}</b> <i>fono:</i> {$data['dg_telefono']} {$data['dg_movil']} {$data['dg_fax']}</td>
	</tr>
	<tr>
		<td>Sitio web</td>
		<td>{$data['dg_sitioweb']}</td>
	</tr>
	<tr>
		<td>Tipo de proveedor</td>
		<td>{$data['dg_tipo_proveedor']}</td>
	</tr>
	<tr>
		<td>Mercado</td>
		<td>{$data['dg_mercado']}</td>
	</tr>
	<tr>
		<td>Fecha de ingreso</td>
		<td>{$data['df_creacion']}</td>
	</tr>
</tbody>
</table><hr />");

$error_man->showInfo("Contactos de <b>{$data['dg_razon']}</b>");

$contactos = $db->select("(SELECT * FROM tb_contacto_proveedor WHERE dc_proveedor={$data['dc_proveedor']}) c
LEFT JOIN tb_tipo_direccion td ON td.dc_tipo_direccion = c.dc_tipo_contacto
LEFT JOIN tb_cargo_contacto cc ON cc.dc_cargo_contacto = c.dc_cargo_contacto
LEFT JOIN tb_comuna com ON com.dc_comuna = c.dc_comuna
JOIN tb_region r ON r.dc_region = com.dc_region",
'c.dc_contacto_proveedor,c.dg_contacto,c.dg_direccion,com.dg_comuna,c.dg_telefono,c.dg_movil,c.dg_fax,
c.dg_codigo_postal,c.dg_horario_atencion,td.dg_tipo_direccion,cc.dg_cargo_contacto');

echo('<table class="tab" width="100%"><caption>Contactos</caption>
<thead><tr>
	<th></th>
	<th>Contacto</th>
	<th>Teléfono</th>
	<th>Móvil</th>
	<th>Fax</th>
	<th>Cargo</th>
</tr></thead><tbody>');
	
$razon = substr($data['dg_razon'],0,15);

foreach($contactos as $c){
	//http://192.168.0.202/callto/manager.php?call=&ext=6804&call=912314212312&Submit+now.x=0&Submit+now.y=0
	//http://192.168.0.202/callto/manager.php?call=&ext={$anexo}&call=9{$c['dg_movil']}&Submit+now.x=0&Submit+now.y=0
	if($anexo != 0){
		
		
		if($c['dg_movil'] && is_numeric($c['dg_movil'])){
			$query_data = http_build_query(array(
				'call' => $c['dg_movil'],
				'ext' => $anexo,
				'razon' => $data['dg_razon']
			));
			
			$c['dg_movil'] = "<a target='callframe' class='right' href='http://192.168.0.202/phone/asterisk_call_trigger.php?{$query_data}'>
				<img src='images/call.png' alt='[LLAMAR]' title='Llamar al número' />
			</a>".$c['dg_movil'];
		}
		
		if(is_numeric($c['dg_telefono'])){
			$query_data = http_build_query(array(
				'call' => $c['dg_telefono'],
				'ext' => $anexo,
				'razon' => $data['dg_razon']
			));
			
			$c['dg_telefono'] = "<a target='callframe' class='right' href='http://192.168.0.202/phone/asterisk_call_trigger.php?{$query_data}'>
				<img src='images/call.png' alt='[LLAMAR]' title='Llamar al número' />
			</a>".$c['dg_telefono'];
		}
	}
	
	echo("<tr>
		<td><a href='sites/proveedores/ed_contacto.php?id={$c['dc_contacto_proveedor']}' class='loadOnOverlay'>
			<img src='images/editbtn.png' alt='editar' />
		</a></td>
		<td><b>{$c['dg_contacto']}</b></td>
		<td>{$c['dg_telefono']}</td>
		<td>{$c['dg_movil']}</td>
		<td>{$c['dg_fax']}</td>
		<td>{$c['dg_cargo_contacto']}</td>
	</tr>");
}
echo('</tbody></table>');
echo('</div>
<iframe name="callframe" class="hidden" />');
?>
<script type="text/javascript" src="jscripts/proveedores/src_proveedor.js?v0_2a"></script>
<script type="text/javascript">
	js_data.prov_rut = '<?=$_POST['prov_rut'] ?>';
</script>
