<?php
define("MAIN",1);
require_once("../../../inc/init.php");

$dc_proveedor = intval($_POST['dc_proveedor']);

if(!$dc_proveedor){
	$error_man->showWarning('Proveedor inválido, compruebe los datos de entrada.');
	exit;
}

$update_proveedor = $db->prepare($db->update('tb_proveedor',array(
	'dg_rut' => '?',
	'dg_razon' => '?',
	'dg_giro' => '?',
	'dg_sitioweb' => '?',
	'dc_cuenta_contable' => '?',
	'dc_tipo_proveedor' => '?',
	'dc_mercado' => '?',
	'dc_cuenta_anticipo' => '?'
),'dc_proveedor = ? AND dc_empresa = ?'));
$update_proveedor->bindValue(1,$_POST['prov_rut'],PDO::PARAM_STR);
$update_proveedor->bindValue(2,$_POST['prov_razon'],PDO::PARAM_STR);
$update_proveedor->bindValue(3,$_POST['prov_giro'],PDO::PARAM_STR);
$update_proveedor->bindValue(4,$_POST['prov_web'],PDO::PARAM_STR);
$update_proveedor->bindValue(5,$_POST['prov_cuenta_contable'],PDO::PARAM_INT);
$update_proveedor->bindValue(6,$_POST['prov_tipo'],PDO::PARAM_INT);
$update_proveedor->bindValue(7,$_POST['prov_mercado'],PDO::PARAM_INT);
$update_proveedor->bindValue(8,$_POST['prov_cuenta_anticipo'],PDO::PARAM_INT);
$update_proveedor->bindValue(9,$dc_proveedor,PDO::PARAM_INT);
$update_proveedor->bindValue(10,$empresa,PDO::PARAM_INT);

$db->stExec($update_proveedor);

$error_man->showConfirm('Los datos del proveedor han sido actualizados correctamente');

?>