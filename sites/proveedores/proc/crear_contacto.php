<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->insert('tb_contacto_proveedor',array(
	'dg_contacto' => $_POST['prov_cont_name'],
	'dc_proveedor' => $_POST['prov_id'],
	'dg_direccion' => $_POST['prov_cont_direccion'],
	'dc_comuna' => $_POST['prov_cont_comuna'],
	'dg_telefono' => $_POST['prov_cont_fono'],
	'dg_movil' => $_POST['prov_cont_movil'],
	'dg_fax' => $_POST['prov_cont_fax'],
	'dg_codigo_postal' => $_POST['prov_cont_postal'],
	'dg_horario_atencion' => $_POST['prov_cont_horario'],
	'dc_tipo_contacto' => $_POST['prov_cont_tipo'],
	'dc_cargo_contacto' => $_POST['prov_cont_cargo']
));

$error_man->showConfirm('Se ha creado el contacto correctamente y se a asignado al proveedor');

?>
<script type="text/javascript">
	js_data.refreshProveedor();
</script>