<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$db->update('tb_proveedor',array('dc_proveedor_estado' => 2),"dc_proveedor={$_POST['id']}");

$error_man->showConfirm('El proveedor ahora es válido para realizar operaciones sobre el en el sistema.');
?>
<script type="text/javascript">
$('#valida_proveedor').remove();
</script>