<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

require_once('../../inc/form-class.php');
$form = new Form($empresa);

echo('<div id="secc_bar">Creación de proveedores</div>
<div id="main_cont"><div class="panes">');

$form->Start('sites/proveedores/proc/crear_proveedor.php','cr_proveedor');
$form->Header('<b>Ingrese los datos para la creación del proveedor</b><br />Los campos marcados con [*] son obligatorios');

$form->Section();
$form->Text("RUT","prov_rut",1,12,"","inputrut");
$form->Text("Giro","prov_giro",1);
$form->Listado("Mercado","prov_mercado","tb_mercado",array("dc_mercado","dg_mercado"),1);
$form->Listado('Cuenta contable asociada','prov_cuenta_contable','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),1);
$form->EndSection();

$form->Section();
$form->Text("Razon social","prov_razon",1,100);
$form->Text("Sitio web","prov_web",0,50);
$form->Listado("Tipo de proveedor","prov_tipo","tb_tipo_proveedor",array("dc_tipo_proveedor","dg_tipo_proveedor"),1);
$form->Listado('Cuenta contable Anticipos','prov_cuenta_anticipo','tb_cuenta_contable',array('dc_cuenta_contable','dg_cuenta_contable'),1);
$form->EndSection();

$form->Group();
$form->Header('<b>Contacto Principal</b><br /><small>(Luego podrá agregar nuevos contactos)</small>');

$form->Section();	 
$form->Text('Nombre','prov_cont_name',1);
$form->Text('Dirección','prov_cont_direccion',1);
$form->Listado('Región','prov_region','tb_region',array('dc_region','dg_region'),0,0,'');
$form->Select('Comuna','prov_cont_comuna',array(),1);
$form->Text('Código Postal','prov_cont_postal');
$form->Text('Horario atención','prov_cont_horario');
$form->EndSection();

$form->Section();
$form->Text('Teléfono','prov_cont_fono');
$form->Text('Fax','prov_cont_fax');
$form->Listado("Tipo de contacto","prov_cont_tipo","tb_tipo_direccion",array("dc_tipo_direccion","dg_tipo_direccion"),1);
$form->Text('Teléfono movil','prov_cont_movil');
$form->Text('E-Mail','prov_cont_mail');
$form->Listado('Cargo contacto','prov_cont_cargo','tb_cargo_contacto',array('dc_cargo_contacto','dg_cargo_contacto'));
$form->EndSection();

$form->Group();
$form->End('Crear','addbtn');

$aux = $db->select('tb_comuna','dc_comuna,dg_comuna,dc_region');
$comunas = array();
foreach($aux as $comuna){
	$comunas[$comuna['dc_region']][] = array($comuna['dc_comuna'],$comuna['dg_comuna']);
}

?>
</div></div>
<script type="text/javascript" src="jscripts/proveedores/cr_proveedor.js"></script>
<script type="text/javascript">
js_data.comunas = <?=json_encode($comunas) ?>;
js_data.init();
</script>