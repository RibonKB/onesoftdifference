<?php

require_once 'inc/AbstractFactoryService.class.php';
require_once 'inc/mail/class.phpmailer.php';

/**
 * Description of SMTPService
 *
 * @author Tomás Lara Valdovinos
 * @date 05-08-2013
 */
class SMTPService extends AbstractFactoryService{
    
    const MODE_SERVICIO = 1;
    const MODE_VENTAS = 2;
    const MODE_CONTABILIDAD = 3;
    const MODE_LOGISTICA = 4;
    
    private $connectionData = array(
        self::MODE_SERVICIO => array(
            'server' => 'pod51009.outlook.com',
            'user' => 'helpdesk@grupotecnologico.cl',
            'pass' => 'Passw0rD2013632',
            'port' => 587,
            'enc' => 'tls',
            'fromName' => 'Helpdesk ONESOFT'
        )
    );
    
    private $activeMode = null;
    /**
     *
     * @var PHPMailer
     */
    private $smtp = null;
    
    public function setMode($mode){
      $this->activeMode = $mode;
    }
    
    public function connect(){
      $connectionData = $this->connectionData[$this->activeMode];
      $this->smtp = new PHPMailer();
      $this->smtp->IsSMTP();
      $this->smtp->SMTPAuth = true;
      $this->smtp->CharSet = 'utf-8';
      $this->smtp->Host = $connectionData['server'];
      $this->smtp->Port = $connectionData['port'];
      $this->smtp->Username = $connectionData['user'];
      $this->smtp->Password = $connectionData['pass'];
      $this->smtp->SMTPSecure = $connectionData['enc'];
      $this->smtp->SetFrom($connectionData['user'], $connectionData['fromName']);
    }
    
    public function setAddress($mail, $name = ''){
      $this->smtp->AddAddress($mail, $name);
    }
    
    public function sendMail($subject, $body){
      $this->smtp->Subject = $subject;
      $this->smtp->MsgHTML($body);
      
      if(!$this->smtp->Send()){
        $this->factory->getErrorMan()->showWarning("No se pudo enviar el correo con las credenciales de <b>{$this->connectionData[$this->activeMode]['user']}</b>");
        return false;
      }
      
      return true;
    }
    
}

?>
