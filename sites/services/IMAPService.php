<?php

require_once 'inc/AbstractFactoryService.class.php';

/**
 * Description of IMAPService
 *
 * @author Tomás Lara Valdovinos
 * @date 14-06-2013
 */
class IMAPService extends AbstractFactoryService {

  private $stream = null;
  private $server;
  private $stringType = array(
      TYPETEXT => 'text',
      TYPEMULTIPART => 'multipart',
      TYPEMESSAGE => 'message',
      TYPEAPPLICATION => 'application',
      TYPEAUDIO => 'audio',
      TYPEIMAGE => 'image',
      TYPEVIDEO => 'video',
      TYPEMODEL => 'model',
      TYPEOTHER => 'other'
  );

  public function login($box, $username, $password) {
    $this->stream = imap_open($box, $username, $password, NULL, 1, array('DISABLE_AUTHENTICATOR' => 'GSSAPI'));
    $this->server = $box;

    if ($this->stream === false) {
      throw new Exception('No se pudo conectar con el servidor remoto para obtener correos electrónicos: ' . imap_last_error());
    }
  }

  public function getFolders($pattern = '*') {
    $list = imap_list($this->stream, $this->server, $pattern);
    array_walk($list, function(&$value, $i) {
              $value = str_replace($this->server, '', $value);
            });
    return $list;
  }

  public function getNumMsg() {
    return imap_num_msg($this->stream);
  }

  public function getHeader($i) {
    $header = imap_header($this->stream, $i);
    $header->subject = imap_utf8($header->subject);
    return $header;
  }

  public function getHeaders() {
    $list = array();
    $n_msgs = $this->getNumMsg();
    for ($i = 1; $i <= $n_msgs; $i++) {
      $list[$i] = $this->getHeader($i);
    }
    return $list;
  }

  public function getBody($i) {
    return imap_body($this->stream, $i);
  }

  public function fetchEstructure($i) {
    return imap_fetchstructure($this->stream, $i);
  }

  public function fetchBody($i, $part) {
    return imap_fetchbody($this->stream, $i, $part);
  }

  public function decodeBase64($text) {
    return imap_base64($text);
  }

  public function get8bitQprint($text) {
    return imap_qprint($text);
  }

  public function getUID($i) {
    return imap_uid($this->stream, $i);
  }

  public function archivar($box, $range = '1:*') {
    if (!imap_mail_move($this->stream, $range, $box)) {
      throw new Exception("Casilla de mensajes leídos no existe " . imap_last_error());
    }
    imap_expunge($this->stream);
  }

  public function getFinalParts($msgno, $root, $actual = array(), $level = '') {
    if ($root->type == TYPEMULTIPART) {
      foreach ($root->parts as $i => $p) {
        $l = $level == '' ? ($i + 1) : $level . '.' . ($i + 1);
        $actual = $this->getFinalParts($msgno, $p, $actual, $l);
      }
    } else if ($root->ifdisposition and !(strtolower($root->disposition) == 'inline' and $root->type == TYPETEXT)) {
      if (!isset($actual[$root->disposition])) {
        $actual[$root->disposition] = array();
      }

      $data = (object) array(
                  'mimetype' => $this->stringType[$root->type] . '/' . strtolower($root->subtype),
                  'path' => $level,
                  'encoding' => $root->encoding,
                  'filename' => $this->getPartFilename($root)
      );

      if ($root->ifid) {
        $data->id = substr($root->id, 1, -1);
      }

      if (strtolower($root->disposition) == 'inline' and $root->type != TYPEIMAGE) {
        $root->disposition = 'attachment';
      }

      $actual[$root->disposition][] = $data;
    } else if ($root->type == TYPETEXT) {
      if ($level == '') {
        $level = '1';
      }

      $value = $this->fetchBody($msgno, $level);

      if ($root->encoding == ENCQUOTEDPRINTABLE) {
        $value = $this->get8bitQprint($value);
      } else if ($root->encoding == ENCBASE64) {
        $value = $this->decodeBase64($value);
      }

      if ($root->ifparameters) {
        foreach ($root->parameters as $param) {
          if ($param->attribute and strtolower($param->value) == 'iso-8859-1') {
            $value = utf8_encode($value);
            break;
          }
        }
      }

      $actual[$root->subtype] = trim($value);
    }

    return $actual;
  }

  private function getPartFilename($p) {
    //get filename of attachment if present
    $filename = '';
    // if there are any dparameters present in this part
    if (count($p->dparameters) > 0) {
      foreach ($p->dparameters as $dparam) {
        if ((strtoupper($dparam->attribute) == 'NAME') || (strtoupper($dparam->attribute) == 'FILENAME'))
          $filename = $dparam->value;
      }
    }
    //if no filename found
    if ($filename == '') {
      // if there are any parameters present in this part
      if (count($p->parameters) > 0) {
        foreach ($p->parameters as $param) {
          if ((strtoupper($param->attribute) == 'NAME') || (strtoupper($param->attribute) == 'FILENAME'))
            $filename = $param->value;
        }
      }
    }

    return $filename;
  }

  public function __destruct() {
    if ($this->stream)
      imap_close($this->stream);
  }

}

?>
