<?php

require_once 'inc/AbstractFactoryService.class.php';

/**
 * Description of LogisticaStuffService
 *
 * @author Tomás Lara Valdovinos
 * @date 07-05-2013
 */
class LogisticaStuffService extends AbstractFactoryService{

    private $rebajaReservado = null;
    private $rebajaReservadoOS = null;
    private $rebajaLibre = null;

    private $cargaLibre = null;
    private $cargaLibreInsert = null;

    private $cargaNV = null;
    private $cargaNVInsert = null;

    private $cargaOS = null;
    private $cargaOSInsert = null;

    public function getConfiguration(){
        return $this->factory->getConnection()
                  ->getRowById('tb_configuracion_logistica', $this->factory->getEmpresa(), 'dc_empresa');
    }

    public function rebajarStockReservadoNV($producto, $cantidad, $bodega){

        $db = $this->factory->getConnection();

        if($this->rebajaReservado === null){
            $this->rebajaReservado = $db->prepare($db->update('tb_stock',array(
                'dq_stock' => "dq_stock-?",
                'dq_stock_reservado' => "dq_stock_reservado-?"
            ),"dc_producto=? AND dc_bodega=? AND dq_stock >= ? AND dq_stock_reservado >= ?"));
        }

        $this->rebajaReservado->bindValue(1,$cantidad,PDO::PARAM_INT);
        $this->rebajaReservado->bindValue(2,$cantidad,PDO::PARAM_INT);
        $this->rebajaReservado->bindValue(3,$producto,PDO::PARAM_INT);
        $this->rebajaReservado->bindValue(4,$bodega,PDO::PARAM_INT);
        $this->rebajaReservado->bindValue(5,$cantidad,PDO::PARAM_INT);
        $this->rebajaReservado->bindValue(6,$cantidad,PDO::PARAM_INT);

        $db->stExec($this->rebajaReservado);

        return $this->rebajaReservado->rowCount();
    }

    public function rebajarStockReservadoOS($producto, $cantidad, $bodega){
        $db = $this->factory->getConnection();

        if($this->rebajaReservadoOS === null){
            $this->rebajaReservadoOS = $db->prepare($db->update('tb_stock',array(
                'dq_stock' => "dq_stock-?",
                'dq_stock_reservado_os' => "dq_stock_reservado_os-?"
            ),"dc_producto=? AND dc_bodega=? AND dq_stock >= ? AND dq_stock_reservado >= ?"));
        }

        $this->rebajaReservadoOS->bindValue(1,$cantidad,PDO::PARAM_INT);
        $this->rebajaReservadoOS->bindValue(2,$cantidad,PDO::PARAM_INT);
        $this->rebajaReservadoOS->bindValue(3,$producto,PDO::PARAM_INT);
        $this->rebajaReservadoOS->bindValue(4,$bodega,PDO::PARAM_INT);
        $this->rebajaReservadoOS->bindValue(5,$cantidad,PDO::PARAM_INT);
        $this->rebajaReservadoOS->bindValue(6,$cantidad,PDO::PARAM_INT);

        $db->stExec($this->rebajaReservadoOS);

        return $this->rebajaReservadoOS->rowCount();
    }

    public function rebajarStockLibre($producto, $cantidad, $bodega){
        $db = $this->factory->getConnection();

        if($this->rebajaLibre === null){
            $this->rebajaLibre = $db->prepare($db->update('tb_stock',array(
                'dq_stock' => "dq_stock-:cant"
            ),"dc_producto=:whereProduct AND dc_bodega=:whereBodega AND (dq_stock-dq_stock_reservado) >= :whereCant"));
        }

        $this->rebajaLibre->bindValue(':cant',$cantidad,PDO::PARAM_INT);
        $this->rebajaLibre->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
        $this->rebajaLibre->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);
        $this->rebajaLibre->bindValue(':whereCant',$cantidad,PDO::PARAM_INT);

        $db->stExec($this->rebajaLibre);

        return $this->rebajaLibre->rowCount();
    }

    public function cargarStockLibre($producto, $cantidad, $bodega){
        $db = $this->factory->getConnection();

        if($this->cargaLibre === null){
            $this->cargaLibre = $db->prepare($db->update('tb_stock',array(
                'dq_stock' => "dq_stock+:cant"
            ),"dc_producto=:whereProduct AND dc_bodega=:whereBodega"));
        }

        $this->cargaLibre->bindValue(':cant',$cantidad,PDO::PARAM_INT);
        $this->cargaLibre->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
        $this->cargaLibre->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);

        $db->stExec($this->cargaLibre);

        if($this->cargaLibre->rowCount() == 0){
            if($this->cargaLibreInsert === null){
                $this->cargaLibreInsert = $db->prepare($db->insert('tb_stock',array(
                    'dq_stock' => ':cant',
                    'dc_producto' => ':producto',
                    'dc_bodega' => ':bodega'
                )));
            }

            $this->cargaLibreInsert->bindValue(':cant',$cantidad,PDO::PARAM_INT);
            $this->cargaLibreInsert->bindValue(':producto',$producto,PDO::PARAM_INT);
            $this->cargaLibreInsert->bindValue(':bodega',$bodega,PDO::PARAM_INT);

            $db->stExec($this->cargaLibreInsert);
        }
    }

    public function cargarStockNV($producto, $cantidad, $bodega){
        $db = $this->factory->getConnection();

        if($this->cargaNV === null){
            $this->cargaNV = $db->prepare($db->update('tb_stock',array(
                'dq_stock' => "dq_stock+:cant",
                'dq_stock_reservado' => "dq_stock_reservado+:cantNV"
            ),"dc_producto=:whereProduct AND dc_bodega=:whereBodega"));
        }

        $this->cargaNV->bindValue(':cant',$cantidad,PDO::PARAM_INT);
        $this->cargaNV->bindValue(':cantNV',$cantidad,PDO::PARAM_INT);
        $this->cargaNV->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
        $this->cargaNV->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);

        $db->stExec($this->cargaNV);

        if($this->cargaNV->rowCount() == 0){
            if($this->cargaNVInsert === null){
                $this->cargaNVInsert = $db->prepare($db->insert('tb_stock',array(
                    'dq_stock' => ':cant',
                    'dq_stock_reservado' => ':cantNV',
                    'dc_producto' => ':producto',
                    'dc_bodega' => ':bodega'
                )));
            }

            $this->cargaNVInsert->bindValue(':cant',$cantidad,PDO::PARAM_INT);
            $this->cargaNVInsert->bindValue(':cantNV',$cantidad,PDO::PARAM_INT);
            $this->cargaNVInsert->bindValue(':producto',$producto,PDO::PARAM_INT);
            $this->cargaNVInsert->bindValue(':bodega',$bodega,PDO::PARAM_INT);

            $db->stExec($this->cargaNVInsert);
        }
    }

    public function cargarStockOS($producto, $cantidad, $bodega){
        $db = $this->factory->getConnection();

        if($this->cargaOS === null){
            $this->cargaOS = $db->prepare($db->update('tb_stock',array(
                'dq_stock' => "dq_stock+:cant",
                'dq_stock_reservado_os' => "dq_stock_reservado_os+:cantOS"
            ),"dc_producto=:whereProduct AND dc_bodega=:whereBodega"));
        }

        $this->cargaOS->bindValue(':cant',$cantidad,PDO::PARAM_INT);
        $this->cargaOS->bindValue(':cantOS',$cantidad,PDO::PARAM_INT);
        $this->cargaOS->bindValue(':whereProduct',$producto,PDO::PARAM_INT);
        $this->cargaOS->bindValue(':whereBodega',$bodega,PDO::PARAM_INT);

        $db->stExec($this->cargaOS);

        if($this->cargaOS->rowCount() == 0){
            if($this->cargaOSInsert == null){
                $this->cargaOSInsert = $db->prepare($db->insert('tb_stock',array(
                    'dq_stock' => ':cant',
                    'dq_stock_reservado_os' => ':cantOS',
                    'dc_producto' => ':producto',
                    'dc_bodega' => ':bodega'
                )));
            }

            $this->cargaOSInsert->bindValue(':cant',$cantidad,PDO::PARAM_INT);
            $this->cargaOSInsert->bindValue(':cantOS',$cantidad,PDO::PARAM_INT);
            $this->cargaOSInsert->bindValue(':producto',$producto,PDO::PARAM_INT);
            $this->cargaOSInsert->bindValue(':bodega',$bodega,PDO::PARAM_INT);

            $db->stExec($this->cargaOSInsert);
        }
    }

    public function comprobarStock($producto, $bodega){
        $db = $this->factory->getConnection();

        $stock = $db->prepare($db->select('tb_stock','dq_stock',"dc_producto = ? AND dc_bodega = ?"));
        $stock->bindValue(1, $producto, PDO::PARAM_INT);
        $stock->bindValue(2, $bodega, PDO::PARAM_INT);
        $db->stExec($stock);
        $stock = $stock->fetch(PDO::FETCH_OBJ);

        if($stock === false){
            return 0;
        }else{
            return $stock->dq_stock;
        }
    }

    public function comprobarTotalStock($producto, $bodega){
        $db = $this->factory->getConnection();

        $stock = $db->prepare($db->select(
                          'tb_stock',
                          'dq_stock,dq_stock_reservado,dq_stock_reservado_os',
                          'dc_producto = ? AND dc_bodega = ?'));
        $stock->bindValue(1, $producto, PDO::PARAM_INT);
        $stock->bindValue(2, $bodega, PDO::PARAM_INT);
        $db->stExec($stock);
        $stock = $stock->fetch(PDO::FETCH_OBJ);

        if($stock === false){
            return array(0,0,0);
        }else{
            return array(
                $stock->dq_stock-$stock->dq_stock_reservado-$stock->dq_stock_reservado_os,
                $stock->dq_stock_reservado,
                $stock->dq_stock_reservado_os
            );
        }
    }

    public function getNewPMP($dc_producto, $dc_bodega, $dc_cantidad, $dq_nuevo_precio){
        $db = $this->factory->getConnection();

        $producto = $db->getRowById('tb_producto', $dc_producto, 'dc_producto');
        $dc_stock = $this->comprobarStock($dc_producto, $dc_bodega);

        if($dc_stock != 0 or $producto->dq_precio_compra != 0){
            $pmp = (($dc_stock*$producto->dq_precio_compra)+($dc_cantidad*$dq_nuevo_precio))/($dc_cantidad+$dc_stock);

            if($dc_cantidad > 0){
              $diff = (abs($pmp-$dq_nuevo_precio)*100)/$pmp;
              if($diff > 30):
                return false;
              endif;
            }

            return $pmp;
        }else{
          return $dq_nuevo_precio;
        }

    }

}

?>
