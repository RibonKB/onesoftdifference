<?php

require_once 'inc/AbstractFactoryService.class.php';

/**
 * Description of DocumentCreationService
 *
 * @author Tomás Lara Valdovinos
 * @date 08-05-2013
 */
class DocumentCreationService  extends AbstractFactoryService{
    
    public function getCorrelativo($table, $campo_number, $format=2, $emision = 'df_emision'){
        $db = $this->factory->getConnection();
        $empresa = $this->factory->getEmpresa();

        $doc_prefix = '';
        $largo_prefix = 0;

        switch ($format) {
            case 1: $doc_prefix = date('Y');
                $largo_prefix = 4;
                break;
            case 2: $doc_prefix = date('Ym');
                $largo_prefix = 6;
                break;
            case 4: $doc_prefix = date('m');
                $largo_prefix = 2;
                break;
        }

        if ($format != 3) {
            $Mactual = date('m');
            $Yactual = date('Y');
            $last = $db->doQuery($db->select($table, "MAX({$campo_number}) AS dq", "MONTH({$emision})='{$Mactual}' AND YEAR({$emision})='{$Yactual}' AND dc_empresa={$empresa}"));
        } else {
            $last = $db->doQuery($db->select($table, "MAX({$campo_number}) AS dq AND dc_empresa={$empresa}"));
        }

        $last = $last->fetch(PDO::FETCH_OBJ)->dq;

        if ($last != NULL) {
            $doc_num = substr($last, $largo_prefix) + 1;
        } else {
            $doc_num = 1;
        }

        return $doc_prefix . str_pad($doc_num, 4, 0, STR_PAD_LEFT);
    }
    
    public function getCorrelativoSiguiente($number, $format=2){
        switch ($format) {
            case 1: $largo_prefix = 4;
                break;
            case 2: $largo_prefix = 6;
                break;
            default: $largo_prefix = 0;
                break;
        }

        $doc_prefix = substr($number,0,$largo_prefix);
        $doc_num = substr($number,$largo_prefix);

        return $doc_prefix . str_pad(++$doc_num, 4, 0, STR_PAD_LEFT);
    }
    
}

?>
