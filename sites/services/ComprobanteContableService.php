<?php

require_once 'inc/AbstractFactoryService.class.php';
require_once 'sites/contabilidad/stuff.class.php';

/**
 * Description of ComprobanteContableService
 *
 * @author Tomás Lara Valdovinos
 * @date 26-04-2013
 */
class ComprobanteContableService extends AbstractFactoryService {

  private $parametrosComprobante;
  private $parametrosFinancieros;
  private $parametrosAnaliticos;
  private $dc_comprobante;
  private $dq_comprobante;
  private $dependencias = null;
  private $dependenciasAsignadas = array();
  private $stComprobante;
  private $stDetalleFinanciero;
  private $stDetalleAnalitico;

  /**
   * @throws Exception debe ser llamado en un try/catch
   * Prepara la cabecera del comprobante contable
   * @param dc_tipo_movimiento = El tipo de movimiento asociado al comprobante
   * @param df_fecha_contable = La fecha contable en que se creará el comprobante, esta debe venir con el formato entregado por $db->sqlDate()
   * @param dm_tipo El tipo de clve asociada por el comprobante, solo los tipo 0 son editables por los usuarios.
   *
   * @returns boolean retorna true si el comprobante se preparó de manera correcta, false en caso de que el periodo contable no esté abierto
   */
  public function prepareComprobante($dc_tipo_movimiento, $df_fecha_contable, $dm_tipo = 0) {
    $db = $this->factory->getConnection();

    try {
      $this->validaPeriodo($df_fecha_contable, $dm_tipo);
    } catch (Exception $e) {
      throw $e;
      return false;
    }

    $this->parametrosComprobante = new stdClass();

    $this->dq_comprobante = ContabilidadStuff::getCorrelativoComprobante($db, $this->factory->getEmpresa());

    $comprobante = $db->prepare($db->insert('tb_comprobante_contable', array(
                'dg_comprobante' => '?',
                'dc_tipo_movimiento' => '?',
                'df_fecha_contable' => '?',
                'dc_mes_contable' => 'MONTH(?)',
                'dc_anho_contable' => 'YEAR(?)',
                'dc_tipo_cambio' => 'NULL',
                'dq_cambio' => 'NULL',
                'dg_glosa' => '?',
                'dq_saldo' => '?',
                'dc_propuesta_pago' => '?',
                'dc_comprobante_pago' => '?',
                'dc_nota_credito_proveedor' => '?',
                'df_fecha_emision' => $db->getNow(),
                'dc_empresa' => $this->factory->getEmpresa(),
                'dc_usuario_creacion' => $this->factory->getUserData()->dc_usuario,
                'dg_numero_interno' => '?',
                'dm_tipo_comprobante' => '?'
    )));
    $comprobante->bindValue(1, $this->dq_comprobante, PDO::PARAM_INT);
    $comprobante->bindValue(2, $dc_tipo_movimiento, PDO::PARAM_INT);
    $comprobante->bindValue(3, $df_fecha_contable, PDO::PARAM_STR);
    $comprobante->bindValue(4, $df_fecha_contable, PDO::PARAM_STR);
    $comprobante->bindValue(5, $df_fecha_contable, PDO::PARAM_STR);
    $comprobante->bindParam(6, $this->parametrosComprobante->dg_glosa, PDO::PARAM_STR);
    $comprobante->bindParam(7, $this->parametrosComprobante->dq_saldo, PDO::PARAM_STR);
    $comprobante->bindParam(8, $this->parametrosComprobante->dc_propuesta_pago, PDO::PARAM_INT);
    $comprobante->bindParam(9, $this->parametrosComprobante->dc_comprobante_pago, PDO::PARAM_INT);
    $comprobante->bindParam(10, $this->parametrosComprobante->dc_nota_credito_proveedor, PDO::PARAM_INT);
    $comprobante->bindParam(11, $this->parametrosComprobante->dg_numero_interno, PDO::PARAM_STR);
    $comprobante->bindValue(12, $dm_tipo, PDO::PARAM_STR);

    $this->parametrosComprobante->dg_glosa = NULL;
    $this->parametrosComprobante->dq_saldo = NULL;
    $this->parametrosComprobante->dc_propuesta_pago = NULL;
    $this->parametrosComprobante->dc_comprobante_pago = NULL;
    $this->parametrosComprobante->dc_nota_credito_proveedor = NULL;
    $this->parametrosComprobante->dg_numero_interno = NULL;

    $this->stComprobante = $comprobante;

    return true;
  }

  /**
  * Asigna un parámetro de cabecera para el comprobante. Este método solo puede ser llamado después de haber preparado el Comprobante contable
  * @param paranName String el nombre del comprobante, puede ser uno de los siguientes:
  *		- dg_glosa
  *		- dq_saldo
  *		- dc_propuesta_pago
  *		- dc_comprobante_pago
  *		- dc_nota_credito_proveedor
  *		- dg_numero_interno
  * @param value Mixed El valor a ser asignado
  *
  *	@notes Cualquier parámetro que no sea ingresado con este método quedará NULL en el comprobante contable
  */
  public function setComprobanteParam($paramName, $value = NULL) {
    $this->parametrosComprobante->$paramName = $value;
  }

	/**
	* Inserta el comprobante contable en la base de datos y asigna un ID para mantenerlo disponible en las próximas operaciones.
	*
	* @notes Es mandatorio realizar una llamada a este método antes de preparar cualquier detalle en el comprobante contable
	*/
    public function ComprobantePersist() {
      $db = $this->factory->getConnection();
      $db->stExec($this->stComprobante);
      $this->dc_comprobante = $db->lastInsertId();
    }

	/**
	*	Retorna información relevante del comprobante contable para ser mostrado por ejemplo en la salida para el usuario
	*
	*	@returns stdClass Retorna un objeto con la siguiente información:
	*		- dc_comprobante (ID del comprobante)
	*		- dq_comprobante (Correlativo del comprobante)
	*		- dg_comproabnte (Correlativo del comprobante para retro-compatibilidad)
	*/
    public function getComprobanteInfo() {
      return (object) array(
                  'dc_comprobante' => $this->dc_comprobante,
                  'dq_comprobante' => $this->dq_comprobante,
                  'dg_comprobante' => $this->dq_comprobante
      );
    }

	/**
	*	Retorna una vista que le indica al usuario los detalles del comprobante que se acaba de crear, util para mantener la estándarización de esta.
	*
	*	@returns String Vista con la información del comprobante creado
	*/
    public function getStandarCreationOutput() {
      return $this->factory->getView('contabilidad/general/mostrar_comprobante_creado', array(
                  'comprobante' => $this->getComprobanteInfo()
      ));
    }

  /**
  *	 Prepara el PDOStatement para insertar detalles financieros, deja los parámetros en blanco y deben ser seteados posteriormente por setParametroFinanciero
  *
  */
  public function prepareDetalleFinanciero() {
    $db = $this->factory->getConnection();

    $this->parametrosFinancieros = new stdClass();

    $detalle = $db->prepare($db->insert('tb_comprobante_contable_detalle', array(
                'dc_comprobante' => $this->dc_comprobante,
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?'
    )));
    $detalle->bindParam(1, $this->parametrosFinancieros->dc_cuenta_contable, PDO::PARAM_INT);
    $detalle->bindParam(2, $this->parametrosFinancieros->dq_debe, PDO::PARAM_STR);
    $detalle->bindParam(3, $this->parametrosFinancieros->dq_haber, PDO::PARAM_STR);
    $detalle->bindParam(4, $this->parametrosFinancieros->dg_glosa, PDO::PARAM_STR);

    $this->stDetalleFinanciero = $detalle;
  }

	/**
	*	Asigna un valor a un parámetro para el detalle financiero
	*	@param paranName String El nombre del parámetro a ser asignado, este puede ser uno de los siguientes:
	*		- dc_cuenta_contable
	*		- dq_debe
	*		- dq_haber
	*		- dg_glosa
	*	@param value Mixed El valor a ser asignado al parámetro
	*
	*	@notes Es mandatorio llamar antes a prepareDetalleFinanciero para que este método tenga sentido
	*/
    public function setParametroFinanciero($paramName, $value = NULL) {
      $this->parametrosFinancieros->$paramName = $value;
    }

	/**
	*	Inserta el detalle financiero en la base de datos y retorna el id generado para ser utilizado por sus comprobantes analiticos
	*
	*	@returns int id del detalle financiero creado
	*/
  public function DetalleFinancieroPersist() {
    $db = $this->factory->getConnection();
    $db->stExec($this->stDetalleFinanciero);

    return $db->lastInsertId();
  }

  /**
  *  Prepara el PDOStatement para insertar detalles analíticos, deja los parametros y deben ser seteados posteriormente utilizando setParametroAnalitico
  */
  public function prepareDetalleAnalitico() {
    $db = $this->factory->getConnection();

    $this->parametrosAnaliticos = new stdClass();

    $detalle = $db->prepare($db->insert('tb_comprobante_contable_detalle_analitico', array(
                'dc_cuenta_contable' => '?',
                'dq_debe' => '?',
                'dq_haber' => '?',
                'dg_glosa' => '?',
                'dc_detalle_financiero' => '?',
                'dc_factura_compra' => '?',
                'dc_factura_venta' => '?',
                'dc_nota_venta' => '?',
                'dc_orden_servicio' => '?',
                'dc_proveedor' => '?',
                'dc_tipo_proveedor' => '?',
                'dc_cliente' => '?',
                'dc_tipo_cliente' => '?',
                'dc_orden_compra' => '?',
                'dc_producto' => '?',
                'dc_cebe' => '?',
                'dc_ceco' => '?',
                'dc_marca' => '?',
                'dc_linea_negocio' => '?',
                'dc_tipo_producto' => '?',
                'dc_banco' => '?',
                'dc_banco_cobro' => '?',
                'dc_guia_recepcion' => '?',
                'dc_medio_pago_proveedor' => '?',
                'dc_medio_cobro_cliente' => '?',
                'dc_mercado_cliente' => '?',
                'dc_mercado_proveedor' => '?',
                'dc_nota_credito' => '?',
                'dc_nota_credito_proveedor' => '?',
                'dc_segmento' => '?',
                'dg_cheque' => '?',
                'df_cheque' => '?',
                'dc_bodega' => '?',
                'dc_ejecutivo' => '?'
    )));
    $detalle->bindParam(1, $this->parametrosAnaliticos->dc_cuenta_contable, PDO::PARAM_INT);
    $detalle->bindParam(2, $this->parametrosAnaliticos->dq_debe, PDO::PARAM_STR);
    $detalle->bindParam(3, $this->parametrosAnaliticos->dq_haber, PDO::PARAM_STR);
    $detalle->bindParam(4, $this->parametrosAnaliticos->dg_glosa, PDO::PARAM_STR);
    $detalle->bindParam(5, $this->parametrosAnaliticos->dc_detalle_financiero, PDO::PARAM_INT);
    $detalle->bindParam(6, $this->parametrosAnaliticos->dc_factura_compra, PDO::PARAM_INT);
    $detalle->bindParam(7, $this->parametrosAnaliticos->dc_factura_venta, PDO::PARAM_INT);
    $detalle->bindParam(8, $this->parametrosAnaliticos->dc_nota_venta, PDO::PARAM_INT);
    $detalle->bindParam(9, $this->parametrosAnaliticos->dc_orden_servicio, PDO::PARAM_INT);
    $detalle->bindParam(10, $this->parametrosAnaliticos->dc_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(11, $this->parametrosAnaliticos->dc_tipo_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(12, $this->parametrosAnaliticos->dc_cliente, PDO::PARAM_INT);
    $detalle->bindParam(13, $this->parametrosAnaliticos->dc_tipo_cliente, PDO::PARAM_INT);
    $detalle->bindParam(14, $this->parametrosAnaliticos->dc_orden_compra, PDO::PARAM_INT);
    $detalle->bindParam(15, $this->parametrosAnaliticos->dc_producto, PDO::PARAM_INT);
    $detalle->bindParam(16, $this->parametrosAnaliticos->dc_cebe, PDO::PARAM_INT);
    $detalle->bindParam(17, $this->parametrosAnaliticos->dc_ceco, PDO::PARAM_INT);
    $detalle->bindParam(18, $this->parametrosAnaliticos->dc_marca, PDO::PARAM_INT);
    $detalle->bindParam(19, $this->parametrosAnaliticos->dc_linea_negocio, PDO::PARAM_INT);
    $detalle->bindParam(20, $this->parametrosAnaliticos->dc_tipo_producto, PDO::PARAM_INT);
    $detalle->bindParam(21, $this->parametrosAnaliticos->dc_banco, PDO::PARAM_INT);
    $detalle->bindParam(22, $this->parametrosAnaliticos->dc_banco_cobro, PDO::PARAM_INT);
    $detalle->bindParam(23, $this->parametrosAnaliticos->dc_guia_recepcion, PDO::PARAM_INT);
    $detalle->bindParam(24, $this->parametrosAnaliticos->dc_medio_pago_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(25, $this->parametrosAnaliticos->dc_medio_cobro_cliente, PDO::PARAM_INT);
    $detalle->bindParam(26, $this->parametrosAnaliticos->dc_mercado_cliente, PDO::PARAM_INT);
    $detalle->bindParam(27, $this->parametrosAnaliticos->dc_mercado_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(28, $this->parametrosAnaliticos->dc_nota_credito, PDO::PARAM_INT);
    $detalle->bindParam(29, $this->parametrosAnaliticos->dc_nota_credito_proveedor, PDO::PARAM_INT);
    $detalle->bindParam(30, $this->parametrosAnaliticos->dc_segmento, PDO::PARAM_INT);
    $detalle->bindParam(31, $this->parametrosAnaliticos->dg_cheque, PDO::PARAM_STR);
    $detalle->bindParam(32, $this->parametrosAnaliticos->df_cheque, PDO::PARAM_STR);
    $detalle->bindParam(33, $this->parametrosAnaliticos->dc_bodega, PDO::PARAM_INT);
    $detalle->bindParam(34, $this->parametrosAnaliticos->dc_ejecutivo, PDO::PARAM_INT);

    $this->parametrosAnaliticos->dg_glosa = NULL;
    $this->parametrosAnaliticos->dc_factura_compra = NULL;
    $this->parametrosAnaliticos->dc_factura_venta = NULL;
    $this->parametrosAnaliticos->dc_nota_venta = NULL;
    $this->parametrosAnaliticos->dc_orden_servicio = NULL;
    $this->parametrosAnaliticos->dc_proveedor = NULL;
    $this->parametrosAnaliticos->dc_tipo_proveedor = NULL;
    $this->parametrosAnaliticos->dc_cliente = NULL;
    $this->parametrosAnaliticos->dc_tipo_cliente = NULL;
    $this->parametrosAnaliticos->dc_orden_compra = NULL;
    $this->parametrosAnaliticos->dc_producto = NULL;
    $this->parametrosAnaliticos->dc_cebe = NULL;
    $this->parametrosAnaliticos->dc_ceco = NULL;
    $this->parametrosAnaliticos->dc_marca = NULL;
    $this->parametrosAnaliticos->dc_linea_negocio = NULL;
    $this->parametrosAnaliticos->dc_tipo_producto = NULL;
    $this->parametrosAnaliticos->dc_banco = NULL;
    $this->parametrosAnaliticos->dc_banco_cobro = NULL;
    $this->parametrosAnaliticos->dc_guia_recepcion = NULL;
    $this->parametrosAnaliticos->dc_medio_pago_proveedor = NULL;
    $this->parametrosAnaliticos->dc_medio_cobro_cliente = NULL;
    $this->parametrosAnaliticos->dc_mercado_cliente = NULL;
    $this->parametrosAnaliticos->dc_mercado_proveedor = NULL;
    $this->parametrosAnaliticos->dc_nota_credito = NULL;
    $this->parametrosAnaliticos->dc_nota_credito_proveedor = NULL;
    $this->parametrosAnaliticos->dc_segmento = NULL;
    $this->parametrosAnaliticos->dg_cheque = NULL;
    $this->parametrosAnaliticos->df_cheque = NULL;
    $this->parametrosAnaliticos->dc_bodega = NULL;
    $this->parametrosAnaliticos->dc_ejecutivo = NULL;

    $this->stDetalleAnalitico = $detalle;

  }

	/**
	*	Asigna un valor a un parámetro para el detalle analítico
	*	@param paranName String El nombre del parámetro a ser asignado, este puede ser uno de los siguientes:
	*		- dg_glosa
    *		- dc_factura_compra
    *		- dc_factura_venta
    *		- dc_nota_venta
    *		- dc_orden_servicio
    *		- dc_proveedor
    *		- dc_tipo_proveedor
    *		- dc_cliente
    *		- dc_tipo_cliente
    *		- dc_orden_compra
    *		- dc_producto
    *		- dc_cebe
    *		- dc_ceco
    *		- dc_marca
    *		- dc_linea_negocio
    *		- dc_tipo_producto
    *		- dc_banco
    *		- dc_banco_cobro
    *		- dc_guia_recepcion
    *		- dc_medio_pago_proveedor
    *		- dc_medio_cobro_cliente
    *		- dc_mercado_cliente
    *		- dc_mercado_proveedor
    *		- dc_nota_credito
    *		- dc_nota_credito_proveedor
    *		- dc_segmento
    *		- dg_cheque
    *		- df_cheque
    *		- dc_bodega
    *		- dc_ejecutivo
	*	@param value Mixed El valor a ser asignado al parámetro
	*
	*	@notes Es mandatorio llamar antes a prepareDetalleAnalitico para que este método tenga sentido
	*/
    public function setParametroAnalitico($paramName, $value = NULL) {
      $this->parametrosAnaliticos->$paramName = $value;
    }

	/**
	*	Setea las dependencias para un parámetro definido
	*
	*	@param parametro String El parámetro a setear sus dependencias
	*/
    public function setDependencias($parametro) {
      if ($this->dependencias === null)
        $this->initDependencias();

      if (!isset($this->dependencias[$parametro]))
        return;

      $empty = array();
      foreach ($this->dependencias[$parametro][2] as $field => $param) {
        if ($this->parametrosAnaliticos->$param == NULL) {
          $empty[$field] = $param;
        }
      }

      if (empty($empty))
        return;

      $row = $this->factory->getConnection()->getRowById($this->dependencias[$parametro][0], $this->parametrosAnaliticos->$parametro, $this->dependencias[$parametro][1]);
  	  if($row === false){
  		return;
  	  }
      unset($this->dependenciasAsignadas[$parametro]);
      $this->dependenciasAsignadas[$parametro] = array($parametro);

      foreach ($empty as $field => $param) {

  		// echo '-------------------';
  		// Factory::debug($param);
  		// Factory::debug($field);
          $this->parametrosAnaliticos->$param = $row->$field;
          $this->dependenciasAsignadas[$parametro][] = $param;
  		// Factory::debug($row);
  		// echo "<br />------------------------------";
      }
    }

	/**
	*	Borra las ependencias seteadas en el detalle analitico
	*/
    public function limpiarDependenciasHasta($parametro){
        if(!isset($this->dependenciasAsignadas[$parametro])){
          return;
        }

        do{
          $data = array_pop($this->dependenciasAsignadas);
          $actual = array_shift($data);
          while($d = array_shift($data)){
              $this->parametrosAnaliticos->$d = NULL;
          }
        }while($actual != $parametro);
    }

	/**
	*	Uso interno, es utilizado por setDependencias para inicializar las dependencias
	*/
  private function initDependencias() {
    $this->dependencias = array(
        'dc_factura_compra' => array(
            'tb_factura_compra',
            'dc_factura',
            array(
                'dc_proveedor' => 'dc_proveedor',
                'dc_nota_venta' => 'dc_nota_venta',
                'dc_orden_servicio' => 'dc_orden_servicio',
                'dc_orden_compra' => 'dc_orden_compra'
            )
        ),
        'dc_factura_venta' => array(
            'tb_factura_venta',
            'dc_factura',
            array(
                'dc_cliente' => 'dc_cliente',
                'dc_nota_venta' => 'dc_nota_venta',
                'dc_orden_servicio' => 'dc_orden_servicio',
                'dc_ejecutivo' => 'dc_ejecutivo'
            )
        ),
        'dc_nota_venta' => array(
            'tb_nota_venta',
            'dc_nota_venta',
            array(
                'dc_cliente' => 'dc_cliente',
                'dc_ejecutivo' => 'dc_ejecutivo'
            )
        ),
        'dc_orden_servicio' => array(
            'tb_orden_servicio',
            'dc_orden_servicio',
            array(
                'dc_cliente' => 'dc_cliente',
                'dc_nota_venta' => 'dc_nota_venta',
                'dc_factura' => 'dc_factura_venta'
            )
        ),
        'dc_orden_compra' => array(
            'tb_orden_compra',
            'dc_orden_compra',
            array(
                'dc_nota_venta' => 'dc_nota_venta',
                'dc_orden_servicio' => 'dc_orden_servicio',
                'dc_proveedor' => 'dc_proveedor'
            )
        ),
        'dc_proveedor' => array(
            'tb_proveedor',
            'dc_proveedor',
            array(
                'dc_tipo_proveedor' => 'dc_tipo_proveedor',
                'dc_mercado' => 'dc_mercado_proveedor'
            )
        ),
        'dc_cliente' => array(
            'tb_cliente',
            'dc_cliente',
            array(
                'dc_tipo_cliente' => 'dc_tipo_cliente',
                'dc_mercado' => 'dc_mercado_cliente'
            )
        ),
        'dc_producto' => array(
            'tb_producto',
            'dc_producto',
            array(
                'dc_tipo_producto' => 'dc_tipo_producto',
                'dc_marca' => 'dc_marca',
                'dc_linea_negocio' => 'dc_linea_negocio',
                'dc_cebe' => 'dc_cebe'
            )
        ),
        'dc_nota_credito' => array(
            'tb_nota_credito',
            'dc_nota_credito',
            array(
                'dc_nota_venta' => 'dc_nota_venta',
                'dc_orden_servicio' => 'dc_orden_servicio',
                'dc_factura' => 'dc_factura_venta',
                'dc_cliente' => 'dc_cliente',
                'dc_ejecutivo' => 'dc_ejecutivo'
            )
        ),
        'dc_nota_credito_proveedor' => array(
            'tb_nota_credito_proveedor',
            'dc_nota_credito',
            array(
                'dc_proveedor' => 'dc_proveedor'
            )
        ),
        'dc_cebe' => array(
            'tb_cebe',
            'dc_cebe',
            array(
                'dc_ceco' => 'dc_ceco'
            )
        ),
        'dc_ceco' => array(
            'tb_ceco',
            'dc_ceco',
            array(
                'dc_segmento' => 'dc_segmento'
            )
        )
    );
  }

	/**
	*	Inserta en la base de datos el detalle analítico con los parametros asignados hasta el momento
	*/
    public function DetalleAnaliticoPersist(){
        $db = $this->factory->getConnection();
        $db->stExec($this->stDetalleAnalitico);

        return $db->lastInsertId();
    }

	/**
	 *  Valida si el periodo contable está preparado para crear comprobantes en él
	 *
	 *  @throws Exception debe ser llamado en un try/catch
	 *  @param periodo dateString el periodo contable en que se intenta crear el comprobante
	 *  @param dm_tipo el tipo de comprobante, puede ser uno de los siguientes:
	 *		- 1 = Comprobante destinado a operaciones contables de compra
	 *		- 2 = Comprobante destinado a operaciones contables de venta
	 *
	 *  @returns true en caso de éxito, false de lo contrario
	 */
	private function validaPeriodo($periodo, $dm_tipo) {
		$c = $this->validaPeriodoContableBase($periodo);
		if ($c) {
		  throw new Exception("Periodo Contable Sin Permisos", $c);
		  return false;
		}

		if ($dm_tipo == 1) {
		  $fc = $this->validaPeriodoCompras($periodo);
		  if ($fc) {
			throw new Exception("Periodo Contable Sin Permisos", $c);
			return false;
		  }
		} else if ($dm_tipo == 2) {
		  $fv = $this->validaPeriodoVentas($periodo);
		  if ($fv) {
			throw new Exception("Periodo Contable Sin Permisos", $c);
			return false;
		  }
		}

		return true;
	  }

	/**
	 *  Valida periodo contable comprobante general
	 */
	private function validaPeriodoContableBase($periodo) {
		return ContabilidadStuff::getEstadoPeriodoFromDate(
						$periodo, ContabilidadStuff::FIELD_ESTADO_CT, $this->factory->getConnection(), $this->factory->getEmpresa(), $this->factory->getUserData()->dc_usuario);
	}

	/**
	 *	 Valida periodo contable comprobante de compra
	 */
	private function validaPeriodoVentas($periodo) {
		return ContabilidadStuff::getEstadoPeriodoFromDate(
						$periodo, ContabilidadStuff::FIELD_ESTADO_FV, $this->factory->getConnection(), $this->factory->getEmpresa(), $this->factory->getUserData()->dc_usuario);
	}

	/**
	 *  Valida periodo contable comprobante de venta
	 */
	private function validaPeriodoCompras($periodo) {
		return ContabilidadStuff::getEstadoPeriodoFromDate(
						$periodo, ContabilidadStuff::FIELD_ESTADO_FC, $this->factory->getConnection(), $this->factory->getEmpresa(), $this->factory->getUserData()->dc_usuario);
	}

	public function getConfiguracionContabilidad(){
		$db = $this->factory->getConnection();
		$empresa = $this->factory->getEmpresa();
		return ContabilidadStuff::getConfiguration($db, $empresa);
	}

  public function getTemplateDetalleAnalitico(){
    return array(
      "dc_cuenta_contable" => null,
      "dq_debe" => 0,
      "dq_haber" => 0,
      "dg_glosa" => null,
      "dc_detalle_financiero" => null,
      "dc_factura_compra" => null,
      "dc_factura_venta" => null,
      "dc_nota_venta" => null,
      "dc_orden_servicio" => null,
      "dc_proveedor" => null,
      "dc_tipo_proveedor" => null,
      "dc_cliente" => null,
      "dc_tipo_cliente" => null,
      "dc_orden_compra" => null,
      "dc_producto" => null,
      "dc_cebe" => null,
      "dc_ceco" => null,
      "dc_marca" => null,
      "dc_linea_negocio" => null,
      "dc_tipo_producto" => null,
      "dc_banco" => null,
      "dc_banco_cobro" => null,
      "dc_guia_recepcion" => null,
      "dc_medio_pago_proveedor" => null,
      "dc_medio_cobro_cliente" => null,
      "dc_mercado_cliente" => null,
      "dc_mercado_proveedor" => null,
      "dc_nota_credito" => null,
      "dc_nota_credito_proveedor" => null,
      "dc_segmento" => null,
      "dg_cheque" => null,
      "df_cheque" => null,
      "dc_bodega" => null,
      "dc_ejecutivo" => null
    );
  }

  public function getTemplateDetalleFinanciero(){
    return array(
      "dc_comprobante" => null,
      "dc_cuenta_contable" => null,
      "dq_debe" => 0,
      "dq_haber" => 0,
      "dg_glosa" => null
    );
  }

}

?>
