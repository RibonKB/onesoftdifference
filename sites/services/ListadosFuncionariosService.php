<?php

require_once 'inc/AbstractFactoryService.class.php';

/**
 * Description of ListadosFuncionariosService
 *
 * @author Tomás Lara Valdovinos
 * @date 23-07-2013
 */
class ListadosFuncionariosService extends AbstractFactoryService{
    
    public function getEjecutivos(){
      $db = $this->factory->getConnection();
      
      $data = $db->prepare(
                $db->select('tb_funcionario f
                             JOIN tb_cargo_funcionario tc ON f.dc_ceco = tc.dc_ceco',
                            'f.dc_funcionario, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno',
                            'f.dc_empresa = ? AND f.dm_activo = 1 AND tc.dc_modulo = 1'));
      $data->bindValue(1, $this->factory->getEmpresa(), PDO::PARAM_INT);
      $db->stExec($data);
      
      $ejecutivos = array();
      
      while($e = $data->fetch(PDO::FETCH_OBJ)):
        $ejecutivos[$e->dc_funcionario] = implode(' ', array(
            $e->dg_nombres,
            $e->dg_ap_paterno,
            $e->dg_ap_materno
        ));
      endwhile;
      
      return $ejecutivos;
      
    }
    
}

?>
