<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdministradorTareaService
 *
 * @author Eduardo
 */

require_once 'inc/AbstractFactoryService.class.php';

class AdministradorTareaService extends AbstractFactoryService {
    
    
    public function Crear_tarea($funcionario, $tipo, $fecha, $cd, $nDocto = NULL,$t=0) {
        $descripcion = $this->getDescripcion_tarea($tipo, $fecha, $nDocto);
        $lst_insert = $this->factory->coneccion('insert', 'tb_tarea', array(
            'df_fecha' => '?',
            'dg_descripcion' => '?',
            'dc_funcionario' => '?',
            'dc_tipo_tarea' => '?',
            'dm_estado' => "'P'"
                ), 0, array(
            'a::' . $fecha => 'STR',
            'b::' . $descripcion => 'STR',
            'c::' . $funcionario => 'STR',
            'd::' . $tipo => 'INT'
                ), 'dc_tarea');
        $id_anterior=$this->id_tarea($cd,$tipo);
        foreach ($cd as $campo => $dato) {
            $this->factory->coneccion('insert', 'tb_datos_tarea', array(
                'dc_tarea' => '?',
                'dg_nombre_campo' => '?',
                'dg_dato' => '?'
                    ), 0, array(
                'a::' . $lst_insert => 'INT',
                'b::' . $campo => 'STR',
                'c::' . $dato => 'STR'
            ));
            
         if($t){
             $this->Usuario_TB_copia($id_anterior->dc_tarea,$lst_insert);
         }else{
             $this->Usuario_TN_inserta($funcionario, $lst_insert);
         }
          
        }
    }

    

    public function getDescripcion_tarea($tipo, $fecha, $nDocto = NULL) {
        $desc = $this->factory->coneccion('select', 'tb_tipo_tarea', 'dg_descripcion_basica', "dc_tipo_tarea={$tipo}");
        $desc = $desc->fetch(PDO::FETCH_OBJ);
        $ex = '';
        if ($nDocto) {
            $ex = "El numero de documento es : " . $nDocto . " ,";
        }
        $descripcion = $desc->dg_descripcion_basica . $ex . " termino de tarea : " . $fecha;
        return $descripcion;
    }

    public function Actualiza_tarea($cd,$tipo) {
        $id_tarea=$this->id_tarea($cd,$tipo);
        $this->factory->coneccion('update', 'tb_tarea', array('dm_estado' => "'C'"), "dc_tarea={$id_tarea->dc_tarea}");
    }
    
    private function id_tarea($cd,$tipo){
        $campos = '';
        $datos = '';
        foreach ($cd as $campo => $dato) {
            if ($campos == '') {
                $campos = "dg_nombre_campo IN ('{$campo}' ";
            } else {
                $campos = $campos . ",'{$campo}' ";
            }
            if ($datos == '') {
                $datos = " AND dg_dato IN ('{$dato}'";
            } else {
                $datos = $datos . ",'{$dato}'";
            }
        }
        if ($campos != '') {
            $campos = $campos . ") ";
        }
        if ($datos != '') {
            $datos = $datos . ") ";
        }
        $id_tarea = $this->factory->coneccion('select', 'tb_datos_tarea dt LEFT JOIN tb_tarea t ON t.dc_tarea=dt.dc_tarea', 'dt.dc_tarea', $campos . $datos ." AND t.dc_tipo_tarea={$tipo} AND dt.dc_tarea IS NOT NULL 
                GROUP BY dt.dc_tarea 
                ORDER BY dt.dc_tarea desc
                LIMIT 1");
        $id_tarea=$id_tarea->fetch(PDO::FETCH_OBJ);
        return $id_tarea;
    }
    
    /**
     * se ocupa al crear la tarea
     * 
     * @param type $usuario
     * @param type $dc_tarea
     */
    public function Usuario_TN_inserta($usuario,$dc_tarea){
        $this->factory->coneccion('insert','tb_tarea_usuario_notificacion',
                array('dc_funcionario'=>'?','dc_tarea'=>'?'),0,
                array('a::'.$usuario=>'INT','b::'.$dc_tarea=>'INT'));
    }
    
    /**
     * 
     * Se opucupa directamente = que crear y actualizar tarea. 
     * 
     */
    public function Usuario_TB_inserta($cd,$funcionario,$tipo){
        $id_tarea=$this->id_tarea($cd,$tipo);
        $this->
                factory->
                         coneccion(
                                 'insert',
                                 'tb_tarea_usuario_notificacion',
                                 array(
                                     'dc_funcionario'=>'?',
                                     'dc_tarea'=>'?')
                                 ,0,
                                 array(
                                     'a::'.$funcionario=>'INT',
                                     'v::'.$id_tarea->dc_tarea=>'INT')
                                        );
    }
    /**
     * Se ocupa cuando se actualiza la tarea
     */
    public function Usuario_TB_copia($id_anterior,$id_actual){
        $anterior=$this->factory->coneccion('select','tb_tarea_usuario_notificacion','dc_funcionario',"dc_tarea={$id_anterior}");
        $anterior=$anterior->fetchAll(PDO::FETCH_OBJ);
        $idant[]='j';
        foreach($anterior as $d):
            if(!in_array($d->dc_funcionario, $idant)):
            $nuevo=$this->
                        factory->
                                coneccion(
                                        'insert',
                                        'tb_tarea_usuario_notificacion',
                                        array(
                                            'dc_funcionario'=>'?',
                                            'dc_tarea'=>'?')
                                        ,0,
                                        array(
                                            'a::'.$d->dc_funcionario=>'INT',
                                            'd::'.$id_actual=>'INT')
                                        );
        $idant[$d->dc_funcionario]=$d->dc_funcionario;
        endif;
        endforeach;
    }
    
    public function Verificar_aviso($user,$tipo,$campos,$datos){
        $ver=$this->factory->coneccion('select','tb_tarea_usuario_notificacion tun 
            LEFT JOIN tb_tarea t ON t.dc_tarea=tun.dc_tarea
            LEFT JOIN tb_datos_tarea dt ON dt.dc_tarea=t.dc_tarea',
                't.dc_funcionario',
                "t.dm_estado IN ('P','A') 
                    AND t.dc_tipo_tarea IN ({$tipo}) 
                        AND (dt.dg_nombre_campo IN ({$campos}) 
                            AND dt.dg_dato IN ({$datos}))
                                AND tun.dc_funcionario={$user}");
        $ver=$ver->fetch(PDO::FETCH_OBJ);
        if($ver != false){
            return true;
        }else{
            return false;
}
    }

    public function verifica_tarea_Existe($cd,$tipo){
        $campos = '';
        $datos = '';
        foreach ($cd as $campo => $dato) {
            if ($campos == '') {
                $campos = "dg_nombre_campo IN ('{$campo}' ";
            } else {
                $campos = $campos . ",'{$campo}' ";
            }
            if ($datos == '') {
                $datos = " AND dg_dato IN ('{$dato}'";
            } else {
                $datos = $datos . ",'{$dato}'";
            }
        }
        if ($campos != '') {
            $campos = $campos . ") ";
        }
        if ($datos != '') {
            $datos = $datos . ") ";
        }
        $id_tarea = $this->factory->coneccion('select', 
                'tb_datos_tarea dt 
                    LEFT JOIN tb_tarea t ON t.dc_tarea=dt.dc_tarea', 
                't.dm_estado', 
                $campos . $datos ." 
                    AND t.dc_tipo_tarea IN ({$tipo}) 
                        AND dt.dc_tarea IS NOT NULL 
                GROUP BY dt.dc_tarea 
                ORDER BY dt.dc_tarea desc
                LIMIT 2");
        $id_tarea=$id_tarea->fetchAll(PDO::FETCH_OBJ);
        $tt='si';
        if($id_tarea != false){
        foreach($id_tarea as $h){
            if($h->dm_estado=='C'){
                $tt='no';
            }
        }
        }else{
            $tt='nunca';
        }
        return $tt;
    }
}

?>
