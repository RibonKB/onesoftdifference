<?php
/**
*	Mantenedor de usuarios que utilizarán el sistema
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Creación de usuarios
</div>
<div id="other_options">
	<ul>
		<li class="oo_active"><a href="#" class="loader">Creación</a></li>
		<li><a href="sites/usuarios/ed_usuario.php" class="loader">Edición</a></li>
		<li><a href="sites/usuarios/del_usuario.php" class="loader">Eliminación</a></li>
		<li><a href="sites/usuarios/permisos_usuario.php" class="loader">Permisos</a></li>
	</ul>
</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("Para poder crear un usuario este debe ser asignado a un funcionario creado anteriormente.<br /><strong>Ingrese el <em>RUT</em> del funcionario al que le creará un usuario</strong>");
	$form->Start("sites/usuarios/proc/cr_usuario.php","busqueda_us","cValidar");
	$form->Text("RUT funcionario","func_rut",1,12,"","inputrut");
	$form->End("Buscar","searchbtn");
?>
</div>
</div>
<script type="text/javascript">
format = function(row){
	return row[1]+" ( "+row[0]+" )";
}
$("#func_rut").autocomplete('sites/proc/autocompleter/funcionario.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>
