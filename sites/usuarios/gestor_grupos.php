<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">
	Gestión de grupos de permisos
</div>
<div id="main_cont">

<br />
<br />
<div align="center" class="opciones">
	<a href="sites/usuarios/cr_grupo.php" class="loader"><img src="images/submenu/creaciongrupos.jpg" alt="Creaci&oacute;n de Grupos" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Creación</div></a>
	<a href="sites/usuarios/ed_grupo.php" class="loader"><img src="images/submenu/ediciongrupos.jpg" alt="Edici&oacute;n de Grupos" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Edición</div></a>
	<a href="sites/usuarios/del_grupo.php" class="loader"><img src="images/submenu/eliminargrupos.jpg" alt="Eliminar de Grupos" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Eliminación</div></a>
</div>

</div>