<?php
/**
*	Mantenedor de usuarios que utilizarán el sistema
**/
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//Obtiene todos los grupos creados por la empresa.
$allgroups = $db->select("tb_grupo","dc_grupo,dg_grupo","dc_empresa={$empresa} AND dm_activo='1'",array("order_by" => "dc_grupo"));
?>
<div id="secc_bar">
	Creación de grupos de permisos
</div>
<div id="other_options">
	<ul>
		<li class="oo_active"><a href="#">Creación</a></li>
		<li><a href="sites/usuarios/ed_grupo.php" class="loader">Edición</a></li>
		<li><a href="sites/usuarios/del_grupo.php" class="loader">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont" class="center">
<div class="panes">
<?php
	include_once("../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Indique los datos necesarios para poder crear el grupo</strong><br />(Los datos marcados con [*] son obligatorios)");
	$form->Start("sites/usuarios/proc/crear_grupo.php","cr_grupo");
	$form->Text("Nombre","gp_nombre",1);
	echo("<fieldset id='ug_container'>
	<label>Permisos heredados<br />
	(Se asignarán los permisos que tiene los grupos seleccionados actualmente)
	</label><br />
	<select multiple='multiple' name='us_groups' class='mselect' id='us_groups'>");
	foreach($allgroups as $g){
		echo("<option value=\"{$g['dc_grupo']}\" {$selected}>{$g['dg_grupo']}</option>");
	}
	echo("
	</select>
	</fieldset>
	<fieldset>
	<div class='info'>Permisos asignados al grupo</div>
	<div class='accordion'>");
	//Módulos de permiso
	$p_modulos = $db->select("tb_permisos_modulo","dc_modulo,dg_modulo");
	foreach($p_modulos as $mod){
		echo("<h2>{$mod['dg_modulo']}</h2>");
		$l_permisos = $db->select("tb_permiso","dc_permiso,dg_permiso","dc_modulo={$mod['dc_modulo']} AND dm_activo='1'");
		echo('<div class="pane"><br /><ul class="list_permiso">');
		foreach($l_permisos as $i => $per){
			$ind = $i%2;
			echo("
			<li class='list_permiso{$ind}'>
			<input type=\"checkbox\" value=\"{$per['dc_permiso']}\" name=\"permisos[]\" id=\"per_{$per['dc_permiso']}\" />
			{$per['dg_permiso']}
			</li>");
		}
		echo("</ul><br /></div>");
	}
	echo("</div></fieldset>");
	$form->End("Crear","addbtn");
?>
</div>
</div>
<script type="text/javascript">

	$(".accordion").tabs(".accordion > div.pane", {tabs: '> h2', effect: 'slide', initialIndex: null, toggle: true});
	var mselect_callback = function(sel){
		if(sel.attr("checked")){
			mselect_activate(sel.val());
		}else{
			mselect_deactivate(sel.val());
		}
	}
	
	mselect_activate = function(v){
		p = "";
		$("#ug_container input[value!='"+v+"']").each(function(){
			if($(this).attr("checked")){
				p += ","+$(this).val();
			}
		});
		$.getJSON("sites/usuarios/proc/group_activate.php", { asinc: "1", id: v, sel: p }, function(data){
			for(i in data){
				$("#per_"+data[i]).attr("checked",true);
			}
    	});
	}
	
	mselect_deactivate = function(v){
		p = "";
		$("#ug_container input[value!='"+v+"']").each(function(){
			if($(this).attr("checked")){
				p += ","+$(this).val();
			}
		});
		$.getJSON("sites/usuarios/proc/group_activate.php", { asinc: "1", id: v, sel: p }, function(data){
			for(i in data){
				$("#per_"+data[i]).attr("checked",false).attr("disabled",false);
			}
    	});
	}
	
	$(".mselect").multiSelect({
		selectAll: false,
		noneSelected: 'Haga click para seleccionar',
		oneOrMoreSelected: '*'
	},mselect_callback);
</script>