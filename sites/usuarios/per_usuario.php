<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la p&aacute;gina especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de usuarios
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/usuarios/cr_usuario.php" class="loader">Creación</a></li>
		<li><a href="sites/usuarios/ed_usuario.php" class="loader">Edición</a></li>
		<li><a href="sites/usuarios/del_usuario.php" class="loader">Eliminación</a></li>
		<li class="oo_active"><a href="#">Permisos</a></li>
	</ul>
</div>

<div id="main_cont">
<br /><br />
	
	<div class="panes" align="center">
	<div class="info">
		<strong>Ingrese el usuario al que desea asignar permisos.</strong><br />
		(Los criterios de búsqueda son usuario,nombre del funcionario, rut del funcionario)
	</div>
	<form id="busqueda_us" class="cValidar" action="sites/usuarios/proc/per_usuario.php" >
	<fieldset><br />
			<label>Usuario:
			<input type="text" class="inputtext" name="us_usuario" maxlength="12" size="32" id="us_usuario" required="required" />
			</label>
			<br />
			<input type="submit" value="Buscar" class="searchbtn" />
	</fieldset>
	</form>

	</div>
	
</div>
<script type="text/javascript">
format = function(row){
	return row[0]+" ( "+row[1]+" ) "+row[2];
}
$("#us_usuario").autocomplete('sites/proc/autocompleter/usuario.php',
{
formatItem: format,
minChars: 2,
width:300
}
);
</script>