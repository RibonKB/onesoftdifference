<?php
// Se comprueba que el archivo no fue accedido de manera directa por su URL. para esto se comprueba el gatillo asinc traido con Ajax
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">
	Gestión de usuarios
</div>
<div id="main_cont">

<br />
<br />
<div align="center" class="opciones">
    <?php if(check_permiso(79)): ?>
    <a href="sites/usuarios/cr_usuario.php" class="loader"><img src="images/submenu/creacionusuarios.jpg" alt="Creaci&oacute;n Usuarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Creación</div></a>
    <?php endif; ?>
    <?php if(check_permiso(82)): ?>
    <a href="sites/usuarios/ed_usuario.php" class="loader"><img src="images/submenu/edicionusuarios.jpg" alt="Edici&oacute;n Usuarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Edición</div></a>
    <?php endif; ?>
    <?php if(check_permiso(83)): ?>
    <a href="sites/usuarios/del_usuario.php" class="loader"><img src="images/submenu/eliminarusuarios.jpg" alt="Eliminar Usuarios" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Eliminación</div></a>
    <?php endif; ?>
    <?php if(check_permiso(84)): ?>
    <a href="sites/usuarios/per_usuario.php" class="loader"><img src="images/submenu/permisosusuarios.jpg" alt="Permisos de Usuario" class="tip" onMouseOver="overImg(this);" onMouseOut="restoreImg(this);" /><div class="tooltip">Permisos</div></a>
    <?php endif; ?>
	
	
	
	
</div>

</div>