<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$new_pass = strtoupper(randomString(8));
$db->update("tb_usuario",array("dg_password" => md5($new_pass),"dm_cambio_pass" => '1'),"dc_usuario={$_POST['us_id']}");

$error_man->showConfirm("Se ha generado una nueva contraseña para el usuario");
$error_man->showInfo("La nueva contraseña es <strong>{$new_pass}</strong>");

?>