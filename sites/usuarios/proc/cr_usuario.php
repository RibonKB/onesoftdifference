<?php
/**
*	Mantenedor de usuarios que utilizarán el sistema
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">Creación de usuarios</div>
<div id="other_options">
	<ul>
		<li class="oo_active"><a href="#">Creación</a></li>
		<li><a href="sites/usuarios/ed_usaurio.php" class="loader">Edición</a></li>
		<li><a href="sites/usuarios/del_usuario.php" class="loader">Eliminación</a></li>
		<li><a href="sites/usuarios/permisos_usuario.php" class="loader">Permisos</a></li>
	</ul>
</div>

<?php
$datos = $db->select(
"(SELECT * FROM tb_funcionario WHERE dc_empresa = {$empresa} AND dg_rut = '{$_POST['func_rut']}' AND dm_activo = '1') f
LEFT JOIN (SELECT * FROM tb_ceco WHERE dc_empresa={$empresa}) cc ON f.dc_ceco = cc.dc_ceco",
"dc_funcionario,dg_nombres,dg_ap_paterno,dg_ap_materno,dg_ceco");

if(!count($datos)){
	$error_man->showErrorRedirect("El funcionario especificado no se ha encontrado","sites/usuarios/cr_usuario.php");
}

$datos = $datos[0];

if(count($db->select("tb_usuario","dc_usuario","dc_funcionario={$datos['dc_funcionario']} AND dm_activo='1'"))){
	$error_man->showErrorRedirect("El funcionario especificado ya tiene un usuario asignado","sites/usuarios/cr_usuario.php");	
}

include("crear_autouser.php");

?>

<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Complete los datos para la creación del usuario</strong><br />
	(Los campos marcados con [*] son obligatorios)<br />
	(La contraseña es generada automáticamente en el registro)");
	$form->Start("sites/usuarios/proc/crear_usuario.php","us_crear");
	$form->Section();
	$form->Text("Usuario [*] (Generado de manera automática)","us_username",1,255,$generated_user);
	$form->EndSection();
	$form->Section();
	echo("
	<fieldset>
	<div class='right'>
	<img src='images/avatar/foto_funcionario.php?fid={$datos['dc_funcionario']}' alt='Avatar' style='width:80px;border:1px solid #DDD;padding:3px;' />
	</div>
	<div class='title' style='width:350px;'>Datos del funcionario</div>
	<label>Nombre:<br /></label>
	'{$datos['dg_nombres']} {$datos['dg_ap_paterno']} {$datos['dg_ap_materno']}'
	<br /><br />
	<label>Centro costo:<br /></label>
	{$datos['dg_ceco']}
	</fieldset>");
	$form->EndSection();
	echo("<hr class='clear' />");
	$form->Hidden("func_id",$datos['dc_funcionario']);
	$form->End('Registrar nuevo usuario','addbtn');
?>
</div>
</div>