<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$usuario =& $_POST['us_id'];

if(count($db->select("tb_usuario","1","dc_usuario={$usuario} AND dm_activo = '0'"))){
	$error_man->showAviso("El usuario ya ha sido eliminado");
}
else{
	$db->update("tb_usuario",array("dm_activo" => '0'),"dc_usuario = {$usuario}");
	$error_man->showConfirm("El usuario ha sido eliminado correctamente");
}
?>