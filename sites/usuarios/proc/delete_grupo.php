<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

$grupo =& $_POST['gp_id'];

if(count($db->select("tb_grupo","1","dc_grupo={$grupo} AND dm_estado = '0'"))){
	$error_man->showAviso("El grupo ya ha sido eliminado");
}
else{
	$db->update("tb_grupo",array("dm_estado" => '0'),"dc_grupo = {$grupo}");
	$error_man->showConfirm("El grupo ha sido eliminado correctamente");
}
?>