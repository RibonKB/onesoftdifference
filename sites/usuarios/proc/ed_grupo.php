<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de grupos de permisos
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/usuarios/cr_grupo.php" class="loader">Creación</a></li>
		<li class="oo_active"><a href="#">Edición</a></li>
		<li><a href="sites/usuarios/del_grupo.php" class="loader">Eliminación</a></li>
	</ul>
</div>

<?php

$datos = $db->select("tb_grupo","dc_grupo,dg_grupo","dg_grupo='{$_POST['gp_grupo']}' AND dc_empresa={$empresa} AND dm_activo='1'");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el grupo especificado","sites/usuarios/ed_grupo.php");
}

$datos = $datos[0];

//Los mudulos de permisos
$t_modulos = $db->select("tb_permisos_modulo","dc_modulo,dg_modulo");
//Los permisos del grupo
$g_permisos_aux = $db->select("tb_grupo_permisos","dc_permiso","dc_grupo = {$datos['dc_grupo']}");
$g_permisos = array();
foreach($g_permisos_aux as $g){
	$g_permisos[] = $g['dc_permiso'];
}
unset($g_permisos_aux);

?>

<div id="main_cont">
<br /><br />
	
	<div class="panes" align="center">
	<div class="info">
		<strong>Indique los datos del grupo que desea editar</strong><br />
		(Los datos marcados con [*] son obligatorios)
	</div>
	
	<form action="sites/usuarios/proc/editar_grupo.php" class="validar" id="cr_grupo">
		<fieldset>
			<div class="center">
				<label>Nombre del grupo [*]<br />
					<input type="text" name="gp_nombre" value="<?=$datos['dg_grupo'] ?>" class="inputtext" size="40" maxlength="255" required="required" />
				</label><br /><br />
				<fieldset>
					<label>Permisos asignados al grupo</label>
					<div class="accordion">
					<?php
						//Módulos de permiso
						$p_modulos = $db->select("tb_permisos_modulo","dc_modulo,dg_modulo");
						foreach($p_modulos as $mod){
						 echo("<h2>{$mod['dg_modulo']}</h2>");
						 $l_permisos = $db->select("tb_permiso","dc_permiso,dg_permiso","dc_modulo={$mod['dc_modulo']} AND dm_activo='1'");
						 echo('<div class="pane"><br /><ul class="list_permiso">');
						 foreach($l_permisos as $i => $per){
							$ind = $i%2;
							if(in_array($per['dc_permiso'],$g_permisos)){
								$active = "checked=\"checked\"";
							}
							else{
								$active = "";
							}
							echo("
							<li class='list_permiso{$ind}'>
							<input type=\"checkbox\" value=\"{$per['dc_permiso']}\" name=\"permisos[]\" id=\"per_{$per['dc_permiso']}\" {$active} />
							{$per['dg_permiso']}
							</li>");
							}
							echo("</ul><br /></div>");
						}
					?>
					</div>
				</fieldset>
				<input type="hidden" name="gp_id" value="<?=$datos['dc_grupo'] ?>" />
				<input type="submit" class="editbtn" value="Actualizar los datos" />
			</div>
		</fieldset>
	</form>
	<div id="cr_grupo_res"></div>
	</div>
</div>
<script type="text/javascript">

	$(".accordion").tabs(".accordion > div.pane", {tabs: '> h2', effect: 'slide', initialIndex: null, toggle: true});
	var mselect_callback = function(sel){
		if(sel.attr("checked")){
			mselect_activate(sel.val());
		}else{
			mselect_deactivate(sel.val());
		}
	}
	
	mselect_callback = function(){
		
	}
</script>