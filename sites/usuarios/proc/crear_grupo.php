<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['permisos'])){
	$error_man->showWarning("Debe asignar permisos al grupo");
	exit();
}

$g_nombre = ucwords(strtolower($_POST['gp_nombre']));

if(count($db->select("tb_grupo","1","dg_grupo='{$g_nombre}' AND dc_empresa={$empresa}"))){
	$error_man->showWarning("Ya existe un grupo con el nombre especificado");
	exit();
}

$id_grupo = $db->insert("tb_grupo",
array(
"dg_grupo" => $g_nombre,
"dc_empresa" => $empresa
));

$permisos = "";

foreach($_POST['permisos'] as $per){
	$permisos .= ",({$id_grupo},{$per},NOW())";
}

$permisos = substr($permisos,1);

$db->query("INSERT INTO tb_grupo_permisos(dc_grupo,dc_permiso,df_creacion) VALUES {$permisos}");

$error_man->showConfirm("Se ha creado el grupo <strong>{$g_nombre}</strong> y se le han asignado sus permisos");

?>