<?php
/**
*	Mantenedor de usuarios que utilizarán el sistema
**/
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>
<div id="secc_bar">
Permisos de usuarios
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/usuarios/cr_usuario.php" class="loader">Creación</a></li>
		<li><a href="sites/usuarios/ed_usuario.php" class="loader">Edición</a></li>
		<li><a href="sites/usuarios/del_usuario.php" class="loader">Eliminación</a></li>
		<li class="oo_active"><a href="#">Permisos</a></li>
	</ul>
</div>
<?php
$datos = $db->select("(SELECT * FROM tb_usuario WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_usuario = '{$_POST['us_usuario']}') us
JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa={$empresa} AND dm_activo = '1') f ON us.dc_funcionario = f.dc_funcionario
LEFT JOIN (SELECT * FROM tb_ceco WHERE dc_empresa={$empresa} AND dm_activo = '1') cc ON f.dc_ceco = cc.dc_ceco",
	"us.dg_usuario, us.dc_usuario,f.dc_funcionario, f.dg_rut, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, cc.dg_ceco");
	
if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el usuario especificado","sites/usuarios/per_usuario.php");
}

$datos = $datos[0];

//Obtiene todos los grupos creados por la empresa.
$allgroups = $db->select("tb_grupo","dc_grupo,dg_grupo","dc_empresa={$empresa} AND dm_activo='1'",array("order_by" => "dc_grupo"));
//Obtiene los grupos a los que pertenece el usuario
$usergroups_a = $db->select("tb_usuario_grupo","dc_grupo","dc_usuario={$datos['dc_usuario']}");
//Almacenará los grupos separados por , (coma)
$str_usergroups = "";
foreach($usergroups_a as $g){
	$usergroups[] = $g['dc_grupo'];
	$str_usergroups .= ",{$g['dc_grupo']}";
}
?>

<div id="main_cont">
<br /><br />
	<div class="panes">
	<div id="us_permisos_res"></div>
	<form action="sites/usuarios/proc/permisos_usuario.php" id="us_permisos" class="validar" >
	<table align="center" cellpadding="0" cellspacing="0">
	<tbody>
	<tr>
	<td align="center" class="lite">
	<ul id="tabs">
		<li><a href="#" class="current">Grupo</a></li>
		<li><a href="#">Permisos individuales</a></li>
	</ul>
	</td>
	</tr>
	<tr>
	<td>
		<div class="tabpanes">
		
			<div>
				<div class="info">
					Indique los grupos al cual pertenecerá el usuario, se heredarán todos los permisos asignados a los grupos.
				</div>
				<fieldset id="ug_container">
					<div class="left">
					<select multiple="multiple" name="us_groups" class="mselect" id="us_groups">
						<?php
							foreach($allgroups as $group){
								$selected = in_array($group['dc_grupo'],$usergroups)?"selected=selected":"";
								echo("<option value=\"{$group['dc_grupo']}\" {$selected}>{$group['dg_grupo']}</option>");
							}
						?>
					</select>
					</div>
					
					<div class="right" style="width:320px;">
					<fieldset>
						<div class="title">Datos del usuario</div>
						<div class="right">
						 <img src="images/avatar/foto_funcionario.php?fid=<?=$datos['dc_funcionario'] ?>" alt="<?=$datos['dg_usuario'] ?>" style="width:80px;border:1px solid #DDD;padding:3px;" />
						</div>
						<div class="left">
							<label>Usuario:<br /></label>
							<?=$datos['dg_usuario'] ?>
							<br /><br />
							<label>Nombre funcionario:<br /></label>
							<?="{$datos['dg_nombres']} {$datos['dg_ap_paterno']} {$datos['dg_ap_materno']}" ?>
							<br /><br />
							<label>Centro costo:<br /></label>
							<?=$datos['dg_ceco'] ?>
						</div>
					</fieldset>
					</div>
				</fieldset>
			</div>
			
			<div>
				<div class="info">
					Indique los permisos individuales del usuario, para dar permisos extra de los que dan los grupos.
				</div>
				<fieldset>
				
				<div class="left" style="width:350px;">
				<div class="accordion">
				
				<?php
					//Módulos de permiso
					$l_modulos = $db->select("tb_permisos_modulo","dc_modulo,dg_modulo");
					//Permisos asignados a los grupos en los que está el usuario.
					$l_pg_permisos = $db->select("tb_grupo_permisos","distinct dc_permiso","dc_grupo in (''{$str_usergroups})");
					//Permisos individuales sobre el usuario.
					$l_ind_permisos = $db->select("tb_usuario_permisos","dc_permiso","dc_usuario={$datos['dc_usuario']}");
					//parsear ambos resultados de permisos a un array numerico
					$pg_permisos = array();
					$ind_permisos = array();
					foreach($l_pg_permisos as $per){
						$pg_permisos[] = $per['dc_permiso'];
					}
					foreach($l_ind_permisos as $per){
						$ind_permisos[] = $per['dc_permiso'];
					}
					foreach($l_modulos as $mod){
						echo("<h2>{$mod['dg_modulo']}</h2>");
						$l_permisos = $db->select("tb_permiso","dc_permiso,dg_permiso","dc_modulo={$mod['dc_modulo']} AND dm_activo='1'");
						echo('<div class="pane"><br /><ul class="list_permiso">');
						foreach($l_permisos as $i => $per){
							$ind = $i%2;
							//si pertenece a los permisos individuales se debe marcar como activo, en caso contrario no se marca.
							$l_enabled = in_array($per['dc_permiso'],$ind_permisos)?"checked=\"checked\"":"";
							//si el permiso es dado por el grupo entonces se debe marcar como activa y además se deshabilita.
							//porque los permisos dados por grupo no son quitables.
							$l_enabled = in_array($per['dc_permiso'],$pg_permisos)?"checked=\"checked\" disabled=\"disabled\"":$l_enabled;
							echo("
							<li class='list_permiso{$ind}'>
							<input type=\"checkbox\" value=\"{$per['dc_permiso']}\" name=\"permisos[]\" id=\"per_{$per['dc_permiso']}\" {$l_enabled} />
							{$per['dg_permiso']}
							</li>");
						}
						echo("</ul><br /></div>");
					}
				?>
					
				</div>
				</div>
				
				<div class="right" style="width:320px;">
					<fieldset>
						<div class="title">Datos del usuario</div>
						<div class="right">
							<img src="images/avatar/foto_funcionario.php?fid=<?php echo($datos['dc_funcionario']) ?>" alt="<?php echo($datos['dg_usuario']); ?>" style="width:80px;border:1px solid #DDD;padding:3px;" />
						</div>
						<div class="left">
							<label>Usuario:<br /></label>
							<?php echo($datos['dg_usuario']); ?>
							<br /><br />
							<label>Nombre funcionario:<br /></label>
							<?php echo("{$datos['dg_nombres']} {$datos['dg_ap_paterno']} {$datos['dg_ap_materno']}"); ?>
							<br /><br />
							<label>Centro costo:<br /></label>
							<?=$datos['dg_ceco'] ?>
						</div>
					</fieldset>
				</div>
				
				<br />
				<hr class="clear" />
				
				<input type="checkbox" onclick='return false;' />Sin el permiso activado<br />
				<input type="checkbox" checked="checked" onclick='return false;' />Con el permiso activado<br />
				<input type="checkbox" checked="checked" disabled="disabled" readonly="readonly" />Con el permiso habilitado por el grupo
					
				</fieldset>
				
			</div>
		<input type="hidden" name="us_id" value="<?=$datos['dc_usuario'] ?>" />
		<input type="submit" class="editbtn" value="Guardar" />
		</div>
		
	
	</td>
	</tr>
	</tbody>
	</table>
	</form>

</div></div>
<script type="text/javascript">
	$(".accordion").tabs(".accordion > div.pane", {tabs: '> h2', effect: 'slide', initialIndex: null, toggle: true});
	var mselect_callback = function(sel){
		if(sel.attr("checked")){
			mselect_activate(sel.val());
		}else{
			mselect_deactivate(sel.val());
		}
	}
	
	mselect_activate = function(v){
		p = "";
		$("#ug_container input[value!='"+v+"']").each(function(){
			if($(this).attr("checked")){
				p += ","+$(this).val();
			}
		});
		$.getJSON("sites/usuarios/proc/group_activate.php", { asinc: "1", id: v, sel: p }, function(data){
			for(i in data){
				$("#per_"+data[i]).attr("checked",true).attr("disabled",true);
			}
    	});
	}
	
	mselect_deactivate = function(v){
		p = "";
		$("#ug_container input[value!='"+v+"']").each(function(){
			if($(this).attr("checked")){
				p += ","+$(this).val();
			}
		});
		$.getJSON("sites/usuarios/proc/group_activate.php", { asinc: "1", id: v, sel: p }, function(data){
			for(i in data){
				$("#per_"+data[i]).attr("checked",false).attr("disabled",false);
			}
    	});
	}
	
	$(".mselect").multiSelect({
		selectAll: false,
		noneSelected: 'Haga click para seleccionar',
		oneOrMoreSelected: '*'
	},mselect_callback);
</script>