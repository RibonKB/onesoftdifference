<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de usuarios
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/usuarios/cr_usuario.php" class="loader">Creación</a></li>
		<li class="oo_active"><a href="#">Edición</a></li>
		<li><a href="sites/usuarios/del_usuario.php" class="loader">Eliminación</a></li>
		<li><a href="sites/usuarios/permisos_usuario.php" class="loader">Permisos</a></li>
	</ul>
</div>

<?php

$datos = $db->select("(SELECT * FROM tb_usuario WHERE dc_empresa={$empresa} AND dm_activo='1' AND dg_usuario = '{$_POST['us_usuario']}') us
JOIN tb_funcionario f ON us.dc_funcionario = f.dc_funcionario AND f.dc_empresa = {$empresa} AND f.dm_activo = 1
LEFT JOIN tb_ceco cc ON f.dc_ceco = cc.dc_ceco AND cc.dc_empresa={$empresa} AND cc.dm_activo = '1'",
	"f.dc_funcionario, us.dg_usuario, us.dc_usuario, f.dg_rut, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, cc.dg_ceco");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el usuario especificado","sites/usuarios/ed_usuario.php");
}

$datos = $datos[0];

?>
<div id="main_cont">
<div class="panes">
<?php
	include_once("../../../inc/form-class.php");
	$form = new Form($empresa);
	
	$form->Header("<strong>Genere una nueva contraseña para el usuario</strong>");
	$form->Start("sites/usuarios/proc/editar_usuario.php","ed_usuario");
	echo("<div class='title'>Datos del usuario</div>");
	$form->Section();
	echo("<img src='images/avatar/foto_funcionario.php?fid={$datos['dc_funcionario']}' alt='{$datos['dg_usuario']}' style='width:120px;border:1px solid #DDD;padding:3px;' />");
	$form->EndSection();
	$form->Section();
	echo("
	<label>Usuario:<br /></label>
		{$datos['dg_usuario']}
		<br /><br />
	<label>Nombre funcionario:<br /></label>
		{$datos['dg_nombres']} {$datos['dg_ap_paterno']} {$datos['dg_ap_materno']}
		<br /><br />
	<label>Centro costo:<br /></label>
		{$datos['dg_ceco']}
	");
	$form->EndSection();
	$form->Hidden("us_id",$datos['dc_usuario']);
	$form->End("Generar una nueva contraseña","editbtn");
?>
</div>
</div>