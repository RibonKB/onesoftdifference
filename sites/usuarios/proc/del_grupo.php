<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de grupos de permisos
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/usuarios/cr_grupo.php" class="loader">Creación</a></li>
		<li><a href="sites/usuarios/ed_grupo.php" class="loader">Edición</a></li>
		<li class="oo_active"><a href="#">Eliminación</a></li>
	</ul>
</div>

<?php

$datos = $db->select("tb_grupo","dc_grupo,dg_grupo","dg_grupo='{$_POST['gp_grupo']}' AND dc_empresa={$empresa} AND dm_estado='1'");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el usuario especificado","sites/usuarios/del_usuario.php");
}
$datos = $datos[0];

?>
<div id="main_cont">
<br /><br />
	<div class="panes">
	<form action="sites/usuarios/proc/delete_grupo.php" class="validar" id="del_usuario">
	<fieldset>
		<div class="center">
			¿Está seguro de querer eliminar el grupo <strong><?=$datos['dg_grupo'] ?></strong>?<br />
				<input type="hidden" name="gp_id" value="<?=$datos['dc_grupo'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
		</div>
	</fieldset>
	</form>
	<div id="del_usuario_res"></div>
	</div>
</div>