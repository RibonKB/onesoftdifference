<?php
	define("MAIN",1);
	require_once("../../../inc/global.php");
	if(!isset($_POST['asinc'])){
		$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
		$error_man->show_fatal_error("Acceso Denegado",$options);
	}
		$us_id = $_POST['us_id'];
		$db->query("DELETE FROM tb_usuario_grupo WHERE dc_usuario={$us_id}");
		$groups = "";
		if(isset($_POST['us_groups'])){
			foreach($_POST['us_groups'] as $g){
				$groups .= ",({$g},{$us_id},NOW())";
			}
			$groups = substr($groups,1);
			$db->query("INSERT INTO tb_usuario_grupo(dc_grupo,dc_usuario,df_creacion) VALUES {$groups}");
		}
		
		
		//Traigo los permisos individuales antiguos.
		$aux = $db->select("tb_usuario_permisos","dc_permiso","dc_usuario={$us_id}");
		$antiguos = array();
		foreach($aux as $p){
			$antiguos[] = $p['dc_permiso'];
		}
		
		if(isset($_POST['permisos'])){
			foreach($_POST['permisos'] as $p){
				//si el permiso actual no está en los antiguos entonces se inserta
				if(!in_array($p,$antiguos)){
					$db->insert("tb_usuario_permisos",
					array(
						"dc_permiso" => $p,
						"dc_usuario" => $us_id,
						"df_creacion" => "NOW()"
					));
				}
	
			}
		}
		
		foreach($antiguos as $p){
			//Si el permiso antiguo antiguo no está entre los nuevos entonces se desmarcó, se borra
			if(!in_array($p,$_POST['permisos'])){
				$db->query("DELETE FROM tb_usuario_permisos WHERE dc_permiso={$p} AND dc_usuario={$us_id}");
			}
		}
		
		$error_man->showConfirm("Se han cambiado los permisos del usuario correctamente");
?>