<?php 
/**
*	Genera un nombre de usuario al azar tomando como referencia los nombres, apellido paterno y apellido materno enviado en
*	$_GET['nombres'], $_GET['ap_paterno'] y $_GET['ap_materno']
*	comprueba para todo si el nombre de usuario generado no está ya en uso.
**/
	
	function getAlUser($n,$ap,$am){
	global $db,$empresa;
		
		for($i=1;$i<strlen($n);$i++){
			$al = substr($n,0,$i).$ap;
			if(!count($db->select(
			"tb_usuario",
			"dg_usuario",
			"dc_empresa = {$empresa} AND dg_usuario='".htmlentities($al,ENT_QUOTES,'UTF-8')."'"))){
				return $al;
			}else{
				for($i=1;$i<strlen($am);$i++){
				$al .= ".".substr($am,0,$i);
					if(!count($db->select(
					"tb_usuario",
					"dg_usuario",
					"dc_empresa = {$empresa} AND dg_usuario='".htmlentities($al,ENT_QUOTES,'UTF-8')."'"))){
						return $al;
					}
				}
			}
		}
		return "";
		
	}
	$n = strtolower(reemplazar_sp_chars(str_replace(" ","",$datos['dg_nombres'])));
	$ap = strtolower(reemplazar_sp_chars(str_replace(" ","",$datos['dg_ap_paterno'])));
	$am = strtolower(reemplazar_sp_chars(str_replace(" ","",$datos['dg_ap_materno'])));
	$generated_user =  getAlUser($n,$ap,$am);
	
?>