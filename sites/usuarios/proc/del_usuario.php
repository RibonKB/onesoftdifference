<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de usuarios
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/usuarios/cr_usuario.php" class="loader">Creación</a></li>
		<li><a href="sites/usuarios/ed_usuario.php" class="loader">Edición</a></li>
		<li class="oo_active"><a href="#">Eliminación</a></li>
		<li><a href="sites/usuarios/permisos_usuario.php" class="loader">Permisos</a></li>
	</ul>
</div>

<?php

$datos = $db->select("(SELECT * FROM tb_usuario WHERE dg_usuario = '{$_POST['us_usuario']}' AND dm_activo = '1') us
JOIN (SELECT * FROM tb_funcionario WHERE dc_empresa = {$empresa} AND dm_activo = '1') f ON us.dc_funcionario = f.dc_funcionario
LEFT JOIN (SELECT * FROM tb_ceco WHERE dc_empresa={$empresa}) cc ON f.dc_ceco = cc.dc_ceco",
	"us.dg_usuario, us.dc_usuario,f.dc_funcionario, f.dg_rut, f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno, cc.dg_ceco");

if(!count($datos)){
	$error_man->showErrorRedirect("No se encontró el usuario especificado","sites/usuarios/del_usuario.php");
}
$datos = $datos[0];

?>

<div id="main_cont">
	<div class="panes">
	<form action="sites/usuarios/proc/delete_usuario.php" class="validar" id="del_usuario">
	<fieldset>
		<div class="title">Datos del usuario</div>
		<div class="left">
			<img src="images/avatar/foto_funcionario.php?fid=<?=$datos['dc_funcionario'] ?>" alt="<?=$datos['dg_usuario'] ?>" style="width:120px;border:1px solid #DDD;padding:3px;" />
		</div>
		
		<div class="left">
			<label>Usuario:<br /></label>
			<?=$datos['dg_usuario'] ?>
			<br /><br />
			<label>Nombre funcionario:<br /></label>
			<?="{$datos['dg_nombres']} {$datos['dg_ap_paterno']} {$datos['dg_ap_materno']}" ?>
			<br /><br />
			<label>Centro costo:<br /></label>
			<?=$datos['dg_ceco'] ?>
		</div>
		<hr class="clear" />
		<div class="center">
			¿Está seguro de querer eliminar el usuario?<br />
				<input type="hidden" name="us_id" value="<?=$datos['dc_usuario'] ?>" />
				<input type="submit" class="delbtn" value="Eliminar" />
		</div>
	</fieldset>
	</form>
	<div id="del_usuario_res"></div>
	</div>
</div>