<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

if(!isset($_POST['permisos'])){
	$error_man->showWarning("Debe asignar permisos al grupo");
	exit();
}

$nombre = ucwords(strtolower($_POST['gp_nombre']));
$id_grupo =& $_POST['gp_id'];

if(count($db->select("tb_grupo","1","dg_grupo='{$nombre}' AND dc_grupo <> {$id_grupo} AND dc_empresa={$empresa}"))){
	$error_man->showWarning("Ya existe un grupo con el nombre especificado");
	exit();
}

$db->update("tb_grupo",array("dg_grupo" => $nombre),"dc_grupo = {$id_grupo}");

//Los permisos actuales del grupo
$g_permisos = $db->select("tb_grupo_permisos","dc_permiso","dc_grupo={$id_grupo}");
$borrar = "";
foreach($g_permisos as $g){
	if(!in_array($g['dc_permiso'],$_POST['permisos'])){
		$borrar .= ",{$g['dc_permiso']}";
	}
}
unset($g_permisos);

$permisos = "";

foreach($_POST['permisos'] as $per){
	$permisos .= ",({$id_grupo},{$per},NOW())";
}

$permisos = substr($permisos,1);

$db->query("DELETE FROM tb_grupo_permisos WHERE dc_permiso IN (''{$borrar})");
$db->query("INSERT IGNORE INTO tb_grupo_permisos (dc_grupo,dc_permiso,df_creacion) VALUES {$permisos}");

$error_man->showConfirm("Se han actualizado los datos y permisos del grupo <strong>{$nombre}</strong>");


?>