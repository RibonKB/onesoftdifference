<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_GET['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

//El identificador del grupo seleccionado en el <select>
$id = $_GET['id'];
//La lista separada por comas de los grupos no seleccionados
$o_groups = "''".$_GET['sel'];

$results = $db->select("tb_grupo_permisos","dc_permiso","dc_grupo = {$id} AND dc_permiso NOT IN (
			SELECT dc_permiso FROM tb_grupo_permisos WHERE dc_grupo IN ({$o_groups})
)");

$return = array();

foreach($results as $r){
	$return[] = $r['dc_permiso'];
}

echo json_encode($return);

?>