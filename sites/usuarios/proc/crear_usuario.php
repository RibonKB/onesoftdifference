<?php
define("MAIN",1);
require_once("../../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

	$user = reemplazar_sp_chars($_POST['us_username']);
	
	if(count($db->select("tb_usuario","1","dg_usuario='{$user}' AND dc_empresa={$empresa}"))){
		$error_man->showWarning("Su empresa ya tiene registrado ese nombre de usuario");
		exit();
	}
	
	$gen_pass = randomString(8);
	
	$db->insert("tb_usuario",
	array(
		"dc_funcionario" => $_POST['func_id'],
		"dc_empresa" => $empresa,
		"df_creacion" => "NOW()",
		"dg_usuario" => $user,
		"dg_password" => md5($gen_pass)
	));
	
	$error_man->showConfirm("Se ha creado el usuario y asignado al funcionario correctamente.<br />
	La contraseña para el nuevo usuario es <b>{$gen_pass}</b>");

?>