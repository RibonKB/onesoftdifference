<?php
define("MAIN",1);
require_once("../../inc/global.php");
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}
?>

<div id="secc_bar">
	Edición de grupos de permisos
</div>
<div id="other_options">
	<ul>
		<li><a href="sites/usuarios/cr_grupo.php" class="loader">Creación</a></li>
		<li><a href="sites/usuarios/ed_grupo.php" class="loader">Edición</a></li>
		<li class="oo_active"><a href="#">Eliminación</a></li>
	</ul>
</div>

<div id="main_cont">
<br /><br />
	
	<div class="panes" align="center">
		<div class="info">
			<strong>Ingrese el grupo que desea eliminar.</strong><br />
			(Los criterios de búsqueda son el nombre del grupo)
		</div>
		<form id="busqueda_us" class="cValidar" action="sites/usuarios/proc/del_grupo.php" >
		<fieldset><br />
				<label>Grupo:
				<input type="text" class="inputtext" name="gp_grupo" maxlength="12" size="32" id="gp_grupo" required="required" />
				</label>
				<br />
				<input type="submit" value="Buscar" class="searchbtn" />
		</fieldset>
		</form>
	</div>
	
</div>
<script type="text/javascript">
$("#gp_grupo").autocomplete('sites/proc/autocompleter/grupo.php',
{
minChars: 2,
width:300
}
);
</script>