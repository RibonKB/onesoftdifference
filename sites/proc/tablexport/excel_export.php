<?php

function numberDetect($text){
	if(preg_match("/^-?[\.\d]+,?\d*$/",$text)){
		return str_replace(',','.',str_replace('.','',$text));
	}
	if(preg_match("/^-?[,\d]+\.?\d*$/",$text)){
		return str_replace(',','.',str_replace('.','',$text));
	}
	return $text;
}

function attrValue(SimpleXMLElement $element, $attr){
	foreach($element->attributes() as $a => $val):
		if($a == $attr):
			return $val;
		endif;
	endforeach;
	
	return false;
}

error_reporting(E_ALL);
ini_set('memory_limit','1024M');

$cacheid = $_GET['cache'];

require_once('../../../inc/PHPExcel.php');

$textData = html_entity_decode(file_get_contents("cache/{$cacheid}.xml"),ENT_XHTML);

$textData = strip_tags($textData,'<table><thead><tbody><tfoot><tr><td><th>');
$textData = str_replace('&','&amp;',$textData);
$textData = str_replace('°','o',$textData);
$textData = str_replace('®','',$textData);
//$textData = utf8_decode($textData);
$textData = str_replace('colspan=','colspan2=',$textData);
$textData = preg_replace('/[[:blank:]]([a-zA-Z]+=".*?")+>/','>',$textData);
$textData = preg_replace('#<t(d|h)>(.*?)<\/t(d|h)>#si','<t$1><![CDATA[$2]]></t$3>',$textData);
$textData = str_replace('<?xml ?>','<?xml version="1.0" encoding="UTF-8" ?>',$textData);
$textData = str_replace('ñ','n',$textData);
//$textData = str_replace('"','',$textData);

//$test = fopen("cache/00test.xml","w");
//fwrite($test, $textData);

try{
libxml_use_internal_errors(true);
$table = new SimpleXMLElement($textData, LIBXML_NOCDATA);
}catch(Exception $e){
	echo $e->getMessage();
	var_dump(libxml_get_errors());
	exit;
}

$excel = new PHPExcel();
$excel->getProperties()->setCreator("Onesoft Pymerp")
						->setLastModifiedBy("Onesoft Pymerp")
						->setTitle("Informe $cacheid")
						->setSubject("Informe $cacheid")
						->setCategory("Informes");
						
$letras = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

//Contador de filas, inicia en A1
$row = 1;

if(isset($table->thead)):
	foreach($table->thead->tr as $tr):
	$col = 0;
		foreach($tr->th as $th):
			$excel->setActiveSheetIndex(0)
					->SetCellValue($letras[$col].$row, trim(strip_tags($th)));
			$col++;
		endforeach;
	$row++;
	endforeach;
endif;

foreach($table->tbody->tr as $tr):
	$col = 0;
	foreach($tr->td as $td):
		$excel->setActiveSheetIndex(0)
				->SetCellValue($letras[$col].$row, numberDetect(trim(strip_tags($td->asXML()))));
		$col++;
	endforeach;
	$row++;
endforeach;

$log = fopen('temp.txt','w');

if(isset($table->tfoot)):
	foreach($table->tfoot->tr as $tr):
	$col = 0;
		foreach($tr->th as $th):
			$col_coord = $letras[$col].$row;
			$colspan = attrValue($th, 'colspan2');
			
			if($colspan != false):
				$colspan = intval($colspan);
				$from = $col_coord;
				$to = $letras[$col+$colspan-1].$row;
				
				$excel->getActiveSheet()->mergeCells($from.':'.$to);
				
			endif;
		
			$excel->setActiveSheetIndex(0)
					->SetCellValue($letras[$col].$row, trim(strip_tags($th)));
			$col++;
			
			if($colspan != false){
				$col += $colspan-1;
			}
		endforeach;
	$row++;
	endforeach;
endif;

foreach($letras as $letra):
	$excel->getActiveSheet()->getColumnDimension($letra)->setAutoSize(true);
endforeach;


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$cacheid.'.xls"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');

$objWriter->save('php://output');
exit;
?>
