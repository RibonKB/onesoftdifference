<?php

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	/*$ind_cot_permisos = $db->select('tb_indicador_cot_permisos','dg_permisos',"dc_usuario={$idUsuario}");
	$cond = '';
	if(!count($ind_cot_permisos)){
		$cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
	}else{
		if($ind_cot_permisos[0]['dg_permisos'] != 'ALL'){
			$cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
		}
	}*/
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("(SELECT * FROM tb_factura_venta WHERE dc_empresa = {$empresa} AND dm_nula = '0') fv
	JOIN tb_cliente cl ON cl.dc_cliente = fv.dc_cliente",
	"fv.dc_factura,fv.dq_factura,fv.dq_folio,fv.dq_neto,cl.dg_razon",
	"fv.dq_factura LIKE '{$q}%' OR fv.dq_folio LIKE '{$q}%' OR cl.dg_razon LIKE '%{$q}%'");
	
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		$r['dq_neto'] = moneda_local($r['dq_neto']);
		echo("{$r['dq_factura']}|({$r['dq_folio']}) {$r['dq_neto']}|{$r['dg_razon']}|{$r['dc_factura']}\n");
	}
	
?>