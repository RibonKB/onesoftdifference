<?php
/**
*	Realiza una búsqueda de grupos de permiso segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda de clientes.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q = htmlentities($_GET['q'],ENT_QUOTES,'UTF-8');
	
	//la cantidad máxima de resultados a obtener
	$limit = $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("tb_grupo","distinct dg_grupo,dc_grupo","dg_grupo LIKE '%{$q}%' AND dc_empresa={$empresa} AND dm_activo='1'",array("limit" => $limit));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_grupo']}|{$r['dc_grupo']}\n");
	}
	
?>