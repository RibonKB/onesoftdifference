<?php

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	/*$ind_cot_permisos = $db->select('tb_indicador_cot_permisos','dg_permisos',"dc_usuario={$idUsuario}");
	$cond = '';
	if(!count($ind_cot_permisos)){
		$cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
	}else{
		if($ind_cot_permisos[0]['dg_permisos'] != 'ALL'){
			$cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
		}
	}*/
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("(SELECT * FROM tb_orden_compra WHERE dc_empresa = {$empresa} AND dm_nula = 0) o
	JOIN (SELECT * FROM tb_proveedor WHERE dc_empresa={$empresa}) pr ON pr.dc_proveedor = o.dc_proveedor",
	"o.dc_orden_compra,o.dq_orden_compra,o.dq_neto,pr.dg_razon",
	"o.dq_orden_compra LIKE '{$q}%' OR pr.dg_razon LIKE '%{$q}%'");
	
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		$r['dq_neto'] = moneda_local($r['dq_neto']);
		echo("{$r['dq_orden_compra']}|{$r['dq_neto']}|{$r['dg_razon']}|{$r['dc_orden_compra']}\n");
	}
	
?>