<?php
/**
*	Realiza una búsqueda de clientes segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda de clientes.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q = htmlentities($_GET['q'],ENT_QUOTES,'UTF-8');
	
	//la cantidad máxima de resultados a obtener
	$limit = $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	if($idUsuario == 145){
		$ejecutivo_cond = 'AND dc_ejecutivo = 182';
	}else{
		$ejecutivo_cond = '';
	}
	$res = $db->select("tb_cliente","distinct dc_cliente,dg_rut,dg_razon,dg_giro",
	"dm_activo ='1' {$ejecutivo_cond} AND dc_empresa={$empresa} AND (dg_rut LIKE '%$q%' OR dg_razon LIKE '%$q%' OR dg_giro LIKE '%$q%')",array("limit" => $limit));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_rut']}|{$r['dg_razon']}|{$r['dg_giro']}|{$r['dc_cliente']}\n");
	}
	
?>