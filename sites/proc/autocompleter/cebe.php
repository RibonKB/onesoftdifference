<?php
/**
*	Realiza una búsqueda de centro de beneficio segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select(
	"tb_cebe AS b
	JOIN tb_ceco AS c ON b.dc_ceco = c.dc_ceco
	JOIN tb_grupo_cebe AS g ON b.dc_grupo_cebe = g.dc_grupo_cebe",
	"distinct b.dg_cebe,c.dg_ceco,b.dg_responsable,g.dg_grupo_cebe",
	"(dg_cebe LIKE '%{$q}%' OR c.dg_ceco LIKE '%{$q}%' OR b.dg_responsable LIKE '%{$q}%') AND b.dc_empresa={$empresa} AND b.dm_activo='1'",
	array("limit" => $limit));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_cebe']}|{$r['dg_grupo_cebe']}|{$r['dg_ceco']}|{$r['dg_responsable']}\n");
	}
	
?>