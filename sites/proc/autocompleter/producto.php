<?php
/**
*	Realiza una búsqueda de productos segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda de productos.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	$conditions = '';
	if(isset($_GET['cond'])){
		if($_GET['cond'])
			$conditions = "AND dm_requiere_costeo = '0'";
	}
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("tb_producto p
		JOIN tb_tipo_producto t ON p.dc_tipo_producto = t.dc_tipo_producto {$conditions}",
	"p.dc_producto, p.dg_producto, p.dg_codigo, p.dq_precio_venta, p.dq_precio_compra, p.dm_requiere_serie, p.dc_cebe, p.dc_empresa",
	"p.dc_empresa = {$empresa} AND p.dm_activo = 1 AND (p.dg_producto LIKE '%$q%' OR p.dg_codigo LIKE '%$q%' OR t.dg_tipo_producto LIKE '%$q%')",
	array("limit" => $limit, "order_by"=>"dg_producto", "group_by" => 'p.dc_producto'));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		$r['dg_producto'] = trim($r['dg_producto']);
		echo("{$r['dg_codigo']}|{$r['dg_producto']}||{$r['dq_precio_venta']}|{$r['dq_precio_compra']}|{$r['dc_producto']}|{$r['dm_requiere_serie']}|{$r['dc_cebe']}\n");
	}
	
?>