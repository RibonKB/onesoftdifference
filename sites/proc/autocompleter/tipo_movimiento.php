<?php
	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q = $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit = $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select(
	"tb_tipo_movimiento",
	"distinct dg_tipo_movimiento,dg_codigo,SUBSTR(dg_descripcion,0,30) as dg_desc",
	"(dg_tipo_movimiento LIKE '%$q%' OR dg_codigo LIKE '%$q%' OR dg_descripcion LIKE '%$q%') AND dc_empresa={$empresa} AND dm_activo = '1'",array("limit" => $limit, "order_by"=>"dg_tipo_movimiento"));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_codigo']}|{$r['dg_tipo_movimiento']}|{$r['dg_desc']}\n");
	}
	
?>