<?php
	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q = $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit = $_GET['limit'];
	
	$res = $db->select('tb_cuenta_contable',
	'dg_descripcion_larga, dg_cuenta_contable, dc_cuenta_contable, dg_codigo',
	"dc_empresa = {$empresa} AND dm_activo = 1 AND (dg_cuenta_contable LIKE '%$q%' OR dg_codigo LIKE '%$q%')",
	array('order_by' => 'dg_cuenta_contable', 'limit' => $limit));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_codigo']}|{$r['dg_cuenta_contable']}|{$r['dg_descripcion_larga']}|{$r['dc_cuenta_contable']}\n");
	}
	
?>