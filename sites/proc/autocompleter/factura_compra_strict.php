<?php

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit = intval($_GET['limit']);
	
	//El proveedor
	if(isset($_GET['dc_proveedor'])){
		$prov_data = "AND dc_proveedor = {$_GET['dc_proveedor']}";
	}else{
		$prov_data = '';
	}
	
	if(!isset($_GET['sin_pago'])){
		$pay_data = '';
		//$pay_data = "AND (dq_total-dq_monto_pagado) > 0";
	}else{
		$pay_data = '';
	}
	
	/*$ind_cot_permisos = $db->select('tb_indicador_cot_permisos','dg_permisos',"dc_usuario={$idUsuario}");
	$cond = '';
	if(!count($ind_cot_permisos)){
		$cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
	}else{
		if($ind_cot_permisos[0]['dg_permisos'] != 'ALL'){
			$cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
		}
	}*/
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("
	(SELECT *
	FROM tb_factura_compra
	WHERE dc_empresa = {$empresa} AND dm_nula = '0' {$prov_data} {$pay_data}) fc
	JOIN tb_proveedor p ON p.dc_proveedor = fc.dc_proveedor",
	"fc.dc_factura,fc.dq_factura,fc.dq_folio,fc.dq_neto,p.dg_razon",
	"fc.dq_factura LIKE '%{$q}%' OR fc.dq_folio LIKE '%{$q}%' OR p.dg_razon LIKE '%{$q}%'");
	
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		$r['dq_neto'] = moneda_local($r['dq_neto']);
		echo("{$r['dq_factura']}|({$r['dq_folio']}) {$r['dq_neto']}|{$r['dg_razon']}|{$r['dc_factura']}\n");
	}
	
?>