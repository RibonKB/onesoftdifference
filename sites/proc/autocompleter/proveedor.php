<?php
/**
*	Realiza una búsqueda de proveedores segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda de proveedores.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q = htmlentities($_GET['q'],ENT_QUOTES,'UTF-8');
	
	//la cantidad máxima de resultados a obtener
	$limit = $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("tb_proveedor","distinct dg_rut,dg_razon,dg_giro","dm_activo = '1' AND dc_empresa={$empresa} AND (dg_rut LIKE '%$q%' OR dg_razon LIKE '%$q%' OR dg_giro LIKE '%$q%')",array("limit" => $limit));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_rut']}|{$r['dg_razon']}|{$r['dg_giro']}\n");
	}
	
?>