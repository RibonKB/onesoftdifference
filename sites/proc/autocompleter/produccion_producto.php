<?php
/**
*	Realiza una búsqueda de productos segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda de productos.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	$conditions = '';
	if(isset($_GET['cond'])){
		if($_GET['cond'])
			$conditions = "AND dm_requiere_costeo = '0'";
	}
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("tb_produccion_producto",
	"DISTINCT dc_producto,dg_producto,dq_precio,DATE_FORMAT(df_creacion,'%d/%m/%Y') as df_creacion",
	"dc_empresa = {$empresa} AND dg_producto LIKE '%$q%'",
	array("limit" => $limit));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_producto']}|{$r['dq_precio']}|{$r['df_creacion']}|{$r['dc_producto']}\n");
	}
	
?>