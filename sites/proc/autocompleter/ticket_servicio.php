<?php
    define("MAIN",1);
	require_once("../../../inc/init.php");
    
    $query = $db->prepare(
              $db->select('tb_ticket_servicio t
                           LEFT JOIN tb_cliente cl ON cl.dc_cliente = t.dc_cliente',
                          't.dc_ticket, t.dq_ticket, t.dg_titulo, cl.dg_razon',
                          't.dc_empresa = ? AND (t.dq_ticket LIKE ? OR cl.dg_razon LIKE ?)',
                      array('limit' => $_GET['limit']))
            );
    $query->bindValue(1,$empresa,PDO::PARAM_INT);
    $query->bindValue(2,"%{$_GET['q']}%",PDO::PARAM_STR);
    $query->bindValue(3,"%{$_GET['q']}%",PDO::PARAM_STR);
    $db->stExec($query);
    
    while($d = $query->fetch(PDO::FETCH_OBJ)):
      echo "{$d->dq_ticket}|{$d->dg_titulo}|{$d->dg_razon}|{$d->dc_ticket}\n";
    endwhile;
?>