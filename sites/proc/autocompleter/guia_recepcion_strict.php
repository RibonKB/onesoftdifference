<?php
	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("(SELECT * FROM tb_guia_recepcion WHERE dc_empresa = {$empresa} AND dm_nula = '0') c
	JOIN tb_tipo_guia_recepcion t ON t.dc_tipo = c.dc_tipo_guia",
	"c.dq_guia_recepcion, c.dg_observacion, t.dg_tipo",
	"c.dq_guia_recepcion LIKE '{$q}%' OR t.dg_tipo LIKE '%{$q}%'");
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		$r['dg_observacion'] = substr($r['dg_observacion'],0,50);
		echo("{$r['dq_guia_recepcion']}|{$r['dg_tipo']}|{$r['dg_observacion']}\n");
	}
?>