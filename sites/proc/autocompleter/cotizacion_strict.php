<?php

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	$ind_cot_permisos = $db->select('tb_indicador_cot_permisos','dg_permisos',"dc_usuario={$idUsuario}");
	$cond = '';
	if(!count($ind_cot_permisos)){
		$cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
	}else{
		if($ind_cot_permisos[0]['dg_permisos'] != 'ALL'){
			$cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
		}
	}
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("(SELECT * FROM tb_cotizacion WHERE dc_empresa = {$empresa} {$cond}) c
	JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON cl.dc_cliente = c.dc_cliente",
	"c.dc_cotizacion,c.dq_cotizacion,c.dq_neto,cl.dg_razon",
	"c.dq_cotizacion LIKE '{$q}%' OR cl.dg_razon LIKE '%{$q}%'");
	
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		$r['dq_neto'] = moneda_local($r['dq_neto']);
		echo("{$r['dq_cotizacion']}|{$r['dq_neto']}|{$r['dg_razon']}|{$r['dc_cotizacion']}\n");
	}
	
?>