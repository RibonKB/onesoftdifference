<?php
/**
*	Realiza una búsqueda de centro de beneficio segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select(
	"tb_ceco AS c
	JOIN tb_segmento AS s ON c.dc_segmento = s.dc_segmento
	JOIN tb_grupo_ceco AS g ON c.dc_grupo_ceco = g.dc_grupo_ceco",
	"distinct c.dg_ceco,c.dg_responsable,s.dg_segmento,g.dg_grupo_ceco",
	"(dg_ceco LIKE '%{$q}%' OR c.dg_responsable LIKE '%{$q}%' OR dg_segmento LIKE '%{$q}%') AND c.dc_empresa={$empresa} AND c.dm_activo='1'",
	array("limit" => $limit));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_ceco']}|{$r['dg_grupo_ceco']}|{$r['dg_segmento']}|{$r['dg_responsable']}\n");
	}
	
?>