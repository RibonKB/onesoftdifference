<?php

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("(SELECT * FROM tb_produccion_material WHERE dc_empresa={$empresa} AND dm_activo='1') m
	LEFT JOIN (SELECT * FROM tb_produccion_categoria WHERE dc_empresa={$empresa} AND dm_activo='1') c ON c.dc_categoria = m.dc_categoria
	LEFT JOIN (SELECT * FROM tb_marca WHERE dc_empresa={$empresa} AND dm_activo='1') mr On mr.dc_marca = m.dc_marca",
	'distinct m.dg_material, c.dg_categoria, mr.dg_marca, m.dc_material',
	"m.dg_material LIKE '%{$q}%' OR c.dg_categoria LIKE '%{$q}%' OR mr.dg_marca LIKE '%{$q}%'");
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_material']}|{$r['dg_categoria']}|{$r['dg_marca']}|{$r['dc_material']}\n");
	}
	
?>