<?php
/**
*	Realiza una búsqueda de clientes segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda de clientes.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q = strtolower(htmlentities($_GET['q'],ENT_QUOTES,'UTF-8'));
	
	//la cantidad máxima de resultados a obtener
	$limit = $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select(
	"tb_funcionario",
	"distinct dg_rut,dg_nombres,dg_ap_paterno,dg_ap_materno",
	"(dg_rut LIKE '%$q%' OR dg_nombres LIKE '%$q%' OR dg_ap_paterno LIKE '%$q%' OR dg_ap_materno LIKE '%$q%') AND dc_empresa={$empresa} AND dm_activo = '1'",array("limit" => $limit, "order_by"=>"dg_nombres"));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_rut']}|{$r['dg_nombres']} {$r['dg_ap_paterno']} {$r['dg_ap_materno']}\n");
	}
	
?>