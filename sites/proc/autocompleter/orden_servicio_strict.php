<?php

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$db->escape($_GET['q']);
	
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit =& $_GET['limit'];
	
	/*$ind_cot_permisos = $db->select('tb_indicador_cot_permisos','dg_permisos',"dc_usuario={$idUsuario}");
	$cond = '';
	if(!count($ind_cot_permisos)){
		$cond = "AND dc_ejecutivo={$userdata['dc_funcionario']}";
	}else{
		if($ind_cot_permisos[0]['dg_permisos'] != 'ALL'){
			$cond = "AND dc_ejecutivo IN ({$ind_op_permisos[0]['dg_permisos']},{$userdata['dc_funcionario']})";
		}
	}*/
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select("(SELECT * FROM tb_orden_servicio WHERE dc_empresa = {$empresa}) c
	JOIN (SELECT * FROM tb_cliente WHERE dc_empresa={$empresa}) cl ON cl.dc_cliente = c.dc_cliente",
	"c.dc_orden_servicio,c.dq_orden_servicio,cl.dg_razon,c.dq_precio",
	"c.dq_orden_servicio LIKE '{$q}%' OR cl.dg_razon LIKE '%{$q}%'");
	
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		$r['dq_precio'] = moneda_local($r['dq_precio']);
		echo("{$r['dq_orden_servicio']}|{$r['dq_precio']}|{$r['dg_razon']}|{$r['dc_orden_servicio']}\n");
	}
	
?>