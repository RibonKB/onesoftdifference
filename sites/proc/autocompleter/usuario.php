<?php
/**
*	Realiza una búsqueda de clientes segun el criterio $_GET['q'] enviado por el script autocompleter en los distintos formularios de b&uacute;squeda de clientes.
**/

	define("MAIN",1);
	require_once("../../../inc/global.php");
	
	//le da formato al criterio para encontrarlo según fue insertado en la base de datos.
	$q =& $_GET['q'];
	
	//la cantidad máxima de resultados a obtener
	$limit = $_GET['limit'];
	
	//realiza la búsqueda con el criterio dado
	$res = $db->select(
	"tb_usuario AS us
	JOIN tb_funcionario AS f ON us.dc_funcionario = f.dc_funcionario",
	"distinct f.dg_rut,f.dg_nombres,f.dg_ap_paterno,f.dg_ap_materno,us.dg_usuario,us.dc_usuario",
	"us.dm_activo = '1' AND
	(
		f.dg_rut LIKE '%$q%' OR
		f.dg_nombres LIKE '%$q%' OR
		f.dg_ap_paterno LIKE '%$q%' OR
		f.dg_ap_materno LIKE '%$q%' OR
		us.dg_usuario LIKE '%$q%'
	)
	AND f.dc_empresa=$empresa",
	array("limit" => $limit, "order_by"=>"dg_usuario"));
	
	//le da el formato requerido por 'autocompleter' para mostrar los resultados en el formulario.
	foreach($res as $r){
		echo("{$r['dg_usuario']}|{$r['dg_rut']}|{$r['dg_nombres']} {$r['dg_ap_paterno']} {$r['dg_ap_materno']}|{$r['dc_usuario']}\n");
	}
	
?>