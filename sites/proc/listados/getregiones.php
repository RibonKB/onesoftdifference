<?php
/**
*	Obtiene todas las regiones(del país) registradas en la base de datos y las enlista para ser recibidas por un <select>
**/
$regiones = $db->select("tb_region","*","dc_region > 0",array("order_by"=>"dg_region"));
foreach($regiones as $region){
	echo('<option value="'.$region['dc_region'].'">'.$region['dg_region'].'</option>');
}
?>