<?php
define("MAIN",1);
require_once("../../inc/settings.php");

// Genera el objeto encargado de mostrar alertas
require_once("../../inc/Error_class.php");
$error_man = new Error();
	
// Generar el objeto que se conecta con la base de datos.
require_once("../../inc/DBConnector.php");

$db = new DBConnector(array("user" => $settings["DBusername"],"password" => $settings["DBpassword"],"server" => $settings["DBserver"],"database" => $settings["DBbasedatos"]));

$db->escape($_GET['rut']);
$data = $db->select('tb_empresa','dc_empresa,dg_razon',"dg_rut='{$_GET['rut']}'");

if(!count($data)){
	echo json_encode("<not-found>");
	exit();
}

$data = $data[0];

echo json_encode($data);

?>