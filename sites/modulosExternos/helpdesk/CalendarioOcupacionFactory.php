<?php

/**
 * Description of CalendarioOcupacionFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 04-07-2013
 */
class CalendarioOcupacionFactory extends Factory {

  protected $title = "Calendario de Ocupación";

  public function indexAction() {
    
    $body_content = $this->getView($this->getTemplateURL('index'));

    echo $this->getView('modulosExternos/helpdesk/layout', array(
        'header_icon' => 'calendar',
        'body_content' => $body_content
    ));
  }

  public function getEventsAction() {
    $ordenes = $this->getOrdenesPorPeriodo(self::getRequest()->start, self::getRequest()->end);
    
    echo json_encode($this->formatOSEvents($ordenes));
  }
  
  public function viewOSDetailAction(){
    $db = $this->getConnection();
    $dc_orden_servicio = self::getRequest()->dc_orden_servicio;
    $orden_servicio = $db->getRowById('tb_orden_servicio', $dc_orden_servicio, 'dc_orden_servicio');
    
    if($orden_servicio->dc_nota_venta){
      $orden_servicio->proyecto = $db->getRowById('tb_nota_venta_proyecto', $orden_servicio->dc_nota_venta, 'dc_nota_venta');
    }
    
    echo $this->getView($this->getTemplateURL('os_detail'), array(
        'orden_servicio' => $orden_servicio,
        'cliente' => $db->getRowById('tb_cliente', $orden_servicio->dc_cliente, 'dc_cliente'),
        'supervisor' => $db->getRowById('tb_funcionario', $orden_servicio->dc_tecnico, 'dc_funcionario'),
        'ejecutores' => $this->getOSEjecutores($dc_orden_servicio),
        'sucursal' => $db->getRowById('tb_cliente_sucursal', $orden_servicio->dc_sucursal, 'dc_sucursal')
    ));
  }
  
  public function getFiltrosDataAction(){
    
    $data = $this->getMonthData();
    $proyectos = $this->getProyectos($data);
    $supervisores = $this->getSupervisores($data);
    $tecnicos = $this->getTecnicos($data);
    
    echo $this->getFormView($this->getTemplateURL('filtros.form'),array(
        'proyectos' => $proyectos,
        'supervisores' => $supervisores,
        'tecnicos' => $tecnicos
    ));
  }
  
  public function getOSEjecutores($dc_orden_servicio){
    $db = $this->getConnection();
    
    $st = $db->prepare(
              $db->select('tb_tecnico_apoyo o
                           JOIN tb_funcionario f ON f.dc_funcionario = o.dc_tecnico',
                          'f.*',
                          'o.dc_orden_servicio = ?')
            );
    $st->bindValue(1, $dc_orden_servicio, PDO::PARAM_INT);
    $db->stExec($st);
    
    return $st->fetchAll(PDO::FETCH_OBJ);
  }
  
  private function getOrdenesPorPeriodo($start, $end){
    $start = date(DATE_ATOM,$start);
    $end = date(DATE_ATOM,$end);
    
    $db = $this->getConnection();
    
    $ordenes = $db->prepare(
                  $db->select(
                     'tb_orden_servicio',
                     '*, DATEDIFF(df_fecha_final,df_compromiso) dc_dias_servicio',
                     'dc_empresa = ? AND
                      (
                        (df_compromiso < ? AND df_fecha_final > ?) OR
                        (df_compromiso > ? AND df_fecha_final < ?) OR
                        (df_compromiso < ? AND df_fecha_final > ?))')
               );
    $ordenes->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
    $ordenes->bindValue(2, $start, PDO::PARAM_STR);
    $ordenes->bindValue(3, $start, PDO::PARAM_STR);
    $ordenes->bindValue(4, $start, PDO::PARAM_STR);
    $ordenes->bindValue(5, $end, PDO::PARAM_STR);
    $ordenes->bindValue(6, $end, PDO::PARAM_STR);
    $ordenes->bindValue(7, $end, PDO::PARAM_STR);
    
    $db->stExec($ordenes);
    
    return $ordenes->fetchAll(PDO::FETCH_OBJ);
  }
  
  private function formatOSEvents($ordenes){
      $data = array();
      foreach($ordenes as $o){
        $data[] = $this->formatSingleOS($o);
      }
      return $data;
  }
  
  private function formatSingleOS($os){
      if($os->dc_nota_venta){
        return $this->formatProyectoOS($os);
      }else{
        return $this->formatSimpleOS($os);
      }
  }
  
  private function formatProyectoOS($os){
    $cliente = $this->getConnection()->getRowById('tb_cliente', $os->dc_cliente, 'dc_cliente');
    return array(
        'id' => $os->dc_orden_servicio,
        'title' => $cliente->dg_razon,
        'start' => $os->df_compromiso,
        'end' => $os->df_fecha_final,
        'color' => '#006400',
        'allDay' => false,
        'solucionado' => $os->dm_estado == 1,
        'facturable' => $os->dm_facturable == 1,
        'telefonica' => $os->dm_gestion_telefonica == 1,
        'evaluacion' => $os->dc_evaluacion_servicio
    );
  }
  
  private function formatSimpleOS($os){
    $cliente = $this->getConnection()->getRowById('tb_cliente', $os->dc_cliente, 'dc_cliente');
    return array(
        'id' => $os->dc_orden_servicio,
        'title' => $cliente->dg_razon,
        'start' => $os->df_compromiso,
        'end' => $os->df_fecha_final,
        'allDay' => false,
        'description' => $cliente->dg_razon,
        'solucionado' => $os->dm_estado == 1,
        'facturable' => $os->dm_facturable == 1,
        'telefonica' => $os->dm_gestion_telefonica == 1,
        'evaluacion' => $os->dc_evaluacion_servicio
    );
  }
  
  private function getMonthData(){
    $r = self::getRequest();
    $fecha_inicio = mktime(0, 0, 0, $r->dc_mes+1, 1, $r->dc_anho);
    $fecha_fin = mktime(0, 0, 0, $r->dc_mes+1, date('t',$fecha_inicio), $r->dc_anho);
    
    return $this->getOrdenesPorPeriodo($fecha_inicio, $fecha_fin);
    
  }
  
  private function getProyectos($data){
    $proyectos = array();
    $ordenes = array();
    
    foreach($data as $d):
      if($d->dc_nota_venta):
        $p = $this->getConnection()->getRowById('tb_nota_venta_proyecto', $d->dc_nota_venta, 'dc_nota_venta');
        if($p):
          $proyectos[$p->dc_nota_venta] = $p->dg_nombre;
        endif;
        
        if(!isset($ordenes[$d->dc_nota_venta])):
          $ordenes[$d->dc_nota_venta] = array();
        endif;
        
        $ordenes[$d->dc_nota_venta][] = $d->dc_orden_servicio;
        
      endif;
    endforeach;
    
    return array($proyectos, $ordenes);
    
  }
  
  private function getSupervisores($data){
    $supervisores = array();
    $ordenes = array();
    
    foreach($data as $d):
      if($d->dc_tecnico):
        $s = $this->getConnection()->getRowById('tb_funcionario', $d->dc_tecnico, 'dc_funcionario');
        $supervisores[$s->dc_funcionario] = implode(' ', array($s->dg_nombres, $s->dg_ap_paterno, $s->dg_ap_materno));
        
        if(!isset($ordenes[$d->dc_tecnico])):
          $ordenes[$d->dc_tecnico] = array();
        endif;
        
        $ordenes[$d->dc_tecnico][] = $d->dc_orden_servicio;
      endif;
    endforeach;
    
    return array($supervisores,$ordenes);
  }
  
  private function getTecnicos($data){
    $tecnicos = array();
    $ordenes = array();
    
    foreach($data as $d):
      $t = $this->getOSEjecutores($d->dc_orden_servicio);
      foreach($t as $s):
        $tecnicos[$s->dc_funcionario] = implode(' ', array($s->dg_nombres, $s->dg_ap_paterno, $s->dg_ap_materno));
      
        if(!isset($ordenes[$s->dc_funcionario])):
          $ordenes[$s->dc_funcionario] = array();
        endif;
        
        $ordenes[$s->dc_funcionario][] = $d->dc_orden_servicio;
      endforeach;
    endforeach;
    
    return array($tecnicos,$ordenes);
  }

}

?>
