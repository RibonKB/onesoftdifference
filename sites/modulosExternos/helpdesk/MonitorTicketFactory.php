<?php

require_once 'sites/modulosExternos/helpdesk/classes/Ticket.php';

/**
 * Description of MonitorTicketFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 01-07-2013
 */
class MonitorTicketFactory extends Factory{
  
    protected $title = "Tickets de Soporte";
    
    public function indexAction(){
      
        $tickets = $this->getTickets();
        
        $body_content = $this->getView($this->getTemplateURL('monitorFull'), array(
            'tickets' => $tickets
        ));
        
        echo $this->getView('modulosExternos/helpdesk/layout', array(
            'header_icon' => 'wrench',
            'body_content' => $body_content
        ));
      
    }
    
    public function verTicketAction(){
        $manager = $this->getCorreoManagerService();
        $db = $this->getConnection();
        
        $ticket = $this->getTicketByRow($db->getRowById('tb_ticket_servicio', self::getRequest()->id, 'dc_ticket'));
        
        $correos = $manager->getMailsByTicket($ticket);
        
        echo $this->getView($this->getTemplateURL('verTicket'), array(
            'ticket'  => $ticket,
            'correos' => $correos
        ));
        
    }
    
    private function getTickets(){
        $db = $this->getConnection();
        
        $select = $db->prepare(
						$db->select(
							'tb_ticket_servicio t
							 LEFT JOIN tb_ticket_orden_servicio tckos ON tckos.dc_ticket = t.dc_ticket',
							'DISTINCT t.*',
							't.dc_empresa = ? AND tckos.dc_orden_servicio IS NULL AND t.dm_estado = 1',
							array(
								'order_by' => 't.df_creacion DESC'
							)));
        $select->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
        $db->stExec($select);
        
        $tickets = array();
        
        while($t = $select->fetch(PDO::FETCH_OBJ)){
          $tickets[] = $this->getTicketByRow($t);
        }
        
        return $tickets;
    }
    
    private function getTicketByRow($row){
      $db = $this->getConnection();
      
      $ticket = new Ticket();
      $ticket->setId($row->dc_ticket);
      $ticket->setFecha($row->df_creacion);
      $ticket->setNumeroTicket($row->dq_ticket);
      $ticket->setTitulo($row->dg_titulo);
      $ticket->setCliente($db->getRowById('tb_cliente', $row->dc_cliente, 'dc_cliente'));
      $ticket->setCorreo_default($row->dc_correo_default);
      
      return $ticket;
    }
    
    /**
     * @return CorreoManagerService
     */
    private function getCorreoManagerService(){
        return $this->getService('CorreoManager');
    }
    
}

?>
