<?php

/**
 * Description of EstadisticasFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 02-07-2013
 */
class EstadisticasFactory extends Factory {

  protected $title = "Estadistica de tickets";

  public function indexAction() {
    $pie_ticketsOS = $this->getPieTicketsOS();
    $bar_ticketsOS = $this->getBarTicketsOS();
    $stat_evaluacion = $this->getEvaluacionTecnica();

    $body_content = $this->getView($this->getTemplateURL('fullEstadisticas'), array(
        'pie_ticketsOS' => $pie_ticketsOS,
        'bar_ticketsOS' => $bar_ticketsOS,
        'stat_evaluacion' => $stat_evaluacion
    ));

    echo $this->getView('modulosExternos/helpdesk/layout', array(
        'header_icon' => 'bar-chart',
        'body_content' => $body_content
    ));
  }

  private function getPieTicketsOS() {

    $sinOS = $this->getCantidadTicketsSinOS();
    $conOS = $this->getCantidadTicketsConOS();

    return array(
        'values' => array($sinOS, $conOS[1], $conOS[0]),
        'options' => array(
            'type' => 'pie',
            'width' => '180',
            'height' => '180',
            'sliceColors' => array(
                $this->getRandomColor(),
                $this->getRandomColor(),
                $this->getRandomColor()),
            'tooltipFormat' => '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)',
            'tooltipValueLookups' => array(
                'names' => array('Tickets Sin OS', 'Tickets con OS Abierta', 'Tickets con OS cerrada')
            )
        )
    );
  }

  private function getCantidadTicketsSinOS() {
    $db = $this->getConnection();

    $query = $db->prepare(
            $db->select('tb_ticket_servicio t
                          LEFT JOIN tb_ticket_orden_servicio o ON o.dc_ticket = t.dc_ticket', 'COUNT(*)', 'o.dc_orden_servicio IS NULL AND t.dm_estado = 1')
    );
    $db->stExec($query);

    return $query->fetchColumn();
  }

  private function getCantidadTicketsConOS() {
    $db = $this->getConnection();

    $query = $db->prepare(
            $db->select('tb_ticket_servicio t
                          JOIN tb_ticket_orden_servicio o ON o.dc_ticket = t.dc_ticket
                          JOIN tb_orden_servicio os ON os.dc_orden_servicio = o.dc_orden_servicio', 'os.dm_estado, COUNT(*) contador', 't.dm_estado = 1', array('group_by' => 'os.dm_estado'))
    );
    $db->stExec($query);

    $ret = array(
        0 => 0,
        1 => 0
    );

    while ($q = $query->fetch(PDO::FETCH_OBJ)) {
      $ret[$q->dm_estado] = intval($q->contador);
    }

    return $ret;
  }
  
  private function getBarTicketsOS(){
      $sinOS = $this->getCantidadTicketsSinOSPeriodos();
      $conOS = array(
          array(0,0,0),
          array(0,0,0)
      );
      
      $names = array('Tickets sin OS','OS Abierta','OS Cerrada');
      
      return array(
          array(
              "className" => ".ticketsPeriodo1",
              "data" => array(
                  array(
                      'x' => $names[0],
                      'y' => $sinOS[0]
                  ),
                  array(
                      'x' => $names[1],
                      'y' => $conOS[0][0]
                  ),
                  array(
                      'x' => $names[2],
                      'y' => $conOS[1][0]
                  )
              )
          ),
          array(
              "className" => ".ticketsPeriodo2",
              "data" => array(
                  array(
                      'x' => $names[0],
                      'y' => $sinOS[1]
                  ),
                  array(
                      'x' => $names[1],
                      'y' => $conOS[0][1]
                  ),
                  array(
                      'x' => $names[2],
                      'y' => $conOS[1][1]
                  )
              )
          ),
          array(
              "className" => ".ticketsPeriodo3",
              "data" => array(
                  array(
                      'x' => $names[0],
                      'y' => $sinOS[2]
                  ),
                  array(
                      'x' => $names[0],
                      'y' => $conOS[0][2]
                  ),
                  array(
                      'x' => $names[0],
                      'y' => $conOS[1][2]
                  )
              )
          )
      );
  }
  
  private function getCantidadTicketsSinOSPeriodos(){
      $db = $this->getConnection();
      
      $query = $db->prepare(
                  $db->select('tb_ticket_servicio t
                               LEFT JOIN tb_ticket_orden_servicio o ON o.dc_ticket = t.dc_ticket',
                              'DATEDIFF(NOW(), t.df_creacion) dias, COUNT(*) cantidad',
                              'o.dc_orden_servicio IS NULL AND t.dm_estado = 1', array(
                                  'group_by' => 'dias'
                              ))
              );
      $db->stExec($query);
      
      $data = array(0,0,0);
      while($q = $query->fetch(PDO::FETCH_OBJ)){
        if($q->dias < 6){
          $data[0] += $q->cantidad;
        }else if($q->dias < 11){
          $data[1] += $q->cantidad;
        }else{
          $data[2] += $q->cantidad;
        }
      }
      
      return $data;
      
  }

  public function getRandomColor() {
    return '#'
            . str_pad(dechex(rand(0, 255)), 2, 0, STR_PAD_LEFT)
            . str_pad(dechex(rand(0, 255)), 2, 0, STR_PAD_LEFT)
            . str_pad(dechex(rand(0, 255)), 2, 0, STR_PAD_LEFT);
  }
  
  private function getBarOcupacionTecnicos(){
    
  }
  
    private function getOcupacionTecnicaMensual(DateTime $date){
      $db = $this->getConnection();
      
      $data = $db->prepare($db->select('', '', ''));
      
      
    }
    
  private function getEvaluacionTecnica(){
      $db = $this->getConnection();
      
      $data = $db->prepare($db->select(
              'tb_tecnico_apoyo t
               JOIN tb_funcionario f ON f.dc_funcionario = t.dc_tecnico
               JOIN tb_orden_servicio os ON os.dc_orden_servicio = t.dc_orden_servicio',
              'f.dg_nombres, f.dg_ap_paterno, f.dg_ap_materno,
               AVG(os.dc_evaluacion_servicio) dq_promedio,
               COUNT(os.dc_orden_servicio) dq_ordenes_servicio',
              'os.dc_empresa = ? AND os.dc_evaluacion_servicio > 0 AND f.dm_activo = 1 AND f.dm_evaluacion_ticket = 1',array(
          'group_by' => 't.dc_tecnico'
      )));
      $data->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
      $db->stExec($data);
      
      return $data->fetchAll(PDO::FETCH_OBJ);
  }

}

?>
