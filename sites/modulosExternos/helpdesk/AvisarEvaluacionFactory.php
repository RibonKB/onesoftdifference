<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AvisarEvaluacionFactory
 *
 * @author ejimenez
 */
require_once('sites/rrhh/enviaM.php');
require_once 'inc/FactoryNoLogin.php';

class AvisarEvaluacionFactory extends FactoryNoLogin {

    //put your code here
    private $dias = 28;

    public function AvisarAction() {
        $en = new enviaM();
        $this->Auto_Evaluacion($en);
        $this->Evaluacion_Funcionario($en);
    }

    private function getNombreFuncionario($id_funcionario) {
        $nombre = $this->coneccion('select', 'tb_funcionario', 'CONCAT(dg_nombres," ",dg_ap_paterno) nombre', "dc_funcionario={$id_funcionario}");
        $nombre = $nombre->fetch(PDO::FETCH_OBJ);
        return $nombre->nombre;
    }

    private function validaDias($id_funcionario, $evaluacion) {
        $s = $this->coneccion('select', 'tb_funcionario', '*', "df_$evaluacion >( SELECT DATE_SUB(NOW(),INTERVAL {$this->dias} day))");
        $s = $s->fetch(PDO::FETCH_OBJ);
    }

    private function getEmail($id_funcionario) {
        $e = $this->coneccion('select', 'tb_funcionario', 'dg_email', "dc_funcionario={$id_funcionario}");
        $e = $e->fetch(PDO::FETCH_OBJ);
        return $e->dg_email;
    }

    private function Auto_Evaluacion($e) {
        $cargo = $this->coneccion('select', 'tb_funcionario fu '
                . 'LEFT JOIN tb_cargos ca ON fu.dc_cargo=ca.dc_cargo', 'fu.dc_funcionario');
        $cargo = $cargo->fetchAll(PDO::FETCH_OBJ);
        foreach ($cargo as $d):
            $dias = $this->validaDias($d->dc_funcionario, 'A_U_Evaluacion');
            if ($dias != false) {
                $email = $this->getEmail($d->dc_funcionario);
                $e->Enviar_mail($email, 'Evaluacion', 'Debe Realizar su Auto-Evaluacion');
            }
        endforeach;
    }

    private function Evaluacion_Funcionario($e) {
        $nombres = '';
        $evaluador = $this->coneccion('select', 'tb_cargo', 'dc_cargo', 'dm_evalua=1');
        $evaluador = $evaluador->fetchAll(PDO::FETCH_OBJ);
        foreach ($evaluador as $d):
            $nombres = '';
            $users = $this->coneccion('select', 'tb_funcionario fu LEFT JOIN tb_cargos ca ON dc.dc_cargo=fu.dc_cargo', 'fu.dc_funcionario', "ca.dc_evaluador={$d->dc_cargo}");
            $users = $users->fetchAll(PDO::FETCH_OBJ);
            foreach ($users as $t):
                $nombre = $this->getNombreFuncionario($t->dc_funcionario);
                if ($nombre != false):
                    $nombres = $nombre . "\n " . $nombres;
                endif;
            endforeach;
            $dc = $this->coneccion('select', 'tb_funcionario fu LEFT JOIN tb_cargo ca ON fu.dc_cargo=ca.dc_cargo', 'fu.funcionario', "dc_cargo={$d->dc_cargo}");
            $dc = $dc->fetch(PDO::FETCH_OBJ);
            $em = $this->getEmail($dc);
            $e->Enviar_mail($em, 'Evaluacion', "Debe Realizar la Evaluacion de los funcionarios : \n {$nombres}");
        endforeach;
    }
    private function Evaluacion_Consenso() {
        
    }

}
