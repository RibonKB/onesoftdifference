<?php

require_once 'inc/AbstractFactoryService.class.php';
require_once 'sites/modulosExternos/helpdesk/classes/Correo.php';

/**
 * Description of CorreoManager
 *
 * @author Tomás Lara Valdovinos
 * @date 14-06-2013
 */
class CorreoManagerService extends AbstractFactoryService {

  /**
   * @return int
   */
  public function insertarCorreo(Correo $correo) {
    $this->persistCorreo($correo);

    if ($correo->getTicket()) {
      $ticket = $correo->getTicket();
      $ticket->setCorreo_default($correo->getId());
      $correo->setRelations(array(
          $this->insertarTicket($ticket)
      ));
    }

    if (count($correo->getRelations())) {
      $this->insertarRelacionConfianza($correo);
    }

    if (count($correo->getAttachments())) {
      $this->insertarAttachment($correo);
    }

    if (count($correo->getInline())) {
      $this->insertarInlineAttachment($correo);
    }
  }

  private function persistCorreo(Correo $correo) {
    $db = $this->factory->getConnection();

    $insert = $db->prepare($db->insert('tb_correo_ticket', array(
                'dc_uid' => '?',
                'df_correo' => '?',
                'dg_asunto' => '?',
                'dc_cuenta' => '?',
                'dg_from' => '?',
                'dg_body' => '?',
                'dm_attachment' => '?',
                'dm_inline' => '?',
                'dc_factor_confianza' => '?',
                'df_creacion' => $db->getNow(),
                'dc_empresa' => $this->factory->getEmpresa()
    )));
    $insert->bindValue(1, $correo->getUID(), PDO::PARAM_INT);
    $insert->bindValue(2, $correo->getFecha(), PDO::PARAM_STR);
    $insert->bindValue(3, $correo->getAsunto(), PDO::PARAM_STR);
    $insert->bindValue(4, $correo->getCuenta(), PDO::PARAM_INT);
    $insert->bindValue(5, $correo->getFrom(), PDO::PARAM_STR);
    $insert->bindValue(6, $correo->getBody(), PDO::PARAM_STR);
    $insert->bindValue(7, (boolean) count($correo->getAttachments()), PDO::PARAM_BOOL);
    $insert->bindValue(8, (boolean) count($correo->getInline()), PDO::PARAM_BOOL);
    $insert->bindValue(9, $correo->getFactorConfianza(), PDO::PARAM_INT);
    $db->stExec($insert);

    $correo->setId($db->lastInsertId());
  }

  public function updateCorreo() {
    
  }

  private function insertarTicket(Ticket $ticket) {
    $db = $this->factory->getConnection();

    $insertar = $db->prepare($db->insert('tb_ticket_servicio', array(
                'dc_cliente' => '?',
                'dq_ticket' => '?',
                'dg_titulo' => '?',
                'df_creacion' => $db->getNow(),
                'dc_correo_default' => '?',
                'dc_empresa' => '?',
                'dm_estado' => 1
    )));
    $insertar->bindValue(1, $ticket->getCliente(), PDO::PARAM_INT);
    $insertar->bindValue(2, $ticket->getNumeroTicket(), PDO::PARAM_INT);
    $insertar->bindValue(3, $ticket->getTitulo(), PDO::PARAM_STR);
    $insertar->bindValue(4, $ticket->getCorreo_default(), PDO::PARAM_INT);
    $insertar->bindValue(5, $ticket->getEmpresa(), PDO::PARAM_INT);

    $db->stExec($insertar);
    
    return $db->lastInsertId();
    
  }

  private function insertarRelacionConfianza(Correo $correo) {
    $db = $this->factory->getConnection();

    $r = $correo->getRelations();
    $dc_correo = $correo->getId();

    $insertar = $db->prepare($db->insert('tb_relacion_correo_ticket', array(
                'dc_correo' => '?',
                'dc_ticket' => '?'
    )));
    $insertar->bindParam(1, $dc_correo, PDO::PARAM_INT);
    $insertar->bindParam(2, $dc_ticket, PDO::PARAM_INT);

    foreach ($r as $dc_ticket) {
      $db->stExec($insertar);
    }
  }

  private function insertarAttachment(Correo $correo) {
    $db = $this->factory->getConnection();

    $att = $correo->getAttachments();
    $dc_correo = $correo->getId();

    $insertar = $db->prepare($db->insert('tb_correo_ticket_attachment', array(
                'dc_correo' => '?',
                'dc_part_content' => '?'
    )));
    $insertar->bindParam(1, $dc_correo, PDO::PARAM_INT);
    $insertar->bindParam(2, $dc_part, PDO::PARAM_INT);

    foreach ($att as $attachment) {
      $dc_part = $this->insertarPartContent($attachment);

      $db->stExec($insertar);
    }
  }

  private function insertarInlineAttachment(Correo $correo) {
    $db = $this->factory->getConnection();

    $in = $correo->getInline();
    $dc_correo = $correo->getId();

    $insertar = $db->prepare($db->insert('tb_correo_ticket_inline', array(
                'dc_correo' => '?',
                'dc_part_content' => '?',
                'dg_id' => '?'
    )));
    $insertar->bindParam(1, $dc_correo, PDO::PARAM_INT);
    $insertar->bindParam(2, $dc_part, PDO::PARAM_INT);
    $insertar->bindParam(3, $dg_id, PDO::PARAM_STR);

    foreach ($in as $inline) {
      
      $dc_part = $this->insertarPartContent($inline);
      $dg_id = $inline->id;

      $db->stExec($insertar);
    }
  }

  private function insertarPartContent($part) {
    $db = $this->factory->getConnection();

    $insertar = $db->prepare($db->insert('tb_correo_part_content', array(
                'dg_mimetype' => '?',
                'dm_downloaded' => '?',
                'dg_part_path' => '?',
                'dg_encoding' => '?',
                'dg_filename' => '?',
                'df_creacion' => $db->getNow()
    )));
    $insertar->bindValue(1, $part->mimetype, PDO::PARAM_STR);
    $insertar->bindValue(2, false, PDO::PARAM_BOOL);
    $insertar->bindValue(3, $part->path, PDO::PARAM_STR);
    $insertar->bindValue(4, $part->encoding, PDO::PARAM_STR);
    $insertar->bindValue(5, $part->filename, PDO::PARAM_STR);

    $db->stExec($insertar);

    return $db->lastInsertId();
  }

  /**
   * @return array 
   */
  public function getFactorConfianza(Correo $correo) {
    $factor = array(
        'confianza' => 0,
        'tickets' => array()
    );

    $realTitle = $this->getRealTitle($correo->getAsunto());

    if (!$realTitle) {
      return $factor;
    }

    $ticketsByTitle = $this->getTicketsByFactorTitulo($realTitle);
    
    //Factory::debug($ticketsByTitle);

    if (!count($ticketsByTitle)) {
      return $factor;
    }
    
    $factor_actual = 0;
    $tickets_factor = array();
    foreach($ticketsByTitle as $ticket){
      $ft = $this->getFactorConfianzaTicket($correo, $ticket);
      //Factory::debug($ft);
      if($ft > $factor_actual){
        $factor_actual = $ft;
        $tickets_factor = array($ticket->dc_ticket);
      }else if($ft == $factor_actual){
        $tickets_factor[] = $ticket->dc_ticket;
      }
    }
    
    Factory::debug($factor_actual);
    
    if($factor_actual >= 4){
      $factor['confianza'] = $factor_actual;
      $factor['tickets'] = $tickets_factor;
    }
    
    return $factor;
    
  }

  private function getRealTitle($title) {
    $pattern = '/^[A-Z]{2}\\:(.+)$/';
    $matches = null;
    $returnValue = preg_match($pattern, $title, $matches);

    if ($returnValue) {
      return trim($matches[1]);
    } else {
      return false;
    }
  }

  private function getTicketsByFactorTitulo($titulo) {
    $db = $this->factory->getConnection();

    $select = $db->prepare(
            $db->select(
                    'tb_ticket_servicio t
                       JOIN tb_correo_ticket c ON c.dc_correo = t.dc_correo_default',
                    't.dc_ticket, t.dc_cliente, c.dg_body, c.dg_from',
                    't.dg_titulo = ? AND t.dm_estado = 1 AND t.df_creacion > DATE_SUB(NOW(), INTERVAL 45 DAY)')
    );
    $select->bindValue(1, $titulo, PDO::PARAM_STR);
    $db->stExec($select);

    return $select->fetchAll(PDO::FETCH_OBJ);
  }
  
  private function getFactorConfianzaTicket(Correo $correo, $ticket){
    $factor = 2;
    
    if($ticket->dc_cliente == $this->guessCliente($correo)){
      $factor += 1;
    }
    
    if($ticket->dg_from == $correo->getFrom()){
      $factor += 1;
    }
    
    
    if(strpos(strip_tags($correo->getBody()), strip_tags($ticket->dg_body)) !== false){
      $factor += 3;
    }
    
    return $factor;
  }

  public function getMailsByTicket(Ticket $ticket) {
      $db = $this->factory->getConnection();
      
      $correos = $db->prepare(
                  $db->select('tb_relacion_correo_ticket r
                               JOIN tb_correo_ticket c ON c.dc_correo = r.dc_correo',
                              'c.*','r.dc_ticket = ?',array('order_by' => 'c.df_creacion ASC'))
              );
      $correos->bindValue(1, $ticket->getId(), PDO::PARAM_INT);
      $db->stExec($correos);
      
      $collection = array();
      
      while($row = $correos->fetch(PDO::FETCH_OBJ)){
        $collection[] = $this->getCorreoByRow($row);
      }
      
      return $collection;
  }

  public function getMailById($dc_correo) {
    $db = $this->factory->getConnection();
    
    $row = $db->getRowById('tb_correo_ticket', $dc_correo, 'dc_correo');
    
    $correo = $this->getCorreoByRow($row);
    
    return $correo;
    
  }
  
  private function getCorreoByRow($row){
    $correo = new Correo();
    $correo->setAsunto($row->dg_asunto);
    $correo->setBody($row->dg_body);
    $correo->setFrom($row->dg_from);
    $correo->setFecha($row->df_correo);
    $correo->setUID($row->dc_uid);
    $correo->setId($row->dc_correo);
    $correo->setAttachments($this->getAttachmentsByCorreo($correo));
    $correo->setHasAttachment($row->dm_attachment);
    $correo->setHasInline($row->dm_inline);
    
    return $correo;
  }
  
  public function getAttachmentsByCorreo(Correo $correo){
    $db = $this->factory->getConnection();
    
    $query = $db->prepare(
              $db->select('tb_correo_ticket_attachment a
                           JOIN tb_correo_part_content p ON p.dc_part = a.dc_part_content',
                      'p.dc_part, p.dg_filename, p.dg_mimetype',
                      'a.dc_correo = ?'));
    $query->bindValue(1, $correo->getId(), PDO::PARAM_INT);
    $db->stExec($query);
    
    return $query->fetchAll(PDO::FETCH_OBJ);
    
  }

  public function guessCliente(Correo $correo) {
    $db = $this->factory->getConnection();
    
    $cliente = $db->prepare($db->select('tb_contacto_cliente', 'dc_cliente', 'dg_email = ?'));
    $cliente->bindValue(1, $correo->getFrom(), PDO::PARAM_INT);
    $db->stExec($cliente);
    
    if(($c = $cliente->fetch(PDO::FETCH_OBJ)) === false){
      return NULL;
    }
    
    return $c->dc_cliente;
    
  }
  
  public function getAttachmentIcon($mimetype){
    switch ($mimetype){
      case 'text/plain':
        return 'icon-file-alt';
        break;
      default :
        return 'icon-file';
    }
  }

}

?>
