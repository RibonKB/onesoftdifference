<?php

/**
 * Description of Ticket
 *
 * @author Tomás Lara Valdovinos
 * @date 21-06-2013
 */
class Ticket {

  private $id;
  private $cliente;
  private $numeroTicket;
  private $titulo;
  private $correo_default;
  private $empresa;
  private $fecha;

  public function getCliente() {
    return $this->cliente;
  }

  public function setCliente($cliente) {
    $this->cliente = $cliente;
  }

  public function getNumeroTicket() {
    return $this->numeroTicket;
  }

  public function setNumeroTicket($numeroTicket) {
    $this->numeroTicket = $numeroTicket;
  }

  public function getTitulo() {
    return $this->titulo;
  }

  public function setTitulo($titulo) {
    $this->titulo = $titulo;
  }

  public function getCorreo_default() {
    return $this->correo_default;
  }

  public function setCorreo_default($correo_default) {
    $this->correo_default = $correo_default;
  }

  public function getEmpresa() {
    return $this->empresa;
  }

  public function setEmpresa($empresa) {
    $this->empresa = $empresa;
  }
  
  public function getFecha() {
    return $this->fecha;
  }

  public function setFecha($fecha) {
    $this->fecha = new DateTime($fecha);
  }

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

}

?>
