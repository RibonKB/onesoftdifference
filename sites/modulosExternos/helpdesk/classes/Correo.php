<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Correo
 *
 * @author Tomás Lara Valdovinos
 * @date 14-06-2013
 */
class Correo {

  private $id;
  private $from;
  private $asunto;
  private $fecha;
  private $cuenta;
  private $body;
  private $UID;
  private $attachments = array();
  private $inline = array();
  private $factorConfianza;
  private $hasAttachment;
  private $hasInline;

  /**
   *
   * @var Ticket
   */
  private $ticket = null;

  /**
   *
   * @var array
   */
  private $relations = array();

  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getFrom() {
    return $this->from;
  }

  public function setFrom($from) {
    $this->from = $from;
  }

  public function getAsunto() {
    return $this->asunto;
  }

  public function setAsunto($asunto) {
    $this->asunto = trim($asunto);
  }

  public function getFecha() {
    return $this->fecha;
  }

  public function getCuenta() {
    return $this->cuenta;
  }

  public function setCuenta($cuenta) {
    $this->cuenta = $cuenta;
  }

  public function setFecha($fecha) {
    $this->fecha = $fecha;
  }

  public function getBody() {
    $body = $this->strip_tags($this->body, 'script');
    $body = $this->strip_tags($body, 'style');
    $body = $this->replaceIdByDefault($body);
    return $body;
  }

  public function setBody($body) {
    $this->body = $body;
  }

  public function getUID() {
    return $this->UID;
  }

  public function setUID($UID) {
    $this->UID = $UID;
  }

  public function getAttachments() {
    return $this->attachments;
  }

  public function setAttachments($attachments) {
    $this->attachments = $attachments;
  }

  public function getInline() {
    return $this->inline;
  }

  public function setInline($inline) {
    $this->inline = $inline;
  }

  public function getTicket() {
    return $this->ticket;
  }

  public function setTicket(Ticket $ticket) {
    $this->ticket = $ticket;
  }

  public function getRelations() {
    return $this->relations;
  }

  public function setRelations($relations) {
    $this->relations = $relations;
  }

  public function getFactorConfianza() {
    return $this->factorConfianza;
  }

  public function setFactorConfianza($factorConfianza) {
    $this->factorConfianza = $factorConfianza;
  }

  public function getHasAttachment() {
    return $this->hasAttachment;
  }

  public function setHasAttachment($hasAttachment) {
    $this->hasAttachment = $hasAttachment;
  }

  public function getHasInline() {
    return $this->hasInline;
  }

  public function setHasInline($hasInline) {
    $this->hasInline = $hasInline;
  }

  public function strip_tags($html, $tag) {
    $pos1 = false;
    $pos2 = false;
    do {
      if ($pos1 !== false && $pos2 !== false) {
        $first = NULL;
        $second = NULL;
        if ($pos1 > 0)
          $first = substr($html, 0, $pos1);
        if ($pos2 < strlen($html) - 1)
          $second = substr($html, $pos2);
        $html = $first . $second;
      }
      preg_match("/<" . $tag . "[^>]*>/i", $html, $matches);
      $str1 = & $matches[0];
      preg_match("/<\/" . $tag . ">/i", $html, $matches);
      $str2 = & $matches[0];
      $pos1 = strpos($html, $str1);
      $pos2 = strpos($html, $str2);
      if ($pos2 !== false)
        $pos2 += strlen($str2);
    } while ($pos1 !== false && $pos2 !== false);
    return $html;
  }
  
  public function replaceIdByDefault($html){
    return preg_replace('/\"cid\:(.+)\"/', '"images/default_mail_image.png" image-data="$1"', $html);
  }

}

?>
