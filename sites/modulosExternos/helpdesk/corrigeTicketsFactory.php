<?php

class corrigeTicketsFactory extends Factory{
	
	public function indexAction(){
		$db = $this->getConnection();
		$tickets = $db->doQuery($db->select('tb_ticket_servicio','dc_ticket, dg_titulo'));
		
		while($t = $tickets->fetch(PDO::FETCH_OBJ)){
			
			$u = $db->prepare($db->update('tb_ticket_servicio',array(
				'dg_titulo' => '?'
			),"dc_ticket = {$t->dc_ticket}"));
			$u->bindValue(1,imap_utf8($t->dg_titulo),PDO::PARAM_STR);
			$db->stExec($u);
			
		}
		
		$correos = $db->doQuery($db->select('tb_correo_ticket','dc_correo, dg_asunto'));
		
		while($t = $correos->fetch(PDO::FETCH_OBJ)){
			
			$u = $db->prepare($db->update('tb_correo_ticket',array(
				'dg_asunto' => '?'
			),"dc_correo = {$t->dc_correo}"));
			$u->bindValue(1,imap_utf8($t->dg_asunto),PDO::PARAM_STR);
			$db->stExec($u);
			
		}
		
	}
	
}

?>