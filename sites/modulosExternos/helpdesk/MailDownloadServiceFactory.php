<?php

require_once 'inc/FactoryNoLogin.php';
require_once 'sites/modulosExternos/helpdesk/classes/Correo.php';
require_once 'sites/modulosExternos/helpdesk/classes/Ticket.php';

/**
 * Description of MailDownloadServiceFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 14-06-2013
 */
class MailDownloadServiceFactory extends FactoryNoLogin {

  private $connectionData;
  private $log;
  private $inicioTime;

  /**
   * @var IMAPService
   */
  private $imap;

  const LOG_DEFAULT_LEVEL = "LOG";
  const LOG_ERROR_LEVEL = "ERR";
  const LOG_WARNING_LEVEL = "WRN";
  const TICKET_CONFIANZA = 0;
  const TICKET_NUEVO = 1;
  
  public function testAction(){
    $man = $this->getCorreoManagerService();
    $correo = new Correo();
    $correo->setAsunto('RV: Prueba');
    $correo->setBody('<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><p dir="ltr">Correo enviado desde una casilla ajena a microsoft</p>
<p dir="ltr">Correo enviado desde una casilla ajena a microsoft</p>
<p dir="ltr">Correo enviado desde una casilla ajena a microsoft</p>');
    $correo->setFrom('sir.thomas@adischile.cl');
    
    self::debug($man->getFactorConfianza($correo));
    
  }

  public function indexAction() {
    $this->initLogFile();
    $this->initIMAPDataConnection();
    $this->IMAPInboxLogin();
    //self::debug($this->imap->getFolders());
    //self::debug($this->imap->fetchEstructure(2));
    //self::debug($this->imap->getHeaders());
    $this->getConnection()->start_transaction();
    $this->insertarCorreos();
    $this->getConnection()->commit();
    $this->archivarLeidos();
    //header("Cache-Control: public");
    //header("Content-Description: File Transfer");
    //header("Content-Disposition: inline; filename=test.pdf");
    //header("Content-Type: application/PDF");
    //ob_clean();
    //flush();
    //echo '<pre>';
    //var_dump($imap->getUID(self::getRequest()->mail));
    //var_dump($imap->fetchEstructure(self::getRequest()->mail));
    //$f = fopen(__DIR__.'/attachments/test.jpg','w');
    //fputs($f, $imap->decodeBase64($imap->fetchBody(self::getRequest()->mail, '2')));
    //fclose($f);
    //echo '</pre>';
    //echo '<hr>';
    //echo $imap->decodeBase64($imap->fetchBody(self::getRequest()->mail, '2'));
  }
  
  public function __construct() {
    parent::__construct();
    $this->inicioTime = microtime(true);
  }

  public function __destruct() {
    if ($this->log) {
      $this->writeLog("La descarga demoró ".(microtime(true) - $this->inicioTime)." segundos");
      $this->closeLogFile();
    }
  }

  private function IMAPInboxLogin() {
    $imap = $this->getImapService();

    try {
      $imap->login('{' . $this->connectionData->server_string . '}INBOX', $this->connectionData->usermail, $this->connectionData->password);
    } catch (Exception $e) {
      $this->writeLog($e->getMessage(), self::LOG_ERROR_LEVEL);
      exit;
    }

    $this->writeLog('Conexión a: ' . $this->connectionData->usermail . '@' . $this->connectionData->server_string . ' INBOX ');

    $this->imap = $imap;
  }

  private function insertarCorreos() {
    $headers = $this->imap->getHeaders();

    if (!count($headers)) {
      $this->writeLog('No se descargaron correos');
      exit;
    }

    $this->writeLog(count($headers) . " correos descargados desde el servidor");
    
    $this->insertarCorreosByHeader($headers);
    
  }

  private function insertarCorreosByHeader($headers) {
    $correoManager = $this->getCorreoManagerService();

    $tipos = array(
        self::TICKET_CONFIANZA => 0,
        self::TICKET_NUEVO => 0
    );

    foreach ($headers as $header) {
      $c = $this->generateCorreo($header);
      $tipo = $this->relacionaTicket($c);
      $correoManager->insertarCorreo($c);
      $tipos[$tipo]++;
    }

    if ($tipos[self::TICKET_NUEVO]) {
      $this->writeLog($tipos[self::TICKET_NUEVO] . " tickets nuevos insertados");
    }

    if ($tipos[self::TICKET_CONFIANZA]) {
      $this->writeLog($tipos[self::TICKET_NUEVO] . " correos asociados a tickets existentes");
    }
  }

  private function generateCorreo($header) {

    $header->Msgno = intval(trim($header->Msgno));

    $correo = new Correo();
    $correo->setAsunto(isset($header->subject) ? $header->subject : '');
    $correo->setFrom($header->from[0]->mailbox . '@' . $header->from[0]->host);
    $correo->setUID($this->imap->getUID($header->Msgno));
    $correo->setFecha(date('Y-m-d H:i',$header->udate));
    $correo->setCuenta($this->connectionData->codigo_cuenta);

    $parts = $this->imap->getFinalParts($header->Msgno, $this->imap->fetchEstructure($header->Msgno));
    if (isset($parts['HTML'])) {
      $correo->setBody($parts['HTML']);
    } else if (isset($parts['PLAIN'])) {
      $correo->setBody($parts['PLAIN']);
    } else {
      $correo->setBody('');
    }

    if (isset($parts['attachment'])) {
      $correo->setAttachments($parts['attachment']);
    }

    if (isset($parts['inline'])) {
      $correo->setInline($parts['inline']);
    }

    return $correo;
  }

  private function relacionaTicket(Correo $c) {
    $relations = $this->getCorreoManagerService()->getFactorConfianza($c);
    if (count($relations['tickets'])) {
      $this->relacionaTicketConfianza($relations, $c);
      return self::TICKET_CONFIANZA;
    } else {
      $this->creaTicket($c);
      return self::TICKET_NUEVO;
    }
  }

  private function creaTicket(Correo $c) {
    $c->setFactorConfianza(10);
    
    $ticket = new Ticket();
    $ticket->setTitulo($c->getAsunto());
    $ticket->setCliente($this->getCorreoManagerService()->guessCliente($c));
    $ticket->setNumeroTicket($this->getDocumentCreationService()->getCorrelativo('tb_ticket_servicio', 'dq_ticket', 2, 'df_creacion'));
    $ticket->setEmpresa($this->getEmpresa());
    
    $c->setTicket($ticket);
    
  }

  private function relacionaTicketConfianza(array $relations, Correo $c) {
    $c->setFactorConfianza($relations['confianza']);
    $c->setRelations($relations['tickets']);
  }

  private function archivarLeidos() {
    
    try {
      $this->imap->archivar($this->connectionData->letto_box);
    } catch (Exception $e) {
      $this->writeLog($e->getMessage(), self::LOG_ERROR_LEVEL);
      exit;
    }
  }

  private function initIMAPDataConnection() {
    $this->connectionData = (object) array(
                'codigo_cuenta' => 1,
                'server_string' => 'outlook.office365.com',
                'letto_box' => 'LETTO',
                'usermail' => 'helpdesk@grupotecnologico.cl',
                'password' => 'Passw0rD2013632'
    );
  }

  /**
   * @return IMAPService Servicio de descarga de correos
   */
  private function getImapService() {
    return $this->getService('IMAP');
  }

  /**
   * @return CorreoManagerService
   */
  private function getCorreoManagerService() {
    return $this->getService('CorreoManager');
  }
  
  /**
   * @return DocumentCreationService
   */
  private function getDocumentCreationService(){
    return $this->getService('DocumentCreation');
  }

  private function initLogFile() {
    $this->log = fopen(__DIR__ . '/logs/service.log', 'a');
  }

  private function writeLog($message, $level = self::LOG_DEFAULT_LEVEL) {
    $date = new DateTime();
    $message = $level . ': ' . $date->format(DateTime::ATOM) . ' ' . $message . "\n";
    fwrite($this->log, $message);
  }

  private function closeLogFile() {
    $this->writeLog('--------------');
    fclose($this->log);
  }

}

?>
