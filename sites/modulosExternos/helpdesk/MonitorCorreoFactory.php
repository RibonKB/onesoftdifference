<?php

/**
 * Description of MonitorCorreoFactory
 *
 * @author Tomás Lara Valdovinos
 * @date 25-06-2013
 */
class MonitorCorreoFactory extends Factory {

  protected $title = "Correos Ticket";

  public function indexAction() {
    $correos = $this->getCorreos();

    $body_content = $this->getView($this->getTemplateURL('monitorFull'), array(
        'correos' => $correos
    ));

    echo $this->getView('modulosExternos/helpdesk/layout', array(
        'header_icon' => 'envelope',
        'body_content' => $body_content
    ));
  }
  
  public function leerCorreoAction(){
    $dc_correo = intval(self::getRequest()->id);
    $correo = $this->getCorreoManagerService()->getMailById($dc_correo);
    
    echo $this->getView($this->getTemplateURL('leerCorreo'), array(
        'correo' => $correo
    ));
  }

  private function getCorreos() {
    $db = $this->getConnection();

    $select = $db->prepare(
                $db->select('tb_correo_ticket', 'dc_correo, df_correo, dg_asunto, dg_from, dm_attachment, dc_factor_confianza', 'dc_empresa = ? AND dm_activo = 1',array('order_by'=>'df_correo DESC'))
            );
    $select->bindValue(1, $this->getEmpresa(), PDO::PARAM_INT);
    $db->stExec($select);

    return $select->fetchAll(PDO::FETCH_OBJ);
  }
  
  /**
   * @return CorreoManagerService
   */
  private function getCorreoManagerService(){
    return $this->getService('CorreoManager');
  }

}

?>
