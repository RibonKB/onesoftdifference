<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActualizaCambio
 *
 * @author ejimenez
 */

require_once 'inc/FactoryNoLogin.php';
class ActualizaCambioFactory extends FactoryNoLogin{
    //put your code here
    
    public function indexAction(){
        $cambio_m=$this->coneccion('select','tb_tipo_cambio_mensual tcm'
                . ' LEFT JOIN tb_tipo_cambio tc ON tc.dc_tipo_cambio=tcm.dc_tipo_cambio',
                'tcm.dq_cambio,tcm.dc_tipo_cambio',"DATE(tcm.df_cambio)=DATE(NOW()) AND tcm.dq_cambio != 0");
        $cambio_m=$cambio_m->fetchAll(PDO::FETCH_OBJ);
       
        foreach($cambio_m as $d):
                $this->coneccion('update','tb_tipo_cambio',array('dq_cambio'=>"'".$d->dq_cambio."'"),"dc_tipo_cambio={$d->dc_tipo_cambio}");
        endforeach;
    }
}
