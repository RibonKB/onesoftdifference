<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of notificaTareasFactory
 *
 * @author Eduardo
 */

require ('enviaM.php');
require_once 'inc/FactoryNoLogin.php';

class notificaTareasFactory extends FactoryNoLogin {
    //put your code here
    
    private $log;
    const LOG_ERROR_LEVEL = "ERR";
    const LOG_DEFAULT_LEVEL = "LOG";
    
    public function indexAction() {
        $this->initLogFile();
        $this->writeLog('1');
        $en = new enviaM;
        $this->writeLog('2');
        $datos = $this->coneccion('select', 'tb_tarea ta
                            LEFT JOIN tb_tipo_tarea tita ON tita.dc_tipo_tarea=ta.dc_tipo_tarea', 'ta.dc_funcionario,
                                tita.dg_tipo_tarea,
                                ta.dc_tipo_tarea,
                                ta.dg_descripcion,
                                ta.dm_estado,
                                ta.df_fecha,
                                Month(ta.df_fecha)-Month(NOW()) Mes_t,
                                Day(ta.df_fecha)-Day(NOW()) Dia_t,
                                Hour(ta.df_fecha)-Day(NOW()) Hora_t,
                                Month(ta.df_notificado)-Month(NOW()) Mes_n,
                                Day(ta.df_notificado)-Day(NOW()) Dia_n,
                                ta.dc_tarea', 
                                'dm_estado IN("P","A")');
        $datos = $datos->fetchAll(PDO::FETCH_OBJ);
        
        $this->writeLog('3');
        $destinatarios=$this->obtener_detinatarios($datos[0]->dc_tarea);

        foreach ($datos as $d) {
            foreach ($destinatarios as $a):
            $func = $this->obtener_datos_FT($a->dc_funcionario);
            $tarea = $d->dg_tipo_tarea;
            //------------------------------
                if($d->Mes_t == 0){
                    if($d->Dia_t == 0){
                        if($d->dm_estado == 'P'){
                            if($d->Hora_t == 1){
                                try{
                                $en->Enviar_mail(
                                        $func->mail, 
                                        $en->subjet(
                                                $tarea
                                                  ), 
                                        $en->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                                }  catch (Exception $e){
                                $this->writeLog($e->getMessage());
                            }
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()'),"dc_tarea={$d->dc_tarea}");
                            }
                            if($d->Hora_t == 0){
                                try{
                                $en->Enviar_mail(
                                        $func->mail, 
                                        $en->subjet(
                                                $tarea
                                                  ), 
                                        $en->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                                }  catch (Exception $e){
                                $this->writeLog($e->getMessage());
                            }
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()','dm_estado'=>'A'),"dc_tarea={$d->dc_tarea}");
                            }
                            if($d->Hora_t == '-1'){
                                try{
                                $en->Enviar_mail(
                                        $func->mail, 
                                        $en->subjet(
                                                $tarea
                                                  ), 
                                        $en->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                                }  catch (Exception $e){
                                $this->writeLog($e->getMessage());
                            }
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()'),"dc_tarea={$d->dc_tarea}");
                            }
                        }
                    }elseif($d->Dia_t > 0){
                        if($d->Dia_n == 1){
                            try{
                            $en->Enviar_mail(
                                        $func->mail, 
                                        $en->subjet(
                                                $tarea
                                                  ), 
                                        $en->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                            }  catch (Exception $e){
                                $this->writeLog($e->getMessage());
                            }
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()'),"dc_tarea={$d->dc_tarea}");
                        }
                    }
                }else{
                    if($d->Dia_n == 1 || $d->Dia_n >1){
                            try{
                            $en->Enviar_mail(
                                        $func->mail, 
                                        $en->subjet(
                                                $tarea
                                                  ), 
                                        $en->getMensaje(
                                                $d->dg_descripcion, 
                                                $func->nombre, 
                                                $d->df_fecha
                                                       )
                                                );
                            }  catch (Exception $e){
                                $this->writeLog($e->getMessage());
                            }
                                $this->coneccion('update','tb_tarea',array('df_notificado'=>'NOW()'),"dc_tarea={$d->dc_tarea}");
                        }
                }
                $this->writeLog('Mensaje no enviado');
            //------------------------------
            endforeach;
        }
    }

    private function obtener_datos_FT($dc_funcionario) {
        $datos = $this->coneccion('select', 'tb_funcionario', 'dg_email mail,CONCAT(dg_nombres," ",dg_ap_paterno," ",dg_ap_materno)  nombre', "dc_funcionario={$dc_funcionario}");
        $datos = $datos->fetch(PDO::FETCH_OBJ);
        return $datos;
    }
    
    private function obtener_detinatarios($id_tarea){
        $dest=$this->coneccion('select','tb_tarea_usuario_notificacion','dc_funcionario',"dc_tarea={$id_tarea}");
        $dest=$dest->fetchAll(PDO::FETCH_OBJ);
        return $dest;
    }
    
    private function initLogFile() {
    $this->log = fopen(__DIR__ . '/logs/service.log', 'a');
  }

  private function writeLog($message, $level = self::LOG_DEFAULT_LEVEL) {
    $date = new DateTime();
    $message = $level . ': ' . $date->format(DateTime::ATOM) . ' ' . $message . "\n";
    fwrite($this->log, $message);
  }

  private function closeLogFile() {
    $this->writeLog('--------------');
    fclose($this->log);
  }
}

?>
