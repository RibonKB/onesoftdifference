<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestorTareasFactory
 *
 * @author Eduardo
 */

require_once 'inc/FactoryNoLogin.php';

class gestorTareasFactory extends FactoryNoLogin {

    public function indexAction() {
        $estado = 'P';
        $tarea = $this->coneccion('select', 'tb_tarea ta 
                    LEFT JOIN tb_tipo_tarea tita ON tita.dc_tipo_tarea=ta.dc_tipo_tarea', 'ta.dc_tarea,
                    MONTH(ta.df_fecha)-MONTH(NOW()) as mes,
                    DAY(ta.df_fecha)-DAY(NOW()) dia, 
                    HOUR(ta.df_fecha)-HOUR(NOW()) hora,
                    tita.dg_tipo_tarea', "dm_estado NOT IN ('C')");
        $tarea = $tarea->fetchAll(PDO::FETCH_OBJ);
        foreach ($tarea as $t):
                if ($t->mes == 0) {
                    if ($t->dia < 0):
                        $estado = 'A';
                    endif;
                    if ($t->dia == 0):
                        if (strstr($t->dg_tipo_tarea, 'Oportunidad')) {
                            if ($t->hora >= 0) {
                                $estado = 'P';
                            } else {
                                $estado = 'A';
                            }
                        } else {
                            $estado = 'P';
                        }
                    endif;
                    if($t->dia > 0):
                        $estado='P';
                    endif;
            }
            $this->coneccion('update', 'tb_tarea', array('dm_estado' => "'".$estado."'"), "dc_tarea={$t->dc_tarea}");
        endforeach;
    }
}

?>
