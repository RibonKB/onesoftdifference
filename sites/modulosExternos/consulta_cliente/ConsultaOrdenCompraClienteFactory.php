<?php

class ConsultaOrdenCompraClienteFactory extends Factory {
    
    private $params;
    private $errorMsg;
    
    public function indexAction(){
        $this->getParams();
        $this->validateParams();
        $data = $this->getData();
        Factory::debug($data);
        //echo json_encode();
    }
    
    private function getParams(){
        $r = $this->getRequest();
        $this->params = new stdClass();
        if(!empty($r->dg_rut)){
            $this->params->rutEmpresa = trim($r->dg_rut);
            $this->params->cotizacion = isset($r->dg_cotizacion) ? trim($r->dg_cotizacion):'';
        }
        else {
            $this->errorMsg = 'Es necesario que ingrese el rut de su empresa para consultar sus pedidos';
        }
    }
    
    private function validateParams(){
        if(!isset($this->params->rutEmpresa)){
            return false;
        }
        
        if(empty($this->params->rutEmpresa)){
            $this->errorMsg = 'Es necesario que ingrese el rut de su empresa para consultar sus pedidos';
        }
    }
    
    private function getData(){
        $cabeceras = $this->getCabeceras();
        $datos = $this->getDetalles($cabeceras);
        return $this->validateDatos($datos);
        
    }
    
    private function getCabeceras(){
        $db = $this->getConnection();
        $table = ' tb_nota_venta nvt 
            JOIN tb_cotizacion cot ON cot.dc_cotizacion = nvt.dc_cotizacion 
            JOIN tb_cliente cte ON nvt.dc_cliente = cte.dc_cliente
            JOIN tb_funcionario fun ON nvt.dc_ejecutivo = fun.dc_funcionario 
            JOIN tb_contacto_cliente clc ON nvt.dc_contacto = clc.dc_contacto';
        $fields = 'nvt.dc_nota_venta, nvt.df_fecha_envio,
            cot.dq_cotizacion, 
            cte.dg_rut, cte.dg_razon, 
            clc.dg_contacto';
       
        $conditions = ' cte.dg_rut = :rut AND nvt.dc_empresa = :empresa AND nvt.dm_nula = :nula';
        
        if(!empty($this->params->cotizacion)){
            $conditions .= ' AND cot.dq_cotizacion = :cotizacion ';
        } else {
            $conditions .= ' AND cot.dq_cotizacion <> 0 ';
        }
        $st = $db->prepare($db->select($table, $fields, $conditions, ' ORDER_BY cot.dq_cotizacion DESC LIMIT 5'));
        $st->bindValue(':rut', $this->params->rutEmpresa, PDO::PARAM_STR);
        $st->bindValue(':empresa', 1, PDO::PARAM_INT);
        $st->bindValue(':nula', 0, PDO::PARAM_INT);
        
        if(!empty($this->params->cotizacion)){
            $st->bindValue(':cotizacion', $this->params->cotizacion, PDO::PARAM_STR);
        }
        
        $db->stExec($st);
        return $st->fetchAll(PDO::FETCH_OBJ);
        
    }
    
    private function getDetalles($cabeceras){
        if(empty($cabeceras)){
            return false;
        }
        
        $db = $this->getConnection();
        $table = 'tb_nota_venta_detalle';
        $fields = 'dg_descripcion, dq_cantidad, dc_recepcionada, dc_despachada, dc_facturada, dc_rutada, df_fecha_arribo';
        $conditions = ' dc_empresa = :empresa AND dc_nota_venta = :notaVenta';
        $st = $db->prepare($db->select($table, $fields, $conditions));
        $st->bindValue(':empresa', 1, PDO::PARAM_INT);
        $datos = array();
        foreach($cabeceras as $c => $v){
            $datos[$c] = $v;
            $st->bindParam(':notaVenta', $v->dc_nota_venta, PDO::PARAM_INT);
            $db->stExec($st);
            $datos[$c]->detalle = $st->fetchAll(PDO::FETCH_OBJ);
        }
        
        return $datos;
        
    }
    
    private function validateDatos($datos){
        if($datos === false){
            $this->errorMsg = 'No hemos encontrado cotizaciones para ustedes. Para mayor información contáctese con su ejecutivo';
            return false;
        }
        foreach($datos as $c => $v){
            if(empty($v->detalle)){
                $datos[$c] = false;
            }
        }
        
        return (object)array_filter($datos);
    }
    
}