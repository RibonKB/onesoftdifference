<?php
session_start();
if(!isset($_POST['asinc'])){
	$options['Detalle'] = "Se prohibe el acceso directo a la página especificada, probablemente tenga desactivado Javascript";
	$error_man->show_fatal_error("Acceso Denegado",$options);
}

define("MAIN",1);
require_once("../inc/settings.php");

// Genera el objeto encargado de mostrar errores
require_once("../inc/Error_class.php");
$error_man = new Error();
	
// Generar el objeto que se conecta con la base de datos.
require_once("../inc/DBConnector.php");

$db = new DBConnector(array("user" => $settings["DBusername"],"password" => $settings["DBpassword"],"server" => $settings["DBserver"],"database" => $settings["DBbasedatos"]));

	
	//Se obtiene el rut de la empresa a loguear, en caso de que no exista se genera un error y se debe reintentar
	$empresa = $db->select("tb_empresa","dg_rut","dc_empresa='{$_POST['empresa_id']}'");
	
	if(!count($empresa)){
		$error_man->showWarning("La empresa especificada no está registrada");
		exit();
	}
	$empresa = $empresa[0];
	setcookie('log_empresa_rut',$empresa['dg_rut'],time()+(60*60*24*7),"/");
	
	$empresa =& $_POST['empresa_id'];

	//si los datos son correctos se redirige a la página principal
	$log = $db->select("tb_usuario","dc_usuario","dc_empresa = {$empresa} AND dg_usuario = '{$_POST['log_username']}' AND dg_password='{$_POST['log_password']}' AND dm_activo='1'");
	if(count($log)){
	$log = $log[0];
		//se registra la variable de sesión con el identificador del usuario
		$_SESSION['uid'] = $log['dc_usuario'];
/*
if($_SESSION['uid'] == 2){
	$_SESSION['uid'] = 12;
        $log['dc_usuario']=12;
}*/
		
		$permisos = $db->query("
		(SELECT dc_permiso FROM tb_grupo_permisos p
		JOIN (	SELECT g.dc_grupo
				FROM tb_grupo g 
     			JOIN (	
					SELECT dc_grupo 
					FROM tb_usuario_grupo 
					WHERE dc_usuario = {$log['dc_usuario']}
				) u ON g.dc_grupo = u.dc_grupo
			 ) gr ON gr.dc_grupo = p.dc_grupo)
		UNION
		(SELECT dc_permiso 
		 FROM tb_usuario_permisos
		 WHERE dc_usuario = {$log['dc_usuario']})");
		 
		$result = array();
		while($r = mysql_fetch_array($permisos,MYSQL_NUM)){
			$result[] = $r[0];
		}
		$_SESSION['usuario_permisos'] = $result;
		
		include("../inc/ipcity/vendor/geoip/geoip/src/geoipcity.inc");
		include("../inc/ipcity/vendor/geoip/geoip/src/geoipregionvars.php");
		
		$gi = geoip_open("../inc/ipcity/GeoLiteCity.dat",GEOIP_STANDARD);
		
		$record = geoip_record_by_addr($gi, $_SERVER['REMOTE_ADDR']);
		
		if($record == NULL){
			//se crea un registro/log de la conexión de un usuario
			$db->insert("tb_usuario_log_conexion",
			array(
				"dc_usuario" => $log['dc_usuario'],
				"df_fecha" => "NOW()",
				"dg_ip" => $_SERVER['REMOTE_ADDR']
			));
		}else{
			
			//se crea un registro/log de la conexión de un usuario
			$db->insert("tb_usuario_log_conexion",
			array(
				"dc_usuario" => $log['dc_usuario'],
				"df_fecha" => "NOW()",
				"dg_pais" => $record->country_code . " " . $record->country_code3 . " " . $record->country_name,
				"dg_ciudad" => $record->city,
				"dg_latitud" => $record->latitude,
				"dg_longitud" => $record->longitude,
				"dg_ip" => $_SERVER['REMOTE_ADDR']
			));
			
		}
		 
		
?>
	<script type="text/javascript">
		window.location = "index.php";
	</script>
<?php
	}
	else{
		$error_man->showWarning("Los datos de conexión especificados son incorrectos");
	}
?>
