var tc = 0;
var tc_dec = 0;

$('#oc_nota_venta').autocomplete('sites/proc/autocompleter/nota_venta_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

$('#oc_orden_servicio').autocomplete('sites/proc/autocompleter/orden_servicio_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
}).attr('disabled',true);

var add_stock_libre_box = function(producto,stock_libre){
	var stock_a = $('<a>').multiAttr({
		href:'sites/ventas/orden_compra/proc/set_free_stock_to_document.php?id='+producto,
		title: 'Hay stock libre disponible de este producto, haga click acá para usarlo en la nota de venta'
	}).click(function(e){
		e.preventDefault();
		loadOverlay(this.href);
	});

	var cajon = $('<img>').multiAttr({
		src:'images/inventario.png',
		alt:'[_]'
	}).css({verticalAlign:'middle', marginRight: '5px'});

	stock_a.prepend($('<strong>').text(stock_libre).css({lineHeight:'25px'})).append(cajon);
	return stock_a
};

var change_nota_venta = function(){
	var v = get_disabled(this);

	$.getJSON('sites/ventas/orden_compra/proc/get_nota_venta_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('La nota de venta especificada no existe o esta no ha sido confirmada o validada');
			return;
		}

		if($('#prod_list .main').size() > 0){
			if(confirm('Se eliminarán todos los detalles que haya agregado\n¿Continuar?')){
				$('#prod_list .main').remove();
			}else{
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				return;
			}
		}

		disable_button('#prod_add');
		$('#oc_orden_servicio').attr('disabled',true);
		$(':hidden[name=fv_id_orden_servicio]').val(0);

		if(data[0] == '<empty>'){
			show_aviso('La nota de venta especificada no posee detalle, pero se asigna de todas formas');
			var id = data[1].dc_nota_venta;
		}else{
			var det = data[0];
			var id = data[1].dc_nota_venta;
			var cambio = data[1].dq_cambio;

			if($('#prod_tipo_cambio').val() != 0){
				if(confirm("¿Quiere cambiar el tipo de cambio de la nota de venta por el que utiliza la nota de venta?")){
					tc = data[1].dq_cambio;
					tc_dec = 2;
					$('#prod_tipo_cambio').val(data[1].dc_tipo_cambio);
					$('#oc_cambio').val(parseFloat(data[1].dq_cambio).toFixed(tc_dec));
				}
			}else{
				$('#prod_tipo_cambio').val(data[1].dc_tipo_cambio);
				tc_dec = 2;
				$('#prod_tipo_cambio').change();
				tc = data[1].dq_cambio;
				$('#oc_cambio').val(parseFloat(data[1].dq_cambio).toFixed(tc_dec));
			}

			for(i in det){
				if(det[i].dc_producto != null){
					var det_id = det[i].dc_nota_venta_detalle;
					var q = det[i].dq_cantidad;
					var stock_libre = det[i].dq_stock_libre?det[i].dq_stock_libre:0;
					var producto = det[i].dc_producto;
					var ceco = det[i].dc_ceco;
					var d = get_valid_detail(det[i]);
					d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_detail[]'}).val(det_id));
					if(stock_libre > 0){
						d.find('.prod_stock_libre').append(add_stock_libre_box(producto,stock_libre)).addClass('alert').css({paddingTop:0, paddingBottom:0});
					}
					d.find('.prod_ceco').val(ceco);
					var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
					d.find('.prod_cant').after(maxv);
					d.prependTo('#prod_list');
				}else{
					add_invalid_detail(det[i]);
				}
			}

			if(valid_det){
				$('#prod_list :hidden[name=id_detail[]]').each(function(){
					for(i in valid_det){
						if(valid_det[i] == $(this).val()){
							$(this).parent().parent().find('.prod_cant').val(valid_cant[i]);
							return;
						}
					}
					$(this).parent().parent().remove();
				});
				valid_det = false;
			}

			actualizar_detalles();
			actualizar_totales();

			create_number_div(v.val(),'nota_venta',change_nota_venta,function(){
				$('#oc_orden_servicio').attr('disabled',false);
				if(confirm('¿Desea mantener los detalles agregados?')){
					$('#prod_list :hidden[name=id_detail[]]').remove();
					$('#prod_list .code-div').each(function(){$(this).prepend(create_edit_code_btn());});
				}else{
					$('#prod_list .strict').remove();
				}
				enable_button('#prod_add');
			}).width(v.width()).replaceAll(v);
			$(':hidden[name=oc_id_nota_venta]').val(id);

		}
	});
};

$('#oc_nota_venta').change(change_nota_venta);

var change_orden_servicio = function(){
	if($('#prod_tipo_cambio').val() == 0){
		$(this).val('');
		show_error('Debe especificar antes de elegir una orden de servicio el tipo de cambio a utilizar');
		return;
	}

	var v = get_disabled(this);

	$.getJSON('sites/ventas/orden_compra/proc/get_orden_servicio_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la orden de servicio especificada, intentelo denuevo');
			return;
		}

		if(data[0] == '<empty>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('La orden de servicio especificada no posee costos pendientes de compra.');
			return;
		}

		if($('#prod_list .main').size() > 0){
			if(confirm('Se eliminarán todos los detalles que haya agregado\n¿Continuar?')){
				$('#prod_list .main').remove();
			}else{
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				return;
			}
		}

		disable_button('#prod_add');
		$('#oc_nota_venta').attr('disabled',true);
		$(':hidden[name=oc_id_nota_venta]').val(0);

			var det = data[0];
			var id = data[1].dc_orden_servicio;

			for(i in det){
				if(det[i].dc_producto != null){
					var det_id = det[i].dc_detalle;
					det[i].dc_proveedor = $(':hidden[name=prov_id]').val();
					var q = det[i].dq_cantidad;
					var stock_libre = det[i].dq_stock_libre;
					var producto = det[i].dc_producto;
					var d = get_valid_detail(det[i]);
					d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_detail[]'}).val(det_id));
					if(stock_libre > 0){
						d.find('.prod_stock_libre').append(add_stock_libre_box(producto,stock_libre)).addClass('alert').css({paddingTop:0, paddingBottom:0});
					}
					var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
					d.find('.prod_cant').after(maxv);
					d.prependTo('#prod_list');
				}else{
					add_invalid_detail(det[i]);
				}
			}

			actualizar_detalles();
			actualizar_totales();

			create_number_div(v.val(),'orden_servicio',change_orden_servicio,function(){
				$('#oc_nota_venta').attr('disabled',false);
				if(confirm('¿Desea mantener los detalles agregados?')){
					$('#prod_list :hidden[name=id_detail[]]').remove();
					$('#prod_list .code-div').each(function(){$(this).prepend(create_edit_code_btn());});
				}else{
					$('#prod_list .strict').remove();
				}
				enable_button('#prod_add');
			}).width(v.width()).replaceAll(v);
			$(':hidden[name=oc_id_orden_servicio]').val(id);

	});
};

$('#oc_orden_servicio').change(change_orden_servicio);

var get_disabled = function(e){
	return $(e).blur().attr('disabled',true).css({
		backgroundImage: 'url(images/ajax-loader.gif)',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center right'
	});
};

var get_valid_detail = function(d){
	row = $('#prods_form tr').clone(true);
	row.find('.prod_desc').val(d.dg_descripcion);
	row.find('.prod_proveedor').val(d.dc_proveedor);
	row.find('.prod_cant').val(d.dq_cantidad);
	row.find('.prod_costo').val(parseFloat(d.dq_precio_compra/tc).toFixed(tc_dec));
	row.find('.fecha_arribo').val(d.df_fecha_arribo);
	create_code_div(d.dg_codigo,d.dc_producto,d.dg_descripcion).replaceAll(row.find('.prod_codigo'));
	return row;
};


var set_detail = function(det){
	for(var i in det){
		if(det[i].dc_producto != null){
			row = $('#prods_form tr').clone(true);
			row.find('.fecha_arribo').val(det[i].df_fecha_arribo);
			row.find('.prod_desc').val(det[i].dg_descripcion);
			row.find('.prod_proveedor').val(det[i].dc_proveedor);
			row.find('.prod_cant').val(det[i].dq_cantidad);
			row.find('.prod_costo').val((det[i].dq_precio_compra/tc).toFixed(tc_dec));
			row.appendTo('#prod_list');
			create_code_div(det[i].dg_codigo,det[i].dc_producto,det[i].dg_descripcion).replaceAll(row.find('.prod_codigo'));
		}else{
			row = $('<tr>');
			col = $('<td>').css({
				border: '1px solid #F00',
				background: '#FFFFBF'
			});

			crbtn = $('<img>').multiAttr({
				src: 'images/addbtn.png',
				alt: '[+]',
				title: 'Crear el producto'
			}).click(function(){
				loadOverlay('sites/ventas/proc/cr_producto.php?c='+encodeURIComponent(det[i].dg_codigo)+'&d='+encodeURIComponent(det[i].dg_descripcion)+'&q='+encodeURIComponent(det[i].dq_precio_compra)+'&v=0');
			});

			col.clone().prepend(crbtn).appendTo(row);
			col.clone().text(det[i].dg_codigo).appendTo(row);
			col.clone().text(det[i].dg_descripcion).appendTo(row);
			col.clone().text('-').appendTo(row);
			col.attr('align','right');
			col.clone().text(mil_format(det[i].dq_cantidad)).appendTo(row);
			col.clone().text(mil_format((det[i].dq_precio_compra/tc).toFixed(tc_dec))).appendTo(row);
			col.clone().text(mil_format((det[i].dq_precio_compra*det[i].dq_cantidad/tc).toFixed(tc_dec))).appendTo(row);
			$('#prod_list').prev('thead').append(row);

		}

		actualizar_detalles();
		actualizar_totales();
	}
};

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac);
});

var create_code_div = function(code,id,desc){
	var div = $('<div>').css({
		border:'1px solid #CCC',
		background:'#EEE',
		padding:'5px',
		fontWeight:'bold'
	}).addClass('code-div').mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.text(code)

	$('<input>').multiAttr({
		type:'hidden',
		name:'prod[]'
	}).val(id).appendTo(div);


	$('<input>').multiAttr({
		type:'hidden',
		class:'hidden_desc'
	}).val(desc).appendTo(div);

	return div;
};

var create_edit_code_btn = function(){
	return $('<img>').multiAttr({
		class: 'right hidden',
		src: 'images/editbtn.png',
		alt: '[|]',
		title: 'Editar código',
		width: '13',
		height: '13'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'searchbtn prod_codigo',
			type: 'text',
			size: '7'
		}).autocomplete('sites/proc/autocompleter/producto.php',{
			formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
		.result(result_ac)
		.replaceAll(innerdiv);
	});
};

var create_number_div = function(number,acl,chfn,cb){
	var edbtn = $('<img>').multiAttr({
		src: 'images/editbtn.png',
		title: 'Modificar',
		alt: '[|]',
		class:'right hidden'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'inputtext',
			type: 'text',
			id: 'oc_'+acl,
			size: '41',
			maxlength: '255'
		}).autocomplete('sites/proc/autocompleter/'+acl+'_strict.php',{
			formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
			minChars: 2,
			width:300
		}).result(function(){
			$(this).change();
		}).change(chfn).replaceAll(innerdiv);

		$(':hidden[name=oc_id_'+acl+']').val(0);
		cb();
	});

	var div = $('<div>').addClass('inputtext').css({
		background:'#EEE',
		fontWeight:'bold'
	}).text(number)
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.prepend(edbtn);

	return div;
};

var result_ac = function(e,row){

		el = $(this).parent().parent();
		cant = el.find('.prod_cant');
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}

		el.find('.prod_desc').val(row[1]);
		el.find('.prod_price').val(mil_format(parseFloat(row[3]/tc).toFixed(tc_dec)));
		el.find('.prod_costo').val(mil_format(parseFloat(row[4]/tc).toFixed(tc_dec)));

		create_code_div(row[0],row[5],row[1]).replaceAll(this).prepend(create_edit_code_btn());

		actualizar_detalles();
		actualizar_totales();
};


$("#prod_tipo_cambio").change(function(){
	indice = this.selectedIndex;
	if(indice == 0){
		$("#prod_info").show();
		$("#prods").hide();
		$('#oc_orden_servicio').attr('disabled',true);
	}else{
		$('#oc_orden_servicio').attr('disabled',false);
		$("#prod_info").hide();
		$("#prods").show();
		var cambio = this.options[indice].innerHTML.split("|");
		cambio = cambio[1];
		tc_dec = $(":hidden[name=decimal"+$(this).val()+"]").val();
		$('#oc_cambio').val(cambio).trigger('change');
	}
});

$('#oc_cambio').change(function(){
	var cambio = parseFloat($(this).val());
	var old = tc;
	if(!cambio){
		$(this).val(tc);
	}else{
		$(this).val(cambio);
		tc = cambio;
	}
	var list = $("#prod_list tr.main");
	if(list.size() && old != tc){
		if(confirm("¿Cambiar la tasa de cambio en los productos del detalle?")){
			list.each(function(){
				var costo = toNumber($(this).find('.prod_costo').val())*old;
				$(this).find('.prod_costo').val(mil_format(costo/tc));
			});
		}
	}
	actualizar_detalles();
	actualizar_totales();
});

$('.margen_p').change(function(){
	cost = toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec);
	margin = toNumber($(this).val());
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	price = $(this).parent().parent().find('.prod_price').val(mil_format(parseFloat(cost)+(cost*margin/100)));
	actualizar_detalles();
	actualizar_totales();
});

var actualizar_detalles = function(){

	$("#prod_list tr.main").each(function(){
		var cant = parseFloat($(this).find('.prod_cant').val());
		var cost = toNumber($(this).find('.prod_costo').val()).toFixed(tc_dec);

		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}

		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(tc_dec)));

		local = $(this).next();
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
	});
};

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
	actualizar_totales();
});

$('.get_description').click(function(){
	var desc = $(this).parent().next().find('.hidden_desc').val();
	$(this).parent().next().next().children('.prod_desc').val(desc);
});

$('.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('.prod_cant').change(function(){
	var mn = $(this).parent().find('.min_value');
	if(mn.size()){
		if(parseFloat($(this).val()) < parseFloat(mn.val())){
			$(this).val(mn.val());
		}
	}
	var mx = $(this).next('.max_value');
	if(mx.size()){
		if(parseFloat($(this).val()) > parseFloat(mx.val())){
			$(this).val(mx.val());
		}
	}
	actualizar_detalles();
	actualizar_totales();
});

$('#oc_cambio').change(function(){
	var nv_cambio = $(this).val();
	if(!parseFloat(tc)){
		nv_cambio = 1.0;
		$(this).val(1);
	}
	tc = nv_cambio;
	actualizar_detalles();
	actualizar_totales();
});

$('.ventas_form').submit(function(e){
	e.preventDefault();
	if(!check_proveedores()){
		show_error('A especificado detalles de proveedores que no corresponden al elegido para la orden de compra');
		$('.prod_proveedor').addClass('invalid');
		return;
	}

	if(!check_codigos()){
		show_error('Los códigos de los productos deben estar todos perfectamente asignados');
		$('#prod_list').parent().addClass('invalid');
		return;
	}
	if($('#prod_list tr.main').size() == 0){
		show_error('No ha agregado productos a la orden de compra');
		return;
	}
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

var actualizar_totales = function(){

var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$("#prod_list tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		cost = toNumber($(this).find('.prod_costo').val());

		total_costo += cost*cant;
		total_neto += cost*tc*cant;
	});

	$('#total_costo').text(mil_format(total_costo.toFixed(tc_dec)));

	$('#total_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$('#total_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$('#total_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));

	$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
	$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));

};

var check_codigos = function(){
	var tr = $("#prod_list tr.main").size();
	var code = $("#prod_list :hidden[name=prod[]]").size();
	return tr==code;
};

var check_proveedores = function(){
var b = true;
var p = $(":hidden[name=prov_id]").val();
$("#prod_list .prod_proveedor").each(function(){
	if(p != $(this).val())
		b = false;
});
return b;
};
