jQuery.fn.tableOverflow = function(rows){
	return this.each(function(){
		
		var tbody = $(this).find('tbody');
		
		var tr = tbody.find('tr');
		
		tr.each(function(index, element) {
            var td = $(this).find('td');
			
			td.each(function(i, e) {
                var h = $(this).html();
				var div = $('<div>').addClass('div-overflow').width(rows[i]).attr('title',h).html(h);
				$(this).html('').append(div);
            });
			
        });
		
	});
};