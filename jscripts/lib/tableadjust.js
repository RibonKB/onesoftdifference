jQuery.fn.tableAdjust = function(rows,withLeft,width){
	
	if(!withLeft){
		withLeft = false;
	}
	
	if(!width){
		width = false;
	}
	
return this.each(function(){
	
		var table = $(this);
		var tr = $('tbody tr',table);
		
		if(tr.size() < rows){
			if(width){
				var div = $('<div>').css({
					maxWidth: width+'px',
					overflow: 'auto'
				});
				
				table.wrap(div);
			}
			return;
		}
		
		var td = $('td',tr.first());
		
		var tdWidths = [];
		td.each(function(){
			tdWidths.push($(this).width());
			$(this).width($(this).width());
		});
		
		tr.first().find('td').each(function(index){
			$(this).width(tdWidths[index]);
		});
		
		var thead = table.children('caption').add(table.children('thead'));
		var tfoot = table.children('tfoot');
		//var thead = $('caption',table).add($('thead',table));
		//var tfoot = $('tfoot',table);
		
		var containerHeight = tr.first().height() * rows;
		
		var widths = table.width();
		
		var div = $('<div>').height(containerHeight).width(widths+20).css({
			overflow: 'auto'
		});
		
		var headDiv = $('<div>');
		var footDiv = $('<div>');
		
		var theadtable = $('<table>').width(widths).append(thead.clone(true)).addClass('tab')
		.append($('<tbody>').append(tr.first().clone().find('td').removeAttr('class').removeAttr('id').html('')));
		thead.addClass('hidden');
		
		var tfoottable = $('<table>').width(widths).append(tfoot.clone(true)).addClass('tab')
		.append($('<tbody>').append(tr.first().clone().find('td').removeAttr('class').removeAttr('id').html('')));
		tfoot.addClass('hidden');
		
		if(withLeft){
			table.wrap($('<div>').css({
				float: 'left',
				marginRight: '10px'
			}));
		}
		
		if(width){
			div.css({
				maxWidth: width+'px',
				overflow: 'auto',
				marginRight: '25px'
			}).scroll(function(){
				theadtable.parent().scrollLeft($(this).scrollLeft());
				tfoottable.parent().scrollLeft($(this).scrollLeft());
			});
			
			headDiv.add(footDiv).css({
				maxWidth: (width-15)+'px',
				overflow: 'hidden'
			});
			
		}else{
			headDiv.width(widths+10);
			footDiv.width(widths+10);
		}
		
		//table.wrap(div).width(widths);
		table.after(tfoottable).before(theadtable).width(widths);
		theadtable.wrap(headDiv);
		tfoottable.wrap(footDiv);
		table.wrap(div);
		
});}