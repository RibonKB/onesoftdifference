var PYMERP_IFRAME_EXPORT = function(EXPORT_URL,cacheid){
	var frame = $('<iframe>').addClass('hidden').appendTo('body').attr('src',EXPORT_URL.concat('?cache=').concat(cacheid));
	frame.load(function(){
		//frame.remove();
	});
};

var PYMERP_EXCEL_EXPORT = function(cacheid){
	var EXPORT_URL = 'sites/proc/tablexport/excel_export.php';
	PYMERP_IFRAME_EXPORT(EXPORT_URL, cacheid);
};
var PYMERP_PDF_EXPORT = function(cacheid){
	var EXPORT_URL = 'sites/proc/tablexport/pdf_export.php';
	PYMERP_IFRAME_EXPORT(EXPORT_URL, cacheid);
};
var PYMERP_EMAIL_EXPORT = function(cacheid){
	var EXPORT_URL = 'sites/proc/tablexport/email_export.php';
	show_aviso("Envio por correo en proceso de implementación");
};

var tableExport = {
	actualCache: null,
	exportURL: null,
	
	
	exportFormatList: [
		{EXPORT_TYPE: 'Excel',	EXPORT_FN: PYMERP_EXCEL_EXPORT,	EXPORT_IMG: 'images/export_icon/excel.jpg',	EXPORT_TITLE: 'Exportar en Excel'},
		{EXPORT_TYPE: 'PDF',	EXPORT_FN: PYMERP_PDF_EXPORT,	EXPORT_IMG: 'images/export_icon/pdf.jpg',	EXPORT_TITLE: 'Exportar en PDF'},
		{EXPORT_TYPE: 'Email',	EXPORT_FN: PYMERP_EMAIL_EXPORT,	EXPORT_IMG: 'images/export_icon/mail.jpg',	EXPORT_TITLE: 'Enviar por correo'},
	],
	
	i: 1,
	
	bufferSize: 32768,

	recursiveCacheSend: function(text,callback,actual,total,loadbar){
		if(text.length > tableExport.bufferSize){
			var subst = text.substring(0,tableExport.bufferSize);
			text = text.substring(tableExport.bufferSize);
		}else{
			var subst = text;
			text = '';
			pymerp.hideLoader();
			loadbar.remove();
		}

		var percent = parseInt(actual*100/total);
		if(loadbar){
			loadbar.text(percent+'%');
		}
		
		var data = {
			content: subst
		};
		
		if(tableExport.actualCache != null)
			data.xmlcache = tableExport.actualCache;
		
		$.ajax({
			url: 'sites/proc/tablexport/tocache.php?p='+percent,
			data: data,
			type: 'POST',
			success: function(data){
				tableExport.actualCache = data;
				if(text != ''){
					tableExport.i++;
					tableExport.recursiveCacheSend(text,callback,actual+1,total,loadbar);
				}else{
					callback(data);
					tableExport.actualCache = null;
				}
			}
		});
	},
	
	contentCache: function(callback,table){
		table = $(table).clone();
		//Elimina los inline tags que trae problemas con el reconocimientos del XML
		table.find('area,base,col,command,embed,img,input,link,meta,param,source,tr:hidden,tr.hidden').remove();
		//table.find('br,hr').replaceWith('\n');
		var content = '<table>'.concat($(table).html()).concat('</table>');
		var times = content.length/tableExport.bufferSize;
		pymerp.showLoader();
		var loadbar = $('<span>').css({
		  'position': 'absolute',
		  'left': 0,
		  'top': 0,
		  'z-index': 99999999,
		  'font-size': '30px'
		}).appendTo($('body'));
		tableExport.recursiveCacheSend(content,callback,0,times,loadbar);
	}
	
}

jQuery.fn.tableExport = function(){
return this.each(function(){
	var table = $(this);
	var div = $('<div>').css({float:'left'});
	var separator = null;
	table.before(div);
	div.after('<br class="clear">');
	for(i in tableExport.exportFormatList){
		var img = $('<img>').multiAttr({
			src: tableExport.exportFormatList[i].EXPORT_IMG,
			title: tableExport.exportFormatList[i].EXPORT_TITLE,
			rel: i
		}).css({
			float:'left',
			margin:'5px 10px'
		}).click(function(){
			var id = $(this).attr('rel');
			tableExport.contentCache(tableExport.exportFormatList[id].EXPORT_FN,table);
		});
		separator = $('<span>').css({float:'left',padding:'5px 0',color:'#999'}).text('|');
		
		div.append(img).append(separator);
	}
	separator.remove();
});
};

//
