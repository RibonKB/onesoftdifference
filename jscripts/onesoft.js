/**
* @fileoverview Archivo con las funciones principales utilizadas por la mayoria de las páginas del sistema
*  es cargado solo una vez al inicio ya que provee funciones de carga de contenido con ajax hace el sistema mucho más rápido
*  y con menos tiempos de carga.
*
*	a lo largo de la documentacion se utilizará [nombre_variable] para referirse a las variables (parametros mayormente) así se sabe cuando se esté hablando de una variable..
*
*	Esta documentación será unicamente referencia para los programadores que se quieren guiar en el desarrollo de onesoft Pymerp
*	Y debe ser borrada (p.e. utilizando un compresor de código javascript) en el producto final para disminuir tiempos de carga.
*
* @author Tomás Lara
* @version 0.2b
**/

/**
*	Carga un archivo de eventos (logs) desde sites/proc/eventos/{nombrearchivo}.php, {nombrearchivo} es en realidad el parametro [f]
*	Todos los datos de la lista de eventos son cargados en el contenedor especificado por [t]
*
*	@param {string} f #file# el nombre del archivo de eventos que se cargará, por ejemplo f = cliente cargará sites/proc/eventos/cliente.php
*	@param {mixed} t #target# el contenedor que mostrará el resultado devuelto por el archivo de eventos (logs)
*	@param {mixed} i #id# el id que referencia al log (p.e. el id del cliente del que se mostrarán sus eventos, o el id del producto del que se mostrarán sus eventos)
*	@param {mixed} c #caller# el elemento que gatilla la muestra de eventos (logs), este debe estar configurado con la propiedad onclick llamando esta función, como la carga de eventos puede ser muy pesada se deshabilita las nuevas cargas eliminando la propiedad onclick
**/
var loadEventos = function(f,t,i,c){
	//alert('Función loadEventos deprecada');
	$(t).html('<img src="images/ajax-loader.gif" alt="cargando... />"');
	$(c).removeAttr("onclick");
	loadFile('sites/proc/eventos/'+f+'.php',t,'id='+i);
};

/**
*	Reemplaza una imagen por otra con su mismo nombre seguido por "_over" en el mismo directorio, este método es usado por las pantallas principales de los gestores de maestros (p.e. gestorclientes.php) y es llamado al pasar el mouse sobre las imagenes.
*
*	@param {img:element} i #image# La imagen que se cambiará.
**/
var overImg = function(i){
	//alert('Función overImg deprecada');
	s=i.src;
	p=s.lastIndexOf(".");
	s=s.substring(p);
	i.src=i.src.replace(s,'_over'+s);
};

/**
*	Restaura la imagen cambiada por el metodo overImg
*
*	@param {img:element} i #image# La imagen que cambió y será restaurada
**/
var restoreImg = function(i){
	//alert('Función restoreImg deprecada');
	i.src=i.src.replace('_over','');
};

/**
*	Importa el formulario para agregar un nuevo contacto (desde sites/proc/contactoForm.php). Requiere haber declarado la variable [contacto]{int} que llevará el indice del contacto (para agregar multiples contactos) 
*
*	@param {string} o #identificador# parte del nombre del identificador del contenedor del formulario de contacto, por ejemplo o="contacto" generara un identificador #contacto_3 (donde 3 es el valor del acumulador [contacto])
*	@param {string} t #type# carpeta donde se buscará el archivo de template de contacto (clientes,proveedores,etc)
*	@returns {string} retorna el identificador del contenedor del formulario de contacto generado (p.e. "#contacto_4")
**/
var addContacto = function(o,t){
	//alert('Función addContacto deprecada');
	contacto++;
	if(addc_inline){
		v="il="+cli;
	}
	else{
		v="";
	}
	$(document.createElement('fieldset')).attr("id",o+"_"+contacto).appendTo('#'+o);
	loadFile("sites/"+t+"/proc/contactoForm.php","#"+o+"_"+contacto,"indice="+contacto+"&target="+o+"&"+v);
	return "#"+o+"_"+contacto;
};

/**
*	Elimina el formulario de nuevo contacto agregado anteriormente, este método es utilizado por el boton [[X]] al tope del formulario para eliminar el formulario de contacto.
*
*	@param {string} o #identificador# el id asignado al agregar el formulario, por ejemplo "contacto", (para mejor referencia ver metodo addContacto)
*	@param {string} i #indice# el indice del formulario a eliminar (en el metodo addContacto este es [contacto]), entre [o] y [i] conforman #o_i el idnetificador del contenedor del formulario generado por addContacto.
**/
var delContacto = function(o,i){
	//alert('Función delContacto deprecada');
	if(confirm("Est\xe1 seguro que quiere eliminar el contacto?")){
	$("#"+o+"_"+i).slideUp("fast",function(){ $(this).remove(); });
	}
};

/**
*	Oculta o muestra el formulario de contacto, para que ocupe menos espacio, este metodo es llamado por el boton [[v]] en la cabeza del formulario generado por addContacto.
*
*	@param {string} o #identificador# el id asignado al agregar el formulario, por ejemplo "contacto", (para mejor referencia ver metodo addContacto)
*	@param {string} i #indice# el indice del formulario a eliminar (en el metodo addContacto este es [contacto]), entre [o] y [i] conforman #o_i el idnetificador del contenedor del formulario generado por addContacto.
**/
var hidContacto = function(o,i){
	//alert('Función hidContacto deprecada');
	div = $("#"+o+"_"+i+" .contacto_cont");
	div.slideToggle("fast");
};

/**
*	Elimina el formulario de un contacto y además lo deshabilita en la bse de datos, este metodo es llamado por el botón [[X]] en el formulario de edición de clientes, muestra advertencias.
*
*	Nota: no lo elimina de la base de datos, solo lo deshabilita
*
*	@param {string} o #identificador# el id asignado al agregar el formulario, por ejemplo "contacto", (para mejor referencia ver metodo addContacto)
*	@param {string} i #indice# el indice del formulario a eliminar y el primarykey del contacto a deshabilitar , entre [o] y [i] conforman #o_i el idnetificador del contenedor del formulario generado por addContacto.
*	@param {string} t #tipo# clientes, proveedores, etc.
**/
var deshContacto = function(o,i,t){
	//alert('Función deshContacto deprecada');
	if(confirm("Est\xe1 seguro que quiere eliminar el contacto de los registros?\nNo estar\xe1 disponible en ning\xfan tipo de contacto\n * Para removerlo de un tipo de contacto especifico solo desmarque el checkbox")){
	$("#"+o+"_"+i+" .contacto_cont").slideUp("fast",function(){
		loadFile("sites/"+t+"/proc/eliminarcontacto.php","#"+o+"_"+i,"id="+i);
	});
	}
};

/**
*	Metodo llamado desde el buscador avanzado de clientes, obtiene el RUT de la fila de la tabla de resultados y se lo asigna al campo de texto identificado por [i]
*
*	@param {mixed} i #id# identificador del campo de texto en el que se mostrará el RUT obtenido.
*	@param {v} v #value# el RUT que se asignará al campo de texto.
**/
var setRutVal = function(i,v){
	//alert('Función setRutVal deprecada');
	$(i).val(v);
	$('.ol[rel]').overlay().close();
};

/**
*	hace desaparecer el formulario especificado, y muestra un boton para volver a mostrarlo, es llamado para que no se hagan multiples envios del formulario a la vez.
*
*	@param {mixed} i #id# identificador del formulario que se ocultará
**/
var disableForm = function(i){
	//alert('Función disableForm deprecada');
	$(i+" input").each(function(){
		$(this).blur();
	});
	show_loader();
	$(i).slideUp("fast");
	var btn = $('<input>').multiAttr({
		'type':'button',
		'class':'button',
		'value':'Volver a mostrar formulario'
	}).click(function(){
		enableForm(i,this);
	});
	$(i).before(btn);
	//$(i).before('<input type="button" class="button" onclick="enableForm(\''+i+'\',this)" value="Volver a mostrar formulario" />');
};

/**
*	Muestra un formulario oculto, este metodo es utilizado por el botón generado por el metodo disableForm para mostrar el formulario que oculta, dejandolo habilitado denuevo, además oculta el botón.
*
*	@param {mixed} t #target# el formulario oculto que se mostrará.
*	@param {mixed} c #caller# el elemento que gatilla la llamada a la funcion, este es ocultado y eliminado luego de que el formulario es mostrado.
**/
var enableForm = function(t,c){
	//alert('Función enableForm deprecada');
	$(t).slideDown("fast");
	$(c).slideUp("fast",function(){ $(this).remove(); });
};

/**
*	Valida que los campos obligatorios de un formulario posean al menos 3 caracteres (text,password,date) o se haya seleccionado al menos un elemento de una lista (select), en caso de que no cumpla para todos los campos se retorna false
*
*	@param {mixed} f #form# Formulario que se validará
*	@returns {boolean} retorna true en caso de que el formulario esté correctamente validado, false en caso contrario
**/
var validarForm = function(f){
	//alert('Función validarForm deprecada');
	v=1;
	$(".invalid",f).removeClass("invalid");
	$("select[required]",f).each(
	function(){
		if((!$(this).val())||($(this).val() == '0')){
			v=0;
			$(this).addClass("invalid");
		}
	});
	$("input[type=text],input[class=date],input[type='password'],textarea",f).each(
	function(){
		if($(this).val().length < 1){
			if($(this).attr("required")){
			v = 0;
			$(this).addClass("invalid");
			}
		}
	});
	return v;
};
//OK
/**
*	Envia el formulario especificado por [f], validandolo y deshabilitandolo. es procesado por el archivo indicado en el atributo 'action'
*
*	@param {mixed} f #Form# el formulario que se procesará
*	@param {mixed} t #target# el contenedor que mostrará los resultados devueltos por el archivo procesador
**/
var enviarForm = function(f,t){
	//alert('Función enviarForm deprecada');
	if(validarForm(f)){
	disableForm(f);
	p=$(f).serialize();
	a=$(f).attr("action");
	$(t).html("<img src='images/ajax-loader.gif' alt='cargando...' />");
	loadFile(a,t,p,function(){ hide_loader(); });
	}else{
		show_error("Los campos marcados son obligatorios");
	}
};

/**
*	similar al metodo enviarForm, solo que este muestra los resultados del procesamiento en el contenedor principal de contenido (#content)
*
*	@param {mixed} f #Form# el formulario que se procesará
**/
var completeEnviarForm = function(f){
	//alert('Función completeEnviarForm deprecada');
	if(validarForm(f)){
	show_loader();
	p=$(f).serialize();
	a=$(f).attr("action");
	loadFile(a,"#content",p,function(){
		globalFunction();
		hide_loader();
	});
	}else{
		show_error("Los campos marcados son obligatorios.");
	}
};

var overlayEnviarForm = function(f){
	//alert('Función overlayEnviarForm deprecada');
	if(validarForm(f)){
		var p = $(f).serialize();
		var a = $(f).attr("action");
		loadOverlay(a,p);
	}else{
		show_error("Los campos marcados son obligatorios.");
	}
};

var confirmEnviarForm = function(f,t){
	//alert('Función confirmEnviarForm deprecada');
	if(confirm("Est\xe1 seguro que quiere continuar?")){
		enviarForm(f,t);
	}
};

/**
*	Carga las comunas para la region especificada por [i] en el <select> especificado por [t]
*
*	@param {string} t #target# el identificador del <select> donde serán cargadas las comunas, es tambien usado para mostrar un cargador en [t]_comloader
*	@param {int} i #id# el identificador único para la región de la que se mostrarán las comunas.
*	@param {int} c #comunaid# el id de la comuna que será mostrada luego de que sean cargadas en el contenedor.
**/
var showComunas = function(t,i,c){
	//alert('Función showComunas deprecada');
	$("#"+t+"_comloader").html('<img src="images/ajax-loader.gif" alt="Cargando..." />');
	id=i.value;
	if(c){
	 loadFile("sites/proc/getcomunas.php","#"+t,"id="+id,function(){
		$("#"+t).val(c);
	 });
	}else{
	 loadFile("../sites/proc/getcomunas.php","#"+t,"id="+id);
	}
	$("#"+t+"_comloader").html('');
};

/**
*	Carga el contenido devuelto por un archivo y lo muestra en el contenedor identificado por [t]
*
*	@param {string} l #link# la ruta del archivo que se cargará, este puede contener variables de cabecera (GET), pero todas estás serán formateadas y enviadas como variables tipo POST
*	@param {mixed} t #target# el contenedor que mostrará la información devuelta por el archivo cargado.
*	@param {string:querystring} p #parameters# parametros extra enviados (por POST) al archivo. en formato querystring (variable=valor&variable2=valor2)
*	@param {function} c #callback# función que será llamada luego de cargados los datos en el contenedor, ideal para asignar eventos a los elementos creados.
**/
var loadFile = function(url,target,parameter,callback){
	//alert('Función loadFile deprecada');
	q='';
	if(url.lastIndexOf('?')+1){
		q = '&'+url.substring(url.lastIndexOf('?')+1);
		url = url.substring(0,url.lastIndexOf('?'));
	}
	$.ajax({
  url: url,
  type: 'POST',
  data: 'asinc=1'+q+'&'+parameter,
  error: function(){
	show_error('Ha ocurrido un error al intentar acceder al recurso, intentelo denuevo o contacte con un administrador del sistema');
  },
  success: function(data) {
	  $(target).html(data);
	  if(callback){
		  callback();
	  }
  }
});
};

var show_error = function(m){
	//alert('Función show_error deprecada');
	show_alert(m,'error','warning');
};

var show_aviso = function(m){
	//alert('Función show_aviso deprecada');
	show_alert(m,'Atención','alert');
};

var show_confirm = function(m){
	//alert('Función show_confirm deprecada');
	show_alert(m,'Atención','confirm');
};

var show_alert = function(m,title,type){
	//alert('Función show_alert deprecada');
	$("#overlay_alert_container").remove();
	$('<div>').attr("class","overlay").attr("id","overlay_alert_container").appendTo("#content");
	$('<div>'+title+'</div>').attr("class","secc_bar").appendTo("#overlay_alert_container");
	$("<br /><div class='"+type+"'>"+m+"</div><br />").appendTo("#overlay_alert_container");
	$("<button class='button right'>Cerrar</button>").click(function(){$("#overlay_alert_container").remove(); $.mask.close();}).appendTo("#overlay_alert_container");
	$("#overlay_alert_container").overlay({load: true, fixed:false, closeOnClick: false, oneInstance: false, mask: {color: '#ddd'}});
	$("#loader").hide();
};

/**
*	Carga el contenido de un archivo en el contenedor principal (#content).
*
*	@param {string} l #link# la ruta del archivo que se cargará.
**/
var loadpage = function(l,p){
	//alert('Función loadpage deprecada');
	js_data = null;
	if(!p) p = '';
	show_loader();
	loadFile(l,"#content",p,function(){ 
		hide_loader();
		$('#content input[type=text]').first().focus();
		globalFunction();
	});
};

var show_loader = function(){
	//alert('Función show_loader deprecada');
	$("#loader").show().expose({loadSpeed:0,closeOnClick:false,color:'#F4F4F4',closeSpeed:0});
};

var hide_loader = function(){
	//alert('Función hide_loader deprecada');
	$("#loader").hide();
	$.mask.close();
};

var loadOverlay = function(l,p){
	//alert('Función loadOverlay deprecada');
	$("#genOverlay").remove();
	$('<div>').attr("class","overlay").attr("id","genOverlay").appendTo("#content");
	show_loader();
	loadFile(l,"#genOverlay",p,function(){
		$("#genOverlay").overlay({load: true, fixed:false, closeOnClick:false, oneInstance: false});
		globalFunction();
		$("#genOverlay").append('<div class="close">');
		$("#genOverlay .close").click(function(){ $("#genOverlay").remove(); });
		hide_loader();
	});
};

/*$(document).ready(
function(){
	
$('.header_menu_item a').toggle(function(){
	$('.header_menu_item div').hide();
	$(this).next('div').show();
},function(){
	$('.header_menu_item div').hide();
});
	
$("#close_menu").toggle(function(){
	$("#secc").hide();
	$("#content").css({paddingLeft:0});
	$("#secc_bar").css({paddingLeft:31});
	$("#main_cont").css({paddingLeft:1});
	$(this).text('Mostrar Menú');
},function(){
	$("#secc").show();
	$("#content").css({paddingLeft:170});
	$("#secc_bar").css({paddingLeft:30});
	$("#main_cont").css({paddingLeft:0});
	$(this).text('Ocultar Menú');
});

$(".accordion ul a").click(
function(e){
	e.preventDefault();
	loadpage(this.href);
	$(".acc_active").removeClass("acc_active");
	$(this).addClass("acc_active");
});

$(".accordion").tabs(".accordion > div.pane", {tabs: '> h2', effect: 'slide', initialIndex: null, toggle: true});
$(".group_index").tabs(".group_index > div.ind_group_body", {tabs: '> .ind_group_head', effect: 'slide', initialIndex: null, toggle: true});

globalFunction();

});*/

/**
*	Función llamada al cargar una página, inicializa los eventos globales
**/
var globalFunction = function(){
	
	//alert('Función globalFunction deprecada');
	$("#content a.loader").click(
function(){
	loadpage(this.href);
	return false;
});

$(".bicolor_tab tr:even td").css({"backgroundColor": "#E5E5E5"});
	
$(".ac_results").remove();

$("a.loadOnOverlay").click(
function(e){
	e.preventDefault();
	loadOverlay(this.href);
});

$(".tip").tooltip({position:'bottom center',effect:'slide'});
$("#tabs").tabs("div.tabpanes > div",{effect:'default'});
$(".ol[rel]").overlay({mask:'#EEE'});
$(".validar").unbind('submit');
$(".validar").submit(function(e){
e.preventDefault();
i="#"+$(this).attr("id");
enviarForm(i,i+"_res");
});

$(".cValidar").submit(function(e){
e.preventDefault();
var i="#"+$(this).attr("id");
completeEnviarForm(i);
});

$(".confirmValidar").submit(function(e){
e.preventDefault();
var i="#"+$(this).attr("id");
confirmEnviarForm(i,i+"_res");
});

$('.overlayValidar').submit(function(e){
	e.preventDefault();
	var i="#"+$(this).attr("id");
	overlayEnviarForm(i,i+"_res");
});

$(".inputrut").Rut();


$("#other_options a").toggle(function(){
	$("#other_options .menu:visible").prev().trigger('click');
	$(this).next('.menu').show();
},function(){
	$(this).next('.menu').hide();
});

$.tools.dateinput.localize("es", {
   months: 'Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre',
   shortMonths:  'Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Sep,Oct,Nov,Dic',
   days:         'Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado',
   shortDays:    'Dom,Lun,Mar,Mie,Jue,Vie,Sab'
});

$("input.date").dateinput({
	lang:'es',
	firstDay:1,
	format:'dd/mm/yyyy',
	selectors:true,
	initialValue:0,
	yearRange:[-80,80]
});
};

var mil_format = function(numero){
	//alert('Función mil_format deprecada');
	var n = numero.toString();
	if(n.indexOf('.') != -1){
		var arr = n.split('.');
		var number = arr[0];
		var decimal = '.'+arr[1];
	}else{
		var number = n;
		var decimal = '';
	}
	var result = '';
	while( number.length > 3 )
	{
	 result = ',' + number.substr(number.length - 3) + result;
	 number = number.substring(0, number.length - 3);
	}
	result = number + result + decimal;
	return result;
};

var toNumber = function(num){
	//alert('Función toNumber deprecada');
	return parseFloat(num.replace(/,/g,''));
};

var disable_button = function(b){
	//alert('Función disable_button deprecada');
	$(b).attr('disabled',true).addClass('disbutton');
};

var enable_button = function(b){
	//alert('Función enable_button deprecada');
	$(b).attr('disabled',false).removeClass('disbutton');
};