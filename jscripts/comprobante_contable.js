init = function(){
	$(".duplicate").unbind("click");
	$(".del_detail").unbind("click");
	$(".det_cta").unbind("change");
	$(".det_haber").unbind("change");
	$(".det_debe").unbind("change");
	$(".show_more").unbind("click");
	
	$(".det_cta").autocomplete(cuentas,
	{
		formatItem: function(row){
			return row.dg_codigo+" "+row.dg_cuenta_contable+" - "+row.dg_descripcion_larga;
		},
		formatMatch: function(row, i, max) {
			return row.dg_codigo+" "+row.dg_cuenta_contable+" "+row.dg_descripcion_larga;
		},
		minChars: 1,
		width:300,
		matchContains: true,
		formatResult: function(row) {
			return row.dg_codigo;
		}
	}).result(function(e,row){
		$(this).parent().next().text(row.dg_cuenta_contable);
		$(this).next().val(row.dc_cuenta_contable);
	});
	
	$('.det_rut').autocomplete('sites/proc/autocompleter/cliente.php',{
		formatItem: function(row){ return row[1]+" ("+row[0]+") "+row[2]; },
		minChars: 2,
		width:300
	});
	
	$(".show_more").click(function(){
		$(this).parent().parent().next().toggle();
		if($(this).attr('src') == "images/minusbtn.png"){
			$(this).attr("src","images/addbtn.png").attr("title","Más detalles");
		}else{
			$(this).attr("src","images/minusbtn.png").attr("title","Menos detalles");
		}
	});
	
	$(".duplicate").click(function(){
		var ref = $(this).parent().parent();
		var ref2 = ref.next();
		var tr = ref.clone();
		var tr2 = ref2.clone();
		var vals = {
			cta: ref.find('.det_cta').val(),
			cta_id: ref.find('.det_cta_id').val(),
			debe: ref.find('.det_debe').val(),
			haber: ref.find('.det_haber').val(),
			glosa: ref.find('.det_glosa').val(),
			tipo_doc: ref.find('.det_tipo_doc').val(),
			num_doc: ref.find('.det_num_doc').val()
		};
		var vals2 = {
			cheque: ref2.find('.det_cheque').val(),
			cheque_date: ref2.find('.det_cheque_date').val(),
			rut: ref2.find('.det_rut').val(),
			activo: ref2.find('.det_activo').val(),
			banco: ref2.find('.det_banco').val(),
			cebe: ref2.find('.det_cebe').val(),
			ceco: ref2.find('.det_ceco').val(),
			doc_venta: ref2.find('.det_doc_venta').val(),
			doc_compra: ref2.find('.det_doc_compra').val()
		};
		for(i in vals){
			tr.find('.det_'+i).val(vals[i]);
		}
		for(i in vals2){
			tr2.find('.det_'+i).val(vals2[i]);
		}
		tr2.find('.det_cheque_date').dateinput({
			lang:'es',
			firstDay:1,
			format:'dd/mm/yyyy',
			selectors:true,
			initialValue:0,
			yearRange:[-80,80]
		});
		tr.appendTo("#detalle_list");
		tr2.appendTo("#detalle_list");
		if($("#detalle_list tr").size() == 12 && header_totals){
			header_totals = false;
			$("#detalle_foot tr").clone().prependTo("#detalle_head");
		}
		init();
		refresh_totals();
	});
	
	$(".del_detail").click(function(){
		if($("#detalle_list tr").size() > 2){
			$(this).parent().parent().next().remove();
			$(this).parent().parent().remove();
			refresh_totals();
		}else{
			alert("Error al eliminar\nEl comprobante contable debe tener al menos 1 detalle");
		}
	});
	
	$(".det_debe").change(function(){
		$(this).val($(this).val().replace(/,/g,''));
		if(!parseFloat($(this).val())){
			$(this).val('');
		}else{
			$(this).val(mil_format((parseFloat($(this).val())).toFixed(2)));
		}
		if($(this).val() != ''){
			$(this).parent().next().children(".det_haber").attr("readonly",true).attr("required",false).val('0.00');
		}else{
			$(this).parent().next().children(".det_haber").attr("readonly",false).attr("required",true).val('');
		}
		refresh_totals();
	});
	
	$(".det_haber").change(function(){
		$(this).val($(this).val().replace(/,/g,''));
		if(!parseFloat($(this).val())){
			$(this).val('');
		}else{
			$(this).val(mil_format((parseFloat($(this).val())).toFixed(2)));
		}
		if($(this).val() != ''){
			$(this).parent().prev().children(".det_debe").attr("readonly",true).attr("required",false).val('0.00');
		}else{
			$(this).parent().prev().children(".det_debe").attr("readonly",false).attr("required",true).val('');
		}
		refresh_totals();
	});
	
	$(".det_cta").change(function(){
		for(i in cuentas){
			if(cuentas[i].dg_codigo == $(this).val())
			return;
		}
		$(this).val('');
		$(this).parent().next().text('-');
	});
}

$("#detalle_add").click(function(){
	gen = $("#detalle_list tr:first").clone();
	ext = $("#detalle_list tr:first").next().clone();
	gen.find("input").val("").attr("readonly",false);
	ext.find("input").val("");
	gen.appendTo("#detalle_list");
	ext.appendTo("#detalle_list");
	if($("#detalle_list tr").size() == 12 && header_totals){
		header_totals = false;
		$("#detalle_foot tr").clone().prependTo("#detalle_head");
	}
	ext.find('.det_cheque_date').dateinput({
		lang:'es',
		firstDay:1,
		format:'dd/mm/yyyy',
		selectors:true,
		initialValue:0,
		yearRange:[-80,80]
	});
	init();
});

$("#comp_tipo_cambio").change(function(){
	$("#comp_cambio").val($("#comp_tipo_cambio option:selected").text().split(' ').pop());
});

$(".comprobante_form").submit(function(e){
	e.preventDefault();
	if($(".diferencia_dh").first().text() != '0.00'){
		alert("Los totales del DEBE y el HABER no cuadran");
		return;
	}
	$("input[name=comp_saldo]").val($(".total_debe").first().text());
	i="#"+$(this).attr("id");
	enviarForm(i,i+"_res");
});

refresh_totals = function(){
	debe = parseFloat('0.00');
	haber = parseFloat('0.00');
	$('.det_debe[value!=""]').each(function(){
		debe += parseFloat($(this).val().replace(/,/g,''));
	});
	$('.det_haber[value!=""]').each(function(){
		haber += parseFloat($(this).val().replace(/,/g,''));
	});
	debe = debe.toFixed(2);
	haber = haber.toFixed(2);
	$(".total_debe").text(mil_format(debe));
	$(".total_haber").text(mil_format(haber));
	$(".diferencia_dh").text(mil_format((Math.abs(debe-haber)).toFixed(2)));
}

var header_totals = true;
init();