var js_saldo_favor = {
	
	init: function(){
		window.setTimeout(function(){
			$('#add_saldos_button').click(js_saldo_favor.set_saldos);
		},100);
	},
	
	set_saldos: function(){
		$('#detalle_saldo').html('');
		var total = 0;
		total += js_saldo_favor.insertHiddens('dc_factura_favor');
		total += js_saldo_favor.insertHiddens('dc_nota_credito_favor');
		total += js_saldo_favor.insertHiddens('dc_orden_compra_favor');
		
		var total_pagar = js_data.getTotal();
		
		if(total >= total_pagar){
			total = total_pagar;
			js_saldo_favor.setDisabledMedioPago();
		}else{
			js_saldo_favor.setEnabledMedioPago();
		}
		
		js_saldo_favor.setGraphicSaldo(total);
		
		$(this).parents('.overlay').remove();
	},
	
	insertHiddens: function(name){
		
		var total = 0;
		
		$('.'+name+':checked').each(function(){
			var hid = $('<input>').multiAttr({
				'type': 'hidden',
				'name': name+'[]',
				'value': $(this).val()
			}).appendTo('#detalle_saldo');
			
			total += parseFloat($(this).next('.dq_saldo_favor_disponible').val());
			
		});
		
		return total;
	},
	
	setGraphicSaldo: function(total){
		
		$('<div>').css({
			'border': '1px solid #BBB',
			'backgroundColor': '#FFF',
			'padding': '5px',
			'margin': '10px 0',
			'lineHeight': '25px'
		})
		.append('<div class="info">Saldo a favor</div>')
		.append('Saldo a favor utilizado: <b>$'+pymerp.milFormat(total)+'</b>')
		.appendTo('#detalle_saldo');
		
	},
	
	setDisabledMedioPago: function(){
		$('#propuesta_medio_pago').addClass('hidden').removeAttr('name').removeAttr('required');
		var div = $('<div>').css({
			'margin': '2px 10px'
		})
		.addClass('confirm')
		.attr('id','no_propuesta_medio_pago')
		.append('<b>Pago completo con saldos a favor</b>')
		.insertAfter('#propuesta_medio_pago');
		
		$('<input>').multiAttr({
			'type': 'hidden',
			'name': 'propuesta_medio_pago'
		}).val(0).appendTo(div);
	},
	
	setEnabledMedioPago: function(){
		var hid = $('#no_propuesta_medio_pago');
		if(hid.size()){
			hid.remove();
			$('#propuesta_medio_pago').removeClass('hidden').multiAttr({
				'name':'propuesta_medio_pago',
				'required': true
			});
		}
	}
	
};
js_saldo_favor.init();