$.extend(js_data,{
	
	gestionForm: $('#cr_propuesta_pago'),
	
	fieldNames: {
		addDoc: '#add_num_doc',
		docsContainer: '#doc_container',
		docInputName: 'gestion_docs[]',
		contacto: '#gestion_contacto',
		contactoInfo: '#contact_data'
	},
	
	messages: {
		docInput: 'Indique el número de documento',
		contactoNotFound: 'No se ha podido encontrar la información del contacto',
	},
	
	docContainer: null,
	docsContainer: null,
	
	overlay: $('#genOverlay'),
	
	cobranzaBaseInit: function(){
		
		//Frame que carga las llamadas a los contactos
		$('<iframe>').attr('name','callframe').addClass('hidden').appendTo(js_data.overlay);
		$(js_data.fieldNames.contacto,js_data.gestionForm).change(js_data.loadContacto);
		
	},
	
	initDocManager: function(){
		
		js_data.docContainer = $('<div>').css({background:'#66E41B',border:'1px solid #53A40B',padding:'3px',fontWeight:'bold',margin:'2px'});
		js_data.docsContainer = $(js_data.fieldNames.docsContainer,js_data.gestionForm);
		
		$('<img>').multiAttr({src:'images/delbtn.png',alt:'(x)',width:15}).click(function(){
			$(this).parent().remove();
		}).appendTo(js_data.docContainer);
		
		$(js_data.fieldNames.addDoc,js_data.gestionForm).click(js_data.addDocument);
		
		
		
	},
	
	addDocument: function(){
		var doc = prompt(js_data.messages.docInput);
		if(doc != ''){
			js_data.docContainer.clone(true).append(doc)
			.append($('<input type="hidden">').attr('name',js_data.fieldNames.docInputName).val(doc))
			.appendTo(js_data.docsContainer);
		}
	},
	
	loadContacto: function(){
		var contacto = $(this).attr('disabled',true).val();
		var enabler = window.setTimeout(js_data.enableContacto,2500);
		$.getJSON('sites/ventas/factura_venta/proc/get_contacto_data.php',{id:contacto},function(data){
			if(data == '<not-found>'){
				show_error(js_data.messages.contactoNotFound);
				return;
			}
			var contactoInfo = $(js_data.fieldNames.contactoInfo,js_data.gestionForm).html('');
			window.clearInterval(enabler);
			js_data.enableContacto();
			//enlace para llamar a teléfono
			var phone = $('<a>').multiAttr({
				href:'http://192.168.0.202/phone/asterisk_call_trigger.php?call='+data.dg_fono+'&ext='+data.anexo+'&razon='+data.dg_razon,
				target: 'callframe'
			}).addClass('right').html('<img src="images/call.png" alt="[C]" title="Llamar" />');
			//Enlace para llamar a teléfono celular
			var movil = $('<a>').multiAttr({
				href:'http://192.168.0.202/phone/asterisk_call_trigger.php?call='+data.dg_movil+'&ext='+data.anexo+'&razon='+data.dg_razon,
				target: 'callframe'
			}).addClass('right').html('<img src="images/call.png" alt="[C]" title="Llamar" />');
			//Enlace para enviar correo electrónico
			var mail = $('<a>').multiAttr({
				href: 'mailto:'+data.dg_email
			}).addClass('right').html('<img src="images/mail.png" alt="[@]" title="Enviar Correo" />');
			
			contactoInfo.css({backgroundColor:'#FFF',lineHeight:'25px',padding:'5px',border:'1px solid #BBB'});
			$('<div>').text('Información del contacto').addClass('info').appendTo(contactoInfo);
			$('<div>').append('Nombre: ').append($('<b>').text(data.dg_contacto)).appendTo(contactoInfo);
			$('<div>').append(phone).append('Telefono: ').append($('<b>').text(data.dg_fono)).appendTo(contactoInfo);
			$('<div>').append(movil).append('Movil: ').append($('<b>').text(data.dg_movil)).appendTo(contactoInfo);
			$('<div>').append(mail).append('e-mail: ').append($('<b>').text(data.dg_email)).appendTo(contactoInfo);
		});
	},
	
	enableContacto: function(){
		$(js_data.fieldNames.contacto,js_data.gestionForm).attr('disabled',false);
	}
});