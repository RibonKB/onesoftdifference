var js_data = {
	
	tabPropuestas: $('#propuestas_data'),
	
	init: function(){
		pymerp.init($('#src_propuesta_pago_res'));
		js_data.tabPropuestas.tableExport().tableAdjust(10);
	},
	
	setStatus: function(dc_propuesta,dm_estado){
		var tr = $('#pt'+dc_propuesta,js_data.tabPropuestas);
		//tr.find('.validar_propuesta').remove();
		var td = tr.find('.estado_propuesta');
		if(td.text() == 'L' || dm_estado == 'L' || dm_estado == 'T'){
			$('#src_propuesta_pago_res').next('.button').remove();
			$('#src_propuesta_pago').trigger('submit');
		}else{
			td.text(dm_estado);
		}
	},
	
	reloadDocAdmin: function(dc_propuesta,dm_estado){
		var tr = $('#pt'+dc_propuesta,js_data.tabPropuestas);
		tr.find('.src_admin_pago').trigger('click');
		if(dm_estado == 'P'){
			tr.find('.estado_propuesta').text(dm_estado);
		}
	},
	
	initConfirmacion: function(dc_propuesta){
		$('#ver_asientos_confirmacion').click(function(){
			$(this).attr('disabled',true);
			pymerp.loadFile('sites/finanzas/pagos/proc/get_asientos_contables.php','#asientos_confirmacion','id='+dc_propuesta,function(){
				window.setTimeout(function(){
					$('#ver_asientos_confirmacion').attr('disabled',false);
					$('#tb_asientos_confirmacion').tableAdjust(10);
				},1000);
			});
		});
	},
	
	refreshListado: function(){
		var form = $('#src_propuesta_pago');
		form.prev('.button').trigger('click');
		pymerp.enviarForm(form,'#src_propuesta_pago_res');
	}
	
};
js_data.init();