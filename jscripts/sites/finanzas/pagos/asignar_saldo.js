js_data = {
	
	form: $('#asignar_saldo_favor_form'),
	autocompleterURL: {
		facturaVenta: null,
		notaCredito: null
	},
	
	init: function(){
		
		// js_data.autocompleterURL.facturaVenta = js_data.buildFactory('facturaVentaAutoCompletar');
		// js_data.autocompleterURL.facturaCompra = js_data.buildFactory('facturaCompraAutoCompletar');
		
		js_data.initFacturaCompraField();
		js_data.initFacturaVentaField();
		
	},
	
	AC_options: {
		formatItem: function(row){
			return row[0]+" ( "+row[1]+" ) "+row[2];
		},
		minChars: 2,
		width: 400
	},
	
	initFacturaCompraField: function(){
		var fc = $('#dc_factura_compra',js_data.form);
		js_data.fc_field = fc.clone();
		
		fc.autocomplete(js_data.autocompleterURL.facturaCompra, js_data.AC_options)
			.result(js_data.acFacturaCompraResult);
	},
	
	initFacturaVentaField: function(){
		var fv = $('#dc_factura_venta',js_data.form);
		js_data.fv_field = fv.clone();
		
		fv.autocomplete(js_data.autocompleterURL.facturaVenta, js_data.AC_options)
			.result(js_data.acFacturaVentaResult);
	},
	
	acFacturaCompraResult: function(e,row){
		var div = pymerp.getEditableBox(row[0],js_data.edFacturaCompraField).width($(this).width());
		div.append($('<input>').attr('name','dc_factura_compra').attr('type','hidden').val(row[3]));
		$(this).replaceWith(div);
		
		pymerp.loadFile(js_data.detalleFacturaCompra,$('#factura_compra_data',js_data.form),'dc_factura_compra='+row[3]);
	},
	
	acFacturaVentaResult: function(e,row){
		var div = pymerp.getEditableBox(row[0],js_data.edFacturaVentaField).width($(this).width());
		div.append($('<input>').attr('name','dc_factura_venta').attr('type','hidden').val(row[3]));
		$(this).replaceWith(div);
		
		pymerp.loadFile(js_data.detalleFacturaVenta,$('#factura_venta_data',js_data.form),'dc_factura_venta='+row[3]);
	},
	
	edFacturaCompraField: function(){
		$(this).parent().replaceWith(js_data.fc_field);
		$('#factura_compra_data',js_data.form).html('');
		js_data.initFacturaCompraField();
	},
	
	edFacturaVentaField: function(){
		$(this).parent().replaceWith(js_data.fv_field);
		$('#factura_venta_data',js_data.form).html('');
		js_data.initFacturaVentaField();
	},
	
	// buildFactory: function(action){
		// return "sites/ventas/nota_credito/index.php?factory=AsignarSaldo&action="+action;
	// }
	
};