var js_data = {
	
	detailContainer: $('#detailContainer'),
	payDetail: $('#payDetail'),
	dc_banco_default: 0,
	
	init: function(dc_banco_default){
		js_data.dc_banco_default = dc_banco_default;
		$('#addPayDetail').click(js_data.addDetail).trigger('click');
	},
	
	addDetail: function(){
		var d = js_data.detailContainer.children('.payDetail').clone();
		js_data.payDetail.prepend(d);
		d.find('.dc_banco').val(js_data.dc_banco_default);
		pymerp.init(d);
	}
	
};