$.extend(js_data,{
	
	massiveFieldNames: {
		mountField: '.mountItem',
		totalField: '#totalMount',
		mountTable: '#mountTable'
	},
	
	cobranzaInit: function(){
		js_data.cobranzaBaseInit();
		js_data.initDocManager();
		
		$(js_data.massiveFieldNames.mountField,js_data.gestionForm).change(js_data.setChangeMount).each(js_data.initMounts);
		js_data.updateTotal();
		window.setTimeout(function(){
			$(js_data.massiveFieldNames.mountTable,js_data.gestionForm).tableAdjust(5);
		},100);
	},
	
	initMounts: function(){
		$(this).val(js_data.toMil($(this).val()))
		.css({
			textAlign: 'right'
		});
	},
	
	setChangeMount: function(){
		var field = $(this);
		var mount = js_data.toNumber(field.val());
		
		if(mount){
			var maxValue = parseFloat(field.parents('td').find('input:hidden').val());
			if(mount > maxValue)
				mount = maxValue;
			field.val(js_data.toMil(mount));
		}else
			field.val(0);
			
		js_data.updateTotal();
	},
	
	updateTotal: function(){
		var total = js_data.getTotal();
		$(js_data.massiveFieldNames.totalField,js_data.gestionForm).text(js_data.toMil(total));
	},
	
	getTotal: function(){
		var total = 0;
		$(js_data.massiveFieldNames.mountField,js_data.gestionForm).each(function(){
			total += js_data.toNumber($(this).val());
		});
		return total;
	}
});
js_data.cobranzaInit();