$.extend(js_data,{
	
	massiveFieldNames: {
		mountField: '.mountItem',
		totalField: '#totalMount',
		mountTable: '#mountTable',
		medioCobro: '#dc_medio_cobro',
		setDetailsBtn: '#set_documents_detail',
		payTabs: '#payTabs'
	},
	
	detailClassName: {
		dg_documento: 'dg_documento',
		df_vencimiento: 'df_vencimiento',
		dg_banco: 'dg_banco',
		dc_banco: 'dc_banco',
		dg_banco_origen: 'dg_banco_origen',
		dc_banco_origen: 'dc_banco_origen',
		df_emision: 'df_emision',
		dq_monto: 'dq_monto',
		dg_codigo_seguro: 'dg_codigo_seguro'
	},
	
	payAdmin: null,
	montoPayAdmin: 0,
	
	cobranzaInit: function(){
		js_data.cobranzaBaseInit();
		js_data.initDocManager();
		js_data.setMedioCobroChange();

		$(js_data.massiveFieldNames.mountField,js_data.gestionForm).change(js_data.setChangeMount).each(js_data.initMounts);
		js_data.updateTotal();
		window.setTimeout(function(){
			$(js_data.massiveFieldNames.mountTable,js_data.gestionForm).tableAdjust(4);
		},100);
		
		$(js_data.fieldNames.contacto,js_data.gestionForm).change();
		
		//$('#cr_gestion_cobranza').unbind('submit').submit(js_data.submitForm);
		
	},
	
	/*submitForm: function(){
		$('#addPayDetail').html('');
		pymerp.overlayEnviarForm(this);
	},*/
	
	initMounts: function(){
		$(this).val(js_data.toMil($(this).val()))
		.css({
			textAlign: 'right'
		});
	},
	
	setChangeMount: function(){
		var field = $(this);
		var mount = js_data.toNumber(field.val());
		
		if(mount){
			var maxValue = parseFloat(field.parents('td').find('input:hidden').val());
			if(mount > maxValue)
				mount = maxValue;
			field.val(js_data.toMil(mount));
		}else
			field.val(0);
			
		js_data.updateTotal();
	},
	
	updateTotal: function(){
		var total = js_data.getTotal();
		$(js_data.massiveFieldNames.totalField,js_data.gestionForm).text(js_data.toMil(total));
	},
	
	getTotal: function(){
		var total = 0;
		$(js_data.massiveFieldNames.mountField,js_data.gestionForm).each(function(){
			total += js_data.toNumber($(this).val());
		});
		return total;
	},
	
	setMedioCobroChange: function(){
		$(js_data.massiveFieldNames.medioCobro).change(js_data.changeMedioCobro);
	},
	
	changeMedioCobro: function(){
		var vals = $(this).val().split(',');
		var extras = vals[1] == '1' || vals[2] == '1';
		var addAccount = vals[3] == '0'  && vals[1] == '0';
		var total = js_data.getTotal();
		
		if(extras){
			$(js_data.massiveFieldNames.payTabs).removeClass('hidden');
			pymerp.loadFile('sites/finanzas/cobros/proc/get_detalles_cobro_documentos.php','#PayDetail','dc_medio_cobro='+vals[0]+'&dq_total='+total,js_data.initPayAdmin);
			js_data.montoPayAdmin = 0;
		}else{
			$(js_data.massiveFieldNames.payTabs).addClass('hidden');
			$('#PayDetail').html('');
		}
		
		if(addAccount){
			$('#cuenta_contable_medio_cobro').removeClass('hidden');
			$('#dc_cuenta_contable').attr('required',true);
			js_data.loadMedioCobroCuentas(vals[0]);
		}else{
			$('#cuenta_contable_medio_cobro').addClass('hidden');
			$('#dc_cuenta_contable').removeAttr('required');
		}
		
		/*if(extras)
			$(js_data.massiveFieldNames.setDetailsBtn).attr('disabled',false).removeClass('disbutton');
		else
			$(js_data.massiveFieldNames.setDetailsBtn).attr('disabled',true).addClass('disbutton');*/
		
	},
	
	loadMedioCobroCuentas: function(dc_medio_cobro){
		
		$('#dc_cuenta_contable option').remove();
		
		$.getJSON('sites/finanzas/cobros/proc/get_medio_cobro_cuentas_contables.php',{'dc_medio_cobro':dc_medio_cobro},function(data){
			
			if(data == '<not-found>'){
				pymerp.showError('Error en la selección de medio de cobro, compruebe los datos de entrada o consulte con un administrador');
				$('#dc_medio_cobro').val(0);
				return;
			}
			
			if(data == '<empty>'){
				pymerp.showError('No se encpontraron cuentas contables para ese medio de cobro, consulte con un adminitrador.');
				$('#dc_medio_cobro').val(0);
				return;
			}
			
			$('#dc_cuenta_contable').append($('<option>').attr('value',0));
			
			for(i in data){
				$('#dc_cuenta_contable').append($('<option>').attr('value',data[i].dc_cuenta_contable).text(data[i].dg_cuenta_contable));
			}
		});
	},
	
	initPayAdmin: function(){
		js_data.payAdmin = $('#addPayDetail');
		pymerp.init(js_data.payAdmin);
		
		$(':input',js_data.payAdmin).removeAttr('name');
		
		$('#addDetail',js_data.payAdmin).click(js_data.addPayDetail);
	},
	
	addPayDetail: function(){
		
		if(!pymerp.validarForm("#addPayDetail")){
			pymerp.showError('Debe Completar todos los campos que sean obligatorios');
			return;
		}
		
		var vals = $(js_data.massiveFieldNames.medioCobro).val().split(',');
		
		var dq_monto = parseFloat($('#dq_monto',js_data.payAdmin).val());
		
		if(!dq_monto){
			pymerp.showError('El formato del monto es inválido, ingrese un número y vuelva a intentarlo');
			return;
		}
		
		if((dq_monto + js_data.montoPayAdmin) > js_data.getTotal()){
			pymerp.showError('El monto del detalle supera el monto a pagar de las facturas, ingrese una suma válida');
			return;
		}
		
		var df_emision = $('#df_emision',js_data.payAdmin).val();
		var dc_banco_origen = $('#dc_banco_origen',js_data.payAdmin).val();
		var dg_banco_origen = $('#dc_banco_origen :selected',js_data.payAdmin).text();
		var dg_codigo_seguro = $('#dg_codigo_seguro',js_data.payAdmin).val();
		if(vals[2] == '1'){
			var df_vencimiento = $('#df_vencimiento',js_data.payAdmin).val();
			var dg_documento = $('#dg_documento',js_data.payAdmin).val();
			
			var valido = true;
			
			$(':input[name=dg_documento\[\]]').each(function(){
				if($(this).val() == dg_documento){
					valido = false;
				}
			});
			
			if(!valido){
				pymerp.showError('El número de documento no debe estar duplicado');
				return;
			}
			
		}
		if(vals[1] == '1'){
			var dc_banco = $('#dc_banco',js_data.payAdmin).val();
			var dg_banco = $('#dc_banco :selected',js_data.payAdmin).text();
		}
		
		var tr = js_data.getEmptyDetailTr(vals);
		
		//Documento
		if(vals[2] == '1'){
			tr.children('.'+js_data.detailClassName.dg_documento)
			.text(dg_documento)
			.append(js_data.getHiddenField(js_data.detailClassName.dg_documento,dg_documento));
			
			//Fecha de vencimiento
			tr.children('.'+js_data.detailClassName.df_vencimiento)
			.text(df_vencimiento)
			.append(js_data.getHiddenField(js_data.detailClassName.df_vencimiento,df_vencimiento));
		}
		
		//Banco
		if(vals[1] == '1'){
			tr.children('.'+js_data.detailClassName.dg_banco)
			.text(dg_banco)
			.append(js_data.getHiddenField(js_data.detailClassName.dc_banco,dc_banco));
		}
		
		//Banco origen
		tr.children('.'+js_data.detailClassName.dg_banco_origen)
		.text(dg_banco_origen)
		.append(js_data.getHiddenField(js_data.detailClassName.dc_banco_origen,dc_banco_origen));
		
		//Fecha emisión
		tr.children('.'+js_data.detailClassName.df_emision)
		.text(df_emision)
		.append(js_data.getHiddenField(js_data.detailClassName.df_emision,df_emision));
		
		//Monto
		tr.children('.'+js_data.detailClassName.dq_monto)
		.text(pymerp.milFormat(dq_monto))
		.append(js_data.getHiddenField(js_data.detailClassName.dq_monto,dq_monto));
		
		//Monto
		tr.children('.'+js_data.detailClassName.dg_codigo_seguro)
		.text(dg_codigo_seguro)
		.append(js_data.getHiddenField(js_data.detailClassName.dg_codigo_seguro,dg_codigo_seguro));
		
		tr.appendTo('#payDetail');
		js_data.montoPayAdmin += dq_monto;
		
	},
	
	getHiddenField: function(name, value){
		return $('<input>').multiAttr({
			'type': 'hidden',
			'name': name+'[]'
		}).val(value);
	},
	
	getEmptyDetailTr: function(required){
		var tr = $('<tr>');
		
		var delButton = $('<img>').multiAttr({
			'src': 'images/delbtn.png',
			'alt': '[X]',
			'title': 'Eliminar Detalle'
		}).click(function(){
			var tr = $(this).parent().parent();
			var monto = parseFloat(tr.find(':input[name=dq_monto\[\]]').val());
			js_data.montoPayAdmin -= monto;
			tr.remove();
		});
		
		tr.append($('<td>').append(delButton));
		if(required[2] == '1'){
			tr.append($('<td>').addClass(js_data.detailClassName.dg_documento));
			tr.append($('<td>').addClass(js_data.detailClassName.df_vencimiento));
		}
		if(required[1] == '1')
			tr.append($('<td>').addClass(js_data.detailClassName.dg_banco));
		tr.append($('<td>').addClass(js_data.detailClassName.dg_banco_origen));
		tr.append($('<td>').addClass(js_data.detailClassName.df_emision));
		tr.append($('<td>').addClass(js_data.detailClassName.dq_monto));
		tr.append($('<td>').addClass(js_data.detailClassName.dg_codigo_seguro));
		
		return tr;
	}
	
});
js_data.cobranzaInit();