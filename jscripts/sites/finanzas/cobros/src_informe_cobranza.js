js_data = {
	fromReport: true,
	
	toDelete: [],
	toEdit: [],
	
	detailPrefix: '#factura_',
	priceContainer: '.mountContainer',
	nextDateContainer: '.nextDateContainer',
	
	clientPrefix: '#client_',
	
	detailed: $('#detailed_report'),
	complete: $('#complete_report'),
	
	init: function(){
		$("#tipo_informes").tabs("div.tabpanes > div",{effect:'default'});
		$('#complete_report a').click(function(e){
			e.preventDefault();
			show_loader();
			loadFile($(this).attr('href'),'#detailed_report','dm_cobranza='+js_data.dm_cobranza,function(){
				$('#to_detail').click();
				hide_loader();
			});
		});
	},
	
	initDetailed: function(){
		$('.onoverlay').click(function(e){
			e.preventDefault();
			loadOverlay(this.href);
		});
		
		js_data.refreshDetails();
		js_data.initDetailedCheckbox();
		js_data.initMassiveActions();
	},
	
	initMassiveActions: function(){
		$('#gestionMasivo').click(js_data.initMassiveManagement);
		$('#pagoMasivo').click(js_data.initMassivePay);
		$('#habilitarCobro').click(js_data.initMassiveHabilitarCobro);
	},
	
	initMassiveManagement: function(e){
		e.preventDefault();
		//update on system core change
		var data = $('.multipleManCheckbox',js_data.detailed).serialize();
		//loadOverlay = function(l,p){
		pymerp.loadOverlay('sites/finanzas/cobros/cr_gestion_cobranza_masiva.php?mode=G',data);
	},
	
	initMassivePay: function(e){
		e.preventDefault();
		//update on system core change
		var data = $('.multipleManCheckbox',js_data.detailed).serialize();
		//loadOverlay = function(l,p){
		pymerp.loadOverlay('sites/finanzas/cobros/cr_gestion_cobranza_masiva.php?mode=P',data);
	},
	
	initMassiveHabilitarCobro: function(e){
		e.preventDefault();
		//update on system core change
		var data = $('.multipleManCheckbox',js_data.detailed).serialize();
		//loadOverlay = function(l,p){
		pymerp.loadOverlay($(this).attr('href'),data);
	},
	
	deleteItemDetail: function(id){
		js_data.toDelete.push(id);
		js_data.deleteItemDetail(id);
	},
	
	editItemDetail: function(id,price,date){
		var data = {
			id: id,
			price: price,
			date: date
		};
		js_data.toEdit.push(data);
		js_data.editIDWObkp(data);
	},
	
	deleteIDWObkp: function(id){
		$(js_data.detailPrefix+id,js_data.detailed).remove();
	},
	
	editIDWObkp: function(data){
		var tr = $(js_data.detailPrefix+data.id,js_data.detailed);
		tr.find('td').css({
			backgroundColor: '#ffec6a'
		});
		tr.find(js_data.priceContainer).text(mil_format(data.price));
		tr.find(js_data.nextDateContainer).text(data.date);
	},
	
	refreshDetails: function(){
		for(i in js_data.toDelete){
			js_data.deleteIDWObkp(js_data.toDelete[i]);
		}
		
		for(i in js_data.toEdit){
			js_data.editIDWObkp(js_data.toEdit[i]);
		}
	},
	
	refreshPeriodo: function(dias,cliente,price){
		dias = parseInt(dias);
		var tr = $(js_data.clientPrefix+cliente,js_data.complete);
		if(dias > 0){
			if(dias <= 30){
				var td = $('.vencer30',tr);
			}else if(dias <= 60){
				var td = $('.vencer60',tr);
			}else if(dias <= 90){
				var td = $('.vencer90',tr);
			}else{
				var td = $('.vencer90more',tr);
			}
		}else{
			var dias = Math.abs(dias);
			if(dias > js_data.porVencer){
				var td = $('.farVencer',tr);
			}else{
				var td = $('.nearVencer',tr);
			}
		}
		
		var actual = js_data.toNumber(td.text());
		td.text(js_data.toMil(actual-price));
		
	},
	
	refreshTotalPeriodo: function(dias, price){
		dias = parseInt(dias);
		
		if(dias > 0){
			if(dias <= 30){
				var td = $('#vencer30',js_data.complete);
			}else if(dias <= 60){
				var td = $('#vencer60',js_data.complete);
			}else if(dias <= 90){
				var td = $('#vencer90',js_data.complete);
			}else{
				var td = $('#vencer90more',js_data.complete);
			}
		}else{
			var dias = Math.abs(dias);
			if(dias > js_data.porVencer){
				var td = $('#farVencer',js_data.complete);
			}else{
				var td = $('#nearVencer',js_data.complete);
			}
		}
		
		var actual = js_data.toNumber(td.text());
		td.text(js_data.toMil(actual-price));
		
	},
	
	refreshTotalCliente: function(){
		var tr = $(js_data.clientPrefix+cliente,js_data.complete);
		var td = $('.totalCliente',tr);
		
		var actual = js_data.toNumber(td.text());
		td.text(js_data.toMil(actual-price));
	},
	
	refreshTotal: function(){
		var td = $('#totalVencido',js_data.complete);
		
		var actual = js_data.toNumber(td.text());
		td.text(js_data.toMil(actual-price));
	},
	
	refreshTotalNoPagado: function(){
		var td = $('#subTotalVencido',js_data.detailed);
		
		var actual = js_data.toNumber(td.text());
		td.text(js_data.toMil(actual-price));
	},
	
	toNumber: function(num){
		var regexp = new RegExp('\\'+js_data.separadorMiles,'g');
		return parseFloat(num.replace(regexp,'').replace(js_data.separadorDecimal,'.'));
	},
	
	toMil: function(num){
		num = new Number(num);
		var stNum = num.toString();
		
		if(stNum.indexOf('.') != -1){
			var arr = stNum.split('.');
			var parteEntera = arr[0];
			var parteDecimal = js_data.separadorDecimal+arr[1];
		}else{
			var parteEntera  = stNum;
			var parteDecimal = '';
		}
		
		var esNegativo = parteEntera.charAt(0)=='-';
		
		if(esNegativo){
			parteEntera = parteEntera.substring(1);
		}
		
		var result = '';
		while( parteEntera.length > 3 )
		{
			result = js_data.separadorMiles + parteEntera.substr(parteEntera.length - 3) + result;
			parteEntera = parteEntera.substring(0, parteEntera.length - 3);
		}
		result = parteEntera + result + parteDecimal;
		
		if(esNegativo){
			result = '-'+result;
		}
		
		return result;
	},
	
	initDetailedCheckbox: function(){
		$('.multipleManCheckbox',js_data.detailed).click(function(){
			var checked = $(this).attr('checked');
			var num_checked = $('.multipleManCheckbox:checked',js_data.detailed).size();
			
			if(checked){
				if(num_checked == 1){
					var clase = $(this).attr('class');
					$('.multipleManCheckbox',js_data.detailed).each(function(){
						var actual = $(this);
						if(actual.attr('class') != clase){
							actual.attr('disabled',true);
						}
					});
					$('#selectAllDetailed',js_data.detailed).attr('disabled',false);
				}
				$(this).parent().parent().find('td').css({
					'backgroundColor' : '#DFFCDC'
				});
			}else{
				if(num_checked == 0){
					$('.multipleManCheckbox',js_data.detailed).attr('disabled',false);
					$('#selectAllDetailed',js_data.detailed).attr('disabled',true).removeAttr('checked');
				}
				$(this).parent().parent().find('td').removeAttr('style');
			}
		});
		
		$('#selectAllDetailed',js_data.detailed).click(function(){
			var checked = $(this).attr('checked');
			if(checked){
				$('.multipleManCheckbox:enabled:not(:checked)',js_data.detailed).attr('checked','checked').parent().parent().find('td').css({
					'backgroundColor' : '#DFFCDC'
				});
			}else{
				$('.multipleManCheckbox:checked',js_data.detailed).removeAttr('checked');
				$('.multipleManCheckbox:disabled',js_data.detailed).attr('disabled',false);
				$('td',js_data.detailed).removeAttr('style');
				$(this).attr('disabled',true);
			}
		});
	}
}
js_data.init();