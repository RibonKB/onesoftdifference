var js_data = {
	editInit: function(){
		$("#ruta_fecha").dateinput({
			lang:'es',
			firstDay:1,
			format:'dd/mm/yyyy',
			selectors:true,
			initialValue:0,
			yearRange:[-80,80]
		});
		
		$('.del_item').click(js_data.onDeleteEditItem);
	},
	
	onDeleteEditItem: function(){
		var checked = $(this).attr('checked');
		var tr = $(this).parent().parent();
		
		if(checked){
			tr.find('.dt_hora,.dq_cantidad').attr('disabled',true);
			tr.find('.dc_detalle,.dq_cantidad_old,.dc_detalle_nota_venta').removeAttr('name');
		}else{
			tr.find('.dt_hora,.dq_cantidad').attr('disabled',false);
			tr.find('.dc_detalle').attr('name','dc_detalle[]');
			tr.find('.dq_cantidad_old').attr('name','dq_cantidad_old[]');
			tr.find('.dc_detalle_nota_venta').attr('name','dc_detalle_nota_venta[]');
		}
	}
};