var js_data = {
	
	ACNotaVentaUrl: 'sites/proc/autocompleter/nota_venta_strict.php',
	ACNotaVentaOptions: {
		formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
		minChars: 2,
		width:300
	},
	
	init: function(){
		$('#dq_nota_venta').autocomplete(js_data.ACNotaVentaUrl,js_data.ACNotaVentaOptions).result(js_data.NVChange);
	},
	
	NVChange: function(e, row){
		var dc_nota_venta = row[3];
		
		pymerp.loadFile('sites/logistica/proc/get_nota_venta_reserva.php','#nv_data','id='+dc_nota_venta);
		$(':hidden[name=dc_nota_venta]').val(dc_nota_venta);
		
	},
};
js_data.init();