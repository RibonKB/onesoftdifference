var js_data = {

    form: $('#cr_asignacion_stock',pymerp.contentContainer),

    fields: {
      dq_orden_compra: null,
      bkp_orden_compra: null
    },

    ACOrdenCompraOptions: {
		formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
		minChars: 2,
		width:300
	},

    init: function(){
      js_data.initFields();
      js_data.initAsignacionOC();
    },

    initFields: function(){
      js_data.fields.dq_orden_compra = $('#dq_orden_compra',js_data.form);
    },

    initAsignacionOC: function(){
      js_data.fields.dq_orden_compra
              .autocomplete(js_data.OCAutocompleteUrl, js_data.ACOrdenCompraOptions)
              .result(js_data.getOCDetailDataResult);
    },

    getOCDetailDataResult: function(e, row){
        var dc_orden_compra = row[3];
        var container = $('#orden_compra_datatable');
        var div = pymerp.getEditableBox(row[0],js_data.restaurarOcField).width(js_data.fields.dq_orden_compra.width());

        div.append($('<input>').multiAttr({
          'type': 'hidden',
          'name': 'dc_orden_compra'
        }).val(dc_orden_compra));

        js_data.fields.bkp_orden_compra = js_data.fields.dq_orden_compra.clone(true).val('');
        div.replaceAll(js_data.fields.dq_orden_compra);
        pymerp.loadFile(js_data.OCDetailDataResultUrl,container,'dc_orden_compra='+dc_orden_compra,function(){
          pymerp.init(container);
          container.removeClass('hidden');
        });
    },

    initAsignacionStock: function(){
        var container = $('#genOverlay');
        js_data.initAsignacionFields(container);
        js_data.initAsignacionSubmit(container);
    },

    restaurarOcField: function(){
        var div = $(this).parent('div');
        js_data.fields.bkp_orden_compra
               .replaceAll(div)
               .autocomplete(js_data.OCAutocompleteUrl, js_data.ACOrdenCompraOptions)
               .result(js_data.getOCDetailDataResult);

    },

    initAsignacionFields: function(container){
        $('.dq_stock_value',container).change(function(){
            var tr = $(this).parent().parent();
            var val = parseInt($(this).val());
            var max = $('.dc_stock_maximo',tr).val();
            if(!val || val > max){
              val = max;
              $(this).val(max);
            }
        });
    },

    initAsignacionSubmit: function(container){
        $('#btn_asignar_stock_libre',container).click(function(){
          var dc_detalle = $('#dc_detalle_oc',container).val();
          var td = $('#dc_detalle_'+dc_detalle,js_data.form);

          $('.dc_cantidad_bodega',td).remove();

          $('.dq_stock_value',container).each(function(){
            var tr = $(this).parent().parent();
            var hidden = $('<input>').multiAttr({
              'type': 'hidden',
              'class': 'dc_cantidad_bodega'
            });

            var dc_bodega = $('.dc_bodega',tr).val();
            var dq_cantidad = $(this).val();

            if(dq_cantidad == '' || dq_cantidad == 0){
              return;
            }


            td.append(hidden.clone().attr('name','dc_bodega[]').val(dc_bodega));
            td.append(hidden.clone().attr('name','dc_cantidad[]').val(dq_cantidad));
            td.append(hidden.clone().attr('name','dc_detalle[]').val(dc_detalle));
          });

          container.remove();
        });
    }

};
