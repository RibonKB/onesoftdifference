var js_data = {
	
	ACFacturaUrl: 'sites/proc/autocompleter/factura_compra_strict.php',
	ACFacturaOptions: {
		formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
		minChars: 2,
		width:300
	},
	
	init: function(){
		js_data.ACFacturaOptions.extraParams = { dc_proveedor: js_data.dc_proveedor, sin_pago: true };
		$('#dq_factura').autocomplete(js_data.ACFacturaUrl, js_data.ACFacturaOptions).result(js_data.ACFacturaResult);
	},
	
	ACFacturaResult: function(e, row){
		
		var dc_factura = row[3];
		var dq_factura = row[0];
		js_data.FacturaCompraField = $(this).clone().val('');
		var field = $(this);
		
		$.getJSON('sites/logistica/guia_despacho_proveedor/proc/get_factura_compra_detalle.php',{dc_factura: dc_factura},function(detalle){
			
			var tbody = $('#prod_list').html('');
			
			for(i in detalle){
				var d = js_data.getDetalle(detalle[i]);
				tbody.append(d);
				
				pymerp.getEditableBox(dq_factura, js_data.editFacturaCompra).width(field.width()).replaceAll(field);
				
			}
			
			$(':hidden[name=dc_factura]').val(dc_factura);
			
			actualizar_detalles();
			actualizar_totales();
			
		});
		
	},
	
	editFacturaCompra: function(){
		
		var div = $(this).parent();
		
		js_data.FacturaCompraField
		.autocomplete(js_data.ACFacturaUrl, js_data.ACFacturaOptions)
		.result(js_data.ACFacturaResult)
		.replaceAll(div).focus();
		
		$(':hidden[name=dc_factura]').val(0);
		
	},
	
	getDetalle: function(d){
		var row = $('#prods_form tr').clone(true);
		
		row.find('.prod_desc').val(d.dg_producto);
		row.find('.prod_cant').val(d.dc_cantidad);
		row.find('.prod_price').val(parseFloat(d.dq_precio).toFixed(pymerp.localDecimales)); 
		row.find('.prod_costo').val(parseFloat(d.dq_precio).toFixed(pymerp.localDecimales));
		row.find('.prod_codigo').removeClass('searchbtn').addClass('inputtext').attr('readonly',true).val(d.dg_codigo);
		if(d.dm_requiere_serie == '0')
			row.find('.prod_series').attr('required',true);
		//create_code_div(d.dg_codigo,d.dc_producto,d.dg_descripcion).replaceAll(row.find('.prod_codigo'));
		return row;
		
	}
	
};