var js_data = {
	
	leftClass: {
		showDetailBtn: '.togglebtn',
		detailContainer: '.nv_detail',
		nvContainer: '.nv_header',
		activeClass: 'nv_active',
		selActiveClass: '.nv_active',
		idDetail: '.detail_id',
		cantDetail: '.detail_cant',
		selectableDetail: 'selectable',
		nvId: '.nv_id',
		detailIdNamePrefix: '#detail'
	},
	
	centerClass: {
		calItem: '.calendar_hour_item',
		calDropable: 'cal_dropable',
		nvContainer: 'nv_on_calendar',
		hiddenTime: '.calendar_time_value',
		detailIdName: 'dc_detalle[]',
		detailCantName: 'dq_cantidad[]',
		timeInputName: 'df_hora[]'
	},
	
	rightClass: {
		containerClass: 'dataContainer',
		titleSpan: 'titleSpan',
		titleNotaVenta: 'titleNotaVenta',
		titleCliente: 'titleCliente',
		cantClass: '.detailCant'
	},
	
	jForm: $('#src_nota_venta_res'),
	
	nvList: $('#ruta_nota_venta',this.jForm),
	calendar: $('#ruta_calendar',this.jForm),
	detailed: $('#detailDataContainer',this.jForm),
	
	activeNv: null,
	activeNvDetail: null,
	
	calendarInitialScroll: 375,
	
	init: function(){
		js_data.initShowDetails();
		js_data.initSelectNV();
		js_data.initSelectNVDetail();
		js_data.initCalendar();
		js_data.setCalendarScroll();
	},
	
	setCalendarScroll: function(){
		window.setTimeout(function(){
			js_data.calendar.scrollTop(js_data.calendarInitialScroll);
		},100);
	},
	
	initShowDetails: function(){
		$(js_data.leftClass.showDetailBtn,js_data.nvList).toggle(function(){
			$(this).parent().next(js_data.leftClass.detailContainer).show();
		},function(){
			$(this).parent().next(js_data.leftClass.detailContainer).hide();
		});
	},
	
	initSelectNV: function(){
		$(js_data.leftClass.nvContainer,js_data.nvList).dblclick(function(){
			if($(this).hasClass(js_data.leftClass.activeClass)){
				$(this).removeClass(js_data.leftClass.activeClass);
				js_data.activeNv = null;
				
				$(js_data.centerClass.calItem,js_data.calendar).removeClass(js_data.centerClass.calDropable);
			}else{
				$(js_data.leftClass.selActiveClass,js_data.nvList).removeClass(js_data.leftClass.activeClass);
				$(this).addClass(js_data.leftClass.activeClass);
				js_data.activeNv = $(this);
				
				$(js_data.centerClass.calItem,js_data.calendar).addClass(js_data.centerClass.calDropable);
			}
		});
	},
	
	initSelectNVDetail: function(){
		$('.'+js_data.leftClass.selectableDetail,js_data.nvList).dblclick(function(){
			if($(this).hasClass(js_data.leftClass.activeClass)){
				$(this).removeClass(js_data.leftClass.activeClass);
				js_data.activeNvDetail = null;
				$(js_data.centerClass.calItem,js_data.calendar).removeClass(js_data.centerClass.calDropable);
			}else if($(this).parent(js_data.leftClass.detailContainer).find('.'+js_data.leftClass.selectableDetail).size() == 1){
				$(this).parent(js_data.leftClass.detailContainer).prev(js_data.leftClass.nvContainer).trigger('dblclick');
			}else{
				$(js_data.leftClass.selActiveClass,js_data.nvList).addClass(js_data.leftClass.activeClass);
				$(this).addClass(js_data.leftClass.activeClass);
				js_data.activeNvDetail = $(this);
				$(js_data.centerClass.calItem,js_data.calendar).addClass(js_data.centerClass.calDropable);
			}
		});
	},
	
	initCalendar: function(){
		$(js_data.centerClass.calItem,js_data.calendar).click(js_data.addElementToCalendar);
	},
	
	addElementToCalendar: function(){
		
		var timeInput = $(this).children(js_data.centerClass.hiddenTime).clone().attr('name',js_data.centerClass.timeInputName);
		
		if(js_data.activeNv != null){
			var nvId = js_data.activeNv.children(js_data.leftClass.nvId).val();
			
			var nvContainer = $('.nv'+nvId,$(this));
			if(nvContainer.size() == 0){
				var nvContainer = js_data.getEmptyNVToCalendar(js_data.activeNv.children('b').text(),js_data.activeNv.children('small').text(),nvId);
				$(this).append(nvContainer);
			}
			js_data.activeNv.removeClass(js_data.leftClass.activeClass).parent().addClass('hidden');
			
			js_data.activeNv.next(js_data.leftClass.detailContainer)
			.find('.'+js_data.leftClass.selectableDetail)
			.removeClass(js_data.leftClass.selectableDetail)
			.each(function(){
				nvContainer.append(
					js_data.getDetailToInsert($(this),timeInput.val())
				).append(timeInput.clone());
			});
			
			js_data.activeNv.next('ul').find('li').addClass('hidden').removeClass(js_data.leftClass.selectableDetail);
			js_data.activeNv = null;
			
		}else if(js_data.activeNvDetail != null){
			
			js_data.activeNvDetail.removeClass(js_data.leftClass.selectableDetail).addClass('hidden');
			
			var nvToAdd = js_data.activeNvDetail.parent().prev(js_data.leftClass.nvContainer);
			var nvId = nvToAdd.children(js_data.leftClass.nvId).val();
			var nvContainer = $('.nv'+nvId,$(this));
			if(nvContainer.size() == 0){
				var nvContainer = js_data.getEmptyNVToCalendar(nvToAdd.children('b').text(),nvToAdd.children('small').text(),nvId);
				$(this).append(nvContainer);
			}
			nvContainer
				.append(js_data.getDetailToInsert(js_data.activeNvDetail),timeInput.val())
				.append(timeInput.clone());
			
		}
		
		$(js_data.centerClass.calItem,js_data.calendar).removeClass(js_data.centerClass.calDropable);
	},
	
	getEmptyNVToCalendar: function(dq_nota_venta, dg_razon, dc_nota_venta){
		var div = $('<div>').addClass(js_data.centerClass.nvContainer).addClass('nv'+dc_nota_venta);
		var nv = $('<b>').text(dq_nota_venta);
		var cliente = $('<small>').text(dg_razon);
		return div.dblclick(js_data.editDetail).append(nv).append(' - ').append(cliente);
	},
	
	getDetailToInsert: function(detail,time){
		var dc_detalle = $(detail).children(js_data.leftClass.idDetail).clone();
		dc_detalle.attr('name',js_data.centerClass.detailIdName);
		
		time = time.replace(':','');
		
		var dq_cantidad = $(detail).children(js_data.leftClass.cantDetail).clone();
		dq_cantidad.attr('name',js_data.centerClass.detailCantName)
		.attr('id',dc_detalle.val()+'_'+time);
		
		return dc_detalle.add(dq_cantidad);
	},
	
	deleteItemDetail: function(timeid){
		var itemField = $(timeid,js_data.calendar);
		
		if(typeof timeid == 'string'){
			var realId = parseInt(timeid.substr(0,timeid.length-5).substr(1));
		}else{
			var fid = itemField.attr('id');
			var realId = fid.substr(0,fid.length-5);
		}
		var value = itemField.val();
		
		var parent = itemField.parent();
		
		itemField.prev().remove()
		itemField.next().remove()
		itemField.remove();
		
		if(parent.find('input:hidden').size() == 0){
			parent.remove();
			$('.'+js_data.rightClass.containerClass,js_data.detailed).remove();
		}
		
		$(js_data.leftClass.detailIdNamePrefix.concat(realId),js_data.nvList)
		.parent('li').removeClass('hidden').addClass('selectable')
		.parent().parent().removeClass('hidden');
		
	},
	
	editDetail: function(){
		var detail = $(this);
		
		var products = $(js_data.leftClass.idDetail,detail);
		js_data.detailed.children('.'+js_data.rightClass.containerClass).remove();
		
		var div = $('<div>').addClass(js_data.rightClass.containerClass);
		
		var dq_nota_venta = detail.children('b').text();
		var dg_razon = detail.children('small').text();
		
		div.append($('<span>').addClass(js_data.rightClass.titleSpan).text('NOTA DE VENTA'))
		.append($('<h3>').addClass(js_data.rightClass.titleNotaVenta).text(dq_nota_venta))
		.append($('<span>').addClass(js_data.rightClass.titleSpan).text('CLIENTE'))
		.append($('<h3>').addClass(js_data.rightClass.titleCliente).text(dg_razon))
		.append($('<span>').addClass(js_data.rightClass.titleSpan).text('PRODUCTOS'));
		
		var table = $('<table>').attr('width','100%').addClass('tab');
		 table.append($('<thead>').append($('<tr>')
			.append($('<th>').text('[X]').width(20))
		 	.append($('<th>').text('CANTIDAD').width(90))
			.append($('<th>').text('NOMBRE'))
		));
		
		var tbody = $('<tbody>');
		
		products.each(function(){
			var hid = $(this);
			var idDt = hid.val();
			
			deleteCol = $('<img>').multiAttr({
				src: 'images/delbtn.png',
				alt: '[X]',
				title: 'Eliminar detalle',
				rel: '#'.concat(idDt).concat('_').concat(hid.next().next().val().replace(':',''))
			}).click(js_data.deleteRightDetail);
			
			var pname = $(js_data.leftClass.detailIdNamePrefix.concat(idDt),js_data.nvList).clone().removeAttr('id');
			
			var cantField = $('<input>').multiAttr({
				'type':'text',
				'id':'chdetail'.concat(idDt).concat('_').concat(hid.next().next().val().replace(':','')),
				'class':'detailCant inputtext',
				'size': '7'
			}).val(hid.next(js_data.leftClass.cantDetail).val())
			.css({
				textAlign: 'right'
			});
			
			$('<tr>').append(deleteCol).append(cantField).append(pname).appendTo(tbody);
			
			deleteCol.wrap('<td>');
			pname.wrap('<td>');
			cantField.wrap('<td>');
			cantField.after($('<input>').attr('type','hidden').val(hid.next(js_data.leftClass.cantDetail).val()).addClass('maxValue'));
		});
		
		table.append(tbody);
		div.append(table);
		
		var confirmButton = $('<button type="button">').text('Reasignar').addClass('editbtn').click(js_data.refreshCants);
		
		div.append('<br>').append(confirmButton);
		
		confirmButton.wrap('<div class="center">');
		js_data.detailed.append(div);
		
	},
	
	deleteRightDetail: function(){
		var id = $(this).attr('rel');
		js_data.deleteItemDetail(id);
		$(this).parent().parent().remove();
	},
	
	refreshCants: function(){
		var cants = $(js_data.rightClass.cantClass,js_data.detailed);
		cants.each(function(){
			var value = parseInt($(this).val());
			var id = '#'+$(this).attr('id').substr(8);
			if(value > 0)
				$(id,js_data.calendar).val(value);
			else
				js_data.deleteItemDetail(id);
		});
		js_data.detailed.children('.'+js_data.rightClass.containerClass).html('').append(
			$('<div>').text('Cantidades actualizadas').addClass('confirm')
		);
	}
	
};
js_data.init();
pymerp.init(js_data.jForm);