var js_data = {
  
    form: $('#cr_comprobante_contable'),
    emptyDetail: $('#detalle_template tr'),
    bodyDetail: null,
    urlContabilidadAnalitica: null,
    activeAnalitico: null,
    itemId: $('#id_it').val(),
    AM_ACData: {},
  
    init: function(){
      js_data.bodyDetail = $('#detalle_list',js_data.form);
      $('#add_detalle',js_data.form).click(js_data.delEmptyDetail);
      var templateTable = $('#detalle_template');
      
      var templateTableOld = $('#detalle_list');
      $('.del_detail',templateTableOld).click(js_data.delDetail);
      $('.show_more',templateTableOld).click(js_data.setContabilidadAnalitica);
      $('.duplicate',templateTableOld).click(js_data.duplicateDetail);
      $('.dq_debe',templateTableOld).change(js_data.changeDebe);
      $('.dq_haber',templateTableOld).change(js_data.changeHaber);
      $('.dq_debe',templateTableOld).keypress(js_data.jumpNext);
      $('.dq_haber',templateTableOld).keypress(js_data.jumpNext);
      
      $('.del_detail',templateTable).click(js_data.delDetail);
      $('.show_more',templateTable).click(js_data.setContabilidadAnalitica);
      $('.duplicate',templateTable).click(js_data.duplicateDetail);
      $('.dq_debe',templateTable).change(js_data.changeDebe);
      $('.dq_haber',templateTable).change(js_data.changeHaber);
      $('.dq_debe',templateTable).keypress(js_data.jumpNext);
      $('.dq_haber',templateTable).keypress(js_data.jumpNext);
      
    },
    
    addDetalle: function(){
      var empty = js_data.emptyDetail.clone(true);
      $('.item_id',empty).val(js_data.itemId++);
      js_data.initCuentaAutocomplete($('.dg_codigo',empty));
      js_data.bodyDetail.append(empty);
      $('.dg_codigo',empty).focus();
    },
            
    delEmptyDetail: function(){
      $('#empty_detalle',js_data.form).remove();
      $(this).unbind('click').click(js_data.addDetalle);
      $(this).click();
    },
            
    delDetail: function(){
		//Verifica si existe un ID, si es así, lo almacenará en la sección de ids eliminados.
		var tr = $(this).parent().parent();
		var idDetalle = tr.find('.id_detalle_comprobante').val();
		if(idDetalle){
			
		}
		
		$(this).parent().parent().remove();
		js_data.refreshTotales();
    },
            
    setContabilidadAnalitica: function(){
      js_data.activeAnalitico = $(this).parent();
      var id = $('.item_id',js_data.activeAnalitico).val();
      var regex = new RegExp('\\%5B'+id+'\\%5D','g');
      var data = $(':input.detalle_analitico_hidden',js_data.activeAnalitico).serialize().replace(regex,'');
      pymerp.loadOverlay(js_data.urlContabilidadAnalitica,data);
    },
            
    duplicateDetail: function(){
		var tr = $(this).parent().parent();
		var clone = tr.clone(true);
		clone.find('.dq_debe').val(tr.find('.dq_debe').val());
		clone.find('.dq_haber').val(tr.find('.dq_haber').val());
		clone.find('.dg_glosa').val(tr.find('.dg_glosa').val());
		//Esto se utiliza para normalizar en el caso que se modifique un comprobante
		var newId = js_data.itemId++;
		//Encontrar el campo item_id y cambiar su valor
		var oldId = clone.find('.item_id').val();
		clone.find('.item_id').val(newId);
		//Cambiar el valor de un detalle analítico
		clone.find('.detalle_analitico_hidden').each(function(){
			var name = $(this).attr('name');
			$(this).attr('name', name.replace('[' + oldId + ']' , '[' + newId + ']'));
		});
		clone.find('.id_detalle_comprobante').remove();
		clone.find('.an_dc_detalle').val('0');
		js_data.bodyDetail.append(clone);
		js_data.refreshTotales();
    },
    
    initCuentaAutocomplete: function(field){
      field.autocomplete(js_data.cuentas,{
        formatItem: function(row){
			return row.dg_codigo+" "+row.dg_cuenta_contable+" - "+row.dg_descripcion_larga;
		},
		formatMatch: function(row, i, max) {
			return row.dg_codigo+" "+row.dg_cuenta_contable+" "+row.dg_descripcion_larga;
		},
		minChars: 1,
		width:300,
		matchContains: true,
		formatResult: function(row) {
			return row.dg_codigo;
		}
      }).result(js_data.ACCuentaResult);
    },
    
    ACCuentaResult: function(e, row){
      var div = pymerp.getEditableBox(row.dg_codigo,js_data.resetCuentaContable);
      div.append($('<input>').multiAttr({
        'type': 'hidden',
        'name': 'dc_cuenta_contable[]',
        'value': row.dc_cuenta_contable
      }));
      $(this).parent().next().text(row.dg_cuenta_contable);
      div.replaceAll(this);
    },
            
    resetCuentaContable: function(){
      var div = $(this).parent();
      var field = $('.dg_codigo',js_data.emptyDetail).clone(true);
      div.parent().next().text('');
      js_data.initCuentaAutocomplete(field);
      field.replaceAll(div).focus();
    },
    
    changeDebe: function(){
      var value = pymerp.toNumber($(this).val());
      if(!value){
        $(this).val('');
        $(this).parent().next().children(".dq_haber").attr("readonly",false).attr("required",true).val('');
      }else{
        $(this).val(pymerp.milFormat(value.toFixed(pymerp.localDecimales)));
        $(this).parent().next().children(".dq_haber").attr("readonly",true).attr("required",false).val('0');
      }
      js_data.refreshTotales();
    },
            
    changeHaber: function(){
      var value = pymerp.toNumber($(this).val());
      if(!value){
        $(this).val('');
        $(this).parent().prev().children(".dq_debe").attr("readonly",false).attr("required",true).val('');
      }else{
        $(this).val(pymerp.milFormat(value.toFixed(pymerp.localDecimales)));
        $(this).parent().prev().children(".dq_debe").attr("readonly",true).attr("required",false).val('0');
      }
      js_data.refreshTotales();
    },
            
    refreshTotales: function(){
      var dq_debe = 0;
      var dq_haber = 0;
      $('.dq_debe[value!=""]',js_data.bodyDetail).each(function(){
        dq_debe += pymerp.toNumber($(this).val())
      });
      $('.dq_haber[value!=""]',js_data.bodyDetail).each(function(){
        dq_haber += pymerp.toNumber($(this).val())
      });
      $('#total_debe',js_data.form).text(pymerp.milFormat(dq_debe));
      $('#total_haber',js_data.form).text(pymerp.milFormat(dq_haber));
      $('#diferencia_debe_haber',js_data.form).text(pymerp.milFormat(Math.abs(dq_debe-dq_haber)));
    },
    
    AM_submitForm: function(e){
		e.preventDefault();
		var td = js_data.activeAnalitico;
		var tr = td.parent();
		var id = $('.item_id',td).val();
		var dq_debe = 0;
		var dq_haber = 0; $('.an_dq_haber',this);

		$('.detalle_analitico_hidden',td).remove();

		$('.an_dq_debe',this).each(function(){
		dq_debe += pymerp.toNumber($(this).val());
		});

		$('.an_dq_haber',this).each(function(){
		dq_haber += pymerp.toNumber($(this).val());
		});
		
		$('.dq_debe',tr).val(pymerp.milFormat(dq_debe)).attr('readonly',true);
		$('.dq_haber',tr).val(pymerp.milFormat(dq_haber)).attr('readonly',true);
		

		$(':input',this).each(function(){
			var name = $(this).attr('name');
			name = name.substr(0,name.length-2);
			if(!name){
				if(dq_debe == 0 && dq_haber == 0){
					$('.dq_debe',tr).val(pymerp.milFormat(dq_debe)).attr('readonly',false);
					$('.dq_haber',tr).val(pymerp.milFormat(dq_haber)).attr('readonly',false);
				}
				return;
			}
			var hid = $('<input>')
				.attr('type','hidden')
				.attr('class','detalle_analitico_hidden')
				.attr('name','detalle_analitico['+id+']['+name+'][]')
				.val($(this).val());
			td.append(hid);
		});
		js_data.refreshTotales();
		$(this).parents('.overlay').remove();
    },
    
    jumpNext: function(event){
        if(event.which == 13){
            event.preventDefault();
            if($(this).val() == ''){
                if($(this).hasClass('dq_debe')){
                    $(this).parent().next('td').find('.dq_haber').focus();
                } else {
                    $(this).parent().prev('td').find('.dq_debe').focus();
                }
                return;
            }
            var nextRow = $(this).parent().parent().next('tr');
            if(nextRow){
                $(this).change();
                if(!nextRow.find('.dq_debe').attr('readonly')){
                    nextRow.find('.dq_debe').focus();
                    nextRow.find('.dq_debe').select();
                }else{
                    nextRow.find('.dq_haber').focus();
                    nextRow.find('.dq_haber').select();
                }
                
            }
        }
    }
  
};