var js_data = {

	activeData: null,

	init: function(){

		$("#grid_centralizacion tr:even td")
			.css({"backgroundColor": "#E5E5E5"});
		$("#grid_centralizacion")
			.tableExport()
			.tableAdjust(15);

		$('#btn_centralizar').click(js_data.centralizar);

		$('#select_all_chbx').click(js_data.selectAll);

	},

	centralizar: function(){
		if(confirm('¿Est\xE1 seguro que desea continuar?')){
			var data = $('#tbody_list :checkbox:checked');
			js_data.activeData = data;
			pymerp.loadOverlay(js_data.urlCentralizacion,data.serialize());
		}
	},

	selectAll: function(){
		var st = $(this).attr('checked');
		if(st){
			$('#tbody_list :checkbox:not(:checked)').attr('checked',true);
		}else{
			$('#tbody_list :checkbox:checked').removeAttr('checked');
		}
	},

	deleteActive: function(){
		js_data.activeData.each(function(index, element) {
            $(this).closest('tr').remove();
        });
	}

};
