js_data = {

  //Contenedores
  form: $("#src_informe_existencias"),
  divShowPendientes:  $('#show_movimientos_pendientes'),
  divShowInforme:     $('#show_informe_existencias'),

  //Variables de asistencia
  loader: $('<div>').css({'display':'none', 'text-align':'center', 'font-size':'20px'}),
  isCerrado: false,

  //Public:
  init: function(){
    js_data.form.submit(js_data.submitForm);
  },

  disableCierre: function(){
    js_data.isCerrado = true;
  },

  //Private:
  submitForm: function(e){
    e.preventDefault();
    js_data.setMovimientosPendientes();
  },

  setMovimientosPendientes: function(){
    var loader = js_data.loader
    .clone()
    .text("Cargando los movimientos pendientes...")
    .show()
    .appendTo('body')
    .expose({
			loadSpeed	:0,
			closeOnClick:false,
			color		:'#F4F4F4',
			closeSpeed	:0,
			closeOnEsc	:false,
			zIndex		:99999
		});
    pymerp.loadFile(js_data.tipoOperacionFechasURL, js_data.divShowPendientes, '', function(){
      loader.remove();
      $.mask.close();
      js_data.loadInformeExistencias();
    });
  },

  loadInformeExistencias: function(){
    pymerp.enviarForm(js_data.form, js_data.divShowInforme);
  }
}
