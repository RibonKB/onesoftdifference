$("#add_periodo").click(function(){
	$("#periodos").parent().show();
	e = $("#plantilla tr").clone(true).appendTo("#periodos");
	
	e.first().next().find('.per_filtro_grupo').autocomplete('sites/proc/autocompleter/grupo.php',
	{width:300}).result(function(e,row){
		hid = $(this).next().next();
		hid.val(hid.val()+row[1]+",");
		$(this).val('');
		$(this).parent().parent().next().find('.filtro_list')
		.append('<div style="border:1px solid #CCC;float:left;padding:2px;margin:1px;background:#EEE;"><img src="images/delbtn.png" style="width:10px;" alt="'+row[1]+'" class="rev_per_elem" /> '+row[0]+'</div>');
		
		$(".rev_per_elem").click(function(){
		v = $(this).attr('alt');
		hd = $(this).parent().parent().parent().prev().find(".per_filtro_list");
		hd.val(hd.val().replace(v+',',''));
		$(this).parent().remove();
		});
	});
	
	e.first().next().find('.per_filtro_usuario').autocomplete('sites/proc/autocompleter/usuario.php',
	{
		width:300,
		formatItem: function(row){
			return "<b>"+row[0]+"</b> ( "+row[1]+" ) "+row[2];
		}
	}).result(function(e,row){
		hid = $(this).next();
		hid.val(hid.val()+row[3]+",");
		$(this).val('');
		$(this).parent().parent().next().find('.filtro_list')
		.append('<div style="border:1px solid #CCC;float:left;padding:2px;margin:1px;background:#EEE;"><img src="images/delbtn.png" style="width:10px;" alt="'+row[3]+'" class="rev_per_elem" /> '+row[0]+'</div>');
		
		$(".rev_per_elem").click(function(){
		v = $(this).attr('alt');
		hd = $(this).parent().parent().parent().prev().find(".per_filtro_list");
		hd.val(hd.val().replace(v+',',''));
		$(this).parent().remove();
		});
	});
});

$(".per_filtro").change(function(){
	if($(this).val() == '1'){
		$(this).parent().parent().next().show();
		$(this).parent().parent().next().next().show();
	}else{
		$(this).parent().parent().next().hide();
		$(this).parent().parent().next().next().hide();
	}
});

$(".del_periodo").click(function(){
	if(confirm("Est\xE1 seguro que quiere eliminar el periodo?")){
		e = $(this).parent().parent();
		e.next().next().remove();
		e.next().remove();
		e.remove();
		if(!$("#periodos tr").size()){
			$("#periodos").parent().hide();
		}
	}
});

$(".per_filtro_tipo").change(function(){
	if($(this).val() == '1'){
		$(this).next().show();
		$(this).next().next().hide();
	}else{
		$(this).next().hide();
		$(this).next().next().show();
	}
	$(this).parent().parent().next().find(".filtro_list div").remove();
	$(this).next().next().next().val('');
});

$("#periodo_mes").change(function(){
	if(parseInt($(this).val())){
		$(this).val(parseInt($(this).val()));
	}else{
		$(this).val('');
	}
});

$(".per_anho").change(function(){
	v = parseInt($(this).val());
	if(v && v > 1900){
		$(this).val(v);
	}else{
		$(this).val('');
	}
});