$.extend(js_data,{
  
  AM_form: $('#AM_formContabilidadAnalitica'),
  AM_container: $('#AM_detalleAnaliticoContainer',this.AM_form),
  AM_template: $('#AM_DetailTemplate'),

  AM_init: function(){
    $('#AM_addDetalle',js_data.AM_form).click(js_data.AM_showDetailsContainer);
    js_data.AM_form.submit(js_data.AM_submitForm);
    js_data.AM_initTemplateActions();
  },

  AM_showDetailsContainer: function(){
    js_data.AM_ShowDetailContainerOnly(this);
    $(this).click();
  },
          
  AM_ShowDetailContainerOnly: function(btn){
      js_data.AM_container.removeClass('hidden');
      $(btn).unbind('click').click(js_data.AM_AddDetalle);
  },

  AM_AddDetalle: function(){
      js_data.AM_InsertInitDetail(js_data.AM_GetEmptyDetail());
  },
          
  AM_GetEmptyDetail: function(){
      return $('.AM_DetailItem',js_data.AM_template).clone();
  },
          
  AM_InsertInitDetail: function(item){
      js_data.AM_container.prepend(item);
      js_data.AM_initTemplateActions(item);
  },

  AM_initTemplateActions: function(item){
    
    //Eliminar un detalle analítico
    $('.AM_DeleteItem',item).click(function(){
      if(confirm('¿Está seguro qe quiere eliminar el detalle?')){
        $(this).parent().parent().remove();
      }
    });
    
    //Esconder detalle analítico
    $('.AM_HideItem',item).toggle(function(){
      $(this).parent().next().addClass('hidden');
    },function(){
      $(this).parent().next().removeClass('hidden');
    });
    
    //Asignar glosa a cabecera
    $('.an_dg_glosa',item).keyup(function(){
      $('.AM_glosaContainer',$(this).parents('.AM_DetailItem').first()).text($(this).val());
    });
    
    //Inicializar basics
    pymerp.init(item);
    
    //Inicializar Autocompletar
    js_data.AM_initAutocompleter(item);
  },
          
  AM_initAutocompleter: function(item){
    var data = js_data.AM_AutocompleteParams;
    for(i in data){
      js_data.AM_setFieldAutocompleter($('.'+i,item),data[i]);
    }
  },
          
  AM_setFieldAutocompleter: function(field, url, item){
    $(field).autocomplete(url,{
      formatItem: function(row){
          return row[0]+" ( "+row[1]+" ) "+row[2];
      },
      minChars: 2,
      width: 400
    }).result(js_data.AM_setFieldACResult);
    $(field).removeAttr('name');
  },
  
  AM_setFieldACResult: function(e, row){
    var div = pymerp.getEditableBox(row[0], js_data.AM_editACField).width($(this).width());
    div.attr('title',row[0]+" ( "+row[1]+" ) "+row[2]);
    var item = $(this).parents('.AM_DetailItem').first();
    $('.an_'+row[4],item).val(row[3]);
    //div.append($('<input>').attr('name','an_'+row[4]+'[]').attr('type','hidden').val(row[3]));
    $(this).after(div).addClass('hidden').val('');
    
    if(!js_data.AM_ACData[row[4]]){
        js_data.AM_ACData[row[4]] = {};
    }
    
    js_data.AM_ACData[row[4]][row[3]] = row;
  },
          
  AM_editACField: function(){
    var div = $(this).parent();
    div.prev().removeClass('hidden');
    div.remove();
  },
  
  AM_setSelectedValues: function(){
    
      var data = js_data.AM_selectedValues;
      if(data == null){
          return;
      }
      
      pymerp.showLoader();
      
      js_data.AM_ShowDetailContainerOnly.call($('#AM_addDetalle',js_data.AM_form));
      
      for(i in data){
          
          var data2 = data[i];
          var item = js_data.AM_GetEmptyDetail();
          js_data.AM_InsertInitDetail(item);
          
          for(j in data2){
            
              if(js_data.AM_nameMapping[j]){
                  var field = item.find(js_data.AM_nameMapping[j][1]);
                  var row = js_data.AM_ACData[js_data.AM_nameMapping[j][0]][data2[j]];
                  js_data.AM_setFieldACResult.call(field,undefined,row);
              }else{
                  item.find('.'+j).val(data2[j]);
              }
          }
          
          var glosa = item.find('.an_dg_glosa')
          if(glosa.val()){
              glosa.trigger('keyup');
          }
          
          item.find('.AM_HideItem').click();
      }
      
      pymerp.hideLoader();
      
  }
  
});