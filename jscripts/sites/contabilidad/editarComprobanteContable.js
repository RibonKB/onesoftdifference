var editarComprobanteContable = {
	init: function(){
		js_data.init();
		js_data.refreshTotales();
		editarComprobanteContable.initFieldCuentasContables();
		
	},
	
	initFieldCuentasContables: function(){
		$('.dg_codigo',js_data.bodyDetail).each(function(){
			var ctacbl=$(this).next().val();
			$(this).next().remove();
			js_data.ACCuentaResult.call($(this),undefined,{ 
				dg_codigo: $(this).val(), 
				dc_cuenta_contable: ctacbl, 
				dg_cuenta_contable: $(this).parent().next().text()
			});
		});
		$('#detalle_list').find('.dq_debe').each(function(){
			$(this).attr('readonly','readonly');
		});
		$('#detalle_list').find('.dq_haber').each(function(){
			$(this).attr('readonly','readonly');
		});
	}
};
