var js_data = {
	fieldNames: {
		cliente: '#os_cliente',
		sucursal: '#os_sucursal',
		areaServicio: '#os_area',
		tipoFalla: '#os_tipo_falla',
		txtTipoFalla: 'input[name=os_tipo_falla_txt]',
		checkSolucionado: 'input[name=os_solucionado]',
		fechaCompromiso: '#os_fecha',
		txtSolucion: '#os_solucion',
		checkOtherFalla: 'input[name=os_other_falla]',
		notaVenta: '#os_nota_venta',
		idNotaVenta: ':hidden[name=dc_nota_venta]',
        ticket: '#os_ticket_servicio',
        idTicket: ':hidden[name=dc_ticket_servicio]',
        tecnicoApoyo: '#os_tecnico_apoyo'
	},
	
	fields: {
		cliente: 			null,
		sucursal: 			null,
		areaServicio: 		null,
		tipoFalla: 			null,
		txtTipoFalla: 		null,
		checkSolucionado: 	null,
		fechaCompromiso: 	null,
		txtSolucion: 		null,
		checkOtherFalla: 	null,
		notaVenta: 			null,
		idNotaVenta: 		null,
        ticket:             null,
        idTicket:           null,
        tecnicoApoyo:       null
	},
	
	jForm: $('#cr_orden_servicio'),
	
	solucionOnDate: $('#solucion_after_date',this.jForm),
	solucionOnText: $('#solucion_after_text',this.jForm),
	
	sucursalDataUrl: 'sites/servicios/proc/get_sucursales_cliente.php',
	tipoFallaDataUrl: 'sites/servicios/proc/get_tipo_falla_area.php',
	notaVentaAcUrl: 'sites/proc/autocompleter/nota_venta_strict.php',
	notaVentaDataUrl: 'sites/servicios/proc/get_nota_venta_detalle.php',
    ticketAcUrl: 'sites/proc/autocompleter/ticket_servicio.php',
	
	//[CTU] Cambiar al estandarizar el sistema
	acOptions: {
		formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
		minChars: 2,
		width:300
	},
	
	init: function(){
		js_data.initFields();
		js_data.fields.cliente.change(js_data.changeCliente);
		js_data.fields.notaVenta.change(js_data.changeNotaVenta)
		.autocomplete(js_data.notaVentaAcUrl,js_data.acOptions)
		.result(function(){
			$(this).change();
		});
        
        js_data.fields.ticket.autocomplete(js_data.ticketAcUrl, js_data.acOptions).result(js_data.changeTicket);
		
		js_data.fields.areaServicio.change(js_data.changeAreaServicio);
		js_data.fields.checkSolucionado.click(js_data.changeSolucionado);
		js_data.fields.checkOtherFalla.click(js_data.changeOtraFalla);
        
        js_data.initMultiselect();
	},
            
    initMultiselect: function(){
        js_data.fields.tecnicoApoyo.multiSelect({
            selectAll: true,
            selectAllText: "Seleccionar todos",
            noneSelected: "---",
            oneOrMoreSelected: "% seleccionado(s)"
        });
    },
	
	initFields: function(){
		for(i in js_data.fields){
			js_data.fields[i] = $(js_data.fieldNames[i],js_data.jForm);
		}
	},
	
	changeCliente: function(){
		var dc_cliente = $(this).val();
		js_data.fields.sucursal.html('').attr('disabled',true);
		if(dc_cliente != 0){
			loadFile(js_data.sucursalDataUrl,js_data.fields.sucursal,'id='+dc_cliente,function(){
				js_data.fields.sucursal.attr('disabled',false);
			});
		}
	},
	
	changeAreaServicio: function(){
		var areaServicio = $(this).val();
		if(areaServicio != 0){
			js_data.fields.tipoFalla.attr('disabled',true);
			loadFile(js_data.tipoFallaDataUrl,js_data.fields.tipoFalla,'area='+areaServicio);
			js_data.fields.tipoFalla.attr('disabled',false);
		}else
			js_data.fields.tipoFalla.attr('disabled',true);
	},
	
	changeSolucionado: function(){
		if($(this).val() == 0){
			js_data.solucionOnDate.hide();
			js_data.solucionOnText.show();
			
			js_data.fields.fechaCompromiso.attr('required',false);
			js_data.fields.txtSolucion.attr('required',true);
		}else{
			js_data.solucionOnDate.show();
			js_data.solucionOnText.hide();
			
			js_data.fields.fechaCompromiso.attr('required',true);
			js_data.fields.txtSolucion.attr('required',false);
		}
	},
	
	changeOtraFalla: function(){
		if($(this).attr('checked')){
			js_data.fields.txtTipoFalla.attr('disabled',false).attr('required',true);
			js_data.fields.tipoFalla.attr('required',false);
		}else{
			js_data.fields.txtTipoFalla.attr('disabled',true).attr('required',false);
			js_data.fields.tipoFalla.attr('required',true);
		}
	},
	
	nvField: null,
	
	changeNotaVenta: function(){
		var field = $(this);
		js_data.nvField = field.clone();
		
		$.getJSON(js_data.notaVentaDataUrl,{number: field.val()},function(data){
			if(data == '<not-found>'){
				field.attr('disabled',false).css({backgroundImage: ''}).val('');
				//[CTU] cambiar al nuevo sistema
				show_error('No se ha encontrado la nota de venta especificada, intentelo denuevo');
				return;
			}
			
			js_data.createEditableDiv(data.dq_nota_venta,js_data.changeNotaVenta).width(field.width()).replaceAll(field);
			js_data.fields.idNotaVenta.val(data.dc_nota_venta);
	
		});
	},
            
    changeTicket: function(e, row){
      var field = $(this);
      js_data.ticketField = field.clone().val('');
      
      pymerp.getEditableBox(row[0],js_data.editTicket).replaceAll(field);
      js_data.fields.idTicket.val(row[3]);
    },
            
    editTicket: function(){
      var div = $(this).attr('src','images/ajax-loader.gif').parent();
      js_data.ticketField
             .autocomplete(js_data.ticketAcUrl, js_data.acOptions)
             .result(js_data.changeTicket)
             .replaceAll(div);
      js_data.fields.idTicket.val(0);
    },
	
	createEditableDiv: function(text, chAction){
		var edbtn = $('<img>').multiAttr({
			src: 'images/editbtn.png',
			title: 'Editar',
			alt: '[ED]',
			class:'right hidden'
		}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
		.mouseout(function(){$(this).css({background:'',border:''})})
		.click(function(){
			var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
			
			js_data.nvField
			.autocomplete(js_data.notaVentaAcUrl,js_data.acOptions)
			.result(function(){
				$(this).change();
			}).change(chAction).replaceAll(innerdiv);
			
			js_data.fields.idNotaVenta.val(0);
		});
		
		var div = $('<div>').addClass('inputtext').css({
			background:'#EEE',
			fontWeight:'bold'
		}).text(text)
		.mouseenter(function(){$(this).children('img').show();})
		.mouseleave(function(){$(this).children('img').hide();})
		.prepend(edbtn);
		
		return div;
		
	}
	
};
js_data.init();