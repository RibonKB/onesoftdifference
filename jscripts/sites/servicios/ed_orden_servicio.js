$.extend(js_data,{
  editInit: function(nota_venta, ticket){
    if(nota_venta){
      js_data.changeNotaVenta.call(js_data.fields.notaVenta);
    }
    
    if(ticket){
      var row = [ticket.dq_ticket,null,null,ticket.dc_ticket];
      js_data.changeTicket.call(js_data.fields.ticket,null,row);
    }
  }
});