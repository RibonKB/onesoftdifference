var gestion_telefonica = {
  
  form: $('#cr_cierre_telefonico'),
  
  init: function(){
    gestion_telefonica.initStars();
    gestion_telefonica.initSolucionPanes();
  },
          
  initStars: function(){
    var star_hidden = $(':hidden[name=dc_evaluacion_servicio]');
    var stars = gestion_telefonica.getStarEvaluation(star_hidden);
    
    star_hidden.before(stars);
  },
  
  getStarEvaluation: function(hidden){
    var img = $('<img>').attr('src','images/star_off.png').mouseover(gestion_telefonica.starOver).mouseleave(gestion_telefonica.starOut);
    var div = $('<span>').css({
      'border': '1px solid #EEE',
      'padding': '5px',
      'backgroundColor': '#FFF'
    });
    
    for(var i = 1; i <= 10; i++){
      var clone = img.clone(true);
      clone.attr('rel',i);
      clone.click(function(){
        hidden.val($(this).attr('rel'));
        $(this).prevAll().add(this).attr('src','images/star.png');
      });
      div.append(clone);
    }
    
    return div;
  },
          
  starOver: function(){
    $(this).prevAll().add(this).attr('src','images/star.png');
    $(this).nextAll().attr('src','images/star_off.png');
  },
          
  starOut: function(){
    $(this).prevAll().add(this).attr('src','images/star_off.png');
    var actual = $(':hidden[name=dc_evaluacion_servicio]').val();
    if(actual > 0){
      var div = $(this).parent();
      var star = $('img[rel='+actual+']',div)
      star.prevAll().add(star).attr('src','images/star.png');;
    }
  },
          
  solucionadoPane: null,
  reabrirPane: null,
          
  initSolucionPanes: function(){
      gestion_telefonica.solucionadoPane = $('#tel_gestion_solucionado',gestion_telefonica.form);
      gestion_telefonica.reabrirPane = $('#tel_gestion_reabrir',gestion_telefonica.form);
      
      gestion_telefonica.reabrirPane.hide();
      
      $(':radio[name=dm_estado_solucion]').click(gestion_telefonica.changeEstadoSolucion);
      
  },
  
  changeEstadoSolucion: function(){
    var value = $(this).val();
    if(value == 1){
      gestion_telefonica.solucionadoPane.hide();
      gestion_telefonica.reabrirPane.show();
    }else{
      gestion_telefonica.solucionadoPane.show();
      gestion_telefonica.reabrirPane.hide();
    }
  }
  
};
gestion_telefonica.init();