var js_data = {
	
	fieldNames: {
		cantDistribution: '#month_cant_distribution',
		amountDistribution: '#month_amount_distribution',
		cantidadAnual: '#dq_cantidad_anual',
		montoAnual: '#dq_monto_anual',
		cantidadMensual: '.dq_cantidad_mensual',
		montoMensual: '.dq_monto_mensual',
		setExecutiveBtn: '.setExecutive',
		
		dc_percentage: '.dc_percentage',
		
		dc_ejecutivo: 'dc_ejecutivo',
		dq_cantidad_ejecutivo: 'dq_cantidad_ejecutivo',
		dc_mes: 'dc_mes'
	},
	
	ejecutivosList: null,
	mesesList: null,
	exSelect: null,
	mesSelect: null,
	
	actualExTbody: null,
	
	jForm: $('#cr_meta'),
	
	init: function(){
		$(js_data.fieldNames.amountDistribution,js_data.jForm).click(js_data.distrMontos);
		$(js_data.fieldNames.cantDistribution,js_data.jForm).click(js_data.distrCantidades);
		js_data.initEjecutivosSelect();
		js_data.initMesesSelect();
		
		$(js_data.fieldNames.setExecutiveBtn,js_data.jForm).click(js_data.setExecutiveForm);
	},
	
	distrCantidades: function(){
		var val = parseInt($(js_data.fieldNames.cantidadAnual,js_data.jForm).val());
		if(!val)return;
		
		var porcion = parseInt(val/12);
		
		var fields = $(js_data.fieldNames.cantidadMensual,js_data.jForm).each(function(){
			$(this).val(porcion);
		});
		
		if(porcion*12 < val){
			fields.first().val(porcion+(val-porcion*12));
		}
	},
	
	distrMontos: function(){
		var val = parseInt($(js_data.fieldNames.montoAnual,js_data.jForm).val());
		if(!val)return;
		
		var porcion = parseInt(val/12);
		
		var fields = $(js_data.fieldNames.montoMensual,js_data.jForm).each(function(){
			$(this).val(porcion);
		});
		
		if(porcion*12 < val){
			fields.first().val(porcion+(val-porcion*12));
		}
	},
	
	initEjecutivosSelect: function(){
		if(js_data.exSelect != null) return;
		var sel = $('<select>').addClass('inputtext').addClass('dc_ejecutivo');
		sel.append($('<option>'));
		for(i in js_data.ejecutivosList){
			$('<option>').val(i).text(js_data.ejecutivosList[i]).appendTo(sel);
		}
		js_data.exSelect = sel;
	},
	
	initMesesSelect: function(){
		if(js_data.mesSelect != null) return;
		var sel = $('<select>').addClass('inputtext').addClass('dc_mes');
		sel.append($('<option>'));
		for(i in js_data.mesesList){
			$('<option>').val(i).text(js_data.mesesList[i]).appendTo(sel);
		}
		js_data.mesSelect = sel;
	},
	
	setExecutiveForm: function(){
		var month = $(this).next(':hidden').val();
		
		var div = $('<div>').addClass('overlay');
		$('<div>').addClass('secc_bar').text('Meta por ejecutivo').appendTo(div);
		
		var fieldset = $('<fieldset>').addClass('decorated');
		
		$('<div>').addClass('info')
			.html('<strong>Indique las metas que tendrá el ejecutivo en el mes escogido</strong><br />El porcentaje total deberá ser 100%')
			.appendTo(fieldset);
		
		$('<div>')
			.append('<span>Copiar información desde mes: </span><br />')
			.append(js_data.mesSelect.clone())
			.appendTo(fieldset);
			
		$('<hr>').appendTo(fieldset);
		
		var tbody = $('<tbody>');
		js_data.actualExTbody = tbody;
		
		$('<table>').addClass('tab').width('80%').css({margin:'auto'})
		.append($('<thead>')
			.append($('<tr>')
				.append($('<th>').text('OPT'))
				.append($('<th>').text('Ejecutivo'))
				.append($('<th>').text('Porcentaje de meta'))
			)
		).append(tbody).appendTo(fieldset);
		
		$('<button>').click(js_data.addExecutive).multiAttr({
			'class': 'addbtn',
			'type': 'button'
		}).text('Agregar ejecutivo').appendTo(fieldset).wrap('<div class="center">');
		
		$('<hr>').appendTo(fieldset);
		
		$('<button>').multiAttr({
			'class': 'checkbtn',
			'type': 'button'
		}).text('Confirmar').appendTo(fieldset)
		.click(js_data.confirmSetExecutive)
		.wrap('<div class="center">')
		.after($('<input>').multiAttr({ 'id': 'dc_mes', 'type': 'hidden' }).val(month));
		
		fieldset.appendTo(div).wrap($('<div>').addClass('panes'));
		$('#content').append(div);
		js_data.addExecutive();
		tbody.find('.dc_percentage').val(100);
		pymerp.objectOverlay(div);
	},
	
	addExecutive: function(){
		var tbody = js_data.actualExTbody;
		if(tbody == null) return;
		
		var delBtn = $('<img>').multiAttr({
			'src': 'images/delbtn.png',
			'alt': '[DEL]',
		}).click(js_data.deleteExecutive);
		
		var exList = js_data.exSelect.clone();
		var exPercentage = $('<input>').multiAttr({
			'type': 'text',
			'class': 'dc_percentage inputtext',
			'size': '20'
		});
		
		return $('<tr>')
			.append($('<td>').append(delBtn))
			.append($('<td>').append(exList))
			.append($('<td>').append(exPercentage))
		.appendTo(tbody);
		
	},
	
	deleteExecutive: function(){
		$(this).parent().parent().remove();
	},
	
	copyMonth: function(){
		
	},
	
	confirmSetExecutive: function(){
		var tr = $('tr',js_data.actualExTbody);
		
		var total_p = 0;
		var woEx = false;
		tr.each(function(){
			total_p += parseFloat($(js_data.fieldNames.dc_percentage,this).val());
			if($('.dc_ejecutivo',this).val() === '')
				woEx = true;
		});
		
		if(total_p != 100.0){
			pymerp.showError('La suma de porcentajes debe ser igual a 100%');
			return;
		}
		
		if(woEx){
			pymerp.showError('Debe indicar todos los ejecutivos');
			return;
		}
		
		var dc_mes = $('#dc_mes').val();
		var container = $('#ex_container'+dc_mes);
		container.find('.ex_container').remove();
		
		tr.each(function(){
			var dc_ejecutivo = $('.dc_ejecutivo',this).val();
			var dg_ejecutivo = $('.dc_ejecutivo option:selected',this).text();
			var dq_cantidad = parseFloat($(js_data.fieldNames.dc_percentage,this).val());
			
			container.append(js_data.getExecutiveContainer(dc_ejecutivo,dg_ejecutivo,dq_cantidad,dc_mes));
		});
		
		$('.overlay').remove();
		js_data.actualExTbody = null;
		
	},
	
	getExecutiveContainer: function(dc_ejecutivo, dg_ejecutivo, dq_cantidad, dc_mes){
		var div = $('<div>').css({
			background: '#DDD',
			color: '#222',
			border: '1px solid #999',
			padding: '4px',
			float: 'left',
			margin: '3px'
		}).addClass('ex_container')
		.html(dg_ejecutivo+' - <strong>'+dq_cantidad+'%</strong>');
		
		$('<input>').multiAttr({'type': 'hidden', 'name': js_data.fieldNames.dc_ejecutivo+'['+dc_mes+'][]' }).val(dc_ejecutivo).appendTo(div);
		$('<input>').multiAttr({'type': 'hidden', 'name': js_data.fieldNames.dq_cantidad_ejecutivo+'['+dc_mes+'][]' }).val(dq_cantidad).appendTo(div);
		
		return div;
		
	}
};