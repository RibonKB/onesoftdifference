var js_data = {
	
	nv_data: [],
	cot_data: [],
	
	ejecutivos: [],
	clientes: [],
	marcas: [],
	lineas_negocio: [],
	level_color: ['#FFF','#EEE','#DDD','#CCC','#BBB'],
	
	up_indicator: $('<img>').multiAttr({
		'src': 'images/up_arrow.png',
		'alt': '^'
	}),
	
	down_indicator: $('<img>').multiAttr({
		'src': 'images/down_arrow.png',
		'alt': 'v'
	}),
	
	collapseBtn: $('<img>').multiAttr({
		'src': 'images/windows_expand.png',
		'alt': '+'
	}).css({
		'margin': '0px 3px'
	}),
	
	collapseImage: 'images/windows_collapse.png',
	expandImage: 'images/windows_expand.png',
	
	cambio: 1,
	
	conditions: {},
	
	metas: [],
	
	labels: {
		dc_marca: [],
		dc_ejecutivo: [],
		dc_cliente: [],
		dc_linea_negocio: [],
		dc_producto: []
	},
	
	tbody: $('tbody',$('#dinamic_table')),
	
	init: function(){
		
		js_data.initPoblation();
		
		$('#load_table').click(function(){
			js_data.initPoblation();
		});
		
		$('#up_field').click(js_data.upField);
		$('#down_field').click(js_data.downField);
		$('#delete_field').click(js_data.deleteField);
		$('#add_field').click(js_data.addField);
		
		$('#tipos_cambio').change(js_data.chTipoCambio);
		
	},
	
	chTipoCambio: function(){
		js_data.cambio = parseFloat($(this).val());
	},
	
	collapseExpand: function(){
		var src = $(this).attr('src');
		var tr = $(this).parent().parent();
		if(src == js_data.expandImage){
			var src = js_data.collapseImage;
			var alt = '-';
		}else{
			var src = js_data.expandImage;
			var alt = '+';
		}
		
		var level = parseInt(tr.attr('rel'));
		
		$(this).multiAttr({
			src: src,
			alt: alt
		});
		
		if(alt == '+'){
			while(true){
				tr = tr.next();
				if(tr.size() == 0)
					return;
				var cur_level = tr.attr('rel');
				if(cur_level == level)
					return;
				else
					tr.hide().children('td').first().children('img').attr('src',js_data.expandImage);
			}
		}else{
			while(true){
				tr = tr.next();
				if(tr.size() == 0)
					return;
				var cur_level = tr.attr('rel');
				if(cur_level == (level+1)){
					tr.show();
				}else if(cur_level == level){
					return;
				}
			}
		}
	},
	
	upField: function(){
		var element = $('#multi_in option:selected').first();
		element.prev().before(element);
	},
	
	downField: function(){
		var element = $('#multi_in option:selected').first();
		element.next().after(element);
	},
	
	deleteField: function(){
		$('#multi_in option:selected').appendTo('#multi_ex');
	},
	
	addField: function(){
		$('#multi_ex option:selected').appendTo('#multi_in');
	},
	
	initPoblation: function(){
		//cambiar al cambiar motor javascript
		show_loader();
		window.setTimeout(function(){
			js_data.conditions = {};
			js_data.tbody.html('');
			js_data.recPoblarTabla(js_data.nv_data,js_data.cot_data,$('#multi_in option').first(),0,0);
			$('tr[rel!=0]',js_data.tbody).hide();
			//"cambiar al cambiar" motor javascript xD!
			hide_loader();
		},500);
	},
	
	recPoblarTabla: function(collection_nv, collection_cot, group_criteria, padding, level){
		//Filtrar
		var filtered_nv = js_data.getFiltered(collection_nv);
		var filtered_cot = js_data.getFiltered(collection_cot);
		
		//Agrupar por nota de venta y factura
		var grouped = [];
		var gindex = group_criteria.attr('value');
		for(i in filtered_nv){
			var dc_index = filtered_nv[i][gindex];
			if(!grouped[dc_index]){
				grouped[dc_index] = [0,0,0,0];
			}
			grouped[dc_index][1] += parseFloat(filtered_nv[i].dq_cierre);
			grouped[dc_index][3] += parseFloat(filtered_nv[i].dq_facturado);
		}
		
		//Agrupar por cotizaciones
		for(i in filtered_cot){
			var dc_index = filtered_cot[i][gindex];
			if(!grouped[dc_index]){
				grouped[dc_index] = [0,0,0,0];
			}
			grouped[dc_index][2] += parseFloat(filtered_cot[i].dq_proyeccion);
		}
		
		//Agrupar metas
		var filtered_goal = js_data.getFilteredGoal(gindex);
		if(filtered_goal == '-'){
			for(i in grouped){
				grouped[i][0] = '-';
			}
		}else{
			for(i in grouped){
				if(filtered_goal[i])
					grouped[i][0] += parseFloat(filtered_goal[i]);
				else
					grouped[i][0] = '-';
			}
		}
		
		//Insertar filas
		var more_subs = group_criteria.next().size() > 0;
		var tr = $('<tr>').attr('rel',level);
		
		var total = [0,0,0,0,0,0,0];
		
		for(i in grouped){
			
			grouped[i][1] = grouped[i][1]/js_data.cambio;
			grouped[i][2] = grouped[i][2]/js_data.cambio;
			grouped[i][3] = grouped[i][3]/js_data.cambio;
			
			total[1] += grouped[i][1];
			total[3] += grouped[i][2];
			total[5] += grouped[i][3];
			
			if(grouped[i][0] != '-'){
				grouped[i][0] = grouped[i][0]/js_data.cambio;
				
				var p_diferencia_nv = (((grouped[i][1]-grouped[i][0])*100)/grouped[i][0]).toFixed(2)+'%';
				var p_diferencia_cot = (((grouped[i][2]-grouped[i][0])*100)/grouped[i][0]).toFixed(2)+'%';
				var p_diferencia_fv = (((grouped[i][3]-grouped[i][0])*100)/grouped[i][0]).toFixed(2)+'%';
				
				var indicador_nv = (grouped[i][1]-grouped[i][0]) < 0 ? js_data.down_indicator.clone(): js_data.up_indicator.clone();
				var indicador_cot= (grouped[i][2]-grouped[i][0]) < 0 ? js_data.down_indicator.clone(): js_data.up_indicator.clone();
				var indicador_fv = (grouped[i][3]-grouped[i][0]) < 0 ? js_data.down_indicator.clone(): js_data.up_indicator.clone();
				
				var diferencia_nv = mil_format((grouped[i][1]-grouped[i][0]).toFixed(2));
				var diferencia_cot = mil_format((grouped[i][2]-grouped[i][0]).toFixed(2));
				var diferencia_fv = mil_format((grouped[i][3]-grouped[i][0]).toFixed(2));
				var meta = mil_format(grouped[i][0].toFixed(2));
				
				total[0] += grouped[i][0];
				total[2] += grouped[i][1]-grouped[i][0];
				total[4] += grouped[i][2]-grouped[i][0];
				total[6] += grouped[i][3]-grouped[i][0];
				
			}else{
				var p_diferencia_nv = '-';
				var p_diferencia_cot = '-';
				var p_diferencia_fv = '-';
				
				var indicador_nv = '-';
				var indicador_cot= '-';
				var indicador_fv = '-';
				
				var diferencia_nv = '-';
				var diferencia_cot = '-';
				var diferencia_fv = '-';
				var meta = '-';
			}
			var facturado = mil_format(grouped[i][3].toFixed(2));
			var proyeccion = mil_format(grouped[i][2].toFixed(2));
			var cierre = mil_format(grouped[i][1].toFixed(2));
			
			var real_tr = tr.clone()
			.append($('<td>').text(js_data.labels[gindex][i]).css({'paddingLeft': padding}))
			.append($('<td align="right">').text(meta))				//Meta para la entidad actual (o un guión si no tiene)
			.append($('<td align="right">').text(cierre))			//La suma de las notas de venta
			.append($('<td align="right">').text(diferencia_nv))	//Lo que le faltó para llegar a la meta
			.append($('<td>').text(p_diferencia_nv)) 				//Diferencia porcentual Cierre
			.append($('<td align="center">').append(indicador_nv))	 							//Indicador NV
			.append($('<td align="right">').text(proyeccion))		//La proyección que tenía de ventas
			.append($('<td align="right">').text(diferencia_cot))	//Lo proyectado versus la meta
			.append($('<td>').text(p_diferencia_cot))				//Diferencia porcentual Proyección
			.append($('<td align="center">').append(indicador_cot))				//Indicador COT
			.append($('<td align="right">').text(facturado))		//Lo facturado
			.append($('<td align="right">').text(diferencia_fv))	//Lo facturado versus la meta
			.append($('<td>').text(p_diferencia_fv))				//Diferencia porcentual Facturación
			.append($('<td align="center">').append(indicador_fv))					//Indicador FV
			.appendTo(js_data.tbody)
			.children('td').css({'backgroundColor': js_data.level_color[level]})
			
			js_data.conditions[gindex] = i;
			if(more_subs){
				js_data.recPoblarTabla(filtered_nv,filtered_cot,group_criteria.next(),padding+20,level+1);
				var expand = js_data.collapseBtn.clone().click(js_data.collapseExpand);
				real_tr.first().prepend(expand);
			}
			delete js_data.conditions[gindex];
		}
		
		if(level == 0){
			$('#total_goal').text(mil_format(total[0].toFixed(2)));
			$('#total_cierre').text(mil_format(total[1].toFixed(2)));
			$('#total_cierre_diferencia').text(mil_format(total[2].toFixed(2)));
			$('#total_cierre_porciento').text('-');
			$('#total_cierre_indicador').text('-');
			$('#total_proyeccion').text(mil_format(total[3].toFixed(2)));
			$('#total_proyeccion_diferencia').text(mil_format(total[4].toFixed(2)));
			$('#total_proyeccion_porciento').text('-');
			$('#total_proyeccion_indicador').text('-');
			$('#total_facturado').text(mil_format(total[5].toFixed(2)));
			$('#total_facturado_diferencia').text(mil_format(total[6].toFixed(2)));
			$('#total_facturado_porciento').text('-');
			$('#total_facturado_indicador').text('-');
		}
		
	},
	
	getFiltered: function(original, conditions){
		if(!conditions)
			conditions = js_data.conditions;
		var cont = 0;
		for(i in conditions){
			cont++;
		}
		if(cont == 0){
			return original;
		}
		
		var filtered = [];
		$.each(original,function(){
			for(i in conditions){
				if(this[i] != conditions[i]){
					return;
				}
			}
			filtered.push(this);
		});
		
		return filtered;
	},
	
	getFilteredGoal: function(type){
		
		if(type != 'dc_ejecutivo' && type != 'dc_marca' && type != 'dc_linea_negocio'){
			return '-';
		}
		
		var conditions = {};
		for(i in js_data.conditions){
			if(i == 'dc_ejecutivo' || i == 'dc_marca' || i == 'dc_linea_negocio'){
				conditions[i] = js_data.conditions[i];
			}
		}
		
		var filtered = js_data.getFiltered(js_data.metas, conditions);
		
		var grouped = [];
		
		for(i in filtered){
			var index = filtered[i][type];
			if(!grouped[index]){
				grouped[index] = 0.0;
			}
			grouped[index] += parseFloat(filtered[i]['dq_meta']);
		}
		
		return grouped;
	}
};