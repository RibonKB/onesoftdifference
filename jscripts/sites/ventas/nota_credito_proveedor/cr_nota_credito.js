var js_data = {
	
	dq_monto: 0,
	
	init: function(){
		$('#get_factura_venta').click(js_data.getMultiFactura);
		$('#get_guia_despacho').click(js_data.getMultiGuiaDespacho);
		js_data.getSingleFactura();
		js_data.formatMontoPropuesto();
	},
	
	formatMontoPropuesto: function(){
		$('#dq_monto_propuesto').change(function(){
			var val = pymerp.toNumber($(this).val());
			if(!val){
				$(this).val('');
				return;
			}
			$(this).val(pymerp.milFormat(val));
			
			var neto = (val/((pymerp.localIVA/100)+1)).toFixed(pymerp.localDecimales);
			
			$('#neto_propuesto').html('Neto: <b>$'+pymerp.milFormat(neto)+'</b>');
			
		});
	},
	
	getMultiFactura: function(){
		var dq_monto = pymerp.toNumber($('#dq_monto_propuesto').val());
		if(!dq_monto){
			pymerp.showAviso('Debe indicar un monto propuesto para poder asignar facturas múltiples');
			return;
		}
		var dc_proveedor = $('input:hidden[name="dc_proveedor"]').val();
		var data = $('#selected_facturas :input').serialize();
		pymerp.loadOverlay('sites/ventas/nota_credito_proveedor/proc/get_multi_factura.php','dc_proveedor='+dc_proveedor+'&dq_monto='+dq_monto+'&'+data);
	},
	
	setSingleFactura: function(e, row){
		if(!parseInt($(this).val())) return;
	
		var v = get_disabled(this);
		var dc_factura = row[3];
		
		$.getJSON('sites/ventas/nota_credito_proveedor/proc/get_factura_compra_detalle.php',{ dc_factura: dc_factura },function(data){
			
			if(data == '<not-found>'){
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				pymerp.showError('No se ha encontrado la factura de compra especificada, intentelo denuevo');
				return;
			}
			
			if(data[0] == '<empty>'){
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				pymerp.showError('El detalle de la factura de compra es inválido, no se puede continuar');
				return;
			}
			
			var det = data[0];
			var dc_factura = data[1].dc_factura;
			
			for(i in det){
				
				if(det[i].dq_cantidad > 0){
					var q = det[i].dq_cantidad;
					var detalle = det[i];

						d = get_valid_detail(det[i]);
						d.addClass('strict');
						var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
						d.find('.prod_cant').after(maxv);
						d.appendTo('#prod_list');

				}
			}
			
			create_number_div(v.val(),'factura_compra',js_data.setSingleFactura,function(){
				$('#prod_list .strict').remove();
			}).width(v.width()).replaceAll(v);
			$(':hidden[name=dc_factura_simple]').val(dc_factura);
			
			actualizar_detalles();
			actualizar_totales();
			
		});
	},
	
	getSingleFactura: function(){
		
		$('#dq_factura').autocomplete('sites/proc/autocompleter/factura_compra_strict.php',{
			formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
			minChars: 2,
			width:300,
			extraParams: {
				dc_proveedor: js_data.dc_proveedor
			}
		}).result(js_data.setSingleFactura);
	},
	
	initMultiFactura: function(){
		window.setTimeout(function(){
			$('#tb_multi_factura_data').tableAdjust(10);
			$('#setMultiFactura').click(js_data.setMultiFactura);
			$('.dc_selected_factura').click(js_data.setFactura);
		},100);
	},
	
	setMultiFactura: function(){
		var div = $('#selected_facturas');
		div.find(':hidden').remove();
		
		$('#facturas_list_div').removeClass('hidden');
		var tbody = $('#facturas_checklist');
		tbody.html('');
		
		$('.dc_selected_factura:checked').each(function(){
			var td = $(this).parent();
			$('<input>').multiAttr({
				'type': 'hidden',
				'name': 'dc_factura[]'
			}).val($(this).val()).appendTo(div);
			
			$('<input>').multiAttr({
				'type': 'hidden',
				'name': 'dq_monto_pagar[]'
			}).val(td.find('.dq_monto_saldo').val())
			.appendTo(div);
			
			var tr = $('<tr>');
			
			var td = $(this).parent();
			
			tr
			.append(td.next().clone())
			.append(td.next().next().clone())
			.append(td.next().next().next().next().next().next().clone());
			
			tbody.append(tr);
			
		});
		$('#genOverlay').remove();
	},
	
	setFactura: function(){
		var checked = $(this).attr('checked');
		if(checked){
			js_data.setFacturaEnabled.call(this);
		}else{
			js_data.setFacturaDisabled.call(this);
		}
	},
	
	setFacturaEnabled: function(){
		var _this = $(this);
		var tr = _this.parent().parent();
		var dq_monto = js_data.dq_monto;
		var dq_saldo_actual = parseFloat(_this.next('.dq_monto_actual').val());
		var dq_saldo_final = dq_monto-dq_saldo_actual;
		
		if(dq_saldo_final > 0){
			tr.find('.dq_monto_final').text(0);
			tr.find('.dq_saldo_ocupado').text(pymerp.milFormat(dq_saldo_actual.toFixed(0)));
			var dq_pagado = dq_saldo_actual;
			dq_monto -= dq_saldo_actual;
		}else{
			tr.find('.dq_monto_final').text(pymerp.milFormat((dq_saldo_actual-dq_monto).toFixed(0)));
			tr.find('.dq_saldo_ocupado').text(pymerp.milFormat(dq_monto.toFixed(0)));
			var dq_pagado = dq_monto;
			dq_monto = 0;
		}
		
		tr.find('.dq_monto_saldo').val(dq_pagado);
		js_data.setDqMonto(dq_monto);
		
		if(dq_monto == 0){
			$('.dc_selected_factura:not(:checked)').attr('disabled',true);
		}
		
	},
	
	setFacturaDisabled: function(){
		var _this = $(this);
		
		var tr = _this.parent().parent();
		var dq_pagado = parseFloat(tr.find('.dq_monto_saldo').val());
		js_data.setDqMonto(js_data.dq_monto+dq_pagado);
		tr.find('dq_monto_saldo').val(0);
		tr.find('.dq_no_num').text('-');
		if(js_data.dq_monto > 0){
			$('.dc_selected_factura:disabled').attr('disabled',false);
		}
	},
	
	setDqMonto: function(dq_monto){
		js_data.dq_monto = dq_monto;
		$('#dq_monto_disponible').val(pymerp.milFormat(dq_monto.toFixed(0)));
	},
	
	getMultiGuiaDespacho: function(){
		var dc_proveedor = $('input:hidden[name="dc_proveedor"]').val();
		pymerp.loadOverlay('sites/ventas/nota_credito_proveedor/proc/get_multiple_guia_despacho.php','dc_proveedor='+dc_proveedor);
	},
	
	setMultiGuiaDespacho: function(e){
		e.preventDefault();
		
		var gdContainer = $('#selected_guias');
		gdContainer.html('');
		
		$(this).find(':checkbox:checked').each(function(){
			$('<input>').multiAttr({
				'type': 'hidden',
				'name': 'dc_guia_despacho[]',
			})
			.val($(this).val())
			.appendTo(gdContainer);
		});
		
		$(this).parents('.overlay').remove();
		
	}
	
};

actualizar_totales = function(t,v,a){
	
t = t?t:"#prod_list";
v = v?v:"#total";
a = a!=undefined?a:true;
	
var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$(t+" tr.main").each(function(){
		var cant = parseFloat($(this).find('.prod_cant').val());
		
		var price = $(this).find('.prod_price').val();
		if(price)
			price = toNumber(price);
		else
			price = 0;
		
		var cost = $(this).find('.prod_costo').val();
		if(cost)
			cost = toNumber(cost);
		else
			cost = 0;
	
		total += price*cant;
		total_costo += cost*cant;
		total_margen += price-cost;
		total_neto += price*tc*cant;
	});
	
	$('#dq_neto').val(mil_format(total_neto.toFixed(empresa_dec)));
	$('#dq_iva').val(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$('#dq_total').val(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));
	
	if(a){
		$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
		$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));
	}

};