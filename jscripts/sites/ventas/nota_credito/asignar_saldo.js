js_data = {
	
	form: $('#asignar_saldo_favor_form'),
	autocompleterURL: {
		facturaVenta: null,
		notaCredito: null
	},
	
	init: function(){
		
		js_data.autocompleterURL.facturaVenta = js_data.buildFactory('facturaVentaAutoCompletar');
		js_data.autocompleterURL.notaCredito = js_data.buildFactory('notaCreditoAutoCompletar');
		
		js_data.initNotaCreditoField();
		js_data.initFacturaVentaField();
		
	},
	
	AC_options: {
		formatItem: function(row){
			return row[0]+" ( "+row[1]+" ) "+row[2];
		},
		minChars: 2,
		width: 400
	},
	
	initNotaCreditoField: function(){
		var nc = $('#dc_nota_credito',js_data.form);
		js_data.nc_field = nc.clone();
		
		nc.autocomplete(js_data.autocompleterURL.notaCredito, js_data.AC_options)
			.result(js_data.acNotaCreditoResult);
	},
	
	initFacturaVentaField: function(){
		var fv = $('#dc_factura_venta',js_data.form);
		js_data.fv_field = fv.clone();
		
		fv.autocomplete(js_data.autocompleterURL.facturaVenta, js_data.AC_options)
			.result(js_data.acFacturaVentaResult);
	},
	
	acNotaCreditoResult: function(e,row){
		var div = pymerp.getEditableBox(row[0],js_data.edNotaCreditoField).width($(this).width());
		div.append($('<input>').attr('name','dc_nota_credito').attr('type','hidden').val(row[3]));
		$(this).replaceWith(div);
		
		pymerp.loadFile(js_data.buildFactory('obtenerDetallesNotaCredito'),$('#nota_credito_data',js_data.form),'dc_nota_credito='+row[3]);
	},
	
	acFacturaVentaResult: function(e,row){
		var div = pymerp.getEditableBox(row[0],js_data.edFacturaVentaField).width($(this).width());
		div.append($('<input>').attr('name','dc_factura_venta').attr('type','hidden').val(row[3]));
		$(this).replaceWith(div);
		
		pymerp.loadFile(js_data.buildFactory('obtenerDetallesFacturaVenta'),$('#factura_venta_data',js_data.form),'dc_factura_venta='+row[3]);
	},
	
	edNotaCreditoField: function(){
		$(this).parent().replaceWith(js_data.nc_field);
		$('#nota_credito_data',js_data.form).html('');
		js_data.initNotaCreditoField();
	},
	
	edFacturaVentaField: function(){
		$(this).parent().replaceWith(js_data.fv_field);
		$('#factura_venta_data',js_data.form).html('');
		js_data.initFacturaVentaField();
	},
	
	buildFactory: function(action){
		return "sites/ventas/nota_credito/index.php?factory=AsignarSaldo&action="+action;
	}
	
};