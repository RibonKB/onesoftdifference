var js_data = {
	cambio: 1,
	cambioDecimales: pymerp.localDecimales,
	
	emptyDetail: $('#prods_form tr'),
	prodList: '#prod_list',
	detailRow: 'tr.main',
	validDetailClass: 'main',
	
	detailsContainer: $('#prods'),
	
	addDetailButton: '#prod_add',
	
	jForm: $('.ventas_form'),
	
	baseInit: function(){
		$(js_data.addDetailButton,js_data.jForm).click(js_data.addEmptyDetail);
		$('.more_details',js_data.emptyDetail).toggle(js_data.showLocalValues,js_data.hideLocalValues);
		$('.del_detail',js_data.emptyDetail).click(js_data.deleteDetail);
		
		$(js_data.fieldNames.cantidad,js_data.emptyDetail).change(js_data.completeRefresh);
		$(js_data.fieldNames.margenPorcentual,js_data.emptyDetail).change(js_data.editMargin);
		$(js_data.fieldNames.precio,js_data.emptyDetail).change(js_data.completeRefresh);
		$(js_data.fieldNames.costo,js_data.emptyDetail).change(js_data.completeRefresh);
	},
	
	editMargin: function(){
		/*var tr = $(this).parent().parent();
		var costo = pymerp.toNumber(tr.find(js_data.fieldNames.costo).val()).toFixed(js_data.cambioDecimales);
		var margin = pymerp.toNumber($(this).val());
		if(margin == 'NaN'){margin=0.0;$(this).val(0);}
		var price = tr.find(js_data.fieldNames.precio).val(pymerp.milFormat(parseFloat(costo)+(costo*margin/100)));
		js_data.completeRefresh();*/
		var tr = $(this).parent().parent();
		var cost = pymerp.toNumber(tr.find(js_data.fieldNames.costo).val()).toFixed(js_data.cambioDecimales);
		var margin = pymerp.toNumber($(this).val())/100;
		if(margin == 'NaN'){margin=0.0;$(this).val(0);}
		var price = -cost/(margin - 1);
		tr.find(js_data.fieldNames.precio).val(pymerp.milFormat(price));
		js_data.completeRefresh();
	},
	
	addEmptyDetail: function(){
		var det = js_data.emptyDetail.clone(true).appendTo(js_data.prodList);
		det.find(js_data.fieldNames.codigo)
			.autocomplete(js_data.AC_prodCode,js_data.AC_options)
			.result(js_data.AC_prodResult);
	},
	
	getEmptyDetail: function(){
		return js_data.emptyDetail.clone(true);
	},
	
	fieldNames: {
		codigo: ".prod_codigo",
		id_producto: "prod[]",
		cantidad: ".prod_cant",
		descripcion: '.prod_desc',
		precio: ".prod_price",
		precioTotal: ".total",
		costo: ".prod_costo",
		costoTotal: ".costo_total",
		margen: ".margen",
		margenPorcentual: ".margen_p",
		localPrecio: ".l_price",
		localPrecioTotal: ".l_total",
		localCosto: ".l_costo",
		localCostoTotal: ".l_costo_total",
		localMargen: ".l_margen",
		footerPrecioTotal: "#total",
		footerCostoTotal: "#total_costo",
		footerMargenTotal: "#total_margen",
		footerMargenPorcentual: "#total_margen_p",
		footerNeto: "#total_neto",
		footerIva: "#total_iva",
		footerTotalPagar: "#total_pagar",
		totalIvaDoc: ":hidden[name='cot_iva']",
		totalNetoDoc: ":hidden[name='cot_neto']"
	},
	
	showLocalValues: function(){
		$(this).parent().parent().next().show();
		$(this).attr('src','images/minusbtn.png');
	},
	
	hideLocalValues: function(){
		$(this).parent().parent().next().hide();
		$(this).attr('src','images/addbtn.png');
	},
	
	deleteDetail: function(){
		$(this).parent().parent().remove();
		js_data.completeRefresh();
	},
	
	/*
	*	AUTOCOMPLETE DATA
	*/
	
	AC_prodCode: 'sites/proc/autocompleter/producto.php',
	AC_baseProdResult: function(tr,row){
		
		var cantidad = tr.find(js_data.fieldNames.cantidad);
		if(!parseFloat(cantidad.val())){
			cantidad.val(1);
			cantidad = 1;
		}else{
			cantidad = cantidad.val();
		}
		
		tr.find(js_data.fieldNames.descripcion).val(row[1]);
		tr.find(js_data.fieldNames.precio).val(pymerp.milFormat(parseFloat(row[3]/js_data.cambio).toFixed(js_data.cambioDecimales)));
		tr.find(js_data.fieldNames.costo).val(pymerp.milFormat(parseFloat(row[4]/js_data.cambio).toFixed(js_data.cambioDecimales)));
		
		return js_data.getEditableDiv(row[0],js_data.prodReturnToInput);
		
	},
	
	prodReturnToInput: function(){
		var div = $(this).parent();
		pymerp.miniLoader.clone().replaceAll(this);
		
		js_data.emptyDetail
		.find(js_data.fieldNames.codigo).clone()
		.autocomplete(js_data.AC_prodCode,js_data.AC_options)
		.result(js_data.AC_prodResult)
		.replaceAll(div);
	},
	
	AC_prodResult: function(e,row){
		var tr = $(this).parent().parent();
		
		var div = js_data.AC_baseProdResult(tr,row);
		
		$('<input>').multiAttr({
			type:'hidden',
			name: js_data.fieldNames.id_producto
		}).val(row[5]).appendTo(div);
		
		div.replaceAll(this);
		
		js_data.detailRefresh();
		js_data.totalRefresh();
	},
	
	AC_options: {
		formatItem: function(row){
			return row[0]+" ( "+row[1]+" ) "+row[2];
		},
		minChars: 2,
		width: 300
	},
	
	/**
	*	CODE EDITABLE DIV
	**/
	
	getEditableDiv: function(text,editAction){

		var div = $('<div>').css({
			border:'1px solid #CCC',
			background:'#EEE',
			padding:'5px',
			fontWeight:'bold'
		}).mouseenter(function(){$(this).children('img').show();})
		.mouseleave(function(){$(this).children('img').hide();})
		.text(text);
		
		var editButton = $('<img>').multiAttr({
			'class': 'right hidden',
			'src': 'images/editbtn.png',
			'alt': '[|]',
			'title': 'Editar código',
			'width': '13',
			'height': '13'
		})
		.mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
		.mouseout(function(){$(this).css({background:'',border:''})})
		.prependTo(div)
		.click(editAction);
		
		return div;
		
	},
	
	simpleDetailRefresh: function(context){
		$(js_data.detailRow,context).each(function(){
			var tr = $(this);
			
			//Validar Cantidad
			var cantidad = parseInt(tr.find(js_data.fieldNames.cantidad).val());
			if(!cantidad){
				cantidad = 1;
				tr.find(js_data.fieldNames.cantidad).val(1);
			}
			
			//Validar precio
			var precio = pymerp.toNumber(tr.find(js_data.fieldNames.precio).val()).toFixed(js_data.cambioDecimales);
			if(precio == 'NaN'){
				precio = 0.0.toFixed(js_data.cambioDecimales);
				tr.find(js_data.fieldNames.precio).val(0.0);
			}
			tr.find(js_data.fieldNames.precio).val(pymerp.milFormat(precio));
			tr.find(js_data.fieldNames.precioTotal).text(pymerp.milFormat((precio*cantidad).toFixed(js_data.cambioDecimales)));
			
			//Validar costo
			var costo = pymerp.toNumber(tr.find(js_data.fieldNames.costo).val()).toFixed(js_data.cambioDecimales);
			if(costo == 'NaN'){
				costo = 0.0.toFixed(js_data.cambioDecimales);
				tr.find(js_data.fieldNames.costo).val(0.0);
			}
			tr.find(js_data.fieldNames.costo).val(pymerp.milFormat(costo));
			tr.find(js_data.fieldNames.costoTotal).text(pymerp.milFormat((costo*cantidad).toFixed(js_data.cambioDecimales)));
			
			//Calcular Margenes
			tr.find(js_data.fieldNames.margen).text(pymerp.milFormat((precio-costo).toFixed(js_data.cambioDecimales)));
			
			if(precio > 0)
				tr.find(js_data.fieldNames.margenPorcentual).val(((precio-costo)*100/precio).toFixed(2));
			else
				tr.find(js_data.fieldNames.margenPorcentual).val('100');
				
			//Calcular Valores en moneda local
			var local = tr.next();
			if(!local.hasClass(js_data.validDetailClass)){
				local
					.find(js_data.fieldNames.localPrecio)
					.text(pymerp.milFormat((precio*js_data.cambio).toFixed(pymerp.localDecimales)));
				
				local
					.find(js_data.fieldNames.localPrecioTotal)
					.text(pymerp.milFormat((precio*js_data.cambio*cantidad).toFixed(pymerp.localDecimales)));
				
				local
					.find(js_data.fieldNames.localCosto)
					.text(pymerp.milFormat((costo*js_data.cambio).toFixed(pymerp.localDecimales)));
				
				local
					.find(js_data.fieldNames.localCostoTotal)
					.text(pymerp.milFormat((costo*js_data.cambio*cantidad).toFixed(pymerp.localDecimales)));
					
				local
					.find(js_data.fieldNames.localMargen)
					.text(pymerp.milFormat(((precio-costo)*js_data.cambio).toFixed(pymerp.localDecimales)));
			}
		});
	},
	
	detailRefresh: function(){
		js_data.simpleDetailRefresh(js_data.detailsContainer);
	},
	
	simpleTotalRefresh: function(context, setTotals){
		var precioTotal = 0;
		var costoTotal = 0;
		var margenTotal = 0;
		var netoTotal = 0;
		
		$(js_data.detailRow,context).each(function(){
			var tr = $(this);
			
			var cantidad = parseInt(tr.find(js_data.fieldNames.cantidad).val());
			var precio = pymerp.toNumber(tr.find(js_data.fieldNames.precio).val());
			var costo = pymerp.toNumber(tr.find(js_data.fieldNames.costo).val());
		
			precioTotal += precio*cantidad;
			costoTotal += costo*cantidad;
		});
		
		margenTotal = precioTotal-costoTotal;
		netoTotal = precioTotal*js_data.cambio;
		ivaTotal = netoTotal*pymerp.localIVA/100;
		
		$(js_data.fieldNames.footerPrecioTotal,context).text(pymerp.milFormat(precioTotal.toFixed(js_data.cambioDecimales)));
		$(js_data.fieldNames.footerCostoTotal,context).text(pymerp.milFormat(costoTotal.toFixed(js_data.cambioDecimales)));
		$(js_data.fieldNames.footerMargenTotal,context).text(pymerp.milFormat(margenTotal.toFixed(js_data.cambioDecimales)));
		
		if(total_costo > 0)
			$(js_data.fieldNames.footerMargenPorcentual,context).text(pymerp.milFormat(((precioTotal-costoTotal)*100/costoTotal).toFixed(2)));
		else
			$(js_data.fieldNames.footerMargenPorcentual,context).text('100');
			
		$(js_data.fieldNames.footerNeto,context).text(pymerp.milFormat(netoTotal.toFixed(pymerp.localDecimales)));
		$(js_data.fieldNames.footerIva,context).text(pymerp.milFormat(ivaTotal.toFixed(pymerp.localDecimales)));
		$(js_data.fieldNames.footerTotalPagar,context).text(pymerp.milFormat((ivaTotal+netoTotal).toFixed(pymerp.localDecimales)));
		
		if(setTotals){
			$(js_data.fieldNames.totalIvaDoc).val(ivaTotal.toFixed(pymerp.localDecimales));
			$(js_data.fieldNames.totalNetoDoc).val(netoTotal.toFixed(pymerp.localDecimales));
		}
		
	},
	
	totalRefresh: function(){
		js_data.simpleTotalRefresh(js_data.detailsContainer,true);
	},
	
	completeRefresh: function(){
		js_data.detailRefresh();
		js_data.totalRefresh();
	},
	
	getDisabledInput : function(input){
		return $(input).blur().attr('disabled',true).css({
			backgroundImage: 'url(images/ajax-loader.gif)',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'center right'
		});
	},
	
	setEnableInput: function(input){
		$(input).attr('disabled',false)
			.css({backgroundImage: ''})
			.val('');
	}
	
};