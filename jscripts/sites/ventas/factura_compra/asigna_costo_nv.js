var asigna_costos = {
    
    init: function(){
        asigna_costos.removeID();
        asigna_costos.add_item();
        asigna_costos.set_input_size();
        asigna_costos.verificar_monto_envio();
    },
    
    add_item: function(){
        $('.add_item').click(function(){
            var detail = asigna_costos.create_empty_detail();
            pymerp.init(detail);            
            asigna_costos.elimina_detalle(detail);
            asigna_costos.monto_event(detail);
            asigna_costos.edit_cuota_event(detail);
            $('#asig_detalles').append(detail);
            asigna_costos.notaventa_autocomplete(detail.find('.auto_notaventa'));
        });
    },
    
    create_empty_detail: function(){
        return $('.asig_copy').clone().removeClass('asig_copy');
    },
    
    set_input_size: function(){
        $('.auto_notaventa').attr('size','15');
        $('.det_monto').attr('size','15');
        $('.det_fecha').attr('size','15');
        $('.det_descripcion').attr('size','15');
    },
    
    elimina_detalle: function(detail){
        detail.find('.del_detail').click(function(){
            $(this).parent().parent().remove();
            asigna_costos.actualiza_total();
        });
    },
    
    //function autocomplete
    notaventa_autocomplete: function(field){
        //agrega el evento autocomplete al inputtext
        field.autocomplete(acnv.autocompleteUrl, 
            asigna_costos.autocomplete_options).result(asigna_costos.autocomplete_field);
    },
    
    autocomplete_options: {
		formatItem: function(row){
			return row[0]+" ("+row[2]+") "+row[1];
		},
		minChars: 2,
		width: 400
	},
    
    autocomplete_field: function(e,row){
        //crear selector de la linea
        var detailRow = $(this).parent().parent();
        //crear el div que contendrá la nota de venta
        var div = pymerp.getEditableBox(row[0],asigna_costos.autocomplete_edit).width($(this).width());
        //agregar un input hidden que contendrá el id de la nota de venta
        div.append($('<input>').attr('name','dc_nota_venta[]').attr('type','hidden').val(row[3]));
        //reemplazar el campo de texto por el div
        $(this).replaceWith(div);
        //asignar los datos de contrato
        asigna_costos.asigna_facturas(detailRow, row);
        asigna_costos.asigna_cuotas(detailRow, row);
    },
    
    autocomplete_edit: function(){
        detail = asigna_costos.create_empty_detail();
        var tr = $(this).parent().parent().parent();
        tr.find('.cb_factura_venta').remove();
        tr.find('.det_divide_cuotas').remove();
        tr.find('.det_cuotas_pendientes').remove();
        tr.find('.dm_contrato').val('0');
        
        asigna_costos.notaventa_autocomplete(detail.find('.auto_notaventa'));
        $(this).parent().replaceWith(detail.find('.auto_notaventa'));
    },
    
    monto_event: function(detail){
        detail.find('.det_monto').change(function() {
            if(pymerp.isNumeric($(this).val())) {
                asigna_costos.actualiza_total();
                asigna_costos.actualiza_monto_cuota(detail);
            } else {
                $(this).val('0');
            }
        });
    },
    
    actualiza_total: function(){
        var suma = 0;
        $('.det_monto','#asig_detalles').each(function(){
            suma += parseFloat($(this).val());
        });
        if(suma > acnv.montoTotal){
            $('#asig_total').css('color','red');
        } else {
            $('#asig_total').css('color','');
        }
        $('#asig_total').html(suma);
    },
    
    removeID: function(){
        $('.inputtext').each(function(){
            $(this).removeAttr('id');
        });
    },
    
    asigna_cuotas: function(detail, row){
        //crear el checkbox que permita distribuir en cuotas sólo si existe el contrato
        if(row[4] > 0){
            detail.find('.det_cbcuotas').append($('<input>').attr('class','det_divide_cuotas').attr('type','checkbox'));
            detail.find('.dm_contrato').val('1');
        }
        //guardar el numero de cuotas pendientes del contrato
        var cuotas_pendientes = parseInt(row[5]) - parseInt(row[6]);
        detail.find('.td_numero_cuotas').append($('<input>').attr('class','det_cuotas_pendientes').attr('type','hidden').val(cuotas_pendientes));
        //habilitar evento de checkbox
        asigna_costos.distribuye_cuotas(detail);
    },
    
    asigna_facturas: function(detail, row){
        //Crear Checkbox que seleccionará la factura de venta
        detail.find('.det_factura_venta').append($('<select>',{
            name:       'dc_factura_venta[]',
            class:      'inputtext cb_factura_venta',
            style:      'width: 150px'
        }));
        
        detail.find('.cb_factura_venta').append($('<option>', {
            value: 0,
            text: 'No asignar factura de venta'
        }));
        
        var facturas = JSON.parse(row[7]);
        $.each(facturas, function(i, item){
            detail.find('.cb_factura_venta').append($('<option>',{
                value:  item.dc_factura,
                text:   item.dq_folio + ' (' + item.dq_factura + ')'
            }));
        });
        
    },
    
    distribuye_cuotas: function(detail){
        detail.find('.det_divide_cuotas').click(function(){
            if($(this).is(':checked')){
                //cuotas_pendientes = detail.find('.det_cuotas_pendientes').val();
                detail.find('.det_cuotas').removeAttr('readonly').val(cuotas_pendientes);
                asigna_costos.actualiza_monto_cuota(detail);
            } else {
                detail.find('.det_cuotas').attr('readonly','readonly').val('1');
                asigna_costos.actualiza_monto_cuota(detail);
            }
        });
    },
    
    actualiza_monto_cuota: function(detail){
        var monto = parseFloat(detail.find('.det_monto').val());
        var cuotas = parseInt(detail.find('.det_cuotas').val());
        var total = parseFloat(monto / cuotas);
        if(pymerp.isNumeric(total)){
            detail.find('.det_monto_cuota').html(total.toFixed(pymerp.localDecimales));
        } else {
            detail.find('.det_monto_cuota').html('0')
        }
    },
    
    edit_cuota_event: function(detail){
        detail.find('.det_cuotas').change(function(){
            var maximoCuotas = detail.find('.det_cuotas_pendientes').val();
            if($(this).val() < 1){
                $(this).val('1');
                alert('No se puede asignar 0 cuotas o un valor negativo');
            }
            if($(this).val() > maximoCuotas){
                $(this).val('1');
                asigna_costos.actualiza_monto_cuota(detail);
                alert('El numero de cuotas de este contrato es ' + maximoCuotas);
            } else {
                asigna_costos.actualiza_monto_cuota(detail);
            }
        });
    },
    
    verificar_monto_envio: function(){
        $('.btn_enviar').click(function( event ){
            var total_asignado = parseFloat($('#asig_total').text());
            if(acnv.montoTotal < total_asignado){
                event.preventDefault();
                alert('Se ha exedido el monto total de la factura de compra');
            }
        });
    }
    
};