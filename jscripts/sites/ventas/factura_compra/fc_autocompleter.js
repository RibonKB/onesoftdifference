var facturaCompraAutoCompleter = {
    init: function(){
        facturaCompraAutoCompleter.fc_autocompleter($('#dq_factura_compra'));
    },
    
    fc_autocompleter: function(field){
        field.autocomplete(fc_auto_url,
            facturaCompraAutoCompleter.autocompleter_options).result(facturaCompraAutoCompleter.autocompleter_result);
    },
    
    autocompleter_options: {
        formatItem: function(row){
			return row[0]+" ("+row[1]+") "+row[2];
		},
		minChars: 2,
		width: 400
    },
    
    autocompleter_result: function(e, row){
        $(this).val(row[0]);
        $(this).parent().parent().find('#dc_factura').val(row[3]);
        
    }
};
facturaCompraAutoCompleter.init();