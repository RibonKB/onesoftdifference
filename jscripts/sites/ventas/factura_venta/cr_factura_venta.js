$.extend(js_data,js_data = {
	
	prodListFake: $('#prod_list_fake'),
	fakeDetailContainer: $('#fake_prods'),
	
	invalidSeriesStyle: {
		backgroundColor:'#FFFFBF'
	},
	
	fv_fieldNames: {
		requiredSeries: '.prod_series[required]',
		notaVenta: '#fv_nota_venta',
		ordenServicio: '#fv_orden_servicio',
		id_notaVenta: ':hidden[name=fv_id_nota_venta]',
		id_ordenServicio: ':hidden[name=fv_id_orden_servicio]',
		tipoImpresion: ':input[name=fv_tipo_impresion]',
		ejecutivo: '#fv_ejecutivo',
		extDespachado: '.despach_cant',
		extRecepcionado: '.recep_cant',
		extFacturado: '.fact_cant'
	},
	
	fv_messages: {
		fakeTotalOverflow: 'Si indica una glosa de facturación los valores totales deben cuadrar',
		errorEmptyDetail: 'No ha agregado productos a la factura',
		errorInvalidSeries: 'Debe ingresar todas las series de los productos que sean seriados',
		errorNotaVentaNotFound: 'No se ha encontrado la nota de venta especificada, intentelo denuevo',
		errorNotaVentaNoFacturable: 'La nota de venta seleccionada no es facturable.',
		confirmDetailToDelete: 'Se eliminarán todos los detalles que haya agregado\n¿Continuar?',
		errorNotaVentaEmptyDetail: 'La nota de venta especificada no posee detalle disponible para facturar',
		alertNotaVentaFacturada: 'La nota de venta ya a sido completamente facturada',
		confirmMantenerDetalles: '¿Desea mantener los detalles agregados?',
		errorOrdenServicioNotFound: 'No se ha encontrado la orden de servicio o esta no es facturable o no se ha cerrado',
		errorOrdenServicioEmptyDetail: 'La orden de servicio es facturable, pero no se ha especificado un detalle de facturación'
	},
	
	init: function(){
		js_data.baseInit();
		
		$.extend(js_data.fieldNames,{
			footerPrecioTotal: ".total",
			footerCostoTotal: ".total_costo",
			footerMargenTotal: ".total_margen",
			footerMargenPorcentual: ".total_margen_p",
			footerNeto: ".total_neto",
			footerIva: ".total_iva",
			footerTotalPagar: ".total_pagar"
		});
		
		js_data.initFake();
		
		js_data.jForm.submit(js_data.formSubmit);
		
		$(js_data.fv_fieldNames.notaVenta,js_data.jForm)
		.autocomplete(js_data.AC_notaVentaUrl,js_data.AC_options)
		.result(js_data.AC_notaVentaResult);
		
		$(js_data.fv_fieldNames.ordenServicio,js_data.jForm)
		.autocomplete(js_data.AC_ordenServicioUrl,js_data.AC_options)
		.result(js_data.AC_ordenServicioResult);
		
		
	},
	
	initFake: function(){
		js_data.setEmptyFakeDetail();
		
		
	},
	
	setEmptyFakeDetail: function(){
		//CODE-HERE
	},
	
	addEmptyFakeDetail: function(){
		//CODE-HERE
	},
	
	formSubmit: function(e){
		e.preventDefault();
		
		//Comprobar que los totales del detalle normal y glosa sean equivalentes
		//Esto solo si se han agregado detalles glosa
		if($(js_data.detailRow,js_data.prodListFake).size()){
			if($(js_data.fieldNames.footerPrecioTotal,js_data.prodListFake).text() != $(js_data.fieldNames.footerPrecioTotal,js_data.prodList).text()){
				pymerp.showError(js_data.fv_messages.fakeTotalOverflow);
				js_data.prodListFake.parent().addClass('invalid');
				return;
			}
		}
		
		//Comprobar que no se envíe la factura sin especificar ningún detalle
		if($(js_data.detailRow,js_data.prodList).size() == 0){
			pymerp.showError(js_data.fv_messages.errorEmptyDetail);
			return;
		}
		
		//Comprobar que se hayan incluido todas las series de los productos con serie obligatoria
		var dets = $(js_data.fv_fieldNames.requiredSeries,js_data.prodList);
		dets.parent().parent().find('td').removeAttr('style');
		var x = dets.size();
		for(var j=0; j<x; j++){
			var cant = $(dets[j]).parent().parent().find(js_data.fieldNames.cantidad).val();
			var val = $(dets[j]).val();
			if(val == ''){
				pymerp.showError(js_data.fv_messages.errorInvalidSeries);
				$(dets[j]).parent().parent().find('td').css(js_data.invalidSeriesStyle);
				return;
			}
			val = val.split(',').length; 
			if(val < cant){
				pymerp.showError(js_data.fv_messages.errorInvalidSeries);
				$(dets[j]).parent().parent().find('td').css(js_data.invalidSeriesStyle);
				return;
			}
		}
		pymerp.confirmEnviarForm(js_data.jForm,js_data.jForm.prev());
	},
	
	completeFakeRefresh: function(){
		js_data.simpleDetailRefresh(js_data.fakeDetailContainer);
		js_data.simpleTotalRefresh(js_data.fakeDetailContainer,false);
	},
	
	AC_notaVentaUrl: 'sites/proc/autocompleter/nota_venta_strict.php',
	AC_notaVentaDataUrl: 'sites/ventas/factura_venta/proc/get_nota_venta_detalle.php',
	
	AC_notaVentaResult: function(e,row){
		if(!parseInt($(this).val())) return;
		
		var input = js_data.getDisabledInput(this);
		
		$.getJSON(js_data.AC_notaVentaDataUrl,{ number: encodeURIComponent(input.val()) },function(data){
			if(data == '<not-found>'){
				js_data.setEnableInput(input);
				pymerp.showError(js_data.fv_messages.errorNotaVentaNotFound);
				return;
			}
			
			if(data == '<not-facturable>'){
				js_data.setEnableInput(input);
				show_error(js_data.fv_messages.errorNotaVentaNoFacturable);
				return;
			}
			
			if($(js_data.detailRow,js_data.prodList).size() > 0){
				if(confirm(js_data.fv_messages.confirmDetailToDelete)){
					$(js_data.detailRow,js_data.prodList).remove();
				}else{
					js_data.setEnableInput(input);
					return;
				}
			}
			
			if(data[0] == '<empty>'){
				pymerp.showError(js_data.fv_messages.errorNotaVentaEmptyDetail);
				js_data.setEnableInput(input);
				return;
			}
			
			pymerp.disableButton($(js_data.addDetailButton,js_data.jForm));
			$(js_data.fv_fieldNames.ordenServicio,js_data.jForm).attr('disabled',true);
			$(js_data.fv_fieldNames.id_ordenServicio,js_data.jForm).val(0);
			
			var detalle = data[0];
			var dc_nota_venta = data[1].dc_nota_venta;
			var dq_cambio = data[1].dq_cambio;
			var dm_al_dia = data[1].dm_al_dia;
			var dm_costeable = data[1].dm_costeable;
			var dc_guias_fisicas = data[1].dc_guias_fisicas;
			var dq_guias = data[1].dq_guias;
			$(js_data.fv_fieldNames.ejecutivo,js_data.jForm).val(data[1].dc_ejecutivo);
			$(js_data.fv_fieldNames.tipoImpresion,js_data.jForm).filter('[value='+data[1].dm_tipo_impresion_factura+']').click();
			var factured = false;
			var cambio_dia = $(':hidden[name=cambio'+data[1].dc_tipo_cambio+']');
			
			cambio_dia = parseFloat(cambio_dia.val());
			
			for(i in detalle){
				
				detalle[i].dq_cantidad -= detalle[i].dc_facturada;
				if(detalle[i].dc_producto != null && detalle[i].dq_cantidad > 0){
				
					if(dm_al_dia != 0 && cambio_dia){
						detalle[i].dq_precio_venta = (detalle[i].dq_precio_venta/dq_cambio)*cambio_dia;
						detalle[i].dq_precio_compra = (detalle[i].dq_precio_compra/dq_cambio)*cambio_dia;
					}
					
					var dq_cantidad = detalle[i].dq_cantidad;
					var dc_despachada = detalle[i].dc_despachada
					var dc_recepcionada = detalle[i].dc_recepcionada;
					var dc_facturada = detalle[i].dc_facturada;
					var dm_tipo = detalle[i].dm_tipo;
					
					var fvDetail = js_data.getBaseDetail(detalle[i]);
					
					fvDetail.find(js_data.fieldNames.codigo)
						.removeClass('searchbtn').addClass('inputtext')
						.attr('readonly',true)
						.val(detalle[i].dg_codigo);
					
					fvDetail
						.addClass('strict')
						.children('td').first()
						.append(
							$('<input>')
							.multiAttr({
								type:'hidden',
								name:'id_detail[]',
								value: detalle[i].dc_nota_venta_detalle
							})
						);
					
					var maxValue = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(dq_cantidad);
					
					fvDetail.find(js_data.fv_fieldNames.extDespachado).val(dc_despachada);
					fvDetail.find(js_data.fv_fieldNames.extRecepcionado).val(dc_recepcionada);
					fvDetail.find(js_data.fv_fieldNames.extFacturado).val(dc_facturada);
					fvDetail.find(js_data.fieldNames.cantidad).after(maxValue);
					
					fvDetail.appendTo(js_data.prodList);
					
					factured = true;
					
					if(dm_costeable == 1 && dm_tipo == 0){
						js_data.getBaseFakeDetail(detalle[i]).appendTo(js_data.prodListFake).find(js_data.fieldNames.cantidad).trigger('change');
					}
				
				}//END IF producto NOT NULL
			
			}//END FOR i IN detalle
			
			if(dc_guias_fisicas > 0){
				js_data.obligarFacturaciónCompleta();
			}
			
			if(!factured){
				js_data.setEnableInput(input);
				pymerp.showAviso(js_data.fv_messages.alertNotaVentaFacturada);
				$(js_data.fv_fieldNames.ordenServicio).attr('disabled',false);
				pymerp.enableButton($(js_data.addDetailButton,js_data.jForm));
				return;
			}
			
			js_data.completeRefresh();
			
			js_data.nvInput = input.clone(true);
			js_data.nvDiv = js_data.getEditableDiv(input.val(),js_data.editNotaVenta)
								.width(input.witdh())
								.replaceAll(input);
			
		});
	},
	
	editNotaVenta: function(){
		js_data.nvInput
		.autocomplete(js_data.AC_notaVentaUrl,js_data.AC_options)
		.result(js_data.AC_notaVentaResult)
		.replaceAll(js_data.nvDiv);
		
		$(js_data.fv_fieldNames.id_notaVenta).val(0);
		
		$(js_data.fv_fieldNames.ordenServicio).attr('disabled',false);
		
		if(confirm(js_data.fv_messages.confirmMantenerDetalles)){
			$(js_data.fieldNames.codigo,js_data.prodList)
				.filter('[readonly]').attr('readonly',false)
				.autocomplete(js_data.AC_prodCode,js_data.AC_options)
				.result(js_data.AC_prodResult);
		}else{
			$('.strict',js_data.prodList).remove();
		}
		
		pymerp.enableButton($(js_data.addDetailButton,js_data.jForm));
	},
	
	AC_ordenServicioUrl: 'sites/proc/autocompleter/orden_servicio_strict.php',
	AC_notaVentaDataUrl: 'sites/ventas/factura_venta/proc/get_nota_venta_detalle.php',
	
	AC_ordenServicioResult: function(e,row){
		if(!parseInt($(this).val())) return;
		
		var input = js_data.getDisabledInput(this);
		
		if(data == '<not-found>'){
			js_data.setEnabled(input);
			pymerp.showError(js_data.fv_messages.errorNotaVentaNotFound);
			return;
		}
		
		if(data == '<empty>'){
			js_data.setEnabled(input);
			pymerp.showError(js_data.fv_messages.errorOrdenServicioEmptyDetail);
			return;
		}
		
		if($(js_data.detailRow,js_data.prodList).size() > 0){
			if(confirm(js_data.fv_messages.confirmDetailToDelete)){
				$(js_data.detailRow,js_data.prodList).remove();
			}else{
				js_data.setEnabled(input);
				return;
			}
		}
		
		pymerp.disableButton($(js_data.addDetailButton,js_data.jForm));
		$(js_data.fv_fieldNames.notaVenta,js_data.jForm).attr('disabled',true);
		$(js_data.fv_fieldNames.id_notaVenta,js_data.jform).val(0);
		
		var detalle = data[0];
		var fake = data[1];
		
		var totalVentaFake = 0.0;
		var totalCostoFake = 0.0;
		
		for(i in detalle){
			totalVentaFake += parseFloat(detalle[i].dq_precio_venta)*detalle[i].dq_cantidad;
			totalCostoFake += parseFloat(detalle[i].dq_precio_compra)*detalle[i].dq_cantidad;
			
			var fvDetail = js_data.getBaseDetail(detalle[i]);
			fvDetail.find('.del_detail').remove();
			fvDetail.addClass('strict').appendTo(js_data.prodList);
		}
		
		js_data.completeRefresh();
		
		if(fake.dg_glosa_facturacion != ''){
			var fvFakeDetail = js_data.getEmptyFakeDetail();
			
			fvFakeDetail.find(js_data.fieldNames.descripcion).val(fake.dg_glosa_facturacion);
			fvFakeDetail.find(js_data.fieldNames.precio).val(totalVentaFake);
			fvFakeDetail.find(js_data.fieldNames.costo).val(totalCostoFake);
			fvFakeDetail.appendTo(js_data.prodListFake);
			
			js_data.completeFakeRefresh();
		}
		
		js_data.osInput = input.clone(true);
		js_data.osDiv = js_data.getEditableDiv(input.val(),js_data.editOrdenServicio)
						.width(input.witdh())
						.replaceAll(input);
		
	},
	
	editOrdenServicio: function(){
		js_data.osInput
		.autocomplete(js_data.AC_ordenServicioUrl,js_data.AC_options)
		.result(js_data.AC_ordenServicioResult)
		.replaceAll(js_data.osDiv);
		
		$(js_data.fv_fieldNames.id_ordenServicio).val(0);
		
		$(js_data.fv_fieldNames.ordenServicio).attr('disabled',false);
		
		$('.strict',js_data.prodList).remove();
		
		pymerp.enableButton($(js_data.addDetailButton,js_data.jForm));
	},
	
	getBaseDetail: function(detail){
		var tr = js_data.getEmptyDetail();
		tr.find(js_data.fieldNames.descripcion).val(detail.dg_descripcion);
		tr.find(js_data.fieldNames.proveedor).val(detail.dc_proveedor);
		tr.find(js_data.fieldNames.cantidad).val(detail.dq_cantidad);
		tr.find(js_data.fieldNames.precio).val(detail.dq_precio_venta);
		tr.find(js_data.fieldNames.costo).val(detail.dq_precio_compra);
		return tr;
	},
	
	getEmptyFakeDetail: function(){
		//CODE-HERE
	},
	
	getBaseFakeDetail: function(){
		//CODE-HERE
	},
	
	obligarFacturaciónCompleta: function(){
		//CODE-HERE
	}
	
});