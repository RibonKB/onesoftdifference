var js_data = {
	
	showButton: 'images/addbtn.png',
	hideButton: 'images/minusbtn.png',
    resultados: $('#src_informe_margenes_res'),
	activeData: undefined,
    
    gruposHechos: {},
	
	init: function(){
        pymerp.init(js_data.resultados);
        js_data.initEnlaceDetalle();
        js_data.initDetallesAgrupados();
		$('#totales_table').tableExport().tableAdjust(10);
		
		//Se agregan nuevas funcionalidades respecto a la centralizacion
		$('#btn_centralizar').click(js_data.centralizar);
		js_data.select_all_init();
		
		
	},
    
    initDetalles: function(){
        pymerp.init('#detalles_cargados');
        $('#tabs').data('tabs').click(1);
        $('#detalles_table').tableExport().tableAdjust(15, true, 1100);
        pymerp.hideLoader();
    },
            
    initEnlaceDetalle: function(){
      $('.enlace_carga_detalle',js_data.resultados).click(function(e){
        e.preventDefault();
        pymerp.showLoader();
        pymerp.loadFile($(this).attr('href'),'#detalles_cargados',undefined,js_data.initDetalles);
      });
    },
    
    initDetallesAgrupados: function(){
      $('.carga_agrupado',js_data.resultados).each(function(){
        var id = $(this).attr('id');
        js_data.gruposHechos[id] = false;
      }).click(function(e){
        var id = $(this).attr('id');
        if(js_data.gruposHechos[id]){
          return;
        }
        js_data.gruposHechos[id] = true;
        var href = $(this).attr('href');
        var data_id = id.replace('carga_','data_');
        pymerp.showLoader();
        pymerp.loadFile(href, '#'+data_id, undefined, function(){
          $('table',$('#'+data_id)).tableExport().tableAdjust(10);
          pymerp.hideLoader();
        });
      });
      
    },
	
	//Agregado de nuevas functiones
	
	select_all_init: function(){
		$('.cb_select_all').click(function(){
			if($(this).is(':checked')){
				$(this).closest('table').find('.cb_documento').attr('checked', true);
			} else {
				$(this).closest('table').find('.cb_documento').attr('checked', false);
			}
		});
	},
	
	centralizar: function(){
		if(confirm('¿Est\xE1 seguro que desea continuar?')){
			var data = $('tbody :checkbox:checked');
			js_data.activeData = data;
			pymerp.loadOverlay(js_data.urlCentralizacion,data.serialize());
		}
	},
	
	deleteActive: function(){
		js_data.activeData.each(function(index, element) {
            $(this).closest('input').remove();
        });
	}
	
	
    
};