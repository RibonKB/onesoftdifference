var FacturaElectronicaLeasing = {
    
    init: function(){
        $('#dm_leasing').click(function(){
            if($(this).attr('checked')){
                $('.div_leasing').removeClass('hidden');
                $('#dc_cod_receptor').attr('required','required');
                $('#df_fecha_odc').attr('required','required');
            } else {
                $('.div_leasing').addClass('hidden');
                $('#dc_cod_receptor').val('');
                $('#df_fecha_odc').val('');
                $('#dc_cod_receptor').removeAttr('required');
                $('#df_fecha_odc').removeAttr('required');
            }
        });
    }
    
};