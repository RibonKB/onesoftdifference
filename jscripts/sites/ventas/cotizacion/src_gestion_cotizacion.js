var js_data = {
	init: function(){
		$('#gestion_estado').change(function(){
			$('input[name=gestion_cambio]').val($(this).val());
		});
		
		$("#gestion_fecha").dateinput({
			lang:'es',
			firstDay:1,
			format:'dd/mm/yyyy',
			selectors:true,
			initialValue:0,
			yearRange:[-80,80]
		});
		
		$('<iframe>').attr('name','callframe').addClass('hidden').appendTo('#genOverlay');
		
		$('#dc_contacto').change(js_data.ch_contacto);
	},
	
	ch_contacto: function(){
		var contacto = $(this).attr('disabled',true).val();
		var enabler = window.setTimeout(js_data.ena_contacto,2500);
		$.getJSON('sites/ventas/factura_venta/proc/get_contacto_data.php',{id:contacto},function(data){
			if(data == '<not-found>'){
				show_error('No se ha podido encontrar la información del contacto');
				return;
			}
			$('#contact_data').html('');
			window.clearInterval(enabler);
			js_data.ena_contacto();
			//<a target='callframe' class='right' href='http://192.168.0.202/phone/asterisk_call_trigger.php?{$query_data}'>
			var phone = $('<a>').multiAttr({
				href:'http://192.168.0.202/phone/asterisk_call_trigger.php?call='+data.dg_fono+'&ext='+data.anexo+'&razon='+data.dg_razon,
				target: 'callframe'
			}).addClass('right').html('<img src="images/call.png" alt="[C]" title="Llamar" />');
			var movil = $('<a>').multiAttr({
				href:'http://192.168.0.202/phone/asterisk_call_trigger.php?call='+data.dg_movil+'&ext='+data.anexo+'&razon='+data.dg_razon,
				target: 'callframe'
			}).addClass('right').html('<img src="images/call.png" alt="[C]" title="Llamar" />');
			var mail = $('<a>').multiAttr({
				href: 'mailto:'+data.dg_email
			}).addClass('right').html('<img src="images/mail.png" alt="[@]" title="Enviar Correo" />');
			$('#contact_data').css({backgroundColor:'#FFF',lineHeight:'25px',padding:'5px',border:'1px solid #BBB'});
			$('<div>').text('Información del contacto').addClass('info').appendTo('#contact_data');
			$('<div>').append('Nombre: ').append($('<b>').text(data.dg_contacto)).appendTo('#contact_data');
			$('<div>').append(phone).append('Telefono: ').append($('<b>').text(data.dg_fono)).appendTo('#contact_data');
			$('<div>').append(movil).append('Movil: ').append($('<b>').text(data.dg_movil)).appendTo('#contact_data');
			$('<div>').append(mail).append('e-mail: ').append($('<b>').text(data.dg_email)).appendTo('#contact_data');
		});
	},
	ena_contacto: function(){
		$('#dc_contacto').attr('disabled',false);
	}
};
js_data.init();