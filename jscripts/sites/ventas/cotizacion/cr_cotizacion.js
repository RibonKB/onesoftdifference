$.extend(js_data,{
	
	opInput: null,
	opDiv: null,
	
	cot_fieldNames: {
		oportunidad: '#cot_oportunidad',
		tipo_cambio: '#prod_tipo_cambio',
		cambio: '#cot_cambio',
		ch_cliente: '#cli_switch',
		id_oportunidad: ':hidden[name=dc_oportunidad]'
	},
	
	cot_messages: {
		noDetailsFound: "No ha agregado productos a la cotización",
		oportunidadNotFound: "La oportunidad ingresada es inválida o no existe"
	},
	
	AC_prodResult: function(e,row){
		var tr = $(this).parent().parent();
		js_data.AC_baseProdResult(tr,row);
		
		js_data.detailRefresh();
		js_data.totalRefresh();
	},
	
	init: function(){
		js_data.baseInit();
		
		js_data.jForm.submit(js_data.formSubmit);
		
		js_data.fieldNames.proveedor = '.prod_proveedor';
		js_data.fieldNames.cambio = js_data.cot_fieldNames.cambio;
		
		$(js_data.cot_fieldNames.oportunidad).change(js_data.opChange);
		
		$(js_data.cot_fieldNames.tipo_cambio).change(js_data.exchangeOnChange);
		$(js_data.cot_fieldNames.cambio).change(js_data.exchangeTextOnChange);
		
		$(js_data.cot_fieldNames.ch_cliente).click(function(){
			pymerp.loadOverlay('sites/ventas/switch_cliente.php');
		});
		
		var opInput = $(js_data.cot_fieldNames.oportunidad);
		if(opInput.val() != ''){
			opInput.trigger('change');
		}
		
		js_data.checkBaseDetails();
		
	},
	
	formSubmit: function(e){
		e.preventDefault();
		/*if(!js_data.checkCodigos()){
			pymerp.showError(js_data.nv_messages.badCodesFound);
			$(js_data.prodList).parent().addClass('invalid');
			return;
		}*/
		if($('#prod_list tr').size() == 0){
			pymerp.showAviso(js_data.cot_messages.noDetailsFound);
			return;
		}
		pymerp.confirmEnviarForm(js_data.jForm,js_data.jForm.prev());
	},
	
	AC_oportunidadDataUrl: 'sites/ventas/cotizacion/proc/get_oportunidad_detalle.php',
	
	opChange: function(){
		js_data.opInput = $(this).clone();
		var input = js_data.getDisabledInput(this);
	
		$.getJSON(js_data.AC_oportunidadDataUrl,{dq_oportunidad: input.val()},function(data){
			if(data == '<not-found>'){
				js_data.setEnableInput(input);
				pymerp.showError(js_data.cot_messages.oportunidadNotFound);
				return;
			}
			
			js_data.opDiv = js_data.getEditableDiv(input.val(),js_data.editOportunidad).width(input.width()).replaceAll(input);
			$(js_data.cot_fieldNames.id_oportunidad).val(data);
			
		});
	},
	
	detallesBase: null,
	checkBaseDetails: function(){
		if(js_data.detallesBase != null){
			$(js_data.nv_fieldNames.tipo_cambio).trigger('change');
			js_data.setDetailFromCot(js_data.detallesBase);
		}
	},
	
	setDetail: function(detail){
		for(i in detail){
			js_data.addDetail(detail[i]);
		}
	},
	
	addDetail: function(detail){
		var tr = js_data.getEmptyDetail();
		tr.find(js_data.fieldNames.descripcion).val(detail.dg_descripcion);
		tr.find(js_data.fieldNames.proveedor).val(detail.dc_proveedor);
		tr.find(js_data.fieldNames.cantidad).val(detail.dq_cantidad);
		tr.find(js_data.fieldNames.precio).val(detail.dq_precio_venta);
		tr.find(js_data.fieldNames.costo).val(detail.dq_precio_compra);
		tr.appendTo(js_data.prodList);
	},
	
	editOportunidad: function(){
		js_data.opInput
		.change(js_data.opChange)
		.replaceAll(js_data.cotDiv);

		$(js_data.cot_fieldNames.id_oportunidad).val(0);
	}
	
});