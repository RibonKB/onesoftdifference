var js_data = {
  
    form: $('.ed_form'),
    emptyDetail: $('#detalle_template tr'),
    bodyDetail: null,
    itemId: 0,
    init: function(){
      js_data.bodyDetail = $('#detalle_list',js_data.form);
      $('#add_detalle',js_data.form).click(js_data.delEmptyDetail);
      var templateTable = $('#detalle_template');
      $('.del_detail',templateTable).click(js_data.delDetail);      
    },
    
    addDetalle: function(){
      var empty = js_data.emptyDetail.clone(true);
      $('.item_id',empty).val(js_data.itemId++);
      js_data.bodyDetail.append(empty);
    },
            
    delEmptyDetail: function(){
      $(this).unbind('click').click(js_data.addDetalle);
      $(this).click();
    },
            
    delDetail: function(){
      $(this).parent().parent().remove();
    }
            
};