var js_data = {
	
	messages: {
		errorValidacion : "Debe completar todos los datos marcados para continuar con la consulta del informe"
	},
	
	fcForm: $('#load_informe'),
	
	init: function(){
		js_data.fcForm.submit(js_data.loadForecast);
	},
	
	loadForecast: function(e){
		e.preventDefault();
		
		if(!validarForm(js_data.fcForm)){
			show_error(js_data.messages.errorValidacion);
			return;
		}
		
		var data = js_data.fcForm.serialize();
		var action = js_data.fcForm.attr('action');
		
		show_loader();
		loadFile(action,'#result_informe',data,function(){
			hide_loader();
			js_data.initForecast();
		});
	},
	
	initForecast: function(){
		$(".bicolor_tab tr:even td").css({"backgroundColor": "#E5E5E5"});
		
		js_data.setEjecutivos();
		js_data.loadPointPlot();
		js_data.loadPiePlot();
		
		js_data.initDetailedTable();
		
	},
	
	setEjecutivos: function(){
		var selct = $('<select>').multiAttr({
			'multiple':'multiple'
		});
		
		for(i in js_data.periodPlotData){
			$('<option>')
				.attr('selected',true)
				.val(i)
				.text(js_data.periodPlotData[i].nombre_completo)
				.attr('id','ex_list')
				.appendTo(selct);
		}
		
		selct.appendTo('#ex_list').multiSelect({
			selectAll: false,
			noneSelected: "Seleccione ejecutivos",
			oneOrMoreSelected: "% seleccionado(s)"
		});
	},
	
	loadPointPlot: function(){
		var series = [];
		$('input:checkbox:checked',$('#ex_list')).each(function(){
			var id = $(this).val();
			var data = {}
			data.label = js_data.periodPlotData[id].nombre_completo;
			data.data = js_data.periodPlotData[id].puntos;
			
			series.push(data);
		});
		
		$.plot($("#grafico_puntos"),series,{
			series: {
				lines: { show: true },
				points: { show: true }
			},
			legend: { position: 'sw' },
			xaxis: {
				tickSize: 1,
				tickDecimals: 0
			}
			
		});
	},
	
	loadPiePlot: function(){
		
		js_data.setPiePlot('#grafico_torta', js_data.piePlotData,0);
		js_data.setPiePlot('#grafico_torta_televenta', js_data.piePlotTeleventa,0);
		js_data.setPiePlot('#grafico_torta_linea_negocio', js_data.piePlotLineaNegocio,0.01);
		
	},
	
	setPiePlot: function(container, data, toOther){
		$.plot($(container), data, 
		{
			series: {
            pie: { 
                show: true,
                combine: {
                    threshold: toOther,
					label: 'Otro'
                },
                radius: 1,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
						var mount = mil_format(series.data[0][1]+'');
                        return '<div title="$ '+mount+'" style="font-size:8pt;text-align:center;padding:2px;color:white;">$ '+mount+'</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
			},
			legend: {
				show: true
			}
		});
	},
	
	showTooltip: function(x, y, contents) {
        $('<div id="plot_tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#fee',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    },
	
	getThead: function(data){
		var thead = $('<thead>');
		for(i in data){
			$('<th>').text(data[i]).appendTo(thead);
		}
		return thead;
	},
	
	initDetailedTable: function(){
		$("tr:even td",$('#detail_data_table')).css({"backgroundColor": "#E5E5E5"});
	},
	
	filterDetails: function(e){
		e.preventDefault();
		var clss = $(this).attr('class');
		var data_table = $('#detail_data_table');
		$('tr',data_table).hide();
		$('td.'+clss,data_table).parent('tr').show();
	}
	
};
js_data.init();