$.extend(js_data,{
	
	ex_fieldNames: {
		prod_info: '#prod_info',
		prods: '#prods'
	},
	
	exchangeOnChange: function(){
		var indice = this.selectedIndex;
		if(indice == 0){
			$(js_data.ex_fieldNames.prod_info).show();
			$(js_data.ex_fieldNames.prods).hide();
		}else{
			$(js_data.ex_fieldNames.prod_info).hide();
			$(js_data.ex_fieldNames.prods).show();
			var tc = this.options[indice].innerHTML.split("|");
			js_data.cambio = pymerp.toNumber(tc[1]);
			$(js_data.fieldNames.cambio).val(js_data.cambio);
			js_data.cambioDecimales = $(":hidden[name=decimal"+$(this).val()+"]").val();
			js_data.detailRefresh();
			js_data.totalRefresh();
		}
	},
	
	exchangeTextOnChange: function(){
		var nv_cambio = $(this).val();
		if(!parseFloat(nv_cambio)){
			nv_cambio = 1.0;
			$(this).val(1);
		}
		js_data.cambio = nv_cambio;
		js_data.detailRefresh();
		js_data.totalRefresh();
	}
});