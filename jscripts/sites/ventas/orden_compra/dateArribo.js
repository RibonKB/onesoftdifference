var dateArribo = {
	fechaArribo: undefined,

	init: function(){
		dateArribo.calendarAction();
	},

	calendarAction: function(){
		$('.set_fecha_arribo').click(function(){
			dateArribo.inputFechaArribo = $(this).parent().find('.fecha_arribo');
			pymerp.loadOverlay(setFechaArriboProducto, 'fecha='+dateArribo.inputFechaArribo.val());
		});
	},

	//función llamada desde la vista dateSelect
	ds_asignarFecha: function(){
		$('.btnAsignarFecha').click(function(event){
			event.preventDefault();
			dateArribo.inputFechaArribo.val($(this).parent().parent().find('.inputtext').val());
			dateArribo.inputFechaArribo = undefined;
			$(this).parents('.overlay').remove();
		});

	}

};
