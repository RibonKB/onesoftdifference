var js_data = {
	
	messages: {
		errorValidacion : "Debe completar todos los datos marcados para continuar con la consulta de Pipeline"
	},
	
	fcForm: $('#load_pipeline'),
	
	init: function(){
		js_data.fcForm.submit(js_data.loadForecast);
	},
	
	loadForecast: function(e){
		e.preventDefault();
		
		if(!validarForm(js_data.fcForm)){
			show_error(js_data.messages.errorValidacion);
			return;
		}
		
		var data = js_data.fcForm.serialize();
		var action = js_data.fcForm.attr('action');
		
		show_loader();
		loadFile(action,'#result_pipeline',data,function(){
			hide_loader();
			js_data.initForecast();
		});
	},
	
	initForecast: function(){
		$(".bicolor_tab tr:even td").css({"backgroundColor": "#E5E5E5"});
		js_data.setEjecutivos();
		
		js_data.loadPointPlot();
		js_data.loadPiePlot();
		
		js_data.initAcumulativeTables();
		js_data.initDetailedTable();
	},
	
	setEjecutivos: function(){
		var selct = $('<select>').multiAttr({
			'multiple':'multiple'
		});
		
		for(i in js_data.periodPlotData){
			$('<option>')
				.attr('selected',true)
				.val(i)
				.text(js_data.periodPlotData[i].nombre_completo)
				.attr('id','ex_list')
				.appendTo(selct);
		}
		
		selct.appendTo('#ex_list').multiSelect({
			selectAll: false,
			noneSelected: "Seleccione ejecutivos",
			oneOrMoreSelected: "% seleccionado(s)"
		});
		$('#ex_list').hide();
	},
	
	loadPointPlot: function(){
		var series = [];
		$('input:checkbox:checked',$('#ex_list')).each(function(){
			var id = $(this).val();
			var data = {}
			data.label = js_data.periodPlotData[id].nombre_completo;
			data.data = js_data.periodPlotData[id].puntos;
			
			series.push(data);
		});
		
		$.plot($("#grafico_puntos"),series,{
			series: {
				lines: { show: true },
				points: { show: true }
			},
			legend: { position: 'sw' },
			xaxis: {
				tickSize: 1,
				tickDecimals: 0
			}
			
		});
	},
	
	loadPiePlot: function(){
		js_data.setPiePlot('#grafico_torta', js_data.piePlotData,0);
		//js_data.setPiePlot('#grafico_torta_estado', js_data.piePlotStatus,0);
		js_data.setPiePlot('#grafico_torta_marca', js_data.piePlotMarca,0.01);
		js_data.setPiePlot('#grafico_torta_linea', js_data.piePlotLinea,0.01);
	},
	
	setPiePlot: function(container, data, toOther){
		$.plot($(container), data, 
		{
			series: {
            pie: { 
                show: true,
                combine: {
                    threshold: toOther,
					label: 'Otro'
                },
                radius: 1,
                label: {
                    show: true,
                    radius: 1,
                    formatter: function(label, series){
						var mount = series.data[0][1]+'';
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">$ '+mil_format(mount)+'</div>';
                    },
                    background: { opacity: 0.8 }
                }
            }
			},
			legend: {
				show: true
			}
		});
	},
	
	showTooltip: function(x, y, contents) {
        $('<div id="plot_tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#fee',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    },
	
	getThead: function(data){
		var thead = $('<thead>');
		for(i in data){
			$('<th>').text(data[i]).appendTo(thead);
		}
		return thead;
	},
	
	initAcumulativeTables: function(){
		var baseTab = $('<table>').addClass('tab').addClass('bicolor_tab').addClass('left').width('47%');
		var data_table = '#data_table';
		
		var opHead = ['Cliente','Neto','Margen'];
		var exHead = ['Ejecutivo','Neto','Margen'];
		var mcHead = ['Marca','Costo','Margen'];
		//var stHead = ['Estado','Neto','Margen'];
		var lnHead = ['Linea de Negocio','Costo','Margen'];
		
		var opTable = baseTab.clone()
						.append(js_data.getThead(opHead))
						.append($('<caption>').text('Montos por Cliente'))
						.append($('<tbody>').attr('id','opBody'))
						.appendTo(data_table);
						
		var exTable = baseTab.clone()
						.append(js_data.getThead(exHead))
						.append($('<caption>').text('Montos por Ejecutivo'))
						.append($('<tbody>').attr('id','exBody'))
						.appendTo(data_table);
						
		$('<br>').addClass('clear').appendTo(data_table);
						
		var mcTable = baseTab.clone()
						.append(js_data.getThead(mcHead))
						.append($('<caption>').text('Montos por Marca'))
						.append($('<tbody>').attr('id','mcBody'))
						.appendTo(data_table);
						
		var lnTable = baseTab.clone()
						.append(js_data.getThead(lnHead))
						.append($('<caption>').text('Montos por Linea de Negocio'))
						.append($('<tbody>').attr('id','lnBody'))
						.appendTo(data_table);
		
		/*var stTable = baseTab.clone()
						.append(js_data.getThead(stHead))
						.append($('<caption>').text('Montos por Estado'))
						.append($('<tbody>').attr('id','stBody'))
						.appendTo(data_table);*/
						
		
		js_data.initAcumulativeData(opTable, 'dc_cliente', ['dg_razon','dq_precio_total','dq_margen'],[1,0,0],2);
		js_data.initAcumulativeData(exTable, 'dc_ejecutivo', ['dg_nombres','dq_precio_total','dq_margen'],[1,0,0],2);
		js_data.initAcumulativeData(mcTable, 'dc_marca', ['dg_marca','dq_costo_total','dq_margen'],[1,0,0],2);
		js_data.initAcumulativeData(lnTable, 'dc_linea_negocio', ['dg_linea_negocio','dq_costo_total','dq_margen'],[1,0,0],2);
		//js_data.initAcumulativeData(stTable, 'dc_estado', ['dg_estado','dq_precio_total','dq_margen'],[1,0,0]);
		
	},
	
	initAcumulativeData: function(table, groupBy, cols, withoutTotals, sortBy){
		var tbody = $('tbody',table);
		var cotData = js_data.cotizacionData;
		var data = {};
		var colCount = cols.length;
		
		for(i in cotData){
			if(!data[cotData[i][groupBy]]){
				data[cotData[i][groupBy]] = [];
				for(var j = 0;j < colCount; j++){
					data[cotData[i][groupBy]].push(0)
				}
			}
			
			for(j in cols){
				var mode = withoutTotals[j];
				if(mode == 1){
					data[cotData[i][groupBy]][j] = cotData[i][cols[j]];
				}else{
					data[cotData[i][groupBy]][j] += parseFloat(cotData[i][cols[j]]);
				}
			}
			
		}
		
		var totals = [];
		for(var i = 0;i < colCount; i++){
			totals.push(0);
		}
		
		var sorted = [];
		for(i in data){
			sorted.push([i,data[i][sortBy]]);
		}
		
		sorted = sorted.sort(function(a,b){
			return b[1] - a[1];
		});
		
		for(k in sorted){
			var tr = $('<tr>');
			var i = sorted[k][0];
			
			for(j in cols){
				var mode = withoutTotals[j];
				
				var content = $('<a>').multiAttr({
					'href':'#',
					'class':groupBy+''+i
				}).click(js_data.filterDetails);
				
				if(mode == 1){
					content.text(data[i][j]);
					$('<td>').append(content).appendTo(tr);
				}else{
					content.text(mil_format(data[i][j].toFixed(js_data.tcDecimals)));
					$('<td>').append(content).attr('align','right').appendTo(tr);
					totals[j] += parseFloat(data[i][j]);
				}
			}
			
			tr.appendTo(tbody);
		}
		
		var trFoot = $('<tr>');
		
		for(i in cols){
			var mode = withoutTotals[i];
			if(mode == 1){
				$('<th>').appendTo(trFoot);
			}else{
				$('<th>').attr('align','right').text(mil_format(totals[i].toFixed(js_data.tcDecimals))).appendTo(trFoot);
			}
		}
		
		$('<tfoot>').append(trFoot).appendTo(table);
		
		table.tableAdjust(10, true);
		
	},
	
	initDetailedTable: function(){
		$("tr:even td",$('#detail_data_table')).css({"backgroundColor": "#E5E5E5"});
		js_data.refreshDetailTotal();
	},
	
	filterDetails: function(e){
		e.preventDefault();
		var clss = $(this).attr('class');
		var data_table = $('tbody',$('#detail_data_table'));
		$('tr',data_table).hide();
		$('td[style]',data_table).removeAttr('style');
		$('td.'+clss,data_table).parent('tr').show();
		$("tr:visible:even td",data_table).css({"backgroundColor": "#E5E5E5"});
		
		js_data.refreshDetailTotal();
	},
	
	refreshDetailTotal: function(){
		var totals = [0,0,0,0,0];
		var index  = [8,9,11,12,13];
		var table = $('#detail_data_table');
		$('tbody tr:visible',table).each(function(){
			var td = $(this).find('td');
			for(i in index){
				var j = index[i];
				var num = parseFloat($(td[j]).text().replace(/\./g,"").replace(/,/g,"."));
				totals[i] += num;
			}
		});
		
		var foot = $('tfoot th',table);
		for(i in index){
			var j = index[i];
			$(foot[j]).text(mil_format(totals[i].toFixed(js_data.tcDecimals)));
		}
	}
	
};
js_data.init();