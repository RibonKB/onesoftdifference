var informeArriboRenovacion = {

    activateMultiselect: function(){
        $('#dc_tipo_producto, #dc_marca').multiSelect({
    		selectAll: true,
    		selectAllText: "Seleccionar todos",
    		noneSelected: "---",
    		oneOrMoreSelected: "% seleccionado(s)"
    	});
    },

    ajustarTabla: function(){
        $('#resultados').tableExport().tableAdjust(20,false,false);
    },

};
