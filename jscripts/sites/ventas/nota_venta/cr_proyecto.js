cr_proyecto = {
  fieldNames: {
    tipo_venta: '#nv_tipo_venta',
    proyectoBtn: 'proyectoBtn',
    proyectoContainer: '#proyecto_container',
    valorCuota: '#dq_cuota',
    proyectoForm: '#cr_proyecto_form',
    financiamientoOption: ':radio[name=dm_financiamiento]',
    financiamientoData: '#financiamiento_data'
  },
  
  cr_proyecto_url: 'sites/ventas/nota_venta/cr_proyecto.php',
  
  init: function() {
    $(cr_proyecto.fieldNames.tipo_venta).change(cr_proyecto.tipoVentaOnChange);
  },
          
  tipoVentaOnChange: function(){
		var valueData = $(this).val().split('|');
		var costeable = valueData[1] == '1';
		
		$(cr_proyecto.fieldNames.proyectoContainer).html('');
		
		if(costeable){
			cr_proyecto.setProyectoButton();
		}
	},
            
    setProyectoButton: function(){
		var button = cr_proyecto.getProyectoBtnInstance();
		button.click(cr_proyecto.proyectoBtnOnclick);
	},
            
    getProyectoBtnInstance: function(){
		var btn = $('<button>').multiAttr({
			type: 'button',
			id: cr_proyecto.fieldNames.proyectoBtn,
			class: 'imgbtn'
		})
		.css({'background-image' : 'url(images/doc.png)'})
		.text('Establecer datos de proyecto')
		.appendTo(cr_proyecto.fieldNames.proyectoContainer);
		
		$('<br>').prependTo(cr_proyecto.fieldNames.proyectoContainer);
		
		return btn;
		
	},
            
    proyectoBtnOnclick: function(){
		var data = $('#proyecto_data_container :input').serialize();
		var valueData = $(cr_proyecto.fieldNames.tipo_venta).val().split('|');
		pymerp.loadOverlay(cr_proyecto.cr_proyecto_url, 'dc_tipo_venta='+valueData[0]+'&'+data);
	},
            
    initCreation: function(){
		
		cr_proyecto.proyectoForm = $(cr_proyecto.fieldNames.proyectoForm);
		
		//Al enviar el form
		cr_proyecto.proyectoForm.submit(cr_proyecto.submitProyectoForm);
		
	},
            
    submitProyectoForm: function(e){
		e.preventDefault();
		if(!pymerp.validarForm(this)){
			pymerp.showError(pymerp.messages.formRequiredError);
			return;
		}

		$('#proyecto_data_container').html('');

		$(':radio:not(:checked)',$(this)).remove();
		
		$(this).find(':input').each(function(){
			$('<input>').multiAttr({
				'type': 'hidden',
				'name': $(this).attr('name')
			}).val($(this).val()).appendTo('#proyecto_data_container');
		});

		$(this).parents('.overlay').remove();
		
	},
          
};