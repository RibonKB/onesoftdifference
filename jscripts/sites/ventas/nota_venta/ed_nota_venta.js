var editarNotaVenta = {
	fechaArribo: undefined,

	init: function(){
		editarNotaVenta.calendarAction();
	},

	calendarAction: function(){
		$('.set_fecha_arribo').click(function(){
			editarNotaVenta.inputFechaArribo = $(this).parent().find('.fecha_arribo');
			pymerp.loadOverlay(setFechaArriboProducto, 'fecha='+editarNotaVenta.inputFechaArribo.val());
		});
	},

	//función llamada desde la vista dateSelect
	ds_asignarFecha: function(){
		$('.btnAsignarFecha').click(function(event){
			event.preventDefault();
			editarNotaVenta.inputFechaArribo.val($(this).parent().parent().find('.inputtext').val());
			editarNotaVenta.inputFechaArribo = undefined;
			$(this).parents('.overlay').remove();
		});

	}

};
