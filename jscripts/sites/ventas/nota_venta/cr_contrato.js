var cr_contrato = {
	
	fieldNames: {
		tipo_venta: '#nv_tipo_venta',
		contratoBtn: 'contratoBtn',
		contratoContainer: '#contrato_container',
		valorCuota: '#dq_cuota',
		contratoForm: '#cr_contrato_form',
		financiamientoOption: ':radio[name=dm_financiamiento]',
		financiamientoData: '#financiamiento_data'
	},
	
	cr_contrato_url: 'sites/ventas/nota_venta/cr_contrato.php',
	cr_distribucion_url: 'sites/ventas/nota_venta/proc/cr_distribucion_contrato.php',
	
	init: function(){
		$(cr_contrato.fieldNames.tipo_venta).change(cr_contrato.tipoVentaOnChange);
	},
	
	tipoVentaOnChange: function(){
		var valueData = $(this).val().split('|');
		var contratable = valueData[2] == '1';
		
		$(cr_contrato.fieldNames.contratoContainer).html('');
		
		if(contratable){
			cr_contrato.setContratoButton();
		}
	},
	
	setContratoButton: function(){
		var button = cr_contrato.getContratoBtnInstance();
		button.click(cr_contrato.contratoBtnOnclick);
	},
	
	getContratoBtnInstance: function(){
		var btn = $('<button>').multiAttr({
			type: 'button',
			id: cr_contrato.fieldNames.contratoBtn,
			class: 'imgbtn'
		})
		.css({'background-image' : 'url(images/doc.png)'})
		.text('Establecer datos de contrato')
		.appendTo(cr_contrato.fieldNames.contratoContainer);
		
		$('<br>').prependTo(cr_contrato.fieldNames.contratoContainer);
		
		return btn;
		
	},
	
	contratoBtnOnclick: function(){
		var data = $('#contrato_data_container :input').serialize();
		var valueData = $(cr_contrato.fieldNames.tipo_venta).val().split('|');
		pymerp.loadOverlay(cr_contrato.cr_contrato_url, 'dc_tipo_venta='+valueData[0]+'&'+data);
	},
	
	initCreation: function(){
		
		cr_contrato.contratoForm = $(cr_contrato.fieldNames.contratoForm);
		
		//Al enviar el form
		cr_contrato.contratoForm.submit(cr_contrato.submitContratoForm);
		
		//Al cambiar tipo de financiamiento se debe ocultar o mostrar los datos de financiamiento indirecto.
		$(cr_contrato.fieldNames.financiamientoOption, cr_contrato.contratoForm).click(cr_contrato.financiamientoChange);
		
		/**
		*	Botón para la distribución de saldos en el tipo de venta
		*/
		var img = $('<img>').multiAttr({
			'src' : 'images/lukas.png',
			'alt' : '$',
			'title' : 'Distribuir cuota'
		}).css({
			'padding' : '3px',
			'border': '1px solid #AFAFAF',
			'verticalAlign': 'middle',
			'marginTop': '-5px'
		})
		.click(function(){
			var data = $('#distribucion_data', cr_contrato.contratoForm).find(':input').serialize();
			var valueData = $(cr_contrato.fieldNames.tipo_venta).val().split('|');
			pymerp.loadOverlay(cr_contrato.cr_distribucion_url, 'dc_tipo_venta='+valueData[0]+'&'+data,true);
		});
		
		var size = parseInt($(cr_contrato.fieldNames.valorCuota).attr('size'))-5;
		$(cr_contrato.fieldNames.valorCuota).multiAttr({'size':size, 'readonly':true}).after(img);
		
	},
	
	submitContratoForm: function(e){
		e.preventDefault();
		if(!pymerp.validarForm(this)){
			pymerp.showError(pymerp.messages.formRequiredError);
			return;
		}

		var dc_cuotas = parseInt($('#dc_cuotas').val());
		
		if(!dc_cuotas){
			pymerp.showError('El valor de las cuotas es inválidos');
			return;
		}

		$('.prod_cant').val(dc_cuotas).attr('readonly',true);

		$('#contrato_data_container').html('');

		$(':radio:not(:checked)',$(this)).remove();
		
		$(this).find(':input').each(function(){
			$('<input>').multiAttr({
				'type': 'hidden',
				'name': $(this).attr('name')
			}).val($(this).val()).appendTo('#contrato_data_container');
		});

		$(this).parents('.overlay').remove();
		
	},
	
	financiamientoChange: function(){
		var field = $(this);
		var data_div = $(cr_contrato.fieldNames.financiamientoData);
		
		if(field.val() == 0){
			data_div.addClass('hidden');
			$(':input',data_div).removeAttr('required');
		}else{
			data_div.removeClass('hidden');
			$(':input',data_div).attr('required',true);
		}
	},
	
	initDistribucion: function(){
		$('.dq_cuota_concepto').css({'textAlign': 'right'});
		$('#cr_distribucion_contrato').submit(cr_contrato.submitDistributionForm).parents('.overlay').css({'minWidth':'420px'});
	},
	
	submitDistributionForm: function(e){
		e.preventDefault();
		var status = false;
		
		var data = $('.dq_cuota_concepto',$('#cr_distribucion_contrato'));
		var total = 0;
		
		data.each(function(){
			var val = parseFloat($(this).val());
			$(this).removeClass('invalid');
			if(!val && val !== 0){
				status = true;
				$(this).addClass('invalid');
			}else{
				total += val;
			}
		});
		
		if(status){
			pymerp.showError('Los datos marcados son inválidos');
			return;
		}
		
		if(total == 0){
			pymerp.showError('Debe ingresar al menos 1 valor para las cuotas');
			return;
		}
		
		var div = $('#distribucion_data').html('');
		
		data.each(function(){
			var dc_detalle = parseInt($(this).attr('id').substr(8));
			var dq_cuota = parseFloat($(this).val());
			
			if(dq_cuota){
				$('<input>').multiAttr({
					'type':'hidden',
					'name': 'dq_cuota_concepto[]',
					'class': 'dq_cuota_concepto'
				}).val(dq_cuota).appendTo(div);
	
				$('<input>').multiAttr({
					'type':'hidden',
					'name':'dc_detalle_concepto[]'
				}).val(dc_detalle).appendTo(div);
			}
			
		});
		
		$('#dq_cuota').val(total);
		$('#cr_distribucion_contrato').parents('.overlay').remove();
		
	}
};