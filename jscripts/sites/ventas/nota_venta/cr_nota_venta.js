$.extend(js_data,{

	//Textbox y Div que se encargan de recibir o mostrar la cotización relacionada
	cotInput: null,
	cotDiv: null,
	inputFechaArribo: undefined,

	//campos exclusivos de la creación de notas de venta
	nv_fieldNames: {
		cotizacion: '#nv_cotizacion',
		tipo_cambio: '#prod_tipo_cambio',
		cambio: '#nv_cambio',
		id_cotizacion: ':hidden[name=nv_id_cotizacion]',
		ch_cliente: '#cli_switch'
	},

	//mensajes utilizados en la creación de la nota de venta, de error y advertencia
	nv_messages: {
		cot_notFound: 'No se ha encontrado la cotización especificada, intentelo denuevo',
		cot_woDetails: 'La cotización especificada no posee detalle, pero se asigna de todas formas',
		cot_chTipoCambio: "¿Quiere cambiar el tipo de cambio de la nota de venta por el que utiliza la cotizaci\u00f3n?",
		noDetailsFound: "No ha agregado productos a la nota de venta",
		badCodesFound: "Los códigos de los productos deben estar todos perfectamente asignados"
	},

	//inicializa las funcionalidades mínimas de la creación de nota de venta
	init: function(){
		js_data.baseInit();

		js_data.jForm.submit(js_data.formSubmit);

		js_data.fieldNames.proveedor = '.prod_proveedor';
		js_data.fieldNames.cambio = js_data.nv_fieldNames.cambio;
		js_data.fieldNames.cebe = '.prod_cebe';
		js_data.fieldNames.ceco = '.prod_ceco';

		$(js_data.nv_fieldNames.cotizacion)
		.autocomplete(js_data.AC_cotizacionUrl,js_data.AC_options)
		.result(js_data.AC_cotizacionResult);

		$(js_data.nv_fieldNames.tipo_cambio).change(js_data.exchangeOnChange);
		$(js_data.nv_fieldNames.cambio).change(js_data.exchangeTextOnChange);

		$(js_data.nv_fieldNames.ch_cliente).click(function(){
			pymerp.loadOverlay('sites/ventas/switch_cliente.php');
		});

		var cotInput = $(js_data.nv_fieldNames.cotizacion);
		if(cotInput.val() != ''){
			js_data.AC_cotizacionResult.call(cotInput);
		}

		js_data.checkBaseDetails();

		js_data.calendarAction();

	},

	//Validación y envio de formulario.
	formSubmit: function(e){
		e.preventDefault();
		if(!js_data.checkCodigos()){
			pymerp.showError(js_data.nv_messages.badCodesFound);
			$(js_data.prodList).parent().addClass('invalid');
			return;
		}
		if($('#prod_list tr').size() == 0){
			pymerp.showAviso(js_data.nv_messages.noDetailsFound);
			return;
		}
		pymerp.confirmEnviarForm(js_data.jForm,js_data.jForm.prev());
	},

	//Validación de que todos los códigos de productos jhayan sido agregados correctamente.
	checkCodigos: function(){
		var tr = $(js_data.detailRow,$(js_data.prodList)).size();
		var code = $(":hidden[name=\'prod[]\']",$(js_data.prodList)).size();
		return tr==code;
	},

	//Los detalles que serán incluidos inicialmente en la nota de venta
	detallesBase: null,
	checkBaseDetails: function(){
		if(js_data.detallesBase != null){
			$(js_data.nv_fieldNames.tipo_cambio).trigger('change');
			js_data.setDetailFromCot(js_data.detallesBase);
		}
	},

	//URL de búsqueda de cotizaciones y detalles de cotización para el autocompletado
	AC_cotizacionUrl: 'sites/proc/autocompleter/cotizacion_strict.php',
	AC_cotizacionDataUrl: 'sites/ventas/nota_venta/proc/get_cotizacion_detalle.php',

	//Función que se encarga de realizar las operaciones post-selección de la cotización
	AC_cotizacionResult: function(e,row){

		js_data.cotInput = $(this).clone();
		var input = js_data.getDisabledInput(this);

		$.getJSON(js_data.AC_cotizacionDataUrl,{ number: encodeURIComponent(input.val()) },function(data){

			if(data == '<not-found>'){
				js_data.setEnableInput(input);
				pymerp.showError(js_data.nv_messages.cot_notFound);
				return;
			}

			if(data[0] == '<empty>'){
				pymerp.showAviso(js_data.nv_messages.cot_woDetails);
				var id = data[1].dc_cotizacion;
			}else{
				var detalle = data[0];
				var id = data[1].dc_cotizacion;

				if($(js_data.nv_fieldNames.tipo_cambio).val() != 0){
					if(confirm(js_data.nv_messages.cot_chTipoCambio)){
						$(js_data.nv_fieldNames.tipo_cambio).val(data[1].dc_tipo_cambio).change();
						js_data.cambio = data[1].dq_cambio;
						$(js_data.nv_fieldNames.cambio).val(parseFloat(data[1].dq_cambio).toFixed(js_data.cambioDecimales));
					}
				}else{
					$(js_data.nv_fieldNames.tipo_cambio).val(data[1].dc_tipo_cambio).change();
					js_data.cambio = data[1].dq_cambio;
					$(js_data.nv_fieldNames.cambio).val(parseFloat(data[1].dq_cambio).toFixed(js_data.cambioDecimales));
				}

				js_data.setDetailFromCot(detalle);
			}

			js_data.cotDiv = js_data.getEditableDiv(input.val(),js_data.editCotizacion).width(input.width()).replaceAll(input);
			$(js_data.nv_fieldNames.id_cotizacion).val(id);

			js_data.detailRefresh();
			js_data.totalRefresh();

		});
	},

	//al hacer click en el botón de edición de cotización
	editCotizacion: function(){
		js_data.cotInput
		.autocomplete(js_data.AC_cotizacionUrl,js_data.AC_options)
		.result(js_data.AC_cotizacionResult)
		.replaceAll(js_data.cotDiv);

		$(js_data.nv_fieldNames.id_cotizacion).val(0);
	},

	//Asignar el detalle desde la selección de la cotización
	setDetailFromCot: function(detail){
		for(i in detail){
			if(detail[i].dc_producto != null)
				js_data.addValidDetail(detail[i]);
			else
				js_data.addInvalidDetail(detail[i]);
		}
	},

	//agrega un detalle (con los campos rellenados) de manera válida
	addValidDetail: function(detail){
		var tr = js_data.getEmptyDetail();
		tr.find(js_data.fieldNames.descripcion).val(detail.dg_descripcion);
		tr.find(js_data.fieldNames.proveedor).val(detail.dc_proveedor);
		tr.find(js_data.fieldNames.cantidad).val(detail.dq_cantidad);
		tr.find(js_data.fieldNames.precio).val(detail.dq_precio_venta/js_data.cambio);
		tr.find(js_data.fieldNames.costo).val(detail.dq_precio_compra/js_data.cambio);
		tr.find(js_data.fieldNames.cebe).val(detail.dc_cebe);
		tr.find(js_data.fieldNames.ceco).val(detail.dc_ceco);

		var div = js_data.getEditableDiv(detail.dg_codigo,js_data.prodReturnToInput);
		//div.find('img').remove();

		$('<input>').multiAttr({
			type:'hidden',
			name: js_data.fieldNames.id_producto
		}).val(detail.dc_producto).appendTo(div);

		div.replaceAll(tr.find(js_data.fieldNames.codigo));

		tr.appendTo(js_data.prodList);
	},

	//Agrega un detalle inválido, este se mostrará en caso de que el producto que se desee agregar no exista en el sistema.
	addInvalidDetail: function(det){
		row = $('<tr>');
		col = $('<td>').css({
			border: '1px solid #F00',
			background: '#FFFFBF'
		});

		crbtn = $('<img>').multiAttr({
			src: 'images/addbtn.png',
			alt: '[+]',
			title: 'Crear el producto'
		}).click(js_data.createInvalidProduct);

		col.clone().appendTo(row);
		col.clone().text(det.dg_producto).appendTo(row);
		col.clone().text(det.dg_descripcion).appendTo(row);
		col.clone().text('-').appendTo(row);
		col.clone().text('-').appendTo(row);
		col.attr('align','right');
		col.clone().text(pymerp.milFormat(det.dq_cantidad)).appendTo(row);
		col.clone().text(pymerp.milFormat((det.dq_precio_venta/js_data.cambio).toFixed(js_data.cambioDecimales))).appendTo(row);
		col.clone().text(pymerp.milFormat((det.dq_precio_venta*det.dq_cantidad/js_data.cambio).toFixed(js_data.cambioDecimales))).appendTo(row);
		col.clone().text(pymerp.milFormat((det.dq_precio_compra/js_data.cambio).toFixed(js_data.cambioDecimales))).appendTo(row);
		col.clone().text(pymerp.milFormat((det.dq_precio_compra*det.dq_cantidad/js_data.cambio).toFixed(js_data.cambioDecimales))).appendTo(row);
		col.clone().text('-').appendTo(row);
		col.clone().text('-').appendTo(row);
		$(js_data.prodList).prev('thead').append(row);
	},

	//Levantar el formulario de creación de
	createInvalidProduct: function(){
		var dg_producto = encodeURIComponent(det[i].dg_producto);
		var dg_descripcion = encodeURIComponent(det[i].dg_descripcion);
		var dq_precio_compra = encodeURIComponent(det[i].dq_precio_compra);
		var dq_precio_venta = encodeURIComponent(det[i].dq_precio_venta);
		pymerp.loadOverlay('sites/ventas/proc/cr_producto.php?c='+dg_producto+'&d='+dg_descripcion+'&q='+dq_precio_compra+'&v='+dq_precio_venta);
	},

	calendarAction: function(){
		$('.set_fecha_arribo').click(function(){
			js_data.inputFechaArribo = $(this).parent().find('.fecha_arribo');
			pymerp.loadOverlay(setFechaArriboProducto, 'fecha='+js_data.inputFechaArribo.val());
		});
	},

	//función llamada desde la vista dateSelect
	ds_asignarFecha: function(){
		$('.btnAsignarFecha').click(function(event){
			event.preventDefault();
			js_data.inputFechaArribo.val($(this).parent().parent().find('.inputtext').val());
			js_data.inputFechaArribo = undefined;
			$(this).parents('.overlay').remove();
		});

	}

});
