var cllr_js = {
	
	open: false,
	active: 0,
	
	objects: {
		reload_btn: null,
		callers_list: null,
		data_container: null,
		open_btn: null,
		container: null,
		content: null
	},
	
	reloadURL: ["god.php?factory=RegistroLlamados&modulo=indicadores&action=reloadData",
				"god.php?factory=EstadisticasIndividuales&modulo=indicadores&action=reloadData",
				"god.php?factory=EstadisticasGrupales&modulo=indicadores&action=reloadData"],

	init: function(){
		cllr_js.objects.reload_btn = [$('#cllr_reload'),$('#cllr_reload2'),$('#cllr_reload3')];
		cllr_js.objects.callers_list = [$('#cllr_callers'),$('#cllr_others'),$('#cllr_others3')];
		cllr_js.objects.data_container = [$('#cllr_data'),$('#cllr_data2'),$('#cllr_data3')];
		cllr_js.objects.open_btn = $('.cllr_activate_button');
		cllr_js.objects.container = $('#callregindex');
		cllr_js.objects.content = $('.cllr_content');
		
		cllr_js.objects.reload_btn[0].click(cllr_js.reloadForMe);
		cllr_js.objects.callers_list[0].change(cllr_js.reloadForCaller);
		
		cllr_js.objects.reload_btn[1].click(cllr_js.cllr2_reloadForMe);
		cllr_js.objects.callers_list[1].change(cllr_js.cllr2_reloadForOther);
		
		cllr_js.objects.reload_btn[2].click(cllr_js.cllr3_reloadForMe);
		cllr_js.objects.callers_list[2].change(cllr_js.cllr3_reloadForOther);
		
		cllr_js.objects.open_btn.click(cllr_js.moveContainer);
	},
	
	/**
	*	Funciones asociadas al registro de llamados
	*	#cllr_content
	**/
	reloadForCaller: function(){
		var caller = $(this).val();
		cllr_js.reloadData(caller);
		$(this).val(0);
	},
	
	reloadForMe: function(){
		cllr_js.reloadData(undefined);
	},
	
	reloadData: function(caller){
		if(caller == undefined){
			pymerp.loadFile(cllr_js.reloadURL[0],cllr_js.objects.data_container[0]);
		}else{
			pymerp.loadFile(cllr_js.reloadURL[0],cllr_js.objects.data_container[0],'id='+caller,function(){
				$('#cllr_datatable').tableOverflow([20,200,130,130]);
				$('.cllr_call_button',$('#cllr_datatable')).click(cllr_js.showCallInfo);
			});
		}
	},
	
	showCallInfo: function(){
		var dc_cliente = $(this).attr('data-dccliente');
		pymerp.loadOverlay("god.php?factory=EstadisticasGrupales&modulo=indicadores&action=showCallInfo&dc_cliente="+dc_cliente);
	},
	
	/**
	*	Funciones asociadas a las estadísticas individuales
	*	#cllr_content2
	**/
	cllr2_reloadForOther: function(){
		var other = $(this).val();
		cllr_js.cllr2_reloadData(other);
		$(this).val(0);
	},
	
	cllr2_reloadForMe: function(){
		cllr_js.cllr2_reloadData(undefined);
	},
	
	cllr2_reloadData: function(id){
		if(id == undefined){
			pymerp.loadFile(cllr_js.reloadURL[1],cllr_js.objects.data_container[1]);
		}else{
			pymerp.loadFile(cllr_js.reloadURL[1],cllr_js.objects.data_container[1],'id='+id,function(){
				$('#cllr_datatable2').tableOverflow([200,150,150]);
			});
		}
	},
	
	
	/**
	*	Funciones asociadas a las estadísticas grupales
	*	#cllr_content3
	**/
	cllr3_reloadForOther: function(){
		var other = $(this).val();
		cllr_js.cllr3_reloadData(other);
		$(this).val(0);
	},
	
	cllr3_reloadForMe: function(){
		cllr_js.cllr3_reloadData(undefined);
	},
	
	cllr3_reloadData: function(id){
		if(id == undefined){
			pymerp.loadFile(cllr_js.reloadURL[2],cllr_js.objects.data_container[2]);
		}else{
			pymerp.loadFile(cllr_js.reloadURL[2],cllr_js.objects.data_container[2],'id='+id,function(){
				$('#cllr_datatable2').tableOverflow([120,100,90,100,50]);
			});
		}
	},
	
	
	/**
	*	Funciones asociadas al comportamiento de la ventana de visualización
	**/
	moveContainer: function(){
		var dst = $(this).attr('data-dst');
		cllr_js.objects.content.hide();
		if(cllr_js.open && dst == cllr_js.active){
				cllr_js.objects.container.css({
					width: 40
				});
				cllr_js.objects.container.find('.cllr_activate_button').each(function(){
					$(this).css({
						left: -1
					});
				});
				cllr_js.open = false;
				return;
			
		}else{
			cllr_js.objects.container.css({
				width: 550
			});
			cllr_js.open = true;
		}
		
		
		cllr_js.objects.container.find('.cllr_activate_button').each(function(){
			$(this).css({left: -25, zIndex:0});
		});
		$(this).css({left:-40, zIndex:100}).width(40);
		$(dst).show();
		cllr_js.active = dst;
		
	}
	
};
$(document).ready(function(){
	cllr_js.init();
});