var js_data = {
	
	calendar: $('#ruta_calendar'),
	dNavigator: $('#calendar_navegating_days'),
	
	currentDay: 0,
	
	rutaClass: {
		nvContainer: 'nv_on_calendar',
		CalendarHourItem: '.calendar_hour_item'
	},
	
	init: function(){
		js_data.loadOnCalendar(js_data.initRutaData);
		$('#righterDiv table').tableExport().tableAdjust(18);
		
		$('.left',js_data.dNavigator).click(js_data.leftNavigation);
		$('.right',js_data.dNavigator).click(js_data.rightNavigation);
	},
	
	loadOnCalendar: function(rdata){
		$('.'.concat(js_data.rutaClass.nvContainer),js_data.calendar).remove();
		//$(js_data.rutaClass.CalendarHourItem,js_data.calendar).show();
		for(i in rdata){
			
			var tipoId = '#tipo'.concat(rdata[i].dc_tipo_ruta);
			var container = $(tipoId,js_data.calendar);
			var nvContainer = container.children('.nv'.concat(rdata[i].dc_nota_venta));
			
			if(nvContainer.size() == 0){
				nvContainer = js_data.getEmptyNVToCalendar(rdata[i].dq_nota_venta,rdata[i].dg_razon,rdata[i].dc_nota_venta,rdata[i].df_fecha_emision).appendTo(container);
			}
			
			nvContainer.append(
				js_data.getDetailToInsert(rdata[i].dq_ruta, rdata[i].dq_cantidad, rdata[i].dt_hora, rdata[i].dg_codigo, rdata[i].dg_producto)
			);
			
			/*var timeId = '#'.concat(
				rdata[i].dt_hora.substring(0,5).replace(':','')
			);
			
			var container = $(timeId,js_data.calendar);
			var nvContainer = container.children('.nv'.concat(rdata[i].dc_nota_venta));
			
			if(nvContainer.size() == 0){
				nvContainer = js_data.getEmptyNVToCalendar(rdata[i].dq_nota_venta,rdata[i].dg_razon,rdata[i].dc_nota_venta).appendTo(container);
			}
			
			nvContainer.append(
				js_data.getDetailToInsert(rdata[i].dq_ruta, rdata[i].dq_cantidad, rdata[i].dt_hora, rdata[i].dg_codigo, rdata[i].dg_producto)
			);*/
			
		}
		/*var first = $('.'.concat(js_data.rutaClass.nvContainer),js_data.calendar);
		if(first.size() != 0){
			first = first.first().parent();
			js_data.deletePrevious(first);
			js_data.calendar.scrollTop(0);
		}*/
	},
	
	deletePrevious: function(Item){
		var prev = Item.prev();
		if(prev.size() > 0){
			prev.hide();
			js_data.deletePrevious(prev);
		}
	},
	
	getEmptyNVToCalendar: function(dq_nota_venta, dg_razon, dc_nota_venta, df_emision){
		var div = $('<div>').addClass(js_data.rutaClass.nvContainer).addClass('nv'+dc_nota_venta);
		var nv = $('<b>').text(dq_nota_venta);
		var emision = $('<small>').text(' (Emisión: '+df_emision+')');
		var cliente = $('<small>').text(dg_razon);
		return div.append(nv).append(emision).append('<br>').append(cliente).dblclick(js_data.showNvDetail);
	},
	
	showNvDetail: function(){
		var div = $('<div>').addClass('overlay');
		
		var dq_nota_venta = $(this).children('b:first').text();
		var dg_razon = $(this).children('small:first').text();
		
		$('<div>').addClass('secc_bar').html("Detalles de nota de venta <strong>"+dq_nota_venta+"</strong> en ruta").appendTo(div);
		var pane = $('<div>').addClass('panes').appendTo(div);
		
		var table = $('<table>').addClass('tab').width('100%');
		
		table.append($('<caption>').text('Cliente: '.concat(dg_razon)));
		
		table.append($('<thead>').append($('<tr>')
			.append($('<th>').text('Ruta'))
			.append($('<th>').text('Hora'))
			.append($('<th>').text('Código'))
			.append($('<th>').text('Producto'))
			.append($('<th>').text('Cantidad'))
		));
		
		tbody = $('<tbody>');
		
		$('.dq_ruta',this).each(function(){
			var dq_ruta = $(this);
			var dq_cantidad = dq_ruta.next();
			var dt_hora = dq_cantidad.next();
			var dg_codigo = dt_hora.next();
			var dg_producto = dg_codigo.next();
			tbody.append($('<tr>')
				.append($('<td>').text(dq_ruta.val()))
				.append($('<td>').text(dt_hora.val()))
				.append($('<td>').text(dg_codigo.val()))
				.append($('<td>').text(dg_producto.val()))
				.append($('<td>').attr('align','right').text(dq_cantidad.val()))
				
			);
		});
		
		table.append(tbody).appendTo(pane);
		
		$('#content').append(div);
		pymerp.objectOverlay(div);
	},
	
	getDetailToInsert: function(dq_ruta, dq_cantidad, dt_hora, dg_codigo, dg_producto){
		var base = $('<input>').attr('type','hidden');
		var iruta = base.clone().addClass('dq_ruta').val(dq_ruta);
		var icantidad = base.clone().addClass('dq_cantidad').val(dq_cantidad);
		var ihora = base.clone().addClass('dt_hora').val(dt_hora.substr(0,5));
		var icodigo = base.clone().addClass('dg_codigo').val(dg_codigo);
		var iproducto = base.clone().addClass('dg_producto').val(dg_producto);
		
		return iruta.add(icantidad).add(ihora).add(icodigo).add(iproducto);
	},
	
	leftNavigation: function(){
		js_data.currentDay--;
		js_data.doNavigation();
	},
	
	rightNavigation: function(){
		js_data.currentDay++;
		js_data.doNavigation();
	},
	
	doNavigation: function(direction){
		$.getJSON('sites/indicadores/proc/get_calendar_data_on_day.php',{direction:js_data.currentDay},function(data){
			$('#date_to_route',js_data.dNavigator).text(data.selectedDay);
			js_data.loadOnCalendar(data.routeData);
		});
	}
	
};