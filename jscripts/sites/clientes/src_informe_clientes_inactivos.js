var js = {

	fields: {
		dc_ejecutivo: $('#dc_ejecutivo')
	},

	init: function(){
		js.initFieldFormat();
	},
	
	initFieldFormat: function(){
		js.fields.dc_ejecutivo.multiSelect({
			selectAll: true,
			selectAllText: "Seleccionar todos",
			noneSelected: "---",
			oneOrMoreSelected: "% seleccionado(s)"
		});
	}
	
};
js.init();