var js_data = {
	
	multiSelOptions: {
		selectAll: true,
		selectAllText: "Seleccionar todos",
		noneSelected: "---",
		oneOrMoreSelected: "% seleccionado(s)"
	},
	
	init: function(){
		$('#dc_ejecutivo,#dc_tipo_cliente,#dc_mercado,#dc_region').multiSelect(js_data.multiSelOptions);
	}
};
js_data.init();