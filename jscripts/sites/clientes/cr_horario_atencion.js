var cr_horario = {
	
	init: function(){
		$('#btn_add_horario_atencion').click(cr_horario.addHorario);
		$('#cr_horario_atencion').submit(cr_horario.horarioSubmit);
	},
	
	addHorario: function(){
		var checks = $(':checkbox[name=dc_dia\[\]]').filter(':checked');
		
		var hora_inicio  = (parseInt($('#dt_hora_desde_hora').val())+parseInt($('#dt_hora_desde_meridiano').val())).toString().concat(':',$('#dt_hora_desde_minuto').val());
		var hora_termino = (parseInt($('#dt_hora_hasta_hora').val())+parseInt($('#dt_hora_hasta_meridiano').val())).toString().concat(':',$('#dt_hora_hasta_minuto').val());
		
		checks.each(function(){
			cr_horario.addHorarioTr($(this).val(),hora_inicio,hora_termino);
		});
	},
	
	addHorarioTr: function(dia, hora_inicio, hora_termino){
		var tr = $('<tr>');
		var delbtn = $('<img>').multiAttr({
			'src': 'images/delbtn.png',
			'alt': '[X]',
			'title': 'Eliminar horario'
		}).click(function(){
			$(this).parent().parent().remove();
		});
		
		var hid = $('<input>').multiAttr({
			'type': 'hidden',
			'name': 'horario_atencion[]'
		}).val(dia.concat('|',hora_inicio,'|',hora_termino));
		
		$('<td>').append(delbtn).append(hid).appendTo(tr);
		$('<td>').append($('<b>').text(cr_horario.dias[dia])).appendTo(tr);
		$('<td>').text(hora_inicio).appendTo(tr);
		$('<td>').text(hora_termino).appendTo(tr);
		
		tr.appendTo('#day_list');
	},
	
	horarioSubmit: function(e){
		e.preventDefault();
		var inputs = $('#day_list :input');
		$('#horario_atencion_data').html('').append(inputs);
		$(this).closest('.overlay').remove();
	}
};
cr_horario.init();