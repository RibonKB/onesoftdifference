$.extend(js_data,{
	cli_ac_data: ['sites/proc/autocompleter/cliente.php',{formatItem: function(row){ return row[1]+" ("+row[0]+") "+row[2]; },minChars: 2,width:300}],
	cli_contacto_data: [],
	
	ct_fieldNames: {
		cambio: '#ct_cambio',
		tipo_cambio: '#prod_tipo_cambio',
		tipo_contrato: '#cont_tipo',
		numero_cuotas: '#ct_cuotas',
		valor_cuota: '#ct_valor_cuota',
		fund_dataset: '#financ_data'
	},
	
	prodListCt: $(js_data.prodList,js_data.jForm),
	
	init: function(){
		this.cli_input = $('#ac_cliente').autocomplete(this.cli_ac_data[0],this.cli_ac_data[1]).result(this.cli_ac_result).clone();
		$('#cont_contacto').attr('disabled',true);
		
		$(js_data.ct_fieldNames.valor_cuota,js_data.jForm).attr('readonly',true);
		
		$(js_data.ct_fieldNames.tipo_contrato,js_data.jForm).change(js_data.editTipoContrato);
		
		$(js_data.ct_fieldNames.numero_cuotas,js_data.jForm).change(js_data.editCuotas);
		
		js_data.fund_data = $(js_data.ct_fieldNames.fund_dataset,js_data.jForm)
		js_data.fund_data.css({ minWidth: js_data.fund_data.children('fieldset').width() });
		
		js_data.baseInit();
		js_data.jForm.submit(js_data.formSubmit);
		
		js_data.fieldNames.proveedor = '.prod_proveedor';
		js_data.fieldNames.cambio = js_data.ct_fieldNames.cambio;
		js_data.fieldNames.cebe = '.prod_cebe';
		js_data.fieldNames.ceco = '.prod_ceco';
		
		$(js_data.ct_fieldNames.tipo_cambio,js_data.jForm).change(js_data.exchangeOnChange);
		$(js_data.ct_fieldNames.cambio,js_data.jForm).change(js_data.exchangeTextOnChange);
		
	},
	
	formSubmit: function(e){
		e.preventDefault();
	},
	
	addEmptyDetail: function(){
		var det = js_data.emptyDetail.clone(true).appendTo(js_data.prodList);
		det.find(js_data.fieldNames.codigo)
			.autocomplete(js_data.AC_prodCode,js_data.AC_options)
			.result(js_data.AC_prodResult);
		det.find(js_data.fieldNames.cantidad).val($(js_data.ct_fieldNames.numero_cuotas,js_data.jForm).val());
	},
	
	editTipoContrato: function(){
		var finan = $(this).val().split('|',2)[1];
		if(finan == 1){
			pymerp.objectOverlay(js_data.fund_data);
			$(':text,select',js_data.fund_data).attr('required',true);
		}else{
			$(':text,select',js_data.fund_data).removeAttr('required');
		}
	},
	
	editCuotas: function(){
		var val = parseInt($(this).val());
		if(!val){
			val = 1;
			$(this).val(1);
		}else{
			$(this).val(val);
		}
		$(js_data.fieldNames.cantidad,js_data.prodListCt).val(val);
	},
	
	cli_ac_result: function(e,row){
		js_data.getDivLabel(row[1],js_data.cl_ac_edit).width($(this).width())
		.append($('<input>').multiAttr({type: 'hidden',name:'cont_cliente'}).val(row[3]))
		.replaceAll(this);
		js_data.getContactos(row[3]);
	},
	cl_ac_edit: function(){
		var parent = $(this).attr('src','images/ajax-loader.gif').unbind('click').parent();
		js_data.cli_input.val('')
		.autocomplete(js_data.cli_ac_data[0],js_data.cli_ac_data[1])
		.result(js_data.cli_ac_result)
		.replaceAll(parent)
		.focus();
		var sel = $('#cont_contacto').html('').attr('disabled',true);
	},
	getDivLabel: function(text,editAction){
		
		var edbtn = $('<img>').multiAttr({
			src: 'images/editbtn.png',
			title: 'Modificar',
			alt: '[|]',
			class:'right hidden'
		}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
		.mouseout(function(){$(this).css({background:'',border:''})}).click(editAction);
		
		var div = $('<div>').addClass('inputtext').css({
			background:'#EEE',
			fontWeight:'bold'
		}).html(text)
		.mouseenter(function(){$(this).children('img').show();})
		.mouseleave(function(){$(this).children('img').hide();})
		.prepend(edbtn);
		
		return div;
	},
	getContactos:function(id){
		if(this.cli_contacto_data[id]){
			var sel = $('#cont_contacto').html('').attr('disabled',false);
			sel.append($('<option>'));
			for(i in this.cli_contacto_data[id]){
				sel.append($('<option>').attr('value',this.cli_contacto_data[id][i].dc_contacto).text(this.cli_contacto_data[id][i].dg_contacto));
			}
			return;
		}
		$.getJSON('sites/contratos/proc/get_contactos_by_client.php',{ cli_id: id },function(data){
			if(data == '<empty>'){
				pymerp.showError("Contactos no encontrados");
				return;
			}
			js_data.cli_contacto_data[id] = data;
			js_data.getContactos(id);
		});
	}
});