var js_data = {
	ac_url: 'sites/proc/autocompleter/producto.php',
	ac_options: {
		formatItem: function(row){return row[0]+" ( "+row[1]+" )";},
		minChars: 2,
		width:300
	},
	
	stock_view_url: 'sites/logistica/proc/src_stock_report.php',
	id_form: '#set_traspaso',
	
	init: function(){
		$('#tr_producto')
		.autocomplete(js_data.ac_url , js_data.ac_options)
		.result(js_data.ac_prod_result);
		
		$('#tr_cantidad').change(js_data.cant_change);
		
		$(js_data.id_form).submit(js_data.form_submit);
		
		js_data.prod_input = $('#tr_producto').clone(true);
	},
	form_submit: function(e){
		e.preventDefault();
		
		if(!validarForm(js_data.id_form)){
			show_error("Los campos marcados son obligatorios");
			return;
		}
		
		if($('#tr_bodega_salida').val() == $('#tr_bodega_entrada').val()){
			show_aviso('Debe indicar bodegas distintas para hacer el traspaso');
			return;
		}
		
		if($('#tr_producto').size()){
			show_aviso('No ha seleccionado un producto correctamente');
			return;
		}
		
		show_loader();
		
		var values = $(js_data.id_form).serialize();
		var action = $(js_data.id_form).attr("action");
		var target = js_data.id_form+'_res';
		
		loadFile(action,target,values,function(){ hide_loader(); });
	},
	ac_prod_result: function(e,row){
		//id: row[5]
		js_data.load_stock_list(row[5]);
		js_data.get_code_div(row[0],row[5]).width($(this).width()).replaceAll(this);
	},
	load_stock_list: function(producto){
		loadFile(js_data.stock_view_url,'#src_stock_container','st_productos[]='+producto);
	},
	get_code_div: function(code,id){
		var div = $('<div>').addClass('inputtext').css({
			background:'#EEE',
			fontWeight:'bold'
		}).text(code)
		.append($('<input>').multiAttr({'type':'hidden','name':'prod_id'}).val(id))
		.mouseenter(function(){$(this).children('img').show();})
		.mouseleave(function(){$(this).children('img').hide();});
		
		var edbtn = $('<img>').multiAttr({
			src: 'images/editbtn.png',
			title: 'Modificar',
			alt: '[|]',
			class:'right hidden'
		}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
		.mouseout(function(){$(this).css({background:'',border:''})})
		.click(js_data.edit_code)
		.appendTo(div);
		
		
		return div;
	},
	edit_code: function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		
		js_data.prod_input.clone(true)
		.autocomplete(js_data.ac_url , js_data.ac_options)
		.result(js_data.ac_prod_result)
		.replaceAll(innerdiv)
		.trigger('focus');
		
		$('#src_stock_container').html('');
	},
	cant_change: function(){
		var cant = parseInt($(this).val());
		$(this).val(cant?cant:1);
	}
};

js_data.init();