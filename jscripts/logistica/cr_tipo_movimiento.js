/*$("#add_cuentas").click(function(){
	$("#lista_cuentas option:selected").remove().appendTo("#tipo_cuentas");
});*/
js_data = {
	cuentas: {},
	urlJSONcuentas: 'sites/logistica/proc/get_tipo_movimiento_cuentas.php',
	init: function(){
		this.rowCount = $('#tipo_cuentas').attr('size')*2+1;
		$('#tipo_mov_cont :button').css({fontSize:17});
		$('#tipo_cuentas').attr('size',this.rowCount);
		$(':radio[name=tipo_asiento]').change(this.toggleTipoMov);
		$('#tipo_tipo_mov').change(this.setTipoMov);
		$('#addDebe').bind('click',{src:'#tipo_cuentas',dest:'#tipo_debe'},this.moveCuenta);
		$('#rmvDebe').bind('click',{src:'#tipo_debe',dest:'#tipo_cuentas'},this.moveCuenta);
		$('#addHaber').bind('click',{src:'#tipo_cuentas',dest:'#tipo_haber'},this.moveCuenta);
		$('#rmvHaber').bind('click',{src:'#tipo_haber',dest:'#tipo_cuentas'},this.moveCuenta);
		$('#cr_tipo_movimiento').submit(this.formSubmit);
	},
	toggleTipoMov: function(){
		var v = $(this).val()=='1'?true:false;
		if(v)
			$('#tipo_mov_cont').show();
		else
			$('#tipo_mov_cont').hide();
		$('#tipo_tipo_mov').attr('required',v);
	},
	setTipoMov: function(){
		var v = parseInt($(this).val());
		$('#tipo_cuentas,#tipo_debe,#tipo_haber').find('option').remove();
		$('#tipo_mov_cont .multiSelectOptions').remove();
		if(v == 0){
			return;
		}
		if(js_data.cuentas[v]){
			js_data.setCuentas(v);
		}else{
			js_data.loadCuentas(v,js_data.setCuentas);
		}
	},
	loadCuentas: function(v,callback){
		show_loader();
		$.getJSON(js_data.urlJSONcuentas,{id:v},function(data){
			js_data.cuentas[v] = data;
			callback(v);
		});
		hide_loader();
	},
	setCuentas: function(v){
		var data = js_data.cuentas[v];
		var sel = $('<select>').multiAttr({multiple:true,name:'tipo_cuentas[]',size:js_data.rowCount});
		for(var i in data){
			$('<option>').val(data[i].dc_cuenta_contable).text(data[i].dg_cuenta_contable).appendTo(sel);
		}
		sel.replaceAll('#tipo_cuentas').attr('id','tipo_cuentas');
	},
	moveCuenta: function(e){
		$(e.data.src+" option:selected").remove().appendTo(e.data.dest);
	},
	formSubmit: function(){
		$('#tipo_debe,#tipo_haber').find('option').attr("selected",true);
	}
};
js_data.init();











