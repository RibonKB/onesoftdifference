var js_data = {
	
	acProductoURL: 'sites/proc/autocompleter/producto.php',
	acNotaVentaURL: 'sites/proc/autocompleter/nota_venta_strict.php',
	acOrdenServicioURL: 'sites/proc/autocompleter/orden_servicio_strict.php',
	acProductoOptions: {
		formatItem: function(row){
			return row[0]+" ( "+row[1]+" ) "+row[2];
		},
		minChars: 2,
		width: 300
	},
	
	tipoAjusteMessage: ['Bodega Salida','Bodega Entrada'],
	
	init: function(){
		$('#dg_producto').autocomplete(js_data.acProductoURL , js_data.acProductoOptions).result(js_data.acProductoResult);
		
		$('#dq_nota_venta').autocomplete(js_data.acNotaVentaURL,js_data.acProductoOptions).result(js_data.acNotaVentaResult);
		$('#dq_orden_servicio').autocomplete(js_data.acOrdenServicioURL,js_data.acProductoOptions).result(js_data.acOrdenServicioResult);
		
		$(':radio[name=dm_tipo_ajuste]').click(js_data.changeTipoAjuste).filter(':checked').trigger('click');
	},
	
	acProductoResult: function(e,row){
		
		js_data.acProductoInput = $(this);
		$(':hidden[name="dc_producto"]').val(row[5]);
		
		var div = js_data.createEditableDiv(row[0],js_data.acProductoChangeCodigo)
		.width($(this).width())
		.replaceAll(this);
		
	},
	
	createEditableDiv: function(text,editAction){
		var ed_btn = $('<img>').multiAttr({
			class: 'right hidden',
			src: 'images/editbtn.png',
			alt: '[|]',
			title: 'Editar código',
			width: '13',
			height: '13'
		}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
		.mouseout(function(){$(this).css({background:'',border:''})})
		.click(editAction);
		
		var div = $('<div>').css({
			border:'1px solid #CCC',
			background:'#EEE',
			padding:'5px',
			fontWeight:'bold'
		}).mouseenter(function(){$(this).children('img').show();})
		.mouseleave(function(){$(this).children('img').hide();})
		.text(text)
		.prepend(ed_btn);
		
		return div;
	},
	
	acProductoChangeCodigo: function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		js_data.acProductoInput
			.autocomplete(js_data.acProductoURL,js_data.acProductoOptions)
			.result(js_data.acProductoResult)
			.replaceAll(innerdiv);
	},
	
	acNotaVentaResult: function(e,row){
		
		js_data.acNotaVentaInput = $(this);
		$(':hidden[name="dc_nota_venta"]').val(row[3]);
		
		var div = js_data.createEditableDiv(row[0],js_data.acNotaVentaChangeCodigo)
		.width($(this).width())
		.replaceAll(this);
		
	},
	
	acNotaVentaChangeCodigo: function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		js_data.acNotaVentaInput
			.autocomplete(js_data.acNotaVentaURL,js_data.acProductoOptions)
			.result(js_data.acNotaVentaResult)
			.replaceAll(innerdiv);
	},
	
	acOrdenServicioResult: function(e,row){
		
		js_data.acOrdenServicioInput = $(this);
		$(':hidden[name="dc_orden_servicio"]').val(row[3]);
		
		var div = js_data.createEditableDiv(row[0],js_data.acOrdenServicioChangeCodigo)
		.width($(this).width())
		.replaceAll(this);
		
	},
	
	acOrdenServicioChangeCodigo: function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		js_data.acOrdenServicioInput
			.autocomplete(js_data.acOrdenServicioURL,js_data.acProductoOptions)
			.result(js_data.acOrdenServicioResult)
			.replaceAll(innerdiv);
	},
	
	changeTipoAjuste: function(){
		var tipo = $(this).val();
		if(tipo == -1)
			tipo = 0;
		$('#tipo_bodega').text(js_data.tipoAjusteMessage[tipo]);
	}
	
};
js_data.init();
