var js_data = {
	dq_gd: '#dq_guia_despacho',
	init: function(){
		$('.det_delete').click(function(){
			$(this).parent().parent().remove();
		});
		
		$('.det_reset').click(js_data.reset_detail);
		
		$('#select_all_detail').click(js_data.select_all_detail);
		$('.selected_item').click(js_data.select_simple_detail);
		
		
		$(js_data.dq_gd).change(js_data.gd_change);
		
		js_data.gd_input = $(js_data.dq_gd).clone(true);
	},
	
	//en caso de que seleccione un detalle para ser devuelto
	select_simple_detail: function(){
		if($(this).attr('checked'))
			js_data.active_prod.call(this);
		else
			js_data.deactive_prod.call(this);
	},
	
	//en caso que se presione el checkbox que selecciona todos los detalles
	select_all_detail: function(){
		if($(this).attr('checked'))
			$('#prod_list .selected_item:not(:checked)').each(function(){
				$(this).attr('checked',true);
				js_data.active_prod.call(this);
			});
		else
			$('#prod_list .selected_item:checked').each(function(){
				$(this).attr('checked',false);
				js_data.deactive_prod.call(this);
			});
	},
	
	//en caso de activar un checkbox de detalle
	active_prod: function(){
		var tr = $(this).parent().parent();
		tr.find('td').addClass('dev_active_prod');
		tr.find(':text,select,input:hidden').removeAttr('disabled');
	},
	
	//en caso de desactivar un checkbox de detalle
	deactive_prod: function(){
		var tr = $(this).parent().parent();
		tr.find('td').removeClass('dev_active_prod');
		tr.find(':text,select,input:hidden').attr('disabled','disabled');
	},
	
	//Si el campo de texto de guía de despacho cambia
	gd_change: function(){
		var value = parseInt($(this).val());
		
		if(!value){
			$(this).val('');
			return;
		}
		
		$(js_data.dq_gd).attr('disabled',true);
		
		$.getJSON('sites/logistica/guia_despacho_proveedor/proc/get_devolucion_doc_data.php',{mode:'gd',number:value},function(data){
                        
                    if(data == '<not-found>'){
				show_error('No se ha encontrado una guía de despacho con el número especificado');
				$(js_data.dq_gd).attr('disabled',false);
				$(js_data.dq_gd).val('');
				return;
			}
			
			js_data.get_code_div({
				code: value,
				doc_id_name: 'dc_guia_despacho',
				doc_id: data.dc_guia_despacho,
				edit_action: js_data.edit_gd,
                                width: $(js_data.dq_gd).width()
                                
			}).replaceAll(js_data.dq_gd);
			
			
			js_data.set_detail(data.dc_guia_despacho);
		});
	},
	
	//Si el campo de texto de orden de servicio cambia
	os_change: function(){
		
	},
	
	//Si el campo de texto de nota de venta cambia
	nv_change: function(){
		
	},
	
	//En caso de presionar el botón de modificar Guía de despacho
	edit_gd: function(){
		alert("╔═══╗ ♪\n\
║███║ ♫\n\
║ (●) ♫\n\
╚═══╝♪♪ ");
	},
	
	//Obtiene el detalle para la guía de despacho dada y lo agrega en el contenedor de detalle del formulario
	set_detail: function(guia){
		show_loader();
		$.getJSON('sites/logistica/guia_despacho_proveedor/proc/get_devolucion_gd_detalle.php',{id: guia},function(data){
			if(data == '<empty>'){
				show_aviso('No se ha encontrado detalles válidos en la guía de despacho para devolución.');
				return;
			}
			$('#detail_info').hide();
			$('#prod_table').show();
			var detalle = data;
			for(i in detalle){
				js_data.get_valid_detail(detalle[i]).appendTo('#prod_list');
			}
		});
		hide_loader();
	},
	
	//Recibe un detalle y lo formatea para insertarlo en la tabla de 
	get_valid_detail: function(det){
		var tr = $('#det_template tr.main').clone(true);
		tr.find('.det_name').append('<b>'+det.dg_producto+'</b><br />'+det.dg_descripcion);
		tr.find('.det_serie').text(det.dg_serie);
		tr.find('.det_cantidad,.det_max_value').val(det.dq_cantidad);
		tr.find('.det_gd_detail').val(det.dc_detalle);
		tr.find('.det_doc_detail').val(det.dc_detalle_nota_venta);
		tr.find('.det_prod_id').val(det.dc_producto);
		tr.find('.prod_serie').val(det.dg_serie);
		tr.find('.det_bodega_entrada').val(det.dc_bodega_salida);
		tr.find('.det_bodega_salida').val(det.dc_bodega_entrada);
		
		return tr;
	},
	
	/**
	*	Genera un cuadro con un código editable por la función parámetro
	*	data ~ {code, doc_id_name, doc_id, edit_action}
	**/
	get_code_div: function(data){
		var div = $('<div>').addClass('inputtext').css({
			background:'#EEE',
			fontWeight:'bold',
                        width: data.width+'px'
		}).text(data.code)
		.append($('<input>').multiAttr({'type':'hidden','name':data.doc_id_name}).val(data.doc_id))
		.mouseenter(function(){$(this).children('img').show();})
		.mouseleave(function(){$(this).children('img').hide();});
		
		var edbtn = $('<img>').multiAttr({
			src: 'images/editbtn.png',
			title: 'Modificar',
			alt: '[|]',
			class:'right hidden'
		}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
		.mouseout(function(){$(this).css({background:'',border:''})})
		.click(data.edit_action)
		.appendTo(div);
		
		return div;
	},
	
	//Restaura las cantidades asignadas, y la bodega de entrada por defecto del detalle
	reset_detail: function(){
		alert('Resetear detalle');
	}
};
js_data.init();