var js_data = {
	init: function(){
		$('#prov_region').removeAttr('name').change(this.changeRegion);
	},
	changeRegion: function(){
		var com = js_data.comunas[$(this).val()];
		$('#prov_cont_comuna').html('<option value="0"></option>');
		if($(this).val() && com){
			for(i in com){
				$('<option>').text(com[i][1]).val(com[i][0]).appendTo('#prov_cont_comuna');
			}
		}
	}
}