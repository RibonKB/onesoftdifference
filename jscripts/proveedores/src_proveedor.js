var js_data = {
	init: function(){
		pymerp.init();
		$('#valida_proveedor').click(function(e){
			e.preventDefault();
			if(confirm('¿Seguro que desea validar el proveedor?')){
				loadOverlay(this.href);
			}
		});
		$('#main_cont .panes').width('80%');
	},
	refreshProveedor: function(){
		loadFile('sites/proveedores/proc/src_proveedor.php?prov_rut='+this.prov_rut,'#src_proveedor_res');
	},
	changeRegion: function(){
		var com = js_data.comunas[$(this).val()];
		$('#prov_cont_comuna').html('<option value="0"></option>');
		if($(this).val() && com){
			for(i in com){
				$('<option>').text(com[i][1]).val(com[i][0]).appendTo('#prov_cont_comuna');
			}
		}
	}
};
js_data.init();