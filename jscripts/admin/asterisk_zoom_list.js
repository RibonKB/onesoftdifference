$.extend(js_data,{
	initZoom: function(){
		this.zoomPageCant = Math.ceil(this.zoomData.length/this.rpt);
		this.zoomTable = $('#res_list').clone().appendTo('#zoom_data');
		this.zoomTable.find('tbody').find('tr').remove();
		
		this.initZoomPages();
	},
	initZoomPages: function(){
		var pages = new Array();
		for(var i=0; i<this.zoomPageCant;i++)
			pages[i] = new Array();
		for(i in this.zoomData){
			pages[Math.floor(i/this.rpt)].push(this.zoomData[i]);
		}
		this.zoomData = pages;
		this.getPaginator(this.zoomPageCant,this.setZoomPage).insertAfter(this.zoomTable).clone(true).insertBefore(this.zoomTable).find('span').first().trigger('click');
	},
	setZoomPage: function(e){
		var cuerpo = js_data.zoomTable.find('tbody');
		cuerpo.find('tr').remove();
		var data = js_data.zoomData[e.data.id];
		for(var i in data){
			js_data.getTrData(data[i]).appendTo(cuerpo);
		}
	}
});