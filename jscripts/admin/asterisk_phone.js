String.prototype.lpad = function(padString, length) {
	var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

var js_data = {
	init: function(){
		this.pageCant = Math.ceil(this.data.length/this.rpt);
		this.table = $('#res_list');
		this.initIds();
		this.initPages();
		this.getPaginator(this.pageCant,this.setPage).insertAfter(this.table).clone(true).insertBefore(this.table).find('span').first().trigger('click');
		this.table.tableExport().tableAdjust(js_data.adjust);
	},
	getPaginator: function(cant,func){
		var div = $('<div>').addClass('center').css({margin:'5px',padding:'5px',display:'none'});
		var span = $('<span>').css({border:'1px solid #BBB',background:'#F6F6F6',color:'#444',padding:'4px',margin:'3px',cursor:'pointer'}).addClass('left');
		for(var i=0; i<cant; i++){
			span.clone().text(i+1).bind('click',{id:i},func).appendTo(div);
		}
		div.append($('<br>').addClass('clear'));
		return div;
	},
	initIds: function(){
		var ids = new Array();
		this.table.children('thead').find('th').each(function(){
			ids.push($(this).attr('id'));
		});
		this.ids = ids;
	},
	initPages: function(){
		var pages = new Array();
		for(var i=0; i<this.pageCant;i++)
			pages[i] = new Array();
		for(i in this.data){
			pages[Math.floor(i/this.rpt)].push(this.data[i]);
		}
		this.data = pages;
	},
	setPage: function(e){
		var cuerpo = js_data.table.find('tbody');
		cuerpo.find('tr').remove();
		var data = js_data.data[e.data.id];
		for(var i in data){
			js_data.getTrData(data[i]).appendTo(cuerpo);
		}
	},
	hourFormat: function(duration){
		var hours = Math.floor(duration/3600);
		var minutes = Math.floor((duration-hours*3600)/60);
		var seconds = Math.round(duration-hours*3600-minutes*60);
		return hours.toString().lpad('0',2)+':'+minutes.toString().lpad('0',2)+':'+seconds.toString().lpad('0',2);
	},
	getTrData: function(data){
		var tr = $('<tr>');
		var duration = data.duration;
		var average = data.time_avg;
		var a_src = data.src;
		var a_dst = data.dst;
		data.duration = js_data.hourFormat(duration);
		data.time_avg = js_data.hourFormat(average);
		data.src = js_data.getZoomAnchor('src='+a_src).text(a_src);
		data.dst = js_data.getZoomAnchor('dst='+a_dst).text(a_dst);
		for(var j in js_data.ids){
			if(data[js_data.ids[j]] == null)
				tr.append($('<td>').append('sin información'));
			else
				tr.append($('<td>').append(data[js_data.ids[j]]));
		}
		data.duration = duration;
		data.time_avg = average;
		data.src = a_src;
		data.dst = a_dst;
		return tr;
	},
	getZoomAnchor: function(data){
		return $('<a>').multiAttr({href: '#', title: 'Ver Detallado'}).click(function(e){
			e.preventDefault();
			loadOverlay('sites/admin/proc/asterisk_zoom_list.php?'+data+'&from='+js_data.fromDate+'&to='+js_data.toDate+'&type='+js_data.type+'&disp='+js_data.disposition);
		});
	}
}