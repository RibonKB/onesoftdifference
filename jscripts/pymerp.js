var pymerp = {
	
	/**
	*	SYSTEM FUNCTION
	**/
	
	init: function(context){
		
		if(!context)
			context = $(pymerp.contentContainer);
		else
			context = $(context);
			
		$('a.loader',context).click(function(e){
			e.preventDefault();
			pymerp.loadPage($(this).attr('href'));
		});
		
		$(".bicolor_tab tr:even td",context).css({"backgroundColor": "#E5E5E5"});
		
		$(".ac_results",context).remove();
		
		$('a.loadOnOverlay',context).click(function(e){
			e.preventDefault();
			pymerp.loadOverlay($(this).attr('href'));
		});
		
		//TO-HELL
		$(".tip",context).tooltip({position:'bottom center',effect:'slide'});
		
		
		window.setTimeout(function(){$("#tabs",context).tabs("div.tabpanes > div",{effect:'default'});},300);
		$(".manual_tab",context).tabs($("div.innertabpanes > div",context),{effect:'default'});
		
		$(".validar",context).unbind('submit').submit(function(e){
			e.preventDefault();
			id="#"+$(this).attr("id");
			pymerp.enviarForm(id,id+"_res");
		});
		
		$(".cValidar",context).submit(function(e){
			e.preventDefault();
			var id="#"+$(this).attr("id");
			pymerp.completeEnviarForm(id);
		});
		
		$(".confirmValidar",context).submit(function(e){
			e.preventDefault();
			var id = "#"+$(this).attr("id");
			pymerp.confirmEnviarForm(id,id+"_res");
		});
		
		$('.overlayValidar',context).submit(function(e){
			e.preventDefault();
			var id="#"+$(this).attr("id");
			pymerp.overlayEnviarForm(id,id+"_res");
		});
		
		$('.iFrameValidar',context).unbind('submit')
		.submit(function(e){
			var id="#"+$(this).attr("id");
			pymerp.iFrameEnviarForm(id,id+"_res",e);
		})
		.each(function(){
			var jForm = $(this);
			var fId = jForm.attr('id');
			var id = fId+"_frame";
			jForm.attr('target',id).attr('method','post');
			var frame = $('<iframe>');
			frame.attr('name',id).attr('id',id).addClass('hidden');
			frame.appendTo(context);
			frame.load(function(){
				var resId = fId+"_res";
				var res = frames[id].document.getElementsByTagName("body")[0].innerHTML;
				$('#'+resId).html(res);
				pymerp.hideLoader();
			});
		});
		
		$(".inputrut",context).Rut();
		
		$("input.date",context).dateinput(pymerp.dateFieldConfiguration);
		
		pymerp.initManualDate($("input.manual_date",context));
		
	},
	
	initManualDate: function(fields){
		fields.each(function(){
			/*var trigger = $('<a>').addClass('button').addClass('caltrigger');
			$(this).after(trigger);*/
			$(this)
			.dateinput($.extend({}, pymerp.dateFieldConfiguration,{ trigger: true }))
			.removeClass('date')
			.unbind('focus')
			.unbind('click')
			.unbind('keydown')
			.next('.caltrigger').addClass('button').attr('tabindex','9999');
			
			$(this).keydown(pymerp.ManualDateKeyDown);
			
		});
	},
	
	ManualDateKeyDown: function(e){
		var key = e.keyCode;
		if(!(key > 47 && key < 58) && !(key > 95 && key < 106) && key != 8 && key != 9 && key != 13){
			e.preventDefault();
			return;
		}
		if(key == 8 || key == 9 || key == 13){
			return;
		}
		var val = $(this).val().replace(/\//gi,'');
		var l = val.length;
		if(l >= 2){
			var res = '';
			res += val.substr(0,2)+'/';
			val = val.substr(2);
			if(l > 3){
				res += val.substr(0,2)+'/';
				val = val.substr(2);
			}
			$(this).val(res+val);
		}
	},
	
	/**
	* RESOURCES
	**/
	messages: {
		formActivateLabel: 'Volver a mostrar formulario',
		formRequiredError: 'Los campos marcados son obligatorios',
		formConfirmMsg: 'Está seguro que desea continuar?',
		
		ajaxFileError: 'Ha ocurrido un error al intentar acceder al recurso, intentelo de nuevo o contacte con un administrador del sistema',
		
		dialogErrorDefault: 'error',
		dialogAvisoDefault: 'Atención',
		dialogConfirmDefault: 'Atención'
	},
	
	miniLoader: $('<img>').multiAttr({
		'src': 'images/ajax-loader.gif',
		'alt': '[CARGANDO]'
	}),
	
	contentContainer: '#content',
	loader: $('#loader'),
	
	dateFieldConfiguration: {
		lang:'es',
		firstDay:1,
		format:'dd/mm/yyyy',
		selectors:true,
		initialValue:0,
		yearRange:[-80,80]
	},
	
	/**
	*	AJAX FILE LOADERS
	**/
	
	loadFile: function(url,target,parameter,callback){
		var q = '';
		if(url.lastIndexOf('?')+1){
			q = '&'+url.substring(url.lastIndexOf('?')+1);
			url = url.substring(0,url.lastIndexOf('?'));
		}
		
		$.ajax({
			url: url,
			type: 'POST',
			data: 'asinc=1'+q+'&'+parameter,
			error: function(){
				pymerp.showError(pymerp.messages.ajaxFileError);
			},
			success: function(data) {
				$(target).html(data);
				if(callback)
					callback();
			}
		});
	},
	
	loadPage: function(url,parameter){
		js_data = null;
		if(!parameter)
			parameter = '';
		pymerp.showLoader();
		pymerp.loadFile(url,pymerp.contentContainer,parameter,function(){
			$('input[type="text"]',$(pymerp.contentContainer)).first().trigger('focus');
			pymerp.init();
			pymerp.hideLoader();
		});
	},
	
	loadOverlay: function(url,parameter,multiple){
		var ov = $('<div>').attr("class","overlay").appendTo(pymerp.contentContainer);
		
		if(!multiple){
			$("#genOverlay").remove();
			ov.attr("id","genOverlay");
		}
		
		pymerp.showLoader();
		pymerp.loadFile(url,ov,parameter,function(){
			ov.overlay({load: true, fixed:false, closeOnClick:false, oneInstance: false});
			ov.find('.close').click(function(){
				ov.remove();
			});
			pymerp.init(ov);
			pymerp.hideLoader();
		});
	},

	objectOverlay: function(object){
		
		$(object).overlay();
		var api = $(object).data("overlay");
		api.load();
		
	},

	/**
	*	LOADER FUNCTIONS
	**/
	showLoader: function(){
		pymerp.loader.show().expose({
			loadSpeed	:0,
			closeOnClick:false,
			color		:'#F4F4F4',
			closeSpeed	:0,
			closeOnEsc	:false,
			zIndex		:99999
		});
	},
	
	hideLoader: function(){
		pymerp.loader.hide();
		$.mask.close();
	},
	
	/**
	*	ALERTS FUNCTIONS
	**/
	
	showError: function(message){
		pymerp.showAlert(message, pymerp.messages.dialogErrorDefault, 'warning');
	},
	
	showAviso: function(message){
		pymerp.showAlert(message, pymerp.messages.dialogAvisoDefault, 'alert');
	},
	
	showConfirm: function(message){
		pymerp.showAlert(message, pymerp.messages.dialogConfirmDefault, 'confirm');
	},
	
	showAlert: function(m,title,type){
		$("#overlay_alert_container").remove();
		$('<div>').attr("class","overlay").attr("id","overlay_alert_container").appendTo(pymerp.contentContainer);
		$('<div>'+title+'</div>').attr("class","secc_bar").appendTo("#overlay_alert_container");
		$("<br /><div class='"+type+"'>"+m+"</div><br />").appendTo("#overlay_alert_container");
		$("<button class='button right'>Cerrar</button>").click(function(){$("#overlay_alert_container").remove(); $.mask.close();}).appendTo("#overlay_alert_container");
		$("#overlay_alert_container").overlay({load: true, fixed:false, closeOnClick: false, oneInstance: false, mask: {color: '#ddd'}});
		$("#loader").hide();
	},
	
	confirmDialog: function(message,callback){
		if(confirm(message)){
			callback();
		}
	},
	
	/**
	*	FORM FUNCTIONS
	**/
	disableForm: function(form){
		var jForm = $(form);
		jForm.find('input').trigger('blur');
		jForm.slideUp('fast');
		
		
		var button = $('<input>').multiAttr({
			'type': 'button',
			'class': 'button',
			'value': pymerp.messages.formActivateLabel
		}).click(function(){
			pymerp.enableForm(form,this);
		});
		
		jForm.before(button);
	},
	
	enableForm: function(form,button){
		var jForm = $(form);
		jForm.slideDown('fast');
		$(button).slideUp("fast",function(){ $(this).remove(); });
	},
	
	validarForm: function(form){
		var valid = true;
		var jForm = $(form);
		$('.invalid',jForm).removeClass('invalid');
		$('select[required]',jForm).each(function(){
			var jSelect = $(this);
			if((!jSelect.val())||(jSelect.val() == '0')){
				valid = false;
				jSelect.addClass("invalid");
			}
		});
		$('input[type="text"], input.date, input[type="password"], textarea, .manual_date',jForm)
		.filter('[required]')
		.each(function(){
			var jText = $(this);
			if(jText.val().length < 1){
				valid = false;
				jText.addClass("invalid");
			}
		});
		$('input.manual_date',jForm).each(function(index, element) {
            var jText = $(this);
			if(jText.attr('required')){
				if(jText.val().length != 10){
					valid = false;
					jText.addClass("invalid");
				}
			}
			if(!pymerp.esFechaValida(jText.val())){
				valid = false;
				jText.addClass("invalid");
			}
        });
		
		return valid;
	},
	
	enviarForm: function(form,target){
		if(!pymerp.validarForm(form)){
			pymerp.showError(pymerp.messages.formRequiredError);
			return;
		}
		
		pymerp.showLoader();
		
		var jForm = $(form);
		//var data = jForm.find(':input').serialize();
		var data = jForm.serialize();
		var action = jForm.attr('action');
		pymerp.disableForm(form);
		
		$(target).html('').append(pymerp.miniLoader.clone());
		
		pymerp.loadFile(action,target,data,function(){ pymerp.hideLoader(); });
		
	},
	
	completeEnviarForm: function(form){
		if(!pymerp.validarForm(form)){
			pymerp.showError(pymerp.messages.formRequiredError);
			return;
		}
		
		var jForm = $(form);
		var data = jForm.serialize();
		var action = jForm.attr('action');
		
		pymerp.loadPage(action,data);
	},
	
	overlayEnviarForm: function(form){
		if(!pymerp.validarForm(form)){
			pymerp.showError(pymerp.messages.formRequiredError);
			return;
		}
		
		var jForm = $(form);
		var data = jForm.serialize();
		var action = jForm.attr('action');
		
		pymerp.loadOverlay(action,data);
	},
	
	confirmEnviarForm: function(form,target){
		pymerp.confirmDialog(pymerp.messages.formConfirmMsg,function(){
			pymerp.enviarForm(form,target);
		});
	},
	
	iFrameEnviarForm: function(form, target, event){
		if(!pymerp.validarForm(form)){
			event.preventDefault();
			pymerp.showError(pymerp.messages.formRequiredError);
			return;
		}
		pymerp.disableForm(form);
		pymerp.showLoader();
	},
	
	/*
	*	NUMERIC FUNCTIONS
	*/
	
	milFormat: function(numero){
		var n = numero.toString();
		if(n.indexOf('.') != -1){
			var arr = n.split('.');
			var number = arr[0];
			var decimal = '.'+arr[1];
		}else{
			var number = n;
			var decimal = '';
		}
		var result = '';
		while( number.length > 3 )
		{
			result = ',' + number.substr(number.length - 3) + result;
			number = number.substring(0, number.length - 3);
		}
		result = number + result + decimal;
		return result;
	},
	
	toNumber: function(num){
		return parseFloat(num.replace(/,/g,''));
	},
	
	disableButton: function(button){
		$(button).attr('disabled',true).addClass('disbutton');
	},
	
	enableButton: function(button){
		$(button).attr('disabled',false).removeClass('disbutton');
	},
	
	confirm: function(message,onYes,onCancel){
		if(confirm(message))
			onYes();
		else{
			if(onCancel)
				onCancel();
		}
	},
	
	getEditableBox: function(content, editAction){
        var div = $('<div>').addClass('editableBox');
        
        $('<img>').attr({
            src:'images/editbtn.png',
            alt:'[|]',
            title:'editar'
        }).addClass('right')
        .click(editAction)
        .appendTo(div);
        
        div.append($('<b>').text(content));
        
        return div;
    },
	
	esFechaValida: function(fecha){
	    if (fecha != "" ){
	        if (!/^\d{2}\/\d{2}\/\d{4}$/.test(fecha)){
	            return false;
	        }
	        var dia  =  parseInt(fecha.substring(0,2),10);
	        var mes  =  parseInt(fecha.substring(3,5),10);
	        var anio =  parseInt(fecha.substring(6),10);
			var numDias;
	 
		    switch(mes){
		        case 1:
		        case 3:
		        case 5:
		        case 7:
		        case 8:
		        case 10:
		        case 12:
		            numDias=31;
		            break;
		        case 4: case 6: case 9: case 11:
		            numDias=30;
		            break;
		        case 2:
		            if (pymerp.comprobarSiBisisesto(anio)){ numDias=29 }else{ numDias=28};
		            break;
		        default:
		            return false;
		    }
	 
	        if (dia>numDias || dia==0){
	            return false;
	        }
	        return true;
	    }else{
			return true;
		}
	},
	
	comprobarSiBisisesto: function(anio){
		if ( ( anio % 100 != 0) && ((anio % 4 == 0) || (anio % 400 == 0))) {
		    return true;
		    }
		else {
		    return false;
		    }
	},
    
    //Implementacion del método incluido en jquery a partir de la versión 1.7, debido a que la utilizada
    //en Onesoft es la 1.2
    isNumeric: function( obj ) {
        return !jQuery.isArray( obj ) && (obj - parseFloat( obj ) + 1) >= 0;
    }
	
};

$(document).ready(function(){

	$.tools.dateinput.localize("es", {
	   months: 		'Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre',
	   shortMonths:	'Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Sep,Oct,Nov,Dic',
	   days:		'Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado',
	   shortDays:	'Dom,Lun,Mar,Mie,Jue,Vie,Sab'
	});
	
	$('.header_menu_item a').toggle(function(){
		$('.header_menu_item div').hide();
		$(this).next('div').show();
	},function(){
		$('.header_menu_item div').hide();
	});
	
	if (typeof window.history.pushState == 'function') {
		$(".accordion ul a").click(function(e){
			e.preventDefault();
			//window.history.pushState({ path: '/index.php' }, '/index.php', '/index.php');
			var url = $(this).attr('href');
			pymerp.loadPage(url);
			//window.history.pushState({ path: this.href }, this.href, this.href);
			$(".acc_active").removeClass("acc_active");
			$(this).addClass("acc_active");
		});
	}else{
		$(".accordion ul a").click(function(e){
			e.preventDefault();
			pymerp.loadPage(this.href);
			$(".acc_active").removeClass("acc_active");
			$(this).addClass("acc_active");
		});
	}
	
	$(".accordion").tabs(".accordion > div.pane", {tabs: '> h2', effect: 'slide', initialIndex: null, toggle: true});
	$(".group_index").tabs(".group_index > div.ind_group_body", {tabs: '> .ind_group_head', effect: 'slide', initialIndex: null, toggle: true});
	
	pymerp.init();
});