var js_data = {
	init: function(){
		$('a.loadOnOverlay').click(function(e){
			e.preventDefault();
			loadOverlay(this.href);
		});
		$('#valida_cliente').click(function(e){
			e.preventDefault();
			if(confirm('¿Seguro que desea validar el cliente?')){
				loadOverlay(this.href);
			}
		});
		$('#main_cont .panes').width('80%');
		$('.show_contactos').click(function(){
			$(this).parent().find('.cont_body').toggle();
		});
	},
	editInit: function(){
		$('#cli_pago').multiSelect({
			selectAll: true,
			selectAllText: "Seleccionar todos",
			noneSelected: "---",
			oneOrMoreSelected: "% seleccionado(s)"
		});
		
		$('#cli_rut').Rut();
		
		$('#cli_linea').change(function(){
			var parsed = parseFloat($(this).val());
			if(parsed)
				$(this).val(parsed);
			else
				$(this).val('0');
		});
	},
	editSucInit: function(){
		
	},
	refreshClient: function(){
		loadFile('sites/clientes/proc/src_cliente.php?cli_rut='+this.cli_rut,'#src_cliente_res');
	},
	changeRegion: function(){
		var com = js_data.comunas[$(this).val()];
		$('#cli_suc_comuna').html('<option value="0"></option>');
		if($(this).val() && com){
			for(i in com){
				$('<option>').text(com[i][1]).val(com[i][0]).appendTo('#cli_suc_comuna');
			}
		}
	}
};
js_data.init();