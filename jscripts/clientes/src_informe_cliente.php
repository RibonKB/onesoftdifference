<?php
define("MAIN",1);
require_once("../../inc/global.php");
require_once("../../inc/form-class.php");
$form = new Form($empresa);
?>
<div id="secc_bar">
	Informe de clientes
</div>
<div id="main_cont"><div class="panes">
	<?php
		$form->Start('sites/clientes/proc/src_informe_cliente.php');
		$form->Header("Indique los filtros que se aplicarán al informe de clientes<br />
		Los datos arcados con [*] son obligatorios.");
	?>
	<table class="tab" style="text-align:left;" id="form_container" width="100%">
		<tr>
			<td width="50">Fecha de emisión</td>
			<td width="280">
				<?php $form->Date('Desde','gd_emision_desde',1,"01/".date("m/Y")); ?>
			</td>
			<td width="280">
				<?php $form->Date('Hasta','gd_emision_hasta',1,0); ?>
			</td>
		</tr>
	</table>
</div></div>