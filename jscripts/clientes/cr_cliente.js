var js_data = {
	init: function(){
		$('#dc_medio_pago').multiSelect({
			selectAll: true,
			selectAllText: "Seleccionar todos",
			noneSelected: "---",
			oneOrMoreSelected: "% seleccionado(s)"
		});
		
		$('#dc_region').removeAttr('name').change(this.changeRegion);
		$('#dq_linea_credito').change(this.changeLinea);
		$('#setup_horario_atencion').click(js_data.setupHorarios);
	},
	changeRegion: function(){
		var com = js_data.comunas[$(this).val()];
		$('#dc_comuna').html('<option value="0"></option>');
		if($(this).val() && com){
			for(i in com){
				$('<option>').text(com[i][1]).val(com[i][0]).appendTo('#dc_comuna');
			}
		}
	},
	
	changeLinea: function(){
		var parsed = parseFloat($(this).val());
		if(parsed)
			$(this).val(parsed);
		else
			$(this).val('0');
	},
	
	setupHorarios: function(){
		var data = $(':input',$('#horario_atencion_data')).serialize();
		pymerp.loadOverlay('sites/clientes/cr_horario_atencion.php?',data,true);
	}
}