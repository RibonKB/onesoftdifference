var js_data = {
	cli_ac_data: ['sites/proc/autocompleter/cliente.php',{formatItem: function(row){ return row[1]+" ("+row[0]+") "+row[2]; },minChars: 2,width:300}],
	cli_contacto_data: [],
	init: function(){
		this.cli_input = $('#ac_cliente').autocomplete(this.cli_ac_data[0],this.cli_ac_data[1]).result(this.cli_ac_result).clone();
		$('#cont_contacto').attr('disabled',true);
	},
	cli_ac_result: function(e,row){
		js_data.getDivLabel(row[1],js_data.cl_ac_edit).width($(this).width())
		.append($('<input>').multiAttr({type: 'hidden',name:'cont_cliente'}).val(row[3]))
		.replaceAll(this);
		js_data.getContactos(row[3]);
	},
	cl_ac_edit: function(){
		var parent = $(this).attr('src','images/ajax-loader.gif').unbind('click').parent();
		js_data.cli_input.val('')
		.autocomplete(js_data.cli_ac_data[0],js_data.cli_ac_data[1])
		.result(js_data.cli_ac_result)
		.replaceAll(parent)
		.focus();
		var sel = $('#cont_contacto').html('').attr('disabled',true);
	},
	getDivLabel: function(text,editAction){
		
		var edbtn = $('<img>').multiAttr({
			src: 'images/editbtn.png',
			title: 'Modificar',
			alt: '[|]',
			class:'right hidden'
		}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
		.mouseout(function(){$(this).css({background:'',border:''})}).click(editAction);
		
		var div = $('<div>').addClass('inputtext').css({
			background:'#EEE',
			fontWeight:'bold'
		}).html(text)
		.mouseenter(function(){$(this).children('img').show();})
		.mouseleave(function(){$(this).children('img').hide();})
		.prepend(edbtn);
		
		return div;
	},
	getContactos:function(id){
		if(this.cli_contacto_data[id]){
			var sel = $('#cont_contacto').html('').attr('disabled',false);
			sel.append($('<option>'));
			for(i in this.cli_contacto_data[id]){
				sel.append($('<option>').attr('value',this.cli_contacto_data[id][i].dc_contacto).text(this.cli_contacto_data[id][i].dg_contacto));
			}
			return;
		}
		$.getJSON('sites/contratos/proc/get_contactos_by_client.php',{ cli_id: id },function(data){
			if(data == '<empty>'){
				show_error("Contactos no encontrados");
				return;
			}
			js_data.cli_contacto_data[id] = data;
			js_data.getContactos(id);
		});
	}
};
js_data.init();