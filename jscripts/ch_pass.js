loadFile = function(l,t,p){
	$.ajax({
  url: l,
  type: 'POST',
  data: 'asinc=1&'+p,
  error: function(){
	alert("A ocurrido un error al intentar acceder al recurso, intentelo denuevo");
  },
  success: function(data) {
	  $(t).html(data);
  }
});
}

$(document).ready(function(){
	$("#ch_pass").submit(function(e){
		e.preventDefault();
		v=1;
		$("#ch_pass input[type=text],#ch_pass input[type=password]").each(
			function(){
				if($(this).val().length == 0){
					v=0;
					$(this).addClass("invalid");
				}
				else{
					$(this).removeClass("invalid");
				}
		});
		
		if(v){
			if($("input[name=pass1]").val() == $("input[name=pass2]").val()){
				ser = "";
				$("#ch_pass input[type=password]").each(function(){
					ser += this.name+"="+encodeURIComponent(hex_md5(this.value))+"&";
				});
				loadFile("sites/cambiar_pass.php","#log_results",ser);
			}
			else{
				$("#log_results").html('<div class="warning">Las contrase&ntilde;as ingresadas no son iguales</div>');
			}
		}else{
			$("#log_results").html('<div class="warning">Los campos marcados son obligatorios</div>');
		}
		
	});
});