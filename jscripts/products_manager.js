var tc;

$("#prod_tipo_cambio").change(function(){
	indice = this.selectedIndex;
	if(indice == 0){
		$("#prod_info").show();
		$("#prods").hide();
	}else{
		$("#prod_info").hide();
		$("#prods").show();
		tc = this.options[indice].innerHTML.split("|");
		tc = toNumber(tc[1]);

		$("#prod_list tr").each(function(){

			cant = $(this).find('.prod_cant').val();
			if(cant){
				loc = $(this).find('.pr1').val();
				$(this).find('.pr2').val((loc/tc).toFixed(2));
				$(this).children('.to2').html((loc*cant/tc).toFixed(2));
			}
		});

	}
});

$("#prod_add").click(function(){
	el = $(document.createElement("tr"));
	$("<td style=\"position:absolute;\"></td>")
	.append("<img src='images/delbtn.png' alt='' title='Eliminar' class='out_opt' />")
	.append("<input type='text' name='prod[]' class='prod_codigo inputtext' size='7' required='required' />")
	.appendTo(el);
	$("<td class='desc'>-</td>").appendTo(el);
	$("<td></td>")
	.append("<input type='text' name='cant[]' class='prod_cant inputtext' size='5' readonly='readonly' />")
	.appendTo(el);
	$("<td><input type='text' name='precio[]' value='0' class='pr1 inputtext' size='5' readonly='readonly' /></td>").appendTo(el);
	$("<td class='to1'>0</td>").appendTo(el);
	$("<td><input type='text' value='0' class='pr2 inputtext' size='5' readonly='readonly' /></td>").appendTo(el);
	$("<td class='to2'>0</td>").appendTo(el);

	$("<td class='co1'>0</td>").appendTo(el);
	$("<td class='co2'>0</td>").appendTo(el);
	$("<td class='cto1'>0</td>").appendTo(el);
	$("<td class='cto2'>0</td>").appendTo(el);
	$("<td class='ma1'>0</td>").appendTo(el);
	$("<td class='ma2'>0</td>").appendTo(el);
	$("<td class='ma3'>0</td>").appendTo(el);
	el.appendTo("#prod_list");

	prod_init();
});

prod_init = function(){
	$(".prod_codigo").autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(function(e,row){
		el = $(this).parent().next();
		cant = el.next().children("input").attr("readonly",false);
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}
		cambio = $("#prod_tipo_cambio :selected").text().split("|");
		cambio = cambio[1];

		el.html(row[1]).next().next().children('.pr1').val(row[3]).attr("readonly",false)
		.parent().next().html((row[3]*cant).toFixed(2))
		.next().children('.pr2').val((row[3]/cambio).toFixed(2)).attr("readonly",false)
		.parent().next().html((row[3]*cant/cambio).toFixed(2)).next().html(row[4])
		.next().html((row[4]/cambio).toFixed(2)).next().html((row[4]*cant).toFixed(2))
		.next().html((row[4]*cant/cambio).toFixed(2)).next().html(((row[3]-row[4])*cant).toFixed(2))
		.next().html((((row[3]-row[4])/cambio)*cant).toFixed(2)).next().html((100-(row[4]*100/row[3])).toFixed(2));
	});

	$(".out_opt").click(function(){
		$(this).parent().parent().remove();
	});

	$(".prod_cant").change(function(){
		if(cant = parseFloat($(this).val())){
			$(this).val(cant);
		}else{
			$(this).val(1);
			cant = 1;
		}
		el = $(this).parent().next();
		loc = el.children('.pr1').val();
		el.next().html((loc*cant).toFixed(2)).next().next().html((loc*cant/tc).toFixed(2));
	});

	$(".pr1").change(function(){
		if(pr = parseFloat($(this).val())){
			$(this).val(pr.toFixed(2));
		}else{
			pr = 1;
			$(this).val(pr.toFixed(2));
		}
		cant = $(this).parent().prev().children('.prod_cant').val();
		$(this).parent().next().html((cant*pr).toFixed(2))
		.next().children('.pr2').val((pr/tc).toFixed(2))
		.parent().next().html((pr*cant/tc).toFixed(2));

	});

	$(".pr2").change(function(){
		if(pr = parseFloat($(this).val())){
			$(this).val(pr.toFixed(2));
		}else{
			pr = 1;
			$(this).val(pr.toFixed(2));
		}
		cant = $(this).parent().parent().find('.prod_cant').val();
		$(this).parent().parent().find('.pr1').val((tc*pr).toFixed(2))
		.parent().next().html((tc*pr*cant).toFixed(2))
		.next().next().html((pr*cant).toFixed(2));

	});

}

prod_init();
