loadFile = function(l,t,p){
	$.ajax({
  url: l,
  type: 'POST',
  data: 'asinc=1&'+p,
  error: function(){
	alert("A ocurrido un error al intentar acceder al recurso, intentelo denuevo");
  },
  success: function(data) {
	  $(t).html(data);
  }
});
}

show_alert = function(m,title,type){
	$("#overlay_alert_container").remove();
	$('<div>').attr("class","overlay").attr("id","overlay_alert_container").appendTo("#content");
	$('<div>'+title+'</div>').attr("class","secc_bar").appendTo("#overlay_alert_container");
	$("<br /><div class='"+type+"'>"+m+"</div><br />").appendTo("#overlay_alert_container");
	$("<button class='button right'>Cerrar</button>").click(function(){$("#overlay_alert_container").remove(); $.mask.close();}).appendTo("#overlay_alert_container");
	$("#overlay_alert_container").overlay({load: true, fixed:false, closeOnClick: false, mask: {color: '#ddd'}});
	$("#loader").hide();
}

$(document).ready(function(){
	
	var change_rut = function(){
		var rut = $(this).val().replace(/\./g,'').split('-');
		var num = rut[0];
		var result = '';
		while( num.length > 3 )
		{
			result = '.' + num.substr(num.length - 3) + result;
		 	num = num.substring(0, num.length - 3);
		}
		result = num + result + '-' + rut[1];
		
		$.getJSON("sites/proc/load_empresa.php",{rut:result},function(data){
			
			if(data == "<not-found>"){
				show_alert('El RUT de la empresa indicado es inválido','ERROR','warning');
				$('#log_rut').val('');
				return;
			}
			
			var div = $('<div>').width($('#log_rut').width()).css({
				background:'#66E41B',
				border:'1px solid #53A40B',
				padding:'10px',
				fontWeight: 'bold',
				textAlign: 'left',
				borderRadius: '5px'
			}).attr('id','empresa_razon').text(data.dg_razon).replaceAll('#log_rut');
			
			$('<img>').multiAttr({
				src:'images/editbtn.png',
				alt:'[|]',
				title: 'Modificar',
				'class':'right'
			}).prependTo(div).click(function(){
				$('<input>').attr({
					type:'text',
					'class': 'inputtext',
					id:'log_rut',
					name:'log_rut'
				}).replaceAll('#empresa_razon').change(change_rut);
			});
			
			$('<input>').multiAttr({
				type:'hidden',
				name:'empresa_id',
				value: data.dc_empresa
			}).appendTo(div);
		})
	}
	
	var v = $('#log_rut').change(change_rut).val();
	if(v != ''){$('#log_rut').change();}
	
	$("#log_form").submit(function(e){
		e.preventDefault();
		v=1;
		if($(':hidden[name=empresa_id]').size() == 0){
			$('#log_rut').addClass('invalid');
			return;
		}
		$("#log_form input[type=text],#log_form input[type=password]").each(
			function(){
				if($(this).val().length == 0){
					v=0;
					$(this).addClass("invalid");
				}
				else{
					$(this).removeClass("invalid");
				}
		});
		if(v){
			ser = "";
			$("#log_username").each(function(){
					ser += this.name+"="+encodeURIComponent(this.value)+"&";
			});
			$(":hidden[name=empresa_id]").each(function(){
					ser += this.name+"="+encodeURIComponent(this.value)+"&";
			});
			ser += "log_password="+encodeURIComponent(hex_md5($("#log_password").val()));
			loadFile("sites/login.php","#log_results",ser);
		}else{
			$("#log_results").html('<div class="warning">Los campos marcados son obligatorios</div>');
		}
	});
});