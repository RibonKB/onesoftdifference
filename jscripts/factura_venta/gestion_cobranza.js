$.extend(js_data,{
	doc_container: $('<div>').css({background:'#66E41B',border:'1px solid #53A40B',padding:'3px',fontWeight:'bold',margin:'2px'}),
	max_mount: parseFloat($(':hidden[name=gestion_monto_maximo]').val()),
	init: function(){
		$('#gestion_compromiso,#gestion_prox_gestion').dateinput({
			lang:'es',
			firstDay:1,
			format:'dd/mm/yyyy',
			selectors:true,
			initialValue:0,
			yearRange:[-80,80]
		});
		
		$('<img>').multiAttr({src:'images/delbtn.png',alt:'(x)',width:15}).click(function(){
			$(this).parent().remove();
		}).appendTo(js_data.doc_container);
		
		//init individual
		$('#gestion_monto').change(js_data.ch_monto).trigger('change');
		
		$('#add_num_doc').click(js_data.add_document);
		
		$('<iframe>').attr('name','callframe').addClass('hidden').appendTo('#genOverlay');
		
		$('#gestion_contacto').change(js_data.ch_contacto);
		
		//init individual
		$('#gestion_modo :radio').click(js_data.enableCobro);
	},
	
	//init individual
	enableCobro: function(){
		var mode = $(this).val();
		if(mode == 0){
			$('#cobros_container').addClass('hidden');
			$('#gestion_monto').attr('readonly',false);;
		}else if(mode == 1){
			$('#cobros_container').removeClass('hidden');
			$('#gestion_monto').attr('readonly',false).val(0);
		}else{
			$('#cobros_container').removeClass('hidden');
			$('#gestion_monto').val($(':hidden[name=gestion_monto_maximo]').val()).trigger('change').attr('readonly',true);
		}
	},
	
	//init individual
	ch_monto: function(){
		var format = parseFloat($(this).val().replace(/,/g,''));
		if(format){
			if(format > js_data.max_mount)
				format = js_data.max_mount;
			$(this).val(mil_format(format));
		}else
			$(this).val(0);
	},
	add_document: function(){
		var doc = prompt('indique el número de documento');
		if(doc != ''){
			js_data.doc_container.clone(true).append(doc)
			.append($('<input type="hidden">').attr('name','gestion_docs[]').val(doc))
			.appendTo('#doc_container');
		}
	},
	ch_contacto: function(){
		var contacto = $(this).attr('disabled',true).val();
		var enabler = window.setTimeout(js_data.ena_contacto,2500);
		$.getJSON('sites/ventas/factura_venta/proc/get_contacto_data.php',{id:contacto},function(data){
			if(data == '<not-found>'){
				show_error('No se ha podido encontrar la información del contacto');
				return;
			}
			$('#contact_data').html('');
			window.clearInterval(enabler);
			js_data.ena_contacto();
			//<a target='callframe' class='right' href='http://192.168.0.202/phone/asterisk_call_trigger.php?{$query_data}'>
			var phone = $('<a>').multiAttr({
				href:'http://192.168.0.202/phone/asterisk_call_trigger.php?call='+data.dg_fono+'&ext='+data.anexo+'&razon='+data.dg_razon,
				target: 'callframe'
			}).addClass('right').html('<img src="images/call.png" alt="[C]" title="Llamar" />');
			var movil = $('<a>').multiAttr({
				href:'http://192.168.0.202/phone/asterisk_call_trigger.php?call='+data.dg_movil+'&ext='+data.anexo+'&razon='+data.dg_razon,
				target: 'callframe'
			}).addClass('right').html('<img src="images/call.png" alt="[C]" title="Llamar" />');
			var mail = $('<a>').multiAttr({
				href: 'mailto:'+data.dg_email
			}).addClass('right').html('<img src="images/mail.png" alt="[@]" title="Enviar Correo" />');
			$('#contact_data').css({backgroundColor:'#FFF',lineHeight:'25px',padding:'5px',border:'1px solid #BBB'});
			$('<div>').text('Información del contacto').addClass('info').appendTo('#contact_data');
			$('<div>').append('Nombre: ').append($('<b>').text(data.dg_contacto)).appendTo('#contact_data');
			$('<div>').append(phone).append('Telefono: ').append($('<b>').text(data.dg_fono)).appendTo('#contact_data');
			$('<div>').append(movil).append('Movil: ').append($('<b>').text(data.dg_movil)).appendTo('#contact_data');
			$('<div>').append(mail).append('e-mail: ').append($('<b>').text(data.dg_email)).appendTo('#contact_data');
		});
	},
	ena_contacto: function(){
		$('#gestion_contacto').attr('disabled',false);
	}
});

js_data.init();