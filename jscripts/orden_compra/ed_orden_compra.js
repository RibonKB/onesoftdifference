$('#prod_info').hide();

if(id_nv != 0)
	disable_button('#prod_add');
	
for(i in detalle){
	var nv_did = detalle[i].dc_detalle_nota_venta;
	var pend = detalle[i].dc_pendiente;
	var det_id = detalle[i].dc_detalle;
	var recp = detalle[i].dc_recepcionada;
	var q = detalle[i].dq_cantidad;
	var d = get_valid_detail(detalle[i]).appendTo('#prod_list');
	var minv = $('<input>').multiAttr({type:'hidden',class:'min_value'}).val(recp);
	var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(pend);
	var ids = $('<input>').multiAttr({type:'hidden',name:'doc_wf_id[]'}).val(det_id+'|'+nv_did+'|'+q);
	d.find('.prod_cant').after(minv).after(maxv);
	d.find('.code-div').after(ids);
	if(nv_did == 0 && recp == 0)
		d.find('.code-div').append(create_edit_code_btn());
	if(recp > 0)
		d.find('.del_detail').remove();
	
}

actualizar_detalles();
actualizar_totales();

$('.prod_cant').change(function(){
	var td = $(this).parent();
	var mn = td.find('.min_value');
	var mx = td.find('.max_value');
	if(mn.size()){
		if(parseFloat($(this).val()) < parseFloat(mn.val())){
			$(this).val(mn.val());
		}
	}
	if(mx.size()){
		if(parseFloat($(this).val()) > parseFloat(mx.val())){
			$(this).val(mx.val());
		}
	}
	actualizar_detalles();
	actualizar_totales();
});

$('.del_detail').unbind('click').click(function(){
	var tr = $(this).parent().parent();
	var id = tr.find(':hidden[name=doc_wf_id[]]');
	if(id.size()){
		$('<input>').multiAttr({'name':'to_delete[]','type':'hidden'}).val(id.val()).appendTo('#cr_orden_compra');
	}
	tr.remove();
	actualizar_detalles();
	actualizar_totales();
});