var js_subdata = {
	error_stock_overflow: "No puede asignar más stock del que requiere el documento asociado",
	init: function(){
		$('#fs_asignar_stock').click(js_subdata.submitData);
		$('.fs_stock').change(js_subdata.setCant);
	},
	submitData: function(){
		var value = '';
		var suma = 0;
		$('.fs_stock').each(function(){
			var stock = $(this).val();
			if(stock<=0)
				return;
			var bodega = $(this).next().next().val();
			value += '|'+stock+','+bodega;
			suma += parseInt(stock);
		});
		value = value.substr(1);
		var tr = $(':hidden[name=prod[]][value='+js_subdata.id_producto+']').parents('tr');
		
		if(value == ''){
			var suma = 0;
			$('#genOverlay .fs_max_stock').each(function(){
				suma += parseInt($(this).val());
			});
			tr.find('.prod_stock_libre').removeClass('confirm').addClass('alert').find('strong').text(suma);
			
			tr.find('.fs_bodega_entrada,.fs_reservado').remove();
			
			$('#genOverlay').remove();
			
			return;
		}
		
		var mx = tr.find('.max_value');
		if(mx.size()){
			if(suma > parseInt(mx.val())){
				show_error(js_subdata.error_stock_overflow);
				return;
			}
		}
		
		tr.find('.fs_reservado').remove();
		tr.find('.fs_bodega_entrada').remove();
		
		var input = $('<input>').multiAttr({
			'name': 'fs_reservado['+js_subdata.id_producto+']',
			'type': 'hidden',
			'class':'fs_reservado'
		}).val(value);
		
		var bodega = $('<input>').multiAttr({
			'name': 'fs_bodega_entrada['+js_subdata.id_producto+']',
			'type': 'hidden',
			'class':'fs_bodega_entrada'
		}).val($('#fs_bodega_entrada').val());
		
		tr.find('.prod_stock_libre').removeClass('alert').addClass('confirm').find('strong').text(suma);
		
		tr.find('[name=prod[]]').after(input).after(bodega);
		tr.find('.min_value').val(suma);
		tr.find('.prod_cant').trigger('change');
		$('#genOverlay').remove();
	},
	setCant: function(){
		var val = parseInt($(this).val());
		if(!val){
			$(this).val(0);
			return;
		}
		$(this).val(val);
		var mx = parseInt($(this).next('.fs_max_stock').val());
		if(val > mx){
			$(this).val(mx);
		}else if(val < 0){
			$(this).val(0);
		}
	}
};