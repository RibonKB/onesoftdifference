inf_campos = ['Número interno','Periodo contable','Tipo de movimiento','Fecha emisión','Banco','Glosa detalle','Tipo de documento','Cheque','RUT','Activo Fijo','Responsable','Centro beneficio','Centro costo','Documento de compra','Documento de venta','Factura de Compra',
    'Factura de Venta','Nota de Venta','Orden de Servicio','Cliente','Tipo Cliente','Orden de Compra','Producto','Marca','Proveedor',
'tipo Proveedor','Tipo Producto','Linea de Negocio','Banco','Banco Cobro','Guia Recepcion','Medio de Pago',
'Medio de Cobro','Mercado Cliente','Mercado Proveedor','Nota de Credito','Nota credito Proveedor','Segmento','Cheque',
'Fecha Cheque','Bodega'];
inf_classes = ['d_interno','d_periodo','d_mov','d_emision','d_banco','d_glosa','d_doc','d_cheque','d_rut','d_activo','d_resp','d_cebe','d_ceco','d_compra','d_venta','d_factura_compra',
    'd_factura_venta','d_nota_venta','d_orden_servicio','d_cliente','d_tipo_cliente','d_orden_compra','d_producto','d_marca',
'd_proveedor','d_tipo_proveedor','d_tipo_producto','d_linea','d_banco_pago','d_banco_cobro','d_guia_re',
'd_medio_pago',
'd_medio_cobro','d_merca_cliente','d_merca_prov','d_nota_credito','d_nota_cred_prov','d_segmento',
'd_cheque','d_cheque_fecha','d_bodega'];
inf_atvs = [0,1,0,0,0,1,0,0,0,0,0,0,0,0,0];
frm_cont = $('<div>').css({display:'none'});
for(i in inf_campos){
	if(i % 8 == 0){
		inner_cont = $('<div class="left"></div>');
		frm_cont.append(inner_cont);
	}
	inf_cb = $('<input />')
		.attr({
		type: 'checkbox',
		name: 'ex_fields[]',
		value: i,
		checked: inf_atvs[i],
		class: inf_classes[i]
		});
	frm_lbl = $('<label />').append(inf_cb).append(inf_campos[i]).after('<br />');
	inner_cont.append(frm_lbl);
}
frm_cont.clone().insertAfter("#sc_export").append('<hr class="clear" />');
frm_cont.show().appendTo('#sc_show_filter');
$('#sc_export').toggle(function(){
	$(this).next().show();
},function(){
	$(this).next().hide();
});
$('.menu input[type=checkbox]').click(function(){
	clase = $(this).attr('class');
	check = $(this).attr('checked');
	$('.menu .'+clase).attr('checked',check);
	if(check){
		$('.detallado .'+clase).show();
		$('#genOverlay').width($('#genOverlay').width()+80);
	}else{
		$('.detallado .'+clase).hide();
		$('#genOverlay').width($('#genOverlay').width()-80);
	}
});
$('#inf_print').click(function(){
	$('#informe_cont').printElement({ leaveOpen: true, printMode: 'popup', overrideElementCSS: ['styles/main.css'] });
});
$('#genOverlay').css({minWidth:700});