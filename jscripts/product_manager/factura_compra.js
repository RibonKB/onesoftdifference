actualizar_totales = function(){
	
t = "#prod_list";
v = "#total";
	
var total = 0;
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
	
		total += price*cant;
	});
	
	$(v).text(mil_format(total.toFixed(tc_dec)));
	$('#fc_total_detalle').val(total);
		
	/*$(v+'_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$(v+'_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$(v+'_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));*/
	
	var exe = parseFloat(toNumber($('#fc_exento').val()));
	exe = exe?exe:0.0;
	var desc = parseFloat(toNumber($('#fc_descuento').val()));
	
	var neto = total-exe-desc;
	$('#fc_neto').val(mil_format(neto)).trigger('change');
	setIvaTotal();

};

$('#fc_exento').change(function(){
	var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
	}
	var total = parseFloat($('#fc_total_detalle').val());
	var desc = parseFloat(toNumber($('#fc_descuento').val()));
	var maximo = total-desc;
	
	if(val > maximo){
		val = maximo;
	}
	
	var dif = maximo-val;
	$(this).val(mil_format(val));
	$('#fc_neto').val(mil_format(dif));
	setIvaTotal();
	
});

$('#fc_neto').change(function(){
	var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
	}
	var total = parseFloat($('#fc_total_detalle').val());
	var desc = parseFloat(toNumber($('#fc_descuento').val()));
	var maximo = total-desc;
	
	if(val > maximo){
		val = maximo;
	}
	
	var dif = maximo-val;
	
	$(this).val(mil_format(val));
	$('#fc_exento').val(mil_format(dif));
	setIvaTotal();
	
});

$('#fc_descuento').change(function(){
	var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
	}
	var exe = parseFloat(toNumber($('#fc_exento').val()));
	exe = exe?exe:0.0;
	var neto = parseFloat($('#fc_total_detalle').val())-exe-val;
	if(val > neto){
		val = neto;
	}
	$(this).val(mil_format(val));
	$('#fc_neto').val(mil_format(neto));
	setIvaTotal();
});

var setIvaTotal = function(){
	var total = parseFloat($('#fc_total_detalle').val());
	var exe = parseFloat(toNumber($('#fc_exento').val()));
	exe = exe?exe:0.0;
	var neto = parseFloat(toNumber($('#fc_neto').val()));
	var desc = parseFloat(toNumber($('#fc_descuento').val()));
	var iva = parseFloat((neto*0.19).toFixed(0));
	var total = neto+exe+iva;
	
	$('#fc_iva').val(mil_format(iva));
	$('#fc_total').val(mil_format(total.toFixed(0)));
}

$('#activate_exent').click(function(){
	var mx = $('.max_val').first().val();
	if($(this).attr('checked')){
		$('#fc_exento').attr('disabled',false).val(mil_format(mx));
		$('#fc_neto').attr('readonly',false).val(0);
	}else{
		$('#fc_exento').attr('disabled',true).val(0);
		$('#fc_neto').attr('readonly',true).val(mil_format(mx));
	}
	actualizar_totales();
	setIvaTotal();
});

$('.ventas_form').unbind('submit').submit(function(e){
	e.preventDefault();
	if($('#prod_list_fake .main').size()){
		if($('#fake_total').text() != $('#total').text()){
			show_error('Si indica una glosa de facturación los valores totales deben cuadrar');
			$('#prod_list_fake').parent().addClass('invalid');
			return;
		}
	}
	if($('#prod_list tr').size() == 0){
		alert('No ha agregado productos a la factura');
		return;
	}
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

change_guia_recepcion = function(){
	var dq_guia_recepcion = parseInt($(this).val());
	if(!dq_guia_recepcion) return;
	
	var field = $(this);
	
	$.getJSON('sites/ventas/factura_compra/proc/get_guia_recepcion_detalle.php',{ dq_guia_recepcion: dq_guia_recepcion },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la guía de recepción especificada, intentelo nuevamente');
			return;
		}
		
		if($('#prod_list .main').size() > 0){
			if(confirm('Se eliminarán todos los detalles que haya agregado\n¿Continuar?')){
				$('#prod_list .main').remove();
			}else{
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				return;
			}
		}
		
		disable_button('#prod_add');
		
		var det = data[0];
		var id = data[1].dc_guia_recepcion;
		
		for(i in det){
			if(det[i].dc_producto != null && det[i].dq_cantidad > 0){
				var q = det[i].dq_cantidad;
				var detalle = det[i];
				d = get_valid_detail(det[i]);
				var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
				d.find('.prod_cant').after(maxv);
				d.appendTo('#prod_list');
			}
		}
		
		actualizar_detalles();
		actualizar_totales();
		
		create_number_div(dq_guia_recepcion,'guia_recepcion',change_guia_recepcion,function(){
			$('#prod_list .strict').remove();
			$(':hidden[name=fv_id_guia_recepcion]').val(0);
			enable_button('#prod_add');
		}).width(field.width()).replaceAll(field);
		$(':hidden[name=fv_id_guia_recepcion]').val(id);
		
	});
};

$('#fv_guia_recepcion').autocomplete('sites/proc/autocompleter/guia_recepcion_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(change_guia_recepcion);

$('#set_guia_recepcion').click(function(){
	var dc_proveedor = $(':hidden[name=prov_id]').val();
	var dc_guia_recepcion = $('#guia_recepcion_data :input').serialize();
	pymerp.loadOverlay('sites/ventas/factura_compra/proc/get_guia_recepcion_form.php','dc_proveedor='+dc_proveedor+'&'+dc_guia_recepcion);
});

var initSetGuiaRecepcion = function(e){
	e.preventDefault();
	var form = $(this);
	$('#guia_recepcion_data').html('');
	form.find(':checkbox:checked').each(function(){
		$('<input>').multiAttr({
			'type': 'hidden',
			'name': $(this).attr('name')
		})
		.val($(this).val())
		.appendTo('#guia_recepcion_data');
    });
	$(this).parents('.overlay').remove();
};

$('#fc_tipo_factura').change(function(){
	var val = $(this).val();
	if(val == 0){
		var dm_factura_manual = 0;
	}else{
		var data = val.split('|');
		var dm_factura_manual = data.pop();
		var dc_tipo = data.pop();
	}
	
	var td = $('.cuenta_servicio');
	
	if(dm_factura_manual == 0){
		td.addClass('hidden');
		td.find('select').removeAttr('required');
		$('.prod_desc').attr('size',50);
	}else{
		td.removeClass('hidden');
		td.find('select').attr('required',true);
		$('.prod_desc').attr('size',20);
		
		$.getJSON('sites/ventas/factura_compra/proc/get_cuentas_tipo_factura.php',{dc_tipo: dc_tipo},function(data){
			td.find('select').each(function(index, element){
				$(this).html('').append('<option value="0">');
            	for(i in data){
					$(this).append($('<option>').attr('value',data[i].dc_cuenta_contable).text(data[i].dg_cuenta_contable));
				}
            });
		});
		
	}
	
});
