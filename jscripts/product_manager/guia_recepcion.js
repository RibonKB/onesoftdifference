$('#gr_orden_compra').autocomplete('sites/proc/autocompleter/orden_compra_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

$('#all_products').click(function(){
	if($(this).attr('checked'))
		$('#prod_list .prod_id:not(:checked)').each(function(){
			$(this).attr('checked',true);
			active_prod.call(this);
		});
	else
		$('#prod_list .prod_id:checked').each(function(){
			$(this).attr('checked',false);
			deactive_prod.call(this);
		});
	actualizar_totales();
});

var change_orden = function(){
	if($('#prod_list tr.main').size() > 0){
		if(!confirm('Se eliminaran los detalles agregados'))
			return;
	}
	$('#prod_list tr,#oc_data').remove();
	var v = get_disabled(this);

	$.getJSON('sites/ventas/geters/get_orden_compra_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la orden de compra especificada, intentelo denuevo');
			return;
		}else if(data[0] == '<empty>'){
			show_aviso('La orden de compra especificada no posee detalle que mueva stock, o ya fue completamente recepcionada. No puede continuar con ella.');
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			return;
		}else{
			var detalle = data[0];
			var id = data[1].dc_orden_compra;
			var nota_venta = data[1].dc_nota_venta;
			var orden_servicio = data[1].dc_orden_servicio;
			var contacto = data[1].dc_contacto;

			$('<div>').multiAttr({
				id: 'oc_data'
			}).css({
				border:'1px solid #BBB',
				background:'#E5E5E5',
				padding:'5px'
			}).appendTo('#oc_data_container');

			$('<div>').html('<b>Detalles Orden de compra</b>').addClass('info').appendTo('#oc_data').after('<br>');
			$('<b>').text('Proveedor: ').appendTo('#oc_data');
			$('<span>').text(data[1].dg_razon).appendTo('#oc_data').after('<br>');
			$('<b>').text('Fecha de emisión: ').appendTo('#oc_data');
			$('<span>').text(data[1].df_emision).appendTo('#oc_data').after('<br>');
			$('<b>').text('Tipo de cambio: ').appendTo('#oc_data');
			$('<span>').text(data[1].dg_tipo_cambio).appendTo('#oc_data').after('<br>');
			$('<b>').text('Cambio: $').appendTo('#oc_data');
			$('<span>').text(mil_format(data[1].dq_cambio)).appendTo('#oc_data').after('<br>');

			for(i in detalle){
				add_detail(detalle[i],data[1].dq_cambio).appendTo('#prod_list');
			}

			$(':hidden[name=gr_oc_id]').val(id);
			$(':hidden[name=gr_nv_id]').val(nota_venta);
			$(':hidden[name=gr_os_id]').val(orden_servicio);
		}
		create_number_div(v.val(),'orden_compra',change_orden,function(){
			$('#prod_list tr,#oc_data').remove();
			$('#all_products').multiAttr({
				checked:false,
				disabled:true
			});
		}).width(v.width()).replaceAll(v);
		$('#all_products').attr('disabled',false);
	});
};

$('#gr_orden_compra').change(change_orden);

var create_number_div = function(number,acl,chfn,cb){
	var edbtn = $('<img>').multiAttr({
		src: 'images/editbtn.png',
		title: 'Modificar',
		alt: '[|]',
		class:'right hidden'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'inputtext',
			type: 'text',
			id: 'gr_'+acl,
			size: '41',
			maxlength: '255'
		}).autocomplete('sites/proc/autocompleter/'+acl+'_strict.php',{
			formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
			minChars: 2,
			width:300
		}).result(function(){
			$(this).change();
		}).change(chfn).replaceAll(innerdiv);

		$(':hidden[name=fv_id_'+acl+']').val(0);
		cb();
	});

	var div = $('<div>').addClass('inputtext').css({
		background:'#EEE',
		fontWeight:'bold'
	}).text(number)
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.prepend(edbtn);

	return div;
};

var get_disabled = function(e){
	return $(e).blur().attr('disabled',true).css({
		backgroundImage: 'url(images/ajax-loader.gif)',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center right'
	});
};

$('.det_price,.prod_cant').change(function(){
	actualizar_totales();
});

var add_detail = function(det,tc){
	var tr = $('#prods_form tr.main').clone(true);
	var det_id = parseInt(det.dc_detalle_nota_venta)+parseInt(det.dc_detalle_orden_servicio);
	tr.find('.prod_id').val(det.dc_detalle+'|'+det_id+'|'+det.dc_ceco);
	tr.find('.prod_codigo').text(det.dg_codigo);
	tr.find('.prod_descripcion').text(det.dg_descripcion);
	tr.find('.prod_cant').val(det.dq_cantidad-det.dc_recepcionada);
	//tr.find('.prod_precio').text(mil_format((det.dq_precio/tc).toFixed(2)));
	tr.find('.det_price').val(det.dq_precio);
	tr.find('.det_prod').val(det.dc_producto);
	tr.find('.prod_precio_total').text(mil_format((det.dq_precio*(det.dq_cantidad-det.dc_recepcionada)/tc).toFixed(2)));
	return tr;
};

var active_prod = function(){
	var tr = $(this).parent().parent();
	tr.find('td').addClass('gr_active_prod');
	tr.find(':text,select,input:hidden').removeAttr('disabled');
};

var deactive_prod = function(){
	var tr = $(this).parent().parent();
	tr.find('td').removeClass('gr_active_prod');
	tr.find(':text,select,input:hidden').attr('disabled',true);
};

$('#prods_form .prod_id').click(function(){
	if($(this).attr('checked'))
		active_prod.call(this);
	else
		deactive_prod.call(this);
	actualizar_totales();
});

$('.ventas_form').submit(function(e){
	e.preventDefault();
	if($('#prod_list .prod_id:checked').size() == 0){
		show_error('Debe seleccionare al menos un producto para recepcionar');
		return;
	}
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

var actualizar_totales = function(){
	var total = 0;
	var tr = $('.prod_id:checked').parents('tr').each(function(){
		total += parseInt($(this).find('.prod_cant').val())*parseFloat($(this).find('.det_price').val());
	});
	$('#prods .max_val').val(total);

	//$(v).text(mil_format(total.toFixed(tc_dec)));
	$('#gr_total_detalle').val(total);

	var exe = parseFloat(toNumber($('#gr_exento').val()));
	exe = exe?exe:0.0;
	var desc = parseFloat(toNumber($('#gr_descuento').val()));

	var neto = total-exe-desc;
	$('#gr_neto').val(mil_format(neto)).trigger('change');
	setIvaTotal();

	/*if($('#activate_exent:checked').size()){
		var exe = parseFloat(toNumber($('#gr_exento').val())).toFixed(0);
		var neto = parseFloat(toNumber($('#gr_neto').val())).toFixed(0);
		var pretotal = exe+neto;

		//$('#cr_guia_recepcion').append("<b>total: </b>"+total+"<br><b>exe: </b>"+exe+"<br><b>neto: </b>"+neto+"<br><b>pretotal: </b>"+pretotal+'<br>');

		if(total > pretotal){
			$('#gr_exento').val(mil_format(exe+total-pretotal));
		}else{
			var dif = exe-(pretotal-total);
			//$('#cr_guia_recepcion').append("<br><b>total: </b>"+dif+'<br>');

			if(dif < 0){
				$('#gr_exento').val(0);
			}else{
				$('#gr_exento').val(mil_format(dif));
			}
			$('#gr_exento').trigger('change');
		}

	}else{
		$('#gr_neto').val(mil_format(total));
	}*/
	setIvaTotal();
};

$('#gr_exento').change(function(){
	var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
	}
	var total = parseFloat($('#gr_total_detalle').val());
	var desc = parseFloat(toNumber($('#gr_descuento').val()));
	var maximo = total-desc;

	if(val > maximo){
		val = maximo;
	}

	var dif = maximo-val;
	$(this).val(mil_format(val));
	$('#gr_neto').val(mil_format(dif));
	setIvaTotal();
	/*var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
		$(this).val(0);
	}
	var total = parseFloat($(this).next('.max_val').val());
	if(total < val){
		val = total;
		$(this).val(mil_format(total));
	}else{
		$(this).val(mil_format(val));
	}
	var dif = total-val;
	$('#gr_neto').val(mil_format(dif));
	setIvaTotal();*/
});

$('#gr_neto').change(function(){
	var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
	}
	var total = parseFloat($('#gr_total_detalle').val());
	var desc = parseFloat(toNumber($('#gr_descuento').val()));
	var maximo = total-desc;

	if(val > maximo){
		val = maximo;
	}

	var dif = maximo-val;

	$(this).val(mil_format(val));
	$('#gr_exento').val(mil_format(dif));
	setIvaTotal();
	/*var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
		$(this).val(0);
	}
	var total = parseFloat($(this).next('.max_val').val());
	if(total < val){
		val = total;
		$(this).val(mil_format(total));
	}else{
		$(this).val(mil_format(val));
	}
	var dif = total-val;
	$('#gr_exento').val(mil_format(dif));
	setIvaTotal();*/
});

$('#gr_descuento').change(function(){
	var val = parseFloat(toNumber($(this).val()));
	if(!val){
		val = 0;
	}
	var exe = parseFloat(toNumber($('#gr_exento').val()));
	exe = exe?exe:0.0;
	var neto = parseFloat($('#gr_total_detalle').val())-exe-val;
	if(val > neto){
		val = neto;
	}
	$(this).val(mil_format(val));
	$('#gr_neto').val(mil_format(neto));
	setIvaTotal();
});

var setIvaTotal = function(){
	var total = parseFloat($('#gr_total_detalle').val());
	var exe = parseFloat(toNumber($('#gr_exento').val()));
	exe = exe?exe:0.0;
	var neto = parseFloat(toNumber($('#gr_neto').val()));
	var desc = parseFloat(toNumber($('#gr_descuento').val()));
	var iva = parseFloat((neto*0.19).toFixed(0));
	var total = neto+exe+iva;

	$('#gr_iva').val(mil_format(iva));
	$('#gr_total').val(mil_format(total.toFixed(0)));
	/*var exe = parseFloat(toNumber($('#gr_exento').val()));
	exe = exe?exe:0.0;
	var neto = parseFloat(toNumber($('#gr_neto').val())).toFixed(0);
	var iva = parseFloat(neto*0.19).toFixed(0);
	var total = (parseFloat(exe)+parseFloat(neto)+parseFloat(iva)).toFixed(0);

	$('#gr_iva').val(mil_format(iva));
	$('#gr_total').val(mil_format(total));*/
}

$('#activate_exent').click(function(){
	var mx = $('.max_val').first().val();
	if($(this).attr('checked')){
		$('#gr_exento').attr('disabled',false).val(mil_format(mx));
	}else{
		$('#gr_exento').attr('disabled',true).val(0);
	}
	actualizar_totales();
	setIvaTotal();
});

$(':input[name=gr_tipo_entrada]').click(function(e){
    var val = $(this).val();
	var nw = $('#new_factura');
	var old = $('#old_factura');

	if(val == 'existente'){
		old.removeClass('hidden');
		nw.addClass('hidden');

		nw.find('[required]').removeAttr('required');
		old.find('#dq_factura_existente').attr('required',true);
		old.find('#gr_fecha_recepcion').attr('required',true);

	}else{
		nw.removeClass('hidden');
		old.addClass('hidden');

		old.find('[required]').removeAttr('required');
		nw.find("#gr_factura_compra, #gr_fc_emision, #gr_fc_vencimiento, #gr_fc_fecha_contable, #gr_fc_tipo_factura").attr('required',true);
	}
});

var fc_existe_result = function(e, row){
	var dc_factura = row[3];
	$(':hidden[name=dc_factura_old]').val(dc_factura);
};

$('#dq_factura_existente').autocomplete('sites/proc/autocompleter/factura_compra_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300,
	extraParams: {
		sin_pago: true
	}
}).result(fc_existe_result);
