var js_data = {
	tr: $('<tr>'),
	table: '#pend_table',
	tdClasses: ['opt','semaforo','chkbx','docnum','docdate','docclient','doctc','docvtc','pcode','pname','pprov','pprice','pcant','pendcant','pendprice'],
	tdAlign: ['left','left','center','left','left','left','left','right','left','left','left','right','center','center','right'],
	clientes: [],
	proveedores: [],
	init: function(){
		$(this.table+' thead th').each(function(i){
			$(this).addClass(js_data.tdClasses[i]);
		});
		
		$('#filter_menu').css({
			background:'#fff',
			border:'1px solid #777',
			position:'absolute',
			top:0,
			right:0
		}).children('button').click(function(){
			$(this).next().next().toggle();
		}).next().next().find(':checkbox').change(function(){
			if(!$(this).attr('checked'))
				$('.'+$(this).attr('id')).addClass('hidden');
			else
				$('.'+$(this).attr('id')).removeClass('hidden');
		});
		
		for(i in this.tdClasses){
			this.tr.append($('<td>').addClass(this.tdClasses[i]).attr('align',this.tdAlign[i]));
		}
		
		this.tr.children('.chkbx').append($('<input>').multiAttr({type:'checkbox',name:'oc_detail[]'}));
		this.tr.children('.opt').append($('<img>').multiAttr({src:'images/editbtn.png',title:'Editar detalle'}).click(js_data.editDetail));
		
		this.initClientes();
		this.initProveedores();
		for(i in this.pendientes){
			this.addDetail(this.pendientes[i]);
		}
		this.updateHidden();
		
		$('#load_compras').submit(this.submitFilter);
		$('#crear_orden_compra').submit(this.submitOCcreation);
		$('#select_all').click(this.selectAll);
		
	},
	addDetail:function(det){
		var aux = this.tr.clone(true);
		var cant_pendiente = det.dq_cantidad-det.dc_comprada;
		aux.children('.chkbx').children(':checkbox').val(det.dc_nota_venta+'|'+det.dc_nota_venta_detalle+'|'+cant_pendiente+'|'+det.dc_proveedor+'|'+det.dc_tipo_cambio+'|'+det.dq_cambio);
		aux.children('.docnum').text(det.dq_nota_venta);
		aux.children('.docdate').text(det.df_fecha_emision);
		aux.children('.docclient').text(this.getCliente(det.dc_cliente));
		aux.children('.doctc').text(det.dg_tipo_cambio);
		aux.children('.docvtc').text(mil_format(det.dq_cambio));
		aux.children('.pcode').text(det.dg_codigo);
		aux.children('.pname').text(det.dg_producto);
		aux.children('.pprov').text(this.getProveedor(det.dc_proveedor));
		var precio_venta = (det.dq_precio_venta/det.dq_cambio).toFixed(det.dn_cantidad_decimales);
		aux.children('.pprice').text(mil_format(precio_venta));
		aux.children('.pcant').text(det.dq_cantidad);
		aux.children('.pendcant').text(cant_pendiente);
		aux.children('.pendprice').text(mil_format(cant_pendiente*precio_venta));
		aux.appendTo('#prod_list');
	},
	initClientes: function(){
		$('#sel_cli option').each(function(){
			js_data.clientes[$(this).attr('value')] = $(this).text();
		});
	},
	getCliente: function(id){
		return this.clientes[id];
	},
	initProveedores: function(){
		$('#sel_prov option').each(function(){
			js_data.proveedores[$(this).attr('value')] = $(this).text();
		});
	},
	getProveedor: function(id){
		return this.proveedores[id];
	},
	updateHidden:function(){
		$('#filter_menu :checkbox').change();
	},
	submitFilter:function(e){
		e.preventDefault();
		if(!validarForm('#'+this.id)){
			show_error("Los filtros marcados son obligatorios");
			return;
		}
		
		filter = {
			doc : $(this.sel_doc).val(),
			inim : parseInt($(this.sel_inim).val()),
			finm : parseInt($(this.sel_finm).val()),
			prov : $(this.sel_prov).val(),
			cli : $(this.sel_cli).val()
		}
		
		$('#prod_list tr').remove();

		for(i in js_data.pendientes){
			js_data.detailFilter(js_data.pendientes[i],filter);
		}
		
		js_data.updateHidden();
	},
	detailFilter: function(det,filter){
		if((parseInt(det.dc_month) > filter.finm) || (parseInt(det.dc_month) < filter.inim))
			return;
		if(filter.prov != 0 && det.dc_proveedor != filter.prov)
			return;
		if(filter.cli != 0 && det.dc_cliente != filter.cli)
			return;
		if(filter.doc != '' && det.dq_nota_venta.indexOf(filter.doc) == -1)
			return;
		this.addDetail(det);
	},
	editDetail: function(){
		$(this).unbind('click');
		var tr = $(this).parent().parent();
		tr.find('td').expose({closeOnEsc:false,closeOnClick:false,color:'#DDD'});
		$(this).multiAttr({src:'images/confirm.png',title:'Confirmar cambios'}).click(js_data.procEditDetail);
		var det_detail = tr.find(':checkbox').val().split('|');
		var det_prov = $('#sel_prov').clone().width(180).attr('id','det_prov').val(det_detail[3]);
		var det_cant = $('<input>').multiAttr({type:'text',id:'det_cant',class:'inputtext'}).val(det_detail[2]);
		tr.find('.pprov').text('').append(det_prov);
		tr.find('.pendcant').text('').append(det_cant);
	},
	procEditDetail:function(){
		$(this).unbind('click');
		var tr = $(this).parent().parent();
		var cb = tr.find(':checkbox');
		var data = cb.val().split('|');
		var prov = tr.find('#det_prov').val();
		var cant = parseInt(tr.find('#det_cant').val());
		if(!cant || cant > parseInt(data[2]))
			cant = data[2];
		cb.val(data[0]+'|'+data[1]+'|'+cant+'|'+prov+'|'+data[4]+'|'+data[5]);
		tr.find('.pprov').html(js_data.getProveedor(prov));
		tr.find('.pendcant').html(cant);
		$(this).multiAttr({src:'images/editbtn.png',title:'Editar detalle'}).click(js_data.editDetail);
		$.mask.close();
	},
	submitOCcreation: function(e){
		e.preventDefault();
		if($('#prod_list .inputtext').size())
			return;
		if($('#prod_list :checked').size() == 0){
			show_error('No ha seleccionado detalles para ser comprados');
			return;
		}
		var p=$(this).serialize();
		var a=$(this).attr("action");
		loadpage(a,p);
		
	},
	selectAll: function(){
		$('#prod_list .chkbx :checkbox').attr('checked',$(this).attr('checked'));
	}
};