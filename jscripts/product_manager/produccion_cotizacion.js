var tc;
var tc_dec;

var ac_dir = 'sites/proc/autocompleter/produccion_producto.php';
var ac_options = {
	formatItem: function(row){return row[0]+" ( $"+mil_format(row[1])+" ) "+row[2];},
	minChars: 2,
	width:300
};

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo')
	.autocomplete(ac_dir,ac_options).result(result_ac);
});

var result_ac = function(e,row){
	var el = $(this).parent().parent();
	var cant = el.find('.prod_cant');
	if(!parseFloat(cant.val())){
		cant.val(1);
		cant = 1;
	}else{
		cant = cant.val();
	}
	
	el.find('.prod_price').val(mil_format(parseFloat(row[1]/tc).toFixed(tc_dec)));
	el.find('.prod_costo').val(mil_format(parseFloat(row[1]/tc).toFixed(tc_dec)));
	
	$(this).next(':hidden[name=type_prod[]]').val(1);
	create_code_div(row[0],row[3]).replaceAll(this);
	
	actualizar_detalles();
	actualizar_totales();
};

var create_code_div = function(name,id){
	var ed_btn = $('<img>').multiAttr({
		class: 'right hidden',
		src: 'images/editbtn.png',
		alt: '[|]',
		title: 'Editar código',
		width: '13',
		height: '13'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		innerdiv.next(':hidden[name=type_prod[]]').val(0);
		
		var ac_field = $('<input>').multiAttr({
			class: 'searchbtn prod_codigo',
			type: 'text',
			size: '35',
			name: 'prod[]'
		}).autocomplete(ac_dir,ac_options).result(result_ac)
		.replaceAll(innerdiv);
	});
	
	var hidden = $('<input>').multiAttr({
		type:'hidden',
		name:'prod[]'
	}).val(id);
	
	return $('<div>').css({
		border:'1px solid #CCC',
		background:'#EEE',
		padding:'5px',
		fontWeight:'bold'
	}).mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.text(name)
	.prepend(ed_btn)
	.append(hidden);
};

$('.prod_codigo').change(function(){
	code = $(this).val();
	count = 0;
	$('#prod_list .prod_codigo').each(function(){
		if($(this).val() == code)
			count++;
	});
	if(count > 1){
		alert("ya ha ingresado un producto con ese código, intente con un nuevo código o modifique el existente");
		$(this).val('');
		el = $(this).parent().parent();
		el.find('.prod_cant').val('');
		el.find('.prod_desc').val('');
		el.find('.prod_price').val('');
		el.find('.prod_costo').val('');
		actualizar_detalles();
		actualizar_totales();
	}
	
});

$("#prod_tipo_cambio").change(function(){
	indice = this.selectedIndex;
	if(indice == 0){
		$("#prod_info").show();
		$("#prods").hide();
	}else{
		$("#prod_info").hide();
		$("#prods").show();
		tc = this.options[indice].innerHTML.split("|");
		tc = tc[1];
		$('#cot_cambio').val(tc);
		tc_dec = $(":hidden[name=decimal"+$(this).val()+"]").val();
		actualizar_detalles();
		actualizar_totales();
	}
	
});

$('.margen_p').change(function(){
	cost = toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec);
	margin = toNumber($(this).val());
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	price = $(this).parent().parent().find('.prod_price').val(mil_format(parseFloat(cost)+(cost*margin/100)));
	actualizar_detalles();
	actualizar_totales();
});

actualizar_detalles = function(){
	$("#prod_list tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val()).toFixed(tc_dec);
		cost = toNumber($(this).find('.prod_costo').val()).toFixed(tc_dec);
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}
		
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(tc_dec)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(tc_dec)));
		$(this).find('.margen').text(mil_format((price-cost).toFixed(tc_dec)));
		if(price > 0)
		$(this).find('.margen_p').val(((price-cost)*100/cost).toFixed(1));
		else
		$(this).find('.margen_p').val('0');
		local = $(this).next();
		local.find('.l_price').text(mil_format((price*tc).toFixed(empresa_dec)));
		local.find('.l_total').text(mil_format((price*tc*cant).toFixed(empresa_dec)));
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
		local.find('.l_margen').text(mil_format(((price-cost)*tc).toFixed(empresa_dec)));
	});
}

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
});

$('.prod_cant,.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('#cot_cambio').change(function(){
	cot_cambio = $(this).val();
	if(!parseFloat(tc)){
		cot_cambio = 1.0;
		$(this).val(1);
	}
	tc = cot_cambio;
	actualizar_detalles();
	actualizar_totales();
});

$('.ventas_form').submit(function(e){
	e.preventDefault();
	if($('#prod_list tr').size() == 0){
		alert('No ha agregado productos a la cotizaci\xf3n');
		return;
	}
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

actualizar_totales = function(){
	
var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$("#prod_list tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
		cost = toNumber($(this).find('.prod_costo').val());
	
		total += price*cant;
		total_costo += cost*cant;
		total_margen += price-cost;
		total_neto += price*tc*cant;
	});
	
	$('#total').text(mil_format(total.toFixed(tc_dec)));
	$('#total_costo').text(mil_format(total_costo.toFixed(tc_dec)));
	$('#total_margen').text(mil_format(total_margen.toFixed(tc_dec)));
	
	if(total > 0)
		$('#total_margen_p').text(mil_format(((total-total_costo)/total_costo*100).toFixed(3)));
	else
		$('#total_margen_p').text('0');
	$('#total_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$('#total_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$('#total_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));
	
	$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
	$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));

}