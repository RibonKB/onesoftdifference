var tc = 1;
var tc_dec = empresa_dec;
var bout = 0;
var bin = 0;

$('#gd_tipo_guia').change(function(){
	bout = out_bod[$(this).val()];
	bin = in_bod[$(this).val()];
	$('#prod_list .prod_bodega_salida').val(bout);
	if(bin != 0){
		$('#prod_list .prod_bodega_entrada').attr('disabled',false).val(bin);
	}else{
		$('#prod_list .prod_bodega_entrada').attr('disabled',true).val(0);
	}
	$(':hidden[name=gd_tipo_mov]').val(tipo_mov[$(this).val()]);
	$(':hidden[name=facturable]').val(facturable[$(this).val()]);
	
	var nota_venta = $(':hidden[name=gd_id_nota_venta]').val();
	if(nota_venta != 0){
		verificar_factura_anticipada(nota_venta);
	}
});

$('#gd_nota_venta').autocomplete('sites/proc/autocompleter/nota_venta_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

change_nota_venta = function(){
	
	var v = get_disabled(this);
	
	$.getJSON('sites/ventas/guia_despacho/proc/get_nota_venta_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la nota de venta especificada, intentelo denuevo');
			return;
		}
	
		if($('#prod_list .main').size() > 0){
			if(confirm('Se eliminarán todos los detalles que haya agregado\n¿Continuar?')){
				$('#prod_list .main').remove();
			}else{
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				return;
			}
		}
		disable_button('#prod_add');
		$('#gd_orden_servicio').attr('disabled',true);
		$(':hidden[name=gd_id_orden_servicio]').val(0);

		if(data[0] == '<empty>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('La nota de venta especificada no posee detalle');
			return;
		}else{
			var det = data[0];
			var id = data[1].dc_nota_venta;
			var cambio = data[1].dq_cambio;
			var al_dia = data[1].dm_al_dia;
			var despachada = false;
			var cambio_dia = $(':hidden[name=cambio'+data[1].dc_tipo_cambio+']');
			
			if(al_dia == 0 || !cambio_dia.size()){
				for(i in det){
					det[i].dq_cantidad -= det[i].dc_despachada;
					if(det[i].dc_producto != null && det[i].dq_cantidad > 0){
						var q = det[i].dq_cantidad;
						var desp = det[i].dc_despachada
						var recep = det[i].dc_recepcionada;
						d = get_valid_detail(det[i]);
						d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_detail[]', value: det[i].dc_nota_venta_detalle}));
						var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
						d.find('.despach_cant').val(desp);
						d.find('.recep_cant').val(recep);
						d.find('.prod_cant').after(maxv);
						d.prependTo('#prod_list');
						despachada = true;
					}
				}
			}else{
				cambio_dia = cambio_dia.val();
				for(i in det){
					det[i].dq_cantidad -= det[i].dc_despachada;
					det[i].dq_precio_venta = (det[i].dq_precio_venta/cambio)*cambio_dia;
					det[i].dq_precio_compra = (det[i].dq_precio_compra/cambio)*cambio_dia;
					if(det[i].dc_producto != null && det[i].dq_cantidad > 0){
						var q = det[i].dq_cantidad;
						var desp = det[i].dc_despachada
						var recep = det[i].dc_recepcionada;
						d = get_valid_detail(det[i]);
						d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_detail[]', value: det[i].dc_nota_venta_detalle}));
						var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
						d.find('.despach_cant').val(desp);
						d.find('.recep_cant').val(recep);
						d.find('.prod_cant').after(maxv);
						d.prependTo('#prod_list');
						despachada = true;
					}
				}

			}
			
			if(!despachada){
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				show_aviso('La nota de venta ya a sido completamente despachada');
				$('#gd_orden_servicio').attr('disabled',false);
				enable_button('#prod_add');
				return;
			}
			
			$('#gd_tipo_guia').val(data[1].dc_tipo_guia_default).trigger('change');
			
			actualizar_detalles();
			actualizar_totales();
			
			create_number_div(v.val(),'nota_venta',change_nota_venta,function(){
				$('#gd_orden_servicio').attr('disabled',false);
				delete_anticipados();
				if(confirm('¿Desea mantener los detalles agregados?')){
					$('#prod_list :hidden[name=id_detail[]]').remove();
					//$('#prod_list .code-div').each(function(){$(this).prepend(create_edit_code_btn());});
					$('#prod_list .prod_codigo[readonly]').attr('readonly',false).addClass('searchbtn').autocomplete('sites/proc/autocompleter/producto.php',{
						formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
					.result(result_ac);
					$('#prod_list .max_value').remove();
				}else{
					$('#prod_list .strict').remove();
				}
				enable_button('#prod_add');
			}).width(v.width()).replaceAll(v);
			$(':hidden[name=gd_id_nota_venta]').val(id);
			
			verificar_factura_anticipada(id);
			
		}
	});
};

$('#gd_nota_venta').change(change_nota_venta);

$('#gd_orden_servicio').autocomplete('sites/proc/autocompleter/orden_servicio_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

change_orden_servicio = function(){
	var v = get_disabled(this);
	
	$.getJSON('sites/ventas/guia_despacho/proc/get_orden_servicio_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la orden de servicio');
			return;
		}
		
		if(data[0] == '<empty>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No hay productos pendientes de despacho para la orden de servicio');
			return;
		}
		
		if($('#prod_list .main').size() > 0){
			if(confirm('Se eliminarán todos los detalles que haya agregado\n¿Continuar?')){
				$('#prod_list .main').remove();
			}else{
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				return;
			}
		}
		
		disable_button('#prod_add');
		$('#gd_nota_venta').attr('disabled',true);
		$(':hidden[name=gd_id_nota_venta]').val(0);
		
		var det = data[0];
		var id = data[1].dc_orden_servicio;
		
		for(i in det){
			var q = det[i].dq_cantidad;
			var desp = det[i].dq_despachado;
			var recep = det[i].dc_recepcionada;
			
			d = get_valid_detail(det[i]);
			d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_os_detail[]', value: det[i].dc_detalle}));
			var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
			d.find('.prod_cant').after(maxv);
			d.find('.despach_cant').val(desp);
			d.find('.recep_cant').val(recep);
			d.prependTo('#prod_list');
		}
		
		create_number_div(v.val(),'orden_servicio',change_orden_servicio,function(){
			$('#gd_nota_venta').attr('disabled',false);
			if(confirm('¿Desea mantener los detalles agregados?')){
				$('#prod_list :hidden[name=id_os_detail[]]').remove();
				//$('#prod_list .code-div').each(function(){$(this).prepend(create_edit_code_btn());});
				$('#prod_list .prod_codigo[readonly]').attr('readonly',false).addClass('searchbtn').autocomplete('sites/proc/autocompleter/producto.php',{
					formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
				.result(result_ac);
				$('#prod_list .max_value').remove();
			}else{
				$('#prod_list .strict').remove();
			}
		}).width(v.width()).replaceAll(v);
		
		$(':hidden[name=gd_id_orden_servicio]').val(id);
		
		actualizar_detalles();
		actualizar_totales();
		
	});
};



$('#gd_orden_servicio').change(change_orden_servicio);

get_disabled = function(e){
	return $(e).blur().attr('disabled',true).css({
		backgroundImage: 'url(images/ajax-loader.gif)',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center right'
	});
};

get_valid_detail = function(d){
	var row = $('#prods_form tr').clone(true);
	row.find('.prod_desc').val(d.dg_descripcion);
	row.find('.prod_proveedor').val(d.dc_proveedor);
	row.find('.prod_bodega_salida').val(bout);
	if(bin == 0)
		row.find('.prod_bodega_entrada').val(0).attr('disabled',true);
	else
		row.find('.prod_bodega_entrada').val(bin).attr('disabled',false);
	row.find('.prod_cant').val(d.dq_cantidad);
	row.find('.prod_price').val(parseFloat(d.dq_precio_venta).toFixed(tc_dec)); 
	row.find('.prod_costo').val(parseFloat(d.dq_precio_compra).toFixed(tc_dec));
	row.find('.prod_codigo').removeClass('searchbtn').addClass('inputtext').attr('readonly',true).val(d.dg_codigo);
	if(d.dm_requiere_serie == '1')
		row.find('.prod_series').attr('required',true);
	//create_code_div(d.dg_codigo,d.dc_producto,d.dg_descripcion).replaceAll(row.find('.prod_codigo'));
	return row;
};

add_invalid_detail = function(d){
	row = $('<tr>');
	col = $('<td>').css({
		border: '1px solid #F00',
		background: '#FFFFBF'
	});
	
	crbtn = $('<img>').multiAttr({
		src: 'images/addbtn.png',
		alt: '[+]',
		title: 'Crear el producto'
	}).click(function(){
		loadOverlay('sites/ventas/proc/cr_producto.php?c='+encodeURIComponent(d.dg_producto)+'&d='+encodeURIComponent(d.dg_descripcion)+'&q='+encodeURIComponent(d.dq_precio_compra)+'&v='+encodeURIComponent(d.dq_precio_venta));
	});
	
	col.clone().prepend(crbtn).appendTo(row);
	col.clone().text(d.dg_producto).appendTo(row);
	col.clone().text(d.dg_descripcion).appendTo(row);
	col.clone().text('-').appendTo(row);
	col.attr('align','right');
	col.clone().text(mil_format(d.dq_cantidad)).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_venta).toFixed(tc_dec))).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_venta*d.dq_cantidad).toFixed(tc_dec))).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_compra).toFixed(tc_dec))).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_compra*d.dq_cantidad).toFixed(tc_dec))).appendTo(row);
	col.clone().text('-').appendTo(row);
	col.clone().text('-').appendTo(row);
	$('#prod_list').prev('thead').append(row);
};

create_code_div = function(code,id,desc,ed){
					
	var div = $('<div>').css({
		border:'1px solid #CCC',
		background:'#EEE',
		padding:'5px',
		fontWeight:'bold'
	}).addClass('code-div')
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.text(code);
	
	if(ed){
		div.prepend(create_edit_code_btn());
	}
					
	$('<input>').multiAttr({
		type:'hidden',
		name:'prod[]'
	}).val(id).appendTo(div);
					
	$('<input>').multiAttr({
		type:'hidden',
		class:'hidden_desc'
	}).val(desc).appendTo(div);
	
	return div;
};

create_edit_code_btn = function(){
	return $('<img>').multiAttr({
		class: 'right hidden',
		src: 'images/editbtn.png',
		alt: '[|]',
		title: 'Editar código',
		width: '13',
		height: '13'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'searchbtn prod_codigo',
			type: 'text',
			size: '7'
		}).autocomplete('sites/proc/autocompleter/producto.php',{
			formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
		.result(result_ac)
		.replaceAll(innerdiv);
	});
};

create_number_div = function(number,acl,chfn,cb){
	var edbtn = $('<img>').multiAttr({
		src: 'images/editbtn.png',
		title: 'Modificar',
		alt: '[|]',
		class:'right hidden'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'inputtext',
			type: 'text',
			id: 'gd_'+acl,
			size: '41',
			maxlength: '255'
		}).autocomplete('sites/proc/autocompleter/'+acl+'_strict.php',{
			formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
			minChars: 2,
			width:300
		}).result(function(){
			$(this).change();
		}).change(chfn).replaceAll(innerdiv);
		
		$(':hidden[name=gd_id_'+acl+']').val(0);
		cb();
	});
	
	var div = $('<div>').addClass('inputtext').css({
		background:'#EEE',
		fontWeight:'bold'
	}).text(number)
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.prepend(edbtn);
	
	return div;
};

$('#prod_add').click(function(){
	var tr = $('#prods_form tr').clone(true);
	tr.appendTo('#prod_list');
	tr.find('.prod_bodega_salida').val(bout);
	if(bin == 0)
		tr.find('.prod_bodega_entrada').val(0).attr('disabled',true);
	else
		tr.find('.prod_bodega_entrada').val(bin).attr('disabled',false);
	tr.find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
		formatItem: function(row){	return row[0]+" ( "+row[1]+" ) "+row[2]; },
		minChars: 2,
		width:300
	}).result(result_ac).trigger('focus');
});

result_ac = function(e,row){
		
		el = $(this).parent().parent();
		cant = el.find('.prod_cant');
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}
		
		el.find('.prod_desc').val(row[1]);
		el.find('.prod_price').val(mil_format(parseFloat(row[3]/tc).toFixed(tc_dec)));
		el.find('.prod_costo').val(mil_format(parseFloat(row[4]/tc).toFixed(tc_dec)));
		el.find('.prod_series').attr('required',row[6]=='1');
		
		//create_code_div(row[0],row[5],row[1],true).replaceAll(this);
		
		actualizar_detalles();
		actualizar_totales();
};

$('.margen_p').change(function(){
	cost = toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec);
	margin = toNumber($(this).val());
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	price = $(this).parent().parent().find('.prod_price').val(mil_format(parseFloat(cost)+(cost*margin/100)));
	actualizar_detalles();
	actualizar_totales();
});

actualizar_detalles = function(t){
	t = t?t:'#prod_list';
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val()).toFixed(tc_dec);
		cost = toNumber($(this).find('.prod_costo').val()).toFixed(tc_dec);
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}
		
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(tc_dec)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(tc_dec)));
		$(this).find('.margen').text(mil_format((price-cost).toFixed(tc_dec)));
		if(cost > 0)
		$(this).find('.margen_p').val(((price-cost)*100/cost).toFixed(1));
		else
		$(this).find('.margen_p').val('0');
		local = $(this).next();
		local.find('.l_price').text(mil_format((price*tc).toFixed(empresa_dec)));
		local.find('.l_total').text(mil_format((price*tc*cant).toFixed(empresa_dec)));
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
		local.find('.l_margen').text(mil_format(((price-cost)*tc).toFixed(empresa_dec)));
	});
};

$('#gd_contacto,#gd_cont_entrega').addClass('lst_contacto');

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
	actualizar_totales();
});

$('.get_description').click(function(){
	var desc = $(this).parent().next().find('.hidden_desc').val();
	$(this).parent().next().next().children('.prod_desc').val(desc);
});

$('.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('.prod_cant').change(function(){
	var mx = $(this).next('.max_value');
	if(mx.size()){
		if(parseFloat($(this).val()) > parseFloat(mx.val())){
			$(this).val(mx.val());
		}
	}
	actualizar_detalles();
	actualizar_totales();
});

var checkCantidadesAnticipadas = function(){
	$('#prod_list tr.main td').removeAttr('style');
	var factura = $(':radio:checked[name=dc_factura_anticipada]');
	if(factura.size() == 0)
		return true;
	factura = factura.val();
	if(factura == 0)
		return true;
	
	if($(':hidden[name=facturable]').val() == 0){
		var completa = true;
		$(':radio.dc_factura_anticipada').each(function(){
			var dc_f = $(this).val();
			$('.F'+dc_f).each(function(){
				if($(this).val() > 0){
					completa = false;
				}
			});
		});
		if(completa){
			$('<input>').multiAttr({
				'type':	'hidden',
				'name':	'to_complete',
				'id':	'to_complete'
			}).val(1).appendTo('#cr_guia_despacho');
			return true;
		}
	}
	var ret = true;
	$('#prod_list tr.main').each(function(){
		var code = $(this).find('.prod_codigo').val().replace(/[\(\)\=#\.\/\s]/gi,'_');
		var cant = $(this).find('.prod_cant').val();
		var limit = $('.F'+factura+'#'+factura+'_'+code);
		
		if(limit.size() == 0){
			show_error('El producto <b>'+code+'</b> No puede ser incluido en este despacho debido a que no se encuentra disponible en la facturación anticipada');
			ret = false;
			return;
		}
		
		if(parseInt(cant) > parseInt(limit.val())){
			ret = false;
			$(this).find('td').css({backgroundColor:'#FFFFBF'});
		}
	});
	
	return ret;
};

$('.ventas_form').submit(function(e){
	e.preventDefault();
	if($('#prod_list tr').size() == 0){
		show_error('No ha agregado productos a la guía de despacho');
		return;
	}
	
	$('#to_complete').remove();
	if(!checkCantidadesAnticipadas()){
		show_error("A ocurrido un error al intentar generar la guía de despacho<br> compruebe que las cantidades en los productos"
		+" marcados no sean mayores a los pendientes de despacho de la factura anticipada o que estén incluidos en la factura anticipada seleccionada");
		return;
	}
	
	var dets = $('#prod_list .prod_series[required]');
	var x = dets.size();
	for(var j=0; j<x; j++){
		var cant = $(dets[j]).parent().parent().find('.prod_cant').val();
		var val = $(dets[j]).val();
		if(val == ''){
			show_error('Debe ingresar todas las series de los productos que sean seriados');
			return;
		}
		val = val.split(',').length;
		if(val < cant){
			show_error('Debe ingresar todas las series de los productos que sean seriados');
			return;
		}
	}
	setCodigosAnticipados();
	
	
	var id = '#'+$(this).attr('id');
	confirmEnviarForm(id,id+'_res');
});

actualizar_totales = function(t,v,a){
	
t = t?t:"#prod_list";
v = v?v:"#total";
a = a!=undefined?a:true;
	
var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
		cost = toNumber($(this).find('.prod_costo').val());
	
		total += price*cant;
		total_costo += cost*cant;
		total_margen += price-cost;
		total_neto += price*tc*cant;
	});
	
	$(v).text(mil_format(total.toFixed(tc_dec)));
	$(v+'_costo').text(mil_format(total_costo.toFixed(tc_dec)));
	$(v+'_margen').text(mil_format(total_margen.toFixed(tc_dec)));
	
	if(total_costo > 0)
		$(v+'_margen_p').text(mil_format(((total-total_costo)/total_costo*100).toFixed(3)));
	else
		$('#total_margen_p').text('0');
	$(v+'_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$(v+'_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$(v+'_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));
	
	if(a){
		$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
		$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));
	}

};

check_codigos = function(){
	var tr = $("#prod_list tr.main").size();
	var code = $("#prod_list :hidden[name=prod[]]").size();
	return tr==code;
};

/*create_fake_prods = function(){
	var tab = $("#prods table").clone();
	tab.find('tbody').attr('id','prod_list_fake');
	tab.find('#total').attr('id','fake_total');
	tab.find('#total_costo').attr('id','fake_total_costo');
	tab.find('#total_margen').attr('id','fake_total');
	tab.find('#total_margen_p').attr('id','fake_total_margen_p');
	tab.find('#total_neto').attr('id','fake_total_neto');
	tab.find('#total_iva').attr('id','fake_total_iva');
	tab.find('#total_pagar').attr('id','fake_total_pagar');
	tab.appendTo('#fake_prods');
	
	var btn = $('<input>').multiAttr({
		type: 'button',
		class: 'addbtn',
		value: 'Agregar detalle'
	}).click(function(){
		get_fake_detail().appendTo('#prod_list_fake');
	});
	
	$("<div class='center'>").append(btn).appendTo('#fake_prods');
};

var get_fake_detail = function(){
	var val_change = function(){
		actualizar_detalles('#prod_list_fake');
		actualizar_totales('#prod_list_fake','#fake_total',false);
	};
	e = $('#prods_form tr').clone(true);
	e.find('.del_detail').click(function(){
		actualizar_totales('#prod_list_fake','#fake_total',false);
	});
	e.find('.prod_codigo').multiAttr({class:'prod_codigo', name:'fake_prod[]'});
	e.find('.prod_desc').attr('name','fake_desc[]');
	e.find('.prod_serie').attr('name','fake_serie[]');
	e.find('.prod_descuento').attr('name','fake_descuento[]');
	e.find('.prod_proveedor').attr('name','fake_proveedor[]');
	e.find('.prod_cant').attr('name','fake_cant[]').change(val_change);
	e.find('.prod_price').attr('name','fake_precio[]').change(val_change);
	e.find('.prod_costo').attr('name','fake_costo[]').change(val_change);
	e.find('.margen_p').change(val_change);
	
	return e;
};*/

$('.set_series').click(function(){
	var tr = $(this).parent().parent();
	var series = tr.find('.prod_series').val().split(',');
	var cant = parseInt(tr.find('.prod_cant').val());
	if(!cant){
		show_error('Debe asignar cantidades en los detalles para poder asignar las series');
		return;
	}
	
	var div = $('<div>').addClass('overlay').addClass('center').css({minWidth:'250px',width:'250px',maxHeight:'500px',overflowY:'auto',overflowX:'hidden'})
	.append($('<div>').addClass('secc_bar')
	.text('Asignar series a los detalles')).appendTo('#content')
	.overlay({load: true, fixed:false, closeOnClick: false, oneInstance: false, mask: {color: '#ddd'}, close: 'no-close'});
	
	for(var i = 0; i<cant; i++){
		var txt = $('<input>').multiAttr({
			type: 'text',
			class: 'inputtext'
		}).width(200).keyup(function(e){
			if(e.keyCode == 13){
				var nxt = $(this).next();
				if(nxt.attr('type') == 'button'){
					nxt.trigger('click');
				}else{
					nxt.trigger('focus').select();
				}
			}
		}).appendTo(div);
		
		if(series[i]){
			txt.val(series[i]);
		}
	}
	
	var send = $('<button type="button">').addClass('addbtn').text('Asignar Series').click(function(){
		var arrser = [];
		var size = $('.overlay :text').each(function(){
			var val = $(this).val();
			if(val != '')
				arrser.push(val);
		}).size();
		
		tr.find('.prod_series').val(arrser.join(','));
		div.overlay().close();
		div.remove();
	}).appendTo(div);
});

/*$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac);
});
*/

var verificar_factura_anticipada = function(nota_venta){
	show_loader();
	delete_anticipados();
	$.getJSON('sites/ventas/guia_despacho/proc/get_facturas_anticipadas.php',{ dc_nota_venta: nota_venta, facturable: $(':hidden[name=facturable]').val() },function(fa_data){
		
		if(fa_data == '<not-found>'){
			hide_loader();
			return;
		}
		
		$('#gd_comentario').multiAttr({'cols':70, 'rows':2}).parent().insertAfter('#after_header');
		
		var data = fa_data[0];
		var ids = fa_data[1];
		
		var table = $('<table>').addClass('tab').width(450).css({marginLeft:20})
		.append($('<caption>').text("Facturas anticipadas hechas sobre la nota de venta"))
		.append($('<thead>')
			.append($('<tr>')
				.append($('<th>').text('Producto').width('33%'))
				.append($('<th>').text('Cantidad Facturada').width('33%'))
				.append($('<th>').text('Cantidad Despachada'))
			)
		).appendTo('#factura_anticipada_container');
		
		for(var i in data){
			$('<tr>')
				.append($('<td>')
					.attr('colspan',3)
					.append($('<input>').multiAttr({type:'radio',name:'dc_factura_anticipada','class':'dc_factura_anticipada'}).val(ids[i]))
					.append('Factura: ')
					.append($('<strong>').text(i))
					.css({backgroundColor:'#EEE'})
				)
				.appendTo(table);
				
			var rows = data[i];
			
			for(var j in rows){
				
				var cants = $('<input>').multiAttr({
					'type':'hidden',
					'class': 'F'+ids[i]+' codigo_anticipado',
					'id': ids[i]+'_'+rows[j][0].replace(/[\(\)\=#\.\/\s]/gi,'_').replace(/^\s*|\s*$/g,"")
				}).val(parseInt(rows[j][1])-parseInt(rows[j][2]));
				
				$('<tr>')
					.append($('<td>')
						.append($('<b>').text(rows[j][0]))
						.append(cants))
					.append($('<td>').text(rows[j][1]).attr('align','center'))
					.append($('<td>').text(rows[j][2]).attr('align','center'))
					.appendTo(table);
			}
		}
		
		$('<tr>')
				.append($('<td>')
					.attr('colspan',3)
					.append($('<input>').multiAttr({type:'radio',name:'dc_factura_anticipada','class':'dc_factura_anticipada'}).val(0))
					.append('No utilizar factura anticipada')
					.css({backgroundColor:'#EEE'})
				)
				.appendTo(table);
		
		$('.dc_factura_anticipada').first().click();
		
		hide_loader();
	});
};

var setCodigosAnticipados = function(){
	$('.codigo_anticipado').removeAttr('name');
	var factura = $(':radio:checked[name=dc_factura_anticipada]');
	if(factura.size() == 0)
		return true;
		
	factura = factura.val();
	
	if(factura == 0)
		return true;
	
	$('.prod_codigo').each(function(){
		var code = $(this).val().replace(/[\(\)\=#\.\/\s]/gi,'_').replace(/^\s*|\s*$/g,"");
		$('.F'+factura+'#'+factura+'_'+code).attr('name','ant_code['+code+']');
	});
	
};

var delete_anticipados = function(){
	$('#factura_anticipada_container').html('');
};