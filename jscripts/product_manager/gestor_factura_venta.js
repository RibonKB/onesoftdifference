var js_data = {
	tr: $('<tr>'),
	pendientes: [],
	table: '#pend_table',
	tdClasses: ['semaforo','chkbx','docnum','docdate','docclient','docneto','dociva','doctotal'],
	tdAlign: ['left','center','left','left','left','right','right','right'],
	clientes: [],
	proveedores: [],
	tipo_doc: 0,
	fac_url: 'sites/ventas/factura_venta/proc/cr_factura_venta.php',
	init: function(){
		$(this.table+' thead th').each(function(i){
			$(this).addClass(js_data.tdClasses[i]);
		});
		
		$('#filter_menu').css({
			background:'#fff',
			border:'1px solid #777',
			position:'absolute',
			top:0,
			right:0
		}).children('button').click(function(){
			$(this).next().next().toggle();
		}).next().next().find(':checkbox').change(function(){
			if(!$(this).attr('checked'))
				$('.'+$(this).attr('id')).addClass('hidden');
			else
				$('.'+$(this).attr('id')).removeClass('hidden');
		});
		
		for(i in this.tdClasses){
			this.tr.append($('<td>').addClass(this.tdClasses[i]).attr('align',this.tdAlign[i]));
		}
		
		this.tr.children('.chkbx')
		.append($('<input>').multiAttr({type:'checkbox',name:'oc_detail[]'}))
		.append($('<input>').multiAttr({type:'hidden'}).addClass('det_cli_id'));
		
		this.initClientes();
		for(i in this.pendientes[this.tipo_doc]){
			this.addDetail(this.pendientes[this.tipo_doc][i]);
		}
		this.updateHidden();
		
		$('#load_documentos').submit(this.submitFilter);
		$('#crear_factura_venta').submit(this.submitFVcreation);
		$('#select_all').click(this.selectAll);
		
	},
	addDetail:function(det){
		var aux = this.tr.clone(true);
		var total = parseFloat(det.dq_neto)+parseFloat(det.dq_iva);

		aux.children('.docnum').text(det.dq_documento);
		aux.children('.docdate').text(det.df_emision);
		aux.children('.docclient').text(this.getCliente(det.dc_cliente));
		aux.children('.docneto').text(mil_format(det.dq_neto));
		aux.children('.dociva').text(mil_format(det.dq_iva));
		aux.children('.doctotal').text(mil_format(total.toFixed(2)));
		aux.children('.chkbx').children(':checkbox').val(det.dc_documento).next(':hidden').val(det.dc_cliente);
		aux.appendTo('#prod_list');
		
	},
	initClientes: function(){
		$('#sel_cli option').each(function(){
			js_data.clientes[$(this).attr('value')] = $(this).text();
		});
	},
	getCliente: function(id){
		return this.clientes[id]?this.clientes[id]:'';
	},
	updateHidden:function(){
		$('#filter_menu :checkbox').change();
	},
	submitFilter:function(e){
		e.preventDefault();
		if(!validarForm('#'+this.id)){
			show_error("Los filtros marcados son obligatorios");
			return;
		}
		
		var filter = {
			doc : $(this.sel_doc).val(),
			inim : parseInt($(this.sel_inim).val()),
			finm : parseInt($(this.sel_finm).val()),
			cli : $(this.sel_cli).val()
		}
		
		$('#prod_list tr').remove();
		var tipo = $(this.sel_tipo).val();

		for(i in js_data.pendientes[tipo]){
			js_data.detailFilter(js_data.pendientes[tipo][i],filter);
		}
		js_data.tipo_doc = tipo;
		js_data.updateHidden();
	},
	detailFilter: function(det,filter){
		if((parseInt(det.dc_month) > filter.finm) || (parseInt(det.dc_month) < filter.inim))
			return;
		if(filter.cli != 0 && det.dc_cliente != filter.cli)
			return;
		if(filter.doc != '' && det.dq_nota_venta.indexOf(filter.doc) == -1)
			return;
		this.addDetail(det);
	},
	submitFVcreation: function(e){
		e.preventDefault();
		var cb = $('#prod_list :checked');
		if(cb.size() == 0){
			show_error('No ha seleccionado detalles para ser comprados');
			return;
		}
		var cli = '0';
		cb.each(function(){
			var c = $(this).next().val();
			if(cli.indexOf(','+c) == -1)
				cli += ','+c;
		});
		js_data.gp_cli = cli.split(',');
		js_data.query_cb = $(this).serialize();
		
		js_data.submitFormated();
	},
	submitFormated: function(){
		if(this.gp_cli.length == 2 && this.tipo_doc == 1){
			loadpage(this.fac_url+'?tipo_doc=1&cli_id_mass='+this.gp_cli[1],this.query_cb);
			return;
		}
		
		this.getFinalForm().appendTo('#content')
		.overlay({load: true, fixed:false, closeOnClick:false, oneInstance: false});
	},
	getFinalForm: function(){
		var div = $('<div>').addClass('panes');
		var head = $('<div>').addClass('secc_bar').text('Crear Factura venta').appendTo(div);
		var form = $('<form>').attr({'action':js_data.fac_url,id:'cr_final_factura_venta'}).submit(js_data.submitValidate).appendTo(div);
		var fs = $('<fieldset>').appendTo(form);
		var info = $('<div>').addClass('info').text('Indique los datos faltantes antes de enviar a crear la factura de venta').appendTo(fs);
		if(js_data.gp_cli.length > 2){
			var lbl = $('<label>').text('Seleccionar cliente [*]').append('<br>').appendTo(fs);
			lbl.after('<br><br>');
			var sel_cli = $('<select>').multiAttr({name:'cli_id_mass', required:'required'}).addClass('inputtext').appendTo(lbl);
			for(var k in js_data.gp_cli){
				$('<option>').text(js_data.getCliente(js_data.gp_cli[k])).val(js_data.gp_cli[k]).appendTo(sel_cli);
			}
		}else{
			$('<input>').multiAttr({name:'cli_id_mass',type:'hidden'}).val(js_data.gp_cli[1]).appendTo(fs);
		}
		if(js_data.tipo_doc == 0){
			var lbl2 = $('<label>').text('Seleccionar modo del detalle [*]').append('<br>').appendTo(fs);
			lbl2.after('<br><br>');
			var sel_mode = $('<select>').multiAttr({name:'mode_id', required:'required'}).addClass('inputtext').appendTo(lbl2);
			$('<option>').text('Acumulado').val(1).appendTo(sel_mode);
			$('<option>').text('Detallado').val(2).appendTo(sel_mode);
			$('<option>').text('Glosa').val(3).appendTo(sel_mode);
		}
		$('<input>').multiAttr({type:'hidden',name:'tipo_doc'}).val(js_data.tipo_doc).appendTo(fs);
		$('<div>').addClass('center').append($('<input>').multiAttr({type:'submit',value:'Generar factura'}).addClass('addbtn')).appendTo(fs);
		return $('<div>').addClass('overlay').append(div);
	},
	submitValidate: function(e){
		e.preventDefault();
		var id = '#'+$(this).attr('id');
		if(!validarForm(id)){
			show_error('Los campos marcados son obligatorios');
			return;
		}
		var query = $(this).serialize();
		loadpage(js_data.fac_url+'?'+query,js_data.query_cb);
	},
	selectAll: function(){
		$('#prod_list .chkbx :checkbox').attr('checked',$(this).attr('checked'));
	}
};