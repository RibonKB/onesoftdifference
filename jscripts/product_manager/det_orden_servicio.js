var tc_dec = 0;
var tc = 1;

$('.del_detail').click(function(){
	var tr = $(this).parent().parent();
	var id = tr.find(':hidden[name=id_os_detail[]]');
	if(id.size()){
		$('<input>').multiAttr({'name':'to_delete[]','type':'hidden'}).val(id.val()).appendTo('#cr_detalle_factura');
	}
	tr.remove();
	actualizar_detalles();
	actualizar_totales();
});

$('.prod_codigo').change(function(){
	$(this).val('');
});

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',{
	formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},
	minChars: 2,
	width:300
	}).result(result_ac);
});

var create_code_div = function(code,id,desc){
	var ed_btn = $('<img>').multiAttr({
		class: 'ed_code right hidden',
		src: 'images/editbtn.png',
		alt: '[|]',
		title: 'Editar código',
		width: '13',
		height: '13'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'searchbtn prod_codigo',
			type: 'text',
			size: '7'
		}).autocomplete('sites/proc/autocompleter/producto.php',{
			formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
		.result(result_ac)
		.replaceAll(innerdiv);
	});
					
	var div = $('<div>').css({
		border:'1px solid #CCC',
		background:'#EEE',
		padding:'5px',
		fontWeight:'bold'
	}).mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.text(code)
	.prepend(ed_btn);
					
	$('<input>').multiAttr({
		type:'hidden',
		name:'prod[]'
	}).val(id).appendTo(div);
					
	$('<input>').multiAttr({
		type:'hidden',
		class:'hidden_desc'
	}).val(desc).appendTo(div);
	
	return div;
};

$('.os_form').submit(function(e){
	e.preventDefault();
	if(!check_codigos()){
		show_error('Los códigos de los productos deben estar todos perfectamente asignados');
		$('#prod_list').parent().addClass('invalid');
		return;
	}
	/*if($('#prod_list tr').size() == 0){
		alert('No ha agregado productos al detalle');
		return;
	}*/
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

var check_codigos = function(){
	var tr = $("#prod_list tr.main").size();
	var code = $("#prod_list :hidden[name=prod[]]").size();
	return tr==code;
};

var actualizar_detalles = function(t){
	/*t = t?t:'#prod_list';
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val()).toFixed(conf.moneda_ndecimal);
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0;$(this).find('.prod_price').val(0);}
		
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant)));
	});*/
	t = t?t:'#prod_list';
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val()).toFixed(conf.moneda_ndecimal);
		cost = toNumber($(this).find('.prod_costo').val()).toFixed(conf.moneda_ndecimal);
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(conf.moneda_ndecimal)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(conf.moneda_ndecimal)));
	});
};

$('.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('.prod_cant').change(function(){
	var mx = $(this).next('.min_value');
	if(mx.size()){
		if(parseFloat($(this).val()) < parseFloat(mx.val())){
			$(this).val(mx.val());
		}
	}
	actualizar_detalles();
	actualizar_totales();
});

var actualizar_totales = function(t,v,a){
	
/*t = t?t:"#prod_list";
v = v?v:"#total";
a = a!=undefined?a:true;
	
var total = 0;
var total_neto = 0;
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
	
		total += price*cant;
		total_neto += price*cant;
	});
	
	$(v).text(mil_format(total));
	
	$(v+'_neto').text(mil_format(total_neto.toFixed(conf.moneda_ndecimal)));
	$(v+'_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(conf.moneda_ndecimal)));
	$(v+'_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(conf.moneda_ndecimal)));
	
	if(a){
		$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(conf.moneda_ndecimal));
		$('input[name=cot_neto]').val(total_neto.toFixed(conf.moneda_ndecimal));
	}*/
	
t = t?t:"#prod_list";
v = v?v:"#total";
a = a!=undefined?a:true;
	
var total = 0;
var total_costo = 0;
var total_neto = 0;
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
		cost = toNumber($(this).find('.prod_costo').val());
		total += price*cant;
		total_costo += cost*cant;
		total_neto += price*cant;
	});
	
	$(v).text(mil_format(total.toFixed(conf.moneda_ndecimal)));
	$(v+'_costo').text(mil_format(total_costo.toFixed(conf.moneda_ndecimal)));
	$(v+'_neto').text(mil_format(total_neto.toFixed(conf.moneda_ndecimal)));
	$(v+'_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(conf.moneda_ndecimal)));
	$(v+'_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(conf.moneda_ndecimal)));
	if(a){
		$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(conf.moneda_ndecimal));
		$('input[name=cot_neto]').val(total_neto.toFixed(conf.moneda_ndecimal));
	}

};

var get_valid_detail = function(d){
	row = $('#prods_form tr').clone(true);
	row.find('.prod_desc').val(d.dg_descripcion);
	row.find('.prod_cant').val(d.dq_cantidad);
	row.find('.prod_price').val(parseFloat(d.dq_precio_venta).toFixed(tc_dec)); 
	row.find('.prod_costo').val(parseFloat(d.dq_precio_compra).toFixed(tc_dec));
	//row.find('.prod_codigo').removeClass('searchbtn').addClass('inputtext').attr('readonly',true).val(d.dg_codigo);
	create_code_div(d.dg_codigo,d.dc_producto,d.dg_descripcion).replaceAll(row.find('.prod_codigo'));
	//(code,id,desc)
	return row;
};

var result_ac = function(e,row){
	el = $(this).parent().parent();
	cant = el.find('.prod_cant');
	
	if(!parseFloat(cant.val())){
		cant.val(1);
		cant = 1;
	}else{
		cant = cant.val();
	}
	
	el.find('.prod_price').val(mil_format(parseFloat(row[3])));
	el.find('.prod_costo').val(mil_format(parseFloat(row[4])));;
	
	create_code_div(row[0],row[5],row[1]).replaceAll(this);
	
	actualizar_detalles();
	actualizar_totales();
};

if(detalle_factura.length){
	for(i in detalle_factura){
		var q = detalle_factura[i].dq_despachado;
		var id = detalle_factura[i].dc_detalle;
		var d = get_valid_detail(detalle_factura[i]);
		d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_os_detail[]', value: id}));
		if(q > 0){
			d.find('.del_detail').remove();
			d.find('.ed_code').remove();
		}
		var minv = $('<input>').multiAttr({type:'hidden',class:'min_value'}).val(q);
		d.find('.prod_cant').after(minv);
		d.appendTo('#prod_list');
	}
	actualizar_detalles();
	actualizar_totales();
}else{
	$('#prod_add').trigger('click');
}

/*$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',{
	formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},
	minChars: 2,
	width:300
	}).result(result_ac);
}).trigger('click');*/