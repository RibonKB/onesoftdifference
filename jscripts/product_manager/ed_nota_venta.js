var tc;
var tc_dec;

$('#nv_cotizacion').autocomplete('sites/proc/autocompleter/cotizacion_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

var change_cotizacion = function(){
	var v = $(this).blur().attr('disabled',true).css({
		backgroundImage: 'url(images/ajax-loader.gif)',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center right'
	});

	$.getJSON('sites/ventas/nota_venta/proc/get_cotizacion_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la cotización especificada, intentelo denuevo');
			return;
		}else if(data[0] == '<empty>'){
			show_aviso('La cotización especificada no posee detalle, pero se asigna de todas formas');
			var id = data[1].dc_cotizacion;
		}else{
			var detalle = data[0];
			var id = data[1].dc_cotizacion;

			if($('#prod_tipo_cambio').val() != 0){
				if(confirm("¿Quiere cambiar el tipo de cambio de la nota de venta por el que utiliza la cotizaci\u00f3n?")){
					tc = data[1].dq_cambio;
					tc_dec = 2;
					$('#prod_tipo_cambio').val(data[1].dc_tipo_cambio);
					$('#nv_cambio').val(parseFloat(data[1].dq_cambio).toFixed(tc_dec));
				}
			}else{
				$('#prod_tipo_cambio').val(data[1].dc_tipo_cambio);
				tc_dec = 2;
				$('#prod_tipo_cambio').change();
				tc = data[1].dq_cambio;
				$('#nv_cambio').val(parseFloat(data[1].dq_cambio).toFixed(tc_dec));
			}
			set_detail(detalle);

		}

		create_cnumber_div(v.val()).width(v.width()).replaceAll(v);

		$(':hidden[name=nv_id_cotizacion]').val(id);

	});
}

$('#nv_cotizacion').change(change_cotizacion);

get_disabled = function(e){
	return $(e).blur().attr('disabled',true).css({
		backgroundImage: 'url(images/ajax-loader.gif)',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center right'
	});
}

set_detail = function(det){
	for(var i in det){
		if(det[i].dc_producto != null){

			var ro = false;
			if(det[i].dc_comprada > 0 && !permiso_41){
				ro = true;
			}

			row = $('#prods_form tr').clone(true);
			row.find('.fecha_arribo').val(det[i].df_fecha_arribo);
			row.find('.prod_desc').val(det[i].dg_descripcion).attr('readonly',ro).next('.id_detail').val(det[i].dc_nota_venta_detalle);
			row.find('.prod_proveedor').val(det[i].dc_proveedor).attr('disabled',ro);
			row.find('.prod_cant').val(det[i].dq_cantidad).attr('readonly',ro);
			if(det[i].dm_controla_inventario == 0){
				row.find('.prod_cant').next('.prod_min_cant').val(det[i].dc_comprada);
			}
			row.find('.prod_price').val((det[i].dq_precio_venta/tc).toFixed(tc_dec)).attr('readonly',ro);
			row.find('.prod_costo').val((det[i].dq_precio_compra/tc).toFixed(tc_dec)).attr('readonly',ro);
			row.find('.p_margen').attr('readonly',ro);
			//row.find('.prod_cebe').val(det[i].dc_cebe).trigger('change');
			row.appendTo('#prod_list');
			var code = create_code_div(det[i].dg_producto,det[i].dc_producto,det[i].dg_descripcion).replaceAll(row.find('.prod_codigo'));
			if(det[i].dc_comprada > 0 && det[i].dm_controla_inventario == 0){
				code.find('img').remove();
				row.find('.del_detail').remove();
			}else if(det[i].dm_controla_inventario == 1 && (det[i].dc_facturada > 0 || det[i].dc_despachada > 0)){
				code.find('img').remove();
				row.find('.del_detail').remove();
			}
		}

	}

	actualizar_detalles();
	actualizar_totales();
};

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac);
});

var create_code_div = function(code,id,desc){

	var ed_btn = $('<img>').multiAttr({
		class: 'right hidden',
		src: 'images/editbtn.png',
		alt: '[|]',
		title: 'Editar código',
		width: '13',
		height: '13'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'searchbtn prod_codigo',
			type: 'text',
			size: '7'
		}).autocomplete('sites/proc/autocompleter/producto.php',{
			formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
		.result(result_ac)
		.replaceAll(innerdiv);
	});

	var div = $('<div>').css({
		border:'1px solid #CCC',
		background:'#EEE',
		padding:'5px',
		fontWeight:'bold'
	}).mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.text(code)
	.prepend(ed_btn);

	$('<input>').multiAttr({
		type:'hidden',
		name:'prod[]'
	}).val(id).appendTo(div);

	$('<input>').multiAttr({
		type:'hidden',
		class:'hidden_desc'
	}).val(desc).appendTo(div);

	return div;
}

var create_cnumber_div = function(number){
	var edbtn = $('<img>').multiAttr({
		src: 'images/editbtn.png',
		title: 'Modificar cotización',
		alt: '[|]',
		class:'right hidden'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'inputtext',
			type: 'text',
			id: 'nv_cotizacion',
			size: '41',
			maxlength: '255'
		}).autocomplete('sites/proc/autocompleter/cotizacion_strict.php',{
			formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
			minChars: 2,
			width:300
		}).result(function(){
			$(this).change();
		}).change(change_cotizacion).replaceAll(innerdiv);

		$(':hidden[name=nv_id_cotizacion]').val(0);
	});

	var div = $('<div>').addClass('inputtext').css({
		background:'#EEE',
		fontWeight:'bold'
	}).text(number)
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.prepend(edbtn);

	return div;
}

var result_ac = function(e,row){

		el = $(this).parent().parent();
		cant = el.find('.prod_cant');
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}

		el.find('.prod_desc').val(row[1]);
		el.find('.prod_price').val(mil_format(parseFloat(row[3]/tc).toFixed(tc_dec)));
		el.find('.prod_costo').val(mil_format(parseFloat(row[4]/tc).toFixed(tc_dec)));
		//el.find('.prod_cebe').val(row[7]).trigger('change');

		create_code_div(row[0],row[5],row[1]).replaceAll(this);

		actualizar_detalles();
		actualizar_totales();
}


$("#prod_tipo_cambio").change(function(){
	indice = this.selectedIndex;
	if(indice == 0){
		$("#prod_info").show();
		$("#prods").hide();
	}else{
		$("#prod_info").hide();
		$("#prods").show();
		tc = this.options[indice].innerHTML.split("|");
		tc = toNumber(tc[1]);
		$('#nv_cambio').val(tc);
		tc_dec = $(":hidden[name=decimal"+$(this).val()+"]").val();
		actualizar_detalles();
		actualizar_totales();
	}
});

$('.margen_p').change(function(){
	var cost = parseFloat(toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec));
	var margin = toNumber($(this).val())/100;
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	var price = -cost/(margin - 1);
	$(this).parent().parent().find('.prod_price').val(mil_format(price));
	actualizar_detalles();
	actualizar_totales();
});

var actualizar_detalles = function(){
	$("#prod_list tr.main").each(function(){
		var cant = parseFloat($(this).find('.prod_cant').val());
		var price = toNumber($(this).find('.prod_price').val()).toFixed(tc_dec);
		var cost = toNumber($(this).find('.prod_costo').val()).toFixed(tc_dec);
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}

		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(tc_dec)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(tc_dec)));
		$(this).find('.margen').text(mil_format(((price-cost)*cant).toFixed(tc_dec)));
		if(price > 0)
			$(this).find('.margen_p').val(((price-cost)*100/price).toFixed(1));
		else
			$(this).find('.margen_p').val('-');
		var local = $(this).next();
		local.find('.l_price').text(mil_format((price*tc).toFixed(empresa_dec)));
		local.find('.l_total').text(mil_format((price*tc*cant).toFixed(empresa_dec)));
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
		local.find('.l_margen').text(mil_format(((price-cost)*tc).toFixed(empresa_dec)));
	});
}

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
});

$('.get_description').click(function(){
	var desc = $(this).parent().next().find('.hidden_desc').val();
	$(this).parent().next().next().children('.prod_desc').val(desc);
});

$('.prod_cebe').change(function(){
	var id = this.options[this.selectedIndex].id;
	$(this).next(':hidden').val(id);
});

$('.prod_cant,.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('.prod_cant').change(function(){
	var mn = $(this).next('.prod_min_cant');

	if(mn.size()){
		if(parseInt($(this).val()) < parseInt(mn.val())){
			$(this).val(mn.val());
			actualizar_detalles();
			actualizar_totales();
		}
	}
});

$('#nv_cambio').change(function(){
	nv_cambio = $(this).val();
	if(!parseFloat(tc)){
		nv_cambio = 1.0;
		$(this).val(1);
	}
	tc = nv_cambio;
	actualizar_detalles();
	actualizar_totales();
});

$('.ventas_form').submit(function(e){
	e.preventDefault();
	if(!check_codigos()){
		show_error('Los códigos de los productos deben estar todos perfectamente asignados');
		$('#prod_list').parent().addClass('invalid');
		return;
	}
	if($('#prod_list tr').size() == 0){
		alert('No ha agregado productos a la nota de venta');
		return;
	}
	i = '#'+$(this).attr('id');
	$('.prod_proveedor').attr('disabled',false);
	confirmEnviarForm(i,i+'_res');
});

actualizar_totales = function(){

var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$("#prod_list tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
		cost = toNumber($(this).find('.prod_costo').val());

		total += price*cant;
		total_costo += cost*cant;
		total_margen += (price-cost)*cant;
		total_neto += price*tc*cant;
	});

	$('#total').text(mil_format(total.toFixed(tc_dec)));
	$('#total_costo').text(mil_format(total_costo.toFixed(tc_dec)));
	$('#total_margen').text(mil_format(total_margen.toFixed(tc_dec)));
	$('#total_margen_tc').text(mil_format((total_margen/tc).toFixed(tc_dec)));

	if(total > 0)
		$('#total_margen_p').text(mil_format(((total-total_costo)*100/total).toFixed(3)));
	else
		$('#total_margen_p').text('0');
	$('#total_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$('#total_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$('#total_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));

	$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
	$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));

}

var check_codigos = function(){
	var tr = $("#prod_list tr.main").size();
	var code = $("#prod_list :hidden[name=prod[]]").size();
	return tr==code;
}
