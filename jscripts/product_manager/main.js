

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac);
});

result_ac = function(e,row){
		
		el = $(this).parent().parent();
		cant = el.find('.prod_cant');
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}
		
		el.find('.prod_desc').val(row[1]);
		el.find('.prod_price').val(mil_format(parseFloat(row[3]/tc).toFixed(tc_dec)));
		el.find('.prod_costo').val(mil_format(parseFloat(row[4]/tc).toFixed(tc_dec)));
		
		create_code_div(row[0],row[5],row[1],true).replaceAll(this);
		
		actualizar_detalles();
		actualizar_totales();
}

$('.margen_p').change(function(){
	cost = toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec);
	margin = toNumber($(this).val());
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	price = $(this).parent().parent().find('.prod_price').val(mil_format(parseFloat(cost)+(cost*margin/100)));
	actualizar_detalles();
	actualizar_totales();
});

actualizar_detalles = function(t){
	t = t?t:'#prod_list';
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val()).toFixed(tc_dec);
		cost = toNumber($(this).find('.prod_costo').val()).toFixed(tc_dec);
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}
		
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(tc_dec)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(tc_dec)));
		$(this).find('.margen').text(mil_format((price-cost).toFixed(tc_dec)));
		if(cost > 0)
		$(this).find('.margen_p').val(((price-cost)*100/cost).toFixed(1));
		else
		$(this).find('.margen_p').val('0');
		local = $(this).next();
		local.find('.l_price').text(mil_format((price*tc).toFixed(empresa_dec)));
		local.find('.l_total').text(mil_format((price*tc*cant).toFixed(empresa_dec)));
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
		local.find('.l_margen').text(mil_format(((price-cost)*tc).toFixed(empresa_dec)));
	});
}

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
	actualizar_totales();
});

$('.get_description').click(function(){
	var desc = $(this).parent().next().find('.hidden_desc').val();
	$(this).parent().next().next().children('.prod_desc').val(desc);
});

$('.prod_cant,.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

actualizar_totales = function(t,v,a){
	
t = t?t:"#prod_list";
v = v?v:"#total";
a = a?a:true;
	
var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
		cost = toNumber($(this).find('.prod_costo').val());
	
		total += price*cant;
		total_costo += cost*cant;
		total_margen += price-cost;
		total_neto += price*tc*cant;
	});
	
	$(v).text(mil_format(total.toFixed(tc_dec)));
	$(v+'_costo').text(mil_format(total_costo.toFixed(tc_dec)));
	$(v+'_margen').text(mil_format(total_margen.toFixed(tc_dec)));
	
	if(total_costo > 0)
		$(v+'_margen_p').text(mil_format(((total-total_costo)/total_costo*100).toFixed(3)));
	else
		$('#total_margen_p').text('0');
	$(v+'_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$(v+'_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$(v+'_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));
	
	if(a){
		$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
		$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));
	}

}