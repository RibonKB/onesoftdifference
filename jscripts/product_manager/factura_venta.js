var tc = 1;
var tc_dec = empresa_dec;

$('#fv_nota_venta').autocomplete('sites/proc/autocompleter/nota_venta_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

var obligar_facturacion_completa = function(guias){
	return;
	$('#prod_list .prod_cant').attr('readonly',true);
	$('#prod_list .del_detail').remove();
	
	$('<div>')
	.append('<strong>Debe facturar la nota de venta completamente debido a que esta posee productos anteriormente despachados</strong><br>')
	.append('En caso de no querer facturar completamente la nota de venta debe facturar antes las guias de despacho asociadas.<br>')
	.append('Las guías que afectaron este proceso fueron las siguientes: <strong style="color:#0a79c7;font-size:1.5em;">'+guias+'</strong>')
	.addClass('alert').prependTo('#prods');
};

change_nota_venta = function(){
	if(!parseInt($(this).val())) return;
	
	var v = get_disabled(this);
	
	$.getJSON('sites/ventas/factura_venta/proc/get_nota_venta_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la nota de venta especificada, intentelo denuevo');
			return;
		}
		
		if(data == '<not-facturable>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('La nota de venta seleccionada no es facturable.');
			return;
		}
	
		if($('#prod_list .main').size() > 0){
			if(confirm('Se eliminarán todos los detalles que haya agregado\n¿Continuar?')){
				$('#prod_list .main').remove();
			}else{
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				return;
			}
		}
		disable_button('#prod_add');
		$('#fv_orden_servicio').attr('disabled',true);
		$(':hidden[name=fv_id_orden_servicio]').val(0);

		if(data[0] == '<empty>'){
			show_aviso('La nota de venta especificada no posee detalle, pero se asigna de todas formas');
			var id = data[1].dc_nota_venta;
		}else{
			var det = data[0];
			var id = data[1].dc_nota_venta;
			var cambio = data[1].dq_cambio;
			var al_dia = data[1].dm_al_dia;
			var costeo = data[1].dm_costeable;
			var gd_fisicas = data[1].dc_guias_fisicas;
			var num_guias = data[1].dq_guias;
			$('#fv_ejecutivo').val(data[1].dc_ejecutivo);
			$(':input[name=fv_tipo_impresion][value='+data[1].dm_tipo_impresion_factura+']').click();
			var factured = false;
			var cambio_dia = $(':hidden[name=cambio'+data[1].dc_tipo_cambio+']');
			
			if(al_dia == 0 && cambio_dia.size()){
				
				for(i in det){
					det[i].dq_cantidad -= det[i].dc_facturada;
					if(det[i].dc_producto != null && det[i].dq_cantidad > 0){
						var q = det[i].dq_cantidad;
						var desp = det[i].dc_despachada
						var recep = det[i].dc_recepcionada;
						var fact = det[i].dc_facturada;
						var tipo = det[i].dm_tipo;
						var detalle = det[i];
						d = get_valid_detail(det[i]);
						d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_detail[]', value: det[i].dc_nota_venta_detalle}));
						var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
						d.find('.despach_cant').val(desp);
						d.find('.recep_cant').val(recep);
						d.find('.fact_cant').val(fact);
						d.find('.prod_cant').after(maxv);
						d.appendTo('#prod_list');
						factured = true;
						if(costeo == 1 && tipo == 0){
							get_fake_detail_cont(detalle).appendTo('#prod_list_fake').find('.prod_cant').trigger('change');
						}
					}
				}
			}else{
				
				cambio_dia = cambio_dia.val();
				for(i in det){
					
					det[i].dq_cantidad -= det[i].dc_facturada;
					det[i].dq_precio_venta = (det[i].dq_precio_venta/cambio)*cambio_dia;
					det[i].dq_precio_compra = (det[i].dq_precio_compra/cambio)*cambio_dia;
					if(det[i].dc_producto != null && det[i].dq_cantidad > 0){
						var q = det[i].dq_cantidad;
						var desp = det[i].dc_despachada
						var recep = det[i].dc_recepcionada;
						d = get_valid_detail(det[i]);
						d.addClass('strict').children('td:first').append($('<input>').multiAttr({type:'hidden', name:'id_detail[]', value: det[i].dc_nota_venta_detalle}));
						var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
						d.find('.despach_cant').val(desp);
						d.find('.recep_cant').val(recep);
						d.find('.prod_cant').after(maxv);
						d.prependTo('#prod_list');
						factured = true;
					}
				}

			}
			
			if(gd_fisicas > 0){
				obligar_facturacion_completa(num_guias);
			}
			
			if(!factured){
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				show_aviso('La nota de venta ya a sido completamente facturada');
				$('#fv_orden_servicio').attr('disabled',false);
				enable_button('#prod_add');
				return;
			}
			
			actualizar_detalles();
			actualizar_totales();
			
			create_number_div(v.val(),'nota_venta',change_nota_venta,function(){
				$('#fv_orden_servicio').attr('disabled',false);
				if(confirm('¿Desea mantener los detalles agregados?')){
					//$('#prod_list :hidden[name=id_detail[]]').remove();
					//$('#prod_list .code-div').each(function(){$(this).prepend(create_edit_code_btn());});
					$('#prod_list .prod_codigo[readonly]').attr('readonly',false).addClass('searchbtn').autocomplete('sites/proc/autocompleter/producto.php',{
						formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
					.result(result_ac);
					$('#prod_list .max_value').remove();
				}else{
					$('#prod_list .strict').remove();
				}
				enable_button('#prod_add');
			}).width(v.width()).replaceAll(v);
			$(':hidden[name=fv_id_nota_venta]').val(id);
			
		}
	});
};

$('#fv_nota_venta').change(change_nota_venta);

$('#fv_orden_servicio').autocomplete('sites/proc/autocompleter/orden_servicio_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

change_orden_servicio = function(){
	var v = get_disabled(this);
	
	$.getJSON('sites/ventas/factura_venta/proc/get_orden_servicio_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la orden de servicio o esta no es facturable o no se ha cerrado');
			return;
		}
		
		if(data == '<empty>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('La orden de servicio es facturable, pero no se ha especificado un detalle de facturación');
			return;
		}
		
		if($('#prod_list .main').size() > 0){
			if(confirm('Se eliminarán todos los detalles que haya agregado\n¿Continuar?')){
				$('#prod_list .main').remove();
			}else{
				v.attr('disabled',false).css({backgroundImage: ''}).val('');
				return;
			}
		}
		disable_button('#prod_add');
		$('#fv_nota_venta').attr('disabled',true);
		$(':hidden[name=fv_id_nota_venta]').val(0);
		
		var detalle = data[0];
		var fake = data[1];

			var total_fake = 0.0;
			var total_costo_fake = 0.0;
			for(i in detalle){
				total_fake += parseFloat(detalle[i].dq_precio_venta)*detalle[i].dq_cantidad;
				total_costo_fake += parseFloat(detalle[i].dq_precio_compra)*detalle[i].dq_cantidad;
				d = get_valid_detail(detalle[i]);
				d.find('.del_detail').remove();
				d.find('.prod_serie').val(detalle[i].dg_serie);
				d.addClass('strict').prependTo('#prod_list');
			}
			
		if(fake.dg_glosa_facturacion != ''){
			var dg = get_fake_detail();
			dg.find('.prod_desc').val(fake.dg_glosa_facturacion);
			
			dg.find('.prod_price').val(total_fake);
			dg.find('.prod_costo').val(total_costo_fake);
			dg.appendTo('#prod_list_fake');
			
			actualizar_detalles('#prod_list_fake');
			actualizar_totales('#prod_list_fake','#fake_total',false);
		$(':radio[name="fv_tipo_impresion"][value=0]').click();
		$('#tabs').children().next().children().click();
		}
		
		$('#fv_comentario').val(fake.dg_solucion);
		
		actualizar_detalles();
		actualizar_totales();
		
		create_number_div(v.val(),'orden_servicio',change_orden_servicio,function(){
			$('#fv_nota_venta').attr('disabled',false);
			$('#prod_list .strict').remove();
			enable_button('#prod_add');
		}).width(v.width()).replaceAll(v);
		$(':hidden[name=fv_id_orden_servicio]').val(fake.dc_orden_servicio);
		
	});
};

$('#fv_orden_servicio').change(change_orden_servicio);

get_disabled = function(e){
	return $(e).blur().attr('disabled',true).css({
		backgroundImage: 'url(images/ajax-loader.gif)',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center right'
	});
};

get_valid_detail = function(d){
	row = $('#prods_form tr').clone(true);
	row.find('.prod_desc').val(d.dg_descripcion);
	row.find('.prod_proveedor').val(d.dc_proveedor);
	row.find('.prod_cant').val(d.dq_cantidad);
	row.find('.prod_price').val(parseFloat(d.dq_precio_venta).toFixed(tc_dec)); 
	row.find('.prod_costo').val(parseFloat(d.dq_precio_compra).toFixed(tc_dec));
	row.find('.prod_codigo').removeClass('searchbtn').addClass('inputtext').attr('readonly',true).val(d.dg_codigo);
	if(d.dm_requiere_serie == '1')
		row.find('.prod_series').attr('required',true);
	if(d.dg_series)
		row.find('.prod_series').val(d.dg_series);
	//create_code_div(d.dg_codigo,d.dc_producto,d.dg_descripcion).replaceAll(row.find('.prod_codigo'));
	return row;
};

add_invalid_detail = function(d){
	row = $('<tr>');
	col = $('<td>').css({
		border: '1px solid #F00',
		background: '#FFFFBF'
	});
	
	crbtn = $('<img>').multiAttr({
		src: 'images/addbtn.png',
		alt: '[+]',
		title: 'Crear el producto'
	}).click(function(){
		loadOverlay('sites/ventas/proc/cr_producto.php?c='+encodeURIComponent(d.dg_producto)+'&d='+encodeURIComponent(d.dg_descripcion)+'&q='+encodeURIComponent(d.dq_precio_compra)+'&v='+encodeURIComponent(d.dq_precio_venta));
	});
	
	col.clone().prepend(crbtn).appendTo(row);
	col.clone().text(d.dg_producto).appendTo(row);
	col.clone().text(d.dg_descripcion).appendTo(row);
	col.clone().text('-').appendTo(row);
	col.attr('align','right');
	col.clone().text(mil_format(d.dq_cantidad)).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_venta).toFixed(tc_dec))).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_venta*d.dq_cantidad).toFixed(tc_dec))).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_compra).toFixed(tc_dec))).appendTo(row);
	col.clone().text(mil_format((d.dq_precio_compra*d.dq_cantidad).toFixed(tc_dec))).appendTo(row);
	col.clone().text('-').appendTo(row);
	col.clone().text('-').appendTo(row);
	$('#prod_list').prev('thead').append(row);
};

create_code_div = function(code,id,desc,ed){
					
	var div = $('<div>').css({
		border:'1px solid #CCC',
		background:'#EEE',
		padding:'5px',
		fontWeight:'bold'
	}).addClass('code-div')
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.text(code);
	
	if(ed){
		div.prepend(create_edit_code_btn());
	}
					
	$('<input>').multiAttr({
		type:'hidden',
		name:'prod[]'
	}).val(id).appendTo(div);
					
	$('<input>').multiAttr({
		type:'hidden',
		class:'hidden_desc'
	}).val(desc).appendTo(div);
	
	return div;
};

create_edit_code_btn = function(){
	return $('<img>').multiAttr({
		class: 'right hidden',
		src: 'images/editbtn.png',
		alt: '[|]',
		title: 'Editar código',
		width: '13',
		height: '13'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'searchbtn prod_codigo',
			type: 'text',
			size: '7'
		}).autocomplete('sites/proc/autocompleter/producto.php',{
			formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
		.result(result_ac)
		.replaceAll(innerdiv);
	});
};

create_number_div = function(number,acl,chfn,cb){
	var edbtn = $('<img>').multiAttr({
		src: 'images/editbtn.png',
		title: 'Modificar',
		alt: '[|]',
		class:'right hidden'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'inputtext',
			type: 'text',
			id: 'fv_'+acl,
			size: '41',
			maxlength: '255'
		}).autocomplete('sites/proc/autocompleter/'+acl+'_strict.php',{
			formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
			minChars: 2,
			width:300
		}).result(function(){
			$(this).change();
		}).change(chfn).replaceAll(innerdiv);
		
		$(':hidden[name=fv_id_'+acl+']').val(0);
		cb();
	});
	
	var div = $('<div>').addClass('inputtext').css({
		background:'#EEE',
		fontWeight:'bold'
	}).text(number)
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.prepend(edbtn);
	
	return div;
};

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac).trigger('focus');
});

result_ac = function(e,row){
		
		el = $(this).parent().parent();
		cant = el.find('.prod_cant');
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}
		
		el.find('.prod_desc').val(row[1]);
		el.find('.prod_price').val(mil_format(parseFloat(row[3]/tc).toFixed(tc_dec)));
		el.find('.prod_costo').val(mil_format(parseFloat(row[4]/tc).toFixed(tc_dec)));
		el.find('.prod_series').attr('required',row[6]=='1');
		//create_code_div(row[0],row[5],row[1],true).replaceAll(this);
		
		actualizar_detalles();
		actualizar_totales();
};

$('.margen_p').change(function(){
	var cost = toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec);
	var margin = toNumber($(this).val());
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	margin = margin/100;
	var price = parseFloat(-cost/(margin-1)).toFixed(tc_dec);
	$(this).parent().parent().find('.prod_price').val(mil_format(price));
	actualizar_detalles();
	actualizar_totales();
});

actualizar_detalles = function(t){
	t = t?t:'#prod_list';
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val()).toFixed(2);
		if($(this).find('.prod_costo').size())
			cost = toNumber($(this).find('.prod_costo').val()).toFixed(2);
		else
			cost = 0.0;
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}
		
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(2)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(2)));
		$(this).find('.margen').text(mil_format(((price-cost)*cant).toFixed(tc_dec)));
		if(price > 0)
		$(this).find('.margen_p').val(((price-cost)*100/price).toFixed(1));
		else
		$(this).find('.margen_p').val('0');
		local = $(this).next();
		local.find('.l_price').text(mil_format((price*tc).toFixed(empresa_dec)));
		local.find('.l_total').text(mil_format((price*tc*cant).toFixed(empresa_dec)));
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
		local.find('.l_margen').text(mil_format(((price-cost)*tc).toFixed(empresa_dec)));
	});
};

$('#fv_contacto,#fv_cont_entrega').addClass('lst_contacto');

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
	actualizar_totales();
});

$('.get_description').click(function(){
	var desc = $(this).parent().next().find('.hidden_desc').val();
	$(this).parent().next().next().children('.prod_desc').val(desc);
});

$('.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('.prod_cant').change(function(){
	var mx = $(this).next('.max_value');
	if(mx.size()){
		if(parseFloat($(this).val()) > parseFloat(mx.val())){
			$(this).val(mx.val());
		}
	}
	actualizar_detalles();
	actualizar_totales();
});

$('.ventas_form').submit(function(e){
	e.preventDefault();
	if($('#prod_list_fake .main').size()){
		if($('#fake_total').text() != $('#total').text()){
			show_error('Si indica una glosa de facturación los valores totales deben cuadrar');
			$('#prod_list_fake').parent().addClass('invalid');
			return;
		}
	}
	if($('#prod_list tr').size() == 0){
		alert('No ha agregado productos a la factura');
		return;
	}
	var dets = $('#prod_list .prod_series[required]');
	dets.parent().parent().find('td').css({backgroundColor:'#FFFFBF'});
	var x = dets.size();
	for(var j=0; j<x; j++){
		var cant = $(dets[j]).parent().parent().find('.prod_cant').val();
		var val = $(dets[j]).val();
		if(val == ''){
			show_error('Debe ingresar todas las series de los productos que sean seriados');
			$(dets[j]).parent().parent().find('td').css({backgroundColor:'#FFFFBF'});
			return;
		}
		val = val.split(',').length;
		if(val < cant){
			show_error('Debe ingresar todas las series de los productos que sean seriados');
			$(dets[j]).parent().parent().find('td').css({backgroundColor:'#FFFFBF'});
			return;
		}
	}
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

actualizar_totales = function(t,v,a){
	
t = t?t:"#prod_list";
v = v?v:"#total";
a = a!=undefined?a:true;
	
var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
		cost = toNumber($(this).find('.prod_costo').val());
	
		total += price*cant;
		total_costo += cost*cant;
		total_margen += (price-cost)*cant;
		total_neto += price*tc*cant;
	});
	
	$(v).text(mil_format(total.toFixed(tc_dec)));
	$(v+'_costo').text(mil_format(total_costo.toFixed(tc_dec)));
	$(v+'_margen').text(mil_format(total_margen.toFixed(tc_dec)));
	
	if(total_costo > 0)
		$(v+'_margen_p').text(mil_format(((total-total_costo)/total_costo*100).toFixed(3)));
	else
		$('#total_margen_p').text('0');
	$(v+'_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$(v+'_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$(v+'_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));
	
	if(a){
		$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
		$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));
	}

};

check_codigos = function(){
	var tr = $("#prod_list tr.main").size();
	var code = $("#prod_list :hidden[name=prod[]]").size();
	return tr==code;
};

create_fake_prods = function(){
	var tab = $("#prods table").clone();
	tab.find('tbody').attr('id','prod_list_fake');
	tab.find('#total').attr('id','fake_total');
	tab.find('#total_costo').attr('id','fake_total_costo');
	tab.find('#total_margen').attr('id','fake_total');
	tab.find('#total_margen_p').attr('id','fake_total_margen_p');
	tab.find('#total_neto').attr('id','fake_total_neto');
	tab.find('#total_iva').attr('id','fake_total_iva');
	tab.find('#total_pagar').attr('id','fake_total_pagar');
	tab.appendTo('#fake_prods');
	
	var btn = $('<input>').multiAttr({
		type: 'button',
		class: 'addbtn',
		value: 'Agregar detalle'
	}).click(function(){
		get_fake_detail().appendTo('#prod_list_fake');
	});
	
	$("<div class='center'>").append(btn).appendTo('#fake_prods');
};

var get_fake_detail = function(){
	var val_change = function(){
		actualizar_detalles('#prod_list_fake');
		actualizar_totales('#prod_list_fake','#fake_total',false);
	};
	e = $('#prods_form tr').clone(true);
	e.find('.del_detail').click(function(){
		actualizar_totales('#prod_list_fake','#fake_total',false);
	});
	e.find('.prod_codigo').multiAttr({class:'prod_codigo', name:'fake_prod[]'});
	e.find('.prod_desc').attr('name','fake_desc[]');
	e.find('.prod_serie').attr('name','fake_serie[]');
	e.find('.prod_descuento').attr('name','fake_descuento[]');
	e.find('.prod_proveedor').attr('name','fake_proveedor[]');
	e.find('.prod_cant').attr('name','fake_cant[]').change(val_change);
	e.find('.prod_price').attr('name','fake_precio[]').change(val_change);
	e.find('.prod_costo').attr('name','fake_costo[]').change(val_change);
	e.find('.margen_p').change(val_change);
	
	return e;
};

get_fake_detail_cont = function(d){
	row = get_fake_detail();
	row.find('.prod_desc').val(d.dg_descripcion);
	row.find('.prod_proveedor').val(d.dc_proveedor);
	row.find('.prod_cant').val(d.dq_cantidad);
	row.find('.prod_price').val(parseFloat(d.dq_precio_venta).toFixed(tc_dec)); 
	row.find('.prod_costo').val(parseFloat(d.dq_precio_compra).toFixed(tc_dec));
	row.find('.prod_codigo').removeClass('searchbtn').addClass('inputtext').attr('readonly',true).val(d.dg_codigo);
	return row;
};



/*$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac);
});
*/

$('.set_series').click(function(){
	var tr = $(this).parent().parent();
	var series = tr.find('.prod_series').val().split(',');
	var cant = parseInt(tr.find('.prod_cant').val());
	if(!cant){
		show_error('Debe asignar cantidades en los detalles para poder asignar las series');
		return;
	}
	
	var div = $('<div>').addClass('overlay').addClass('center').css({minWidth:'250px',width:'250px',maxHeight:'500px',overflowY:'auto',overflowX:'hidden'})
	.append($('<div>').addClass('secc_bar')
	.text('Asignar series a los detalles')).appendTo('#content')
	.overlay({load: true, fixed:false, closeOnClick: false, oneInstance: false, mask: {color: '#ddd'}, close: 'no-close'});
	
	for(var i = 0; i<cant; i++){
		var txt = $('<input>').multiAttr({
			type: 'text',
			class: 'inputtext'
		}).width(200).keyup(function(e){
			if(e.keyCode == 13){
				var nxt = $(this).next();
				if(nxt.attr('type') == 'button'){
					nxt.trigger('click');
				}else{
					nxt.trigger('focus').select();
				}
			}
		}).appendTo(div);
		
		if(series[i]){
			txt.val(series[i]);
		}
	}
	
	var send = $('<button type="button">').addClass('addbtn').text('Asignar Series').click(function(){
		var arrser = [];
		var size = $('.overlay :text').each(function(){
			var val = $(this).val();
			if(val != '')
				arrser.push(val);
		}).size();
		
		tr.find('.prod_series').val(arrser.join(','));
		div.overlay().close();
		div.remove();
	}).appendTo(div);
});

var set_fact_anticipada = function(){
	$('#facturar_anticipado_completa').click(function(){
		var hidden = $('<input type="hidden">').attr('name','anticipada').val(1).appendTo('#cr_factura_venta');
		$('#cr_factura_venta').prev('input').remove();
		$('#cr_factura_venta').submit();
		hidden.remove();
	});
	
	$('#facturar_anticipado_parcial').click(function(){
		var hidden = $('<input type="hidden">').attr('name','anticipada').val(2).appendTo('#cr_factura_venta');
		$('#cr_factura_venta').prev('input').remove();
		$('#cr_factura_venta').submit();
		hidden.remove();
	});
};

var copy_to_glosa = function(){
	$('#prod_list .main').each(function(){
		get_fake_detail_cont({
			dg_descripcion: $(this).find('.prod_desc').val(),
			dc_proveedor: 0,
			dq_cantidad: $(this).find('.prod_cant').val(),
			dq_precio_venta: toNumber($(this).find('.prod_price').val()),
			dq_precio_compra: toNumber($(this).find('.prod_costo').val()),
			dg_codigo: $(this).find('.prod_codigo').val()
		}).appendTo('#prod_list_fake');
	});
	
	actualizar_detalles('#prod_list_fake');
	actualizar_totales('#prod_list_fake','#fake_total',false);
};
$('#load_fake_details_from_detail').click(copy_to_glosa);
