$("#res_list").slideDown();
$("table.sortable").tablesorter();

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
});

$('#show_hide_list').click(function(){
	$('#res_list').toggle();
});

$(".pr_load").click(function(e){
	e.preventDefault();
	$('#show_oportunidad').html("<img src='images/ajax-loader.gif' alt='' /> cargando proyecto ...");
	$("#res_list td").removeClass('confirm');
	$(this).parent().addClass('confirm');
	$('.panes').width(900);
	loadFile($(this).attr('href'),'#show_proyecto','',globalFunction);
}).first().trigger('click');

var cr_name_div = function(name){
var div = $('<div>').css({
	background:'#EEE',
	color:'#333',
	padding:'3px',
	border:'1px solid #CCC',
	margin:'3px'
}).mouseenter(function(){$(this).children('img').show();})
.mouseleave(function(){$(this).children('img').hide();})
.text(name);

return div;
};
	
var add_cost_material = function(target){
	$("#template_detail tr.templ_material").clone(true).appendTo(target).find('.src_material')
	.width('85%')
	.autocomplete('sites/proc/autocompleter/produccion_material.php',{
		formatItem: mat_format,
		minChars: 2,
		width:300
	}).result(mat_result);
};

var mat_format = function(row){
	return "<b>"+row[0]+"</b> "+row[1]+" "+row[2];
};

var mat_result = function(e,row){
	$(this).next(':hidden').val(row[3]);
	$(this).parent().next().text(unidades_medida[row[3]]);
	if(e.precio){
		$(this).parent().next().next().next().children('input').val(e.precio).trigger('change');
	}
	var div = cr_name_div(row[0]).replaceAll(this).prepend(create_edit_btn('material',mat_format,mat_result));
};

var add_cost_service = function(target){
	$("#template_detail tr.templ_service").clone(true).appendTo(target).find('.src_service')
	.width('85%')
	.autocomplete('sites/proc/autocompleter/produccion_servicio.php',{
		formatItem: srv_format,
		minChars: 2,
		width:300
	}).result(srv_result);
};

var srv_format = function(row){
	return "<b>"+row[0]+"</b> "+row[1];
};

var srv_result = function(e,row){
	$(this).next(':hidden').val(row[2]);
	$(this).parent().next().next().next().children('input').val(row[3]).trigger('change');
	var div = cr_name_div(row[0]).replaceAll(this).append(create_edit_btn('servicio',srv_format,srv_result));
};

var create_edit_btn = function(type,format,res){
return $('<img>').multiAttr({
	src:'images/editbtn.png',
	class:'right hidden',
	width: '13',
	height: '13',
	title:'Modificar'
}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
.mouseout(function(){$(this).css({background:'',border:''})})
.click(function(){
	$(this).parent().next(':hidden').val(0);
	$('<input>').addClass('searchbtn').width('85%').replaceAll($(this).parent())
	.autocomplete('sites/proc/autocompleter/produccion_'+type+'.php',{
		formatItem: format,
		minChars:2,
		width:300
	}).result(res).focus();
});
};

var add_producto = function(proy){
	loadOverlay("sites/produccion/cr_producto.php?id="+proy);
};

var ver_detalle_costeo = function(id){
	loadOverlay("sites/produccion/proc/show_costeo.php?id="+id);
};

var ver_cotizacion = function(n){
	if(confirm('¿Incluir las imagenes asociadas al proyecto?'))
	window.open("sites/produccion/ver_cotizacion.php?img=1&id="+n,'print_cotizacion','width=800;height=600');
	else
	window.open("sites/produccion/ver_cotizacion.php?id="+n,'print_cotizacion','width=800;height=600');
};

var templ_costeo = function(costeo){
	if(confirm('Se duplicará el costeo seleccionado y se asignará como el costeo actual')){
		loadOverlay("sites/produccion/proc/set_duplicate_costeo.php?c="+costeo+"&p="+id_proyecto);
	}
};

$('input[name=cost_cantidad[]],input[name=cost_cantidad2[]]').change(function(){
	price = $(this).parent().next().find('input').val();
	cant = $(this).val();
	$(this).parent().next().next().text(mil_format(cant*price));
});

$('input[name=cost_precio[]],input[name=cost_precio2[]]').change(function(){
	cant = $(this).parent().prev().find('input').val();
	price = $(this).val();
	$(this).parent().next().text(mil_format(cant*price));
});

var init_buttons = function(){
	
};