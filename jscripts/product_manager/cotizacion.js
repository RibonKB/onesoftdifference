var tc;
var tc_dec;

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list');
	init_autocomplete();
});

init_autocomplete = function(){
	$(".prod_codigo").autocomplete('sites/proc/autocompleter/producto.php',{
		formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
		minChars: 2,
		width:300
	}).result(function(e,row){
		var el = $(this).parent().parent();
		var cant = el.find('.prod_cant');
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}
		
		el.find('.prod_desc').val(row[1]);
		el.find('.prod_price').val(mil_format(parseFloat(row[3]/tc).toFixed(tc_dec)));
		el.find('.prod_costo').val(mil_format(parseFloat(row[4]/tc).toFixed(tc_dec)));
		actualizar_detalles();
		actualizar_totales();
	});
};

$('.prod_codigo').change(function(){
	var code = $(this).val();
	var count = 0;
	$('#prod_list .prod_codigo').each(function(){
		if($(this).val() == code)
			count++;
	});
	if(count > 1){
		alert("ya ha ingresado un producto con ese código, intente con un nuevo código o modifique el existente");
		$(this).val('');
		var el = $(this).parent().parent();
		el.find('.prod_cant').val('');
		el.find('.prod_desc').val('');
		el.find('.prod_price').val('');
		el.find('.prod_costo').val('');
		actualizar_detalles();
		actualizar_totales();
	}
});

$("#prod_tipo_cambio").change(function(){
	var indice = this.selectedIndex;
	if(indice == 0){
		$("#prod_info").show();
		$("#prods").hide();
	}else{
		$("#prod_info").hide();
		$("#prods").show();
		tc = this.options[indice].innerHTML.split("|");
		tc = toNumber(tc[1]);
		$('#cot_cambio').val(tc);
		tc_dec = $(":hidden[name=decimal"+$(this).val()+"]").val();
		actualizar_detalles();
		actualizar_totales();
	}
	
});

$('.margen_p').change(function(){
	var cost = parseFloat(toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec));
	var margin = toNumber($(this).val())/100;
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	var price = -cost/(margin - 1);
	$(this).parent().parent().find('.prod_price').val(mil_format(price));
	actualizar_detalles();
	actualizar_totales();
});

actualizar_detalles = function(){
	$("#prod_list tr.main").each(function(){
		var cant = parseFloat($(this).find('.prod_cant').val());
		var price = toNumber($(this).find('.prod_price').val()).toFixed(tc_dec);
		var cost = toNumber($(this).find('.prod_costo').val()).toFixed(tc_dec);
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}
		
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(tc_dec)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(tc_dec)));
		$(this).find('.margen').text(mil_format(((price-cost)*cant).toFixed(tc_dec)));
		if(price > 0)
		$(this).find('.margen_p').val(((price-cost)*100/price).toFixed(1));
		else
		$(this).find('.margen_p').val('-');
		var local = $(this).next();
		local.find('.l_price').text(mil_format((price*tc).toFixed(empresa_dec)));
		local.find('.l_total').text(mil_format((price*tc*cant).toFixed(empresa_dec)));
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
		local.find('.l_margen').text(mil_format(((price-cost)*tc*cant).toFixed(empresa_dec)));
	});
};

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
        actualizar_detalles();
	actualizar_totales();
});

$('.prod_cant,.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('#cot_cambio').change(function(){
	var cot_cambio = $(this).val();
	if(!parseFloat(cot_cambio)){
		cot_cambio = 1.0;
		$(this).val(1);
	}
	tc = cot_cambio;
	actualizar_detalles();
	actualizar_totales();
});

$('.ventas_form').submit(function(e){
	e.preventDefault();
	if($('#prod_list tr').size() == 0){
		alert('No ha agregado productos a la cotizaci\xf3n');
		return;
	}
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

actualizar_totales = function(){
	
var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$("#prod_list tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val());
		cost = toNumber($(this).find('.prod_costo').val());
	
		total += price*cant;
		total_costo += cost*cant;
		total_margen += (price-cost)*cant;
		total_neto += price*tc*cant;
	});
	
	$('#total').text(mil_format(total.toFixed(tc_dec)));
	$('#total_costo').text(mil_format(total_costo.toFixed(tc_dec)));
	$('#total_margen').text(mil_format(total_margen.toFixed(tc_dec)));
	
	if(total > 0)
		$('#total_margen_p').text(mil_format(((total-total_costo)/total*100).toFixed(3)));
	else
		$('#total_margen_p').text('-');
	$('#total_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$('#total_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$('#total_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));
	
	$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
	$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));

};