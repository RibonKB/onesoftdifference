$('#template_detail .del_detail').click(function(){
	$(this).parent().parent().remove();
});

$('#template_detail .src_material').change(function(){
	$(this).val('');
});

$('.add_material').click(function(){
	var tr = $('#template_detail .templ_material').clone(true);
	tr.find('.src_material').autocomplete('sites/proc/autocompleter/produccion_material.php',{
		formatItem: mat_format,
		minChars: 2,
		width:300
	}).result(mat_result);
	var id = $(this).attr('id').substr(4);
	tr.find(':hidden[name=cub_prod[]]').val(id);
	tr.appendTo($(this).prev('table').children('.prod_list'));
});

var mat_format = function(row){
	return "<b>"+row[0]+"</b> "+row[1]+" "+row[2];
};

var mat_result = function(e,row){
	$(this).next(':hidden').val(row[3]);
	$(this).parent().next().text(unidades_medida[row[3]]);
	var div = cr_name_div(row[0]).replaceAll(this).prepend(create_edit_btn('material',mat_format,mat_result));
};

var create_edit_btn = function(type,format,res){
return $('<img>').multiAttr({
	src:'images/editbtn.png',
	class:'right hidden',
	width: '13',
	height: '13',
	title:'Modificar'
}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
.mouseout(function(){$(this).css({background:'',border:''})})
.click(function(){
	$(this).parent().next(':hidden').val(0);
	$('#template_detail .src_material').clone(true).width('85%').replaceAll($(this).parent())
	.autocomplete('sites/proc/autocompleter/produccion_'+type+'.php',{
		formatItem: format,
		minChars:2,
		width:300
	}).result(res).focus();
});
};

var cr_name_div = function(name){
var div = $('<div>').css({
	background:'#EEE',
	color:'#333',
	padding:'3px',
	border:'1px solid #CCC',
	margin:'3px'
}).mouseenter(function(){$(this).children('img').show();})
.mouseleave(function(){$(this).children('img').hide();})
.text(name);

return div;
};