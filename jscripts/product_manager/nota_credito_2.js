var tc = 1;
var tc_dec = empresa_dec;

$('#nc_factura_venta').autocomplete('sites/proc/autocompleter/factura_venta_strict.php',{
	formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
	minChars: 2,
	width:300
}).result(function(){
	$(this).change();
});

change_factura_venta = function(){
	if(!parseInt($(this).val())) return;
	
	var v = get_disabled(this);
	
	$.getJSON('sites/ventas/nota_credito/proc/get_factura_venta_detalle.php',{ number: encodeURIComponent(v.val()) },function(data){
		if(data == '<not-found>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('No se ha encontrado la factura de venta especificada, intentelo denuevo');
			return;
		}
		
		if(data[0] == '<empty>'){
			v.attr('disabled',false).css({backgroundImage: ''}).val('');
			show_error('El detalle de la factura de venta es inválido, no se puede continuar');
			return;
		}
		
		var det = data[0];
		var dc_factura = data[1].dc_factura;
		var dc_nota_venta = data[1].dc_nota_venta;
		var dc_orden_servicio = data[1].dc_orden_servicio;
		var dq_neto = data[1].dq_neto;
		
		for(i in det){
			
			if(det[i].dq_cantidad > 0){
				var q = det[i].dq_cantidad;
				var tipo = det[i].dm_tipo;
				var detalle = det[i];
				if(tipo == 0){
					d = get_valid_detail(det[i]);
					d.addClass('strict');
					var maxv = $('<input>').multiAttr({type:'hidden',class:'max_value'}).val(q);
					d.find('.prod_cant').after(maxv);
					d.appendTo('#prod_list');
				}else if(tipo == 1){
					get_fake_detail_cont(detalle).appendTo('#prod_list_fake').find('.prod_cant').trigger('change');
				}
			}
		}
		
		actualizar_detalles();
		actualizar_totales();
		
		create_number_div(v.val(),'factura_venta',change_factura_venta,function(){
			$('#prod_list .strict').remove();
		}).width(v.width()).replaceAll(v);
		$(':hidden[name=nc_id_factura_venta]').val(dc_factura);
		$(':hidden[name=fv_id_nota_venta]').val(dc_nota_venta);
		$(':hidden[name=fv_id_orden_servicio]').val(dc_orden_servicio);
		$(':hidden[name=nc_max_neto]').val(dq_neto);
		
	});
};

$('#nc_factura_venta').change(change_factura_venta);

get_disabled = function(e){
	return $(e).blur().attr('disabled',true).css({
		backgroundImage: 'url(images/ajax-loader.gif)',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center right'
	});
};

get_valid_detail = function(d){
	row = $('#prods_form tr').clone(true);
	row.find('.prod_desc').val(d.dg_descripcion);
	row.find('.prod_proveedor').val(d.dc_proveedor);
	row.find('.prod_cant').val(d.dq_cantidad);
	row.find('.prod_price').val(parseFloat(d.dq_precio_venta).toFixed(tc_dec)); 
	row.find('.prod_costo').val(parseFloat(d.dq_precio_compra).toFixed(tc_dec));
	row.find('.prod_codigo').removeClass('searchbtn').addClass('inputtext').attr('readonly',true).val(d.dg_codigo);
	//create_code_div(d.dg_codigo,d.dc_producto,d.dg_descripcion).replaceAll(row.find('.prod_codigo'));
	return row;
};

create_code_div = function(code,id,desc,ed){
					
	var div = $('<div>').css({
		border:'1px solid #CCC',
		background:'#EEE',
		padding:'5px',
		fontWeight:'bold'
	}).addClass('code-div')
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.text(code);
	
	if(ed){
		div.prepend(create_edit_code_btn());
	}
					
	$('<input>').multiAttr({
		type:'hidden',
		name:'prod[]'
	}).val(id).appendTo(div);
					
	$('<input>').multiAttr({
		type:'hidden',
		class:'hidden_desc'
	}).val(desc).appendTo(div);
	
	return div;
};

create_edit_code_btn = function(){
	return $('<img>').multiAttr({
		class: 'right hidden',
		src: 'images/editbtn.png',
		alt: '[|]',
		title: 'Editar código',
		width: '13',
		height: '13'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'searchbtn prod_codigo',
			type: 'text',
			size: '7'
		}).autocomplete('sites/proc/autocompleter/producto.php',{
			formatItem: function(row){return row[0]+" ( "+row[1]+" ) "+row[2];},minChars: 2,width:300})
		.result(result_ac)
		.replaceAll(innerdiv);
	});
};

create_number_div = function(number,acl,chfn,cb){
	var edbtn = $('<img>').multiAttr({
		src: 'images/editbtn.png',
		title: 'Modificar',
		alt: '[|]',
		class:'right hidden'
	}).mouseover(function(){$(this).css({background:'#DDD',border:'1px solid #CCC'});})
	.mouseout(function(){$(this).css({background:'',border:''})})
	.click(function(){
		var innerdiv = $(this).attr('src','images/ajax-loader.gif').parent();
		var ac_field = $('<input>').multiAttr({
			class: 'inputtext',
			type: 'text',
			id: 'nc_'+acl,
			size: '41',
			maxlength: '255'
		}).autocomplete('sites/proc/autocompleter/'+acl+'_strict.php',{
			formatItem: function(row){ return row[0]+" ( "+row[1]+" ) "+row[2]; },
			minChars: 2,
			width:300
		}).result(function(){
			$(this).change();
		}).change(chfn).replaceAll(innerdiv);
		
		$(':hidden[name=nc_id_'+acl+']').val(0);
		cb();
	});
	
	var div = $('<div>').addClass('inputtext').css({
		background:'#EEE',
		fontWeight:'bold'
	}).text(number)
	.mouseenter(function(){$(this).children('img').show();})
	.mouseleave(function(){$(this).children('img').hide();})
	.prepend(edbtn);
	
	return div;
};

$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac).trigger('focus');
});

result_ac = function(e,row){
		
		el = $(this).parent().parent();
		cant = el.find('.prod_cant');
		if(!parseFloat(cant.val())){
			cant.val(1);
			cant = 1;
		}else{
			cant = cant.val();
		}
		
		el.find('.prod_desc').val(row[1]);
		el.find('.prod_price').val(mil_format(parseFloat(row[3]/tc).toFixed(tc_dec)));
		el.find('.prod_costo').val(mil_format(parseFloat(row[4]/tc).toFixed(tc_dec)));
		el.find('.prod_series').attr('required',row[6]=='1');
		//create_code_div(row[0],row[5],row[1],true).replaceAll(this);
		
		actualizar_detalles();
		actualizar_totales();
};

$('.margen_p').change(function(){
	cost = toNumber($(this).parent().parent().find('.prod_costo').val()).toFixed(tc_dec);
	margin = toNumber($(this).val());
	if(margin == 'NaN'){margin=0.0;$(this).val(0);}
	price = $(this).parent().parent().find('.prod_price').val(mil_format(parseFloat(cost)+(cost*margin/100)));
	actualizar_detalles();
	actualizar_totales();
});

actualizar_detalles = function(t){
	t = t?t:'#prod_list';
	$(t+" tr.main").each(function(){
		cant = parseFloat($(this).find('.prod_cant').val());
		price = toNumber($(this).find('.prod_price').val()).toFixed(tc_dec);
		if($(this).find('.prod_costo').size())
			cost = toNumber($(this).find('.prod_costo').val()).toFixed(tc_dec);
		else
			cost = 0.0;
		if(!cant){cant=1; $(this).find('.prod_cant').val(1);}
		if(price == 'NaN'){price=0.0.toFixed(tc_dec);$(this).find('.prod_price').val(0);}
		if(cost == 'NaN'){cost=0.0.toFixed(tc_dec);$(this).find('.prod_costo').val(0);}
		
		$(this).find('.prod_price').val(mil_format(price));
		$(this).find('.total').text(mil_format((price*cant).toFixed(tc_dec)));
		$(this).find('.prod_costo').val(mil_format(cost));
		$(this).find('.costo_total').text(mil_format((cost*cant).toFixed(tc_dec)));
		$(this).find('.margen').text(mil_format((price-cost).toFixed(tc_dec)));
		if(cost > 0)
		$(this).find('.margen_p').val(((price-cost)*100/cost).toFixed(1));
		else
		$(this).find('.margen_p').val('0');
		local = $(this).next();
		local.find('.l_price').text(mil_format((price*tc).toFixed(empresa_dec)));
		local.find('.l_total').text(mil_format((price*tc*cant).toFixed(empresa_dec)));
		local.find('.l_costo').text(mil_format((cost*tc).toFixed(empresa_dec)));
		local.find('.l_costo_total').text(mil_format((cost*tc*cant).toFixed(empresa_dec)));
		local.find('.l_margen').text(mil_format(((price-cost)*tc).toFixed(empresa_dec)));
	});
};

$('#fv_contacto,#fv_cont_entrega').addClass('lst_contacto');

$('.more_details').toggle(function(){
	$(this).parent().parent().next().show();
	$(this).attr('src','images/minusbtn.png');
},function(){
	$(this).parent().parent().next().hide();
	$(this).attr('src','images/addbtn.png');
});

$('.del_detail').click(function(){
	$(this).parent().parent().remove();
	actualizar_totales();
});

$('.get_description').click(function(){
	var desc = $(this).parent().next().find('.hidden_desc').val();
	$(this).parent().next().next().children('.prod_desc').val(desc);
});

$('.prod_price,.prod_costo').change(function(){
	actualizar_detalles();
	actualizar_totales();
});

$('.prod_cant').change(function(){
	var mx = $(this).next('.max_value');
	if(mx.size()){
		if(parseFloat($(this).val()) > parseFloat(mx.val())){
			$(this).val(mx.val());
		}
	}
	actualizar_detalles();
	actualizar_totales();
});

$('.ventas_form').submit(function(e){
	e.preventDefault();
	
	var neto = parseFloat($(':hidden[name=cot_neto]').val());
	var neto_max = parseFloat($(':hidden[name=nc_max_neto]').val());
	
	/*if(neto > neto_max){
		show_error('El precio neto no puede superar el valor del neto de la factura asignada');
		return;
	}*/
	
	if($('#prod_list_fake .main').size()){
		if($('#fake_total').text() != $('#total').text()){
			show_error('Si indica una glosa de facturación los valores totales deben cuadrar');
			$('#prod_list_fake').parent().addClass('invalid');
			return;
		}
	}
	if($('#prod_list tr').size() == 0){
		alert('No ha agregado productos a la nota de credito');
		return;
	}
	var dets = $('#prod_list .prod_series[required]');
	dets.parent().parent().find('td').css({backgroundColor:'#FFFFBF'});
	var x = dets.size();
	for(var j=0; j<x; j++){
		var cant = $(dets[j]).parent().parent().find('.prod_cant').val();
		var val = $(dets[j]).val();
		if(val == ''){
			show_error('Debe ingresar todas las series de los productos que sean seriados');
			$(dets[j]).parent().parent().find('td').css({backgroundColor:'#FFFFBF'});
			return;
		}
		val = val.split(',').length;
		if(val < cant){
			show_error('Debe ingresar todas las series de los productos que sean seriados');
			$(dets[j]).parent().parent().find('td').css({backgroundColor:'#FFFFBF'});
			return;
		}
	}
	i = '#'+$(this).attr('id');
	confirmEnviarForm(i,i+'_res');
});

actualizar_totales = function(t,v,a){
	
t = t?t:"#prod_list";
v = v?v:"#total";
a = a!=undefined?a:true;
	
var total = 0;
var total_costo = 0;
var total_margen = 0;
var total_neto = 0;
	$(t+" tr.main").each(function(){
		var cant = parseFloat($(this).find('.prod_cant').val());
		
		var price = $(this).find('.prod_price').val();
		if(price)
			price = toNumber(price);
		else
			price = 0;
		
		var cost = $(this).find('.prod_costo').val();
		if(cost)
			cost = toNumber(cost);
		else
			cost = 0;
	
		total += price*cant;
		total_costo += cost*cant;
		total_margen += price-cost;
		total_neto += price*tc*cant;
	});
	
	$(v).text(mil_format(total.toFixed(tc_dec)));
	$(v+'_costo').text(mil_format(total_costo.toFixed(tc_dec)));
	$(v+'_margen').text(mil_format(total_margen.toFixed(tc_dec)));
	
	if(total_costo > 0)
		$(v+'_margen_p').text(mil_format(((total-total_costo)/total_costo*100).toFixed(3)));
	else
		$('#total_margen_p').text('0');
	$(v+'_neto').text(mil_format(total_neto.toFixed(empresa_dec)));
	$(v+'_iva').text(mil_format((total_neto*empresa_iva/100).toFixed(empresa_dec)));
	$(v+'_pagar').text(mil_format(((total_neto*empresa_iva/100)+total_neto).toFixed(empresa_dec)));
	
	if(a){
		$('input[name=cot_iva]').val((total_neto*empresa_iva/100).toFixed(empresa_dec));
		$('input[name=cot_neto]').val(total_neto.toFixed(empresa_dec));
	}

};

check_codigos = function(){
	var tr = $("#prod_list tr.main").size();
	var code = $("#prod_list :hidden[name=prod[]]").size();
	return tr==code;
};

create_fake_prods = function(){
	var tab = $("#prods table").clone();
	tab.find('tbody').attr('id','prod_list_fake');
	tab.find('#total').attr('id','fake_total');
	tab.find('#total_costo').attr('id','fake_total_costo');
	tab.find('#total_margen').attr('id','fake_total');
	tab.find('#total_margen_p').attr('id','fake_total_margen_p');
	tab.find('#total_neto').attr('id','fake_total_neto');
	tab.find('#total_iva').attr('id','fake_total_iva');
	tab.find('#total_pagar').attr('id','fake_total_pagar');
	tab.appendTo('#fake_prods');
	
	var btn = $('<input>').multiAttr({
		type: 'button',
		class: 'addbtn',
		value: 'Agregar detalle'
	}).click(function(){
		get_fake_detail().appendTo('#prod_list_fake');
	});
	
	$("<div class='center'>").append(btn).appendTo('#fake_prods');
};

var get_fake_detail = function(){
	var val_change = function(){
		actualizar_detalles('#prod_list_fake');
		actualizar_totales('#prod_list_fake','#fake_total',false);
	};
	e = $('#prods_form tr').clone(true);
	e.find('.del_detail').click(function(){
		actualizar_totales('#prod_list_fake','#fake_total',false);
	});
	e.find('.prod_codigo').multiAttr({class:'prod_codigo', name:'fake_prod[]'});
	e.find('.prod_desc').attr('name','fake_desc[]');
	e.find('.prod_serie').attr('name','fake_serie[]');
	e.find('.prod_descuento').attr('name','fake_descuento[]');
	e.find('.prod_proveedor').attr('name','fake_proveedor[]');
	e.find('.prod_cant').attr('name','fake_cant[]').change(val_change);
	e.find('.prod_price').attr('name','fake_precio[]').change(val_change);
	e.find('.prod_costo').attr('name','fake_costo[]').change(val_change);
	e.find('.margen_p').change(val_change);
	
	return e;
};

get_fake_detail_cont = function(d){
	row = get_fake_detail();
	row.find('.prod_desc').val(d.dg_descripcion);
	row.find('.prod_proveedor').val(d.dc_proveedor);
	row.find('.prod_cant').val(d.dq_cantidad);
	row.find('.prod_price').val(parseFloat(d.dq_precio_venta).toFixed(tc_dec)); 
	row.find('.prod_costo').val(parseFloat(d.dq_precio_compra).toFixed(tc_dec));
	row.find('.prod_codigo').removeClass('searchbtn').addClass('inputtext').attr('readonly',true).val(d.dg_codigo);
	return row;
};



/*$('#prod_add').click(function(){
	$('#prods_form tr').clone(true).appendTo('#prod_list').find('.prod_codigo').autocomplete('sites/proc/autocompleter/producto.php',
	{
	formatItem: function(row){
		return row[0]+" ( "+row[1]+" ) "+row[2];
	},
	minChars: 2,
	width:300
	}
	).result(result_ac);
});
*/

$('.set_series').click(function(){
	var tr = $(this).parent().parent();
	var series = tr.find('.prod_series').val().split(',');
	var cant = parseInt(tr.find('.prod_cant').val());
	if(!cant){
		show_error('Debe asignar cantidades en los detalles para poder asignar las series');
		return;
	}
	
	var div = $('<div>').addClass('overlay').addClass('center').css({minWidth:'250px',width:'250px',maxHeight:'500px',overflowY:'auto',overflowX:'hidden'})
	.append($('<div>').addClass('secc_bar')
	.text('Asignar series a los detalles')).appendTo('#content')
	.overlay({load: true, fixed:false, closeOnClick: false, oneInstance: false, mask: {color: '#ddd'}, close: 'no-close'});
	
	for(var i = 0; i<cant; i++){
		var txt = $('<input>').multiAttr({
			type: 'text',
			class: 'inputtext'
		}).width(200).keyup(function(e){
			if(e.keyCode == 13){
				var nxt = $(this).next();
				if(nxt.attr('type') == 'button'){
					nxt.trigger('click');
				}else{
					nxt.trigger('focus').select();
				}
			}
		}).appendTo(div);
		
		if(series[i]){
			txt.val(series[i]);
		}
	}
	
	var send = $('<button type="button">').addClass('addbtn').text('Asignar Series').click(function(){
		var arrser = [];
		var size = $('.overlay :text').each(function(){
			var val = $(this).val();
			if(val != '')
				arrser.push(val);
		}).size();
		
		tr.find('.prod_series').val(arrser.join(','));
		div.overlay().close();
		div.remove();
	}).appendTo(div);
});

var set_fact_anticipada = function(){
	$('#facturar_anticipado_completa').click(function(){
		var hidden = $('<input type="hidden">').attr('name','anticipada').val(1).appendTo('#cr_factura_venta');
		$('#cr_factura_venta').prev('input').remove();
		$('#cr_factura_venta').submit();
		hidden.remove();
	});
	
	$('#facturar_anticipado_parcial').click(function(){
		var hidden = $('<input type="hidden">').attr('name','anticipada').val(2).appendTo('#cr_factura_venta');
		$('#cr_factura_venta').prev('input').remove();
		$('#cr_factura_venta').submit();
		hidden.remove();
	});
};