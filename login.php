<?php
	if(isset($_COOKIE['log_empresa_rut']))
		$rut_empresa = $_COOKIE['log_empresa_rut'];
	else
		$rut_empresa = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Onesoft - PYMEERP</title>
<link type="text/css" href="styles/main.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="styles/login.css">
<script type="text/javascript" src="jscripts/lib/jquery-1.7.1.min.js" ></script>
<script type="text/javascript" src="jscripts/lib/jquery.tools.min.js" ></script>
<script type="text/javascript" src="jscripts/md5-min.js"></script>
<script type="text/javascript" src="jscripts/lib/jquery.multiattr.js"></script>
<script type="text/javascript" src="jscripts/login.js?v=3"></script>
</head>

<body>

<div id="header">
<div class="left">
<table>
	<tr>
		<td><img src="images/logo.png" alt="PYMEERP" id="system_logo" /></td>
		<td><div class="country"></div></td>
	</tr>
</table>
</div>

<br class="clear" />
</div>

<div class="secc_bar">
	Inicio de sesión en el sistema.
</div>
<br />
<br />
<br />
<br />

<div id="login_form">
	<form action="sites/login.php" method="post" id="log_form">
	<div id="log_results">
	<?php if(isset($_GET['action'])) echo("<div class='info'>Se ha desconectado del sistema</div>"); ?>
	</div>
	<fieldset>
	<legend>Indique los datos de inicio de sesión</legend>
	<br />
	<table width="80%" cellpadding="0" cellspacing="0" align="center">
		<tbody valign="middle" align="right">
			<tr>
				<td valign="middle"><label>RUT Empresa:</label></td>
				<td><input type="text" class="inputtext" id="log_rut" name="log_rut" value="<?=$rut_empresa ?>" /></td>
			</tr>
			<tr>
				<td><br /><label>Usuario:</label></td>
				<td><br /><input type="text" class="inputtext" id="log_username" name="log_username" /><br /></td>
			</tr>
			<tr>
				<td><label>Contraseña:</label></td>
				<td><input type="password" class="inputtext" id="log_password" name="log_password" /><br /></td>
			</tr>
		</tbody>
	</table>
	<br />
	<div align="center">
		<input type="submit" class="button" value="Entrar" />
	</div>
	<br />
	</fieldset>
	</form>
	
</div>

</body>
</html>
