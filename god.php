<?php
define('MAIN',1);
require_once('inc/Factory.class.php');

$r = Factory::getRequest();

$modulo = $r->modulo;
$factory = $r->factory;

if(isset($r->action))
	$action = $r->action;
else
	$action = 'index';

  

Factory::validaUrlParam($modulo);
Factory::validaUrlParam($factory);
Factory::validaUrlParam($action);

Factory::$factoryName = $factory;
Factory::$action = $action;
Factory::$modulo = $modulo;

$factory_url = __DIR__.'/sites/'.$modulo;

if(isset($r->submodulo)){
	$submodulo = $r->submodulo;
	
	Factory::validaUrlParam($submodulo);
	
	Factory::$submodulo = $submodulo;
	
	$factory_url .= '/'.$submodulo;
}

$factory = Factory::getFactoryInstance($factory_url,$factory);

Factory::callAction($action,$factory);