#Pedro Escobar, y que le quede claro
sites/contabilidad/informe_existencias/*;
templates/contabilidad/informe_existencias/TipoOperacionFechas/*;
jscripts/sites/contabilidad/informe_existencias/*;

sites/ventas/guia_recepcion/CrearGuiaRecepcionFactory.php

*   Preguntar a sol por arriendos
*   Crear en tb_movimiento_bodega el campo dc_
*   Crear devolucion_guia_despacho_proveedor y validar el tipo de devolucion.
*   cambiar los movimientos de la empresa 5


//agregados
Se agrega el campo dc_devolucion_guia_despacho (ver sql)


tipos de operacion:


    1           La fecha se obtiene desde la misma fecha de creaci�n del movimiento
                    14, 21, 38, 45, 48
    2           La fecha se obtiene de la devoluci�n de gu�as de despacho.
                    13, 20
    2           La fecha se obtiene de la devoluci�n de gu�as de despacho Proveedor.
                    p - Se debe crear los tipos de movimiento para devolucion de guia de despacho de proveedor para todas las empresas
    3           La fecha se obtiene de la emisi�n de una gu�a de despacho a proveedor
                    44
    4           La fecha se obtiene de la emisi�n del ajuste de cantidades
                    27, 28
                    p - Crear nuevos tipos de movimiento para entrada y salida en ADIS. en remplazo del 8 y 9

    5           La fecha se obtendr� de la emisi�n de la gu�a de despacho a cliente
                    1, 3, 4, 5, 8, 15, 16, 17, 18, 19, 22, 32, 33, 49 ,
                    p - Crear Tipo Movimiento Anulaci�n gu�a despacho adis e iscomp
    6           Se utilizar� la fecha de emisi�n de la factura de compra
                    2, 6, 16, 17, 22, 34, 43, Crear tipo de movimiento Devolucion por anulaci�n de Factura de compra
    7           Se utlizar� la fecha de emisi�n de la nota de credito
                    30, 31, 46, 47
    default     Se solicitar� proporcionar la fecha manualmente
                    3, 19 o cualquier otro
                    En el caso del tipo de movimiento 3: Si est� asociada a una gu�a de despacho, se utilizar� esa fecha.


*   Se deber� crear un tipo de movimiento nuevo para las siguientes operaciones:

        -   Anulacion gu�a de despacho (adis e iscomp)
        -   Devoluci�n gu�a de despacho proveedor (adis e iscomp)
        -   Anular factura de venta (iscomp, GT)
        -   Anular gu�a Recepcion (Iscomp)
        -   Crear tipo de movimiento Ajuste de entrada (adis)
        -   Crear tipo de movimiento Ajuste de salida (adis)
        -


*   En el caso del traspaso simple:
        -   Si posee una guia de despacho y esta es traslado por arriendo, se debe utilizar la
            fecha de emisi�n de esta gu�a de despacho.


-------- CREAR TABLAS O CAMPOS --------

ALTER TABLE `tb_comprobante_ajuste_logistico`
  ADD `df_fecha_creacion` datetime NOT NULL;


UPDATE TABLE `tb_comprobante_ajuste_logistico`
SET `df_creacion` = `df_emision`
WHERE true;


ALTER TABLE `tb_movimiento_bodega`
ADD `dc_comprobante_ajuste_logistico` INT UNSIGNED NULL DEFAULT NULL ,
ADD INDEX (`dc_comprobante_ajuste_logistico`) ;


ALTER TABLE `tb_movimiento_bodega`
ADD CONSTRAINT `FK_MOVIMIENTO_AJUSTE_LOGISTICO`
FOREIGN KEY (`dc_comprobante_ajuste_logistico`)
REFERENCES `pymerp`.`tb_comprobante_ajuste_logistico`(`dc_comprobante`)
ON DELETE RESTRICT ON UPDATE RESTRICT;


ALTER TABLE `tb_tipo_movimiento_logistico`
ADD `tb_tipo_movimiento_logistico`.`dc_tipo_operacion` INT NOT NULL DEFAULT 0;

UPDATE `pymerp`.`tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 1
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (14, 21, 38, 45, 48);

UPDATE `pymerp`.`tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 2
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (13, 20);

UPDATE `pymerp`.`tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 3
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (44);

UPDATE `pymerp`. `tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 4
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (25, 26, 27, 28, 39, 40, 51, 52);

UPDATE `pymerp`. `tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 5
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (53, 54, 55, 56);

UPDATE `pymerp`.`tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 6
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (1, 3, 4, 5, 8, 15, 16, 17, 18, 19, 22, 32, 33, 49);

UPDATE `pymerp`.`tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 7
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (2, 6, 16, 23, 34, 43);

UPDATE `pymerp`.`tb_tipo_movimiento_logistico`
SET `dc_tipo_operacion` = 8
WHERE `tb_tipo_movimiento_logistico`.`dc_tipo_movimiento` IN (30, 31, 46, 47);



ALTER TABLE `tb_movimiento_bodega`
ADD `dc_devolucion_guia_despacho_proveedor` INT UNSIGNED NULL DEFAULT NULL
AFTER `dc_devolucion_guia_despacho`,
ADD INDEX (`dc_devolucion_guia_despacho_proveedor`) ;

ALTER TABLE `tb_movimiento_bodega`
ADD CONSTRAINT `FK_DEVOLUCION_GUIA_DESPACHO_PROVEEDOR`
FOREIGN KEY (`dc_devolucion_guia_despacho_proveedor`)
REFERENCES `pymerp`.`tb_devolucion_guia_despacho_proveedor`(`dc_devolucion`)
ON DELETE RESTRICT ON UPDATE RESTRICT;


UPDATE tb_movimiento_logistico
SET dc_tipo_movimiento = 23
WHERE dc_tipo_movimiento = 22
AND dc_bodega_entrada > 0
AND dc_empresa = 5;

UPDATE `pymerp`.`tb_tipo_guia_recepcion` 
SET `dc_tipo_movimiento` = '23' 
WHERE `tb_tipo_guia_recepcion`.`dc_tipo` = 10;

UPDATE `pymerp`.`tb_configuracion_logistica` 
SET `dc_tipo_movimiento_ajuste_entrada` = '25', `dc_tipo_movimiento_ajuste_salida` = '26' 
WHERE `tb_configuracion_logistica`.`dc_empresa` = 1;

UPDATE `pymerp`.`tb_configuracion_logistica` 
SET `dc_tipo_movimiento_ajuste_entrada` = '39', `dc_tipo_movimiento_ajuste_salida` = '40' 
WHERE `tb_configuracion_logistica`.`dc_empresa` = 6;

INSERT INTO `pymerp`.`tb_tipo_movimiento_logistico` (`dc_tipo_movimiento`, `dg_tipo_movimiento`, `dg_codigo`, `dm_genera_asiento`, `dc_tipo_movimiento_contable`, `df_creacion`, `dc_usuario_creacion`, `dm_activo`, `dc_empresa`, `dc_tipo_operacion`) 
VALUES (NULL, 'Entrada por ajuste', 'AJIN', '0', '0', '2012-01-11 08:47:17', '2', '1', '5', '0');

INSERT INTO `pymerp`.`tb_tipo_movimiento_logistico` (`dc_tipo_movimiento`, `dg_tipo_movimiento`, `dg_codigo`, `dm_genera_asiento`, `dc_tipo_movimiento_contable`, `df_creacion`, `dc_usuario_creacion`, `dm_activo`, `dc_empresa`, `dc_tipo_operacion`) 
VALUES (NULL, 'Salida por ajuste', 'AJOUT', '0', '0', '2012-01-11 08:47:29', '2', '1', '5', '0');

UPDATE `pymerp`.`tb_configuracion_logistica` 
SET `dc_tipo_movimiento_ajuste_entrada` = '51', `dc_tipo_movimiento_ajuste_salida` = '52' 
WHERE `tb_configuracion_logistica`.`dc_empresa` = 5;


UPDATE `pymerp`.`tb_movimiento_bodega`
SET `dc_guia_despacho_proveedor` = 'dc_guia_despacho', `dc_guia_despacho` = '0'
WHERE  `tb_movimiento_bodega`.`dc_tipo_movimiento` = '44'
AND `tb_movimiento_bodega`.`dc_bodega_salida` = '1'
AND `tb_movimiento_bodega`.`dc_empresa` = '1'


ALTER TABLE `tb_factura_compra_detalle`  
ADD `dc_recepcionado` INT NOT NULL DEFAULT '0'  
AFTER `dc_cantidad`;


-----------------------------------------
Codigo desechado - caca caca
tipo operacion antiguo:

    1           La fecha se obtiene desde la misma fecha de creaci�n del movimiento
    2           La fecha se obtiene de la devoluci�n de la gu�a de despacho de cliente.
    3           La fecha se obtiene de la emisi�n de una gu�a de despacho a proveedor
    4           La fecha se obtiene de la emisi�n del ajuste de cantidades
    5           La fecha se obtiene de la emisi�n de la devoluci�n de la gu�a de despacho de proveedor
    6           La fecha se obtendr� de la emisi�n de la gu�a de despacho a cliente
    7           Se utilizar� la fecha de emisi�n de la factura de compra
    8           Se utlizar� la fecha de emisi�n de la nota de credito
    default     Se solicitar� proporcionar la fecha manualmente



1   La fecha tiene que ser la del movimiento en el mismo dia, si tiene un movimiento relacionado
    correspondiente a la misma bodega. De otro modo, preguntar

    Tipos de movimiento:

        14, 21, 38

2.- Tiene que utilizar la fecha de emisi�n de su propio documento o de su

    Tipos de movimiento:
        1, 2, 4, 5, 6, 8, 9, 15, 16, 22, 27, 28, 30, 31, 33, 34, 44, 49

3.- preguntar siempre la fecha

    Tipos de movimiento:
        3, 19
        *   En el caso del tipo de movimiento 3: si corresponde a una gu�a por traslado a cliente se tomar�
            la fecha de la emisi�n de la gu�a de despacho.

4.- Consultar la fecha a la Factura de venta

    Tipos de movimiento:
        17, 18, 32

5.- Consultar la fecha a la Guia de despacho

    Tipos de movimiento:


6.- Consultar la fecha en la Factura de Compra relacionada

    Tipos de movimiento:
    20, 43

7.- Consultar la fecha a la Nota de Cr�dito relacionada.

    Tipos de movimiento:
    47, 46
